<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Page language="c#" Codebehind="OrderSurveyForm.aspx.cs" AutoEventWireup="false" Inherits="ROS.OrderSurveyForm" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Order Survey</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<SCRIPT language="javascript"> 
			function ScrollIt()
			{
				window.scrollTo(document.OrderSurvey.PageX.value, document.OrderSurvey.PageY.value);
			}
			function setcoords()
			{
				var myPageX;
				var myPageY;
				if (document.all)
				{
					myPageX = document.body.scrollLeft;
					myPageY = document.body.scrollTop;
				}
				else
				{
					myPageX = window.pageXOffset;
					myPageY = window.pageYOffset;
				}
				document.OrderSurvey.PageX.value = myPageX;
				document.OrderSurvey.PageY.value = myPageY;
			}
		</SCRIPT>
	</HEAD>
	<body onscroll="javascript:setcoords()" onload="javascript:ScrollIt()">
		<form id="OrderSurvey" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language2" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="Sitepositionbar2" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<TABLE class="grid_table" id="tblCriteria" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															<asp:label id="lblDates" runat="server" Width="100%" Font-Bold="True">Dates</asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" id="Table13" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="WIDTH: 108px" vAlign="bottom"><asp:label id="lblSearchFor" runat="server">Search for</asp:label><BR>
																		<asp:dropdownlist id="ddlstDeliveryStatus" runat="server"></asp:dropdownlist></TD>
																	<TD style="WIDTH: 184px" vAlign="bottom"><asp:label id="lblProduct" runat="server">Product</asp:label><BR>
																		<asp:dropdownlist id="ddlProducts" runat="server"></asp:dropdownlist></TD>
																	<TD vAlign="bottom" style="WIDTH: 199px"><asp:label id="lblDateFrom" runat="server">Date from</asp:label><BR>
																		<asp:textbox id="txtDateFrom" runat="server" MaxLength="10"></asp:textbox>&nbsp;<INPUT class="popup" onclick="PopupPicker('txtDateFrom', 250, 250);" type="button" value="...">&nbsp;
																		<asp:customvalidator id="ValidatorDateFromFormat" runat="server" ErrorMessage="CustomValidator" ControlToValidate="txtDateFrom">*</asp:customvalidator></TD>
																	<TD vAlign="bottom"><asp:label id="lblDateTo" runat="server">Date to</asp:label><BR>
																		<asp:textbox id="txtDateTo" runat="server" MaxLength="10"></asp:textbox>&nbsp;<INPUT class="popup" onclick="PopupPicker('txtDateTo', 250, 250);" type="button" value="...">&nbsp;
																		<asp:comparevalidator id="ValidatorDateMismatch" runat="server" ErrorMessage="Prufan" ControlToValidate="txtDateTo"
																			Operator="GreaterThanEqual" Type="Date" ControlToCompare="txtDateFrom">*</asp:comparevalidator><asp:customvalidator id="ValidatorDateToFormat" runat="server" ErrorMessage="CustomValidator" ControlToValidate="txtDateTo">*</asp:customvalidator></TD>
																</TR>
																<tr>
																	<td height="23" style="WIDTH: 108px"></td>
																</tr>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<TABLE class="grid_table" id="CustomerInfo" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															<asp:label id="lblCustomer" runat="server" Width="100%" Font-Bold="True">Customer</asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" id="tblCustomerFrame" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<TABLE id="Table15" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 40px"><asp:radiobutton id="rbtnCustomerCompany" runat="server" CssClass="radio" Checked="True" GroupName="CustomerType"
																						AutoPostBack="True" Text=" "></asp:radiobutton>
																					<asp:image id="imgCustomerCompany" runat="server" imageurl="../img/company.gif"></asp:image></TD>
																				<TD><asp:radiobutton id="rbtnCustomerSubject" runat="server" CssClass="radio" GroupName="CustomerType"
																						AutoPostBack="True" Text=" "></asp:radiobutton>
																					<asp:image id="imgCustomerIndividual" runat="server" imageurl="../img/individual.gif"></asp:image></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 40px">&nbsp;</TD>
																				<TD>&nbsp;</TD>
																			</TR>
																		</TABLE>
																		<TABLE id="Table14" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 124px"><asp:label id="lblCustomerIDNumer" runat="server">ID Number</asp:label><BR>
																					<asp:textbox id="txtCustomerIDNumber" runat="server" MaxLength="100"></asp:textbox></TD>
																				<TD style="WIDTH: 215px"><asp:label id="lblCustomerIDNumberType" runat="server">Type</asp:label><BR>
																					<asp:dropdownlist id="ddlstCustomerIDNumberType" runat="server"></asp:dropdownlist></TD>
																				<TD style="WIDTH: 224px"><asp:label id="lblCustomerFirstNameNative" runat="server">First name</asp:label><BR>
																					<asp:textbox id="txtCustomerFirstNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																				<TD><asp:label id="lblCustomerLastNameNative" runat="server">Last name</asp:label><BR>
																					<asp:textbox id="txtCustomerLastNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																			</TR>
																			<tr>
																				<td height="23">&nbsp;</td>
																			</tr>
																		</TABLE>
																	</TD>
																</TR>
															</TABLE>
															<asp:label id="lblCustomerErrorMessage" runat="server" Width="100%" ForeColor="Red">ErrorMessage</asp:label>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<TABLE class="grid_table" id="tblSubjectInfo" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															<asp:label id="lblSubject" runat="server" Width="134px" Font-Bold="True">Subject</asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" id="tblSubjectframe" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<TABLE id="Table17" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 39px"><asp:radiobutton id="rbtnSubjectCompany" runat="server" Checked="True" GroupName="SubjectType" AutoPostBack="True"
																						CssClass="radio" Text=" "></asp:radiobutton>
																					<asp:image id="imgSubjectCompany" runat="server" imageurl="../img/company.gif"></asp:image></TD>
																				<TD><asp:radiobutton id="rbtnSubjectIndividual" runat="server" GroupName="SubjectType" AutoPostBack="True"
																						CssClass="radio" Text=" "></asp:radiobutton>
																					<asp:image id="imgSubjectIndividual" runat="server" imageurl="../img/individual.gif"></asp:image></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 39px">&nbsp;</TD>
																				<TD>&nbsp;</TD>
																			</TR>
																		</TABLE>
																		<TABLE id="Table18" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 122px"><asp:label id="lblSubjectIDNumber" runat="server">ID Number</asp:label><BR>
																					<asp:textbox id="txtSubjectIDNumber" runat="server" MaxLength="100"></asp:textbox></TD>
																				<TD style="WIDTH: 215px"><asp:label id="lblSubjectIDNumberType" runat="server">Type</asp:label><BR>
																					<asp:dropdownlist id="ddlstSubjectIDNumberType" runat="server"></asp:dropdownlist></TD>
																				<TD style="WIDTH: 230px"><asp:label id="lblSubjectFirstNameNative" runat="server">First name</asp:label><BR>
																					<asp:textbox id="txtSubjectFirstNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																				<TD><asp:label id="lblSubjectLastNameNative" runat="server">Last name</asp:label><BR>
																					<asp:textbox id="txtSubjectLastNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																			</TR>
																			<tr>
																				<td height="23">&nbsp;</td>
																			</tr>
																		</TABLE>
																	</TD>
																</TR>
															</TABLE>
															<P>&nbsp;</P>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD>
															<TABLE id="Table19" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="WIDTH: 648px">
																		<asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary>
																		<asp:label id="lblSubjectErrorMessage" runat="server" Width="584px" ForeColor="Red">ErrorMessage</asp:label>
																		<asp:label id="lblOrderErrorMessage" runat="server" Width="100%" ForeColor="Red">ErrorMessage</asp:label>
																	</TD>
																	<TD align="right"><asp:button id="btnSearch" runat="server" CssClass="confirm_button" Text="Search"></asp:button></TD>
																</TR>
															</TABLE>
															<TABLE class="grid_table" id="tblOrderDataGrid" cellSpacing="0" cellPadding="0" runat="server">
																<tr>
																	<td height="10"></td>
																</tr>
																<TR>
																	<td align="right">
																		<div><IMG alt="Select" src="../img/select.gif" align="middle">&nbsp;=&nbsp;<asp:label id="Label1" runat="server">Select</asp:label></div>
																	</td>
																</TR>
																<tr>
																	<td height="10"></td>
																</tr>
																<TR>
																	<TH>
																		<asp:label id="lblOrders" runat="server">Ord</asp:label>
																	</TH>
																</TR>
																<TR>
																	<TD width="100%">
																		<DIV class="datagrid" id="DivOrders" style="OVERFLOW-X: auto; OVERFLOW: auto; CLIP: rect(auto auto auto auto); HEIGHT: 250px"
																			runat="server"><asp:datagrid id="dgOrderSurvey" runat="server" Width="100%" CellPadding="4" BorderWidth="1px"
																				AutoGenerateColumns="False" GridLines="None">
																				<FooterStyle CssClass="grid_footer"></FooterStyle>
																				<SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
																				<AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
																				<ItemStyle CssClass="grid_item"></ItemStyle>
																				<HeaderStyle CssClass="grid_header"></HeaderStyle>
																				<Columns>
																					<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																						CommandName="Order">
																						<ItemStyle CssClass="leftpadding"></ItemStyle>
																					</asp:ButtonColumn>
																					<asp:BoundColumn Visible="False" DataField="OrderID" HeaderText="OrderID">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="Date" HeaderText="Date">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="DispatchDeadline" HeaderText="Dispatch Deadline">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:ButtonColumn DataTextField="CustomerNameEN" HeaderText="CustomerNameEN" CommandName="Customer">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:ButtonColumn>
																					<asp:ButtonColumn Visible="False" DataTextField="CustomerNameNative" HeaderText="CustomerNameNative"
																						CommandName="Customer">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:ButtonColumn>
																					<asp:BoundColumn Visible="False" DataField="CustomerID" HeaderText="Customer ID">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="CustomerIsCompany" HeaderText="CustomerIsCompany">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:ButtonColumn DataTextField="SubjectNameEN" HeaderText="SubjectNameEN" CommandName="Subject">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:ButtonColumn>
																					<asp:ButtonColumn Visible="False" DataTextField="SubjectNameNative" HeaderText="SubjectNameNative"
																						CommandName="Subject">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:ButtonColumn>
																					<asp:BoundColumn Visible="False" DataField="SubjectID" HeaderText="Subject ID">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="SubjectIsCompany" HeaderText="SubjectIsCompany">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="RemarksEN" HeaderText="RemarksEN">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="RemarksNative" HeaderText="RemarksNative">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="CustomerRefNo" HeaderText="Customer Ref. No.">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="ProductEN" HeaderText="ProductEN">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="ProductNative" HeaderText="ProductNative">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="DeliveryStatus" HeaderText="DeliveryStatus">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle CssClass="grid_pager"></PagerStyle>
																			</asp:datagrid>
																		</DIV>
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<TABLE class="grid_table" id="Orders" cellSpacing="0" cellPadding="0">
													<TR>
														<TD>
															<TABLE class="fields" id="tblOrderForm" cellSpacing="0" cellPadding="0" border="0">
															</TABLE>
															<P>&nbsp;</P>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td>
												<INPUT id="PageX" type="hidden" value="0" name="PageX" runat="server"> <INPUT id="PageY" type="hidden" value="0" name="PageY" runat="server">
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
