﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderForm.aspx.cs" Inherits="cb3.ROS.OrderForm" %>

<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc2" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register src="../new_user_controls/panelBar.ascx" tagname="panelbar" tagprefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Order</title>

    <script language="JavaScript" src="../DatePicker.js"></script>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="Order" method="post" runat="server">
    <table height="600" width="997" align="center" border="0">
        <tr>
            <td colspan="4">
                <uc1:head ID="Head1" runat="server"></uc1:head>
            </td>
        </tr>
        <tr valign="top">
            <td width="1">
            </td>
            <td>
                <table height="100%" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <uc1:language ID="Language1" runat="server"></uc1:language>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <ucl:options ID="Options1" runat="server"></ucl:options>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#000000" colspan="3" height="1">
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <uc1:sitePositionBar ID="SitePositionBar1" runat="server"></uc1:sitePositionBar>
                        </td>
                        <td align="right">
                            <ucl:UserInfo ID="UserInfo1" runat="server"></ucl:UserInfo>
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td valign="top" align="left" width="150">
                            <table width="98%">
                                <tr>
                                    <td>
                                        &nbsp;
                                    <uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2">
                            <!-- Main Body Starts -->
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblHeader" runat="server">Order</asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 380px">
                                                                <asp:Label ID="lblProduct" runat="server">Product</asp:Label><br>
                                                                <asp:DropDownList ID="ddlProducts" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblOrderDate" runat="server" Width="100%">Order Date</asp:Label><br>
                                                                <asp:TextBox ID="txtOrderDate" runat="server" MaxLength="10"></asp:TextBox>&nbsp;<input
                                                                    onclick="PopupPicker('txtOrderDate', 250, 250);" type="button" value="..." class="popup">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorOrderDate" runat="server" ErrorMessage="RequiredFieldValidator"
                                                                    ControlToValidate="txtOrderDate">*</asp:RequiredFieldValidator><asp:CustomValidator
                                                                        ID="CustomValidatorOrderDateFormat" runat="server" ErrorMessage="CustomValidator"
                                                                        ControlToValidate="txtOrderDate">*</asp:CustomValidator>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblDispatchDeadline" runat="server" Width="100%">Dispatch Deadline</asp:Label><br>
                                                                <asp:TextBox ID="txtDispatchDeadline" runat="server" MaxLength="10"></asp:TextBox>&nbsp;<input
                                                                    onclick="PopupPicker('txtDispatchDeadline', 250, 250);" type="button" value="..."
                                                                    class="popup">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDispatchDeadline" runat="server"
                                                                    ErrorMessage="RequiredFieldValidator" ControlToValidate="txtDispatchDeadline">*</asp:RequiredFieldValidator><asp:CompareValidator
                                                                        ID="CompareValidatorDateMismatch" runat="server" ErrorMessage="CompareValidator"
                                                                        ControlToValidate="txtDispatchDeadline" ControlToCompare="txtOrderDate" Type="Date"
                                                                        Operator="GreaterThanEqual">*</asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblDeliverySpeed" runat="server" Width="120px">Delivery Speed</asp:Label><br>
                                                                <asp:DropDownList ID="ddlDeliverySpeed" runat="server" AutoPostBack="True" DataValueField="ID"
                                                                    DataTextField="DeliverySpeedEN">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOrderType" runat="server">Order Type</asp:Label><br>
                                                                <asp:DropDownList ID="ddlstOrderType" runat="server" DataValueField="ID" DataTextField="TypeEN">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 380px">
                                                                <asp:Label ID="lblOrderComment" runat="server">Order comment:</asp:Label><br>
                                                                <asp:TextBox ID="txtOrderComment" runat="server" Enabled="False" Width="400px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblRemarksNative" runat="server">Remarks Native</asp:Label><br>
                                                                <asp:TextBox ID="txtRemarksNative" runat="server" Width="350px" MaxLength="1024"
                                                                    TextMode="MultiLine" Height="104px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRemarksEN" runat="server">Remarks (EN)</asp:Label><br>
                                                                <asp:TextBox ID="txtRemarksEN" runat="server" Width="350px" MaxLength="1024" TextMode="MultiLine"
                                                                    Height="104px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblClientReferenceNumber" runat="server">Client Reference Number</asp:Label><br>
                                                                <asp:TextBox ID="txtClientReferenceNumber" runat="server" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblCustomer" runat="server">Customer</asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 190px">
                                                                <asp:Label ID="lblCustomerIDNumber" runat="server">ID Number</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerIDNumber" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 190px">
                                                                <asp:Label ID="lblCustomerCIID" runat="server" Visible="False">CIID</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerCIIDNumber" runat="server" Enabled="False" ReadOnly="True"
                                                                    Visible="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 190px">
                                                                <asp:Label ID="lblCustomerNameNative" runat="server">Name</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerNameNative" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCustomerNameEN" runat="server">Name (EN)</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerNameEN" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblCustomerFirstNameNative" runat="server">First name</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerFirstNameNative" runat="server" Enabled="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCustomerFirstNameEN" runat="server">First name (EN)</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerFirstNameEN" runat="server" Enabled="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCustomerLastNameNative" runat="server">Last name</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerLastNameNative" runat="server" Enabled="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCustomerLastNameEN" runat="server">Last name (EN)</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerLastNameEN" runat="server" Enabled="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblCustomerAddressNative" runat="server">Address</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerAddressNative" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCustomerAddressEN" runat="server">Address (EN)</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerAddressEN" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCustomerPostalCode" runat="server">Postal Code</asp:Label><br>
                                                                <asp:TextBox ID="txtCustomerPostalCode" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCustomerCity" runat="server">City</asp:Label><br>
                                                                <asp:DropDownList ID="ddlstCustomerCity" runat="server" Enabled="False">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblCustomerCountryNative" runat="server">Country</asp:Label><br>
                                                                <asp:DropDownList ID="ddlstCustomerCountry" runat="server" Enabled="False">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblSubject" runat="server" Font-Bold="True">Subject</asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 190px">
                                                                <asp:Label ID="lblSubjectIDNumber" runat="server">ID Number</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectIDNumber" runat="server" Width="140px" Enabled="False"
                                                                    ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 190px">
                                                                <asp:Label ID="lblSubjectCIID" runat="server" Visible="False">CIID</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectCIIDNumber" runat="server" Width="140px" Enabled="False"
                                                                    ReadOnly="True" Visible="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 190px">
                                                                <asp:Label ID="lblSubjectNameNative" runat="server">Name</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectNameNative" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSubjectNameEN" runat="server">Name (EN)</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectNameEN" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblSubjectFirstNameNative" runat="server">First name</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectFirstNameNative" runat="server" Enabled="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSubjectFirstNameEN" runat="server">First name (EN)</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectFirstNameEN" runat="server" Enabled="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSubjectLastNameNative" runat="server">Last name</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectLastNameNative" runat="server" Enabled="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSubjectLastNameEN" runat="server">Last name (EN)</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectLastNameEN" runat="server" Enabled="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblSubjectAddressNative" runat="server">Address</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectAddressNative" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSubjectAddressEN" runat="server">Address (EN)</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectAddressEN" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSubjectPostalCode" runat="server">Postal Code</asp:Label><br>
                                                                <asp:TextBox ID="txtSubjectPostalCode" runat="server" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSubjectCity" runat="server">City</asp:Label><br>
                                                                <asp:DropDownList ID="ddlstSubjectCity" runat="server" Enabled="False">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblSubjectCountryNative" runat="server">Country</asp:Label><br>
                                                                <asp:DropDownList ID="ddlstSubjectCountry" runat="server" Enabled="False">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="empty_table" cellspacing="0">
                                            <tr valign="top">
                                                <td align="left">
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
                                                    <br>
                                                    <asp:Label ID="lblErrorMessage1" runat="server" Width="100%" ForeColor="Red">Error message</asp:Label>
                                                </td>
                                                <td align="right">
                                                    <asp:Button ID="btnDelete" runat="server" CssClass="cancel_button" Text="Delete Order">
                                                    </asp:Button><asp:Button ID="btnDelivered" runat="server" CssClass="gray_button"
                                                        Text="Delivered"></asp:Button><asp:Button ID="btnSubmitOrder" runat="server" CssClass="confirm_button"
                                                            Text="Submit Order"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- Main Body Ends -->
                        </td>
                    </tr>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#000000" colspan="3" height="1">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="2">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <uc1:footer ID="Footer1" runat="server"></uc1:footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>