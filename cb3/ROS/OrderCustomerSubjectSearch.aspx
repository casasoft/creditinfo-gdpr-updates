<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="OrderCustomerSubjectSearch.aspx.cs" AutoEventWireup="false" Inherits="ROS.OrderClientSubjectSearch" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Order Report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="javascript"> 
	function ScrollIt()
	{
		window.scrollTo(document.OrderCustomerSubjectSearch.PageX.value, document.OrderCustomerSubjectSearch.PageY.value);
    }
	function setcoords()
	{
		var myPageX;
		var myPageY;
		if (document.all)
		{
			myPageX = document.body.scrollLeft;
			myPageY = document.body.scrollTop;
        }
		else
		{
			myPageX = window.pageXOffset;
			myPageY = window.pageYOffset;
        }
		document.OrderCustomerSubjectSearch.PageX.value = myPageX;
		document.OrderCustomerSubjectSearch.PageY.value = myPageY;
    }
		</script>
	</HEAD>
	<body onscroll="javascript:setcoords()" onload="javascript:ScrollIt()">
		<form id="OrderCustomerSubjectSearch" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts --><input id="PageX" type="hidden" value="0" name="PageX" runat="server">
									<input id="PageY" type="hidden" value="0" name="PageY" runat="server">
									<table width="100%">
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblCustomerInformation" runat="server">Customer Information</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria1" cellSpacing="0" cellPadding="0">
																<tr>
																	<td colSpan="4"><asp:radiobutton id="rbtnCustomerIndividual" runat="server" cssclass="radio" autopostback="True"
																			groupname="CustomerType"></asp:radiobutton><asp:image id="imgCustomerIndividual" runat="server" imageurl="../img/individual.gif"></asp:image><asp:radiobutton id="rbtnCustomerCompany" runat="server" cssclass="radio" autopostback="True" groupname="CustomerType"
																			checked="True"></asp:radiobutton><asp:image id="imgCustomerCompany" runat="server" imageurl="../img/company.gif"></asp:image></td>
																</tr>
																<tr>
																	<td style="WIDTH: 123px"><asp:label id="lblCustomerIDNumber" runat="server" width="88px">ID Number</asp:label><br>
																		<asp:textbox id="txtCustomerIDNumber" runat="server" maxlength="100"></asp:textbox></td>
																	<td style="WIDTH: 159px"><asp:label id="lblCustomerIDNumberType" runat="server">Type</asp:label><br>
																		<asp:dropdownlist id="ddlstCustomerIDNumberType" runat="server"></asp:dropdownlist></td>
																	<td style="WIDTH: 161px"><asp:label id="lblCustomerFirstNameNative" runat="server" width="88px">First name</asp:label><BR>
																		<asp:textbox id="txtCustomerFirstNameNative" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblCustomerLastNameNative" runat="server" width="64px">Last name</asp:label><BR>
																		<asp:textbox id="txtCustomerLastNameNative" runat="server" maxlength="50"></asp:textbox></td>
																</tr>
																<TR>
																	<TD style="WIDTH: 123px"><asp:label id="lblCustomerAddressNative" runat="server">Address</asp:label><BR>
																		<asp:textbox id="txtCustomerAddressNative" runat="server" maxlength="100"></asp:textbox></TD>
																	<TD style="WIDTH: 159px"><asp:label id="lblCustomerPostalCode" runat="server" width="88px">Postal Code</asp:label><BR>
																		<asp:textbox id="txtCustomerPostalCode" runat="server" maxlength="30"></asp:textbox></TD>
																	<TD style="WIDTH: 161px"><asp:label id="lblCustomerCity" runat="server">City</asp:label><BR>
																		<asp:dropdownlist id="ddlistCustomerCity" runat="server"></asp:dropdownlist></TD>
																	<TD align="right"><asp:button id="btnFindCustomer" runat="server" cssclass="search_button" text="Search"></asp:button><BR>
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 123px"><asp:label id="lblCustomerCIID" runat="server" visible="False">CIID</asp:label><BR>
																		<asp:textbox id="txtCustomerCIID" runat="server" maxlength="10" visible="False"></asp:textbox></TD>
																	<TD style="WIDTH: 159px"><asp:label id="lblCustomerCountry" runat="server">Country</asp:label><BR>
																		<asp:dropdownlist id="ddlstCustomerCountry" runat="server" datatextfield="NameNative" datavaluefield="CountryID"></asp:dropdownlist></TD>
																	<td style="WIDTH: 161px" align="right"></td>
																	<td></td>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="grid_table" id="outerGridTableCustomer" cellSpacing="0" cellPadding="0" runat="server">
													<TR>
														<TD align="right"><IMG alt="Select" src="../img/select.gif" align="middle">&nbsp;=&nbsp;
															<asp:label id="lblSelectIcon" runat="server">Select</asp:label></TD>
													</TR>
													<TR>
														<TD height="5"></TD>
													</TR>
													<TR>
														<TH>
															<asp:label id="lblCustomerDataGrid" runat="server"></asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="datagrid" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD>
																		<DIV class="TA" runat="server" id="divCustomers" style="OVERFLOW-X: auto; OVERFLOW: auto; CLIP: rect(auto auto auto auto); HEIGHT: 250px"><asp:datagrid id="dgCustomer" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="CreditInfoID" headertext="CreditInfoID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="IDNumber" headertext="ID Number">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="IDNumberType" headertext="ID Number Type">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="FirstNameEN" headertext="FirstNameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="FirstNameNative" headertext="FirstNameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="LastNameEN" headertext="LastNameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="LastNameNative" headertext="LastNameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="AddressEN" headertext="Address">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="AddressNative" headertext="AddressNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CityEN" headertext="City">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CityNative" headertext="City">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityID" headertext="City ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="PostalCode" headertext="Postal Code">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CountryEN" headertext="Country">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CountryNative" headertext="Country">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CountryID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="IsCompany">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></DIV>
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblCustomerErrorMessage" runat="server" Font-Bold="True"></asp:label></td>
														<td align="right"><asp:button id="btnCustomerClear" runat="server" cssclass="cancel_button" text="Clear"></asp:button>&nbsp;
															<asp:button id="btnCreateNewCustomer" runat="server" cssclass="gray_button" text="Create New Customer"></asp:button>&nbsp;
														</td>
													</tr>
												</TABLE>
											</TD>
										</TR>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblSubjectInformation" runat="server" width="100%" font-bold="True">Subject Information</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td colSpan="4"><asp:radiobutton id="rbtnSubjectIndividual" runat="server" cssclass="radio" autopostback="True" groupname="SubjectType"></asp:radiobutton><asp:image id="imgSubjectIndividual" runat="server" imageurl="../img/individual.gif"></asp:image><asp:radiobutton id="rbtnSubjectCompany" runat="server" cssclass="radio" autopostback="True" groupname="SubjectType"
																			checked="True"></asp:radiobutton><asp:image id="imgSubjectCompany" runat="server" imageurl="../img/company.gif"></asp:image></td>
																</tr>
																<tr>
																	<td style="WIDTH: 120px"><asp:label id="lblSubjectRegistrationNumber" runat="server" width="80px">ID Number:</asp:label><br>
																		<asp:textbox id="txtSubjectIDNumber" runat="server" maxlength="100"></asp:textbox></td>
																	<td style="WIDTH: 167px"><asp:label id="lblSubjectIDNumberType" runat="server">Type</asp:label><br>
																		<asp:dropdownlist id="ddlstSubjectIDNumberType" runat="server"></asp:dropdownlist></td>
																	<td style="WIDTH: 169px"><asp:label id="lblSubjectFirstNameNative" runat="server" width="96px">First name</asp:label><BR>
																		<asp:textbox id="txtSubjectFirstNameNative" runat="server" maxlength="50"></asp:textbox></td>
																	<TD><asp:label id="lblSubjectLastNameNative" runat="server" width="80px">Last name</asp:label><BR>
																		<asp:textbox id="txtSubjectLastNameNative" runat="server" maxlength="50"></asp:textbox></TD>
																</tr>
																<tr>
																	<td style="WIDTH: 120px"><asp:label id="lblSubjectAddressNative" runat="server">Address</asp:label><br>
																		<asp:textbox id="txtSubjectAddressNative" runat="server" maxlength="100"></asp:textbox></td>
																	<td style="WIDTH: 167px"><asp:label id="lblSubjectPostalCode" runat="server" width="88px">Postal Code:</asp:label><br>
																		<asp:textbox id="txtSubjectPostalCode" runat="server" maxlength="30"></asp:textbox></td>
																	<td style="WIDTH: 169px"><asp:label id="lblSubjectCity" runat="server">City</asp:label><BR>
																		<asp:dropdownlist id="ddlstSubjectCity" runat="server"></asp:dropdownlist></td>
																	<TD align="right"><BR>
																		<asp:button id="btnFindSubject" runat="server" cssclass="search_button" text="Search"></asp:button></TD>
																</tr>
																<TR>
																	<TD style="WIDTH: 120px"><asp:label id="lblSubjectCIID" runat="server" visible="False">CIID</asp:label><BR>
																		<asp:textbox id="txtSubjectCIID" runat="server" maxlength="10" visible="False"></asp:textbox></TD>
																	<TD style="WIDTH: 167px"><asp:label id="lblSubjectCountry" runat="server">Country</asp:label><BR>
																		<asp:dropdownlist id="ddlstSubjectCountry" runat="server" datatextfield="NameNative" datavaluefield="CountryID"></asp:dropdownlist></TD>
																	<TD align="right"></TD>
																	<td></td>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="grid_table" id="outerGridTableSubject" cellSpacing="0" cellPadding="0" runat="server">
													<TR>
														<TD align="right"><IMG alt="Select" src="../img/select.gif" align="middle">&nbsp;=&nbsp;
															<asp:label id="lblSelectIcon2" runat="server">Select</asp:label></TD>
													</TR>
													<TR>
														<TD height="5"></TD>
													</TR>
													<TR>
														<TH style="HEIGHT: 25px">
															<asp:label id="lblSubjectDataGrid" runat="server"></asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="datagrid" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD>
																		<DIV class="TA" id="divSubjects" runat="server" style="OVERFLOW-X: auto; OVERFLOW: auto; CLIP: rect(auto auto auto auto); HEIGHT: 250px"><asp:datagrid id="dgSubject" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="CreditInfoID" headertext="CreditInfoID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="IDNumber" headertext="ID Number">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="IDNumberType" headertext="ID Number Type">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="FirstNameEN" headertext="FirstNameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="FirstNameNative" headertext="FirstNameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="LastNameEN" headertext="LastNameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="LastNameNative" headertext="LastNameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="AddressEN" headertext="AddressEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="AddressNative" headertext="AddressNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CityEN" headertext="CityEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CityNative" headertext="CityNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityID" headertext="CityID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="PostalCode" headertext="Postal Code">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CountryEN" headertext="CountryEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CountryNative" headertext="CountryNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CountryID" headertext="CountryID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="IsCompany" headertext="IsCompany">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></DIV>
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="empty_table" cellSpacing="0">
													<TR>
														<TD align="left"><asp:label class="error_text" id="lblSubjectErrorMessage" runat="server"></asp:label></TD>
														<TD align="right"><asp:button id="btnSubjectClear" runat="server" cssclass="cancel_button" text="Clear"></asp:button><asp:button id="btnCreateNewSubject" runat="server" cssclass="gray_button" text="Create New Subject"></asp:button>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="empty_table" cellSpacing="0">
													<TR>
														<TD align="left"></TD>
														<TD align="right"><asp:button id="btnNext" runat="server" cssclass="confirm_button" text="Next"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</table> <!-- Main Body Ends --></td>
							</tr>
							<TR>
								<TD height="20"></TD>
							</TR>
							<TR>
								<TD align="center" bgColor="#000000" colSpan="3" height="1"></TD>
							</TR>
						</table>
					</td>
					<TD width="2"></TD>
				</tr>
				<TR>
					<TD align="center" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
