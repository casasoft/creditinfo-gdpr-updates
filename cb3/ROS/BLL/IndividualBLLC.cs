using System;

namespace ROS.BLL {
    [Serializable]
    /// <summary>
        /// Summary description for Individual.
        /// </summary>
    public class IndividualBLLC : CustomerBLLC {
        public IndividualBLLC() {
            DateOfBirth = new DateTime(1900, 1, 1);
            FirstNameNative = "";
            FirstNameEN = "";
            LastNameNative = "";
            LastNameEN = "";
            EducationID = -1;
            ProfessionID = -1;

            NationalID = "";
            NationalName = "";
            NationalPostNumber = "";
            NationalAddress = "";
        }

        /// <summary>
        /// Gets/Sets the DateOfBirth attribute
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        public String FirstNameNative { get; set; }
        public String FirstNameEN { get; set; }
        public String LastNameNative { get; set; }
        public String LastNameEN { get; set; }
        public int EducationID { get; set; }
        public int ProfessionID { get; set; }
        public String NationalID { get; set; }
        public String NationalName { get; set; }
        public String NationalPostNumber { get; set; }
        public String NationalAddress { get; set; }
    }
}