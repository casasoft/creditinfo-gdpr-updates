using System;

namespace ROS.BLL {
    [Serializable]
    /// <summary>
        /// Summary description for PhoneNumber.
        /// </summary>
    public class PhoneNumberBLLC {
        public PhoneNumberBLLC() {
            NumberID = -1;
            NumberTypeID = -1;
            Number = "";
            CreditInfoID = -1;
            NumberTypeNative = "";
            NumberTypeEN = "";
        }

        public int NumberID { get; set; }
        public int NumberTypeID { get; set; }
        public String Number { get; set; }
        public int CreditInfoID { get; set; }
        public String NumberTypeNative { get; set; }
        public String NumberTypeEN { get; set; }
    }
}