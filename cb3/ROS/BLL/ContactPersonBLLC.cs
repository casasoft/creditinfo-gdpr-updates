using System;

namespace ROS.BLL {
    [Serializable]
    /// <summary>
        /// Summary description for ContactPersonBLLC.
        /// </summary>
    public class ContactPersonBLLC {
        public ContactPersonBLLC() {
            FirstNameEN = "";
            FirstNameNative = "";
            LastNameEN = "";
            LastNameNative = "";
            Email = "";
        }

        /// <summary>
        /// Gets/Sets the first name in english
        /// </summary>
        public String FirstNameEN { get; set; }

        /// <summary>
        /// Gets/Sets the first name in native language
        /// </summary>
        public String FirstNameNative { get; set; }

        /// <summary>
        /// Gets/Sets the last name in english
        /// </summary>
        public String LastNameEN { get; set; }

        /// <summary>
        /// Gets/Sets the last name in native language
        /// </summary>
        public String LastNameNative { get; set; }

        /// <summary>
        /// Gets/Sets the email address
        /// </summary>
        public String Email { get; set; }
    }
}