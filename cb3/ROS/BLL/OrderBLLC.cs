using System;

namespace ROS.BLL {
    [Serializable]
    /// <remarks>
        /// This class provides all neccessary info for Order
        /// </remarks>
    public class OrderBLLC {
        public OrderBLLC() {
            OrderID = -1;
            CustomerReferenceNumber = "";
            OrderDate = new DateTime();
            DispatchDeadline = new DateTime();
            DeliverySpeedID = -1;
            OrderTypeID = -1;
            Customer = null;
            Subject = null;
            RemarksEN = "";
            RemarksNative = "";
            Delivered = false;

            LastUpdate = new DateTime();
            Deleted = false;
            RegisteredByID = -1;
            Product = new ProductBLLC();
        }

        /// <summary>
        /// Gets/sets the orderID attribute
        /// </summary>
        public int OrderID { get; set; }

        /// <summary>
        /// Gets/Sets the customer reference number attribute
        /// </summary>
        public String CustomerReferenceNumber { get; set; }

        /// <summary>
        /// Gets/sets the orderDate attribute
        /// </summary>
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Gets/sets the dispatchDeadline attribute
        /// </summary>
        public DateTime DispatchDeadline { get; set; }

        /// <summary>
        /// Gets/sets the DeliverySpeed attribute
        /// </summary>
        public int DeliverySpeedID { get; set; }

        /// <summary>
        /// Gets/Sets the order type attribute
        /// </summary>
        public int OrderTypeID { get; set; }

        /// <summary>
        /// Gets/Sets the customer attribute
        /// </summary>
        public CustomerBLLC Customer { get; set; }

        /// <summary>
        /// Gets/Sets the subject attribute
        /// </summary>
        public CustomerBLLC Subject { get; set; }

        /// <summary>
        /// Gets/sets the remarks attribute in english
        /// </summary>
        public string RemarksEN { get; set; }

        /// <summary>
        /// Gets/sets the remarks attribute in native language
        /// </summary>
        public string RemarksNative { get; set; }

        /// <summary>
        /// Gets/sets the delivered attribute
        /// </summary>
        public bool Delivered { get; set; }

        public DateTime LastUpdate { get; set; }
        public bool Deleted { get; set; }
        public int RegisteredByID { get; set; }
        public ProductBLLC Product { get; set; }
        public string Comment { get; set; }
/*
		/// <summary>
		/// The clients reference number (national ID)
		/// </summary>
		private string clientReference;

		/// <summary>
		/// The clients internal system number (CIID)
		/// </summary>
		private string lTReference;

		/// <summary>
		/// The clients country
		/// </summary>
		private string country;

		/// <summary>
		/// The questioned company internal system id (CIID)
		/// </summary>
		private string questionedCompanyCIID;



		/// <summary>
		/// Gets/sets the faxEmail attribute
		/// </summary>
		public bool FaxEmail
		{
			get {return this.faxEmail;}
			set {this.faxEmail = value;}
		}

		/// <summary>
		/// Gets/sets the reportFromLTWeb attribute
		/// </summary>
		public bool ReportFromLTWeb
		{
			get {return this.reportFromLTWeb;}
			set {this.reportFromLTWeb = value;}
		}

		/// <summary>
		/// Gets/sets the orderFromLTWeb attribute
		/// </summary>
		public bool OrderFromLTWeb
		{
			get {return this.orderFromLTWeb;}
			set {this.OrderFromLTWeb = value;}
		}

		/// <summary>
		/// Gets/sets the clientReference attribute
		/// </summary>
		public string ClientReference
		{
			get {return this.clientReference;}
			set {this.clientReference = value;}
		}

		/// <summary>
		/// Gets/sets the lTReference attribute
		/// </summary>
		public string LTReference
		{
			get {return this.lTReference;}
			set {this.lTReference = value;}
		}

		/// <summary>
		/// Gets/sets the country attribute
		/// </summary>
		public string Country
		{
			get {return this.country;}
			set {this.country = value;}
		}

		/// <summary>
		/// Gets/sets the questionedCompanyCIID attribute
		/// </summary>
		public string QuestionedCompanyCIID
		{
			get {return this.questionedCompanyCIID;}
			set {this.questionedCompanyCIID = value;}
		}




*/
    } // END CLASS DEFINITION OrderBLLC
}