using System;

namespace ROS.BLL {
    [Serializable]
    /// <summary>
        /// Summary description for AddressBLLC.
        /// </summary>
    public class AddressBLLC {
        private readonly String _cityLocationEN;
        private String _cityNameEN;

        public AddressBLLC() {
            AddressID = -1;
            StreetNumber = -1;
            CityLocationNative = "";
            _cityLocationEN = "";
            OtherInfoNative = "";
            OtherInfoEN = "";
            StreetEN = "";
            StreetNative = "";
            CityID = -1;
            _cityNameEN = "";
            CityNameNative = "";
            PostalCode = "";
            CountryID = -1;
            CountryNameEN = "";
            CountryNameNative = "";
            InfoEN = "";
            InfoNative = "";
            PostBox = "";
        }

        public int AddressID { get; set; }
        public int StreetNumber { get; set; }
        public String CityLocationNative { get; set; }
        public String CityLocationEN { get { return _cityLocationEN; } set { _cityNameEN = value; } }
        public String OtherInfoNative { get; set; }
        public String OtherInfoEN { get; set; }

        /// <summary>
        /// Gets/sets the street name attribute in english
        /// </summary>
        public String StreetEN { get; set; }

        /// <summary>
        /// Gets/sets the street name attribute in native language
        /// </summary>
        public String StreetNative { get; set; }

        /// <summary>
        /// Gets/Sets the city name in english attribute
        /// </summary>
        public int CityID { get; set; }

        public String CityNameEN { get { return _cityNameEN; } set { _cityNameEN = value; } }
        public String CityNameNative { get; set; }

        /// <summary>
        /// Gets/Sets the postal code attribute
        /// </summary>
        public String PostalCode { get; set; }

        /// <summary>
        /// Gets/Sets the country ID attribute
        /// </summary>
        public int CountryID { get; set; }

        public String CountryNameEN { get; set; }
        public String CountryNameNative { get; set; }

        /// <summary>
        /// Gets/Sets the info attribute in english
        /// </summary>
        public String InfoEN { get; set; }

        /// <summary>
        /// Gets/Sets the info attribute in native language
        /// </summary>
        public String InfoNative { get; set; }

        /// <summary>
        /// Gets/Sets the postbox address attribute
        /// </summary>
        public String PostBox { get; set; }
    }
}