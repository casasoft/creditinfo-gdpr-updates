using System;

namespace ROS.BLL {
    /// <remarks>
    /// This class inherits CreditInfoUser and add Orders specific attributes
    /// </remarks>
    public class OrderCustomerBLLC {
        /// <summary>
        /// Gets/sets the email2 attribute
        /// </summary>
        public string Email2 { get; set; }

        /// <summary>
        /// Gets/sets the email3 attribute
        /// </summary>
        public string Email3 { get; set; }

        /// <summary>
        /// Gets/sets the contact1 attribute
        /// </summary>
        public string Contact1 { get; set; }

        /// <summary>
        /// Gets/sets the contact1Email attribute
        /// </summary>
        public string Contact1Email { get; set; }

        /// <summary>
        /// Gets/sets the contact2 attribute
        /// </summary>
        public string Contact2 { get; set; }

        /// <summary>
        /// Gets/sets the contact2Email attribute
        /// </summary>
        public string Contact2Email { get; set; }

        /// <summary>
        /// Gets/sets the contact3 attribute
        /// </summary>
        public string Contact3 { get; set; }

        /// <summary>
        /// Gets/sets the contact3Email attribute
        /// </summary>
        public string Contact3Email { get; set; }

        /// <summary>
        /// Gets/sets the remarks attribute
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Gets/sets the regDate attribute
        /// </summary>
        public DateTime RegDate { get; set; }
    } // END CLASS DEFINITION OrderCustomerBLLC
}