using System;
using System.Collections;
using System.Data;
using ROS.DAL;

namespace ROS.BLL {
    /// <summary>
    /// Summary description for FactoryBLLC.
    /// </summary>
    public class FactoryBLLC {
        private readonly ReportOrderingSystemDALC _rosDatabase;
        public FactoryBLLC() { _rosDatabase = new ReportOrderingSystemDALC(); }

        #region Company

        public int CreateCompany(CompanyBLLC theCompany) { return _rosDatabase.CreateCompany(theCompany); }
        public int UpdateCompany(CompanyBLLC theCompany) { return _rosDatabase.UpdateCompany(theCompany); }
        public int CountCompaniesInNationalAndCreditInfo(CompanyBLLC theCustomer) { return _rosDatabase.CountCompaniesInNationalAndCreditInfo(theCustomer); }
        public int CountCompanies(CompanyBLLC theCustomer) { return _rosDatabase.CountCompanies(theCustomer); }
        public ArrayList FindCompanyInNationalAndCreditInfo(CompanyBLLC theCustomer) { return _rosDatabase.FindCompanyInNationalAndCreditInfo(theCustomer); }
        public DataSet FindCompany(CompanyBLLC theCompany) { return _rosDatabase.FindCompany(theCompany); }
        public ArrayList FindCompanyAsArray(CompanyBLLC theCompany) { return _rosDatabase.FindCompanyAsArray(theCompany); }
        public CompanyBLLC GetCompany(String creditInfoID) { return _rosDatabase.GetCompany(creditInfoID); }

        #endregion

        #region Individual

        public ArrayList FindIndividualInNationalAndCreditInfo(IndividualBLLC theIndi) { return _rosDatabase.FindIndividualInNationalAndCreditInfo(theIndi); }
        public int CountIndividualsInNationalAndCreditInfo(IndividualBLLC theCustomer) { return _rosDatabase.CountIndividualsInNationalAndCreditInfo(theCustomer); }
        public int CountIndividuals(IndividualBLLC theCustomer) { return _rosDatabase.CountIndividuals(theCustomer); }
        public IndividualBLLC GetIndividual(String creditInfoID) { return _rosDatabase.GetIndividual(creditInfoID); }
        public int CreateIndividual(IndividualBLLC theCustomer) { return _rosDatabase.CreateIndividual(theCustomer); }
        public int UpdateIndividual(IndividualBLLC theIndividual) { return _rosDatabase.UpdateIndividual(theIndividual); }
        public DataSet FindIndividual(IndividualBLLC theIndividual) { return _rosDatabase.FindIndividual(theIndividual); }
        public ArrayList FindIndividualAsArray(IndividualBLLC theIndividual) { return _rosDatabase.FindIndividualAsArray(theIndividual); }

        #endregion

        #region Order

        public bool CreateReportOrder(OrderBLLC theOrder) { return _rosDatabase.CreateReportOrder(theOrder); }
        public bool UpdateOrder(OrderBLLC theOrder) { return _rosDatabase.UpdateReportOrder(theOrder); }
        public bool DeleteOrder(String orderID) { return _rosDatabase.DeleteReportOrder(orderID); }
        public int OrderIsDelivered(OrderBLLC theOrder, String ipAddress, int registeredByID) { return _rosDatabase.OrderIsDelivered(theOrder, ipAddress, registeredByID); }
        public ArrayList GetUndeliveredOrders() { return _rosDatabase.GetUndeliveredOrders(); }

        public ArrayList FindOrder(
            DateTime dateFrom, DateTime dateTo, CustomerBLLC myCustomer, CustomerBLLC mySubject, int productID) { return _rosDatabase.FindOrder(dateFrom, dateTo, myCustomer, mySubject, productID); }

        public ArrayList FindDeliveredOrder(
            DateTime dateFrom, DateTime dateTo, CustomerBLLC myCustomer, CustomerBLLC mySubject, int productID) { return _rosDatabase.FindDeliveredOrder(dateFrom, dateTo, myCustomer, mySubject, productID); }

        public ArrayList FindUndeliveredOrder(
            DateTime dateFrom, DateTime dateTo, CustomerBLLC myCustomer, CustomerBLLC mySubject, int productID) { return _rosDatabase.FindUndeliveredOrder(dateFrom, dateTo, myCustomer, mySubject, productID); }

        public OrderBLLC GetOrder(String orderID) { return _rosDatabase.GetOrder(orderID); }

        #endregion

        #region Common DropDownBox Get

        public DataSet GetAllProducts() { return _rosDatabase.GetAllProducts(); }
        public DataSet GetCompanyNaceCodes() { return _rosDatabase.GetCompanyNaceCodes(); }
        public DataSet GetAllCountries() { return _rosDatabase.GetAllCountries(); }
        public DataSet GetAllCities() { return _rosDatabase.GetAllCities(); }
        public DataSet GetCityListAsDataSet(string cityName, bool en) { return _rosDatabase.GetCityListAsDataSet(cityName, en); }
        public string GetCityName(int cityID, bool en) { return _rosDatabase.GetCityName(cityID, en); }
        public DataSet GetIDNumberTypes() { return _rosDatabase.GetIDNumberTypes(); }
        public DataSet GetPhoneNumberTypes() { return _rosDatabase.GetPhoneNumberTypes(); }
        public DataSet GetAllProfessions() { return _rosDatabase.GetAllProfessions(); }
        public DataSet GetAllEducations() { return _rosDatabase.GetAllEducations(); }
        public DataSet GetDeliverySpeeds() { return _rosDatabase.GetDeliverySpeeds(); }
        public ArrayList GetDeliverySpeedsAsArrayList() { return _rosDatabase.GetDeliverySpeedsAsArrayList(); }
        public DataSet GetOrderTypes() { return _rosDatabase.GetOrderTypes(); }
        public DataSet GetOrgStatus() { return _rosDatabase.GetOrgStatus(); }

        #endregion
    }
}