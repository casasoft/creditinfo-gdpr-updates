using System;

namespace ROS.BLL {
    /// <summary>
    /// Summary description for DeliverySpeed.
    /// </summary>
    public class DeliverySpeedBLLC {
        public DeliverySpeedBLLC() {
            ID = -1;
            DeliverySpeedEN = "";
            DeliverySpeedNative = "";
            DaysCount = -1;
        }

        public int ID { get; set; }
        public String DeliverySpeedEN { get; set; }
        public String DeliverySpeedNative { get; set; }
        public int DaysCount { get; set; }
    }
}