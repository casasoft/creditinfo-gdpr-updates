using System;

namespace ROS.BLL {
    [Serializable]
    /// <summary>
        /// Summary description for ProductBLLC.
        /// </summary>
    public class ProductBLLC {
        public ProductBLLC() {
            ID = -1;
            NameEN = "";
            NameNative = "";
            DescriptionEN = "";
            DescriptionNative = "";
        }

        public int ID { get; set; }
        public String NameEN { get; set; }
        public String NameNative { get; set; }
        public String DescriptionEN { get; set; }
        public String DescriptionNative { get; set; }
    }
}