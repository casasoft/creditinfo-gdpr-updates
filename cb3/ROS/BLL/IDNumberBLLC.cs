using System;

namespace ROS.BLL {
    [Serializable]
    /// <summary>
        /// Summary description for IDNumber.
        /// </summary>
    public class IDNumberBLLC {
        public IDNumberBLLC() {
            IDNumberID = -1;
            Number = "";
            NumberTypeID = -1;
            CreditInfoID = -1;
            NumberTypeNative = "";
            NumberTypeEN = "";
        }

        public int IDNumberID { get; set; }
        public String Number { get; set; }
        public int NumberTypeID { get; set; }
        public int CreditInfoID { get; set; }
        public String NumberTypeNative { get; set; }
        public String NumberTypeEN { get; set; }
    }
}