using System;

namespace ROS.BLL {
    [Serializable]
    /// <summary>
        /// Summary description for Company.
        /// </summary>
    public class CompanyBLLC : CustomerBLLC {
        public CompanyBLLC() {
            NameEN = "";
            NameNative = "";
            Established = new DateTime(1900, 1, 1);
            OrgStatus = -1;
            NaceID = "";

            NationalName = "";
            NationalPostNumber = "";
            NationalAddress = "";
            NationalFaxNumber = "";
            NationalPhoneNumber = "";
        }

        /// <summary>
        /// Gets/Sets the name in english attribute
        /// </summary>
        public String NameEN { get; set; }

        /// <summary>
        /// Gets/Sets the name in native language attribute
        /// </summary>
        public String NameNative { get; set; }

        public DateTime Established { get; set; }
        public int OrgStatus { get; set; }
        public String NaceID { get; set; }
        public String NationalName { get; set; }
        public String NationalPostNumber { get; set; }
        public String NationalAddress { get; set; }
        public String NationalFaxNumber { get; set; }
        public String NationalPhoneNumber { get; set; }
    }
}