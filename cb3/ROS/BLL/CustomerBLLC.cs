using System;
using System.Collections;

namespace ROS.BLL {
    [Serializable]
    /// <remarks>
        /// This class inherits CreditInfoUser and add Orders specific attributes
        /// </remarks>
    public class CustomerBLLC {
        /// <summary>
        /// The constructor
        /// </summary>
        public CustomerBLLC() {
            CreditInfoID = -1;
            IsCompany = true;
            IDNumbers = new ArrayList();
            TelephoneNumbers = new ArrayList();
            WebPage = "";
            LTUserName = "";
            EmailAddress = new ArrayList();
            Address = new ArrayList();
            ContactPersons = new ArrayList();
            RemarksEN = "";
            RemarksNative = "";
            LastContacted = new DateTime(1900, 1, 1);
            Type = "";
        }

        /// <summary>
        /// Gets/Sets the CreditInfoID attribute
        /// </summary>
        public int CreditInfoID { get; set; }

        /// <summary>
        /// Gets/Sets a flag that tells if this customer is a company or an individual
        /// </summary>
        public bool IsCompany { get; set; }

        /// <summary>
        /// Gets/Sets the registration number attribute that identifies this customer for example social security number
        /// </summary>
        public ArrayList IDNumbers { get; set; }

        /// <summary>
        /// Gets/Sets the telephone number attribute
        /// </summary>
        public ArrayList TelephoneNumbers { get; set; }

        /// <summary>
        /// Gets/Sets the webpage address attribute
        /// </summary>
        public String WebPage { get; set; }

        /// <summary>
        /// Gets/Sets the customers user name at LT
        /// </summary>
        public String LTUserName { get; set; }

        /// <summary>
        /// Gets/Sets the customers email address
        /// </summary>
        public ArrayList EmailAddress { get; set; }

        public ArrayList Address { get; set; }
        public ArrayList ContactPersons { get; set; }

        /// <summary>
        /// Gets/Sets the remarks about this customer in english
        /// </summary>
        public String RemarksEN { get; set; }

        /// <summary>
        /// Gets/Sets remarks about this customer in native language
        /// </summary>
        public String RemarksNative { get; set; }

        /// <summary>
        /// Gets/Sets the LastContacted attribute
        /// </summary>
        public DateTime LastContacted { get; set; }

        public String Type { get; set; }
    } // END CLASS DEFINITION OrderCustomerBLLC
}