using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logging.BLL;
using ROS.BLL;
using ROS.Localization;

using Cig.Framework.Base.Configuration;

namespace ROS {
    /// <summary>
    /// Summary description for NewCustomer.
    /// </summary>
    public class Customer : Page {
        public static CultureInfo ci;
        private const string className = "Customer";
        public static ResourceManager rm;
        private readonly FactoryBLLC _theFactory = new FactoryBLLC();
        protected Button btCancel;
        protected Button btnCity2Search;
        protected Button btnCitySearch;
        protected Button btRebDebtor;
        protected DropDownList ddlAddress1City;
        protected DropDownList ddlAddress1Country;
        protected DropDownList ddlAddress2City;
        protected DropDownList ddlAddress2Country;
        protected DropDownList ddlEducation;
        protected DropDownList ddlIDNumberType1;
        protected DropDownList ddlIDNumberType2;
        protected DropDownList ddlPhoneNumber1Type;
        protected DropDownList ddlPhoneNumber2Type;
        protected DropDownList ddlPhoneNumber3Type;
        protected DropDownList ddlPhoneNumber4Type;
        protected DropDownList ddlProfession;
        private bool EN;
        protected Label lbAddress1InfoNative;
        protected Label lbCustomerEmailPrimary;
        protected Label lbDebtorCIID;
        protected Label lbDebtorFirstName;
        protected Label lbDebtorSurName;
        protected Label lbFirstNameEN;
        protected Label lblAdditionalInfo;
        protected Label lblAddress1;
        protected Label lblAddress1City;
        protected Label lblAddress1CityEN;
        protected Label lblAddress1CityNative;
        protected Label lblAddress1Country;
        protected Label lblAddress1EN;
        protected Label lblAddress1InfoEN;
        protected Label lblAddress1PostalCodeNative;
        protected Label lblAddress1PostBox;
        protected Label lblAddress2City;
        protected Label lblAddress2Country;
        protected Label lblAddress2EN;
        protected Label lblAddress2InfoEN;
        protected Label lblAddress2InfoNative;
        protected Label lblAddress2Native;
        protected Label lblAddress2PostalCode;
        protected Label lblAddress2PostBox;
        protected Label lblCity2Search;
        protected Label lblCitySearch;
        protected Label lblContact1Email;
        protected Label lblContact1FirstNameEN;
        protected Label lblContact1FirstNameNative;
        protected Label lblContact1SurnameEN;
        protected Label lblContact1SurnameNative;
        protected Label lblContact2Email;
        protected Label lblContact2FirstName;
        protected Label lblContact2FirstNameEN;
        protected Label lblContact2Surname;
        protected Label lblContact2SurnameEN;
        protected Label lblContact3Email;
        protected Label lblContact3FirstName;
        protected Label lblContact3FirstNameEN;
        protected Label lblContact3Surname;
        protected Label lblContact3SurnameEN;
        protected Label lblContactPerson1;
        protected Label lblContactPerson2;
        protected Label lblContactPerson3;
        protected Label lblCustomerEmailSecondary;
        protected Label lblCustomerEmailThird;
        protected Label lblCustomerRemarks;
        protected Label lblCustomerRemarksEN;
        protected Label lblEducation;
        protected Label lblErrorMessage;
        protected Label lblIDNumber;
        protected Label lblIDNumber2;
        protected Label lblIDNumberType;
        protected Label lblIDNumberType2;
        protected Label lblNewCustomer;
        protected Label lblNumbers;
        protected Label lblPhoneNumber1;
        protected Label lblPhoneNumber2;
        protected Label lblPhonenumber2Type;
        protected Label lblPhoneNumber3;
        protected Label lblPhoneNumber3Type;
        protected Label lblPhoneNumber4;
        protected Label lblPhoneNumber4Type;
        protected Label lblPhonNumber1Type;
        protected Label lblProfession;
        protected Label lblTheAddress1;
        protected Label lblTheAddress2;
        protected Label lbPersonInformation;
        protected Label lbPhoneNumber2;
        protected Label lbPhonenumber2Type;
        protected Label lbPhoneNumber3;
        protected Label lbPhoneNumber3Type;
        protected Label lbPhoneNumber4;
        protected Label lbPhoneNumber4Type;
        protected Label lbPhonNumber1Type;
        protected Label lbSurnameEN;
        protected RequiredFieldValidator RequiredFieldValidatorFirstName;
        protected RequiredFieldValidator RequiredFieldValidatorSurname;
        protected RequiredFieldValidator rfvCity;
        protected RequiredFieldValidator rfvCity2;
        protected TextBox tbAddress1EN;
        protected TextBox tbPhoneNumber1;
//		protected System.Web.UI.WebControls.DropDownList ddPhoneNumber1Type;
        protected TextBox tbPhoneNumber2;
        protected TextBox tbPhoneNumber3;
//		protected System.Web.UI.WebControls.DropDownList ddPhoneNumber3Type;
        protected TextBox tbPhoneNumber4;
        protected TextBox txtAddress1EN;
        protected TextBox txtAddress1InfoEN;
        protected TextBox txtAddress1InfoNative;
        protected TextBox txtAddress1Native;
        protected TextBox txtAddress1PostalCode;
        protected TextBox txtAddress1PostBox;
        protected TextBox txtAddress2EN;
        protected TextBox txtAddress2InfoEN;
        protected TextBox txtAddress2InfoNative;
        protected TextBox txtAddress2Native;
        protected TextBox txtAddress2PostalCode;
        protected TextBox txtAddress2PostalCode1;
        protected TextBox txtAddress2PostBox;
        protected TextBox txtCity2Search;
        protected TextBox txtCitySearch;
        protected TextBox txtContact1Email;
        protected TextBox txtContact1FirstNameEN;
        protected TextBox txtContact1FirstNameNative;
        protected TextBox txtContact1SurnameEN;
        protected TextBox txtContact1SurnameNative;
        protected TextBox txtContact2;
        protected TextBox txtContact2Email;
        protected TextBox txtContact2FirstName;
        protected TextBox txtContact2FirstNameEN;
        protected TextBox txtContact2FirstNameNative;
        protected TextBox txtContact2Surname;
        protected TextBox txtContact2SurnameEN;
        protected TextBox txtContact2SurnameNative;
        protected TextBox txtContact3Email;
        protected TextBox txtContact3FirstName;
        protected TextBox txtContact3FirstNameEN;
        protected TextBox txtContact3FirstNameNative;
        protected TextBox txtContact3Surname;
        protected TextBox txtContact3SurnameEN;
        protected TextBox txtContact3SurnameNative;
        protected TextBox txtCustomerCIID;
        protected TextBox txtCustomerEmail1;
        protected TextBox txtCustomerEmail2;
        protected TextBox txtCustomerEmail3;
        protected TextBox txtCustomerFirstNameEN;
        protected TextBox txtCustomerFirstNameNative;
        protected TextBox txtCustomerRemarksEN;
        protected TextBox txtCustomerRemarksNative;
        protected TextBox txtCustomerSurnameEN;
        protected TextBox txtCustomerSurNameNative;
        protected TextBox txtIDNumber1;
        protected TextBox txtIDNumber2;
        protected TextBox txtPhoneNumber1;
        protected TextBox txtPhoneNumber2;
        protected TextBox txtPhoneNumber3;
        protected TextBox txtPhoneNumber4;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = CigConfig.Configure("lookupsettings.ROSPageIDNewCustomer").Trim();

            const string funcName = "Page_Load(object sender, System.EventArgs e)";
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            lblErrorMessage.Visible = false;
            lblAddress1Country.Visible = false;
            lblAddress2Country.Visible = false;
            ddlAddress1Country.Visible = false;
            ddlAddress2Country.Visible = false;

            if (IsPostBack) {
                return;
            }
            var state = (string) Session["State"];
            var theOrder = (OrderBLLC) Session["RosOrder"];
            var customerType = (string) Session["CustomerType"];

            txtCustomerCIID.Enabled = false;

            IndividualBLLC theCustomer;

            //�arf a� hla�a inn � �ll dropdownboxin
            InitDDBoxes();

            string defaultCountry = CigConfig.Configure("lookupsettings.ROSDefaultCountry").Trim();
            ddlAddress1Country.SelectedValue = defaultCountry;
            ddlAddress2Country.SelectedValue = defaultCountry;

            if (state == "Update") {
                theCustomer = Request.QueryString["user"] == null ? null : _theFactory.GetIndividual(Request.QueryString["user"]);
            } else {
                lbDebtorCIID.Visible = false;
                txtCustomerCIID.Visible = false;

                //Get the customer information from the session
                if (customerType == "Customer") {
                    theCustomer = (IndividualBLLC) theOrder.Customer;
                } else {
                    theCustomer = (IndividualBLLC) theOrder.Subject;
                }
            }

            if (theCustomer == null) {
                lblErrorMessage.Text = rm.GetString("txtErrorCouldNotGetUser", ci);
                lblErrorMessage.Visible = true;
                Logger.WriteToLog(className + " : " + funcName + "theCustomer is null", true);
            } else {
                InsertIntoCustomerInfo(theCustomer);
            }

            if (state == "Update") {
                LocalizeText(true);
            } else {
                LocalizeText(false);
            }
        }

        private void InitDDBoxes() {
            var theFactory = new FactoryBLLC();
            BindPhoneNumberTypes(theFactory);
            BindIDNumberTypes(theFactory);
            BindEducationTypes(theFactory);
            BindProfessionTypes(theFactory);
            //BindCityList(theFactory);
            BindCountries(theFactory);
        }

        private void BindPhoneNumberTypes(FactoryBLLC theFactory) {
            var phoneTypeSet = theFactory.GetPhoneNumberTypes();
            ddlPhoneNumber1Type.DataSource = phoneTypeSet;
            ddlPhoneNumber1Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlPhoneNumber1Type.DataValueField = "NumberTypeID";
            ddlPhoneNumber1Type.DataBind();

            ddlPhoneNumber2Type.DataSource = phoneTypeSet;
            ddlPhoneNumber2Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlPhoneNumber2Type.DataValueField = "NumberTypeID";
            ddlPhoneNumber2Type.DataBind();

            ddlPhoneNumber3Type.DataSource = phoneTypeSet;
            ddlPhoneNumber3Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlPhoneNumber3Type.DataValueField = "NumberTypeID";
            ddlPhoneNumber3Type.DataBind();

            ddlPhoneNumber4Type.DataSource = phoneTypeSet;
            ddlPhoneNumber4Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlPhoneNumber4Type.DataValueField = "NumberTypeID";
            ddlPhoneNumber4Type.DataBind();
        }

        private void BindIDNumberTypes(FactoryBLLC theFactory) {
            var idNumberType = theFactory.GetIDNumberTypes();

            ddlIDNumberType1.DataSource = idNumberType;
            ddlIDNumberType1.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlIDNumberType1.DataValueField = "NumberTypeID";
            ddlIDNumberType1.DataBind();

            ddlIDNumberType2.DataSource = idNumberType;
            ddlIDNumberType2.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlIDNumberType2.DataValueField = "NumberTypeID";
            ddlIDNumberType2.DataBind();
        }

        private void BindEducationTypes(FactoryBLLC theFactory) {
            var educationTypeSet = theFactory.GetAllEducations();
            ddlEducation.DataSource = educationTypeSet;

            ddlEducation.DataTextField = EN ? "NameEN" : "NameNative";

            ddlEducation.DataValueField = "EducationID";
            ddlEducation.DataBind();
        }

        private void BindProfessionTypes(FactoryBLLC theFactory) {
            var professionTypeSet = theFactory.GetAllProfessions();
            ddlProfession.DataSource = professionTypeSet;
            ddlProfession.DataTextField = EN ? "NameEN" : "NameNative";
            ddlProfession.DataValueField = "ProfessionID";
            ddlProfession.DataBind();
        }

        private void BindCityList(DataSet citySet) {
            ddlAddress1City.DataSource = citySet;
            ddlAddress1City.DataTextField = EN ? "NameEN" : "NameNative";

            ddlAddress1City.DataValueField = "CityID";
            ddlAddress1City.DataBind();
        }

        private void BindCity2List(DataSet citySet) {
            ddlAddress2City.DataSource = citySet;
            ddlAddress2City.DataTextField = EN ? "NameEN" : "NameNative";

            ddlAddress2City.DataValueField = "CityID";
            ddlAddress2City.DataBind();
        }

        private void BindCountries(FactoryBLLC theFactory) {
            var countrySet = theFactory.GetAllCountries();
            ddlAddress1Country.DataSource = countrySet;
            ddlAddress1Country.DataTextField = EN ? "NameEN" : "NameNative";

            ddlAddress1Country.DataValueField = "CountryID";
            ddlAddress1Country.DataBind();

            ddlAddress2Country.DataSource = countrySet;
            ddlAddress2Country.DataTextField = EN ? "NameEN" : "NameNative";

            ddlAddress2Country.DataValueField = "CountryID";
            ddlAddress2Country.DataBind();
        }

        private void btRebDebtor_Click(object sender, EventArgs e) {
            lblErrorMessage.Visible = false;
            var theCustomer = GetInfoFromPage();
            theCustomer.Type = CigConfig.Configure("lookupsettings.individualID");

            var state = (string) Session["State"];
            var creditInfoID = -1;

            if (state == "Update") {
                var lastPage = (string) Session["LastPage"];

                creditInfoID = _theFactory.UpdateIndividual(theCustomer);

                if (creditInfoID != -1) {
                    if (lastPage != null) {
                        Server.Transfer(lastPage + ".aspx");
                    } else {
                        Server.Transfer("OrderCustomerSubjectSearch.aspx");
                    }
                } else {
                    //Ekki t�kst a� uppf�ra kappann
                    lblErrorMessage.Text = rm.GetString("txtErrorUpdatingUser", ci);
                    lblErrorMessage.Visible = true;
                }
            } else {
                creditInfoID = _theFactory.CreateIndividual(theCustomer);

                if (creditInfoID != -1) {
                    //Setja �ennan vi�skiptavin � sessionina
                    var theOrder = (OrderBLLC) Session["RosOrder"];

                    theCustomer.CreditInfoID = creditInfoID;

                    var customerType = (string) Session["CustomerType"];
                    if (customerType == "Customer") {
                        theOrder.Customer = theCustomer;
                        Session["CustomerSelected"] = true;
                    } else {
                        theOrder.Subject = theCustomer;
                        Session["SubjectSelected"] = true;
                    }

                    Session["RosOrder"] = theOrder;
                    Server.Transfer("OrderCustomerSubjectSearch.aspx");
                } else {
                    //Ekki t�kst a� skr� vi�skiptavininn � gagnagrunninn.
                    lblErrorMessage.Text = rm.GetString("txtErrorCreatingUser", ci);
                    lblErrorMessage.Visible = true;
                }
            }
        }

        private IndividualBLLC GetInfoFromPage() {
            var theCustomer = new IndividualBLLC {IsCompany = false};

//			if(this.txtCustomerCIID.Text.Length > 0)
//              theCustomer.CreditInfoID = int.Parse(this.txtCustomerCIID.Text.Trim());
            if (Request.QueryString["user"] != null) {
                theCustomer.CreditInfoID = int.Parse(Request.QueryString["user"].Trim());
            }
            theCustomer.FirstNameEN = txtCustomerFirstNameEN.Text;
            theCustomer.FirstNameNative = txtCustomerFirstNameNative.Text;
            theCustomer.LastNameEN = txtCustomerSurnameEN.Text;
            theCustomer.LastNameNative = txtCustomerSurNameNative.Text;

            if (txtCustomerEmail1.Text.Length > 0) {
                theCustomer.EmailAddress.Add(txtCustomerEmail1.Text);
            }
            if (txtCustomerEmail2.Text.Length > 0) {
                theCustomer.EmailAddress.Add(txtCustomerEmail2.Text);
            }
            if (txtCustomerEmail3.Text.Length > 0) {
                theCustomer.EmailAddress.Add(txtCustomerEmail3.Text);
            }

            if (txtAddress1EN.Text.Length > 0 || txtAddress1Native.Text.Length > 0 || txtAddress1InfoEN.Text.Length > 0 ||
                txtAddress1InfoNative.Text.Length > 0 || txtAddress1PostalCode.Text.Length > 0 ||
                txtAddress1PostBox.Text.Length > 0) {
                var theAddress = new AddressBLLC
                                 {
                                     StreetEN = txtAddress1EN.Text,
                                     StreetNative = txtAddress1Native.Text,
                                     InfoEN = txtAddress1InfoEN.Text,
                                     InfoNative = txtAddress1InfoNative.Text,
                                     PostalCode = txtAddress1PostalCode.Text,
                                     PostBox = txtAddress1PostBox.Text
                                 };
                if (ddlAddress1City.SelectedItem.Value.Trim() != "") {
                    theAddress.CityID = int.Parse(ddlAddress1City.SelectedValue);
                }
                theAddress.CountryID = int.Parse(ddlAddress1Country.SelectedValue);
                theCustomer.Address.Add(theAddress);
            }
            if (txtAddress2EN.Text.Length > 0 || txtAddress2Native.Text.Length > 0 || txtAddress2InfoEN.Text.Length > 0 ||
                txtAddress2InfoNative.Text.Length > 0 || txtAddress2PostalCode.Text.Length > 0 ||
                txtAddress2PostBox.Text.Length > 0) {
                var theAddress = new AddressBLLC
                                 {
                                     StreetEN = txtAddress2EN.Text,
                                     StreetNative = txtAddress2Native.Text,
                                     InfoEN = txtAddress2InfoEN.Text,
                                     InfoNative = txtAddress2InfoNative.Text,
                                     PostalCode = txtAddress2PostalCode.Text,
                                     PostBox = txtAddress2PostBox.Text
                                 };
                if (ddlAddress2City.SelectedItem.Value.Trim() != "") {
                    theAddress.CityID = int.Parse(ddlAddress2City.SelectedValue);
                }
                theAddress.CountryID = int.Parse(ddlAddress2Country.SelectedValue);
                theCustomer.Address.Add(theAddress);
            }

            theCustomer.ProfessionID = int.Parse(ddlProfession.SelectedValue);
            theCustomer.EducationID = int.Parse(ddlEducation.SelectedValue);

            if (txtIDNumber1.Text.Length > 0) {
                var theID = new IDNumberBLLC
                            {
                                Number = txtIDNumber1.Text,
                                NumberTypeID = int.Parse(ddlIDNumberType1.SelectedValue)
                            };
                theCustomer.IDNumbers.Add(theID);
            }
            if (txtIDNumber2.Text.Length > 0) {
                var theID = new IDNumberBLLC
                            {
                                Number = txtIDNumber2.Text,
                                NumberTypeID = int.Parse(ddlIDNumberType2.SelectedValue)
                            };
                theCustomer.IDNumbers.Add(theID);
            }

            if (txtPhoneNumber1.Text.Length > 0) {
                var thePhone = new PhoneNumberBLLC
                               {
                                   Number = txtPhoneNumber1.Text,
                                   NumberTypeID = int.Parse(ddlPhoneNumber1Type.SelectedValue)
                               };
                theCustomer.TelephoneNumbers.Add(thePhone);
            }
            if (txtPhoneNumber2.Text.Length > 0) {
                var thePhone = new PhoneNumberBLLC
                               {
                                   Number = txtPhoneNumber2.Text,
                                   NumberTypeID = int.Parse(ddlPhoneNumber2Type.SelectedValue)
                               };
                theCustomer.TelephoneNumbers.Add(thePhone);
            }
            if (txtPhoneNumber3.Text.Length > 0) {
                var thePhone = new PhoneNumberBLLC
                               {
                                   Number = txtPhoneNumber3.Text,
                                   NumberTypeID = int.Parse(ddlPhoneNumber3Type.SelectedValue)
                               };
                theCustomer.TelephoneNumbers.Add(thePhone);
            }
            if (txtPhoneNumber4.Text.Length > 0) {
                var thePhone = new PhoneNumberBLLC
                               {
                                   Number = txtPhoneNumber4.Text,
                                   NumberTypeID = int.Parse(ddlPhoneNumber4Type.SelectedValue)
                               };
                theCustomer.TelephoneNumbers.Add(thePhone);
            }

            theCustomer.RemarksEN = txtCustomerRemarksEN.Text;
            theCustomer.RemarksNative = txtCustomerRemarksNative.Text;

            if (txtContact1Email.Text.Length > 0 || txtContact1FirstNameEN.Text.Length > 0 ||
                txtContact1FirstNameNative.Text.Length > 0
                || txtContact1SurnameEN.Text.Length > 0 || txtContact1SurnameNative.Text.Length > 0) {
                var theContact = new ContactPersonBLLC
                                 {
                                     Email = txtContact1Email.Text,
                                     FirstNameEN = txtContact1FirstNameEN.Text,
                                     FirstNameNative = txtContact1FirstNameNative.Text,
                                     LastNameEN = txtContact1SurnameEN.Text,
                                     LastNameNative = txtContact1SurnameNative.Text
                                 };
                theCustomer.ContactPersons.Add(theContact);
            }
            if (txtContact2Email.Text.Length > 0 || txtContact2FirstNameEN.Text.Length > 0 ||
                txtContact2FirstNameNative.Text.Length > 0
                || txtContact2SurnameEN.Text.Length > 0 || txtContact2SurnameNative.Text.Length > 0) {
                var theContact = new ContactPersonBLLC
                                 {
                                     Email = txtContact2Email.Text,
                                     FirstNameEN = txtContact2FirstNameEN.Text,
                                     FirstNameNative = txtContact2FirstNameNative.Text,
                                     LastNameEN = txtContact2SurnameEN.Text,
                                     LastNameNative = txtContact2SurnameNative.Text
                                 };
                theCustomer.ContactPersons.Add(theContact);
            }
            if (txtContact3Email.Text.Length > 0 || txtContact3FirstNameEN.Text.Length > 0 ||
                txtContact3FirstNameNative.Text.Length > 0
                || txtContact3SurnameEN.Text.Length > 0 || txtContact3SurnameNative.Text.Length > 0) {
                var theContact = new ContactPersonBLLC
                                 {
                                     Email = txtContact3Email.Text,
                                     FirstNameEN = txtContact3FirstNameEN.Text,
                                     FirstNameNative = txtContact3FirstNameNative.Text,
                                     LastNameEN = txtContact3SurnameEN.Text,
                                     LastNameNative = txtContact3SurnameNative.Text
                                 };
                theCustomer.ContactPersons.Add(theContact);
            }

            return theCustomer;
        }

        private void LocalizeText(bool update) {
            lbPersonInformation.Text = rm.GetString("txtPersonInfo", ci);
            lbDebtorCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lbFirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lbDebtorFirstName.Text = rm.GetString("txtFirstName", ci);
            lbSurnameEN.Text = rm.GetString("txtSurNameEN", ci);
            lbDebtorSurName.Text = rm.GetString("txtSurName", ci);
            lbCustomerEmailPrimary.Text = rm.GetString("txtEmailPrimary", ci);
            lblCustomerEmailSecondary.Text = rm.GetString("txtEmailSecondary", ci);
            lblCustomerEmailThird.Text = rm.GetString("txtEmailThird", ci);
            lblTheAddress1.Text = rm.GetString("txtAddress1", ci);
            lblAddress1.Text = rm.GetString("txtAddress", ci);
            lblAddress1EN.Text = rm.GetString("txtAddress1EN", ci);
            lbAddress1InfoNative.Text = rm.GetString("txtAddressInfo", ci);
            lblAddress1InfoEN.Text = rm.GetString("txtAddressInfoEN", ci);
            lblAddress1PostalCodeNative.Text = rm.GetString("txtPostalCode", ci);
            lblAddress1PostBox.Text = rm.GetString("txtPostBox", ci);
            lblAddress1City.Text = rm.GetString("txtCity", ci);
            lblAddress1Country.Text = rm.GetString("txtCountry", ci);
            lblTheAddress2.Text = rm.GetString("txtAddress2", ci);
            lblAddress2Native.Text = rm.GetString("txtAddress", ci);
            lblAddress2EN.Text = rm.GetString("txtAddressEN", ci);
            lblAddress2InfoNative.Text = rm.GetString("txtAddressInfo", ci);
            lblAddress2InfoEN.Text = rm.GetString("txtAddressInfoEN", ci);
            lblAddress2PostalCode.Text = rm.GetString("txtPostalCode", ci);
            lblAddress2PostBox.Text = rm.GetString("txtPostBox", ci);
            lblAddress2City.Text = rm.GetString("txtCity", ci);
            lblAddress2Country.Text = rm.GetString("txtCountry", ci);

            lblNumbers.Text = rm.GetString("lblNumbers", ci);
            lblProfession.Text = rm.GetString("txtProfession", ci);
            lblEducation.Text = rm.GetString("txtEducation", ci);
            lblIDNumber.Text = rm.GetString("txtIDNumber1", ci);
            lblIDNumber2.Text = rm.GetString("txtIDNumber2", ci);
            lblPhoneNumber1.Text = rm.GetString("txtPhoneNumber1", ci);
            lblPhoneNumber2.Text = rm.GetString("txtPhoneNumber2", ci);
            lblPhoneNumber3.Text = rm.GetString("txtPhoneNumber3", ci);
            lblPhoneNumber4.Text = rm.GetString("txtPhoneNumber4", ci);
            lblIDNumberType.Text = rm.GetString("txtType", ci);
            lblIDNumberType2.Text = rm.GetString("txtType", ci);
            lblPhonNumber1Type.Text = rm.GetString("txtType", ci);
            lblPhonenumber2Type.Text = rm.GetString("txtType", ci);
            lblPhoneNumber3Type.Text = rm.GetString("txtType", ci);
            lblPhoneNumber4Type.Text = rm.GetString("txtType", ci);

            lblAdditionalInfo.Text = rm.GetString("lblAdditionalInfo", ci);

            lblCustomerRemarks.Text = rm.GetString("txtRemarksNative", ci);
            lblCustomerRemarksEN.Text = rm.GetString("txtRemarksEN", ci);
            // contact 1
            lblContactPerson1.Text = rm.GetString("txtContactPerson", ci);
            lblContact1FirstNameNative.Text = rm.GetString("txtFirstName", ci);
            lblContact1FirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lblContact1SurnameNative.Text = rm.GetString("txtSurName", ci);
            lblContact1SurnameEN.Text = rm.GetString("txtSurNameEN", ci);
            lblContact1Email.Text = rm.GetString("txtEmail", ci);
            // contact 2
            lblContactPerson2.Text = rm.GetString("txtContactPerson", ci);
            lblContact2FirstName.Text = rm.GetString("txtFirstName", ci);
            lblContact2FirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lblContact2Surname.Text = rm.GetString("txtSurName", ci);
            lblContact2SurnameEN.Text = rm.GetString("txtSurNameEN", ci);
            lblContact2Email.Text = rm.GetString("txtEmail", ci);
            // contact 3
            lblContactPerson3.Text = rm.GetString("txtContactPerson", ci);
            lblContact3FirstName.Text = rm.GetString("txtFirstName", ci);
            lblContact3FirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lblContact3Surname.Text = rm.GetString("txtSurName", ci);
            lblContact3SurnameEN.Text = rm.GetString("txtSurNameEN", ci);
            lblContact3Email.Text = rm.GetString("txtEmail", ci);

//			lbNewUserAddet.Text = rm.GetString("txtNewUserHasBeenRegistered",ci);
//			lbUserUpdated.Text = rm.GetString("txtTheUserHasBeenUpdated",ci);

            btCancel.Text = rm.GetString("txtCancel", ci);
            RequiredFieldValidatorFirstName.ErrorMessage = rm.GetString("txtErrorFirstNameMissing", ci);
            RequiredFieldValidatorSurname.ErrorMessage = rm.GetString("txtErrorSurnameMissing", ci);

            rfvCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvCity2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblCitySearch.Text = rm.GetString("txtSearch", ci);
            lblCity2Search.Text = rm.GetString("txtSearch", ci);

            btRebDebtor.Text = update ? rm.GetString("txtUpdate", ci) : rm.GetString("txtSubmit", ci);
        }

        private void InsertIntoCustomerInfo(IndividualBLLC theCustomer) {
            if (theCustomer.CreditInfoID != -1) {
                txtCustomerCIID.Text = "" + theCustomer.CreditInfoID;
            }

            txtCustomerFirstNameEN.Text = theCustomer.FirstNameEN;
            txtCustomerFirstNameNative.Text = theCustomer.FirstNameNative;
            txtCustomerSurnameEN.Text = theCustomer.LastNameEN;
            txtCustomerSurNameNative.Text = theCustomer.LastNameNative;

            for (int i = 0; i < theCustomer.EmailAddress.Count; i++) {
                var email = (string) theCustomer.EmailAddress[i];

                if (i == 0) {
                    txtCustomerEmail1.Text = email;
                }
                if (i == 1) {
                    txtCustomerEmail2.Text = email;
                }
                if (i == 2) {
                    txtCustomerEmail3.Text = email;
                }
            }

            if (theCustomer.Address.Count > 0) {
                var theAddress = (AddressBLLC) theCustomer.Address[0];
                txtAddress1EN.Text = theAddress.StreetEN;
                txtAddress1Native.Text = theAddress.StreetNative;
                txtAddress1InfoEN.Text = theAddress.OtherInfoEN;
                txtAddress1InfoNative.Text = theAddress.OtherInfoNative;
                txtAddress1PostalCode.Text = theAddress.PostalCode;
                txtAddress1PostBox.Text = theAddress.PostBox;

                if (theAddress.CityID != -1) {
                    string cityName = _theFactory.GetCityName(theAddress.CityID, EN);
                    if (!string.IsNullOrEmpty(cityName)) {
                        ddlAddress1City.Items.Add(new ListItem(cityName, theAddress.CityID.ToString()));
                        ddlAddress1City.SelectedValue = theAddress.CityID.ToString();
                    }
                }

                //   this.ddlAddress1City.SelectedValue = "" + theAddress.CityID;
                if (theAddress.CountryID != -1) {
                    ddlAddress1Country.SelectedValue = "" + theAddress.CountryID;
                }
            }

            if (theCustomer.Address.Count > 1) {
                var theAddress = (AddressBLLC) theCustomer.Address[1];
                txtAddress2EN.Text = theAddress.StreetEN;
                txtAddress2Native.Text = theAddress.StreetNative;
                txtAddress2InfoEN.Text = theAddress.OtherInfoEN;
                txtAddress2InfoNative.Text = theAddress.OtherInfoNative;
                txtAddress2PostalCode.Text = theAddress.PostalCode;
                txtAddress2PostBox.Text = theAddress.PostBox;
                if (theAddress.CityID != -1) {
                    string cityName = _theFactory.GetCityName(theAddress.CityID, EN);
                    if (!string.IsNullOrEmpty(cityName)) {
                        ddlAddress2City.Items.Add(new ListItem(cityName, theAddress.CityID.ToString()));
                        ddlAddress2City.SelectedValue = theAddress.CityID.ToString();
                    }
                }
                //this.ddlAddress2City.SelectedValue = "" + theAddress.CityID;
                if (theAddress.CountryID != -1) {
                    ddlAddress2Country.SelectedValue = "" + theAddress.CountryID;
                }
            }

            //Profession
            if (theCustomer.ProfessionID != -1) {
                ddlProfession.SelectedValue = "" + theCustomer.ProfessionID;
            }
            //Education
            if (theCustomer.EducationID != -1) {
                ddlEducation.SelectedValue = "" + theCustomer.EducationID;
            }

            if (theCustomer.IDNumbers.Count > 0) {
                var theID = (IDNumberBLLC) theCustomer.IDNumbers[0];
                txtIDNumber1.Text = theID.Number;
                ddlIDNumberType1.SelectedValue = "" + theID.NumberTypeID;
            }

            if (theCustomer.IDNumbers.Count > 1) {
                var theID = (IDNumberBLLC) theCustomer.IDNumbers[1];
                txtIDNumber2.Text = theID.Number;
                ddlIDNumberType2.SelectedValue = "" + theID.NumberTypeID;
            }

            if (theCustomer.TelephoneNumbers.Count > 0) {
                var thePhone = (PhoneNumberBLLC) theCustomer.TelephoneNumbers[0];
                txtPhoneNumber1.Text = thePhone.Number;
                ddlPhoneNumber1Type.SelectedValue = "" + thePhone.NumberTypeID;
            }
            if (theCustomer.TelephoneNumbers.Count > 1) {
                var thePhone = (PhoneNumberBLLC) theCustomer.TelephoneNumbers[1];
                txtPhoneNumber2.Text = thePhone.Number;
                ddlPhoneNumber2Type.SelectedValue = "" + thePhone.NumberTypeID;
            }
            if (theCustomer.TelephoneNumbers.Count > 2) {
                var thePhone = (PhoneNumberBLLC) theCustomer.TelephoneNumbers[2];
                txtPhoneNumber3.Text = thePhone.Number;
                ddlPhoneNumber3Type.SelectedValue = "" + thePhone.NumberTypeID;
            }
            if (theCustomer.TelephoneNumbers.Count > 3) {
                var thePhone = (PhoneNumberBLLC) theCustomer.TelephoneNumbers[3];
                txtPhoneNumber4.Text = thePhone.Number;
                ddlPhoneNumber4Type.SelectedValue = "" + thePhone.NumberTypeID;
            }

            txtCustomerRemarksEN.Text = theCustomer.RemarksEN;
            txtCustomerRemarksNative.Text = theCustomer.RemarksNative;

            for (int i = 0; i < theCustomer.ContactPersons.Count; i++) {
                var theContact = (ContactPersonBLLC) theCustomer.ContactPersons[i];

                if (i == 0) {
                    txtContact1Email.Text = theContact.Email;
                    txtContact1FirstNameEN.Text = theContact.FirstNameEN;
                    txtContact1FirstNameNative.Text = theContact.FirstNameNative;
                    txtContact1SurnameEN.Text = theContact.LastNameEN;
                    txtContact1SurnameNative.Text = theContact.LastNameNative;
                }
                if (i == 1) {
                    txtContact2Email.Text = theContact.Email;
                    txtContact2FirstNameEN.Text = theContact.FirstNameEN;
                    txtContact2FirstNameNative.Text = theContact.FirstNameNative;
                    txtContact2SurnameEN.Text = theContact.LastNameEN;
                    txtContact2SurnameNative.Text = theContact.LastNameNative;
                }
                if (i == 2) {
                    txtContact3Email.Text = theContact.Email;
                    txtContact3FirstNameEN.Text = theContact.FirstNameEN;
                    txtContact3FirstNameNative.Text = theContact.FirstNameNative;
                    txtContact3SurnameEN.Text = theContact.LastNameEN;
                    txtContact3SurnameNative.Text = theContact.LastNameNative;
                }
            }
        }

        private void btCancel_Click(object sender, EventArgs e) {
            var theOrder = (OrderBLLC) Session["RosOrder"];

            var customerType = (string) Session["CustomerType"];

            if (customerType == "Customer") {
                theOrder.Customer = null;
            }
            if (customerType == "Subject") {
                theOrder.Subject = null;
            }

            Session["RosOrder"] = theOrder;

            var lastPage = (string) Session["LastPage"];
            if (lastPage != null) {
                Server.Transfer(lastPage + ".aspx");
            } else {
                Server.Transfer("OrderCustomerSubjectSearch.aspx");
            }
        }

        private void btnCitySearch_Click(object sender, EventArgs e) {
            if (txtCitySearch.Text.Trim() == "") {
                return;
            }
            var dsCity = _theFactory.GetCityListAsDataSet(txtCitySearch.Text, EN);
            BindCityList(dsCity);
        }

        private void btnCity2Search_Click(object sender, EventArgs e) {
            if (txtCity2Search.Text.Trim() == "") {
                return;
            }
            var dsCity = _theFactory.GetCityListAsDataSet(txtCity2Search.Text, EN);
            BindCity2List(dsCity);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCitySearch.Click += new System.EventHandler(this.btnCitySearch_Click);
            this.btnCity2Search.Click += new System.EventHandler(this.btnCity2Search_Click);
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            this.btRebDebtor.Click += new System.EventHandler(this.btRebDebtor_Click);
            this.ID = "Customer";
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}