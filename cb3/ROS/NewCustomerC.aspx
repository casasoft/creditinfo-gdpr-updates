<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="NewCustomerC.aspx.cs" AutoEventWireup="false" Inherits="ROS.NewCustomerC" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Customer</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						newuser.btRegCompany.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.newuser.tbCompanyEstablished.focus();
				}

		</SCRIPT>
	</HEAD>
	<body>
		<form id="CustomerC" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelBar id="PanelBar1" runat="server"></uc1:panelBar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<TABLE id="FormTable" cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lbCompanyInformation" runat="server">Company Info</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lbDebtorCIID" runat="server" Width="100%">CreditInfoID</asp:label><BR>
																		<asp:textbox id="txtCompanyCIID" runat="server" MaxLength="10" EnableViewState="False" ReadOnly="True"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"></TD>
																	<TD style="WIDTH: 25%"></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lbCompanyName" runat="server" Width="100%">Name</asp:label><BR>
																		<asp:textbox id="txtCompanyNameNative" runat="server" MaxLength="100" EnableViewState="False"></asp:textbox>&nbsp;
																		<asp:requiredfieldvalidator id="RequiredFieldValidatorName" runat="server" ControlToValidate="txtCompanyNameNative"
																			ErrorMessage="RequiredFieldValidator">*</asp:requiredfieldvalidator></TD>
																	<TD>
																		<asp:label id="lbCompanyNameEN" runat="server" Width="100%"> Name(EN)</asp:label><BR>
																		<asp:textbox id="txtCompanyNameEN" runat="server" MaxLength="100" EnableViewState="False"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lbEstablished" runat="server" Width="100%">Established</asp:label><BR>
																		<asp:textbox id="txtCompanyEstablished" runat="server" MaxLength="10" EnableViewState="False"></asp:textbox>&nbsp;<INPUT class="popup" onclick="PopupPicker('txtCompanyEstablished', 250, 250);" type="button"
																			value="...">&nbsp;
																		<asp:customvalidator id="CustomValidatorEstablishedFormat" runat="server" ControlToValidate="txtCompanyEstablished"
																			ErrorMessage="CustomValidator">*</asp:customvalidator></TD>
																	<TD>
																		<asp:label id="lbLastContacted" runat="server" Width="100%">Last Contacted</asp:label><BR>
																		<asp:textbox id="txtCompanyLastContacted" runat="server" MaxLength="10" EnableViewState="False"></asp:textbox>&nbsp;<INPUT class="popup" onclick="PopupPicker('txtCompanyLastContacted', 250, 250);" type="button"
																			value="...">&nbsp;
																		<asp:customvalidator id="CustomValidatorLastContactedFormat" runat="server" ControlToValidate="txtCompanyLastContacted"
																			ErrorMessage="CustomValidator">*</asp:customvalidator></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblEmail1" runat="server" Width="100%">Email 1</asp:label><BR>
																		<asp:textbox id="txtEmail1" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lblEmail2" runat="server" Width="100%">Email 2</asp:label><BR>
																		<asp:textbox id="txtEmail2" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lblEmail3" runat="server" Width="100%">Email 3</asp:label><BR>
																		<asp:textbox id="txtEmail3" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lbCompURL" runat="server" Width="100%">URL</asp:label><BR>
																		<asp:textbox id="txtCompanyURL" runat="server" MaxLength="100" EnableViewState="False"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lbCompFunction" runat="server" Width="100%">Company function</asp:label><BR>
																		<asp:dropdownlist id="ddCompanyNace" runat="server"></asp:dropdownlist></TD>
																	<TD>
																		<asp:label id="lbOrg_status" runat="server" Width="100%">Org status</asp:label>
																		<asp:dropdownlist id="ddlstOrgStatus" runat="server"></asp:dropdownlist></TD>
																	<TD><BR>
																	</TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															<asp:label id="lblTheAddress1" runat="server" Font-Bold="True"></asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblAddress1" runat="server" Width="100%">Address</asp:label><BR>
																		<asp:textbox id="txtAddress1Native" runat="server" MaxLength="100"></asp:textbox></TD>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lbAddress1Info" runat="server" Width="100%">Address Info</asp:label><BR>
																		<asp:textbox id="txtAddress1InfoNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lbAddress1EN" runat="server" Width="100%">Address (EN)</asp:label><BR>
																		<asp:textbox id="txtAddress1EN" runat="server" MaxLength="100"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lbAddr1InfoEN" runat="server" Width="100%">Address Info (EN)</asp:label><BR>
																		<asp:textbox id="txtAddress1InfoEN" runat="server" MaxLength="50" EnableViewState="False"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lbPostalCode" runat="server" Width="100%">PostalCode</asp:label><BR>
																		<asp:textbox id="txtAddress1PostalCode" runat="server" MaxLength="30"></asp:textbox></TD>
																	<TD colSpan="2">
																		<asp:label id="lbCity1" runat="server">City</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		<asp:label id="lblCitySearch" runat="server">Search</asp:label><BR>
																		<asp:dropdownlist id="ddlCity1" runat="server"></asp:dropdownlist>
																		<asp:textbox id="txtCitySearch" runat="server" CssClass="short_input" MaxLength="10"></asp:textbox>&nbsp;
																		<asp:button id="btnCitySearch" runat="server" CssClass="popup" Text="..." CausesValidation="False"></asp:button>&nbsp;
																		<asp:requiredfieldvalidator id="rfvCity" runat="server" ControlToValidate="ddlCity1" ErrorMessage="Value missing!">*</asp:requiredfieldvalidator></TD>
																	<TD>
																		<asp:label id="lbPostBox" runat="server" Width="100%">PostBox</asp:label><BR>
																		<asp:textbox id="txtAddress1PostBox" runat="server" MaxLength="20"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblCountry1" runat="server" Width="100%">Country</asp:label><BR>
																		<asp:dropdownlist id="ddlstAddress1Country" runat="server" DataTextField="NameNative" DataValueField="CountryID"></asp:dropdownlist></TD>
																	<TD></TD>
																	<TD><BR>
																	</TD>
																	<TD></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															<asp:label id="lblTheAddress2" runat="server" Font-Bold="True">Address 2</asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblAddress2" runat="server" Width="100%">Address</asp:label><BR>
																		<asp:textbox id="txtAddress2Native" runat="server" MaxLength="100" EnableViewState="False"></asp:textbox></TD>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblAddress2Info" runat="server" Width="100%">Address Info</asp:label><BR>
																		<asp:textbox id="txtAddress2InfoNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblAddress2EN" runat="server" Width="100%">Address (EN)</asp:label><BR>
																		<asp:textbox id="txtAddress2EN" runat="server" MaxLength="100"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lblAddress2InfoEN" runat="server" Width="100%">Address Info (EN)</asp:label><BR>
																		<asp:textbox id="txtAddress2InfoEN" runat="server" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblPostalCode2" runat="server" Width="100%">Postal Code</asp:label><BR>
																		<asp:textbox id="txtAddress2PostalCode" runat="server" MaxLength="30"></asp:textbox></TD>
																	<TD colSpan="2">
																		<asp:label id="lblCity2" runat="server">City</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		<asp:label id="lblCity2Search" runat="server">Search</asp:label><BR>
																		<asp:dropdownlist id="ddlCity2" runat="server"></asp:dropdownlist>
																		<asp:textbox id="txtCity2Search" runat="server" CssClass="short_input" MaxLength="10"></asp:textbox>&nbsp;
																		<asp:button id="btnCity2Search" runat="server" CssClass="popup" Text="..." CausesValidation="False"></asp:button>&nbsp;
																		<asp:requiredfieldvalidator id="rfvCity2" runat="server" ControlToValidate="ddlCity2" ErrorMessage="Value missing!"
																			Enabled="False">*</asp:requiredfieldvalidator></TD>
																	<TD>
																		<asp:label id="lblPostBox2" runat="server" Width="100%">PostBox</asp:label><BR>
																		<asp:textbox id="txtAddress2PostBox" runat="server" MaxLength="20"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblCountry2" runat="server" Width="100%">Country</asp:label><BR>
																		<asp:dropdownlist id="ddlstAddress2Country" runat="server" DataTextField="NameNative" DataValueField="CountryID"></asp:dropdownlist><BR>
																	</TD>
																	<TD></TD>
																	<TD><BR>
																	</TD>
																	<TD></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblNumbers" runat="server">Label</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lbIDNumber1" runat="server" Width="100%">IDNumber 1</asp:label><BR>
																		<asp:textbox id="txtIDNumber1" runat="server" MaxLength="100"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lbType1" runat="server">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlstIDNumberType1" runat="server"></asp:dropdownlist></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lbIDNumber2" runat="server">IDNumber 2</asp:label><BR>
																		<asp:textbox id="txtIDNumber2" runat="server" MaxLength="100"></asp:textbox></TD>
																	<TD><asp:label id="lbType2" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlstIDNumberType2" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lbPhoneNumber1" runat="server" Width="100%">Phone number 1</asp:label><BR>
																		<asp:textbox id="txtPhoneNumber1" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblPhoneNumberType1" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlPhoneNumber1Type" runat="server"></asp:dropdownlist></TD>
																	<TD><asp:label id="lbPhoneNumber2" runat="server">Phone number 2</asp:label><BR>
																		<asp:textbox id="txtPhoneNumber2" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblPhoneNumberType2" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlPhoneNumber2Type" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblPhoneNumber3" runat="server">Phone number 3</asp:label><BR>
																		<asp:textbox id="txtPhoneNumber3" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lblPhoneNumberType3" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlPhoneNumber3Type" runat="server"></asp:dropdownlist></TD>
																	<TD>
																		<asp:label id="lblPhoneNumber4" runat="server">Phone number 4</asp:label><BR>
																		<asp:textbox id="txtPhoneNumber4" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblPhoneNumberType4" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlPhoneNumber4Type" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD height="23"></TD>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblAdditionalInfo" runat="server">Label</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 50%"><asp:label id="lblCustomerRemarks" runat="server" Width="100%">Remarks</asp:label><BR>
																		<asp:textbox id="txtCustomerRemarksNative" runat="server" MaxLength="1024" TextMode="MultiLine"
																			Height="114px" Width="100%"></asp:textbox></TD>
																	<TD><asp:label id="lblRemarksEN" runat="server" Width="100%">RemarksEN</asp:label><BR>
																		<asp:textbox id="txtCustomerRemarksEN" runat="server" MaxLength="1024" TextMode="MultiLine" Height="114px"
																			Width="100%"></asp:textbox></TD>
																</TR>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</TR>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblContactPerson1" runat="server" Font-Bold="True">Contact Person</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact1FirstNameNative" runat="server">First Name</asp:label><BR>
																		<asp:textbox id="txtContact1FirstNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact1SurnameNative" runat="server">Surname</asp:label><BR>
																		<asp:textbox id="txtContact1SurnameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact1FirstNameEN" runat="server">First Name (EN)</asp:label><BR>
																		<asp:textbox id="txtContact1FirstNameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lblContact1SurnameEN" runat="server">Surname (EN)</asp:label><BR>
																		<asp:textbox id="txtContact1SurnameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblContact1Email" runat="server">Email</asp:label><BR>
																		<asp:textbox id="txtContact1Email" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															<asp:label id="lblContactPerson2" runat="server" Font-Bold="True">Contact Person</asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblContact2FirstNameNative" runat="server">First Name</asp:label><BR>
																		<asp:textbox id="txtContact2FirstNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblContact2SurnameNative" runat="server">Surname</asp:label><BR>
																		<asp:textbox id="txtContact2SurnameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblContact2FirstNameEN" runat="server">First Name (EN)</asp:label><BR>
																		<asp:textbox id="txtContact2FirstNameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lblContact2SurnameEN" runat="server">Surname (EN)</asp:label><BR>
																		<asp:textbox id="txtContact2SurnameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblContact2Email" runat="server">Email</asp:label><BR>
																		<asp:textbox id="txtContact2Email" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															<asp:label id="lblContactPerson3" runat="server" Font-Bold="True">Contact Person</asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblContact3FirstNameNative" runat="server">First Name</asp:label><BR>
																		<asp:textbox id="txtContact3FirstNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblContact3SurnameNative" runat="server">Surname</asp:label><BR>
																		<asp:textbox id="txtContact3SurnameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblContact3FirstNameEN" runat="server">First Name (EN)</asp:label><BR>
																		<asp:textbox id="txtContact3FirstNameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lblContact3SurnameEN" runat="server">Surname (EN)</asp:label><BR>
																		<asp:textbox id="txtContact3SurnameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblContact3Email" runat="server">Email</asp:label><BR>
																		<asp:textbox id="txtContact3Email" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE id="Table6" cellSpacing="1" cellPadding="1" border="0" width="100%">
													<TR valign="top">
														<TD>
															<asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary>
															<asp:label id="lblErrorMessage" runat="server" ForeColor="Red">ErrorMessage</asp:label></TD>
														<TD align="right">
															<asp:button id="btnCancel" runat="server" CssClass="cancel_button" Text="cancel" CausesValidation="False"></asp:button><asp:button id="btRegCompany" runat="server" CssClass="confirm_button" Text="submit"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE> <!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
