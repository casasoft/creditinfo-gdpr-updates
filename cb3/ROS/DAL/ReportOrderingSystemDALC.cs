using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using Logging.BLL;
using ROS.BLL;
using cb3;

using Cig.Framework.Base.Configuration;
using cb3.Audit;

namespace ROS.DAL
{
    /// <remarks>
    /// This class provides the database functions and handling for Reports
    /// </remarks>
    public class ReportOrderingSystemDALC : BaseDALC
    {
        private const string className = "ReportOrderingSystemDALC";

        #region Init


        #endregion

        #region Individual

        public int CreateIndividual(IndividualBLLC theIndi)
        {
            const string funcName = "CreateIndividual(IndividualBLLC theIndi)";
            OleDbCommand myOleDbCommand;
            int creditInfoID = -1;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    myOleDbCommand = new OleDbCommand(
                        "INSERT INTO np_CreditInfoUser(Type,Email) VALUES(?,?)", myOleDbConn) { Transaction = myTrans };
                    var myParam = new OleDbParameter("Type", OleDbType.Char, 10)
                                  {
                                      Value = theIndi.Type,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Email", OleDbType.WChar)
                              {
                                  Value = (theIndi.EmailAddress.Count > 0 ? theIndi.EmailAddress[0] : ""),
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    // n� � s��ast innsetta ID
                    myOleDbCommand.Parameters.Clear();
                    myOleDbCommand.CommandText = "SELECT @@IDENTITY FROM np_CreditInfoUser";
                    creditInfoID = Convert.ToInt32(myOleDbCommand.ExecuteScalar());

                    myOleDbCommand.Parameters.Clear();
                    myOleDbCommand.CommandText =
                        "INSERT INTO np_Individual(CreditInfoID,FirstNameNative,FirstNameEN,SurNameNative,SurNameEN,ProfessionID,EducationID) VALUES(?,?,?,?,?,?,?)";
                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = creditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert FirstName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "FirstNameNative",
                        "FirstNameEN",
                        ParameterDirection.Input,
                        theIndi.FirstNameNative,
                        theIndi.FirstNameEN,
                        OleDbType.WChar);

                    // Insert FirstName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "SurNameNative",
                        "SurNameEN",
                        ParameterDirection.Input,
                        theIndi.LastNameNative,
                        theIndi.LastNameEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("ProfessionID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theIndi.ProfessionID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("EducationID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theIndi.EducationID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    if (theIndi.TelephoneNumbers.Count > 0)
                    {
                        InsertPhoneNumbers(theIndi.TelephoneNumbers, myOleDbCommand, creditInfoID);
                    }
                    if (theIndi.Address.Count > 0)
                    {
                        InsertAddress(theIndi.Address, myOleDbCommand, creditInfoID);
                    }
                    if (theIndi.IDNumbers.Count > 0)
                    {
                        InsertIDNumbers(theIndi.IDNumbers, myOleDbCommand, creditInfoID);
                    }
                    if (theIndi.EmailAddress.Count > 1 || theIndi.RemarksEN.Length > 0 ||
                        theIndi.RemarksNative.Length > 0)
                    {
                        InsertAdditionalCustomerInfo(theIndi, myOleDbCommand, creditInfoID);
                    }
                    if (theIndi.ContactPersons.Count > 0)
                    {
                        InsertContactPersons(theIndi.ContactPersons, myOleDbCommand, creditInfoID);
                    }

                    myTrans.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    myTrans.Rollback();
                    return -1;
                }
                finally
                {
                    // myOleDbCommand.Connection.Close(); // � ekki a� �urfa ef using er nota�
                }
                return creditInfoID;
            }
        }

        public int CountIndividualsInNationalAndCreditInfo(IndividualBLLC theIndi)
        {
            const string funcName = "CountIndividualsInNationalAndCreditInfo(IndividualBLLC theIndi)";

            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "Select COUNT(*) from ros_CIIDNationalIndividualView where ";

                if (theIndi.IDNumbers.Count > 0)
                {
                    var theID = (IDNumberBLLC)theIndi.IDNumbers[0];
                    if (theID.Number != "")
                    {
                        if (whereStatement)
                        {
                            query += " AND ";
                        }
                        query += "(IDNumber LIKE('" + theID.Number + "%') AND NumberTypeID = " + theID.NumberTypeID +
                                 ") ";
                        whereStatement = true;
                    }
                }
                if (theIndi.CreditInfoID > 0)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "CreditInfoID = " + theIndi.CreditInfoID + " ";
                    whereStatement = true;
                }
                if (theIndi.FirstNameNative != "" && theIndi.LastNameNative != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "((FirstNameNative LIKE(N'" + theIndi.FirstNameNative + "%') AND LastNameNative LIKE(N'%" +
                             theIndi.LastNameNative + "')) OR NationalName Like(N'" + theIndi.FirstNameNative + "%" +
                             theIndi.LastNameNative + "%'))";
                    whereStatement = true;
                }
                else
                {
                    if (theIndi.FirstNameNative != "")
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        query += "(FirstNameNative LIKE(N'" + theIndi.FirstNameNative + "%') OR NationalName Like(N'" +
                                 theIndi.FirstNameNative + "%')) ";
                        whereStatement = true;
                    }
                    if (theIndi.LastNameNative != "")
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        query += "(LastNameNative LIKE(N'" + theIndi.LastNameNative + "%') OR NationalName LIKE(N'%" +
                                 theIndi.LastNameNative + "%')) ";
                        whereStatement = true;
                    }
                }

                if (theIndi.FirstNameEN != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "FirstNameEN LIKE(N'" + theIndi.FirstNameEN + "%') ";
                    whereStatement = true;
                }

                if (theIndi.LastNameEN != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "LastNameEN LIKE(N'" + theIndi.LastNameEN + "%') ";
                    whereStatement = true;
                }
                if (theIndi.Address.Count > 0)
                {
                    var theAddress = (AddressBLLC)theIndi.Address[0];
                    if (theAddress.StreetEN != "")
                    {
                        if (whereStatement)
                        {
                            query += " AND ";
                        }
                        query += "AddressEN LIKE(N'" + theAddress.StreetEN + "%') ";
                        whereStatement = true;
                    }
                    if (theAddress.StreetNative != "")
                    {
                        if (whereStatement)
                        {
                            query += " AND ";
                        }
                        query += "(AddressNative LIKE(N'" + theAddress.StreetNative + "%') OR NationalAddress LIKE(N'" +
                                 theAddress.StreetNative + "%'))";
                        whereStatement = true;
                    }
                    if (theAddress.PostalCode != "")
                    {
                        if (whereStatement)
                        {
                            query += " AND ";
                        }
                        query += "(PostalCode = '" + theAddress.PostalCode + "' OR NationalPostNumber = '" +
                                 theAddress.PostalCode + "') ";
                        whereStatement = true;
                    }
                    /*					if(theAddress.CityID != "?????????")
															{

										}
										if(theAddress.CountryID != "??????????")
										{
					
										}
					*/
                }

                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    query += "IDNumber LIKE('%') AND NumberTypeID = 1 ";
                }

                int theCount = -1;

                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            theCount = reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return -1;
                }
                return theCount;
            }
        }

        public ArrayList FindIndividualInNationalAndCreditInfo(IndividualBLLC theIndi)
        {
            const string funcName = "FindIndividualInNationalAndCreditInfo(IndividualBLLC theIndi)";

            string hits = CigConfig.Configure("lookupsettings.ROSMaxHits").Trim();


            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "Select TOP " + hits + " * from ros_CIIDNationalIndividualView ";
                string where = "where ";

                if (theIndi.IDNumbers.Count > 0)
                {
                    var theID = (IDNumberBLLC)theIndi.IDNumbers[0];
                    if (theID.Number != "")
                    {
                        where += "(IDNumber LIKE('" + theID.Number + "%') AND NumberTypeID = " + theID.NumberTypeID +
                                 ") ";

                        whereStatement = true;
                    }
                }
                if (theIndi.CreditInfoID > 0)
                {
                    if (whereStatement)
                    {
                        where += "AND ";
                    }

                    where += "CreditInfoID = " + theIndi.CreditInfoID + " ";

                    whereStatement = true;
                }
                if (theIndi.FirstNameNative != "" && theIndi.LastNameNative != "")
                {
                    if (whereStatement)
                    {
                        where += "AND ";
                    }

                    where += "((FirstNameNative LIKE(N'" + theIndi.FirstNameNative + "%') AND LastNameNative LIKE(N'%" +
                             theIndi.LastNameNative + "')) OR NationalName Like(N'" + theIndi.FirstNameNative + "%" +
                             theIndi.LastNameNative + "%'))";

                    whereStatement = true;
                }
                else
                {
                    if (theIndi.FirstNameNative != "")
                    {
                        if (whereStatement)
                        {
                            where += "AND ";
                        }

                        where += "(FirstNameNative LIKE(N'" + theIndi.FirstNameNative + "%') OR NationalName Like(N'" +
                                 theIndi.FirstNameNative + "%')) ";

                        whereStatement = true;
                    }
                    if (theIndi.LastNameNative != "")
                    {
                        if (whereStatement)
                        {
                            where += "AND ";
                        }

                        where += "(LastNameNative LIKE(N'" + theIndi.LastNameNative + "%') OR NationalName LIKE(N'%" +
                                 theIndi.LastNameNative + "%')) ";

                        whereStatement = true;
                    }
                }

                if (theIndi.FirstNameEN != "")
                {
                    if (whereStatement)
                    {
                        where += "AND ";
                    }

                    where += "FirstNameEN LIKE(N'" + theIndi.FirstNameEN + "%') ";

                    whereStatement = true;
                }

                if (theIndi.LastNameEN != "")
                {
                    if (whereStatement)
                    {
                        where += "AND ";
                    }

                    where += "LastNameEN LIKE(N'" + theIndi.LastNameEN + "%') ";

                    whereStatement = true;
                }
                if (theIndi.Address.Count > 0)
                {
                    var theAddress = (AddressBLLC)theIndi.Address[0];
                    if (theAddress.StreetEN != "")
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "AddressEN LIKE(N'" + theAddress.StreetEN + "%') ";

                        whereStatement = true;
                    }
                    if (theAddress.StreetNative != "")
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "(AddressNative LIKE(N'" + theAddress.StreetNative + "%') OR NationalAddress LIKE(N'" +
                                 theAddress.StreetNative + "%'))";

                        whereStatement = true;
                    }
                    if (theAddress.PostalCode != "")
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }
                        where += "(PostalCode = '" + theAddress.PostalCode + "' OR NationalPostNumber = '" +
                                 theAddress.PostalCode + "') ";

                        whereStatement = true;
                    }
                    /*if(theAddress.CityID != -1)
					{
						if(whereStatement)
							query += " AND ";
						query += "CityID = " + theAddress.CityID + " ";
						whereStatement = true;
					}					
					if(theAddress.CountryID != "??????????")
					{
					
					}*/
                }

                // nothing to query by ... send it all back
                if (whereStatement)
                {
                    query += where;
                }

                query += "ORDER BY NationalName ";

                var theIndividuals = new ArrayList();

                try
                {
                    myOleDbConn.Open();
                    //	var myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    IndividualBLLC myIndi;

                    while (reader.Read())
                    {
                        myIndi = new IndividualBLLC();

                        if (!reader.IsDBNull(0))
                        {
                            myIndi.NationalName = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            myIndi.NationalPostNumber = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            myIndi.NationalAddress = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myIndi.CreditInfoID = reader.GetInt32(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myIndi.FirstNameEN = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            myIndi.FirstNameNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            myIndi.LastNameEN = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            myIndi.LastNameNative = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8) || !reader.IsDBNull(9) || !reader.IsDBNull(10) || !reader.IsDBNull(11) ||
                            !reader.IsDBNull(12) || !reader.IsDBNull(13) || !reader.IsDBNull(14) || !reader.IsDBNull(15) ||
                            !reader.IsDBNull(17))
                        {
                            var theAddress = new AddressBLLC();

                            if (!reader.IsDBNull(8))
                            {
                                theAddress.StreetEN = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                theAddress.StreetNative = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                theAddress.CityNameEN = reader.GetString(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                theAddress.CityNameNative = reader.GetString(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                theAddress.CityID = reader.GetInt32(12);
                            }
                            if (!reader.IsDBNull(13))
                            {
                                theAddress.PostalCode = reader.GetString(13);
                            }
                            if (!reader.IsDBNull(14))
                            {
                                theAddress.CountryNameEN = reader.GetString(14);
                            }
                            if (!reader.IsDBNull(15))
                            {
                                theAddress.CountryNameNative = reader.GetString(15);
                            }
                            if (!reader.IsDBNull(17))
                            {
                                theAddress.CountryID = reader.GetInt32(17);
                            }

                            myIndi.Address.Add(theAddress);
                        }

                        myIndi.IsCompany = false;

                        if (!reader.IsDBNull(18))
                        {
                            var theID = new IDNumberBLLC { Number = reader.GetString(18) };

                            if (!reader.IsDBNull(19))
                            {
                                theID.NumberTypeID = reader.GetInt32(19);
                            }

                            myIndi.IDNumbers.Add(theID);
                        }

                        theIndividuals.Add(myIndi);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                }
                return theIndividuals;
            }
        }

        public int CountIndividuals(IndividualBLLC theIndi)
        {
            const string funcName = "CountIndividuals(IndividualBLLC theIndi)";
            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "SELECT COUNT(*) FROM ros_IndividualView";
                string where = " WHERE ";
                if (theIndi.CreditInfoID != -1)
                {
                    where += "CreditInfoID = " + theIndi.CreditInfoID;
                    whereStatement = true;
                }
                if (theIndi.IDNumbers.Count > 0)
                {
                    var theID = (IDNumberBLLC)theIndi.IDNumbers[0];

                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "IDNumber = '" + theID.Number + "' AND NumberTypeID = " + theID.NumberTypeID;
                    whereStatement = true;
                }
                if (theIndi.FirstNameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "FirstNameNative LIKE(N'" + theIndi.FirstNameNative + "%')";
                    whereStatement = true;
                }
                if (theIndi.FirstNameEN.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "FirstNameEN LIKE(N'" + theIndi.FirstNameEN + "%')";
                    whereStatement = true;
                }
                if (theIndi.LastNameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "LastNameNative LIKE(N'" + theIndi.LastNameNative + "%')";
                    whereStatement = true;
                }
                if (theIndi.LastNameEN.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "LastNameEN LIKE(N'" + theIndi.LastNameEN + "%')";
                    whereStatement = true;
                }
                if (theIndi.Address.Count > 0)
                {
                    var theAddress = new AddressBLLC();
                    theAddress = (AddressBLLC)theIndi.Address[0];

                    if (theAddress.CityID != -1)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "CityID = " + theAddress.CityID;
                        whereStatement = true;
                    }
                    /*					if(theAddress.CountryID != -1)
                                        {
                                            if(whereStatement)
                                                where += " AND ";

                                            where += "CountryID = " + theAddress.CountryID;
                                            whereStatement = true;
                                        }
                    */
                    if (theAddress.StreetNative.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "AddressNative LIKE(N'" + theAddress.StreetNative + "%')";
                        whereStatement = true;
                    }
                    if (theAddress.StreetEN.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "AddressEN LIKE(N'" + theAddress.StreetEN + "%')";
                        whereStatement = true;
                    }
                    if (theAddress.PostalCode.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "PostalCode = '" + theAddress.PostalCode + "'";
                        whereStatement = true;
                    }
                }

                // nothing to query by ... send it all back
                if (whereStatement)
                {
                    query += where;
                }

                int count = -1;

                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            count = reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return -1;
                }
                return count;
            }
        }

        public DataSet FindIndividual(IndividualBLLC theIndi)
        {
            const string funcName = "FindIndividual(IndividualBLLC theIndi)";

            string hits = CigConfig.Configure("lookupsettings.ROSMaxHits").Trim();

            var mySet = new DataSet();
            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "SELECT DISTINCT TOP " + hits +
                               " CreditInfoID, IDNumber, NumberTypeID AS IDNumberType, '' AS NameEN, '' AS NameNative, FirstNameEN, FirstNameNative, LastNameEN, LastNameNative, AddressEN, AddressNative, CityEN, CityNative, CityID, PostalCode, CountryEN, CountryNative, CountryID, 'False' AS IsCompany FROM ros_IndividualView";
                string where = " WHERE ";
                if (theIndi.CreditInfoID != -1)
                {
                    where += "CreditInfoID = " + theIndi.CreditInfoID;
                    whereStatement = true;
                }
                if (theIndi.IDNumbers.Count > 0)
                {
                    var theID = (IDNumberBLLC)theIndi.IDNumbers[0];

                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "IDNumber = '" + theID.Number + "' AND NumberTypeID = " + theID.NumberTypeID;

                    whereStatement = true;
                }
                if (theIndi.FirstNameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "FirstNameNative LIKE(N'" + theIndi.FirstNameNative + "%')";
                    whereStatement = true;
                }
                if (theIndi.FirstNameEN.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "FirstNameEN LIKE(N'" + theIndi.FirstNameEN + "%')";
                    whereStatement = true;
                }
                if (theIndi.LastNameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "LastNameNative LIKE(N'" + theIndi.LastNameNative + "%')";
                    whereStatement = true;
                }
                if (theIndi.LastNameEN.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "LastNameEN LIKE(N'" + theIndi.LastNameEN + "%')";
                    whereStatement = true;
                }
                if (theIndi.Address.Count > 0)
                {
                    var theAddress = (AddressBLLC)theIndi.Address[0];

                    if (theAddress.CityID != -1)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "CityID = " + theAddress.CityID;
                        whereStatement = true;
                    }
                    /*					if(theAddress.CountryID != -1)
                                        {
                                            if(whereStatement)
                                                where += " AND ";

                                            where += "CountryID = " + theAddress.CountryID;
                                            whereStatement = true;
                                        }
                    */
                    if (theAddress.StreetNative.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "AddressNative LIKE(N'" + theAddress.StreetNative + "%')";
                        whereStatement = true;
                    }
                    if (theAddress.StreetEN.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "AddressEN LIKE(N'" + theAddress.StreetEN + "%')";
                        whereStatement = true;
                    }
                    if (theAddress.PostalCode.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "PostalCode = '" + theAddress.PostalCode + "'";
                        whereStatement = true;
                    }
                }

                // nothing to query by ... send it all back
                if (whereStatement)
                {
                    query += where;
                }

                try
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                }
                return mySet;
            }
        }

        public ArrayList FindIndividualAsArray(IndividualBLLC theIndi)
        {
            const string funcName = "FindIndividualAsArray(IndividualBLLC theIndi)";

            string hits = CigConfig.Configure("lookupsettings.ROSMaxHits").Trim();

            var theIndividuals = new ArrayList();
            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "SELECT DISTINCT TOP " + hits +
                               " CreditInfoID, IDNumber, NumberTypeID, FirstNameEN, FirstNameNative, LastNameEN, LastNameNative, AddressEN, AddressNative, CityEN, CityNative, CityID, PostalCode, CountryEN, CountryNative, CountryID FROM ros_IndividualView";
                string where = " WHERE ";
                if (theIndi.CreditInfoID != -1)
                {
                    where += "CreditInfoID = " + theIndi.CreditInfoID;
                    whereStatement = true;
                }
                if (theIndi.IDNumbers.Count > 0)
                {
                    var theID = new IDNumberBLLC();
                    theID = (IDNumberBLLC)theIndi.IDNumbers[0];

                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "IDNumber = '" + theID.Number + "' AND NumberTypeID = " + theID.NumberTypeID;

                    whereStatement = true;
                }

                if (theIndi.FirstNameNative.Length > 0 && theIndi.LastNameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "((FirstNameNative LIKE(N'" + theIndi.FirstNameNative + "%') AND LastNameNative LIKE(N'" +
                             theIndi.LastNameNative + "%'))";
                    where += " OR (FirstNameEN LIKE(N'" + theIndi.FirstNameNative + "%') AND LastNameEN LIKE(N'" +
                             theIndi.LastNameNative + "%')))";

                    whereStatement = true;
                }
                else if (theIndi.FirstNameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "(FirstNameNative LIKE(N'" + theIndi.FirstNameNative + "%')";
                    where += " OR (FirstNameEN LIKE(N'" + theIndi.FirstNameNative + "%')))";

                    whereStatement = true;
                }
                else if (theIndi.LastNameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "(LastNameNative LIKE(N'" + theIndi.LastNameNative + "%')";
                    where += " OR (LastNameEN LIKE(N'" + theIndi.LastNameNative + "%')))";

                    whereStatement = true;
                }

                /*
                                if(theIndi.FirstNameNative.Length > 0)
                                {
                                    if(whereStatement)
                                        where += " AND ";
                                    where += "FirstNameNative LIKE(N'" + theIndi.FirstNameNative + "%')";
                                    whereStatement = true;	
                                }
                                if(theIndi.FirstNameEN.Length > 0)
                                {
                                    if(whereStatement)
                                        where += " AND ";
                                    where += "FirstNameEN LIKE(N'" + theIndi.FirstNameEN + "%')";
                                    whereStatement = true;	
                                }
                                if(theIndi.LastNameNative.Length > 0)
                                {
                                    if(whereStatement)
                                        where += " AND ";
                                    where += "LastNameNative LIKE(N'" + theIndi.LastNameNative + "%')";
                                    whereStatement = true;	
                                }
                                if(theIndi.LastNameEN.Length > 0)
                                {
                                    if(whereStatement)
                                        where += " AND ";
                                    where += "LastNameEN LIKE(N'" + theIndi.LastNameEN + "%')";
                                    whereStatement = true;	
                                }
                */
                if (theIndi.Address.Count > 0)
                {
                    var theAddress = (AddressBLLC)theIndi.Address[0];

                    if (theAddress.CityID != -1)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "CityID = " + theAddress.CityID;
                        whereStatement = true;
                    }
                    /*					if(theAddress.CountryID != -1)
                                        {
                                            if(whereStatement)
                                                where += " AND ";

                                            where += "CountryID = " + theAddress.CountryID;
                                            whereStatement = true;
                                        }
                    */
                    if (theAddress.StreetNative.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "(AddressNative LIKE(N'" + theAddress.StreetNative + "%') OR AddressEN LIKE(N'" +
                                 theAddress.StreetEN + "%'))";
                        whereStatement = true;
                    }
                    /*					if(theAddress.StreetEN.Length > 0)
                                        {
                                            if(whereStatement)
                                                where += " AND ";

                                            where += "AddressEN LIKE(N'" + theAddress.StreetEN + "%')";
                                            whereStatement = true;
                                        }
                    */
                    if (theAddress.PostalCode.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "PostalCode = '" + theAddress.PostalCode + "'";
                        whereStatement = true;
                    }
                }

                // nothing to query by ... send it all back
                if (whereStatement)
                {
                    query += where;
                }

                try
                {
                    myOleDbConn.Open();
                    //	var myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    IndividualBLLC myIndi;

                    while (reader.Read())
                    {
                        myIndi = new IndividualBLLC();

                        if (!reader.IsDBNull(0))
                        {
                            myIndi.CreditInfoID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            var theID = new IDNumberBLLC { Number = reader.GetString(1) };

                            if (!reader.IsDBNull(2))
                            {
                                theID.NumberTypeID = reader.GetInt32(2);
                            }

                            myIndi.IDNumbers.Add(theID);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myIndi.FirstNameEN = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myIndi.FirstNameNative = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            myIndi.LastNameEN = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            myIndi.LastNameNative = reader.GetString(6);
                        }

                        if (!reader.IsDBNull(7) || !reader.IsDBNull(8) || !reader.IsDBNull(9) || !reader.IsDBNull(10) ||
                            !reader.IsDBNull(11) || !reader.IsDBNull(12) || !reader.IsDBNull(13) || !reader.IsDBNull(14) ||
                            !reader.IsDBNull(15))
                        {
                            var theAddress = new AddressBLLC();

                            if (!reader.IsDBNull(7))
                            {
                                theAddress.StreetEN = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8))
                            {
                                theAddress.StreetNative = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                theAddress.CityNameEN = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                theAddress.CityNameNative = reader.GetString(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                theAddress.CityID = reader.GetInt32(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                theAddress.PostalCode = reader.GetString(12);
                            }
                            if (!reader.IsDBNull(13))
                            {
                                theAddress.CountryNameEN = reader.GetString(13);
                            }
                            if (!reader.IsDBNull(14))
                            {
                                theAddress.CountryNameNative = reader.GetString(14);
                            }
                            if (!reader.IsDBNull(15))
                            {
                                theAddress.CountryID = reader.GetInt32(15);
                            }

                            myIndi.Address.Add(theAddress);
                        }

                        myIndi.IsCompany = false;

                        theIndividuals.Add(myIndi);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
                return theIndividuals;
            }
        }

        public int UpdateIndividual(IndividualBLLC theIndi)
        {
            const string funcName = "UpdateIndividual(IndividualBLLC theIndi)";
            OleDbCommand myOleDbCommand;
            int creditInfoID = theIndi.CreditInfoID;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    myOleDbCommand =
                        new OleDbCommand(
                            "UPDATE np_Individual SET FirstNameNative = ?,FirstNameEN = ?,SurNameNative = ?,SurNameEN = ?,ProfessionID = ?,EducationID = ?, LastUpdate = ?, Processed = 0 WHERE CreditInfoID = ?",
                            myOleDbConn) { Transaction = myTrans };

                    // Insert FirstName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "FirstNameNative",
                        "FirstNameEN",
                        ParameterDirection.Input,
                        theIndi.FirstNameNative,
                        theIndi.FirstNameEN,
                        OleDbType.WChar);

                    // Insert FirstName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "SurNameNative",
                        "SurNameEN",
                        ParameterDirection.Input,
                        theIndi.LastNameNative,
                        theIndi.LastNameEN,
                        OleDbType.WChar);

                    var myParam = new OleDbParameter("ProfessionID", OleDbType.Integer)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = theIndi.ProfessionID
                                  };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("EducationID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theIndi.EducationID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("LastUpdate", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = DateTime.Today
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theIndi.CreditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    if (theIndi.EmailAddress.Count > 0)
                    {
                        myOleDbCommand.CommandText = "UPDATE np_CreditInfoUser SET Email = ? WHERE CreditInfoID = ?";

                        myParam = new OleDbParameter();

                        myOleDbCommand.Parameters.Clear();
                        var email = (string)theIndi.EmailAddress[0];
                        myParam = new OleDbParameter("Email", OleDbType.VarChar)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = email
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = creditInfoID
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    }

                    UpdatePhoneNumbers(theIndi.TelephoneNumbers, myOleDbCommand, theIndi.CreditInfoID);
                    UpdateAddress(theIndi.Address, myOleDbCommand, theIndi.CreditInfoID);
                    UpdateIDNumbers(theIndi.IDNumbers, myOleDbCommand, theIndi.CreditInfoID);
                    UpdateContactPersons(theIndi.ContactPersons, myOleDbCommand, creditInfoID);
                    UpdateAdditionalCustomerInfo(theIndi, myOleDbCommand, creditInfoID);

                    myTrans.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    myTrans.Rollback();
                    return -1;
                }
                return creditInfoID;
            }
        }

        public IndividualBLLC GetIndividual(string creditInfoID)
        {
            const string funcName = "GetIndividual(string creditInfoID)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(
                        "SELECT cre.CreditInfoID, " +
                        "cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, " +
                        "ind.SurNameEN, prof.NameNative AS ProfessionNameNative, prof.NameEN AS ProfessionNameEN, " +
                        "prof.DescriptionEN, prof.DescriptionNative, prof.ProfessionID, " +
                        "edu.NameEN AS EdicationNameEN, edu.NameNative AS EducationNameNative, " +
                        "edu.DescriptionEN AS EducationDescriptionEN, edu.DescriptionNative AS EducationDescriptionNative, " +
                        "edu.EducationID, cre.Email, dbo.ros_AdditionalCustomerInfo.Email1, dbo.ros_AdditionalCustomerInfo.Email2, " +
                        "dbo.ros_AdditionalCustomerInfo.RemarksEN, dbo.ros_AdditionalCustomerInfo.RemarksNative " +
                        "FROM dbo.np_Individual ind INNER JOIN dbo.np_CreditInfoUser cre ON ind.CreditInfoID = cre.CreditInfoID " +
                        "LEFT OUTER JOIN dbo.ros_AdditionalCustomerInfo ON cre.CreditInfoID = dbo.ros_AdditionalCustomerInfo.CreditInfoID " +
                        "LEFT OUTER JOIN dbo.np_Profession prof ON ind.ProfessionID = prof.ProfessionID " +
                        "LEFT OUTER JOIN dbo.np_Education edu ON ind.EducationID = edu.EducationID " +
                        "WHERE (cre.CreditInfoID = " + creditInfoID + ")",
                        myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    var myIndi = new IndividualBLLC();
                    if (reader.Read())
                    {
                        myIndi.IsCompany = false;
                        myIndi.CreditInfoID = reader.GetInt32(0);
                        myIndi.Type = reader.GetString(1);
                        if (!reader.IsDBNull(2))
                        {
                            myIndi.FirstNameNative = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myIndi.FirstNameEN = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myIndi.LastNameNative = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            myIndi.LastNameEN = reader.GetString(5);
                        }
                        //					if(!reader.IsDBNull(6))
                        //						myIndi.ProfessionNameNative = reader.GetString(6);
                        //					if(!reader.IsDBNull(7))
                        //						myIndi.ProfessionNameEN = reader.GetString(7);
                        //					if(!reader.IsDBNull(8))
                        //						myIndi.ProfessionDescriptionEN = reader.GetString(8);
                        //					if(!reader.IsDBNull(9))
                        //						myIndi.ProfessionDescriptionNative = reader.GetString(9);
                        if (!reader.IsDBNull(10))
                        {
                            myIndi.ProfessionID = reader.GetInt32(10);
                        }
                        //					if(!reader.IsDBNull(11))
                        //						myIndi.EducationEN = reader.GetString(11);
                        //					if(!reader.IsDBNull(12))
                        //						myIndi.EducationNative = reader.GetString(12);
                        //					if(!reader.IsDBNull(13))
                        //						myIndi.EducationDescriptionEN = reader.GetString(13);
                        //					if(!reader.IsDBNull(14))
                        //						myIndi.EducationDescriptionNative = reader.GetString(14);
                        if (!reader.IsDBNull(15))
                        {
                            myIndi.EducationID = reader.GetInt32(15);
                        }

                        if (!reader.IsDBNull(16))
                        {
                            myIndi.EmailAddress.Add(reader.GetString(16));
                        }
                        if (!reader.IsDBNull(17))
                        {
                            myIndi.EmailAddress.Add(reader.GetString(17));
                        }
                        if (!reader.IsDBNull(18))
                        {
                            myIndi.EmailAddress.Add(reader.GetString(18));
                        }
                        if (!reader.IsDBNull(19))
                        {
                            myIndi.RemarksEN = reader.GetString(19);
                        }
                        if (!reader.IsDBNull(20))
                        {
                            myIndi.RemarksNative = reader.GetString(20);
                        }

                        // get the collections
                        myIndi.Address = GetAddressList(myIndi.CreditInfoID);
                        myIndi.TelephoneNumbers = GetPhoneNumberList(myIndi.CreditInfoID);
                        myIndi.IDNumbers = GetIDNumberList(myIndi.CreditInfoID);
                        myIndi.ContactPersons = GetContactPersons(myIndi.CreditInfoID);
                    }
                    return myIndi;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        #endregion

        #region Company

        public int CreateCompany(CompanyBLLC theCompany)
        {
            const string funcName = "CreateCompany(CompanyBLLC theCompany)";
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                int creditInfoID = -1;
                try
                {
                    GetLatestCreditInfoID();
                    myOleDbCommand = new OleDbCommand(
                        "INSERT INTO np_CreditInfoUser(Type,Email) VALUES(?,?)", myOleDbConn) { Transaction = myTrans };
                    var myParam = new OleDbParameter("Type", OleDbType.Char, 10)
                                  {
                                      Value = theCompany.Type,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Email", OleDbType.WChar);
                    if (theCompany.EmailAddress.Count > 0)
                    {
                        myParam.Value = theCompany.EmailAddress[0];
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);
                    }
                    else
                    {
                        myParam.Value = "";
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);
                    }
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    // n� � s��ast innsetta ID
                    myOleDbCommand.Parameters.Clear();
                    myOleDbCommand.CommandText = "SELECT @@IDENTITY FROM np_CreditInfoUser";
                    creditInfoID = Convert.ToInt32(myOleDbCommand.ExecuteScalar());

                    myOleDbCommand.Parameters.Clear();
                    myOleDbCommand.CommandText =
                        "INSERT INTO np_Companys(CreditInfoID,NameNative,NameEN,Established,LastContacted,URL,FuncID,org_status_code) VALUES(?,?,?,?,?,?,?,?)";
                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = creditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert FirstName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "NameNative",
                        "NameEN",
                        ParameterDirection.Input,
                        theCompany.NameNative,
                        theCompany.NameEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("Established", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = theCompany.Established
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("LastContacted", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = theCompany.LastContacted
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("URL", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theCompany.WebPage
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("FuncID", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theCompany.NaceID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("org_status_code", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theCompany.OrgStatus
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    if (theCompany.TelephoneNumbers.Count > 0)
                    {
                        InsertPhoneNumbers(theCompany.TelephoneNumbers, myOleDbCommand, creditInfoID);
                    }
                    if (theCompany.Address.Count > 0)
                    {
                        InsertAddress(theCompany.Address, myOleDbCommand, creditInfoID);
                    }
                    if (theCompany.IDNumbers.Count > 0)
                    {
                        InsertIDNumbers(theCompany.IDNumbers, myOleDbCommand, creditInfoID);
                    }
                    if (theCompany.EmailAddress.Count > 1 || theCompany.RemarksEN.Length > 0 ||
                        theCompany.RemarksNative.Length > 0)
                    {
                        InsertAdditionalCustomerInfo(theCompany, myOleDbCommand, creditInfoID);
                    }
                    if (theCompany.ContactPersons.Count > 0)
                    {
                        InsertContactPersons(theCompany.ContactPersons, myOleDbCommand, creditInfoID);
                    }

                    myTrans.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    myTrans.Rollback();
                    return -1;
                }
                return creditInfoID;
            }
        }

        public int UpdateCompany(CompanyBLLC theComp)
        {
            const string funcName = "UpdateCompany(CompanyBLLC theComp)";
            OleDbCommand myOleDbCommand;
            int creditInfoID = theComp.CreditInfoID;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    myOleDbCommand =
                        new OleDbCommand(
                            "UPDATE np_Companys SET NameNative = ?,NameEN = ?,Established = ?,LastContacted = ?,URL = ?,FuncID = ?, org_status_code = ?, LastUpdate = ? " +
                            "WHERE CreditInfoID = ?",
                            myOleDbConn) { Transaction = myTrans };

                    // Insert FirstName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "NameNative",
                        "NameEN",
                        ParameterDirection.Input,
                        theComp.NameNative,
                        theComp.NameEN,
                        OleDbType.WChar);

                    var myParam = new OleDbParameter("Established", OleDbType.Date)
                                             {
                                                 IsNullable = true,
                                                 Direction = ParameterDirection.Input,
                                                 Value = theComp.Established
                                             };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("LastContacted", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.LastContacted
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("URL", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.WebPage
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("FuncID", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.NaceID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("org_status_code", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.OrgStatus
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("LastUpdate", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = DateTime.Today
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.CreditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    if (theComp.EmailAddress.Count > 0)
                    {
                        myOleDbCommand.CommandText = "UPDATE np_CreditInfoUser SET Email = ? WHERE CreditInfoID = ?";

                        myParam = new OleDbParameter();

                        myOleDbCommand.Parameters.Clear();
                        var email = (string)theComp.EmailAddress[0];
                        myParam = new OleDbParameter("Email", OleDbType.VarChar)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = email
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = creditInfoID
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    }

                    UpdatePhoneNumbers(theComp.TelephoneNumbers, myOleDbCommand, theComp.CreditInfoID);
                    UpdateAddress(theComp.Address, myOleDbCommand, theComp.CreditInfoID);
                    UpdateIDNumbers(theComp.IDNumbers, myOleDbCommand, theComp.CreditInfoID);
                    UpdateAdditionalCustomerInfo(theComp, myOleDbCommand, theComp.CreditInfoID);
                    UpdateContactPersons(theComp.ContactPersons, myOleDbCommand, theComp.CreditInfoID);

                    myTrans.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    myTrans.Rollback();
                    return -1;
                }
                finally
                {
                    // myOleDbCommand.Connection.Close(); // � ekki a� �urfa ef using er nota�
                }
                return creditInfoID;
            }
        }

        public int CountCompaniesInNationalAndCreditInfo(CompanyBLLC theCompany)
        {
            const string funcName = "CountCompanyInNationalAndCreditInfo(CompanyBLLC theCompany)";

            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "Select COUNT(*) from ros_CIIDNationalCompanyView where ";

                if (theCompany.IDNumbers.Count > 0)
                {
                    var theID = (IDNumberBLLC)theCompany.IDNumbers[0];
                    if (theID.Number != "")
                    {
                        query += "(IDNumber LIKE('" + theID.Number + "%') AND NumberTypeID = " + theID.NumberTypeID +
                                 ") ";
                        whereStatement = true;
                    }
                }
                if (theCompany.CreditInfoID > 0)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "CreditInfoID = " + theCompany.CreditInfoID + " ";
                    whereStatement = true;
                }
                if (theCompany.NameNative != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "((NameNative LIKE(N'%" + theCompany.NameNative + "%')) OR (NationalName LIKE(N'%" +
                             theCompany.NameNative + "%'))) ";
                    whereStatement = true;
                }

                if (theCompany.NameEN != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "NameEN LIKE(N'%" + theCompany.NameEN + "%') ";
                    whereStatement = true;
                }

                if (theCompany.Address.Count > 0)
                {
                    var theAddress = (AddressBLLC)theCompany.Address[0];
                    if (theAddress.StreetEN != "")
                    {
                        if (whereStatement)
                        {
                            query += " AND ";
                        }
                        query += "AddressEN LIKE(N'" + theAddress.StreetEN + "%') ";
                        whereStatement = true;
                    }
                    if (theAddress.StreetNative != "")
                    {
                        if (whereStatement)
                        {
                            query += " AND ";
                        }
                        query += "(AddressNative LIKE(N'" + theAddress.StreetNative + "%') OR NationalAddress LIKE(N'" +
                                 theAddress.StreetNative + "%'))";
                        whereStatement = true;
                    }
                    /*if(theAddress.CityID != -1)
					{
						if(whereStatement)
							query += " AND ";
						query += "CityID = " + theAddress.CityID + " ";
						whereStatement = true;
					}
					if(theAddress.CountryID != "??????????")
					{
					
					}*/
                    if (theAddress.PostalCode != "")
                    {
                        if (whereStatement)
                        {
                            query += " AND ";
                        }
                        query += "(NationalPostalCode = '" + theAddress.PostalCode + "' OR PostalCode = '" +
                                 theAddress.PostalCode + "') ";
                        whereStatement = true;
                    }
                }

                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    query += "IDNumber LIKE('%') AND NumberTypeID = 1 ";
                }

                int theCount = -1;

                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            theCount = reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return -1;
                }
                return theCount;
            }
        }

        public ArrayList FindCompanyInNationalAndCreditInfo(CompanyBLLC theCompany)
        {
            const string funcName = "FindCompanyInNationalAndCreditInfo(CompanyBLLC theCompany)";

            string hits = CigConfig.Configure("lookupsettings.ROSMaxHits").Trim();


            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "Select TOP " + hits + " * from ros_CIIDNationalCompanyView ";
                string where = "where ";

                if (theCompany.IDNumbers.Count > 0)
                {
                    var theID = (IDNumberBLLC)theCompany.IDNumbers[0];
                    if (theID.Number != "")
                    {
                        where += "(IDNumber LIKE('" + theID.Number + "%') AND NumberTypeID = " + theID.NumberTypeID +
                                 ") ";

                        whereStatement = true;
                    }
                }
                if (theCompany.CreditInfoID > 0)
                {
                    if (whereStatement)
                    {
                        where += "AND ";
                    }

                    where += "CreditInfoID = " + theCompany.CreditInfoID + " ";

                    whereStatement = true;
                }
                if (theCompany.NameNative != "")
                {
                    if (whereStatement)
                    {
                        where += "AND ";
                    }

                    where += "((NameNative LIKE(N'%" + theCompany.NameNative + "%')) OR (NationalName LIKE(N'%" +
                             theCompany.NameNative + "%'))) ";

                    whereStatement = true;
                }

                if (theCompany.NameEN != "")
                {
                    if (whereStatement)
                    {
                        where += "AND ";
                    }

                    where += "NameEN LIKE(N'%" + theCompany.NameEN + "%') ";

                    whereStatement = true;
                }

                if (theCompany.Address.Count > 0)
                {
                    var theAddress = (AddressBLLC)theCompany.Address[0];
                    if (theAddress.StreetEN != "")
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "AddressEN LIKE(N'" + theAddress.StreetEN + "%') ";

                        whereStatement = true;
                    }
                    if (theAddress.StreetNative != "")
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "(AddressNative LIKE(N'" + theAddress.StreetNative + "%') OR NationalAddress LIKE(N'" +
                                 theAddress.StreetNative + "%'))";

                        whereStatement = true;
                    }
                    /*if(theAddress.CityID != -1)
					{
						if(whereStatement)
							query += " AND ";
						query += "CityID = " + theAddress.CityID + " ";
						whereStatement = true;
					}
					if(theAddress.CountryID != "??????????")
					{
					
					}*/
                    if (theAddress.PostalCode != "")
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "(NationalPostalCode = '" + theAddress.PostalCode + "' OR PostalCode = '" +
                                 theAddress.PostalCode + "') ";

                        whereStatement = true;
                    }
                }

                // nothing to query by ... send it all back
                if (whereStatement)
                {
                    query += where;
                }

                query += " ORDER BY NationalName ";

                var theCompanies = new ArrayList();

                try
                {
                    myOleDbConn.Open();
                    //	var myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    CompanyBLLC myCompany;

                    while (reader.Read())
                    {
                        myCompany = new CompanyBLLC();

                        if (!reader.IsDBNull(0))
                        {
                            myCompany.NationalPostNumber = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            myCompany.NationalName = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            myCompany.NationalAddress = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myCompany.NationalFaxNumber = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myCompany.NationalPhoneNumber = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            myCompany.NameNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            myCompany.NameEN = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            myCompany.CreditInfoID = reader.GetInt32(7);
                        }

                        if (!reader.IsDBNull(8) || !reader.IsDBNull(9) || !reader.IsDBNull(10) || !reader.IsDBNull(11) ||
                            !reader.IsDBNull(12) || !reader.IsDBNull(13) || !reader.IsDBNull(14) || !reader.IsDBNull(15) ||
                            !reader.IsDBNull(16))
                        {
                            var theAddress = new AddressBLLC();

                            if (!reader.IsDBNull(8))
                            {
                                theAddress.StreetNative = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                theAddress.StreetEN = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                theAddress.CityID = reader.GetInt32(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                theAddress.CityNameNative = reader.GetString(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                theAddress.CityNameEN = reader.GetString(12);
                            }
                            if (!reader.IsDBNull(13))
                            {
                                theAddress.PostalCode = reader.GetString(13);
                            }
                            if (!reader.IsDBNull(14))
                            {
                                theAddress.CountryID = reader.GetInt32(14);
                            }
                            if (!reader.IsDBNull(15))
                            {
                                theAddress.CountryNameNative = reader.GetString(15);
                            }
                            if (!reader.IsDBNull(16))
                            {
                                theAddress.CountryNameEN = reader.GetString(16);
                            }

                            myCompany.Address.Add(theAddress);
                        }

                        myCompany.IsCompany = true;

                        if (!reader.IsDBNull(17))
                        {
                            var theID = new IDNumberBLLC { Number = reader.GetString(17) };

                            if (!reader.IsDBNull(18))
                            {
                                theID.NumberTypeID = reader.GetInt32(18);
                            }

                            myCompany.IDNumbers.Add(theID);
                        }

                        theCompanies.Add(myCompany);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
                return theCompanies;
            }
        }

        public int CountCompanies(CompanyBLLC theCompany)
        {
            const string funcName = "CountCompanies(CompanyBLLC theCompany)";
            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "SELECT COUNT(*) FROM ros_CompanyView";
                string where = " WHERE ";
                if (theCompany.CreditInfoID != -1)
                {
                    where += "CreditInfoID = " + theCompany.CreditInfoID;
                    whereStatement = true;
                }
                if (theCompany.IDNumbers.Count > 0)
                {
                    var theID = new IDNumberBLLC();
                    theID = (IDNumberBLLC)theCompany.IDNumbers[0];

                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "IDNumber = '" + theID.Number + "' AND NumberTypeID = " + theID.NumberTypeID;
                    whereStatement = true;
                }
                if (theCompany.NameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "NameNative LIKE(N'" + theCompany.NameNative + "%')";
                    whereStatement = true;
                }
                if (theCompany.NameEN.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "NameEN LIKE(N'" + theCompany.NameEN + "%')";
                    whereStatement = true;
                }
                if (theCompany.Address.Count > 0)
                {
                    var theAddress = (AddressBLLC)theCompany.Address[0];

                    if (theAddress.CityID != -1)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "CityID = " + theAddress.CityID;
                        whereStatement = true;
                    }
                    /*					if(theAddress.CountryID != -1)
                                        {
                                            if(whereStatement)
                                                where += " AND ";

                                            where += "CountryID = " + theAddress.CountryID;
                                            whereStatement = true;
                                        }
                    */
                    if (theAddress.StreetNative.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "AddressNative LIKE(N'" + theAddress.StreetNative + "%')";
                        whereStatement = true;
                    }
                    if (theAddress.StreetEN.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "AddressEN LIKE(N'" + theAddress.StreetEN + "%')";
                        whereStatement = true;
                    }
                    if (theAddress.PostalCode.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "PostalCode = '" + theAddress.PostalCode + "'";
                        whereStatement = true;
                    }
                }

                // nothing to query by ... send it all back
                if (whereStatement)
                {
                    query += where;
                }

                int count = -1;

                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            count = reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return -1;
                }
                return count;
            }
        }

        public CompanyBLLC GetCompany(string creditInfoID)
        {
            const string funcName = "GetCompany(string creditInfoID)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(
                        "SELECT dbo.np_CreditInfoUser.CreditInfoID, " +
                        "dbo.np_CreditInfoUser.Type, dbo.np_CreditInfoUser.Email, dbo.np_Companys.NameNative, " +
                        "dbo.np_Companys.NameEN, dbo.np_Companys.Established, dbo.np_Companys.LastContacted, " +
                        "dbo.np_Companys.URL, dbo.ros_AdditionalCustomerInfo.Email1, dbo.ros_AdditionalCustomerInfo.Email2, " +
                        "dbo.ros_AdditionalCustomerInfo.RemarksEN, dbo.ros_AdditionalCustomerInfo.RemarksNative, dbo.np_Companys.FuncID, dbo.np_Companys.org_status_code " +
                        "FROM dbo.np_CreditInfoUser INNER JOIN dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = " +
                        "dbo.np_Companys.CreditInfoID LEFT OUTER JOIN dbo.ros_AdditionalCustomerInfo ON " +
                        "dbo.np_CreditInfoUser.CreditInfoID = dbo.ros_AdditionalCustomerInfo.CreditInfoID WHERE " +
                        "(dbo.np_CreditInfoUser.CreditInfoID = " + creditInfoID + ")",
                        myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    var myCompany = new CompanyBLLC();
                    if (reader.Read())
                    {
                        myCompany.IsCompany = true;
                        if (!reader.IsDBNull(0))
                        {
                            myCompany.CreditInfoID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            myCompany.Type = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            myCompany.EmailAddress.Add(reader.GetString(2));
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myCompany.NameNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myCompany.NameEN = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            myCompany.Established = reader.GetDateTime(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            myCompany.LastContacted = reader.GetDateTime(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            myCompany.WebPage = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            myCompany.EmailAddress.Add(reader.GetString(8));
                        }
                        if (!reader.IsDBNull(9))
                        {
                            myCompany.EmailAddress.Add(reader.GetString(9));
                        }
                        if (!reader.IsDBNull(10))
                        {
                            myCompany.RemarksEN = reader.GetString(10);
                        }
                        if (!reader.IsDBNull(11))
                        {
                            myCompany.RemarksNative = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12))
                        {
                            myCompany.NaceID = reader.GetString(12);
                        }
                        if (!reader.IsDBNull(13))
                        {
                            myCompany.OrgStatus = reader.GetInt32(13);
                        }

                        // get the collections
                        myCompany.Address = GetAddressList(myCompany.CreditInfoID);
                        myCompany.TelephoneNumbers = GetPhoneNumberList(myCompany.CreditInfoID);
                        myCompany.IDNumbers = GetIDNumberList(myCompany.CreditInfoID);
                        myCompany.ContactPersons = GetContactPersons(myCompany.CreditInfoID);
                    }
                    return myCompany;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public DataSet FindCompany(CompanyBLLC theCompany)
        {
            const string funcName = "FindCompany(CompanyBLLC theCompany)";

            string hits = CigConfig.Configure("lookupsettings.ROSMaxHits").Trim();

            var mySet = new DataSet();
            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "SELECT DISTINCT TOP " + hits +
                               " CreditInfoID, IDNumber, NumberTypeID AS IDNumberType, NameEN, NameNative, '' AS FirstNameEN, '' AS FirstNameNative, '' AS LastNameEN, '' AS LastNameNative, AddressEN, AddressNative, CityEN, CityNative, CityID, PostalCode, CountryEN, CountryNative, CountryID, 'True' AS IsCompany FROM ros_CompanyView";
                string where = " WHERE ";

                if (theCompany.IDNumbers.Count > 0)
                {
                    var theID = new IDNumberBLLC();
                    theID = (IDNumberBLLC)theCompany.IDNumbers[0];

                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "IDNumber = '" + theID.Number + "' AND NumberTypeID = " + theID.NumberTypeID;

                    whereStatement = true;
                }
                if (theCompany.NameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "NameNative LIKE(N'" + theCompany.NameNative + "%')";
                    where += " OR NameEN LIKE(N'" + theCompany.NameNative + "%')";

                    whereStatement = true;
                }

                if (whereStatement)
                {
                    query += where;
                }

                try
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
                return mySet;
            }
        }

        public ArrayList FindCompanyAsArray(CompanyBLLC theCompany)
        {
            const string funcName = "FindCompanyAsArray(CompanyBLLC theCompany)";

            string hits = CigConfig.Configure("lookupsettings.ROSMaxHits").Trim();

            var theCompanies = new ArrayList();
            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "SELECT DISTINCT TOP " + hits +
                               " CreditInfoID, IDNumber, NumberTypeID, NameEN, NameNative, AddressEN, AddressNative, CityEN, CityNative, CityID, PostalCode, CountryEN, CountryNative, CountryID FROM ros_CompanyView";
                string where = " WHERE ";
                if (theCompany.CreditInfoID != -1)
                {
                    where += "CreditInfoID = " + theCompany.CreditInfoID;
                    whereStatement = true;
                }
                if (theCompany.IDNumbers.Count > 0)
                {
                    var theID = new IDNumberBLLC();
                    theID = (IDNumberBLLC)theCompany.IDNumbers[0];

                    if (whereStatement)
                    {
                        where += " AND ";
                    }

                    where += "IDNumber = '" + theID.Number + "' AND NumberTypeID = " + theID.NumberTypeID;

                    whereStatement = true;
                }
                if (theCompany.NameNative.Length > 0)
                {
                    if (whereStatement)
                    {
                        where += " AND ";
                    }
                    where += "(NameNative LIKE(N'" + theCompany.NameNative + "%') OR NameEN LIKE(N'" + theCompany.NameEN +
                             "%'))";
                    whereStatement = true;
                }
                /*				if(theCompany.NameEN.Length > 0)
                                {
                                    if(whereStatement)
                                        where += " AND ";
                                    where += "NameEN LIKE(N'" + theCompany.NameEN + "%')";
                                    whereStatement = true;	
                                }
                */
                if (theCompany.Address.Count > 0)
                {
                    var theAddress = (AddressBLLC)theCompany.Address[0];

                    if (theAddress.CityID != -1)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "CityID = " + theAddress.CityID;
                        whereStatement = true;
                    }
                    /*					if(theAddress.CountryID != -1)
                                        {
                                            if(whereStatement)
                                                where += " AND ";

                                            where += "CountryID = " + theAddress.CountryID;
                                            whereStatement = true;
                                        }
                    */
                    if (theAddress.StreetNative.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "(AddressNative LIKE(N'" + theAddress.StreetNative + "%') OR AddressEN LIKE(N'" +
                                 theAddress.StreetEN + "%'))";
                        whereStatement = true;
                    }
                    /*					if(theAddress.StreetEN.Length > 0)
                                        {
                                            if(whereStatement)
                                                where += " AND ";

                                            where += "AddressEN LIKE(N'" + theAddress.StreetEN + "%')";
                                            whereStatement = true;
                                        }
                    */
                    if (theAddress.PostalCode.Length > 0)
                    {
                        if (whereStatement)
                        {
                            where += " AND ";
                        }

                        where += "PostalCode = '" + theAddress.PostalCode + "'";
                        whereStatement = true;
                    }
                }

                // nothing to query by ... send it all back
                if (whereStatement)
                {
                    query += where;
                }

                try
                {
                    myOleDbConn.Open();
                    //	var myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    CompanyBLLC myCompany;

                    while (reader.Read())
                    {
                        myCompany = new CompanyBLLC();

                        if (!reader.IsDBNull(0))
                        {
                            myCompany.CreditInfoID = reader.GetInt32(0);
                        }

                        if (!reader.IsDBNull(1))
                        {
                            var theID = new IDNumberBLLC { Number = reader.GetString(1) };

                            if (!reader.IsDBNull(2))
                            {
                                theID.NumberTypeID = reader.GetInt32(2);
                            }

                            myCompany.IDNumbers.Add(theID);
                        }

                        if (!reader.IsDBNull(3))
                        {
                            myCompany.NameEN = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myCompany.NameNative = reader.GetString(4);
                        }

                        if (!reader.IsDBNull(5) || !reader.IsDBNull(6) || !reader.IsDBNull(7) || !reader.IsDBNull(8) ||
                            !reader.IsDBNull(9) || !reader.IsDBNull(10) || !reader.IsDBNull(11) || !reader.IsDBNull(12) ||
                            !reader.IsDBNull(13))
                        {
                            var theAddress = new AddressBLLC();

                            if (!reader.IsDBNull(5))
                            {
                                theAddress.StreetEN = reader.GetString(5);
                            }
                            if (!reader.IsDBNull(6))
                            {
                                theAddress.StreetNative = reader.GetString(6);
                            }
                            if (!reader.IsDBNull(7))
                            {
                                theAddress.CityNameEN = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8))
                            {
                                theAddress.CityNameNative = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                theAddress.CityID = reader.GetInt32(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                theAddress.PostalCode = reader.GetString(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                theAddress.CountryNameEN = reader.GetString(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                theAddress.CountryNameNative = reader.GetString(12);
                            }
                            if (!reader.IsDBNull(13))
                            {
                                theAddress.CountryID = reader.GetInt32(13);
                            }

                            myCompany.Address.Add(theAddress);
                        }

                        myCompany.IsCompany = true;

                        theCompanies.Add(myCompany);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
                return theCompanies;
            }
        }

        #endregion

        #region Order

        public int OrderIsDelivered(OrderBLLC theOrder, string ipAddress, int registeredByID)
        {
            const string funcName = "OrderIsDelivered(OrderBLLC theOrder, string ipAddress, int registeredByID)";

            //F� nafni� � deliverySpeedID
            //			int offlineUsageID = this.GetDeliverySpeedOfflineUsageID(theOrder.DeliverySpeedID);

            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    myOleDbCommand =
                        new OleDbCommand(
                            "UPDATE ros_Order SET Delivered = ?,ActualDateDelivered = ?,Date = ?,DispatchDeadline = ?,DeliverySpeedID = ?,TypeOfOrderID = ?,CustomerReferenceNumber = ?, RemarksNative = ?, RemarksEN = ?, np_UsageTypesID = ? WHERE ID = ?",
                            myOleDbConn) { Transaction = myTrans };
                    var myParam = new OleDbParameter("Delivered", OleDbType.VarChar)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = "True"
                                  };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("ActualDateDelivered", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = DateTime.Today
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Date", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.OrderDate
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("DispatchDeadline", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.DispatchDeadline
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("DeliverySpeedID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.DeliverySpeedID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("TypeOfOrderID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.OrderTypeID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CustomerReferenceNumber", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.CustomerReferenceNumber
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert FirstName Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "RemarksNative",
                        "RemarksEN",
                        ParameterDirection.Input,
                        theOrder.RemarksNative,
                        theOrder.RemarksEN,
                        OleDbType.VarChar);

                    myParam = new OleDbParameter("np_UsageTypesID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.Product.ID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("ID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.OrderID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    //H�r �arf a� kalla � this.InsertInto_np_UsageOffline
                    InsertInto_np_UsageOffline(
                        myOleDbCommand,
                        theOrder.Customer.CreditInfoID,
                        theOrder.Product.ID,
                        "",
                        1,
                        ipAddress,
                        registeredByID,
                        theOrder.DeliverySpeedID);

                    myTrans.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    myTrans.Rollback();
                    return -1;
                }
                return 1;
            }
        }

        public ArrayList GetUndeliveredOrders()
        {
            const string funcName = "GetUndeliveredOrders()";


            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                // FIXING BackOffice - orders - undelivered OLD QUERY
                //const string query = "Select ID,Date,DispatchDeadline,CustomerReferenceNumber,CustomerCreditInfoID,SubjectCreditInfoID,CreditInfoID,Type,NameNative,NameEN,FirstNameEN,FirstNameNative,SurNameEN,SurNameNative,RemarksEN,RemarksNative,DeliverySpeedID,ProductEN,ProductNative from ros_OrderView where Delivered = 'False' AND Deleted = 'False' ORDER By ID";
                
                // FIXING BackOffice - orders - undelivered QUERY
                const string query = @"Select 
	y.ID,Date,DispatchDeadline,CustomerReferenceNumber,CustomerCreditInfoID,SubjectCreditInfoID,
	CreditInfoID,Type,NameNative,NameEN,FirstNameEN,FirstNameNative,SurNameEN,SurNameNative,
	RemarksEN,RemarksNative,DeliverySpeedID,ProductEN,ProductNative
from 
	ros_OrderView y
	 INNER JOIN (SELECT
                       ID, COUNT(*) AS CountOf
                        FROM ros_OrderView
                        GROUP BY ID
                        HAVING COUNT(*)=2
                    ) dt ON y.id=dt.ID 

where 
	y.Delivered = 'False' AND y.Deleted = 'False'
  order by y.ID";

                var theOrders = new ArrayList();

                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    var theOrder = new OrderBLLC();
                    var theCustomer = new CustomerBLLC();

                    int counter = 0;
                    while (reader.Read())
                    {
                        int customerCreditInfoID = -1;
                        int subjectCreditInfoID = -1;

                        if (counter == 2)
                        {
                            counter = 0;
                            theOrder = new OrderBLLC();
                        }

                        if (!reader.IsDBNull(0))
                        {
                            theOrder.OrderID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            theOrder.OrderDate = reader.GetDateTime(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            theOrder.DispatchDeadline = reader.GetDateTime(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            theOrder.CustomerReferenceNumber = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            customerCreditInfoID = reader.GetInt32(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            subjectCreditInfoID = reader.GetInt32(5);
                        }

                        if (!reader.IsDBNull(7))
                        {
                            string type = CigConfig.Configure("lookupsettings.individualID");
                            if (reader.GetString(7).Trim() == type)
                            {
                                theCustomer = new IndividualBLLC { Type = type };

                                if (!reader.IsDBNull(6))
                                {
                                    theCustomer.CreditInfoID = reader.GetInt32(6);
                                }

                                if (!reader.IsDBNull(10))
                                {
                                    ((IndividualBLLC)theCustomer).FirstNameEN = reader.GetString(10);
                                }
                                if (!reader.IsDBNull(11))
                                {
                                    ((IndividualBLLC)theCustomer).FirstNameNative = reader.GetString(11);
                                }
                                if (!reader.IsDBNull(12))
                                {
                                    ((IndividualBLLC)theCustomer).LastNameEN = reader.GetString(12);
                                }
                                if (!reader.IsDBNull(13))
                                {
                                    ((IndividualBLLC)theCustomer).LastNameNative = reader.GetString(13);
                                }
                            }
                            else
                            {
                                theCustomer = new CompanyBLLC();
                                type = CigConfig.Configure("lookupsettings.companyID");
                                theCustomer.Type = type;

                                if (!reader.IsDBNull(6))
                                {
                                    theCustomer.CreditInfoID = reader.GetInt32(6);
                                }

                                if (!reader.IsDBNull(8))
                                {
                                    ((CompanyBLLC)theCustomer).NameNative = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    ((CompanyBLLC)theCustomer).NameEN = reader.GetString(9);
                                }
                            }
                        }

                        if (theCustomer.CreditInfoID == customerCreditInfoID)
                        {
                            theOrder.Customer = theCustomer;
                        }
                        else
                        {
                            theOrder.Subject = theCustomer;
                        }

                        if (!reader.IsDBNull(14))
                        {
                            theOrder.RemarksEN = reader.GetString(14);
                        }
                        if (!reader.IsDBNull(15))
                        {
                            theOrder.RemarksNative = reader.GetString(15);
                        }
                        if (!reader.IsDBNull(16))
                        {
                            theOrder.DeliverySpeedID = reader.GetInt32(16);
                        }
                        if (!reader.IsDBNull(17))
                        {
                            theOrder.Product.NameEN = reader.GetString(17);
                        }
                        if (!reader.IsDBNull(18))
                        {
                            theOrder.Product.NameNative = reader.GetString(18);
                        }

                        if (counter == 1)
                        {
                                theOrders.Add(theOrder);
                        }

                        counter++;
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
                return theOrders;
            }
        }

        public ArrayList FindOrder(
            DateTime dateFrom, DateTime dateTo, CustomerBLLC myCustomer, CustomerBLLC mySubject, int productID)
        {
            const string funcName = "FindOrder(DateTime dateFrom, DateTime dateTo, int customerCreditInfoID, int subjectCreditInfoID, int productID)";



            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query =
                    "Select ID,Date,DispatchDeadline,CustomerReferenceNumber,RemarksEN,RemarksNative,Delivered,DeliverySpeedID,ProductEN,ProductNative,  CreditInfoID,Type,NameNative,NameEN,FirstNameNative,FirstNameEN,SurNameNative,SurNameEN,    SubjectCreditInfoID,SubjectType,SubjectNameNative,SubjectNameEN,SubjectFirstNameNative,SubjectFirstNameEN,SubjectSurNameNative,SubjectSurNameEN from ros_OrderSurveyView";
                string param = " where (Date BETWEEN '" + dateFrom.Year + "-" + dateFrom.Month + "-" + dateFrom.Day +
                               "' AND '" + dateTo.Year + "-" + dateTo.Month + "-" + dateTo.Day +
                               "') AND Deleted = 'False'";

                if (productID > -1)
                {
                    param += " AND ProductID = " + productID;
                }

                var theOrders = new ArrayList();

                if (myCustomer.IsCompany)
                {
                    //the customer is a company
                    if (((CompanyBLLC)myCustomer).NameNative.Length > 0 || myCustomer.IDNumbers.Count > 0)
                    {
                        param += " AND type = " + CigConfig.Configure("lookupsettings.companyID") + " ";
                    }

                    if (((CompanyBLLC)myCustomer).NameNative.Length > 0)
                    {
                        param += " AND (NameNative LIKE(N'" + ((CompanyBLLC)myCustomer).NameNative +
                                 "%') OR NameEN LIKE(N'" + ((CompanyBLLC)myCustomer).NameNative + "%'))";
                    }
                }
                else
                {
                    //the customer is an individual
                    if (((IndividualBLLC)myCustomer).FirstNameNative.Length > 0 || myCustomer.IDNumbers.Count > 0)
                    {
                        param += " AND type = " + CigConfig.Configure("lookupsettings.individualID") + " ";
                    }

                    if (((IndividualBLLC)myCustomer).FirstNameNative.Length > 0 &&
                        ((IndividualBLLC)myCustomer).LastNameNative.Length > 0)
                    {
                        param += " AND ((FirstNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative +
                                 "%') AND SurNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%'))";
                        param += " OR (FirstNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative +
                                 "%') AND SurNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%')))";
                    }
                    else if (((IndividualBLLC)myCustomer).FirstNameNative.Length > 0)
                    {
                        param += " AND (FirstNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative + "%')";
                        param += " OR (FirstNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative + "%')))";
                    }
                    else if (((IndividualBLLC)myCustomer).LastNameNative.Length > 0)
                    {
                        param += " AND (SurNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%')";
                        param += " OR (SurNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%')))";
                    }
                }

                if (myCustomer.IDNumbers.Count > 0)
                {
                    param += " AND Number = '" + ((IDNumberBLLC)myCustomer.IDNumbers[0]).Number +
                             "' AND NumberTypeID = " + ((IDNumberBLLC)myCustomer.IDNumbers[0]).NumberTypeID;
                }

                if (mySubject.IsCompany)
                {
                    //the subject is a company
                    if (((CompanyBLLC)mySubject).NameNative.Length > 0 || mySubject.IDNumbers.Count > 0)
                    {
                        param += " AND SubjectType = " + CigConfig.Configure("lookupsettings.companyID") + " ";
                    }

                    if (((CompanyBLLC)mySubject).NameNative.Length > 0)
                    {
                        param += " AND (SubjectNameNative LIKE(N'" + ((CompanyBLLC)mySubject).NameNative +
                                 "%') OR SubjectNameEN LIKE(N'" + ((CompanyBLLC)mySubject).NameNative + "%'))";
                    }
                }
                else
                {
                    //the subject is a individual
                    if (((IndividualBLLC)mySubject).FirstNameNative.Length > 0 || mySubject.IDNumbers.Count > 0)
                    {
                        param += " AND SubjectType = " + CigConfig.Configure("lookupsettings.individualID") + " ";
                    }

                    if (((IndividualBLLC)mySubject).FirstNameNative.Length > 0 &&
                        ((IndividualBLLC)mySubject).LastNameNative.Length > 0)
                    {
                        param += " AND ((SubjectFirstNameNative LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%') AND SubjectSurNameNative LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative +
                                 "%'))";
                        param += " OR (SubjectFirstNameEN LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%') AND SubjectSurNameEN LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative +
                                 "%')))";
                    }
                    else if (((IndividualBLLC)mySubject).FirstNameNative.Length > 0)
                    {
                        param += " AND (SubjectFirstNameNative LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%')";
                        param += " OR (SubjectFirstNameEN LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%')))";
                    }
                    else if (((IndividualBLLC)mySubject).LastNameNative.Length > 0)
                    {
                        param += " AND (SubjectSurNameNative LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative +
                                 "%')";
                        param += " OR (SubjectSurNameEN LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative + "%')))";
                    }
                }

                if (mySubject.IDNumbers.Count > 0)
                {
                    param += " AND Number = '" + ((IDNumberBLLC)mySubject.IDNumbers[0]).Number +
                             "' AND NumberTypeID = " + ((IDNumberBLLC)mySubject.IDNumbers[0]).NumberTypeID;
                }

                query += param;

                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        var theOrder = new OrderBLLC();

                        if (!reader.IsDBNull(0))
                        {
                            theOrder.OrderID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            theOrder.OrderDate = reader.GetDateTime(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            theOrder.DispatchDeadline = reader.GetDateTime(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            theOrder.CustomerReferenceNumber = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            theOrder.RemarksEN = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            theOrder.RemarksNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            theOrder.Delivered = bool.Parse(reader.GetString(6));
                        }
                        if (!reader.IsDBNull(7))
                        {
                            theOrder.DeliverySpeedID = reader.GetInt32(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            theOrder.Product.NameEN = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9))
                        {
                            theOrder.Product.NameNative = reader.GetString(9);
                        }

                        if (!reader.IsDBNull(11))
                        {
                            string type = CigConfig.Configure("lookupsettings.individualID");
                            CustomerBLLC theCustomer;

                            if (reader.GetString(11).Trim() == type)
                            {
                                theCustomer = new IndividualBLLC { Type = type };

                                if (!reader.IsDBNull(10))
                                {
                                    theCustomer.CreditInfoID = reader.GetInt32(10);
                                }

                                if (!reader.IsDBNull(14))
                                {
                                    ((IndividualBLLC)theCustomer).FirstNameNative = reader.GetString(14);
                                }
                                if (!reader.IsDBNull(15))
                                {
                                    ((IndividualBLLC)theCustomer).FirstNameEN = reader.GetString(15);
                                }
                                if (!reader.IsDBNull(16))
                                {
                                    ((IndividualBLLC)theCustomer).LastNameNative = reader.GetString(16);
                                }
                                if (!reader.IsDBNull(17))
                                {
                                    ((IndividualBLLC)theCustomer).LastNameEN = reader.GetString(17);
                                }
                            }
                            else
                            {
                                theCustomer = new CompanyBLLC
                                              {
                                                  Type = CigConfig.Configure("lookupsettings.companyID")
                                              };

                                if (!reader.IsDBNull(10))
                                {
                                    theCustomer.CreditInfoID = reader.GetInt32(10);
                                }

                                if (!reader.IsDBNull(12))
                                {
                                    ((CompanyBLLC)theCustomer).NameNative = reader.GetString(12);
                                }
                                if (!reader.IsDBNull(13))
                                {
                                    ((CompanyBLLC)theCustomer).NameEN = reader.GetString(13);
                                }
                            }

                            theOrder.Customer = theCustomer;
                        }

                        if (!reader.IsDBNull(19))
                        {
                            string type = CigConfig.Configure("lookupsettings.individualID");
                            CustomerBLLC theSubject;
                            if (reader.GetString(19).Trim() == type)
                            {
                                theSubject = new IndividualBLLC { Type = type };

                                if (!reader.IsDBNull(18))
                                {
                                    theSubject.CreditInfoID = reader.GetInt32(18);
                                }

                                if (!reader.IsDBNull(22))
                                {
                                    ((IndividualBLLC)theSubject).FirstNameNative = reader.GetString(22);
                                }
                                if (!reader.IsDBNull(23))
                                {
                                    ((IndividualBLLC)theSubject).FirstNameEN = reader.GetString(23);
                                }
                                if (!reader.IsDBNull(24))
                                {
                                    ((IndividualBLLC)theSubject).LastNameNative = reader.GetString(24);
                                }
                                if (!reader.IsDBNull(25))
                                {
                                    ((IndividualBLLC)theSubject).LastNameEN = reader.GetString(25);
                                }
                            }
                            else
                            {
                                theSubject = new CompanyBLLC
                                             {
                                                 Type = CigConfig.Configure("lookupsettings.companyID")
                                             };

                                if (!reader.IsDBNull(18))
                                {
                                    theSubject.CreditInfoID = reader.GetInt32(18);
                                }

                                if (!reader.IsDBNull(20))
                                {
                                    ((CompanyBLLC)theSubject).NameNative = reader.GetString(20);
                                }
                                if (!reader.IsDBNull(21))
                                {
                                    ((CompanyBLLC)theSubject).NameEN = reader.GetString(21);
                                }
                            }

                            theOrder.Subject = theSubject;
                        }

                        theOrders.Add(theOrder);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                }

                return theOrders;
            }
        }

        public ArrayList FindDeliveredOrder(
            DateTime dateFrom, DateTime dateTo, CustomerBLLC myCustomer, CustomerBLLC mySubject, int productID)
        {
            const string funcName = "FindDeliveredOrder(DateTime dateFrom, DateTime dateTo, CustomerBLLC myCustomer, CustomerBLLC mySubject, int productID)";


            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query =
                    "Select ID,Date,DispatchDeadline,CustomerReferenceNumber,RemarksEN,RemarksNative,Delivered,DeliverySpeedID,ProductEN,ProductNative,  CreditInfoID,Type,NameNative,NameEN,FirstNameNative,FirstNameEN,SurNameNative,SurNameEN,    SubjectCreditInfoID,SubjectType,SubjectNameNative,SubjectNameEN,SubjectFirstNameNative,SubjectFirstNameEN,SubjectSurNameNative,SubjectSurNameEN from ros_OrderSurveyView";
                string param = " where (Date BETWEEN '" + dateFrom.Year + "-" + dateFrom.Month + "-" + dateFrom.Day +
                               "' AND '" + dateTo.Year + "-" + dateTo.Month + "-" + dateTo.Day +
                               "') AND Delivered = 'True' AND Deleted = 'False'";

                if (productID > -1)
                {
                    param += " AND ProductID = " + productID;
                }

                var theOrders = new ArrayList();

                if (myCustomer.IsCompany)
                {
                    //the customer is a company
                    if (((CompanyBLLC)myCustomer).NameNative.Length > 0 || myCustomer.IDNumbers.Count > 0)
                    {
                        param += " AND type = " + CigConfig.Configure("lookupsettings.companyID") + " ";
                    }

                    if (((CompanyBLLC)myCustomer).NameNative.Length > 0)
                    {
                        param += " AND (NameNative LIKE(N'" + ((CompanyBLLC)myCustomer).NameNative +
                                 "%') OR NameEN LIKE(N'" + ((CompanyBLLC)myCustomer).NameNative + "%'))";
                    }
                }
                else
                {
                    //the customer is an individual
                    if (((IndividualBLLC)myCustomer).FirstNameNative.Length > 0 || myCustomer.IDNumbers.Count > 0)
                    {
                        param += " AND type = " + CigConfig.Configure("lookupsettings.individualID") + " ";
                    }

                    if (((IndividualBLLC)myCustomer).FirstNameNative.Length > 0 &&
                        ((IndividualBLLC)myCustomer).LastNameNative.Length > 0)
                    {
                        param += " AND ((FirstNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative +
                                 "%') AND SurNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%'))";
                        param += " OR (FirstNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative +
                                 "%') AND SurNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%')))";
                    }
                    else if (((IndividualBLLC)myCustomer).FirstNameNative.Length > 0)
                    {
                        param += " AND (FirstNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative + "%')";
                        param += " OR (FirstNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative + "%')))";
                    }
                    else if (((IndividualBLLC)myCustomer).LastNameNative.Length > 0)
                    {
                        param += " AND (SurNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%')";
                        param += " OR (SurNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%')))";
                    }
                }

                if (myCustomer.IDNumbers.Count > 0)
                {
                    param += " AND Number = '" + ((IDNumberBLLC)myCustomer.IDNumbers[0]).Number +
                             "' AND NumberTypeID = " + ((IDNumberBLLC)myCustomer.IDNumbers[0]).NumberTypeID;
                }

                if (mySubject.IsCompany)
                {
                    //the subject is a company
                    if (((CompanyBLLC)mySubject).NameNative.Length > 0 || mySubject.IDNumbers.Count > 0)
                    {
                        param += " AND SubjectType = " + CigConfig.Configure("lookupsettings.companyID") + " ";
                    }

                    if (((CompanyBLLC)mySubject).NameNative.Length > 0)
                    {
                        param += " AND (SubjectNameNative LIKE(N'" + ((CompanyBLLC)mySubject).NameNative +
                                 "%') OR SubjectNameEN LIKE(N'" + ((CompanyBLLC)mySubject).NameNative + "%'))";
                    }
                }
                else
                {
                    //the subject is a individual
                    if (((IndividualBLLC)mySubject).FirstNameNative.Length > 0 || mySubject.IDNumbers.Count > 0)
                    {
                        param += " AND SubjectType = " + CigConfig.Configure("lookupsettings.individualID") + " ";
                    }

                    if (((IndividualBLLC)mySubject).FirstNameNative.Length > 0 &&
                        ((IndividualBLLC)mySubject).LastNameNative.Length > 0)
                    {
                        param += " AND ((SubjectFirstNameNative LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%') AND SubjectSurNameNative LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative +
                                 "%'))";
                        param += " OR (SubjectFirstNameEN LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%') AND SubjectSurNameEN LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative +
                                 "%')))";
                    }
                    else if (((IndividualBLLC)mySubject).FirstNameNative.Length > 0)
                    {
                        param += " AND (SubjectFirstNameNative LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%')";
                        param += " OR (SubjectFirstNameEN LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%')))";
                    }
                    else if (((IndividualBLLC)mySubject).LastNameNative.Length > 0)
                    {
                        param += " AND (SubjectSurNameNative LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative +
                                 "%')";
                        param += " OR (SubjectSurNameEN LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative + "%')))";
                    }
                }

                if (mySubject.IDNumbers.Count > 0)
                {
                    param += " AND Number = '" + ((IDNumberBLLC)mySubject.IDNumbers[0]).Number +
                             "' AND NumberTypeID = " + ((IDNumberBLLC)mySubject.IDNumbers[0]).NumberTypeID;
                }

                query += param;

                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        var theOrder = new OrderBLLC();

                        if (!reader.IsDBNull(0))
                        {
                            theOrder.OrderID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            theOrder.OrderDate = reader.GetDateTime(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            theOrder.DispatchDeadline = reader.GetDateTime(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            theOrder.CustomerReferenceNumber = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            theOrder.RemarksEN = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            theOrder.RemarksNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            theOrder.Delivered = bool.Parse(reader.GetString(6));
                        }
                        if (!reader.IsDBNull(7))
                        {
                            theOrder.DeliverySpeedID = reader.GetInt32(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            theOrder.Product.NameEN = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9))
                        {
                            theOrder.Product.NameNative = reader.GetString(9);
                        }

                        if (!reader.IsDBNull(11))
                        {
                            string type = CigConfig.Configure("lookupsettings.individualID");
                            CustomerBLLC theCustomer;

                            if (reader.GetString(11).Trim() == type)
                            {
                                theCustomer = new IndividualBLLC { Type = type };

                                if (!reader.IsDBNull(10))
                                {
                                    theCustomer.CreditInfoID = reader.GetInt32(10);
                                }

                                if (!reader.IsDBNull(14))
                                {
                                    ((IndividualBLLC)theCustomer).FirstNameNative = reader.GetString(14);
                                }
                                if (!reader.IsDBNull(15))
                                {
                                    ((IndividualBLLC)theCustomer).FirstNameEN = reader.GetString(15);
                                }
                                if (!reader.IsDBNull(16))
                                {
                                    ((IndividualBLLC)theCustomer).LastNameNative = reader.GetString(16);
                                }
                                if (!reader.IsDBNull(17))
                                {
                                    ((IndividualBLLC)theCustomer).LastNameEN = reader.GetString(17);
                                }
                            }
                            else
                            {
                                theCustomer = new CompanyBLLC
                                              {
                                                  Type = CigConfig.Configure("lookupsettings.companyID")
                                              };

                                if (!reader.IsDBNull(10))
                                {
                                    theCustomer.CreditInfoID = reader.GetInt32(10);
                                }

                                if (!reader.IsDBNull(12))
                                {
                                    ((CompanyBLLC)theCustomer).NameNative = reader.GetString(12);
                                }
                                if (!reader.IsDBNull(13))
                                {
                                    ((CompanyBLLC)theCustomer).NameEN = reader.GetString(13);
                                }
                            }

                            theOrder.Customer = theCustomer;
                        }

                        if (!reader.IsDBNull(19))
                        {
                            string type = CigConfig.Configure("lookupsettings.individualID");
                            CustomerBLLC theSubject;
                            if (reader.GetString(19).Trim() == type)
                            {
                                theSubject = new IndividualBLLC { Type = type };

                                if (!reader.IsDBNull(18))
                                {
                                    theSubject.CreditInfoID = reader.GetInt32(18);
                                }

                                if (!reader.IsDBNull(22))
                                {
                                    ((IndividualBLLC)theSubject).FirstNameNative = reader.GetString(22);
                                }
                                if (!reader.IsDBNull(23))
                                {
                                    ((IndividualBLLC)theSubject).FirstNameEN = reader.GetString(23);
                                }
                                if (!reader.IsDBNull(24))
                                {
                                    ((IndividualBLLC)theSubject).LastNameNative = reader.GetString(24);
                                }
                                if (!reader.IsDBNull(25))
                                {
                                    ((IndividualBLLC)theSubject).LastNameEN = reader.GetString(25);
                                }
                            }
                            else
                            {
                                theSubject = new CompanyBLLC
                                             {
                                                 Type = CigConfig.Configure("lookupsettings.companyID")
                                             };

                                if (!reader.IsDBNull(18))
                                {
                                    theSubject.CreditInfoID = reader.GetInt32(18);
                                }

                                if (!reader.IsDBNull(20))
                                {
                                    ((CompanyBLLC)theSubject).NameNative = reader.GetString(20);
                                }
                                if (!reader.IsDBNull(21))
                                {
                                    ((CompanyBLLC)theSubject).NameEN = reader.GetString(21);
                                }
                            }

                            theOrder.Subject = theSubject;
                        }

                        theOrders.Add(theOrder);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                }

                return theOrders;
            }
        }

        public ArrayList FindUndeliveredOrder(
            DateTime dateFrom, DateTime dateTo, CustomerBLLC myCustomer, CustomerBLLC mySubject, int productID)
        {
            const string funcName = "FindUndeliveredOrder(DateTime dateFrom, DateTime dateTo, int customerCreditInfoID, int subjectCreditInfoID, int productID)";



            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query =
                    "Select ID,Date,DispatchDeadline,CustomerReferenceNumber,RemarksEN,RemarksNative,Delivered,DeliverySpeedID,ProductEN,ProductNative,  CreditInfoID,Type,NameNative,NameEN,FirstNameNative,FirstNameEN,SurNameNative,SurNameEN,    SubjectCreditInfoID,SubjectType,SubjectNameNative,SubjectNameEN,SubjectFirstNameNative,SubjectFirstNameEN,SubjectSurNameNative,SubjectSurNameEN from ros_OrderSurveyView";
                string param = " where (Date BETWEEN '" + dateFrom.Year + "-" + dateFrom.Month + "-" + dateFrom.Day +
                               "' AND '" + dateTo.Year + "-" + dateTo.Month + "-" + dateTo.Day +
                               "') AND Delivered = 'False' AND Deleted = 'False'";

                if (productID > -1)
                {
                    param += " AND ProductID = " + productID;
                }

                var theOrders = new ArrayList();

                if (myCustomer.IsCompany)
                {
                    //the customer is a company
                    if (((CompanyBLLC)myCustomer).NameNative.Length > 0 || myCustomer.IDNumbers.Count > 0)
                    {
                        param += " AND type = " + CigConfig.Configure("lookupsettings.companyID") + " ";
                    }

                    if (((CompanyBLLC)myCustomer).NameNative.Length > 0)
                    {
                        param += " AND (NameNative LIKE(N'" + ((CompanyBLLC)myCustomer).NameNative +
                                 "%') OR NameEN LIKE(N'" + ((CompanyBLLC)myCustomer).NameNative + "%'))";
                    }
                }
                else
                {
                    //the customer is an individual
                    if (((IndividualBLLC)myCustomer).FirstNameNative.Length > 0 || myCustomer.IDNumbers.Count > 0)
                    {
                        param += " AND type = " + CigConfig.Configure("lookupsettings.individualID") + " ";
                    }

                    if (((IndividualBLLC)myCustomer).FirstNameNative.Length > 0 &&
                        ((IndividualBLLC)myCustomer).LastNameNative.Length > 0)
                    {
                        param += " AND ((FirstNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative +
                                 "%') AND SurNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%'))";
                        param += " OR (FirstNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative +
                                 "%') AND SurNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%')))";
                    }
                    else if (((IndividualBLLC)myCustomer).FirstNameNative.Length > 0)
                    {
                        param += " AND (FirstNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative + "%')";
                        param += " OR (FirstNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).FirstNameNative + "%')))";
                    }
                    else if (((IndividualBLLC)myCustomer).LastNameNative.Length > 0)
                    {
                        param += " AND (SurNameNative LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%')";
                        param += " OR (SurNameEN LIKE(N'" + ((IndividualBLLC)myCustomer).LastNameNative + "%')))";
                    }
                }

                if (myCustomer.IDNumbers.Count > 0)
                {
                    param += " AND Number = '" + ((IDNumberBLLC)myCustomer.IDNumbers[0]).Number +
                             "' AND NumberTypeID = " + ((IDNumberBLLC)myCustomer.IDNumbers[0]).NumberTypeID;
                }

                if (mySubject.IsCompany)
                {
                    //the subject is a company
                    if (((CompanyBLLC)mySubject).NameNative.Length > 0 || mySubject.IDNumbers.Count > 0)
                    {
                        param += " AND SubjectType = " + CigConfig.Configure("lookupsettings.companyID") + " ";
                    }

                    if (((CompanyBLLC)mySubject).NameNative.Length > 0)
                    {
                        param += " AND (SubjectNameNative LIKE(N'" + ((CompanyBLLC)mySubject).NameNative +
                                 "%') OR SubjectNameEN LIKE(N'" + ((CompanyBLLC)mySubject).NameNative + "%'))";
                    }
                }
                else
                {
                    //the subject is a individual
                    if (((IndividualBLLC)mySubject).FirstNameNative.Length > 0 || mySubject.IDNumbers.Count > 0)
                    {
                        param += " AND SubjectType = " + CigConfig.Configure("lookupsettings.individualID") + " ";
                    }

                    if (((IndividualBLLC)mySubject).FirstNameNative.Length > 0 &&
                        ((IndividualBLLC)mySubject).LastNameNative.Length > 0)
                    {
                        param += " AND ((SubjectFirstNameNative LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%') AND SubjectSurNameNative LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative +
                                 "%'))";
                        param += " OR (SubjectFirstNameEN LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%') AND SubjectSurNameEN LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative +
                                 "%')))";
                    }
                    else if (((IndividualBLLC)mySubject).FirstNameNative.Length > 0)
                    {
                        param += " AND (SubjectFirstNameNative LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%')";
                        param += " OR (SubjectFirstNameEN LIKE(N'" + ((IndividualBLLC)mySubject).FirstNameNative +
                                 "%')))";
                    }
                    else if (((IndividualBLLC)mySubject).LastNameNative.Length > 0)
                    {
                        param += " AND (SubjectSurNameNative LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative +
                                 "%')";
                        param += " OR (SubjectSurNameEN LIKE(N'" + ((IndividualBLLC)mySubject).LastNameNative + "%')))";
                    }
                }

                if (mySubject.IDNumbers.Count > 0)
                {
                    param += " AND Number = '" + ((IDNumberBLLC)mySubject.IDNumbers[0]).Number +
                             "' AND NumberTypeID = " + ((IDNumberBLLC)mySubject.IDNumbers[0]).NumberTypeID;
                }

                query += param;

                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        var theOrder = new OrderBLLC();

                        if (!reader.IsDBNull(0))
                        {
                            theOrder.OrderID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            theOrder.OrderDate = reader.GetDateTime(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            theOrder.DispatchDeadline = reader.GetDateTime(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            theOrder.CustomerReferenceNumber = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            theOrder.RemarksEN = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            theOrder.RemarksNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            theOrder.Delivered = bool.Parse(reader.GetString(6));
                        }
                        if (!reader.IsDBNull(7))
                        {
                            theOrder.DeliverySpeedID = reader.GetInt32(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            theOrder.Product.NameEN = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9))
                        {
                            theOrder.Product.NameNative = reader.GetString(9);
                        }

                        if (!reader.IsDBNull(11))
                        {
                            string type = CigConfig.Configure("lookupsettings.individualID");
                            CustomerBLLC theCustomer;

                            if (reader.GetString(11).Trim() == type)
                            {
                                theCustomer = new IndividualBLLC { Type = type };

                                if (!reader.IsDBNull(10))
                                {
                                    theCustomer.CreditInfoID = reader.GetInt32(10);
                                }

                                if (!reader.IsDBNull(14))
                                {
                                    ((IndividualBLLC)theCustomer).FirstNameNative = reader.GetString(14);
                                }
                                if (!reader.IsDBNull(15))
                                {
                                    ((IndividualBLLC)theCustomer).FirstNameEN = reader.GetString(15);
                                }
                                if (!reader.IsDBNull(16))
                                {
                                    ((IndividualBLLC)theCustomer).LastNameNative = reader.GetString(16);
                                }
                                if (!reader.IsDBNull(17))
                                {
                                    ((IndividualBLLC)theCustomer).LastNameEN = reader.GetString(17);
                                }
                            }
                            else
                            {
                                theCustomer = new CompanyBLLC
                                              {
                                                  Type = CigConfig.Configure("lookupsettings.companyID")
                                              };

                                if (!reader.IsDBNull(10))
                                {
                                    theCustomer.CreditInfoID = reader.GetInt32(10);
                                }

                                if (!reader.IsDBNull(12))
                                {
                                    ((CompanyBLLC)theCustomer).NameNative = reader.GetString(12);
                                }
                                if (!reader.IsDBNull(13))
                                {
                                    ((CompanyBLLC)theCustomer).NameEN = reader.GetString(13);
                                }
                            }

                            theOrder.Customer = theCustomer;
                        }

                        if (!reader.IsDBNull(19))
                        {
                            string type = CigConfig.Configure("lookupsettings.individualID");
                            CustomerBLLC theSubject;
                            if (reader.GetString(19).Trim() == type)
                            {
                                theSubject = new IndividualBLLC { Type = type };

                                if (!reader.IsDBNull(18))
                                {
                                    theSubject.CreditInfoID = reader.GetInt32(18);
                                }

                                if (!reader.IsDBNull(22))
                                {
                                    ((IndividualBLLC)theSubject).FirstNameNative = reader.GetString(22);
                                }
                                if (!reader.IsDBNull(23))
                                {
                                    ((IndividualBLLC)theSubject).FirstNameEN = reader.GetString(23);
                                }
                                if (!reader.IsDBNull(24))
                                {
                                    ((IndividualBLLC)theSubject).LastNameNative = reader.GetString(24);
                                }
                                if (!reader.IsDBNull(25))
                                {
                                    ((IndividualBLLC)theSubject).LastNameEN = reader.GetString(25);
                                }
                            }
                            else
                            {
                                theSubject = new CompanyBLLC
                                             {
                                                 Type = CigConfig.Configure("lookupsettings.companyID")
                                             };

                                if (!reader.IsDBNull(18))
                                {
                                    theSubject.CreditInfoID = reader.GetInt32(18);
                                }

                                if (!reader.IsDBNull(20))
                                {
                                    ((CompanyBLLC)theSubject).NameNative = reader.GetString(20);
                                }
                                if (!reader.IsDBNull(21))
                                {
                                    ((CompanyBLLC)theSubject).NameEN = reader.GetString(21);
                                }
                            }

                            theOrder.Subject = theSubject;
                        }

                        theOrders.Add(theOrder);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                }

                return theOrders;
            }
        }

        public bool UpdateReportOrder(OrderBLLC theOrder)
        {
            const string funcName = "UpdateReportOrder(OrderBLLC theOrder)";
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    myOleDbCommand =
                        new OleDbCommand(
                            "UPDATE ros_Order SET Date = ?,DeliverySpeedID = ?, DispatchDeadline = ?, TypeOfOrderID = ?, CustomerReferenceNumber = ?, RemarksNative = ?, RemarksEN = ?, np_UsageTypesID = ?  WHERE ID = ?",
                            myOleDbConn) { Transaction = myTrans };

                    var myParam = new OleDbParameter("Date", OleDbType.Date)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = theOrder.OrderDate
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DeliverySpeedID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.DeliverySpeedID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DispatchDeadline", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.DispatchDeadline
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("TypeOfOrderID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.OrderTypeID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CustomerReferenceNumber", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.CustomerReferenceNumber
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert FirstName Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "RemarksNative",
                        "RemarksEN",
                        ParameterDirection.Input,
                        theOrder.RemarksNative,
                        theOrder.RemarksEN,
                        OleDbType.VarChar);

                    myParam = new OleDbParameter("np_UsageTypesID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.Product.ID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("ID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.OrderID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    myTrans.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    myTrans.Rollback();
                    return false;
                }
                finally
                {
                    // myOleDbCommand.Connection.Close(); // � ekki a� �urfa ef using er nota�
                }
                return true;
            }
        }

        public bool DeleteReportOrder(string orderID)
        {
            const string funcName = "DeleteReportOrder(string orderID)";
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    myOleDbCommand = new OleDbCommand("UPDATE ros_Order SET Deleted = ? WHERE ID = ?", myOleDbConn) { Transaction = myTrans };

                    var myParam = new OleDbParameter("Deleted", OleDbType.VarChar)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = "True"
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("ID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = orderID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    myTrans.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    myTrans.Rollback();
                    return false;
                }
                return true;
            }

            /*			const string funcName = "DeleteReportOrder(string orderID)";
                        OleDbCommand myOleDbCommand;
			
                        using(var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                        {
                            myOleDbConn.Open();
                            // begin transaction...
                            var myTrans = myOleDbConn.BeginTransaction();
                            try 
                            {
                                myOleDbCommand = new OleDbCommand("DELETE FROM ros_Order WHERE ID = ?" ,myOleDbConn);
                                myOleDbCommand.Transaction = myTrans;
					
                                var myParam = new OleDbParameter("ID", OleDbType.Integer);
                                myParam.Direction = ParameterDirection.Input;
                                myParam.Value = orderID;
                                myOleDbCommand.Parameters.Add(myParam);

                                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                                myTrans.Commit();
                            }
                            catch(Exception e) 
                            {
                                Logger.WriteToLog(className +" : " + funcName + e.Message,true);
                                myTrans.Rollback();
                                return false;
                            }
                            finally 
                            {
                                // myOleDbCommand.Connection.Close(); // � ekki a� �urfa ef using er nota�
                            }
                            return true;
                        }*/
        }

        public bool CreateReportOrder(OrderBLLC theOrder)
        {
            const string funcName = "CreateReportOrder(OrderBLLC theOrder)";

            bool isRegistered = IsIDRegistedIn_au_Subscribers(theOrder.Customer.CreditInfoID);

            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    myOleDbCommand =
                        new OleDbCommand(
                            "INSERT INTO ros_Order(Date,DeliverySpeedID,DispatchDeadline,TypeOfOrderID,CustomerReferenceNumber,RemarksNative,RemarksEN,CustomerCreditInfoID,SubjectCreditInfoID,RegisteredBy,np_UsageTypesID, Comment) VALUES(?,?,?,?,?,?,?,?,?,?,?, ?)",
                            myOleDbConn) { Transaction = myTrans };

                    var myParam = new OleDbParameter("Date", OleDbType.Date)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = theOrder.OrderDate
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DeliverySpeedID", OleDbType.Integer)
                              {
                                  Value = theOrder.DeliverySpeedID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DispatchDeadline", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theOrder.DispatchDeadline
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("TypeOfOrderID", OleDbType.Integer)
                              {
                                  Value = theOrder.OrderTypeID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CustomerReferenceNumber", OleDbType.VarChar, 50)
                              {
                                  Value = theOrder.CustomerReferenceNumber,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "RemarksNative",
                        "RemarksEN",
                        ParameterDirection.Input,
                        theOrder.RemarksNative,
                        theOrder.RemarksEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("CustomerCreditInfoID", OleDbType.Integer)
                              {
                                  Value = theOrder.Customer.CreditInfoID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("SubjectCreditInfoID", OleDbType.Integer)
                              {
                                  Value = theOrder.Subject.CreditInfoID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("RegisteredBy", OleDbType.Integer)
                              {
                                  Value = theOrder.RegisteredByID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("np_UsageTypesID", OleDbType.Integer)
                              {
                                  Value = theOrder.Product.ID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Comment", OleDbType.WChar)
                              {
                                  Value = theOrder.Comment,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    //IF CreditInfoID does not exist already in au_Subscribers then add it
                    if (!isRegistered)
                    {
                        InsertInto_au_Subscribers(
                            myOleDbCommand, theOrder.Customer.CreditInfoID, theOrder.RegisteredByID);
                    }

                    myTrans.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    myTrans.Rollback();
                    return false;
                }
                return true;
            }
        }

        public OrderBLLC GetOrder(string orderID)
        {
            const string funcName = "GetOrder(string orderID)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT ID, Date, DeliverySpeedID, DispatchDeadline, TypeOfOrderID, CustomerReferenceNumber, RemarksEN, RemarksNative, Delivered, CustomerCreditInfoID, SubjectCreditInfoID, LastUpdate, np_UsageTypesID, Comment FROM ros_Order WHERE ID = " +
                            orderID,
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    var theOrder = new OrderBLLC();
                    int customerCreditInfoID = -1;
                    int subjectCreditInfoID = -1;
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            theOrder.OrderID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            theOrder.OrderDate = reader.GetDateTime(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            theOrder.DeliverySpeedID = reader.GetInt32(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            theOrder.DispatchDeadline = reader.GetDateTime(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            theOrder.OrderTypeID = reader.GetInt32(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            theOrder.CustomerReferenceNumber = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            theOrder.RemarksEN = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            theOrder.RemarksNative = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            string delivered = reader.GetString(8).Trim();
                            theOrder.Delivered = delivered == "True";
                        }
                        if (!reader.IsDBNull(9))
                        {
                            customerCreditInfoID = reader.GetInt32(9);
                        }
                        if (!reader.IsDBNull(10))
                        {
                            subjectCreditInfoID = reader.GetInt32(10);
                        }
                        if (!reader.IsDBNull(11))
                        {
                            theOrder.LastUpdate = reader.GetDateTime(11);
                        }
                        if (!reader.IsDBNull(12))
                        {
                            theOrder.Product.ID = reader.GetInt32(12);
                        }
                        if (!reader.IsDBNull(13))
                        {
                            theOrder.Comment = reader.GetString(13);
                        }
                    }
                    reader.Close();

                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Type FROM np_CreditInfoUser WHERE CreditInfoID = " + customerCreditInfoID,
                            myOleDbConn);
                    reader = myOleDbCommand.ExecuteReader();
                    string type = "";
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            type = reader.GetString(0).Trim();
                        }
                    }

                    if (type == CigConfig.Configure("lookupsettings.individualID"))
                    {
                        theOrder.Customer = new IndividualBLLC();
                        theOrder.Customer = GetIndividual("" + customerCreditInfoID);
                    }
                    else
                    {
                        theOrder.Customer = new CompanyBLLC();
                        theOrder.Customer = GetCompany("" + customerCreditInfoID);
                    }
                    reader.Close();

                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Type FROM np_CreditInfoUser WHERE CreditInfoID = " + subjectCreditInfoID,
                            myOleDbConn);
                    reader = myOleDbCommand.ExecuteReader();
                    type = "";
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            type = reader.GetString(0).Trim();
                        }
                    }

                    if (type == CigConfig.Configure("lookupsettings.individualID"))
                    {
                        theOrder.Subject = new IndividualBLLC();
                        theOrder.Subject = GetIndividual("" + subjectCreditInfoID);
                    }
                    else
                    {
                        theOrder.Subject = new CompanyBLLC();
                        theOrder.Subject = GetCompany("" + subjectCreditInfoID);
                    }

                    return theOrder;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public bool IsIDRegistedIn_au_Subscribers(int creditInfoID)
        {

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                var myOleDbCommand =
                    new OleDbCommand(
                        "SELECT CreditInfoID FROM au_Subscribers WHERE CreditInfoID = " + creditInfoID, myOleDbConn);
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read())
                {
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region Common DropDownList Get

        public DataSet GetAllProducts()
        {
            const string funcName = "GetAllProducts()";
            string theConnectionString = DatabaseHelper.ConnectionString();

            const string sqlQuery = "Select id, typeNative AS 'nameNative', typeEN AS 'nameEN' from np_UsageTypes where Orderable = 'True'";

            using (var theConnection = new OleDbConnection(theConnectionString))
            {
                try
                {
                    theConnection.Open();
                    var theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    var theAdapter = new OleDbDataAdapter { SelectCommand = theDBCommand };

                    //Return the results in a DataSet
                    var theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public ArrayList GetDeliverySpeedsAsArrayList()
        {
            const string funcName = "GetDeliverySpeedsAsArrayList()";


            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                const string query = "Select ID, DeliverySpeedEN, DeliverySpeedNative from ros_DeliverySpeed";

                var theDeliverySpeeds = new ArrayList();

                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        var theSpeed = new DeliverySpeedBLLC();

                        if (!reader.IsDBNull(0))
                        {
                            theSpeed.ID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            theSpeed.DeliverySpeedEN = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            theSpeed.DeliverySpeedNative = reader.GetString(2);
                        }

                        theDeliverySpeeds.Add(theSpeed);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
                return theDeliverySpeeds;
            }
        }

        public DataSet GetDeliverySpeeds()
        {
            const string funcName = "GetDeliverySpeeds()";
            string theConnectionString = DatabaseHelper.ConnectionString();

            const string sqlQuery = "Select * from ros_DeliverySpeed WHERE VisibleInCheckBoxes = 'True'";

            using (var theConnection = new OleDbConnection(theConnectionString))
            {
                try
                {
                    theConnection.Open();
                    var theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    var theAdapter = new OleDbDataAdapter { SelectCommand = theDBCommand };

                    //Return the results in a DataSet
                    var theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public DataSet GetOrderTypes()
        {
            const string funcName = "GetOrderTypes()";
            string theConnectionString = DatabaseHelper.ConnectionString();

            const string sqlQuery = "Select * from ros_TypeOfOrder";

            using (var theConnection = new OleDbConnection(theConnectionString))
            {
                try
                {
                    theConnection.Open();
                    var theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    var theAdapter = new OleDbDataAdapter { SelectCommand = theDBCommand };

                    //Return the results in a DataSet
                    var theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        ///  This function returns a list of all the countries in the world
        /// </summary>
        public DataSet GetAllCountries()
        {
            const string funcName = "GetAllCountries()";
            string theConnectionString = DatabaseHelper.ConnectionString();

            const string sqlQuery = "Select * from cpi_countries order by NameNative";

            using (var theConnection = new OleDbConnection(theConnectionString))
            {
                try
                {
                    theConnection.Open();
                    var theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    var theAdapter = new OleDbDataAdapter { SelectCommand = theDBCommand };

                    //Return the results in a DataSet
                    var theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        ///  This function returns a list of all the educations
        /// </summary>
        public DataSet GetAllEducations()
        {
            const string funcName = "GetAllEducations()";
            string theConnectionString = DatabaseHelper.ConnectionString();

            const string sqlQuery = "Select * from np_Education order by NameNative";

            using (var theConnection = new OleDbConnection(theConnectionString))
            {
                try
                {
                    theConnection.Open();
                    var theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    var theAdapter = new OleDbDataAdapter { SelectCommand = theDBCommand };

                    //Return the results in a DataSet
                    var theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        ///  This function returns a list of all the professions
        /// </summary>
        public DataSet GetAllProfessions()
        {
            const string funcName = "GetAllProfessions()";
            string theConnectionString = DatabaseHelper.ConnectionString();

            const string sqlQuery = "Select * from np_Profession order by NameNative";

            using (var theConnection = new OleDbConnection(theConnectionString))
            {
                try
                {
                    theConnection.Open();
                    var theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    var theAdapter = new OleDbDataAdapter { SelectCommand = theDBCommand };

                    //Return the results in a DataSet
                    var theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        ///  This function returns a list of all the cities
        /// </summary>
        public DataSet GetAllCities()
        {
            const string funcName = "GetAllCities()";
            string theConnectionString = DatabaseHelper.ConnectionString();

            const string sqlQuery = "Select * from np_City order by NameNative";

            using (var theConnection = new OleDbConnection(theConnectionString))
            {
                try
                {
                    theConnection.Open();
                    var theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    var theAdapter = new OleDbDataAdapter { SelectCommand = theDBCommand };

                    //Return the results in a DataSet
                    var theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets list of cities that starts with given cityName
        /// </summary>
        /// <param name="cityName">Name that the city shoult stats with</param>
        /// <param name="en">Wether the list should be ordered by en or native column</param>
        /// <returns>List of available cities</returns>
        public DataSet GetCityListAsDataSet(string cityName, bool en)
        {
            const string funcName = "GetCityListAsDataSet(bool en)";
            var mySet = new DataSet();

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = en ? new OleDbCommand(
                                                       "SELECT TOP 200 * FROM np_City WHERE NameEN LIKE '" + cityName + "%' ORDER BY NameEN",
                                                       myOleDbConn) : new OleDbCommand(
                                                                          "SELECT TOP 200 * FROM np_City WHERE NameNative LIKE '" + cityName +
                                                                          "%' ORDER BY NameNative",
                                                                          myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return mySet;
        }

        /// <summary>
        /// Finds name of the city with given cityID
        /// </summary>
        /// <param name="cityID">THei d to find city name for</param>
        /// <param name="en">If to find the english name or native</param>
        /// <returns>The name of the city if found, null otherwise</returns>
        public string GetCityName(int cityID, bool en)
        {
            const string funcName = "GetCityName(int cityID, bool en)";

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    myOleDbConn.Open();
                    string query = "select NameNative, NameEN from np_CITY WHERE CityID = " + cityID;
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        string nameNative = "";
                        string nameEN = "";
                        if (!reader.IsDBNull(0))
                        {
                            nameNative = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            nameEN = reader.GetString(1);
                        }
                        if (en)
                        {
                            if (nameEN == null || nameEN.Trim() == "")
                            {
                                return nameNative;
                            }
                            return nameEN;
                        }
                        return nameNative == null || nameNative.Trim() == "" ? nameEN : nameNative;
                    }
                    return null;
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
        }

        /// <summary>
        ///  This function returns a list of all the ID number types
        /// </summary>
        public DataSet GetIDNumberTypes()
        {
            const string funcName = "GetIDNumberTypes()";
            string theConnectionString = DatabaseHelper.ConnectionString();

            const string sqlQuery = "Select * from np_NumberTypes order by TypeNative";

            using (var theConnection = new OleDbConnection(theConnectionString))
            {
                try
                {
                    theConnection.Open();
                    var theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    var theAdapter = new OleDbDataAdapter { SelectCommand = theDBCommand };

                    //Return the results in a DataSet
                    var theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        ///  This function returns a list of all the phone number types
        /// </summary>
        public DataSet GetPhoneNumberTypes()
        {
            const string funcName = "GetPhoneNumberTypes()";
            string theConnectionString = DatabaseHelper.ConnectionString();

            const string sqlQuery = "Select * from np_PNumberTypes order by TypeNative";

            using (var theConnection = new OleDbConnection(theConnectionString))
            {
                try
                {
                    theConnection.Open();
                    var theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    var theAdapter = new OleDbDataAdapter { SelectCommand = theDBCommand };

                    //Return the results in a DataSet
                    var theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public DataSet GetCompanyNaceCodes()
        {
            const string funcName = "GetCompanyNaceCodes()";
            var theSet = new DataSet();

            using (var theOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    var theAdapter = new OleDbDataAdapter();
                    theOleDbConn.Open();
                    theAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_CompanyFunction", theOleDbConn);
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public DataSet GetOrgStatus()
        {
            const string funcName = "GetOrgStatus()";
            var theSet = new DataSet();

            using (var theOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    var theAdapter = new OleDbDataAdapter();
                    theOleDbConn.Open();
                    theAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_org_status", theOleDbConn);
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        #endregion

        #region Common Get

        /*		private int GetDeliverySpeedOfflineUsageID(int deliverySpeedID) 
		{
			const string funcName = "GetDeliverySpeedOfflineUsageID(int deliverySpeedID)";
			
			using(var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
			{
				try
				{
					myOleDbConn.Open();
					var myOleDbCommand = new OleDbCommand("SELECT UsageOfflineID FROM ros_DeliverySpeed WHERE ID = " + deliverySpeedID,myOleDbConn);
					var reader = myOleDbCommand.ExecuteReader();

					if(reader.Read()) 
					{
						if(!reader.IsDBNull(0))
							return reader.GetInt32(0);
					}
					return -1;
				}
				catch(Exception e)
				{
					Logger.WriteToLog(className +" : " + funcName + e.Message,true);
					return -1;
				}
				finally
				{
					// Ef �a� �arf a� ganga fr� einhverju �� � a� gera �a� h�r. � ekki a� �urfa me� "using" statements.
				}
			}
		}
*/

        public ArrayList GetAddressList(int CreditInfoID)
        {
            const string funcName = "GetAddressList(int CreditInfoID)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT addr.ID,addr.StreetNative,addr.StreetEN, addr.StreetNumber,addr.CityID, city.NameNative, city.NameEN,city.LocationNative,city.LocationEN, addr.PostalCode, addr.PostBox,addr.OtherInfoNative,addr.OtherInfoEN, addr.CountryID FROM np_Address addr, np_City city WHERE CreditInfoID = " +
                            CreditInfoID + " AND addr.CityID = city.CityID",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    var myArr = new ArrayList();
                    while (reader.Read())
                    {
                        var myAddress = new AddressBLLC { AddressID = reader.GetInt32(0) };
                        if (!reader.IsDBNull(1))
                        {
                            myAddress.StreetNative = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            myAddress.StreetEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myAddress.StreetNumber = reader.GetInt32(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myAddress.CityID = reader.GetInt32(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            myAddress.CityNameNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            myAddress.CityNameEN = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            myAddress.CityLocationNative = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            myAddress.CityLocationEN = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9))
                        {
                            myAddress.PostalCode = reader.GetString(9);
                        }
                        if (!reader.IsDBNull(10))
                        {
                            myAddress.PostBox = reader.GetString(10);
                        }
                        if (!reader.IsDBNull(11))
                        {
                            myAddress.OtherInfoNative = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12))
                        {
                            myAddress.OtherInfoEN = reader.GetString(12);
                        }
                        if (!reader.IsDBNull(13))
                        {
                            myAddress.CountryID = reader.GetInt32(13);
                        }

                        myArr.Add(myAddress);
                    }
                    return myArr;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public ArrayList GetPhoneNumberList(int CreditInfoID)
        {
            const string funcName = "GetPhoneNumberList(int CreditInfoID)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT pnum.ID, pnum.Number, ptype.TypeEN, ptype.TypeNative,pnum.NumberTypeID FROM np_PNumbers pnum, np_PNumberTypes ptype WHERE CreditInfoID = " +
                            CreditInfoID + " AND pnum.NumberTypeID = ptype.NumberTypeID",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    var myArr = new ArrayList();
                    while (reader.Read())
                    {
                        var myNumber = new PhoneNumberBLLC
                                       {
                                           NumberID = reader.GetInt32(0),
                                           Number = reader.GetString(1)
                                       };
                        if (!reader.IsDBNull(2))
                        {
                            myNumber.NumberTypeEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myNumber.NumberTypeNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myNumber.NumberTypeID = reader.GetInt32(4);
                        }

                        myArr.Add(myNumber);
                    }
                    return myArr;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public ArrayList GetContactPersons(int CreditInfoID)
        {
            const string funcName = "GetContactPersons(int CreditInfoID)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT FirstNameEN, SurNameEN, FirstNameNative, SurNameNative, Email FROM ros_ContactPerson WHERE CreditInfoID = " +
                            CreditInfoID,
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    var theContacts = new ArrayList();
                    while (reader.Read())
                    {
                        var theContact = new ContactPersonBLLC();

                        if (!reader.IsDBNull(0))
                        {
                            theContact.FirstNameEN = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            theContact.LastNameEN = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            theContact.FirstNameNative = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            theContact.LastNameNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            theContact.Email = reader.GetString(4);
                        }

                        theContacts.Add(theContact);
                    }
                    return theContacts;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>GetIDNumberList gets a IDNumber list belongin to customer. </summary>
        /// <returns>Returns ArrayList of numbers.</returns>
        public ArrayList GetIDNumberList(int CreditInfoID)
        {
            const string funcName = "GetIDNumberList(int CreditInfoID)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT idnum.ID ,idnum.Number, idtypes.TypeEN, idtypes.TypeNative,idnum.NumberTypeID FROM np_IDNumbers idnum, np_NumberTypes idtypes WHERE CreditInfoID = " +
                            CreditInfoID + " AND idnum.NumberTypeID = idtypes.NumberTypeID",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    var myArr = new ArrayList();
                    while (reader.Read())
                    {
                        var myNumber = new IDNumberBLLC
                                       {
                                           IDNumberID = reader.GetInt32(0),
                                           Number = reader.GetString(1)
                                       };
                        if (!reader.IsDBNull(2))
                        {
                            myNumber.NumberTypeEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myNumber.NumberTypeNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myNumber.NumberTypeID = reader.GetInt32(4);
                        }

                        myArr.Add(myNumber);
                    }
                    return myArr;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public int GetLatestCreditInfoID()
        {
            const string funcName = "GetLatestCreditInfoID()";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    var myOleDbCommand = new OleDbCommand(
                        "SELECT MAX(CreditInfoID) FROM np_CreditInfoUser", myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader.IsDBNull(0))
                        {
                            return 0;
                        }
                        return reader.GetInt32(0);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                }
                return 0;
            }
        }

        #endregion

        #region Common Inserts into tables

        /// <summary>AddNewCustomer adds a new istance of individual to the database. </summary>
        /// <returns>Returns the idnumber if ok otherwise -1.</returns>
        /// 
        public void InsertInto_np_UsageOffline(
            OleDbCommand myOleDbCommand,
            int creditInfoID,
            int queryType,
            string query,
            int resultCount,
            string ip,
            int userID,
            int deliverySpeedID)
        {
            myOleDbCommand.CommandText =
                "INSERT INTO np_UsageOffline(CreditInfoID,QueryType,Query,ResultCount,IP,UserID,DeliverySpeedID) VALUES(?,?,?,?,?,?,?)";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("QueryType", OleDbType.Integer)
                      {
                          Direction = ParameterDirection.Input,
                          Value = queryType
                      };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("Query", OleDbType.VarChar)
                      {
                          Direction = ParameterDirection.Input,
                          Value = query
                      };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("ResultCount", OleDbType.Integer)
                      {
                          Direction = ParameterDirection.Input,
                          Value = resultCount
                      };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("IP", OleDbType.VarChar) { Direction = ParameterDirection.Input, Value = ip };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("UserID", OleDbType.Integer)
                      {
                          Direction = ParameterDirection.Input,
                          Value = userID
                      };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("DeliverySpeedID", OleDbType.Integer)
                      {
                          Direction = ParameterDirection.Input,
                          Value = deliverySpeedID
                      };
            myOleDbCommand.Parameters.Add(myParam);

            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        public void InsertInto_au_Subscribers(OleDbCommand myOleDbCommand, int creditInfoID, int registeredBy)
        {
            myOleDbCommand.CommandText =
                "INSERT INTO au_Subscribers(CreditInfoID,RegisteredBy,SubscriberNickName,NationalID,MaxUsers,IsOpen,Updated) VALUES(?,?,?,?,?,?,?)";

            myOleDbCommand.Parameters.Clear();

            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("RegisteredBy", OleDbType.Integer)
                      {
                          Direction = ParameterDirection.Input,
                          Value = registeredBy
                      };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("SubscriberNickName", OleDbType.VarChar)
                      {
                          Direction = ParameterDirection.Input,
                          Value = ""
                      };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("NationalID", OleDbType.Integer)
                      {
                          Direction = ParameterDirection.Input,
                          Value = 0
                      };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("MaxUsers", OleDbType.Integer)
                      {
                          Direction = ParameterDirection.Input,
                          Value = 0
                      };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("IsOpen", OleDbType.Char)
                      {
                          Direction = ParameterDirection.Input,
                          Value = "False"
                      };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("Updated", OleDbType.Date)
                      {
                          Direction = ParameterDirection.Input,
                          Value = DateTime.Now
                      };
            myOleDbCommand.Parameters.Add(myParam);

            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        public void InsertPhoneNumbers(ArrayList theNumber, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "INSERT INTO np_PNumbers(NumberTypeID,Number,CreditInfoID) VALUES(?,?,?)";

            for (int i = 0; i < theNumber.Count; i++)
            {
                myOleDbCommand.Parameters.Clear();
                var myNum = (PhoneNumberBLLC)theNumber[i];
                var myParam = new OleDbParameter("PNumberTypeID", OleDbType.Integer)
                                         {
                                             Direction = ParameterDirection.Input,
                                             Value = myNum.NumberTypeID
                                         };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Number", OleDbType.WChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myNum.Number
                          };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                          {
                              Direction = ParameterDirection.Input,
                              Value = creditInfoID
                          };
                myOleDbCommand.Parameters.Add(myParam);
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            }
        }

        public void InsertAddress(ArrayList theAddress, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText =
                "INSERT INTO np_Address(StreetNative,StreetEN,CityID,PostalCode,PostBox,OtherInfoNative,OtherInfoEN,CreditInfoID,CountryID) VALUES(?,?,?,?,?,?,?,?,?)";

            for (int i = 0; i < theAddress.Count; i++)
            {
                myOleDbCommand.Parameters.Clear();
                var myAddr = (AddressBLLC)theAddress[i];

                // Insert Native and EN strings
                CreateTransliteratedParameterFromString(
                    myOleDbCommand,
                    "StreetNative",
                    "StreetEN",
                    ParameterDirection.Input,
                    myAddr.StreetNative,
                    myAddr.StreetEN,
                    OleDbType.WChar);

                var myParam = new OleDbParameter("CityID", OleDbType.Integer)
                                         {
                                             Direction = ParameterDirection.Input,
                                             Value = myAddr.CityID
                                         };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("PostalCode", OleDbType.VarChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myAddr.PostalCode
                          };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("PostBox", OleDbType.VarChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myAddr.PostBox
                          };
                myOleDbCommand.Parameters.Add(myParam);

                // Insert Native and EN strings
                CreateParameterFromString(
                    myOleDbCommand,
                    "OtherInfoNative",
                    "OtherInfoEN",
                    ParameterDirection.Input,
                    myAddr.InfoNative,
                    myAddr.InfoEN,
                    OleDbType.WChar);

                myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                          {
                              Direction = ParameterDirection.Input,
                              Value = creditInfoID
                          };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("CountryID", OleDbType.Integer)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myAddr.CountryID
                          };
                myOleDbCommand.Parameters.Add(myParam);

                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            }
        }

        public void InsertIDNumbers(ArrayList theIDNumbers, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "INSERT INTO np_IDNumbers(NumberTypeID,Number,CreditInfoID) VALUES(?,?,?)";
            for (int i = 0; i < theIDNumbers.Count; i++)
            {
                myOleDbCommand.Parameters.Clear();
                var myNum = (IDNumberBLLC)theIDNumbers[i];
                var myParam = new OleDbParameter("PNumberTypeID", OleDbType.Integer)
                                         {
                                             Direction = ParameterDirection.Input,
                                             Value = myNum.NumberTypeID
                                         };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Number", OleDbType.WChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myNum.Number
                          };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                          {
                              Direction = ParameterDirection.Input,
                              Value = creditInfoID
                          };
                myOleDbCommand.Parameters.Add(myParam);
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            }
        }

        public void InsertContactPersons(ArrayList theContacts, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText =
                "INSERT INTO ros_ContactPerson(CreditInfoID,FirstNameNative,FirstNameEN,SurNameNative,SurNameEN,Email) VALUES(?,?,?,?,?,?)";
            for (int i = 0; i < theContacts.Count; i++)
            {
                myOleDbCommand.Parameters.Clear();
                var myContact = (ContactPersonBLLC)theContacts[i];

                var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                         {
                                             Direction = ParameterDirection.Input,
                                             Value = creditInfoID
                                         };
                myOleDbCommand.Parameters.Add(myParam);

                // Insert FirstName Native and EN strings
                CreateTransliteratedParameterFromString(
                    myOleDbCommand,
                    "FirstNameNative",
                    "FirstNameEN",
                    ParameterDirection.Input,
                    myContact.FirstNameNative,
                    myContact.FirstNameEN,
                    OleDbType.VarChar);

                // Insert FirstName Native and EN strings
                CreateTransliteratedParameterFromString(
                    myOleDbCommand,
                    "SurNameNative",
                    "SurNameEN",
                    ParameterDirection.Input,
                    myContact.LastNameNative,
                    myContact.LastNameEN,
                    OleDbType.VarChar);

                myParam = new OleDbParameter("Email", OleDbType.VarChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myContact.Email
                          };
                myOleDbCommand.Parameters.Add(myParam);

                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            }
        }

        public void InsertAdditionalCustomerInfo(
            CustomerBLLC theCustomer, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText =
                "INSERT INTO ros_AdditionalCustomerInfo(CreditInfoID,Email1,Email2,RemarksNative,RemarksEN) VALUES(?,?,?,?,?)";

            myOleDbCommand.Parameters.Clear();

            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);

            string email = "";
            if (theCustomer.EmailAddress.Count > 1)
            {
                email = (string)theCustomer.EmailAddress[1];
            }
            myParam = new OleDbParameter("Email1", OleDbType.VarChar)
                      {
                          Direction = ParameterDirection.Input,
                          Value = email
                      };
            myOleDbCommand.Parameters.Add(myParam);

            email = "";
            if (theCustomer.EmailAddress.Count > 2)
            {
                email = (string)theCustomer.EmailAddress[2];
            }
            myParam = new OleDbParameter("Email2", OleDbType.VarChar)
                      {
                          Direction = ParameterDirection.Input,
                          Value = email
                      };
            myOleDbCommand.Parameters.Add(myParam);

            // Insert Native and EN strings
            CreateParameterFromString(
                myOleDbCommand,
                "RemarksNative",
                "RemarksEN",
                ParameterDirection.Input,
                theCustomer.RemarksNative,
                theCustomer.RemarksEN,
                OleDbType.VarChar);

            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        #endregion

        public void UpdatePhoneNumbers(ArrayList theNumbers, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            DeletePhoneNumbers(myOleDbCommand, creditInfoID);
            if (theNumbers.Count > 0)
            {
                InsertPhoneNumbers(theNumbers, myOleDbCommand, creditInfoID);
            }
        }

        public void DeletePhoneNumbers(OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "DELETE FROM np_PNumbers WHERE CreditInfoID = ?";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        public void DeleteAddress(OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "DELETE FROM np_Address WHERE CreditInfoID = ?";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        public void UpdateAddress(ArrayList theAddress, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            DeleteAddress(myOleDbCommand, creditInfoID);
            if (theAddress.Count > 0)
            {
                InsertAddress(theAddress, myOleDbCommand, creditInfoID);
            }
        }

        public void UpdateIDNumbers(ArrayList theIDNumbers, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            DeleteIDNumbers(myOleDbCommand, creditInfoID);
            if (theIDNumbers.Count > 0)
            {
                InsertIDNumbers(theIDNumbers, myOleDbCommand, creditInfoID);
            }
        }

        public void DeleteIDNumbers(OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "DELETE FROM np_IDNumbers WHERE CreditInfoID = ?";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        private void UpdateContactPersons(ArrayList theContactPersons, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            DeleteContactPersons(myOleDbCommand, creditInfoID);
            if (theContactPersons.Count > 0)
            {
                InsertContactPersons(theContactPersons, myOleDbCommand, creditInfoID);
            }
        }

        public void DeleteContactPersons(OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "DELETE FROM ros_ContactPerson WHERE CreditInfoID = ?";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        private void UpdateAdditionalCustomerInfo(
            CustomerBLLC theCustomer, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            DeleteAdditionalCustomerInfo(myOleDbCommand, creditInfoID);
            InsertAdditionalCustomerInfo(theCustomer, myOleDbCommand, creditInfoID);
        }

        private static void DeleteAdditionalCustomerInfo(IDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "DELETE FROM ros_AdditionalCustomerInfo WHERE CreditInfoID = ?";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
           // new AuditFactory(myOleDbCommand).PerformAuditProcess(); --- to check this
            myOleDbCommand.ExecuteNonQuery();
        }
    } // END CLASS DEFINITION ReportOrderingSystemDALC
}