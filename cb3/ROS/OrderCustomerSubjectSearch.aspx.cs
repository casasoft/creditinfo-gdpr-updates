using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using ROS.BLL;
using ROS.Localization;

using Cig.Framework.Base.Configuration;


namespace ROS {
    /// <summary>
    /// Summary description for OrderClientSubjectSearch.
    /// </summary>
    public class OrderClientSubjectSearch : Page {
        public static CultureInfo ci;
        private const string className = "OrderClientSubjectSearch";
//		private OrderBLLC _theOrder;
        // Localization
        public static ResourceManager rm;
        protected Button btnCreateNewCustomer;
        protected Button btnCreateNewSubject;
        protected Button btnCustomerClear;
        protected Button btnFindCustomer;
        protected Button btnFindSubject;
        protected Button btnNext;
        protected Button btnSubjectClear;
        protected DropDownList ddlistCustomerCity;
        protected DropDownList ddlstCustomerCountry;
        protected DropDownList ddlstCustomerIDNumberType;
        protected DropDownList ddlstSubjectCity;
        protected DropDownList ddlstSubjectCountry;
        protected DropDownList ddlstSubjectIDNumberType;
        protected DataGrid dgCustomer;
        protected DataGrid dgSubject;
        protected HtmlGenericControl divCustomers;
        protected HtmlGenericControl divSubjects;
        private bool EN;
        protected Image imgCustomerCompany;
        protected Image imgCustomerIndividual;
        protected Image imgSubjectCompany;
        protected Image imgSubjectIndividual;
        protected Label lblCustomerAddressNative;
        protected Label lblCustomerCIID;
        protected Label lblCustomerCity;
        protected Label lblCustomerCountry;
        protected Label lblCustomerDataGrid;
        protected Label lblCustomerErrorMessage;
        protected Label lblCustomerFirstNameNative;
        protected Label lblCustomerIDNumber;
        protected Label lblCustomerIDNumberType;
        protected Label lblCustomerInformation;
        protected Label lblCustomerLastNameNative;
        protected Label lblCustomerPostalCode;
        protected Label lblSelectIcon;
        protected Label lblSelectIcon2;
        protected Label lblSubjectAddressNative;
        protected Label lblSubjectCIID;
        protected Label lblSubjectCity;
        protected Label lblSubjectCityEN;
        protected Label lblSubjectCountry;
        protected Label lblSubjectDataGrid;
        protected Label lblSubjectErrorMessage;
        protected Label lblSubjectFirstNameNative;
        protected Label lblSubjectIDNumberType;
        protected Label lblSubjectInformation;
        protected Label lblSubjectLastNameNative;
        protected Label lblSubjectPostalCode;
        protected Label lblSubjectRegistrationNumber;
        protected HtmlTable outerGridTableCustomer;
        protected HtmlTable outerGridTableSubject;
        protected HtmlInputHidden PageX;
        protected HtmlInputHidden PageY;
        protected RadioButton rbtnCustomerCompany;
        protected RadioButton rbtnCustomerIndividual;
        protected RadioButton rbtnSubjectCompany;
        protected RadioButton rbtnSubjectIndividual;
        protected TextBox txtCustomerAddressNative;
        protected TextBox txtCustomerCIID;
        protected TextBox txtCustomerFirstNameNative;
        protected TextBox txtCustomerIDNumber;
        protected TextBox txtCustomerLastNameNative;
        protected TextBox txtCustomerPostalCode;
        protected TextBox txtSubjectAddressNative;
        protected TextBox txtSubjectCIID;
        protected TextBox txtSubjectFirstName;
        protected TextBox txtSubjectFirstNameNative;
        protected TextBox txtSubjectIDNumber;
        protected TextBox txtSubjectLastName;
        protected TextBox txtSubjectLastNameNative;
        protected TextBox txtSubjectPostalCode;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = CigConfig.Configure("lookupsettings.ROSPageIDOrderCustomerSubjectSearch");

            string funcName = "Page_Load(object sender, System.EventArgs e)";
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            // check the current culture
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");

            if (!IsPostBack) {
                //Hide customer city
                lblCustomerCity.Visible = false;
                ddlistCustomerCity.Visible = false;

                //Hide customer country
                lblCustomerCountry.Visible = false;
                ddlstCustomerCountry.Visible = false;

                //Hide subject city
                lblSubjectCity.Visible = false;
                ddlstSubjectCity.Visible = false;

                //Hide subject country
                lblSubjectCountry.Visible = false;
                ddlstSubjectCountry.Visible = false;

                var theOrder = new OrderBLLC();

                if (Session["RosOrder"] != null) {
                    theOrder = (OrderBLLC) Session["RosOrder"];
                } else {
                    Session["RosOrder"] = theOrder;
                }

                InitDDBoxes();
                InitPage();
                LocalizeText();
                SetGridHeaders();
                Session["LastPage"] = "OrderCustomerSubjectSearch";
                Session["State"] = null;

                try {
                    string defaultCountry = CigConfig.Configure("lookupsettings.ROSDefaultCountry").Trim();
                    ddlstSubjectCountry.SelectedValue = defaultCountry;
                    ddlstCustomerCountry.SelectedValue = defaultCountry;
                } catch (Exception ex) {
                    Logger.WriteToLog(
                        className + " : " + funcName + "Error getting ROSDefaultCountry from web.config: " + ex.Message,
                        true);
                }
                try {
                    string defaultCity = CigConfig.Configure("lookupsettings.ROSDefaultCity").Trim();
                    ddlistCustomerCity.SelectedValue = defaultCity;
                    ddlstSubjectCity.SelectedValue = defaultCity;
                } catch (Exception ex) {
                    Logger.WriteToLog(
                        className + " : " + funcName + "Error getting ROSDefaultCity from web.config: " + ex.Message,
                        true);
                }

                string nationalID = "";

                try {
                    nationalID = CigConfig.Configure("lookupsettings.ROSNationalID").Trim();
                } catch (Exception ex) {
                    Logger.WriteToLog(
                        className + " : " + funcName + "Error getting ROSNationalID from web.config: " + ex.Message,
                        true);
                }
/*
				int theNationalID = -1;
				try
				{
					theNationalID = int.Parse(nationalID);
				}
				catch(Exception ex)
				{
					Logger.WriteToLog(className +" : " + funcName + "Error parsing ROSNationalID: " + ex.Message,true);
				}

				if(theNationalID > 0)
				{
					this.lblCustomerCity.Visible = false;
					this.ddlistCustomerCity.Visible = false;
					this.lblSubjectCity.Visible = false;
					this.ddlstSubjectCity.Visible = false;
				}
*/

                InsertIntoCustomerInfo(theOrder.Customer);
                InsertIntoSubjectInfo(theOrder.Subject);

                ShowNextButton();

                outerGridTableCustomer.Visible = false;
                outerGridTableSubject.Visible = false;
            }
        }

        private void InitPage() {
            //Customer part initialization
            rbtnCustomerCompany.Checked = true;
            lblCustomerErrorMessage.Visible = false;
            outerGridTableCustomer.Visible = false;
            CheckCustomerRadioButtons();

            //Subject part initialization
            rbtnSubjectCompany.Checked = true;
            lblSubjectErrorMessage.Visible = false;
            outerGridTableSubject.Visible = false;
            CheckSubjectRadioButtons();

            btnNext.Visible = false;
            //this.btnNextDiv.Visible = false;
        }

        private void InitDDBoxes() {
            var theFactory = new FactoryBLLC();
            BindCountries(theFactory);
            //	this.BindCities(theFactory);
            BindIDNumberTypes(theFactory);
        }

        private void btnNext_Click(object sender, EventArgs e) { Server.Transfer("OrderForm.aspx"); }

        private void ShowNextButton() {
            //If both customer and subject have been selected then show the next button
            bool customerSelected = false;
            bool subjectSelected = false;

            if (Session["CustomerSelected"] != null) {
                customerSelected = (bool) Session["CustomerSelected"];
            }
            if (Session["SubjectSelected"] != null) {
                subjectSelected = (bool) Session["SubjectSelected"];
            }

            if (customerSelected && subjectSelected) {
                //this.btnNextDiv.Visible = true;
                btnNext.Visible = true;
            }
        }

        private void BindIDNumberTypes(FactoryBLLC theFactory) {
            var allIDTypes = theFactory.GetIDNumberTypes();
            ddlstCustomerIDNumberType.DataSource = allIDTypes;

            ddlstCustomerIDNumberType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlstCustomerIDNumberType.DataValueField = "NumberTypeID";
            ddlstCustomerIDNumberType.DataBind();

            ddlstSubjectIDNumberType.DataSource = allIDTypes;

            ddlstSubjectIDNumberType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlstSubjectIDNumberType.DataValueField = "NumberTypeID";
            ddlstSubjectIDNumberType.DataBind();
        }

        private void BindCountries(FactoryBLLC theFactory) {
            //Here we need to get all the countries from the database
            DataSet allCountries = theFactory.GetAllCountries();
            ddlstCustomerCountry.DataSource = allCountries;

            ddlstCustomerCountry.DataTextField = EN ? "NameEN" : "NameNative";

            ddlstCustomerCountry.DataValueField = "CountryID";
            ddlstCustomerCountry.DataBind();

            ddlstSubjectCountry.DataSource = allCountries;

            ddlstSubjectCountry.DataTextField = EN ? "NameEN" : "NameNative";

            ddlstSubjectCountry.DataValueField = "CountryID";
            ddlstSubjectCountry.DataBind();
        }

        private void LocalizeText() {
            //	Customer Info
            //	lblCustomerType.Text = rm.GetString("txtCustomerType",ci);
            lblCustomerInformation.Text = rm.GetString("txtCustomerInformation", ci);
            lblCustomerIDNumber.Text = rm.GetString("txtIDNumber", ci);
            lblCustomerIDNumberType.Text = rm.GetString("txtType", ci);
            lblCustomerCIID.Text = rm.GetString("txtCIID", ci);
            //		lblCustomerNameNative.Text = rm.GetString("txtNameNative",ci);
            //		lblCustomerNameEN.Text = rm.GetString("txtNameEN",ci);
            //		lblCustomerFirstNameNative.Text = rm.GetString("txtFirstName",ci);
            //		lblCustomerFirstNameEN.Text = rm.GetString("txtFirstNameEN",ci);
            lblCustomerLastNameNative.Text = rm.GetString("txtLastNameNative", ci);
            //		lblCustomerLastNameEN.Text = rm.GetString("txtLastNameEN",ci);
            lblCustomerCity.Text = rm.GetString("txtCity", ci);
            lblCustomerCountry.Text = rm.GetString("txtCountry", ci);
            lblCustomerAddressNative.Text = rm.GetString("txtAddressNative", ci);
            //		lblCustomerAddresENs.Text = rm.GetString("txtAddressEN",ci);
            lblCustomerPostalCode.Text = rm.GetString("txtPostalCode", ci);
            btnFindCustomer.Text = rm.GetString("txtSearch", ci);
            btnCreateNewCustomer.Text = rm.GetString("txtCreateNewCustomer", ci);
            btnCustomerClear.Text = rm.GetString("txtClear", ci);
            rbtnCustomerCompany.Text = ""; //rm.GetString("txtCompany",ci);
            imgCustomerCompany.AlternateText = rm.GetString("txtCompany", ci);
            rbtnCustomerIndividual.Text = ""; //rm.GetString("txtIndividual",ci);
            imgCustomerIndividual.AlternateText = rm.GetString("txtIndividual", ci);
            lblCustomerDataGrid.Text = rm.GetString("txtResults", ci);
            //	Subject Info
            //	lblSubjectType.Text = rm.GetString("txtSubjectType",ci);
            lblSubjectInformation.Text = rm.GetString("txtSubjectInformation", ci);
            lblSubjectRegistrationNumber.Text = rm.GetString("txtIDNumber", ci);
            lblSubjectIDNumberType.Text = rm.GetString("txtType", ci);
            lblSubjectCIID.Text = rm.GetString("txtCIID", ci);
            //	lblSubjectNameNative.Text = rm.GetString("txtNameNative",ci);
            //	lblSubjectNameEN.Text = rm.GetString("txtNameEN",ci);
            //	lblSubjectFirstNameNative.Text = rm.GetString("txtFirstName",ci);
            //	lblSubjectFirstNameEN.Text = rm.GetString("txtFirstNameEN",ci);
            lblSubjectLastNameNative.Text = rm.GetString("txtLastNameNative", ci);
            //	lblSubjectLastNameEN.Text = rm.GetString("txtLastNameEN",ci);
            lblSubjectCity.Text = rm.GetString("txtCity", ci);
            lblSubjectCountry.Text = rm.GetString("txtCountry", ci);
            lblSubjectAddressNative.Text = rm.GetString("txtAddressNative", ci);
            //	lblSubjectAddressEN.Text = rm.GetString("txtAddressEN",ci);
            lblSubjectPostalCode.Text = rm.GetString("txtPostalCode", ci);
            rbtnSubjectCompany.Text = ""; //rm.GetString("txtCompany",ci);
            imgSubjectCompany.AlternateText = rm.GetString("txtCompany", ci);
            rbtnSubjectIndividual.Text = ""; //rm.GetString("txtIndividual",ci);
            imgSubjectIndividual.AlternateText = rm.GetString("txtIndividual", ci);
            lblSubjectDataGrid.Text = rm.GetString("txtResults", ci);

            btnFindSubject.Text = rm.GetString("txtSearch", ci);
            btnCreateNewSubject.Text = rm.GetString("txtCreateNewCustomer", ci);
            btnSubjectClear.Text = rm.GetString("txtClear", ci);
            btnNext.Text = rm.GetString("txtNext", ci);
        }

        private void SetGridHeaders() {
            if (EN) {
                dgCustomer.Columns[0].Visible = true;
                dgCustomer.Columns[1].Visible = true;
                dgCustomer.Columns[2].Visible = true;
                dgCustomer.Columns[3].Visible = false;
                dgCustomer.Columns[4].Visible = true;
                dgCustomer.Columns[5].Visible = false;
                dgCustomer.Columns[6].Visible = false;
                dgCustomer.Columns[7].Visible = false;
                dgCustomer.Columns[8].Visible = false;
                dgCustomer.Columns[9].Visible = false;
                dgCustomer.Columns[10].Visible = true;
                dgCustomer.Columns[11].Visible = false;
                dgCustomer.Columns[12].Visible = true;
                dgCustomer.Columns[13].Visible = false;
                dgCustomer.Columns[14].Visible = false;
                dgCustomer.Columns[15].Visible = true;
                dgCustomer.Columns[16].Visible = false;
                dgCustomer.Columns[17].Visible = false;
                dgCustomer.Columns[18].Visible = false;
                dgCustomer.Columns[19].Visible = false;

                dgSubject.Columns[0].Visible = true;
                dgSubject.Columns[1].Visible = true;
                dgSubject.Columns[2].Visible = true;
                dgSubject.Columns[3].Visible = false;
                dgSubject.Columns[4].Visible = true;
                dgSubject.Columns[5].Visible = false;
                dgSubject.Columns[6].Visible = false;
                dgSubject.Columns[7].Visible = false;
                dgSubject.Columns[8].Visible = false;
                dgSubject.Columns[9].Visible = false;
                dgSubject.Columns[10].Visible = true;
                dgSubject.Columns[11].Visible = false;
                dgSubject.Columns[12].Visible = true;
                dgSubject.Columns[13].Visible = false;
                dgSubject.Columns[14].Visible = false;
                dgSubject.Columns[15].Visible = true;
                dgSubject.Columns[16].Visible = false;
                dgSubject.Columns[17].Visible = false;
                dgSubject.Columns[18].Visible = false;
                dgSubject.Columns[19].Visible = false;
            } else {
                dgCustomer.Columns[0].Visible = true;
                dgCustomer.Columns[1].Visible = true;
                dgCustomer.Columns[2].Visible = true;
                dgCustomer.Columns[3].Visible = false;
                dgCustomer.Columns[4].Visible = false;
                dgCustomer.Columns[5].Visible = true;
                dgCustomer.Columns[6].Visible = false;
                dgCustomer.Columns[7].Visible = false;
                dgCustomer.Columns[8].Visible = false;
                dgCustomer.Columns[9].Visible = false;
                dgCustomer.Columns[10].Visible = false;
                dgCustomer.Columns[11].Visible = true;
                dgCustomer.Columns[12].Visible = false;
                dgCustomer.Columns[13].Visible = true;
                dgCustomer.Columns[14].Visible = false;
                dgCustomer.Columns[15].Visible = true;
                dgCustomer.Columns[16].Visible = false;
                dgCustomer.Columns[17].Visible = false;
                dgCustomer.Columns[18].Visible = false;
                dgCustomer.Columns[19].Visible = false;

                dgSubject.Columns[0].Visible = true;
                dgSubject.Columns[1].Visible = true;
                dgSubject.Columns[2].Visible = true;
                dgSubject.Columns[3].Visible = false;
                dgSubject.Columns[4].Visible = false;
                dgSubject.Columns[5].Visible = true;
                dgSubject.Columns[6].Visible = false;
                dgSubject.Columns[7].Visible = false;
                dgSubject.Columns[8].Visible = false;
                dgSubject.Columns[9].Visible = false;
                dgSubject.Columns[10].Visible = false;
                dgSubject.Columns[11].Visible = true;
                dgSubject.Columns[12].Visible = false;
                dgSubject.Columns[13].Visible = true;
                dgSubject.Columns[14].Visible = false;
                dgSubject.Columns[15].Visible = true;
                dgSubject.Columns[16].Visible = false;
                dgSubject.Columns[17].Visible = false;
                dgSubject.Columns[18].Visible = false;
                dgSubject.Columns[19].Visible = false;
            }

            // Customer Grid
            dgCustomer.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
//			dgCustomer.Columns[2].HeaderText = rm.GetString("txtIDNumber",ci);
            dgCustomer.Columns[2].HeaderText = ddlstCustomerIDNumberType.SelectedItem.Text;
            dgCustomer.Columns[3].HeaderText = rm.GetString("txtIDNumberType", ci);
            dgCustomer.Columns[4].HeaderText = rm.GetString("txtNameEN", ci);
            dgCustomer.Columns[5].HeaderText = rm.GetString("txtName", ci);
            dgCustomer.Columns[6].HeaderText = rm.GetString("txtFirstNameEN", ci);
            dgCustomer.Columns[7].HeaderText = rm.GetString("txtFirstName", ci);
            dgCustomer.Columns[8].HeaderText = rm.GetString("txtSurNameEN", ci);
            dgCustomer.Columns[9].HeaderText = rm.GetString("txtSurName", ci);
            dgCustomer.Columns[10].HeaderText = rm.GetString("txtAddressEN", ci);
            dgCustomer.Columns[11].HeaderText = rm.GetString("txtAddress", ci);
            dgCustomer.Columns[12].HeaderText = rm.GetString("txtCityEN", ci);
            dgCustomer.Columns[13].HeaderText = rm.GetString("txtCity", ci);
            dgCustomer.Columns[14].HeaderText = rm.GetString("txtCityID", ci);
            dgCustomer.Columns[15].HeaderText = rm.GetString("txtPostalCode", ci);
            dgCustomer.Columns[16].HeaderText = rm.GetString("txtCountryEN", ci);
            dgCustomer.Columns[17].HeaderText = rm.GetString("txtCountry", ci);
            dgCustomer.Columns[18].HeaderText = rm.GetString("txtCity", ci);
            dgCustomer.Columns[19].HeaderText = rm.GetString("txtCity", ci);

            // Subject Grid
            dgSubject.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
//			dgSubject.Columns[2].HeaderText = rm.GetString("txtIDNumber",ci);
            dgSubject.Columns[2].HeaderText = ddlstSubjectIDNumberType.SelectedItem.Text;
            dgSubject.Columns[3].HeaderText = rm.GetString("txtIDNumberType", ci);
            dgSubject.Columns[4].HeaderText = rm.GetString("txtNameEN", ci);
            dgSubject.Columns[5].HeaderText = rm.GetString("txtName", ci);
            dgSubject.Columns[6].HeaderText = rm.GetString("txtFirstNameEN", ci);
            dgSubject.Columns[7].HeaderText = rm.GetString("txtFirstName", ci);
            dgSubject.Columns[8].HeaderText = rm.GetString("txtSurNameEN", ci);
            dgSubject.Columns[9].HeaderText = rm.GetString("txtSurName", ci);
            dgSubject.Columns[10].HeaderText = rm.GetString("txtAddressEN", ci);
            dgSubject.Columns[11].HeaderText = rm.GetString("txtAddress", ci);
            dgSubject.Columns[12].HeaderText = rm.GetString("txtCityEN", ci);
            dgSubject.Columns[13].HeaderText = rm.GetString("txtCity", ci);
            dgSubject.Columns[14].HeaderText = rm.GetString("txtCityID", ci);
            dgSubject.Columns[15].HeaderText = rm.GetString("txtPostalCode", ci);
            dgSubject.Columns[16].HeaderText = rm.GetString("txtCountryEN", ci);
            dgSubject.Columns[17].HeaderText = rm.GetString("txtCountry", ci);
            dgSubject.Columns[18].HeaderText = rm.GetString("txtCity", ci);
            dgSubject.Columns[19].HeaderText = rm.GetString("txtCity", ci);
        }

        private void btnCustomerClear_Click(object sender, EventArgs e) {
            //	this.txtCustomerAddressEN.Text = "";
            txtCustomerAddressNative.Text = "";
            txtCustomerCIID.Text = "";
            //	this.txtCustomerFirstNameEN.Text = "";
            txtCustomerFirstNameNative.Text = "";
            txtCustomerIDNumber.Text = "";
            //	this.txtCustomerLastNameEN.Text = "";
            txtCustomerLastNameNative.Text = "";
            //	this.txtCustomerNameEN.Text = "";
            //	this.txtCustomerNameNative.Text = "";
            txtCustomerPostalCode.Text = "";

            try {
                string defaultCountry = CigConfig.Configure("lookupsettings.ROSDefaultCountry").Trim();
                ddlstCustomerCountry.SelectedValue = defaultCountry;
            } catch (Exception ex) {
                Logger.WriteToLog(
                    "OrderCustomerSubjectSearch : btnCustomerClear_Click Error getting ROSDefaultCountry from web.config: " +
                    ex.Message,
                    true);
            }
            try {
                string defaultCity = CigConfig.Configure("lookupsettings.ROSDefaultCity").Trim();
                ddlistCustomerCity.SelectedValue = defaultCity;
            } catch (Exception ex) {
                Logger.WriteToLog(
                    "OrderCustomerSubjectSearch : btnCustomerClear_Click Error getting ROSDefaultCity from web.config: " +
                    ex.Message,
                    true);
            }

            Session["CustomerSelected"] = false;

            var theOrder = (OrderBLLC) Session["RosOrder"];

            theOrder.Customer = null;

            Session["RosOrder"] = theOrder;

            btnNext.Visible = false;
            //this.btnNextDiv.Visible = false;
        }

        private void btnSubjectClear_Click(object sender, EventArgs e) {
            //	this.txtSubjectAddressEN.Text = "";
            txtSubjectAddressNative.Text = "";
            txtSubjectCIID.Text = "";
            //	this.txtSubjectFirstNameEN.Text = "";
            txtSubjectFirstNameNative.Text = "";
            txtSubjectIDNumber.Text = "";
            //	this.txtSubjectLastNameEN.Text = "";
            txtSubjectLastNameNative.Text = "";
            //	this.txtSubjectNameEN.Text = "";
            //	this.txtSubjectNameNative.Text = "";
            txtSubjectPostalCode.Text = "";

            try {
                string defaultCountry = CigConfig.Configure("lookupsettings.ROSDefaultCountry").Trim();
                ddlstSubjectCountry.SelectedValue = defaultCountry;
            } catch (Exception ex) {
                Logger.WriteToLog(
                    "OrderCustomerSubjectSearch : btnSubjectClear_Click Error getting ROSDefaultCountry from web.config: " +
                    ex.Message,
                    true);
            }
            try {
                string defaultCity = CigConfig.Configure("lookupsettings.ROSDefaultCity").Trim();
                ddlstSubjectCity.SelectedValue = defaultCity;
            } catch (Exception ex) {
                Logger.WriteToLog(
                    "OrderCustomerSubjectSearch : btnSubjectClear_Click Error getting ROSDefaultCity from web.config: " +
                    ex.Message,
                    true);
            }

            var theOrder = (OrderBLLC) Session["RosOrder"];

            theOrder.Subject = null;

            Session["RosOrder"] = theOrder;

            Session["SubjectSelected"] = false;

            btnNext.Visible = false;
//			this.btnNextDiv.Visible = false;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rbtnCustomerIndividual.CheckedChanged +=
                new System.EventHandler(this.rbtnCustomerIndividual_CheckedChanged);
            this.rbtnCustomerCompany.CheckedChanged += new System.EventHandler(this.rbtnCustomerCompany_CheckedChanged);
            this.btnFindCustomer.Click += new System.EventHandler(this.btnFindCustomer_Click);
            this.dgCustomer.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCustomer_ItemCommand);
            this.dgCustomer.SelectedIndexChanged += new System.EventHandler(this.dgCustomer_SelectedIndexChanged);
            this.btnCustomerClear.Click += new System.EventHandler(this.btnCustomerClear_Click);
            this.btnCreateNewCustomer.Click += new System.EventHandler(this.btnCreateNewCustomer_Click);
            this.rbtnSubjectIndividual.CheckedChanged +=
                new System.EventHandler(this.rbtnSubjectIndividual_CheckedChanged);
            this.rbtnSubjectCompany.CheckedChanged += new System.EventHandler(this.rbtnSubjectCompany_CheckedChanged);
            this.btnFindSubject.Click += new System.EventHandler(this.btnFindSubject_Click);
            this.dgSubject.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgSubject_ItemCommand);
            this.btnSubjectClear.Click += new System.EventHandler(this.btnSubjectClear_Click);
            this.btnCreateNewSubject.Click += new System.EventHandler(this.btnCreateNewSubject_Click);
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

        #region Customer

        private void rbtnCustomerIndividual_CheckedChanged(object sender, EventArgs e) {
            outerGridTableCustomer.Visible = false;
            CheckCustomerRadioButtons();
        }

        private void rbtnCustomerCompany_CheckedChanged(object sender, EventArgs e) {
            outerGridTableCustomer.Visible = false;
            CheckCustomerRadioButtons();
        }

        private void CheckCustomerRadioButtons() {
            if (rbtnCustomerCompany.Checked) {
                //		this.lblCustomerFirstNameEN.Visible = false;

                lblCustomerFirstNameNative.Text = rm.GetString("txtNameNative", ci);
                //		this.lblCustomerFirstNameNative.Visible = false;
                //		this.lblCustomerLastNameEN.Visible = false;
                lblCustomerLastNameNative.Visible = false;
                //		this.txtCustomerFirstNameEN.Visible = false;
                //		this.txtCustomerFirstNameNative.Visible = false;
                //		this.txtCustomerLastNameEN.Visible = false;
                txtCustomerLastNameNative.Visible = false;

                //		this.lblCustomerNameEN.Visible = true;
                //		this.lblCustomerNameNative.Visible = true;
                //		this.txtCustomerNameEN.Visible = true;
                //		this.txtCustomerNameNative.Visible = true;
            } else {
                //		this.lblCustomerNameEN.Visible = false;
                //		this.lblCustomerNameNative.Visible = false;
                //		this.txtCustomerNameEN.Visible = false;
                //		this.txtCustomerNameNative.Visible = false;

                //		this.lblCustomerFirstNameEN.Visible = true;

                lblCustomerFirstNameNative.Text = rm.GetString("txtFirstName", ci);
                //		this.lblCustomerFirstNameNative.Visible = true;
                //		this.lblCustomerLastNameEN.Visible = true;
                lblCustomerLastNameNative.Visible = true;
                //		this.txtCustomerFirstNameEN.Visible = true;
                //		this.txtCustomerFirstNameNative.Visible = true;
                //		this.txtCustomerLastNameEN.Visible = true;
                txtCustomerLastNameNative.Visible = true;
            }
        }

        private void InsertIntoCustomerInfo(CustomerBLLC theCustomer) {
            if (theCustomer != null) {
                if (theCustomer.IsCompany) {
                    rbtnCustomerCompany.Checked = true;
                    rbtnCustomerIndividual.Checked = false;
                    CheckCustomerRadioButtons();

                    txtCustomerFirstNameNative.Text = EN ? ((CompanyBLLC) theCustomer).NameEN : ((CompanyBLLC) theCustomer).NameNative;
                } else {
                    rbtnCustomerIndividual.Checked = true;
                    rbtnCustomerCompany.Checked = false;
                    CheckCustomerRadioButtons();

                    if (EN) {
                        txtCustomerFirstNameNative.Text = ((IndividualBLLC) theCustomer).FirstNameEN;
                        txtCustomerLastNameNative.Text = ((IndividualBLLC) theCustomer).LastNameEN;
                    } else {
                        txtCustomerFirstNameNative.Text = ((IndividualBLLC) theCustomer).FirstNameNative;
                        txtCustomerLastNameNative.Text = ((IndividualBLLC) theCustomer).LastNameNative;
                    }
                }

                if (theCustomer.IDNumbers.Count > 0) {
                    var theID = (IDNumberBLLC) theCustomer.IDNumbers[0];
                    txtCustomerIDNumber.Text = theID.Number;
                    try {
                        ddlstCustomerIDNumberType.SelectedValue = "" + theID.NumberTypeID;
                    } catch (Exception ex) {
                        Logger.WriteToLog(
                            "Caught an exception while trying to set ddlstCustomerIDNumberType.SelectedValue = " +
                            theID.NumberTypeID + " in InsertIntoCustomerInfo in class OrderCustomerSubjectSearch: " +
                            ex.Message,
                            true);
                    }
                }

                if (theCustomer.Address.Count > 0) {
                    var theAddress = (AddressBLLC) theCustomer.Address[0];

                    //			this.txtCustomerAddressEN.Text = theAddress.StreetEN;
                    txtCustomerAddressNative.Text = theAddress.StreetNative;
                    try {
                        ddlistCustomerCity.SelectedValue = "" + theAddress.CityID;
                    } catch (Exception ex) {
                        Logger.WriteToLog(
                            "Caught an exception while trying to set ddlistCustomerCity.SelectedValue = " +
                            theAddress.CityID + " in InsertIntoCustomerInfo in class OrderCustomerSubjectSearch: " +
                            ex.Message,
                            true);
                    }
                    try {
                        ddlstCustomerCountry.SelectedValue = "" + theAddress.CountryID;
                    } catch (Exception ex) {
                        Logger.WriteToLog(
                            "Caught an exception while trying to set ddlstCustomerCountry.SelectedValue = " +
                            theAddress.CountryID + " in InsertIntoCustomerInfo in class OrderCustomerSubjectSearch: " +
                            ex.Message,
                            true);
                    }

                    txtCustomerPostalCode.Text = theAddress.PostalCode;
                }

                txtCustomerCIID.Text = "" + theCustomer.CreditInfoID;
            }
        }

        private void InsertIntoDatagrid(ArrayList theList, bool customer, bool company) {
            var theTable = new DataTable();
            theTable.Columns.Add("CreditInfoID");
            theTable.Columns.Add("IDNumber");
            theTable.Columns.Add("IDNumberType");
            theTable.Columns.Add("NameEN");
            theTable.Columns.Add("NameNative");
            theTable.Columns.Add("FirstNameEN");
            theTable.Columns.Add("FirstNameNative");
            theTable.Columns.Add("LastNameEN");
            theTable.Columns.Add("LastNameNative");
            theTable.Columns.Add("AddressEN");
            theTable.Columns.Add("AddressNative");
            theTable.Columns.Add("CityEN");
            theTable.Columns.Add("CityNative");
            theTable.Columns.Add("CityID");
            theTable.Columns.Add("PostalCode");
            theTable.Columns.Add("CountryEN");
            theTable.Columns.Add("CountryNative");
            theTable.Columns.Add("CountryID");
            theTable.Columns.Add("IsCompany");

            if (company) {
                foreach (CompanyBLLC theCompany in theList) {
                    DataRow aRow = theTable.NewRow();
                    if (theCompany.CreditInfoID != -1) {
                        aRow[0] = theCompany.CreditInfoID;
                    }

                    if (theCompany.IDNumbers.Count > 0) {
                        var theID = (IDNumberBLLC) theCompany.IDNumbers[0];
                        aRow[1] = theID.Number;
                        aRow[2] = theID.NumberTypeID;
                    }
/*					else
					{
						aRow[1] = theIndi.NationalID;
						aRow[2] = "";
					}
*/
                    if (theCompany.CreditInfoID == -1) {
                        aRow[3] = theCompany.NationalName;
                        aRow[4] = theCompany.NationalName;
                    } else {
                        aRow[3] = theCompany.NameEN.Length > 0 ? theCompany.NameEN : theCompany.NameNative;

                        aRow[4] = theCompany.NameNative.Length > 0 ? theCompany.NameNative : theCompany.NameEN;
                    }
                    aRow[5] = "";
                    aRow[6] = "";
                    aRow[7] = "";
                    aRow[8] = "";
                    if (theCompany.Address.Count > 0) {
                        var theAddress = (AddressBLLC) theCompany.Address[0];

                        aRow[9] = theAddress.StreetEN.Length > 0 ? theAddress.StreetEN : theAddress.StreetNative;

                        aRow[10] = theAddress.StreetNative.Length > 0 ? theAddress.StreetNative : theAddress.StreetEN;

                        aRow[11] = theAddress.CityNameEN;
                        aRow[12] = theAddress.CityNameNative;
                        aRow[13] = theAddress.CityID;
                        aRow[14] = theAddress.PostalCode;
                        aRow[15] = theAddress.CountryNameEN;
                        aRow[16] = theAddress.CountryNameNative;
                        aRow[17] = theAddress.CountryID;
                    } else {
                        if (theCompany.CreditInfoID == -1) {
                            aRow[9] = theCompany.NationalAddress;
                            aRow[10] = theCompany.NationalAddress;
                            aRow[14] = theCompany.NationalPostNumber;
                        }
                    }
                    aRow[18] = theCompany.IsCompany;
                    theTable.Rows.Add(aRow);
                }
            } else {
                foreach (IndividualBLLC theIndi in theList) {
                    DataRow aRow = theTable.NewRow();
                    if (theIndi.CreditInfoID != -1) {
                        aRow[0] = theIndi.CreditInfoID;
                    }

                    if (theIndi.IDNumbers.Count > 0) {
                        var theID = (IDNumberBLLC) theIndi.IDNumbers[0];
                        aRow[1] = theID.Number;
                        aRow[2] = theID.NumberTypeID;
                    }
/*					else
					{
						aRow[1] = theIndi.NationalID;
						aRow[2] = "";
					}
*/
                    if (theIndi.CreditInfoID == -1) {
                        aRow[3] = theIndi.NationalName;
                        aRow[4] = theIndi.NationalName;
                    } else {
                        if (theIndi.FirstNameEN.Length > 0 || theIndi.LastNameEN.Length > 0) {
                            aRow[3] = theIndi.FirstNameEN + " " + theIndi.LastNameEN;
                        } else {
                            aRow[3] = theIndi.FirstNameNative + " " + theIndi.LastNameNative;
                        }

                        if (theIndi.FirstNameNative.Length > 0 || theIndi.LastNameNative.Length > 0) {
                            aRow[4] = theIndi.FirstNameNative + " " + theIndi.LastNameNative;
                        } else {
                            aRow[4] = theIndi.FirstNameEN + " " + theIndi.LastNameEN;
                        }
                    }
                    aRow[5] = theIndi.FirstNameEN;
                    aRow[6] = theIndi.FirstNameNative;
                    aRow[7] = theIndi.LastNameEN;
                    aRow[8] = theIndi.LastNameNative;
                    if (theIndi.Address.Count > 0) {
                        var theAddress = (AddressBLLC) theIndi.Address[0];
                        aRow[9] = theAddress.StreetEN.Length > 0 ? theAddress.StreetEN : theAddress.StreetNative;

                        aRow[10] = theAddress.StreetNative.Length > 0 ? theAddress.StreetNative : theAddress.StreetEN;

                        aRow[11] = theAddress.CityNameEN;
                        aRow[12] = theAddress.CityNameNative;
                        aRow[13] = theAddress.CityID;
                        aRow[14] = theAddress.PostalCode;
                        aRow[15] = theAddress.CountryNameEN;
                        aRow[16] = theAddress.CountryNameNative;
                        aRow[17] = theAddress.CountryID;
                    } else {
                        aRow[9] = theIndi.NationalAddress;
                        aRow[10] = theIndi.NationalAddress;
                        aRow[14] = theIndi.NationalPostNumber;
                    }
                    aRow[18] = theIndi.IsCompany;
                    theTable.Rows.Add(aRow);
                }
            }

            if (customer) {
                dgCustomer.DataSource = theTable;
                dgCustomer.DataBind();
            } else {
                dgSubject.DataSource = theTable;
                dgSubject.DataBind();
            }
        }

        private void btnCreateNewCustomer_Click(object sender, EventArgs e) {
            btnCreateNewCustomer.Visible = false;
            Session["State"] = "";
            Session["CustomerSelected"] = false;

            var theOrder = (OrderBLLC) Session["RosOrder"];

            AddressBLLC theAddress = null;

            if (txtCustomerAddressNative.Text.Trim().Length > 0 || ddlistCustomerCity.SelectedValue.Trim().Length > 0 ||
                ddlstCustomerCountry.SelectedValue.Trim().Length > 0 || txtCustomerPostalCode.Text.Trim().Length > 0) {
                theAddress = new AddressBLLC
                             {
                                 StreetEN = txtCustomerAddressNative.Text,
                                 StreetNative = txtCustomerAddressNative.Text
                             };
                try {
                    if (ddlistCustomerCity.SelectedValue.Trim().Length > 0) {
                        theAddress.CityID = int.Parse(ddlistCustomerCity.SelectedValue);
                    }
                    if (ddlstCustomerCountry.SelectedValue.Trim().Length > 0) {
                        theAddress.CountryID = int.Parse(ddlstCustomerCountry.SelectedValue);
                    }
                } catch (Exception ex) {
                    Logger.WriteToLog(
                        "Caught an exception in OrderCustomerSubjectSearch - btnCreateNewCustomer - when trying to parse ddlstCustomerCountry.SelectedValue or ddlistCustomerCity.SelectedValue: " +
                        ex.Message,
                        true);
                }
                theAddress.PostalCode = txtCustomerPostalCode.Text;
            }

            if (rbtnCustomerCompany.Checked) {
                //This is a new company
                var theCompany = new CompanyBLLC
                                 {
                                     IsCompany = true,
                                     NameEN = txtCustomerFirstNameNative.Text,
                                     NameNative = txtCustomerFirstNameNative.Text
                                 };
                if (theAddress != null) {
                    theCompany.Address.Add(theAddress);
                }
                if (txtCustomerIDNumber.Text.Length > 0) {
                    var theID = new IDNumberBLLC {Number = txtCustomerIDNumber.Text};
                    try {
                        theID.NumberTypeID = int.Parse(ddlstCustomerIDNumberType.SelectedValue);
                    } catch (Exception ex) {
                        Logger.WriteToLog(
                            "Caught an exception while trying to parse idnumber in btnCreateNewCustomer_Click: " +
                            ex.Message,
                            true);
                    }
                    theCompany.IDNumbers.Add(theID);
                }

                theOrder.Customer = theCompany;

                Session["RosOrder"] = theOrder;
                Session["CustomerType"] = "Customer";

                Server.Transfer("NewCustomerC.aspx");
            } else {
                //This is an individual
                var theIndividual = new IndividualBLLC
                                    {
                                        IsCompany = false,
                                        FirstNameEN = txtCustomerFirstNameNative.Text,
                                        FirstNameNative = txtCustomerFirstNameNative.Text,
                                        LastNameEN = txtCustomerLastNameNative.Text,
                                        LastNameNative = txtCustomerLastNameNative.Text
                                    };
                if (theAddress != null) {
                    theIndividual.Address.Add(theAddress);
                }
                if (txtCustomerIDNumber.Text.Length > 0) {
                    var theID = new IDNumberBLLC {Number = txtCustomerIDNumber.Text};
                    try {
                        theID.NumberTypeID = int.Parse(ddlstCustomerIDNumberType.SelectedValue);
                    } catch (Exception ex) {
                        Logger.WriteToLog(
                            "Caught an exception while trying to parse idnumber in btnCreateNewCustomer_Click: " +
                            ex.Message,
                            true);
                    }
                    theIndividual.IDNumbers.Add(theID);
                }

                theOrder.Customer = theIndividual;

                Session["RosOrder"] = theOrder;
                Session["CustomerType"] = "Customer";

                Server.Transfer("NewCustomer.aspx");
            }
        }

        private void btnFindCustomer_Click(object sender, EventArgs e) {
            try {
                btnFindCustomer.Enabled = false;
                lblCustomerErrorMessage.Visible = false;
                outerGridTableCustomer.Visible = false;
                Session["CustomerSelected"] = false;

                CustomerBLLC theCustomer;

                if (rbtnCustomerCompany.Checked) {
                    theCustomer = new CompanyBLLC {IsCompany = true};

                    ((CompanyBLLC) theCustomer).NameEN = txtCustomerFirstNameNative.Text;
                    ((CompanyBLLC) theCustomer).NameNative = txtCustomerFirstNameNative.Text;
                } else {
                    theCustomer = new IndividualBLLC {IsCompany = false};

                    ((IndividualBLLC) theCustomer).FirstNameEN = txtCustomerFirstNameNative.Text;
                    ((IndividualBLLC) theCustomer).FirstNameNative = txtCustomerFirstNameNative.Text;
                    ((IndividualBLLC) theCustomer).LastNameEN = txtCustomerLastNameNative.Text;
                    ((IndividualBLLC) theCustomer).LastNameNative = txtCustomerLastNameNative.Text;
                }

                var theAddress = new AddressBLLC
                                 {
                                     StreetEN = txtCustomerAddressNative.Text,
                                     StreetNative = txtCustomerAddressNative.Text
                                 };
                if (txtCustomerCIID.Text.Length > 0) {
                    theCustomer.CreditInfoID = int.Parse(txtCustomerCIID.Text);
                }
                if (ddlistCustomerCity.SelectedValue.Trim().Length > 0) {
                    theAddress.CityID = int.Parse(ddlistCustomerCity.SelectedValue);
                }
                if (ddlstCustomerCountry.SelectedValue.Trim().Length > 0) {
                    theAddress.CountryID = int.Parse(ddlstCustomerCountry.SelectedValue);
                }
                theAddress.PostalCode = txtCustomerPostalCode.Text;
                theCustomer.Address.Add(theAddress);

                if (txtCustomerIDNumber.Text.Length > 0) {
                    var theID = new IDNumberBLLC
                                {
                                    Number = txtCustomerIDNumber.Text,
                                    NumberTypeID = int.Parse(ddlstCustomerIDNumberType.SelectedValue)
                                };
                    theCustomer.IDNumbers.Add(theID);
                }

                ArrayList theList;

                //Get the ID of the nationalID!
                string nationalID = CigConfig.Configure("lookupsettings.ROSNationalID").Trim();

                //Leita � b��i einstaklingagrunni og fyrirt�kjagrunni
                //B��i � �j��skr� og hj� LT
                var theFactory = new FactoryBLLC();
                if (rbtnCustomerCompany.Checked) {
                    //The customer to search for is a company

                    theList = nationalID == ddlstCustomerIDNumberType.SelectedValue.Trim() ? theFactory.FindCompanyInNationalAndCreditInfo((CompanyBLLC) theCustomer) : theFactory.FindCompanyAsArray((CompanyBLLC) theCustomer);
                } else {
                    //The customer to search for is an individual

                    theList = nationalID == ddlstCustomerIDNumberType.SelectedValue.Trim() ? theFactory.FindIndividualInNationalAndCreditInfo((IndividualBLLC) theCustomer) : theFactory.FindIndividualAsArray((IndividualBLLC) theCustomer);
                }

                if (theList == null) {
                    //An error was encountered while trying to retrieve data from the database
                    lblCustomerErrorMessage.Text = rm.GetString("txtDatabaseError", ci);
                    lblCustomerErrorMessage.Visible = true;
                } else {
                    if (theList.Count > 0) {
                        //If some customers were found display them
                        lblCustomerErrorMessage.Visible = false;
                        InsertIntoDatagrid(theList, true, rbtnCustomerCompany.Checked);

                        if (theList.Count < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll"))) {
                            divCustomers.Style.Remove("HEIGHT");
                        } else {
                            divCustomers.Style.Add("HEIGHT", "150px");
                        }

                        outerGridTableCustomer.Visible = true;
                    } else {
                        //Else offer the user to create a new customer
                        lblCustomerErrorMessage.Text = rm.GetString("txtNoCustomerFound", ci);
                        lblCustomerErrorMessage.Visible = true;
                    }
                }
                btnFindCustomer.Enabled = true;
                //this.btnFindCustomer.Text = rm.GetString("txtSearchAgain",ci);
            } catch (Exception ex) {
                Logger.WriteToLog("Exception caught in btnFindCustomer_Click: " + ex.Message, true);
                lblCustomerErrorMessage.Text = rm.GetString("txtExceptionCaught", ci);
                lblCustomerErrorMessage.Visible = true;
            }
        }

        private void dgCustomer_SelectedIndexChanged(object sender, EventArgs e) { }

        private void dgCustomer_ItemCommand(object source, DataGridCommandEventArgs e) {
            try {
                if (e.CommandName == "Select") {
                    //Put the selected customer into the order
                    CustomerBLLC theCustomer;
                    bool isCompany = true;

                    if (e.Item.Cells[19].Text != "") {
                        isCompany = bool.Parse(e.Item.Cells[19].Text);
                    }

                    int creditInfoID = -1;

                    try {
                        creditInfoID = int.Parse(e.Item.Cells[1].Text.Trim());
                    } catch {
                        creditInfoID = -1;
                    }

                    if (isCompany) {
                        theCustomer = new CompanyBLLC {IsCompany = true};
                        rbtnCustomerCompany.Checked = true;
                        rbtnCustomerIndividual.Checked = false;
                        CheckCustomerRadioButtons();

                        theCustomer.CreditInfoID = creditInfoID;

                        if (e.Item.Cells[4].Text != "&nbsp;") {
                            ((CompanyBLLC) theCustomer).NameEN = e.Item.Cells[4].Text.Trim();
                        }
                        if (e.Item.Cells[5].Text != "&nbsp;") {
                            ((CompanyBLLC) theCustomer).NameNative = e.Item.Cells[5].Text.Trim();
                        }
                    } else {
                        theCustomer = new IndividualBLLC {IsCompany = false};
                        rbtnCustomerIndividual.Checked = true;
                        rbtnCustomerCompany.Checked = false;
                        CheckCustomerRadioButtons();

                        theCustomer.CreditInfoID = creditInfoID;

                        if (theCustomer.CreditInfoID != -1) {
                            if (e.Item.Cells[6].Text != "&nbsp;") {
                                ((IndividualBLLC) theCustomer).FirstNameEN = e.Item.Cells[6].Text.Trim();
                            }
                            if (e.Item.Cells[7].Text != "&nbsp;") {
                                ((IndividualBLLC) theCustomer).FirstNameNative = e.Item.Cells[7].Text.Trim();
                            }
                            if (e.Item.Cells[8].Text != "&nbsp;") {
                                ((IndividualBLLC) theCustomer).LastNameEN = e.Item.Cells[8].Text.Trim();
                            }
                            if (e.Item.Cells[9].Text != "&nbsp;") {
                                ((IndividualBLLC) theCustomer).LastNameNative = e.Item.Cells[9].Text.Trim();
                            }
                        } else {
                            string name = e.Item.Cells[5].Text.Trim();
                            int index = name.LastIndexOf(" ");
                            string lastName = name.Substring(index + 1);
                            string firstName = name.Substring(0, index);

                            ((IndividualBLLC) theCustomer).FirstNameNative = firstName;
                            ((IndividualBLLC) theCustomer).LastNameNative = lastName;
                        }
                    }

                    //		if(e.Item.Cells[1].Text.Length > 0)
                    if (e.Item.Cells[2].Text != "&nbsp;") {
                        var theID = new IDNumberBLLC {Number = e.Item.Cells[2].Text.Trim()};
                        if (e.Item.Cells[3].Text.Length > 0) {
                            theID.NumberTypeID = int.Parse(e.Item.Cells[3].Text.Trim());
                        }
                        theCustomer.IDNumbers.Add(theID);
                    }

                    var theAddress = new AddressBLLC();

                    if (theCustomer.CreditInfoID != -1) {
                        if (e.Item.Cells[10].Text != "&nbsp;") {
                            theAddress.StreetEN = e.Item.Cells[10].Text.Trim();
                        }
                        if (e.Item.Cells[11].Text != "&nbsp;") {
                            theAddress.StreetNative = e.Item.Cells[11].Text.Trim();
                        }
                        if (e.Item.Cells[14].Text != "&nbsp;") {
                            theAddress.CityID = int.Parse(e.Item.Cells[14].Text.Trim());
                        }
                        if (e.Item.Cells[15].Text != "&nbsp;") {
                            theAddress.PostalCode = e.Item.Cells[15].Text.Trim();
                        }
                        if (e.Item.Cells[18].Text != "&nbsp;") {
                            theAddress.CountryID = int.Parse(e.Item.Cells[18].Text.Trim());
                        }
                    } else {
                        if (e.Item.Cells[11].Text != "&nbsp;") {
                            theAddress.StreetNative = e.Item.Cells[11].Text.Trim();
                        }
                        if (e.Item.Cells[15].Text != "&nbsp;") {
                            theAddress.PostalCode = e.Item.Cells[15].Text.Trim();
                        }
                    }
                    theCustomer.Address.Add(theAddress);

                    var theOrder = (OrderBLLC) Session["RosOrder"];
                    theOrder.Customer = theCustomer;
                    Session["RosOrder"] = theOrder;

                    if (theCustomer.CreditInfoID == -1) {
                        // The customer does not exist in our database
                        Session["CustomerType"] = "Customer";

                        try {
                            if (isCompany) {
                                Server.Transfer("NewCustomerC.aspx");
                            } else {
                                Server.Transfer("NewCustomer.aspx");
                            }
                        } catch (ThreadAbortException) {}
                    } else {
                        //Put the selected customer into the textfields

                        if (EN) {
                            if (isCompany) {
                                if (e.Item.Cells[4].Text != "&nbsp;") {
                                    txtCustomerFirstNameNative.Text = e.Item.Cells[4].Text;
                                }
                            } else {
                                if (e.Item.Cells[6].Text != "&nbsp;") {
                                    txtCustomerFirstNameNative.Text = e.Item.Cells[6].Text;
                                }
                                if (e.Item.Cells[8].Text != "&nbsp;") {
                                    txtCustomerLastNameNative.Text = e.Item.Cells[8].Text;
                                }
                            }

                            if (e.Item.Cells[10].Text != "&nbsp;") {
                                txtCustomerAddressNative.Text = e.Item.Cells[10].Text;
                            }
                        } else {
                            if (isCompany) {
                                if (e.Item.Cells[5].Text != "&nbsp;") {
                                    txtCustomerFirstNameNative.Text = e.Item.Cells[5].Text;
                                }
                            } else {
                                if (e.Item.Cells[7].Text != "&nbsp;") {
                                    txtCustomerFirstNameNative.Text = e.Item.Cells[7].Text;
                                }
                                if (e.Item.Cells[9].Text != "&nbsp;") {
                                    txtCustomerLastNameNative.Text = e.Item.Cells[9].Text;
                                }
                            }

                            if (e.Item.Cells[11].Text != "&nbsp;") {
                                txtCustomerAddressNative.Text = e.Item.Cells[11].Text;
                            }
                        }

                        if (e.Item.Cells[14].Text != "&nbsp;") {
                            ddlistCustomerCity.SelectedValue = e.Item.Cells[14].Text;
                        }
//						if(e.Item.Cells[18].Text != "&nbsp;")
//							this.ddlstCustomerCountry.SelectedValue = e.Item.Cells[18].Text;
                        if (e.Item.Cells[15].Text != "&nbsp;") {
                            txtCustomerPostalCode.Text = e.Item.Cells[15].Text;
                        }
                        if (e.Item.Cells[3].Text != "&nbsp;") {
                            ddlstCustomerIDNumberType.SelectedValue = e.Item.Cells[3].Text;
                        }
                        if (e.Item.Cells[2].Text != "&nbsp;") {
                            txtCustomerIDNumber.Text = e.Item.Cells[2].Text;
                        }
                        if (e.Item.Cells[1].Text != "&nbsp;") {
                            txtCustomerCIID.Text = e.Item.Cells[1].Text;
                        }

                        Session["CustomerSelected"] = true;
                    }

                    outerGridTableCustomer.Visible = false;
                    ShowNextButton();
                }
            } catch (Exception ex) {
                Logger.WriteToLog("Exception caught in dgCustomer_ItemCommand: " + ex.Message, true);
                lblCustomerErrorMessage.Text = rm.GetString("txtExceptionCaught", ci);
                lblCustomerErrorMessage.Visible = true;
            }
        }

        #endregion

        #region Subject

        private void rbtnSubjectCompany_CheckedChanged(object sender, EventArgs e) {
            outerGridTableSubject.Visible = false;
            CheckSubjectRadioButtons();
        }

        private void rbtnSubjectIndividual_CheckedChanged(object sender, EventArgs e) {
            outerGridTableSubject.Visible = false;
            CheckSubjectRadioButtons();
        }

        private void CheckSubjectRadioButtons() {
            if (rbtnSubjectCompany.Checked) {
                //	this.lblSubjectFirstNameEN.Visible = false;
                lblSubjectFirstNameNative.Text = rm.GetString("txtNameNative", ci);
                //	this.lblSubjectFirstNameNative.Visible = false;
                //	this.lblSubjectLastNameEN.Visible = false;
                lblSubjectLastNameNative.Visible = false;
                //	this.txtSubjectFirstNameEN.Visible = false;
                //	this.txtSubjectFirstNameNative.Visible = false;
                //	this.txtSubjectLastNameEN.Visible = false;
                txtSubjectLastNameNative.Visible = false;

                //	this.lblSubjectNameEN.Visible = true;
                //	this.lblSubjectNameNative.Visible = true;
                //	this.txtSubjectNameEN.Visible = true;
                //	this.txtSubjectNameNative.Visible = true;
            } else {
                //	this.lblSubjectNameEN.Visible = false;
                lblSubjectFirstNameNative.Text = rm.GetString("txtFirstName", ci);
                //	this.lblSubjectNameNative.Visible = false;
                //	this.txtSubjectNameEN.Visible = false;
                //	this.txtSubjectNameNative.Visible = false;

                //	this.lblSubjectFirstNameEN.Visible = true;
                //	this.lblSubjectFirstNameNative.Visible = true;
                //	this.lblSubjectLastNameEN.Visible = true;
                lblSubjectLastNameNative.Visible = true;
                //	this.txtSubjectFirstNameEN.Visible = true;
                //	this.txtSubjectFirstNameNative.Visible = true;
                //	this.txtSubjectLastNameEN.Visible = true;
                txtSubjectLastNameNative.Visible = true;
            }
        }

        private void btnFindSubject_Click(object sender, EventArgs e) {
            try {
                btnFindSubject.Enabled = false;
                lblSubjectErrorMessage.Visible = false;
                outerGridTableSubject.Visible = false;
                Session["SubjectSelected"] = false;

                CustomerBLLC theCustomer;

                if (rbtnSubjectCompany.Checked) {
                    theCustomer = new CompanyBLLC {IsCompany = true};

                    ((CompanyBLLC) theCustomer).NameEN = txtSubjectFirstNameNative.Text;
                    ((CompanyBLLC) theCustomer).NameNative = txtSubjectFirstNameNative.Text;
                } else {
                    theCustomer = new IndividualBLLC {IsCompany = false};

                    ((IndividualBLLC) theCustomer).FirstNameEN = txtSubjectFirstNameNative.Text;
                    ((IndividualBLLC) theCustomer).FirstNameNative = txtSubjectFirstNameNative.Text;
                    ((IndividualBLLC) theCustomer).LastNameEN = txtSubjectLastNameNative.Text;
                    ((IndividualBLLC) theCustomer).LastNameNative = txtSubjectLastNameNative.Text;
                }

                var theAddress = new AddressBLLC
                                 {
                                     StreetEN = txtSubjectAddressNative.Text,
                                     StreetNative = txtSubjectAddressNative.Text
                                 };
                if (txtSubjectCIID.Text.Length > 0) {
                    theCustomer.CreditInfoID = int.Parse(txtSubjectCIID.Text);
                }
                if (ddlstSubjectCity.SelectedValue.Trim().Length > 0) {
                    theAddress.CityID = int.Parse(ddlstSubjectCity.SelectedValue);
                }
                if (ddlstSubjectCountry.SelectedValue.Trim().Length > 0) {
                    theAddress.CountryID = int.Parse(ddlstSubjectCountry.SelectedValue);
                }
                theAddress.PostalCode = txtSubjectPostalCode.Text;
                theCustomer.Address.Add(theAddress);

                if (txtSubjectIDNumber.Text.Length > 0) {
                    var theID = new IDNumberBLLC
                                {
                                    Number = txtSubjectIDNumber.Text,
                                    NumberTypeID = int.Parse(ddlstSubjectIDNumberType.SelectedValue)
                                };

                    theCustomer.IDNumbers.Add(theID);
                }

                ArrayList theList;

                //Get the ID of the nationalID!
                string nationalID = CigConfig.Configure("lookupsettings.ROSNationalID").Trim();

                //Leita � b��i einstaklingagrunni og fyrirt�kjagrunni
                //B��i � �j��skr� og hj� LT
                var theFactory = new FactoryBLLC();
                if (rbtnSubjectCompany.Checked) {
                    //The customer to search for is a company

                    theList = nationalID == ddlstSubjectIDNumberType.SelectedValue.Trim() ? theFactory.FindCompanyInNationalAndCreditInfo((CompanyBLLC) theCustomer) : theFactory.FindCompanyAsArray((CompanyBLLC) theCustomer);
                } else {
                    //The customer to search for is an individual

                    theList = nationalID == ddlstSubjectIDNumberType.SelectedValue.Trim() ? theFactory.FindIndividualInNationalAndCreditInfo((IndividualBLLC) theCustomer) : theFactory.FindIndividualAsArray((IndividualBLLC) theCustomer);
                }

                if (theList == null) {
                    //An error was encountered while trying to retrieve data from the database
                    lblSubjectErrorMessage.Text = rm.GetString("txtDatabaseError", ci);
                    lblSubjectErrorMessage.Visible = true;
                } else {
                    if (theList.Count > 0) {
                        //If some customers were found display them
                        lblSubjectErrorMessage.Visible = false;
                        InsertIntoDatagrid(theList, false, rbtnSubjectCompany.Checked);

                        if (theList.Count < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll"))) {
                            divSubjects.Style.Remove("HEIGHT");
                        } else {
                            divSubjects.Style.Add("HEIGHT", "150px");
                        }

                        outerGridTableSubject.Visible = true;
                    } else {
                        //offer the user to create a new customer
                        lblSubjectErrorMessage.Text = rm.GetString("txtNoSubjectFound", ci);
                        lblSubjectErrorMessage.Visible = true;
                    }
                }
                btnFindSubject.Enabled = true;
                //this.btnFindSubject.Text = rm.GetString("txtSearchAgain",ci);
            } catch (Exception ex) {
                Logger.WriteToLog("Exception caught in btnFindSubject_Click: " + ex.Message, true);
                lblSubjectErrorMessage.Text = rm.GetString("txtExceptionCaught", ci);
                lblSubjectErrorMessage.Visible = true;
            }
        }

        private void InsertIntoSubjectInfo(CustomerBLLC theCustomer) {
            if (theCustomer == null) {
                return;
            }
            if (theCustomer.IsCompany) {
                rbtnSubjectCompany.Checked = true;
                rbtnSubjectIndividual.Checked = false;
                CheckSubjectRadioButtons();

                txtSubjectFirstNameNative.Text = EN ? ((CompanyBLLC) theCustomer).NameEN : ((CompanyBLLC) theCustomer).NameNative;
            } else {
                rbtnSubjectIndividual.Checked = true;
                rbtnSubjectCompany.Checked = false;
                CheckSubjectRadioButtons();

                if (EN) {
                    txtSubjectFirstNameNative.Text = ((IndividualBLLC) theCustomer).FirstNameEN;
                    txtSubjectFirstNameNative.Text = ((IndividualBLLC) theCustomer).LastNameEN;
                } else {
                    txtSubjectFirstNameNative.Text = ((IndividualBLLC) theCustomer).FirstNameNative;
                    txtSubjectLastNameNative.Text = ((IndividualBLLC) theCustomer).LastNameNative;
                }
            }

            if (theCustomer.IDNumbers.Count > 0) {
                var theID = (IDNumberBLLC) theCustomer.IDNumbers[0];
                txtSubjectIDNumber.Text = theID.Number;
                try {
                    ddlstSubjectIDNumberType.SelectedValue = "" + theID.NumberTypeID;
                } catch (Exception ex) {
                    Logger.WriteToLog(
                        "Caught an exception while trying to set ddlstSubjectIDNumberType.SelectedValue = " +
                        theID.NumberTypeID + " in InsertIntoSubjectInfo in class OrderCustomerSubjectSearch: " +
                        ex.Message,
                        true);
                }
            }

            if (theCustomer.Address.Count > 0) {
                var theAddress = (AddressBLLC) theCustomer.Address[0];

                //	this.txtSubjectAddressEN.Text = theAddress.StreetNative;
                txtSubjectAddressNative.Text = theAddress.StreetNative;
                try {
                    ddlstSubjectCity.SelectedValue = "" + theAddress.CityID;
                } catch (Exception ex) {
                    Logger.WriteToLog(
                        "Caught an exception while trying to set ddlstSubjectCity.SelectedValue = " +
                        theAddress.CityID + " in InsertIntoSubjectInfo in class OrderCustomerSubjectSearch: " +
                        ex.Message,
                        true);
                }
                try {
                    ddlstSubjectCountry.SelectedValue = "" + theAddress.CountryID;
                } catch (Exception ex) {
                    Logger.WriteToLog(
                        "Caught an exception while trying to set ddlstSubjectCountry.SelectedValue = " +
                        theAddress.CountryID + " in InsertIntoSubjectInfo in class OrderCustomerSubjectSearch: " +
                        ex.Message,
                        true);
                }
                txtSubjectPostalCode.Text = theAddress.PostalCode;
            }

            txtSubjectCIID.Text = "" + theCustomer.CreditInfoID;
        }

        private void btnCreateNewSubject_Click(object sender, EventArgs e) {
            btnCreateNewSubject.Visible = false;
            Session["State"] = "";
            Session["SubjectSelected"] = false;

            var theOrder = (OrderBLLC) Session["RosOrder"];

            AddressBLLC theAddress = null;

            if (txtSubjectAddressNative.Text.Trim().Length > 0 || ddlstSubjectCity.SelectedValue.Trim().Length > 0 ||
                ddlstSubjectCountry.SelectedValue.Trim().Length > 0 || txtSubjectPostalCode.Text.Trim().Length > 0) {
                theAddress = new AddressBLLC
                             {
                                 StreetEN = txtSubjectAddressNative.Text,
                                 StreetNative = txtSubjectAddressNative.Text
                             };
                if (ddlstSubjectCity.SelectedValue.Trim().Length > 0) {
                    theAddress.CityID = int.Parse(ddlstSubjectCity.SelectedValue);
                }
                if (ddlstSubjectCountry.SelectedValue.Trim().Length > 0) {
                    theAddress.CountryID = int.Parse(ddlstSubjectCountry.SelectedValue);
                }
                theAddress.PostalCode = txtSubjectPostalCode.Text;
            }

            IDNumberBLLC theID = null;
            if (txtSubjectIDNumber.Text.Length > 0) {
                theID = new IDNumberBLLC {Number = txtSubjectIDNumber.Text};
                try {
                    theID.NumberTypeID = int.Parse(ddlstSubjectIDNumberType.SelectedValue);
                } catch (Exception ex) {
                    Logger.WriteToLog(
                        "Caught an exception while trying to parse ddlstSubjectIDNumberType.SelectedValue in btnCreateNewSubject_Click in class OrderCustomerSubjectSearch: " +
                        ex.Message,
                        true);
                }
            }

            if (rbtnSubjectCompany.Checked) {
                //This is a new company
                var theCompany = new CompanyBLLC
                                 {
                                     IsCompany = true,
                                     NameEN = txtSubjectFirstNameNative.Text,
                                     NameNative = txtSubjectFirstNameNative.Text
                                 };
                theCompany.Address.Add(theAddress);
                if (theID != null) {
                    theCompany.IDNumbers.Add(theID);
                }

                theOrder.Subject = theCompany;

                Session["RosOrder"] = theOrder;
                Session["CustomerType"] = "Subject";

                Server.Transfer("NewCustomerC.aspx");
            } else {
                //This is an individual
                var theIndividual = new IndividualBLLC
                                    {
                                        IsCompany = false,
                                        FirstNameEN = txtSubjectFirstNameNative.Text,
                                        FirstNameNative = txtSubjectFirstNameNative.Text,
                                        LastNameEN = txtSubjectLastNameNative.Text,
                                        LastNameNative = txtSubjectLastNameNative.Text
                                    };
                if (theAddress != null) {
                    theIndividual.Address.Add(theAddress);
                }
                if (theID != null) {
                    theIndividual.IDNumbers.Add(theID);
                }

                theOrder.Subject = theIndividual;

                Session["RosOrder"] = theOrder;
                Session["CustomerType"] = "Subject";

                Server.Transfer("NewCustomer.aspx");
            }
        }

        private void dgSubject_ItemCommand(object source, DataGridCommandEventArgs e) {
            try {
                if (e.CommandName == "Select") {
                    //Put the selected customer into the order
                    CustomerBLLC theCustomer;
                    bool isCompany = true;

                    if (e.Item.Cells[19].Text != "") {
                        isCompany = bool.Parse(e.Item.Cells[19].Text);
                    }

                    int creditInfoID = -1;

                    try {
                        creditInfoID = int.Parse(e.Item.Cells[1].Text.Trim());
                    } catch {
                        creditInfoID = -1;
                    }

                    if (isCompany) {
                        theCustomer = new CompanyBLLC {IsCompany = true};
                        rbtnSubjectCompany.Checked = true;
                        rbtnSubjectIndividual.Checked = false;
                        CheckSubjectRadioButtons();

                        theCustomer.CreditInfoID = creditInfoID;

                        if (e.Item.Cells[4].Text != "&nbsp;") {
                            ((CompanyBLLC) theCustomer).NameEN = e.Item.Cells[4].Text.Trim();
                        }
                        if (e.Item.Cells[5].Text != "&nbsp;") {
                            ((CompanyBLLC) theCustomer).NameNative = e.Item.Cells[5].Text.Trim();
                        }
                    } else {
                        theCustomer = new IndividualBLLC {IsCompany = false};
                        rbtnSubjectIndividual.Checked = true;
                        rbtnSubjectCompany.Checked = false;
                        CheckSubjectRadioButtons();

                        theCustomer.CreditInfoID = creditInfoID;

                        if (theCustomer.CreditInfoID != -1) {
                            if (e.Item.Cells[6].Text != "&nbsp;") {
                                ((IndividualBLLC) theCustomer).FirstNameEN = e.Item.Cells[6].Text.Trim();
                            }
                            if (e.Item.Cells[7].Text != "&nbsp;") {
                                ((IndividualBLLC) theCustomer).FirstNameNative = e.Item.Cells[7].Text.Trim();
                            }
                            if (e.Item.Cells[8].Text != "&nbsp;") {
                                ((IndividualBLLC) theCustomer).LastNameEN = e.Item.Cells[8].Text.Trim();
                            }
                            if (e.Item.Cells[9].Text != "&nbsp;") {
                                ((IndividualBLLC) theCustomer).LastNameNative = e.Item.Cells[9].Text.Trim();
                            }
                        } else {
                            string name = e.Item.Cells[5].Text.Trim();
                            int index = name.LastIndexOf(" ");
                            string lastName = name.Substring(index + 1);
                            string firstName = name.Substring(0, index);

                            ((IndividualBLLC) theCustomer).FirstNameNative = firstName;
                            ((IndividualBLLC) theCustomer).LastNameNative = lastName;
                        }
                    }

                    //			if(e.Item.Cells[1].Text.Length > 0)
                    if (e.Item.Cells[2].Text != "&nbsp;") {
                        var theID = new IDNumberBLLC {Number = e.Item.Cells[2].Text.Trim()};
                        if (e.Item.Cells[3].Text.Length > 0) {
                            theID.NumberTypeID = int.Parse(e.Item.Cells[3].Text.Trim());
                        }
                        theCustomer.IDNumbers.Add(theID);
                    }

                    var theAddress = new AddressBLLC();

                    if (theCustomer.CreditInfoID != -1) {
                        if (e.Item.Cells[10].Text != "&nbsp;") {
                            theAddress.StreetEN = e.Item.Cells[10].Text.Trim();
                        }
                        if (e.Item.Cells[11].Text != "&nbsp;") {
                            theAddress.StreetNative = e.Item.Cells[11].Text.Trim();
                        }
                        if (e.Item.Cells[14].Text != "&nbsp;") {
                            theAddress.CityID = int.Parse(e.Item.Cells[14].Text.Trim());
                        }
                        if (e.Item.Cells[15].Text != "&nbsp;") {
                            theAddress.PostalCode = e.Item.Cells[15].Text.Trim();
                        }
                        if (e.Item.Cells[18].Text != "&nbsp;") {
                            theAddress.CountryID = int.Parse(e.Item.Cells[18].Text.Trim());
                        }
                    } else {
                        if (e.Item.Cells[11].Text != "&nbsp;") {
                            theAddress.StreetNative = e.Item.Cells[11].Text.Trim();
                        }
                        if (e.Item.Cells[15].Text != "&nbsp;") {
                            theAddress.PostalCode = e.Item.Cells[15].Text.Trim();
                        }
                    }
                    theCustomer.Address.Add(theAddress);

                    var theOrder = (OrderBLLC) Session["RosOrder"];
                    theOrder.Subject = theCustomer;
                    Session["RosOrder"] = theOrder;

                    if (theCustomer.CreditInfoID == -1) {
                        // The customer does not exist in our database
                        Session["CustomerType"] = "Subject";
                        if (isCompany) {
                            Server.Transfer("NewCustomerC.aspx");
                        } else {
                            Server.Transfer("NewCustomer.aspx");
                        }
                    } else {
                        if (EN) {
                            if (isCompany) {
                                if (e.Item.Cells[4].Text != "&nbsp;") {
                                    txtSubjectFirstNameNative.Text = e.Item.Cells[4].Text;
                                }
                            } else {
                                if (e.Item.Cells[6].Text != "&nbsp;") {
                                    txtSubjectFirstNameNative.Text = e.Item.Cells[6].Text;
                                }
                                if (e.Item.Cells[8].Text != "&nbsp;") {
                                    txtSubjectLastNameNative.Text = e.Item.Cells[8].Text;
                                }
                            }

                            if (e.Item.Cells[10].Text != "&nbsp;") {
                                txtSubjectAddressNative.Text = e.Item.Cells[10].Text;
                            }
                        } else {
                            if (isCompany) {
                                if (e.Item.Cells[5].Text != "&nbsp;") {
                                    txtSubjectFirstNameNative.Text = e.Item.Cells[5].Text;
                                }
                            } else {
                                if (e.Item.Cells[7].Text != "&nbsp;") {
                                    txtSubjectFirstNameNative.Text = e.Item.Cells[7].Text;
                                }
                                if (e.Item.Cells[9].Text != "&nbsp;") {
                                    txtSubjectLastNameNative.Text = e.Item.Cells[9].Text;
                                }
                            }

                            if (e.Item.Cells[11].Text != "&nbsp;") {
                                txtSubjectAddressNative.Text = e.Item.Cells[11].Text;
                            }
                        }
                        //Put the selected customer into the textfields

                        if (e.Item.Cells[14].Text != "&nbsp;") {
                            ddlstSubjectCity.SelectedValue = e.Item.Cells[14].Text;
                        }
                        //					if(e.Item.Cells[18].Text != "&nbsp;")
                        //						this.ddlstSubjectCountry.SelectedValue = e.Item.Cells[18].Text;
                        if (e.Item.Cells[15].Text != "&nbsp;") {
                            txtSubjectPostalCode.Text = e.Item.Cells[15].Text;
                        }
                        if (e.Item.Cells[3].Text != "&nbsp;") {
                            ddlstSubjectIDNumberType.SelectedValue = e.Item.Cells[3].Text;
                        }
                        if (e.Item.Cells[2].Text != "&nbsp;") {
                            txtSubjectIDNumber.Text = e.Item.Cells[2].Text;
                        }
                        if (e.Item.Cells[1].Text != "&nbsp;") {
                            txtSubjectCIID.Text = e.Item.Cells[1].Text;
                        }

                        Session["SubjectSelected"] = true;
                    }

                    outerGridTableSubject.Visible = false;
                    ShowNextButton();
                }
            } catch (Exception ex) {
                Logger.WriteToLog("Exception caught in dgSubject_ItemCommand: " + ex.Message, true);
                lblSubjectErrorMessage.Text = rm.GetString("txtExceptionCaught", ci);
                lblSubjectErrorMessage.Visible = true;
            }
        }

        #endregion
    }
}