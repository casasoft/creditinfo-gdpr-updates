﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logging.BLL;
using ROS.BLL;
using ROS.Localization;

using Cig.Framework.Base.Configuration;

namespace cb3.ROS
{
    public partial class OrderForm : System.Web.UI.Page
    {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly FactoryBLLC _theFactory = new FactoryBLLC();
        private bool EN;

        private void Page_Load(object sender, EventArgs e)
        {
            Page.ID = CigConfig.Configure("lookupsettings.ROSPageIDOrderForm");

            try
            {
                string culture = Thread.CurrentThread.CurrentCulture.Name;
                rm = CIResource.CurrentManager;
                ci = Thread.CurrentThread.CurrentCulture;
                if (culture.Equals("en-US"))
                {
                    EN = true;
                }
                // check the current culture
                string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
                lblErrorMessage1.Visible = false;

                if (!IsPostBack)
                {
                    txtClientReferenceNumber.Visible = false;
                    lblClientReferenceNumber.Visible = false;
                    lblCustomerCountryNative.Visible = false;
                    lblSubjectCountryNative.Visible = false;
                    ddlstCustomerCountry.Visible = false;
                    ddlstSubjectCountry.Visible = false;
                    lblCustomerCity.Visible = false;
                    ddlstCustomerCity.Visible = false;
                    lblSubjectCity.Visible = false;
                    ddlstSubjectCity.Visible = false;

                    bool showComment = false;
                    try
                    {
                        showComment = bool.Parse(CigConfig.Configure("lookupsettings.allowReportOrderComment"));
                    }
                    catch
                    {
                        showComment = false;
                    }
                    lblOrderComment.Visible = showComment;
                    txtOrderComment.Visible = showComment;

                    InitDDBoxes();
                    LocalizeText();

                    var theOrder = new OrderBLLC();

                    var error = false;

                    var state = (string)Session["State"];
                    if (state == "Update")
                    {
                        if (Request.QueryString["order"] == null)
                        {
                            //Query string is empty
                            error = true;
                        }
                        else
                        {
                            //Get the order from the DB
                            theOrder = _theFactory.GetOrder(Request.QueryString["order"]);

                            if (theOrder == null)
                            {
                                lblErrorMessage1.Text = rm.GetString("txtDatabaseError", ci);
                                lblErrorMessage1.Visible = true;
                                error = true;
                            }
                        }

                        //Breyta hnappi
                        btnSubmitOrder.Text = rm.GetString("txtUpdateOrder", ci);
                        btnDelete.Visible = true;

                        if (theOrder != null)
                        {
                            btnDelivered.Visible = !theOrder.Delivered;
                        }
                    }
                    else
                    {
                        btnDelivered.Visible = false;
                        btnDelete.Visible = false;

                        try
                        {
                            //Get the order from the Session
                            theOrder = (OrderBLLC)Session["RosOrder"];

                            theOrder.OrderDate = DateTime.Today;
                            theOrder.DispatchDeadline = DateTime.Today;
                            theOrder.DeliverySpeedID = 1;
                            theOrder.OrderTypeID = 1; //int.Parse(this.ddlstOrderType.SelectedValue);
                            //					theOrder.Product.ID = 1;
                        }
                        catch
                        {
                            lblErrorMessage1.Text = rm.GetString("txtErrorGettingOrderFromSession", ci);
                            lblErrorMessage1.Visible = true;
                            Logger.WriteToLog(
                                "Caught an exception in Page_Load in class OrderForm when trying to parse ddlstOrderType.SelectedValue. Value is: " +
                                ddlstOrderType.SelectedValue,
                                true);
                            error = true;
                        }
                    }

                    if (!error)
                    {
                        InitPage(theOrder);
                    }
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage1.Text = rm.GetString("txtExceptionCaught", ci);
                lblErrorMessage1.Visible = true;
                Logger.WriteToLog("Caught an exception in Page_Load in class OrderForm: " + ex.Message, true);
            }
        }

        private void InitPage(OrderBLLC theOrder)
        {
            try
            {
                if (theOrder.Product.ID > -1)
                {
                    ddlProducts.SelectedValue = "" + theOrder.Product.ID;
                }
            }
            catch (Exception) { }

            //Setja dagsetninguna í order date
            txtOrderDate.Text = theOrder.OrderDate.ToShortDateString();

            //Setja dagsetninguna í dispatch deadline
            txtDispatchDeadline.Text = theOrder.DispatchDeadline.ToShortDateString();

            var state = (string)Session["State"];
            if (state != "Update")
            {
                UpdateDispatchDeadline();
            }

            //Setja delivery speed
            if (CigConfig.Configure("lookupsettings.ROSorderFiscalUpdateID") != null)
            {
                if (theOrder.DeliverySpeedID !=
                    Convert.ToInt32(CigConfig.Configure("lookupsettings.ROSorderFiscalUpdateID")))
                {
                    ddlDeliverySpeed.SelectedIndex = theOrder.DeliverySpeedID - 1;
                }
            }
            else
            {
                ddlDeliverySpeed.SelectedIndex = theOrder.DeliverySpeedID - 1;
            }

            //Setja order type
            ddlstOrderType.SelectedValue = "" + theOrder.OrderTypeID;

            txtOrderComment.Text = theOrder.Comment;
            txtRemarksEN.Text = theOrder.RemarksEN;
            txtRemarksNative.Text = theOrder.RemarksNative;
            txtClientReferenceNumber.Text = theOrder.CustomerReferenceNumber;

            InsertIntoCustomerInfo(theOrder);
            InsertIntoSubjectInfo(theOrder);
        }

        private void UpdateDispatchDeadline()
        {
            string selected = ddlDeliverySpeed.SelectedValue;
            string daysToDispatch = selected.Substring(selected.LastIndexOf("=") + 1);

            DateTime dispatchDate;
            try
            {
                dispatchDate = DateTime.Parse(txtOrderDate.Text);
            }
            catch
            {
                dispatchDate = DateTime.Today;
            }

            int days = int.Parse(daysToDispatch);

            txtDispatchDeadline.Text = dispatchDate.AddDays(days).ToShortDateString();
        }

        private void InsertIntoCustomerInfo(OrderBLLC theOrder)
        {
            //Insert the information about the customer into the text fields
            if (theOrder.Customer.Address.Count > 0)
            {
                var theAddress = (AddressBLLC)theOrder.Customer.Address[0];

                //				this.ddlstCustomerCountry.SelectedValue = "" + theAddress.CountryID;
                txtCustomerAddressEN.Text = theAddress.StreetEN;
                txtCustomerAddressNative.Text = theAddress.StreetNative;
                if (theAddress.CityID != -1)
                {
                    try
                    {
                        ddlstCustomerCity.SelectedValue = "" + theAddress.CityID;
                    }
                    catch
                    {
                        Logger.WriteToLog(
                            "Caught an exception when trying to set ddlstCustomerCity.SelectedValue = " +
                            theAddress.CityID + " in InsertIntoCustomerInfo in class OrderForm",
                            true);
                    }
                }
                if (theAddress.CountryID != -1)
                {
                    try
                    {
                        ddlstCustomerCountry.SelectedValue = "" + theAddress.CountryID;
                    }
                    catch
                    {
                        Logger.WriteToLog(
                            "Caught an exception when trying to set ddlstCustomerCountry.SelectedValue = " +
                            theAddress.CountryID + " in InsertIntoCustomerInfo in class OrderForm",
                            true);
                    }
                }
                txtCustomerPostalCode.Text = theAddress.PostalCode;
            }

            txtCustomerCIIDNumber.Text = "" + theOrder.Customer.CreditInfoID;

            if (theOrder.Customer.IsCompany)
            {
                txtCustomerNameEN.Text = ((CompanyBLLC)theOrder.Customer).NameEN;
                txtCustomerNameNative.Text = ((CompanyBLLC)theOrder.Customer).NameNative;

                lblCustomerFirstNameEN.Visible = false;
                lblCustomerFirstNameNative.Visible = false;
                lblCustomerLastNameEN.Visible = false;
                lblCustomerLastNameNative.Visible = false;
                txtCustomerFirstNameEN.Visible = false;
                txtCustomerFirstNameNative.Visible = false;
                txtCustomerLastNameEN.Visible = false;
                txtCustomerLastNameNative.Visible = false;
            }
            else
            {
                txtCustomerFirstNameEN.Text = ((IndividualBLLC)theOrder.Customer).FirstNameEN;
                txtCustomerFirstNameNative.Text = ((IndividualBLLC)theOrder.Customer).FirstNameNative;
                txtCustomerLastNameEN.Text = ((IndividualBLLC)theOrder.Customer).LastNameEN;
                txtCustomerLastNameNative.Text = ((IndividualBLLC)theOrder.Customer).LastNameNative;

                lblCustomerNameEN.Visible = false;
                lblCustomerNameNative.Visible = false;
                txtCustomerNameEN.Visible = false;
                txtCustomerNameNative.Visible = false;
            }

            if (theOrder.Customer.IDNumbers.Count <= 0)
            {
                return;
            }
            var theID = (IDNumberBLLC)theOrder.Customer.IDNumbers[0];
            txtCustomerIDNumber.Text = theID.Number;
        }

        private void InsertIntoSubjectInfo(OrderBLLC theOrder)
        {
            //Insert the information about the subject into the text fields
            if (theOrder.Subject.Address.Count > 0)
            {
                var theAddress = (AddressBLLC)theOrder.Subject.Address[0];

                //				this.ddlstSubjectCountry.SelectedValue = theAddress.CountryID;
                txtSubjectAddressEN.Text = theAddress.StreetEN;
                txtSubjectAddressNative.Text = theAddress.StreetNative;
                if (theAddress.CityID != -1)
                {
                    try
                    {
                        ddlstSubjectCity.SelectedValue = "" + theAddress.CityID;
                    }
                    catch
                    {
                        Logger.WriteToLog(
                            "Caught an exception when trying to set ddlstSubjectCity.SelectedValue = " +
                            theAddress.CityID + " in InsertIntoSubjectInfo in class OrderForm",
                            true);
                    }
                }
                if (theAddress.CountryID != -1)
                {
                    try
                    {
                        ddlstSubjectCountry.SelectedValue = "" + theAddress.CountryID;
                    }
                    catch
                    {
                        Logger.WriteToLog(
                            "Caught an exception when trying to set ddlstSubjectCountry.SelectedValue = " +
                            theAddress.CountryID + " in InsertIntoSubjectInfo in class OrderForm",
                            true);
                    }
                }
                txtSubjectPostalCode.Text = theAddress.PostalCode;
            }

            txtSubjectCIIDNumber.Text = "" + theOrder.Subject.CreditInfoID;

            if (theOrder.Subject.IsCompany)
            {
                txtSubjectNameEN.Text = ((CompanyBLLC)theOrder.Subject).NameEN;
                txtSubjectNameNative.Text = ((CompanyBLLC)theOrder.Subject).NameNative;

                lblSubjectFirstNameEN.Visible = false;
                lblSubjectFirstNameNative.Visible = false;
                lblSubjectLastNameEN.Visible = false;
                lblSubjectLastNameNative.Visible = false;
                txtSubjectFirstNameEN.Visible = false;
                txtSubjectFirstNameNative.Visible = false;
                txtSubjectLastNameEN.Visible = false;
                txtSubjectLastNameNative.Visible = false;
            }
            else
            {
                txtSubjectFirstNameEN.Text = ((IndividualBLLC)theOrder.Subject).FirstNameEN;
                txtSubjectFirstNameNative.Text = ((IndividualBLLC)theOrder.Subject).FirstNameNative;
                txtSubjectLastNameEN.Text = ((IndividualBLLC)theOrder.Subject).LastNameEN;
                txtSubjectLastNameNative.Text = ((IndividualBLLC)theOrder.Subject).LastNameNative;

                lblSubjectNameEN.Visible = false;
                lblSubjectNameNative.Visible = false;
                txtSubjectNameEN.Visible = false;
                txtSubjectNameNative.Visible = false;
            }

            if (theOrder.Subject.IDNumbers.Count <= 0)
            {
                return;
            }
            var theID = (IDNumberBLLC)theOrder.Subject.IDNumbers[0];
            txtSubjectIDNumber.Text = theID.Number;
        }

        private void btnSubmitOrder_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) { }
            else
            {
                try
                {
                    //Insert the dates into the order
                    var theOrder = new OrderBLLC();
                    if (Session["RosOrder"] != null)
                    {
                        theOrder = (OrderBLLC)Session["RosOrder"];
                    }
                    theOrder.Product.ID = int.Parse(ddlProducts.SelectedValue);
                    theOrder.OrderDate = DateTime.Parse(txtOrderDate.Text);
                    theOrder.DispatchDeadline = DateTime.Parse(txtDispatchDeadline.Text);
                    theOrder.RemarksEN = txtRemarksEN.Text;
                    theOrder.RemarksNative = txtRemarksNative.Text;
                    theOrder.CustomerReferenceNumber = txtClientReferenceNumber.Text;
                    theOrder.OrderTypeID = int.Parse(ddlstOrderType.SelectedValue);

                    string deliveryID = ddlDeliverySpeed.SelectedValue.Substring(
                        0, ddlDeliverySpeed.SelectedValue.IndexOf("T"));
                    theOrder.DeliverySpeedID = int.Parse(deliveryID.Substring(deliveryID.IndexOf("=", 0) + 1));
                    //ID=1TIME=7

                    theOrder.RegisteredByID = Convert.ToInt32(Session["UserLoginID"]);

                    var state = (string)Session["State"];
                    if (state == "Update")
                    {
                        //Update the order
                        if (Request.QueryString["order"] != null)
                        {
                            theOrder.OrderID = int.Parse(Request.QueryString["order"].Trim());
                            if (_theFactory.UpdateOrder(theOrder))
                            {
                                try
                                {
                                    //Send the user to another page
                                    if (Session["LastPage"] != null)
                                    {
                                        string lastPage = ((string)Session["LastPage"]).Trim();

                                        if (lastPage == "UndeliveredOrdersForm")
                                        {
                                            Server.Transfer("UndeliveredOrdersForm.aspx");
                                        }
                                        if (lastPage == "OrderSurveyForm")
                                        {
                                            Server.Transfer("OrderSurveyForm.aspx");
                                        }
                                    }
                                    else
                                    {
                                        //Send somewhere else
                                        Server.Transfer("OrderCustomerSubjectSearch.aspx");
                                    }
                                }
                                catch (ThreadAbortException) { }
                            }
                            else
                            {
                                lblErrorMessage1.Text = rm.GetString("txtErrorUpdatingOrder", ci);
                                lblErrorMessage1.Visible = true;
                            }
                        }
                        else
                        {
                            lblErrorMessage1.Text = rm.GetString("txtErrorRetrievingOrder", ci);
                            lblErrorMessage1.Visible = true;
                        }
                    }
                    else
                    {
                        //the order is ready to be sent to the database
                        if (_theFactory.CreateReportOrder(theOrder))
                        {
                            //The order was saved successfully
                            var newOrder = new OrderBLLC();
                            Session["RosOrder"] = newOrder;
                            Session["CustomerSelected"] = false;
                            Session["SubjectSelected"] = false;
                            try
                            {
                                Server.Transfer("UndeliveredOrdersForm.aspx");
                            }
                            catch (ThreadAbortException) { }
                        }
                        else
                        {
                            //Could not save the order
                            lblErrorMessage1.Text = rm.GetString("txtErrorCreatingOrder", ci);
                            lblErrorMessage1.Visible = true;
                        }
                    }
                    //				}
                }
                catch (Exception ex)
                {
                    Logger.WriteToLog(
                        "Caught an exception in btnSubmitOrder_Click in class OrderForm: " + ex.Message, true);
                    lblErrorMessage1.Text = rm.GetString("txtExceptionCaught", ci);
                    lblErrorMessage1.Visible = true;
                }
            }
        }

        private void ddlDeliverySpeed_SelectedIndexChanged(object sender, EventArgs e) { UpdateDispatchDeadline(); }

        private void InitDDBoxes()
        {
            var theFactory = new FactoryBLLC();
            //BindCountry(theFactory);
            BindDeliverySpeed(theFactory);
            BindOrderType(theFactory);
            BindProducts(theFactory);
            //BindCity(theFactory);
        }

        private void BindProducts(FactoryBLLC theFactory)
        {
            var productSet = theFactory.GetAllProducts();
            ddlProducts.DataSource = productSet;
            ddlProducts.DataTextField = EN ? "NameEN" : "NameNative";
            ddlProducts.DataValueField = "ID";
            ddlProducts.DataBind();
        }

        private void BindDeliverySpeed(FactoryBLLC theFactory)
        {
            //Get the delivery speed from the database
            var theSpeeds = theFactory.GetDeliverySpeeds();

            foreach (DataRow row in theSpeeds.Tables[0].Rows)
            {
                var theItem = new ListItem
                {
                    Value = ("ID=" + row["ID"] + "TIME=" + row["DaysCount"]),
                    Text =
                        (EN ? row["DeliverySpeedEN"].ToString() : row["DeliverySpeedNative"].ToString())
                };

                //Setja í ddlDeliverySpeed
                ddlDeliverySpeed.Items.Add(theItem);
            }
        }

        private void BindOrderType(FactoryBLLC theFactory)
        {
            var orderTypeSet = theFactory.GetOrderTypes();
            ddlstOrderType.DataSource = orderTypeSet;
            ddlstOrderType.DataTextField = EN ? "TypeEN" : "TypeNative";
            ddlstOrderType.DataValueField = "ID";
            ddlstOrderType.DataBind();
        }

        private void ddlstSubjectCountry_SelectedIndexChanged(object sender, EventArgs e) { }

        private void LocalizeText()
        {
            lblHeader.Text = rm.GetString("txtOrder", ci);
            lblProduct.Text = rm.GetString("txtProducts", ci);
            lblOrderDate.Text = rm.GetString("txtOrderDate", ci);
            lblDispatchDeadline.Text = rm.GetString("txtDispatchDeadline", ci);
            lblDeliverySpeed.Text = rm.GetString("txtDeliverySpeed", ci);
            lblOrderType.Text = rm.GetString("txtOrderType", ci);

            RequiredFieldValidatorOrderDate.ErrorMessage = rm.GetString("ErrorOrderDateMissing", ci);
            RequiredFieldValidatorDispatchDeadline.ErrorMessage = rm.GetString("ErrorDispatchDeadlineMissing", ci);
            CompareValidatorDateMismatch.ErrorMessage = rm.GetString("txtOrderDateMismatch", ci);
            CustomValidatorOrderDateFormat.ErrorMessage = rm.GetString("ErrorOrderDateFormat");

            lblRemarksNative.Text = rm.GetString("txtRemarksNative", ci);
            lblRemarksEN.Text = rm.GetString("txtRemarksEN", ci);
            lblClientReferenceNumber.Text = rm.GetString("txtReferenceNumber", ci);

            lblOrderComment.Text = rm.GetString("txtOrderComment", ci);

            // Customer Info	
            lblCustomer.Text = rm.GetString("txtCustomer", ci);
            lblCustomerIDNumber.Text = rm.GetString("txtIDNumber", ci);
            lblCustomerCIID.Text = rm.GetString("txtCIID", ci);
            lblCustomerNameNative.Text = rm.GetString("txtNameNative", ci);
            lblCustomerNameEN.Text = rm.GetString("txtNameEN", ci);
            lblCustomerFirstNameNative.Text = rm.GetString("txtFirstNameNative", ci);
            lblCustomerFirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lblCustomerLastNameNative.Text = rm.GetString("txtLastNameNative", ci);
            lblCustomerLastNameEN.Text = rm.GetString("txtLastNameEN", ci);
            lblCustomerCity.Text = rm.GetString("txtCity", ci);
            lblCustomerCountryNative.Text = rm.GetString("txtCountry", ci);
            lblCustomerAddressNative.Text = rm.GetString("txtAddressNative", ci);
            lblCustomerAddressEN.Text = rm.GetString("txtAddressEN", ci);
            lblCustomerPostalCode.Text = rm.GetString("txtPostalCode", ci);
            // Subject Info
            lblSubject.Text = rm.GetString("txtSubject", ci);
            lblSubjectIDNumber.Text = rm.GetString("txtIDNumber", ci);
            lblSubjectCIID.Text = rm.GetString("txtCIID", ci);
            lblSubjectNameNative.Text = rm.GetString("txtNameNative", ci);
            lblSubjectNameEN.Text = rm.GetString("txtNameEN", ci);
            lblSubjectFirstNameNative.Text = rm.GetString("txtFirstNameNative", ci);
            lblSubjectFirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lblSubjectLastNameNative.Text = rm.GetString("txtLastNameNative", ci);
            lblSubjectLastNameEN.Text = rm.GetString("txtLastNameEN", ci);
            lblSubjectCity.Text = rm.GetString("txtCity", ci);
            lblSubjectCountryNative.Text = rm.GetString("txtCountry", ci);
            lblSubjectAddressNative.Text = rm.GetString("txtAddressNative", ci);
            lblSubjectAddressEN.Text = rm.GetString("txtAddressEN", ci);
            lblSubjectPostalCode.Text = rm.GetString("txtPostalCode", ci);

            btnSubmitOrder.Text = rm.GetString("txtSubmitOrder", ci);
            btnDelete.Text = rm.GetString("txtDeleteOrder", ci);
            btnDelivered.Text = rm.GetString("txtDelivered", ci);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            lblErrorMessage1.Visible = false;

            try
            {
                if (Request.QueryString["order"] != null)
                {
                    if (_theFactory.DeleteOrder(Request.QueryString["order"].Trim()))
                    {
                        try
                        {
                            if (Session["LastPage"] != null)
                            {
                                string lastPage = ((string)Session["LastPage"]).Trim();

                                if (lastPage == "UndeliveredOrdersForm")
                                {
                                    Server.Transfer("UndeliveredOrdersForm.aspx");
                                }
                                if (lastPage == "OrderSurvey")
                                {
                                    Server.Transfer("OrderSurveyForm.aspx");
                                }
                                else
                                {
                                    Server.Transfer("OrderCustomerSubjectSearch.aspx");
                                }
                            }
                            else
                            {
                                //Send somewhere else
                                Server.Transfer("OrderCustomerSubjectSearch.aspx");
                            }
                        }
                        catch (ThreadAbortException) { }
                    }
                    else
                    {
                        throw new Exception("_theFactory.DeleteOrder returned -1");
                    }
                }
                else
                {
                    throw new Exception("QueryString order is null");
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage1.Text = rm.GetString("txtErrorDeletingOrder", ci);
                lblErrorMessage1.Visible = true;
                Logger.WriteToLog("Caught an exception in btnDelete_Click in class OrderForm: " + ex.Message, true);
            }
        }

        private void btnDelivered_Click(object sender, EventArgs e)
        {
            lblErrorMessage1.Visible = false;

            try
            {
                var theOrder = new OrderBLLC();
                theOrder.OrderDate = DateTime.Parse(txtOrderDate.Text.Trim());
                theOrder.DispatchDeadline = DateTime.Parse(txtDispatchDeadline.Text.Trim());

                theOrder.Product.ID = int.Parse(ddlProducts.SelectedValue);

                string deliveryID = ddlDeliverySpeed.SelectedValue.Substring(
                    0, ddlDeliverySpeed.SelectedValue.IndexOf("T"));
                theOrder.DeliverySpeedID = int.Parse(deliveryID.Substring(deliveryID.IndexOf("=", 0) + 1));
                //ID=1TIME=7

                theOrder.OrderTypeID = int.Parse(ddlstOrderType.SelectedValue.Trim());
                theOrder.RemarksNative = txtRemarksNative.Text;
                theOrder.RemarksEN = txtRemarksEN.Text;
                theOrder.CustomerReferenceNumber = txtClientReferenceNumber.Text;
                theOrder.Customer = new CustomerBLLC
                {
                    CreditInfoID = int.Parse(txtCustomerCIIDNumber.Text.Trim())
                };

                int registeredByID = Convert.ToInt32(Session["UserLoginID"]);
                string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

                if (Request.QueryString["order"] != null)
                {
                    theOrder.OrderID = int.Parse(Request.QueryString["order"].Trim());

                    int success = -1;
                    success = _theFactory.OrderIsDelivered(theOrder, ipAddress, registeredByID);
                    if (success != -1)
                    {
                        try
                        {
                            if (Session["LastPage"] != null)
                            {
                                string lastPage = ((string)Session["LastPage"]).Trim();

                                Server.Transfer(lastPage + ".aspx");
                            }
                            else
                            {
                                //Send somewhere else
                                Server.Transfer("OrderCustomerSubjectSearch.aspx");
                            }
                        }
                        catch (ThreadAbortException) { }
                    }
                    else
                    {
                        throw new Exception("_theFactory.OrderIsDelivered returned -1");
                    }
                }
                else
                {
                    throw new Exception("QueryString order is null");
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage1.Text = rm.GetString("txtErrorDeliveringOrder", ci);
                lblErrorMessage1.Visible = true;
                Logger.WriteToLog("Caught an exception in btnDelivered_Click in class OrderForm: " + ex.Message, true);
            }
        }

        private void CustomValidatorOrderDateFormat_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value != "")
            {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try
                {
                    DateTime.Parse(args.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    args.IsValid = true;
                }
                catch (Exception)
                {
                    args.IsValid = false;
                }
            }
            else
            {
                args.IsValid = true;
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CustomValidatorOrderDateFormat.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(
                    this.CustomValidatorOrderDateFormat_ServerValidate);
            this.ddlDeliverySpeed.SelectedIndexChanged +=
                new System.EventHandler(this.ddlDeliverySpeed_SelectedIndexChanged);
            this.ddlstSubjectCountry.SelectedIndexChanged +=
                new System.EventHandler(this.ddlstSubjectCountry_SelectedIndexChanged);
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.btnDelivered.Click += new System.EventHandler(this.btnDelivered_Click);
            this.btnSubmitOrder.Click += new System.EventHandler(this.btnSubmitOrder_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}