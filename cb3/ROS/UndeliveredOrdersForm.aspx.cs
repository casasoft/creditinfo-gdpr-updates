using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using ROS.BLL;
using ROS.Localization;

using Cig.Framework.Base.Configuration;


namespace ROS {
    /// <summary>
    /// Summary description for UndeliveredOrdersForm.
    /// </summary>
    public class UndeliveredOrdersForm : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private bool EN;
        protected Label lblErrorMessage;
        protected PlaceHolder myPlaceHolder;
        // Localization
        protected Repeater Repeater1;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = CigConfig.Configure("lookupsettings.ROSPageIDUndeliveredOrders");

            // check the current culture
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (culture.Equals("en-US")) {
                EN = true;
            }

            lblErrorMessage.Visible = false;

            //		if(!IsPostBack)
            //		{
            Session.Remove("RosOrder");
            Session["LastPage"] = "UndeliveredOrdersForm";

            var theFactory = new FactoryBLLC();

            var theOrderList = theFactory.GetUndeliveredOrders();
            if (theOrderList == null) {
                //Error in the database
                lblErrorMessage.Text = rm.GetString("txtDatabaseError", ci);
                lblErrorMessage.Visible = true;
            } else {
                InsertIntoDataGrids(theOrderList);
            }
            //		}
            LocalizeText();
        }

        private static void LocalizeText() { }

        private void InsertIntoDataGrids(ArrayList theOrderList) {
            var theTable = new DataTable();
            theTable.Columns.Add("Date");
            theTable.Columns.Add("CustomerNameEN");
            theTable.Columns.Add("CustomerNameNative");
            theTable.Columns.Add("CustomerID");
            theTable.Columns.Add("CustomerIsCompany");
            theTable.Columns.Add("SubjectNameEN");
            theTable.Columns.Add("SubjectNameNative");
            theTable.Columns.Add("SubjectID");
            theTable.Columns.Add("SubjectIsCompany");
            theTable.Columns.Add("DispatchDeadline");
            theTable.Columns.Add("RemarksEN");
            theTable.Columns.Add("RemarksNative");
            theTable.Columns.Add("OrderID");
            theTable.Columns.Add("CustomerRefNo");
            theTable.Columns.Add("DeliverySpeedID");
            theTable.Columns.Add("ProductEN");
            theTable.Columns.Add("ProductNative");

            foreach (OrderBLLC anOrder in theOrderList) {
                var aRow = theTable.NewRow();
                aRow[0] = anOrder.OrderDate.ToShortDateString();

                string type = CigConfig.Configure("lookupsettings.individualID");
                if (anOrder.Customer.Type == type) {
                    aRow[4] = false;

                    if (((IndividualBLLC) anOrder.Customer).FirstNameEN.Length > 0 ||
                        ((IndividualBLLC) anOrder.Customer).LastNameEN.Length > 0) {
                        aRow[1] = ((IndividualBLLC) anOrder.Customer).FirstNameEN + " " +
                                  ((IndividualBLLC) anOrder.Customer).LastNameEN;
                    } else {
                        aRow[1] = ((IndividualBLLC) anOrder.Customer).FirstNameNative + " " +
                                  ((IndividualBLLC) anOrder.Customer).LastNameNative;
                    }

                    if (((IndividualBLLC) anOrder.Customer).FirstNameNative.Length > 0 ||
                        ((IndividualBLLC) anOrder.Customer).LastNameNative.Length > 0) {
                        aRow[2] = ((IndividualBLLC) anOrder.Customer).FirstNameNative + " " +
                                  ((IndividualBLLC) anOrder.Customer).LastNameNative;
                    } else {
                        aRow[2] = ((IndividualBLLC) anOrder.Customer).FirstNameEN + " " +
                                  ((IndividualBLLC) anOrder.Customer).LastNameEN;
                    }
                } else {
                    aRow[4] = true;

                    aRow[1] = ((CompanyBLLC) anOrder.Customer).NameEN.Length > 0 ? ((CompanyBLLC) anOrder.Customer).NameEN : ((CompanyBLLC) anOrder.Customer).NameNative;

                    aRow[2] = ((CompanyBLLC) anOrder.Customer).NameNative.Length > 0 ? ((CompanyBLLC) anOrder.Customer).NameNative : ((CompanyBLLC) anOrder.Customer).NameEN;
                }

                aRow[3] = anOrder.Customer.CreditInfoID;

                if (anOrder.Subject.Type == type) {
                    aRow[8] = false;

                    if (((IndividualBLLC) anOrder.Subject).FirstNameEN.Length > 0 ||
                        ((IndividualBLLC) anOrder.Subject).LastNameEN.Length > 0) {
                        aRow[5] = ((IndividualBLLC) anOrder.Subject).FirstNameEN + " " +
                                  ((IndividualBLLC) anOrder.Subject).LastNameEN;
                    } else {
                        aRow[5] = ((IndividualBLLC) anOrder.Subject).FirstNameNative + " " +
                                  ((IndividualBLLC) anOrder.Subject).LastNameNative;
                    }

                    if (((IndividualBLLC) anOrder.Subject).FirstNameNative.Length > 0 ||
                        ((IndividualBLLC) anOrder.Subject).LastNameNative.Length > 0) {
                        aRow[6] = ((IndividualBLLC) anOrder.Subject).FirstNameNative + " " +
                                  ((IndividualBLLC) anOrder.Subject).LastNameNative;
                    } else {
                        aRow[6] = ((IndividualBLLC) anOrder.Subject).FirstNameEN + " " +
                                  ((IndividualBLLC) anOrder.Subject).LastNameEN;
                    }
                } else {
                    aRow[8] = true;

                    aRow[5] = ((CompanyBLLC) anOrder.Subject).NameEN.Length > 0 ? ((CompanyBLLC) anOrder.Subject).NameEN : ((CompanyBLLC) anOrder.Subject).NameNative;

                    aRow[6] = ((CompanyBLLC) anOrder.Subject).NameNative.Length > 0 ? ((CompanyBLLC) anOrder.Subject).NameNative : ((CompanyBLLC) anOrder.Subject).NameEN;
                }

                aRow[7] = anOrder.Subject.CreditInfoID;
                aRow[9] = anOrder.DispatchDeadline.ToShortDateString();
                aRow[10] = anOrder.RemarksEN;
                aRow[11] = anOrder.RemarksNative;
                aRow[12] = anOrder.OrderID;
                aRow[13] = anOrder.CustomerReferenceNumber;
                aRow[14] = anOrder.DeliverySpeedID;
                aRow[15] = anOrder.Product.NameEN;
                aRow[16] = anOrder.Product.NameNative;

                theTable.Rows.Add(aRow);
            }

/*
			DataView normalView = new DataView(theTable);
			normalView.RowFilter = "DeliverySpeedID = 1";
			this.dgNormal.DataSource = normalView;
			this.dgNormal.DataBind();

			DataView expressView = new DataView(theTable);
			expressView.RowFilter = "DeliverySpeedID = 2";
			this.dgExpress.DataSource = expressView;
			this.dgExpress.DataBind();

			DataView superExpressView = new DataView(theTable);
			superExpressView.RowFilter = "DeliverySpeedID = 3";
			this.dgSuperExpress.DataSource = superExpressView;
			this.dgSuperExpress.DataBind();
*/

            var myFactory = new FactoryBLLC();
            var myDeliverySpeeds = myFactory.GetDeliverySpeedsAsArrayList();

            foreach (DeliverySpeedBLLC mySpeed in myDeliverySpeeds) {
                Control uc = null;

                var myView = new DataView(theTable);
/*				myView.RowFilter = "DeliverySpeedID = " + mySpeed.ID;
*/
                Session.Add("RosDataView", myView);
                Session.Add("RosDeliverySpeeds", myDeliverySpeeds);

                uc = LoadControl("user_controls/OrderDatagrid.ascx");
                uc.ID = "UndeliveredOrders" + mySpeed.ID;

                if (uc != null) {
                    myPlaceHolder.Controls.Add(uc);
                }
            }

//			Session.Remove("RosDataView");
//			Session.Remove("RosDeliverySpeed");

            ///			DataView myView = new DataView(theTable);
//			OrderDatagrid myDatagrid = new OrderDatagrid();
//			myDatagrid.SetDataSource(myView);
//			myDatagrid.BindTheData();

//			this.myPlaceHolder.Controls.Add(this.lblExpress);
//			this.myPlaceHolder.Controls.Add(myDatagrid);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}