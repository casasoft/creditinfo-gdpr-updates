using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using ROS.BLL;
using ROS.Localization;

using Cig.Framework.Base.Configuration;

namespace ROS {
    /// <summary>
    ///		Summary description for OrderDatagrid.
    /// </summary>
    public class OrderDatagrid : UserControl {
        // Localization
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected DataGrid dgOrders;
        protected HtmlGenericControl divOrders;
        private bool EN;
        protected Label lblDatagridIcons;
        protected Label lblOrderType;
        protected Label lblUndeliveredOrders;
        protected HtmlTable outerGridTable;

        private void Page_Load(object sender, EventArgs e) {
            // check the current culture
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (culture.Equals("en-US")) {
                EN = true;
            }

            // Put user code to initialize the page here
            if (Session["RosDataView"] == null) {
                return;
            }
            var myView = (DataView) Session["RosDataView"];
            var mySpeeds = (ArrayList) Session["RosDeliverySpeeds"];

            var mySpeed = (DeliverySpeedBLLC) mySpeeds[0];

            myView.RowFilter = "DeliverySpeedID = " + mySpeed.ID;

            SetLabel(mySpeed);

            SetGridHeaders();

            dgOrders.DataSource = myView;
            dgOrders.DataBind();

            if (myView.Count < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll"))) {
                divOrders.Style.Remove("HEIGHT");
            } else {
                divOrders.Style.Add("HEIGHT", "150px");
            }

            mySpeeds.RemoveAt(0);

            if (mySpeeds.Count == 0) {
                Session["RosDeliverySpeeds"] = null;
                Session["RosDataView"] = null;
            } else {
                Session["RosDeliverySpeeds"] = mySpeeds;
            }
        }

        private void ItemCommand(object source, DataGridCommandEventArgs e) {
//			Session["LastPage"] = "UndeliveredOrders";
            Session["State"] = "Update";

            if (e.CommandName == "Customer") {
                //If Customer is a company
                if (e.Item.Cells[6].Text == "True") {
                    Server.Transfer("NewCustomerC.aspx?user=" + e.Item.Cells[5].Text);
                } else {
                    //Else if customer is an individual
                    Server.Transfer("NewCustomer.aspx?user=" + e.Item.Cells[5].Text);
                }
            }
            if (e.CommandName == "Subject") {
                //If subject is a company
                if (e.Item.Cells[10].Text == "True") {
                    Server.Transfer("NewCustomerC.aspx?user=" + e.Item.Cells[9].Text);
                } else {
                    //Else if customer is an individual
                    Server.Transfer("NewCustomer.aspx?user=" + e.Item.Cells[9].Text);
                }
            }
            if (e.CommandName.Equals(DataGrid.SelectCommandName)) {
                //Kalla � s��u sem birtir p�ntunina
                //Response.Redirect("OrderForm.aspx?order=" + e.Item.Cells[1].Text);
                Server.Transfer("OrderForm.aspx?order=" + e.Item.Cells[1].Text);
            }
        }

        private void ItemDataBound(object sender, DataGridItemEventArgs e) {
/*			if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
			{
				((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
			}
*/
            ChangeRowColor(sender, e);

            WebDesign.CreateExplanationIcons(dgOrders.Columns, lblDatagridIcons, rm, ci);
        }

        private static void ChangeRowColor(object sender, DataGridItemEventArgs e) {
            try {
                if (DateTime.Parse(e.Item.Cells[11].Text) <= DateTime.Today) {
                    //	e.Item.BackColor = Color.FromName("#FFFF00");
                    e.Item.ForeColor = Color.Red;
                }
            } catch {
//				Logger.WriteToLog("Caught an exception in ChangeRowColor in class UndeliveredOrderForm when trying to parse 
            }
        }

        private void SetLabel(DeliverySpeedBLLC mySpeed) {
            lblOrderType.Text = EN ? mySpeed.DeliverySpeedEN : mySpeed.DeliverySpeedNative;

            //	lblUndeliveredOrders.Text = rm.GetString("txtUndeliveredOrders", ci);
        }

        private void SetGridHeaders() {
            if (EN) {
                dgOrders.Columns[0].Visible = true;
                dgOrders.Columns[1].Visible = false;
                dgOrders.Columns[2].Visible = true;
                dgOrders.Columns[3].Visible = true;
                dgOrders.Columns[4].Visible = false;
                dgOrders.Columns[5].Visible = true;
                dgOrders.Columns[6].Visible = false;
                dgOrders.Columns[7].Visible = true;
                dgOrders.Columns[8].Visible = false;
                dgOrders.Columns[9].Visible = true;
                dgOrders.Columns[10].Visible = false;
                dgOrders.Columns[11].Visible = true;
                dgOrders.Columns[12].Visible = false;
                dgOrders.Columns[13].Visible = false;
                dgOrders.Columns[14].Visible = false;
                dgOrders.Columns[15].Visible = false;
                dgOrders.Columns[16].Visible = true;
                dgOrders.Columns[17].Visible = false;
            } else {
                dgOrders.Columns[0].Visible = true;
                dgOrders.Columns[1].Visible = false;
                dgOrders.Columns[2].Visible = true;
                dgOrders.Columns[3].Visible = false;
                dgOrders.Columns[4].Visible = true;
                dgOrders.Columns[5].Visible = true;
                dgOrders.Columns[6].Visible = false;
                dgOrders.Columns[7].Visible = false;
                dgOrders.Columns[8].Visible = true;
                dgOrders.Columns[9].Visible = true;
                dgOrders.Columns[10].Visible = false;
                dgOrders.Columns[11].Visible = true;
                dgOrders.Columns[12].Visible = false;
                dgOrders.Columns[13].Visible = false;
                dgOrders.Columns[14].Visible = false;
                dgOrders.Columns[15].Visible = false;
                dgOrders.Columns[16].Visible = false;
                dgOrders.Columns[17].Visible = true;
            }

            dgOrders.Columns[0].HeaderText = rm.GetString("txtOrderID", ci);
            dgOrders.Columns[2].HeaderText = rm.GetString("txtDate", ci);
            dgOrders.Columns[3].HeaderText = rm.GetString("txtCustomerName", ci);
            dgOrders.Columns[4].HeaderText = rm.GetString("txtCustomerName", ci);
            dgOrders.Columns[5].HeaderText = rm.GetString("txtCustomerID", ci);
            dgOrders.Columns[7].HeaderText = rm.GetString("txtSubjectName", ci);
            dgOrders.Columns[8].HeaderText = rm.GetString("txtSubjectName", ci);
            dgOrders.Columns[9].HeaderText = rm.GetString("txtSubjectID", ci);
            dgOrders.Columns[11].HeaderText = rm.GetString("txtDispatchDeadline", ci);
//			dgOrders.Columns[12].HeaderText = rm.GetString("txtRemarksEN",ci);
//			dgOrders.Columns[13].HeaderText = rm.GetString("txtRemarksNative",ci);
//			dgOrders.Columns[14].HeaderText = rm.GetString("txtCustomerRefNo",ci);
            dgOrders.Columns[16].HeaderText = rm.GetString("txtProduct", ci);
            dgOrders.Columns[17].HeaderText = rm.GetString("txtProduct", ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dgOrders.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.ItemCommand);
            this.dgOrders.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}