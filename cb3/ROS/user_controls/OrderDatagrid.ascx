<%@ Control Language="c#" AutoEventWireup="false" Codebehind="OrderDatagrid.ascx.cs" Inherits="ROS.OrderDatagrid" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table width="100%">
	<tr>
		<td>
			<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
				<tr>
					<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<th>
						<asp:label id="lblOrderType" runat="server"></asp:label>
					</th>
				</tr>
				<tr>
					<td>
						<table class="datagrid" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<DIV runat="server" id="divOrders" style="OVERFLOW-X: auto; OVERFLOW: auto; CLIP: rect(auto auto auto auto); HEIGHT: 250px"><asp:datagrid id="dgOrders" runat="server" autogeneratecolumns="False" gridlines="None">
											<FooterStyle CssClass="grid_footer"></FooterStyle>
											<SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
											<AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
											<ItemStyle CssClass="grid_item"></ItemStyle>
											<HeaderStyle CssClass="grid_header"></HeaderStyle>
											<Columns>
												<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
													CommandName="Select">
													<ItemStyle CssClass="leftpadding"></ItemStyle>
												</asp:ButtonColumn>
												<asp:BoundColumn Visible="False" DataField="OrderID" HeaderText="Order ID">
													<ItemStyle CssClass="nopadding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Date" HeaderText="Date">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:ButtonColumn DataTextField="CustomerNameEN" CommandName="Customer">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:ButtonColumn>
												<asp:ButtonColumn DataTextField="CustomerNameNative" CommandName="Customer">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:ButtonColumn>
												<asp:BoundColumn Visible="False" DataField="CustomerID" HeaderText="Customer ID">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="CustomerIsCompany" HeaderText="Customer is a company">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:ButtonColumn DataTextField="SubjectNameEN" HeaderText="SubjectNameEN" CommandName="Subject">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:ButtonColumn>
												<asp:ButtonColumn DataTextField="SubjectNameNative" HeaderText="SubjectNameNative" CommandName="Subject">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:ButtonColumn>
												<asp:BoundColumn Visible="False" DataField="SubjectID" HeaderText="Subject ID">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="SubjectIsCompany" HeaderText="Subject is a company">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="DispatchDeadline" HeaderText="Dispatch Deadline">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="RemarksEN" HeaderText="RemarksEN">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="RemarksNative" HeaderText="RemarksNative">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="CustomerRefNo" HeaderText="Customer Ref. No.">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="DeliverySpeedID" HeaderText="Delivery Speed">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ProductEN" HeaderText="ProductEN">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ProductNative" HeaderText="ProductNative">
													<ItemStyle CssClass="padding"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle CssClass="grid_pager"></PagerStyle>
										</asp:datagrid></DIV>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
</table>
