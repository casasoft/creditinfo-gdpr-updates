using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ROS.BLL;
using ROS.Localization;

using Cig.Framework.Base.Configuration;


namespace ROS {
    /// <summary>
    /// Summary description for OrderSurveyForm.
    /// </summary>
    public class OrderSurveyForm : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private FactoryBLLC _theFactory;
        protected Button btnInsert;
        protected Button btnSearch;
        protected DropDownList ddlProducts;
        protected DropDownList ddlstCustomerIDNumberType;
        protected DropDownList ddlstDeliveryStatus;
        protected DropDownList ddlstSubjectIDNumberType;
        protected DataGrid dgCurrencyList;
        protected DataGrid dgOrderSurvey;
        protected DataGrid dgSubject;
        protected HtmlGenericControl DivOrders;
        protected HtmlGenericControl DivSubjects;
        private bool EN;
        protected Image imgCustomerCompany;
        protected Image imgCustomerIndividual;
        protected Image imgSubjectCompany;
        protected Image imgSubjectIndividual;
        protected Label Label1;
        protected Label lblCurrencyCode;
        protected Label lblCurrencyDescriptionEN;
        protected Label lblCurrencyDescriptionNative;
        protected Label lblCurrencyListHeader;
        protected Label lblCustomer;
        protected Label lblCustomerErrorMessage;
        protected Label lblCustomerFirstNameNative;
        protected Label lblCustomerIDNumberType;
        protected Label lblCustomerIDNumer;
        protected Label lblCustomerLastNameNative;
        protected Label lblDateFrom;
//		protected System.Web.UI.WebControls.Button btnSubjectSearch;
        protected Label lblDates;
        protected Label lblDateTo;
        protected Label lblDeleteIcon;
        protected Label lblEditIcon;
        protected Label lblHeader;
        protected Label lblMessage;
        protected Label lblOrderErrorMessage;
        protected Label lblOrders;
        protected Label lblPostalCode;
        protected Label lblProduct;
        protected Label lblSearchFor;
        protected Label lblSubject;
        protected Label lblSubjectErrorMessage;
        protected Label lblSubjectFirstNameNative;
        protected Label lblSubjectIDNumber;
        protected Label lblSubjectIDNumberType;
        protected Label lblSubjectLastNameNative;
        protected HtmlTable outerGridTable;
        protected HtmlTableRow outerGridTableInfo;
        protected HtmlInputHidden PageX;
        protected HtmlInputHidden PageY;
        protected RadioButton rbtnCustomerCompany;
//		protected System.Web.UI.WebControls.Button btnCustomerClear;
//		protected System.Web.UI.WebControls.Button btnSubjectClear;
        protected RadioButton rbtnCustomerSubject;
        protected RadioButton rbtnSubjectCompany;
        protected RadioButton rbtnSubjectIndividual;
        protected HtmlTable tblOrderDataGrid;
        protected HtmlTable tblorders;
        protected HtmlTable tblSubjectDataGrid;
        protected TextBox txtCurrencyCode;
        protected TextBox txtCurrencyDescEN;
        protected TextBox txtCurrencyDescNative;
        protected TextBox txtCustomerFirstNameNative;
        protected TextBox txtCustomerIDNumber;
        protected TextBox txtCustomerLastNameNative;
        protected TextBox txtDateFrom;
        protected TextBox txtDateTo;
        protected TextBox txtPostalCode;
        protected TextBox txtSubjectFirstNameNative;
        protected TextBox txtSubjectIDNumber;
        protected TextBox txtSubjectLastNameNative;
        protected ValidationSummary ValidationSummary1;
        protected CustomValidator ValidatorDateFromFormat;
        protected CompareValidator ValidatorDateMismatch;
        protected CustomValidator ValidatorDateToFormat;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = CigConfig.Configure("lookupsettings.ROSPageIDOrderSurveyForm");

            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            // check the current culture
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");

            lblCustomerErrorMessage.Visible = false;
            lblSubjectErrorMessage.Visible = false;
            lblOrderErrorMessage.Visible = false;

            if (!IsPostBack) {
                Session["RosOrder"] = null;
                Session["LastPage"] = "OrderSurveyForm";

                _theFactory = new FactoryBLLC();

                InitPage();

                tblOrderDataGrid.Visible = false;
            }
        }

        private void InitPage() {
            InitDDBoxes();
            LocalizeText();
            SetGridHeaders();
            CheckCustomerRadioButtonState();
            CheckSubjectRadioButtonState();

            txtDateTo.Text = DateTime.Today.ToShortDateString();

            var dateFrom = DateTime.Today;
            txtDateFrom.Text = dateFrom.AddYears(-1).ToShortDateString();
        }

        private void CheckCustomerRadioButtonState() {
            if (rbtnCustomerCompany.Checked) {
                lblCustomerFirstNameNative.Text = rm.GetString("txtNameNative", ci);

                lblCustomerLastNameNative.Visible = false;
                txtCustomerLastNameNative.Visible = false;
            } else {
                lblCustomerFirstNameNative.Text = rm.GetString("txtFirstName", ci);

                lblCustomerFirstNameNative.Visible = true;
                txtCustomerFirstNameNative.Visible = true;
                lblCustomerLastNameNative.Visible = true;
                txtCustomerLastNameNative.Visible = true;
            }
        }

        private void CheckSubjectRadioButtonState() {
            if (rbtnSubjectCompany.Checked) {
                lblSubjectFirstNameNative.Text = rm.GetString("txtNameNative", ci);

                lblSubjectLastNameNative.Visible = false;
                txtSubjectLastNameNative.Visible = false;
            } else {
                lblSubjectFirstNameNative.Text = rm.GetString("txtFirstName", ci);

                lblSubjectFirstNameNative.Visible = true;
                txtSubjectFirstNameNative.Visible = true;
                lblSubjectLastNameNative.Visible = true;
                txtSubjectLastNameNative.Visible = true;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {
                return;
            }
            lblOrderErrorMessage.Visible = false;
            tblOrderDataGrid.Visible = false;

            var theDateFrom = new DateTime();
            var theDateTo = new DateTime();

            //Get the to/from dates
            if (txtDateFrom.Text.Length > 0) {
                try {
                    theDateFrom = DateTime.Parse(txtDateFrom.Text);
                } catch {}
            }
            if (txtDateTo.Text.Length > 0) {
                try {
                    theDateTo = DateTime.Parse(txtDateTo.Text);
                } catch {}
            }

            int productID = int.Parse(ddlProducts.SelectedValue);

            var myCustomer = GetCustomerInfoFromPage();

            var mySubject = GetSubjectInfoFromPage();

            var theFactory = new FactoryBLLC();

            var theOrderList = new ArrayList();

            if (ddlstDeliveryStatus.SelectedValue == "0") {
                theOrderList = theFactory.FindDeliveredOrder(
                    theDateFrom, theDateTo, myCustomer, mySubject, productID);
            }
            if (ddlstDeliveryStatus.SelectedValue == "1") {
                theOrderList = theFactory.FindUndeliveredOrder(
                    theDateFrom, theDateTo, myCustomer, mySubject, productID);
            }
            if (ddlstDeliveryStatus.SelectedValue == "2") {
                theOrderList = theFactory.FindOrder(theDateFrom, theDateTo, myCustomer, mySubject, productID);
            }

            if (theOrderList.Count > 0) {
                InsertIntoOrderListDatagrid(theOrderList);
            } else {
                //Birta villubo�
                lblOrderErrorMessage.Text = rm.GetString("txtCouldNotFindOrder", ci);
                lblOrderErrorMessage.Visible = true;
                //N�llstilla creditinfoid � session?
            }
        }

        private void InsertIntoOrderListDatagrid(ArrayList theOrderList) {
            var theTable = new DataTable();
            theTable.Columns.Add("Date");
            theTable.Columns.Add("CustomerNameEN");
            theTable.Columns.Add("CustomerNameNative");
            theTable.Columns.Add("CustomerID");
            theTable.Columns.Add("CustomerIsCompany");
            theTable.Columns.Add("SubjectNameEN");
            theTable.Columns.Add("SubjectNameNative");
            theTable.Columns.Add("SubjectID");
            theTable.Columns.Add("SubjectIsCompany");
            theTable.Columns.Add("DispatchDeadline");
            theTable.Columns.Add("RemarksEN");
            theTable.Columns.Add("RemarksNative");
            theTable.Columns.Add("OrderID");
            theTable.Columns.Add("CustomerRefNo");
            theTable.Columns.Add("DeliveryStatus");
            theTable.Columns.Add("ProductEN");
            theTable.Columns.Add("ProductNative");

            foreach (OrderBLLC anOrder in theOrderList) {
                var aRow = theTable.NewRow();
                aRow[0] = anOrder.OrderDate.ToShortDateString();

                string type = CigConfig.Configure("lookupsettings.individualID");
                if (anOrder.Customer.Type == type) {
                    aRow[4] = false;

                    if (((IndividualBLLC) anOrder.Customer).FirstNameEN.Length > 0 ||
                        ((IndividualBLLC) anOrder.Customer).LastNameEN.Length > 0) {
                        aRow[1] = ((IndividualBLLC) anOrder.Customer).FirstNameEN + " " +
                                  ((IndividualBLLC) anOrder.Customer).LastNameEN;
                    } else {
                        aRow[1] = ((IndividualBLLC) anOrder.Customer).FirstNameNative + " " +
                                  ((IndividualBLLC) anOrder.Customer).LastNameNative;
                    }

                    if (((IndividualBLLC) anOrder.Customer).FirstNameNative.Length > 0 ||
                        ((IndividualBLLC) anOrder.Customer).LastNameNative.Length > 0) {
                        aRow[2] = ((IndividualBLLC) anOrder.Customer).FirstNameNative + " " +
                                  ((IndividualBLLC) anOrder.Customer).LastNameNative;
                    } else {
                        aRow[2] = ((IndividualBLLC) anOrder.Customer).FirstNameEN + " " +
                                  ((IndividualBLLC) anOrder.Customer).LastNameEN;
                    }
                } else {
                    aRow[4] = true;

                    aRow[1] = ((CompanyBLLC) anOrder.Customer).NameEN.Length > 0 ? ((CompanyBLLC) anOrder.Customer).NameEN : ((CompanyBLLC) anOrder.Customer).NameNative;

                    aRow[2] = ((CompanyBLLC) anOrder.Customer).NameNative.Length > 0 ? ((CompanyBLLC) anOrder.Customer).NameNative : ((CompanyBLLC) anOrder.Customer).NameEN;
                }

                aRow[3] = anOrder.Customer.CreditInfoID;

                if (anOrder.Subject.Type == type) {
                    aRow[8] = false;

                    if (((IndividualBLLC) anOrder.Subject).FirstNameEN.Length > 0 ||
                        ((IndividualBLLC) anOrder.Subject).LastNameEN.Length > 0) {
                        aRow[5] = ((IndividualBLLC) anOrder.Subject).FirstNameEN + " " +
                                  ((IndividualBLLC) anOrder.Subject).LastNameEN;
                    } else {
                        aRow[5] = ((IndividualBLLC) anOrder.Subject).FirstNameNative + " " +
                                  ((IndividualBLLC) anOrder.Subject).LastNameNative;
                    }

                    if (((IndividualBLLC) anOrder.Subject).FirstNameNative.Length > 0 ||
                        ((IndividualBLLC) anOrder.Subject).LastNameNative.Length > 0) {
                        aRow[6] = ((IndividualBLLC) anOrder.Subject).FirstNameNative + " " +
                                  ((IndividualBLLC) anOrder.Subject).LastNameNative;
                    }
                } else {
                    aRow[8] = true;

                    aRow[5] = ((CompanyBLLC) anOrder.Subject).NameEN.Length > 0 ? ((CompanyBLLC) anOrder.Subject).NameEN : ((CompanyBLLC) anOrder.Subject).NameNative;

                    aRow[6] = ((CompanyBLLC) anOrder.Subject).NameNative.Length > 0 ? ((CompanyBLLC) anOrder.Subject).NameNative : ((CompanyBLLC) anOrder.Subject).NameEN;
                }

                aRow[7] = anOrder.Subject.CreditInfoID;
                aRow[9] = anOrder.DispatchDeadline.ToShortDateString();
                aRow[10] = anOrder.RemarksEN;
                aRow[11] = anOrder.RemarksNative;
                aRow[12] = anOrder.OrderID;
                aRow[13] = anOrder.CustomerReferenceNumber;
                aRow[14] = anOrder.Delivered ? rm.GetString("txtDelivered", ci) : rm.GetString("txtUndelivered", ci);
                aRow[15] = anOrder.Product.NameEN;
                aRow[16] = anOrder.Product.NameNative;

                theTable.Rows.Add(aRow);
            }

            var orderView = new DataView(theTable);
            dgOrderSurvey.DataSource = orderView;
            dgOrderSurvey.DataBind();

            if (theTable.Rows.Count < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll"))) {
                DivOrders.Style.Remove("HEIGHT");
            } else {
                DivOrders.Style.Add("HEIGHT", "150px");
            }

            tblOrderDataGrid.Visible = true;
        }

        private void BindProducts() {
            var productSet = _theFactory.GetAllProducts();
            ddlProducts.DataSource = productSet;
            ddlProducts.DataTextField = EN ? "nameEN" : "nameNative";
            ddlProducts.DataValueField = "ID";
            ddlProducts.DataBind();

            var myItem = new ListItem(rm.GetString("txtAllProducts", ci), "-1");
            ddlProducts.Items.Add(myItem);
        }

        private void BindIDNumbers() {
            var allIDTypes = _theFactory.GetIDNumberTypes();
            ddlstCustomerIDNumberType.DataSource = allIDTypes;

            ddlstCustomerIDNumberType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlstCustomerIDNumberType.DataValueField = "NumberTypeID";
            ddlstCustomerIDNumberType.DataBind();

            ddlstSubjectIDNumberType.DataSource = allIDTypes;

            ddlstSubjectIDNumberType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlstSubjectIDNumberType.DataValueField = "NumberTypeID";
            ddlstSubjectIDNumberType.DataBind();
        }

        private void InsertIntoDeliveryStatus() {
            var deliveredItem = new ListItem(rm.GetString("txtOrdersDelivered", ci), "0");
            ddlstDeliveryStatus.Items.Add(deliveredItem);

            var undeliveredItem = new ListItem(rm.GetString("txtOrdersUndelivered", ci), "1");
            ddlstDeliveryStatus.Items.Add(undeliveredItem);

            var bothItem = new ListItem(rm.GetString("txtBoth", ci), "2");
            ddlstDeliveryStatus.Items.Add(bothItem);
        }

        private void InitDDBoxes() {
            BindIDNumbers();
            InsertIntoDeliveryStatus();
            BindProducts();
        }

        private void rbtnCustomerCompany_CheckedChanged(object sender, EventArgs e) { CheckCustomerRadioButtonState(); }
        private void rbtnSubjectCompany_CheckedChanged(object sender, EventArgs e) { CheckSubjectRadioButtonState(); }
        private void rbtnSubjectIndividual_CheckedChanged(object sender, EventArgs e) { CheckSubjectRadioButtonState(); }

        private CustomerBLLC GetCustomerInfoFromPage() {
            //get the customer info
            CustomerBLLC theCustomer;
            if (rbtnCustomerCompany.Checked) {
                //We are searching for a company
                theCustomer = new CompanyBLLC {IsCompany = true};

                ((CompanyBLLC) theCustomer).NameNative = txtCustomerFirstNameNative.Text;
            } else {
                //We are searching for an individual
                theCustomer = new IndividualBLLC {IsCompany = false};

                ((IndividualBLLC) theCustomer).FirstNameNative = txtCustomerFirstNameNative.Text;
                ((IndividualBLLC) theCustomer).LastNameNative = txtCustomerLastNameNative.Text;
            }

            if (txtCustomerIDNumber.Text.Length > 0) {
                var theID = new IDNumberBLLC
                            {
                                Number = txtCustomerIDNumber.Text,
                                NumberTypeID = int.Parse(ddlstCustomerIDNumberType.SelectedValue)
                            };
                theCustomer.IDNumbers.Add(theID);
            }

            return theCustomer;
        }

        private CustomerBLLC GetSubjectInfoFromPage() {
            //Get the subject info
            CustomerBLLC theSubject;

            if (rbtnSubjectCompany.Checked) {
                //We want to search for a company
                theSubject = new CompanyBLLC {IsCompany = true};

                ((CompanyBLLC) theSubject).NameNative = txtSubjectFirstNameNative.Text;
            } else {
                //We want to search for an individual
                theSubject = new IndividualBLLC {IsCompany = false};

                ((IndividualBLLC) theSubject).FirstNameNative = txtSubjectFirstNameNative.Text;
                ((IndividualBLLC) theSubject).LastNameNative = txtSubjectLastNameNative.Text;
            }

            if (txtSubjectIDNumber.Text.Length > 0) {
                var theID = new IDNumberBLLC
                                     {
                                         Number = txtSubjectIDNumber.Text,
                                         NumberTypeID = int.Parse(ddlstSubjectIDNumberType.SelectedValue)
                                     };
                theSubject.IDNumbers.Add(theID);
            }

            return theSubject;
        }

        private void LocalizeText() {
            //lblHeader.Text = rm.GetString("txtOrders",ci);

            lblSearchFor.Text = rm.GetString("txtSearchFor", ci);
            lblProduct.Text = rm.GetString("txtProduct", ci);

            lblDates.Text = rm.GetString("txtDates", ci);
            lblDateFrom.Text = rm.GetString("txtDateFrom", ci);
            lblDateTo.Text = rm.GetString("txtDateTo", ci);
            ValidatorDateMismatch.ErrorMessage = rm.GetString("txtDateMismatch", ci);
            ValidatorDateFromFormat.ErrorMessage = rm.GetString("ErrorDateFormat", ci);
            ValidatorDateToFormat.ErrorMessage = rm.GetString("ErrorDateFormat", ci);

            // the customer section 	
            lblCustomer.Text = rm.GetString("txtCustomer", ci);
            //		rbtnCustomerCompany.Text = rm.GetString("txtCompany",ci);
            //		rbtnCustomerSubject.Text = rm.GetString("txtIndividual",ci);
            lblCustomerIDNumberType.Text = rm.GetString("txtType", ci);
            lblCustomerIDNumer.Text = rm.GetString("txtIDNumber", ci);
            lblCustomerFirstNameNative.Text = rm.GetString("txtFirstNameNative", ci);
            lblCustomerLastNameNative.Text = rm.GetString("txtLastNameNative", ci);
            //		btnCustomerSearch.Text = rm.GetString("txtSearchForCustomer",ci);
            //		btnCustomerSearch.ToolTip = rm.GetString("txtSearchForCustomer",ci);
            //		btnCustomerClear.Text = rm.GetString("txtCustomerClear",ci);
            //		btnCustomerClear.ToolTip = rm.GetString("txtCustomerClear",ci);
            //txtSelect

            // the subject section
            lblSubject.Text = rm.GetString("txtSubject", ci);
            //		rbtnSubjectCompany.Text = rm.GetString("txtCompany",ci);
            //		rbtnSubjectIndividual.Text = rm.GetString("txtIndividual",ci);
            lblSubjectIDNumberType.Text = rm.GetString("txtType", ci);
            lblSubjectIDNumber.Text = rm.GetString("txtIDNumber", ci);
            lblSubjectFirstNameNative.Text = rm.GetString("txtFirstNameNative", ci);
            lblSubjectLastNameNative.Text = rm.GetString("txtLastNameNative", ci);
            //		btnSubjectSearch.Text = rm.GetString("txtSearchForSubject",ci);
            //		btnSubjectSearch.ToolTip = rm.GetString("txtSearchForSubject",ci);
            //		btnSubjectClear.Text = rm.GetString("txtSubjectClear",ci);
            //		btnSubjectClear.ToolTip = rm.GetString("txtSubjectClear",ci);

            lblOrders.Text = rm.GetString("lblOrders", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);
            btnSearch.ToolTip = rm.GetString("txtSearch", ci);
        }

        private void SetGridHeaders() {
            if (EN) {
                //Order grid
                dgOrderSurvey.Columns[0].Visible = true;
                dgOrderSurvey.Columns[1].Visible = false;
                dgOrderSurvey.Columns[2].Visible = true;
                dgOrderSurvey.Columns[3].Visible = true;
                dgOrderSurvey.Columns[4].Visible = true;
                dgOrderSurvey.Columns[5].Visible = false;
                dgOrderSurvey.Columns[6].Visible = false;
                dgOrderSurvey.Columns[7].Visible = false;
                dgOrderSurvey.Columns[8].Visible = true;
                dgOrderSurvey.Columns[9].Visible = false;
                dgOrderSurvey.Columns[10].Visible = false;
                dgOrderSurvey.Columns[11].Visible = false;
                dgOrderSurvey.Columns[12].Visible = false;
                dgOrderSurvey.Columns[13].Visible = false;
                dgOrderSurvey.Columns[14].Visible = false;

                dgOrderSurvey.Columns[15].Visible = true;
                dgOrderSurvey.Columns[16].Visible = false;
                dgOrderSurvey.Columns[17].Visible = true;
            } else {
                //Order grid
                dgOrderSurvey.Columns[0].Visible = true;
                dgOrderSurvey.Columns[1].Visible = false;
                dgOrderSurvey.Columns[2].Visible = true;
                dgOrderSurvey.Columns[3].Visible = true;
                dgOrderSurvey.Columns[4].Visible = false;
                dgOrderSurvey.Columns[5].Visible = true;
                dgOrderSurvey.Columns[6].Visible = false;
                dgOrderSurvey.Columns[7].Visible = false;
                dgOrderSurvey.Columns[8].Visible = false;
                dgOrderSurvey.Columns[9].Visible = true;
                dgOrderSurvey.Columns[10].Visible = false;
                dgOrderSurvey.Columns[11].Visible = false;
                dgOrderSurvey.Columns[12].Visible = false;
                dgOrderSurvey.Columns[13].Visible = false;
                dgOrderSurvey.Columns[14].Visible = false;

                dgOrderSurvey.Columns[15].Visible = false;
                dgOrderSurvey.Columns[16].Visible = true;
                dgOrderSurvey.Columns[17].Visible = true;
            }

            //Order grid
            dgOrderSurvey.Columns[1].HeaderText = rm.GetString("txtOrderID", ci);
            dgOrderSurvey.Columns[2].HeaderText = rm.GetString("txtDate", ci);
            dgOrderSurvey.Columns[3].HeaderText = rm.GetString("txtDispatchDeadline", ci);
            dgOrderSurvey.Columns[4].HeaderText = rm.GetString("txtCustomerName", ci);
            dgOrderSurvey.Columns[5].HeaderText = rm.GetString("txtCustomerName", ci);
            dgOrderSurvey.Columns[6].HeaderText = rm.GetString("txtCustomerID", ci);
            dgOrderSurvey.Columns[8].HeaderText = rm.GetString("txtSubjectName", ci);
            dgOrderSurvey.Columns[9].HeaderText = rm.GetString("txtSubjectName", ci);
            dgOrderSurvey.Columns[10].HeaderText = rm.GetString("txtSubjectID", ci);
            dgOrderSurvey.Columns[12].HeaderText = rm.GetString("txtRemarksEN", ci);
            dgOrderSurvey.Columns[13].HeaderText = rm.GetString("txtRemarksNative", ci);
            dgOrderSurvey.Columns[14].HeaderText = rm.GetString("txtCustomerRefNo", ci);

            dgOrderSurvey.Columns[15].HeaderText = rm.GetString("txtProduct", ci);
            dgOrderSurvey.Columns[16].HeaderText = rm.GetString("txtProduct", ci);
            dgOrderSurvey.Columns[17].HeaderText = rm.GetString("txtDeliveryStatus", ci);
        }

        private void dgOrderSurvey_ItemCommand_2(object source, DataGridCommandEventArgs e) {
            Session["LastPage"] = "OrderSurveyForm";
            Session["State"] = "Update";

            if (e.CommandName == "Customer") {
                //If Customer is a company
                if (e.Item.Cells[7].Text == "True") {
                    Server.Transfer("NewCustomerC.aspx?user=" + e.Item.Cells[6].Text);
                } else {
                    //Else if customer is an individual
                    Server.Transfer("NewCustomer.aspx?user=" + e.Item.Cells[6].Text);
                }
            }
            if (e.CommandName == "Subject") {
                //If subject is a company
                if (e.Item.Cells[11].Text == "True") {
                    Server.Transfer("NewCustomerC.aspx?user=" + e.Item.Cells[10].Text);
                } else {
                    //Else if customer is an individual
                    Server.Transfer("NewCustomer.aspx?user=" + e.Item.Cells[10].Text);
                }
            }
            if (e.CommandName == "Order") {
                //Kalla � s��u sem birtir p�ntunina
                Server.Transfer("OrderForm.aspx?order=" + e.Item.Cells[1].Text);
            }
        }

        private void ValidatorDateFromFormat_ServerValidate(object source, ServerValidateEventArgs args) {
            if (args.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(args.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    args.IsValid = true;
                } catch (Exception) {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        private void ValidatorDateToFormat_ServerValidate(object source, ServerValidateEventArgs args) {
            if (args.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(args.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    args.IsValid = true;
                } catch (Exception) {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        private void dgOrderSurvey_ItemDataBound(object sender, DataGridItemEventArgs e) { }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ValidatorDateFromFormat.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidatorDateFromFormat_ServerValidate);
            this.ValidatorDateToFormat.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidatorDateToFormat_ServerValidate);
            this.rbtnCustomerCompany.CheckedChanged += new System.EventHandler(this.rbtnCustomerCompany_CheckedChanged);
            this.rbtnCustomerSubject.CheckedChanged += new System.EventHandler(this.rbtnCustomerCompany_CheckedChanged);
            this.rbtnSubjectCompany.CheckedChanged += new System.EventHandler(this.rbtnSubjectCompany_CheckedChanged);
            this.rbtnSubjectIndividual.CheckedChanged +=
                new System.EventHandler(this.rbtnSubjectIndividual_CheckedChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.dgOrderSurvey.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgOrderSurvey_ItemCommand_2);
            this.dgOrderSurvey.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgOrderSurvey_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}