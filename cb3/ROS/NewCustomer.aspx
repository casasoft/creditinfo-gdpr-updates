<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="NewCustomer.aspx.cs" AutoEventWireup="false" Inherits="ROS.Customer" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Customer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
<?xml version="1.0" ?>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language2" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="Sitepositionbar2" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<TABLE id="FormTable" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lbPersonInformation" runat="server" Width="100%">Person Info</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lbDebtorCIID" runat="server" Width="100%">CreditInfoID</asp:label><BR>
																		<asp:textbox id="txtCustomerCIID" runat="server" EnableViewState="False" MaxLength="10"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"></TD>
																	<TD style="WIDTH: 25%"></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lbDebtorFirstName" runat="server" Width="100%">First name</asp:label><BR>
																		<asp:textbox id="txtCustomerFirstNameNative" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox>&nbsp;
																		<asp:requiredfieldvalidator id="RequiredFieldValidatorFirstName" runat="server" ErrorMessage="RequiredFieldValidator"
																			ControlToValidate="txtCustomerFirstNameNative">*</asp:requiredfieldvalidator></TD>
																	<TD><asp:label id="lbDebtorSurName" runat="server" Width="100%">Surname</asp:label><BR>
																		<asp:textbox id="txtCustomerSurNameNative" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidatorSurname" runat="server" ErrorMessage="RequiredFieldValidator"
																			ControlToValidate="txtCustomerSurNameNative">*</asp:requiredfieldvalidator></TD>
																	<TD><asp:label id="lbFirstNameEN" runat="server" Width="100%">First name(EN)</asp:label><BR>
																		<asp:textbox id="txtCustomerFirstNameEN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lbSurnameEN" runat="server" Width="100%">Surname(EN)</asp:label><BR>
																		<asp:textbox id="txtCustomerSurnameEN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lbCustomerEmailPrimary" runat="server" Width="100%">Email (Primary)</asp:label><BR>
																		<asp:textbox id="txtCustomerEmail1" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD><asp:label id="lblCustomerEmailSecondary" runat="server" Width="100%">Email (Secondary)</asp:label><BR>
																		<asp:textbox id="txtCustomerEmail2" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD><asp:label id="lblCustomerEmailThird" runat="server" Width="100%">Email (Third)</asp:label><BR>
																		<asp:textbox id="txtCustomerEmail3" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 179px" height="23"></TD>
																	<TD style="WIDTH: 187px" height="23"></TD>
																	<TD height="23" style="WIDTH: 190px"></TD>
																	<TD height="23"></TD>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															<asp:label id="lblTheAddress1" runat="server" Width="100%" Font-Bold="True">Address 1</asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lblAddress1" runat="server" Width="100%">Address</asp:label><BR>
																		<asp:textbox id="txtAddress1Native" runat="server" EnableViewState="False" MaxLength="100"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lbAddress1InfoNative" runat="server" Width="100%">Address Info</asp:label><BR>
																		<asp:textbox id="txtAddress1InfoNative" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblAddress1EN" runat="server" Width="100%">Address1(EN)</asp:label><BR>
																		<asp:textbox id="txtAddress1EN" runat="server" EnableViewState="False" MaxLength="100"></asp:textbox></TD>
																	<TD><asp:label id="lblAddress1InfoEN" runat="server" Width="100%">Address Info(EN)</asp:label><BR>
																		<asp:textbox id="txtAddress1InfoEN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lblAddress1PostalCodeNative" runat="server" Width="100%">PostalCode</asp:label><BR>
																		<asp:textbox id="txtAddress1PostalCode" runat="server" MaxLength="30"></asp:textbox></TD>
																	<TD colSpan="2"><asp:label id="lblAddress1City" runat="server" Width="150px">City</asp:label>&nbsp;
																		<asp:label id="lblCitySearch" runat="server">Search</asp:label><BR>
																		<asp:dropdownlist id="ddlAddress1City" runat="server"></asp:dropdownlist>&nbsp;<asp:textbox id="txtCitySearch" runat="server" CssClass="short_input" MaxLength="10"></asp:textbox>&nbsp;
																		<asp:button id="btnCitySearch" runat="server" CssClass="popup" CausesValidation="False" Text="..."></asp:button>&nbsp;
																		<asp:requiredfieldvalidator id="rfvCity" runat="server" ErrorMessage="Value missing!" ControlToValidate="ddlAddress1City">*</asp:requiredfieldvalidator></TD>
																	<TD><asp:label id="lblAddress1PostBox" runat="server" Width="100%">PostBox</asp:label><BR>
																		<asp:textbox id="txtAddress1PostBox" runat="server" EnableViewState="False" MaxLength="20"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lblAddress1Country" runat="server" Width="100%">Country</asp:label><BR>
																		<asp:dropdownlist id="ddlAddress1Country" runat="server"></asp:dropdownlist></TD>
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															<asp:label id="lblTheAddress2" runat="server" Width="100%" Font-Bold="True">Address 2</asp:label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lblAddress2Native" runat="server" Width="100%">Address</asp:label><BR>
																		<asp:textbox id="txtAddress2Native" runat="server" EnableViewState="False" MaxLength="100"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblAddress2InfoNative" runat="server" Width="100%">Address Info</asp:label><BR>
																		<asp:textbox id="txtAddress2InfoNative" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblAddress2EN" runat="server" Width="100%">Address(EN)</asp:label><BR>
																		<asp:textbox id="txtAddress2EN" runat="server" EnableViewState="False" MaxLength="100"></asp:textbox></TD>
																	<TD><asp:label id="lblAddress2InfoEN" runat="server" Width="100%">Address Info(EN)</asp:label><BR>
																		<asp:textbox id="txtAddress2InfoEN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 63px"><asp:label id="lblAddress2PostalCode" runat="server" Width="100%">PostalCode</asp:label><BR>
																		<asp:textbox id="txtAddress2PostalCode" runat="server" EnableViewState="False" MaxLength="30"></asp:textbox></TD>
																	<TD colSpan="2"><asp:label id="lblAddress2City" runat="server" Width="150px">City</asp:label>&nbsp;
																		<asp:label id="lblCity2Search" runat="server">Search</asp:label><BR>
																		<asp:dropdownlist id="ddlAddress2City" runat="server"></asp:dropdownlist>&nbsp;<asp:textbox id="txtCity2Search" runat="server" CssClass="short_input" MaxLength="10"></asp:textbox>
																		<asp:button id="btnCity2Search" runat="server" CssClass="popup" CausesValidation="False" Text="..."></asp:button>&nbsp;
																		<asp:requiredfieldvalidator id="rfvCity2" runat="server" ErrorMessage="Value missing!" ControlToValidate="ddlAddress2City"
																			Enabled="False">*</asp:requiredfieldvalidator></TD>
																	<TD><asp:label id="lblAddress2PostBox" runat="server" Width="100%">PostBox</asp:label><BR>
																		<asp:textbox id="txtAddress2PostBox" runat="server" EnableViewState="False" MaxLength="20"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lblAddress2Country" runat="server" Width="100%">Country</asp:label><BR>
																		<asp:dropdownlist id="ddlAddress2Country" runat="server"></asp:dropdownlist></TD>
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<TR>
														<TH>
															&nbsp;
															<asp:Label id="lblNumbers" runat="server">Label</asp:Label></TH></TR>
													<TR>
														<TD>
															<TABLE class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><BR>
																	</TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblProfession" runat="server" Width="100%">Profession</asp:label><BR>
																		<asp:dropdownlist id="ddlProfession" runat="server"></asp:dropdownlist></TD>
																	<TD style="WIDTH: 25%"><BR>
																	</TD>
																	<TD><asp:label id="lblEducation" runat="server" Width="100%">Education</asp:label><BR>
																		<asp:dropdownlist id="ddlEducation" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lblIDNumber" runat="server" Width="100%">IDNumber 1</asp:label><BR>
																		<asp:textbox id="txtIDNumber1" runat="server" Width="140px" EnableViewState="False" MaxLength="100"></asp:textbox></TD>
																	<TD><asp:label id="lblIDNumberType" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlIDNumberType1" runat="server"></asp:dropdownlist></TD>
																	<TD><asp:label id="lblIDNumber2" runat="server" Width="100%">IDNumber 2</asp:label><BR>
																		<asp:textbox id="txtIDNumber2" runat="server" Width="140px" EnableViewState="False" MaxLength="100"></asp:textbox></TD>
																	<TD><asp:label id="lblIDNumberType2" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlIDNumberType2" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lblPhoneNumber1" runat="server" Width="100%">Phone number 1</asp:label><BR>
																		<asp:textbox id="txtPhoneNumber1" runat="server" Width="140px" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblPhonNumber1Type" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlPhoneNumber1Type" runat="server"></asp:dropdownlist></TD>
																	<TD><asp:label id="lblPhoneNumber2" runat="server" Width="100%">Phone number 2</asp:label><BR>
																		<asp:textbox id="txtPhoneNumber2" runat="server" Width="140px" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblPhonenumber2Type" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlPhoneNumber2Type" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lblPhoneNumber3" runat="server" Width="100%">Phone number 3</asp:label><BR>
																		<asp:textbox id="txtPhoneNumber3" runat="server" Width="140px" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblPhoneNumber3Type" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlPhoneNumber3Type" runat="server"></asp:dropdownlist></TD>
																	<TD><asp:label id="lblPhoneNumber4" runat="server" Width="100%">Phone number 4</asp:label><BR>
																		<asp:textbox id="txtPhoneNumber4" runat="server" Width="140px" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblPhoneNumber4Type" runat="server" Width="100%">Type</asp:label><BR>
																		<asp:dropdownlist id="ddlPhoneNumber4Type" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 108px" height="23"></TD>
																	<TD style="WIDTH: 233px" height="23"></TD>
																	<TD style="WIDTH: 109px" height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<td height="10"></td>
										</TR>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th style="HEIGHT: 24px">
															&nbsp;
															<asp:Label id="lblAdditionalInfo" runat="server">Label</asp:Label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 50%"><asp:label id="lblCustomerRemarks" runat="server" Width="100%">Remarks</asp:label><BR>
																		<asp:textbox id="txtCustomerRemarksNative" runat="server" Width="400px" MaxLength="1024" TextMode="MultiLine"
																			Height="114px"></asp:textbox></TD>
																	<TD style="WIDTH: 50%"><asp:label id="lblCustomerRemarksEN" runat="server" Width="100%">Remarks</asp:label><BR>
																		<asp:textbox id="txtCustomerRemarksEN" runat="server" Width="400px" MaxLength="1024" TextMode="MultiLine"
																			Height="114px"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 348px" height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<td height="10"></td>
										</TR>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblContactPerson1" runat="server" Width="100%" Font-Bold="True">Contact person 1</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact1FirstNameNative" runat="server" Width="100%">First Name</asp:label><BR>
																		<asp:textbox id="txtContact1FirstNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact1SurnameNative" runat="server" Width="100%">Surname</asp:label><BR>
																		<asp:textbox id="txtContact1SurnameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact1FirstNameEN" runat="server" Width="100%">First Name (EN)</asp:label><BR>
																		<asp:textbox id="txtContact1FirstNameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblContact1SurnameEN" runat="server" Width="100%">Surname (EN)</asp:label><BR>
																		<asp:textbox id="txtContact1SurnameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lblContact1Email" runat="server" Width="100%">Email</asp:label><BR>
																		<asp:textbox id="txtContact1Email" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<TD height="10"></TD>
										</TR>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblContactPerson2" runat="server" Width="100%" Font-Bold="True">Contact Person 2</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact2FirstName" runat="server" Width="100%">First Name</asp:label><BR>
																		<asp:textbox id="txtContact2FirstNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact2Surname" runat="server" Width="100%">Surname</asp:label><BR>
																		<asp:textbox id="txtContact2SurnameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact2FirstNameEN" runat="server" Width="100%">First Name (EN)</asp:label><BR>
																		<asp:textbox id="txtContact2FirstNameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblContact2SurnameEN" runat="server" Width="100%">Surname (EN)</asp:label><BR>
																		<asp:textbox id="txtContact2SurnameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lblContact2Email" runat="server" Width="100%">Email</asp:label><BR>
																		<asp:textbox id="txtContact2Email" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblContactPerson3" runat="server" Width="100%" Font-Bold="True">Contact Person 3</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact3FirstName" runat="server" Width="100%">First Name</asp:label><BR>
																		<asp:textbox id="txtContact3FirstNameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact3Surname" runat="server" Width="100%">Surname</asp:label><BR>
																		<asp:textbox id="txtContact3SurnameNative" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblContact3FirstNameEN" runat="server" Width="100%">First Name (EN)</asp:label><BR>
																		<asp:textbox id="txtContact3FirstNameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lblContact3SurnameEN" runat="server" Width="100%">Surname (EN)</asp:label><BR>
																		<asp:textbox id="txtContact3SurnameEN" runat="server" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD><asp:label id="lblContact3Email" runat="server" Width="100%">Email</asp:label><BR>
																		<asp:textbox id="txtContact3Email" runat="server" MaxLength="255"></asp:textbox></TD>
																	<TD></TD>
																	<TD></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<TR>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD>
												<TABLE id="Table8" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR valign="top">
														<td>
															<asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary><BR>
															<asp:label id="lblErrorMessage" runat="server" Width="100%" ForeColor="Red" BackColor="Transparent">ErrorMessage</asp:label>
														</td>
														<TD align="right">
															<asp:button id="btCancel" runat="server" CssClass="cancel_button" Text="cancel" CausesValidation="False"></asp:button><asp:button id="btRebDebtor" runat="server" CssClass="confirm_button" Text="submit"></asp:button>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><uc1:footer id="Footer2" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
