using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logging.BLL;
using ROS.BLL;
using ROS.Localization;

using Cig.Framework.Base.Configuration;

namespace ROS {
    /// <summary>
    /// Summary description for NewCustomerC.
    /// </summary>
    public class NewCustomerC : Page {
        public static CultureInfo ci;
        private const string className = "NewCustomerC";
        public static ResourceManager rm;
        private readonly FactoryBLLC _theFactory = new FactoryBLLC();
        protected Button btnCancel;
        protected Button btnCity2Search;
        protected Button btnCitySearch;
        protected Button btRegCompany;
        protected CustomValidator CustomValidatorEstablishedFormat;
        protected CustomValidator CustomValidatorLastContactedFormat;
        protected DropDownList ddCompanyNace;
        protected DropDownList ddlCity1;
        protected DropDownList ddlCity2;
        protected DropDownList ddlPhoneNumber1Type;
        protected DropDownList ddlPhoneNumber2Type;
        protected DropDownList ddlPhoneNumber3Type;
        protected DropDownList ddlPhoneNumber4Type;
        protected DropDownList ddlstAddress1Country;
        protected DropDownList ddlstAddress2Country;
        protected DropDownList ddlstIDNumberType1;
        protected DropDownList ddlstIDNumberType2;
        protected DropDownList ddlstOrgStatus;
        private bool EN;
        protected Label lbAddr1InfoEN;
        protected Label lbAddress1EN;
        protected Label lbAddress1Info;
        protected Label lbCity1;
        protected Label lbCompanyInformation;
        protected Label lbCompanyName;
        protected Label lbCompanyNameEN;
        protected Label lbCompFunction;
        protected Label lbCompURL;
        protected Label lbDebtorCIID;
        protected Label lbEstablished;
        protected Label lbFaxNumber;
        protected Label lbIDNumber1;
        protected Label lbIDNumber2;
        protected Label lblAdditionalInfo;
        protected Label lblAddress1;
        protected Label lblAddress2;
        protected Label lblAddress2EN;
        protected Label lblAddress2Info;
        protected Label lblAddress2InfoEN;
        protected Label lbLastContacted;
        protected Label lblCity2;
        protected Label lblCity2Search;
        protected Label lblCitySearch;
        protected Label lblContact1Email;
        protected Label lblContact1FirstNameEN;
        protected Label lblContact1FirstNameNative;
        protected Label lblContact1SurnameEN;
        protected Label lblContact1SurnameNative;
        protected Label lblContact2Email;
        protected Label lblContact2FirstNameEN;
        protected Label lblContact2FirstNameNative;
        protected Label lblContact2SurnameEN;
        protected Label lblContact2SurnameNative;
        protected Label lblContact3Email;
        protected Label lblContact3FirstNameEN;
        protected Label lblContact3FirstNameNative;
        protected Label lblContact3SurnameEN;
        protected Label lblContact3SurnameNative;
        protected Label lblContactPerson1;
        protected Label lblContactPerson2;
        protected Label lblContactPerson3;
        protected Label lblCountry1;
        protected Label lblCountry2;
        protected Label lblCustomer;
        protected Label lblCustomerRemarks;
        protected Label lblEmail1;
        protected Label lblEmail2;
        protected Label lblEmail3;
        protected Label lblErrorMessage;
        protected Label lblNumbers;
        protected Label lblPhoneNumber3;
        protected Label lblPhoneNumber4;
        protected Label lblPhoneNumberType1;
        protected Label lblPhoneNumberType2;
        protected Label lblPhoneNumberType3;
        protected Label lblPhoneNumberType4;
        protected Label lblPostalCode2;
        protected Label lblPostBox2;
        protected Label lblRemarksEN;
        protected Label lblTheAddress1;
        protected Label lblTheAddress2;
        protected Label lbOrg_status;
        protected Label lbPhoneNumber;
        protected Label lbPhoneNumber1;
        protected Label lbPhoneNumber2;
        protected Label lbPostalCode;
        protected Label lbPostBox;
        protected Label lbType1;
        protected Label lbType2;
        protected RequiredFieldValidator RequiredFieldValidatorName;
        protected RequiredFieldValidator rfvCity;
        protected RequiredFieldValidator rfvCity2;
        protected TextBox tbAddress1EN;
        protected TextBox tbAddress1InfoEN;
        protected TextBox tbAddress1InfoN;
        protected TextBox tbAddress1Native;
        protected TextBox tbAddress2EN;
        protected TextBox tbAddress2InfoEN;
        protected TextBox tbAddress2InfoN;
        protected TextBox tbAddress2Native;
        protected TextBox tbCompanyCIID;
        protected TextBox tbCompanyEstablished;
        protected TextBox tbCompanyLContacted;
        protected TextBox tbCompanyNameEN;
        protected TextBox tbCompanyNameNative;
        protected TextBox tbCompanyURL;
        protected TextBox tbEmail;
        protected TextBox tbIDNumber1;
        protected TextBox tbIDNumber2;
        protected TextBox tbOrgStatus;
        protected TextBox tbPageID;
        protected TextBox tbPhoneNumber1;
        protected TextBox tbPhoneNumber2;
        protected TextBox tbPostalCode1;
        protected TextBox tbPostalCode2;
        protected TextBox tbPostBox1;
        protected TextBox tbPostBox2;
        protected TextBox txtAddress1EN;
        protected TextBox txtAddress1InfoEN;
        protected TextBox txtAddress1InfoNative;
        protected TextBox txtAddress1Native;
        protected TextBox txtAddress1PostalCode;
        protected TextBox txtAddress1PostBox;
        protected TextBox txtAddress2EN;
        protected TextBox txtAddress2InfoEN;
        protected TextBox txtAddress2InfoNative;
        protected TextBox txtAddress2Native;
        protected TextBox txtAddress2PostalCode;
        protected TextBox txtAddress2PostBox;
        protected TextBox txtCity2Search;
        protected TextBox txtCitySearch;
        protected TextBox txtCompanyCIID;
        protected TextBox txtCompanyEstablished;
        protected TextBox txtCompanyLastContacted;
        protected TextBox txtCompanyNameEN;
        protected TextBox txtCompanyNameNative;
        protected TextBox txtCompanyURL;
        protected TextBox txtContact1Email;
        protected TextBox txtContact1FirstNameEN;
        protected TextBox txtContact1FirstNameNative;
        protected TextBox txtContact1SurnameEN;
        protected TextBox txtContact1SurnameNative;
        protected TextBox txtContact2Email;
        protected TextBox txtContact2FirstNameEN;
        protected TextBox txtContact2FirstNameNative;
        protected TextBox txtContact2SurnameEN;
        protected TextBox txtContact2SurnameNative;
        protected TextBox txtContact3Email;
        protected TextBox txtContact3FirstNameEN;
        protected TextBox txtContact3FirstNameNative;
        protected TextBox txtContact3SurnameEN;
        protected TextBox txtContact3SurnameNative;
        protected TextBox txtCustomerRemarksEN;
        protected TextBox txtCustomerRemarksNative;
        protected TextBox txtEmail1;
        protected TextBox txtEmail2;
        protected TextBox txtEmail3;
        protected TextBox txtFaxNumber;
        protected TextBox txtIDNumber1;
        protected TextBox txtIDNumber2;
        protected TextBox txtOrgStatus;
        protected TextBox txtPhoneNumber;
        protected TextBox txtPhoneNumber1;
        protected TextBox txtPhoneNumber2;
        protected TextBox txtPhoneNumber3;
        protected TextBox txtPhoneNumber4;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = CigConfig.Configure("lookupsettings.ROSPageIDNewCustomerC");

            const string funcName = "Page_Load(object sender, System.EventArgs e)";
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            // check the current culture
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            lblErrorMessage.Visible = false;
            lblCountry1.Visible = false;
            lblCountry2.Visible = false;
            ddlstAddress1Country.Visible = false;
            ddlstAddress2Country.Visible = false;

            if (IsPostBack) {
                return;
            }
            // Put user code to initialize the page here

            var state = (string) Session["State"];
            var theOrder = (OrderBLLC) Session["RosOrder"];
            var customerType = (string) Session["CustomerType"];

            txtCompanyCIID.Enabled = false;

            CompanyBLLC theCustomer;

            InitDDBoxes();

            if (state == "Update") {
                if (Request.QueryString["user"] == null) {
                    //Query string is empty
                    theCustomer = null;
                } else {
                    //Get user from the database
                    var theFactory = new FactoryBLLC();
                    theCustomer = theFactory.GetCompany(Request.QueryString["user"].Trim());
                }
            } else {
                txtCompanyCIID.Visible = false;
                lbDebtorCIID.Visible = false;

                //Get the customer information from the session
                if (customerType == "Customer") {
                    theCustomer = (CompanyBLLC) theOrder.Customer;
                } else {
                    theCustomer = (CompanyBLLC) theOrder.Subject;
                }
            }

            if (theCustomer == null) {
                //Error getting the user from the database
                lblErrorMessage.Text = rm.GetString("txtDatabaseError", ci);
                lblErrorMessage.Visible = true;
                Logger.WriteToLog(className + " : " + funcName + "theCustomer is null", true);
            } else {
                InsertIntoCustomerInfo(theCustomer);
            }

            if (state == "Update") {
                LocalizeText(true);
            } else {
                LocalizeText(false);
            }
        }

        private void btRegCompany_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                lblErrorMessage.Visible = false;

                //Check if the dates are correct
                if (txtCompanyEstablished.Text.Length > 0) {
                    try {
                        DateTime.Parse(txtCompanyEstablished.Text);
                    } catch {
                        lblErrorMessage.Text = rm.GetString("txtErrorCompanyEstablishedFormat", ci);
                        lblErrorMessage.Visible = true;
                        return;
                    }
                }
                if (txtCompanyLastContacted.Text.Length > 0) {
                    try {
                        DateTime.Parse(txtCompanyLastContacted.Text);
                    } catch {
                        lblErrorMessage.Text = rm.GetString("txtErrorCompanyLastContacted", ci);
                        lblErrorMessage.Visible = true;
                        return;
                    }
                }

                //Get all the information from the textboxes

                var theCustomer = GetInfoFromPage();
                theCustomer.Type = CigConfig.Configure("lookupsettings.companyID");
                int creditInfoID = -1;
                var state = (string) Session["State"];

                if (state == "Update") {
                    //We only want to update this customers information
                    var lastPage = (string) Session["LastPage"];

                    creditInfoID = _theFactory.UpdateCompany(theCustomer);

                    if (creditInfoID != -1) {
                        if (lastPage != null) {
                            Server.Transfer(lastPage + ".aspx");
                        } else {
                            Server.Transfer("OrderCustomerSubjectSearch.aspx");
                        }
                    } else {
                        //Ekki t�kst a� uppf�ra kappann
                        lblErrorMessage.Text = rm.GetString("txtErrorUpdatingUser", ci);
                        lblErrorMessage.Visible = true;
                    }
                } else {
                    //We want to create this user
                    creditInfoID = _theFactory.CreateCompany(theCustomer);

                    if (creditInfoID != -1) {
                        //User was created

                        var customerType = (string) Session["CustomerType"];
                        var theOrder = (OrderBLLC) Session["RosOrder"];

                        theCustomer.CreditInfoID = creditInfoID;

                        if (customerType == "Customer") {
                            theOrder.Customer = theCustomer;
                            Session["CustomerSelected"] = true;
                        } else {
                            theOrder.Subject = theCustomer;
                            Session["SubjectSelected"] = true;
                        }

                        //Send the user back to the OrderCustomerSubjectSearch
                        Session["RosOrder"] = theOrder;
                        Server.Transfer("OrderCustomerSubjectSearch.aspx");
                    } else {
                        //Could not create new customer
                        lblErrorMessage.Text = rm.GetString("txtErrorCreatingUser", ci);
                        lblErrorMessage.Visible = true;
                    }
                }
            }
        }

        private void InitDDBoxes() {
            var theFactory = new FactoryBLLC();
            BindPhoneNumberTypes(theFactory);
            BindIDNumberTypes(theFactory);
            BindCountries(theFactory);
            BindCompanyFunction(theFactory);
            BindOrgStatus(theFactory);
        }

        private void BindPhoneNumberTypes(FactoryBLLC theFactory) {
            DataSet phoneTypeSet = theFactory.GetPhoneNumberTypes();
            ddlPhoneNumber1Type.DataSource = phoneTypeSet;
            ddlPhoneNumber1Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlPhoneNumber1Type.DataValueField = "NumberTypeID";
            ddlPhoneNumber1Type.DataBind();

            ddlPhoneNumber2Type.DataSource = phoneTypeSet;
            ddlPhoneNumber2Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlPhoneNumber2Type.DataValueField = "NumberTypeID";
            ddlPhoneNumber2Type.DataBind();

            ddlPhoneNumber3Type.DataSource = phoneTypeSet;
            ddlPhoneNumber3Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlPhoneNumber3Type.DataValueField = "NumberTypeID";
            ddlPhoneNumber3Type.DataBind();

            ddlPhoneNumber4Type.DataSource = phoneTypeSet;
            ddlPhoneNumber4Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlPhoneNumber4Type.DataValueField = "NumberTypeID";
            ddlPhoneNumber4Type.DataBind();
        }

        private void BindIDNumberTypes(FactoryBLLC theFactory) {
            var idNumberType = theFactory.GetIDNumberTypes();

            ddlstIDNumberType1.DataSource = idNumberType;
            ddlstIDNumberType1.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlstIDNumberType1.DataValueField = "NumberTypeID";
            ddlstIDNumberType1.DataBind();

            ddlstIDNumberType2.DataSource = idNumberType;
            ddlstIDNumberType2.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlstIDNumberType2.DataValueField = "NumberTypeID";
            ddlstIDNumberType2.DataBind();
        }

        private void BindCityList(DataSet citySet) {
            ddlCity1.DataSource = citySet;
            ddlCity1.DataTextField = EN ? "NameEN" : "NameNative";

            ddlCity1.DataValueField = "CityID";
            ddlCity1.DataBind();
        }

        private void BindCity2List(DataSet citySet) {
            ddlCity2.DataSource = citySet;
            ddlCity2.DataTextField = EN ? "NameEN" : "NameNative";

            ddlCity2.DataValueField = "CityID";
            ddlCity2.DataBind();
        }

        private void BindCountries(FactoryBLLC theFactory) {
            var countrySet = theFactory.GetAllCountries();
            ddlstAddress1Country.DataSource = countrySet;
            ddlstAddress1Country.DataTextField = EN ? "NameEN" : "NameNative";

            ddlstAddress1Country.DataValueField = "CountryID";
            ddlstAddress1Country.DataBind();

            ddlstAddress2Country.DataSource = countrySet;
            ddlstAddress2Country.DataTextField = EN ? "NameEN" : "NameNative";

            ddlstAddress2Country.DataValueField = "CountryID";
            ddlstAddress2Country.DataBind();
        }

        private void BindCompanyFunction(FactoryBLLC theFactory) {
            var companyFunctionSet = theFactory.GetCompanyNaceCodes();
            ddCompanyNace.DataSource = companyFunctionSet;
            ddCompanyNace.DataTextField = EN ? "DescriptionShortEN" : "DescriptionShortNative";
            ddCompanyNace.DataValueField = "FuncID";
            ddCompanyNace.DataBind();
        }

        private void BindOrgStatus(FactoryBLLC theFactory) {
            var orgStatusSet = theFactory.GetOrgStatus();
            ddlstOrgStatus.DataSource = orgStatusSet;
            ddlstOrgStatus.DataTextField = EN ? "nativeEN" : "nameNative";
            ddlstOrgStatus.DataValueField = "id";
            ddlstOrgStatus.DataBind();
        }

        private void InsertIntoCustomerInfo(CompanyBLLC theCustomer) {
            if (theCustomer.CreditInfoID != -1) {
                txtCompanyCIID.Text = "" + theCustomer.CreditInfoID;
            }
            txtCompanyNameEN.Text = theCustomer.NameEN;
            txtCompanyNameNative.Text = theCustomer.NameNative;
            txtCompanyEstablished.Text = theCustomer.Established.ToShortDateString();
            txtCompanyLastContacted.Text = theCustomer.LastContacted.ToShortDateString();
            txtCompanyURL.Text = theCustomer.WebPage;
            if (theCustomer.NaceID.Length > 0) {
                ddCompanyNace.SelectedValue = theCustomer.NaceID;
            }
            if (theCustomer.OrgStatus != -1) {
                ddlstOrgStatus.SelectedValue = "" + theCustomer.OrgStatus;
            }

            for (int i = 0; i < theCustomer.EmailAddress.Count; i++) {
                var email = (string) theCustomer.EmailAddress[i];

                if (i == 0) {
                    txtEmail1.Text = email;
                }
                if (i == 1) {
                    txtEmail2.Text = email;
                }
                if (i == 2) {
                    txtEmail3.Text = email;
                }
            }

            if (theCustomer.Address.Count > 0) {
                var theAddress = (AddressBLLC) theCustomer.Address[0];

                txtAddress1EN.Text = theAddress.StreetEN;
                txtAddress1Native.Text = theAddress.StreetNative;
                txtAddress1InfoEN.Text = theAddress.OtherInfoEN;
                txtAddress1InfoNative.Text = theAddress.OtherInfoNative;
                txtAddress1PostalCode.Text = theAddress.PostalCode;
                txtAddress1PostBox.Text = theAddress.PostBox;
                if (theAddress.CityID != -1) {
                    var cityName = _theFactory.GetCityName(theAddress.CityID, EN);
                    if (!string.IsNullOrEmpty(cityName)) {
                        ddlCity1.Items.Add(new ListItem(cityName, theAddress.CityID.ToString()));
                        ddlCity1.SelectedValue = theAddress.CityID.ToString();
                    }
                }
                if (theAddress.CountryID != -1) {
                    ddlstAddress1Country.SelectedValue = "" + theAddress.CountryID;
                }
            }

            if (theCustomer.Address.Count > 1) {
                var theAddress = (AddressBLLC) theCustomer.Address[1];

                txtAddress2EN.Text = theAddress.StreetEN;
                txtAddress2Native.Text = theAddress.StreetNative;
                txtAddress2InfoEN.Text = theAddress.OtherInfoEN;
                txtAddress2InfoNative.Text = theAddress.OtherInfoNative;
                txtAddress2PostalCode.Text = theAddress.PostalCode;
                txtAddress2PostBox.Text = theAddress.PostBox;
                if (theAddress.CityID != -1) {
                    string cityName = _theFactory.GetCityName(theAddress.CityID, EN);
                    if (!string.IsNullOrEmpty(cityName)) {
                        ddlCity2.Items.Add(new ListItem(cityName, theAddress.CityID.ToString()));
                        ddlCity2.SelectedValue = theAddress.CityID.ToString();
                    }
                }
                if (theAddress.CountryID != -1) {
                    ddlstAddress2Country.SelectedValue = "" + theAddress.CountryID;
                }
            }

            if (theCustomer.IDNumbers.Count > 0) {
                var theID = (IDNumberBLLC) theCustomer.IDNumbers[0];
                txtIDNumber1.Text = theID.Number;
                ddlstIDNumberType1.SelectedValue = "" + theID.NumberTypeID;
            }
            if (theCustomer.IDNumbers.Count > 1) {
                var theID = (IDNumberBLLC) theCustomer.IDNumbers[1];
                txtIDNumber2.Text = theID.Number;
                ddlstIDNumberType2.SelectedValue = "" + theID.NumberTypeID;
            }

            if (theCustomer.TelephoneNumbers.Count > 0) {
                var thePhone = (PhoneNumberBLLC) theCustomer.TelephoneNumbers[0];
                txtPhoneNumber1.Text = thePhone.Number;
                ddlPhoneNumber1Type.SelectedValue = "" + thePhone.NumberTypeID;
            }
            if (theCustomer.TelephoneNumbers.Count > 1) {
                var thePhone = (PhoneNumberBLLC) theCustomer.TelephoneNumbers[1];
                txtPhoneNumber2.Text = thePhone.Number;
                ddlPhoneNumber2Type.SelectedValue = "" + thePhone.NumberTypeID;
            }
            if (theCustomer.TelephoneNumbers.Count > 2) {
                var thePhone = (PhoneNumberBLLC) theCustomer.TelephoneNumbers[2];
                txtPhoneNumber3.Text = thePhone.Number;
                ddlPhoneNumber3Type.SelectedValue = "" + thePhone.NumberTypeID;
            }
            if (theCustomer.TelephoneNumbers.Count > 3) {
                var thePhone = (PhoneNumberBLLC) theCustomer.TelephoneNumbers[3];
                txtPhoneNumber4.Text = thePhone.Number;
                ddlPhoneNumber4Type.SelectedValue = "" + thePhone.NumberTypeID;
            }

            txtCustomerRemarksEN.Text = theCustomer.RemarksEN;
            txtCustomerRemarksNative.Text = theCustomer.RemarksNative;

            for (int i = 0; i < theCustomer.ContactPersons.Count; i++) {
                var theContact = (ContactPersonBLLC) theCustomer.ContactPersons[i];

                if (i == 0) {
                    txtContact1Email.Text = theContact.Email;
                    txtContact1FirstNameEN.Text = theContact.FirstNameEN;
                    txtContact1FirstNameNative.Text = theContact.FirstNameNative;
                    txtContact1SurnameEN.Text = theContact.LastNameEN;
                    txtContact1SurnameNative.Text = theContact.LastNameNative;
                }
                if (i == 1) {
                    txtContact2Email.Text = theContact.Email;
                    txtContact2FirstNameEN.Text = theContact.FirstNameEN;
                    txtContact2FirstNameNative.Text = theContact.FirstNameNative;
                    txtContact2SurnameEN.Text = theContact.LastNameEN;
                    txtContact2SurnameNative.Text = theContact.LastNameNative;
                }
                if (i == 2) {
                    txtContact3Email.Text = theContact.Email;
                    txtContact3FirstNameEN.Text = theContact.FirstNameEN;
                    txtContact3FirstNameNative.Text = theContact.FirstNameNative;
                    txtContact3SurnameEN.Text = theContact.LastNameEN;
                    txtContact3SurnameNative.Text = theContact.LastNameNative;
                }
            }
        }

        private CompanyBLLC GetInfoFromPage() {
            var theCustomer = new CompanyBLLC();

//			if(this.txtCompanyCIID.Text.Length > 0)
//				theCustomer.CreditInfoID = int.Parse(this.txtCompanyCIID.Text);

            if (Request.QueryString["user"] != null) {
                theCustomer.CreditInfoID = int.Parse(Request.QueryString["user"].Trim());
            }
            theCustomer.NameEN = txtCompanyNameEN.Text;
            theCustomer.NameNative = txtCompanyNameNative.Text;
            theCustomer.Established = DateTime.Parse(txtCompanyEstablished.Text);
            theCustomer.LastContacted = DateTime.Parse(txtCompanyLastContacted.Text);
            theCustomer.WebPage = txtCompanyURL.Text;
            theCustomer.NaceID = ddCompanyNace.SelectedValue;
            theCustomer.OrgStatus = int.Parse(ddlstOrgStatus.SelectedValue);

            if (txtEmail1.Text.Length > 0) {
                theCustomer.EmailAddress.Add(txtEmail1.Text);
            }
            if (txtEmail2.Text.Length > 0) {
                theCustomer.EmailAddress.Add(txtEmail2.Text);
            }
            if (txtEmail3.Text.Length > 0) {
                theCustomer.EmailAddress.Add(txtEmail3.Text);
            }

            if (txtAddress1EN.Text.Length > 0 || txtAddress1Native.Text.Length > 0 || txtAddress1InfoEN.Text.Length > 0 ||
                txtAddress1InfoNative.Text.Length > 0 || txtAddress1PostalCode.Text.Length > 0 ||
                txtAddress1PostBox.Text.Length > 0) {
                var theAddress = new AddressBLLC
                                 {
                                     StreetEN = txtAddress1EN.Text,
                                     StreetNative = txtAddress1Native.Text,
                                     InfoEN = txtAddress1InfoEN.Text,
                                     InfoNative = txtAddress1InfoNative.Text,
                                     PostalCode = txtAddress1PostalCode.Text,
                                     PostBox = txtAddress1PostBox.Text
                                 };
                if (ddlCity1.SelectedItem.Value.Trim() != "") {
                    theAddress.CityID = int.Parse(ddlCity1.SelectedValue);
                }
                theAddress.CountryID = int.Parse(ddlstAddress1Country.SelectedValue);
                theCustomer.Address.Add(theAddress);
            }
            if (txtAddress2EN.Text.Length > 0 || txtAddress2Native.Text.Length > 0 || txtAddress2InfoEN.Text.Length > 0 ||
                txtAddress2InfoNative.Text.Length > 0 || txtAddress2PostalCode.Text.Length > 0 ||
                txtAddress2PostBox.Text.Length > 0) {
                var theAddress = new AddressBLLC
                                 {
                                     StreetEN = txtAddress2EN.Text,
                                     StreetNative = txtAddress2Native.Text,
                                     InfoEN = txtAddress2InfoEN.Text,
                                     InfoNative = txtAddress2InfoNative.Text,
                                     PostalCode = txtAddress2PostalCode.Text,
                                     PostBox = txtAddress2PostBox.Text
                                 };
                if (ddlCity2.SelectedItem.Value.Trim() != "") {
                    theAddress.CityID = int.Parse(ddlCity2.SelectedValue);
                }
                theAddress.CountryID = int.Parse(ddlstAddress2Country.SelectedValue);
                theCustomer.Address.Add(theAddress);
            }

            if (txtIDNumber1.Text.Length > 0) {
                var theID = new IDNumberBLLC
                            {
                                Number = txtIDNumber1.Text,
                                NumberTypeID = int.Parse(ddlstIDNumberType1.SelectedValue)
                            };
                theCustomer.IDNumbers.Add(theID);
            }
            if (txtIDNumber2.Text.Length > 0) {
                var theID = new IDNumberBLLC
                            {
                                Number = txtIDNumber2.Text,
                                NumberTypeID = int.Parse(ddlstIDNumberType2.SelectedValue)
                            };
                theCustomer.IDNumbers.Add(theID);
            }

            if (txtPhoneNumber1.Text.Length > 0) {
                var thePhone = new PhoneNumberBLLC
                               {
                                   Number = txtPhoneNumber1.Text,
                                   NumberTypeID = int.Parse(ddlPhoneNumber1Type.SelectedValue)
                               };
                theCustomer.TelephoneNumbers.Add(thePhone);
            }
            if (txtPhoneNumber2.Text.Length > 0) {
                var thePhone = new PhoneNumberBLLC
                               {
                                   Number = txtPhoneNumber2.Text,
                                   NumberTypeID = int.Parse(ddlPhoneNumber2Type.SelectedValue)
                               };
                theCustomer.TelephoneNumbers.Add(thePhone);
            }
            if (txtPhoneNumber3.Text.Length > 0) {
                var thePhone = new PhoneNumberBLLC
                               {
                                   Number = txtPhoneNumber3.Text,
                                   NumberTypeID = int.Parse(ddlPhoneNumber3Type.SelectedValue)
                               };
                theCustomer.TelephoneNumbers.Add(thePhone);
            }
            if (txtPhoneNumber4.Text.Length > 0) {
                var thePhone = new PhoneNumberBLLC
                               {
                                   Number = txtPhoneNumber4.Text,
                                   NumberTypeID = int.Parse(ddlPhoneNumber4Type.SelectedValue)
                               };
                theCustomer.TelephoneNumbers.Add(thePhone);
            }

            theCustomer.RemarksEN = txtCustomerRemarksEN.Text;
            theCustomer.RemarksNative = txtCustomerRemarksNative.Text;

            if (txtContact1Email.Text.Length > 0 || txtContact1FirstNameEN.Text.Length > 0 ||
                txtContact1FirstNameNative.Text.Length > 0 ||
                txtContact1SurnameEN.Text.Length > 0 || txtContact1SurnameNative.Text.Length > 0) {
                var theContact = new ContactPersonBLLC
                                 {
                                     Email = txtContact1Email.Text,
                                     FirstNameEN = txtContact1FirstNameEN.Text,
                                     FirstNameNative = txtContact1FirstNameNative.Text,
                                     LastNameEN = txtContact1SurnameEN.Text,
                                     LastNameNative = txtContact1SurnameNative.Text
                                 };
                theCustomer.ContactPersons.Add(theContact);
            }

            if (txtContact2Email.Text.Length > 0 || txtContact2FirstNameEN.Text.Length > 0 ||
                txtContact2FirstNameNative.Text.Length > 0 ||
                txtContact2SurnameEN.Text.Length > 0 || txtContact2SurnameNative.Text.Length > 0) {
                var theContact = new ContactPersonBLLC
                                 {
                                     Email = txtContact2Email.Text,
                                     FirstNameEN = txtContact2FirstNameEN.Text,
                                     FirstNameNative = txtContact2FirstNameNative.Text,
                                     LastNameEN = txtContact2SurnameEN.Text,
                                     LastNameNative = txtContact2SurnameNative.Text
                                 };
                theCustomer.ContactPersons.Add(theContact);
            }

            if (txtContact3Email.Text.Length > 0 || txtContact3FirstNameEN.Text.Length > 0 ||
                txtContact3FirstNameNative.Text.Length > 0 ||
                txtContact3SurnameEN.Text.Length > 0 || txtContact3SurnameNative.Text.Length > 0) {
                var theContact = new ContactPersonBLLC
                                 {
                                     Email = txtContact3Email.Text,
                                     FirstNameEN = txtContact3FirstNameEN.Text,
                                     FirstNameNative = txtContact3FirstNameNative.Text,
                                     LastNameEN = txtContact3SurnameEN.Text,
                                     LastNameNative = txtContact3SurnameNative.Text
                                 };
                theCustomer.ContactPersons.Add(theContact);
            }

            return theCustomer;
        }

        private void LocalizeText(bool update) {
            //	lblUndeliveredOrders.Text = rm.GetString("txtUndeliveredOrders",ci);
            btRegCompany.Text = update ? rm.GetString("txtUpdate", ci) : rm.GetString("txtCreateNewCustomer", ci);

            CustomValidatorLastContactedFormat.ErrorMessage = rm.GetString("ErrorLastContactedDateFormat", ci);
            CustomValidatorEstablishedFormat.ErrorMessage = rm.GetString("ErrorEstablishedDateFormat", ci);

            lbCompanyInformation.Text = rm.GetString("txtCompanyInfo", ci);
            lbDebtorCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lbCompanyName.Text = rm.GetString("txtCompanyName", ci);
            lbCompanyNameEN.Text = rm.GetString("txtCompanyNameEN", ci);
            lbEstablished.Text = rm.GetString("txtEstablished", ci);
            lbLastContacted.Text = rm.GetString("txtLastContacted", ci);
            lbCompURL.Text = rm.GetString("txtURL", ci);
            lbCompFunction.Text = rm.GetString("txtCompanyFunction", ci);
            lbOrg_status.Text = rm.GetString("txtOrgStatus", ci);
            lblEmail1.Text = rm.GetString("txtEmailPrimary", ci);
            lblEmail2.Text = rm.GetString("txtEmailSecondary", ci);
            lblEmail3.Text = rm.GetString("txtEmailThird", ci);
            lblTheAddress1.Text = rm.GetString("txtAddress1", ci);
            lblAddress1.Text = rm.GetString("txtAddress", ci);
            lbAddress1EN.Text = rm.GetString("txtAddressEN", ci);
            lbAddress1Info.Text = rm.GetString("txtAddressInfo", ci);
            lbAddr1InfoEN.Text = rm.GetString("txtAddressInfoEN", ci);
            lbPostalCode.Text = rm.GetString("txtPostalCode", ci);
            lbPostBox.Text = rm.GetString("txtPostBox", ci);
            lbCity1.Text = rm.GetString("txtCity", ci);
            lblCountry1.Text = rm.GetString("txtCountry", ci);
            lblTheAddress2.Text = rm.GetString("txtAddress2", ci);
            lblAddress2.Text = rm.GetString("txtAddress", ci);
            lblAddress2EN.Text = rm.GetString("txtAddressEN", ci);
            lblAddress2Info.Text = rm.GetString("txtAddressInfo", ci);
            lblAddress2InfoEN.Text = rm.GetString("txtAddressInfoEN", ci);
            lblPostalCode2.Text = rm.GetString("txtPostalCode", ci);
            lblPostBox2.Text = rm.GetString("txtPostBox", ci);
            lblCity2.Text = rm.GetString("txtCity", ci);
            lblCountry2.Text = rm.GetString("txtCountry", ci);

            lblNumbers.Text = rm.GetString("lblNumbers", ci);
            lbIDNumber1.Text = rm.GetString("txtIDNumber1", ci);
            lbIDNumber2.Text = rm.GetString("txtIDNumber2", ci);
            lbPhoneNumber1.Text = rm.GetString("txtPhoneNumber1", ci);
            lbPhoneNumber2.Text = rm.GetString("txtPhoneNumber2", ci);
            lblPhoneNumber3.Text = rm.GetString("txtPhoneNumber3", ci);
            lblPhoneNumber4.Text = rm.GetString("txtPhoneNumber4", ci);
            lbType1.Text = rm.GetString("txtType", ci);
            lbType2.Text = rm.GetString("txtType", ci);
            lblPhoneNumberType1.Text = rm.GetString("txtType", ci);
            lblPhoneNumberType2.Text = rm.GetString("txtType", ci);
            lblPhoneNumberType3.Text = rm.GetString("txtType", ci);
            lblPhoneNumberType4.Text = rm.GetString("txtType", ci);

            lblAdditionalInfo.Text = rm.GetString("lblAdditionalInfo", ci);
            lblCustomerRemarks.Text = rm.GetString("txtRemarksNative", ci);
            lblRemarksEN.Text = rm.GetString("txtRemarksEN", ci);
            // contact #1
            lblContactPerson1.Text = rm.GetString("txtContactPerson", ci);
            lblContact1FirstNameNative.Text = rm.GetString("txtFirstName", ci);
            lblContact1FirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lblContact1SurnameNative.Text = rm.GetString("txtSurName", ci);
            lblContact1SurnameEN.Text = rm.GetString("txtSurNameEN", ci);
            lblContact1Email.Text = rm.GetString("txtEmail", ci);
            // contact #2
            lblContactPerson2.Text = rm.GetString("txtContactPerson", ci);
            lblContact2FirstNameNative.Text = rm.GetString("txtFirstName", ci);
            lblContact2FirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lblContact2SurnameNative.Text = rm.GetString("txtSurName", ci);
            lblContact2SurnameEN.Text = rm.GetString("txtSurNameEN", ci);
            lblContact2Email.Text = rm.GetString("txtEmail", ci);
            // contact #3
            lblContactPerson3.Text = rm.GetString("txtContactPerson", ci);
            lblContact3FirstNameNative.Text = rm.GetString("txtFirstName", ci);
            lblContact3FirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lblContact3SurnameNative.Text = rm.GetString("txtSurName", ci);
            lblContact3SurnameEN.Text = rm.GetString("txtSurNameEN", ci);
            lblContact3Email.Text = rm.GetString("txtEmail", ci);

            btnCancel.Text = rm.GetString("txtCancel", ci);

            rfvCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvCity2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblCitySearch.Text = rm.GetString("txtSearch", ci);
            lblCity2Search.Text = rm.GetString("txtSearch", ci);
//			btRegCompany.Text = rm.GetString("txtRegisterCompany",ci);

            RequiredFieldValidatorName.ErrorMessage = rm.GetString("txtErrorNameMissing", ci);
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            var theOrder = (OrderBLLC) Session["RosOrder"];

            var customerType = (string) Session["CustomerType"];

            if (customerType == "Customer") {
                theOrder.Customer = null;
            }
            if (customerType == "Subject") {
                theOrder.Subject = null;
            }

            Session["RosOrder"] = theOrder;

            var lastPage = (string) Session["LastPage"];
            if (lastPage != null) {
                Server.Transfer(lastPage + ".aspx");
            } else {
                Server.Transfer("OrderCustomerSubjectSearch.aspx");
            }
        }

        private void CustomValidatorLastContactedFormat_ServerValidate(object source, ServerValidateEventArgs args) {
            if (args.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(args.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    args.IsValid = true;
                } catch (Exception) {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        private void CustomValidatorEstablishedFormat_ServerValidate(object source, ServerValidateEventArgs args) {
            if (args.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(args.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    args.IsValid = true;
                } catch (Exception) {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        private void btnCitySearch_Click(object sender, EventArgs e) {
            if (txtCitySearch.Text.Trim() != "") {
                DataSet dsCity = _theFactory.GetCityListAsDataSet(txtCitySearch.Text, EN);
                BindCityList(dsCity);
            }
        }

        private void btnCity2Search_Click(object sender, EventArgs e) {
            if (txtCity2Search.Text.Trim() != "") {
                DataSet dsCity = _theFactory.GetCityListAsDataSet(txtCity2Search.Text, EN);
                BindCity2List(dsCity);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// 		/// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.CustomValidatorEstablishedFormat.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(
                    this.CustomValidatorEstablishedFormat_ServerValidate);
            this.CustomValidatorLastContactedFormat.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(
                    this.CustomValidatorLastContactedFormat_ServerValidate);
            this.btnCitySearch.Click += new System.EventHandler(this.btnCitySearch_Click);
            this.btnCity2Search.Click += new System.EventHandler(this.btnCity2Search_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btRegCompany.Click += new System.EventHandler(this.btRegCompany_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}