﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logon.aspx.cs" Inherits="cb3.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CIG-Login</title>
    <link href="css/LoginPageStyle.css" rel="stylesheet" type="text/css" />
    <link href="css/Bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div id="loginPanelBox">
            <div id="loginBox">
                <asp:Panel ID="LoginPanel" runat="server" DefaultButton="btnLogin">
                    <div class="loginHeader">
                        <img src="images/CI_logo.png" alt="CI logo" />
                    </div>
                    <asp:UpdatePanel ID="pnlLoginBox" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="insideText">
                                <h3>Login</h3>
                                <div class="arialR14Black">Username:</div>
                                <div>
                                    <asp:TextBox ID="tbUserName" TabIndex="1" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="valUsername" Display="None" EnableClientScript="false" ControlToValidate="tbUsername" ErrorMessage="Username" runat="server">*</asp:RequiredFieldValidator>
                                <br />
                                <div id="lbPassword" runat="server" class="arialR14Black">Password:</div>
                                <div>
                                    <asp:TextBox ID="tbPassword" TabIndex="2" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="valPassword" Display="None" EnableClientScript="false" ControlToValidate="tbPassword" ErrorMessage="Password" runat="server">*</asp:RequiredFieldValidator>


                                <div class="smallbtn" id="loginbtn">
                                    <asp:Button ID="btnLogin" runat="server" TabIndex="4" Text="Log in" OnClick="btnLogin_Click" />
                                </div>
                            </div>
                            

                            <div id="errormessage" runat="server" class="errormessage" visible="false">
                                <div class="<%=alertCSSStyle %>" id="errorbox" role="alert">
                                    <asp:Literal ID="loginErrorMessage" runat="server" />
                                </div>
                            </div>
                        </ContentTemplate>

                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnLogin" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
                
                <div runat="server" id="passwordExpired" visible="false">
                    <div>Enter the password fields below to change your passowrd</div>
                    
                    <div class="arialR14Black">New Password:</div>
                    <div>
                        <asp:TextBox ID="tbNewPass"  TextMode="Password" TabIndex="5" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <br>
                    
                        
                    <div class="arialR14Black">Confirm New Password:</div>
                    <div>
                        <asp:TextBox ID="tbReEnterNewPass"  TextMode="Password" TabIndex="6" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <br>
                    
                    <div class="smallbtn" style="float: none" >
                        <asp:Button ID="ChangePass" runat="server" TabIndex="7" Text="Change Password" OnClick="ChangePass_Click"/>
                    </div>

                
                </div>
                
            </div>
            
           

            <div id="LeftBox">
                <%=leftBoxText %>
            </div>

            <div id="rightBox">
                <%=rightBoxText %>
            </div>
        </div>
    </form>

</body>
</html>
