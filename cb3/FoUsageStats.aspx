<%@ Page language="c#" Codebehind="FoUsageStats.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.FoUsageStats" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="new_user_controls/FoOptions.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="new_user_controls/UserInfo.ascx" %>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en" >
<html>
  <head>
		<title>Creditinfo</title>
<meta content="Microsoft Visual Studio .NET 7.1" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><LINK href="css/FoCIGStyles.css" type=text/css rel=stylesheet >
<script language=javascript> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						frmUsageStatistics.btnSearch.click(); 
					}
				}
		
				function SetFormFocus()
				{
					document.frmUsageStatistics.txtDateFrom.focus();
				}
				
		</script>
</head>
<body style="BACKGROUND-IMAGE: url(img/mainback.gif)" leftMargin=0 
onload=SetFormFocus() rightMargin=0 
ms_positioning="GridLayout">
<form id=frmUsageStatistics method=post runat="server">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td><uc1:head id=Head2 runat="server"></uc1:head></td></tr>
  <tr>
    <td>
      <table cellSpacing=0 cellPadding=0 width="100%" align=center border=0 
      >
        <tr>
          <td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" 
          width="50%"></td>
          <td>
            <table cellSpacing=0 cellPadding=0 width=640 align=center 
            bgColor=#951e16 border=0>
              <tr>
                <td bgColor=#951e16><span 
                  style="WIDTH: 3px"></span></td>
                <td bgColor=#951e16><uc1:panelbar id=PanelBar1 runat="server"></uc1:panelbar></td>
                <td align=right bgColor=#951e16><uc1:language id=Language1 runat="server"></uc1:language></td></tr></table></td>
          <td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" 
          width="50%"></td></tr></table></td></tr>
  <tr>
    <td>
      <table cellSpacing=0 cellPadding=0 width=640 align=center bgColor=white 
      border=0>
        <tr>
          <td>
            <p>
            <table id=tbDefault cellSpacing=0 cellPadding=0 width="100%" 
            align=center>
              <tr>
                <td>
                  <table cellSpacing=0 cellPadding=0 width="97%" align=center 
                  border=0>
                    <tr>
                      <td><uc1:userinfo id=UserInfo1 runat="server"></uc1:userinfo></td>
                      <td align=right><uc1:fooptions id=FoOptions1 runat="server"></uc1:fooptions></td></tr></table></td></tr>
              <tr>
                <td>
                  <table cellSpacing=0 cellPadding=0 width="97%" align=center 
                  border=0>
                    <tr>
                      <td class=pageheader colSpan=4><asp:label id=lblHeader runat="server" cssclass="HeadMain">[Usage statistics]</asp:label></td></tr>
                    <tr>
                      <td colSpan=4 height=15></td></tr>
                    <tr>
                      <td><asp:label id=lblDateFrom runat="server">[Date from]</asp:label></td>
                      <td><asp:label id=lblDateTo runat="server">[Date to]</asp:label></td>
                      <td><asp:label id="lblBillable" runat="server">[Billable]</asp:label></td>
                     </tr>
                    <tr>
                      <td><asp:textbox id=txtDateFrom runat="server"></asp:textbox><asp:requiredfieldvalidator id=rfvDateFrom runat="server" ControlToValidate="txtDateFrom" Display="Dynamic">*</asp:requiredfieldvalidator><asp:comparevalidator id=cpvDateFrom runat="server" ControlToValidate="txtDateFrom" Display="Dynamic" Operator="DataTypeCheck" Type="Date">*</asp:comparevalidator></td>
                      <td><asp:textbox id=txtDateTo runat="server"></asp:textbox><asp:requiredfieldvalidator id=rfvDateTo runat="server" ControlToValidate="txtDateTo">*</asp:requiredfieldvalidator><asp:comparevalidator id=cpvDateTo runat="server" ControlToValidate="txtDateTo" Operator="DataTypeCheck" Type="Date">*</asp:comparevalidator></td>
                      <td><asp:radiobuttonlist id="rblBillable" runat="server" cssclass="radio" repeatdirection="Horizontal">
<asp:ListItem Value="True">[True]</asp:ListItem>
<asp:ListItem Value="False">[False]</asp:ListItem>
<asp:ListItem Value="null" Selected="True">[Both]</asp:ListItem>
</asp:radiobuttonlist></td>
                      </tr>
                    <tr>
                      <td height=15></td></tr>
                    <tr>
                      <td colspan=2><asp:label id=lblMessage runat="server" visible="False"></asp:label><asp:validationsummary id=valSummary runat="server"></asp:validationsummary></td>
                      <td vAlign=top align=right>
                        <div class=AroundButtonSmallMargin 
                        style="WIDTH: 90px; HEIGHT: 10px"><asp:button id=btnSearch runat="server" cssclass="RegisterButton" text="[Search]"></asp:button></div></td></tr>
                    <tr>
                      <td height=15></td></tr></table>
                  <table id=tblInformation cellSpacing=0 cellPadding=0 
                  width="97%" align=center border=0 
                  runat="server">
                    <tr>
                      <td style="HEIGHT: 15px" align=center bgColor=#951e16 
                      ></td></tr>
                    <tr>
                      <td>
                        <table cellSpacing=0 cellPadding=0 width="100%" 
                        >
                          <tr>
                            <td colSpan=2><asp:label id=lblSubscriberTitle runat="server">[Subscriber]</asp:label></td></tr>
                          <tr>
                            <td colSpan=2><asp:label id=lblSubscriber runat="server" Font-Bold="True"></asp:label></td></tr>
                          <tr>
                            <td colSpan=2><asp:label id=lblUsers runat="server">[Users]</asp:label></td>
                            <td><asp:label id=lblUserCount runat="server"></asp:label></td></tr>
                          <tr>
                            <td colSpan=2><asp:label id=lblProducts runat="server">[Products]</asp:label></td>
                            <td><asp:label id=lblProductsCount runat="server"></asp:label></td></tr>
                          <tr>
                            <td width=20></td>
                            <td><asp:label id=lblProductName runat="server"></asp:label></td>
                            <td><asp:label id=lblProductCount runat="server"></asp:label></td></tr></table></td></tr>
                    <tr>
                      <td height=15></td></tr>
                    <tr>
                      <td style="HEIGHT: 15px" align=center bgColor=#951e16 
                      ></td></tr>
                    <tr>
                      <td height=15></td></tr>
                    <tr>
                      <td>
                        <table width="100%">
                          <tr>
                            <td><asp:dropdownlist id=ddlProducts runat="server" AutoPostBack="True"></asp:dropdownlist></td>
                            <td style="WIDTH: 255px"><asp:dropdownlist id=ddlUsers runat="server" AutoPostBack="True"></asp:dropdownlist></td>
                            <td style="WIDTH: 105px" 
                          ></td></tr></table><asp:datagrid id=dgrUsage runat="server" pagesize="3" width="100%" bordercolor="Gray" borderstyle="None" borderwidth="1px" backcolor="White" cellpadding="4" allowsorting="True" autogeneratecolumns="False">
<footerstyle forecolor="#330099" backcolor="#FFFFCC">
</FooterStyle>

<selecteditemstyle font-bold="True" forecolor="#663399" backcolor="#FFCC66">
</SelectedItemStyle>

<alternatingitemstyle backcolor="#E0E0E0">
</AlternatingItemStyle>

<itemstyle forecolor="Black" backcolor="White">
</ItemStyle>

<headerstyle font-bold="True" forecolor="White" backcolor="#895A4F">
</HeaderStyle>

<columns>
<asp:BoundColumn DataField="TypeEN" SortExpression="TypeEN" HeaderText="[Product]"></asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="TypeNative" SortExpression="TypeNative" HeaderText="[Product]"></asp:BoundColumn>
<asp:BoundColumn DataField="username" SortExpression="username" HeaderText="[Username]">
<headerstyle width="250px">
</HeaderStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="Type_count" SortExpression="Type_count" HeaderText="[Count]">
<headerstyle width="100px">
</HeaderStyle>
</asp:BoundColumn>
</Columns>

<pagerstyle horizontalalign="Center" forecolor="#330099" backcolor="#FFFFCC">
</PagerStyle>
																	</asp:datagrid></td></tr></table>
													<!-- Content ends --></td></tr></table></p></td></tr>
        <tr>
          <td>&nbsp; &nbsp;<asp:hyperlink id=hlMail runat="server" navigateurl="mailto:info@creditinfo.com.mt">e-mail:info@creditinfo.com.mt</asp:hyperlink> 
          </td></tr></table></td></tr>
  <tr>
    <td bgColor=transparent height=6></td></tr>
  <tr>
    <td align=center><uc1:footer id="Footer1" runat="server"></uc1:footer></td></tr></table></form>
	</body>
</html>
