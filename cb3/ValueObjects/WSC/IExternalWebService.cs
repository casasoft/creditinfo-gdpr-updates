/*
 * $Header: /CreditInfoGroup_ge/CreditInfoGroup.root/CreditInfoGroup/ValueObjects/WSC/IExternalWebService.cs 1     26.03.07 17:44 J.ondrak $Log:
 */
using System.Collections;

using Nullables;

namespace ValueObjects.WSC
{
	/// <summary>
	/// The ExternalWebService class represents connection parameters for an external web service.
	/// <para>Copyright (C)2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
	/// </summary>
	public interface IExternalWebService
	{
		/// <summary>
		/// ID of the note - sequence number
		/// </summary>
		int WebServiceId
		{
			get;
			set;
		}

		/// <summary>
		/// Web service name in native language.
		/// </summary>
		string NameNative
		{
			get;
			set;
		}

		/// <summary>
		/// Web service name in English.
		/// </summary>
		string NameEN
		{
			get;
			set;
		}

		/// <summary>
		/// Path of the web service.
		/// </summary>
		string Path
		{
			get;
			set;
		}

		/// <summary>
		/// Native description of the web service.
		/// </summary>
		string DescriptionNative
		{
			get;
			set;
		}

		/// <summary>
		/// English description of the web service.
		/// </summary>
		string DescriptionEN
		{
			get;
			set;
		}

		/// <summary>
		/// Unique identifier for the master user of the web service.
		/// </summary>
		int MasterUserId
		{
			get;
			set;
		}
		/// <summary>
		/// Master user of the web service.
		/// </summary>
		IExternalWebServiceAccess MasterUser
		{
			get;
			set;
		}

		/// <summary>
		/// Type of web service (.NET, Java, ASP).
		/// </summary>
		int Type
		{
			get;
			set;
		}

		/// <summary>
		/// A short code used to find the web service.
		/// </summary>
		string ShortCode
		{
			get;
			set;
		}

		/// <summary>
		/// Xslt filename used to transform the response.
		/// </summary>
		string XsltName
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets time of disablement.
		/// </summary>
		NullableDateTime Disabled
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets unique id of the disabling user.
		/// </summary>
		int DisabledBy
		{
			get;
			set;
		}
		
		/// <summary>
		/// All access objects on this web service.
		/// </summary>
		IList ExternalWebServiceAccess
		{
			get;
			set;
		}
	}
}
