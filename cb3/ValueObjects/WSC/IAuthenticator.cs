/*
 * $Header: /CreditInfoGroup_ge/CreditInfoGroup.root/CreditInfoGroup/ValueObjects/WSC/IAuthenticator.cs 1     26.03.07 17:44 J.ondrak $Log:
 */

using System;

namespace ValueObjects.WSC
{
	/// <summary>
	/// Summary description for IAuthenticator.
	/// <para>Copyright (C)2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
	/// </summary>
	public interface IAuthenticator
	{
		/// <summary>
		/// Unique user name.
		/// </summary>
		string Username
		{
			get;
			set;
		}

		/// <summary>
		/// The user password.
		/// </summary>
		string Password
		{
			get;
			set;
		}
	}
}
