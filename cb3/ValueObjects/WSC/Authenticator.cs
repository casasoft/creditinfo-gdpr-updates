/* 
 * $Header: /CreditInfoGroup_ge/CreditInfoGroup.root/CreditInfoGroup/ValueObjects/WSC/Authenticator.cs 1     26.03.07 17:44 J.ondrak $
 * $Log: /CreditInfoGroup_ge/CreditInfoGroup.root/CreditInfoGroup/ValueObjects/WSC/Authenticator.cs $ 
 * 
 * 1     26.03.07 17:44 J.ondrak
 * 
 * 3     4.08.05 8:41 Haukur
 * Commenting
 * 
 * 2     8.07.05 9:41 Gudjon
 * Adding VSS comment.
 */


using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.WSC
{
	/// <summary>
	/// Summary description for Authenticator.
	/// </summary>
	public class Authenticator : CigBaseValueObject, IAuthenticator
	{
		private string _username;
		private string _password;

		/// <summary>
		/// Default constructor.
		/// </summary>
		public Authenticator()
		{
		}

		#region IAuthenticator Members

		/// <summary>
		/// The user name
		/// </summary>
		public string Username
		{
			get
			{
				return _username;
			}
			set
			{
				_username = value;
			}
		}

		/// <summary>
		/// The password
		/// </summary>
		public string Password
		{
			get
			{
				return _password;
			}
			set
			{
				_password = value;
			}
		}

		#endregion
	}
}
