/*
 * $Header: /CreditInfoGroup_ge/CreditInfoGroup.root/CreditInfoGroup/ValueObjects/WSC/IExternalWebServiceAccess.cs 1     26.03.07 17:44 J.ondrak $Log:
 */
using Nullables;

namespace ValueObjects.WSC
{
	/// <summary>
	/// The ExternalWebServiceAccess class represents access permissions to an external web service.
	/// <para>Copyright (C)2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
	/// </summary>
	public interface IExternalWebServiceAccess
	{
		/// <summary>
		/// Unique identifier for the web service.
		/// </summary>
		int WebServiceId
		{
			get;
			set;
		}

		/// <summary>
		/// Unique identifier for the user.
		/// </summary>
		int UserId
		{
			get;
			set;
		}

		/// <summary>
		/// Username used to connect to the web service.
		/// </summary>
		string Username
		{
			get;
			set;
		}

		/// <summary>
		/// Password used to connect to the web service. It must be decrypted before usage.
		/// </summary>
		string PasswordEncrypted
		{
			get;
			set;
		}

		/// <summary>
		/// Date and time of disablement.
		/// </summary>
		NullableDateTime Disabled
		{
			get;
			set;
		}

		/// <summary>
		/// Unique identifier of the disabling user.
		/// </summary>
		int DisabledBy		
		{
			get;
			set;
		}

		/// <summary>
		/// Gets external web service information.
		/// </summary>
		IExternalWebService WebService
		{
			get;
			set;
		}

		/// <summary>
		/// Returns an username and password to use in a connection to a specfic web service.
		/// </summary>
		/// <returns></returns>
		IAuthenticator GetUsernameAndPassword();

	}
}
