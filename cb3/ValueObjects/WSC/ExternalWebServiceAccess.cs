/*
 * $Header: /CreditInfoGroup_ge/CreditInfoGroup.root/CreditInfoGroup/ValueObjects/WSC/ExternalWebServiceAccess.cs 1     26.03.07 17:44 J.ondrak $Log:
 */

using Nullables;
using Cig.Framework.Base.Common.DataProtection;

namespace ValueObjects.WSC
{
	/// <summary>
	/// Summary description for ExternalWebServiceAccess.
	/// </summary>
	public class ExternalWebServiceAccess : IExternalWebServiceAccess
	{

		private int _webServiceId;
		private int _userId;
		private string _username;
		private string _passwordEncrypted;
		private NullableDateTime _disabled;
		private int _disabledBy;
		private IExternalWebService _webService;

		private NullableDateTime _inserted;
		private NullableDateTime _updated;
		private int _insertedBy;
		private int _updatedBy;

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ExternalWebServiceAccess()
		{
		}

		#region IExternalWebServiceAccess Members

		/// <summary>
		/// Unique identifier for the web service.
		/// </summary>
		public int WebServiceId
		{
			get
			{
				return _webServiceId;
			}
			set
			{
				_webServiceId = value;
			}
		}

		/// <summary>
		/// Unique identifier for the user.
		/// </summary>
		public int UserId
		{
			get
			{
				return _userId;
			}
			set
			{
				_userId = value;
			}
		}

		/// <summary>
		/// Username used to connect to the web service.
		/// </summary>
		public string Username
		{
			get
			{
				return _username;
			}
			set
			{
				_username = value;
			}
		}

		/// <summary>
		/// Password used to connect to the web service. It must be decrypted before usage.
		/// </summary>
		public string PasswordEncrypted
		{
			get
			{
				return _passwordEncrypted;
			}
			set
			{
				_passwordEncrypted = value;
			}
		}

		/// <summary>
		/// Date and time of disablement.
		/// </summary>
		public NullableDateTime Disabled
		{
			get
			{
				return _disabled;
			}
			set
			{
				_disabled = value;
			}
		}

		/// <summary>
		/// Unique identifier of the disabling user.
		/// </summary>
		public int DisabledBy
		{
			get
			{
				return _disabledBy;
			}
			set
			{
				_disabledBy = value;
			}
		}

		/// <summary>
		/// The external web service
		/// </summary>
		public IExternalWebService WebService
		{
			get { return _webService; }
			set { _webService = value; }
		}

		#endregion

		#region ICigInsertUpdateValueObject Members

		/// <summary>
		/// Date and time of insertion.
		/// </summary>
		public NullableDateTime Inserted
		{
			get
			{
				return _inserted;
			}
			set
			{
				_inserted = value;
			}
		}

		/// <summary>
		/// Id of user making the update.
		/// </summary>
		public int UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}

		/// <summary>
		/// Date and time of update.
		/// </summary>
		public NullableDateTime Updated
		{
			get
			{
				return _updated;
			}
			set
			{
				_updated = value;
			}
		}

		/// <summary>
		/// Id of user making the insertion.
		/// </summary>
		public int InsertedBy
		{
			get
			{
				return _insertedBy;
			}
			set
			{
				_insertedBy = value;
			}
		}

		#endregion

		
		/// <summary>
		/// Returns an username and password to use in a connection to a specfic web service.
		/// </summary>
		/// <returns></returns>
		public IAuthenticator GetUsernameAndPassword()
		{
			IAuthenticator authenticator = null;
			if(Username == null || Username.Length < 1)
			{
				IExternalWebServiceAccess master = WebService.MasterUser;
				if(master.Username != null && master.Username.Length > 0)
				{
					authenticator = new Authenticator();
					authenticator.Username = master.Username;
					authenticator.Password = Crypto.Decrypt(master.PasswordEncrypted);
				}
			}
			else
			{
				authenticator = new Authenticator();
				authenticator.Username = Username;
				authenticator.Password = Crypto.Decrypt(PasswordEncrypted);
			}

			return authenticator;
		}
	}
}
