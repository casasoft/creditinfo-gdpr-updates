/*
 * $Header: /CreditInfoGroup_ge/CreditInfoGroup.root/CreditInfoGroup/ValueObjects/WSC/ExternalWebService.cs 1     26.03.07 17:44 J.ondrak $Log:
 */

using System;
using System.Collections;
using System.Xml.Serialization;

using Nullables;

using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.WSC
{
	/// <summary>
	/// Summary description for ExternalWebService.
	/// </summary>
	public class ExternalWebService : CigBaseValueObject, IExternalWebService
	{
		private int _WebServiceId;
		private string _NameNative;
		private string _NameEN;
		private string _Path;
		private string _DescriptionNative;
		private string _DescriptionEN;
		private int _MasterUserId;
		private IExternalWebServiceAccess _MasterUser;
		private int _Type;
		private string _shortCode;
		private string _xsltName;
		private IList _ExternalWebServiceAccess;

		private NullableDateTime _Inserted;
		private NullableDateTime _Updated;
		private NullableDateTime _Disabled;
		private int _InsertedBy;
		private int _UpdatedBy;
		private int _DisabledBy;

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ExternalWebService()
		{
		}

		#region IExternalWebService Members

		/// <summary>
		/// Gets or sets unique id for external web service.
		/// </summary>
		public int WebServiceId
		{
			get
			{
				return _WebServiceId;
			}
			set
			{
				_WebServiceId = value;
			}
		}

		/// <summary>
		/// Gets or sets native name of external web service.
		/// </summary>
		public string NameNative
		{
			get
			{
				return _NameNative;
			}
			set
			{
				_NameNative = value;
			}
		}

		/// <summary>
		/// Gets or sets english name of external web service.
		/// </summary>
		public string NameEN
		{
			get
			{
				return _NameEN;
			}
			set
			{
				_NameEN = value;
			}
		}

		/// <summary>
		/// Gets or set path to access the web service.
		/// </summary>
		public string Path
		{
			get
			{
				return _Path;
			}
			set
			{
				_Path = value;
			}
		}

		/// <summary>
		/// Gets or sets native description.
		/// </summary>
		public string DescriptionNative
		{
			get
			{
				return _DescriptionNative;
			}
			set
			{
				_DescriptionNative = value;
			}
		}

		/// <summary>
		/// Gets or sets english description.
		/// </summary>
		public string DescriptionEN
		{
			get
			{
				return _DescriptionEN;
			}
			set
			{
				_DescriptionEN = value;
			}
		}

		/// <summary>
		/// Unique identifier for the master user of the web service.
		/// </summary>
		public int MasterUserId
		{
			get { return _MasterUserId; }
			set { _MasterUserId = value; }
		}

		/// <summary>
		/// Master user of the web service.
		/// </summary>
		public IExternalWebServiceAccess MasterUser
		{
			get
			{
				return _MasterUser;
			}
			set
			{
				_MasterUser = value;
			}
		}

		/// <summary>
		/// Gets or sets type of web service.
		/// </summary>
		public int Type
		{
			get
			{
				return _Type;
			}
			set
			{
				_Type = value;
			}
		}

		/// <summary>
		/// A short code used to find the web service.
		/// </summary>
		public string ShortCode
		{
			get { return _shortCode; }
			set { _shortCode = value; }
		}

		/// <summary>
		/// Xslt filename used to transform the response.
		/// </summary>
		public string XsltName
		{
			get { return _xsltName; }
			set { _xsltName = value; }
		}

		/// <summary>
		/// All access objects on this web service.
		/// </summary>
		public IList ExternalWebServiceAccess
		{
			get { return _ExternalWebServiceAccess; }
			set { _ExternalWebServiceAccess = value; }
		}

		/// <summary>
		/// Gets or sets data and time when web service was disabled.
		/// </summary>
		public NullableDateTime Disabled
		{
			get
			{
				return _Disabled;
			}
			set
			{
				_Disabled = value;
			}
		}

		/// <summary>
		/// Gets or sets unique id of the user disabling the web service.
		/// </summary>
		public int DisabledBy
		{
			get
			{
				return _DisabledBy;
			}
			set
			{
				_DisabledBy = value;
			}
		}
		#endregion

		#region ICigInsertUpdateValueObject Members

		/// <summary>
		/// Gets or sets date and time when the web service was inserted.
		/// </summary>
		public Nullables.NullableDateTime Inserted
		{
			get
			{
				return _Inserted;
			}
			set
			{
				_Inserted = value;
			}
		}

		/// <summary>
		/// Gets or sets unique id of the user that updated the web service.
		/// </summary>
		public int UpdatedBy
		{
			get
			{
				return _UpdatedBy;
			}
			set
			{
				_UpdatedBy = value;
			}
		}

		/// <summary>
		/// Gets or sets data and time of updating.
		/// </summary>
		public Nullables.NullableDateTime Updated
		{
			get
			{
				return _Updated;
			}
			set
			{
				_Updated = value;
			}
		}

		/// <summary>
		/// Gets or sets unique id of the user who inserted the web service.
		/// </summary>
		public int InsertedBy
		{
			get
			{
				return _InsertedBy;
			}
			set
			{
				_InsertedBy = value;
			}
		}

		#endregion
	}
}
