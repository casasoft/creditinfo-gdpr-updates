namespace ValueObjects.General {
    /// <summary>
    /// This interface represents a ID number of some type
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IIDNumber {
        /// <summary>
        /// Id of the nuber
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// The ID number
        /// </summary>
        string Number { get; set; }

        /// <summary>
        /// Type of the ID
        /// </summary>
        int NumberTypeId { get; set; }

        /// <summary>
        /// ID of the CIG entity that owns this number
        /// </summary>
        int CreditinfoId { get; set; }
    }
}