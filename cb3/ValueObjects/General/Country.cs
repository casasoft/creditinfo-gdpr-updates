#region

using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;

#endregion

namespace ValueObjects.General {
    #region

    

    #endregion

    /// <summary>
    /// This class represents a Country
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson & Jiri Ondrak - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    [XmlRoot("Country")]
    public class Country : CigBaseValueObject, ICountry {
        #region ICountry Members

        /// <summary>
        /// Id of the country
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name native
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// Name English
        /// </summary>
        public string NameEN { get; set; }

        #endregion
    }
}