using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.General {
    /// <summary>
    /// This class represents a ID number associcated to CIG Entity
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    [XmlRoot("IDNumber")]
    public class IDNumber : CigBaseValueObject, IIDNumber {
        #region IIDNumber Members

        /// <summary>
        /// Id of the nuber
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The ID number
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Type of the ID
        /// </summary>
        public int NumberTypeId { get; set; }

        /// <summary>
        /// ID of the CIG entity that owns this number
        /// </summary>
        public int CreditinfoId { get; set; }

        #endregion
    }
}