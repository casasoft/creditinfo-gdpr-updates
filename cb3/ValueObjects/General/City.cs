using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.General {
    /// <summary>
    /// This class represents a City
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson & Jiri Ondrak - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    [XmlRoot("City")]
    public class City : CigBaseValueObject, ICity {
        private ICountry country;
        private int countryId;

        #region ICity Members

        /// <summary>
        /// Id of the city
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name native
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// Name English
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Location of the City
        /// </summary>
        public string LocationNative { get; set; }

        /// <summary>
        /// Location english
        /// </summary>
        public string LocationEN { get; set; }

        #endregion
    }
}