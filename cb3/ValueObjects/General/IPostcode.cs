namespace ValueObjects.General {
    /// <summary>
    /// This interface describes a single post code / ZIP Code.
    /// </summary>
    public interface IPostcode {
        /// <summary>
        /// ID of the Postcode 
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Post code native 
        /// </summary>
        string NameNative { get; set; }

        /// <summary>
        /// Post code English
        /// </summary>
        string NameEN { get; set; }

        /// <summary>
        /// Post code is disabled 
        /// </summary>
        bool Disabled { get; set; }
    }
}