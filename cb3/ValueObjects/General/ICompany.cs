using System;
using Nullables;

namespace ValueObjects.General {
    /// <summary>
    /// This interface represents a company
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ICompany : ICigBaseEntity {
        /// <summary>
        /// Native name of the company
        /// </summary>
        string NameNative { get; set; }

        /// <summary>
        /// English name of the company
        /// </summary>
        string NameEN { get; set; }

        /// <summary>
        /// Date the company was established
        /// </summary>
        DateTime Established { get; set; }

        /// <summary>
        /// Date the company was last contacted
        /// </summary>
        DateTime LastContacted { get; set; }

        /// <summary>
        /// Last update of the company information
        /// </summary>
        DateTime LastUpdate { get; set; }

        /// <summary>
        /// Date the company was registered
        /// </summary>
        DateTime Registered { get; set; }

        /// <summary>
        /// Url of the company home page
        /// </summary>
        string Url { get; set; }

        /// <summary>
        /// Id of the company function
        /// </summary>
        string FuncId { get; set; }

        /// <summary>
        /// Id of the company status
        /// </summary>
        NullableInt32 OrgStatusCode { get; set; }

        /// <summary>
        /// Status of the company name (former or current)
        /// </summary>
        string OrgNameStatusCode { get; set; }

        /// <summary>
        /// Date of the company status
        /// </summary>
        DateTime OrgStatusDate { get; set; }
    }
}