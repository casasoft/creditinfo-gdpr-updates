using System;
using System.Xml.Serialization;

namespace ValueObjects.General {
    /// <summary>
    /// This class represents an individual
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    [XmlRoot("Individual")]
    public class Individual : CigBaseEntity, IIndividual {
        private string firstNameEN;
        private string firstNameNative;
        private string surNameEN;
        private string surNameNative;

        #region IIndividual Members

        /// <summary>
        /// The individual native first name 
        /// </summary>
        public string FirstNameNative { get { return firstNameNative; } set { firstNameNative = value; } }

        /// <summary>
        /// The individual english first name 
        /// </summary>
        public string FirstNameEN { get { return firstNameEN; } set { firstNameEN = value; } }

        /// <summary>
        /// The individual native surname 
        /// </summary>
        public string SurNameNative { get { return surNameNative; } set { surNameNative = value; } }

        /// <summary>
        /// The individual english surname 
        /// </summary>
        public string SurNameEN { get { return surNameEN; } set { surNameEN = value; } }

        /// <summary>
        /// Native name (firstname + surname)
        /// </summary>
        public string NameNative {
            get {
                string nameToReturn = "";
                if (firstNameNative != null && firstNameNative.Trim().Length > 0) {
                    nameToReturn = firstNameNative;
                    if (surNameNative != null && surNameNative.Trim().Length > 0) {
                        nameToReturn += " " + surNameNative;
                    }
                } else if (firstNameEN != null && firstNameEN.Trim().Length > 0) {
                    nameToReturn = firstNameEN;
                    if (surNameEN != null && surNameEN.Trim().Length > 0) {
                        nameToReturn += " " + surNameEN;
                    }
                }

                return nameToReturn;
            }
        }

        /// <summary>
        /// English name (first name + surname)
        /// </summary>
        public string NameEN {
            get {
                string nameToReturn = "";
                if (firstNameEN != null && firstNameEN.Trim().Length > 0) {
                    nameToReturn = firstNameEN;
                    if (surNameEN != null && surNameEN.Trim().Length > 0) {
                        nameToReturn += " " + surNameEN;
                    }
                } else if (firstNameNative != null && firstNameNative.Trim().Length > 0) {
                    nameToReturn = firstNameNative;
                    if (surNameNative != null && surNameNative.Trim().Length > 0) {
                        nameToReturn += " " + surNameNative;
                    }
                }

                return nameToReturn;
            }
        }

        /// <summary>
        /// Id of the profession
        /// </summary>
        public int ProfessionId { get; set; }

        /// <summary>
        /// Id of the education
        /// </summary>
        public int EducationId { get; set; }

        /// <summary>
        /// Date of the last update of information
        /// </summary>
        public DateTime LastUpdate { get; set; }

        #endregion
    }
}