namespace ValueObjects.General {
    /// <summary>
    /// This interface represent a index mapping from a word in a name of Cig Entity to Creditinfo id.
    /// The mapping is used for faster search for a name containing specific word.  
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IWord2CIID {
        /// <summary>
        /// Type of entity
        /// </summary>
        string Type { get; set; }

        /// <summary>
        /// Creditinfo id of the entity
        /// </summary>
        int CreditinfoId { get; set; }

        /// <summary>
        /// The word contained in the entity name
        /// </summary>
        string Word { get; set; }
    }
}