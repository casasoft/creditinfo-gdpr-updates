using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.General {
    /// <summary>
    /// This class represent a index mapping from a word in a name of Cig Entity to Creditinfo id.
    /// The mapping is used for faster search for a name containing specific word.  
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    [XmlRoot("Word2CIID")]
    public class Word2CIID : CigBaseValueObject, IWord2CIID {
        #region IWord2CIID Members

        /// <summary>
        /// Type of entity
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Creditinfo id of the entity
        /// </summary>
        public int CreditinfoId { get; set; }

        /// <summary>
        /// The word contained in the entity name
        /// </summary>
        public string Word { get; set; }

        #endregion
    }
}