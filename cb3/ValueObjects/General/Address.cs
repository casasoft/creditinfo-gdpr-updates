#region

using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;

#endregion

namespace ValueObjects.General {
    #region

    

    #endregion

    /// <summary>
    /// This class represents an address
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    [XmlRoot("Address")]
    public class Address : CigBaseValueObject, IAddress {
        #region IAddress Members

        /// <summary>
        /// Id of the address
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The Creditinfo id of the cig entity that this address is registered to
        /// </summary>
        public int CreditinfoId { get; set; }

        /// <summary>
        /// Native street name
        /// </summary>
        public string StreetNative { get; set; }

        /// <summary>
        /// English street name
        /// </summary>
        public string StreetEN { get; set; }

        /// <summary>
        /// Street number
        /// </summary>
        public int StreetNumber { get; set; }

        /// <summary>
        /// Id of the city the address belongs to
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// Postal code of the address
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// The postbox
        /// </summary>
        public string PostBox { get; set; }

        /// <summary>
        /// Other native information about the address
        /// </summary>
        public string OtherInfoNative { get; set; }

        /// <summary>
        /// Other english information about the address
        /// </summary>
        public string OtherInfoEN { get; set; }

        /// <summary>
        /// Id of the country the address belongs to
        /// </summary>
        public int CountryId { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }

        /// <summary>
        /// Tells wether the address is trading address or regular address
        /// </summary>
        public string IsTradingAddress { get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }

        public ICity City { get; set; }

        #endregion
    }
}