namespace ValueObjects.General {
    /// <summary>
    /// This interface describes one Country
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson & Jiri Ondrak - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ICountry {
        /// <summary>
        /// Id of the Country
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// name of the country
        /// </summary>
        string NameNative { get; set; }

        /// <summary>
        /// name eng of the country
        /// </summary>
        string NameEN { get; set; }
    }
}