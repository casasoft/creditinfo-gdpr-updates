/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/General/IAddress.cs 2     22.06.05 16:34 Ondrakj $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/General/IAddress.cs $ 
 * 
 * 2     22.06.05 16:34 Ondrakj
 * new property City
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
namespace ValueObjects.General
{
	/// <summary>
	/// This interface describes one address
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	public interface IAddress
	{
		/// <summary>
		/// Id of the address
		/// </summary>
		int Id
		{
			get;
			set;
		}

		/// <summary>
		/// The Creditinfo id of the cig entity that this address is registered to
		/// </summary>
		int CreditinfoId
		{
			get;
			set;
		}

		/// <summary>
		/// Native street name
		/// </summary>
		string StreetNative
		{
			get;
			set;
		}

		/// <summary>
		/// English street name
		/// </summary>
		string StreetEN
		{
			get;
			set;
		}

		/// <summary>
		/// Street number
		/// </summary>
		int StreetNumber
		{
			get;
			set;
		}

		/// <summary>
		/// Id of the city the address belongs to
		/// </summary>
		int CityId
		{
			get;
			set;
		}

		/// <summary>
		/// Postal code of the address
		/// </summary>
		string PostalCode
		{
			get;
			set;
		}

		/// <summary>
		/// The postbox
		/// </summary>
		string PostBox
		{
			get;
			set;
		}

		/// <summary>
		/// Other native information about the address
		/// </summary>
		string OtherInfoNative
		{
			get;
			set;
		}

		/// <summary>
		/// Other english information about the address
		/// </summary>
		string OtherInfoEN
		{
			get;
			set;
		}

		/// <summary>
		/// Id of the country the address belongs to
		/// </summary>
		int CountryId
		{
			get;
			set;
		}

		/// <summary>
		/// Tells wether the address is trading address or regular address
		/// </summary>
		string IsTradingAddress
		{
			get;
			set;
		}
	
		/// <summary>
		/// city
		/// </summary>
		ICity City
		{
			get;
			set;
		}
	}
}
