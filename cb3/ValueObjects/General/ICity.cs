#region



#endregion

namespace ValueObjects.General {
    /// <summary>
    /// This interface describes one City
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson & Jiri Ondrak - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ICity {
        /// <summary>
        /// Id of the City
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// name of the city
        /// </summary>
        string NameNative { get; set; }

        /// <summary>
        /// name eng of the city
        /// </summary>
        string NameEN { get; set; }

        /// <summary>
        /// location of teh city
        /// </summary>
        string LocationNative { get; set; }

        /// <summary>
        /// location en of the city
        /// </summary>
        string LocationEN { get; set; }
    }
}