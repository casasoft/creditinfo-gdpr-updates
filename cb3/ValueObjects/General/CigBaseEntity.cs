#region

using System;
using System.Collections;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;

#endregion

namespace ValueObjects.General {
    #region

    

    #endregion

    /// <summary>
    /// This class is a base class for CIG Entity.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    [XmlRoot("CigBaseEntity")]
    public class CigBaseEntity : CigBaseValueObject, ICigBaseEntity {
        private IList addresses;
        private IList companyList;
        private IList idNumbers;
        private IList individualList;

        #region ICigBaseEntity Members

        /// <summary>
        /// A list of addresses assiciated to the entity
        /// </summary>
        public IList Addresses { get { return addresses; } set { addresses = value; } }

        /// <summary>
        /// Native street name for the first address associated to the entity 
        /// </summary>
        public string StreetNative {
            get {
                if (addresses != null && addresses.Count > 0) {
                    return ((IAddress) addresses[0]).StreetNative;
                }
                return "";
            }
        }

        /// <summary>
        /// First ID number associated to the entity
        /// </summary>
        public string NationalId {
            get {
                if (idNumbers != null && idNumbers.Count > 0) {
                    return ((IIDNumber) idNumbers[0]).Number;
                }
                return "";
            }
        }

        /// <summary>
        /// The Creditinfo ID given to the entity
        /// </summary>
        public int CreditinfoId { get; set; }

        /// <summary>
        /// List of all ID numbers associated to the entity
        /// </summary>
        public IList IDNumbers { get { return idNumbers; } set { idNumbers = value; } }

        /// <summary>
        /// List of all ID numbers associated to the entity
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Concrete company with specified creditinfoid
        /// </summary>
        public ICompany CompanyEntity {
            get {
                if ((companyList != null) && (companyList.Count > 0)) {
                    return companyList[0] as ICompany;
                }
                return null;
            }
        }

        /// <summary>
        /// Concrete individual with specified creditinfoid
        /// </summary>
        public IIndividual IndividualEntity {
            get {
                if ((individualList != null) && (individualList.Count > 0)) {
                    return individualList[0] as IIndividual;
                }
                return null;
            }
        }

        /// <summary>
        /// Hack to store Company with specified creditinfoid or nothing
        /// </summary>
        public IList CompanyList { get { return companyList; } set { companyList = value; } }

        /// <summary>
        /// Hack to store Individual with specified creditinfoid or nothing
        /// </summary>
        public IList IndividualList { get { return individualList; } set { individualList = value; } }

        #endregion
    }
}