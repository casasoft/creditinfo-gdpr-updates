#region

using System;
using System.Xml.Serialization;
using Nullables;

#endregion

namespace ValueObjects.General {
    #region

    

    #endregion

    /// <summary>
    /// This class represents a company
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    [XmlRoot("Company")]
    public class Company : CigBaseEntity, ICompany {
        #region ICompany Members

        /// <summary>
        /// Native name of the company
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// English name of the company
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Date the company was established
        /// </summary>
        public DateTime Established { get; set; }

        /// <summary>
        /// Date the company was last contacted
        /// </summary>
        public DateTime LastContacted { get; set; }

        /// <summary>
        /// Url of the company home page
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Id of the company function
        /// </summary>
        public string FuncId { get; set; }

        /// <summary>
        /// Id of the company status
        /// </summary>
        public NullableInt32 OrgStatusCode { get; set; }

        /// <summary>
        /// Status of the company name (former or current)
        /// </summary>
        public string OrgNameStatusCode { get; set; }

        /// <summary>
        /// Date of the company status
        /// </summary>
        public DateTime OrgStatusDate { get; set; }

        /// <summary>
        /// Last update of the company information
        /// </summary>
        public DateTime LastUpdate { get; set; }

        /// <summary>
        /// Date the company was registered
        /// </summary>
        public DateTime Registered { get; set; }

        #endregion
    }
}