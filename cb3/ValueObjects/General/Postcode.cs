using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.General {
    /// <summary>
    /// Summary description for Postcode.
    /// </summary>
    [Serializable]
    [XmlRoot("Postcode")]
    public class Postcode : CigBaseValueObject, IPostcode {
        #region IPostcode Members

        /// <summary>
        /// ID of the Postcode 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Postcode name native 
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// Postcode name English
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Post code is disabled
        /// </summary>
        public bool Disabled { get; set; }

        #endregion
    }
}