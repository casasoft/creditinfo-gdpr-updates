using System;

namespace ValueObjects.General {
    /// <summary>
    /// This interface represents an individual
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IIndividual : ICigBaseEntity {
        /// <summary>
        /// The individual native first name 
        /// </summary>
        string FirstNameNative { get; set; }

        /// <summary>
        /// The individual english first name 
        /// </summary>
        string FirstNameEN { get; set; }

        /// <summary>
        /// The individual native surname 
        /// </summary>
        string SurNameNative { get; set; }

        /// <summary>
        /// The individual english surname 
        /// </summary>
        string SurNameEN { get; set; }

        /// <summary>
        /// Id of the profession
        /// </summary>
        int ProfessionId { get; set; }

        /// <summary>
        /// Id of the education
        /// </summary>
        int EducationId { get; set; }

        /// <summary>
        /// Date of the last update of information
        /// </summary>
        DateTime LastUpdate { get; set; }

        /// <summary>
        /// Native name (firstname + surname)
        /// </summary>
        string NameNative { get; }

        /// <summary>
        /// English name (first name + surname)
        /// </summary>
        string NameEN { get; }
    }
}