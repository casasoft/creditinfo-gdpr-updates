#region

using System.Collections;

#endregion

namespace ValueObjects.General {
    #region

    

    #endregion

    /// <summary>
    /// This is a base interface representing a CIG entity
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ICigBaseEntity {
        /// <summary>
        /// A list of addresses assiciated to the entity
        /// </summary>
        IList Addresses { get; set; }

        /// <summary>
        /// Native street name for the first address associated to the entity 
        /// </summary>
        string StreetNative { get; }

        /// <summary>
        /// First ID number associated to the entity
        /// </summary>
        string NationalId { get; }

        /// <summary>
        /// The Creditinfo ID given to the entity
        /// </summary>
        int CreditinfoId { get; set; }

        /// <summary>
        /// List of all ID numbers associated to the entity
        /// </summary>
        IList IDNumbers { get; set; }

        int TypeId { get; set; }

        /// <summary>
        /// Concrete company with specified creditinfoid
        /// </summary>
        ICompany CompanyEntity { get; }

        /// <summary>
        /// Concrete individual with specified creditinfoid
        /// </summary>
        IIndividual IndividualEntity { get; }

        /// <summary>
        /// Hack to store Company with specified creditinfoid or nothing
        /// </summary>
        IList CompanyList { get; set; }

        /// <summary>
        /// Hack to store Individual with specified creditinfoid or nothing
        /// </summary>
        IList IndividualList { get; set; }
    }
}