namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// This interface describes a operator in a financial statement validation.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IOperator {
        /// <summary>
        /// Id of operator
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Native name of operator
        /// </summary>
        string NameNative { get; set; }

        /// <summary>
        /// English name of operator
        /// </summary>
        string NameEN { get; set; }

        /// <summary>
        /// Operator symbol
        /// </summary>
        string Symbol { get; set; }
    }
}