using System.Collections;

namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// This interface describes a financial statement input template.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ITemplate {
        /// <summary>
        /// Id of template
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Native name of template
        /// </summary>
        string NameNative { get; set; }

        /// <summary>
        /// English name of template
        /// </summary>
        string NameEN { get; set; }

        /// <summary>
        /// Native description name of template
        /// </summary>
        string DescriptionNative { get; set; }

        /// <summary>
        /// English description of template
        /// </summary>
        string DescriptionEN { get; set; }

        /// <summary>
        /// Fields included in the template
        /// </summary>
        IList TemplateFields { get; set; }

        /// <summary>
        /// Ratios included in the template
        /// </summary>
        IList TemplateRatios { get; set; }
    }
}