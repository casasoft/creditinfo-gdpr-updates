using System;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// Implementation of a connection between template and field.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    public class TemplateField : CigBaseValueObject, ITemplateField {
        private int id = -1;

        #region ITemplateField Members

        /// <summary>
        /// Id of template field
        /// </summary>
        public int ID { get { return id; } set { id = value; } }

        /// <summary>
        /// The field
        /// </summary>
        public IField Field { get; set; }

        /// <summary>
        /// The template
        /// </summary>
        public ITemplate Template { get; set; }

        /// <summary>
        /// Id of field
        /// </summary>
        public int FieldID { get; set; }

        #endregion
    }
}