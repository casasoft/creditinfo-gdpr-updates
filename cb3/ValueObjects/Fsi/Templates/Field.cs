using System;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// Implementation of a field in a financial statement.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    public class Field : CigBaseValueObject, IField {
        #region IField Members

        /// <summary>
        /// Id of field
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Native name of field
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// Engilsh name of field
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Type of field
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Db name of field
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Native description of field
        /// </summary>
        public string DescriptionNative { get; set; }

        /// <summary>
        /// English description of field
        /// </summary>
        public string DescriptionEN { get; set; }

        /// <summary>
        /// Id of the category the field belongs to
        /// </summary>
        public int CategoryID { get; set; }

        #endregion
    }
}