using System;
using System.Collections;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// Implementation of a ratio for a financial statement.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    public class Ratio : CigBaseValueObject, IRatio {
        private int id = -1;
        private string percentageFormat;

        #region IRatio Members

        /// <summary>
        /// Id of ratio
        /// </summary>
        public int ID { get { return id; } set { id = value; } }

        /// <summary>
        /// Native name of ratio
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// English name of ratio
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Native description
        /// </summary>
        public string DescriptionNative { get; set; }

        /// <summary>
        /// English description
        /// </summary>
        public string DescriptionEN { get; set; }

        /// <summary>
        /// List of formula that calculates the ratio
        /// </summary>
        public IList Formula { get; set; }

        /// <summary>
        /// If the ratio should be displayed in percentage format (sting boolean value)
        /// </summary>
        public string PercentageFormat {
            get {
                if (percentageFormat == null || percentageFormat.Trim().Equals("")) {
                    return "false";
                }
                return percentageFormat;
            }
            set { percentageFormat = value; }
        }

        /// <summary>
        /// If the ratio should be displayed in percentage format
        /// </summary>
        public bool InPercentageFormat {
            get { return percentageFormat.ToLower().Equals("true"); }
            set {
                percentageFormat = value ? "true" : "false";
            }
        }

        #endregion
    }
}