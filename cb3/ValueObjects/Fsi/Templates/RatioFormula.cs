using System;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// Implementation of a formula item in a financial statement ratio.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    public class RatioFormula : CigBaseValueObject, IRatioFormula {
        private string formerYear;

        #region IRatioFormula Members

        /// <summary>
        /// Id of formula item
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// It of ratio the item belings to
        /// </summary>
        public int RatioID { get; set; }

        /// <summary>
        /// Type of formula item (operator or field)
        /// </summary>
        public string FormulaItemType { get; set; }

        /// <summary>
        /// Id of formula item
        /// </summary>
        public int FormulaItemID { get; set; }

        /// <summary>
        /// Order of this item in the formula
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// If this formula item represents a former year or current year (if this is field type)
        /// </summary>
        public string FormerYear {
            get {
                if (formerYear == null || formerYear.Trim().Equals("")) {
                    return "false";
                }
                return formerYear;
            }
            set { formerYear = value; }
        }

        /// <summary>
        /// If this formula item represents a former year or current year (if this is field type)
        /// </summary>
        public bool IsFormerYear {
            get { return formerYear.ToLower().Equals("true"); }
            set {
                formerYear = value ? "true" : "false";
            }
        }

        /// <summary>
        /// Is this formula item a field.
        /// </summary>
        public bool IsField { get { return FormulaItemType == "FIELD"; } }

        #endregion
    }
}