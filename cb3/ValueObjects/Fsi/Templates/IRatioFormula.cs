namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// This interface describes a formula item in a financial statement ratio.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IRatioFormula {
        /// <summary>
        /// Id of formula item
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// It of ratio the item belings to
        /// </summary>
        int RatioID { get; set; }

        /// <summary>
        /// Type of formula item (operator or field)
        /// </summary>
        string FormulaItemType { get; set; }

        /// <summary>
        /// Id of formula item
        /// </summary>
        int FormulaItemID { get; set; }

        /// <summary>
        /// Order of this item in the formula
        /// </summary>
        int Order { get; set; }

        /// <summary>
        /// If this formula item represents a former year or current year (if this is field type)
        /// </summary>
        string FormerYear { get; set; }

        /// <summary>
        /// If this formula item represents a former year or current year (if this is field type)
        /// </summary>
        bool IsFormerYear { get; set; }

        /// <summary>
        /// If this is a field
        /// </summary>
        bool IsField { get; }
    }
}