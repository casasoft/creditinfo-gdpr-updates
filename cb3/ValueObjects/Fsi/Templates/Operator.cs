using System;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// Implementation of a operator in a financial statement validation.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    public class Operator : CigBaseValueObject, IOperator {
        #region IOperator Members

        /// <summary>
        /// Id of operator
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Native name of operator
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// English name of operator
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Operator symbol
        /// </summary>
        public string Symbol { get; set; }

        #endregion
    }
}