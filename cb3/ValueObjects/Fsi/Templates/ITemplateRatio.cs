namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// This interface describes a connection between template and ratio.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ITemplateRatio {
        /// <summary>
        /// Id of template ratio
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Id of template
        /// </summary>
        int TemplateID { get; set; }

        /// <summary>
        /// Id of ratio
        /// </summary>
        int RatioID { get; set; }

        /// <summary>
        /// The order of ratio in the template
        /// </summary>
        int Order { get; set; }

        /// <summary>
        /// The ratio
        /// </summary>
        Ratio Ratio { get; set; }
    }
}