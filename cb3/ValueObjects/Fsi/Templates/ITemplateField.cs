namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// This interface describes a connection between template and field.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ITemplateField {
        /// <summary>
        /// Id of template field
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// The field
        /// </summary>
        IField Field { get; set; }

        /// <summary>
        /// The template
        /// </summary>
        ITemplate Template { get; set; }

        /// <summary>
        /// Id of field
        /// </summary>
        int FieldID { get; set; }
    }
}