using System;
using System.Collections;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// Implementation of a financial statement imput template.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    [Serializable]
    public class Template : CigBaseValueObject, ITemplate {
        #region ITemplate Members

        /// <summary>
        /// Id of template
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Native name of template
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// English name of template
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Native description name of template
        /// </summary>
        public string DescriptionNative { get; set; }

        /// <summary>
        /// English description of template
        /// </summary>
        public string DescriptionEN { get; set; }

        /// <summary>
        /// Fields included in the template
        /// </summary>
        public IList TemplateFields { get; set; }

        /// <summary>
        /// Ratios included in the template
        /// </summary>
        public IList TemplateRatios { get; set; }

        #endregion
    }
}