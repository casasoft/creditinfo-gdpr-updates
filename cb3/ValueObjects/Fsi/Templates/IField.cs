namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// This interface represents a field in a financial statement.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IField {
        /// <summary>
        /// Id of field
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Native name of field
        /// </summary>
        string NameNative { get; set; }

        /// <summary>
        /// English name of field
        /// </summary>
        string NameEN { get; set; }

        /// <summary>
        /// Type of field
        /// </summary>
        string Type { get; set; }

        /// <summary>
        /// Db name of field
        /// </summary>
        string FieldName { get; set; }

        /// <summary>
        /// Native description of field
        /// </summary>
        string DescriptionNative { get; set; }

        /// <summary>
        /// English description of field
        /// </summary>
        string DescriptionEN { get; set; }

        /// <summary>
        /// Id of the category the field belongs to
        /// </summary>
        int CategoryID { get; set; }
    }
}