using System.Collections;

namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// This interface describes a ratio for a financial statement.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IRatio {
        /// <summary>
        /// Id of ratio
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Native name of ratio
        /// </summary>
        string NameNative { get; set; }

        /// <summary>
        /// English name of ratio
        /// </summary>
        string NameEN { get; set; }

        /// <summary>
        /// Native description
        /// </summary>
        string DescriptionNative { get; set; }

        /// <summary>
        /// English description
        /// </summary>
        string DescriptionEN { get; set; }

        /// <summary>
        /// If the ratio should be displayed in percentage format (sting boolean value)
        /// </summary>
        string PercentageFormat { get; set; }

        /// <summary>
        /// If the ratio should be displayed in percentage format
        /// </summary>
        bool InPercentageFormat { get; set; }

        /// <summary>
        /// List of formula that calculates the ratio
        /// </summary>
        IList Formula { get; set; }
    }
}