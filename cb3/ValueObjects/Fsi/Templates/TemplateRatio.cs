using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.FSI.Templates {
    /// <summary>
    /// Implementation of a connection between a template and ratio.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class TemplateRatio : CigBaseValueObject, ITemplateRatio {
        #region ITemplateRatio Members

        /// <summary>
        /// Id of template ratio
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Id of template
        /// </summary>
        public int TemplateID { get; set; }

        /// <summary>
        /// Id of ratio
        /// </summary>
        public int RatioID { get; set; }

        /// <summary>
        /// The order of ratio in the template
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// The ratio
        /// </summary>
        public Ratio Ratio { get; set; }

        #endregion
    }
}