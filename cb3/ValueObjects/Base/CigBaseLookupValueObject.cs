﻿using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Schema;
using Cig.Framework.Base.Common.Logging;

namespace ValueObjects.Base {
    [Serializable]
    /// <summary>
        /// The Base Lookup object for Georgia
        /// <para>Copyright (c) 2008 J.Ondrak CreditInfo Solutions</para>
        /// </summary>
    public class CigBaseLookupValueObject : IComparable, ICloneable, IXmlSerializable {
        /// <summary>
        /// Auto-number id of value object
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Native name of lookup value
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// English name of lookup value
        /// </summary>
        public string NameEN { get; set; }

        #region ICloneable Members

        /// <summary>
        /// Clones an object by making a complete "deep" copy of it. NOT IMPLEMENTED YET.
        /// </summary>
        /// <returns>Cloned object</returns>
        public object Clone() {
            // TODO:  Add CIGBaseValueObject.Clone implementation
            return null;
        }

        #endregion

        #region IComparable Members

        /// <summary>
        /// Reflection based "compare to" method. Used for ordering. NOT IMPLEMENTED YET.
        /// </summary>
        /// <param name="obj">Object to compare to</param>
        /// <returns>Zero if equal, else an int value that indicates a "lower" or "higher" value</returns>
        public int CompareTo(object obj) {
            // Check to see if the objects being compared are of the same type
            // If they are not then throw an error and log the problem
            if (GetType() != obj.GetType()) {
                LogAndThrow.ArgumentException(
                    String.Format("The object {0} are not comparable with {1}", obj, this), GetType());
            }

            // Default comparison is made by comparing object hash values. If the Hash value is the same
            // then the objects are concluded to be identical

            // The default method is not used if the target class uses the CompareValue Attribute. Then
            // the fields that are marked with that attribute are used directly in the comparison and nothing else

            return 0;
        }

        #endregion

        #region IXmlSerializable Members

        /// <summary>
        /// Empty schema to override based on the main object
        /// </summary>
        /// <returns>Empty schema - the schema is based on the concrete object</returns>
        public virtual XmlSchema GetSchema() {
            // Left blank for higher purposes...
            return null;
        }

        /// <summary>
        /// Provides general deserialization via reflection
        /// </summary>
        /// <param name="reader">XmlReader to read from and instantiate object</param>
        public virtual void ReadXml(XmlReader reader) {
            reader.MoveToContent();
            var startElement = reader.Name;
            while (reader.Read()) {
                // In recursive calls checks if it's time to return
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == startElement) {
                    break;
                }

                // Deserialize what's supposed to be value type property
                if (reader.NodeType == XmlNodeType.Element && !reader.HasAttributes) {
                    var pi = GetType().GetProperty(reader.Name);
                    pi.SetValue(this, Convert.ChangeType(reader.ReadString(), pi.PropertyType), null);
                }
                    // And now the rest
                else if (reader.NodeType == XmlNodeType.Element && reader.HasAttributes) {
                    InstantiateSubClasses(reader);
                }
            }
        }

        /// <summary>
        /// Provides general serialization via reflection
        /// </summary>
        /// <param name="w">XmlWriter to write content of the object to it</param>
        public virtual void WriteXml(XmlWriter w) {
            var pis = GetType().GetProperties();
            foreach (var pi in pis) {
                // if property has Attribute 'XmlIgnoreAttribute' it is not serialized
                if (pi.GetCustomAttributes(typeof (XmlIgnoreAttribute), false).Length > 0) {
                    continue;
                }

                var value = pi.GetValue(this, null);
                if (value == null) {
                    continue;
                }

                // Serializes value type and string properties
                if (value.GetType().IsValueType || value.GetType().IsAssignableFrom(typeof (string))) {
                    w.WriteStartElement(pi.Name);
                    w.WriteString(value.ToString());
                    w.WriteEndElement();
                }
                    // Serializes reference type properties which implements IXmlSerializable
                else if (value is IXmlSerializable) {
                    w.WriteStartElement(pi.Name);
                    w.WriteAttributeString("type", value.GetType().ToString());
                    ((IXmlSerializable) value).WriteXml(w);
                    w.WriteEndElement();
                }
                    // Serializes list object and all its child objects
                else if (value is IList) {
                    w.WriteStartElement(pi.Name);
                    w.WriteAttributeString("type", value.GetType().ToString());
                    // TODO: implement some PropertyInfo cache for this loop
                    //		? inherit IXmlSerializable and add one performance oriented method, which would accept PropertyInfo argument ?
                    foreach (var obj in (IList) value) {
                        if (obj == null) {
                            continue;
                        }
                        if (!(obj is IXmlSerializable)) {
                            continue;
                        }
                        w.WriteStartElement(obj.GetType().Name);
                        w.WriteAttributeString("type", obj.GetType().ToString());
                        ((IXmlSerializable) obj).WriteXml(w);
                        w.WriteEndElement();
                    }
                    w.WriteEndElement();
                } else {
                    var serializer = new XmlSerializer(value.GetType());
                    serializer.Serialize(w, value);
                }
            }
        }

        #endregion

        /// <summary>
        /// Instantiates subclasses for serialization
        /// </summary>
        /// <param name="reader">XmlReader</param>
        private void InstantiateSubClasses(XmlReader reader) {
            var pi = GetType().GetProperty(reader.Name);
            var typeName = reader.GetAttribute("type");
            var objType = Type.GetType(typeName);
            if (objType.GetInterface("IXmlSerializable", false) != null) {
                var value = InstantiateObject(objType, reader);
                pi.SetValue(this, value, null);
            } else if (objType.GetInterface("IList", false) != null) {
                var value = (IList) Activator.CreateInstance(objType);
                while (!(reader.NodeType == XmlNodeType.EndElement && reader.Name == pi.Name)) {
                    reader.Read();
                    if (reader.NodeType != XmlNodeType.Element || !reader.HasAttributes) {
                        continue;
                    }
                    typeName = reader.GetAttribute("type");
                    objType = Type.GetType(typeName);
                    if (objType.GetInterface("IXmlSerializable", false) == null) {
                        continue;
                    }
                    var innerValue = InstantiateObject(objType, reader);
                    value.Add(innerValue);
                }
                pi.SetValue(this, value, null);
            }
        }

        /// <summary>
        /// Instantiates objects for serialization
        /// </summary>
        /// <param name="type">The type of object to serialize</param>
        /// <param name="reader">The reader used for serialization</param>
        /// <returns>Instatiated object</returns>
        private static object InstantiateObject(Type type, XmlReader reader) {
            var value = Activator.CreateInstance(type);
            ((IXmlSerializable) value).ReadXml(reader);
            return value;
        }
    }
}