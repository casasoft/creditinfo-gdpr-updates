﻿namespace ValueObjects.Base {
    /// <summary>
    /// CigBaseValuobject interface
    /// </summary>
    public interface ICigBaseLookupValueObject {
        /// <summary>
        /// Auto-number id of value object
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Native name of lookup value
        /// </summary>
        string NameNative { get; set; }

        /// <summary>
        /// English name of lookup value
        /// </summary>
        string NameEN { get; set; }
    }
}