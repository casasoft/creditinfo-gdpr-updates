/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/RelatedPartyType.cs 1     2.06.05 13:30 Haukur $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/RelatedPartyType.cs $ 
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
using System;
using System.Xml.Serialization;
using ValueObjects.Base;

namespace ValueObjects.NP
{
	/// <summary>
	/// The RelatedPartyType class represent type of a related party to negative data
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	[Serializable]
	[XmlRoot("RelatedPartyType")]
	public class RelatedPartyType : CigBaseLookupValueObject, ICigBaseLookupValueObject
	{

		/// <summary>
		/// Default constructor
		/// </summary>
		public RelatedPartyType()
		{
		}
	}
}
