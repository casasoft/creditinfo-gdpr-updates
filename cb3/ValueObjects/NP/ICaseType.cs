/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/ICaseType.cs 1     27.06.05 7:03 Adalgeir $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/ICaseType.cs $ 
 * 
 * 1     27.06.05 7:03 Adalgeir
 * File added.
 * 
 * 1     25.06.05 12:50 Adalgeir
 * File added
 */
using System;

namespace ValueObjects.NP
{
	/// <summary>
	/// The CaseType class represent type of negative data
	/// <para>Copyright (C) 2005 A�algeir �orgr�msson - Creditinfo Group ltd.</para>
	/// </summary>
	public interface ICaseType
	{
		/// <summary>
		/// ID of the note - sequence number
		/// </summary>
		int Id
		{
			get;
			set;
		}

		/// <summary>
		/// Name of the type in native language.
		/// </summary>
		string NameNative
		{
			get;
			set;
		}

		/// <summary>
		/// Name of the type in English.
		/// </summary>
		string NameEN
		{
			get;
			set;
		}

		/// <summary>
		/// Is letter status.
		/// </summary>
		string Letter
		{
			get;
			set;
		}

		/// <summary>
		/// Boolean representation for Letter.
		/// </summary>
		bool BoolLetter
		{
			get;
			set;
		}
	}
}
