/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/Collateral.cs 4     29.06.05 9:33 Ondrakj $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/Collateral.cs $ 
 * 
 * 4     29.06.05 9:33 Ondrakj
 * comments
 * 
 * 3     27.06.05 14:21 Jirava
 * added new property AddressCityNameEN
 * 
 * 2     24.06.05 6:54 Jirava
 * Collaterals objects
 * 
 * 1     23.06.05 9:02 Jirava
*/
using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;
using ValueObjects.NP;
using ValueObjects.General;
using ValueObjects.Base;

namespace ValueObjects.NP
{
	/// <summary>
	/// Summary description for Collateral.
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	[Serializable]
	[XmlRoot("Collateral")]
	public class Collateral: CigBaseValueObject, ICollateral
	{

		#region Private declaration
		private int id;
		private int caseID;
		private INPCase npCase;
		private ICollateralType collateralType;
		private string collateralTypeID;
		private int owner;
		private string address;
		private ICity addressCity;
		private int addressCityID;
		private IPostcode addressZip;
		private int addressZipID;
		private ICountry addressCountry;
		private int addressCountryID;
		private decimal marketValue;
		private ICigBaseLookupValueObject marketValueType;
		private int marketValueTypeID;

		private int insertedBy;
		private int updatedBy;
		private DateTime inserted;
		private DateTime updated;


		#endregion

		/// <summary>
		/// Default constructor
		/// </summary>
		public Collateral()
		{
			
		}
		#region ICollateral Members

		/// <summary>
		/// ID of the collateral - sequence number
		/// </summary>
		public int Id
		{
			get	{	return id; }
			set	{ id = value; }
		}

		/// <summary>
		/// A unique number used by a participant to identify the contract (Case number)
		/// </summary>
		public int CaseID
		{
			get	{	return caseID;	}
			set	{ caseID = value; }
		}

		/// <summary>
		///  The case this collateral relates to
		/// </summary>
		public INPCase NPCase
		{
			get	{ return npCase; }
			set	{ npCase = value; }
		}

		/// <summary>
		/// Type of collateral
		/// </summary>
		public ICollateralType CollateralType
		{
			get	{ return collateralType; }
			set	{ collateralType = value; }
		}

		/// <summary>
		/// Type id of collateral
		/// </summary>
		public string CollateralTypeID
		{
			get	{	return collateralTypeID; }
			set	{ collateralTypeID = value; }
		}

		/// <summary>
		/// Type name native of collateral
		/// </summary>
		public string CollateralTypeNameNative
		{
			get	{ return collateralType.NameNative; }
		}

		/// <summary>
		/// Type name EN of collateral
		/// </summary>
		public string CollateralTypeNameEN
		{
			get	{	return collateralType.NameEN;	}
		}

		/// <summary>
		/// CIID of either a company or individual
		/// </summary>
		public int Owner
		{
			get {	return owner; }
			set	{ owner = value; }
		}

		/// <summary>
		///  Address of the collateral, if one is supplied
		/// </summary>
		public string Address
		{
			get	{	return address; }
			set	{ address = value; }
		}

		/// <summary>
		/// City name for the collateral address
		/// </summary>
		public ICity AddressCity
		{
			get	{	return addressCity;	}
			set	{ addressCity = value; }
		}

		/// <summary>
		/// City id for the collateral address
		/// </summary>
		public int AddressCityID
		{
			get	{	return addressCityID; }
			set	{ addressCityID = value; }
		}

		/// <summary>
		/// City name native for the collateral address
		/// </summary>
		public string AddressCityNameNative
		{
			get	{ return addressCity.NameNative; }
		}

		/// <summary>
		/// City name en for the collateral address
		/// </summary>
		public string AddressCityNameEN
		{
			get	{ return addressCity.NameEN; }
		}

		/// <summary>
		/// Address ZIP / Postal Code - This code will be used to determine a city name to link with the company address
		/// </summary>
		public IPostcode AddressZip
		{
			get	{ return addressZip; }
			set	{ addressZip = value; }
		}

		/// <summary>
		/// Zip id for the collateral address
		/// </summary>
		public int AddressZipID
		{
			get	{ return addressZipID; }
			set	{ addressZipID = value; }
		}

		/// <summary>
		/// Zip city name native for the collateral address
		/// </summary>
		public string AddressZipNameNative
		{
			get	{ return addressZip.NameNative; }
		}

		/// <summary>
		/// Zip city name EN for the collateral address
		/// </summary>
		public string AddressZipNameEN
		{
			get	{ return addressZip.NameEN; }
		}

		/// <summary>
		/// Country code that identifies the origin of the individual.
		/// </summary>
		public ICountry AddressCountry
		{
			get	{ return addressCountry; }
			set	{ addressCountry = value; }
		}

		/// <summary>
		/// Country code id for the collateral address
		/// </summary>
		public int AddressCountryID
		{
			get	{ return addressCountryID; }
			set { addressCountryID = value; }
		}

		/// <summary>
		///  Country name native for the collateral address
		/// </summary>
		public string AddressCountryNameNative
		{
			get	{ return addressCountry.NameNative; }
		}

		/// <summary>
		/// Country name EN for the collateral address
		/// </summary>
		public string AddressCountryNameEN
		{
			get	{ return addressCountry.NameEN; }
		}

		/// <summary>
		/// Market value of the collateral
		/// </summary>
		public Decimal MarketValue
		{
			get	{ return marketValue; }
			set	{ marketValue = value; }
		}

		/// <summary>
		/// Market value type
		/// </summary>
		public ICigBaseLookupValueObject MarketValueType
		{
			get	{ return marketValueType; }
			set	{ marketValueType = value; }
		}

		/// <summary>
		/// Market value type id
		/// </summary>
		public int MarketValueTypeID
		{
			get	{ return marketValueTypeID; }
			set	{ marketValueTypeID = value; }
		}

		/// <summary>
		/// Market value name native
		/// </summary>
		public string MarketValueTypeNameNative
		{
			get	{ return marketValueType.NameNative; }
		}

		/// <summary>
		/// Market value name EN
		/// </summary>
		public string MarketValueTypeNameEN
		{
			get	{ return marketValueType.NameEN; }
		}

		/// <summary>
		/// The date the party was inserted
		/// </summary>
		public DateTime Inserted
		{
			get { return inserted; }
			set { inserted = value; }
		}

		/// <summary>
		/// Who inserted the party
		/// </summary>
		public int InsertedBy
		{
			get { return insertedBy; }
			set { insertedBy = value; }
		}

		/// <summary>
		/// Date of last update
		/// </summary>
		public DateTime Updated
		{
			get { return updated; }
			set { updated = value; }
		}

		/// <summary>
		/// Who updated.
		/// </summary>
		public int UpdatedBy
		{
			get { return updatedBy; }
			set { updatedBy = value; }
		}

		#endregion
	}
}
