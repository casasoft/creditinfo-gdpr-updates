/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/CollateralType.cs 2     29.06.05 9:33 Ondrakj $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/CollateralType.cs $ 
 * 
 * 2     29.06.05 9:33 Ondrakj
 * comments
 * 
 * 1     23.06.05 9:03 Jirava
*/
using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;
using ValueObjects.NP;

namespace ValueObjects.NP
{
	/// <summary>
	/// CollateralType
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	[Serializable]
	[XmlRoot("CollateralType")]
	public class CollateralType: CigBaseValueObject, ICollateralType
	{

		/// <summary>
		/// Default constructor
		/// </summary>
		public CollateralType()
		{
		}

		#region Private declaration
		private string code;
		private string nameNative;
		private string nameEN;
		#endregion

		/// <summary>
		/// A unique code of the collateral type
		/// </summary>
		public string Code
		{
			get { return code; }
			set { code = value; }
		}

		/// <summary>
		/// Native name of the collateral
		/// </summary>
		public string NameNative 
		{
			get { return nameNative; }
			set { nameNative = value; }
		}

		/// <summary>
		/// English name of the collateral
		/// </summary>
		public string NameEN
		{
			get { return nameEN; }
			set { nameEN = value; }
		}
	}
}
