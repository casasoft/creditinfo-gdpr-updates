/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/RelatedParty.cs 6     4.08.05 7:14 Jirava $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/RelatedParty.cs $ 
 * 
 * 6     4.08.05 7:14 Jirava
 * change in EntityName property
 * 
 * 5     28.07.05 6:55 Jirava
 * change in get EntityName property
 * 
 * 4     26.06.05 12:06 Adalgeir
 * Minor modifications.
 * 
 * 3     20.06.05 10:53 Ondrakj
 * 
 * 2     16.06.05 8:04 Ondrakj
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;
using ValueObjects.Base;
using ValueObjects.General;

namespace ValueObjects.NP
{
	/// <summary>
	/// Summary description for RelatedParty.
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	[Serializable]
	[XmlRoot("RelatedParty")]
	public class RelatedParty : CigBaseValueObject, IRelatedParty
	{
		private int id;
		private int caseId;
		private INPCase npCase;
		private int creditinfoId;
		private int typeId;
		private ICigBaseLookupValueObject type;
		private int insertedBy;
		private int updatedBy;
		private DateTime inserted;
		private DateTime updated;
		private ICigBaseEntity cigEntity;
		private IIndividual individual;
		private ICompany company;
		private int order;

		/// <summary>
		/// Default constructor
		/// </summary>
		public RelatedParty()
		{
		}

		/// <summary>
		/// ID of the related party - sequence number
		/// </summary>
		public int Id
		{
			get { return id; }
			set { id = value; }
		}

		/// <summary>
		/// ID of the case this party is related to
		/// </summary>
		public int CaseId
		{
			get { return caseId; }
			set { caseId = value; }
		}
		/// <summary>
		/// The case this party relates to
		/// </summary>
		public INPCase NPCase
		{
			get { return npCase; }
			set { npCase = value; }
		}

		/// <summary>
		/// Creditinfo id of the party
		/// </summary>
		public int CreditinfoId
		{
			get { return creditinfoId; }
			set { creditinfoId = value; }
		}

		/// <summary>
		/// The CIG entity this related party points to
		/// </summary>
		public ICigBaseEntity CigEntity
		{
			get {return cigEntity;}
			set {cigEntity = value;}
//			set 
//			{
//				if (Company != null)
//				{
//					cigEntity = Company;
//				}
//				if (Individual != null)
//				{
//					cigEntity = Individual;
//				}
//			}

		}

		/// <summary>
		/// Name of the cig entity
		/// </summary>
		public string EntityName
		{
			get
			{
//				if(cigEntity!=null)
//				{
//					if(cigEntity.TypeId == 1)
//					{
//						if(((ICompany)cigEntity).NameNative!=null)
//						{
//							return ((ICompany)cigEntity).NameNative;
//						}
//					}
//					else if(cigEntity.TypeId == 2)
//					{
//						if(((IIndividual)cigEntity).FirstNameNative!=null)
//						{
//							return ((IIndividual)cigEntity).StreetNative;
//						}
//					}
//				}
//				return 	"";		



				if(cigEntity!=null)
				{
					if(cigEntity is ICompany)
					{
						if(((ICompany)cigEntity).NameNative!=null)
							return ((ICompany)cigEntity).NameNative;
					}
					else if(cigEntity is IIndividual)
					{
						if(((IIndividual)cigEntity).FirstNameNative!=null)
							return ((IIndividual)cigEntity).NameNative;
					}
					if (cigEntity.CompanyEntity != null) 
					{
						if (cigEntity.CompanyEntity.NameNative != null)
							return cigEntity.CompanyEntity.NameNative;
					}
					if (cigEntity.IndividualEntity != null)
						if (cigEntity.IndividualEntity.NameNative != null)
							return cigEntity.IndividualEntity.NameNative;
				}
				return "";
												  
			}
		}

		/// <summary>
		/// National id for the cig entity
		/// </summary>
		public string EntityNationalId
		{
			get
			{
				if(cigEntity!=null)
				{
					return cigEntity.NationalId;
				}
				return "";
			}
		}

		/// <summary>
		/// ID of type of relations
		/// </summary>
		public int TypeId
		{
			get { return typeId; }
			set { typeId = value; }
		}

		/// <summary>
		/// Native name of the related party type
		/// </summary>
		public string  TypeNameNative
		{
			get
			{
				if(Type!=null)
					return Type.NameNative;
				return "";
			}
		}

		/// <summary>
		/// English name of the related party type
		/// </summary>
		public string TypeNameEN
		{
			get
			{
				if(Type!=null)
					return Type.NameEN;
				return "";
			}
		}

		/// <summary>
		/// Type of relation
		/// </summary>
		public ICigBaseLookupValueObject Type
		{
			get { return type; }
			set { type = value; }
		}

		/// <summary>
		/// The date the party was inserted
		/// </summary>
		public DateTime Inserted
		{
			get { return inserted; }
			set { inserted = value; }
		}

		/// <summary>
		/// Who inserted the party
		/// </summary>
		public int InsertedBy
		{
			get { return insertedBy; }
			set { insertedBy = value; }
		}

		/// <summary>
		/// Date of last update
		/// </summary>
		public DateTime Updated
		{
			get { return updated; }
			set { updated = value; }
		}

		/// <summary>
		/// Who updated.
		/// </summary>
		public int UpdatedBy
		{
			get { return updatedBy; }
			set { updatedBy = value; }
		}

		/// <summary>
		/// Order of the related types.
		/// </summary>
		public int Order
		{
			get { return order; }
			set { order = value; }
		}

	}
}
