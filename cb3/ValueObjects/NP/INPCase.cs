/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/INPCase.cs 7     26.06.05 12:06 Adalgeir $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/INPCase.cs $ 
 * 
 * 7     26.06.05 12:06 Adalgeir
 * Minor modifications.
 * 
 * 6     24.06.05 9:19 Jirava
 * Added collaterals property
 * 
 * 5     22.06.05 16:44 Ondrakj
 * 
 * 4     16.06.05 8:04 Ondrakj
 * 
 * 3     9.06.05 14:46 Ondrakj
 * 
 * 2     9.06.05 13:53 Adalgeir
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
using System;
using System.Collections;
using ValueObjects.Base;

namespace ValueObjects.NP
{
	/// <summary>
	/// This interface describes a single case.
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	public interface INPCase
	{
		/// <summary>
		/// ID of the case - sequence number
		/// </summary>
		int Id
		{
			get;
			set;
		}
		/// <summary>
		/// Case identification number
		/// </summary>
		string CaseNumber
		{
			get;
			set;
		}
		/// <summary>
		/// ID if the type of case
		/// </summary>
		int CaseTypeId
		{
			get;
			set;
		}
		/// <summary>
		/// NameNative if the type of case
		/// </summary>
		string CaseTypeNameNative
		{
			get;		
		}
		/// <summary>
		/// NameEN if the type of case
		/// </summary>
		string CaseTypeNameEN
		{
			get;			
		}
		/// <summary>
		/// Type of case
		/// </summary>
		ICaseType CaseType
		{
			get;
			set;
		}
		/// <summary>
		/// ID of the information source of the case
		/// </summary>
		int InformationSourceId
		{
			get;
			set;
		}
		/// <summary>
		/// Information source of the case
		/// </summary>
		ICigBaseLookupValueObject InformationSource
		{
			get;
			set;
		}
		/// <summary>
		/// Information source NameNative of the case
		/// </summary>
		string InformationSourceNameNative
		{
			get;			
		}

		/// <summary>
		/// Information source Name|EN of the case
		/// </summary>
		string InformationSourceNameEN
		{
			get;			
		}
		/// <summary>
		/// The date the party was inserted
		/// </summary>
		DateTime Inserted
		{
			get;
			set;
		}
		/// <summary>
		/// Who inserted the party
		/// </summary>
		int InsertedBy
		{
			get;
			set;
		}
		/// <summary>
		/// Date of last update
		/// </summary>
		DateTime Updated
		{
			get;
			set;
		}
		/// <summary>
		/// Who updated.
		/// </summary>
		int UpdatedBy
		{
			get;
			set;
		}
		/// <summary>
		/// Registration date of the case
		/// </summary>
		DateTime CaseDate
		{
			get;
			set;
		}
		/// <summary>
		/// Status id of the case
		/// </summary>
		int StatusId
		{
			get;
			set;
		}
		/// <summary>
		/// Status of the case
		/// </summary>
		ICigBaseLookupValueObject Status
		{
			get;
			set;
		}

		/// <summary>
		/// Status NameNative of the case
		/// </summary>
		string StatusNameNative
		{
			get;
			
		}

		/// <summary>
		/// Status NameEn of the case
		/// </summary>
		string StatusNameEN
		{
			get;			
		}

		

		/// <summary>
		/// Notes for case
		/// </summary>
		IList Notes
		{
			get;
			set;
		}

		/// <summary>
		/// List of related parties for case
		/// </summary>
		IList RelatedParties
		{
			get;
			set;
		}

		/// <summary>
		/// List of collaterals for case
		/// </summary>
		IList Collaterals
		{
			get;
			set;
		}

		/// <summary>
		/// Debtor Credinfo Id
		/// </summary>
		int DebtorCreditInfoID
		{
			get;
		}
		/// <summary>
		/// Creditor Credinfo Id
		/// </summary>
		int CreditorCreditInfoID
		{
			get;
		}
		/// <summary>
		/// Warrant Credinfo Id
		/// </summary>
		int WarrantCreditInfoID
		{
			get;
		}


		/// <summary>
		/// List of related parties for case
		/// </summary>
		Hashtable DynamicValues
		{
			get;
			set;
		}
	}
}
