/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/INote.cs 1     2.06.05 13:30 Haukur $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/INote.cs $ 
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
using System;

namespace ValueObjects.NP
{
	/// <summary>
	/// This interface describes a note for case.  Each case can have 
	/// multiple noted order by the Order field.  Notes can be both internal or external.
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	public interface INote
	{
		/// <summary>
		/// ID of the note - sequence number
		/// </summary>
		int Id
		{
			get;
			set;
		}
		/// <summary>
		/// ID of the case this note belongs to
		/// </summary>
		int CaseId
		{
			get;
			set;
		}
		/// <summary>
		/// The case this note belongs to
		/// </summary>
		INPCase NPCase
		{
			get;
			set;
		}
		/// <summary>
		/// Order of the note
		/// </summary>
		int Order
		{
			get;
			set;
		}
		/// <summary>
		/// Native text of the note
		/// </summary>
		string NoteNative
		{
			get;
			set;
		}
		/// <summary>
		/// English text of the note
		/// </summary>
		string NoteEN
		{
			get;
			set;
		}
		/// <summary>
		/// The date the note was inserted
		/// </summary>
		DateTime Inserted
		{
			get;
			set;
		}
		/// <summary>
		/// Who inserted the note
		/// </summary>
		int InsertedBy
		{
			get;
			set;
		}
		/// <summary>
		/// Date of last update
		/// </summary>
		DateTime Updated
		{
			get;
			set;
		}
		/// <summary>
		/// Who updated.
		/// </summary>
		int UpdatedBy
		{
			get;
			set;
		}
		/// <summary>
		/// Is the note internal or external
		/// </summary>
		string IsExternal
		{
			get;
			set;
		}
		/// <summary>
		/// Boolean representation for IsExternal
		/// </summary>
		bool BoolIsExternal
		{
			get;
			set;
		}
	}
}
