/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/NPCase.cs 8     29.06.05 9:33 Ondrakj $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/NPCase.cs $ 
 * 
 * 8     29.06.05 9:33 Ondrakj
 * comments
 * 
 * 7     26.06.05 12:06 Adalgeir
 * Minor modifications.
 * 
 * 6     24.06.05 9:19 Jirava
 * Added collaterals property
 * 
 * 5     22.06.05 16:44 Ondrakj
 * 
 * 3     16.06.05 8:04 Ondrakj
 * 
 * 2     9.06.05 14:46 Ondrakj
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
using System;
using System.Collections;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;
using ValueObjects.Base;
using ValueObjects.NP;

namespace ValueObjects.NP
{
	/// <summary>
	/// Summary description for Case.
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	[Serializable]
	[XmlRoot("Case")]
	public class NPCase : CigBaseValueObject, INPCase
	{
		private int id;
		private string caseNumber;
		private int caseTypeID;
		private ICaseType caseType;
		private int informationSourceId;
		private ICigBaseLookupValueObject informationSource;
		private int insertedBy;
		private int updatedBy;
		private DateTime caseDate;
		private int statusId;
		private ICigBaseLookupValueObject status;
		private IList notes;
		private IList relatedParties;
		private IList collaterals;
		private DateTime inserted;
		private DateTime updated;
		private Hashtable dynamicValues;

		/// <summary>
		/// Default constructor
		/// </summary>
		public NPCase()
		{
		}

		/// <summary>
		/// ID of the case - sequence number
		/// </summary>
		public int Id
		{
			get { return id; }
			set { id = value; }
		}

		/// <summary>
		/// Case identification number
		/// </summary>
		public string CaseNumber
		{
			get {return caseNumber;}
			set {caseNumber = value;}
		}

		/// <summary>
		/// ID if the type of case
		/// </summary>
		public int CaseTypeId
		{
			get { return caseTypeID; }
			set { caseTypeID = value; }
		}

		/// <summary>
		/// Type of case
		/// </summary>
		public ICaseType CaseType
		{
			get { return caseType; }
			set { caseType = value; }
		}
		/// <summary>
		/// NameNative if the type of case
		/// </summary>
		public string CaseTypeNameNative
		{
			get { return CaseType.NameNative; }
			
		}
		/// <summary>
		/// NameEN if the type of case
		/// </summary>
		public string CaseTypeNameEN
		{
			get { return CaseType.NameEN; }
			
		}
		/// <summary>
		/// ID of the information source of the case
		/// </summary>
		public int InformationSourceId
		{
			get { return informationSourceId; }
			set { informationSourceId = value; }
		}

		/// <summary>
		/// Information source of the case
		/// </summary>
		public ICigBaseLookupValueObject InformationSource
		{
			get { return informationSource; }
			set { informationSource = value; }
		}

		/// <summary>
		/// Information source NameNative of the case
		/// </summary>
		public string InformationSourceNameEN
		{
			get { return informationSource.NameEN; }
			
		}

		/// <summary>
		/// Information source NameNative of the case
		/// </summary>
		public string InformationSourceNameNative
		{
			get { return informationSource.NameNative; }			
		}		
		/// <summary>
		/// The date the party was inserted
		/// </summary>
		public DateTime Inserted
		{
			get { return inserted; }
			set { inserted = value; }
		}

		/// <summary>
		/// Who inserted the party
		/// </summary>
		public int InsertedBy
		{
			get { return insertedBy; }
			set { insertedBy = value; }
		}

		/// <summary>
		/// Date of last update
		/// </summary>
		public DateTime Updated
		{
			get { return updated; }
			set { updated = value; }
		}

		/// <summary>
		/// Who updated.
		/// </summary>
		public int UpdatedBy
		{
			get { return updatedBy; }
			set { updatedBy = value; }
		}

		/// <summary>
		/// Registration date of the case
		/// </summary>
		public DateTime CaseDate
		{
			get { return caseDate; }
			set { caseDate = value; }
		}

		/// <summary>
		/// Status id of the case
		/// </summary>
		public int StatusId
		{
			get { return statusId; }
			set { statusId = value; }
		}

		/// <summary>
		/// Status of the case
		/// </summary>
		public ICigBaseLookupValueObject Status
		{
			get { return status; }
			set { status = value; }
		}

		/// <summary>
		/// Status NameNative of the case
		/// </summary>
		public string StatusNameNative
		{
			get { return status.NameNative; }
			
		}

		/// <summary>
		/// Status NameEN of the case
		/// </summary>
		public string StatusNameEN
		{
			get { return status.NameEN; }
			
		}

		/// <summary>
		/// Notes for case
		/// </summary>
		public IList Notes
		{
			get { return notes; }
			set { notes = value; }
		}

		/// <summary>
		/// List of related parties for case
		/// </summary>
		public IList RelatedParties
		{
			get { return relatedParties; }
			set { relatedParties = value; }
		}

		/// <summary>
		/// List of collaterals for case
		/// </summary>
		public IList Collaterals 
		{
			get { return collaterals; }
			set { collaterals = value; }
		}

		/// <summary>
		/// Debtor creditinfo id for case
		/// </summary>
		public int DebtorCreditInfoID
		{
			get
			{
				
				int creditinfoIdToreturn = -1;
				if(relatedParties!=null && relatedParties.Count>0)
				{
					for (int i = 0; i< relatedParties.Count; i++)
					{

						if (Convert.ToInt16(((IRelatedParty)relatedParties[i]).TypeId) == 1)
							{
								creditinfoIdToreturn = ((IRelatedParty)relatedParties[i]).CreditinfoId;
							}
					}
				}
				
				return creditinfoIdToreturn;
			}
			
		}

		/// <summary>
		/// Creditor creditinfo id for case
		/// </summary>
		public int CreditorCreditInfoID
		{
			get
			{
				int creditinfoIdToreturn = -1;
				if(relatedParties!=null && relatedParties.Count>0)
				{
					for (int i = 0; i< relatedParties.Count; i++)
					{
						//configurable Creditor type ID

						if (Convert.ToInt16(((IRelatedParty)relatedParties[i]).TypeId) == 2)
						{
							creditinfoIdToreturn = ((IRelatedParty)relatedParties[i]).CreditinfoId;
						}
					}
				}
				
				return creditinfoIdToreturn;
			}
			
		}

		/// <summary>
		/// Warrant creditinfo id for case
		/// </summary>
		public int WarrantCreditInfoID
		{
			get
			{
				int creditinfoIdToreturn = -1;
				if(relatedParties!=null && relatedParties.Count>0)
				{
					for (int i = 0; i< relatedParties.Count; i++)
					{
						//configurable Warrant type ID
						if (Convert.ToInt16(((IRelatedParty)relatedParties[i]).TypeId) == 4)
						{
							creditinfoIdToreturn = ((IRelatedParty)relatedParties[i]).CreditinfoId;
						}
					}
				}
				
				return creditinfoIdToreturn;
			}
			
		}
		

		/// <summary>
		/// List of related parties for case
		/// </summary>
		public Hashtable DynamicValues
		{
			get { return dynamicValues; }
			set { dynamicValues = value; }
		}
	}
}
