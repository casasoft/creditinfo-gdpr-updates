/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/IRelatedParty.cs 2     26.06.05 12:06 Adalgeir $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/IRelatedParty.cs $ 
 * 
 * 2     26.06.05 12:06 Adalgeir
 * Minor modifications.
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
using System;
using ValueObjects.General;
using ValueObjects.Base;

namespace ValueObjects.NP
{
	/// <summary>
	/// This interface describes related party for case.  Case can have multiple related parties
	/// associated and they all have type of relation.
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	public interface IRelatedParty
	{
		/// <summary>
		/// ID of the related party - sequence number
		/// </summary>
		int Id
		{
			get;
			set;
		}
		/// <summary>
		/// ID of the case this party is related to
		/// </summary>
		int CaseId
		{
			get;
			set;
		}
		/// <summary>
		/// The case this party relates to
		/// </summary>
		INPCase NPCase
		{
			get;
			set;
		}
		/// <summary>
		/// Creditinfo id of the party
		/// </summary>
		int CreditinfoId
		{
			get;
			set;
		}

		/// <summary>
		/// The cig entity class matching the CreditinfoID
		/// </summary>
		ICigBaseEntity CigEntity
		{
			get;
			set;
		}

		/// <summary>
		/// Name of the cig entity
		/// </summary>
		string EntityName
		{
			get;
		}

		/// <summary>
		/// National id for the cig entity
		/// </summary>
		string EntityNationalId
		{
			get;
		}

		/// <summary>
		/// ID of type of relations
		/// </summary>
		int TypeId
		{
			get;
			set;
		}

		/// <summary>
		/// Native name of the related party type
		/// </summary>
		string TypeNameNative
		{
			get;
		}

		/// <summary>
		/// English name of the related party type
		/// </summary>
		string TypeNameEN
		{
			get;
		}

		/// <summary>
		/// Type of relation
		/// </summary>
		ICigBaseLookupValueObject Type
		{
			get;
			set;
		}
		/// <summary>
		/// The date the party was inserted
		/// </summary>
		DateTime Inserted
		{
			get;
			set;
		}
		/// <summary>
		/// Who inserted the party
		/// </summary>
		int InsertedBy
		{
			get;
			set;
		}
		/// <summary>
		/// Date of last update
		/// </summary>
		DateTime Updated
		{
			get;
			set;
		}
		/// <summary>
		/// Who updated.
		/// </summary>
		int UpdatedBy
		{
			get;
			set;
		}

		/// <summary>
		/// Order of the related types.
		/// </summary>
		int Order
		{
			get;
			set;
		}
	}
}
