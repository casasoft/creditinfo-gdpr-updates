/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/CaseType.cs 2     26.06.05 12:06 Adalgeir $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/CaseType.cs $ 
 * 
 * 2     26.06.05 12:06 Adalgeir
 * Minor modifications.
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.NP
{
	/// <summary>
	/// The CaseType class represent type of negative data
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	[Serializable]
	[XmlRoot("CaseType")]
	public class CaseType : CigBaseValueObject, ICaseType
	{
		private int id;
		private string nameNative;
		private string nameEN;
		private string letter;

		/// <summary>
		/// Default constructor
		/// </summary>
		public CaseType()
		{}

		/// <summary>
		/// A unique code of the case type
		/// </summary>
		public int Id
		{
			get { return id; }
			set { id = value; }
		}

		/// <summary>
		/// Native name of the case type
		/// </summary>
		public string NameNative 
		{
			get { return nameNative; }
			set { nameNative = value; }
		}

		/// <summary>
		/// English name of the case type
		/// </summary>
		public string NameEN
		{
			get { return nameEN; }
			set { nameEN = value; }
		}

		/// <summary>
		/// Is letter status.
		/// </summary>
		public string Letter
		{
			get { return letter; }
			set { letter = value; }
		}

		/// <summary>
		/// Boolean representation for Letter.
		/// </summary>
		public bool BoolLetter
		{
			get { return bool.Parse(letter); }
			set
			{
				if(value)
					letter="True";
				else
					letter="False";
			}
		}
	}
}
