/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/CollateralValueType.cs 2     29.06.05 9:33 Ondrakj $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/CollateralValueType.cs $ 
 * 
 * 2     29.06.05 9:33 Ondrakj
 * comments
 * 
 * 1     23.06.05 9:03 Jirava
*/
using System;
using System.Xml.Serialization;
using ValueObjects.Base;

namespace ValueObjects.NP
{
	/// <summary>
	/// The CollateralValueType class represent type of a market value type
	/// </summary>
	[Serializable]
	[XmlRoot("CollateralValueType")]
	public class CollateralValueType : CigBaseLookupValueObject, ICigBaseLookupValueObject
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		public CollateralValueType()
		{
			
		}
	}
}
