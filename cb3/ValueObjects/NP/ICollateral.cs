/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/ICollateral.cs 3     27.06.05 14:21 Jirava $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/ICollateral.cs $ 
 * 
 * 3     27.06.05 14:21 Jirava
 * added new property AddressCityNameEN
 * 
 * 2     24.06.05 6:54 Jirava
 * Collaterals objects
 * 
 * 1     23.06.05 9:02 Jirava
*/
using System;
using ValueObjects.Base;
using ValueObjects.General;

namespace ValueObjects.NP
{
	/// <summary>
	/// This interface describes a single collateral.
	/// </summary>
	public interface ICollateral
	{
		/// <summary>
		/// ID of the collateral - sequence number
		/// </summary>
		int Id
		{
			get;
			set;
		}

		/// <summary>
		/// A unique number used by a participant to identify the contract (Case number)
		/// </summary>
		int CaseID
		{
			get;
			set;
		}

		/// <summary>
		/// The case this collateral relates to
		/// </summary>
		INPCase NPCase
		{
			get;
			set;
		}

		/// <summary>
		/// Type of collateral
		/// </summary>
		ICollateralType CollateralType
		{
			get;
			set;
		}

		/// <summary>
		/// Type id of collateral
		/// </summary>
		string CollateralTypeID
		{
			get;
			set; 
		}

		/// <summary>
		/// Type name native of collateral
		/// </summary>
		string CollateralTypeNameNative
		{
			get; 
		}

		/// <summary>
		/// Type name EN of collateral
		/// </summary>
		string CollateralTypeNameEN
		{
			get;
		}

		/// <summary>
		/// CIID of either a company or individual
		/// </summary>
		int Owner
		{
			get;
			set;
		}

		/// <summary>
		/// Address of the collateral, if one is supplied
		/// </summary>
		string Address
		{
			get;
			set;
		}

		/// <summary>
		/// City name for the collateral address
		/// </summary>
		ICity AddressCity
		{
			get;
			set;
		}

		/// <summary>
		/// City id for the collateral address
		/// </summary>
		int AddressCityID
		{
			get;
			set;
		}

		/// <summary>
		/// City name native for the collateral address
		/// </summary>
		string AddressCityNameNative
		{ 
			get;
		}

		/// <summary>
		/// City name EN for the collateral address
		/// </summary>
		string AddressCityNameEN
		{
			get;
		}
		/// <summary>
		/// Address ZIP / Postal Code - This code will be used to determine a city name to link with the company address
		/// </summary>
		IPostcode AddressZip
		{
			get;
			set;
		}

		/// <summary>
		/// Zip id for the collateral address
		/// </summary>
		int AddressZipID
		{
			get;
			set;
		}

		/// <summary>
		/// Zip city name native for the collateral address
		/// </summary>
		string AddressZipNameNative
		{ 
			get;
		}

		/// <summary>
		/// Zip city name EN for the collateral address
		/// </summary>
		string AddressZipNameEN
		{ 
			get;
		}

		/// <summary>
		/// Country code that identifies the origin of the individual.
		/// </summary>
		ICountry AddressCountry
		{
			get;
			set;
		}

		/// <summary>
		/// Country code id for the collateral address
		/// </summary>
		int AddressCountryID
		{
			get;
			set;
		}

		/// <summary>
		/// Country name native for the collateral address
		/// </summary>
		string AddressCountryNameNative
		{ 
			get;
		}

		/// <summary>
		/// Country name EN for the collateral address
		/// </summary>
		string AddressCountryNameEN
		{ 
			get;
		}

		/// <summary>
		/// Market value of the collateral
		/// </summary>
		Decimal MarketValue
		{
			get;
			set;
		}

		/// <summary>
		/// Market value type
		/// </summary>
		ICigBaseLookupValueObject MarketValueType
		{
			get;
			set;
		}

		/// <summary>
		/// Market value type id
		/// </summary>
		int MarketValueTypeID
		{
			get;
			set;
		}

		/// <summary>
		/// Market value name native
		/// </summary>
		string MarketValueTypeNameNative
		{
			get;
		}

		/// <summary>
		/// Market value name EN
		/// </summary>
		string MarketValueTypeNameEN
		{
			get;
		}

		/// <summary>
		/// The date the party was inserted
		/// </summary>
		DateTime Inserted
		{
			get;
			set;
		}
		/// <summary>
		/// Who inserted the party
		/// </summary>
		int InsertedBy
		{
			get;
			set;
		}
		/// <summary>
		/// Date of last update
		/// </summary>
		DateTime Updated
		{
			get;
			set;
		}
		/// <summary>
		/// Who updated.
		/// </summary>
		int UpdatedBy
		{
			get;
			set;
		}
	}
}
