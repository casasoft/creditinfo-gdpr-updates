/* 
 * $Header: /CIG_System/CreditinfoGroup_v2/ValueObjects/NP/Note.cs 2     10.06.05 10:49 Haukur $
 * $Log: /CIG_System/CreditinfoGroup_v2/ValueObjects/NP/Note.cs $ 
 * 
 * 2     10.06.05 10:49 Haukur
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
using System;
using System.Xml.Serialization;
using Cig.Framework.Data.ValueObject.Base;

namespace ValueObjects.NP
{
	/// <summary>
	/// Summary description for Note.
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	[Serializable]
	[XmlRoot("Note")]
	public class Note : CigBaseValueObject, INote
	{
		private int id;
		private int caseId;
		private INPCase npCase;
		private string noteNative;
		private int order;
		private string noteEN;
		private int insertedBy;
		private int updatedBy;
		private DateTime inserted;
		private DateTime updated;
		private string isExternal;

		/// <summary>
		/// Default constructor
		/// </summary>
		public Note()
		{}

		/// <summary>
		/// ID of the note - sequence number
		/// </summary>
		public int Id
		{
			get { return id; }
			set { id = value; }
		}

		/// <summary>
		/// ID of the case this note belongs to
		/// </summary>
		public int CaseId
		{
			get { return caseId; }
			set { caseId = value; }
		}
		/// <summary>
		/// The case this note belongs to
		/// </summary>
		public INPCase NPCase
		{
			get { return npCase; }
			set { npCase = value; }
		}

		/// <summary>
		/// Order of the note
		/// </summary>
		public int Order
		{
			get { return order; }
			set { order = value; }
		}

		/// <summary>
		/// Native text of the note
		/// </summary>
		public string NoteNative
		{
			get { return noteNative; }
			set { noteNative = value; }
		}

		/// <summary>
		/// English text of the note
		/// </summary>
		public string NoteEN
		{
			get { return noteEN; }
			set { noteEN = value; }
		}

		/// <summary>
		/// The date the note was inserted
		/// </summary>
		public DateTime Inserted
		{
			get { return inserted; }
			set { inserted = value; }
		}

		/// <summary>
		/// Who inserted the note
		/// </summary>
		public int InsertedBy
		{
			get { return insertedBy; }
			set { insertedBy = value; }
		}

		/// <summary>
		/// Date of last update
		/// </summary>
		public DateTime Updated
		{
			get { return updated; }
			set { updated = value; }
		}

		/// <summary>
		/// Who updated.
		/// </summary>
		public int UpdatedBy
		{
			get { return updatedBy; }
			set { updatedBy = value; }
		}

		/// <summary>
		/// Is the note internal or external
		/// </summary>
		public string IsExternal
		{
			get
			{
				if(isExternal == null)
					return "False";
				else
					return isExternal;
			}
			set { isExternal = value; }
		}

		/// <summary>
		/// Boolean representation for IsExternal
		/// </summary>
		public bool BoolIsExternal
		{
			get { return bool.Parse(isExternal); }
			set
			{
				if(value)
					isExternal="True";
				else
					isExternal="False";
			}
		}
	}
}
