/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/ICollateralType.cs 1     23.06.05 9:03 Jirava $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/ICollateralType.cs $ 
 * 
 * 1     23.06.05 9:03 Jirava
*/
namespace ValueObjects.NP
{
	/// <summary>
	/// This interface describe a single collateral type
	/// </summary>
	public interface ICollateralType
	{

		/// <summary>
		/// A unique code of the collateral type
		/// </summary>
		string Code
		{
			get;
			set;
		}

		/// <summary>
		/// Native name of the collateral
		/// </summary>
		string NameNative
		{
			get;
			set;
		}

		/// <summary>
		/// English name of the collateral
		/// </summary>
		string NameEN
		{
			get;
			set;
		}
	}
}
