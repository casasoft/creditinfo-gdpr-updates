/* 
 * $Header: /CIG_System/CreditInfoGroup/ValueObjects/NP/CaseInformationSource.cs 1     2.06.05 13:30 Haukur $
 * $Log: /CIG_System/CreditInfoGroup/ValueObjects/NP/CaseInformationSource.cs $ 
 * 
 * 1     2.06.05 13:30 Haukur
 * File added
 */
using System;
using System.Xml.Serialization;
using ValueObjects.Base;

namespace ValueObjects.NP
{
	/// <summary>
	/// The CaseInformationSource class represent one information source for negative data
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	[Serializable]
	[XmlRoot("CaseInformationSource")]
	public class CaseInformationSource : CigBaseLookupValueObject, ICigBaseLookupValueObject
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		public CaseInformationSource()
		{
		}
	}
}
