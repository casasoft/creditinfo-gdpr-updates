<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/head.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="new_user_controls/options.ascx" %>
<%@ Page language="c#" Codebehind="Logon_old.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.Logon" %>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en" >
<html>
  <head>
		<title>CIG - Login</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="css/CIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
	</style>
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						search.btnLogon.click(); 
					}
				} 
		
				function SetFormFocus() {
					document.search.tbUserName.focus();
	}

		</script>
</head>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="search" name="search" action="" method="post" runat="server">
			<table width="500" height="500" align="center">
				<tr valign="middle">
					<td width="1"></td>
					<td width="100%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<img src="img/creditinfoschufa_logo_4web.jpg" width="206px" height="46px" alt="CreditInfo Group">
								</td>
								<td valign="bottom" align="right" style="PADDING-RIGHT: 3px">
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
							</tr>
							<!--tr>
								<td height="5"></td>
							</tr-->
							<tr valign="top">
								<td colspan="2">
									<!-- Main Body -->
									<table width="100%">
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbLogin" runat="server" cssclass="HeadMain">Login</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lbUserName" runat="server">User name</asp:label>:</td>
																	<td>
																		<asp:textbox id="tbUserName" tabindex="1" runat="server"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lbPassword" runat="server">Password</asp:label>:</td>
																	<td>
																		<asp:textbox id="tbPassword" tabindex="2" runat="server" textmode="Password"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left" >
															<asp:label id="lblMessage" runat="server" cssclass="error_text"></asp:label>
														</td>
														<td align="right">
															<asp:button id="btCancel" runat="server" cssclass="cancel_button" text="Cancel"></asp:button><asp:button id="btnLogon" tabindex="3" runat="server" cssclass="confirm_button" text="Logon"></asp:button>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- Main Body -->
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td align="left" colspan="2">
									<div class="head_foot">Creditinfo Group Ltd | <a href="http://www.creditinfo.com" target="_blank">
											www.creditinfo.com</a> | <a href="mailto:info@creditinfo.com.mt">info@creditinfo.com.mt</a></div>
								</td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
			</table>
		</form>
	</body>
</html>
