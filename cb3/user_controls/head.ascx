<%@ Register TagPrefix="cc1" Namespace="CYBERAKT.WebControls.Navigation" Assembly="ASPnetMenu" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="head.ascx.cs" Inherits="CreditInfoGroup.user_controls.head" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<HTML>
	<HEAD>
		<title>head</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="<%=Request.ApplicationPath%>/styles/cy_styles.css" type="text/css" rel="stylesheet">
		<LINK href="<%=Request.ApplicationPath%>/styles/menustyle.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<table id="Table3" borderColor="#0000ff" cellSpacing="0" cellPadding="0" width="100%" align="center"
			bgColor="#ffffff" border="0">
			<TBODY>
				<tr>
					<td vAlign="top" align="left" height="50">&nbsp;<IMG src="<%=Request.ApplicationPath%>/img/haus2-400.bmp"></td>
				</tr>
				<tr>
					<td><cc1:aspnetmenu id="myMenu" DefaultItemSpacing="0" DefaultItemCssClassOver="MenuItemOver" DefaultItemCssClass="MenuItem"
							DefaultGroupCssClass="MenuGroup" runat="server" CausesValidation="False" Visible="False"></cc1:aspnetmenu></td>
				</tr>
				<tr>
					<td vAlign="top" align="center" height="100%">
						<table class="list" id="Table2" cellSpacing="0" cellPadding="2" width="100%" align="center">
							<TBODY>
								<tr class="dark-row">
									<td style="WIDTH: 34px" align="left"><asp:imagebutton id="imbtEN" runat="server" ImageUrl="<%=Request.ApplicationPath%>/img/uk.gif" Height="20px"
											CausesValidation="False" ImageAlign="Middle" Width="30px"></asp:imagebutton></td>
									<td align="left"><asp:imagebutton id="imgbtIS" runat="server" ImageUrl="<%=Request.ApplicationPath%>/img/iceland.gif" Height="20px"
											CausesValidation="False" ImageAlign="Middle" Width="30px"></asp:imagebutton>&nbsp;
										<asp:linkbutton id="hlbtnLogout" runat="server" Visible="False" CausesValidation="False">Logout</asp:linkbutton></td>
									<TD align="right"><asp:linkbutton id="lnkbtnPassword" runat="server">Forgot your password?</asp:linkbutton></TD>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
			</TBODY>
		</table>
	</body>
</HTML>
