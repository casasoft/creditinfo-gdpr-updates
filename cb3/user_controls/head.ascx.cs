using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;
using CYBERAKT.WebControls.Navigation;
using UserAdmin.BLL;
using MenuItem=CYBERAKT.WebControls.Navigation.MenuItem;

using Cig.Framework.Base.Configuration;

namespace CreditInfoGroup.user_controls {
    /// <summary>
    ///		Summary description for head.
    /// </summary>
    public class head : UserControl {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        protected LinkButton hlbtnLogout;
        protected HyperLink hlCreditWatch;
        protected HyperLink hlNegativePayments;
        protected HyperLink hlSearchClaims;
        protected HyperLink hlUploadClaims;
        protected HyperLink hlUserAdmin;
        protected ImageButton imbtEN;
        protected ImageButton imgbtIS;
        protected LinkButton lnkbtnPassword;
        protected ASPnetMenu myMenu;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();

            hlbtnLogout.Visible = Context.User.Identity.IsAuthenticated;

            if (!IsPostBack) {
                BuildSimpleMenu();
            }
        }

        private void imbtEN_Click(object sender, ImageClickEventArgs e) {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            Session["culture"] = culture;
            // forcing reload to get the proper Culture in global.asax
            Response.AddHeader("Refresh", "0");
        }

        private void imgbtCYP_Click(object sender, ImageClickEventArgs e) {
            //	Thread.CurrentThread.CurrentCulture = new CultureInfo("el-GR",true);
            Thread.CurrentThread.CurrentCulture = new CultureInfo("is-IS", true);
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            Session["culture"] = culture;
            // forcing reload to get the proper Culture in global.asax
            Response.AddHeader("Refresh", "0");
        }

        private void LocalizeText() {
            hlbtnLogout.Text = rm.GetString("txtLogOut", ci);
            lnkbtnPassword.Text = rm.GetString("lnkbtnPassword", ci);
        }

        private void hlbtnLogout_Click(object sender, EventArgs e) {
            FormsAuthentication.SignOut();
            Response.AddHeader("Refresh", "0");
            Session.Abandon();
            // Server.Transfer("CreditInfoGroup_is/Default.aspx");
            hlbtnLogout.Text = rm.GetString("txtLogOut", ci);
        }

        // Byggja upp menu me� �v� a� lesa upp �r Products t�flunni...
        private void BuildSimpleMenu() {
            if (Context.User.Identity.Name == "") {
                return;
            }

            // Nota caching
            var myProductSet = (DataSet) Cache["myProductSet"];

            if (myProductSet == null) {
                myProductSet = myFactory.GetAllTheOpenProducts();
                Cache["myProductSet"] = myProductSet;
            }

            MenuItem newItem;
            MenuGroup newGroup = null;

            // Athuga hvort notandi hafi a�gang a� v�rum. Birta menu samkv�mt a�gangi notenda...
            for (int i = 0; i < myProductSet.Tables[0].Rows.Count; i++) {
                // Fyrir Parent gr�ppu - n.b. Datasetti� kemur ra�a� m.v. parentID og svo ProductID. Parents eru me�
                // "0" � ParentID og koma �v� fremst...
                if ((int) myProductSet.Tables[0].Rows[i]["ParentID"] == 0) {
                    // Ef notandinn hefur a�gang a� �essari v�ru...
                    if (Context.User.IsInRole(myProductSet.Tables[0].Rows[i]["ProductID"].ToString())) {
                        newItem = myMenu.TopGroup.Items.Add();

                        // Fyrir native e�a ensku...
                        newItem.Label = ci.Name == Session["Defculture"].ToString() ? myProductSet.Tables[0].Rows[i]["ProductNameNative"].ToString() : myProductSet.Tables[0].Rows[i]["ProductNameEN"].ToString();

                        // Fyrri partur af URL (root url) tekinn �r web.config...
                        newItem.URL = CigConfig.Configure("lookupsettings.rootURL") +
                                      myProductSet.Tables[0].Rows[i]["NavigateUrl"];
                        newItem.ID = myProductSet.Tables[0].Rows[i]["ProductID"].ToString();
                    }
                }
                    // Fyrir child gr�ppu. Ra�ast undir parent menu items...
                else {
                    // Ef notandi hefur a�gang a� parent...
                    if (Context.User.IsInRole(myProductSet.Tables[0].Rows[i]["ParentID"].ToString())) {
                        // Ef child gr�ppa er ekki til fyrir parent �� er h�n b�in til...
                        if (myMenu.TopGroup.Items[(myProductSet.Tables[0].Rows[i]["ParentID"]).ToString()].SubGroup ==
                            null) {
                            newGroup =
                                myMenu.TopGroup.Items[(myProductSet.Tables[0].Rows[i]["ParentID"]).ToString()].
                                    AddSubGroup();
                        }

                        if (newGroup != null) {
                            newItem = newGroup.Items.Add();

                            // Fyrir native e�a ensku...
                            newItem.Label = ci.Name == CigConfig.Configure("lookupsettings.nativeCulture") ? myProductSet.Tables[0].Rows[i]["ProductNameNative"].ToString() : myProductSet.Tables[0].Rows[i]["ProductNameEN"].ToString();

                            newItem.URL = CigConfig.Configure("lookupsettings.rootURL") +
                                          myProductSet.Tables[0].Rows[i]["NavigateUrl"];
                            newItem.ID = myProductSet.Tables[0].Rows[i]["ProductID"].ToString();
                        }
                    }
                }
            }

            if (myMenu.TopGroup.Items.Count > 0) {
                myMenu.TopGroup.Width = "600";
                myMenu.Visible = true;
            } else {
                myMenu.Visible = false;
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.imbtEN.Click += new System.Web.UI.ImageClickEventHandler(this.imbtEN_Click);
            this.imgbtIS.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtCYP_Click);
            this.hlbtnLogout.Click += new System.EventHandler(this.hlbtnLogout_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}