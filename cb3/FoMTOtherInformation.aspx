<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="new_user_controls/FoOptions.ascx" %>
<%@ Page language="c#" Codebehind="FoMTOtherInformation.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.FoMTOtherInformation" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Other Information</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="popup.js"></script>
		<link href="css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						FoOtherInformation.btnFind.click(); 
					}
				} 
				
				function clear_info(){
					FoOtherInformation.txtNationalID.value='';
					FoOtherInformation.txtName.value='';
					return false;
				}
		
				function SetFormFocus()
				{
					document.FoOtherInformation.txtName.focus();
				}
				
		</script>
	</HEAD>
	<body style="BACKGROUND-IMAGE: url(img/mainback.gif)" leftmargin="0" onload="SetFormFocus()"
		rightmargin="0" ms_positioning="GridLayout">
		<form id="FoOtherInformation" title="Other Information" name="Other Information" method="post"
			runat="server">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
								<td>
									<table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="#951e16" border="0">
										<tr>
											<td bgcolor="#951e16"><span style="WIDTH: 3px"></span></td>
											<td bgcolor="#951e16"><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
											<td align="right" bgcolor="#951e16"><uc1:language id="Language2" runat="server"></uc1:language></td>
										</tr>
									</table>
								</td>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="white" border="0">
							<tr>
								<td>
									<p>
										<table id="tbDefault" cellspacing="0" cellpadding="0" width="100%" align="center">
											<tr>
												<td>
													<table cellspacing="0" cellpadding="0" border="0" width="97%" align="center">
														<tr>
															<td>
																<uc1:userinfo id="UserInfo1" runat="server"></uc1:userinfo>
															</td>
															<td align="right">
																<uc1:fooptions id="FoOptions1" runat="server"></uc1:fooptions></td>
														</tr>
													</table>
												</td>
											</tr>
											<!-- Content begins -->
											<tr>
												<td>
													<table id="Table5" cellspacing="0" cellpadding="0" width="97%" border="0" align="center">
														<tr>
															<td class="pageheader" colspan="4"><asp:label id="lblOtherInformation" runat="server" cssclass="HeadMain">Other Information</asp:label></td>
														</tr>
														<tr>
															<td colspan="4" height="15"></td>
														</tr>
														<tr>
															<td class="sectionheader" colspan="4">
																<asp:label id="lblTitle" runat="server" cssclass="sectionheader">Interdictments, Incapacitations and Court Orders</asp:label></td>
														</tr>
														<tr>
															<td colspan="4"></td>
														</tr>
														<tr>
															<td><asp:label id="lblNationalID" runat="server">ID:</asp:label></td>
															<td><asp:label id="lblName" runat="server">Name</asp:label></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td valign="top"><asp:textbox id="txtNationalID" runat="server" width="96px"></asp:textbox></td>
															<td valign="top"><asp:textbox id="txtName" runat="server" width="190px"></asp:textbox></td>
															<td valign="top">
																<table width="200" cellspacing="0" cellpadding="0">
																	<tr>
																		<td>
																			<div class="AroundButtonSmallMargin" style="WIDTH: 90px; HEIGHT: 10px"><asp:button id="btnFind" runat="server" cssclass="RegisterButton" text="Find"></asp:button></div>
																		</td>
																		<td>
																			<div class="AroundButtonSmallMargin" style="WIDTH: 90px; HEIGHT: 10px"><asp:button id="btnClear" runat="server" cssclass="RegisterButton" text="Clear"></asp:button></div>
																		</td>
																	</tr>
																</table>
															</td>
															<td></td>
														</tr>
														<tr>
															<td valign="top" height="15"></td>
															<td valign="top" height="15"></td>
															<td valign="top" height="15"></td>
															<td height="15"></td>
														</tr>
													</table>
													<table id="Table1" cellspacing="0" cellpadding="0" width="97%" border="0" align="center">
														<tr id="trReportDisplay" runat="server" enableviewstate="false">
															<td id="tdReportDisplay" runat="server" colspan="5"></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td style="HEIGHT: 15px" align="left"><asp:label id="lblMessage" runat="server" font-bold="True" forecolor="Black"></asp:label></td>
														</tr>
														<tr>
															<td style="HEIGHT: 15px" align="center" bgcolor="#951e16"></td>
														</tr>
													</table>
													&nbsp;
												</td>
											</tr>
											<!-- Content ends --></table>
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor="transparent" height="6"></td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
