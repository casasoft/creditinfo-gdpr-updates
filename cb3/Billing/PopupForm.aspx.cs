#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace Billing {
    /// <summary>
    /// Summary description for PopupForm.
    /// </summary>
    public class PopupForm : Page {
        protected Label lblHeader;
        protected PlaceHolder placeHolder;

        private void Page_Load(object sender, EventArgs e) {
            Control uc;
            if (true) {
                uc = LoadControl("user_controls/NewCIUserCtrl.ascx");
                uc.ID = "NewCIUserCtrl";
            }

            if (uc != null) {
                placeHolder.Controls.Add(uc);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}