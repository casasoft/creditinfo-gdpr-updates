#region

using System;
using System.Reflection;
using System.Resources;
using System.Web;

#endregion

namespace Billing.Localization {
    /// <summary>
    /// CIResource class is used for localization. It has a Resource manager property that is used
    /// to manage system resource files.
    /// </summary>
    /// <remarks>Class for multilanguage systems only.</remarks>
    public class CIResource {
        /// <summary>
        /// Set application Name.
        /// </summary>
        private const string ApplicationVariableName = "Billing";

        /// <summary>
        /// The CurrentManager property returns and caches the shared ResourceManager instance.
        /// <seealso cref="System.Resources.ResourceManager"/>
        /// <seealso cref="System.Web.HttpContext"/>
        /// </summary>
        /// <remarks>ResourceManager instance is shared over session.</remarks>
        /// <exception cref="System.ArgumentException">Thrown when no current http context is found.</exception>
        /// <value>ResourceManager</value>
        public static ResourceManager CurrentManager {
            get {
                var context = HttpContext.Current;
                if (null == context) {
                    throw new ArgumentException("Global_NoHttpContext");
                }
                var mgr = context.Cache[ApplicationVariableName] as ResourceManager;
                if (null == mgr) {
                    mgr = new ResourceManager(
                        "cb3.Billing.resources.strings", Assembly.GetExecutingAssembly(), null);

                    // Add to the cache
                    context.Cache.Insert(ApplicationVariableName, mgr);
                }
                return mgr;
            }
        }
    }
}