<%@ Control Language="c#" AutoEventWireup="false" Codebehind="AgreementCtrl.ascx.cs" Inherits="Billing.user_controls.Business_Ctrls.AgreementCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" src="CheckEnterKey.js"></script>
<script language="JavaScript" src="DatePicker.js"></script>
<script language="javascript"> 
	function checkEnterKey(path) 
	{    
		if (event.keyCode == 13) 
		{        
			event.cancelBubble = true;
			event.returnValue = false;
			document.BillingMain.AgreementCtrl1_btnFinish.click(); 
		}
	} 
	
	function SetFormFocus()
	{
		if (document.BillingMain.AgreementCtrl1_txtAgreementID.disabled == false)
		{
			document.BillingMain.AgreementCtrl1_txtAgreementID.focus();
		}	
	}	
</script>
<body onload="SetFormFocus()">
	<form id="Agreement" method="post">
		<table width="100%">
			<tr>
				<td>
					<table class="grid_table" cellspacing="0" cellpadding="0">
						<tr>
							<th>
								<asp:label id="lblPageHeader" runat="server"></asp:label></th></tr>
						<tr>
							<td>
								<table class="fields" id="tblMainRegion" cellspacing="0" cellpadding="0">
									<tr>
										<td colspan="4"><asp:label id="lblSubscriber" runat="server"></asp:label><br>
											<asp:label id="lblSubscriberName" runat="server" font-bold="True"></asp:label></td>
									</tr>
									<tr>
										<td style="WIDTH: 24%"><asp:label id="lblCurrency" runat="server"></asp:label><br>
											<asp:dropdownlist id="ddlCurrencyList" runat="server"></asp:dropdownlist></td>
										<td style="WIDTH: 24%"><asp:label id="lblAgreementID" runat="server"></asp:label><br>
											<asp:textbox id="txtAgreementID" runat="server" maxlength="50"></asp:textbox>&nbsp;<asp:label id="Label1" cssclass="error_text" runat="server">*</asp:label><asp:requiredfieldvalidator id="requiredAgreementID" runat="server" errormessage="requiredAgreementID" controltovalidate="txtAgreementID"
												display="None">*</asp:requiredfieldvalidator></td>
										<td style="WIDTH: 27%"><asp:label id="lblBeginDate" runat="server"></asp:label><br>
											<asp:textbox id="txtBeginDate" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('AgreementCtrl1_txtBeginDate', 250, 250);" type="button"
												value="...">
											<asp:label id="Label2" cssclass="error_text" runat="server">*</asp:label><asp:requiredfieldvalidator id="requiredBeginDate" runat="server" errormessage="requiredBeginDate" controltovalidate="txtBeginDate"
												display="None">*</asp:requiredfieldvalidator><asp:comparevalidator id="CompareBeginDate" runat="server" errormessage="CompareBeginDate" controltovalidate="txtBeginDate"
												display="None" operator="DataTypeCheck" type="Date">*</asp:comparevalidator></td>
										<td><asp:label id="lblEndDate" runat="server"></asp:label><br>
											<asp:textbox id="txtEndDate" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('AgreementCtrl1_txtEndDate', 250, 250);" type="button"
												value="...">
											<asp:label id="Label3" cssclass="error_text" runat="server">*</asp:label><asp:requiredfieldvalidator id="requiredEndDate" runat="server" errormessage="requiredEndDate" controltovalidate="txtEndDate"
												display="None">*</asp:requiredfieldvalidator><asp:comparevalidator id="CompareEndDate" runat="server" errormessage="CompareEndDate" controltovalidate="txtEndDate"
												display="None" operator="DataTypeCheck" type="Date">*</asp:comparevalidator></td>
									</tr>
									<tr>
										<td><asp:label id="lblAgreementPrice" runat="server"></asp:label><br>
											<asp:textbox id="txtAgreementPrice" cssclass="rightAlign" runat="server" maxlength="100">0</asp:textbox>&nbsp;<asp:label id="Label6" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="ComparePrice" runat="server" errormessage="ComparePrice" controltovalidate="txtAgreementPrice"
												display="None" operator="DataTypeCheck" type="Double">*</asp:comparevalidator></td>
										<td><asp:label id="lblAgreementDiscount" runat="server"></asp:label><br>
											<asp:textbox id="txtAgreementDiscount" cssclass="rightAlign" runat="server" maxlength="100">0</asp:textbox>&nbsp;<asp:label id="Label4" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="CompareDiscount" runat="server" errormessage="CompareDiscount" controltovalidate="txtAgreementDiscount"
												display="None" operator="DataTypeCheck" type="Double">*</asp:comparevalidator></td>
										<td><asp:label id="lblSignerName" runat="server"></asp:label><br>
											<asp:dropdownlist id="ddlSignedBy" runat="server"></asp:dropdownlist></td>
										<td><asp:label id="lblReminderOffset" runat="server"></asp:label><br>
											<asp:textbox id="txtReminderOffset" runat="server"></asp:textbox>&nbsp;<asp:label id="Label5" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="CompareOffset" runat="server" errormessage="CompareOffset" controltovalidate="txtReminderOffset"
												display="None" operator="DataTypeCheck" type="Integer">*</asp:comparevalidator></td>
									</tr>
									<tr>
										<td colspan="2"><asp:label id="lblNotificationEmails" runat="server"></asp:label><br>
											<asp:textbox id="txtNotificationEmails" runat="server" maxlength="128" width="91%"></asp:textbox>&nbsp;<asp:regularexpressionvalidator id="NotificationEmailRegularExpression" runat="server" errormessage="NotificationEmailRegularExpression"
												controltovalidate="txtNotificationEmails" display="None" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\;([ ])*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*">*</asp:regularexpressionvalidator></td>
									</tr>
									<tr>
										<td valign="top" colspan="4"><asp:label id="lblGeneralDescription" runat="server"></asp:label><br>
											<asp:textbox id="txtGeneralDescription" runat="server" maxlength="1000" width="100%" textmode="MultiLine"
												height="72px"></asp:textbox></td>
									</tr>
									<tr>
										<td height="23"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						<!-- ITEMS -->
						<tr>
							<td>
								<table class="grid_table" cellspacing="0" cellpadding="0">
									<tr>
										<th style="HEIGHT: 18px">
											<asp:label id="lblItemFieldsHeader" runat="server"></asp:label></th></tr>
									<tr>
										<td>
											<table class="fields" id="tblItemRegion" cellspacing="0" cellpadding="0">
												<tr>
													<td colspan="2"><asp:label id="lblItem" runat="server"></asp:label><br>
														<asp:dropdownlist id="ddlItemList" runat="server" autopostback="True"></asp:dropdownlist></td>
													<td><asp:label id="lblItemBeginDate" runat="server"></asp:label><br>
														<asp:textbox id="txtItemBeginDate" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('AgreementCtrl1_txtItemBeginDate', 250, 250);"
															type="button" value="...">
														<asp:label id="Label11" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="cpvItemBeginDate" runat="server" errormessage="CompareBeginDate" controltovalidate="txtItemBeginDate"
															display="None" operator="DataTypeCheck" type="Date" visible="False">*</asp:comparevalidator><asp:requiredfieldvalidator id="rfvItemBeginDate" runat="server" errormessage="requiredBeginDate" controltovalidate="txtItemBeginDate"
															display="None" visible="False">*</asp:requiredfieldvalidator></td>
													<td><asp:label id="lblItemEndDate" runat="server"></asp:label><br>
														<asp:textbox id="txtItemEndDate" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('AgreementCtrl1_txtItemEndDate', 250, 250);"
															type="button" value="...">
														<asp:label id="Label13" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="cpvItemEndDate" runat="server" errormessage="CompareEndDate" controltovalidate="txtItemEndDate"
															display="None" operator="DataTypeCheck" type="Date" visible="False">*</asp:comparevalidator><asp:requiredfieldvalidator id="rfvItemEndDate" runat="server" errormessage="requiredEndDate" controltovalidate="txtItemEndDate"
															display="None" visible="False">*</asp:requiredfieldvalidator></td>
												</tr>
												<tr>
													<td style="WIDTH: 24%" width="175"><asp:label id="lblFixedAmount" runat="server"></asp:label><br>
														<asp:textbox id="txtFixedAmount" cssclass="rightAlign" runat="server">0</asp:textbox>&nbsp;<asp:comparevalidator id="cpvFixedAmount" runat="server" errormessage="CompareFixedPrice" controltovalidate="txtFixedAmount"
															operator="DataTypeCheck" type="Double" visible="False">*</asp:comparevalidator></td>
													<td style="WIDTH: 24%"><asp:label id="lblFixedDiscount" runat="server"></asp:label><br>
														<asp:textbox id="txtFixedDiscount" cssclass="rightAlign" runat="server">0</asp:textbox>&nbsp;<asp:comparevalidator id="cpvFixedDiscount" runat="server" errormessage="CompareFiexedDiscount" controltovalidate="txtFixedDiscount"
															operator="DataTypeCheck" type="Double" visible="False">*</asp:comparevalidator></td>
												</tr>
												<tr>
													<td><asp:label id="lblMonthlyAmount" runat="server"></asp:label><br>
														<asp:label id="lblTheMonthlyAmount" runat="server" font-bold="True"></asp:label></td>
													<td><asp:label id="lblMonthlyDiscount" runat="server"></asp:label><br>
														<asp:textbox id="txtMonthlyDiscount" cssclass="rightAlign" runat="server">0</asp:textbox>&nbsp;<asp:comparevalidator id="cpvMonthlyDiscount" runat="server" errormessage="CompareMonthlyDiscount" controltovalidate="txtMonthlyDiscount"
															operator="DataTypeCheck" type="Double" visible="False">*</asp:comparevalidator></td>
												</tr>
												<tr>
													<td><asp:label id="lblUsageAmount" runat="server"></asp:label><br>
														<asp:label id="lblTheUsageAmount" runat="server" font-bold="True"></asp:label></td>
													<td><asp:label id="lblUsageDiscount" runat="server"></asp:label><br>
														<asp:textbox id="txtUsageDiscount" cssclass="rightAlign" runat="server">0</asp:textbox>&nbsp;<asp:comparevalidator id="cpvUsageDiscount" runat="server" errormessage="CompareUsageDiscount" controltovalidate="txtUsageDiscount"
															operator="DataTypeCheck" type="Double" visible="False">*</asp:comparevalidator></td>
												</tr>
												<tr>
													<td><asp:label id="lblCounterLink" runat="server"></asp:label><br>
														<asp:dropdownlist id="ddlCounterLink" cssclass="rammi" runat="server"></asp:dropdownlist></td>
													<td><asp:label id="lblFreezone" runat="server"></asp:label><br>
														<asp:textbox id="txtFreezone" cssclass="rightAlign" runat="server">0</asp:textbox></td>
													<td style="WIDTH: 25%"><asp:label id="lblItemReminderOffset" runat="server"></asp:label><br>
														<asp:textbox id="txtItemReminderOffset" cssclass="rightAlign" runat="server">0</asp:textbox>&nbsp;<asp:comparevalidator id="cpvItemReminderOffset" runat="server" errormessage="CompareOffset" controltovalidate="txtReminderOffset"
															display="None" operator="DataTypeCheck" type="Integer" visible="False">*</asp:comparevalidator></td>
													<td><asp:label id="lblIsVisible" runat="server"></asp:label><br>
														<asp:checkbox id="chkVisible" cssclass="radio" runat="server" checked="True"></asp:checkbox></td>
												</tr>
												<tr>
													<td style="HEIGHT: 42px" colspan="2"><asp:label id="lblItemNotificationEmails" runat="server"></asp:label><br>
														<asp:textbox id="txtItemNotificationEmails" runat="server" maxlength="128" width="91%"></asp:textbox>&nbsp;<asp:regularexpressionvalidator id="revItemNotifyEmails" runat="server" errormessage="NotificationEmailRegularExpression"
															controltovalidate="txtItemNotificationEmails" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\;([ ])*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*" visible="False">*</asp:regularexpressionvalidator></td>
													<td style="HEIGHT: 42px"><asp:label id="lblIndexLink" runat="server"></asp:label><br>
														<asp:dropdownlist id="ddlIndexLink" runat="server"></asp:dropdownlist></td>
												</tr>
												<tr>
													<td colspan="4" style="HEIGHT: 63px">
														<asp:Label id="lblDepartments" runat="server"></asp:Label><br>
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr>
																<td style="WIDTH: 45%">
																	<asp:label id="lblDepartmentsAvailable" runat="server"></asp:label><br>
																	<asp:listbox id="lbxDepartmentsAvailable" runat="server" width="100%" rows="7" selectionmode="Multiple"></asp:listbox>
																</td>
																<td valign=middle align=center>
																	<table>
																		<tr>
																			<td>
																				<asp:button id="btnDepartmentMoveAll" runat="server" cssclass="popup" width="25px" text=">>"></asp:button>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<asp:button id="btnDepartmentMoveOne" runat="server" cssclass="popup" width="25px" text=">"></asp:button>
																			</td>
																		</tr>
																		<tr>
																			<td style="HEIGHT: 25px">
																				<asp:button id="btnDepartmentRemoveOne" runat="server" cssclass="popup" width="25px" text="<"></asp:button>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<asp:button id="btnDepartmentRemoveAll" runat="server" cssclass="popup" width="25px" text="<<"></asp:button>
																			</td>
																		</tr>																																																						
																	</table>
																</td>
																<td style="WIDTH: 45%">
																	<asp:label id="lblDepartmentsSelected" runat="server"></asp:label><br>
																	<asp:ListBox id="lbxDepartmentsSelected" runat="server" Width="100%" rows="7" selectionmode="Multiple"></asp:ListBox>
																</td>
															</tr>
														</table>														
													</td>
												</tr>
												<tr>
													<td style="WIDTH: 175px" height="23"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table class="grid_table" id="tblIndexesGrid" cellspacing="0" cellpadding="0" runat="server">
									<tr>
										<td height="10"></td>
									</tr>
									<tr>
										<td align="right">
											<asp:label id="lblIndexGridIcons" runat="server">Label</asp:label>&nbsp;
										</td>
									</tr>
									<tr>
										<td height="5"></td>
									</tr>
									<tr>
										<th>
											<asp:label id="lblIndexesGridHeader" runat="server">[Header]</asp:label></th></tr>
									<tr>
										<td>
											<table class="datagrid" cellspacing="0" cellpadding="0">
												<tr>
													<td><asp:datagrid id="dgIndexes" runat="server" width="100%" gridlines="None" autogeneratecolumns="False">
															<footerstyle cssclass="grid_footer"></footerstyle>
															<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
															<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
															<itemstyle cssclass="grid_item"></itemstyle>
															<headerstyle cssclass="grid_header"></headerstyle>
															<columns>
																<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;" commandname="Delete">
																	<itemstyle cssclass="leftpadding"></itemstyle>
																</asp:buttoncolumn>
																<asp:boundcolumn datafield="Name">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="DescNative">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="DescEN">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
															</columns>
														</asp:datagrid></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table class="grid_table" id="tblItemsGrid" cellspacing="0" cellpadding="0" runat="server">
									<tr>
										<td height="10"></td>
									</tr>
									<tr>
										<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
									</tr>
									<tr>
										<td height="5"></td>
									</tr>
									<tr>
										<th>
											<asp:label id="lblAgreementItemsHeader" runat="server"></asp:label></th></tr>
									<tr>
										<td>
											<table class="datagrid" cellspacing="0" cellpadding="0">
												<tr>
													<td><asp:datagrid id="dgItems" runat="server" width="100%" height="100%" gridlines="None" autogeneratecolumns="False"
															allowsorting="True" bordercolor="Gray" borderstyle="None" borderwidth="1px" backcolor="White"
															cellpadding="4">
															<footerstyle cssclass="grid_footer"></footerstyle>
															<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
															<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
															<itemstyle cssclass="grid_item"></itemstyle>
															<headerstyle cssclass="grid_header"></headerstyle>
															<columns>
																<asp:buttoncolumn text="&lt;img src=&quot;../img/view.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;" commandname="Select">
																	<itemstyle cssclass="leftpadding"></itemstyle>
																</asp:buttoncolumn>
																<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;" commandname="Delete">
																	<itemstyle cssclass="nopadding"></itemstyle>
																</asp:buttoncolumn>
																<asp:boundcolumn visible="False" datafield="ID" headertext="ID">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="ItemNameNative" sortexpression="ItemNameNative" headertext="ItemNameNative">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="ItemNameEN" sortexpression="ItemNameEN" headertext="ItemNameEN">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="FixedAmount" sortexpression="FixedAmount" headertext="Fixed">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="FixedDiscountPercent" sortexpression="FixedDiscountPercent" headertext="Fixed%">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="MonthlyAmount" sortexpression="MonthlyAmount" headertext="Monthly">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="MonthlyDiscountPercent" sortexpression="MonthlyDiscountPercent" headertext="Monthly%">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="UsageAmount" sortexpression="UsageAmount" headertext="Usage">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="UsageDiscountPercent" sortexpression="UsageDiscountPercent" headertext="Usage%">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:templatecolumn sortexpression="IsVisible" headertext="Visible">
																	<itemtemplate>
																		<asp:checkbox id="IsVisible" checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem,"IsVisible"))%>' runat="server" enabled="False" cssclass="radio">
																		</asp:checkbox>
																	</itemtemplate>
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:templatecolumn>
																<asp:boundcolumn visible="False" datafield="Tag">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
															</columns>
															<pagerstyle cssclass="grid_pager"></pagerstyle>
														</asp:datagrid></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td>
								<table class="empty_table" cellspacing="0">
									<tr>
										<td align="left"></td>
										<td align="right"><asp:button id="btnAddIndex" cssclass="confirm_button" runat="server" causesvalidation="False"></asp:button><asp:button id="btnInsertItem" cssclass="confirm_button" runat="server" text="Insert"></asp:button></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						<!-- COMMENTS -->
						<tr>
							<td>
								<table class="grid_table" cellspacing="0" cellpadding="0">
									<tr>
										<th>
											<asp:label id="lblCommentFieldsHeader" runat="server"></asp:label></th></tr>
									<tr>
										<td>
											<table class="fields" id="tblCommentRegion" cellspacing="0" cellpadding="0">
												<tr>
													<td width="50%"><asp:label id="lblCommentNotificationEmail" runat="server"></asp:label><br>
														<asp:textbox id="txtCommentNotificationEmails" runat="server" maxlength="128" width="91%"></asp:textbox>&nbsp;<asp:label id="Label27" cssclass="error_text" runat="server">*</asp:label>&nbsp;<asp:regularexpressionvalidator id="revCommentNotifyEmails" runat="server" errormessage="NotificationEmailRegularExpression"
															controltovalidate="txtCommentNotificationEmails" display="None" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\;([ ])*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*" visible="False">*</asp:regularexpressionvalidator><asp:requiredfieldvalidator id="rfvCommentEmails" runat="server" errormessage="RequiredEmail" controltovalidate="txtCommentNotificationEmails"
															display="None" visible="False">*</asp:requiredfieldvalidator></td>													
													<td><asp:label id="lblNotificationDate" runat="server"></asp:label><br>
														<asp:textbox id="txtNotificationDate" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('AgreementCtrl1_txtNotificationDate', 250, 250);"
															type="button" value="...">
														<asp:label id="Label26" cssclass="error_text" runat="server">*</asp:label><asp:requiredfieldvalidator id="rfvCommentNotifyDate" runat="server" errormessage="RequiredNotificationDate"
															controltovalidate="txtNotificationDate" display="None" visible="False">*</asp:requiredfieldvalidator></td>													
												</tr>
												<tr>
													<td colspan="2"><asp:label id="lblComment" runat="server"></asp:label>&nbsp;<asp:label id="Label28" cssclass="error_text" runat="server">*</asp:label><asp:requiredfieldvalidator id="rfvComment" runat="server" errormessage="RequiredText" controltovalidate="txtComment"
															display="None" visible="False">*</asp:requiredfieldvalidator><br>
														<asp:textbox id="txtComment" runat="server" maxlength="1024" width="100%" textmode="MultiLine"
															height="72px"></asp:textbox></td>
												</tr>
												<tr>
													<td height="23"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table class="grid_table" id="tblCommentsGrid" cellspacing="0" cellpadding="0" runat="server">
									<tr>
										<td height="10"></td>
									</tr>
									<tr>
										<td align="right"><asp:label id="lblCommentsGridHeader" runat="server"></asp:label></td>
									</tr>
									<tr>
										<td height="5"></td>
									</tr>
									<tr>
										<th>
											<asp:label id="lblCommentsHeader" runat="server"></asp:label></th></tr>
									<tr>
										<td>
											<table class="datagrid" cellspacing="0" cellpadding="0">
												<tr>
													<td><asp:datagrid id="dgComments" runat="server" width="100%" gridlines="None" autogeneratecolumns="False"
															cellpadding="4">
															<footerstyle cssclass="grid_footer"></footerstyle>
															<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
															<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
															<itemstyle cssclass="grid_item"></itemstyle>
															<headerstyle cssclass="grid_header"></headerstyle>
															<columns>
																<asp:buttoncolumn text="&lt;img src=&quot;../img/view.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;" commandname="Select">
																	<itemstyle cssclass="leftpadding"></itemstyle>
																</asp:buttoncolumn>
																<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;" commandname="Delete">
																	<itemstyle cssclass="nopadding"></itemstyle>
																</asp:buttoncolumn>
																<asp:boundcolumn visible="False" datafield="ID" headertext="ID">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="Comment" sortexpression="Comment" headertext="Comment">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="ReminderDate" sortexpression="ReminderDate" headertext="ReminderDate" dataformatstring="{0:d}">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="NotificationEmails" sortexpression="NotificationEmail" headertext="NotificationEmail">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn visible="False" datafield="Tag">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
															</columns>
															<pagerstyle cssclass="grid_pager"></pagerstyle>
														</asp:datagrid></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td>
								<table class="empty_table" cellspacing="0">
									<tr>
										<td align="left"></td>
										<td align="right"><asp:button id="btnInsertComment" cssclass="confirm_button" runat="server" text="Insert"></asp:button></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td>
								<table class="empty_table" cellspacing="0">
									<tr valign="top">
										<td align="left" rowspan="2"><asp:validationsummary id="ValidationSummary" runat="server" width="100%" displaymode="List"></asp:validationsummary><asp:label id="lblMessage" runat="server" visible="False"></asp:label></td>
										<td align="right"><asp:button id="btnCancel" cssclass="cancel_button" runat="server" causesvalidation="False"></asp:button><asp:button id="btnSave" cssclass="confirm_button" runat="server"></asp:button></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
