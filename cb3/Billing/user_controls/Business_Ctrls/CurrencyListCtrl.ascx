<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CurrencyListCtrl.ascx.cs" Inherits="Billing.user_controls.Business_Ctrls.CurrencyListCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="javascript"> 
	function checkEnterKey(path) 
	{    
		if (event.keyCode == 13) 
		{        
			event.cancelBubble = true;
			event.returnValue = false;
			document.BillingMain.CurrencyListCtrl1_btnInsert.click(); 
		}
	} 
	
	function SetFormFocus()
	{
		document.BillingMain.CurrencyListCtrl1_txtCurrencyCode.focus();
	}
</script>
<body onload="SetFormFocus()">
	<form id="Currency" method="post">
		<table width="100%">
			<tr>
				<td>
					<table class="grid_table" cellspacing="0" cellpadding="0">
						<tr>
							<th>
								<asp:label id="lblPageHeader" runat="server" ></asp:label>
							</th>
						</tr>
						<tr>
							<td>
								<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
									<tr>
										<td style="WIDTH: 33%">
											<asp:label id="lblCurrencyCode" runat="server"></asp:label><br>
											<asp:textbox id="txtCurrencyCode" runat="server" maxlength="3"></asp:textbox></td>
										<td style="WIDTH: 33%">
											<asp:label id="lblCurrencyDescriptionNative" runat="server"></asp:label><br>
											<asp:textbox id="txtCurrencyDescNative" runat="server" maxlength="50"></asp:textbox></td>
										<td>
											<asp:label id="lblCurrencyDescriptionEN" runat="server"></asp:label>
											<br>
											<asp:textbox id="txtCurrencyDescEN" runat="server" maxlength="50"></asp:textbox>
										</td>
									</tr>
									<tr>
										<td height="23"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td>
					<table class="empty_table" cellspacing="0">
						<tr>
							<td align="left">
								<asp:label id="lblMessage" runat="server" width="100%" visible="False"></asp:label>
							</td>
							<td align="right">
								<asp:button id="btnInsert" runat="server" cssclass="confirm_button"></asp:button>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td align="right">
								<asp:label id="lblDatagridIcons" runat="server"></asp:label>
							</td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblCurrencyListHeader" runat="server"></asp:label>
							</th>
						</tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<asp:datagrid id="dgCurrencyList" runat="server" width="100%" cellpadding="4" backcolor="White"
												borderwidth="1px" borderstyle="None" bordercolor="Gray" autogeneratecolumns="False" gridlines="None">
												<footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
												<columns>
													<asp:editcommandcolumn buttontype="LinkButton" updatetext="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;" 
 canceltext="&lt;img src=&quot;../img/cancel.gif&quot; alt=&quot;Cancel&quot; border=&quot;0&quot;&gt;" 
 edittext="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;">
														<itemstyle cssclass="leftpadding"></itemstyle>
													</asp:editcommandcolumn>
													<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;" 
 commandname="Delete">
														<itemstyle cssclass="nopadding"></itemstyle>
													</asp:buttoncolumn>
													<asp:boundcolumn datafield="CurrencyCode" readonly="True" headertext="CurrencyCode">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="CurrencyDescriptionEN" headertext="CurrencyDescriptionEN">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="CurrencyDescriptionNative" headertext="CurrencyDescriptionNative">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
												</columns>
											</asp:datagrid>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
