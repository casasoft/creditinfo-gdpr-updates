#region

using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;
using Billing.user_controls.Business_Ctrls;
using Logging.BLL;

#endregion

namespace Billing.user_controls {
    /// <summary>
    ///		Summary description for AgreementCommentsCtrl.
    /// </summary>
    public class AgreementCommentsCtrl : AgreementGroundCtrl {
        protected Button btnCancel;
        protected Button btnInsertComment;
        protected Button btnSave;
        protected DataGrid dgItems;
        protected Label Label1;
        protected Label Label2;
        protected Label Label4;
        protected Label lblComment;
        protected Label lblCommentsHeader;
        protected Label lblDatagridIcons;
        protected Label lblMessage;
        protected Label lblNotificationDate;
        protected Label lblNotificationEmail;
        protected Label lblPageHeader;
        protected Label lblSubscriber;
        protected Label lblSubscriberName;
        protected Button lnkAgreement;
        protected Button lnkItems;
        protected RegularExpressionValidator NotificationEmailRegularExpression;
        protected HtmlTable outerGridTable;
        protected RequiredFieldValidator RequiredEmail;
        protected RequiredFieldValidator RequiredNotificationDate;
        protected RequiredFieldValidator RequiredText;
        protected TextBox txtComment;
        protected TextBox txtNotificationDate;
        protected TextBox txtNotificationEmail;
        protected ValidationSummary ValidationSummary;

        private void Page_Load(object sender, EventArgs e) {
            PageLoad(sender, e);

            // Initialize the agreement data object so it is ready for use. Either set it as a "new" object
            // or fetch values from the session object. This function is handled by the parent class
            // AgreementGroundCtrl
            InitializeAgreement();

            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();
            LocalizeText();

            if (!IsPostBack) {
                SetCommentsDataGrid();

                // Lock or unlock controls to conform to user selection for viewable or updateable agreements
                // Uses the LockControls function of the AgreementGroundControl (inherited)
                LockControls(AgreementUnlock, this);
                if (!AgreementUnlock) {
                    btnSave.Visible = false;
                    btnInsertComment.Visible = false;
                } else {
                    btnSave.Visible = true;
                    btnInsertComment.Visible = true;
                }
            }
        }

        /// <summary>
        /// Links an "enter event" to input controls
        /// </summary>
        /// <remarks>
        /// Links an "enter event" to all input controls so as to allow the user to press the
        /// enter key to submit a new item.
        /// </remarks>
        private void AddEnterEvent() {
            txtNotificationDate.Attributes.Add("onkeypress", "checkEnterKey();");
            txtNotificationEmail.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        /// <summary>
        /// Populate the comments datagrid with comments that are linked to the current agreement
        /// </summary>
        private void SetCommentsDataGrid() {
            // TODO: Fix this code please - This db call should not be necessary and is only made to
            // get "production ready" code erlier...
            DataTable mySubscriberTable = BillingFactory.GetSingleSubscribersAsDataTable(Agreement.SubscriberID);
            lblSubscriberName.Text = mySubscriberTable.Rows[0]["NameNative"].ToString();
            mySubscriberTable.Dispose();

            // Use AgreementGroundCtrl
            ArrayList arrComments = ReturnAgreementCommentsArray();
            dgItems.DataSource = arrComments;
            dgItems.DataBind();
            dgItems.SelectedIndex = -1;

            if (arrComments == null || arrComments.Count < 1) {
                outerGridTable.Visible = false;
            } else {
                outerGridTable.Visible = true;
            }
        }

        /// <summary>
        /// For text localization. Text is set by using resource files that are XML based.
        /// </summary>
        protected override void LocalizeText() {
            base.LocalizeText();

            // Labels...
            lblComment.Text = rm.GetString("AgreementCommentsCtrl.lblComment", ci);
            lblCommentsHeader.Text = rm.GetString("AgreementCommentsCtrl.lblCommentsHeader", ci);
            lblNotificationDate.Text = rm.GetString("AgreementCommentsCtrl.lblNotificationDate", ci);
            lblNotificationEmail.Text = rm.GetString("lblNotificationEmail", ci);
            lblPageHeader.Text = rm.GetString("AgreementCommentsCtrl.lblPageHeader", ci);
            lblSubscriber.Text = rm.GetString("AgreementCtrl.lblSubscriber", ci);

            // Links
            lnkItems.Text = rm.GetString("Agreement.lnkItems", ci);
            lnkAgreement.Text = rm.GetString("Agreement.lnkAgreement", ci);

            // Buttons
            btnCancel.Text = rm.GetString("Agreement.btnCancel", ci);
            btnSave.Text = rm.GetString("Agreement.btnSave", ci);
            btnInsertComment.Text = rm.GetString("AgreementCommentsCtrl.btnInsertComment", ci);

            // Validators
            RequiredEmail.ErrorMessage = rm.GetString("AgreementCommentsCtrl.RequiredEmail", ci);
            RequiredNotificationDate.ErrorMessage = rm.GetString("AgreementCommentsCtrl.RequiredNotificationDate", ci);
            RequiredText.ErrorMessage = rm.GetString("AgreementCommentsCtrl.RequiredText", ci);
            NotificationEmailRegularExpression.ErrorMessage =
                rm.GetString("AgreementCommentsCtrl.NotificationEmailRegularExpression", ci);

            // Datagrid columns
            dgItems.Columns[3].HeaderText = rm.GetString("AgreementCommentsCtrl.CommentCol", ci);
            dgItems.Columns[4].HeaderText = rm.GetString("AgreementCommentsCtrl.ReminderDateCol", ci);
            dgItems.Columns[5].HeaderText = rm.GetString("AgreementCommentsCtrl.EmailCol", ci);
        }

        /// <summary>
        /// Insert a comment to the comments array for the current agreement
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnInsertComment_Click(object sender, EventArgs e) {
            // Check to see if any comment is already in "update mode". This can be seen by checking the appropriate
            // Session variable...
            if (Session["agreementComment"] != null) {
                var theComment = (AgreementCommentBLLC) Session["agreementComment"];

                // Set new comment values...
                theComment.Comment = txtComment.Text;
                theComment.ReminderDate = Convert.ToDateTime(txtNotificationDate.Text);
                theComment.NotificationEmails = txtNotificationEmail.Text;
//				theComment.NotificationEmail	= txtNotificationEmail.Text;

                // Function call to update or insert the new comment into the Comments array 
                // for the agreement in memory...
                UpdateAgreementCommentsArray(theComment);
                Session["agreementComment"] = null;
            } else {
                // Function call to get the correct agreement ID
                AgreementCommentBLLC newComment = NewAgreementComment();

                // Set comment values...
                newComment.Comment = txtComment.Text;
                newComment.ReminderDate = Convert.ToDateTime(txtNotificationDate.Text);
                newComment.NotificationEmails = txtNotificationEmail.Text;
//				newComment.NotificationEmail	= txtNotificationEmail.Text;

                if (newComment.Inserted == DateTime.MinValue) {
                    newComment.Inserted = DateTime.Now;
                }

                if (newComment.InsertedBy == -1) {
                    newComment.InsertedBy = Convert.ToInt32(Session["UserLoginID"]);
                }

                // Function call to insert the new comment into the Comments array for the agreement in memory...
                UpdateAgreementCommentsArray(newComment);
            }

            // Function call to update the comments datagrid after the comment is added to the agreement in memory...
            SetCommentsDataGrid();
        }

        /// <summary>
        /// Remove a comment from the comments array of the current agreement.
        /// </summary>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgItems_DeleteCommand(object source, DataGridCommandEventArgs e) {
            RemoveFromCommentsList(Convert.ToInt32(e.Item.Cells[6].Text));
            SetCommentsDataGrid();
        }

        /// <summary>
        /// A comment is selected from the grid for updating on the current agreement
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void dgItems_SelectedIndexChanged(object sender, EventArgs e) {
            AgreementCommentBLLC theComment = GetAgreementComment(Convert.ToInt32(dgItems.SelectedItem.Cells[6].Text));

            txtComment.Text = theComment.Comment;
            txtNotificationEmail.Text = theComment.NotificationEmails;
//			txtNotificationEmail.Text		= theComment.NotificationEmail;
            txtNotificationDate.Text = theComment.ReminderDate.ToShortDateString();

            Session["agreementComment"] = theComment;
        }

        /// <summary>
        /// Go to agreement items window
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void lnkItems_Click(object sender, EventArgs e) { Response.Redirect(@"TableUpdates.aspx?Type=AgreementItems"); }

        /// <summary>
        /// Go to agreement overview window
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void lnkAgreement_Click(object sender, EventArgs e) { Response.Redirect(@"TableUpdates.aspx?Type=Agreement&New=0"); }

        /// <summary>
        /// Save the agreement to database
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnSave_Click(object sender, EventArgs e) {
            if (!MoveAgreementToDatabase()) {
                lblMessage.Text = rm.GetString("global.inserterrormsg", ci);
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
                return;
            }
        }

        /// <summary>
        /// Cancel agreement operation and return to the FindCtrl.ascx user control.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnCancel_Click(object sender, EventArgs e) {
            // Clear "in memory" agreement...
            ReleaseInMemoryAgreement();
            // Transfer the user...
            Server.Transfer(@"TableUpdates.aspx?Type=def");
        }

        private void dgItems_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
            }

            WebDesign.CreateExplanationIcons(dgItems.Columns, lblDatagridIcons, rm, ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dgItems.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgItems_DeleteCommand);
            this.dgItems.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgItems_ItemDataBound);
            this.dgItems.SelectedIndexChanged += new System.EventHandler(this.dgItems_SelectedIndexChanged);
            this.btnInsertComment.Click += new System.EventHandler(this.btnInsertComment_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.lnkItems.Click += new System.EventHandler(this.lnkItems_Click);
            this.lnkAgreement.Click += new System.EventHandler(this.lnkAgreement_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}