<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ReportCtrl.ascx.cs" Inherits="Billing.user_controls.Business_Ctrls.ReportCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" src="DatePicker.js"></script>
<body>
	<form id="Search" method="post">
		<table width="100%">
			<tr>
				<td>
					<table class="grid_table" cellspacing="0" cellpadding="0">
						<tr>
							<th style="HEIGHT: 18px">
								<asp:label id="lblPageHeader" runat="server" >Billing report</asp:label>
							</th>
						</tr>
						<tr>
							<td>
								<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<asp:label id="lblSubscriberID" runat="server">Subscriber ID</asp:label><br>
											<asp:textbox id="txtSubscriberID" runat="server"></asp:textbox>
										</td>
										<td>
											<asp:label id="lblPeriod_from" runat="server">Period from</asp:label><br>
											<asp:textbox id="txtPeriodFrom" runat="server"></asp:textbox>
											<input onclick="PopupPicker('ReportCtrl1_txtPeriodFrom', 250, 250);" type="button" value="..."
												class="popup">
										</td>
										<td>
											<asp:label id="lblPeriod_to" runat="server">Period to</asp:label><br>
											<asp:textbox id="txtPeriodTo" runat="server"></asp:textbox>
											<input onclick="PopupPicker('ReportCtrl1_txtPeriodTo', 250, 250);" type="button" value="..."
												class="popup">
										</td>
										<td>
											<asp:label id="lblDetailLevel" runat="server">Detail level</asp:label><br>
											<asp:dropdownlist id="ddlDetailLevels" runat="server">
												<asp:listitem value="0" selected="True">Low (sum)</asp:listitem>
												<asp:listitem value="1">Medium (agreement)</asp:listitem>
												<asp:listitem value="2">High (lines)</asp:listitem>
											</asp:dropdownlist>
										</td>
									</tr>
									<tr>
										<td id="tdIsBillable" runat="server">
											<asp:label id="lblIsBillable" runat="server">[Billing events]</asp:label>
											<asp:radiobuttonlist id="rblIsBillable" runat="server" repeatdirection="Horizontal" cssclass="radio">
												<asp:listitem value="0" selected="True">[All]</asp:listitem>
												<asp:listitem value="1">[Billed]</asp:listitem>
											</asp:radiobuttonlist>
										</td>
									</tr>
									<tr>
										<td>
											<asp:hyperlink id="hlExcel" runat="server" visible="False" target="_blank">HyperLink</asp:hyperlink>
										</td>
									</tr>
									<tr>
										<td height="23">
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td>
					<table class="empty_table" cellspacing="0">
						<tr>
							<td align="left">
								<asp:label id="lblMessage" runat="server" visible="False"></asp:label>
							</td>
							<td align="right">
								<asp:button id="btnSearch" runat="server" cssclass="confirm_button" text="Go"></asp:button>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
