<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ItemRegistryCtrl.ascx.cs" Inherits="Billing.user_controls.Business_Ctrls.ItemRegistryCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" src="CheckEnterKey.js"></script>
<script language="javascript"> 
	function checkEnterKey(path) 
	{    
		if (event.keyCode == 13) 
		{        
			event.cancelBubble = true;
			event.returnValue = false;
			document.BillingMain.ItemRegistryCtrl_btnInsert.click(); 
		}
	} 
	
	function SetFormFocus()
	{
		if (document.BillingMain.ItemRegistryCtrl_txtItemNameNative.disabled == false)
		{
			document.BillingMain.ItemRegistryCtrl_txtItemNameNative.focus();
		}
	}
</script>
<body onload="SetFormFocus()">
	<form id="ItemRegistry" method="post">
		<table width="100%">
			<tr>
				<td>
					<table class="grid_table" cellspacing="0" cellpadding="0">
						<tr>
							<th>
								<asp:label id="lblPageHeader" runat="server"></asp:label></th></tr>
						<tr>
							<td>
								<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
									<tr>
										<td style="WIDTH: 25%"><asp:label id="lblItemName" runat="server"></asp:label><br>
											<asp:textbox id="txtItemNameNative" runat="server" maxlength="100"></asp:textbox><asp:requiredfieldvalidator id="validateItemNameNative" runat="server" controltovalidate="txtItemNameNative"
												display="Static" errormessage="txtItemNameNative">*</asp:requiredfieldvalidator></td>
										<td style="WIDTH: 25%" width="25%"><asp:label id="lblItemNameEN" runat="server"></asp:label><br>
											<asp:textbox id="txtItemNameEN" runat="server" maxlength="100"></asp:textbox><asp:requiredfieldvalidator id="validateItemNameEN" runat="server" controltovalidate="txtItemNameEN" display="Static"
												errormessage="txtItemNameEN" enabled="False">*</asp:requiredfieldvalidator></td>
										<td style="WIDTH: 25%" width="25%"><asp:label id="lblCurrency" runat="server"></asp:label><br>
											<asp:dropdownlist id="ddlCurrency" runat="server"></asp:dropdownlist></td>
										<td><asp:label id="lblFinancialKey" runat="server"></asp:label><br>
											<asp:textbox id="txtFinancialKey" runat="server" maxlength="50"></asp:textbox></td>
									</tr>
									<tr>
										<td><asp:label id="lblMonthlyAmount" runat="server"></asp:label><br>
											<asp:textbox id="txtMonthlyAmount" runat="server" maxlength="100" cssclass="rightAlign">0</asp:textbox><asp:comparevalidator id="validateDoubleForMonthlyAmount" runat="server" controltovalidate="txtMonthlyAmount"
												display="Static" errormessage="txtMonthlyAmount" operator="DataTypeCheck" type="Double">*</asp:comparevalidator></td>
										<td><asp:label id="lblUsageAmount" runat="server"></asp:label><br>
											<asp:textbox id="txtUsageAmount" runat="server" maxlength="100" cssclass="rightAlign">0</asp:textbox><asp:comparevalidator id="validateDoubleForUsageAmount" runat="server" controltovalidate="txtUsageAmount"
												display="Static" errormessage="txtUsageAmount" operator="DataTypeCheck" type="Double">*</asp:comparevalidator></td>
										<td><asp:label id="lblMonthlyDisc" runat="server"></asp:label><br>
											<asp:textbox id="txtMonthlyDisc" runat="server" maxlength="100" cssclass="rightAlign">0</asp:textbox><asp:comparevalidator id="validateIntForMonthlyDisc" runat="server" controltovalidate="txtMonthlyDisc"
												display="Static" errormessage="txtMonthlyDisc" operator="DataTypeCheck" type="Double">*</asp:comparevalidator></td>
										<td><asp:label id="lblUsageDisc" runat="server"></asp:label><br>
											<asp:textbox id="txtUsageDisc" runat="server" maxlength="100" cssclass="rightAlign">0</asp:textbox><asp:comparevalidator id="validateIntForUsageDisc" runat="server" controltovalidate="txtUsageDisc" display="Static"
												errormessage="txtUsageDisc" operator="DataTypeCheck" type="Double">*</asp:comparevalidator></td>
									</tr>
									<tr>
										<td><asp:label id="lblProductLink" runat="server"></asp:label><br>
											<asp:dropdownlist id="ddlProductLink" runat="server"></asp:dropdownlist></td>
										<td><asp:label id="lblUsageLink" runat="server"></asp:label><br>
											<asp:dropdownlist id="ddlUsageLink" runat="server"></asp:dropdownlist></td>
										<td colspan="2"><asp:label id="lblIsOpen" runat="server"></asp:label><br>
											<asp:checkbox id="chkIsOpen" runat="server" cssclass="radio" checked="True"></asp:checkbox></td>
									</tr>
									<tr>
										<td style="HEIGHT: 23px"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td>
								<table class="empty_table" cellspacing="0">
									<tr valign="top">
										<td align="left"><asp:label id="lblMessage" runat="server" visible="False"></asp:label><br>
											<asp:validationsummary id="ValidationSummary1" runat="server" width="100%"></asp:validationsummary></td>
										<td id="tdInsertButton" align="right" runat="server"><asp:button id="btnInsert" runat="server" cssclass="confirm_button"></asp:button></td>
										<td id="tdSelectedButtons" align="right" runat="server"><asp:button id="btnCancel" runat="server" cssclass="cancel_button"></asp:button><asp:button id="btnCopy" runat="server" cssclass="gray_button"></asp:button><asp:button id="btnUpdate" runat="server" cssclass="confirm_button"></asp:button></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
									<tr>
										<td height="10"></td>
									</tr>
									<tr>
										<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
									</tr>
									<tr>
										<td height="5"></td>
									</tr>
									<tr>
										<th>
											<asp:label id="lblItemIndexHeader" runat="server"></asp:label></th></tr>
									<tr>
										<td>
											<table class="datagrid" cellspacing="0" cellpadding="0">
												<tr>
													<td><asp:datagrid id="dgItems" runat="server" cssclass="grid" gridlines="None" allowsorting="True"
															autogeneratecolumns="False">
															<footerstyle cssclass="grid_footer"></footerstyle>
															<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
															<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
															<itemstyle cssclass="grid_item"></itemstyle>
															<headerstyle cssclass="grid_header"></headerstyle>
															<columns>
																<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																	commandname="Select">
																	<itemstyle cssclass="leftpadding"></itemstyle>
																</asp:buttoncolumn>
																<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
																	commandname="Delete">
																	<itemstyle cssclass="nopadding"></itemstyle>
																</asp:buttoncolumn>
																<asp:boundcolumn visible="False" datafield="ID" headertext="ID">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="ItemNameNative" sortexpression="ItemNameNative" headertext="ItemNameNative">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="ItemNameEN" sortexpression="ItemNameEN" headertext="ItemNameEN">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="Currency" sortexpression="Currency" headertext="Currency">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="MonthlyAmount" sortexpression="MonthlyAmount" headertext="Monthly">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="UsageAmount" sortexpression="UsageAmount" headertext="Usage">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="MonthlyDiscountPercent" sortexpression="MonthlyDiscountPercent" headertext="Monthly%">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="UsageDiscountPercent" sortexpression="UsageDiscountPercent" headertext="Usage%">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:boundcolumn datafield="FinancialKey" sortexpression="FinancialKey" headertext="FinancialKey">
																	<itemstyle cssclass="padding"></itemstyle>
																</asp:boundcolumn>
																<asp:templatecolumn sortexpression="IsActive">
																	<itemstyle cssclass="padding"></itemstyle>
																	<itemtemplate>
																		<asp:CheckBox id="IsActive" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem,"IsActive"))%>' Runat="server" Enabled="False" cssclass="radio">
																		</asp:checkbox>
																	</itemtemplate>
																</asp:templatecolumn>
															</columns>
															<pagerstyle cssclass="grid_pager"></pagerstyle>
														</asp:datagrid></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
