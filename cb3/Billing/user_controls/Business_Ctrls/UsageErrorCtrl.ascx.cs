#region

using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;

#endregion

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    ///		Summary description for UsageErrorCtrl.
    /// </summary>
    public class UsageErrorCtrl : GroundCtrl {
        protected DataView _UsageWithMultipleBillingItemsView;
        protected DataView _UsageWithoudProductView;
        protected DataView _UsageWithoutAgreementView;
        protected DataView _UsageWithoutUsersView;
        protected Button btnSearch;
        protected CompareValidator CompareValidator2;
        protected CompareValidator cpvDateFrom;
        protected DataGrid dgrWithMultiBillItems;
        protected DataGrid dgrWithoutAgreement;
        protected DataGrid dgrWithoutProduct;
        protected DataGrid dgrWithoutUser;
        protected Label lblDateFrom;
        protected Label lblDateTo;
        protected Label lblPageHeader;
        protected Label lblWithMultiBillItems;
        protected Label lblWithoutAgreement;
        protected Label lblWithoutProductHeader;
        protected Label lblWithoutUser;
        protected RequiredFieldValidator rfvDateFrom;
        protected RequiredFieldValidator rfvDateTo;
        protected HtmlTable tblWithMultiBillItems;
        protected HtmlTable tblWithoutAgreement;
        protected HtmlTable tblWithoutProductHeader;
        protected HtmlTable tblWithoutUser;
        protected TextBox txtDateFrom;
        protected TextBox txtDateTo;

        public DataView UsageWithoudProductView {
            get {
                if (_UsageWithoudProductView == null) {
                    DateTime dtTo = DateTime.Parse(txtDateTo.Text);
                    dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                    _UsageWithoudProductView =
                        new DataView(
                            BillingFactory.GetUsageWithoutProduct(DateTime.Parse(txtDateFrom.Text), dtTo).Tables[0]);
                }
                return _UsageWithoudProductView;
            }
        }

        public DataView UsageWithoutAgreementView {
            get {
                if (_UsageWithoutAgreementView == null) {
                    DateTime dtTo = DateTime.Parse(txtDateTo.Text);
                    dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                    _UsageWithoutAgreementView =
                        new DataView(
                            BillingFactory.GetUsageWithoutAgreement(DateTime.Parse(txtDateFrom.Text), dtTo).Tables[0]);
                }
                return _UsageWithoutAgreementView;
            }
        }

        public DataView UsageWithoutUsersView {
            get {
                if (_UsageWithoutUsersView == null) {
                    DateTime dtTo = DateTime.Parse(txtDateTo.Text);
                    dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                    _UsageWithoutUsersView =
                        new DataView(
                            BillingFactory.GetUsageWithoutUsers(DateTime.Parse(txtDateFrom.Text), dtTo).Tables[0]);
                }
                return _UsageWithoutUsersView;
            }
        }

        public DataView UsageWithMultipleBillingItemsView {
            get {
                if (_UsageWithMultipleBillingItemsView == null) {
                    DateTime dtTo = DateTime.Parse(txtDateTo.Text);
                    dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                    _UsageWithMultipleBillingItemsView =
                        new DataView(
                            BillingFactory.GetUsageWithMultipleBillingItems(DateTime.Parse(txtDateFrom.Text)).Tables[0]);
                }
                return _UsageWithMultipleBillingItemsView;
            }
        }

        private void Page_Load(object sender, EventArgs e) {
            base.PageLoad(sender, e);
            LocalizeText();

            if (!IsPostBack) {
                tblWithoutProductHeader.Visible = false;
                tblWithMultiBillItems.Visible = false;
                tblWithoutAgreement.Visible = false;
                tblWithoutUser.Visible = false;
            }
        }

        protected override void LocalizeText() {
            // Buttons
            btnSearch.Text = rm.GetString("btnSearch", ci);

            // Labels
            lblPageHeader.Text = rm.GetString("lblErrorPage", ci);
            lblDateFrom.Text = rm.GetString("txtBeginDate", ci);
            lblDateTo.Text = rm.GetString("txtEndDate", ci);
            lblWithoutProductHeader.Text = rm.GetString("lblWithoutProduct", ci);
            lblWithoutAgreement.Text = rm.GetString("lblWithoutAgreement", ci);
            lblWithoutUser.Text = rm.GetString("lblWithoutUser", ci);
            lblWithMultiBillItems.Text = rm.GetString("lblWithMultiAgreementItems", ci);

            // DataGrid - Without Product
            int i = 0;
            dgrWithoutProduct.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.lblUsageLink", ci);
            dgrWithoutProduct.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.colItemName", ci);
            dgrWithoutProduct.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.colUsage", ci);
            // DataGrid - Without Agreement
            i = 0;
            dgrWithoutAgreement.Columns[i++].HeaderText = rm.GetString("FindCtrl.lblCreditInfoID", ci);
            dgrWithoutAgreement.Columns[i++].HeaderText = rm.GetString("AgreementCtrl.lblSubscriber", ci);
            dgrWithoutAgreement.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.lblUsageLink", ci);
            dgrWithoutAgreement.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.colItemName", ci);
            dgrWithoutAgreement.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.colUsage", ci);
            dgrWithoutAgreement.Columns[i++].HeaderText = rm.GetString("txtBeginDate", ci);
            dgrWithoutAgreement.Columns[i++].HeaderText = rm.GetString("txtEndDate", ci);
            // DataGrid - Without User
            i = 0;
            dgrWithoutUser.Columns[i++].HeaderText = rm.GetString("FindCtrl.lblCreditInfoID", ci);
            dgrWithoutUser.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.lblUsageLink", ci);
            dgrWithoutUser.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.colItemName", ci);
            dgrWithoutUser.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.colUsage", ci);
            dgrWithoutUser.Columns[i++].HeaderText = rm.GetString("txtBeginDate", ci);
            dgrWithoutUser.Columns[i++].HeaderText = rm.GetString("txtEndDate", ci);
            // DataGrid - With Multiple Agreement Lines			
            i = 0;
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("FindCtrl.lblCreditInfoID", ci);
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("AgreementCtrl.lblSubscriber", ci);
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("Agreement.lnkAgreement", ci);
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.lblUsageLink", ci);
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("ItemRegistryCtrl.colItemName", ci);
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("Agreement.lnkAgreement", ci);
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("txtBeginDate", ci);
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("txtEndDate", ci);
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("lblConflictFrom", ci);
            dgrWithMultiBillItems.Columns[i++].HeaderText = rm.GetString("lblConflictTo", ci);
        }

        private void btnSearch_Click(object sender, EventArgs e) { Search(); }

        private void Search() {
            dgrWithoutProduct.DataSource = UsageWithoudProductView;
            dgrWithoutProduct.DataBind();

            dgrWithoutAgreement.DataSource = UsageWithoutAgreementView;
            dgrWithoutAgreement.DataBind();

            dgrWithoutUser.DataSource = UsageWithoutUsersView;
            dgrWithoutUser.DataBind();

            dgrWithMultiBillItems.DataSource = UsageWithMultipleBillingItemsView;
            dgrWithMultiBillItems.DataBind();

            tblWithoutProductHeader.Visible = true;
            tblWithMultiBillItems.Visible = true;
            tblWithoutAgreement.Visible = true;
            tblWithoutUser.Visible = true;
        }

        private void dgrWithoutProduct_SortCommand(object source, DataGridSortCommandEventArgs e) {
            Search();
            UsageWithoudProductView.Sort = e.SortExpression;
            dgrWithoutProduct.DataBind();
            if (dgrWithoutProduct.Items.Count > 0) {
                tblWithoutProductHeader.Visible = true;
            } else {
                tblWithoutProductHeader.Visible = false;
            }
        }

        private void dgrWithoutAgreement_SortCommand(object source, DataGridSortCommandEventArgs e) {
            Search();
            UsageWithoutAgreementView.Sort = e.SortExpression;
            dgrWithoutAgreement.DataBind();
            if (dgrWithoutAgreement.Items.Count > 0) {
                tblWithoutAgreement.Visible = true;
            } else {
                tblWithoutAgreement.Visible = false;
            }
        }

        private void dgrWithoutUser_SortCommand(object source, DataGridSortCommandEventArgs e) {
            Search();
            UsageWithoutUsersView.Sort = e.SortExpression;
            dgrWithoutUser.DataBind();
            if (dgrWithoutUser.Items.Count > 0) {
                tblWithoutUser.Visible = true;
            } else {
                tblWithoutUser.Visible = false;
            }
        }

        private void dgrWithMultiBillItems_SortCommand(object source, DataGridSortCommandEventArgs e) {
            Search();
            UsageWithMultipleBillingItemsView.Sort = e.SortExpression;
            dgrWithMultiBillItems.DataBind();
            if (dgrWithMultiBillItems.Items.Count > 0) {
                tblWithMultiBillItems.Visible = true;
            } else {
                tblWithMultiBillItems.Visible = false;
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}