<%@ Control Language="c#" AutoEventWireup="false" Codebehind="IndexListCtrl.ascx.cs" Inherits="Billing.user_controls.Business_Ctrls.IndexListCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" src="DatePicker.js"></script>
<form id="Indexes" method="post">
	<table width="100%">
		<tr>
			<td>
				<table class="grid_table" cellspacing="0" cellpadding="0">
					<tr>
						<th>
							<asp:label id="lblPageHeader" runat="server"></asp:label></th></tr>
					<tr>
						<td>
							<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
								<tr>
									<td style="WIDTH: 33%"><asp:label id="lblIndexName" runat="server"></asp:label><br>
										<asp:dropdownlist id="ddlIndexes" runat="server" autopostback="True"></asp:dropdownlist></td>
									<td width="33%" style="WIDTH: 33%"><asp:label id="lblIndexDate" runat="server"></asp:label><br>
										<asp:textbox id="txtIndexDate" runat="server"></asp:textbox>&nbsp; <input onclick="PopupPicker('Indexlistctrl1_txtIndexDate', 250, 250);" type="button" value="..."
											class="popup">
										<asp:requiredfieldvalidator id="validateBeginDate" runat="server" controltovalidate="txtIndexDate" errormessage="IndexBeginDate">*</asp:requiredfieldvalidator>
									</td>
									<td><asp:label id="lblIndexValue" runat="server"></asp:label><br>
										<asp:textbox id="txtIndexValue" runat="server">0</asp:textbox><asp:requiredfieldvalidator id="validatePercent" runat="server" controltovalidate="txtIndexValue" errormessage="IndexValue">*</asp:requiredfieldvalidator>
									</td>
								</tr>
								<tr>
									<td height="23"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="10"></td>
		</tr>
		<tr>
			<td>
				<table class="empty_table" cellspacing="0">
					<tr valign="top">
						<td align="left"><asp:label id="lblMessage" runat="server"></asp:label><asp:validationsummary id="ValidationSummary" runat="server" width="100%"></asp:validationsummary></td>
						<td align="right"><asp:button id="btnInsert" cssclass="confirm_button" runat="server"></asp:button></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
					<tr>
						<td height="10"></td>
					</tr>
					<tr>
						<td align="right">
							<asp:label id="lblDatagridIcons" runat="server"></asp:label>
						</td>
					</tr>
					<tr>
						<td height="5"></td>
					</tr>
					<tr>
						<th>
							<asp:label id="lblIndexHeader" runat="server"></asp:label></th></tr>
					<tr>
						<td>
							<table class="datagrid" cellspacing="0" cellpadding="0">
								<tr>
									<td><asp:datagrid id="dgIndexList" runat="server" allowsorting="True" bordercolor="Gray" borderstyle="None"
											borderwidth="1px" backcolor="White" cellpadding="4" width="100%" autogeneratecolumns="False"
											gridlines="None">
											<footerstyle cssclass="grid_footer"></footerstyle>
											<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
											<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
											<itemstyle cssclass="grid_item"></itemstyle>
											<headerstyle cssclass="grid_header"></headerstyle>
											<columns>
												<asp:editcommandcolumn buttontype="LinkButton" updatetext="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;"
													canceltext="&lt;img src=&quot;../img/cancel.gif&quot; alt=&quot;Cancel&quot; border=&quot;0&quot;&gt;"
													edittext="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;">
													<itemstyle cssclass="leftpadding"></itemstyle>
												</asp:editcommandcolumn>
												<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
													commandname="Delete">
													<itemstyle cssclass="nopadding"></itemstyle>
												</asp:buttoncolumn>
												<asp:boundcolumn visible="False" datafield="IndexName" sortexpression="IndexName" readonly="True"
													headertext="IndexName">
													<itemstyle cssclass="padding"></itemstyle>
												</asp:boundcolumn>
												<asp:boundcolumn datafield="IndexDate" sortexpression="IndexDate" headertext="IndexDate" dataformatstring="{0:d}">
													<itemstyle cssclass="padding"></itemstyle>
												</asp:boundcolumn>
												<asp:boundcolumn datafield="IndexValue" sortexpression="IndexValue" headertext="IndexValue">
													<itemstyle cssclass="padding"></itemstyle>
												</asp:boundcolumn>
											</columns>
											<pagerstyle cssclass="grid_pager"></pagerstyle>
										</asp:datagrid></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
