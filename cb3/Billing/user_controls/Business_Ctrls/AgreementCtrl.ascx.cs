#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;
using Logging.BLL;
using UserAdmin.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    ///		Summary description for AgreementCtrl.
    /// </summary>
    /// <attention>
    /// This page is special, it uses Page.Validation and Custom validation for Item and Comment.
    /// Custom validation is done by 2 methods, IsItemValid() & IsCommentValid().
    /// This is done because each part is validated individually.
    /// So any new Validator for Item or Comment must be added into the appropriate method 
    /// and marked with Visible=false in order for it to work correctly.
    /// </attention>
    public class AgreementCtrl : AgreementGroundCtrl {
        protected static int AGREEMENT = 1;
        protected static int COMMENT = 4;
        protected static int ITEM = 2;
        protected Button btnAddIndex;
        protected Button btnCancel;
        protected Button btnDepartmentMoveAll;
        protected Button btnDepartmentMoveOne;
        protected Button btnDepartmentRemoveAll;
        protected Button btnDepartmentRemoveOne;
        protected Button btnInsertComment;
        protected Button btnInsertItem;
        protected Button btnSave;
        protected CheckBox chkVisible;
        protected CompareValidator CompareBeginDate;
        protected CompareValidator CompareDiscount;
        protected CompareValidator CompareEndDate;
        protected CompareValidator CompareOffset;
        protected CompareValidator ComparePrice;
        protected CompareValidator cpvFixedAmount;
        protected CompareValidator cpvFixedDiscount;
        protected CompareValidator cpvItemBeginDate;
        protected CompareValidator cpvItemEndDate;
        protected CompareValidator cpvItemReminderOffset;
        protected CompareValidator cpvMonthlyDiscount;
        protected CompareValidator cpvUsageDiscount;
        protected DropDownList ddlCounterLink;
        protected DropDownList ddlCurrencyList;
        protected DropDownList ddlIndexLink;
        protected DropDownList ddlItemList;
        protected DropDownList ddlSignedBy;
        protected DataGrid dgComments;
        protected DataGrid dgIndexes;
        protected DataGrid dgItems;
        protected Label Label1;
        protected Label Label11;
        protected Label Label13;
        protected Label Label2;
        protected Label Label26;
        protected Label Label27;
        protected Label Label28;
        protected Label Label3;
        protected Label Label4;
        protected Label Label5;
        protected Label Label6;
        protected Label lblAgreementDiscount;
        protected Label lblAgreementID;
        protected Label lblAgreementItemsHeader;
        protected Label lblAgreementPrice;
        protected Label lblBeginDate;
        protected Label lblComment;
        protected Label lblCommentFieldsHeader;
        protected Label lblCommentNotificationEmail;
        protected Label lblCommentsGridHeader;
        protected Label lblCommentsHeader;
        protected Label lblCounterLink;
        protected Label lblCurrency;
        protected Label lblDatagridIcons;
        protected Label lblDepartments;
        protected Label lblDepartmentsAvailable;
        protected Label lblDepartmentsSelected;
        protected Label lblEndDate;
        protected Label lblFixedAmount;
        protected Label lblFixedDiscount;
        protected Label lblFreezone;
        protected Label lblGeneralDescription;
        protected Label lblIndexesGridHeader;
        protected Label lblIndexGridIcons;
        protected Label lblIndexLink;
        protected Label lblIsVisible;
        protected Label lblItem;
        protected Label lblItemBeginDate;
        protected Label lblItemEndDate;
        protected Label lblItemFieldsHeader;
        protected Label lblItemNotificationEmails;
        protected Label lblItemReminderOffset;
        protected Label lblMessage;
        protected Label lblMonthlyAmount;
        protected Label lblMonthlyDiscount;
        protected Label lblNotificationDate;
        protected Label lblNotificationEmails;
        protected Label lblPageHeader;
        protected Label lblReminderOffset;
        protected Label lblSignerName;
        protected Label lblSubscriber;
        protected Label lblSubscriberName;
        protected Label lblTheMonthlyAmount;
        protected Label lblTheUsageAmount;
        protected Label lblUsageAmount;
        protected Label lblUsageDiscount;
        protected ListBox lbxDepartmentsAvailable;
        protected ListBox lbxDepartmentsSelected;
        protected RegularExpressionValidator NotificationEmailRegularExpression;
        protected RequiredFieldValidator requiredAgreementID;
        protected RequiredFieldValidator requiredBeginDate;
        protected RequiredFieldValidator requiredEndDate;
        protected RegularExpressionValidator revCommentNotifyEmails;
        protected RegularExpressionValidator revItemNotifyEmails;
        protected RequiredFieldValidator rfvComment;
        protected RequiredFieldValidator rfvCommentEmails;
        protected RequiredFieldValidator rfvCommentNotifyDate;
        protected RequiredFieldValidator rfvItemBeginDate;
        protected RequiredFieldValidator rfvItemEndDate;
        protected HtmlTable tblCommentsGrid;
        protected HtmlTable tblIndexesGrid;
        protected HtmlTable tblItemsGrid;
        protected TextBox txtAgreementDiscount;
        protected TextBox txtAgreementID;
        protected TextBox txtAgreementPrice;
        protected TextBox txtBeginDate;
        protected TextBox txtComment;
        protected TextBox txtCommentNotificationEmails;
        protected TextBox txtEndDate;
        protected TextBox txtFixedAmount;
        protected TextBox txtFixedDiscount;
        protected TextBox txtFreezone;
        protected TextBox txtGeneralDescription;
        protected TextBox txtItemBeginDate;
        protected TextBox txtItemEndDate;
        protected TextBox txtItemNotificationEmails;
        protected TextBox txtItemReminderOffset;
        protected TextBox txtMonthlyDiscount;
        protected TextBox txtNotificationDate;
        protected TextBox txtNotificationEmails;
        protected TextBox txtReminderOffset;
        protected TextBox txtUsageDiscount;
        protected ValidationSummary ValidationSummary;

        private void Page_Load(object sender, EventArgs e) {
            PageLoad(sender, e);

            // Add <ENTER> event handler to input controls
            AddEnterEvent();

            LocalizeText();
            lblMessage.Visible = false;

            // Initialize the agreement data object so it is ready for use. Either set it as a "new" object
            // or fetch values from the session object. This function is handled by the parent class
            // AgreementGroundCtrl
            InitializeAgreement();

            if (!IsPostBack) {
                tblItemsGrid.Visible = false;
                tblIndexesGrid.Visible = false;
                tblCommentsGrid.Visible = false;

                // Set dropdown menus
                SetDropDowns();

                // Set default values
                SetDefaults(AGREEMENT + ITEM + COMMENT);

                SetupData();
                SetItemsDataGrid();
                SetCommentsDataGrid();

                // Lock or unlock controls to conform to user selection for viewable or updateable agreements
                // Uses the LockControls function of the AgreementGroundControl (inherited)
                LockControls(AgreementUnlock, this);

                // Check to see if there is an agreement loaded...
                if (AlreadyAnAgreement()) {
                    // If there is an agreement then load the controls with appropriate values.
                    SetControlsForAgreement();
                } else {
                    // Function to update the agreement. It is implemented in the parent class.
                    UpdateInMemoryAgreement();
                }

                if (!AgreementUnlock) {
                    btnSave.Visible = false;
                    btnAddIndex.Visible = false;
                    btnInsertItem.Visible = false;
                    btnInsertComment.Visible = false;
                } else {
                    btnSave.Visible = true;
                    btnAddIndex.Visible = true;
                    btnInsertItem.Visible = true;
                    btnInsertComment.Visible = true;
                }

                btnSave.Visible = AgreementUnlock;
            }
        }

        /// <summary>
        /// Sets the controls accoring to the "in memory" agreement.
        /// </summary>
        private void SetControlsForAgreement() {
            DataTable mySubscriberTable = BillingFactory.GetSingleSubscribersAsDataTable(Agreement.SubscriberID);
            lblSubscriberName.Text = mySubscriberTable.Rows[0]["NameNative"].ToString();
            mySubscriberTable.Dispose();

            // If the "in memory" agreement is already registered....
            if (!NewAgreementKnownSubscriber()) {
                if (Agreement.AgreementNumber != null) {
                    txtAgreementID.Text = Agreement.AgreementNumber;
                    txtAgreementID.Enabled = false;
                }

                if (Agreement.GeneralDescription != null) {
                    txtGeneralDescription.Text = Agreement.GeneralDescription;
                }

                if (Agreement.BeginDate != DateTime.MinValue) {
                    txtBeginDate.Text = Agreement.BeginDate.ToShortDateString();
                }

                if (Agreement.EndDate != DateTime.MinValue) {
                    txtEndDate.Text = Agreement.EndDate.ToShortDateString();
                }

                if (Agreement.Currency != null) {
                    ddlCurrencyList.SelectedValue = Agreement.Currency;
                }

                if (Agreement.NotificationEmails != null) {
                    txtNotificationEmails.Text = Agreement.NotificationEmails;
                }

                if (Agreement.SignerCIID > 0) {
                    ddlSignedBy.SelectedValue = Agreement.SignerCIID.ToString();
                }

                // These params are set to "0" for new agreements (int values are stucts)
                // so we don't have to check them specifically for values...
                txtAgreementPrice.Text = Agreement.AgreementAmount.ToString();
                txtAgreementDiscount.Text = Agreement.Discountpercent.ToString();
                txtReminderOffset.Text = Agreement.ReminderOffset.ToString();
            }
        }

        /// <summary>
        /// Links an "enter event" to input controls
        /// </summary>
        /// <remarks>
        /// Links an "enter event" to all input controls so as to allow the user to press the
        /// enter key to submit a new item.
        /// </remarks>
        private void AddEnterEvent() {
            txtAgreementDiscount.Attributes.Add("onkeypress", "checkEnterKey();");
            txtAgreementID.Attributes.Add("onkeypress", "checkEnterKey();");
            txtAgreementPrice.Attributes.Add("onkeypress", "checkEnterKey();");
            txtBeginDate.Attributes.Add("onkeypress", "checkEnterKey();");
            txtEndDate.Attributes.Add("onkeypress", "checkEnterKey();");
            txtGeneralDescription.Attributes.Add("onkeypress", "checkEnterKey();");
            txtNotificationEmails.Attributes.Add("onkeypress", "checkEnterKey();");
            txtReminderOffset.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        /// <summary>
        /// For text localization. Text is set by using resource files that are XML based.
        /// </summary>
        protected override void LocalizeText() {
            base.LocalizeText();

            // AGREEMENT
            // Labels...
            lblAgreementDiscount.Text = rm.GetString("AgreementCtrl.lblAgreementDiscount", ci);
            lblAgreementID.Text = rm.GetString("AgreementCtrl.lblAgreementID", ci);
            lblAgreementPrice.Text = rm.GetString("AgreementCtrl.lblAgreementPrice", ci);
            lblBeginDate.Text = rm.GetString("AgreementCtrl.lblBeginDate", ci);
            lblCurrency.Text = rm.GetString("AgreementCtrl.lblCurrency", ci);
            lblEndDate.Text = rm.GetString("AgreementCtrl.lblEndDate", ci);
            lblGeneralDescription.Text = rm.GetString("AgreementCtrl.lblGeneralDescription", ci);
            lblNotificationEmails.Text = rm.GetString("lblNotificationEmail", ci);
            lblReminderOffset.Text = rm.GetString("AgreementCtrl.lblReminderOffset", ci);
            lblSignerName.Text = rm.GetString("AgreementCtrl.lblSignerName", ci);
            lblSubscriber.Text = rm.GetString("AgreementCtrl.lblSubscriber", ci);
            lblPageHeader.Text = rm.GetString("AgreementCtrl.lblPageHeader", ci);

            // Buttons
            btnCancel.Text = rm.GetString("Agreement.btnCancel", ci);
            btnSave.Text = rm.GetString("Agreement.btnSave", ci);

            // Validators
            requiredAgreementID.ErrorMessage = rm.GetString("Agreement.requiredAgreementID", ci);
            requiredBeginDate.ErrorMessage = rm.GetString("Agreement.requiredBeginDate", ci);
            requiredEndDate.ErrorMessage = rm.GetString("Agreement.requiredEndDate", ci);
            CompareBeginDate.ErrorMessage = rm.GetString("Agreement.CompareBeginDate", ci);
            CompareDiscount.ErrorMessage = rm.GetString("Agreement.CompareDiscount", ci);
            CompareEndDate.ErrorMessage = rm.GetString("Agreement.CompareEndDate", ci);
            CompareOffset.ErrorMessage = rm.GetString("Agreement.CompareOffset", ci);
            ComparePrice.ErrorMessage = rm.GetString("Agreement.ComparePrice", ci);
            NotificationEmailRegularExpression.ErrorMessage =
                rm.GetString("AgreementCommentsCtrl.NotificationEmailRegularExpression", ci);

            // ITEMS
            // Labels...
            lblAgreementItemsHeader.Text = rm.GetString("AgreementItemsCtrl.lblAgreementItemsHeader", ci);
            lblItemBeginDate.Text = rm.GetString("AgreementItemsCtrl.lblBeginDate", ci);
            lblCounterLink.Text = rm.GetString("AgreementItemsCtrl.lblCounterLink", ci);
            lblItemEndDate.Text = rm.GetString("AgreementItemsCtrl.lblEndDate", ci);
            lblFixedAmount.Text = rm.GetString("AgreementItemsCtrl.lblFixedAmount", ci);
            lblFixedDiscount.Text = rm.GetString("AgreementItemsCtrl.lblFixedDiscount", ci);
            lblIndexLink.Text = rm.GetString("AgreementItemsCtrl.lblIndexLink", ci);
            lblIsVisible.Text = rm.GetString("AgreementItemsCtrl.lblIsVisible", ci);
            lblItem.Text = rm.GetString("AgreementItemsCtrl.lblItem", ci);
            lblMonthlyAmount.Text = rm.GetString("AgreementItemsCtrl.lblMonthlyAmount", ci);
            lblMonthlyDiscount.Text = rm.GetString("AgreementItemsCtrl.lblMonthlyDiscount", ci);
            lblItemNotificationEmails.Text = rm.GetString("lblNotificationEmail", ci);
            lblItemReminderOffset.Text = rm.GetString("AgreementItemsCtrl.lblReminderOffset", ci);
            lblUsageAmount.Text = rm.GetString("AgreementItemsCtrl.lblUsageAmount", ci);
            lblUsageDiscount.Text = rm.GetString("AgreementItemsCtrl.lblUsageDiscount", ci);
            lblItemFieldsHeader.Text = rm.GetString("AgreementItemsCtrl.lblPageHeader", ci);
            lblFreezone.Text = rm.GetString("AgreementItemsCtrl.lblFreezone", ci);
            lblIndexesGridHeader.Text = rm.GetString("AgreementItemsCtrl.btnAddIndex", ci);
            lblDepartments.Text = rm.GetString("lblDepartments", ci);
            lblDepartmentsAvailable.Text = rm.GetString("lblAvailable", ci);
            lblDepartmentsSelected.Text = rm.GetString("lblSelected", ci);

            // Buttons
            btnAddIndex.Text = rm.GetString("AgreementItemsCtrl.btnAddIndex", ci);
            btnInsertItem.Text = rm.GetString("AgreementItemsCtrl.btnInsertItem", ci);

            // Validators
            rfvItemBeginDate.ErrorMessage = rm.GetString("AgreementItemsCtrl.requiredBeginDate", ci);
            rfvItemEndDate.ErrorMessage = rm.GetString("AgreementItemsCtrl.requiredEndDate", ci);
            cpvItemBeginDate.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareBeginDate", ci);
            cpvItemEndDate.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareEndDate", ci);
            cpvFixedDiscount.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareFiexedDiscount", ci);
            cpvFixedAmount.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareFixedPrice", ci);
            cpvMonthlyDiscount.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareMonthlyDiscount", ci);
            cpvItemReminderOffset.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareOffset", ci);
            cpvUsageDiscount.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareUsageDiscount", ci);
            revItemNotifyEmails.ErrorMessage = rm.GetString(
                "AgreementCommentsCtrl.NotificationEmailRegularExpression", ci);

            // Datagrid columns
            dgItems.Columns[3].HeaderText = rm.GetString("AgreementCommentsCtrl.ItemNameNativeCol", ci);
            dgItems.Columns[4].HeaderText = rm.GetString("AgreementCommentsCtrl.ItemNameEnCol", ci);
            dgItems.Columns[5].HeaderText = rm.GetString("AgreementCommentsCtrl.FixedCol", ci);
            dgItems.Columns[6].HeaderText = rm.GetString("AgreementCommentsCtrl.FixedPerCol", ci);
            dgItems.Columns[7].HeaderText = rm.GetString("AgreementCommentsCtrl.MonthlyCol", ci);
            dgItems.Columns[8].HeaderText = rm.GetString("AgreementCommentsCtrl.MonthlyPerCol", ci);
            dgItems.Columns[9].HeaderText = rm.GetString("AgreementCommentsCtrl.UsageCol", ci);
            dgItems.Columns[10].HeaderText = rm.GetString("AgreementCommentsCtrl.UsagePerCol", ci);
            dgItems.Columns[11].HeaderText = rm.GetString("AgreementCommentsCtrl.Visible", ci);

            dgIndexes.Columns[1].HeaderText = rm.GetString("AgreementItemsCtrl.lblIndexLink", ci);
            dgIndexes.Columns[2].HeaderText = rm.GetString("AgreementCommentsCtrl.ItemNameNativeCol", ci);
            dgIndexes.Columns[3].HeaderText = rm.GetString("AgreementCommentsCtrl.ItemNameEnCol", ci);

            // COMMENTS
            // Labels...
            lblComment.Text = rm.GetString("AgreementCommentsCtrl.lblComment", ci);
            lblCommentsHeader.Text = rm.GetString("AgreementCommentsCtrl.lblCommentsHeader", ci);
            lblNotificationDate.Text = rm.GetString("AgreementCommentsCtrl.lblNotificationDate", ci);
            lblCommentNotificationEmail.Text = rm.GetString("lblNotificationEmail", ci);
            lblCommentFieldsHeader.Text = rm.GetString("AgreementCommentsCtrl.lblPageHeader", ci);
            lblSubscriber.Text = rm.GetString("AgreementCtrl.lblSubscriber", ci);

            // Buttons
            btnInsertComment.Text = rm.GetString("AgreementCommentsCtrl.btnInsertComment", ci);

            // Validators
            rfvCommentEmails.ErrorMessage = rm.GetString("AgreementCommentsCtrl.RequiredEmail", ci);
            rfvCommentNotifyDate.ErrorMessage = rm.GetString("AgreementCommentsCtrl.RequiredNotificationDate", ci);
            rfvComment.ErrorMessage = rm.GetString("AgreementCommentsCtrl.RequiredText", ci);
            NotificationEmailRegularExpression.ErrorMessage =
                rm.GetString("AgreementCommentsCtrl.NotificationEmailRegularExpression", ci);

            // Datagrid columns
            dgComments.Columns[3].HeaderText = rm.GetString("AgreementCommentsCtrl.CommentCol", ci);
            dgComments.Columns[4].HeaderText = rm.GetString("AgreementCommentsCtrl.ReminderDateCol", ci);
            dgComments.Columns[5].HeaderText = rm.GetString("AgreementCommentsCtrl.EmailCol", ci);
        }

        /// <summary>
        /// Sets all dropdown controls on the page for initial values.
        /// </summary>
        /// <remarks>
        /// Currency and subscriber dropdowns are set by fetching data from the database. 
        /// </remarks>
        private void SetDropDowns() {
            // Use cache...
            var myCurrencyList = (ArrayList) Cache["myCurrencyList"];

            // Currency...
            if (myCurrencyList == null || myCurrencyList.Count < 1) {
                myCurrencyList = BillingFactory.GetCurrencyCodesAsArrayList();
                Cache["myCurrencyList"] = myCurrencyList;
            }
            ddlCurrencyList.DataSource = myCurrencyList;
            ddlCurrencyList.DataBind();

            ddlSignedBy.DataSource = BillingFactory.GetEmployees();
            ddlSignedBy.DataValueField = "CreditInfoID";
            ddlSignedBy.DataTextField = EN ? "NameEN" : "NameNative";
            ddlSignedBy.DataBind();

            ddlSignedBy.Items.Insert(0, new ListItem(" ", "-1"));

            LoadDepartments(Agreement.SubscriberID);
        }

        /// <summary>
        /// Sets form according to default system settings
        /// </summary>
        /// <remarks>
        /// System settings are registered in the web.config file. They are nested elements 
        /// under the appSettings element. 
        /// </remarks>
        /// <param name="nFormType">Binary based. Controls which Forms are to be loaded with default data. Use constants COMMENT, ITEM, AGREEMENT</param>
        private void SetDefaults(int nFormType) {
            // TODO: Move default settings to a database table...?

            // COMMENTS
            if (nFormType >= COMMENT) {
                txtNotificationDate.Text = DateTime.Now.ToShortDateString();

                nFormType -= COMMENT;
            }

            // ITEMS
            if (nFormType >= ITEM) {
                // Notification email default
                txtItemNotificationEmails.Text = CigConfig.Configure("lookupsettings.defaultNotificatonEmail");
                // Offset default
                txtItemReminderOffset.Text = CigConfig.Configure("lookupsettings.defaultNotificationOffset");
                // So the dates follow what has been defined on the agreement...
                if (!Agreement.BeginDate.Equals(DateTime.MinValue)) {
                    txtItemBeginDate.Text = Agreement.BeginDate.ToShortDateString();
                }
                if (!Agreement.EndDate.Equals(DateTime.MinValue)) {
                    txtItemEndDate.Text = Agreement.EndDate.ToShortDateString();
                }

                nFormType -= ITEM;
            }

            // AGREEMENT
            if (nFormType >= AGREEMENT) {
                // Currency default
                ddlCurrencyList.SelectedValue = CigConfig.Configure("lookupsettings.defaultCurrency");
                // Notification email default
                txtNotificationEmails.Text = CigConfig.Configure("lookupsettings.defaultNotificatonEmail");
                // Offset default
                txtReminderOffset.Text = CigConfig.Configure("lookupsettings.defaultNotificationOffset");

                nFormType -= AGREEMENT;
            }
        }

        /// <summary>
        /// Update the current agreement with information registered by the user...
        /// </summary>
        private void UpdateTheAgreementDataset() {
            Agreement.AgreementNumber = txtAgreementID.Text;
            // Agreement.SubscriberID			= Convert.ToInt32(ddlSubscriberList.SelectedValue);
            Agreement.AgreementAmount = Convert.ToDecimal(txtAgreementPrice.Text);
            Agreement.GeneralDescription = txtGeneralDescription.Text;
            Agreement.BeginDate = Convert.ToDateTime(txtBeginDate.Text);
            Agreement.EndDate = Convert.ToDateTime(txtEndDate.Text);
            Agreement.Currency = ddlCurrencyList.SelectedValue;
            Agreement.Discountpercent = Convert.ToDecimal(txtAgreementDiscount.Text);
            Agreement.ReminderOffset = Convert.ToInt32(txtReminderOffset.Text);
            Agreement.NotificationEmails = txtNotificationEmails.Text;
            Agreement.SignerCIID = Convert.ToInt32(ddlSignedBy.SelectedItem.Value);

            // Date of insertion is set as the smallest available value upon creation of a new instance of
            // The AgreementBLLC class. We change this if if has not been done already...
            if (Agreement.Inserted == DateTime.MinValue) {
                Agreement.Inserted = DateTime.Now;
            }

            // The inserting user id is set to "-1" when a new instance of the AgreementBLLC class is created. If
            // nothing has hanged this then we should do it...
            if (Agreement.InsertedBy == -1) {
                Agreement.InsertedBy = Convert.ToInt32(Session["UserLoginID"]);
            }
        }

        /// <summary>
        /// save the current agreement to database
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnSave_Click(object sender, EventArgs e) { Store(true); }

        /// <summary>
        /// Writes Agreement to database.
        /// </summary>
        /// <param name="IsDataSetup">If new data should be loaded after saving</param>
        private void Store(bool IsDataSetup) {
            // Update the agreement in memory
            UpdateTheAgreementDataset();

            // Try to insert the agreement into the database...
            if (!MoveAgreementToDatabase()) {
                lblMessage.Text = rm.GetString("global.inserterrormsg", ci);
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
                return;
            }
            if (IsDataSetup) {
                SetupData();
                SetDefaults(ITEM + COMMENT);
            }
        }

        /// <summary>
        /// Cancel agreement operation and return to the FindCtrl.ascx user control.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnCancel_Click(object sender, EventArgs e) {
            // Clear "in memory" agreement...
            ReleaseInMemoryAgreement();
            // Transfer the user...
            Server.Transfer(@"TableUpdates.aspx?Type=def");
        }

        /// <summary>
        /// Insert the item...
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnInsertItem_Click(object sender, EventArgs e) {
            // Check is agreement and item is valid
            if (!IsItemValid()) {
                return;
            }

            Store(false);

            // Check for item selection. Some item must be selected.
            if (ddlItemList.SelectedValue == "-1") {
                lblMessage.Text = "Select item";
                lblMessage.Visible = true;
                lblMessage.ForeColor = Color.Red;
                return;
            }

            // Check for item selection. Some item must be selected.
            if (Convert.ToDateTime(txtBeginDate.Text) < Agreement.BeginDate ||
                Convert.ToDateTime(txtEndDate.Text) > Agreement.EndDate) {
                if (rm != null) {
                    lblMessage.Text = string.Format(
                        rm.GetString("AgreementItemsCtrl.InvalidDatesError", ci),
                        Agreement.BeginDate.ToShortDateString(),
                        Agreement.EndDate.ToShortDateString());
                }
                lblMessage.Visible = true;
                lblMessage.ForeColor = Color.Red;
                return;
            }

            // Check to see if any item is already in "update mode". This can be seen by checking the appropriate
            // Session variable...
            if (Session["agreementItem"] != null) {
                var theItem = (AgreementItemBLLC) Session["agreementItem"];

                // Set item values...
                theItem.ItemID = Convert.ToInt32(ddlItemList.SelectedValue);
                theItem.FixedAmount = Convert.ToDecimal(txtFixedAmount.Text);
                theItem.FixedDiscountPercent = Convert.ToDecimal(txtFixedDiscount.Text);
                theItem.MonthlyDiscountPercent = Convert.ToDecimal(txtMonthlyDiscount.Text);
                theItem.UsageDiscountPercent = Convert.ToDecimal(txtUsageDiscount.Text);
                theItem.CounterLinkID = Convert.ToInt32(ddlCounterLink.SelectedValue);
                theItem.IsVisible = chkVisible.Checked;
                theItem.BeginDate = Convert.ToDateTime(txtBeginDate.Text);
                theItem.EndDate = Convert.ToDateTime(txtEndDate.Text);
                theItem.ReminderOffset = Convert.ToInt32(txtReminderOffset.Text);
                theItem.NotificationEmails = txtNotificationEmails.Text;
//				theItem.NotificationEmail1		= txtNotificationEmail1.Text;
//				theItem.NotificationEmail2		= txtNotificationEmail2.Text;
                theItem.Freezone = Convert.ToInt32(txtFreezone.Text);

                // Clear indexes that are already registered on the item. They will reappear if they
                // have not been deleted from it.
                theItem.IndexesOnAgreementItems.Clear();

                // Add the indexes that are registered with the item. Well hurrah!
                if (dgIndexes.Items != null) {
                    foreach (DataGridItem Item in dgIndexes.Items) {
                        theItem.IndexesOnAgreementItems.Add(Item.Cells[1].Text);
                    }
                }

                // Clear department list and add new entries.
                theItem.DepartmentIds.Clear();
                foreach (ListItem department in lbxDepartmentsSelected.Items) {
                    theItem.DepartmentIds.Add(int.Parse(department.Value));
                }

                // Function call to update or insert the new item into the Items array 
                // for the agreement in memory...
                UpdateAgreementItemsArray(theItem);
                Session["agreementItem"] = null;
            } else {
                // Function call to get the correct agreement ID
                AgreementItemBLLC newItem = NewAgreementItem();

                // Set item values...
                newItem.ItemID = Convert.ToInt32(ddlItemList.SelectedValue);
                newItem.FixedAmount = Convert.ToDecimal(txtFixedAmount.Text);
                newItem.FixedDiscountPercent = Convert.ToDecimal(txtFixedDiscount.Text);
                newItem.MonthlyDiscountPercent = Convert.ToDecimal(txtMonthlyDiscount.Text);
                newItem.UsageDiscountPercent = Convert.ToDecimal(txtUsageDiscount.Text);
                newItem.CounterLinkID = Convert.ToInt32(ddlCounterLink.SelectedValue);
                newItem.IsVisible = chkVisible.Checked;
                newItem.BeginDate = Convert.ToDateTime(txtItemBeginDate.Text);
                newItem.EndDate = Convert.ToDateTime(txtItemEndDate.Text);
                newItem.ReminderOffset = Convert.ToInt32(txtItemReminderOffset.Text);
                newItem.NotificationEmails = txtItemNotificationEmails.Text;
//				newItem.NotificationEmail1		= txtItemNotificationEmail1.Text;
//				newItem.NotificationEmail2		= txtItemNotificationEmail2.Text;
                newItem.Freezone = Convert.ToInt32(txtFreezone.Text);

                // Add the indexes that are registered with the item. Well hurrah!
                if (dgIndexes.Items != null) {
                    foreach (DataGridItem theItem in dgIndexes.Items) {
                        newItem.IndexesOnAgreementItems.Add(theItem.Cells[1].Text);
                    }
                }

                // Clear department list and add new entries.
                newItem.DepartmentIds.Clear();
                foreach (ListItem department in lbxDepartmentsSelected.Items) {
                    newItem.DepartmentIds.Add(int.Parse(department.Value));
                }

                if (newItem.Inserted == DateTime.MinValue) {
                    newItem.Inserted = DateTime.Now;
                }

                if (newItem.InsertedBy == -1) {
                    newItem.InsertedBy = Convert.ToInt32(Session["UserLoginID"]);
                }

                // Function call to insert the new comment into the Comments array for the agreement in memory...
                UpdateAgreementItemsArray(newItem);
            }

            // Function call to update the items datagrid after the item is added to the agreement in memory...
            SetItemsDataGrid();
            CleanTheForm(ITEM);
        }

        /// <summary>
        /// Reset the form for data entry...
        /// </summary>
        /// <param name="nTypeForm">Binary based. Controls which Forms are to be emptied. Use constants COMMENT, ITEM, AGREEMENT</param>
        private void CleanTheForm(int nTypeForm) {
            // Clean the Comments form
            if (nTypeForm >= COMMENT) {
                txtCommentNotificationEmails.Text = "";
                txtComment.Text = "";

                nTypeForm -= COMMENT;
            }

            // Clean the Items form
            if (nTypeForm >= ITEM) {
                lblTheMonthlyAmount.Text = "0";
                lblTheUsageAmount.Text = "0";
                txtFixedAmount.Text = "0";
                txtFixedDiscount.Text = "0";
                txtMonthlyDiscount.Text = "0";
                txtUsageDiscount.Text = "0";
                txtFreezone.Text = "0";
                ddlItemList.SelectedIndex = 0;
                ddlCounterLink.SelectedIndex = 0;
                ddlIndexLink.SelectedIndex = 0;
                tblIndexesGrid.Visible = false;
                dgIndexes.DataBind();
                chkVisible.Checked = true;
                //cblDepartments.SelectedIndex	= -1;
                lbxDepartmentsSelected.Items.Clear();
                LoadDepartments(Agreement.SubscriberID);

                nTypeForm -= ITEM;
            }

            // Clean the Agreement form
            if (nTypeForm >= AGREEMENT) {
                txtBeginDate.Text = "";
                txtEndDate.Text = "";
                txtReminderOffset.Text = "";

                nTypeForm -= AGREEMENT;
            }

            // Get defaults
            SetDefaults(nTypeForm);
        }

        /// <summary>
        /// Populate the items datagrid with items that are linked to the current agreement
        /// </summary>
        private void SetItemsDataGrid() {
            DataTable dt = ReturnAgreementItemsArray();
            dgItems.DataSource = dt;
            dgItems.DataBind();
            dgItems.SelectedIndex = -1;

            tblItemsGrid.Visible = dt.Rows.Count >= 1;

            dt.Dispose();
        }

        /// <summary>
        /// Adds selected indexes to a dgIndexes datagrid.
        /// </summary>
        /// <remarks>
        /// Add to arraylist which will be used to save indexes that are linked to
        /// a certain agreement item...
        /// </remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnAddIndex_Click(object sender, EventArgs e) {
            // Add to arraylist which will be used to save indexes that are linked to
            // a certain item...
            var myIndexList = new ArrayList();

            // First we get the arraylist from the dgIndexes DataGrid...

            if (dgIndexes.Items != null) {
                foreach (DataGridItem theItem in dgIndexes.Items) {
                    myIndexList.Add(theItem.Cells[1].Text);
                }
            }

            if (myIndexList.Contains(ddlIndexLink.SelectedItem.ToString())) {
                return;
            }

            myIndexList.Add(ddlIndexLink.SelectedItem.ToString());

            // TODO: HACK
            var arrIndex = new ArrayList();
            foreach (string strIndex in myIndexList) {
                var index = new IndexOverviewBLLC {Name = strIndex};
                try {
                    index.DescEN = dgItems.SelectedItem.Cells[4].Text;
                    index.DescNative = dgItems.SelectedItem.Cells[3].Text;
                } catch (Exception) {}
                arrIndex.Add(index);
            }

            // Set arraylist as datarource for Index Datagrid...
            dgIndexes.DataSource = arrIndex;
            dgIndexes.DataBind();

            if (tblIndexesGrid.Visible == false) {
                tblIndexesGrid.Visible = true;
            }
        }

        private void SetupData() {
            // TODO: Fix this code please - This db call should not be necessary and is only made to
            // get "production ready" code erlier...
            var mySubscriberTable = BillingFactory.GetSingleSubscribersAsDataTable(Agreement.SubscriberID);
            lblSubscriberName.Text = mySubscriberTable.Rows[0]["NameNative"].ToString();
            mySubscriberTable.Dispose();

            // Use caching
            // ArrayList myIndexArrayList = (ArrayList)Cache["myIndexArrayList"];
            // DataTable myItemsTable = (DataTable)Cache["myItemsTable"];
            var myCounterLinkTable = (DataTable) Cache["myCounterLinkTable"];

            // Indexes...
            // if (myIndexArrayList == null)
            // {
            var myIndexArrayList = BillingFactory.GetActiveIndexesAsArrayList();
            // 	Cache["myIndexArrayList"] = myIndexArrayList;
            // }

            ddlIndexLink.DataSource = myIndexArrayList;
            ddlIndexLink.DataBind();

            // Items...
            // if (myItemsTable == null)
            // {
            var myItemsTable = BillingFactory.GetAllActiveRegistryItemsAsDataTable(Agreement.Currency);
            // Cache["myItemsTable"] = myItemsTable;
            // }
            ddlItemList.DataSource = myItemsTable;
            ddlItemList.DataValueField = "ID";

            // Display right language...
            ddlItemList.DataTextField = EN ? "ItemNameEN" : "ItemNameNative";

            // Add a value for "null" or blank...
            // Set it as "default" (.Selected = true)
            ddlItemList.DataBind();
            ddlItemList.Items.Insert(0, "...");
            ddlItemList.Items[0].Value = "-1";
            ddlItemList.Items[0].Selected = true;

            // Counter Links...
            if (myCounterLinkTable == null) {
                myCounterLinkTable = BillingFactory.GetCounterLinksAsDataTable();
                Cache["myCounterLinkTable"] = myCounterLinkTable;
            }
            ddlCounterLink.DataSource = myCounterLinkTable;
            ddlCounterLink.DataValueField = "ID";
            ddlCounterLink.DataTextField = "Name";

            // Add a value for "null" or blank...
            // Set it as "default" (.Selected = true)
            ddlCounterLink.DataBind();
            ddlCounterLink.Items.Insert(0, "...");
            ddlCounterLink.Items[0].Value = "-1";
            ddlCounterLink.Items[0].Selected = true;

            myItemsTable.Dispose();
            myCounterLinkTable.Dispose();
        }

        /// <summary>
        /// Change the selected item and the information that is linked to it.
        /// If no item is selected then we'd like to clear the form.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void ddlItemList_SelectedIndexChanged(object sender, EventArgs e) {
            // If no item is selected we'd like to clean the form and return...
            if (ddlItemList.SelectedValue == "-1") {
                return;
            }

            // DataTable myItemsTable = (DataTable)Cache["myItemsTable"];
            // Items...
            // if (myItemsTable == null)
            // {
            DataTable myItemsTable = BillingFactory.GetAllActiveRegistryItemsAsDataTable();
            // 	Cache["myItemsTable"] = myItemsTable;
            // }

            DataRow theItemRow = myItemsTable.Rows.Find(ddlItemList.SelectedValue);

            lblTheMonthlyAmount.Text = theItemRow["MonthlyAmount"].ToString();
            lblTheUsageAmount.Text = theItemRow["UsageAmount"].ToString();
            txtMonthlyDiscount.Text = theItemRow["MonthlyDiscountPercent"].ToString();
            txtUsageDiscount.Text = theItemRow["UsageDiscountPercent"].ToString();

            myItemsTable.Dispose();
        }

        /// <summary>
        /// Populate the comments datagrid with comments that are linked to the current agreement
        /// </summary>
        private void SetCommentsDataGrid() {
            // TODO: Fix this code please - This db call should not be necessary and is only made to
            // get "production ready" code erlier...
            DataTable mySubscriberTable = BillingFactory.GetSingleSubscribersAsDataTable(Agreement.SubscriberID);
            lblSubscriberName.Text = mySubscriberTable.Rows[0]["NameNative"].ToString();
            mySubscriberTable.Dispose();

            // Use AgreementGroundCtrl
            ArrayList arrComments = ReturnAgreementCommentsArray();
            dgComments.DataSource = arrComments;
            dgComments.DataBind();
            dgComments.SelectedIndex = -1;

            if (arrComments == null || arrComments.Count < 1) {
                tblCommentsGrid.Visible = false;
            } else {
                tblCommentsGrid.Visible = true;
            }
        }

        /// <summary>
        /// Insert a comment to the comments array for the current agreement
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnInsertComment_Click(object sender, EventArgs e) {
            // Check if agreement and comment is valid
            if (!IsCommentValid()) {
                return;
            }

            Store(false);

            // Check to see if any comment is already in "update mode". This can be seen by checking the appropriate
            // Session variable...
            if (Session["agreementComment"] != null) {
                var theComment = (AgreementCommentBLLC) Session["agreementComment"];

                // Set new comment values...
                theComment.Comment = txtComment.Text;
                theComment.ReminderDate = Convert.ToDateTime(txtNotificationDate.Text);
                theComment.NotificationEmails = txtCommentNotificationEmails.Text;

                // Function call to update or insert the new comment into the Comments array 
                // for the agreement in memory...
                UpdateAgreementCommentsArray(theComment);
                Session["agreementComment"] = null;
            } else {
                // Function call to get the correct agreement ID
                AgreementCommentBLLC newComment = NewAgreementComment();

                // Set comment values...
                newComment.Comment = txtComment.Text;
                newComment.ReminderDate = Convert.ToDateTime(txtNotificationDate.Text);
                newComment.NotificationEmails = txtCommentNotificationEmails.Text;

                if (newComment.Inserted == DateTime.MinValue) {
                    newComment.Inserted = DateTime.Now;
                }

                if (newComment.InsertedBy == -1) {
                    newComment.InsertedBy = Convert.ToInt32(Session["UserLoginID"]);
                }

                // Function call to insert the new comment into the Comments array for the agreement in memory...
                UpdateAgreementCommentsArray(newComment);
            }

            CleanTheForm(COMMENT);
            // Function call to update the comments datagrid after the comment is added to the agreement in memory...
            SetCommentsDataGrid();
        }

        /// <summary>
        /// Deletes one index form the datagrid and updates the array list.
        /// </summary>
        /// <remarks>Deletes one index form the datagrid and updates the array list.</remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgIndexes_DeleteCommand(object source, DataGridCommandEventArgs e) {
            // Take datagrid out of "edit" mode...
            dgIndexes.EditItemIndex = -1;

            // Add to arraylist which will be used to save indexes that are linked to
            // a certain item...
            var myIndexList = new ArrayList();

            // First we get the arraylist from the dgIndexes DataGrid...
            if (dgIndexes.Items != null) {
                foreach (DataGridItem theItem in dgIndexes.Items) {
                    myIndexList.Add(theItem.Cells[1].Text);
                }
            }

            myIndexList.Remove(e.Item.Cells[1].Text);

            // TODO: HACK
            var arrIndex = new ArrayList();
            foreach (string strIndex in myIndexList) {
                var index = new IndexOverviewBLLC {Name = strIndex};
                try {
                    index.DescEN = dgItems.SelectedItem.Cells[4].Text;
                    index.DescNative = dgItems.SelectedItem.Cells[3].Text;
                } catch (Exception) {}
                arrIndex.Add(index);
            }

            // Set arraylist as datarource for Index Datagrid...
            dgIndexes.DataSource = arrIndex;
            dgIndexes.DataBind();

            if (dgIndexes.Items.Count <= 0) {
                tblIndexesGrid.Visible = false;
            }
        }

        /// <summary>
        /// If the index is changed in the "items" datagrid, then we'll have to fetch correct values
        /// and set them up so the form will be acurate
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void dgItems_SelectedIndexChanged(object sender, EventArgs e) {
            if (Session["agreementItem"] != null && dgItems.SelectedIndex != -1) {
                if (Convert.ToInt32(dgItems.SelectedItem.Cells[12].Text) ==
                    ((AgreementItemBLLC) Session["agreementItem"]).Tag) {
                    dgItems.SelectedIndex = -1;
                    CleanTheForm(ITEM);
                    Session["agreementItem"] = null;
                    return;
                }
            }

            AgreementItemBLLC theItem = GetAgreementItem(Convert.ToInt32(dgItems.SelectedItem.Cells[12].Text));

            ddlItemList.SelectedValue = theItem.ItemID.ToString();
            txtFixedAmount.Text = theItem.FixedAmount.ToString();
            txtFixedDiscount.Text = theItem.FixedDiscountPercent.ToString();
            txtMonthlyDiscount.Text = theItem.MonthlyDiscountPercent.ToString();
            txtUsageDiscount.Text = theItem.UsageDiscountPercent.ToString();
            txtItemBeginDate.Text = theItem.BeginDate.ToShortDateString();
            txtItemEndDate.Text = theItem.EndDate.ToShortDateString();
            ddlCounterLink.SelectedValue = theItem.CounterLinkID.ToString();
            chkVisible.Checked = theItem.IsVisible;
            txtItemReminderOffset.Text = theItem.ReminderOffset.ToString();
            txtItemNotificationEmails.Text = theItem.NotificationEmails;
            txtFreezone.Text = theItem.Freezone.ToString();

            lbxDepartmentsSelected.Items.Clear();
            LoadDepartments(Agreement.SubscriberID);
            foreach (int DepartmentID in theItem.DepartmentIds) {
                ListItem item = lbxDepartmentsAvailable.Items.FindByValue(DepartmentID.ToString());
                lbxDepartmentsSelected.Items.Add(item);
                lbxDepartmentsAvailable.Items.Remove(item);
            }

            // Set arraylist as datarource for Index Datagrid...

            // TODO: HACK
            var arrIndex = new ArrayList();
            foreach (string strIndex in theItem.IndexesOnAgreementItems) {
                var index = new IndexOverviewBLLC {Name = strIndex};
                try {
                    index.DescEN = dgItems.SelectedItem.Cells[4].Text;
                    index.DescNative = dgItems.SelectedItem.Cells[3].Text;
                } catch (Exception) {}
                arrIndex.Add(index);
            }

            dgIndexes.DataSource = arrIndex;
            dgIndexes.DataBind();

            if (theItem.IndexesOnAgreementItems == null || theItem.IndexesOnAgreementItems.Count < 1) {
                tblIndexesGrid.Visible = false;
            } else {
                tblIndexesGrid.Visible = true;
            }

            var myItemsTable = (DataTable) Cache["myItemsTable"];
            // Items...
            if (myItemsTable == null) {
                myItemsTable = BillingFactory.GetAllActiveRegistryItemsAsDataTable();
                Cache["myItemsTable"] = myItemsTable;
            }

            DataRow theItemRow = myItemsTable.Rows.Find(ddlItemList.SelectedValue);

            lblTheMonthlyAmount.Text = theItemRow["MonthlyAmount"].ToString();
            lblTheUsageAmount.Text = theItemRow["UsageAmount"].ToString();
            myItemsTable.Dispose();

            Session["agreementItem"] = theItem;
        }

        /// <summary>
        /// Delete an item from the agreement items collection and refresh the datagrid
        /// </summary>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgItems_DeleteCommand(object source, DataGridCommandEventArgs e) {
            // Send in the tag!!!
            // The tag is used to store the location of the current line in the items collection on the agreement
            RemoveFromItemsList(Convert.ToInt32(e.Item.Cells[12].Text));
            SetItemsDataGrid();
        }

        /// <summary>
        /// Validates input for Item
        /// </summary>
        /// <returns>True/False</returns>
        private bool IsItemValid() {
            rfvItemBeginDate.Visible = true;
            rfvItemEndDate.Visible = true;

            if (lbxDepartmentsSelected.Items.Count <= 0) {
                lblMessage.Text = rm.GetString("lblErrorNoDepartmentSelected", ci);
                lblMessage.CssClass = "error_text";
                lblMessage.Visible = true;
                return false;
            }

            rfvItemBeginDate.Validate();
            rfvItemEndDate.Validate();
            if (rfvItemBeginDate.IsValid && rfvItemEndDate.IsValid) {
                rfvItemBeginDate.Visible = false;
                rfvItemEndDate.Visible = false;

                cpvItemBeginDate.Validate();
                cpvItemEndDate.Validate();
                cpvFixedAmount.Validate();
                cpvFixedDiscount.Validate();
                cpvItemReminderOffset.Validate();
                cpvMonthlyDiscount.Validate();
                cpvUsageDiscount.Validate();
                revItemNotifyEmails.Validate();

                if (cpvItemBeginDate.IsValid && cpvItemEndDate.IsValid && cpvFixedAmount.IsValid &&
                    cpvFixedDiscount.IsValid && cpvItemReminderOffset.IsValid && cpvMonthlyDiscount.IsValid &&
                    cpvUsageDiscount.IsValid && revItemNotifyEmails.IsValid) {
                    return true;
                } else {
                    return false;
                }
            } else {
                rfvItemBeginDate.Visible = false;
                rfvItemEndDate.Visible = false;

                return false;
            }
        }

        /// <summary>
        /// Validates input for Comment
        /// </summary>
        /// <returns>True/False</returns>
        private bool IsCommentValid() {
            rfvComment.Validate();
            rfvCommentEmails.Validate();
            rfvCommentNotifyDate.Validate();
            if (rfvComment.IsValid && rfvCommentEmails.IsValid && rfvCommentNotifyDate.IsValid) {
                revCommentNotifyEmails.Validate();
                if (revCommentNotifyEmails.IsValid) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        private void dgComments_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
            }

            WebDesign.CreateExplanationIcons(dgItems.Columns, lblDatagridIcons, rm, ci);
        }

        /// <summary>
        /// Remove a comment from the comments array of the current agreement.
        /// </summary>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgComments_DeleteCommand(object source, DataGridCommandEventArgs e) {
            RemoveFromCommentsList(Convert.ToInt32(e.Item.Cells[6].Text));
            SetCommentsDataGrid();
        }

        /// <summary>
        /// A comment is selected from the grid for updating on the current agreement
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void dgComments_SelectedIndexChanged(object sender, EventArgs e) {
            AgreementCommentBLLC theComment = GetAgreementComment(Convert.ToInt32(dgItems.SelectedItem.Cells[6].Text));

            txtComment.Text = theComment.Comment;
            txtCommentNotificationEmails.Text = theComment.NotificationEmails;
            txtNotificationDate.Text = theComment.ReminderDate.ToShortDateString();

            Session["agreementComment"] = theComment;
        }

        /// <summary>
        /// Connect "special" instance items to the rows (or specific sells) of the datagrid.
        /// In this case a javascript confirmation window is connected to the delete button of the grid.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridItemEventArgs</param>
        private void dgItems_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
            }

            WebDesign.CreateExplanationIcons(dgItems.Columns, lblDatagridIcons, rm, ci);
        }

        private void dgIndexes_ItemDataBound(object sender, DataGridItemEventArgs e) { WebDesign.CreateExplanationIcons(dgIndexes.Columns, lblIndexGridIcons, rm, ci); }

        private void LoadDepartments(int CIID) {
            var factory = new uaFactory();

            lbxDepartmentsAvailable.Items.Clear();

            lbxDepartmentsAvailable.DataSource = factory.GetDepartmentsAsDataSet(CIID);
            lbxDepartmentsAvailable.DataValueField = "DepartmentID";
            lbxDepartmentsAvailable.DataTextField = EN ? "NameEN" : "NameNative";
            lbxDepartmentsAvailable.DataBind();

            if (lbxDepartmentsAvailable.Items.Count == 1 && dgItems.SelectedIndex == -1) {
                btnDepartmentMoveAll_Click(null, null);
            }
        }

        private void btnDepartmentMoveAll_Click(object sender, EventArgs e) {
            var items = new ListItem[lbxDepartmentsAvailable.Items.Count];
            lbxDepartmentsAvailable.Items.CopyTo(items, 0);
            lbxDepartmentsSelected.Items.AddRange(items);
            lbxDepartmentsAvailable.Items.Clear();
        }

        private void btnDepartmentRemoveAll_Click(object sender, EventArgs e) {
            var items = new ListItem[lbxDepartmentsSelected.Items.Count];
            lbxDepartmentsSelected.Items.CopyTo(items, 0);
            lbxDepartmentsAvailable.Items.AddRange(items);
            lbxDepartmentsSelected.Items.Clear();
        }

        private void btnDepartmentMoveOne_Click(object sender, EventArgs e) {
            for (int i = 0; i < lbxDepartmentsAvailable.Items.Count; i++) {
                var item = lbxDepartmentsAvailable.Items[i];
                if (!item.Selected) {
                    continue;
                }
                lbxDepartmentsAvailable.Items.RemoveAt(i--);
                item.Selected = false;
                lbxDepartmentsSelected.Items.Add(item);
            }
        }

        private void btnDepartmentRemoveOne_Click(object sender, EventArgs e) {
            for (int i = 0; i < lbxDepartmentsSelected.Items.Count; i++) {
                ListItem item = lbxDepartmentsSelected.Items[i];
                if (item.Selected) {
                    lbxDepartmentsSelected.Items.RemoveAt(i--);
                    item.Selected = false;
                    lbxDepartmentsAvailable.Items.Add(item);
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ddlItemList.SelectedIndexChanged += new System.EventHandler(this.ddlItemList_SelectedIndexChanged);
            this.btnDepartmentMoveAll.Click += new System.EventHandler(this.btnDepartmentMoveAll_Click);
            this.btnDepartmentMoveOne.Click += new System.EventHandler(this.btnDepartmentMoveOne_Click);
            this.btnDepartmentRemoveOne.Click += new System.EventHandler(this.btnDepartmentRemoveOne_Click);
            this.btnDepartmentRemoveAll.Click += new System.EventHandler(this.btnDepartmentRemoveAll_Click);
            this.dgIndexes.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgIndexes_DeleteCommand);
            this.dgIndexes.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgIndexes_ItemDataBound);
            this.dgItems.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgItems_DeleteCommand);
            this.dgItems.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgItems_ItemDataBound);
            this.dgItems.SelectedIndexChanged += new System.EventHandler(this.dgItems_SelectedIndexChanged);
            this.btnAddIndex.Click += new System.EventHandler(this.btnAddIndex_Click);
            this.btnInsertItem.Click += new System.EventHandler(this.btnInsertItem_Click);
            this.dgComments.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgComments_DeleteCommand);
            this.dgComments.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgComments_ItemDataBound);
            this.dgComments.SelectedIndexChanged += new System.EventHandler(this.dgComments_SelectedIndexChanged);
            this.btnInsertComment.Click += new System.EventHandler(this.btnInsertComment_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}