#region

using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Billing.BLL;

#endregion

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    /// AgreementGroundCtrl is a base class for all the control classes that deal with agreements
    /// and is thus inherited by them. It is used to manipulate an instance of an agreement centrally
    /// along with operations that pertain to that agreement. It hides certain complexities from other
    /// agreement oriented controls that are derived from it.
    /// </summary>
    public class AgreementGroundCtrl : GroundCtrl {
        public AgreementBLLC Agreement { get; set; }

        /// <summary>
        /// Hides the session variable "billing_agreementLock" which is used to control if the agreement 
        /// controls are in view mode or insert / update mode. The session variable converts to a boolean. 
        /// </summary>
        public bool AgreementUnlock { get { return (bool) Session["billing_agreementUnlock"]; } set { Session["billing_agreementUnlock"] = value; } }

        /// <summary>
        /// Hides a session object for the agreement instance.
        /// </summary>
        protected void InitializeAgreement() {
            if (Session["billing_Agreement"] == null) {
                Agreement = new AgreementBLLC();
            } else {
                Agreement = (AgreementBLLC) Session["billing_Agreement"];
            }
        }

        /// <summary>
        /// Updates the hidden agreement session object with the agreement.
        /// </summary>
        protected void UpdateInMemoryAgreement() { Session["billing_Agreement"] = Agreement; }

        /// <summary>
        /// Sets the hidded agreement session object to a null value.
        /// </summary>
        protected void ReleaseInMemoryAgreement() {
            Session["agreementItem"] = null;
            Session["billing_Agreement"] = null;
        }

        /// <summary>
        /// This function is used to check if there is already an agreement resident in memory.
        /// </summary>
        /// <returns>True if there is an agreement in memory, false otherwise</returns>
        protected bool AlreadyAnAgreement() {
            return Session["billing_Agreement"] != null;
        }

        /// <summary>
        /// Check if this is a new agreement for a known subscriber (subscriber has already been
        /// set but this is a new agreement).
        /// </summary>
        /// <returns>True if this is a new agreement for a know subscriber, otherwise false.</returns>
        protected bool NewAgreementKnownSubscriber() {
            return Agreement.AgreementNumber == null;
        }

        /// <summary>
        /// Returns an array of the comments that are linked to a specific agreement.
        /// </summary>
        /// <returns>Array og comment objects.</returns>
        protected ArrayList ReturnAgreementCommentsArray() { return Agreement.CommentsOnAgreement; }

        /// <summary>
        /// Returns a datatable for the items that are linked to a specific agreement. 
        /// </summary>
        /// <remarks>
        /// Returns a datatable for the items that are linked to a specific agreement. The datatable combines the items
        /// array that is found on the "in memory" agreement (AgreementBLLC that is stored as a session object) and
        /// the Item Registry datatable that is stored in the SQL database. this implimentation allows for data viewing
        /// without the contract or contract items being stored in the database first. The data is thus combined in
        /// memory only. When an agreement is first saved it is inserted into the database in a single transaction so
        /// items that have been registered do not reside in the database but we'd still like a combined view of them
        /// with info from the registry items table.
        /// </remarks>
        /// <returns>Items on agreement Datatable</returns>
        protected DataTable ReturnAgreementItemsArray() {
            // Create two tables that hold the item information. One table that is supposed to hold the combined info
            // from the item registry and items that are linked to a specific agreement. Another table to hold the 
            // item registry table.
            var itemsTable = new DataTable("Items");

            itemsTable.Columns.Add("ID");
            itemsTable.Columns.Add("ItemNameNative");
            itemsTable.Columns.Add("ItemNameEN");
            itemsTable.Columns.Add("FixedAmount");
            itemsTable.Columns.Add("MonthlyAmount");
            itemsTable.Columns.Add("UsageAmount");
            itemsTable.Columns.Add("FixedDiscountPercent");
            itemsTable.Columns.Add("MonthlyDiscountPercent");
            itemsTable.Columns.Add("UsageDiscountPercent");
            itemsTable.Columns.Add("IsVisible");
            itemsTable.Columns.Add("Tag");

            DataTable itemRegistryTable = null; //(DataTable)Cache["myItemsTable"];

            // Cashing is used for the item registry table
            if (itemRegistryTable == null) {
                itemRegistryTable = BillingFactory.GetAllActiveRegistryItemsAsDataTable();
                Cache["myItemsTable"] = itemRegistryTable;
            }

            // We need a datarows array to hold the appropriate rows from the registy item table. Appropriate rows
            // are those that cohere to the item id of the current agreement item.

            // We need an item placeholder...
            AgreementItemBLLC theItem;

            // For each item that is linked to a specific agreement...
            for (int i = 0; i < Agreement.ItemsOnAgreement.Count; i++) {
                // Get the item
                theItem = (AgreementItemBLLC) Agreement.ItemsOnAgreement[i];
                // Select an item from the item registry that coheres to the agreement item
                DataRow[] selectedItemsArray = itemRegistryTable.Select(String.Format("id = {0}", theItem.ItemID));

                // Create a new row in the combining table that combines information from the registry and
                // the agreement.
                itemsTable.Rows.Add(
                    new[]
                    {
                        theItem.ItemID,
                        selectedItemsArray[0]["ItemNameNative"],
                        selectedItemsArray[0]["ItemNameEN"],
                        theItem.FixedAmount,
                        selectedItemsArray[0]["MonthlyAmount"],
                        selectedItemsArray[0]["UsageAmount"],
                        theItem.FixedDiscountPercent,
                        theItem.MonthlyDiscountPercent,
                        theItem.UsageDiscountPercent,
                        theItem.IsVisible,
                        theItem.Tag
                    });
            }

            return itemsTable;
        }

        /// <summary>
        /// Used to update the comments array for a specific agreement.
        /// </summary>
        /// <param name="theComment">AgreementCommentBLLC</param>
        protected void UpdateAgreementCommentsArray(AgreementCommentBLLC theComment) {
            // If the tag value of a comment is set to "-1" then it signifies a new comment entry. All other values
            // indicate an update, that is if they are positive...

            if (theComment.Tag < 0) {
                theComment.Tag = Agreement.CommentsOnAgreement.Add(theComment);
            }
                // If this is an update then we remove the old comment item and add the new, changed, comment item
            else {
                Agreement.CommentsOnAgreement.Insert(theComment.Tag, theComment);
                Agreement.CommentsOnAgreement.RemoveAt(theComment.Tag + 1);
            }
        }

        /// <summary>
        /// Removes a specific comment from the comments array of an agreement.
        /// </summary>
        /// <param name="index">The index of the comment to be removed.</param>
        protected void RemoveFromCommentsList(int index) {
            Agreement.CommentsOnAgreement.RemoveAt(index);

            for (int i = index; i < Agreement.CommentsOnAgreement.Count; i++) {
                GetAgreementComment(i).Tag -= 1;
            }
        }

        /// <summary>
        /// Removes a specific item from the items array of an agreement.
        /// </summary>
        /// <param name="index">The index of the item to be removed.</param>
        protected void RemoveFromItemsList(int index) {
            BillingFactory.DeleteAgreementItem(GetAgreementItem(index));
            Agreement.ItemsOnAgreement.RemoveAt(index);

            for (int i = index; i < Agreement.ItemsOnAgreement.Count; i++) {
                GetAgreementItem(i).Tag -= 1;
            }
        }

        /// <summary>
        /// Used to update the items array for a specific agreement.
        /// </summary>
        /// <param name="theItem">AgreementItemBLLC</param>
        protected void UpdateAgreementItemsArray(AgreementItemBLLC theItem) {
            // If the tag value of an item is set to "-1" then it signifies a new item entry. All other values
            // indicate an update, that is if they are positive...

            if (theItem.Tag < 0) {
                theItem.Tag = Agreement.ItemsOnAgreement.Add(theItem);
            }
                // If this is an update then we remove the old item and add the new, changed, item
            else {
                Agreement.ItemsOnAgreement.Insert(theItem.Tag, theItem);
                Agreement.ItemsOnAgreement.RemoveAt(theItem.Tag + 1);
            }
        }

        /// <summary>
        /// Adds a new comment to the comments array of an agreement.
        /// </summary>
        /// <returns>AgreementCommentBLLC object</returns>
        protected AgreementCommentBLLC NewAgreementComment() {
            var theComment = new AgreementCommentBLLC {AgreementID = Agreement.AgreementID, ID = 1};
            return theComment;
        }

        /// <summary>
        /// Adds a new item to the items array of an agreement.
        /// </summary>
        /// <returns>AgreementItemBLLC object</returns>
        protected AgreementItemBLLC NewAgreementItem() {
            var theItem = new AgreementItemBLLC {AgreementID = Agreement.AgreementID, ID = 1};
            return theItem;
        }

        /// <summary>
        /// Get a specific comment from the agreement comments array.
        /// </summary>
        /// <param name="index">Index of the specific comment.</param>
        /// <returns>AgreementCommentBLLC</returns>
        protected AgreementCommentBLLC GetAgreementComment(int index) { return ((AgreementCommentBLLC) Agreement.CommentsOnAgreement[index]); }

        /// <summary>
        /// Get a specific item from the agreement items array.
        /// </summary>
        /// <param name="index">Index of the specific item.</param>
        /// <returns>AgreementItemBLLC</returns>
        protected AgreementItemBLLC GetAgreementItem(int index) { return ((AgreementItemBLLC) Agreement.ItemsOnAgreement[index]); }

        /// <summary>
        /// Insert / Update the agreement into the database, along with all comments, items and indexes linked to it.
        /// </summary>
        /// <returns>True if successful, otherwise false.</returns>
        protected bool MoveAgreementToDatabase() {
            if (Agreement == null) {
                return false;
            }

            if (!AlreadyAnAgreement()) {
                return false;
            }

            // If this is a new agreement then the id should be equal to -1 since this is the value that is set
            // whenever a new instance of the agreement class is created...
            if (Agreement.AgreementID == -1) {
                // Set up agreement for insert...
                Agreement.Inserted = DateTime.Now;
                Agreement.InsertedBy = Convert.ToInt32(Session["UserLoginID"]);
                return BillingFactory.AddNewAgreement(Agreement);
            }
            // Set up agreement for update..
            Agreement.Updated = DateTime.Now;
            Agreement.UpdatedBy = Convert.ToInt32(Session["UserLoginID"]);
            return BillingFactory.UpdateAgreement(Agreement);
        }

        /// <summary>
        /// Locks or unlocks all the "input" controls of the sender (UserControl)
        /// </summary>
        /// <param name="isLock">true to lock the controls, false to unlock them</param>
        /// <param name="sender"></param>
        protected static void LockControls(bool isLock, object sender) {
            // Convert the sender object to a UserControl object
            var myControl = (UserControl) sender;

            // Loop through allt the controls of the UserControl
            foreach (Control control in myControl.Controls) {
                // If we find a textbox then we'll changed it's "enabled" status
                if (control.ToString() == "System.Web.UI.WebControls.TextBox") {
                    ((TextBox) control).Enabled = isLock;
                }

                // The same goes for dropdown lists
                if (control.ToString() == "System.Web.UI.WebControls.DropDownList") {
                    ((DropDownList) control).Enabled = isLock;
                }

                // The same goes for checkboxes
                if (control.ToString() == "System.Web.UI.WebControls.CheckBox") {
                    ((CheckBox) control).Enabled = isLock;
                }
            }
        }
    }
}