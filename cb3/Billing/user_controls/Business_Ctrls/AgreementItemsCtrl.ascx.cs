#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    ///		Summary description for AgreementItemsCtrl.
    /// </summary>
    public class AgreementItemsCtrl : AgreementGroundCtrl {
        protected Button btnAddIndex;
        protected Button btnCancel;
        protected Button btnInsertItem;
        protected Button btnSave;
        protected CheckBox chkVisible;
        protected CompareValidator CompareBeginDate;
        protected CompareValidator CompareEndDate;
        protected CompareValidator CompareFiexedDiscount;
        protected CompareValidator CompareFixedPrice;
        protected CompareValidator CompareMonthlyDiscount;
        protected CompareValidator CompareOffset;
        protected CompareValidator CompareUsageDiscount;
        protected DropDownList ddlCounterLink;
        protected DropDownList ddlIndexLink;
        protected DropDownList ddlItemList;
        protected DataGrid dgIndexes;
        protected DataGrid dgItems;
        protected Label Label1;
        protected Label Label2;
        protected Label Label3;
        protected Label Label4;
        protected Label Label5;
        protected Label Label6;
        protected Label Label7;
        protected Label lblAgreementItemsHeader;
        protected Label lblBeginDate;
        protected Label lblCounterLink;
        protected Label lblDatagridIcons;
        protected Label lblDeleteIcon;
        protected Label lblEndDate;
        protected Label lblFixedAmount;
        protected Label lblFixedDiscount;
        protected Label lblFreezone;
        protected Label lblIndexesGridHeader;
        protected Label lblIndexLink;
        protected Label lblIsVisible;
        protected Label lblItem;
        protected Label lblMessage;
        protected Label lblMonthlyAmount;
        protected Label lblMonthlyDiscount;
        protected Label lblNotificationEmail1;
        protected Label lblNotificationEmail2;
        protected Label lblPageHeader;
        protected Label lblReminderOffset;
        protected Label lblSubscriber;
        protected Label lblSubscriberName;
        protected Label lblTheMonthlyAmount;
        protected Label lblTheUsageAmount;
        protected Label lblUsageAmount;
        protected Label lblUsageDiscount;
        protected Button lnkAgreement;
        protected Button lnkComments;
        protected RegularExpressionValidator NotificationEmailRegularExpression;
        protected RegularExpressionValidator NotificationEmailRegularExpression2;
        protected HtmlTable outerGridTable;
        protected HtmlTableRow outerGridTableInfoLower;
        protected HtmlTable outerGridTableLower;
        protected RequiredFieldValidator requiredBeginDate;
        protected RequiredFieldValidator requiredEndDate;
        protected TextBox txtBeginDate;
        protected TextBox txtEndDate;
        protected TextBox txtFixedAmount;
        protected TextBox txtFixedDiscount;
        protected TextBox txtFreezone;
        protected TextBox txtMonthlyDiscount;
        protected TextBox txtNotificationEmail1;
        protected TextBox txtNotificationEmail2;
        protected TextBox txtReminderOffset;
        protected TextBox txtUsageDiscount;
        protected ValidationSummary ValidationSummary;

        private void Page_Load(object sender, EventArgs e) {
            PageLoad(sender, e);

            // Initialize the agreement data object so it is ready for use. Either set it as a "new" object
            // or fetch values from the session object. This function is handled by the parent class
            // AgreementGroundCtrl
            InitializeAgreement();

            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();
            LocalizeText();
            lblMessage.Visible = false;

            if (!IsPostBack) {
                outerGridTable.Visible = false;
                outerGridTableLower.Visible = false;

                SetupData();
                SetItemsDataGrid();
                SetDefaults();

                // Lock or unlock controls to conform to user selection for viewable or updateable agreements
                // Uses the LockControls function of the AgreementGroundControl (inherited)
                LockControls(AgreementUnlock, this);
                if (!AgreementUnlock) {
                    btnSave.Visible = false;
                    btnAddIndex.Visible = false;
                    btnInsertItem.Visible = false;
                } else {
                    btnSave.Visible = true;
                    btnAddIndex.Visible = true;
                    btnInsertItem.Visible = true;
                }
            }
        }

        /// <summary>
        /// Links an "enter event" to input controls
        /// </summary>
        /// <remarks>
        /// Links an "enter event" to all input controls so as to allow the user to press the
        /// enter key to submit a new item.
        /// </remarks>
        private void AddEnterEvent() {
            txtBeginDate.Attributes.Add("onkeypress", "checkEnterKey();");
            txtEndDate.Attributes.Add("onkeypress", "checkEnterKey();");
            txtFixedAmount.Attributes.Add("onkeypress", "checkEnterKey();");
            txtFixedDiscount.Attributes.Add("onkeypress", "checkEnterKey();");
            txtMonthlyDiscount.Attributes.Add("onkeypress", "checkEnterKey();");
            txtNotificationEmail1.Attributes.Add("onkeypress", "checkEnterKey();");
//			this.txtNotificationEmail2.Attributes.Add		("onkeypress", "checkEnterKey();");
            txtReminderOffset.Attributes.Add("onkeypress", "checkEnterKey();");
            txtUsageDiscount.Attributes.Add("onkeypress", "checkEnterKey();");
            txtFreezone.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        /// <summary>
        /// Populate the items datagrid with items that are linked to the current agreement
        /// </summary>
        private void SetItemsDataGrid() {
            DataTable dt = ReturnAgreementItemsArray();
            dgItems.DataSource = dt;
            dgItems.DataBind();
            dgItems.SelectedIndex = -1;

            outerGridTableLower.Visible = dt.Rows.Count >= 1;

            dt.Dispose();
        }

        /// <summary>
        /// For text localization. Text is set by using resource files that are XML based.
        /// </summary>
        protected override void LocalizeText() {
            base.LocalizeText();

            // Labels...
            lblAgreementItemsHeader.Text = rm.GetString("AgreementItemsCtrl.lblAgreementItemsHeader", ci);
            lblBeginDate.Text = rm.GetString("AgreementItemsCtrl.lblBeginDate", ci);
            lblCounterLink.Text = rm.GetString("AgreementItemsCtrl.lblCounterLink", ci);
            lblEndDate.Text = rm.GetString("AgreementItemsCtrl.lblEndDate", ci);
            lblFixedAmount.Text = rm.GetString("AgreementItemsCtrl.lblFixedAmount", ci);
            lblFixedDiscount.Text = rm.GetString("AgreementItemsCtrl.lblFixedDiscount", ci);
            lblIndexLink.Text = rm.GetString("AgreementItemsCtrl.lblIndexLink", ci);
            lblIsVisible.Text = rm.GetString("AgreementItemsCtrl.lblIsVisible", ci);
            lblItem.Text = rm.GetString("AgreementItemsCtrl.lblItem", ci);
            lblMonthlyAmount.Text = rm.GetString("AgreementItemsCtrl.lblMonthlyAmount", ci);
            lblMonthlyDiscount.Text = rm.GetString("AgreementItemsCtrl.lblMonthlyDiscount", ci);
            lblNotificationEmail1.Text = rm.GetString("lblNotificationEmail", ci);
            lblNotificationEmail2.Text = rm.GetString("AgreementItemsCtrl.lblNotificationEmail2", ci);
            lblReminderOffset.Text = rm.GetString("AgreementItemsCtrl.lblReminderOffset", ci);
            lblUsageAmount.Text = rm.GetString("AgreementItemsCtrl.lblUsageAmount", ci);
            lblUsageDiscount.Text = rm.GetString("AgreementItemsCtrl.lblUsageDiscount", ci);
            lblPageHeader.Text = rm.GetString("AgreementItemsCtrl.lblPageHeader", ci);
            lblFreezone.Text = rm.GetString("AgreementItemsCtrl.lblFreezone", ci);
            lblSubscriber.Text = rm.GetString("AgreementCtrl.lblSubscriber", ci);
            lblIndexesGridHeader.Text = rm.GetString("AgreementItemsCtrl.btnAddIndex", ci);

            // Links
            lnkComments.Text = rm.GetString("Agreement.lnkComments", ci);
            lnkAgreement.Text = rm.GetString("Agreement.lnkAgreement", ci);

            // Buttons
            btnCancel.Text = rm.GetString("Agreement.btnCancel", ci);
            btnSave.Text = rm.GetString("Agreement.btnSave", ci);
            btnAddIndex.Text = rm.GetString("AgreementItemsCtrl.btnAddIndex", ci);
            btnInsertItem.Text = rm.GetString("AgreementItemsCtrl.btnInsertItem", ci);

            // Validators
            requiredBeginDate.ErrorMessage = rm.GetString("AgreementItemsCtrl.requiredBeginDate", ci);
            requiredEndDate.ErrorMessage = rm.GetString("AgreementItemsCtrl.requiredEndDate", ci);
            CompareBeginDate.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareBeginDate", ci);
            CompareEndDate.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareEndDate", ci);
            CompareFiexedDiscount.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareFiexedDiscount", ci);
            CompareFixedPrice.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareFixedPrice", ci);
            CompareMonthlyDiscount.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareMonthlyDiscount", ci);
            CompareOffset.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareOffset", ci);
            CompareUsageDiscount.ErrorMessage = rm.GetString("AgreementItemsCtrl.CompareUsageDiscount", ci);
            NotificationEmailRegularExpression.ErrorMessage =
                rm.GetString("AgreementCommentsCtrl.NotificationEmailRegularExpression", ci);

            // Datagrid columns
            dgItems.Columns[3].HeaderText = rm.GetString("AgreementCommentsCtrl.ItemNameNativeCol", ci);
            dgItems.Columns[4].HeaderText = rm.GetString("AgreementCommentsCtrl.ItemNameEnCol", ci);
            dgItems.Columns[5].HeaderText = rm.GetString("AgreementCommentsCtrl.FixedCol", ci);
            dgItems.Columns[6].HeaderText = rm.GetString("AgreementCommentsCtrl.FixedPerCol", ci);
            dgItems.Columns[7].HeaderText = rm.GetString("AgreementCommentsCtrl.MonthlyCol", ci);
            dgItems.Columns[8].HeaderText = rm.GetString("AgreementCommentsCtrl.MonthlyPerCol", ci);
            dgItems.Columns[9].HeaderText = rm.GetString("AgreementCommentsCtrl.UsageCol", ci);
            dgItems.Columns[10].HeaderText = rm.GetString("AgreementCommentsCtrl.UsagePerCol", ci);
            dgItems.Columns[11].HeaderText = rm.GetString("AgreementCommentsCtrl.Visible", ci);

            //
            lblDeleteIcon.Text = rm.GetString("txtDelete", ci);
        }

        private void SetupData() {
            // TODO: Fix this code please - This db call should not be necessary and is only made to
            // get "production ready" code erlier...
            var mySubscriberTable = BillingFactory.GetSingleSubscribersAsDataTable(Agreement.SubscriberID);
            lblSubscriberName.Text = mySubscriberTable.Rows[0]["NameNative"].ToString();
            mySubscriberTable.Dispose();

            // Use caching
            // ArrayList myIndexArrayList = (ArrayList)Cache["myIndexArrayList"];
            // DataTable myItemsTable = (DataTable)Cache["myItemsTable"];
            var myCounterLinkTable = (DataTable) Cache["myCounterLinkTable"];

            // Indexes...
            // if (myIndexArrayList == null)
            // {
            var myIndexArrayList = BillingFactory.GetActiveIndexesAsArrayList();
            // 	Cache["myIndexArrayList"] = myIndexArrayList;
            // }

            ddlIndexLink.DataSource = myIndexArrayList;
            ddlIndexLink.DataBind();

            // Items...
            // if (myItemsTable == null)
            // {
            var myItemsTable = BillingFactory.GetAllActiveRegistryItemsAsDataTable(Agreement.Currency);
            // Cache["myItemsTable"] = myItemsTable;
            // }
            ddlItemList.DataSource = myItemsTable;
            ddlItemList.DataValueField = "ID";

            // Display right language...
            ddlItemList.DataTextField = EN ? "ItemNameEN" : "ItemNameNative";

            // Add a value for "null" or blank...
            // Set it as "default" (.Selected = true)
            ddlItemList.DataBind();
            ddlItemList.Items.Insert(0, "...");
            ddlItemList.Items[0].Value = "-1";
            ddlItemList.Items[0].Selected = true;

            // Counter Links...
            if (myCounterLinkTable == null) {
                myCounterLinkTable = BillingFactory.GetCounterLinksAsDataTable();
                Cache["myCounterLinkTable"] = myCounterLinkTable;
            }
            ddlCounterLink.DataSource = myCounterLinkTable;
            ddlCounterLink.DataValueField = "ID";
            ddlCounterLink.DataTextField = "Name";

            // Add a value for "null" or blank...
            // Set it as "default" (.Selected = true)
            ddlCounterLink.DataBind();
            ddlCounterLink.Items.Insert(0, "...");
            ddlCounterLink.Items[0].Value = "-1";
            ddlCounterLink.Items[0].Selected = true;

            myItemsTable.Dispose();
            myCounterLinkTable.Dispose();
        }

        /// <summary>
        /// Change the selected item and the information that is linked to it.
        /// If no item is selected then we'd like to clear the form.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void ddlItemList_SelectedIndexChanged(object sender, EventArgs e) {
            // If no item is selected we'd like to clean the form and return...
            if (ddlItemList.SelectedValue == "-1") {
                return;
            }

            // DataTable myItemsTable = (DataTable)Cache["myItemsTable"];
            // Items...
            // if (myItemsTable == null)
            // {
            DataTable myItemsTable = BillingFactory.GetAllActiveRegistryItemsAsDataTable();
            // 	Cache["myItemsTable"] = myItemsTable;
            // }

            DataRow theItemRow = myItemsTable.Rows.Find(ddlItemList.SelectedValue);

            lblTheMonthlyAmount.Text = theItemRow["MonthlyAmount"].ToString();
            lblTheUsageAmount.Text = theItemRow["UsageAmount"].ToString();
            txtMonthlyDiscount.Text = theItemRow["MonthlyDiscountPercent"].ToString();
            txtUsageDiscount.Text = theItemRow["UsageDiscountPercent"].ToString();

            myItemsTable.Dispose();
        }

        /// <summary>
        /// Adds selected indexes to a dgIndexes datagrid.
        /// </summary>
        /// <remarks>
        /// Add to arraylist which will be used to save indexes that are linked to
        /// a certain agreement item...
        /// </remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnAddIndex_Click(object sender, EventArgs e) {
            // Add to arraylist which will be used to save indexes that are linked to
            // a certain item...
            var myIndexList = new ArrayList();

            // First we get the arraylist from the dgIndexes DataGrid...

            if (dgIndexes.Items != null) {
                foreach (DataGridItem theItem in dgIndexes.Items) {
                    myIndexList.Add(theItem.Cells[1].Text);
                }
            }

            if (myIndexList.Contains(ddlIndexLink.SelectedItem.ToString())) {
                return;
            }

            myIndexList.Add(ddlIndexLink.SelectedItem.ToString());

            // Set arraylist as datarource for Index Datagrid...
            dgIndexes.DataSource = myIndexList;
            dgIndexes.DataBind();

            if (outerGridTable.Visible == false) {
                outerGridTable.Visible = true;
            }
        }

        /// <summary>
        /// Deletes one index form the datagrid and updates the array list.
        /// </summary>
        /// <remarks>Deletes one index form the datagrid and updates the array list.</remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgIndexes_DeleteCommand(object source, DataGridCommandEventArgs e) {
            // Take datagrid out of "edit" mode...
            dgIndexes.EditItemIndex = -1;

            // Add to arraylist which will be used to save indexes that are linked to
            // a certain item...
            var myIndexList = new ArrayList();

            // First we get the arraylist from the dgIndexes DataGrid...

            if (dgIndexes.Items != null) {
                foreach (DataGridItem theItem in dgIndexes.Items) {
                    myIndexList.Add(theItem.Cells[1].Text);
                }
            }

            myIndexList.Remove(e.Item.Cells[1].Text);

            // Set arraylist as datarource for Index Datagrid...
            dgIndexes.DataSource = myIndexList;
            dgIndexes.DataBind();

            if (dgIndexes.Items.Count <= 0) {
                outerGridTable.Visible = false;
            }
        }

        /// <summary>
        /// Insert the item...
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnInsertItem_Click(object sender, EventArgs e) {
            // Check for item selection. Some item must be selected.
            if (ddlItemList.SelectedValue == "-1") {
                lblMessage.Text = "Select item";
                lblMessage.Visible = true;
                lblMessage.ForeColor = Color.Red;
                return;
            }

            // Check for item selection. Some item must be selected.
            if (Convert.ToDateTime(txtBeginDate.Text) < Agreement.BeginDate ||
                Convert.ToDateTime(txtEndDate.Text) > Agreement.EndDate) {
                lblMessage.Text = string.Format(
                    rm.GetString("AgreementItemsCtrl.InvalidDatesError", ci),
                    Agreement.BeginDate.ToShortDateString(),
                    Agreement.EndDate.ToShortDateString());
                lblMessage.Visible = true;
                lblMessage.ForeColor = Color.Red;
                return;
            }

            // Check to see if any item is already in "update mode". This can be seen by checking the appropriate
            // Session variable...
            if (Session["agreementItem"] != null) {
                var theItem = (AgreementItemBLLC) Session["agreementItem"];

                // Set item values...
                theItem.ItemID = Convert.ToInt32(ddlItemList.SelectedValue);
                theItem.FixedAmount = Convert.ToDecimal(txtFixedAmount.Text);
                theItem.FixedDiscountPercent = Convert.ToDecimal(txtFixedDiscount.Text);
                theItem.MonthlyDiscountPercent = Convert.ToDecimal(txtMonthlyDiscount.Text);
                theItem.UsageDiscountPercent = Convert.ToDecimal(txtUsageDiscount.Text);
                theItem.CounterLinkID = Convert.ToInt32(ddlCounterLink.SelectedValue);
                theItem.IsVisible = chkVisible.Checked;
                theItem.BeginDate = Convert.ToDateTime(txtBeginDate.Text);
                theItem.EndDate = Convert.ToDateTime(txtEndDate.Text);
                theItem.ReminderOffset = Convert.ToInt32(txtReminderOffset.Text);
                theItem.NotificationEmails = txtNotificationEmail1.Text;
//				theItem.NotificationEmail1		= txtNotificationEmail1.Text;
//				theItem.NotificationEmail2		= txtNotificationEmail2.Text;
                theItem.Freezone = Convert.ToInt32(txtFreezone.Text);

                // Clear indexes that are already registered on the item. They will reappear if they
                // have not been deleted from it.
                theItem.IndexesOnAgreementItems.Clear();

                // Add the indexes that are registered with the item. Well hurrah!
                if (dgIndexes.Items != null) {
                    foreach (DataGridItem Item in dgIndexes.Items) {
                        theItem.IndexesOnAgreementItems.Add(Item.Cells[1].Text);
                    }
                }

                // Function call to update or insert the new item into the Items array 
                // for the agreement in memory...
                UpdateAgreementItemsArray(theItem);
                Session["agreementItem"] = null;
            } else {
                // Function call to get the correct agreement ID
                AgreementItemBLLC newItem = NewAgreementItem();

                // Set item values...
                newItem.ItemID = Convert.ToInt32(ddlItemList.SelectedValue);
                newItem.FixedAmount = Convert.ToDecimal(txtFixedAmount.Text);
                newItem.FixedDiscountPercent = Convert.ToDecimal(txtFixedDiscount.Text);
                newItem.MonthlyDiscountPercent = Convert.ToDecimal(txtMonthlyDiscount.Text);
                newItem.UsageDiscountPercent = Convert.ToDecimal(txtUsageDiscount.Text);
                newItem.CounterLinkID = Convert.ToInt32(ddlCounterLink.SelectedValue);
                newItem.IsVisible = chkVisible.Checked;
                newItem.BeginDate = Convert.ToDateTime(txtBeginDate.Text);
                newItem.EndDate = Convert.ToDateTime(txtEndDate.Text);
                newItem.ReminderOffset = Convert.ToInt32(txtReminderOffset.Text);
                newItem.NotificationEmails = txtNotificationEmail1.Text;
//				newItem.NotificationEmail1		= txtNotificationEmail1.Text;
//				newItem.NotificationEmail2		= txtNotificationEmail2.Text;
                newItem.Freezone = Convert.ToInt32(txtFreezone.Text);

                // Add the indexes that are registered with the item. Well hurrah!
                if (dgIndexes.Items != null) {
                    foreach (DataGridItem theItem in dgIndexes.Items) {
                        newItem.IndexesOnAgreementItems.Add(theItem.Cells[1].Text);
                    }
                }

                if (newItem.Inserted == DateTime.MinValue) {
                    newItem.Inserted = DateTime.Now;
                }

                if (newItem.InsertedBy == -1) {
                    newItem.InsertedBy = Convert.ToInt32(Session["UserLoginID"]);
                }

                // Function call to insert the new comment into the Comments array for the agreement in memory...
                UpdateAgreementItemsArray(newItem);
            }

            // Function call to update the items datagrid after the item is added to the agreement in memory...
            SetItemsDataGrid();
            CleanTheForm();
        }

        /// <summary>
        /// Sets form according to default system settings
        /// </summary>
        /// <remarks>
        /// System settings are registered in the web.config file. They are nested elements 
        /// under the appSettings element. 
        /// </remarks>
        private void SetDefaults() {
            // Notification email default
            txtNotificationEmail1.Text = CigConfig.Configure("lookupsettings.defaultNotificatonEmail");

            // Offset default
            txtReminderOffset.Text = CigConfig.Configure("lookupsettings.defaultNotificationOffset");

            // So the dates follow what has been defined on the agreement...
            txtBeginDate.Text = Agreement.BeginDate.ToShortDateString();
            txtEndDate.Text = Agreement.EndDate.ToShortDateString();
        }

        /// <summary>
        /// Go to the agreement main overview
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void lnkAgreement_Click(object sender, EventArgs e) {
            Session["agreementItem"] = null;
            Response.Redirect(@"TableUpdates.aspx?Type=Agreement");
        }

        /// <summary>
        /// Go to agreement comments
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void lnkComments_Click(object sender, EventArgs e) {
            Session["agreementItem"] = null;
            Response.Redirect(@"TableUpdates.aspx?Type=AgreementComments");
        }

        /// <summary>
        /// If the index is changed in the "items" datagrid, then we'll have to fetch correct values
        /// and set them up so the form will be acurate
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void dgItems_SelectedIndexChanged(object sender, EventArgs e) {
            if (Session["agreementItem"] != null && dgItems.SelectedIndex != -1) {
                if (Convert.ToInt32(dgItems.SelectedItem.Cells[12].Text) ==
                    ((AgreementItemBLLC) Session["agreementItem"]).Tag) {
                    dgItems.SelectedIndex = -1;
                    CleanTheForm();
                    Session["agreementItem"] = null;
                    return;
                }
            }

            AgreementItemBLLC theItem = GetAgreementItem(Convert.ToInt32(dgItems.SelectedItem.Cells[12].Text));

            ddlItemList.SelectedValue = theItem.ItemID.ToString();
            txtFixedAmount.Text = theItem.FixedAmount.ToString();
            txtFixedDiscount.Text = theItem.FixedDiscountPercent.ToString();
            txtMonthlyDiscount.Text = theItem.MonthlyDiscountPercent.ToString();
            txtUsageDiscount.Text = theItem.UsageDiscountPercent.ToString();
            txtBeginDate.Text = theItem.BeginDate.ToShortDateString();
            txtEndDate.Text = theItem.EndDate.ToShortDateString();
            ddlCounterLink.SelectedValue = theItem.CounterLinkID.ToString();
            chkVisible.Checked = theItem.IsVisible;
            txtReminderOffset.Text = theItem.ReminderOffset.ToString();
            txtNotificationEmail1.Text = theItem.NotificationEmails;
//			txtNotificationEmail1.Text		= theItem.NotificationEmail1;
//			txtNotificationEmail2.Text		= theItem.NotificationEmail2;
            txtFreezone.Text = theItem.Freezone.ToString();

            // Set arraylist as datarource for Index Datagrid...
            dgIndexes.DataSource = theItem.IndexesOnAgreementItems;
            dgIndexes.DataBind();

            if (theItem.IndexesOnAgreementItems == null || theItem.IndexesOnAgreementItems.Count < 1) {
                outerGridTable.Visible = false;
            } else {
                outerGridTable.Visible = true;
            }

            var myItemsTable = (DataTable) Cache["myItemsTable"];
            // Items...
            if (myItemsTable == null) {
                myItemsTable = BillingFactory.GetAllActiveRegistryItemsAsDataTable();
                Cache["myItemsTable"] = myItemsTable;
            }

            DataRow theItemRow = myItemsTable.Rows.Find(ddlItemList.SelectedValue);

            lblTheMonthlyAmount.Text = theItemRow["MonthlyAmount"].ToString();
            lblTheUsageAmount.Text = theItemRow["UsageAmount"].ToString();
            myItemsTable.Dispose();

            Session["agreementItem"] = theItem;
        }

        /// <summary>
        /// Delete an item from the agreement items collection and refresh the datagrid
        /// </summary>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgItems_DeleteCommand(object source, DataGridCommandEventArgs e) {
            // Send in the tag!!!
            // The tag is used to store the location of the current line in the items collection on the agreement
            RemoveFromItemsList(Convert.ToInt32(e.Item.Cells[12].Text));
            SetItemsDataGrid();
        }

        /// <summary>
        /// Try to save the agreement, with items and comments, to the database...
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnSave_Click(object sender, EventArgs e) {
            if (!MoveAgreementToDatabase()) {
                lblMessage.Text = rm.GetString("global.inserterrormsg", ci);
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
                return;
            }
        }

        /// <summary>
        /// Connect "special" instance items to the rows (or specific sells) of the datagrid.
        /// In this case a javascript confirmation window is connected to the delete button of the grid.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridItemEventArgs</param>
        private void dgItems_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
            }

            WebDesign.CreateExplanationIcons(dgItems.Columns, lblDatagridIcons, rm, ci);
        }

        /// <summary>
        /// Reset the form for data entry...
        /// </summary>
        private void CleanTheForm() {
            lblTheMonthlyAmount.Text = "0";
            lblTheUsageAmount.Text = "0";

            txtBeginDate.Text = "";
            txtEndDate.Text = "";
            txtFixedAmount.Text = "0";
            txtFixedDiscount.Text = "0";
            txtMonthlyDiscount.Text = "0";
            txtNotificationEmail1.Text = "";
//			txtNotificationEmail2.Text		= "";
            txtReminderOffset.Text = "";
            txtUsageDiscount.Text = "0";
            txtFreezone.Text = "0";
            chkVisible.Checked = true;
            ddlItemList.SelectedIndex = 0;
            ddlCounterLink.SelectedIndex = 0;
            ddlIndexLink.SelectedIndex = 0;
            outerGridTable.Visible = false;
            dgIndexes.DataBind();

            // Get defaults
            SetDefaults();
        }

        /// <summary>
        /// Cancel agreement operation and return to the FindCtrl.ascx user control.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnCancel_Click(object sender, EventArgs e) {
            // Clear "in memory" agreement...
            ReleaseInMemoryAgreement();
            // Transfer the user...
            Server.Transfer(@"TableUpdates.aspx?Type=def");
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ddlItemList.SelectedIndexChanged += new System.EventHandler(this.ddlItemList_SelectedIndexChanged);
            this.dgIndexes.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgIndexes_DeleteCommand);
            this.dgItems.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgItems_DeleteCommand);
            this.dgItems.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgItems_ItemDataBound);
            this.dgItems.SelectedIndexChanged += new System.EventHandler(this.dgItems_SelectedIndexChanged);
            this.btnAddIndex.Click += new System.EventHandler(this.btnAddIndex_Click);
            this.btnInsertItem.Click += new System.EventHandler(this.btnInsertItem_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.lnkComments.Click += new System.EventHandler(this.lnkComments_Click);
            this.lnkAgreement.Click += new System.EventHandler(this.lnkAgreement_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}