<%@ Control Language="c#" AutoEventWireup="false" Codebehind="AgreementCommentsCtrl.ascx.cs" Inherits="Billing.user_controls.AgreementCommentsCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" src="CheckEnterKey.js"></script>
<script language="JavaScript" src="DatePicker.js"></script>
<script language="javascript"> 
	function checkEnterKey(path) 
	{    
		if (event.keyCode == 13) 
		{        
			event.cancelBubble = true;
			event.returnValue = false;
			document.BillingMain.AgreementCommentsCtrl1_btnInsertComment.click(); 
		}
	} 
	
	function SetFormFocus()
	{
		if (document.BillingMain.AgreementCommentsCtrl1_txtComment.disabled == false)
		{
			document.BillingMain.AgreementCommentsCtrl1_txtComment.focus();
		}
	}
</script>
<body onload="SetFormFocus()">
	<form id="Agreement" method="post">
		<table width="100%">
			<tr>
				<td>
					<table class="grid_table" cellspacing="0" cellpadding="0">
						<tr>
							<th>
								<asp:label id="lblPageHeader" cssclass="HeadMain" runat="server"></asp:label></th></tr>
						<tr>
							<td>
								<table class="fields" id="tblMainRegion" cellspacing="0" cellpadding="0">
									<tr>
										<td colspan="2"><asp:label id="lblSubscriber" runat="server"></asp:label>&nbsp;-&nbsp;
											<asp:label id="lblSubscriberName" runat="server"></asp:label></td>
									</tr>
									<tr>
										<td width="50%"><asp:label id="lblNotificationDate" runat="server"></asp:label><br>
											<asp:textbox id="txtNotificationDate" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('AgreementCommentsCtrl1_txtNotificationDate', 250, 250);"
												type="button" value="...">
											<asp:label id="Label1" cssclass="error_text" runat="server">*</asp:label><asp:requiredfieldvalidator id="RequiredNotificationDate" runat="server" display="None" errormessage="RequiredNotificationDate"
												controltovalidate="txtNotificationDate">*</asp:requiredfieldvalidator></td>
										<td><asp:label id="lblNotificationEmail" runat="server"></asp:label><br>
											<asp:textbox id="txtNotificationEmail" runat="server" width="91%" maxlength="128"></asp:textbox><asp:label id="Label4" cssclass="error_text" runat="server">*</asp:label><asp:regularexpressionvalidator id="NotificationEmailRegularExpression" runat="server" display="None" errormessage="NotificationEmailRegularExpression"
												controltovalidate="txtNotificationEmail" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\;([ ])*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*">*</asp:regularexpressionvalidator><asp:requiredfieldvalidator id="RequiredEmail" runat="server" display="None" errormessage="RequiredEmail" controltovalidate="txtNotificationEmail">*</asp:requiredfieldvalidator></td>
									</tr>
									<tr>
										<td colspan="2"><asp:label id="lblComment" runat="server"></asp:label><asp:label id="Label2" cssclass="error_text" runat="server">*</asp:label><asp:requiredfieldvalidator id="RequiredText" runat="server" display="None" errormessage="RequiredText" controltovalidate="txtComment">*</asp:requiredfieldvalidator><br>
											<asp:textbox id="txtComment" runat="server" width="100%" maxlength="1024" height="72px" textmode="MultiLine"></asp:textbox></td>
									</tr>
									<tr>
										<td height="23"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblCommentsHeader" runat="server"></asp:label></th></tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td><asp:datagrid id="dgItems" runat="server" width="100%" gridlines="None" autogeneratecolumns="False"
												cellpadding="4">
												<footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<columns>
													<asp:buttoncolumn text="&lt;img src=&quot;../img/view.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
														commandname="Select">
														<itemstyle cssclass="leftpadding"></itemstyle>
													</asp:buttoncolumn>
													<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
														commandname="Delete">
														<itemstyle cssclass="nopadding"></itemstyle>
													</asp:buttoncolumn>
													<asp:boundcolumn visible="False" datafield="ID" headertext="ID">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="Comment" sortexpression="Comment" headertext="Comment">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="ReminderDate" sortexpression="ReminderDate" headertext="ReminderDate"
														dataformatstring="{0:d}">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="NotificationEmails" sortexpression="NotificationEmail" headertext="NotificationEmail">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn visible="False" datafield="Tag">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
												</columns>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
											</asp:datagrid></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td>
					<table class="empty_table" cellspacing="0">
						<tr>
							<td align="left" rowspan="3"><asp:validationsummary id="ValidationSummary" runat="server" width="100%" displaymode="List"></asp:validationsummary><asp:label id="lblMessage" runat="server" visible="False"></asp:label></td>
							<td align="right"><asp:button id="btnInsertComment" cssclass="confirm_button" runat="server" text="Insert"></asp:button></td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<tr>
							<td align="right"><asp:button id="btnCancel" cssclass="cancel_button" runat="server" causesvalidation="False"></asp:button><asp:button id="lnkItems" cssclass="gray_button" runat="server" causesvalidation="False"></asp:button><asp:button id="lnkAgreement" cssclass="gray_button" runat="server" causesvalidation="False"></asp:button><asp:button id="btnSave" cssclass="confirm_button" runat="server"></asp:button></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
