<%@ Control Language="c#" AutoEventWireup="false" Codebehind="IndexOverviewCtrl.ascx.cs" Inherits="Billing.user_controls.Business_Ctrls.IndexOverviewCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<form id="IndexOverView" method="post">
	<table width="100%">
		<tr>
			<td>
				<table class="grid_table" cellspacing="0" cellpadding="0">
					<tr>
						<th>
							<asp:label id="lblPageHeader" runat="server"></asp:label></th></tr>
					<tr>
						<td>
							<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
								<tr>
									<td style="WIDTH: 25%"><asp:label id="lblIndexName" runat="server"></asp:label><br>
										<asp:textbox id="txtIndexName" runat="server" maxlength="20"></asp:textbox><asp:requiredfieldvalidator id="validateIndexName" runat="server" cssclass="subject" errormessage="IndexName"
											controltovalidate="txtIndexName" visible="True">*</asp:requiredfieldvalidator></td>
									<td style="WIDTH: 25%"><asp:label id="lblDescNative" runat="server"></asp:label><br>
										<asp:textbox id="txtDescNative" runat="server" maxlength="255"></asp:textbox></td>
									<td style="WIDTH: 25%"><asp:label id="lblDescEN" runat="server"></asp:label><br>
										<asp:textbox id="txtDescEN" runat="server" maxlength="255"></asp:textbox></td>
									<td><asp:label id="lblIndexisOpen" runat="server"></asp:label><br>
										<asp:checkbox id="chkIsIndexOpen" runat="server" checked="True" cssclass="radio"></asp:checkbox></td>
								</tr>
								<tr>
									<td height="23"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="10"></td>
		</tr>
		<tr>
			<td>
				<table class="empty_table" cellspacing="0">
					<tr valign="top">
						<td align="left">
							<asp:label id="lblMessage" runat="server"></asp:label>
							<br>
							<asp:validationsummary id="ValidationSummary" runat="server" width="100%"></asp:validationsummary>
						</td>
						<td align="right">
							<asp:button id="btnInsert" runat="server" cssclass="confirm_button"></asp:button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table class="grid_table" cellspacing="0" cellpadding="0" id="outerGridTable" runat="server">
					<tr>
						<td height="10"></td>
					</tr>
					<tr>
						<td align="right">
							<asp:label id="lblDatagridIcons" runat="server"></asp:label>
						</td>
					</tr>
					<tr>
						<td height="5"></td>
					</tr>
					<tr>
						<th>
							<asp:label id="lblIndexHeader" runat="server"></asp:label></th></tr>
					<tr>
						<td>
							<table class="datagrid" cellspacing="0" cellpadding="0">
								<tr>
									<td><asp:datagrid id="dgIndexList" runat="server" autogeneratecolumns="False" allowsorting="True"
											width="100%" cellpadding="4" gridlines="None">
											<footerstyle cssclass="grid_footer"></footerstyle>
											<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
											<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
											<itemstyle cssclass="grid_item"></itemstyle>
											<headerstyle cssclass="grid_header"></headerstyle>
											<columns>
												<asp:buttoncolumn text="&lt;img src=&quot;../img/view.gif&quot; alt=&quot;View&quot; border=&quot;0&quot;&gt;"
													commandname="ShowDetails">
													<itemstyle cssclass="leftpadding"></itemstyle>
												</asp:buttoncolumn>
												<asp:editcommandcolumn buttontype="LinkButton" updatetext="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;"
													canceltext="&lt;img src=&quot;../img/cancel.gif&quot; alt=&quot;Cancel&quot; border=&quot;0&quot;&gt;"
													edittext="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;">
													<itemstyle cssclass="nopadding"></itemstyle>
												</asp:editcommandcolumn>
												<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
													commandname="Delete">
													<itemstyle cssclass="nopadding"></itemstyle>
												</asp:buttoncolumn>
												<asp:boundcolumn datafield="IndexName" sortexpression="IndexName" readonly="True" headertext="IndexName">
													<itemstyle cssclass="padding"></itemstyle>
												</asp:boundcolumn>
												<asp:boundcolumn datafield="DescriptionNative" sortexpression="DescriptionNative" headertext="DescriptionNative">
													<itemstyle cssclass="padding"></itemstyle>
												</asp:boundcolumn>
												<asp:boundcolumn datafield="DescriptionEN" sortexpression="DescriptionEN" headertext="DescriptionEN">
													<itemstyle cssclass="padding"></itemstyle>
												</asp:boundcolumn>
												<asp:templatecolumn headertext="IsActive">
													<itemtemplate>
														<asp:checkbox id="Checkbox1" checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem,"IsActive"))%>' runat="server" enabled="False" cssclass="radio">
														</asp:checkbox>
													</itemtemplate>
													<edititemtemplate>
														<asp:checkbox id="Checkbox2" checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem,"IsActive"))%>' runat="server" enabled="True" cssclass="radio">
														</asp:checkbox>
													</edititemtemplate>
													<itemstyle cssclass="padding"></itemstyle>
												</asp:templatecolumn>
											</columns>
											<pagerstyle cssclass="grid_pager"></pagerstyle>
										</asp:datagrid></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
