#region

using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;
using Logging.BLL;

#endregion

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    ///		Summary description for ItemRegistryCtrl.
    /// </summary>
    public class ItemRegistryCtrl : GroundCtrl {
        /// <summary>Used to maintain the datatable linked to the datagrid (items)</summary>
        /// <remarks>Used to maintain the datatable linked to the datagrid (items)</remarks>
        protected static DataTable itemTable;

        /// <summary>One instance of an item in item registry</summary>
        /// <remarks>One instance of an item in item registry</remarks>
        private static BillingItemBLLC theItem;

        protected Button btnCancel;
        protected Button btnCopy;
        protected Button btnInsert;
        protected Button btnUpdate;
        protected CheckBox chkIsOpen;
        protected DropDownList ddlCurrency;
        protected DropDownList ddlProductLink;
        protected DropDownList ddlUsageLink;
        protected DataGrid dgItems;
        protected Label lblCurrency;
        protected Label lblDatagridIcons;
        protected Label lblFinancialKey;
        protected Label lblIsOpen;
        protected Label lblItemIndexHeader;
        protected Label lblItemName;
        protected Label lblItemNameEN;
        protected Label lblMessage;
        protected Label lblMonthlyAmount;
        protected Label lblMonthlyDisc;
        protected Label lblPageHeader;
        protected Label lblProductLink;
        protected Label lblUsageAmount;
        protected Label lblUsageDisc;
        protected Label lblUsageLink;
        protected HtmlTable outerGridTable;
        protected HtmlTableCell tdInsertButton;
        protected HtmlTableCell tdSelectedButtons;
        protected TextBox txtFinancialKey;
        protected TextBox txtItemNameEN;
        protected TextBox txtItemNameNative;
        protected TextBox txtMonthlyAmount;
        protected TextBox txtMonthlyDisc;
        protected TextBox txtUsageAmount;
        protected TextBox txtUsageDisc;
        protected CompareValidator validateDoubleForMonthlyAmount;
        protected CompareValidator validateDoubleForUsageAmount;
        protected CompareValidator validateIntForMonthlyDisc;
        protected CompareValidator validateIntForUsageDisc;
        protected RequiredFieldValidator validateItemNameEN;
        protected RequiredFieldValidator validateItemNameNative;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            PageLoad(sender, e);

            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();

            LocalizeText();

            if (IsPostBack) {
                return;
            }
            SetDropDowns();
            SetDataGridControl();
            tdSelectedButtons.Visible = false;
        }

        /// <summary>
        /// For text localization. Text is set by using resource files that are XML based.
        /// </summary>
        protected override void LocalizeText() {
            base.LocalizeText();

            // Labels...
            lblMonthlyAmount.Text = rm.GetString("ItemRegistryCtrl.lblMonthlyAmount", ci);
            lblMonthlyDisc.Text = rm.GetString("ItemRegistryCtrl.lblMonthlyDisc", ci);
            lblUsageAmount.Text = rm.GetString("ItemRegistryCtrl.lblUsageAmount", ci);
            lblUsageDisc.Text = rm.GetString("ItemRegistryCtrl.lblUsageDisc", ci);

            lblItemIndexHeader.Text = rm.GetString("ItemRegistryCtrl.lblItemIndexHeader", ci);
            lblItemName.Text = rm.GetString("ItemRegistryCtrl.lblItemName", ci);
            lblItemNameEN.Text = rm.GetString("ItemRegistryCtrl.lblItemNameEN", ci);
            lblProductLink.Text = rm.GetString("ItemRegistryCtrl.lblProductLink", ci);
            lblUsageLink.Text = rm.GetString("ItemRegistryCtrl.lblUsageLink", ci);
            lblCurrency.Text = rm.GetString("ItemRegistryCtrl.lblCurrency", ci);
            lblIsOpen.Text = rm.GetString("ItemRegistryCtrl.lblIsOpen", ci);
            lblFinancialKey.Text = rm.GetString("ItemRegistryCtrl.lblFinancialKey", ci);
            lblPageHeader.Text = rm.GetString("ItemRegistryCtrl.lblPageHeader", ci);

            // Buttons...
            btnInsert.Text = rm.GetString("ItemRegistryCtrl.btnInsert", ci);
            btnCopy.Text = rm.GetString("ItemRegistryCtrl.btnCopy", ci);
            btnUpdate.Text = rm.GetString("ItemRegistryCtrl.btnUpdate", ci);
            btnCancel.Text = rm.GetString("ItemRegistryCtrl.btnCancel", ci);

            //Validators...
            validateDoubleForMonthlyAmount.ErrorMessage = rm.GetString("ItemRegistryCtrl.validateDoubleForAmount", ci);
            validateDoubleForUsageAmount.ErrorMessage = rm.GetString("ItemRegistryCtrl.validateDoubleForAmount", ci);
            validateIntForMonthlyDisc.ErrorMessage = rm.GetString("ItemRegistryCtrl.validateIntForDiscountPercent", ci);
            validateIntForUsageDisc.ErrorMessage = rm.GetString("ItemRegistryCtrl.validateIntForDiscountPercent", ci);
            validateItemNameEN.ErrorMessage = rm.GetString("ItemRegistryCtrl.validateItemNameEN", ci);
            validateItemNameNative.ErrorMessage = rm.GetString("ItemRegistryCtrl.validateItemNameNative", ci);

            // DataGrid Columns...
            if (!EN) {
                dgItems.Columns[3].HeaderText = rm.GetString("ItemRegistryCtrl.colItemName", ci);
                dgItems.Columns[3].Visible = true;
                dgItems.Columns[4].Visible = false;
            } else {
                dgItems.Columns[4].HeaderText = rm.GetString("ItemRegistryCtrl.colItemName", ci);
                dgItems.Columns[3].Visible = false;
                dgItems.Columns[4].Visible = true;
            }
            dgItems.Columns[5].HeaderText = rm.GetString("ItemRegistryCtrl.colCurrency", ci);
            dgItems.Columns[6].HeaderText = rm.GetString("ItemRegistryCtrl.colMonthly", ci);
            dgItems.Columns[7].HeaderText = rm.GetString("ItemRegistryCtrl.colUsage", ci);
            dgItems.Columns[8].HeaderText = rm.GetString("ItemRegistryCtrl.colMonthlyDisc", ci);
            dgItems.Columns[9].HeaderText = rm.GetString("ItemRegistryCtrl.colUsageDisc", ci);
            dgItems.Columns[10].HeaderText = rm.GetString("ItemRegistryCtrl.colFinancialKey", ci);
            dgItems.Columns[11].HeaderText = rm.GetString("ItemRegistryCtrl.colIsActive", ci);
        }

        /// <summary>
        /// Fetch all available Item data and bind it to the dgItems datagrid
        /// </summary>
        /// <remarks>Fetch all available Item data and bind it to the dgItems datagrid</remarks>
        private void SetDataGridControl() {
            // Show all available items...
            itemTable = BillingFactory.GetAllRegistryItemsAsDataTable();
            dgItems.DataSource = itemTable;
            dgItems.DataBind();

            outerGridTable.Visible = itemTable.Rows.Count >= 0;
        }

        /// <summary>
        /// Sets all dropdown controls on the page for initial values.
        /// </summary>
        /// <remarks>
        /// Currency, Product and logging dropdowns are set by fetching data from the database. To
        /// minimize database connection time all the lists are cached. This means that whenever the user adds
        /// some currency, product or logging data, he'll probably have to sign out of the system and
        /// log in again for all lists to display the correct data.
        /// </remarks>
        private void SetDropDowns() {
            var myCurrencyList = (ArrayList) Cache["myCurrencyList"];
            var myProductsTable = (DataTable) Cache["myProductsTable"];
            var myUsageTable = (DataTable) Cache["myUsageTable"];

            // Currency...
            if (myCurrencyList == null || myCurrencyList.Count < 1) {
                myCurrencyList = BillingFactory.GetCurrencyCodesAsArrayList();
                Cache["myCurrencyList"] = myCurrencyList;
            }
            ddlCurrency.DataSource = myCurrencyList;
            ddlCurrency.DataBind();

            // Products...
            if (myProductsTable == null) {
                myProductsTable = BillingFactory.GetActiveProductsAsDataTable();
                Cache["myProductsTable"] = myProductsTable;
            }

            // Display right language...
            if (EN) {
                ddlProductLink.DataTextField = "ProductNameEN";
                myProductsTable.DefaultView.Sort = "ProductNameEN";
            } else {
                ddlProductLink.DataTextField = "ProductNameNative";
                myProductsTable.DefaultView.Sort = "ProductNameNative";
            }

            // Add a value for "null" or blank...
            // Set it as "default" (.Selected = true)
            ddlProductLink.DataSource = myProductsTable.DefaultView;
            ddlProductLink.DataValueField = "ProductID";
            ddlProductLink.DataBind();
            ddlProductLink.Items.Insert(0, "...");
            ddlProductLink.Items[0].Value = "-1";
            ddlProductLink.Items[0].Selected = true;

            // Usage codes...
            if (myUsageTable == null) {
                myUsageTable = BillingFactory.GetUsageTypesAsDataTable();
                Cache["myUsageTable"] = myUsageTable;
            }

            // Display right language...
            if (EN) {
                ddlUsageLink.DataTextField = "typeEN";
                myUsageTable.DefaultView.Sort = "typeEN";
            } else {
                ddlUsageLink.DataTextField = "typeNative";
                myUsageTable.DefaultView.Sort = "typeNative";
            }

            // Add a value for "null" or blank...
            // Set it as "default" (.Selected = true)
            ddlUsageLink.DataSource = myUsageTable.DefaultView;
            ddlUsageLink.DataValueField = "id";
            ddlUsageLink.DataBind();
            ddlUsageLink.Items.Insert(0, "...");
            ddlUsageLink.Items[0].Value = "-1";
            ddlUsageLink.Items[0].Selected = true;

            // Dispose of the datatables...
            myProductsTable.Dispose();
            myUsageTable.Dispose();
        }

        /// <summary>
        /// Links an "enter event" to all input controls
        /// </summary>
        /// <remarks>
        /// Links an "enter event" to all input controls so as to allow the user to press the
        /// enter key to submit a new item.
        /// </remarks>
        private void AddEnterEvent() {
            txtFinancialKey.Attributes.Add("onkeypress", "checkEnterKey();");
            txtItemNameEN.Attributes.Add("onkeypress", "checkEnterKey();");
            txtItemNameNative.Attributes.Add("onkeypress", "checkEnterKey();");
            txtMonthlyAmount.Attributes.Add("onkeypress", "checkEnterKey();");
            txtMonthlyDisc.Attributes.Add("onkeypress", "checkEnterKey();");
            txtUsageAmount.Attributes.Add("onkeypress", "checkEnterKey();");
            txtUsageDisc.Attributes.Add("onkeypress", "checkEnterKey();");
            ddlCurrency.Attributes.Add("onkeypress", "checkEnterKey();");
            ddlProductLink.Attributes.Add("onkeypress", "checkEnterKey();");
            ddlUsageLink.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        /// <summary>
        /// Inserts or updates one item in the item registry. Validates the data that "on page"
        /// validators do not handle. Resets the form after insert, if nothing happens.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnInsert_Click(object sender, EventArgs e) {
            lblMessage.Visible = false;

            try {
                // Create a new item datastructure instance...
                var newItem = new BillingItemBLLC
                              {
                                  ItemCurrency = ddlCurrency.SelectedValue,
                                  ItemNameNative = txtItemNameNative.Text,
                                  ItemNameEN = txtItemNameEN.Text,
                                  MonthlyAmount =
                                      (txtMonthlyAmount.Text != "" ? Convert.ToDecimal(txtMonthlyAmount.Text) : 0),
                                  MonthlyDiscountPercent =
                                      (txtMonthlyDisc.Text != "" ? Convert.ToDecimal(txtMonthlyDisc.Text) : 0),
                                  UsageAmount = (txtUsageAmount.Text != "" ? Convert.ToDecimal(txtUsageAmount.Text) : 0),
                                  UsageDiscountPercent =
                                      (txtUsageDisc.Text != "" ? Convert.ToDecimal(txtUsageDisc.Text) : 0),
                                  LoggingCodeID = Convert.ToInt32(ddlUsageLink.SelectedValue),
                                  ProductCodeID = Convert.ToInt32(ddlProductLink.SelectedValue),
                                  IsActive = chkIsOpen.Checked,
                                  FinancialKey = txtFinancialKey.Text
                              };

                // Set the item values...

                // If amount and discount fields are empty then set as zero
                // Do not have to worry about convert because all fields are type validated

                // Check if this is an update or not...
                // If this is an update then we'd like to set the "update" fields of the item and
                // ceep the old item id (to update by).
                if (isUpdate) {
                    newItem.ItemID = theItem.ItemID;
                    newItem.Updated = DateTime.Now;
                    newItem.UpdatedBy = Convert.ToInt32(Session["UserLoginID"]);
                } else {
                    // Same item id and master item id for new (master) items (ItemID = -1)...
                    // Done to enable item copies which can be traced to the item master, that was copied.
                    if (theItem != null) {
                        newItem.MasterItemID = theItem.MasterItemID == -1 ? theItem.ItemID : theItem.MasterItemID;
                    } else {
                        newItem.MasterItemID = newItem.ItemID;
                    }

                    newItem.Inserted = DateTime.Now;
                    newItem.InsertedBy = Convert.ToInt32(Session["UserLoginID"]);
                }

                // Try to add or update the item...
                try {
                    if (isUpdate) {
                        if (!BillingFactory.UpdateRegistryItem(newItem)) {
                            lblMessage.Text = rm.GetString("global.inserterrormsg", ci);
                            lblMessage.ForeColor = Color.Red;
                            lblMessage.Visible = true;
                            return;
                        }
                        // Reset textboxes and dropdowns...
                        ResetForm();
                    } else {
                        if (!BillingFactory.AddRegistryItem(newItem)) {
                            lblMessage.Text = rm.GetString("global.inserterrormsg", ci);
                            lblMessage.ForeColor = Color.Red;
                            lblMessage.Visible = true;
                            return;
                        }
                        // Reset textboxes and dropdowns...
                        ResetForm();
                    }
                } catch {
                    // Display error message if data connection failed. Error message is red.
                    lblMessage.Text = rm.GetString("global.inserterrormsg", ci);
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Visible = true;
                    return;
                }

                // Reset the item datagrid...
                SetDataGridControl();
            } catch {
                // Display error message if data connection failed. Error message is red.
                lblMessage.Text = rm.GetString("global.inserterrormsg", ci);
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
                return;
            }
        }

        /// <summary>
        /// Indicates that the user is going to update the selected item. Other choices are to
        /// copy it or cancel the transaction
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnUpdate_Click(object sender, EventArgs e) {
            // To indicate that the control is in "update" mode
            // isUpdate is a boolean variable on groundcontrol that "hides" a session variable
            isUpdate = true;
            tdSelectedButtons.Visible = false;
            tdInsertButton.Visible = true;

            // Re enable the controls...
            SetControlsEnabeled(true);
        }

        /// <summary>
        /// Indicates that the user is going to "copy" the selected item. Other choices are to
        /// update it or cancel the transaction 
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnCopy_Click(object sender, EventArgs e) {
            // To indicate that the control is in "copy" mode
            // isUpdate is a boolean variable on groundcontrol that "hides" a session variable
            isUpdate = false;
            tdSelectedButtons.Visible = false;
            tdInsertButton.Visible = true;

            // Re enable the controls...
            SetControlsEnabeled(true);
        }

        /// <summary>
        /// Indicates that the user is going to cancel the transaction (item select). Other choices are to
        /// update or copy the selected item.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnCancel_Click(object sender, EventArgs e) {
            // To indicate that the transaction is canceled.
            tdSelectedButtons.Visible = false;
            tdInsertButton.Visible = true;

            // Reset the form and enable all controls...
            ResetForm();
            SetControlsEnabeled(true);
        }

        /// <summary>
        /// Delete an item from the item registry...
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e">System.EventArgs</param>
        private void dgItems_DeleteCommand(object source, DataGridCommandEventArgs e) {
            // Try to delete the item... 
            // Show a popup if this is unsuccessful - Probable cause is a foreign key constraint

            // TODO: In later versions it should be impossible to try to delete items that are bound by foreign key constraints...
            if (!BillingFactory.DeleteFromRegistryItems(Convert.ToInt32(e.Item.Cells[2].Text))) {
                const string myScript = "<script language=Javascript>alert('Could not delete - Probable constraint conflict.');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
            } else {
                // Deteted form static DataTable itemTable. This means that in order to update the ddg there is no
                // need in getting fresh data from the database, even if the deletion has already taken place there.

                itemTable.Rows.Remove(itemTable.Rows.Find(e.Item.Cells[2].Text));
                dgItems.DataSource = itemTable;
                dgItems.DataBind();
            }

            // Reset datagrid selection index...
            dgItems.EditItemIndex = -1;
        }

        /// <summary>
        /// The user has selected a specific item in the datagrid.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void dgItems_SelectedIndexChanged(object sender, EventArgs e) {
            // Get the selected item with all information...
            theItem = BillingFactory.GetRegistryItem(Convert.ToInt32(dgItems.SelectedItem.Cells[2].Text));

            // Set all input fields and disable them temporarily until the user decides what he is going to
            // do with the selected item..
            txtItemNameNative.Text = theItem.ItemNameNative;
            txtItemNameEN.Text = theItem.ItemNameEN;
            txtFinancialKey.Text = theItem.FinancialKey;
            txtMonthlyAmount.Text = theItem.MonthlyAmount.ToString();
            txtMonthlyDisc.Text = theItem.MonthlyDiscountPercent.ToString();
            txtUsageAmount.Text = theItem.UsageAmount.ToString();
            txtUsageDisc.Text = theItem.UsageDiscountPercent.ToString();
            chkIsOpen.Checked = theItem.IsActive;

            // Reset all dropdowns and other controls...
            ddlCurrency.SelectedValue = theItem.ItemCurrency;

            if (theItem.ProductCodeID > 0) {
                ddlProductLink.SelectedValue = theItem.ProductCodeID.ToString();
            } else {
                ddlProductLink.SelectedIndex = 0;
            }

            if (theItem.LoggingCodeID > 0) {
                ddlUsageLink.SelectedValue = theItem.LoggingCodeID.ToString();
            } else {
                ddlUsageLink.SelectedIndex = 0;
            }

            // Disable all the "input controls" such as textboxes and dropdowns.
            // The controls remain disabled until the user decides if the wants to "update"
            // or "copy" the selected item.
            SetControlsEnabeled(false);

            // To indicate that the control is in "locked" mode
            tdSelectedButtons.Visible = true;
            tdInsertButton.Visible = false;
        }

        /// <summary>
        /// Sort the items in the datagrid according to a selected column.
        /// </summary>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridSortCommandEventArgs</param>
        private void dgItems_SortCommand(object source, DataGridSortCommandEventArgs e) {
            DataView myView = itemTable.DefaultView;
            myView.Sort = e.SortExpression;
            dgItems.DataSource = myView;

            dgItems.DataBind();
        }

        /// <summary>
        /// Enable / disable "input" controls such as textboxes or dropdowns...
        /// </summary>
        /// <param name="Enable">boolean value, true = enable, false = disable</param>
        private void SetControlsEnabeled(bool Enable) {
            txtItemNameNative.Enabled = Enable;
            txtItemNameEN.Enabled = Enable;
            txtFinancialKey.Enabled = Enable;
            txtMonthlyAmount.Enabled = Enable;
            txtMonthlyDisc.Enabled = Enable;
            txtUsageAmount.Enabled = Enable;
            txtUsageDisc.Enabled = Enable;

            ddlCurrency.Enabled = Enable;
            ddlProductLink.Enabled = Enable;
            ddlUsageLink.Enabled = Enable;
            chkIsOpen.Enabled = Enable;
        }

        /// <summary>
        /// Reset "input" controls such as textboxes and dropdowns...
        /// </summary>
        private void ResetForm() {
            // Reset all input fields...
            txtItemNameNative.Text = "";
            txtItemNameEN.Text = "";
            txtFinancialKey.Text = "";
            txtMonthlyAmount.Text = "0";
            txtMonthlyDisc.Text = "0";
            txtUsageAmount.Text = "0";
            txtUsageDisc.Text = "0";

            // Reset all dropdowns and other controls...
            ddlCurrency.SelectedIndex = 0;
            ddlProductLink.SelectedIndex = 0;
            ddlUsageLink.SelectedIndex = 0;
            chkIsOpen.Checked = true;

            // Remove update mode...
            isUpdate = false;

            // Dispose of current item if one is set...
            theItem = null;

            // Reset datagrid
            dgItems.SelectedIndex = -1;
        }

        /// <summary>
        /// Add a confirmation step for Datagrid delete...
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridItemEventArgs</param>
        private void dgItems_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
            }

            WebDesign.CreateExplanationIcons(dgItems.Columns, lblDatagridIcons, rm, ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnInsert.Click += new EventHandler(this.btnInsert_Click);
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnCopy.Click += new EventHandler(this.btnCopy_Click);
            this.btnUpdate.Click += new EventHandler(this.btnUpdate_Click);
            this.dgItems.SortCommand += new DataGridSortCommandEventHandler(this.dgItems_SortCommand);
            this.dgItems.DeleteCommand += new DataGridCommandEventHandler(this.dgItems_DeleteCommand);
            this.dgItems.ItemDataBound += new DataGridItemEventHandler(this.dgItems_ItemDataBound);
            this.dgItems.SelectedIndexChanged += new EventHandler(this.dgItems_SelectedIndexChanged);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}