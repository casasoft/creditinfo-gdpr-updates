#region

using System;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;
using Logging.BLL;

#endregion

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    ///		Summary description for IndexOverviewCtrl.
    /// </summary>
    public class IndexOverviewCtrl : GroundCtrl {
        protected static DataTable indexTable;
        protected Button btnInsert;
        protected CheckBox chkIsIndexOpen;
        protected DataGrid dgIndexList;
        protected Label lblDatagridIcons;
        protected Label lblDescEN;
        protected Label lblDescNative;
        protected Label lblIndexHeader;
        protected Label lblIndexisOpen;
        protected Label lblIndexName;
        protected Label lblMessage;
        protected Label lblPageHeader;
        protected HtmlTable outerGridTable;
        protected TextBox txtDescEN;
        protected TextBox txtDescNative;
        protected TextBox txtIndexName;
        protected RequiredFieldValidator validateIndexName;
        protected ValidationSummary ValidationSummary;

        /// <summary>Called when page is loaded. Used to initialize the page and set default values</summary>
        /// <remarks>Sets rm and ci, static variables. Calls LocalizeText method.</remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void Page_Load(object sender, EventArgs e) {
            base.PageLoad(sender, e);

            LocalizeText();

            if (!IsPostBack) {
                // Function sets index controls to display index lists and such...
                SetIndexControls();
            }
        }

        /// <summary>Called each time the page is loaded. Localizes all displayed text</summary>
        /// <remarks>
        /// Uses the rm.GetString method to set language according to the current
        /// culture settings. Resource files must be available for each languge that can be set. Example:
        /// <code>
        ///		public void LocalizeText()
        ///		{
        ///			this.lblBilling.Text = rm.GetString("lblBilling",ci);
        ///		}
        /// </code>
        /// </remarks>
        protected override void LocalizeText() {
            // Labels
            lblIndexName.Text = rm.GetString("colIndexName", ci);
            lblIndexisOpen.Text = rm.GetString("colIsActive", ci);
            lblDescEN.Text = rm.GetString("IndexOverviewCtrl.lblDescEN", ci);
            lblDescNative.Text = rm.GetString("IndexOverviewCtrl.lblDescNative", ci);
            lblPageHeader.Text = rm.GetString("IndexOverviewCtrl.lblPageHeader", ci);

            // Buttons
            btnInsert.Text = rm.GetString("btnInsert", ci);

            // Datagrid textar
            dgIndexList.Columns[3].HeaderText = rm.GetString("colIndexName", ci);

            if (!EN) {
                dgIndexList.Columns[4].HeaderText = rm.GetString("colDescription", ci);
                dgIndexList.Columns[4].Visible = true;
                dgIndexList.Columns[5].Visible = false;
            } else {
                dgIndexList.Columns[5].HeaderText = rm.GetString("colDescription", ci);
                dgIndexList.Columns[4].Visible = false;
                dgIndexList.Columns[5].Visible = true;
            }
            dgIndexList.Columns[6].HeaderText = rm.GetString("colIsActive", ci);

            // Error messages

            validateIndexName.ErrorMessage = rm.GetString("validateIndexName", ci);
        }

        /// <summary>
        /// Gets the index data from the database (through the factory class) and links it to
        /// a datagrid
        /// </summary>
        /// <remarks>Gets the index data from the database (through the factory class) and links it to
        /// a datagrid. Errors are displayed via label and resource text (lblIndexHeader_Error).</remarks>
        private void SetIndexControls() {
            try {
                lblIndexHeader.Text = rm.GetString("lblIndexHeader", ci);
                //this.lblIndexHeader.ForeColor = Color.Black;

                // Show all available indexes...
                indexTable = BillingFactory.GetCompleteIndexListAsDataTable();

                dgIndexList.DataSource = indexTable;
                dgIndexList.DataBind();

                outerGridTable.Visible = indexTable.Rows.Count >= 0;
            } catch (Exception) {
                // Display error message if data connection failed. Error message is red.
                lblIndexHeader.Text = rm.GetString("lblIndexHeader_Error", ci);
                lblIndexHeader.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// Insert a new index into the billing_Indexes datatable.
        /// </summary>
        /// <remarks>Creates an instance of an index based on user input.</remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnInsert_Click(object sender, EventArgs e) {
            lblMessage.Visible = false;
            IndexOverviewBLLC myIndex;

            try {
                myIndex = new IndexOverviewBLLC(
                    txtIndexName.Text,
                    chkIsIndexOpen.Checked,
                    txtDescNative.Text,
                    txtDescEN.Text);
            } catch {
                // Display error message if it's not possible to construct the index instance...
                lblMessage.Text = rm.GetString("lblMessage_IndexInstanceError", ci);
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
                return;
            }

            try {
                if (!BillingFactory.AddNewIndex(myIndex, Convert.ToInt32(Session["UserLoginID"]))) {
                    lblMessage.Text = rm.GetString("lblMessage_AddIndexTruncate", ci);
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Visible = true;
                    return;
                }
            } catch {
                // Display error message if data connection failed. Error message is red.
                lblMessage.Text = rm.GetString("lblMessage_InsertIndexError", ci);
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
                return;
            }

            SetIndexControls();
            ClearBoxes();
        }

        /// <summary>
        /// Deletes one index form the ddb and updates the index datagrid.
        /// </summary>
        /// <remarks>Deletes one index form the ddb and updates the index datagrid.</remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgIndexList_DeleteCommand(object source, DataGridCommandEventArgs e) {
            if (!BillingFactory.DeleteFromIndexes(e.Item.Cells[3].Text)) {
                const string myScript = "<script language=Javascript>alert('Could not delete. A probable cause is a foreign key constraint in the database.');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
            } else {
                // Deteted form static DataTable indexTable. This means that in order to update the ddg there is no
                // need in getting fresh data from the database, even if the deletion has already taken place there.
                indexTable.Rows.Remove(indexTable.Rows.Find(e.Item.Cells[3].Text));
                dgIndexList.DataSource = indexTable;
                dgIndexList.DataBind();
            }
            dgIndexList.EditItemIndex = -1;
        }

        /// <summary>
        /// Sort datagrid by available sort fields.
        /// </summary>
        /// <remarks>Uses a datatable (static) variable to set up the sorted data via DefaultView.</remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridSortCommandEventArgs</param>
        private void dgIndexList_SortCommand(object source, DataGridSortCommandEventArgs e) {
            var myView = indexTable.DefaultView;

            myView.Sort = e.SortExpression;
            dgIndexList.DataSource = myView;
            dgIndexList.DataBind();
        }

        /// <summary>
        /// Cancel index edit.
        /// </summary>
        /// <remarks>
        /// <code>
        /// ResetValidation();	
        /// dgIndexList.EditItemIndex = -1;
        /// dgIndexList.DataSource = indexTable;
        /// dgIndexList.DataBind();	
        /// </code>
        /// </remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgIndexList_CancelCommand(object source, DataGridCommandEventArgs e) {
            ResetValidation();

            dgIndexList.EditItemIndex = -1;
            dgIndexList.DataSource = indexTable;
            dgIndexList.DataBind();
        }

        /// <summary>
        /// Update index.
        /// </summary>
        /// <remarks>
        /// Updates both datagrid and ddb with user input. Validates user input before update attempt. Uses
        /// javascripts for popup messages if input is not ok or if something fails during updates.
        /// <code>
        /// string myScript = "<script language=Javascript>alert('Check amount and percentage values. Must register one, not both.');</script>"; 
        /// Page.RegisterClientScriptBlock("alert", myScript); 
        /// return;
        /// </code>
        /// </remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgIndexList_UpdateCommand(object source, DataGridCommandEventArgs e) {
            try {
                // Create a new index...
                var theIndex = new IndexOverviewBLLC
                               {
                                   Name = e.Item.Cells[3].Text,
                                   DescNative = ((TextBox) e.Item.Cells[4].Controls[0]).Text,
                                   DescEN = ((TextBox) e.Item.Cells[5].Controls[0]).Text,
                                   Active = ((CheckBox) e.Item.Cells[6].Controls[1]).Checked
                               };

                try {
                    // Try to update the index in the ddb...
                    if (!BillingFactory.UpdateIndexOverView(theIndex, Convert.ToInt32(Session["UserLoginID"]))) {
                        const string myScript = "<script language=Javascript>alert('Failed to update index.');</script>";
                        Page.RegisterClientScriptBlock("alert", myScript);
                        return;
                    }

                    // Try to update the index in the datatable that is linked to the datagrid...
                    var indexArray = new object[4]
                                          {theIndex.Name, theIndex.Active, theIndex.DescNative, theIndex.DescEN};
                    indexTable.LoadDataRow(indexArray, true);
                } catch {
                    const string myScript = "<script language=Javascript>alert('Failed to update index.');</script>";
                    Page.RegisterClientScriptBlock("alert", myScript);
                    dgIndexList.EditItemIndex = -1;
                    return;
                }
            } catch {
                const string myScript = "<script language=Javascript>alert('Check values.');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
                dgIndexList.EditItemIndex = -1;
                return;
            }

            // reset code for datagrid...
            dgIndexList.EditItemIndex = -1;
            ResetValidation();

            dgIndexList.DataSource = indexTable;
            dgIndexList.DataBind();
        }

        private void dgIndexList_EditCommand(object source, DataGridCommandEventArgs e) {
            dgIndexList.EditItemIndex = e.Item.ItemIndex;

            dgIndexList.Columns[4].Visible = true;
            dgIndexList.Columns[5].Visible = true;

            // Set validation for DataGrid...
            validateIndexName.Enabled = false;

            // Disable the insert textboxes...
            txtIndexName.Enabled = false;
            txtDescEN.Enabled = false;
            txtDescNative.Enabled = false;
            chkIsIndexOpen.Enabled = false;

            dgIndexList.DataSource = indexTable;
            dgIndexList.DataBind();
        }

        /// <summary>
        /// Used to reset the form for new index insertion.
        /// </summary>
        /// <remarks>When the form is in "update" mode (through the datagrid) it disables all the validators and
        /// disables all the textfields. This function resets both the validators and the textfields.</remarks>
        private void ResetValidation() {
            // Reset validation functions so they validate the "new item" textboxes...
            validateIndexName.Enabled = true;

            // Enable the insert textboxes...
            txtIndexName.Enabled = true;
            txtDescEN.Enabled = true;
            txtDescNative.Enabled = true;
            chkIsIndexOpen.Enabled = true;
        }

        private void dgIndexList_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName == "ShowDetails") {
                dgIndexList.SelectedIndex = -1;
                Session["IndexNameForDetails"] = e.Item.Cells[3].Text;
                Server.Transfer("TableUpdates.aspx?Type=Indexes");
            }
        }

        private void ClearBoxes() {
            txtDescEN.Text = "";
            txtDescNative.Text = "";
            txtIndexName.Text = "";
            chkIsIndexOpen.Enabled = true;
            chkIsIndexOpen.Checked = true;
        }

        private void dgIndexList_ItemDataBound_1(object sender, DataGridItemEventArgs e) { WebDesign.CreateExplanationIcons(dgIndexList.Columns, lblDatagridIcons, rm, ci); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnInsert.Click += new EventHandler(this.btnInsert_Click);
            this.dgIndexList.ItemCommand += new DataGridCommandEventHandler(this.dgIndexList_ItemCommand);
            this.dgIndexList.CancelCommand += new DataGridCommandEventHandler(this.dgIndexList_CancelCommand);
            this.dgIndexList.EditCommand += new DataGridCommandEventHandler(this.dgIndexList_EditCommand);
            this.dgIndexList.SortCommand += new DataGridSortCommandEventHandler(this.dgIndexList_SortCommand);
            this.dgIndexList.UpdateCommand += new DataGridCommandEventHandler(this.dgIndexList_UpdateCommand);
            this.dgIndexList.DeleteCommand += new DataGridCommandEventHandler(this.dgIndexList_DeleteCommand);
            this.dgIndexList.ItemDataBound += new DataGridItemEventHandler(this.dgIndexList_ItemDataBound_1);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}