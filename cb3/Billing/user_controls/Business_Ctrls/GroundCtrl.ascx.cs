#region

using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using Billing.BLL;
using Billing.Localization;

#endregion

namespace Billing.user_controls.Business_Ctrls {
    // Resource and globalization
    /// <summary>
    ///		Summary description for GroundCtrl.
    /// </summary>
    public class GroundCtrl : UserControl {
        /// <summary>Static CultureInfo object
        /// <seealso cref="System.Globalization.CultureInfo"/>
        /// <seealso cref="System.Threading.Thread"/>
        /// <seealso cref="System.Threading.Thread.CurrentThread"/></summary>
        /// <remarks>Variable that holds the CurrentCulture for the CurrentThread. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		ci = Thread.CurrentThread.CurrentCulture;
        ///		}
        /// </code></remarks>
        public static CultureInfo ci;

        /// <summary>True if culture = "en-US"</summary>
        /// <remarks>True if culture = "en-US"</remarks>
        public static bool EN;

        /// <summary>Initializes a new static instance of the BLL.HelpFunctions class
        /// <seealso cref="Billing.BLL.HelpFunctions"/></summary>
        /// <remarks>Static HelpFunctions class is used to access methods that are designed to help the
        /// system in regard to flexibility and usability. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		string Subject = "Mail sent by the HelpFunctions Class via smtp";
        ///		string Message = "This is the message";
        ///		myHelpFunctions.SendMail(Message, Subject);
        ///		}
        /// </code></remarks>
        public static HelpFunctions myHelpFunctions = new HelpFunctions();

//		public  static HelpFunctions myHelpFunctions
//		{
//			get { return _myHelpFunctions; }
//			set { _myHelpFunctions = value;}
//		}
        /// <summary>Static ResourceManager object
        /// <seealso cref="System.Resources.ResourceManager"/>
        /// <seealso cref="Billing.Localization.CIResource"/></summary>
        /// <remarks>Used to hold the return value of the CIResource.CurrentManager property. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		rm = CIResource.CurrentManager;
        ///		}
        /// </code></remarks>
        public static ResourceManager rm;

        public bool isUpdate { get { return Convert.ToBoolean(Session["isUpdate"]); } set { Session["isUpdate"] = value; } }

        public void PageLoad(object sender, EventArgs e) {
            // For multilanguage control...
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            String culture = Thread.CurrentThread.CurrentCulture.Name;

            if (culture.Equals("en-US")) {
                EN = true;
            } else {
                EN = false;
            }
        }

        protected virtual void LocalizeText() { }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
//			myHelpFunctions = new HelpFunctions();
//			this.Load += new System.EventHandler(this.PageLoad);
        }

        #endregion
    }
}