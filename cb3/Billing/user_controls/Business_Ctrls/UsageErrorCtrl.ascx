<%@ Control Language="c#" AutoEventWireup="false" Codebehind="UsageErrorCtrl.ascx.cs" Inherits="Billing.user_controls.Business_Ctrls.UsageErrorCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" src="DatePicker.js"></script>
<body>
	<form id="UsageError" method="post">
		<table width="100%">
			<tr>
				<td>
					<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
					<table class="grid_table" cellspacing="0" cellpadding="0">
						<tr>
							<th style="HEIGHT: 19px">
								<asp:label id="lblPageHeader" runat="server" ></asp:label></th></tr>
						<tr>
							<td>
								<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
									<tr>
										<td><asp:label id="lblDateFrom" runat="server">Label</asp:label><br>
											<asp:textbox id="txtDateFrom" runat="server"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('UsageErrorCtrl_txtDateFrom', 250, 250);" type="button"
												value="...">
											<asp:requiredfieldvalidator id="rfvDateFrom" runat="server" controltovalidate="txtDateFrom" errormessage="RequiredFieldValidator">*</asp:requiredfieldvalidator><asp:comparevalidator id="cpvDateFrom" runat="server" controltovalidate="txtDateFrom" errormessage="CompareValidator"
												operator="DataTypeCheck" type="Date">*</asp:comparevalidator></td>
										<td><asp:label id="lblDateTo" runat="server">Label</asp:label><br>
											<asp:textbox id="txtDateTo" runat="server"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('UsageErrorCtrl_txtDateTo', 250, 250);" type="button"
												value="...">
											<asp:requiredfieldvalidator id="rfvDateTo" runat="server" controltovalidate="txtDateTo" errormessage="RequiredFieldValidator">*</asp:requiredfieldvalidator><asp:comparevalidator id="CompareValidator2" runat="server" controltovalidate="txtDateTo" errormessage="CompareValidator"
												operator="DataTypeCheck" type="Date">*</asp:comparevalidator></td>
										<td align="right"><asp:button id="btnSearch" runat="server" cssclass="search_button" text="Button"></asp:button></td>
									</tr>
									<tr>
										<td height="23"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
					<table class="grid_table" id="tblWithoutProductHeader" cellspacing="0" cellpadding="0"
						runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblWithoutProductHeader" runat="server"></asp:label></th></tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td><asp:datagrid id="dgrWithoutProduct" runat="server" cssclass="grid" autogeneratecolumns="False"
												allowsorting="True" gridlines="None">
												<footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
												<columns>
													<asp:boundcolumn datafield="query_type">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="typeNative">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="total">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
												</columns>
											</asp:datagrid></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
					<table class="grid_table" id="tblWithoutAgreement" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblWithoutAgreement" runat="server"></asp:label></th></tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td><asp:datagrid id="dgrWithoutAgreement" runat="server" cssclass="grid" autogeneratecolumns="False"
												allowsorting="True" gridlines="None">
												<footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
												<columns>
													<asp:boundcolumn datafield="subscriberid">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="SubscriberNickName">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="query_type">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="typeNative">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="total">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="fromdate">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="todate">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
												</columns>
											</asp:datagrid></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
					<table class="grid_table" id="tblWithoutUser" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblWithoutUser" runat="server"></asp:label>
							</th>
						</tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<asp:datagrid id="dgrWithoutUser" runat="server" autogeneratecolumns="False" allowsorting="True"
												gridlines="None" cssclass="grid">
												<footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
												<columns>
													<asp:boundcolumn datafield="Creditinfoid">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="query_type">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="typeNative">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="total">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="fromdate">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="todate">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
												</columns>
											</asp:datagrid>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
					<table class="grid_table" id="tblWithMultiBillItems" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblWithMultiBillItems" runat="server"></asp:label>
							</th>
						</tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<asp:datagrid id="dgrWithMultiBillItems" runat="server" autogeneratecolumns="False" allowsorting="True"
												gridlines="None" cssclass="grid">
												<footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
												<columns>
													<asp:boundcolumn datafield="subscriberid">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="SubscriberNickName">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="AgreementNumber">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn visible="False" datafield="id">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="typeNative">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="ConflictAgreementNumber">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="BeginDate">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="EndDate">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="ConflictBeginDate">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="ConflictEndDate">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
												</columns>
											</asp:datagrid>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
