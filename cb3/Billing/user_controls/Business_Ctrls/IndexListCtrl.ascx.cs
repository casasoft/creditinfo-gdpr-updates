#region

using System;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;
using Logging.BLL;

#endregion

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    ///		IndexListCtrl is used for index manipulation, insertion and deletion.
    /// </summary>
    /// <remarks>IndexListCtrl is used for index manipulation, insertion and deletion.</remarks>
    public class IndexListCtrl : GroundCtrl {
        protected static DataTable indexTable;
        protected Button btnInsert;
        protected DropDownList ddlIndexes;
        protected DataGrid dgIndexList;
        protected Label lblDatagridIcons;
        protected Label lblIndexDate;
        protected Label lblIndexHeader;
        protected Label lblIndexName;
        protected Label lblIndexValue;
        protected Label lblMessage;
        protected Label lblPageHeader;
        protected HtmlTable outerGridTable;
        protected TextBox txtIndexDate;
        protected TextBox txtIndexValue;
        protected RequiredFieldValidator validateBeginDate;
        protected RequiredFieldValidator validatePercent;
        protected ValidationSummary ValidationSummary;

        /// <summary>Called when page is loaded. Used to initialize the page and set default values</summary>
        /// <remarks>Sets rm and ci, static variables. Calls LocalizeText method.</remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void Page_Load(object sender, EventArgs e) {
            base.PageLoad(sender, e);

            lblMessage.Visible = false;
            LocalizeText();

            if (!IsPostBack) {
                outerGridTable.Visible = false;
                // Function sets index controls to display index lists and such...
                SetIndexControls();
            }
        }

        /// <summary>Called each time the page is loaded. Localizes all displayed text</summary>
        /// <remarks>
        /// Uses the rm.GetString method to set language according to the current
        /// culture settings. Resource files must be available for each languge that can be set. Example:
        /// <code>
        ///		public void LocalizeText()
        ///		{
        ///			this.lblBilling.Text = rm.GetString("lblBilling",ci);
        ///		}
        /// </code>
        /// </remarks>
        protected override void LocalizeText() {
            // Labels
            lblIndexName.Text = rm.GetString("colIndexName", ci);
            lblIndexDate.Text = rm.GetString("lblIndexDate", ci);
            lblIndexValue.Text = rm.GetString("lblIndexValue", ci);
            lblPageHeader.Text = rm.GetString("IndexListCtrl.lblPageHeader", ci);

            // Buttons
            btnInsert.Text = rm.GetString("btnInsert", ci);

            // Datagrid textar
            dgIndexList.Columns[2].HeaderText = rm.GetString("colIndexName", ci);
            dgIndexList.Columns[3].HeaderText = rm.GetString("colIndexDate", ci);
            dgIndexList.Columns[4].HeaderText = rm.GetString("colIndexValue", ci);

            // Error messages
            validateBeginDate.ErrorMessage = rm.GetString("validateBeginDate", ci);
            validatePercent.ErrorMessage = rm.GetString("validatePercent", ci);
        }

        /// <summary>
        /// Gets the index data from the database (through the factory class) and links it to
        /// a datagrid
        /// </summary>
        /// <remarks>Gets the index data from the database (through the factory class) and links it to
        /// a datagrid. Errors are displayed via label and resource text (lblIndexHeader_Error).</remarks>
        private void SetIndexControls() {
            try {
                lblIndexHeader.Text = rm.GetString("lblIndexHeader", ci);
                //this.lblIndexHeader.ForeColor = Color.Black;

                // Show all available indexes...
                ddlIndexes.DataSource = BillingFactory.GetActiveIndexesAsArrayList();
                ddlIndexes.DataBind();

                if (Session["IndexNameForDetails"] != null) {
                    FillDataGrid();
                } else {
                    ddlIndexes.Items.Insert(0, "...");
                    ddlIndexes.Items[0].Value = "-1";
                    ddlIndexes.Items[0].Selected = true;
                }
            } catch (Exception ex) {
                // Display error message if data connection failed. Error message is red.
                lblIndexHeader.Text = rm.GetString("lblIndexHeader_Error", ci);
                lblIndexHeader.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// Insert a new index into the billing_IndexDetails datatable.
        /// </summary>
        /// <remarks>Creates an instance of an index based on user input.</remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnInsert_Click(object sender, EventArgs e) {
            lblMessage.Visible = false;

            if (ddlIndexes.SelectedItem.Text == "...") {
                return;
            }

            IndexBLLC myIndex = null;

            try {
                myIndex = new IndexBLLC(
                    ddlIndexes.SelectedValue,
                    Convert.ToDecimal(txtIndexValue.Text),
                    Convert.ToDateTime(txtIndexDate.Text));
            } catch {
                // Display error message if it's not possible to construct the index instance...
                lblMessage.Text = rm.GetString("lblMessage_IndexInstanceError", ci);
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
                return;
            }

            try {
                if (!BillingFactory.AddNewIndex(myIndex, Convert.ToInt32(Session["UserLoginID"]))) {
                    lblMessage.Text = rm.GetString("lblMessage_AddIndexDetailsTruncate", ci);
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Visible = true;
                    return;
                }

                lblMessage.Text = rm.GetString("IndexListCtrl.RecordAddedMessage");
                lblMessage.Visible = true;
            } catch {
                // Display error message if data connection failed. Error message is red.
                lblMessage.Text = rm.GetString("lblMessage_InsertIndexDetailError", ci);
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
                return;
            }

            Session["IndexNameForDetails"] = ddlIndexes.SelectedItem.Text;
            FillDataGrid();

//			// Code that let's us update the datagrid without consulting with the database which has already been
//			// updated. An object array is placed within a new datarow which is then loaded into the static table
//			// indexTable that holds all the index rows. The DataGrid is then relinked and rebound.
//			object[] indexArray = new object[6] {myIndex.Name, myIndex.BeginDate, myIndex.EndDate, myIndex.Percent, myIndex.Amount, myIndex.Active};
//			DataRow newRow = indexTable.NewRow();
//			newRow.ItemArray = indexArray;
//
//			indexTable.Rows.Add(newRow);
//			dgIndexList.DataSource = indexTable;
//			dgIndexList.DataBind();		
        }

        /// <summary>
        /// Deletes one index form the ddb and updates the index datagrid.
        /// </summary>
        /// <remarks>Deletes one index form the ddb and updates the index datagrid.</remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgIndexList_DeleteCommand(object source, DataGridCommandEventArgs e) {
            if (!BillingFactory.DeleteFromIndexDetails(e.Item.Cells[2].Text, Convert.ToDateTime(e.Item.Cells[3].Text))) {
                string myScript = "<script language=Javascript>alert('Could not delete.');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
            } else {
                // Deteted form static DataTable indexTable. This means that in order to update the ddg there is no
                // need in getting fresh data from the database, even if the deletion has already taken place there.

                object[] oArray = new object[2] {e.Item.Cells[2].Text, Convert.ToDateTime(e.Item.Cells[3].Text)};
                indexTable.Rows.Remove(indexTable.Rows.Find(oArray));
                dgIndexList.DataSource = indexTable;
                dgIndexList.DataBind();

                //FillDataGrid();
            }
            dgIndexList.EditItemIndex = -1;
        }

        /// <summary>
        /// Sort datagrid by available sort fields.
        /// </summary>
        /// <remarks>Uses a datatable (static) variable to set up the sorted data via DefaultView.</remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridSortCommandEventArgs</param>
        private void dgIndexList_SortCommand(object source, DataGridSortCommandEventArgs e) {
            DataView myView;
            myView = indexTable.DefaultView;

            myView.Sort = e.SortExpression;
            dgIndexList.DataSource = myView;
            dgIndexList.DataBind();
        }

        /// <summary>
        /// Cancel index edit.
        /// </summary>
        /// <remarks>
        /// <code>
        /// ResetValidation();	
        /// dgIndexList.EditItemIndex = -1;
        /// dgIndexList.DataSource = indexTable;
        /// dgIndexList.DataBind();	
        /// </code>
        /// </remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgIndexList_CancelCommand(object source, DataGridCommandEventArgs e) {
            ResetValidation();

            dgIndexList.EditItemIndex = -1;
            dgIndexList.DataSource = indexTable;
            dgIndexList.DataBind();
        }

        /// <summary>
        /// Update index.
        /// </summary>
        /// <remarks>
        /// Updates both datagrid and ddb with user input. Validates user input before update attempt. Uses
        /// javascripts for popup messages if input is not ok or if something fails during updates.
        /// <code>
        /// string myScript = "<script language=Javascript>alert('Check amount and percentage values. Must register one, not both.');</script>"; 
        /// Page.RegisterClientScriptBlock("alert", myScript); 
        /// return;
        /// </code>
        /// </remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgIndexList_UpdateCommand(object source, DataGridCommandEventArgs e) {
            try {
                // Create a new index...
                IndexBLLC theIndex = new IndexBLLC();
                theIndex.Name = e.Item.Cells[2].Text;
                theIndex.IndexDate = Convert.ToDateTime(((TextBox) e.Item.Cells[3].Controls[0]).Text);
                theIndex.IndexValue = Convert.ToDecimal(((TextBox) e.Item.Cells[4].Controls[0]).Text);

                try {
                    if (
                        !BillingFactory.UpdateIndex(
                             theIndex,
                             Convert.ToInt32(Session["UserLoginID"]),
                             Convert.ToDateTime(Session["orgFromDate"]))) {
                        string myScript = "<script language=Javascript>alert('Failed to update index.');</script>";
                        Page.RegisterClientScriptBlock("alert", myScript);
                        dgIndexList.EditItemIndex = -1;
                        return;
                    }

                    // Try to update the index in the datatable that is linked to the datagrid...
                    object[] indexArray = new object[3] {theIndex.Name, theIndex.IndexDate, theIndex.IndexValue};

                    // Remove updated row (previous to update)...
                    object[] oRemoveArray = new object[2] {theIndex.Name, Convert.ToDateTime(Session["orgFromDate"])};
                    indexTable.Rows.Remove(indexTable.Rows.Find(oRemoveArray));

                    // Add updated row...
                    indexTable.LoadDataRow(indexArray, true);
                } catch {
                    string myScript = "<script language=Javascript>alert('Failed to update index.');</script>";
                    Page.RegisterClientScriptBlock("alert", myScript);
                    dgIndexList.EditItemIndex = -1;
                    return;
                }
            } catch {
                string myScript = "<script language=Javascript>alert('Check date and number values.');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
                dgIndexList.EditItemIndex = -1;
                return;
            }

            // reset code for datagrid...
            dgIndexList.EditItemIndex = -1;
            ResetValidation();

            dgIndexList.DataSource = indexTable;
            dgIndexList.DataBind();
        }

        /// <summary>
        /// For index updates
        /// </summary>
        /// <remarks>
        /// <code>
        /// dgIndexList.EditItemIndex = e.Item.ItemIndex;
        ///	
        /// // Set validation for DataGrid...
        /// validateAmount.Enabled = false;
        /// validateBeginDate.Enabled = false;
        /// validateEndDate.Enabled = false;
        /// validatePercent.Enabled = false;
        /// validateIndexName.Enabled = false;
        /// validateAmountAgainstPercent.Enabled = false;
        /// validateCompareDates.Enabled = false;
        ///	
        /// // Disable the insert textboxes...
        /// txtAmount.Enabled = false;
        /// txtIndexName.Enabled = false;
        /// txtDateFrom.Enabled = false;
        /// txtDateTo.Enabled = false;
        /// txtPercent.Enabled = false;
        /// chkIsIndexOpen.Enabled = false;
        ///	
        /// dgIndexList.DataSource = indexTable;
        /// dgIndexList.DataBind();	
        /// </code>
        /// </remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgIndexList_EditCommand(object source, DataGridCommandEventArgs e) {
            dgIndexList.EditItemIndex = e.Item.ItemIndex;

            Session["orgFromDate"] = Convert.ToDateTime(e.Item.Cells[3].Text);

            // Set validation for DataGrid...
            validateBeginDate.Enabled = false;
            validatePercent.Enabled = false;

            // Disable the insert textboxes...
            txtIndexDate.Enabled = false;
            txtIndexValue.Enabled = false;

            dgIndexList.DataSource = indexTable;
            dgIndexList.DataBind();
        }

        /// <summary>
        /// Used to reset the form for new index insertion.
        /// </summary>
        /// <remarks>When the form is in "update" mode (through the datagrid) it disables all the validators and
        /// disables all the textfields. This function resets both the validators and the textfields.</remarks>
        private void ResetValidation() {
            // Reset validation functions so they validate the "new item" textboxes...
            validateBeginDate.Enabled = true;
            validatePercent.Enabled = true;

            // Enable the insert textboxes...
            txtIndexDate.Enabled = true;
            txtIndexValue.Enabled = true;
        }

        private void FillDataGrid() {
            indexTable = BillingFactory.GetIndexDetailsAsDataTable(Convert.ToString(Session["IndexNameForDetails"]));
            dgIndexList.DataSource = indexTable;
            dgIndexList.DataBind();
            (ddlIndexes.Items.FindByText(Convert.ToString(Session["IndexNameForDetails"]))).Selected = true;
            Session["IndexNameForDetails"] = null;

            if (indexTable.Rows.Count < 0) {
                outerGridTable.Visible = false;
            } else {
                outerGridTable.Visible = true;
            }
        }

        private void ddlIndexes_SelectedIndexChanged(object sender, EventArgs e) {
            Session["IndexNameForDetails"] = ddlIndexes.SelectedItem.Text;
            FillDataGrid();
        }

        private void dgIndexList_ItemDataBound(object sender, DataGridItemEventArgs e) { WebDesign.CreateExplanationIcons(dgIndexList.Columns, lblDatagridIcons, rm, ci); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ddlIndexes.SelectedIndexChanged += new System.EventHandler(this.ddlIndexes_SelectedIndexChanged);
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            this.dgIndexList.CancelCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgIndexList_CancelCommand);
            this.dgIndexList.EditCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgIndexList_EditCommand);
            this.dgIndexList.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgIndexList_SortCommand);
            this.dgIndexList.UpdateCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgIndexList_UpdateCommand);
            this.dgIndexList.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgIndexList_DeleteCommand);
            this.dgIndexList.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgIndexList_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}