#region

using System;
using System.Configuration;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    ///		Summary description for ReportCtrl.
    /// </summary>
    public class ReportCtrl : GroundCtrl {
        private bool _isBillable;
        protected Button btnSearch;
        protected DropDownList ddlDetailLevels;
        protected HyperLink hlExcel;
        protected Label lblDetailLevel;
        protected Label lblIsBillable;
        protected Label lblMessage;
        protected Label lblPageHeader;
        protected Label lblPeriod_from;
        protected Label lblPeriod_to;
        protected Label lblSubscriberID;
        protected RadioButtonList rblIsBillable;
        protected HtmlTableCell tdIsBillable;
        protected TextBox txtPeriodFrom;
        protected TextBox txtPeriodTo;
        protected TextBox txtSubscriberID;

        private void Page_Load(object sender, EventArgs e) {
            base.PageLoad(sender, e);

            LocalizeText();
            lblMessage.Visible = false;
            txtPeriodFrom.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString();
            txtPeriodTo.Text =
                new DateTime(
                    DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).
                    ToShortDateString();

            try {
                _isBillable = Convert.ToBoolean(CigConfig.Configure("lookupsettings.IsBillableActive"));
            } catch {
                _isBillable = false;
                rblIsBillable.SelectedIndex = -1;
            }
            tdIsBillable.Visible = _isBillable;
        }

        /// <summary>
        /// For text localization. Text is set by using resource files that are XML based.
        /// </summary>
        protected override void LocalizeText() {
            base.LocalizeText();

            lblDetailLevel.Text = rm.GetString("ReportCtrl.lblDetailLevel", ci);
            lblPageHeader.Text = rm.GetString("ReportCtrl.lblPageHeader", ci);
            lblPeriod_from.Text = rm.GetString("ReportCtrl.lblPeriod_from", ci);
            lblPeriod_to.Text = rm.GetString("ReportCtrl.lblPeriod_to", ci);
            lblSubscriberID.Text = rm.GetString("ReportCtrl.lblSubscriberID", ci);
            lblIsBillable.Text = rm.GetString("ReportCtrl.lblBillingEvents", ci);
            rblIsBillable.Items[0].Text = rm.GetString("ReportCtrl.lblAllEvents", ci);
            rblIsBillable.Items[1].Text = rm.GetString("ReportCtrl.lblBilledEvents", ci);

            btnSearch.Text = rm.GetString("ReportCtrl.btnSearch", ci);
            hlExcel.Text = rm.GetString("ReportCtrl.hlExcel", ci);
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            if (txtPeriodFrom.Text.Length <= 0 && txtPeriodTo.Text.Length <= 0) {
                lblMessage.Visible = true;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Text = "You have to define a date range.";
                return;
            }

            int iSubscriberID = -1;

            if (txtSubscriberID.Text.Length > 0) {
                iSubscriberID = Convert.ToInt32(txtSubscriberID.Text);
            }

            int iIsBillable;
            if (_isBillable) {
                try {
                    iIsBillable = Convert.ToInt32(rblIsBillable.SelectedValue);
                } catch {
                    iIsBillable = -1;
                }
            } else {
                iIsBillable = -1;
            }

            try {
                string excelOut = "ExcelOutput";

                string path = Server.MapPath(Request.ApplicationPath + @"\Billing");
                if(!System.IO.Directory.Exists(string.Format(@"{0}\{1}", path, excelOut)))
                    System.IO.Directory.CreateDirectory(string.Format(@"{0}\{1}", path, excelOut));

                hlExcel.NavigateUrl += @"~/billing/" + excelOut + "/" +
                                       BillingFactory.MakeBillingExcelFile(
                                           path,
                                           iSubscriberID,
                                           Convert.ToDateTime(txtPeriodFrom.Text),
                                           Convert.ToDateTime(txtPeriodTo.Text),
                                           Convert.ToInt32(ddlDetailLevels.SelectedValue),
                                           iIsBillable);
            } catch (Exception ex) {
                Logger.WriteToLog("ReportCtrl:btnSearch_Click", ex, true);
                throw ex;
            }

            //Include a link to the Excel file.
            hlExcel.Visible = true;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}