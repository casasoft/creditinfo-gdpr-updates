#region

using System;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;
using Logging.BLL;

#endregion

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    ///		CurrencyListCtrl is used for currency manipulation, insertion and deletion.
    /// </summary>
    /// <remarks>CurrencyListCtrl is used for currency manipulation, insertion and deletion.</remarks>
    public class CurrencyListCtrl : GroundCtrl {
        protected Button btnInsert;
        protected DataGrid dgCurrencyList;
        protected Label lblCurrencyCode;
        protected Label lblCurrencyDescriptionEN;
        protected Label lblCurrencyDescriptionNative;
        protected Label lblCurrencyListHeader;
        protected Label lblDatagridIcons;
        protected Label lblMessage;
        protected Label lblPageHeader;
        protected HtmlTable outerGridTable;
        protected TextBox txtCurrencyCode;
        protected TextBox txtCurrencyDescEN;
        protected TextBox txtCurrencyDescNative;

        /// <summary>Called when page is loaded. Used to initialize the page and set default values</summary>
        /// <remarks>Sets rm and ci, static variables. Calls LocalizeText method.</remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void Page_Load(object sender, EventArgs e) {
            PageLoad(sender, e);
            LocalizeText();
            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();

            if (!IsPostBack) {
                GetCurrencyList();
            }
        }

        /// <summary>Called each time the page is loaded. Localizes all displayed text</summary>
        /// <remarks>
        /// Uses the rm.GetString method to set language according to the current
        /// culture settings. Resource files must be available for each languge that can be set. Example:
        /// <code>
        ///		public void LocalizeText()
        ///		{
        ///			this.lblBilling.Text = rm.GetString("lblBilling",ci);
        ///		}
        /// </code>
        /// </remarks>
        protected override void LocalizeText() {
            // Labels
            lblCurrencyCode.Text = rm.GetString("lblCurrencyCode", ci);
            lblCurrencyDescriptionEN.Text = rm.GetString("lblCurrencyDescriptionEN", ci);
            lblCurrencyDescriptionNative.Text = rm.GetString("lblCurrencyDescriptionNative", ci);
            lblPageHeader.Text = rm.GetString("CurrencyListCtrl.lblPageHeader", ci);

            // Datagrid textar
            // this.dgCurrencyList.ShowHeader = false;
            dgCurrencyList.Columns[2].HeaderText = rm.GetString("lblCurrencyCode", ci);
            dgCurrencyList.Columns[3].HeaderText = rm.GetString("lblCurrencyDescriptionEN", ci);
            dgCurrencyList.Columns[4].HeaderText = rm.GetString("lblCurrencyDescriptionNative", ci);

            // Buttons
            btnInsert.Text = rm.GetString("btnInsert", ci);
        }

        /// <summary>
        /// Add an enter event to the textboxes of the current form
        /// </summary>
        private void AddEnterEvent() {
            txtCurrencyCode.Attributes.Add("onkeypress", "checkEnterKey();");
            txtCurrencyDescEN.Attributes.Add("onkeypress", "checkEnterKey();");
            txtCurrencyDescNative.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        /// <summary>
        /// Gets the currency data from the database (through the factory class) and links it to
        /// a datagrid
        /// </summary>
        /// <remarks>Gets the currency data from the database (through the factory class) and links it to
        /// a datagrid. Errors are displayed via label and resource text (lblCurrencyListHeader_Error).</remarks>
        private void GetCurrencyList() {
            try {
                lblCurrencyListHeader.Text = rm.GetString("lblCurrencyListHeader", ci);

                DataTable myTable = BillingFactory.GetCurrencyListAsDataTable();
                dgCurrencyList.DataSource = myTable;
                dgCurrencyList.DataBind();

                outerGridTable.Visible = myTable.Rows.Count >= 1;
            } catch {
                // Display error message if data connection failed. Error message is red.
                lblCurrencyListHeader.Text = rm.GetString("lblCurrencyListHeader_Error", ci);
                lblCurrencyListHeader.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// Inserts a new currency instance via the Factory class
        /// </summary>
        /// <remarks>Creates a CurrencyBLLC instance for insertion. CurrencyBLLC constructor used for
        /// setting data for the instance. CurrencyBLLC instance sent to the database through the
        /// Factory class. Errors are displayed via label and resource text (lblMessage_InsertTruncate,
        /// lblMessage_InsertError & lblMessage_Length).
        /// </remarks>
        /// <param name="sender">oject</param>
        /// <param name="e">System.EventArgs</param>
        private void btnInsert_Click(object sender, EventArgs e) {
            // All fields must contain data
            if (txtCurrencyCode.Text.Length > 2 && txtCurrencyDescNative.Text.Length > 0) {
                var myCurrency = new CurrencyBLLC(
                    txtCurrencyCode.Text.ToUpper(), txtCurrencyDescEN.Text, txtCurrencyDescNative.Text);

                try {
                    lblMessage.Visible = false;

                    if (!BillingFactory.InsertCurrency(myCurrency, Convert.ToInt32(Session["UserLoginID"]))) {
                        lblMessage.Text = "* " + rm.GetString("lblMessage_InsertTruncate", ci);
                        lblMessage.ForeColor = Color.Red;
                        lblMessage.Visible = true;
                    }
                } catch {
                    // Display error message if data connection failed. Error message is red.
                    lblMessage.Text = "* " + rm.GetString("lblMessage_InsertError", ci);
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Visible = true;
                    return;
                }

                GetCurrencyList();
                ClearBoxes();
            } else {
                lblMessage.Text = "* " + rm.GetString("lblMessage_Length", ci);
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
                return;
            }
        }

        /// <summary>
        /// Deletes one currency form the ddb and updates the currency datagrid.
        /// </summary>
        /// <remarks>Deletes one currency form the ddb and updates the currency datagrid.</remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgCurrencyList_DeleteCommand(object source, DataGridCommandEventArgs e) {
            if (!BillingFactory.DeleteCurrency(e.Item.Cells[2].Text)) {
                const string myScript = "<script language=Javascript>alert('Could not delete. A probable cause is a foreign key constraint in the database.');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
            } else {
                GetCurrencyList();
            }

            dgCurrencyList.EditItemIndex = -1;
        }

        /// <summary>
        /// For currency updates.
        /// </summary>
        /// <remarks>
        /// <code>
        /// dgCurrencyList.EditItemIndex = e.Item.ItemIndex;
        /// GetCurrencyList();
        /// </code>
        /// </remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgCurrencyList_EditCommand(object source, DataGridCommandEventArgs e) {
            dgCurrencyList.EditItemIndex = e.Item.ItemIndex;
            GetCurrencyList();
        }

        /// <summary>
        /// Cancel currency edit.
        /// </summary>
        /// <remarks>
        /// <code>
        /// dgCurrencyList.EditItemIndex = -1;
        /// GetCurrencyList();
        /// </code>
        /// </remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgCurrencyList_CancelCommand(object source, DataGridCommandEventArgs e) {
            dgCurrencyList.EditItemIndex = -1;
            GetCurrencyList();
        }

        /// <summary>
        /// Update currency.
        /// </summary>
        /// <remarks>
        /// <code>
        /// CurrencyBLLC myCurrency = new CurrencyBLLC(e.Item.Cells[2].Text, ((TextBox)e.Item.Cells[3].Controls[0]).Text, ((TextBox)e.Item.Cells[4].Controls[0]).Text);
        /// myFactory.UpdateCurrency(myCurrency);
        /// dgCurrencyList.EditItemIndex = -1;
        /// GetCurrencyList();
        /// </code>
        /// </remarks>
        /// <param name="source">object</param>
        /// <param name="e">System.Web.UI.WebControls.DataGridCommandEventArgs</param>
        private void dgCurrencyList_UpdateCommand(object source, DataGridCommandEventArgs e) {
            var myCurrency = new CurrencyBLLC(
                e.Item.Cells[2].Text,
                ((TextBox) e.Item.Cells[3].Controls[0]).Text,
                ((TextBox) e.Item.Cells[4].Controls[0]).Text);
            BillingFactory.UpdateCurrency(myCurrency, Convert.ToInt32(Session["UserLoginID"]));
            dgCurrencyList.EditItemIndex = -1;
            GetCurrencyList();
        }

        /// <summary>
        /// clean the control
        /// </summary>
        private void ClearBoxes() {
            txtCurrencyCode.Text = "";
            txtCurrencyDescEN.Text = "";
            txtCurrencyDescNative.Text = "";
        }

        /// <summary>
        /// For delete confirmation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgCurrencyList_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
            }

            WebDesign.CreateExplanationIcons(dgCurrencyList.Columns, lblDatagridIcons, rm, ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            this.dgCurrencyList.CancelCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCurrencyList_CancelCommand);
            this.dgCurrencyList.EditCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCurrencyList_EditCommand);
            this.dgCurrencyList.UpdateCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCurrencyList_UpdateCommand);
            this.dgCurrencyList.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCurrencyList_DeleteCommand);
            this.dgCurrencyList.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCurrencyList_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}