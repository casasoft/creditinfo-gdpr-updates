<%@ Control Language="c#" AutoEventWireup="false" Codebehind="AgreementItemsCtrl.ascx.cs" Inherits="Billing.user_controls.Business_Ctrls.AgreementItemsCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" src="CheckEnterKey.js"></script>
<script language="JavaScript" src="DatePicker.js"></script>
<script language="javascript"> 
	function checkEnterKey(path) 
	{    
		if (event.keyCode == 13) 
		{        
			event.cancelBubble = true;
			event.returnValue = false;
			document.BillingMain.AgreementItemsCtrl1_btnFinish.click(); 
		}
	} 
	
	function SetFormFocus()
	{
		if (document.BillingMain.AgreementItemsCtrl1_ddlItemList.disabled == false)
		{
			document.BillingMain.AgreementItemsCtrl1_ddlItemList.focus();
		}
	}
</script>
<body onload="SetFormFocus()">
	<form id="Agreement" method="post">
		<table width="100%">
			<tr>
				<td>
					<table class="grid_table" cellspacing="0" cellpadding="0">
						<tr>
							<th>
								<asp:label id="lblPageHeader" cssclass="HeadMain" runat="server"></asp:label></th></tr>
						<tr>
							<td>
								<table class="fields" id="tblMainRegion" cellspacing="0" cellpadding="0">
									<tr>
										<td colspan="4"><asp:label id="lblSubscriber" runat="server"></asp:label>&nbsp;-&nbsp;
											<asp:label id="lblSubscriberName" runat="server"></asp:label></td>
									</tr>
									<tr>
										<td colspan="2"><asp:label id="lblItem" runat="server"></asp:label><br>
											<asp:dropdownlist id="ddlItemList" runat="server" autopostback="True"></asp:dropdownlist></td>
										<td><asp:label id="lblBeginDate" runat="server"></asp:label><br>
											<asp:textbox id="txtBeginDate" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('AgreementItemsCtrl1_txtBeginDate', 250, 250);"
												type="button" value="...">
											<asp:label id="Label1" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="CompareBeginDate" runat="server" display="None" type="Date" operator="DataTypeCheck"
												errormessage="CompareBeginDate" controltovalidate="txtBeginDate">*</asp:comparevalidator><asp:requiredfieldvalidator id="requiredBeginDate" runat="server" display="None" errormessage="requiredBeginDate"
												controltovalidate="txtBeginDate">*</asp:requiredfieldvalidator></td>
										<td><asp:label id="lblEndDate" runat="server"></asp:label><br>
											<asp:textbox id="txtEndDate" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('AgreementItemsCtrl1_txtEndDate', 250, 250);"
												type="button" value="...">
											<asp:label id="Label2" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="CompareEndDate" runat="server" display="None" type="Date" operator="DataTypeCheck"
												errormessage="CompareEndDate" controltovalidate="txtEndDate">*</asp:comparevalidator><asp:requiredfieldvalidator id="requiredEndDate" runat="server" display="None" errormessage="requiredEndDate"
												controltovalidate="txtEndDate">*</asp:requiredfieldvalidator></td>
									</tr>
									<tr>
										<td style="WIDTH: 25%" width="175"><asp:label id="lblFixedAmount" runat="server"></asp:label><br>
											<asp:textbox id="txtFixedAmount" cssclass="rightAlign" runat="server">0</asp:textbox><asp:label id="Label3" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="CompareFixedPrice" runat="server" display="None" type="Double" operator="DataTypeCheck"
												errormessage="CompareFixedPrice" controltovalidate="txtFixedAmount">*</asp:comparevalidator></td>
										<td style="WIDTH: 25%"><asp:label id="lblFixedDiscount" runat="server"></asp:label><br>
											<asp:textbox id="txtFixedDiscount" cssclass="rightAlign" runat="server">0</asp:textbox><asp:label id="Label4" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="CompareFiexedDiscount" runat="server" display="None" type="Double" operator="DataTypeCheck"
												errormessage="CompareFiexedDiscount" controltovalidate="txtFixedDiscount">*</asp:comparevalidator></td>
									</tr>
									<tr>
										<td><asp:label id="lblMonthlyAmount" runat="server"></asp:label><br>
											<asp:label id="lblTheMonthlyAmount" runat="server" font-bold="True"></asp:label></td>
										<td><asp:label id="lblMonthlyDiscount" runat="server"></asp:label><br>
											<asp:textbox id="txtMonthlyDiscount" cssclass="rightAlign" runat="server">0</asp:textbox><asp:label id="Label5" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="CompareMonthlyDiscount" runat="server" display="None" type="Double" operator="DataTypeCheck"
												errormessage="CompareMonthlyDiscount" controltovalidate="txtMonthlyDiscount">*</asp:comparevalidator></td>
									</tr>
									<tr>
										<td><asp:label id="lblUsageAmount" runat="server"></asp:label><br>
											<asp:label id="lblTheUsageAmount" runat="server" font-bold="True"></asp:label></td>
										<td><asp:label id="lblUsageDiscount" runat="server"></asp:label><br>
											<asp:textbox id="txtUsageDiscount" cssclass="rightAlign" runat="server">0</asp:textbox><asp:label id="Label6" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="CompareUsageDiscount" runat="server" display="None" type="Double" operator="DataTypeCheck"
												errormessage="CompareUsageDiscount" controltovalidate="txtUsageDiscount">*</asp:comparevalidator></td>
									</tr>
									<tr>
										<td><asp:label id="lblCounterLink" runat="server"></asp:label><br>
											<asp:dropdownlist id="ddlCounterLink" cssclass="rammi" runat="server"></asp:dropdownlist></td>
										<td><asp:label id="lblFreezone" runat="server"></asp:label><br>
											<asp:textbox id="txtFreezone" cssclass="rightAlign" runat="server">0</asp:textbox></td>
										<td style="WIDTH: 25%"><asp:label id="lblReminderOffset" runat="server"></asp:label><br>
											<asp:textbox id="txtReminderOffset" cssclass="rightAlign" runat="server">0</asp:textbox><asp:label id="Label7" cssclass="error_text" runat="server">*</asp:label><asp:comparevalidator id="CompareOffset" runat="server" display="None" type="Integer" operator="DataTypeCheck"
												errormessage="CompareOffset" controltovalidate="txtReminderOffset">*</asp:comparevalidator></td>
										<td><asp:label id="lblIsVisible" runat="server"></asp:label><br>
											<asp:checkbox id="chkVisible" cssclass="radio" runat="server" checked="True"></asp:checkbox></td>
									</tr>
									<tr>
										<td style="HEIGHT: 42px"><asp:label id="lblNotificationEmail1" runat="server"></asp:label><br>
											<asp:textbox id="txtNotificationEmail1" runat="server" maxlength="128"></asp:textbox><asp:regularexpressionvalidator id="NotificationEmailRegularExpression" runat="server" display="None" errormessage="NotificationEmailRegularExpression"
												controltovalidate="txtNotificationEmail1" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\;([ ])*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*">*</asp:regularexpressionvalidator></td>
										<td style="HEIGHT: 42px"><asp:label id="lblNotificationEmail2" runat="server"></asp:label><br>
											<asp:textbox id="txtNotificationEmail2" runat="server" maxlength="128"></asp:textbox><asp:regularexpressionvalidator id="NotificationEmailRegularExpression2" runat="server" display="None" errormessage="NotificationEmailRegularExpression"
												controltovalidate="txtNotificationEmail2" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\;([ ])*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*">*</asp:regularexpressionvalidator></td>
										<td style="HEIGHT: 42px"><asp:label id="lblIndexLink" runat="server"></asp:label><br>
											<asp:dropdownlist id="ddlIndexLink" runat="server"></asp:dropdownlist></td>
									</tr>
									<tr>
										<td style="WIDTH: 175px" height="23"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td align="right"><img alt="Delete" src="../img/delete.gif" align="middle">&nbsp;=&nbsp;<asp:label id="lblDeleteIcon" runat="server">Delete</asp:label>
							</td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblIndexesGridHeader" runat="server">[Header]</asp:label></th></tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td><asp:datagrid id="dgIndexes" runat="server" width="45%" gridlines="None" showheader="False">
												<columns>
													<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
														commandname="Delete">
														<itemstyle cssclass="leftpadding"></itemstyle>
													</asp:buttoncolumn>
												</columns>
											</asp:datagrid></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="grid_table" id="outerGridTableLower" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblAgreementItemsHeader" runat="server"></asp:label></th></tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td><asp:datagrid id="dgItems" runat="server" width="100%" gridlines="None" cellpadding="4" backcolor="White"
												borderwidth="1px" borderstyle="None" bordercolor="Gray" allowsorting="True" autogeneratecolumns="False"
												height="100%">
												<footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<columns>
													<asp:buttoncolumn text="&lt;img src=&quot;../img/view.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
														commandname="Select">
														<itemstyle cssclass="leftpadding"></itemstyle>
													</asp:buttoncolumn>
													<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
														commandname="Delete">
														<itemstyle cssclass="nopadding"></itemstyle>
													</asp:buttoncolumn>
													<asp:boundcolumn visible="False" datafield="ID" headertext="ID">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="ItemNameNative" sortexpression="ItemNameNative" headertext="ItemNameNative">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="ItemNameEN" sortexpression="ItemNameEN" headertext="ItemNameEN">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="FixedAmount" sortexpression="FixedAmount" headertext="Fixed">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="FixedDiscountPercent" sortexpression="FixedDiscountPercent" headertext="Fixed%">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="MonthlyAmount" sortexpression="MonthlyAmount" headertext="Monthly">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="MonthlyDiscountPercent" sortexpression="MonthlyDiscountPercent" headertext="Monthly%">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="UsageAmount" sortexpression="UsageAmount" headertext="Usage">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="UsageDiscountPercent" sortexpression="UsageDiscountPercent" headertext="Usage%">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:templatecolumn sortexpression="IsVisible" headertext="Visible">
														<itemtemplate>
															<asp:checkbox id="IsVisible" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem,"IsVisible"))%>' runat="server" enabled="False" cssclass="radio">
															</asp:checkbox>
														</itemtemplate>
														<itemstyle cssclass="padding"></itemstyle>
													</asp:templatecolumn>
													<asp:boundcolumn visible="False" datafield="Tag">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
												</columns>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
											</asp:datagrid></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td>
					<table class="empty_table" cellspacing="0">
						<tr>
							<td align="left" rowspan="3"><asp:validationsummary id="ValidationSummary" runat="server" width="100%" displaymode="List"></asp:validationsummary><asp:label id="lblMessage" runat="server" visible="False"></asp:label></td>
							<td align="right"><asp:button id="btnAddIndex" cssclass="confirm_button" runat="server" causesvalidation="False"></asp:button><asp:button id="btnInsertItem" cssclass="confirm_button" runat="server" text="Insert"></asp:button></td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<tr>
							<td align="right"><asp:button id="btnCancel" cssclass="cancel_button" runat="server"></asp:button><asp:button id="lnkComments" cssclass="gray_button" runat="server" causesvalidation="False"></asp:button><asp:button id="lnkAgreement" cssclass="gray_button" runat="server" causesvalidation="False"></asp:button><asp:button id="btnSave" cssclass="confirm_button" runat="server" causesvalidation="False"></asp:button></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
