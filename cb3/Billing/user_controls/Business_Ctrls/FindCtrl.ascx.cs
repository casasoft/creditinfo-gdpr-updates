#region

using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Billing.BLL;
using Logging.BLL;

#endregion

namespace Billing.user_controls.Business_Ctrls {
    /// <summary>
    ///		Summary description for FindCtrl.
    /// </summary>
    public class FindCtrl : AgreementGroundCtrl {
        // Localization
        protected Button btnSearch;
        protected DropDownList ddlSignedBy;
        protected DataGrid dgSubscriberList;
        protected Label lblAddress;
        protected Label lblAgreementID;
        protected Label lblAgreementsOnly;
        protected Label lblCreditInfoID;
        protected Label lblDatagridIcons;
        protected Label lblFirstName;
        protected Label lblIdNumber;
        protected Label lblOpenClosed;
        protected Label lblPageHeader;
        protected Label lblRegisterdBy;
        protected Label lblSignedBy;
        protected Label lblSubscriberGridHeader;
        protected HtmlTable outerGridTable;
        protected HtmlTableRow outerGridTableInfo;
        protected RadioButtonList rblAgreementsOnly;
        protected RadioButtonList rbListOpenClosed;
        protected TextBox txtAddress;
        protected TextBox txtAgreementID;
        protected TextBox txtCreditInfoID;
        protected TextBox txtFirstName;
        protected TextBox txtIdNumber;
        protected TextBox txtRegisteredBy;

        private void Page_Load(object sender, EventArgs e) {
            PageLoad(sender, e);
            if (!Page.IsPostBack) {
                outerGridTable.Visible = false;
                outerGridTableInfo.Visible = false;

                ddlSignedBy.DataSource = BillingFactory.GetEmployees();
                ddlSignedBy.DataValueField = "CreditInfoID";
                ddlSignedBy.DataTextField = EN ? "NameEN" : "NameNative";
                ddlSignedBy.DataBind();

                ddlSignedBy.Items.Insert(0, new ListItem(" ", "-1"));
            }

            // b�ta <ENTER> event handler � textboxin			
            AddEnterEvent();
            SetGridHeaders();
            LocalizeText();
        }

        private void AddEnterEvent() {
            txtAddress.Attributes.Add("onkeypress", "checkEnterKey();");
            txtAgreementID.Attributes.Add("onkeypress", "checkEnterKey();");
            txtFirstName.Attributes.Add("onkeypress", "checkEnterKey();");
            txtIdNumber.Attributes.Add("onkeypress", "checkEnterKey();");
            txtRegisteredBy.Attributes.Add("onkeypress", "checkEnterKey();");
            txtCreditInfoID.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        protected override void LocalizeText() {
            lblIdNumber.Text = rm.GetString("lblIDNumber", ci);
            lblCreditInfoID.Text = rm.GetString("FindCtrl.lblCreditInfoID", ci);
            lblRegisterdBy.Text = rm.GetString("lblRegisterdBy", ci);
            lblFirstName.Text = rm.GetString("lblFirstName", ci);
            lblAddress.Text = rm.GetString("lblAddress", ci);
            lblOpenClosed.Text = rm.GetString("lblOpenClosed", ci);
            lblAgreementsOnly.Text = rm.GetString("FindCtrl.lblAgreementsOnly", ci);
            lblPageHeader.Text = rm.GetString("FindCtrl.lblPageHeader", ci);
            lblSignedBy.Text = rm.GetString("FindCtrl.lblSignedBy", ci);
            lblSubscriberGridHeader.Text = rm.GetString("txtResults", ci);
            lblAgreementID.Text = rm.GetString("lblAgreementID", ci);

            rbListOpenClosed.Items[0].Text = rm.GetString("txtOpen", ci);
            rbListOpenClosed.Items[1].Text = rm.GetString("txtClosed", ci);
            rbListOpenClosed.Items[2].Text = rm.GetString("txtBoth", ci);
            rblAgreementsOnly.Items[0].Text = rm.GetString("FindCtrl.lblWithAgreement", ci);
            rblAgreementsOnly.Items[1].Text = rm.GetString("FindCtrl.lblWithoutAgreement", ci);
            rblAgreementsOnly.Items[2].Text = rm.GetString("FindCtrl.lblBothAgreement", ci);

            btnSearch.Text = rm.GetString("btnSearch", ci);
        }

        private void btnSearch_Click(object sender, EventArgs e) { Search(); }

        private void Search() {
            string CreditInfoID = txtCreditInfoID.Text;
            string FirstName = txtFirstName.Text;
            string Address = txtAddress.Text;
            string RegisteredBy = txtRegisteredBy.Text;
            string AgreementID = txtAgreementID.Text;
            string OpenClosed = rbListOpenClosed.SelectedValue;
            string IDNumber = txtIdNumber.Text;
            string IsAgreementOnly = rblAgreementsOnly.SelectedValue;
            //string SignerName			= txtSignedBy.Text;
            int nSignerCIID = -1;
            try {
                nSignerCIID = int.Parse(ddlSignedBy.SelectedValue);
            } catch (Exception) {}

            DataSet mySet = BillingFactory.SearchSubscribersAndAgreements(
                CreditInfoID,
                FirstName,
                Address,
                RegisteredBy,
                AgreementID,
                OpenClosed,
                IDNumber,
                nSignerCIID,
                IsAgreementOnly);

            dgSubscriberList.DataSource = mySet;
            dgSubscriberList.DataBind();
            outerGridTable.Visible = true;
            outerGridTableInfo.Visible = true;

            Session["billing_FindCtrl_SubscriberListView"] = mySet.Tables[0];
        }

        private void dgSubscriberList_ItemCommand(object source, DataGridCommandEventArgs e) {
            AgreementUnlock = true;

            if (e.CommandName == "Update") {
                // Release the agreement that is already stored in memory
                ReleaseInMemoryAgreement();

                Agreement = BillingFactory.GetAgreement(Convert.ToInt32(e.Item.Cells[10].Text));
                UpdateInMemoryAgreement();
                Response.Redirect(@"TableUpdates.aspx?Type=Agreement");
            }

            // For new agreements
            if (e.CommandName == "Insert") {
                // Release the agreement that is already stored in memory
                ReleaseInMemoryAgreement();
                // Initialize a new agreement
                InitializeAgreement();

                // Set the subscriber for the new agreement..
                Agreement.SubscriberID = Convert.ToInt32(e.Item.Cells[5].Text);

                // Update the agreement and transfer the user...
                UpdateInMemoryAgreement();
                Response.Redirect(@"TableUpdates.aspx?Type=Agreement");
            }

            // For agreement viewing
            if (e.CommandName == "View") {
                // Release the agreement that is already stored in memory				
                ReleaseInMemoryAgreement();

                Session["ReportType"] = "BillingReport"; //reportType;
                Session["ReportCulture"] = EN ? "EN" : "Native";
                Session["CompanyCIID"] = e.Item.Cells[5].Text;
                Session["AgreementID"] = e.Item.Cells[10].Text;

                Page.RegisterClientScriptBlock(
                    "openwindow2", "<script language=\"javascript\">window.open(\"../CR/Report.aspx\")</script>");
                Search();
            }

            if (e.CommandName == "Delete") {
                Agreement = BillingFactory.GetAgreement(Convert.ToInt32(e.Item.Cells[10].Text));

                if (BillingFactory.DeleteAgreement(Agreement)) {
                    ReleaseInMemoryAgreement();
                    btnSearch_Click(null, null);
                } else {
                    const string myScript = "<script language=Javascript>alert('Could not delete - Please contact system administrator.');</script>";
                    Page.RegisterClientScriptBlock("alert", myScript);
                }
            }
        }

        private void dgSubscriberList_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[3].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";

                // If there is no agreement then don't allow the user to try to update it...
                if (e.Item.Cells[10].Text == "&nbsp;") {
                    // Hide the view button
                    e.Item.Cells[1].Text = "";
                    e.Item.Cells[1].Enabled = false;
                    //e.Item.Cells[1].Visible = false;

                    // Hide the edit button
                    e.Item.Cells[2].Text = "";
                    e.Item.Cells[2].Enabled = false;
                    //e.Item.Cells[2].Visible = false;

                    // Hide the Delete button
                    e.Item.Cells[3].Text = "";
                    e.Item.Cells[3].Enabled = false;
                    //e.Item.Cells[3].Visible = false;
                }

                if (e.Item.Cells[9].Text.ToLower().Trim() == "false") {
                    // Hide the "new" button
                    e.Item.Cells[0].Text = "";
                    e.Item.Cells[0].Enabled = false;
                }
            }

            // Localize ... not working on link buttons??
            /*	
			e.Item.Cells[0].Text = rm.GetString("txtUpdate",ci);
			e.Item.Cells[1].Text = rm.GetString("txtInsert",ci);
			e.Item.Cells[2].Text = rm.GetString("txtDelete",ci);
		*/
            WebDesign.CreateExplanationIcons(dgSubscriberList.Columns, lblDatagridIcons, rm, ci);
        }

        private void SetGridHeaders() {
            dgSubscriberList.Columns[6].HeaderText = rm.GetString("lblFirstName", ci);
            dgSubscriberList.Columns[8].HeaderText = rm.GetString("txtRegBy", ci);
            dgSubscriberList.Columns[9].HeaderText = rm.GetString("txtOpen", ci);
            dgSubscriberList.Columns[10].HeaderText = rm.GetString("lblAgreementID", ci);
            dgSubscriberList.Columns[11].HeaderText = rm.GetString("txtAgreementNumber", ci);
            dgSubscriberList.Columns[12].HeaderText = rm.GetString("txtBeginDate", ci);
            dgSubscriberList.Columns[13].HeaderText = rm.GetString("txtEndDate", ci);
            dgSubscriberList.Columns[14].HeaderText = rm.GetString("AgreementCtrl.lblCurrency", ci);
        }

        private void dgSubscriberList_SortCommand(object source, DataGridSortCommandEventArgs e) {
            var myTable = (DataTable) Session["billing_FindCtrl_SubscriberListView"];

            var myView = myTable.DefaultView;
            myView.Sort = e.SortExpression;
            dgSubscriberList.DataSource = myView;
            dgSubscriberList.DataBind();

            Session["billing_FindCtrl_SubscriberListView"] = myTable;
            myView.Dispose();
            myTable.Dispose();
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.dgSubscriberList.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgSubscriberList_ItemCommand);
            this.dgSubscriberList.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgSubscriberList_SortCommand);
            this.dgSubscriberList.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSubscriberList_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}