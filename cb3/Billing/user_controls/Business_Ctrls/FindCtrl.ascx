<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FindCtrl.ascx.cs" Inherits="Billing.user_controls.Business_Ctrls.FindCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" src="CheckEnterKey.js"></script>
<script language="JavaScript" src="DatePicker.js"></script>
<script language="javascript"> 
	function checkEnterKey(path) 
	{    
		if (event.keyCode == 13) 
		{        
			event.cancelBubble = true;
			event.returnValue = false;
			document.BillingMain.FindCtrl1_btnSearch.click(); 
		}
	} 
	
	function SetFormFocus()
	{
		document.BillingMain.FindCtrl1_txtIdNumber.focus();
	}
	
	function openwindow()			
	{
		window.open("../CR/Report.aspx", "mywindow","location=0,status=0,scrollbars=1,width=600,height=600");			
	}	
</script>
<body onload="SetFormFocus()">
	<form id="Search" method="post">
		<table width="100%">
			<tr>
				<td>
					<table class="grid_table" cellspacing="0" cellpadding="0">
						<tr>
							<th>
								<asp:label id="lblPageHeader" runat="server" ></asp:label></th></tr>
						<tr>
							<td>
								<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
									<tr>
										<td style="WIDTH: 33%"><asp:label id="lblCreditInfoID" runat="server">CreditInfo ID</asp:label><br>
											<asp:textbox id="txtCreditInfoID" runat="server"></asp:textbox></td>
										<td style="WIDTH: 33%"><asp:label id="lblRegisterdBy" runat="server">Registered by</asp:label><br>
											<asp:textbox id="txtRegisteredBy" runat="server"></asp:textbox></td>
										<td><asp:label id="lblAgreementID" runat="server">Agreement 
                  ID</asp:label><br>
											<asp:textbox id="txtAgreementID" runat="server"></asp:textbox></td>
									</tr>
									<tr>
										<td><asp:label id="lblFirstName" runat="server">Name</asp:label><br>
											<asp:textbox id="txtFirstName" runat="server"></asp:textbox></td>
										<td><asp:label id="lblAddress" runat="server">Address</asp:label><br>
											<asp:textbox id="txtAddress" runat="server"></asp:textbox></td>
										<td><asp:label id="lblIdNumber" runat="server">ID number</asp:label><br>
											<asp:textbox id="txtIdNumber" runat="server"></asp:textbox></td>
									</tr>
									<tr>
										<td><asp:label id="lblSignedBy" runat="server">Signed by</asp:label><br>
											<asp:dropdownlist id="ddlSignedBy" runat="server"></asp:dropdownlist></td>
										<td><asp:label id="lblOpenClosed" runat="server">Open / 
                  Closed</asp:label><br>
											<asp:radiobuttonlist id="rbListOpenClosed" runat="server" cssclass="radio" font-size="X-Small" height="8px"
												width="80%" repeatdirection="Horizontal">
												<asp:listitem value="Open">Open</asp:listitem>
												<asp:listitem value="Closed">Closed</asp:listitem>
												<asp:listitem value="Both" selected="True">Both</asp:listitem>
											</asp:radiobuttonlist></td>
										<td><asp:label id="lblAgreementsOnly" runat="server"></asp:label><br>
											<asp:radiobuttonlist id="rblAgreementsOnly" runat="server" cssclass="radio" width="80%" repeatdirection="Horizontal">
												<asp:listitem value="True">With</asp:listitem>
												<asp:listitem value="False">Without</asp:listitem>
												<asp:listitem value="null" selected="True">Both</asp:listitem>
											</asp:radiobuttonlist></td>
									</tr>
									<tr>
										<td align="right" colspan="4"><asp:button id="btnSearch" runat="server" cssclass="search_button" text="Search"></asp:button></td>
									</tr>
									<tr>
										<td height="23"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="10"></td>
			</tr>
			<tr id="outerGridTableInfo" runat="server">
				<td align="right">
					<div><asp:label id="lblDatagridIcons" runat="server"></asp:label></div>
				</td>
			</tr>
			<tr>
				<td>
					<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<th>
								<asp:label id="lblSubscriberGridHeader" runat="server"></asp:label></th></tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td><asp:datagrid id="dgSubscriberList" runat="server" cssclass="grid" allowsorting="True" autogeneratecolumns="False"
												gridlines="None"><footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<columns>
													<asp:buttoncolumn text='<img src="../img/new.gif" alt="New" border="0">' commandname="Insert">
														<itemstyle cssclass="leftpadding"></itemstyle>
													</asp:buttoncolumn>
													<asp:buttoncolumn text='<img src="../img/view.gif" alt="View" border="0">' commandname="View">
														<itemstyle cssclass="nopadding"></itemstyle>
													</asp:buttoncolumn>
													<asp:buttoncolumn text='<img src="../img/edit.gif" alt="Edit" border="0">' commandname="Update">
														<itemstyle cssclass="nopadding"></itemstyle>
													</asp:buttoncolumn>
													<asp:buttoncolumn text='<img src="../img/delete.gif" alt="Delete" border="0">' commandname="Delete">
														<itemstyle cssclass="nopadding"></itemstyle>
													</asp:buttoncolumn>
													<asp:boundcolumn headertext="SubscriberID" sortexpression="SubscriberID" datafield="SubscriberID" visible="False">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="CreditInfoID" sortexpression="CreditInfoID" datafield="CreditInfoID" visible="False">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="Name" sortexpression="NameNative" datafield="NameNative">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="Address" sortexpression="StreetNative" datafield="StreetNative" visible="False">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="Reg. By" sortexpression="InsertedBy" datafield="InsertedBy">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="Open" sortexpression="IsOpen" datafield="IsOpen">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="AgreementID" sortexpression="AgreementID" datafield="AgreementID" visible="False">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="AgreementNumber" sortexpression="AgreementNumber" datafield="AgreementNumber">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="BeginDate" sortexpression="BeginDate" datafield="BeginDate" dataformatstring="{0:d}">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="EndDate" sortexpression="EndDate" datafield="EndDate" dataformatstring="{0:d}">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn headertext="Currency" sortexpression="Currency" datafield="Currency" visible="False">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
												</columns>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
											</asp:datagrid></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
