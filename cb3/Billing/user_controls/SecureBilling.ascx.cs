#region

using System;
using System.Web.UI;

#endregion

namespace Billing.user_controls {
    /// <summary>
    /// SecureBilling class. Security User Control for Billing project.
    /// </summary>
    /// <example>
    /// This User control is used by declaring it in the HTML part of each web form that resides in the
    /// skstk assembly.
    /// </example>
    /// <remarks>
    /// Secures the web forms of the Billing assembly by cheking the roles for the current user. The
    /// roles decide which products the user can access (according to the CreditInfoGroup product database).
    /// Roles for the CreditInfoGroup_IS system are defined as:
    /// <list type="bullet">
    /// <item>Role 1:		Administration</item>
    /// <item>Role 2:		Negative Payments</item>
    /// <item>Role 3:		Credit Watch</item>
    /// <item>Role 4:		Claim search</item>
    /// <item>Role 5:		Development</item>
    /// <item>Role 6:		Dept Information System (skstk)</item>
    /// <item>Role 7:		Billing</item>
    /// </list>
    /// </remarks>
    public class SecureBilling : UserControl {
        private void Page_Load(object sender, EventArgs e) {
            /*
			Role 100:		Administration
			Role 200:		Negative Payments
			Role 300:		Credit Watch
			Role 400:		Claim search
			Role 500:		Development
			Role 600:		Dept Information System (skstk)
			Role 700:		Billing
			*/

            if (!Context.User.IsInRole("700")) {
                Server.Transfer("../NoAuthorization.aspx");
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new EventHandler(this.Page_Load); }

        #endregion
    }
}