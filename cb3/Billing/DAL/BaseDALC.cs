#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using Logging.BLL;
using cb3;
using cb3.Audit;

#endregion

namespace Billing.DAL {
    /// <summary>
    /// This is a base class for database access. It impliments generic data access functions for other classes to use.
    /// </summary>
    /// <remarks>
    /// All Data Access Calls are made through this class. 
    /// </remarks>
    public class BaseDALC {
        #region Class logic (this)

        /// <summary>
        /// Used for class name in error handling
        /// </summary>
        private string strClassName;

        /// <summary>
        /// Used for function name in error handling.
        /// </summary>
        private string strFunctionName;

        /// <summary>
        /// Constructor - Sets the (static) connection string for Data Access.
        /// <seealso href="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnnetsec/html/SecNetHT08.asp">MSDN - DataProtector Assembly</seealso> 
        /// </summary>
        /// <remarks>
        /// DatabaseHelper.ConnectionString() decrypts the encrypted connection string that is registered in web.config. The decrypted
        /// string is used to connect to the ddb.
        /// The ConnectionString method uses DataProtector.dll to encrypt and decrypt data. Data can be encrypted 
        /// using the USER_STORE or the MACHINE_STORE. This application uses the MACHINE_STORE so each setup is
        /// machine dependent for an initially decrypted connection string which is created outside of the system.
        /// </remarks>
        public BaseDALC() {
        }

        public string ClassName { get { return strClassName; } set { strClassName = value; } }
        public string FunctionName { get { return strFunctionName; } set { strFunctionName = value; } }

        #endregion

        /// <summary>
        /// OpenConnection opens a connection to a database
        /// </summary>
        /// <remarks>
        /// This function is defined as virtual. This means that child classes can override this function
        /// if any of them would like to use another connection method or connetion string. The calling of this
        /// function should be followed with a connection close and dispose statements for the returned connection.
        /// </remarks>
        /// <returns>OleDbConnection - Open connection</returns>
        public virtual OleDbConnection OpenConnection() {
            OleDbConnection connection = new OleDbConnection(DatabaseHelper.ConnectionString());
            connection.Open();

            return connection;
        }

        /// <summary>
        /// LoadData returns data according to the supplied SQL statement
        /// </summary>
        /// <remarks>
        /// LoadData is used to return data from the database. The supplied SQL statement is used to select data. If
        /// some error occurs then the function returns "null".
        /// </remarks>
        /// <param name="strSQL">The SQL string (usually a SELECT statement) that is used.</param>
        /// <returns>DataSet.</returns>
        public DataSet LoadDataSet(String strSQL) {
            DataSet ds = null;
            OleDbConnection connection = null;
            OleDbCommand command = null;
            OleDbDataAdapter adapter = null;

            try {
                connection = OpenConnection();

                command = new OleDbCommand(strSQL, connection);
                adapter = new OleDbDataAdapter(command);

                ds = new DataSet();
                adapter.Fill(ds);
                //adapter.FillSchema(ds, SchemaType.Source);

                connection.Close();
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                throw ex;
            } finally {
                if (adapter != null) {
                    adapter.Dispose();
                }
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Dispose();
                }
            }

            return ds;
        }

        /// <summary>
        /// LoadData returns data according to the supplied SQL statement
        /// </summary>
        /// <remarks>
        /// LoadData is used to return data from the database. The supplied SQL statement is used to select data. If
        /// some error occurs then the function returns "null".
        /// </remarks>
        /// <param name="strSQL">The SQL string (usually a SELECT statement) that is used.</param>
        /// <returns>DataTable.</returns>
        public DataTable LoadDataTable(String strSQL) {
            DataTable dt = null;
            OleDbConnection connection = null;
            OleDbCommand command = null;
            OleDbDataAdapter adapter = null;

            try {
                connection = OpenConnection();

                command = new OleDbCommand(strSQL, connection);
                adapter = new OleDbDataAdapter(command);

                dt = new DataTable();
                adapter.Fill(dt);
                adapter.FillSchema(dt, SchemaType.Source);

                connection.Close();
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                throw ex;
            } finally {
                if (adapter != null) {
                    adapter.Dispose();
                }
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Dispose();
                }
            }

            return dt;
        }

        /// <summary>
        /// ExecuteSQL executes the SQL statement that it takes as a parameter
        /// </summary>
        /// <remarks>Executes the SQL statement supplied as a parameter (f.x. a DELETE statement).</remarks>
        /// <param name="strSQL">SQL statement</param>
        /// <returns>true if execution was successful, otherwise false.</returns>
        public bool ExecuteSQL(String strSQL) {
            OleDbConnection connection = null;
            OleDbCommand command = null;

            try {
                connection = OpenConnection();

                command = new OleDbCommand(strSQL, connection);
                new AuditFactory(command).PerformAuditProcess(); command.ExecuteNonQuery();

                connection.Close();

                return true;
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                return false;
            } finally {
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Dispose();
                }
            }
        }

        /// <summary>
        /// ExecuteSQL executes the SQL statement that it takes as a parameter
        /// </summary>
        /// <remarks>Executes the SQL statement supplied as a parameter (f.x. a DELETE statement).</remarks>
        /// <param name="strSQL">SQL statement</param>
        /// <returns>true if execution was successful, otherwise false.</returns>
        public bool ExecuteSQL(String strSQL, OleDbCommand command) {
            OleDbConnection connection = null;

            try {
                connection = OpenConnection();

                command.Connection = connection;
                command.CommandText = strSQL;
                new AuditFactory(command).PerformAuditProcess(); command.ExecuteNonQuery();

                connection.Close();

                return true;
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                return false;
            } finally {
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Dispose();
                }
            }
        }

        /// <summary>
        /// ExecuteSQL executes the SQL statement that it takes as a parameter
        /// </summary>
        /// <remarks>Executes the SQL statement supplied as a parameter (f.x. a DELETE statement).</remarks>
        /// <param name="strSQL">SQL statement</param>
        /// <returns>true if execution was successful, otherwise false.</returns>
        public bool ExecuteSQL(String strSQL, OleDbCommand command, OleDbConnection connection) {
            try {
                command.Connection = connection;
                command.CommandText = strSQL;
                new AuditFactory(command).PerformAuditProcess(); command.ExecuteNonQuery();

                return true;
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                return false;
            }
        }

        /// <summary>
        /// ExecuteSQL executes the SQL statement that it takes as a parameter
        /// </summary>
        /// <remarks>Executes the SQL statement supplied as a parameter (f.x. a DELETE statement).</remarks>
        /// <param name="strSQL">SQL statement</param>
        /// <returns>true if execution was successful, otherwise false.</returns>
        public int ExecuteScalarSQL(String strSQL, OleDbCommand command, OleDbConnection connection) {
            try {
                command.Connection = connection;
                command.CommandText = strSQL;
                return Convert.ToInt32(command.ExecuteScalar());
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                return -1;
            }
        }

        /// <summary>
        /// ExecuteSQL executes the SQL statement that it takes as a parameter
        /// </summary>
        /// <remarks>Executes the SQL statement supplied as a parameter.</remarks>
        /// <param name="strSQL">SQL statement</param>
        /// <returns>ArrayList</returns>
        public ArrayList ExecuteSQLReaderArrayList(String strSQL) {
            OleDbConnection connection = null;
            OleDbCommand command = new OleDbCommand();
            OleDbDataReader reader = null;
            ArrayList theArrayList = new ArrayList();

            try {
                connection = OpenConnection();

                command.Connection = connection;
                command.CommandText = strSQL;

                reader = command.ExecuteReader();

                while (reader.Read()) {
                    if (!reader.IsDBNull(0)) {
                        theArrayList.Add(reader.GetString(0));
                    }
                }
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                throw ex;
            } finally {
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Close();
                    connection.Dispose();
                }
                if (reader != null) {
                    reader.Close();
                }
            }

            return theArrayList;
        }

        /// <summary>
        /// Creates a parameter for an INSERT/UPDATE statement.
        /// </summary>
        /// <remarks>Creates a parameter for an INSERT/UPDATE statement.</remarks>
        /// <param name="command">OleDbCommand - parameter parent command</param>
        /// <param name="strName">Variable name - both for source column and variable</param>
        /// <param name="oValue">The entered value</param>
        /// <param name="paramDirect">Parameter direction</param>
        /// <param name="type">Column datatype</param>
        /// <returns></returns>
        public OleDbParameter CreateParameter(
            OleDbCommand command, String strName, ParameterDirection paramDirect, object oValue, OleDbType type) {
            OleDbParameter param = command.Parameters.Add(strName, type);
            param.Direction = paramDirect;
            param.Value = oValue;
            return param;
        }

        /// <summary>
        /// Creates a parameter for an INSERT/UPDATE statement. For EN and Native strings. If both strings have values then they're both
        /// created as parameters. If only one has values then it's text is set, by default, into both fields in the db.
        /// </summary>
        /// <remarks>Creates a parameter for an INSERT/UPDATE statement.</remarks>
        /// <param name="command">OleDbCommand - parameter parent command</param>
        /// <param name="strNameNative">Variable name - both for source column and variable</param>
        /// <param name="strNameEN">Variable name - both for source column and variable</param>
        /// <param name="oValueNative">The entered native value</param>
        /// <param name="oValueEN">The entered EN value</param>
        /// <param name="paramDirect">Parameter direction</param>
        /// <param name="type">Column datatype</param>
        /// <returns></returns>
        public void CreateParameterFromString(
            OleDbCommand command,
            String strNameNative,
            String strNameEN,
            ParameterDirection paramDirect,
            object oValueNative,
            object oValueEN,
            OleDbType type) {
            OleDbParameter paramNative = command.Parameters.Add(strNameNative, type);
            OleDbParameter paramEN = command.Parameters.Add(strNameEN, type);
            paramNative.Direction = paramDirect;
            paramEN.Direction = paramDirect;

            // If both values are null then we'll enter them both as null to the database
            if (oValueNative == null && oValueEN == null) {
                paramNative.Value = DBNull.Value;
                paramEN.Value = DBNull.Value;
                return;
            }

            // If either value (but not both) is null we'd like to convert it into an empty string
            if (oValueNative == null) {
                oValueNative = "";
            }

            if (oValueEN == null) {
                oValueEN = "";
            }

            // Check the stings and switch them if either is an empty string.
            if (oValueNative.ToString().Trim() != string.Empty) {
                paramNative.Value = oValueNative;
            } else if (oValueEN.ToString().Trim() != string.Empty) {
                paramNative.Value = oValueEN;
                // oValueNative = oValueEN;
            }

            if (oValueEN.ToString().Trim() != string.Empty) {
                paramEN.Value = oValueEN;
            } else if (oValueNative.ToString().Trim() != string.Empty) {
                paramEN.Value = oValueNative;
                // oValueEN = oValueNative;
            }
        }

        /// <summary>
        /// Creates a parameter for an INSERT/UPDATE statement.
        /// </summary>
        /// <remarks>Creates a parameter for an INSERT/UPDATE statement.</remarks>
        /// <param name="command">OleDbCommand - parameter parent command</param>
        /// <param name="strName">Variable name - both for source column and variable</param>
        /// <param name="oValue">The entered value</param>
        /// <param name="paramDirect">Parameter direction</param>
        /// <param name="type">Column datatype</param>
        /// <param name="strDefault">Default value</param>
        /// <returns></returns>
        public OleDbParameter CreateParameterDefValue(
            OleDbCommand command,
            String strName,
            ParameterDirection paramDirect,
            object oValue,
            OleDbType type,
            string strDefault,
            string strCompare) {
            OleDbParameter param = command.Parameters.Add(strName, type);
            param.Direction = paramDirect;

            // If comparison is successful then we'd like to insert the default value instead of the value contained
            // in the object class
            if (oValue.ToString() == strCompare) {
                // If the default value is null then we'll send it straight down to the ddb
                if (strDefault == null) {
                    param.Value = strDefault;
                }
                    // Else we'll check the input type and handle the default string accordingly...
                else {
                    switch (type.ToString()) {
                        case "Integer":
                            param.Value = Convert.ToInt32(strDefault);
                            break;
                        case "VarChar":
                            param.Value = strDefault;
                            break;
                    }
                }
                //param.Value	= strDefault != null ? Convert.ToInt32(strDefault) : strDefault;
            } else {
                param.Value = oValue;
            }

            return param;
        }
    }
}