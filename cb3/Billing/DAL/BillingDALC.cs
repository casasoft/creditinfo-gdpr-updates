#region

using System;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using Billing.BLL;
using Logging.BLL;
using cb3;

#endregion

namespace Billing.DAL {
    /// <summary>
    /// This class handles all neccessary database handling for the billing part
    /// </summary>
    /// <remarks>
    /// All Data Access Calls are made through this class. It is composed into one class since
    /// all Data Access is related for this system component. The class accesses the following tables:
    /// <list type="bullet">
    /// <item>
    /// <description>billing_Currency (Currency list)</description>
    /// <description>billing_Agreement (main agreement table)</description>
    /// <description>billing_AgreementComments(Comments related to agreements)</description>
    /// <description>billing_AgreementItems (All items for a specific agreement)</description>
    /// <description>billing_itemCounterLink(For special item counting situations)</description>
    /// <description>billing_IndexesOnAgreementItems (Index table linked to agreement item table)</description>
    /// <description>billing_Indexes (Index list)</description>
    /// <description>billing_IndexDetails (Details for each index)</description>
    /// <description>np_UsageTypes (For usage type id's)</description>
    /// <description>au_Products (Product base)</description>
    /// <description>au_Subscribers (Subscriber list)</description>
    /// </item>
    /// </list>
    /// </remarks>
    public class BillingDALC : BaseDALC {
        #region Class logic (this)

        #endregion

        #region Currency region

        /// <summary>
        /// Returns a complete list of currencies as DataSet.
        /// </summary>
        /// <remarks>
        /// Returns all fields and rows from the table billing_Currency. The fields are:
        /// <list type="bullet">
        /// <item>CurrencyCode (nvarchar, 3)</item>
        /// <item>CurrencyDescriptionEN (nvarchar, 50)</item>
        /// <item>CurrencyDescriptionNative (nvarchar, 50)</item>
        /// </list>
        /// </remarks>
        /// <returns>Complete currency list (all fields) as DataTable</returns>
        public DataTable GetCurrencyListAsDataTable() {
            FunctionName = "DataTable GetCurrencyListAsDataTable()";

            string strCommand = "SELECT * FROM billing_Currency ORDER BY CurrencyCode";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Inserts one row into the billing_Currency datatable.
        /// </summary>
        /// <remarks>Inserts one row into the billing_Currency datatable.</remarks>
        /// <param name="CurrencyCode">Key value for billing_Currency table. Used to find the correct row to delete</param>
        /// <param name="DescNative">Native currency description</param>
        /// <param name="DescEN">Engilsh currency description</param>
        /// <param name="UserID">ID of the user requesting the update</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool InsertCurrency(string CurrencyCode, string DescNative, string DescEN, int UserID) {
            // Define function name for error handling
            FunctionName = "InsertCurrency (string CurrencyCode, string DescNative, string DescEN, int UserID)";

            // Define the SQL statement to execute
            string strCommand =
                "INSERT INTO billing_Currency (CurrencyCode, CurrencyDescriptionNative, CurrencyDescriptionEN, Inserted, InsertedBy) Values (?,?,?,?,?)";

            // Define a command object
            OleDbCommand command = new OleDbCommand();
            // Along with parameters
            CreateParameter(command, "CurrencyCode", ParameterDirection.Input, CurrencyCode, OleDbType.VarWChar);
            CreateParameterFromString(
                command,
                "CurrencyDescriptionNative",
                "CurrencyDescriptionEN",
                ParameterDirection.Input,
                DescNative,
                DescEN,
                OleDbType.VarWChar);
            CreateParameter(
                command, "Inserted", ParameterDirection.Input, DateTime.Now.ToShortDateString(), OleDbType.Date);
            CreateParameter(command, "InsertedBy", ParameterDirection.Input, UserID, OleDbType.Integer);

            return ExecuteSQL(strCommand, command);
        }

        /// <summary>
        /// Updates currency information for one given currency.
        /// </summary>
        /// <remarks>Updates currency information for one given currency.</remarks>
        /// <param name="CurrencyCode">Key value for billing_Currency table. Used to find the correct row to update</param>
        /// <param name="DescNative">Native currency description</param>
        /// <param name="DescEN">Engilsh currency description</param>
        /// <param name="UserId">ID of the user requesting the update</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdateCurrency(string CurrencyCode, string DescNative, string DescEN, int UserId) {
            // Define function name for error handling
            FunctionName = "UpdateCurrency (string CurrencyCode, string DescNative, string DescEN, int UserId)";

            // Define the SQL statement to execute
            string strCommand =
                "UPDATE billing_Currency SET CurrencyDescriptionNative = ?, CurrencyDescriptionEN = ?, Updated = ?, UpdatedBy = ? WHERE CurrencyCode = ?";

            // Define a command object
            OleDbCommand command = new OleDbCommand();
            // Along with parameters
            CreateParameterFromString(
                command,
                "CurrencyDescriptionNative",
                "CurrencyDescriptionEN",
                ParameterDirection.Input,
                DescNative,
                DescEN,
                OleDbType.VarWChar);
            CreateParameter(
                command, "Updated", ParameterDirection.Input, DateTime.Now.ToShortDateString(), OleDbType.Date);
            CreateParameter(command, "UpdatedBy", ParameterDirection.Input, UserId, OleDbType.Integer);
            CreateParameter(command, "CurrencyCode", ParameterDirection.Input, CurrencyCode, OleDbType.VarWChar);

            return ExecuteSQL(strCommand, command);
        }

        /// <summary>
        /// Deletes one row (currency) from the billing_Currency table
        /// </summary>
        /// <remarks>Deletes one row (currency) from the billing_Currency table.</remarks>
        /// <param name="CurrencyCode">Key value for billing_Currency table. Used to find the correct row to delete</param>
        /// <returns>True if ok, false otherwise.</returns>
        public bool DeleteCurrency(string CurrencyCode) {
            // Define function name for error handling
            FunctionName = "DeleteCurrency(string CurrencyCode)";

            // Define the SQL statement to execute and then execute it!
            string strCommand = String.Format("DELETE FROM billing_Currency WHERE CurrencyCode = '{0}'", CurrencyCode);
            return ExecuteSQL(strCommand);
        }

        #endregion

        #region Index region

        /// <summary>
        /// Returns a list of all index detail rows by name
        /// </summary>
        /// <remarks>
        /// Returns all fields and rows from the table billing_IndexDetails.
        /// </remarks>
        /// <returns>Index details as DataTable</returns>
        public DataTable GetIndexDetailsAsDataTable(string IndexName) {
            FunctionName = "DataTable GetCurrencyListAsDataTable()";

            string strCommand =
                String.Format(
                    "SELECT * FROM billing_IndexDetails WHERE IndexName = N'{0}' ORDER BY IndexDate", IndexName);
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Returns a list of all active indexes (overview)
        /// </summary>
        /// <remarks>
        /// Returns all fields and rows from the table billing_Indexes. The fields are:
        /// <list type="bullet">
        /// <item>IndexName (nvarchar, 20)</item>
        /// <item>IsActive (Char, 10)</item>
        /// <item>DescNative (nvarchar, 255)</item>
        /// <item>DescEN (nvarchar, 255)</item>
        /// </list>
        /// </remarks>
        /// <returns>Active Indexes as DataTable</returns>
        public DataTable GetActiveIndexesAsDataTable() {
            FunctionName = "DataTable GetActiveIndexesAsDataTable()";

            string strCommand = "SELECT * FROM billing_Indexes WHERE UPPER(IsActive) = 'TRUE' ORDER BY IndexName";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Returns a list of all indexes
        /// </summary>
        /// <remarks>
        /// Returns all fields and rows from the table billing_IndexDetails. The fields are:
        /// <list type="bullet">
        /// <item>IndexName (nvarchar, 20)</item>
        /// <item>BeginDate (DateTime)</item>
        /// <item>EndDate (DateTime)</item>
        /// <item>IndexPercent (int)</item>
        /// <item>IndexAmount (float)</item>
        /// </list>
        /// </remarks>
        /// <returns>Indexes as DataTable</returns>
        public DataTable GetAllIndexesAsDataTable() {
            FunctionName = "DataTable GetAllIndexesAsDataTable()";

            string strCommand = "SELECT * FROM billing_IndexDetails ORDER BY IndexName";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Returns a list of all indexes
        /// </summary>
        /// <remarks>
        /// Returns all fields and rows from the table billing_Indexes. The fields are:
        /// <list type="bullet">
        /// <item>IndexName (nvarchar, 20)</item>
        /// <item>IsActive (Char, 10)</item>
        /// <item>DescNative (nvarchar, 255)</item>
        /// <item>DescEN (nvarchar, 255)</item>
        /// </list>
        /// </remarks>
        /// <returns>Indexes as DataTable</returns>
        public DataTable GetCompleteIndexListAsDataTable() {
            FunctionName = "DataTable GetCompleteIndexListAsDataTable()";

            string strCommand = "SELECT * FROM billing_Indexes ORDER BY IndexName";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Inserts one row into the billing_IndexDetails datatable.
        /// </summary>
        /// <remarks>Inserts one row into the billing_IndexDetails datatable.</remarks>
        /// <param name="theIndex">IndexBLLC theIndex denotes an instance of an index.</param>
        /// <param name="UserId">UserId of the user that is updating the Index.</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddNewIndex(IndexBLLC theIndex, int UserID) {
            // Define function name for error handling
            FunctionName = "AddNewIndex(IndexBLLC theIndex, int UserID)";

            // Define the SQL statement to execute
            string strCommand =
                "INSERT INTO billing_IndexDetails (IndexName, IndexDate, IndexValue, Inserted, InsertedBy) Values (?,?,?,?,?)";

            // Define a command object
            OleDbCommand command = new OleDbCommand();
            // Along with parameters
            CreateParameter(command, "IndexName", ParameterDirection.Input, theIndex.Name, OleDbType.VarWChar);
            CreateParameter(command, "IndexDate", ParameterDirection.Input, theIndex.IndexDate, OleDbType.Date);
            CreateParameter(command, "IndexValue", ParameterDirection.Input, theIndex.IndexValue, OleDbType.Decimal);
            CreateParameter(
                command, "Inserted", ParameterDirection.Input, DateTime.Now.ToShortDateString(), OleDbType.Date);
            CreateParameter(command, "InsertedBy", ParameterDirection.Input, UserID, OleDbType.Integer);

            return ExecuteSQL(strCommand, command);
        }

        /// <summary>
        /// Inserts one row into the billing_Indexes datatable.
        /// </summary>
        /// <remarks>Inserts one row into the billing_Indexes datatable.</remarks>
        /// <param name="theIndex">IndexOverviewBLLC theIndex denotes an instance of an index.</param>
        /// <param name="UserId">UserId of the user that is updating the Index.</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddNewIndex(IndexOverviewBLLC theIndex, int UserID) {
            // Define function name for error handling
            FunctionName = "AddNewIndex(IndexOverviewBLLC theIndex, int UserID)";

            // Define the SQL statement to execute
            string strCommand =
                "INSERT INTO billing_Indexes (IndexName, IsActive, DescriptionNative, DescriptionEN, Inserted, InsertedBy) Values (?,?,?,?,?,?)";

            // Define a command object
            OleDbCommand command = new OleDbCommand();
            // Along with parameters
            CreateParameter(command, "IndexName", ParameterDirection.Input, theIndex.Name, OleDbType.VarWChar);
            CreateParameter(command, "IsActive", ParameterDirection.Input, theIndex.Active.ToString(), OleDbType.Char);
            CreateParameterFromString(
                command,
                "DescriptionNative",
                "DescriptionEN",
                ParameterDirection.Input,
                theIndex.DescNative,
                theIndex.DescEN,
                OleDbType.VarWChar);
            CreateParameter(
                command, "Inserted", ParameterDirection.Input, DateTime.Now.ToShortDateString(), OleDbType.Date);
            CreateParameter(command, "InsertedBy", ParameterDirection.Input, UserID, OleDbType.Integer);

            return ExecuteSQL(strCommand, command);
        }

        /// <summary>
        /// Deletes one row (index) from the billing_IndexDetails table
        /// </summary>
        /// <remarks>Deletes one row (index) from the billing_IndexDetails table.</remarks>
        /// <param name="IndexName">Key value for billing_IndexDetails table. Used to find the correct row to delete</param>
        /// <param name="BeginDate">Key value begin date, for identifying the index</param>
        /// <param name="EndDate">Key value end date, for identifying the index</param>
        /// <returns>True if ok, false otherwise.</returns>
        public bool DeleteFromIndexDetails(string IndexName, DateTime IndexDate) {
            // Define function name for error handling
            FunctionName = "DeleteFromIndexDetails(string IndexName, DateTime IndexDate)";

            // Define the SQL statement to execute
            string strCommand = "DELETE FROM billing_IndexDetails WHERE IndexName = ? AND IndexDate = ?";

            // Define a command object
            OleDbCommand command = new OleDbCommand();
            // Along with parameters
            CreateParameter(command, "IndexName", ParameterDirection.Input, IndexName, OleDbType.VarWChar);
            CreateParameter(command, "IndexDate", ParameterDirection.Input, IndexDate, OleDbType.Date);

            return ExecuteSQL(strCommand, command);
        }

        /// <summary>
        /// Deletes one row (index) from the billing_Indexes table
        /// </summary>
        /// <remarks>Deletes one row (index) from the billing_Indexes table.</remarks>
        /// <param name="IndexName">Key value for billing_Indexes table. Used to find the correct row to delete</param>
        /// <returns>True if ok, false otherwise.</returns>
        public bool DeleteFromIndexes(string IndexName) {
            // Define function name for error handling
            FunctionName = "DeleteFromIndexes(string IndexName)";

            // Define the SQL statement to execute and then execute it!
            string strCommand = String.Format("DELETE FROM billing_Indexes WHERE IndexName = '{0}'", IndexName);
            return ExecuteSQL(strCommand);
        }

        /// <summary>
        /// Updates index information for one given index.
        /// </summary>
        /// <remarks>Updates index information for one given index.</remarks>
        /// <param name="theIndex">Holds all avaliable index information for updating a single index.</param>
        /// <param name="UserId">UserId of the user that is updating the Index.</param>
        /// <param name="OrginalBeginDate">The original begin date for the record.</param>
        /// <param name="OrginalEndDate">The original end date for the record.</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdateIndex(IndexBLLC theIndex, int UserId, DateTime OrginalIndexDate) {
            // Define function name for error handling
            FunctionName = "UpdateIndex(IndexBLLC theIndex, int UserId, DateTime OrginalIndexDate)";

            // Define the SQL statement to execute
            string strCommand =
                "UPDATE billing_IndexDetails SET IndexDate = ?, IndexValue = ?, UpdatedBy = ?, Updated = ? WHERE IndexName = ? AND IndexDate = ?";

            // Define a command object
            OleDbCommand command = new OleDbCommand();
            // Along with parameters
            CreateParameter(command, "IndexDate", ParameterDirection.Input, theIndex.IndexDate, OleDbType.Date);
            CreateParameter(command, "IndexValue", ParameterDirection.Input, theIndex.IndexValue, OleDbType.Decimal);
            CreateParameter(command, "UpdatedBy", ParameterDirection.Input, UserId, OleDbType.Integer);
            CreateParameter(
                command, "Updated", ParameterDirection.Input, DateTime.Now.ToShortDateString(), OleDbType.Date);
            CreateParameter(command, "IndexName", ParameterDirection.Input, theIndex.Name, OleDbType.VarWChar);
            CreateParameter(command, "IndexDate", ParameterDirection.Input, OrginalIndexDate, OleDbType.Date);

            return ExecuteSQL(strCommand, command);
        }

        /// <summary>
        /// Updates index overview information for one given index.
        /// </summary>
        /// <remarks>Updates index overview information for one given index.</remarks>
        /// <param name="theIndex">Holds all avaliable index overview information for updating a single index.</param>
        /// <param name="UserId">UserId of the user that is updating the Index.</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdateIndexOverView(IndexOverviewBLLC theIndex, int UserId) {
            // Define function name for error handling
            FunctionName = "UpdateIndexOverView(IndexOverviewBLLC theIndex, int UserId)";

            // Define the SQL statement to execute
            string strCommand =
                "UPDATE billing_Indexes SET IsActive = ?, DescriptionNative = ?, DescriptionEN = ?, UpdatedBy = ?, Updated = ? WHERE IndexName = ?";

            // Define a command object
            OleDbCommand command = new OleDbCommand();
            // Along with parameters
            CreateParameter(command, "IsActive", ParameterDirection.Input, theIndex.Active.ToString(), OleDbType.Char);
            CreateParameterFromString(
                command,
                "DescriptionNative",
                "DescriptionEN",
                ParameterDirection.Input,
                theIndex.DescNative,
                theIndex.DescEN,
                OleDbType.VarWChar);
            CreateParameter(command, "UpdatedBy", ParameterDirection.Input, UserId, OleDbType.Integer);
            CreateParameter(
                command, "Updated", ParameterDirection.Input, DateTime.Now.ToShortDateString(), OleDbType.Date);
            CreateParameter(command, "IndexName", ParameterDirection.Input, theIndex.Name, OleDbType.VarWChar);

            return ExecuteSQL(strCommand, command);
        }

        /// <summary>
        /// Returns an arraylist of Index names (strings)
        /// </summary>
        /// <remarks>The arraylist consists of strings that represent active indexes.</remarks>
        /// <returns>ArrayList - List of strings that represent active index names.</returns>
        public ArrayList GetActiveIndexesAsArrayList() {
            FunctionName = "GetActiveIndexesAsArrayList()";

            // Define the SQL statement to execute
            string strCommand = "SELECT IndexName FROM billing_Indexes WHERE UPPER(IsActive) = 'TRUE'";

            return ExecuteSQLReaderArrayList(strCommand);
        }

        #endregion

        #region ItemIndex

        /// <summary>
        /// Returns a list of all active products
        /// </summary>
        /// <remarks>
        /// Returns product id and product names (native and english)
        /// </remarks>
        /// <returns>Active products as DataTable</returns>
        public DataTable GetActiveProductsAsDataTable() {
            FunctionName = "DataTable GetActiveProductsAsDataTable()";

            string strCommand =
                "SELECT ProductID, ProductNameNative, ProductNameEN FROM au_Products WHERE UPPER(IsOpen) = 'TRUE' AND UPPER(IsBaseProduct) = 'TRUE' ORDER BY ProductID";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Returns a list of all usage (logging) types
        /// </summary>
        /// <remarks>
        /// Returns type id and description in native and english tounge.
        /// </remarks>
        /// <returns>Usage types as DataTable</returns>
        public DataTable GetUsageTypesAsDataTable() {
            FunctionName = "DataTable GetUsageTypesAsDataTable()";

            string strCommand = "SELECT id, typeNative, typeEN FROM np_UsageTypes ORDER BY id";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Returns a list of all counter links
        /// </summary>
        /// <remarks>
        /// Returns all fields from the billing_ItemCounterLink table
        /// </remarks>
        /// <returns>counter links as DataTable</returns>
        public DataTable GetCounterLinksAsDataTable() {
            FunctionName = "DataTable GetCounterLinksAsDataTable()";

            string strCommand = "SELECT * FROM billing_ItemCounterLink ORDER BY id";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Returns an arraylist of Currency codes (strings)
        /// </summary>
        /// <remarks>The arraylist consists of strings that represent currency codes.</remarks>
        /// <returns>ArrayList - List of strings that represent currency codes.</returns>
        public ArrayList GetCurrencyCodesAsArrayList() {
            FunctionName = "GetCurrencyCodesAsArrayList()";

            // Define the SQL statement to execute
            string strCommand = "SELECT CurrencyCode FROM billing_Currency";
            return ExecuteSQLReaderArrayList(strCommand);
        }

        /// <summary>
        /// Inserts one row into the billing_ItemRegistry datatable. Can also insert into billing_IndexesOnItems
        /// </summary>
        /// <remarks>Inserts one row into the billing_ItemRegistry datatable. Can also insert into billing_IndexesOnItems.</remarks>
        /// <param name="theItem">BillingItemBLLC theItem denotes an instance of an item.</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddRegistryItem(BillingItemBLLC theItem) {
            // Define function name for error handling
            FunctionName = "AddRegistryItem(BillingItemBLLC theItem)";

            // Define the SQL statement to execute
            string strCommand =
                "INSERT INTO billing_ItemRegistry (Currency, ItemNameNative, ItemNameEN, MonthlyAmount, UsageAmount, MonthlyDiscountPercent, UsageDiscountPercent, LoggingCodeID, ProductCodeID, IsActive, FinancialKey, MasterItemID, Inserted, InsertedBy) Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            // Define a command object
            OleDbCommand command = new OleDbCommand();
            // Along with parameters
            CreateParameter(command, "Currency", ParameterDirection.Input, theItem.ItemCurrency, OleDbType.VarWChar);
            CreateParameterFromString(
                command,
                "ItemNameNative",
                "ItemNameEN",
                ParameterDirection.Input,
                theItem.ItemNameNative,
                theItem.ItemNameEN,
                OleDbType.VarWChar);
            CreateParameter(
                command, "MonthlyAmount", ParameterDirection.Input, theItem.MonthlyAmount, OleDbType.Decimal);
            CreateParameter(command, "UsageAmount", ParameterDirection.Input, theItem.UsageAmount, OleDbType.Decimal);
            CreateParameter(
                command,
                "MonthlyDiscountPercent",
                ParameterDirection.Input,
                theItem.MonthlyDiscountPercent,
                OleDbType.Decimal);
            CreateParameter(
                command,
                "UsageDiscountPercent",
                ParameterDirection.Input,
                theItem.UsageDiscountPercent,
                OleDbType.Decimal);
            // Only works for integer types for now...
            CreateParameterDefValue(
                command, "LoggingCodeID", ParameterDirection.Input, theItem.LoggingCodeID, OleDbType.Integer, null, "-1");
            CreateParameterDefValue(
                command, "ProductCodeID", ParameterDirection.Input, theItem.ProductCodeID, OleDbType.Integer, null, "-1");
            CreateParameter(
                command, "IsActive", ParameterDirection.Input, Convert.ToString(theItem.IsActive), OleDbType.Char);
            CreateParameter(command, "FinancialKey", ParameterDirection.Input, theItem.FinancialKey, OleDbType.Char);
            CreateParameter(
                command,
                "MasterItemID",
                ParameterDirection.Input,
                Convert.ToString(theItem.MasterItemID),
                OleDbType.Char);
            CreateParameter(command, "Inserted", ParameterDirection.Input, theItem.Inserted, OleDbType.Date);
            CreateParameter(command, "InsertedBy", ParameterDirection.Input, theItem.InsertedBy, OleDbType.Integer);

            return ExecuteSQL(strCommand, command);
        }

        /// <summary>
        /// Returns a list of all items
        /// </summary>
        /// <remarks>
        /// Returns all fields and rows from the table billing_ItemRegistry. 
        /// </remarks>
        /// <returns>Items as DataTable</returns>
        public DataTable GetAllRegistryItemsAsDataTable() {
            FunctionName = "DataTable GetAllRegistryItemsAsDataTable()";

            string strCommand = "SELECT * FROM billing_ItemRegistry ORDER BY ID";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Returns a list of all active items
        /// </summary>
        /// <remarks>
        /// Returns all fields and rows from the table billing_ItemRegistry where IsActive = 'True'. 
        /// </remarks>
        /// <returns>Items as DataTable</returns>
        public DataTable GetAllActiveRegistryItemsAsDataTable() {
            FunctionName = "DataTable GetAllActiveRegistryItemsAsDataTable()";

            string strCommand = "SELECT * FROM billing_ItemRegistry WHERE IsActive = 'True' ORDER BY ID";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Returns a list of all active items for a specific currency
        /// </summary>
        /// <remarks>
        /// Returns all fields and rows from the table billing_ItemRegistry where IsActive = 'True'
        /// and Currency = ?. 
        /// </remarks>
        /// <returns>Items as DataTable</returns>
        public DataTable GetAllActiveRegistryItemsAsDataTable(string theCurrency) {
            FunctionName = "DataTable GetAllActiveRegistryItemsAsDataTable()";

            string strCommand = "SELECT * FROM billing_ItemRegistry WHERE IsActive = 'True' AND Currency = '" +
                                theCurrency + "' ORDER BY ID";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Deletes one row (item) from the billing_ItemRegistry table
        /// </summary>
        /// <remarks>Deletes one row (item) from the billing_ItemRegistry table.</remarks>
        /// <param name="ItemID">Key value for billing_ItemRegistry table. Used to find the correct row to delete</param>
        /// <returns>True if ok, false otherwise.</returns>
        public bool DeleteFromRegistryItems(int ItemID) {
            // Define function name for error handling
            FunctionName = "DeleteFromRegistryItems(int ItemID)";

            // Define the SQL statement to execute and then execute it!
            string strCommand = String.Format("DELETE FROM billing_ItemRegistry WHERE ID = '{0}'", ItemID);
            return ExecuteSQL(strCommand);
        }

        /// <summary>
        /// Gets one Item from billing_ItemRegistry
        /// </summary>
        /// <remarks>Get one Item from billing_ItemRegistry (items)</remarks>
        /// <param name="ItemID">Unique identifier for item.</param>
        /// <returns>Instance of an Item (BillingItemBLLC)</returns>
        public BillingItemBLLC GetRegistryItem(int ItemID) {
            String funcName = "BillingItemBLLC GetRegistryItem(int ItemID)";
            BillingItemBLLC myItem = new BillingItemBLLC();
            OleDbDataReader reader = null;
            OleDbCommand myOleDbCommand = null;

            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    myOleDbCommand = new OleDbCommand(
                        "SELECT * FROM billing_ItemRegistry WHERE ID =" + ItemID, myOleDbConn);
                    reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        myItem.ItemID = reader.GetInt32(0);

                        if (!reader.IsDBNull(1)) {
                            myItem.ItemCurrency = reader.GetString(1);
                        }

                        if (!reader.IsDBNull(2)) {
                            myItem.ItemNameNative = reader.GetString(2);
                        }

                        if (!reader.IsDBNull(3)) {
                            myItem.ItemNameEN = reader.GetString(3);
                        }

                        if (!reader.IsDBNull(4)) {
                            myItem.MonthlyAmount = reader.GetDecimal(4);
                        }

                        if (!reader.IsDBNull(5)) {
                            myItem.UsageAmount = reader.GetDecimal(5);
                        }

                        if (!reader.IsDBNull(6)) {
                            myItem.MonthlyDiscountPercent = reader.GetDecimal(6);
                        }

                        if (!reader.IsDBNull(7)) {
                            myItem.UsageDiscountPercent = reader.GetDecimal(7);
                        }

                        if (!reader.IsDBNull(8)) {
                            myItem.LoggingCodeID = reader.GetInt32(8);
                        }

                        if (!reader.IsDBNull(9)) {
                            myItem.ProductCodeID = reader.GetInt32(9);
                        }

                        if (!reader.IsDBNull(10)) {
                            myItem.IsActive = Convert.ToBoolean(reader.GetString(10));
                        }

                        if (!reader.IsDBNull(11)) {
                            myItem.FinancialKey = reader.GetString(11);
                        }

                        if (!reader.IsDBNull(12)) {
                            myItem.MasterItemID = reader.GetInt32(12);
                        }

                        if (!reader.IsDBNull(13)) {
                            myItem.Inserted = reader.GetDateTime(13);
                        }

                        if (!reader.IsDBNull(14)) {
                            myItem.InsertedBy = reader.GetInt32(14);
                        }

                        if (!reader.IsDBNull(15)) {
                            myItem.Updated = reader.GetDateTime(15);
                        }

                        if (!reader.IsDBNull(16)) {
                            myItem.UpdatedBy = reader.GetInt32(16);
                        }
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(ClassName + " : " + funcName + e.Message, true);
                } finally {
                    if (reader != null) {
                        reader.Close();
                    }

                    if (myOleDbConn != null) {
                        myOleDbConn.Dispose();
                    }

                    if (myOleDbCommand != null) {
                        myOleDbCommand.Dispose();
                    }
                }
                return myItem;
            }
        }

        /// <summary>
        /// Updates a specific item in the item registry
        /// </summary>
        /// <remarks>Updates a specific item that recides in the billing_ItemRegistry table.</remarks>
        /// <param name="theItem">Holds all avaliable item information for updating a single registry item.</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdateRegistryItem(BillingItemBLLC theItem) {
            // Define function name for error handling
            FunctionName = "UpdateRegistryItem(BillingItemBLLC theItem)";

            // Define the SQL statement to execute
            string strCommand =
                "UPDATE billing_ItemRegistry SET Currency = ?, ItemNameNative = ?, ItemNameEN = ?, MonthlyAmount = ?, UsageAmount = ?, MonthlyDiscountPercent = ?, UsageDiscountPercent = ?, LoggingCodeID = ?, ProductCodeID = ?, IsActive = ?, FinancialKey = ?, Updated = ?, UpdatedBy = ? WHERE ID = ?";

            // Define a command object
            OleDbCommand command = new OleDbCommand();

            // Along with parameters
            CreateParameter(command, "Currency", ParameterDirection.Input, theItem.ItemCurrency, OleDbType.VarWChar);
            CreateParameterFromString(
                command,
                "ItemNameNative",
                "ItemNameEN",
                ParameterDirection.Input,
                theItem.ItemNameNative,
                theItem.ItemNameEN,
                OleDbType.VarWChar);
            CreateParameter(
                command, "MonthlyAmount", ParameterDirection.Input, theItem.MonthlyAmount, OleDbType.Decimal);
            CreateParameter(command, "UsageAmount", ParameterDirection.Input, theItem.UsageAmount, OleDbType.Decimal);
            CreateParameter(
                command,
                "MonthlyDiscountPercent",
                ParameterDirection.Input,
                theItem.MonthlyDiscountPercent,
                OleDbType.Decimal);
            CreateParameter(
                command,
                "UsageDiscountPercent",
                ParameterDirection.Input,
                theItem.UsageDiscountPercent,
                OleDbType.Decimal);
            // Only works for integer types for now...
            CreateParameterDefValue(
                command, "LoggingCodeID", ParameterDirection.Input, theItem.LoggingCodeID, OleDbType.Integer, null, "-1");
            CreateParameterDefValue(
                command, "ProductCodeID", ParameterDirection.Input, theItem.ProductCodeID, OleDbType.Integer, null, "-1");
            CreateParameter(
                command, "IsActive", ParameterDirection.Input, Convert.ToString(theItem.IsActive), OleDbType.Char);
            CreateParameter(command, "FinancialKey", ParameterDirection.Input, theItem.FinancialKey, OleDbType.Char);
            CreateParameter(command, "Updated", ParameterDirection.Input, theItem.Updated, OleDbType.Date);
            CreateParameter(command, "UpdatedBy", ParameterDirection.Input, theItem.UpdatedBy, OleDbType.Integer);
            CreateParameter(command, "ID", ParameterDirection.Input, theItem.ItemID, OleDbType.Integer);

            return ExecuteSQL(strCommand, command);
        }

        #endregion

        #region AgreementItems

        /// <summary>
        /// Inserts one row into the billing_IndexesOnAgreementItems datatable. This function is only called from the "AddAgreementItem" function.
        /// </summary>
        /// <remarks>Inserts one row into the billing_IndexesOnAgreementItems datatable. This function is only called from the "AddAgreementItem" function.</remarks>
        /// <param name="IndexNames">The name of the index that should be linked to an item</param>
        /// <param name="ItemID">The id of the item to be linked to an index</param>
        /// <param name="Inserted">DateTime value</param>
        /// <param name="InsertedBy">UserID</param>
        /// <param name="myOleDbCommand">Reuse of the command object</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddIndexForAgreementItem(string IndexName, int ItemID, DateTime Inserted, int InsertedBy) {
            // Define function name for error handling
            FunctionName = "AddIndexForAgreementItem(string IndexName, int ItemID, DateTime Inserted, int InsertedBy)";

            // Define the SQL statement to execute
            string strCommand =
                "INSERT INTO billing_IndexesOnAgreementItems(ItemID, IndexName, Inserted, InsertedBy) VALUES(?,?,?,?)";

            // Define a command object
            OleDbCommand command = new OleDbCommand();

            // Along with parameters
            CreateParameter(command, "ItemID", ParameterDirection.Input, ItemID, OleDbType.Integer);
            CreateParameter(command, "IndexName", ParameterDirection.Input, IndexName, OleDbType.VarWChar);
            CreateParameter(command, "Inserted", ParameterDirection.Input, Inserted, OleDbType.Date);
            CreateParameter(command, "InsertedBy", ParameterDirection.Input, InsertedBy, OleDbType.Integer);

            return ExecuteSQL(strCommand, command);
        }

        /// <summary>
        /// Returns a list of all items for a certain agreement
        /// </summary>
        /// <remarks>
        /// Returns all fields and rows from the table billing_ItemRegistry where agreement id equals a special number. 
        /// </remarks>
        /// <returns>Items as DataTable</returns>
        public DataTable GetAgreementItemsAsDataTable(int AgreementID) {
            FunctionName = "DataTable GetAgreementItemsAsDataTable(int AgreementID)";

            string strCommand =
                string.Format("SELECT * FROM billing_AgreementItems WHERE AgreementID = '{0}' ORDER BY ID", AgreementID);
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Deletes one row (item) from the billing_AgreementItems table
        /// </summary>
        /// <remarks>Deletes one row (item) from the billing_AgreementItems table.</remarks>
        /// <param name="ItemID">Key value for billing_AgreementItems table. Used to find the correct row to delete</param>
        /// <returns>True if ok, false otherwise.</returns>
        public bool DeleteFromAgreementItems(int ItemID) {
            // Define function name for error handling
            FunctionName = "DeleteFromAgreementItems(int ItemID)";

            // First we need do delete all rows from the billing_IndexesOnAgreementItems table fro the current item
            if (!DeleteFromIndexesOnAgreementItems(ItemID)) {
                return false;
            }

            // Define the SQL statement to execute and then execute it!
            string strCommand = String.Format("DELETE FROM billing_AgreementItems WHERE ID = '{0}'", ItemID);
            return ExecuteSQL(strCommand);
        }

        /// <summary>
        /// Deletes one row (index on item) from the billing_IndexesOnAgreementItems table
        /// </summary>
        /// <remarks>Deletes one row (index on item) from the billing_IndexesOnAgreementItems table.</remarks>
        /// <param name="ItemID">Key value for billing_IndexesOnAgreementItems table. Used to find the correct row(s) to delete</param>
        /// <returns>True if ok, false otherwise.</returns>
        public bool DeleteFromIndexesOnAgreementItems(int ItemID) {
            // Define function name for error handling
            FunctionName = "DeleteFromIndexesOnAgreementItems(int ItemID)";

            // Define the SQL statement to execute and then execute it!
            string strCommand = String.Format(
                "DELETE FROM billing_IndexesOnAgreementItems WHERE ItemID = '{0}'", ItemID);
            return ExecuteSQL(strCommand);
        }

        /// <summary>
        /// Gets one Item from billing_AgreementItems
        /// </summary>
        /// <remarks>Get one Item from billing_AgreementItems (items)</remarks>
        /// <param name="ItemID">Unique identifier for item.</param>
        /// <returns>Instance of an Agreement Item (AgreementItemBLLC)</returns>
        public AgreementItemBLLC GetAgreementItem(int ItemID) {
            String funcName = "AgreementItemBLLC GetAgreementItem(int ItemID)";
            AgreementItemBLLC myItem = new AgreementItemBLLC();
            OleDbDataReader reader = null;
            OleDbCommand myOleDbCommand = null;

            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try {
                    myOleDbConn.Open();
                    myOleDbCommand = new OleDbCommand(
                        "SELECT * FROM billing_AgreementItems WHERE ID =" + ItemID, myOleDbConn);
                    reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        myItem.ID = reader.GetInt32(0);

                        if (!reader.IsDBNull(1)) {
                            myItem.AgreementID = reader.GetInt32(1);
                        }

                        if (!reader.IsDBNull(2)) {
                            myItem.ItemID = reader.GetInt32(2);
                        }

                        if (!reader.IsDBNull(3)) {
                            myItem.FixedAmount = reader.GetDecimal(3);
                        }

                        if (!reader.IsDBNull(4)) {
                            myItem.FixedDiscountPercent = reader.GetDecimal(4);
                        }

                        if (!reader.IsDBNull(5)) {
                            myItem.MonthlyDiscountPercent = reader.GetDecimal(5);
                        }

                        if (!reader.IsDBNull(6)) {
                            myItem.UsageDiscountPercent = reader.GetDecimal(6);
                        }

                        if (!reader.IsDBNull(7)) {
                            myItem.CounterLinkID = reader.GetInt32(7);
                        }

                        if (!reader.IsDBNull(8)) {
                            myItem.IsVisible = Convert.ToBoolean(reader.GetString(8));
                        }

                        if (!reader.IsDBNull(9)) {
                            myItem.BeginDate = reader.GetDateTime(9);
                        }

                        if (!reader.IsDBNull(10)) {
                            myItem.EndDate = reader.GetDateTime(10);
                        }

                        if (!reader.IsDBNull(11)) {
                            myItem.ReminderOffset = reader.GetInt32(11);
                        }

                        if (!reader.IsDBNull(12)) {
                            myItem.NotificationEmails = reader.GetString(12);
                        }

                        if (!reader.IsDBNull(13)) {
                            myItem.Inserted = reader.GetDateTime(13);
                        }

                        if (!reader.IsDBNull(14)) {
                            myItem.InsertedBy = reader.GetInt32(14);
                        }

                        if (!reader.IsDBNull(15)) {
                            myItem.Updated = reader.GetDateTime(15);
                        }

                        if (!reader.IsDBNull(16)) {
                            myItem.UpdatedBy = reader.GetInt32(16);
                        }

                        if (!reader.IsDBNull(18)) {
                            myItem.Freezone = reader.GetInt32(18);
                        }

                        // get the collections
                        myItem.IndexesOnAgreementItems = GetIndexesOnAgreementItems(myItem.ItemID);
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(ClassName + " : " + funcName + e.Message, true);
                } finally {
                    if (reader != null) {
                        reader.Close();
                    }

                    if (myOleDbConn != null) {
                        myOleDbConn.Dispose();
                    }

                    if (myOleDbCommand != null) {
                        myOleDbCommand.Dispose();
                    }
                }

                return myItem;
            }
        }

        /// <summary>
        /// Returns an arraylist of Index names that are linked to a specific agreement item (strings)
        /// </summary>
        /// <remarks>The arraylist consists of strings that represent indexes on an agreement item.</remarks>
        /// <returns>ArrayList - List of strings that represent active index names.</returns>
        public ArrayList GetIndexesOnAgreementItems(int ItemID) {
            String funcName = "GetIndexesOnAgreementItems(int ItemID)";
            ArrayList IndexArrayList = new ArrayList();
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT IndexName FROM billing_IndexesOnAgreementItems WHERE ItemID =" + ItemID +
                            "ORDER BY IndexName",
                            myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            IndexArrayList.Add(reader.GetString(0));
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(ClassName + " : " + funcName + err.Message, true);
            }
            return IndexArrayList;
        }

        /// <summary>
        /// Returns an arraylist of Index names that are linked to a specific agreement item (strings)
        /// </summary>
        /// <remarks>The arraylist consists of strings that represent indexes on an agreement item.</remarks>
        /// <returns>ArrayList - List of strings that represent active index names.</returns>
        public ArrayList GetDepartmentsOnAgreementItems(int ItemID) {
            String funcName = "GetDepartmentsOnAgreementItems(int ItemID)";
            ArrayList arrDepartments = new ArrayList();
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT DepartmentID FROM c_billing_AgreementItem_Department WHERE AgreementItemID =" +
                            ItemID,
                            myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            arrDepartments.Add(reader.GetInt32(0));
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(ClassName + " : " + funcName + err.Message, true);
            }
            return arrDepartments;
        }

        /// <summary>
        /// Updates a specific item in the agreement item
        /// </summary>
        /// <remarks>Updates a specific agreement item that recides in the billing_AgreementItems table.</remarks>
        /// <param name="theItem">Holds all avaliable item information for updating a single agreement item.</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdateAgreementItem(AgreementItemBLLC theItem) {
            // Define function name for error handling
            FunctionName = "UpdateAgreementItem(AgreementItemBLLC theItem)";

            // Define the SQL statement to execute
            string strCommand =
                "UPDATE billing_AgreementItems SET FixedAmount = ?, FixedDiscountPercent = ?, MonthlyDiscountPercent = ?, UsageDiscountPercent = ?, CounterLinkID = ?, IsVisible = ?, BeginDate = ?, EndDate = ?, ReminderOffset = ?, NotificationEmails = ?, Updated = ?, UpdatedBy = ?, FreeZone = ? WHERE ID = ?";

            // Define a command object
            OleDbCommand command = new OleDbCommand();

            // Along with parameters
            CreateParameter(command, "FixedAmount", ParameterDirection.Input, theItem.FixedAmount, OleDbType.Decimal);
            CreateParameter(
                command,
                "FixedDiscountPercent",
                ParameterDirection.Input,
                theItem.FixedDiscountPercent,
                OleDbType.Decimal);
            CreateParameter(
                command,
                "MonthlyDiscountPercent",
                ParameterDirection.Input,
                theItem.MonthlyDiscountPercent,
                OleDbType.Decimal);
            CreateParameter(
                command,
                "UsageDiscountPercent",
                ParameterDirection.Input,
                theItem.UsageDiscountPercent,
                OleDbType.Decimal);
            CreateParameterDefValue(
                command, "CounterLinkID", ParameterDirection.Input, theItem.CounterLinkID, OleDbType.Integer, null, "-1");
            CreateParameter(
                command, "IsVisible", ParameterDirection.Input, Convert.ToString(theItem.IsVisible), OleDbType.Char);
            CreateParameter(command, "BeginDate", ParameterDirection.Input, theItem.BeginDate, OleDbType.Date);
            CreateParameter(command, "EndDate", ParameterDirection.Input, theItem.EndDate, OleDbType.Date);
            CreateParameter(
                command, "ReminderOffset", ParameterDirection.Input, theItem.ReminderOffset, OleDbType.Integer);
            CreateParameter(
                command, "NotificationEmails", ParameterDirection.Input, theItem.NotificationEmails, OleDbType.VarWChar);
            CreateParameter(command, "Updated", ParameterDirection.Input, theItem.Updated, OleDbType.Date);
            CreateParameter(command, "UpdatedBy", ParameterDirection.Input, theItem.UpdatedBy, OleDbType.Integer);
            CreateParameter(command, "FreeZone", ParameterDirection.Input, theItem.Freezone, OleDbType.Integer);
            CreateParameter(command, "ID", ParameterDirection.Input, theItem.ID, OleDbType.Integer);

            return ExecuteSQL(strCommand, command);
        }

        #endregion

        #region Agreement functions

        /// <summary>
        /// Returns a list of all active subscribers
        /// </summary>
        /// <remarks>
        /// Returns all rows from the view ua_SubscriberDetailsView
        /// </remarks>
        /// <returns>Open subscribers as DataTable</returns>
        public DataTable GetOpenSubscribersAsDataTable() {
            FunctionName = "DataTable GetOpenSubscribersAsDataTable()";

            string strCommand = "SELECT * FROM ua_SubscriberDetailsView WHERE IsOpen = 'True' ORDER BY NameNative";
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Returns a single subscriber in a datatable
        /// </summary>
        /// <remarks>
        /// Returns a single subscriber in a datatable from the view ua_SubscriberDetailsView
        /// </remarks>
        /// <returns>Subscriber as DataTable</returns>
        public DataTable GetSingleSubscribersAsDataTable(int SubscriberID) {
            FunctionName = "DataTable GetSingleSubscribersAsDataTable(int SubscriberID)";

            string strCommand =
                String.Format(
                    "SELECT * FROM ua_SubscriberDetailsView WHERE IsOpen = 'True' AND CreditInfoID = {0} ORDER BY NameNative",
                    SubscriberID);
            return LoadDataTable(strCommand);
        }

        /// <summary>
        /// Takes an instance of Agreement and adds to the database
        /// </summary>
        /// <param name="theAgreement">One agreement instance with all available info (items and comments)</param>
        /// <returns>true if OK, false otherwise</returns>
        public bool AddNewAgreement(AgreementBLLC theAgreement) {
            // Define function name for error handling
            FunctionName = "AddNewAgreement(AgreementBLLC theAgreement";

            // Define a connection
            OleDbConnection connection = OpenConnection();

            // Define a transaction object.
            OleDbTransaction transaction = connection.BeginTransaction();

            // Define the SQL statement to execute
            string strCommand =
                "INSERT INTO billing_Agreement (AgreementNumber, SubscriberID, AgreementAmount, GeneralDescription, BeginDate, EndDate, Currency, DiscountPercentage, ReminderOffset, NotificationEmails, SignerCIID, Inserted, InsertedBy) Values (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            // Define a command object and link it to the transaction...
            OleDbCommand command = new OleDbCommand();
            command.Transaction = transaction;

            // Along with parameters
            CreateParameter(
                command, "AgreementNumber", ParameterDirection.Input, theAgreement.AgreementNumber, OleDbType.VarWChar);
            CreateParameter(
                command, "SubscriberID", ParameterDirection.Input, theAgreement.SubscriberID, OleDbType.Integer);
            CreateParameter(
                command, "AgreementAmount", ParameterDirection.Input, theAgreement.AgreementAmount, OleDbType.Currency);
            CreateParameter(
                command,
                "GeneralDescription",
                ParameterDirection.Input,
                theAgreement.GeneralDescription,
                OleDbType.VarWChar);
            CreateParameter(command, "BeginDate", ParameterDirection.Input, theAgreement.BeginDate, OleDbType.Date);
            CreateParameter(command, "EndDate", ParameterDirection.Input, theAgreement.EndDate, OleDbType.Date);
            CreateParameter(command, "Currency", ParameterDirection.Input, theAgreement.Currency, OleDbType.VarWChar);
            CreateParameter(
                command, "DiscountPercentage", ParameterDirection.Input, theAgreement.Discountpercent, OleDbType.Decimal);
            CreateParameter(
                command, "ReminderOffset", ParameterDirection.Input, theAgreement.ReminderOffset, OleDbType.Integer);
            CreateParameter(
                command,
                "NotificationEmails",
                ParameterDirection.Input,
                theAgreement.NotificationEmails,
                OleDbType.VarWChar);
            CreateParameter(command, "SignerCIID", ParameterDirection.Input, theAgreement.SignerCIID, OleDbType.Integer);
            CreateParameter(command, "Inserted", ParameterDirection.Input, theAgreement.Inserted, OleDbType.Date);
            CreateParameter(command, "InsertedBy", ParameterDirection.Input, theAgreement.InsertedBy, OleDbType.Integer);

            try {
                // Execute the insert statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    throw new Exception("Could not insert agreement");
                }

                // Get the identity of the newly inserted row...
                strCommand = "SELECT MAX(AgreementID) AS MaxID FROM billing_Agreement";

                // Execute the insert statement with a function from BaseDALC...
                if ((theAgreement.AgreementID = ExecuteScalarSQL(strCommand, command, connection)) == -1) {
                    throw new Exception("Could not retrieve highest id");
                }

                // Insert all comments...
                foreach (AgreementCommentBLLC comment in theAgreement.CommentsOnAgreement) {
                    // Set the matching values between agreement and it's comments
                    comment.AgreementID = theAgreement.AgreementID;
                    comment.Inserted = theAgreement.Inserted;
                    comment.InsertedBy = theAgreement.InsertedBy;

                    if (!InsertAgreementComments(command, comment, connection)) {
                        throw new Exception("Could not insert comment");
                    }
                }

                // Insert all items...
                foreach (AgreementItemBLLC item in theAgreement.ItemsOnAgreement) {
                    // Set the matching values between agreement and it's items
                    item.AgreementID = theAgreement.AgreementID;
                    item.Inserted = theAgreement.Inserted;
                    item.InsertedBy = theAgreement.InsertedBy;

                    if (!InsertAgreementItems(command, item, connection, false)) {
                        throw new Exception("Could not insert item");
                    }
                }

                transaction.Commit();
                return true;
            } catch (Exception err) {
                // Rollback the transaction if something went wrong...
                transaction.Rollback();
                theAgreement.AgreementID = -1;
                Logger.WriteToLog(ClassName + " : " + FunctionName + err.Message, true);
                return false;
            } finally {
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Dispose();
                }
            }
        }

        /// <summary>
        /// Inserts a single comment for a single agreement in a single transaction
        /// </summary>
        /// <param name="command">The command that is linked to the transaction (agreement, items, comments)</param>
        /// <param name="comment">The comment instance</param>
        /// <param name="connection">The connection that is linked to the transaction (agreement, items, comments)</param>
        /// <returns>true if all is ok, otherwise false</returns>
        private bool InsertAgreementComments(
            OleDbCommand command, AgreementCommentBLLC comment, OleDbConnection connection) {
            string strCommand =
                "INSERT INTO billing_AgreementComments (AgreementID, Comment, ReminderDate, NotificationEmails, Inserted, InsertedBy, Updated, UpdatedBy) Values (?,?,?,?,?,?,?,?)";
            command.Parameters.Clear();

            // Set up parameters
            CreateParameter(command, "AgreementID", ParameterDirection.Input, comment.AgreementID, OleDbType.Integer);
            CreateParameter(command, "Comment", ParameterDirection.Input, comment.Comment, OleDbType.VarWChar);
            CreateParameter(command, "ReminderDate", ParameterDirection.Input, comment.ReminderDate, OleDbType.Date);
            CreateParameter(
                command, "NotificationEmails", ParameterDirection.Input, comment.NotificationEmails, OleDbType.VarWChar);
            CreateParameter(command, "Inserted", ParameterDirection.Input, comment.Inserted, OleDbType.Date);
            CreateParameter(command, "InsertedBy", ParameterDirection.Input, comment.InsertedBy, OleDbType.Integer);
            CreateParameterDefValue(
                command,
                "Updated",
                ParameterDirection.Input,
                comment.Updated,
                OleDbType.Date,
                null,
                DateTime.MinValue.ToString());
            CreateParameterDefValue(
                command, "UpdatedBy", ParameterDirection.Input, comment.UpdatedBy, OleDbType.Integer, null, "0");

            // Execute the insert statement with a function from BaseDALC...
            if (!ExecuteSQL(strCommand, command, connection)) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes one row from the billing_AgreementItems datatable. Can also delete from billing_IndexesOnAgreementItems
        /// </summary>
        /// <remarks>Deletes one row from the billing_AgreementItems datatable. Can also delete from billing_IndexesOnAgreementItems.</remarks>
        /// <param name="item">AgreementItemBLLC theItem denotes an instance of an item.</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool DeleteAgreementItem(AgreementItemBLLC item) {
            // Delete all indexes that have already been registered with the item...
            string strCommand = "Delete FROM billing_IndexesOnAgreementItems WHERE ItemID = ?";

            // Define a command object
            OleDbCommand command = new OleDbCommand();
            CreateParameter(command, "ItemID", ParameterDirection.Input, item.ID, OleDbType.Integer);

            // Execute the delete comments statement with a function from BaseDALC...
            if (!ExecuteSQL(strCommand, command)) {
                throw new Exception("Could not delete agreement item indexes");
            }

            // Delete the departments that have already been registered with the agreement item...
            strCommand = "DELETE FROM c_billing_AgreementItem_Department WHERE AgreementItemID=?";
            command.Parameters.Clear();
            CreateParameter(command, "AgreementItemID", ParameterDirection.Input, item.ID, OleDbType.Integer);

            // Execute the delete comments statement with a function from BaseDALC...
            if (!ExecuteSQL(strCommand, command)) {
                throw new Exception("Could not delete c_billing_AgreementItem_Department");
            }

            // Delete the item that has already been registered with the agreement...
            strCommand = "Delete FROM billing_AgreementItems WHERE ID = ?";
            command.Parameters.Clear();
            CreateParameter(command, "ID", ParameterDirection.Input, item.ID, OleDbType.Integer);

            // Execute the delete comments statement with a function from BaseDALC...
            if (!ExecuteSQL(strCommand, command)) {
                throw new Exception("Could not delete agreement items");
            }

            return true;
        }

        /// <summary>
        /// Inserts one row into the billing_AgreementItems datatable. Can also insert into billing_IndexesOnAgreementItems
        /// </summary>
        /// <remarks>Inserts one row into the billing_AgreementItems datatable. Can also insert into billing_IndexesOnAgreementItems.</remarks>
        /// <param name="command">OleDbCommand</param>
        /// <param name="item">AgreementItemBLLC theItem denotes an instance of an item.</param>
        /// <param name="connection">OleDbConnection</param>
        /// <param name="isUpdate">Is the action update or insert.</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool InsertAgreementItems(
            OleDbCommand command, AgreementItemBLLC item, OleDbConnection connection, bool isUpdate) {
            string strCommand;

            // Begin by deteting the indexes if this is an update...
            if (isUpdate && item.ID != -1) {
                // Delete all indexes that have already been registered with the item...
                strCommand = "Delete FROM billing_IndexesOnAgreementItems WHERE ItemID = ?";
                command.Parameters.Clear();
                CreateParameter(command, "ItemID", ParameterDirection.Input, item.ID, OleDbType.Integer);

                // Execute the delete comments statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    throw new Exception("Could not delete agreement item indexes");
                }

                // Delete the departments that have already been registered with the agreement item...
                strCommand = "DELETE FROM c_billing_AgreementItem_Department WHERE AgreementItemID=?";
                command.Parameters.Clear();
                CreateParameter(command, "AgreementItemID", ParameterDirection.Input, item.ID, OleDbType.Integer);

                // Execute the delete comments statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    throw new Exception("Could not delete c_billing_AgreementItem_Department");
                }

                // Delete the item that has already been registered with the agreement...
                strCommand = "Delete FROM billing_AgreementItems WHERE ID = ?";
                command.Parameters.Clear();
                CreateParameter(command, "ID", ParameterDirection.Input, item.ID, OleDbType.Integer);

                // Execute the delete comments statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    throw new Exception("Could not delete agreement items");
                }
            }

            strCommand =
                "INSERT INTO billing_AgreementItems (AgreementID, ItemID, FixedAmount, FixedDiscountPercent, MonthlyDiscountPercent, UsageDiscountPercent, CounterLinkID, IsVisible, BeginDate, EndDate, ReminderOffset, NotificationEmails, Inserted, InsertedBy, Updated, UpdatedBy, FreeZone) Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            command.Parameters.Clear();

            // Set up parameters
            CreateParameter(command, "AgreementID", ParameterDirection.Input, item.AgreementID, OleDbType.Integer);
            CreateParameter(command, "ItemID", ParameterDirection.Input, item.ItemID, OleDbType.Integer);
            CreateParameter(command, "FixedAmount", ParameterDirection.Input, item.FixedAmount, OleDbType.Currency);
            CreateParameter(
                command, "FixedDiscountPercent", ParameterDirection.Input, item.FixedDiscountPercent, OleDbType.Decimal);
            CreateParameter(
                command,
                "MonthlyDiscountPercent",
                ParameterDirection.Input,
                item.MonthlyDiscountPercent,
                OleDbType.Decimal);
            CreateParameter(
                command, "UsageDiscountPercent", ParameterDirection.Input, item.UsageDiscountPercent, OleDbType.Decimal);
            CreateParameterDefValue(
                command, "CounterLinkID", ParameterDirection.Input, item.CounterLinkID, OleDbType.Integer, null, "-1");
            CreateParameter(
                command, "IsVisible", ParameterDirection.Input, Convert.ToString(item.IsVisible), OleDbType.Char);
            CreateParameter(command, "BeginDate", ParameterDirection.Input, item.BeginDate, OleDbType.Date);
            CreateParameter(command, "EndDate", ParameterDirection.Input, item.EndDate, OleDbType.Date);
            CreateParameter(command, "ReminderOffset", ParameterDirection.Input, item.ReminderOffset, OleDbType.Integer);
            CreateParameter(
                command, "NotificationEmails", ParameterDirection.Input, item.NotificationEmails, OleDbType.VarWChar);
            CreateParameter(command, "Inserted", ParameterDirection.Input, item.Inserted, OleDbType.Date);
            CreateParameter(command, "InsertedBy", ParameterDirection.Input, item.InsertedBy, OleDbType.Integer);
            CreateParameterDefValue(
                command,
                "Updated",
                ParameterDirection.Input,
                item.Updated,
                OleDbType.Date,
                null,
                DateTime.MinValue.ToString());
            CreateParameterDefValue(
                command, "UpdatedBy", ParameterDirection.Input, item.UpdatedBy, OleDbType.Integer, null, "0");
            CreateParameter(command, "FreeZone", ParameterDirection.Input, item.Freezone, OleDbType.Integer);

            // Execute the insert statement with a function from BaseDALC...
            if (!ExecuteSQL(strCommand, command, connection)) {
                return false;
            }

            // Get the identity of the newly inserted row...
            strCommand = "SELECT @@IDENTITY FROM billing_AgreementItems";

            // Execute the insert statement with a function from BaseDALC...
            if ((item.ID = ExecuteScalarSQL(strCommand, command, connection)) == -1) {
                return false;
            }

            // Insert for Indexes...
            foreach (string indexname in item.IndexesOnAgreementItems) {
                if (!AddIndexForAgreementItem(command, indexname, item.ID, item.Inserted, item.InsertedBy, connection)) {
                    return false;
                }
            }

            InsertAgreementItemDepartment(command, item, connection, isUpdate);

            // item and index list insert succeeded...
            return true;
        }

        /// <summary>
        /// Inserts one row into the billing_IndexesOnAgreementItems datatable. This function is only called from the "AddAgreementItem" function.
        /// </summary>
        /// <remarks>Inserts one row into the billing_IndexesOnAgreementItems datatable. This function is only called from the "AddAgreementItem" function.</remarks>
        /// <param name="command">Reuse of the command object</param>
        /// <param name="indexname">The name of the index that should be linked to an item</param>
        /// <param name="itemID">The id of the item to be linked to an index</param>
        /// <param name="inserted">DateTime value</param>
        /// <param name="insertedBy">UserID</param>
        /// <param name="connection">OleDbConnection</param>
        /// <returns>true if ok, false otherwise</returns>
        private bool AddIndexForAgreementItem(
            OleDbCommand command,
            string indexname,
            int itemID,
            DateTime inserted,
            int insertedBy,
            OleDbConnection connection) {
            string strCommand =
                "INSERT INTO billing_IndexesOnAgreementItems(ItemID, IndexName, Inserted, InsertedBy) VALUES(?,?,?,?)";

            command.Parameters.Clear();

            // Set up parameters
            CreateParameter(command, "ItemID", ParameterDirection.Input, itemID, OleDbType.Integer);
            CreateParameter(command, "IndexName", ParameterDirection.Input, indexname, OleDbType.VarWChar);
            CreateParameter(command, "Inserted", ParameterDirection.Input, inserted, OleDbType.Date);
            CreateParameter(command, "InsertedBy", ParameterDirection.Input, insertedBy, OleDbType.Integer);

            // Execute the insert statement with a function from BaseDALC...
            if (!ExecuteSQL(strCommand, command, connection)) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets Agreements ID and deletes matching Agreement from database
        /// </summary>
        /// <param name="theAgreement">Agreement entity.</param>
        /// <returns>true if OK, false otherwise</returns>
        public bool DeleteAgreement(AgreementBLLC theAgreement) {
            // Define function name for error handling
            FunctionName = "DeleteAgreement(AgreementBLLC theAgreement)";

            // Define a connection
            OleDbConnection connection = OpenConnection();

            // Define a transaction object.
            OleDbTransaction transaction = connection.BeginTransaction();

            // Define a command object and link it to the transaction...
            OleDbCommand command = new OleDbCommand();
            command.Transaction = transaction;
            string strCommand = null;

            try {
                // Delete indexes for all items...
                foreach (AgreementItemBLLC item in theAgreement.ItemsOnAgreement) {
                    // Delete all indexes that have already been registered with the item...
                    strCommand = "Delete FROM billing_IndexesOnAgreementItems WHERE ItemID = ?";
                    command.Parameters.Clear();
                    CreateParameter(command, "ItemID", ParameterDirection.Input, item.ID, OleDbType.Integer);

                    // Execute the delete comments statement with a function from BaseDALC...
                    if (!ExecuteSQL(strCommand, command, connection)) {
                        throw new Exception("Could not delete agreement item indexes");
                    }
                }

                // Delete the items that are a part of the agreement...
                strCommand = "Delete FROM billing_AgreementItems WHERE AgreementID = ?";
                command.Parameters.Clear();
                CreateParameter(
                    command, "AgreementID", ParameterDirection.Input, theAgreement.AgreementID, OleDbType.Integer);

                // Execute the delete comments statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    throw new Exception("Could not delete agreement items");
                }

                // Delete all comments that have already been registered with the agreement...
                strCommand = "Delete FROM billing_AgreementComments WHERE AgreementID = ?";
                command.Parameters.Clear();
                CreateParameter(
                    command, "AgreementID", ParameterDirection.Input, theAgreement.AgreementID, OleDbType.Integer);

                // Execute the delete comments statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    throw new Exception("Could not delete agreement comments");
                }

                // Delete the agreement...
                strCommand = "Delete FROM billing_Agreement WHERE AgreementID = ?";
                command.Parameters.Clear();
                CreateParameter(
                    command, "AgreementID", ParameterDirection.Input, theAgreement.AgreementID, OleDbType.Integer);

                // Execute the delete comments statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    throw new Exception("Could not delete agreement");
                }

                transaction.Commit();
                return true;
            } catch (Exception err) {
                // Rollback the transaction if something went wrong...
                transaction.Rollback();
                Logger.WriteToLog(ClassName + " : " + FunctionName + err.Message, true);
                return false;
            } finally {
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Dispose();
                }
            }
        }

        /// <summary>
        /// Takes instance of Agreement and updates in the database
        /// </summary>
        /// <param name="theAgreement"></param>
        /// <returns>true if OK, false otherwise</returns>
        public bool UpdateAgreement(AgreementBLLC theAgreement) {
            // Define function name for error handling
            FunctionName = "UpdateAgreement(AgreementBLLC theAgreement)";

            // Define a connection
            OleDbConnection connection = OpenConnection();

            // Define a transaction object.
            OleDbTransaction transaction = connection.BeginTransaction();

            // Define the SQL statement to execute
            string strCommand =
                "UPDATE billing_Agreement SET AgreementNumber = ?, SubscriberID = ?, AgreementAmount = ?, GeneralDescription = ?, BeginDate = ?, EndDate = ?, Currency = ?,DiscountPercentage = ?, ReminderOffset = ?, NotificationEmails = ?, SignerCIID = ?, Updated = ?, UpdatedBy = ? WHERE AgreementID = ?";

            // Define a command object and link it to the transaction...
            OleDbCommand command = new OleDbCommand();
            command.Transaction = transaction;

            // Along with parameters
            CreateParameter(
                command, "AgreementNumber", ParameterDirection.Input, theAgreement.AgreementNumber, OleDbType.VarWChar);
            CreateParameter(
                command, "SubscriberID", ParameterDirection.Input, theAgreement.SubscriberID, OleDbType.Integer);
            CreateParameter(
                command, "AgreementAmount", ParameterDirection.Input, theAgreement.AgreementAmount, OleDbType.Currency);
            CreateParameter(
                command,
                "GeneralDescription",
                ParameterDirection.Input,
                theAgreement.GeneralDescription,
                OleDbType.VarWChar);
            CreateParameter(command, "BeginDate", ParameterDirection.Input, theAgreement.BeginDate, OleDbType.Date);
            CreateParameter(command, "EndDate", ParameterDirection.Input, theAgreement.EndDate, OleDbType.Date);
            CreateParameter(command, "Currency", ParameterDirection.Input, theAgreement.Currency, OleDbType.WChar);
            CreateParameter(
                command, "DiscountPercentage", ParameterDirection.Input, theAgreement.Discountpercent, OleDbType.Decimal);
            CreateParameter(
                command, "ReminderOffset", ParameterDirection.Input, theAgreement.ReminderOffset, OleDbType.Integer);
            CreateParameter(
                command,
                "NotificationEmails",
                ParameterDirection.Input,
                theAgreement.NotificationEmails,
                OleDbType.VarWChar);
            CreateParameter(command, "SignerCIID", ParameterDirection.Input, theAgreement.SignerCIID, OleDbType.Integer);
            CreateParameter(command, "Updated", ParameterDirection.Input, theAgreement.Updated, OleDbType.Date);
            CreateParameter(command, "UpdatedBy", ParameterDirection.Input, theAgreement.UpdatedBy, OleDbType.Integer);
            CreateParameter(
                command, "AgreementID", ParameterDirection.Input, theAgreement.AgreementID, OleDbType.Integer);

            try {
                // Execute the update statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    throw new Exception("Could not update agreement");
                }

                // Delete all comments that have already been registered with the agreement...
                strCommand = "Delete FROM billing_AgreementComments WHERE AgreementID = ?";
                command.Parameters.Clear();
                CreateParameter(
                    command, "AgreementID", ParameterDirection.Input, theAgreement.AgreementID, OleDbType.Integer);

                // Execute the delete comments statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    throw new Exception("Could not delete agreement comments");
                }

                // Insert all comments...
                foreach (AgreementCommentBLLC comment in theAgreement.CommentsOnAgreement) {
                    // Set the matching values between agreement and it's comments
                    comment.Updated = theAgreement.Updated;
                    comment.UpdatedBy = theAgreement.UpdatedBy;

                    if (!InsertAgreementComments(command, comment, connection)) {
                        throw new Exception("Could not update comment");
                    }
                }

                // Insert all items...
                foreach (AgreementItemBLLC item in theAgreement.ItemsOnAgreement) {
                    // Set the matching values between agreement and it's items
                    item.Updated = theAgreement.Updated;
                    item.UpdatedBy = theAgreement.UpdatedBy;

                    if (!InsertAgreementItems(command, item, connection, true)) {
                        throw new Exception("Could not insert item");
                    }
                }

                transaction.Commit();
                return true;
            } catch (Exception err) {
                // Rollback the transaction if something went wrong...
                transaction.Rollback();
                Logger.WriteToLog(ClassName + " : " + FunctionName + err.Message, true);
                return false;
            } finally {
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Dispose();
                }
            }
        }

        /// <summary>
        /// Takes agreement ID and returns list of Agreement Items
        /// </summary>
        /// <param name="nAgreementID">ID of the agreement</param>
        /// <returns>List of Agreement items</returns>
        public ArrayList GetAgreementItems(int nAgreementID) {
            FunctionName = "ArrayList GetAgreementItems(int nAgreementID)";
            ArrayList arrItems = new ArrayList();
            OleDbDataReader reader = null;
            OleDbCommand myOleDbCommand = null;
            // Get the agreement items
            AgreementItemBLLC myItem;

            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM billing_AgreementItems WHERE AgreementID =" + nAgreementID, myOleDbConn);
                    reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        myItem = new AgreementItemBLLC();
                        myItem.ID = reader.GetInt32(0);

                        if (!reader.IsDBNull(1)) {
                            myItem.AgreementID = reader.GetInt32(1);
                        }

                        if (!reader.IsDBNull(2)) {
                            myItem.ItemID = reader.GetInt32(2);
                        }

                        if (!reader.IsDBNull(3)) {
                            myItem.FixedAmount = reader.GetDecimal(3);
                        }

                        if (!reader.IsDBNull(4)) {
                            myItem.FixedDiscountPercent = reader.GetDecimal(4);
                        }

                        if (!reader.IsDBNull(5)) {
                            myItem.MonthlyDiscountPercent = reader.GetDecimal(5);
                        }

                        if (!reader.IsDBNull(6)) {
                            myItem.UsageDiscountPercent = reader.GetDecimal(6);
                        }

                        if (!reader.IsDBNull(7)) {
                            myItem.CounterLinkID = reader.GetInt32(7);
                        }

                        if (!reader.IsDBNull(8)) {
                            myItem.IsVisible = Convert.ToBoolean(reader.GetString(8));
                        }

                        if (!reader.IsDBNull(9)) {
                            myItem.BeginDate = reader.GetDateTime(9);
                        }

                        if (!reader.IsDBNull(10)) {
                            myItem.EndDate = reader.GetDateTime(10);
                        }

                        if (!reader.IsDBNull(11)) {
                            myItem.ReminderOffset = reader.GetInt32(11);
                        }

                        if (!reader.IsDBNull(12)) {
                            myItem.NotificationEmails = reader.GetString(12);
                        }

                        if (!reader.IsDBNull(13)) {
                            myItem.Inserted = reader.GetDateTime(13);
                        }

                        if (!reader.IsDBNull(14)) {
                            myItem.InsertedBy = reader.GetInt32(14);
                        }

                        if (!reader.IsDBNull(15)) {
                            myItem.Updated = reader.GetDateTime(15);
                        }

                        if (!reader.IsDBNull(16)) {
                            myItem.UpdatedBy = reader.GetInt32(16);
                        }

                        if (!reader.IsDBNull(18)) {
                            myItem.Freezone = reader.GetInt32(18);
                        }

                        // get the collections
                        myItem.IndexesOnAgreementItems = GetIndexesOnAgreementItems(myItem.ID);

                        // Get department collection
                        myItem.DepartmentIds = GetDepartmentsOnAgreementItems(myItem.ID);

                        // Set the item index in the agreement array (all items must have an index
                        // whithin the array to identify them)
                        myItem.Tag = arrItems.Count;

                        // Add the item
                        arrItems.Add(myItem);
                    }
                    reader.Close();
                } catch (Exception e) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + e.Message, true);
                } finally {
                    if (reader != null) {
                        reader.Close();
                    }

                    if (myOleDbConn != null) {
                        myOleDbConn.Dispose();
                    }

                    if (myOleDbCommand != null) {
                        myOleDbCommand.Dispose();
                    }
                }
            }
            return arrItems;
        }

        /// <summary>
        /// Takes agreement ID and returns list of Agreement Comments
        /// </summary>
        /// <param name="nAgreementID">ID of the agreement</param>
        /// <returns>List of Agreement comments</returns>
        public ArrayList GetAgreementComments(int nAgreementID) {
            FunctionName = "ArrayList GetAgreementComments(int nAgreementID)";
            ArrayList arrComments = new ArrayList();
            OleDbDataReader reader = null;
            OleDbCommand myOleDbCommand = null;

            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    // Get the agreement comments
                    AgreementCommentBLLC myComment;
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM billing_AgreementComments WHERE AgreementID =" + nAgreementID, myOleDbConn);
                    reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        myComment = new AgreementCommentBLLC();
                        myComment.ID = reader.GetInt32(0);
                        myComment.AgreementID = reader.GetInt32(1);

                        if (!reader.IsDBNull(2)) {
                            myComment.Comment = reader.GetString(2);
                        }

                        if (!reader.IsDBNull(3)) {
                            myComment.ReminderDate = reader.GetDateTime(3);
                        }

                        if (!reader.IsDBNull(4)) {
                            myComment.NotificationEmails = reader.GetString(4);
                        }

                        if (!reader.IsDBNull(5)) {
                            myComment.Inserted = reader.GetDateTime(5);
                        }

                        if (!reader.IsDBNull(6)) {
                            myComment.InsertedBy = reader.GetInt32(6);
                        }

                        if (!reader.IsDBNull(7)) {
                            myComment.Updated = reader.GetDateTime(7);
                        }

                        if (!reader.IsDBNull(8)) {
                            myComment.UpdatedBy = reader.GetInt32(8);
                        }

                        // Set the item index in the agreement array (all items must have an index
                        // whithin the array to identify them)
                        myComment.Tag = arrComments.Count;

                        // Add the item
                        arrComments.Add(myComment);
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + e.Message, true);
                } finally {
                    if (reader != null) {
                        reader.Close();
                    }

                    if (myOleDbConn != null) {
                        myOleDbConn.Dispose();
                    }

                    if (myOleDbCommand != null) {
                        myOleDbCommand.Dispose();
                    }
                }
            }
            return arrComments;
        }

        /// <summary>
        /// Takes agreement ID and returns instance of Agreement
        /// </summary>
        /// <param name="AgreementID">ID of the agreement</param>
        /// <returns>Instance of Agreement</returns>
        public AgreementBLLC GetAgreement(int AgreementID) {
            FunctionName = "AgreementBLLC GetAgreement(int AgreementID)";
            AgreementBLLC theAgreement = new AgreementBLLC();

            OleDbDataReader reader = null;
            OleDbCommand myOleDbCommand = null;

            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM billing_Agreement WHERE AgreementID =" + AgreementID, myOleDbConn);
                    reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        theAgreement.AgreementID = reader.GetInt32(0);
                        theAgreement.AgreementNumber = reader.GetString(1);
                        theAgreement.SubscriberID = reader.GetInt32(2);

                        if (!reader.IsDBNull(3)) {
                            theAgreement.AgreementAmount = reader.GetDecimal(3);
                        }

                        if (!reader.IsDBNull(4)) {
                            theAgreement.GeneralDescription = reader.GetString(4);
                        }

                        theAgreement.BeginDate = reader.GetDateTime(5);
                        theAgreement.EndDate = reader.GetDateTime(6);
                        theAgreement.Currency = reader.GetString(7);

                        if (!reader.IsDBNull(8)) {
                            theAgreement.Discountpercent = reader.GetDecimal(8);
                        }

                        if (!reader.IsDBNull(9)) {
                            theAgreement.ReminderOffset = reader.GetInt32(9);
                        }

                        if (!reader.IsDBNull(10)) {
                            theAgreement.NotificationEmails = reader.GetString(10);
                        }

                        if (!reader.IsDBNull(11)) {
                            theAgreement.SignerCIID = reader.GetInt32(11);
                        }

                        theAgreement.Inserted = reader.GetDateTime(12);
                        theAgreement.InsertedBy = reader.GetInt32(13);

                        if (!reader.IsDBNull(14)) {
                            theAgreement.Updated = reader.GetDateTime(14);
                        }

                        if (!reader.IsDBNull(15)) {
                            theAgreement.UpdatedBy = reader.GetInt32(15);
                        }
                    }
                    reader.Close();

                    theAgreement.ItemsOnAgreement = GetAgreementItems(AgreementID);
                    theAgreement.CommentsOnAgreement = GetAgreementComments(AgreementID);
                } catch (Exception e) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + e.Message, true);
                } finally {
                    if (reader != null) {
                        reader.Close();
                    }

                    if (myOleDbConn != null) {
                        myOleDbConn.Dispose();
                    }

                    if (myOleDbCommand != null) {
                        myOleDbCommand.Dispose();
                    }
                }

                return theAgreement;
            }
        }

        /// <summary>
        /// Stores connections between agreement items and departments in a data structure.
        /// </summary>
        /// <param name="command">OleDbCommand.</param>
        /// <param name="item">The agreement item being created.</param>
        /// <param name="connection">OleDbConnection</param>
        /// <param name="isUpdate">Is this update or insert.</param>
        /// <returns>True/False.</returns>
        public bool InsertAgreementItemDepartment(
            OleDbCommand command, AgreementItemBLLC item, OleDbConnection connection, bool isUpdate) {
            string strCommand;
            strCommand = "INSERT INTO c_billing_AgreementItem_Department(AgreementItemID,DepartmentID)VALUES(?,?)";

            foreach (int DepartmentID in item.DepartmentIds) {
                command.Parameters.Clear();
                // Set up parameters
                CreateParameter(command, "AgreementID", ParameterDirection.Input, item.ID, OleDbType.Integer);
                CreateParameter(command, "DepartmentID", ParameterDirection.Input, DepartmentID, OleDbType.Integer);

                // Execute the insert statement with a function from BaseDALC...
                if (!ExecuteSQL(strCommand, command, connection)) {
                    return false;
                }
            }

            // item and index list insert succeeded...
            return true;
        }

//		/// <summary>
//		/// Takes customers system ID (CIID) and returns Agreement as DataSet
//		/// </summary>
//		/// <param name="creditInfoID"></param>
//		/// <returns>Agreement as DataSet</returns>
//		public DataSet GetAgreementAsDataSet(int creditInfoID)
//		{
//			// Delete all indexes on all  that have already been registered with the agreement...
//			strCommand = "Delete FROM billing_AgreementComments WHERE AgreementID = ?";
//			command.Parameters.Clear();
//			CreateParameter(command, "AgreementID", ParameterDirection.Input, theAgreement.AgreementID, OleDbType.Integer);
//				
//			// Execute the delete comments statement with a function from BaseDALC...
//			if (!ExecuteSQL(strCommand, command, connection))
//				throw new Exception("Could not delete agreement comments");
//		}
//		/// <summary>
//		/// Deletes one agreement and all that is linked to it
//		/// </summary>
//		/// <param name="AgreementID"></param>
//		/// <returns>boolean if the agreement was successfully deleted.</returns>
//		public DataSet DeleteAgreement(int AgreementID)
//		{
//			DataSet dsAgreement = new DataSet();
//
//			return dsAgreement;
//		}
        /// <summary>
        /// Searches for an agreement by subscriber and agreement information.
        /// </summary>
        /// <param name="CreditInfoID">Unique CIID for a subscriber.</param>
        /// <param name="Name"></param>
        /// <param name="Address"></param>
        /// <param name="RegisteredBy"></param>
        /// <param name="AgreementID"></param>
        /// <param name="OpenClosed"></param>
        /// <param name="IdNumber"></param>
        /// <param name="nSignerCIID"></param>
        /// <param name="IsAgreementOnly"></param>
        /// <returns>DataSet.</returns>
        public DataSet SearchSubscribersAndAgreements(
            string CreditInfoID,
            string Name,
            string Address,
            string RegisteredBy,
            string AgreementID,
            string OpenClosed,
            string IdNumber,
            int nSignerCIID /*string SignerName*/,
            string IsAgreementOnly) {
            // Define function name for error handling
            FunctionName = "SearchSubscribersAndAgreements";

            DataSet mySet = new DataSet();
            bool whereStatement = false;
            bool NoID = true;

            string query = "SELECT * FROM billing_SubscribersAndAgreements WHERE ";

            if (IdNumber != "") {
                query += String.Format("Number = '{0}'", IdNumber);
                NoID = false;
            }

            // Fyrir CreditInfoID - Ef �a� er autt �� er leita� a� �llu	
            if (NoID) {
                query += "CreditInfoID LIKE('" + CreditInfoID + "%')";
                whereStatement = true;
            }

            if (Name != "" && NoID) {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "NameNative LIKE (N'" + Name + "%')";
                whereStatement = true;
            }

            if (Address != "" && NoID) {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "StreetNative LIKE (N'" + Address + "%')";
                whereStatement = true;
            }

            if (RegisteredBy != "" && NoID) {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "InsertedBy LIKE (N'" + RegisteredBy + "%')";
                whereStatement = true;
            }

            /*if(SignerName != "" && NoID) 
			{
				if(whereStatement)
					query += " AND ";
				
				query += "SignerName = " + SignerName + " ";
				whereStatement = true;
			}*/
            if (nSignerCIID > 0 && NoID) {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "SignerCIID = " + nSignerCIID + " ";
                whereStatement = true;
            }

            if (AgreementID != "" && NoID) {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "AgreementNumber LIKE (N'" + AgreementID + "%')";
                whereStatement = true;
            }

            if (OpenClosed == "Open" && NoID) {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "IsOpen = 'True'";
                whereStatement = true;
            }

            if (OpenClosed == "Closed" && NoID) {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "IsOpen = 'False'";
                whereStatement = true;
            }

            try {
                bool isAgreement = bool.Parse(IsAgreementOnly);
                if (whereStatement) {
                    query += " AND ";
                }

                if (isAgreement) {
                    query += "AgreementID is not null";
                } else {
                    query += "AgreementID is null";
                }
                whereStatement = true;
            } catch (Exception) {}

            // Define the SQL statement to execute and then execute it!
            return LoadDataSet(query);
        }

        #endregion

        #region Users

        /// <summary>
        /// Fetches a complete list a Employees.
        /// </summary>
        /// <remarks>
        /// Returns all rows from the table au_users
        /// </remarks>
        /// <returns>Employees as DataSet</returns>
        public DataSet GetEmployees() {
            string strSql = "SELECT npe.CreditInfoID, (npi.FirstNameNative + ' ' + npi.SurNameNative) AS NameNative, (npi.FirstNameEN + ' ' + npi.SurNameEN) AS NameEN "
                            + "FROM dbo.np_Individual AS npi "
                            + "INNER JOIN np_Employee AS npe ON npi.CreditInfoID = npe.CreditInfoID";
            return LoadDataSet(strSql);
        }

        public string GetEmployeeName(int nEmployeeCIID, bool IsNative) {
            FunctionName = "string GetEmployeeName(int nEmployeeCIID, bool IsNative)";

            string strSelect;
            if (IsNative) {
                strSelect = "(npi.FirstNameNative + ' ' + npi.SurNameNative) AS NameNative ";
            } else {
                strSelect = "(npi.FirstNameEN + ' ' + npi.SurNameEN) AS NameEN ";
            }
            string strSql = "SELECT " + strSelect
                            + "FROM dbo.np_Individual AS npi "
                            + "INNER JOIN np_Employee AS npe ON npi.CreditInfoID = npe.CreditInfoID "
                            + "WHERE npe.CreditInfoID = " + nEmployeeCIID;

            OleDbDataReader reader = null;
            OleDbCommand myOleDbCommand = null;
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    myOleDbCommand = new OleDbCommand(strSql, myOleDbConn);
                    reader = myOleDbCommand.ExecuteReader();

                    if (reader.Read() && !reader.IsDBNull(0)) {
                        return reader.GetString(0);
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + e.Message, true);
                } finally {
                    if (reader != null) {
                        reader.Close();
                    }

                    if (myOleDbConn != null) {
                        myOleDbConn.Dispose();
                    }

                    if (myOleDbCommand != null) {
                        myOleDbCommand.Dispose();
                    }
                }
            }
            return null;
        }

        #endregion

        #region BillingErrorPage

        public DataSet GetUsageWithoutProduct(DateTime dtFrom, DateTime dtTo) {
            // Notkun �n v�ru � billing.
            string strSql = "SELECT a.query_type,b.typeNative,count(*) total " // Total fr� upphafi.
                            + "FROM   np_usage a left join " // notkunf�rslur 
                            + "np_UsageTypes b on a.query_type=b.id left join " // logk��ar.
                            + "billing_itemregistry c on a.query_type=c.loggingcodeid " // billing v�run�mer
                            + "WHERE c.id IS NULL " // V�run�mer �n tengingar.
                            + "and created >='" + dtFrom.ToString("yyyy-MM-dd HH:mm:ss") + "' "
                            + "and created < '" + dtTo.ToString("yyyy-MM-dd HH:mm:ss") + "' "
                            + "GROUP BY a.query_type,b.typeNative";

            return LoadDataSet(strSql);
        }

        public DataSet GetUsageWithoutAgreement(DateTime dtFrom, DateTime dtTo) {
            // Notkun �n samnings
            string strSql = "SELECT users.subscriberid,e.SubscriberNickName,a.query_type,b.typeNative, COUNT(*) total, Min(a.created) fromdate, MAX(a.created) todate "
                            + "FROM   np_usage a left join " // notkunarf�rslur
                            + "np_UsageTypes b on a.query_type=b.id join " // Logk��ar
                            + "billing_itemregistry c on a.query_type=c.loggingcodeid left join " // v�run�mer
                            + "au_users users on a.userid=users.id left join " // tengja notendur vi� subscriber-a
                            + "au_subscribers e on users.subscriberid=e.creditinfoid " // S�kja nafn � subscriber.
                            + "WHERE " // Finna v�run�mer sem eiga s�r ekki samning � �eim t�ma sem notkun var skr��.
                            +
                            "c.id not in(SELECT d.itemid FROM billing_agreementitems d join billing_agreement f on d.agreementid=f.agreementid "
                            +
                            "WHERE f.subscriberid=e.creditinfoid and d.begindate <= a.created and DATEADD(dd,1,d.enddate)>= a.created) "
                            // ver�ur a� vera created � f�rsluna en ekki parameters.
                            + "and a.created >='" + dtFrom.ToString("yyyy-MM-dd HH:mm:ss") + "' "
                            + "and a.created < '" + dtTo.ToString("yyyy-MM-dd HH:mm:ss") + "' "
                            + "GROUP BY users.subscriberid,e.SubscriberNickName,a.query_type,b.typeNative "
                            + "ORDER BY e.SubscriberNickName";
            return LoadDataSet(strSql);
        }

        public DataSet GetUsageWithoutUsers(DateTime dtFrom, DateTime dtTo) {
            string strSql = "SELECT Creditinfoid, query_type, b.typeNative, COUNT(*) total, Min(created) fromdate, MAX(created) todate "
                            + "FROM np_Usage u "
                            + "LEFT JOIN np_UsageTypes b on u.query_type=b.id "
                            + "WHERE u.UserID NOT IN "
                            + "( SELECT id FROM au_users ) "
                            + "AND created >='" + dtFrom.ToString("yyyy-MM-dd HH:mm:ss") + "' "
                            + "AND created < '" + dtTo.ToString("yyyy-MM-dd HH:mm:ss") + "' "
                            + "GROUP BY Creditinfoid, query_type, b.typeNative";
            return LoadDataSet(strSql);
        }

        public DataSet GetUsageWithMultipleBillingItems(DateTime dtFrom) {
            string strSql = "SELECT ba.subscriberid,e.SubscriberNickName, ba.AgreementNumber, bi.id, b.typeNative, c.AgreementNumber as ConflictAgreementNumber, bai.BeginDate, bai.EndDate, c.BeginDate ConflictBeginDate, c.EndDate ConflictEndDate "
                            + "FROM billing_agreement ba "
                            + " JOIN billing_agreementitems bai ON bai.agreementid=ba.agreementid "
                            + " JOIN billing_ItemRegistry bi ON bai.ItemID=bi.id "
                            +
                            " JOIN (SELECT ba2.SubscriberID, ba2.AgreementNumber, bi2.id, bai2.BeginDate, bai2.EndDate, bai2.id baiID "
                            + "		FROM billing_agreement ba2 "
                            + "			JOIN billing_agreementitems bai2 ON bai2.agreementid=ba2.agreementid "
                            + "			JOIN billing_ItemRegistry bi2 ON bai2.ItemID=bi2.id "
                            +
                            "	) c ON c.SubscriberID=ba.SubscriberID AND bi.id = c.id AND c.BeginDate <= bai.EndDate AND c.EndDate >= bai.BeginDate AND c.EndDate >= '" +
                            dtFrom.ToString("yyyy-MM-dd HH:mm:ss") + "' AND c.baiID <> bai.ID "
                            + "	JOIN np_UsageTypes b on bi.loggingcodeid=b.id "
                            + "	LEFT JOIN au_subscribers e on ba.subscriberid=e.creditinfoid "
                            // S�kja nafn � subscriber.				
                            + "WHERE bai.EndDate >= '" + dtFrom.ToString("yyyy-MM-dd HH:mm:ss") + "'";

            return LoadDataSet(strSql);
        }

        #endregion

        public string MakeBillingExcelFile(
            string AppPath, int SubscriberID, DateTime Period_from, DateTime Period_to, int DetailLevel, int IsBillable) {
            FunctionName =
                "MakeBillingExcelFile (string AppPath, int SubscriberID, DateTime Period_from, DateTime Period_to, int DetailLevel)";

            //You use these variables throughout the application.
            string fileExcel, fileName, strLine, sql;
            FileStream objFileStream;
            StreamWriter objStreamWriter;
            OleDbDataReader dr = null;

            Random nRandom = new Random(DateTime.Now.Millisecond);

            //Create a random file name.
            fileExcel = "t" + nRandom.Next() + ".xls";

            //Set a virtual folder to save the file.
            //Make sure to change the application name to match your folder.
            fileName = AppPath + @"\ExcelOutput\" + fileExcel;

            //Use FileSystem objects to create the .xls file.
            objFileStream = new FileStream(fileName, FileMode.CreateNew, FileAccess.Write);
            objStreamWriter = new StreamWriter(objFileStream, Encoding.Unicode);

            //Use a DataReader object to connect to the Pubs database.
            OleDbCommand myOleDbCommand = null;

            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    if (IsBillable > -1) {
                        sql = @"select * from billing_getamount(?, ?, ?, ?, ?)";
                    } else {
                        sql = @"select * from billing_getamount(?, ?, ?, ?)";
                    }

                    myOleDbConn.Open();
                    myOleDbCommand = new OleDbCommand(sql, myOleDbConn);

                    CreateParameterDefValue(
                        myOleDbCommand,
                        "@p_subscriber_id",
                        ParameterDirection.Input,
                        SubscriberID,
                        OleDbType.Integer,
                        null,
                        "-1");
                    CreateParameter(
                        myOleDbCommand, "@p_period_from", ParameterDirection.Input, Period_from, OleDbType.Date);
                    CreateParameter(myOleDbCommand, "@p_period_to", ParameterDirection.Input, Period_to, OleDbType.Date);
                    CreateParameter(
                        myOleDbCommand, "@p_detail", ParameterDirection.Input, DetailLevel, OleDbType.Integer);
                    if (IsBillable > -1) {
                        CreateParameter(
                            myOleDbCommand, "@p_OnlyBillable", ParameterDirection.Input, IsBillable, OleDbType.Boolean);
                    }

                    dr = myOleDbCommand.ExecuteReader();

                    //Initialize the string that is used to build the file.
                    strLine = "";

                    //Enumerate the field names and the records that are used to build 
                    //the file.
                    for (int i = 0; i <= dr.FieldCount - 1; i++) {
                        strLine = strLine + dr.GetName(i) + Convert.ToChar(9);
                    }

                    //Write the field name information to the file.
                    objStreamWriter.WriteLine(strLine);

                    //Reinitialize the string for data.
                    strLine = "";

                    //Enumerate the database that is used to populate the file.
                    while (dr.Read()) {
                        for (int i = 0; i <= dr.FieldCount - 1; i++) {
                            strLine = strLine + dr.GetValue(i) + Convert.ToChar(9);
                        }
                        objStreamWriter.WriteLine(strLine);
                        strLine = "";
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + e.Message, true);
                } finally {
                    //Clean up.
                    if (dr != null) {
                        dr.Close();
                    }

                    if (myOleDbConn != null) {
                        myOleDbConn.Dispose();
                    }

                    if (myOleDbCommand != null) {
                        myOleDbCommand.Dispose();
                    }

                    objStreamWriter.Flush();
                    objStreamWriter.Close();
                    objFileStream.Close();
                }
            }

            //Return the path to the Excel file.
            return fileExcel;
        }
    } // END CLASS DEFINITION BillingDALC
}