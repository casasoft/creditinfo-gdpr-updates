<%@ Page language="c#" Codebehind="PopupForm.aspx.cs" AutoEventWireup="false" EnableEventValidation="false" Inherits="Billing.PopupForm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>
			<asp:label id="lblHeader" runat="server">Label</asp:label></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../DatePicker.js"></script>
		<script language="Javascript">
		function SetCIID(ciid)
        {
           document.write("inside SetCIID(ciid) function");
            // retrieve from the querystring the value of the Ctl param,
            // that is the name of the input control on the parent form
            // that the user want to set with the clicked date
           ctl = window.location.search.substr(1).substring(4);
            // set the value of that control with the passed date
           window.opener.document.forms[0].elements[ctl].value = ciid;
            // close this popup
           self.close();
        }
		</script>
	</head>
	<body ms_positioning="GridLayout" onload="SetFormFocus()">
		<form id="UserHandlingForm" name="UserHandlingForm" method="post" runat="server">
			<table cellspacing="0" cellpadding="2" width="700" border="0">
				<tr>
					<td>
						<!-- IF setningar sem �kvar�a hva�a user control er loada� inn � selluna... -->
						<asp:placeholder id="placeHolder" runat="server"></asp:placeholder>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
