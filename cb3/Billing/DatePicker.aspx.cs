#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace Billing {
    /// <summary>
    /// Summary description for DatePicker.
    /// </summary>
    public class DatePicker : Page {
        protected Calendar DatePick;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
        }

        private void DatePick_DayRender(object sender, DayRenderEventArgs e) {
            // create a hyperlink control, and set its text to the cell's current text
            var hl = new HyperLink
                     {
                         Text = ((LiteralControl) e.Cell.Controls[0]).Text,
                         NavigateUrl = ("javascript:SetDate('" + e.Day.Date.ToShortDateString() + "');")
                     };

            // set the navigation url (the href attribute) to the javascript procedure
            // defined wihin the client-side <script> block, and pass in input
            // the clicked date in short format

            // remove the cell's current child controls, and add the new hyperlink
            e.Cell.Controls.Clear();
            e.Cell.Controls.Add(hl);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.DatePick.DayRender += new DayRenderEventHandler(this.DatePick_DayRender);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}