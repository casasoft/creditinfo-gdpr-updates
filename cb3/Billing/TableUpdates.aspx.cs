#region

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

#endregion

namespace Billing {
    /// <summary>
    /// Summary description for TableUpdates.
    /// </summary>
    public class TableUpdates : Page {
        protected HtmlTableCell blas;
        protected PlaceHolder myPlaceHolder;
        // public Control uc;
        private void Page_Load(object sender, EventArgs e) {
            Control uc;
            var strType = Request.QueryString["Type"];

            if (strType == "") {
                strType = ViewState["CurrentPage"].ToString();
            }

            ViewState["CurrentPage"] = strType;

            switch (strType) {
                case "Currency":
                    uc = LoadControl("user_controls/Business_Ctrls/CurrencyListCtrl.ascx");
                    uc.ID = "CurrencyListCtrl1";
                    // Billing - Number comes form Products table
                    Page.ID = "702";
                    break;

                case "Report":
                    uc = LoadControl("user_controls/Business_Ctrls/ReportCtrl.ascx");
                    uc.ID = "ReportCtrl1";
                    Page.ID = "706";
                    break;

                case "IndexOverview":
                    uc = LoadControl("user_controls/Business_Ctrls/IndexOverviewCtrl.ascx");
                    uc.ID = "IndexOverviewctrl1";
                    // Billing - Number comes form Products table
                    Page.ID = "703";
                    break;

                case "Indexes":
                    uc = LoadControl("user_controls/Business_Ctrls/IndexListCtrl.ascx");
                    uc.ID = "Indexlistctrl1";
                    // Billing - Number comes form Products table
                    Page.ID = "704";
                    break;

                case "Items":
                    uc = LoadControl("user_controls/Business_Ctrls/ItemRegistryCtrl.ascx");
                    uc.ID = "ItemRegistryCtrl";
                    // Billing - Number comes form Products table
                    Page.ID = "705";
                    break;

                case "Agreement":

                    uc = LoadControl("user_controls/Business_Ctrls/AgreementCtrl.ascx");
                    uc.ID = "AgreementCtrl1";
                    // Billing - Number comes form Products table
                    Page.ID = "707";
                    break;

                case "AgreementComments":
                    uc = LoadControl("user_controls/Business_Ctrls/AgreementCommentsCtrl.ascx");
                    uc.ID = "AgreementCommentsCtrl1";
                    // Billing - Number comes form Products table
                    Page.ID = "708";
                    break;

                case "AgreementItems":
                    uc = LoadControl("user_controls/Business_Ctrls/AgreementItemsCtrl.ascx");
                    uc.ID = "AgreementItemsCtrl1";
                    // Billing - Number comes form Products table
                    Page.ID = "709";
                    break;

                case "UsageError":
                    uc = LoadControl("user_controls/Business_Ctrls/UsageErrorCtrl.ascx");
                    uc.ID = "UsageErrorCtrl";
                    Page.ID = "710";
                    break;

                default:
                    uc = LoadControl("user_controls/Business_Ctrls/FindCtrl.ascx");
                    uc.ID = "FindCtrl1";
                    // Billing - Number comes form Products table
                    Page.ID = "701";
                    break;
            }

            if (uc != null) {
                blas.Controls.Add(uc);
            }

//			// Fyrir Currency control
//			if(Request.Params["Type"].ToString() == "Currency")
//			{
//				uc = LoadControl("user_controls/Business_Ctrls/CurrencyListCtrl.ascx");
//				uc.ID = "CurrencyListCtrl1";
//				blas.Controls.Add(uc);
//			}
//
//			// Fyrir Index control
//			if(Request.Params["Type"].ToString() == "Indexes")
//			{
//				uc = LoadControl("user_controls/Business_Ctrls/IndexListCtrl.ascx");
//				uc.ID = "Indexlistctrl1";
//				blas.Controls.Add(uc);
//			}
//			
//			// Fyrir Item control
//			if(Request.Params["Type"].ToString() == "Items")
//			{
//				uc = LoadControl("user_controls/Business_Ctrls/ItemIndexCtrl.ascx");
//				uc.ID = "Indexlistctrl1";
//				blas.Controls.Add(uc);
//			}

            // Response.Write(blas.Controls.Count.ToString());
            // blas.Controls.Clear();
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}