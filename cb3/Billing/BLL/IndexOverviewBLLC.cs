namespace Billing.BLL {
    /// <summary>
    ///  This class keeps overview information about indexes
    /// </summary>
    public class IndexOverviewBLLC {
        public IndexOverviewBLLC() {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// Initalizes the index according to variables.
        /// </summary>
        /// <remarks>Index initialization must contain all fields</remarks>
        public IndexOverviewBLLC(string IndexName, bool IndexActive, string NativeDesc, string EngDesc) {
            Name = IndexName;
            Active = IndexActive;
            DescNative = NativeDesc;
            DescEN = EngDesc;
        }

        /// <summary>
        /// Gets/Sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets/Sets the active attribute
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets/Sets the descNative attribute
        /// </summary>
        public string DescNative { get; set; }

        /// <summary>
        /// Gets/Sets the descEN attribute
        /// </summary>
        public string DescEN { get; set; }
    }
}