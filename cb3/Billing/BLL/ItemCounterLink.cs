namespace Billing.BLL {
    /// <summary>
    /// Summary description for ItemCounterLink.
    /// </summary>
    public class ItemCounterLink {
        public int ID { get; set; }
        public string Name { get; set; }
        public string LinkRule { get; set; }
    }
}