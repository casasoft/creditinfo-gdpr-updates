#region

using System;
using System.Data;

#endregion

namespace Billing.BLL {
    /// <remarks>
    /// This class provides neccessary calc. functions for billing handling
    /// </summary>
    public class BillingBLLC {
        /// <summary>
        /// Calculates amount payable (Used with index)
        /// </summary>
        /// <param name="userCIID">The users internal system number</param>
        /// <param name="from">Start period</param>
        /// <param name="to">End period</param>
        /// <returns>The amount payable</returns>
        public double CalculateAmountPayable(int userCIID, DateTime from, DateTime to) { return -1.1; }

        /// <summary>
        /// Calculates price
        /// </summary>
        /// <param name="userCIID">The users internal system number</param>
        /// <param name="from">Start period</param>
        /// <param name="to">End period</param>
        /// <returns>The calculated price</returns>
        public double CalculatePrice(int userCIID, DateTime from, DateTime to) { return -1.1; }

        /// <summary>
        /// Calculates price with index
        /// </summary>
        /// <param name="userCIID">The users internal system number</param>
        /// <param name="from">Start period</param>
        /// <param name="to">End period</param>
        /// <returns>Calculated price with index</returns>
        public double CalculatePriceWithIndex(int userCIID, DateTime from, DateTime to) { return -1.1; }

        /// <summary>
        /// Calculates discount amount
        /// </summary>
        /// <param name="userCIID">The users internal system number</param>
        /// <param name="from">Start period</param>
        /// <param name="to">End period</param>
        /// <returns>Calculated discont amount</returns>
        public double CalculateDiscountAmount(int userCIID, DateTime from, DateTime to) { return -1.1; }

        /// <summary>
        /// Calculates total price
        /// </summary>
        /// <param name="userCIID">The users internal system number</param>
        /// <param name="from">Start period</param>
        /// <param name="to">End period</param>
        /// <returns>Calculated total price</returns>
        public double CalculateTotalPrice(int userCIID, DateTime from, DateTime to) { return -1.1; }

        /// <summary>
        /// Get all info neccessary to make report
        /// </summary>
        /// <param name="userCIID"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns>Billing report as DataSet</returns>
        public DataSet GetBillingReportAsDataSet(int userCIID, DateTime from, DateTime to) {
            DataSet dsBillingReport = new DataSet();
            return dsBillingReport;
        }

        /// <summary>
        /// Get all info needed for billing as comma seperated string
        /// </summary>
        /// <param name="userCIID"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns>Billing report as comma seperated string</returns>
        public string GetBillingReportCommaSeperated(int userCIID, DateTime from, DateTime to) { return "tmp"; }

        /// <summary>
        /// Indicates whether notification email should be send or not
        /// </summary>
        /// <returns>bool true/false</returns>
        public bool SendNotificationEmail() { return false; }
    } // END CLASS DEFINITION BillingBLLC
}