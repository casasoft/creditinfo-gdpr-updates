#region

using System.Web.Mail;

#endregion

namespace Billing.BLL {
    /// <summary>
    /// HelpFunctions Class is used to host methods and variables that extend the functionality and usability 
    /// of the system.
    /// </summary>
    /// <remarks>Generic methods and variables that extend the system are supposed to reside in the HelpFunctions
    /// class. Internal functionality and algorithms (f.x. calculations) are stored elsewhere.</remarks>
    public class HelpFunctions {
        /// <summary>
        /// Used to send mail through smtpMail
        /// </summary>
        /// <remarks>
        /// The method uses a fixed mail server (f.x. hermes.lt.is). Message and Subject variables are used
        /// to set the mail content. All mail messages sent through this method are set to high priority. Fixed
        /// elements include:
        /// <code>
        /// myMail.From = "error@lt.is";
        /// myMail.To = "error@lt.is";
        /// myMail.Priority = MailPriority.High;
        /// myMail.BodyFormat = MailFormat.Text;
        /// SmtpMail.SmtpServer = "hermes.lt.is";
        /// </code>
        /// <seealso cref="System.Web.Mail.MailMessage"/>
        /// </remarks>
        /// <param name="Message">Reference by Value - Message content</param>
        /// <param name="Subject">Reference by Value - Message subject</param>
        public void SendMail(string Message, string Subject) {
            MailMessage myMail = new MailMessage();
            myMail.From = "error@lt.is";
            myMail.To = "error@lt.is";
            myMail.Subject = Subject;
            myMail.Priority = MailPriority.High;
            myMail.BodyFormat = MailFormat.Text;
            myMail.Body = Message;
            SmtpMail.SmtpServer = "hermes.lt.is";
            SmtpMail.Send(myMail);
        }
    }
}