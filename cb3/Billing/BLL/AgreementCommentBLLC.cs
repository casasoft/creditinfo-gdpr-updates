#region

using System;
using System.Reflection;
using System.Text;

#endregion

namespace Billing.BLL {
    /// <summary>
    /// Summary description for AgreementCommentBLLC.
    /// </summary>
    [Serializable]
    public class AgreementCommentBLLC {
        public AgreementCommentBLLC() {
            Inserted = DateTime.MinValue;
            InsertedBy = -1;
            Tag = -1;
        }

        /// <summary>
        /// Get / set the unique identifier of the comment
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Get / set the Agreement ID that connects to the comment
        /// </summary>
        public int AgreementID { get; set; }

        /// <summary>
        /// Get / set the comment text
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Get / set the reminderDate
        /// </summary>
        public DateTime ReminderDate { get; set; }

//		/// <summary>
//		/// the email addresses of the notification recievers
//		/// </summary>
//		private string notificationEmail;
//
//		/// <summary>
//		/// Get / set the notificationEmails
//		/// </summary>
//		public string NotificationEmail
//		{
//			get { return notificationEmail ; }
//			set { notificationEmail = value; }
//		}
        /// <summary>
        /// Get / set the notificationEmails
        /// </summary>
        public string NotificationEmails { get; set; }

        /// <summary>
        /// Get / set the inserted date
        /// </summary>
        public DateTime Inserted { get; set; }

        /// <summary>
        /// Get / set who inserted (user id)
        /// </summary>
        public int InsertedBy { get; set; }

        /// <summary>
        /// Get / set the updated date
        /// </summary>
        public DateTime Updated { get; set; }

        /// <summary>
        /// Get / set who updatedBy (user id)
        /// </summary>
        public int UpdatedBy { get; set; }

        /// <summary>
        /// Get / set internal array list tag
        /// </summary>
        public int Tag { get; set; }

        public string ToXml() {
            StringBuilder sbXml = new StringBuilder();
            sbXml.Append("<" + GetType().Name + ">");
            foreach (PropertyInfo property in GetType().GetProperties()) {
                sbXml.Append("<" + property.Name);
                sbXml.Append(" type=\"" + property.PropertyType.Name + "\">");
                Object oValue = property.GetValue(this, null);
                sbXml.Append(oValue);
                sbXml.Append("</" + property.Name + ">");
            }
            sbXml.Append("</" + GetType().Name + ">");
            return sbXml.ToString();
        }
    }
}