#region

using System;

#endregion

namespace Billing.BLL {
    /// <summary>
    /// This class represents Billing Items from the item registry
    /// </summary>
    /// <remarks>One instance of this class represents one billing item from the item registry (billing_ItemRegistry).</remarks>
    public class BillingItemBLLC {
        public BillingItemBLLC() {
            // -1 means a new record....
            ItemID = -1;
            IsActive = true;
        }

        /// <summary>
        /// Gets / sets the Item ID (automatically incremented in the ddb)
        /// </summary>
        public int ItemID { get; set; }

        /// <summary>
        /// Gets / sets the item currency
        /// </summary>
        public string ItemCurrency { get; set; }

        /// <summary>
        /// Gets / sets the item name in native tounge
        /// </summary>
        public string ItemNameNative { get; set; }

        /// <summary>
        /// Gets / sets the item name in english tounge
        /// </summary>
        public string ItemNameEN { get; set; }

        /// <summary>
        /// Gets / sets the item monthly amount payable
        /// </summary>
        public Decimal MonthlyAmount { get; set; }

        /// <summary>
        /// Gets / sets the item usage amount payable
        /// </summary>
        public Decimal UsageAmount { get; set; }

        /// <summary>
        /// Gets / sets the default monthly discount (as percent)
        /// </summary>
        public Decimal MonthlyDiscountPercent { get; set; }

        /// <summary>
        /// Gets / sets the default usage discount (as percent)
        /// </summary>
        public Decimal UsageDiscountPercent { get; set; }

        /// <summary>
        /// Gets / sets the item logging code ID
        /// </summary>
        public int LoggingCodeID { get; set; }

        /// <summary>
        /// Gets / sets the item product code ID
        /// </summary>
        public int ProductCodeID { get; set; }

        /// <summary>
        /// Gets / sets if the item is active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets / sets the financial statement key value.
        /// </summary>
        public string FinancialKey { get; set; }

        /// <summary>
        /// Gets / sets the master item ID.
        /// </summary>
        public int MasterItemID { get; set; }

        /// <summary>
        /// Gets / sets DateTime inserted
        /// </summary>
        public DateTime Inserted { get; set; }

        /// <summary>
        /// Gets / sets who inserted
        /// </summary>
        public int InsertedBy { get; set; }

        /// <summary>
        /// Gets / sets DateTime updated
        /// </summary>
        public DateTime Updated { get; set; }

        /// <summary>
        /// Gets / sets who updated
        /// </summary>
        public int UpdatedBy { get; set; }
    }
}