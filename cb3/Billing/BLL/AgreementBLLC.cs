#region

using System;
using System.Collections;
using System.Reflection;
using System.Text;

#endregion

namespace Billing.BLL {
    /// <summary>This class keeps info about Agreements</summary>
    /// <remarks>
    /// This class keeps info about Agreements (billing_Agreement)
    /// </remarks>
    [Serializable]
    public class AgreementBLLC {
//		/// <summary>
//		/// Notification email entry.
//		/// </summary>
//		private string notificationEmail1;
//
//		/// <summary>
//		/// Notification email entry.
//		/// </summary>
//		private string notificationEmail2;
        /// <summary>
        /// The name of the person that signed the contract.
        /// </summary>
        private string signerName;

        public AgreementBLLC() {
            AgreementID = -1;

            Inserted = DateTime.MinValue;
            InsertedBy = -1;

//			SignerCIID = -1;

            ItemsOnAgreement = new ArrayList();
            CommentsOnAgreement = new ArrayList();
        }

        /// <summary>
        /// Get/Sets the agreementID
        /// </summary>
        public int AgreementID { get; set; }

        /// <summary>
        /// Get/Sets the agreementNumber
        /// </summary>
        public string AgreementNumber { get; set; }

        /// <summary>
        /// Get/Sets the subscriberID
        /// </summary>
        public int SubscriberID { get; set; }

        /// <summary>
        /// Get/Sets the agreementAmount
        /// </summary>
        public Decimal AgreementAmount { get; set; }

        /// <summary>
        /// Get/Sets the generalDescription
        /// </summary>
        public string GeneralDescription { get; set; }

        /// <summary>
        /// Get/Sets the beginDate
        /// </summary>
        public DateTime BeginDate { get; set; }

        /// <summary>
        /// Get/Sets the endDate
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Get/Sets the currency
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Get/Sets the discount percentage
        /// </summary>
        public Decimal Discountpercent { get; set; }

        /// <summary>
        /// Get/Sets the reminderOffset
        /// </summary>
        public int ReminderOffset { get; set; }

//		/// <summary>
//		/// Get/Sets the notificationEmail1
//		/// </summary>
//		public string NotificationEmail1
//		{
//			get {return this.notificationEmail1;}
//			set {this.notificationEmail1 = value;}
//		}
//
//		/// <summary>
//		/// Get/Sets the notificationEmail1
//		/// </summary>
//		public string NotificationEmail2
//		{
//			get {return this.notificationEmail2;}
//			set {this.notificationEmail2 = value;}
//		}
        /// <summary>
        /// Get/Sets the notificationEmail1
        /// </summary>
        public string NotificationEmails { get; set; }

//		/// <summary>
//		/// Get/Sets the signerName
//		/// </summary>
//		public string SignerName
//		{
//			get {return this.signerName;}
//			set {this.signerName = value;}
//		}
        /// <summary>
        /// Get/Sets the _nSignerCIID
        /// </summary>
        public int SignerCIID { get; set; }

        /// <summary>
        /// Get/Sets the estimated total price (calculated property)
        /// </summary>
        public double EstimatedTotalPrice { get; set; }

        /// <summary>
        /// Gets / sets DateTime inserted
        /// </summary>
        public DateTime Inserted { get; set; }

        /// <summary>
        /// Gets / sets who inserted
        /// </summary>
        public int InsertedBy { get; set; }

        /// <summary>
        /// Gets / sets DateTime updated
        /// </summary>
        public DateTime Updated { get; set; }

        /// <summary>
        /// Gets / sets who updated
        /// </summary>
        public int UpdatedBy { get; set; }

        /// <summary>
        /// Gets / sets the array list that holds items
        /// </summary>
        public ArrayList ItemsOnAgreement { get; set; }

        /// <summary>
        /// Gets / sets the array list that holds comments
        /// </summary>
        public ArrayList CommentsOnAgreement { get; set; }

        public string ToXml() {
            StringBuilder sbXml = new StringBuilder();
            sbXml.Append("<" + GetType().Name + ">");
            foreach (PropertyInfo property in GetType().GetProperties()) {
                sbXml.Append("<" + property.Name);
                sbXml.Append(" type=\"" + property.PropertyType.Name + "\">");
                Object oValue = property.GetValue(this, null);
                sbXml.Append(oValue);
                sbXml.Append("</" + property.Name + ">");
            }
            sbXml.Append("</" + GetType().Name + ">");
            return sbXml.ToString();
        }
    }
}