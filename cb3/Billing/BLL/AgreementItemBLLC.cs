#region

using System;
using System.Collections;
using System.Reflection;
using System.Text;

#endregion

namespace Billing.BLL {
    /// <summary>
    /// Summary description for AgreementItemBLLC.
    /// </summary>
    [Serializable]
    public class AgreementItemBLLC {
        public AgreementItemBLLC() {
            IndexesOnAgreementItems = new ArrayList();
            ID = -1;
            CounterLinkID = -1;
            Tag = -1;
            Freezone = 0;
            DepartmentIds = new ArrayList();
        }

        /// <summary>
        /// Get / set the unique identifier of the agreement item
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Get / set the Agreement ID that connects to the item
        /// </summary>
        public int AgreementID { get; set; }

        /// <summary>
        /// Get / set the itemID that connects to the agreement item
        /// </summary>
        public int ItemID { get; set; }

        /// <summary>
        /// Get / set the fixedAmount
        /// </summary>
        public Decimal FixedAmount { get; set; }

        /// <summary>
        /// Get / set the fixedDiscountPercent
        /// </summary>
        public Decimal FixedDiscountPercent { get; set; }

        /// <summary>
        /// Get / set the monthlyDiscountPercent
        /// </summary>
        public Decimal MonthlyDiscountPercent { get; set; }

        /// <summary>
        /// Get / set the usageDiscountPercent
        /// </summary>
        public Decimal UsageDiscountPercent { get; set; }

        /// <summary>
        /// Get / set the counterLinkID
        /// </summary>
        public int CounterLinkID { get; set; }

        /// <summary>
        /// Get / set the isVisible
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Get / set the beginDate
        /// </summary>
        public DateTime BeginDate { get; set; }

        /// <summary>
        /// Get / set the endDate
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Get / set the reminderOffset
        /// </summary>
        public int ReminderOffset { get; set; }

//		/// <summary>
//		/// Email addresses for recipients
//		/// </summary>
//		private string notificationEmail1;
//
//		/// <summary>
//		/// Get / set the notificationEmails
//		/// </summary>
//		public string NotificationEmail1
//		{
//			get { return notificationEmail1; }
//			set { notificationEmail1 = value; }
//		}
//
//		/// <summary>
//		/// Email addresses for recipients
//		/// </summary>
//		private string notificationEmail2;
//
//		/// <summary>
//		/// Get / set the notificationEmails
//		/// </summary>
//		public string NotificationEmail2
//		{
//			get { return notificationEmail2; }
//			set { notificationEmail2 = value; }
//		}
        /// <summary>
        /// Get / set the notificationEmails
        /// </summary>
        public string NotificationEmails { get; set; }

        /// <summary>
        /// Gets / sets DateTime inserted
        /// </summary>
        public DateTime Inserted { get; set; }

        /// <summary>
        /// Gets / sets who inserted
        /// </summary>
        public int InsertedBy { get; set; }

        /// <summary>
        /// Gets / sets DateTime updated
        /// </summary>
        public DateTime Updated { get; set; }

        /// <summary>
        /// Gets / sets who updated
        /// </summary>
        public int UpdatedBy { get; set; }

        /// <summary>
        /// Gets / sets who updated
        /// </summary>
        public int Freezone { get; set; }

        /// <summary>
        /// Gets / sets IndexesOnAgreementItems arraylist
        /// </summary>
        public ArrayList IndexesOnAgreementItems { get; set; }

        /// <summary>
        /// Get / set internal array list tag
        /// </summary>
        public int Tag { get; set; }

        /// <summary>
        /// Gets or sets a collection of department id's.
        /// </summary>
        public ArrayList DepartmentIds { get; set; }

        public string ToXml() {
            StringBuilder sbXml = new StringBuilder();
            sbXml.Append("<" + GetType().Name + ">");
            foreach (PropertyInfo property in GetType().GetProperties()) {
                sbXml.Append("<" + property.Name);
                sbXml.Append(" type=\"" + property.PropertyType.Name + "\">");
                Object oValue = property.GetValue(this, null);
                sbXml.Append(oValue);
                sbXml.Append("</" + property.Name + ">");
            }
            sbXml.Append("</" + GetType().Name + ">");
            return sbXml.ToString();
        }
    }
}