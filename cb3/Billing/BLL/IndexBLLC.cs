#region

using System;

#endregion

namespace Billing.BLL {
    /// <remarks>
    /// This class keeps detail information about indexes
    /// </remarks>
    public class IndexBLLC {
        /// <summary>
        /// Desfault contructor
        /// </summary>
        /// <remarks></remarks>
        public IndexBLLC() { }

        /// <summary>
        /// Initalizes the index according to variables.
        /// </summary>
        /// <remarks>Index initialization must contain all fields</remarks>
        public IndexBLLC(string IndexName, Decimal IndexVal, DateTime IndexDate) {
            Name = IndexName;
            IndexValue = IndexVal;
            this.IndexDate = IndexDate;
        }

        /// <summary>
        /// Gets/Sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets/Sets the percent attribute
        /// </summary>
        public Decimal IndexValue { get; set; }

        /// <summary>
        /// Gets/Sets the beginDate attribute
        /// </summary>
        public DateTime IndexDate { get; set; }
    } // END CLASS DEFINITION IndexBLLC
}