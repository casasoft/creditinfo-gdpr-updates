#region

using System;
using System.Collections;
using System.Data;
using Billing.DAL;

#endregion

namespace Billing.BLL {
    /// <summary>Factory classes are used to access all "public" methods found on the Data Access Lair.</summary>
    /// <remarks>
    /// The Factory class is also used to tie together data and functionality (DAL and BLL).
    /// Most of the methods that are defined in the factory class are used to encapsulate DAL functions. These
    /// methods very seldom add any new functionality or feature. When that happens the method description should
    /// mention this.
    /// <para>
    /// The Factory class can also be used to overload method names, even for methods that truly reside
    /// in other classes. It represents a solid front for the user and closes down any direct access to the DAL.
    /// </para>
    /// </remarks>
    public class BillingFactory {
        /// <summary>
        /// Instance of BillingDALC class. It is used to connect to all Billing data in the database.
        /// <seealso cref="Billing.DAL.BillingDALC"/>
        /// </summary>
        /// <remarks>Factory objects access the DAL through instanciated objects of DAL classes.</remarks>
        private static readonly BillingDALC _myBillingDALC = new BillingDALC();

        #region Currency

        /// <summary>
        /// BillingDALC - Gets all rows in billing_Currency. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetCurrencyListAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		DataTable myTable = new DataTable();
        ///		myTable = myBillingFactory.GetCurrencyListAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetCurrencyListAsDataTable"/>
        /// </remarks>
        /// <returns>Datatable - Currency List</returns>
        public static DataTable GetCurrencyListAsDataTable() { return _myBillingDALC.GetCurrencyListAsDataTable(); }

        /// <summary>
        /// BillingDALC - Uses CurrencyBLLC to isolate a currency instance. Inserts a single curreny instance in billing_Currency.
        /// <see cref="Billing.DAL.BillingDALC.InsertCurrency"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		// myCurrency has some values for currency
        ///		myBoolValue = myBillingFactory.InsertCurrency(myCurrency, UserID);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.InsertCurrency"/>
        /// </remarks>
        /// <param name="myCurrency">Currency instance</param>
        /// <returns>true if ok, else false.</returns>
        public static bool InsertCurrency(CurrencyBLLC myCurrency, int UserID) {
            return _myBillingDALC.InsertCurrency(
                myCurrency.Code, myCurrency.DescriptionNative, myCurrency.DescriptionEN, UserID);
        }

        /// <summary>
        /// BillingDALC - Uses CurrencyBLLC to isolate a currency instance. Updates a single currency instance in billing_Currency.
        /// <see cref="Billing.DAL.BillingDALC.UpdateCurrency"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		// myCurrency has some values for currency
        ///		myBoolValue = myBillingFactory.UpdateCurrency(myCurrency, UserId);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.UpdateCurrency"/>
        /// </remarks>
        /// <param name="myCurrency">Currency instance</param>
        /// <returns>true if ok, else false.</returns>
        public static bool UpdateCurrency(CurrencyBLLC myCurrency, int UserId) {
            return _myBillingDALC.UpdateCurrency(
                myCurrency.Code, myCurrency.DescriptionNative, myCurrency.DescriptionEN, UserId);
        }

        /// <summary>
        /// BillingDALC - Delete one row from billing_Currency. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.DeleteCurrency"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		// myCurrency has some values for currency
        ///		myBoolValue = myBillingFactory.DeleteCurrency(myCurrency);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.DeleteCurrency"/>
        /// </remarks>
        /// <param name="CurrencyCode">Key value for billing_Currency table. Used to find the correct row to delete</param>
        /// <returns>True if ok, false otherwise.</returns>
        public static bool DeleteCurrency(string CurrencyCode) { return _myBillingDALC.DeleteCurrency(CurrencyCode); }

        #endregion

        #region Indexes

        /// <summary>
        /// BillingDALC - Returns all rows from billing_IndexDetails. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetIndexDetailsAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		myTable = myBillingFactory.GetIndexDetailsAsDataTable(IndexName);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetIndexDetailsAsDataTable"/>
        /// </remarks>
        /// <returns>Index details as DataTable</returns>
        public static DataTable GetIndexDetailsAsDataTable(string IndexName) { return _myBillingDALC.GetIndexDetailsAsDataTable(IndexName); }

        /// <summary>
        /// BillingDALC - Returns all rows where IsActive = true from billing_Indexes. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetActiveIndexesAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		myTable = myBillingFactory.GetActiveIndexesAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetActiveIndexesAsDataTable"/>
        /// </remarks>
        /// <returns>Active Indexes as DataTable</returns>
        public static DataTable GetActiveIndexesAsDataTable() { return _myBillingDALC.GetActiveIndexesAsDataTable(); }

        /// <summary>
        /// BillingDALC - Returns all rows from billing_Indexes. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetAllIndexesAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		myTable = myBillingFactory.GetAllIndexesAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetAllIndexesAsDataTable"/>
        /// </remarks>
        /// <returns>All indexes as DataTable</returns>
        public static DataTable GetAllIndexesAsDataTable() { return _myBillingDALC.GetAllIndexesAsDataTable(); }

        /// <summary>
        /// BillingDALC - No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetCompleteIndexListAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		myTable = myBillingFactory.GetCompleteIndexListAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetCompleteIndexListAsDataTable"/>
        /// </remarks>
        /// <returns>Indexes as DataTable</returns>
        public static DataTable GetCompleteIndexListAsDataTable() { return _myBillingDALC.GetCompleteIndexListAsDataTable(); }

        /// <summary>
        /// BillingDALC - Adds one row to billing_IndexDetails. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.AddNewIndex"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		bool DidItWork;
        ///		IndexBLLC theIndex = new IndexBLLX();
        ///		UserId = Convert.ToInt32(Session["TheID"]);
        ///		// ...
        ///		// Set index values...
        ///		// ...
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		DidItWork = myBillingFactory.AddNewIndex(theIndex, UserId);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.AddNewIndex"/>
        /// </remarks>
        /// <returns>true if ok, false otherwise</returns>
        public static bool AddNewIndex(IndexBLLC theIndex, int UserID) { return _myBillingDALC.AddNewIndex(theIndex, UserID); }

        /// <summary>
        /// BillingDALC - Adds one row to billing_Indexes. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.AddNewIndex"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		bool DidItWork;
        ///		IndexOverviewBLLC theIndex = new IndexBLLX();
        ///		UserId = Convert.ToInt32(Session["TheID"]);
        ///		// ...
        ///		// Set index values...
        ///		// ...
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		DidItWork = myBillingFactory.AddNewIndex(theIndex, UserId);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.AddNewIndex"/>
        /// </remarks>
        /// <returns>true if ok, false otherwise</returns>
        public static bool AddNewIndex(IndexOverviewBLLC theIndex, int UserID) { return _myBillingDALC.AddNewIndex(theIndex, UserID); }

        /// <summary>
        /// BillingDALC - Delete one row from billing_IndexDetails. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.DeleteIndex"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		bool DidItWork;
        ///		string someIndexName;
        ///		// ...
        ///		// Set someIndexName to some Index;
        ///		// ...
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		DidItWork = myBillingFactory.DeleteIndex(someIndexName);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.DeleteIndex"/>
        /// </remarks>
        /// <returns>true if ok, false otherwise</returns>
        public static bool DeleteFromIndexDetails(string IndexName, DateTime IndexDate) { return _myBillingDALC.DeleteFromIndexDetails(IndexName, IndexDate); }

        /// <summary>
        /// BillingDALC - Delete one row from billing_Indexes. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.DeleteFromIndexes"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		bool DidItWork;
        ///		string someIndexName;
        ///		// ...
        ///		// Set someIndexName to some Index;
        ///		// ...
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		DidItWork = myBillingFactory.DeleteFromIndexes(someIndexName);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.DeleteFromIndexes"/>
        /// </remarks>
        /// <returns>True if ok, false otherwise.</returns>
        public static bool DeleteFromIndexes(string IndexName) { return _myBillingDALC.DeleteFromIndexes(IndexName); }

        /// <summary>
        /// BillingDALC - Update one row in billing_IndexDetails talbe. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.UpdateIndex"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		bool DidItWork;
        ///		IndexBLLC someIndexName = new IndexBLLC;
        ///		UserId = Convert.ToInt32(Session["TheID"]);
        ///		// ...
        ///		// Set someIndexName to some Index;
        ///		// ...
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		DidItWork = myBillingFactory.UpdateIndex(someIndexName, UserID);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.UpdateIndex"/>
        /// </remarks>
        /// <returns>true if ok, false otherwise</returns>
        public static bool UpdateIndex(IndexBLLC theIndex, int UserId, DateTime OriginalIndexDate) { return _myBillingDALC.UpdateIndex(theIndex, UserId, OriginalIndexDate); }

        /// <summary>
        /// BillingDALC - Update one row in billing_Indexes table. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.UpdateIndexOverView"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		bool DidItWork;
        ///		IndexBLLC someIndexName = new IndexBLLC;
        ///		UserId = Convert.ToInt32(Session["TheID"]);
        ///		// ...
        ///		// Set someIndexName to some Index;
        ///		// ...
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		DidItWork = myBillingFactory.UpdateIndexOverView(someIndexName, UserID);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.UpdateIndexOverView"/>
        /// </remarks>
        /// <returns>true if ok, false otherwise</returns>
        public static bool UpdateIndexOverView(IndexOverviewBLLC theIndex, int UserId) { return _myBillingDALC.UpdateIndexOverView(theIndex, UserId); }

        /// <summary>
        /// BillingDALC - Gets indexnames where IsActive = true from billing_Indexes. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetActiveIndexesAsArrayList"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		ArrayList myArray = myBillingFactory.GetActiveIndexesAsArrayList();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetActiveIndexesAsArrayList"/>
        /// </remarks>
        /// <returns>ArrayList - List of strings that represent active index names.</returns>
        public static ArrayList GetActiveIndexesAsArrayList() { return _myBillingDALC.GetActiveIndexesAsArrayList(); }

        #endregion

        #region Item Registry

        /// <summary>
        /// BillingDALC - Get Products where IsOpen = true from au_Products. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetActiveProductsAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		DataTable myTable = myBillingFactory.GetActiveProductsAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetActiveProductsAsDataTable"/>
        /// </remarks>
        /// <returns>Active products as DataTable</returns>
        public static DataTable GetActiveProductsAsDataTable() { return _myBillingDALC.GetActiveProductsAsDataTable(); }

        /// <summary>
        /// BillingDALC - Get UsageType ID from np_UsageTypes. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetUsageTypesAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		DataTable myTable = myBillingFactory.GetUsageTypesAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetUsageTypesAsDataTable"/>
        /// </remarks>
        /// <returns>Usage types as DataTable</returns>
        public static DataTable GetUsageTypesAsDataTable() { return _myBillingDALC.GetUsageTypesAsDataTable(); }

        /// <summary>
        /// BillingDALC - Get Item Counter Links from billing_ItemCounterLink table. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetCounterLinksAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		DataTable myTable = BillingFactory.GetCounterLinksAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetCounterLinksAsDataTable"/>
        /// </remarks>
        /// <returns>counter links as DataTable</returns>
        public static DataTable GetCounterLinksAsDataTable() { return _myBillingDALC.GetCounterLinksAsDataTable(); }

        /// <summary>
        /// BillingDALC - Get 3 letter code from billing_Currency. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetCurrencyCodesAsArrayList"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		ArrayList myList = myBillingFactory.GetCurrencyCodesAsArrayList();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetCurrencyCodesAsArrayList"/>
        /// </remarks>
        /// <returns>ArrayList - List of strings that represent currency codes.</returns>
        public static ArrayList GetCurrencyCodesAsArrayList() { return _myBillingDALC.GetCurrencyCodesAsArrayList(); }

        /// <summary>
        /// BillingDALC - Add row to billing_ItemRegistry. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.AddRegistryItem"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		bool isOk = myBillingFactory.AddRegistryItem(theItem);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.AddRegistryItem"/>
        /// </remarks>
        /// <returns>true if ok, false otherwise</returns>
        public static bool AddRegistryItem(BillingItemBLLC theItem) { return _myBillingDALC.AddRegistryItem(theItem); }

        /// <summary>
        /// BillingDALC - Get all rows from billing_ItemRegistry. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetAllRegistryItemsAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		DataTable myTable = BillingFactory.GetAllRegistryItemsAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetAllRegistryItemsAsDataTable"/>
        /// </remarks>
        /// <returns>Items as DataTable</returns>
        public static DataTable GetAllRegistryItemsAsDataTable() { return _myBillingDALC.GetAllRegistryItemsAsDataTable(); }

        /// <summary>
        /// BillingDALC - Get all rows from billing_ItemRegistry where IsActive = 'True'. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetAllActiveRegistryItemsAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		DataTable myTable = BillingFactory.GetAllActiveRegistryItemsAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetAllActiveRegistryItemsAsDataTable"/>
        /// </remarks>
        /// <returns>Items as DataTable</returns>
        public static DataTable GetAllActiveRegistryItemsAsDataTable() { return _myBillingDALC.GetAllActiveRegistryItemsAsDataTable(); }

        /// <summary>
        /// BillingDALC - Get all rows from billing_ItemRegistry where IsActive = 'True' for a specific currency. +
        /// No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetAllActiveRegistryItemsAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		DataTable myTable = BillingFactory.GetAllActiveRegistryItemsAsDataTable(theCurrency);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetAllActiveRegistryItemsAsDataTable"/>
        /// </remarks>
        /// <returns>Items as DataTable</returns>
        public static DataTable GetAllActiveRegistryItemsAsDataTable(string theCurrency) { return _myBillingDALC.GetAllActiveRegistryItemsAsDataTable(theCurrency); }

        /// <summary>
        /// BillingDALC - Delete from billing_ItemIndex. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.DeleteFromRegistryItems"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		bool DidItWork = myBillingFactory.DeleteFromRegistryItems(ItemID);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.DeleteFromRegistryItems"/>
        /// </remarks>
        /// <returns>True if ok, false otherwise.</returns>
        public static bool DeleteFromRegistryItems(int ItemID) { return _myBillingDALC.DeleteFromRegistryItems(ItemID); }

        /// <summary>
        /// BillingDALC - Get one item with from billing_ItemIndex. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetRegistryItem"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		BillingItemBLLC theItem = myBillingFactory.GetRegistryItem(ItemID);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetRegistryItem"/>
        /// </remarks>
        /// <returns>Instance of an Item (BillingItemBLLC)</returns>
        public static BillingItemBLLC GetRegistryItem(int ItemID) { return _myBillingDALC.GetRegistryItem(ItemID); }

        /// <summary>
        /// BillingDALC - Update one item in billing_ItemIndex. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.UpdateRegistryItem"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		BillingItemBLLC theItem = myBillingFactory.UpdateRegistryItem(TheItem);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.UpdateRegistryItem"/>
        /// </remarks>
        /// <returns>true if ok, false otherwise</returns>
        public static bool UpdateRegistryItem(BillingItemBLLC theItem) { return _myBillingDALC.UpdateRegistryItem(theItem); }

        #endregion

        #region Agreement items

        /// <summary>
        /// BillingDALC - Add row to billing_IndexesOnAgreementItems. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.AddIndexForAgreementItem"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		bool isOk = myBillingFactory.AddIndexForAgreementItem(IndexName, theItemID, InsertedDate, InsertedByUserID);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.AddIndexForAgreementItem"/>
        /// </remarks>
        /// <returns>true if ok, false otherwise</returns>
        public static bool AddIndexForAgreementItem(string IndexName, int ItemID, DateTime Inserted, int InsertedBy) { return _myBillingDALC.AddIndexForAgreementItem(IndexName, ItemID, Inserted, InsertedBy); }

        /// <summary>
        /// BillingDALC - Delete from billing_IndexesOnAgreementItems for specific item. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.DeleteFromIndexesOnAgreementItems"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		bool DidItWork = myBillingFactory.DeleteFromIndexesOnAgreementItems(ItemID);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.DeleteFromIndexesOnAgreementItems"/>
        /// </remarks>
        /// <returns>True if ok, false otherwise.</returns>
        public static bool DeleteFromIndexesOnAgreementItems(int ItemID) { return _myBillingDALC.DeleteFromIndexesOnAgreementItems(ItemID); }

        /// <summary>
        /// BillingDALC - Get one indexes for an item from billing_IndexesOnAgreementItems. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetIndexesOnAgreementItems"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.BillingFactory myBillingFactory = new BLL.billingFactory();
        ///		Arraylist theItemIndexes = myBillingFactory.GetIndexesOnAgreementItems(ItemID);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetIndexesOnAgreementItems"/>
        /// </remarks>
        /// <returns>ArrayList - List of strings that represent active index names.</returns>
        public static ArrayList GetIndexesOnAgreementItems(int ItemID) { return _myBillingDALC.GetIndexesOnAgreementItems(ItemID); }

        /// <summary>
        /// BillingDALC - Adds a new agreement to the database. Adds all items, comments and item indexes
        /// as well. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.AddNewAgreement"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		AgreementBLLC theAgreement = new AgreementBLLC();
        ///		//
        ///		// Insert some agreement values...
        ///		//
        ///		Arraylist theItemIndexes = BillingFactory.AddNewAgreement(theAgreement);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.AddNewAgreement"/>
        /// </remarks>
        /// <returns>true if OK, false otherwise</returns>
        public static bool AddNewAgreement(AgreementBLLC theAgreement) { return _myBillingDALC.AddNewAgreement(theAgreement); }

        /// <summary>
        /// BillingDALC - Updates a specific. Deletes and re-adds all items, comments and item indexes
        /// as well. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.UpdateAgreement"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		AgreementBLLC theAgreement = new AgreementBLLC();
        ///		//
        ///		// Insert some agreement values...
        ///		//
        ///		Arraylist theItemIndexes = BillingFactory.UpdateAgreement(theAgreement);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.UpdateAgreement"/>
        /// </remarks>
        /// <returns>true if OK, false otherwise</returns>
        public static bool UpdateAgreement(AgreementBLLC theAgreement) { return _myBillingDALC.UpdateAgreement(theAgreement); }

        /// <summary>
        /// Gets Agreements ID and deletes matching Agreement from database
        /// </summary>
        /// <param name="agreementID">the Agreement to delete</param>
        /// <returns>true if OK, false otherwise</returns>
        public static bool DeleteAgreement(AgreementBLLC theAgreement) { return _myBillingDALC.DeleteAgreement(theAgreement); }

        /// <summary>
        /// BillingDALC - Gets a specific agreement. Gets all items, comments and item indexes
        /// as well. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetAgreement"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		AgreementBLLC theAgreement = BillingFactory.GetAgreement(SomeAgreementID);
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetAgreement"/>
        /// </remarks>
        /// <returns>Instance of Agreement</returns>
        public static AgreementBLLC GetAgreement(int AgreementID) { return _myBillingDALC.GetAgreement(AgreementID); }

        /// <summary>
        /// BillingDALC - Deletes a specific agreement item and it's indexes
        /// <see cref="Billing.DAL.BillingDALC.DeleteAgreementItem"/>
        /// </summary>
        /// <returns>true if ok, false otherwise</returns>
        public static bool DeleteAgreementItem(AgreementItemBLLC item) { return _myBillingDALC.DeleteAgreementItem(item); }

        #endregion

        #region Users

        /// <summary>
        /// Fetches a complete list a Employees.
        /// </summary>
        /// <see cref="Billing.DAL.BillingDALC.GetUsers"/>
        /// <returns>Employees as DataSet</returns>
        public static DataSet GetEmployees() { return _myBillingDALC.GetEmployees(); }

        #endregion

        #region BillingErrorPage

        public static DataSet GetUsageWithoutProduct(DateTime dtFrom, DateTime dtTo) { return _myBillingDALC.GetUsageWithoutProduct(dtFrom, dtTo); }
        public static DataSet GetUsageWithoutAgreement(DateTime dtFrom, DateTime dtTo) { return _myBillingDALC.GetUsageWithoutAgreement(dtFrom, dtTo); }
        public static DataSet GetUsageWithoutUsers(DateTime dtFrom, DateTime dtTo) { return _myBillingDALC.GetUsageWithoutUsers(dtFrom, dtTo); }
        public static DataSet GetUsageWithMultipleBillingItems(DateTime dtFrom) { return _myBillingDALC.GetUsageWithMultipleBillingItems(dtFrom); }

        #endregion

        /// <summary>
        /// Default Constructor logic - Instanciate DAL and BLL classes for Factory access.
        /// </summary>
        /// <remarks>
        /// BillingFactory objects create the following DAL and BLL ojects:
        /// <list type="bullet">
        /// <item>_myBillingDALC
        /// <description>Instance of BillingDALC</description>
        /// </item>
        /// </list>
        /// </remarks>
//		public BillingFactory()
//		{
//			_myBillingDALC = new BillingDALC();
//		}
        /// <summary>
        /// BillingDALC - Returns open subscribers with detail info. No added functionality.
        /// <see cref="Billing.DAL.BillingDALC.GetOpenSubscribersAsDataTable"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		DataTable SubscriberTable = myBillingFactory.GetOpenSubscribersAsDataTable();
        /// </code>
        /// <seealso cref="Billing.DAL.BillingDALC.GetOpenSubscribersAsDataTable"/>
        /// </remarks>
        /// <returns>Open subscribers as DataTable</returns>
        public static DataTable GetOpenSubscribersAsDataTable() { return _myBillingDALC.GetOpenSubscribersAsDataTable(); }

        public static DataTable GetSingleSubscribersAsDataTable(int SubscriberID) { return _myBillingDALC.GetSingleSubscribersAsDataTable(SubscriberID); }

        public static DataSet SearchSubscribersAndAgreements(
            string CreditInfoID,
            string Name,
            string Address,
            string RegisteredBy,
            string AgreementID,
            string OpenClosed,
            string IdNumber,
            int nSignerCIID /*string SignerName*/,
            string IsAgreementOnly) {
            return _myBillingDALC.SearchSubscribersAndAgreements(
                CreditInfoID,
                Name,
                Address,
                RegisteredBy,
                AgreementID,
                OpenClosed,
                IdNumber,
                nSignerCIID,
                IsAgreementOnly);
        }

        public static string MakeBillingExcelFile(
            string AppPath, int SubscriberID, DateTime Period_from, DateTime Period_to, int DetailLevel, int IsBillable) {
            return _myBillingDALC.MakeBillingExcelFile(
                AppPath, SubscriberID, Period_from, Period_to, DetailLevel, IsBillable);
        }
    }
}