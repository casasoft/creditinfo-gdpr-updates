namespace Billing.BLL {
    /// <summary>
    /// Holds one Currency instance
    /// </summary>
    /// <remarks>Currency objects are composed of 3 letter codes and a description.</remarks>
    public class CurrencyBLLC {
        /// <summary>
        /// Desfault contructor
        /// </summary>
        /// <remarks></remarks>
        public CurrencyBLLC() {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// Initalizes the code and both description values according to variables.
        /// </summary>
        /// <remarks>Code can only be three letters long. Description (both english and native) can hold 50 chars.</remarks>
        public CurrencyBLLC(string CurrencyCode, string DescEN, string DescNative) {
            Code = CurrencyCode;
            DescriptionEN = DescEN;
            DescriptionNative = DescNative;
        }

        /// <summary>
        /// Gets/Sets the currency code
        /// </summary>
        /// <remarks>Impliments a simple Get / Set structure.</remarks>
        public string Code { get; set; }

        /// <summary>
        /// Gets/Sets the currency description (english)
        /// </summary>
        /// <remarks>Impliments a simple Get / Set structure.</remarks>
        public string DescriptionEN { get; set; }

        /// <summary>
        /// Gets/Sets the currency description (native)
        /// </summary>
        /// <remarks>Impliments a simple Get / Set structure.</remarks>
        public string DescriptionNative { get; set; }
    }
}