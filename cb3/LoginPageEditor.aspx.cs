﻿using System;
using System.IO;
using System.Web;

namespace cb3
{
    public partial class LoginPageEditor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadLeftBox(this, EventArgs.Empty);
                LoadRightBox(this, EventArgs.Empty);          
            }
        }

        protected void LoadLeftBox(object sender, EventArgs e)
        {
            var leftText = File.ReadAllText(Login.LeftBoxPath);
            leftBoxEditor.Content = HttpUtility.HtmlDecode(leftText);
        }

        protected void LoadRightBox(object sender, EventArgs e)
        {
            var rightText = File.ReadAllText(Login.RightBoxPath);
            rightBoxEditor.Content = HttpUtility.HtmlDecode(rightText);
        }

        protected void submitLeftText_Click(object sender, EventArgs e)
        {            
            File.WriteAllText(Login.LeftBoxPath, leftBoxEditor.Content);
        }

        protected void submitRightText_Click(object sender, EventArgs e)
        {
            File.WriteAllText(Login.RightBoxPath, rightBoxEditor.Content);
        }
    }
}