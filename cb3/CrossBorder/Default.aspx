﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="cb3.CrossBorder._default" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="../new_user_controls/FoOptions.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
		<title>Creditinfo</title>
		<link href="../css/FoCIGStyles.css" type="text/css" rel="stylesheet" />
		
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
  </head>
	<body ms_positioning="GridLayout" style="BACKGROUND-IMAGE: url(../img/mainback.gif)" leftmargin="0"
		rightmargin="0">
		
	  
		   
		<form id="formDefault" method="post" runat="server">
	  <asp:ScriptManager ID="__ScriptManager1" runat="server" />
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
								<td>
									<table cellspacing="0" cellpadding="0" width="640" align="center" border="0" bgcolor="#951e16">
										<tr>
											<td bgcolor="#951e16"><span style="WIDTH: 3px"></span></td>
											<td bgcolor="#951e16"><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
											<td bgcolor="#951e16" align="right">
												<uc1:language id="Language1" runat="server"></uc1:language></td>
										</tr>
									</table>
								</td>
								<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="640" align="center" border="0" bgcolor="white">
							<tr>
								<td>
									<p>
										<table id="tbDefault" cellspacing="0" cellpadding="0" width="100%" align="center">
											<tr>
												<td>
													<table cellspacing="0" cellpadding="0" border="0" width="97%" align="center">
														<tr>
															<td>
																<uc1:userinfo id="UserInfo1" runat="server"></uc1:userinfo>
															</td>
															<td align="right">
																<uc1:fooptions id="FoOptions1" runat="server"></uc1:fooptions>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<table cellspacing="0" cellpadding="0" border="0" width="97%" align="center">
														<tr>
															<td class="pageheader" width="97%">
																<asp:label id="lblFoDefault" runat="server"></asp:label></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<asp:PlaceHolder runat="server" ID="CrossBorderPlaceHolder" />
												</td>
											</tr>
										</table>
									</p>
								</td>
							</tr>
							<tr>
								<td>&nbsp; &nbsp;<asp:hyperlink id="hlMail" runat="server" navigateurl="mailto:info@creditinfo.com.mt">e-mail:info@creditinfo.com.mt</asp:hyperlink>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="6" bgcolor="transparent"></td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		 
		</form>
	</body>
</html>
