﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Cig.Framework.Base.Configuration;
using System.Globalization;
using UserAdmin.BLL;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp;

namespace cb3.CrossBorder
{
    public partial class _default : System.Web.UI.Page
    {
        private ContentControl contentControl = new ContentControl();
        uaFactory myUserAdminFactory;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            this.InitControls();
            base.OnInit(e);
        }


        public void InitControls()
        {
            myUserAdminFactory = new uaFactory();
            var user = myUserAdminFactory.GetSpecificUser(Context.User.Identity.Name);
            contentControl.OnAccount = Context.User.Identity.Name;
            contentControl.Culture = CultureInfo.CurrentUICulture;
            contentControl.Email = user.Email;
            contentControl.WebServicePwd = CigConfig.Configure("CrossBorder.WebServicePwd");
            contentControl.WebServiceSubscriber = CigConfig.Configure("CrossBorder.WebServiceSubscriber");
            contentControl.WebServiceSubscriberUnit = CigConfig.Configure("CrossBorder.WebServiceSubscriberUnit");
            contentControl.WebServiceURL = CigConfig.Configure("CrossBorder.WebServiceURL");
            contentControl.WebServiceUser = CigConfig.Configure("CrossBorder.WebServiceUser");
            CrossBorderPlaceHolder.Controls.Add(contentControl);
        }
    }
}
