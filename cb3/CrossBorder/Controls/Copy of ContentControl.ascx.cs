﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Proxy;
using System.Globalization;
using System.Threading;
using System.Net.Mime;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Enums;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp
{   
  
    public partial class ContentControl : System.Web.UI.UserControl
    {
        #region Private fields
        private CultureInfo _culture;
        private string _onAccount;
        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        string _content;
        ExportType _exportType;
        bool _exportReport = false;
        #endregion

        #region Constructor
        public ContentControl()
        {

        }
        #endregion

        #region Properties
        public string OnAccount
        {
            get { return _onAccount; }
            set { _onAccount = value; }
        }

        public CultureInfo Culture
        {
            get { return _culture; }
            set { _culture = value; }
        }

    
       
        #endregion;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

     
        protected void Page_Load(object sender, EventArgs e)
        {
            AddStylesheet();
            this.SendRequest();
            if (!_exportReport)
            {
                this._content = this.TransformResponse();
                ParseControls(this._content);
            }
            else
            {
                string contentType = Helper.GetContentType(_exportType);
                Response.ContentType = contentType;
                Response.ContentEncoding = System.Text.Encoding.UTF8;
                Response.AddHeader("Content-Disposition",
                    string.Format("attachment; filename=report.{0}", _exportType.ToString().ToLower()));

                Response.BinaryWrite(Helper.GetExtraReportOutput(this._content));
                Response.Flush();
                Response.End();
            }
        }

        private string TransformResponse()
        {
            WebServiceProxy proxy = new WebServiceProxy(_onAccount, _culture, _email);
            String ret = proxy.TransformResponse(this._content);
            return ret.Replace("xmlns:asp=\"remove\"", String.Empty);
        }
     
        private void SendRequest()
        {
            WebServiceProxy proxy = new WebServiceProxy(_onAccount, _culture, _email);
            ParamCollection param = GetParamCollection();
            this._content = proxy.Send(param);
        }


        #region Support functions
        private void AddStylesheet()
        {
            string fullName = "{0}.css.{1}";
            fullName = string.Format(fullName, (typeof(ContentControl)).Assembly.GetName().Name, "stl.css");
            string cssUrl = Page.ClientScript.GetWebResourceUrl(typeof(ContentControl), fullName);
            HtmlLink cssLink = new HtmlLink();
            cssLink.Href = cssUrl;
            cssLink.Attributes.Add("rel", "stylesheet");
            cssLink.Attributes.Add("type", "text/css");
            this.Page.Header.Controls.Add(cssLink);
        }

        private void ParseControls(string content)
        {
            Control ctrl = this.Page.ParseControl(content);
            Control[] dest = new Control[ctrl.Controls.Count];
            ctrl.Controls.CopyTo(dest, 0);
            this.AddImageUrl(dest);
            foreach(Control c in dest)
                this.Controls.Add(c);
        }
        /*
        protected override void OnPreRender(EventArgs e)
        {
            string fullName = "{0}.script.{1}";
            fullName = string.Format(fullName, (typeof(ContentControl)).Assembly.GetName().Name, "hltable.js");
            string url = Page.ClientScript.GetWebResourceUrl(typeof(ContentControl), fullName);

            Page.ClientScript.RegisterClientScriptInclude("hltable",
                  url);

            base.OnPreRender(e);
        }
          */

        private void AddImageUrl(Control[] ctrlCollection)
        {
            foreach (Control c in ctrlCollection)
            {
                ImageButton button = c as ImageButton;
                string fullName = "{0}.img.{1}";
                string imgUrl = string.Empty;

                if (button != null)
                    imgUrl = button.ImageUrl;
                HtmlInputSubmit input = c as HtmlInputSubmit;
                if (input != null && input.ID == "CrossBorderSubmitButton1")
                    imgUrl = input.Attributes["ImageUrl"];

                fullName = string.Format(fullName, (typeof(ContentControl)).Assembly.GetName().Name, imgUrl);
                string webResourceURL = Page.ClientScript.GetWebResourceUrl(typeof(ContentControl), fullName);

                if (button != null)
                    button.ImageUrl = webResourceURL;

                if (input != null && input.ID == "CrossBorderSubmitButton1")
                    input.Style.Add("background-image", webResourceURL);

                Control[] dest = new Control[c.Controls.Count];
                c.Controls.CopyTo(dest, 0);
                this.AddImageUrl(dest);
            }
        }

      
        private ParamCollection GetParamCollection()
        {
            Hashtable other = new Hashtable();

            string requestId = string.Empty;
            ParamCollection paramCollection = new ParamCollection();
            foreach (string key in Page.Request.Form.Keys)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    
                    string value = Page.Request.Form.Get(key);
                    if (paramCollection.CheckPrefix(key))
                    {
                        if (key.IndexOf("OtherParams") != -1)
                        {
                            other.Add(key, value);
                            continue;
                        }

                        ParentItem param = paramCollection.Get(key);
                        param.Parse(key, value);
                        if (key.IndexOf("QueueRequest_CommandType_Queue") != -1)
                        {
                            requestId = param.Value;
                        }
                        this.IsExport(key, value);
                    }
                    else
                    {
                        if (paramCollection.CheckExcludedPrefix(key))
                        {
                            ParentItem param = new ParentItem(false);
                            param.Key = key;
                            param.Value = value;
                            paramCollection.Add(param.Key, param);
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(requestId))
            {
                if (other.Count > 0)
                {
                    foreach (object key in other.Keys)
                    {
                        if (key.ToString().IndexOf(requestId) != -1)
                        {
                            ParentItem param = paramCollection.Get(key.ToString());
                            param.Parse(key.ToString(), other[key].ToString(), requestId);
                            break;
                        }
                    }
                }
            }
            return paramCollection;
        }

        private bool IsExport(string key, string value)
        {
            if (!_exportReport)
            {
                _exportReport = (key.ToLower().IndexOf("exportreport_exportformat") != -1);
                if (_exportReport)
                    this._exportType = (ExportType)Enum.Parse(typeof(ExportType), value);
            }
            return _exportReport;
        }
        #endregion
    }
}
