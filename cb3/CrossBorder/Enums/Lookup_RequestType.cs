﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Enums
{
    public enum Lookup_RequestType
    {

        /// <summary>
        /// StrucLookupItem to 'QueueRequest'
        /// </summary>
        QueueRequest,
        /// <summary>
        /// StrucLookupItem to 'GetReportRequest'
        /// </summary>
        GetReportRequest,
        /// <summary>
        /// StrucLookupItem to 'OnDemandReportRequest'
        /// </summary>
        OnDemandReportRequest,
        /// <summary>
        /// StrucLookupItem to 'ExportReport'
        /// </summary>
        ExportReport,
        /// <summary>
        /// StrucLookupItem to 'Search'
        /// </summary>
        Search,
        /// <summary>
        /// StrucLookupItem to 'Queue'
        /// </summary>
        Queue,
        /// <summary>
        /// StrucLookupItem to 'Report'
        /// </summary>
        Report,
        /// <summary>
        /// StrucLookupItem to 'ResponseLcid'
        /// </summary>
        ResponseLcid,
        /// <summary>
        /// StrucLookupItem to 'Default'
        /// </summary>
        Default,
        /// <summary>
        /// StrucLookupItem to 'Refresh'
        /// </summary>
        Refresh


    }
}
