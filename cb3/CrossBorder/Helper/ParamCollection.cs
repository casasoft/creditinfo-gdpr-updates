﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp
{
    class ParamCollection : BaseItem<ParentItem>
    {

        public bool CheckPrefix(string key)
        {
            return (key.StartsWith(prefixParams) || key.IndexOf(prefixParam) != -1);
        }


        internal bool CheckExcludedPrefix(string key)
        {
            return (!key.StartsWith("__") && key.IndexOf("$") == -1);
        }

        private ParentItem SingleItem(string[] nameValue)
        {
            // Command example: __PARAM__Lookups.ResponseLcid
            ParentItem node = new ParentItem();
            if (TryGetValue(nameValue[0], out node))
                return node;

            string value = string.Empty;
            if (nameValue.Length != 1)
                value = nameValue[1];

            if (!string.IsNullOrEmpty(value))
                node.Value = value;
            else
                node.CommonPrefix = false;

            node.Key = nameValue[0];
            this.Add(node.Key, node);
            return node;
        }

        private ParentItem GetItem(string[] nameValue)
        {
            if (nameValue.Length > 2)
            {
                ParentItem item = null;
                for (int i = 0; i < nameValue.Length; i++)
                {
                    if (i % 2 == 0)
                    {

                        TryGetValue(nameValue[i], out item);
                        item.Key = nameValue[i];
                        this.Add(item.Key, item);
                    }
                    else
                    {
                        if (item != null)
                            item.Value = nameValue[i];

                    }
                }
                return item;
            }
            else
                return SingleItem(nameValue);
        }

        public new ParentItem Get(string inputParams)
        {
            if (inputParams.IndexOf(this.prefixParam) != -1)
            {
                string[] param = inputParams.Split('$');
                foreach (string p in param)
                {
                    string tmp = p;
                    if (tmp.IndexOf(this.prefixParam) != -1)
                    {
                        tmp = tmp.Replace(this.prefixParam, string.Empty);
                        // input type button. Format Ctrl01$Button_Name.x, Ctrl01$Button_Name.y
                        if (inputParams.IndexOf('$') != -1 && tmp.IndexOf('.') != -1)
                            if (tmp.EndsWith(".x") || tmp.EndsWith(".y"))
                                tmp = tmp.Substring(0, tmp.IndexOf('.'));
                      
                        string[] nameValue = tmp.Split('_');
                        return this.GetItem(nameValue);
                    }
                }
            }

            inputParams = inputParams.Replace(this.prefixParams, string.Empty);
            string[] s = inputParams.ToString().Split('.');

            string key = s[0];

            ParentItem root = new ParentItem();
            if (TryGetValue(key, out root))
                return root;

            root.Key = key;

            this.Add(FullName(key), root);
            return root;
        }
    }
}
