﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp
{
    class ParentItem : BaseItem<ChildItem>
    {
        public ParentItem()
        {
        }
        public ParentItem(bool commonPrefix) : base(commonPrefix) { }

        public void Parse(string inputParam, string value)
        {
            this.Parse(inputParam, value, string.Empty);
        }

        public void Parse(string inputParam, string value, string requestId)
        {
            if (inputParam.IndexOf(this.prefixParam) != -1)
            {
                if(!this.CommonPrefix)
                    this.Value = value; 
                return;
            }

            inputParam = inputParam.Replace(this.prefixParams, string.Empty);

            string[] s = inputParam.ToString().Split('.');
            string rootName = s[0];
            string nodeName = s[1];
            string key = s[2];

            ChildItem childItem = this.Get(rootName);

            childItem.Name = nodeName;
            ItemNode item = childItem.Get(key);
            if (!string.IsNullOrEmpty(requestId) && key.IndexOf("_") != -1)
            {
                string[] tmp = key.Split('_');
                key = tmp[0];
            }

            item.Key = key;
            if (inputParam.IndexOf(base.ValueTag) != -1)
                item.Value = value;

            if (inputParam.IndexOf(base.OperatorTag) != -1)
                item.Operator = value;
        }

        int _nodeKey = 0;

        public int NodeKey
        {
            get { return _nodeKey++; }
        }


        internal string GetXml()
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml("<root/>");
            XmlNode root = xDoc.CreateElement(this.Key);
            xDoc.DocumentElement.AppendChild(root);

            foreach (ChildItem childItem in this.Collection)
            {
                foreach (ItemNode item in childItem.Collection)
                {
                    //if (!string.IsNullOrEmpty(item.Value))
                    {
                        XmlNode node = xDoc.CreateElement(childItem.Name);


                        XmlAttribute keyAttribute = xDoc.CreateAttribute("key");
                        keyAttribute.InnerText = (this.NodeKey).ToString();
                        node.Attributes.Append(keyAttribute);
                        root.AppendChild(node);

                        XmlNode childNode = xDoc.CreateElement("Common.Key");
                        childNode.InnerText = item.Key;
                        node.AppendChild(childNode);

                        childNode = xDoc.CreateElement("Common.Value");
                        childNode.InnerText = item.Value;
                        node.AppendChild(childNode);

                        if (!string.IsNullOrEmpty(item.Operator))
                        {
                            childNode = xDoc.CreateElement("Common.Operator");
                            node.AppendChild(childNode);
                            childNode.InnerText = item.Operator;
                        }
                    }
                }

            }
            return xDoc.DocumentElement.InnerXml;
        }
    }
}
