﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp
{
    public class ItemNode
    {
        string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
        string _key;

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }
        string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        string _operator;
        public string Operator
        {
            get { return _operator; }
            set { _operator = value; }
        }
    }
}
