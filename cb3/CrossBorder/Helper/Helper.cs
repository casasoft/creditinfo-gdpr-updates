﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Xml.XPath;
using System.Text;
using Cis.CB4.Tools.ServiceClient;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Enums;
using System.Globalization;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp
{
    public class Helper
    {

        public static Lookup_Languages CultureToLanguageLookup(CultureInfo culture)
        {
            switch (culture.LCID)
            {
                case 1029: return Lookup_Languages.Czech;
                case 1032: return Lookup_Languages.Greek;
                case 6153: return Lookup_Languages.English;
                case 1063: return Lookup_Languages.Lithuanian;
                case 2072: return Lookup_Languages.Romanian;
                case 2073: return Lookup_Languages.Russian;
                case 1043: return Lookup_Languages.Dutch;
                case 2057: return Lookup_Languages.English;
                case 1033: return Lookup_Languages.English;
                default: return Lookup_Languages.English;
            }
        }

        private const string BatchNS = "http://cb4.creditinfosolutions.com/BatchUploader/Batch";

        public static byte [] GetExtraReportOutput(string response)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(response);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(new NameTable());
            nsmgr.AddNamespace("ns", BatchNS);

            XmlNode node = xDoc.SelectSingleNode("//ns:Common.ExtraReportOutput", nsmgr);
            if (node != null)
                return Convert.FromBase64String(node.InnerText);

            throw new Exception("");
        }


        public static string Transform(string content, string xslt)
        {
        
            xslt = HttpUtility.HtmlDecode(xslt);
            XmlParserContext context = new XmlParserContext(null, null, null, XmlSpace.None);
            XmlTextReader xmltr = new XmlTextReader(xslt, XmlNodeType.Document, context);
            XslCompiledTransform xslTrans = new XslCompiledTransform();
            xslTrans.Load(xmltr);

            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(content));

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            ms.Position = 0;
            
            using (XmlReader reader = XmlReader.Create(ms, settings))
            {
                reader.Read();
                reader.MoveToContent();
                XPathDocument xdoc = new XPathDocument(reader);
              
                using (ms = new MemoryStream())
                {
                    XmlWriterSettings writerSettings = new XmlWriterSettings();
                    writerSettings.ConformanceLevel = ConformanceLevel.Auto;
              
                    using (XmlWriter writer = XmlWriter.Create(ms, writerSettings))
                        xslTrans.Transform(xdoc, new XsltArgumentList(), writer);

                    return Encoding.UTF8.GetString(ms.ToArray());
                }
            }
        }

        internal static string GetContentType(Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Enums.ExportType exportType)
        {
            switch (exportType)
            {
                case ExportType.XML:
                    return "text/xml";
                case ExportType.PDF:
                    return "application/pdf";
                case ExportType.XLS:
                    return "application/vnd.ms-excel";
                case ExportType.DOC:
                    return "application/msword";
            }
            return string.Empty;
        }
    }
}
