﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp
{
    public class BaseItem<T> where T : new()
    {
        bool _commonPrefix = true;

        public bool CommonPrefix
        {
            get { return _commonPrefix; }
            set { _commonPrefix = value; }
        }

        public readonly string prefixParams = "__PARAMS__";
        public readonly string prefixParam = "__PARAM__";
        public readonly string ValueTag = "Value";
        public readonly string OperatorTag = "Operator";
        public readonly string prefixCommon = "Common";

        string _key;
        string _value;
        string _name;

        public BaseItem() { }
        public BaseItem(bool commonPrefix)
        {
            _commonPrefix = commonPrefix;
        }
        public string Name
        {
            get { return _name; }
            set { _name = FullName(value); }
        }

        public string Key
        {
            get { return _key; }
            set { _key = FullName(value); }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }


        public string FullName(string key)
        {
            if (!_commonPrefix)
                return key;

            return string.Join(".", new string[] { prefixCommon, key });
        }

        SortedList<string, T> _collection = new SortedList<string, T>();

        public bool TryGetValue(string key, out T t)
        {

            if (_collection.ContainsKey(FullName(key)))
            {
                t = _collection[FullName(key)];
                return true;
            }
            t = new T();
            return false;

        }

        public T Get(string key)
        {
            T t;
            if (!TryGetValue(key, out t))
                _collection.Add(FullName(key), t);
            return t;
        }

        public void Add(string key, T t)
        {
            if (!_collection.ContainsKey(key))
                _collection.Add(key, t);
        }

        public IList<T> Collection
        {
            get
            {
                return _collection.Values;

            }
        }
    }

}
