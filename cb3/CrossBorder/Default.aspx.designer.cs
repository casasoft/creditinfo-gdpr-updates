﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5448
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cb3.CrossBorder {
    
    
    public partial class _default {
        
        /// <summary>
        /// formDefault control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm formDefault;
        
        /// <summary>
        /// __ScriptManager1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.ScriptManager @__ScriptManager1;
        
        /// <summary>
        /// Head2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CreditInfoGroup.new_user_controls.FoHead Head2;
        
        /// <summary>
        /// PanelBar1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CreditInfoGroup.new_user_controls.FoPanelBar PanelBar1;
        
        /// <summary>
        /// Language1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CreditInfoGroup.new_user_controls.language Language1;
        
        /// <summary>
        /// UserInfo1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CreditInfoGroup.new_user_controls.UserInfo UserInfo1;
        
        /// <summary>
        /// FoOptions1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CreditInfoGroup.new_user_controls.FoOptions FoOptions1;
        
        /// <summary>
        /// lblFoDefault control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblFoDefault;
        
        /// <summary>
        /// CrossBorderPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder CrossBorderPlaceHolder;
        
        /// <summary>
        /// hlMail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink hlMail;
        
        /// <summary>
        /// Footer1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CreditInfoGroup.new_user_controls.FoFooter Footer1;
    }
}
