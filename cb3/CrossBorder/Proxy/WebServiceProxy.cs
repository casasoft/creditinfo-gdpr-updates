﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Cis.CB4.Tools.ServiceClient;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Enums;
using System.Globalization;


namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Proxy
{


    class WebServiceProxy
    {
        public string WebServiceURL { get; set; }
        public string WebServiceUser { get; set; }
        public string WebServicePwd { get; set; }
        public string WebServiceSubscriber { get; set; }
        public string WebServiceSubscriberUnit { get; set; }


        XmlNamespaceManager nsmgr;

        const string ns = "http://cb4.creditinfosolutions.com/BatchUploader/Batch";
        SimpleXmlQuery query;

        string _onAccount;
        CultureInfo _culture;
        string _email;

        public WebServiceProxy(string webServiceURL, string webServiceUser, string webServicePwd, string webServiceSubscriber,
            string webServiceSubscriberUnit, string onAccount, CultureInfo culture, string email)
        {

            if (string.IsNullOrEmpty(webServiceURL) && string.IsNullOrEmpty(webServiceUser) && string.IsNullOrEmpty(webServicePwd))
            {
                AppSettingsReader configReader = new AppSettingsReader();
                this.WebServiceURL = configReader.GetValue("webServiceURL", typeof(string)).ToString();
                this.WebServiceUser = configReader.GetValue("WebServiceUser", typeof(string)).ToString();
                this.WebServicePwd = configReader.GetValue("WebServicePwd", typeof(string)).ToString();
                this.WebServiceSubscriber = configReader.GetValue("Subscriber", typeof(string)).ToString();
                this.WebServiceSubscriberUnit = configReader.GetValue("SubscriberUnit", typeof(string)).ToString();
            }
            else
            {
                this.WebServiceURL = webServiceURL;
                this.WebServiceUser = webServiceUser;
                this.WebServicePwd = webServicePwd;
                this.WebServiceSubscriber = webServiceSubscriber;
                this.WebServiceSubscriberUnit = webServiceSubscriberUnit;
            }
            nsmgr = new XmlNamespaceManager(new NameTable());
            nsmgr.AddNamespace("ns", ns);

            query = new SimpleXmlQuery(WebServiceURL, WebServiceUser, WebServicePwd, int.MaxValue);

            _onAccount = onAccount;
            _culture = culture;
            _email = email;
        }

        private XmlDocument Send(XmlDocument command)
        {
            XmlDocument response = query.Execute(command);
            return response;
        }

        
        public string GetTransformation()
        {
            XmlDocument command = BatchCommandHelper.ConstructBatch(BatchCommandType.Transform, this.WebServiceSubscriber,
                this.WebServiceSubscriberUnit, _onAccount, _culture, _email);
            XmlDocument response = Send(command);
            
            XmlNode node = response.SelectSingleNode("//ns:Response.TransformOutput", nsmgr);
            if (node == null)
                throw new Exception();
            return node.InnerText; 
        }

       
        internal string Send(ParamCollection param)
        {
            XmlDocument command = BatchCommandHelper.ConstructBatch(
                        param, this.WebServiceSubscriber, this.WebServiceSubscriberUnit, _onAccount, _culture, _email);
            XmlDocument response = this.Send(command);

            return response.InnerXml; 
        }

        internal string TransformResponse(string response)
        {
            string xslt = this.GetTransformation();
            return Helper.Transform(response, xslt);
        }
    }
}
