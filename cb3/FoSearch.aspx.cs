using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using CPI.BLL;
using CR.BLL;
using CR.Localization;
using CR.MTBLL;
using CreditInfoGroup.new_user_controls;
using CreditWatch.BLL;
using CreditWatch.BLL.WatchUniqueID;
using ROS.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.auUsers;
using UserAdmin.BLL.CIUsers;
using Logger=Logging.BLL.Logger;

using Cig.Framework.Base.Configuration;
using System.Collections.Generic;

#pragma warning disable 618,612

namespace CreditInfoGroup
{
    /// <summary>
    /// Summary description for FoSearch.
    /// </summary>
    public class FoSearch : Page
    {
        private readonly CPIFactory cpiFactory = new CPIFactory();
        private readonly cwFactory creditWatchFactory = new cwFactory();
        private readonly CRFactory crFactory = new CRFactory();
        private readonly uaFactory userAdminFactory = new uaFactory();
        protected Button btnClear;
        protected Button btnFind;
        protected Button btnOrderReport;
        private CultureInfo ci;
        private string culture;
        protected DropDownList ddlCity;
        protected DropDownList ddlFS;
        protected DataGrid dtgrCompanys;
        protected DataGrid dtgrIndividual;
        private bool isCompany = true;
        protected Label lblAdditionalInformations;
        protected Label lblAddressLabel;
        protected Label lblCity;
        protected Label lblCityLabel;
        protected Label lblCompanyGridHeader;
        protected Label lblCountryLabel;
        protected Label lblPostalCodeLabel;
        protected Label lblCreditinfoIDLabel;
        protected Label lblDateOfReport;
        protected Label lblDateOfReportTitle;
        protected Label lblDisclaimer1;
        protected Label lblDisclaimer2;
        protected Label lblIndividualGridHeader;
        protected Label lblIsCompany;
        protected Label lblLastArchived;
        protected Label lblLastUpdate;
        protected Label lblMessage;
        protected Label lblName;
        protected Label lblNameLabel;
        protected Label lblNationalID;
        protected Label lblNationalIDLabel;
        protected Label lblNote;
        protected Label lblOrderDeliveryTypeLabel;
        protected Label lblOrderDispCompanyName;
        protected Label lblOrderDispCompanyNationalID;
        protected Label lblOrderDispSubscriber;
        protected Label lblOrderDispUserName;
        protected Label lblOrderNameLabel;
        protected Label lblOrderNationalIDLabel;
        protected Label lblOrderProduct;
        protected Label lblOrderProductID;
        protected Label lblOrderProductLabel;
        protected Label lblOrderReceived;
        protected Label lblOrderSubscriberLabel;
        protected Label lblOrderSummary;
        protected Label lblOrderUserNameLabel;
        protected Label lblRecName;
        protected Label lblRecNameLabel;
        protected Label lblRecNationalID;
        protected Label lblRecNationalIDLabel;
        protected Label lblRegDateTitle;
        protected Label lblReportOrder;
        protected Label lblReportRefID;
        protected Label lblReportRefIDTitle;
        protected LinkButton lblResultAddress;
        protected Label lblResultCity;
        protected Label lblResultCountry;
        protected Label lblResultPostalCode;

        protected Label lblResultCreditinfoID;
        protected Label lblResultName;
        protected Label lblResultNationalID;
        protected Label lblResultRegDate;
        protected Label lblResultSecretary;
        protected Label lblSearch;
        protected Label lblSecretaryLabel;
        protected Label lblSubjectIsOnMonitoring;



        protected Label lblResultTelephone;
        protected Label lblResultFax;
        protected Label lblResultWeb;
        protected Label lblResultEmail;
        protected Label lblResultLegalForm;
        protected Label lblResultCompanyStatus;
       

        protected Label lblTelephoneLabel;
        protected Label lblFaxLabel;
        protected Label lblWebLabel;
        protected Label lblEmailLabel;
        protected Label lblLegalFormLabel;
        protected Label lblCompanyStatusLabel;

        protected Label lblMainActivity;
        protected Label lblMainActivityResult;

        protected LinkButton lbtnAddToMonitoring;
        protected LinkButton lbtnBasicReport;
        protected LinkButton lbtnCompanyReport;
        protected LinkButton lbtnCreditinfoReport;
        protected LinkButton lbtnDDD;
        protected LinkButton lbtnNatIdTAFReport;
        protected LinkButton lbtnOrderCompanyReport;
        protected LinkButton lbtnOrderFiscalUpdate;
        protected LinkButton lbtnOrderReport;
        protected LinkButton lbtnPaymentBehaviour;
        protected LinkButton lbtnTAFReport;
        private bool nativeCult;
        private bool orderFiscalStatementsUpdate;
        protected RadioButtonList rbtnlDeliveryType;
        private ResourceManager rm;
        protected HtmlTableCell tdFinancialStatement;

        protected HtmlTableRow trTelephone;
        protected HtmlTableRow trFax;
        protected HtmlTableRow trWeb;
        protected HtmlTableRow trEmail;
        protected HtmlTableRow trLegalForm;
        protected HtmlTableRow trCompanyStatus;
        protected HtmlTableRow trVatNumbers;


        protected HtmlTableCell tdPaymentBahaviour;
        protected HtmlTableCell tdReportDisplay;
        protected HtmlTableRow trAdditionalInformation;
        protected HtmlTableRow trLastArchived;
       


        protected HtmlTableCell trBasicRow;
        protected HtmlTableRow trCompanyGridHeader;
        protected HtmlTableRow trCompanyInfoRow;
        protected HtmlTableRow trCompanyRow;
        protected HtmlTableRow trCompanySearchGrid;
        protected HtmlTableRow trCreditInfoRow;
        protected HtmlTableRow trDDDSelect;

        protected HtmlTableRow trAFS;
               
        protected HtmlTableRow trDeliveryType;
        protected HtmlTableRow trFinancialStatementSelect;
        protected HtmlTableRow trIndividualGridHeader;
        protected HtmlTableRow trIndividualSearchGrid;
        protected HtmlTableRow trOrderCompanyReportRow;
        protected HtmlTableRow trOrderReceipe;
        protected HtmlTableRow trOrderReport;
        protected HtmlTableRow trOrderRow;
        protected HtmlTableRow trOrderStatementsUpdate;
        protected HtmlTableRow trPaymentBehaviourSelect;
        protected HtmlTableRow trRegDate;
        protected HtmlTableRow trReportDisplay;
        protected HtmlTableRow trSecretary;
        protected HtmlTableRow trTAFReport;
        protected HtmlTableRow trMainActivity;
        protected TextBox txtName;
        protected TextBox txtNationalID;

        protected Label lblIndividualLImit;
        protected Label lblCompanyLImit;
        protected TextBox txtVatNumber;
        protected Repeater rptVatNumbers;

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                AddEnterEvent();
                culture = Thread.CurrentThread.CurrentCulture.Name;
                string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
                rm = CIResource.CurrentManager;
                ci = Thread.CurrentThread.CurrentCulture;
                if (culture.Equals(nativeCulture))
                {
                    nativeCult = true;
                }

                //If userLogin  is not exist then go to loin page.
                if (Session["UserLoginID"] == null || Session["UserLoginID"].ToString() == string.Empty)
                {
                    FormsAuthentication.SignOut();
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/fodefault.aspx");
                    Response.End();
                }

                tdFinancialStatement.Visible = false;
                tdPaymentBahaviour.Visible = false;
                trReportDisplay.Visible = false;
                trOrderReceipe.Visible = false;
                trOrderReport.Visible = false;
                //trCreditInfoRow.Visible=false;
                //trOrderRow.Visible=false;

                lblMessage.Visible = false;

                isCompany = lblIsCompany.Text.Trim().ToLower() == "true";

                if (!Page.IsPostBack)
                {
                    Disclaimer(false);
                    LocalizeText();
                    LocalizeGridHeader();
                    FillDeliveryTypeDropDownBox();
                    //FillCityBox();
                    trCompanyInfoRow.Visible = false;
                    HideCompanySearchGrid();
                    HideIndividualSearchGrid();
                    trOrderReceipe.Visible = false;
                    trOrderStatementsUpdate.Visible = false;
                    trOrderReport.Visible = false;
                    trAdditionalInformation.Visible = false;
                    if (Request["reqNationalID"] != null && Request["reqNationalID"].Trim() != "")
                    {
                        if (Request["TAFReport"] != null)
                        {
                            if (Request["TAFReport"].Trim().ToLower() == "true")
                            {
                                int userID = int.Parse(Session["UserLoginID"].ToString());
                                if (userAdminFactory.HasUserAccessToProduct(
                                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_BasicTafReport"))))
                                {
                                    //Open basic taf
                                    Search(Request["reqNationalID"], "", "", "", false, false, false, string.Empty, true);
                                }
                                else
                                {
                                    //Open taf
                                    Search(Request["reqNationalID"], "", "", "", true, false, false, string.Empty, false);
                                }
                            }
                        }
                        else if (Request["BasicTAFReport"] != null)
                        {
                            Search(Request["reqNationalID"], "", "", "", false, false, false, string.Empty, true);
                        }
                        else if (Request["CProfile"] != null)
                        {
                            if (Request["CProfile"].Trim().ToLower() == "true")
                            {
                                Search(Request["reqNationalID"], "", "", "", false, true, false, string.Empty, false);
                            }
                        }
                        else
                        {
                            Search(Request["reqNationalID"], "", "", "", false, false, false, string.Empty, false);
                        }
                    }
                }
                else
                {
                    DisableFSIDDl(null, null);
                }
            }
            catch (NullReferenceException)
            {
                FormsAuthentication.SignOut();
                Response.AddHeader("Refresh", "0");
                Session.Abandon();
            }
        }

        /*private void FillCityBox()
		{
			FactoryBLLC rosFactory = new FactoryBLLC();
			
			//Get the delivery speed from the database
			DataSet theCities = rosFactory.GetAllCities();
			if(theCities!=null&&theCities.Tables!=null &&theCities.Tables.Count>0)
			{
				DataTable dt = theCities.Tables[0];				
				ddlCity.DataSource = dt;
				if(nativeCult)ok
					ddlCity.DataTextField = "NameNative";
				else
					ddlCity.DataTextField = "NameEN";
				ddlCity.DataValueField = "CityID";
				ddlCity.DataBind();
				ddlCity.Items.Insert(0, new ListItem("Not specified", "0"));
			}
		}*/

        private void FillDeliveryTypeDropDownBox()
        {
            var rosFactory = new FactoryBLLC();

            //Get the delivery speed from the database
            var theSpeeds = rosFactory.GetDeliverySpeeds();

            foreach (DataRow row in theSpeeds.Tables[0].Rows)
            {
                var theItem = new ListItem
                              {
                                  Value = ("ID=" + row["ID"] + "TIME=" + row["DaysCount"]),
                                  Text =
                                      (nativeCult
                                           ? row["DeliverySpeedNative"].ToString()
                                           : row["DeliverySpeedEN"].ToString())
                              };

                //Setja � ddlDeliverySpeed
                rbtnlDeliveryType.Items.Add(theItem);
            }
            if (rbtnlDeliveryType.Items.Count > 0)
            {
                rbtnlDeliveryType.Items[0].Selected = true;
            }
        }

        private void FillFSDropDownBox(int ciid)
        {
            ddlFS.Items.Clear();
            //Get the avaialble fs from the database
            var theFS = cpiFactory.GetAvailableReportYearsAsDataSet(ciid);
            if (theFS.Tables == null || theFS.Tables.Count <= 0 || theFS.Tables[0].Rows == null ||
                theFS.Tables[0].Rows.Count <= 0)
            {
                ddlFS.Items.Insert(0, rm.GetString("txtNoFinancialStatementsAvailable", ci));
              //  trOrderCompanyReportRow.Visible = true;
            }
            else
            {
                trOrderCompanyReportRow.Visible = false;
                ddlFS.DataSource = theFS;
                ddlFS.DataTextField = "Financial_year";
                ddlFS.DataValueField = "AFS_id";
                ddlFS.DataBind();
                ddlFS.Items.Insert(0, rm.GetString("txtFinancialStatements", ci));
            }
            
            if (ddlFS.Items.Count == 1)
            {
               // trOrderCompanyReportRow.Visible = false;
               // this.lbtnCompanyReport.Visible = false;

                lblLastUpdate.Visible = false;
                this.trCompanyRow.Visible = this.lbtnCompanyReport.Visible ||  this.lbtnOrderFiscalUpdate.Visible;
            }
        }

        private void Disclaimer(bool show)
        {
            lblDisclaimer1.Visible = show;
            lblDisclaimer2.Visible = show;
        }

        private void AddEnterEvent()
        {
            Control frm = FindControl("FoSearchForm");
            foreach (Control ctrl in frm.Controls)
            {
                if (!(ctrl is TextBox))
                {
                    continue;
                }
                ((TextBox)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
            }
        }

        private void LocalizeText()
        {
            lblName.Text = rm.GetString("txtName", ci);
            btnClear.Text = rm.GetString("txtClear", ci);
            lblNationalID.Text = rm.GetString("txtNationalID", ci);
            //btnOrderReport.Text = rm.GetString("txtOrderReport",ci);
            btnFind.Text = rm.GetString("txtSearch", ci);
            lblSearch.Text = rm.GetString("txtSearch", ci);
            lblNationalIDLabel.Text = rm.GetString("txtNationalID", ci);
            lblNameLabel.Text = rm.GetString("txtName", ci);
            lblOrderNationalIDLabel.Text = rm.GetString("txtNationalID", ci);
            lblOrderNameLabel.Text = rm.GetString("txtName", ci);
            lbtnBasicReport.Text = rm.GetString("txtBasicReport", ci);
            lbtnCompanyReport.Text = rm.GetString("txtCreditInfoReport", ci);
            lbtnCreditinfoReport.Text = rm.GetString("txtCreditInfoReport", ci);
            lbtnOrderReport.Text = rm.GetString("txtOrderCreditReport", ci);
            lblAddressLabel.Text = rm.GetString("txtAddress", ci);
            lblCityLabel.Text = rm.GetString("txtLocality", ci) + ":";
            lblRecNameLabel.Text = rm.GetString("txtName", ci);
            lblRecNationalIDLabel.Text = rm.GetString("txtNationalID", ci);
            lblOrderReceived.Text = rm.GetString("txtOrderReceived", ci);
            lblOrderSubscriberLabel.Text = rm.GetString("txtSubscriber", ci);
            lblOrderUserNameLabel.Text = rm.GetString("txtUserName", ci);
            lblSubjectIsOnMonitoring.Text = rm.GetString("txtSubjectIsOnMonitoring", ci);
            lblAdditionalInformations.Text = rm.GetString("txtAdditionalInformations", ci);
            lblOrderDeliveryTypeLabel.Text = rm.GetString("txtDeliveryType", ci);
            lbtnAddToMonitoring.Text = rm.GetString("txtAddToMonitoring", ci);
            lbtnDDD.Text = rm.GetString("txtDDD", ci);
            lblReportOrder.Text = rm.GetString("txtReportOrder", ci);
            lblOrderSummary.Text = rm.GetString("txtOrderSummary", ci);
            lblCreditinfoIDLabel.Text = rm.GetString("txtCreditInfoID", ci);
            lbtnOrderFiscalUpdate.Text = rm.GetString("txtOrderFiscalUpdate", ci);
            lbtnTAFReport.Text = rm.GetString("txtTAFReport", ci);
            lbtnOrderFiscalUpdate.Text = rm.GetString("txtOrderFiscalUpdate", ci);
            lbtnPaymentBehaviour.Text = rm.GetString("txtPaymentBehaviour", ci);

            //MT
            lbtnNatIdTAFReport.Text = rm.GetString("txtTAFReport", ci);
            lblDateOfReportTitle.Text = rm.GetString("txtDateOfReport", ci) + ":";
            lblSecretaryLabel.Text = rm.GetString("txtSecretary", ci) + ":";
            lblRegDateTitle.Text = rm.GetString("txtRegDate", ci) + ":";
            lblCountryLabel.Text = rm.GetString("txtCountry", ci) + ":";
            lblPostalCodeLabel.Text = rm.GetString("txtPostalCode", ci) + ":";

            lblTelephoneLabel.Text = rm.GetString("txtTelephone", ci) + ":";
            lblFaxLabel.Text = rm.GetString("txtFax", ci) + ":";
            lblWebLabel.Text = rm.GetString("txtWeb", ci) + ":";
            lblEmailLabel.Text = rm.GetString("txtEmail", ci) + ":";
            lblLegalFormLabel.Text = rm.GetString("txtLegalForm", ci) + ":";
            lblCompanyStatusLabel.Text = rm.GetString("txtCompanyStatus", ci) + ":";


          


            btnOrderReport.Text = rm.GetString("txtOrder", ci);

            lblDisclaimer1.Text = rm.GetString("txtReportDisclaimer", ci);
            lblDisclaimer2.Text = rm.GetString("txtMaltaFODisclaimer2", ci);

            lblReportRefIDTitle.Text = rm.GetString("txtReportRefID", ci);

            lblMainActivity.Text = rm.GetString("lblMainActivity", ci);
        }

        private void LocalizeGridHeader()
        {
            dtgrCompanys.Columns[1].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrCompanys.Columns[2].HeaderText = rm.GetString("txtName", ci);
            dtgrCompanys.Columns[5].HeaderText = rm.GetString("txtAddress", ci);
            //this.dtgrCompanys.Columns[8].HeaderText = rm.GetString("txtPostalCode",ci);
            dtgrCompanys.Columns[9].HeaderText = rm.GetString("txtCity", ci);
        }

        private void dtgrCompanys_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                ((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect", ci);

                if (nativeCult)
                {
                    // Name			
                    e.Item.Cells[2].Text = e.Item.Cells[3].Text;
                    if (e.Item.Cells[2].Text == "&nbsp;")
                    {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    }
                    // Address		
                    e.Item.Cells[5].Text = e.Item.Cells[6].Text;
                    if (e.Item.Cells[5].Text == "&nbsp;")
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[7].Text;
                    }
                    // City			
                    e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    if (e.Item.Cells[9].Text == "&nbsp;")
                    {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    }
                }
                else
                {
                    //Name
                    e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    if (e.Item.Cells[2].Text == "&nbsp;")
                    {
                        e.Item.Cells[2].Text = e.Item.Cells[3].Text;
                    }
                    //Address
                    e.Item.Cells[5].Text = e.Item.Cells[7].Text;
                    if (e.Item.Cells[5].Text == "&nbsp;")
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[6].Text;
                    }
                    //City
                    e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    if (e.Item.Cells[9].Text == "&nbsp;")
                    {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    }
                }
            }
        }

        protected void displayReport(string reportType)
        {
            string nationalID = lblResultNationalID.Text;
            int ciid = -1;
            if (nationalID != "")
            {
                ciid = cpiFactory.GetCIIDByNationalID(nationalID);
            }

            if (ciid > -1)
            {
                var reportYears = cpiFactory.GetAvailableReportYearsAsDataSet(ciid);

                var afsIds = GetAFSIDs(reportYears, 5);

                Session["CompanyCIID"] = ciid;
                Session["ReportType"] = reportType;
                Session["ReportCulture"] = nativeCult ? "Native" : "en-US";
                Session["AFSID"] = !string.IsNullOrEmpty(afsIds) ? afsIds : "";
                Server.Transfer("CR/FoReport.aspx");
            }
            else
            {
                DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
            }
        }

        private static string GetAFSIDs(DataSet ds, int limit)
        {
            var afs_id = new StringBuilder();
            var first = true;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count < limit)
                {
                    limit = ds.Tables[0].Rows.Count;
                }
                for (int i = 0; i < limit; i++)
                {
                    if (!first)
                    {
                        afs_id.Append(","); // Add the comma
                    }
                    afs_id.Append(ds.Tables[0].Rows[i]["AFS_ID"].ToString());
                    first = false;
                }
                return afs_id.ToString();
            }
            return null;
        }

        /*/// <summary>
		/// Checks if report for given company has expired and returns true if expired
		/// </summary>
		/// <param name="ciid">CreditInfo id for the company to check</param>
		/// <returns>True if the report has expired, false if not</returns>
		private bool CheckIfReportIsExpired(int ciid)
		{
			ReportCompanyBLLC report = cpiFactory.GetCompanyReport("", ciid, false);	
			
			if(report.ExpireDate<DateTime.Now)
			{
				return true;
			}
			else if(report.LastContacted<DateTime.Now.AddMonths(-int.Parse(CigConfig.Configure("lookupsettings.monthsReportExpiresAfterLastContacted"].Trim())))
			{
				return true;
			}
			else
			{
				return false;
			}			
		}	*/

        private void SetReportButtonVisability()
        {
            trOrderCompanyReportRow.Visible = false;
            trPaymentBehaviourSelect.Visible = false;
            

            var reportAvailable = false;
            var nationalID = lblResultNationalID.Text;
            var ciid = cpiFactory.GetCIIDByNationalID(nationalID);
            if (ciid > 0)
            {
                var theCompany = cpiFactory.GetCompanyReport(ciid);
                if (theCompany != null)
                {
                    //Fyrir credit report - ef til eitthva� � cpi og innan vi� 18 m�nu�ir s��an s��ast 
                    //var haft samband e�a sk�rsla ekki �trunnin �� birta hnapp - annars order hnappur
                    if (!theCompany.HasExpired)
                    {
                        reportAvailable = theCompany.IsReady;
                    }
                }
                lblResultCreditinfoID.Text = ciid.ToString();
            }

            var userID = int.Parse(Session["UserLoginID"].ToString());


            int productKey = int.Parse(CigConfig.Configure("lookupsettings.ProductKey_DDD"));

            trDDDSelect.Visible = userAdminFactory.HasUserAccessToProduct(
                userID, productKey);
            //lbtnOrderFiscalUpdate right.
            
            this.lblLastUpdate.Visible = userAdminFactory.HasUserAccessToProduct(userID, 1502) && isCompany;
            this.lblLastArchived.Visible = userAdminFactory.HasUserAccessToProduct(userID, 1501) && isCompany;

            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_BasicTafReport"))))
            {
                lbtnTAFReport.Text = rm.GetString("txtBasicTafReport", ci);
                lbtnNatIdTAFReport.Text = rm.GetString("txtBasicTafReport", ci);
                
                this.lbtnCompanyReport.Visible = false;
                this.lbtnOrderFiscalUpdate.Visible = false;
                this.trCompanyRow.Visible = this.lbtnCompanyReport.Visible ||  this.lbtnOrderFiscalUpdate.Visible;

            }
            else
            {
                lbtnTAFReport.Text = "TAF report";
                lbtnNatIdTAFReport.Text = "TAF report";
                bool cRepVisible = userAdminFactory.HasUserAccessToProduct(userID, 1300);
                bool oRepVisible = userAdminFactory.HasUserAccessToProduct(userID, 1301);
                this.trCompanyRow.Visible = cRepVisible || oRepVisible;
                this.lbtnCompanyReport.Visible = cRepVisible;
                this.lbtnOrderFiscalUpdate.Visible = oRepVisible;               
            }

            if (isCompany)
            {
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_BasicTafReport"))))
                {
                    trTAFReport.Visible = true;
                    trBasicRow.Visible = false;
                    trOrderCompanyReportRow.Visible = false;
                    trFinancialStatementSelect.Visible = false;
                    if (!this.lblLastUpdate.Visible && !this.lblLastArchived.Visible)
                    {
                        trCompanyRow.Visible = false;
                    }
                    trDDDSelect.Visible = false;
                    trCreditInfoRow.Visible = false;
                    trOrderRow.Visible = false;
                }
                else
                {
                    // Company profile and Company report (!?) reports shall always be available. Company reports shall however not be available if no fiscal statements are registed				
                    trBasicRow.Visible = userAdminFactory.HasUserAccessToProduct(
                        userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_BasicReport")));

                    if (userAdminFactory.HasUserAccessToProduct(
                        userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_CompanyReport"))))
                    {
                        lbtnCompanyReport.Text = rm.GetString("txtCreditInfoReport", ci);
                        var reportYears = cpiFactory.GetAvailableReportYearsAsDataSet(ciid);
                        if (reportYears.Tables.Count > 0)
                        {
                            int fiscalYearCount = reportYears.Tables[0].Rows.Count;
                            // if no fiscal statements then display order fiscal statements button
                            if (fiscalYearCount == 0)
                            {
                             //   trOrderCompanyReportRow.Visible = true;
                                //trOrderStatementsUpdate.Visible = true;
                                //trFinancialStatementSelect.Visible = true;
                                if (!this.lblLastUpdate.Visible && !this.lblLastArchived.Visible)
                                {
                                    trCompanyRow.Visible = false;
                                }
                            }
                            else
                            {
                                int fsireports;
                                try
                                {
                                    fsireports = int.Parse(CigConfig.Configure("lookupsettings.FoSearch.FsiLimit"));
                                }
                                catch
                                {
                                    fsireports = 3;
                                }
                                if (fiscalYearCount < fsireports) { }
                                //			fiscalCountString = String.Format(rm.GetString("txtFiscalCount",ci), fsireports);
                                //			lbtnCompanyReport.Text = rm.GetString("txtCompanyReport",ci) + " " +  fiscalCountString;
                                lbtnCompanyReport.Text = rm.GetString("txtMTCompanyReport", ci);
                                trOrderCompanyReportRow.Visible = false;
                                trCompanyRow.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        if (!this.lblLastUpdate.Visible && !this.lblLastArchived.Visible)
                        {
                            trCompanyRow.Visible = false;
                        }
                        trOrderCompanyReportRow.Visible = false;
                    }

                    //Fyrir company report - alltaf til company report en ef ekki �rsreiknigur til - 
                    //�� birta hnapp fyrir order company report	
                    if (userAdminFactory.HasUserAccessToProduct(
                        userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_CompanyReport"))))
                    {
                        if (reportAvailable)
                        {
                            trCreditInfoRow.Visible = true;
                            trOrderRow.Visible = false;
                        }
                        else
                        {
                            trCreditInfoRow.Visible = false;
                            trOrderRow.Visible = true;
                        }
                    }
                    else
                    {
                        trCreditInfoRow.Visible = false;
                        trOrderRow.Visible = false;
                    }

                    /*if(userAdminFactory.HasUserAccessToProduct(userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_PaymentBehaviour"])))
					{
						if(creditInfoFactory.IsPaymentBehaviourDataAvailable(ciid))
							trPaymentBehaviourSelect.Visible=true;
						else
							trPaymentBehaviourSelect.Visible=false;
					}
					else
						trPaymentBehaviourSelect.Visible=false;*/

                    DisableFSIDDl(userID, ciid);

                    DataSet reportLastArchivedDates = null;
                    if (this.lblLastUpdate.Visible || this.lblLastArchived.Visible)
                    {
                        reportLastArchivedDates = cpiFactory.GetArchivedDatesAsDataSet(ciid);
                    }

                    if (this.lblLastUpdate.Visible)
                    {

                     //   var reportMaxDates = cpiFactory.GetMaxDatesAsDataSet(ciid);
                        //if (reportMaxDates != null)
                        //{
                        //    lblLastUpdate.Text = MaxDateFromDataSet(reportMaxDates);
                        //}

                        if (ddlFS.Items.Count > 1)
                            lblLastUpdate.Text = MaxDateFromDataSet(/*reportMaxDates*/ddlFS.Items[1].Text);
                       

                        if (userAdminFactory.HasUserAccessToProduct(
                            userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_CreditInfoReport"))))
                        {
                            this.lbtnCompanyReport.Visible = true;
                            trCompanyRow.Visible = true;

                        }
                        else
                        {
                            this.lbtnCompanyReport.Visible = false;
                            trCompanyRow.Visible = false;

                        }

                        if (string.IsNullOrEmpty(this.lblLastUpdate.Text.Trim()))
                        {
                            this.lbtnCompanyReport.Visible = false;
                        }

                    }

                    if (this.lblLastArchived.Visible)
                    {
                        try
                        {
                            lblLastArchived.Text = "Financial statements last archived at MFSA on " +
                                                   Convert.ToDateTime(
                                                       reportLastArchivedDates.Tables[0].Rows[0]["DataArchivedPub"].
                                                           ToString()).ToShortDateString();
                        }
                        catch (Exception)
                        {
                            lblLastArchived.Text = "(Filed Accounts)";
                        }
                    }

                }
            }
            else
            {
                trBasicRow.Visible = false;
                if (!this.lblLastUpdate.Visible && !this.lblLastArchived.Visible)
                {
                    trCompanyRow.Visible = false;
                }
                trCreditInfoRow.Visible = false;
                trOrderRow.Visible = false;
                trFinancialStatementSelect.Visible = false;
                trPaymentBehaviourSelect.Visible = false;
                trTAFReport.Visible = true;
            }

            if (string.IsNullOrEmpty(this.lblLastUpdate.Text.Trim()))
            {
         //       this.lbtnCompanyReport.Visible = false;
            }


            if (!lbtnCompanyReport.Visible)
            {
              //  trBasicRow.RowSpan = 2;
                lbtnOrderFiscalUpdate.Visible = false;
            }
            else
            {
                trBasicRow.RowSpan = 1;

            }
        }

        private bool HasAccessFSI
        {
            get { return ViewState["HasAccessFSI"] != null ? (bool)ViewState["HasAccessFSI"] : false; }
            set { ViewState["HasAccessFSI"] = value; }
        }

        private void DisableFSIDDl(int? userID, int? ciid)
        {

            bool fsiAllowed = false;
            bool fsiDDLEnabled = false;
            bool hasAccessFSI = false;

            if (userID.HasValue && ciid.HasValue)
            {
                 fsiAllowed = userAdminFactory.HasUserAccessToProduct(userID.Value, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FinancialStatement")));
                 fsiDDLEnabled  = userAdminFactory.HasUserAccessToProduct(userID.Value, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FinancialStatementDDL")));


                FillFSDropDownBox(ciid.Value);

                if (fsiAllowed)
                {
                  
                    trFinancialStatementSelect.Visible = true;
                    if (!fsiDDLEnabled)
                    {
                        hasAccessFSI = false;
                    }
                    else
                    {
                        hasAccessFSI = true;
                    }
                }
                else
                {
                    trFinancialStatementSelect.Visible = false;
                }
            }
            if (hasAccessFSI)
            {
                ddlFS.AutoPostBack = true;
                foreach (ListItem itm in ddlFS.Items)
                    itm.Attributes.Remove("disabled");
            }
            else
            {
                ddlFS.AutoPostBack = false;
                foreach (ListItem itm in ddlFS.Items)
                    itm.Attributes.Add("disabled", "");
            }
            if (!fsiAllowed)
            {
                ddlFS.Visible = false;
                lblLastArchived.Visible = false;
            }
            else
            {
                lblLastArchived.Visible = true;
                ddlFS.Visible = true;
            }
        }

        private void btnClear_Click(object sender, EventArgs e) { ClearForm(); }
        /*private void ClearGrids()
		{
			dtgrCompanys.DataSource=null;
			dtgrIndividual.DataSource = null;
			dtgrCompanys.DataBind();
			dtgrIndividual.DataBind();
		}*/

        private void ClearForm()
        {
            this.txtVatNumber.Text = string.Empty;
            txtName.Text = "";
            //txtSurName.Text="";
            txtNationalID.Text = "";
            //lblNoCompanyFound.Visible=false;
            trCompanyInfoRow.Visible = false;
            tdFinancialStatement.Visible = false;
            trAdditionalInformation.Visible = false;
            trCompanySearchGrid.Visible = false;
            trIndividualSearchGrid.Visible = false;

            trOrderReceipe.Visible = false;
            trOrderReport.Visible = false;
            ddlCity.SelectedIndex = 0;

            lblCompanyGridHeader.Visible = false;
            lblIndividualGridHeader.Visible = false;

            lblIndividualLImit.Visible = false;
            lblCompanyLImit.Visible = false;

            lblDisclaimer1.Visible = false;
            lblDisclaimer2.Visible = false;
            Session["orderFiscalStatementsUpdate"] = null;
        }

        private void btnOrderReport_Click(object sender, EventArgs e)
        {
            var nationalID = lblOrderDispCompanyNationalID.Text;
            if (nationalID == "") { return; }
            var ciid = cpiFactory.GetCIIDByNationalID(nationalID);

            if (ciid == -1)
            {
                var company = new Company
                              {
                                  NationalID = nationalID,
                                  NameNative = lblOrderDispCompanyName.Text,
                                  NameEN = lblOrderDispCompanyName.Text,
                                  Type = CigConfig.Configure("lookupsettings.companyID")
                              };
                ciid = userAdminFactory.AddCompany(company);
            }
            if (ciid != -1)
            {
                var myOrder = new OrderBLLC();
                var subject = new CustomerBLLC { CreditInfoID = ciid };
                myOrder.Subject = subject;
                var customer = new CustomerBLLC
                               {
                                   CreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString())
                               };
                myOrder.Customer = customer;

                myOrder.OrderDate = DateTime.Now;

                myOrder.Product.ID = int.Parse(lblOrderProductID.Text);
                myOrder.OrderTypeID = int.Parse(CigConfig.Configure("lookupsettings.ROSOrderTypeWeb").Trim());

                if (Session["orderFiscalStatementsUpdate"] != null)
                {
                    orderFiscalStatementsUpdate = (bool)Session["orderFiscalStatementsUpdate"];
                }

                if (!orderFiscalStatementsUpdate)
                {
                    String deliveryID = rbtnlDeliveryType.SelectedValue.Substring(
                        0, rbtnlDeliveryType.SelectedValue.IndexOf("T"));
                    myOrder.DeliverySpeedID = int.Parse(deliveryID.Substring(deliveryID.IndexOf("=", 0) + 1));
                }
                else
                {
                    myOrder.DeliverySpeedID = int.Parse(CigConfig.Configure("lookupsettings.ROSorderFiscalUpdateID"));
                }
                //ID=1TIME=7

                var rosFactory = new FactoryBLLC();
                rosFactory.CreateReportOrder(myOrder);
                lblRecNationalID.Text = nationalID;
                lblRecName.Text = lblResultName.Text;
                //	ClearForm();
                //	trOrderRow.Visible=false;
                trOrderReceipe.Visible = true;
            }
            else
            {
                DisplayErrorMessage(rm.GetString("txtCouldNotOrderReport", ci));
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            Disclaimer(false);
            //Clear old values
            lblResultName.Text = "";
            lblResultAddress.Text = "";
            lblResultCity.Text = "";
            this.lblResultCountry.Text = "";
            lblResultPostalCode.Text = "";
            lblResultCreditinfoID.Text = "";
            lblResultNationalID.Text = "";
            lblResultRegDate.Text = "";
            trOrderReceipe.Visible = false;
            trOrderReport.Visible = false;

           //this.lblResultTelephone.Visible = false;
            //this.lblResultFax.Visible = false;
            //this.lblResultWeb.Visible = false;
            //this.lblResultEmail.Visible = false;
            //this.lblResultLegalForm.Visible = false;
            //this.lblResultCompanyStatus.Visible = false;


            trCompanyInfoRow.Visible = false;
            trAdditionalInformation.Visible = false;

            dtgrCompanys.SelectedIndex = -1;
            dtgrIndividual.SelectedIndex = -1;

            try
            {
                if (txtNationalID.Text.Trim() == "" && txtName.Text.Trim() == "" && this.txtVatNumber.Text.Trim() == "" && ddlCity.SelectedIndex == 0)
                {
                    DisplayMessage(rm.GetString("txtNoSearchCriteria", ci));
                }
                else
                {
                    if (ddlCity.SelectedIndex > 0)
                    {
                        Search(
                            txtNationalID.Text.Trim(),
                            txtName.Text.Trim(),
                            "",
                            ddlCity.SelectedItem.Text,
                            false,
                            false,
                            true, this.txtVatNumber.Text.Trim(),false);
                    }
                    else
                    {
                        Search(txtNationalID.Text.Trim(), txtName.Text.Trim(), "", "", false, false, false, this.txtVatNumber.Text.Trim(),false);
                    }
                }
            }
            catch (NullReferenceException)
            {
                FormsAuthentication.SignOut();
                Response.AddHeader("Refresh", "0");
                Session.Abandon();
            }
        }

        private void Search(
            string nationalID,
            string name,
            string address,
            string city,
            bool displayTAF,
            bool displayCompanyProfile,
            bool exactAddress,
            string vatNumber,
            bool displayBasicTafReport)
        {
            Disclaimer(false);
            //Clear old values

            //*********************** SPECIAL CASE FOR MALTA HSJ/HH 2005.1012 ********** BEGIN
            //special characters in city name lost in dropdownlist due to en-gb cultureinfo.  
            //framework functions search by city name, not id
            //in future code revisions please search by id.
            if (city.ToLower() == "pieta'" || city.ToLower() == "pieta")
            {
                city = "Pieta�";
            }
            if (city.ToLower() == "ta' xbiex" || city.ToLower() == "ta xbiex")
            {
                city = "Ta� Xbiex";
            }
            //*********************** SPECIAL CASE FOR MALTA HSJ/HH 2005.1012 ********** END 

            lblResultCreditinfoID.Text = "";
            lblResultNationalID.Text = "";
            lblResultRegDate.Text = "";

            trCompanyInfoRow.Visible = false;
            trAdditionalInformation.Visible = false;

            dtgrCompanys.SelectedIndex = -1;
            dtgrIndividual.SelectedIndex = -1;

            HideCompanySearchGrid();
            HideIndividualSearchGrid();

            if (nationalID.Trim() != "")
            {
                txtNationalID.Text = nationalID.Trim();
                txtName.Text = "";
            }

            lblResultName.Text = "";
            lblResultAddress.Text = "";
            lblResultCity.Text = "";
            trOrderReceipe.Visible = false;
            trOrderReport.Visible = false;

            var uaFact = new uaFactory();
            DataSet dsCompany = new DataSet();
            DataSet dsIndividual = new DataSet();

            var comp = new Company { NameNative = name, NationalID = nationalID, IsSearchable = true };

            var debtor = new Debtor
                         {
                             FirstName = name.Trim(),
                             IDNumber1 = nationalID,
                             IDNumber2Type = 1,
                             IsSearchable = true
                         };

            Address indAddr = null;
            Address compAddr = null;

            if (city.Trim() != "")
            {
                indAddr = new Address { CityNameNative = city };
                compAddr = new Address { CityNameNative = city };
            }

            if (address.Trim() != "")
            {
                compAddr = new Address { StreetNative = address };

                if (indAddr == null)
                {
                    indAddr = new Address();
                }
                indAddr.StreetNative = address;
            }

            if (compAddr != null)
            {
                comp.Address = new ArrayList { compAddr };
            }

            if (indAddr != null)
            {
                debtor.Address = new ArrayList { indAddr };
            }

            bool showCompany = false;
            bool showIndividual = false;
            bool showIndividualWithVat = false;
            var userID = int.Parse(Session["UserLoginID"].ToString());
            var subscriberId = uaFact.GetUser(userID).SubscriberID;
            var searchFiltersSubscriber = uaFact.GetSearchFilters(subscriberId, true); // search filters for subscribers
            var searchFiltersUsers = uaFact.GetSearchFilters(userID, false); // search filters for users
            if(searchFiltersSubscriber == null & searchFiltersUsers == null) // if no record for search filter exist then display all records
            {
                showCompany = true;
                showIndividual = true;
                showIndividualWithVat = true;
            }
            else
            {
                if(searchFiltersSubscriber != null)
                {
                    showCompany = searchFiltersSubscriber.isComp;
                    showIndividual = searchFiltersSubscriber.isInd;
                    showIndividualWithVat = searchFiltersSubscriber.isIndVat;
                }
                if (searchFiltersUsers != null)
                {
                    showCompany = searchFiltersUsers.isComp;
                    showIndividual = searchFiltersUsers.isInd;
                    showIndividualWithVat = searchFiltersUsers.isIndVat;
                }

            }

            if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry").ToLower() == "true")
            {
                if (showCompany) { dsCompany = uaFact.FindCompanyInNationalAndCreditInfo(comp, exactAddress); }

                if (showIndividual) { dsIndividual = uaFact.FindCustomerInNationalAndCreditInfo(debtor, exactAddress); }
            }
            else
            {
                if (showCompany) { dsCompany = uaFact.FindMaltaCompany(comp, exactAddress, false, vatNumber, true); }
                //	dsCompany = uaFact.FindCompany(comp, exactAddress);
                // if show individual with VAT is checked then show individual should be true because i need it to show individuals with VAT records

               if (showIndividualWithVat && showIndividual) { showIndividualWithVat = false; showIndividual = true; }
               else if (showIndividualWithVat && !showIndividual) { showIndividual = true; }
                if (showIndividual) { dsIndividual = uaFact.FindCustomer(debtor, exactAddress, vatNumber, showIndividualWithVat); }
            }

            if ((dsIndividual.Tables.Count <= 0 || dsIndividual.Tables[0].Rows.Count <= 0) &&
                (dsCompany.Tables.Count <= 0 || dsCompany.Tables[0].Rows.Count <= 0))
            {
                HideCompanySearchGrid();
                HideIndividualSearchGrid();
                DisplayErrorMessage(rm.GetString("txtNoDataOnSubject", ci));
                return;
            }

            //Check if only one individual found and no company - then display 
            if (dsIndividual.Tables.Count > 0 && dsIndividual.Tables[0].Rows.Count == 1)
            {
                if (dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count == 0)
                {
                    SetCompany(false);
                    DisplayDetailsFromDataSet(false, dsIndividual);
                    if (displayTAF)
                    {
                        DisplayTAFReport(int.Parse(lblResultCreditinfoID.Text), nationalID, false);
                    }
                    else if (displayBasicTafReport)
                    {
                        this.DisplayBasicTAFReport(int.Parse(lblResultCreditinfoID.Text), nationalID, false);
                    }
                    return;
                }
            }

            //Check if only one company found and no individual - then display 
            if (dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count == 1)
            {
                trOrderStatementsUpdate.Visible = true;
                trFinancialStatementSelect.Visible = true;
                if (dsIndividual.Tables.Count > 0 && dsIndividual.Tables[0].Rows.Count == 0)
                {
                    SetCompany(true);
                    DisplayDetailsFromDataSet(true, dsCompany);
                    if (displayTAF)
                    {
                        DisplayTAFReport(int.Parse(lblResultCreditinfoID.Text), nationalID, true);
                    }
                    else if (displayBasicTafReport)
                    {
                        this.DisplayBasicTAFReport(int.Parse(lblResultCreditinfoID.Text), nationalID, true);
                    }
                    else if (displayCompanyProfile)
                    {
                        DisplayCompanyProfile(int.Parse(lblResultCreditinfoID.Text), nationalID);
                    }
                    return;
                }
            }

            if (dsIndividual.Tables.Count > 0 && dsIndividual.Tables[0].Rows.Count > 0)
            {
                //If search by national id - dont display grid - 
                if (nationalID.Trim() != "")
                {
                    SetCompany(false);
                    DisplayDetailsFromDataSet(false, dsIndividual);
                    if (displayTAF)
                    {
                        DisplayTAFReport(int.Parse(lblResultCreditinfoID.Text), nationalID, false);
                    }
                    else if (displayBasicTafReport)
                    {
                        this.DisplayBasicTAFReport(int.Parse(lblResultCreditinfoID.Text), nationalID, false);
                    }
                }
                else
                {
                    dtgrIndividual.SelectedIndex = -1;
                    dtgrIndividual.DataSource = dsIndividual;
                    dtgrIndividual.DataBind();
                    DisplayIndividualSearchGrid();
                }
            }

            if (dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count > 0)
            {
                //If search by national id - dont display grid - 
                if (nationalID.Trim() != "")
                {
                    SetCompany(true);
                    DisplayDetailsFromDataSet(true, dsCompany);
                    if (displayTAF)
                    {
                        DisplayTAFReport(int.Parse(lblResultCreditinfoID.Text), nationalID, true);
                    }
                    else if (displayBasicTafReport)
                    {
                        this.DisplayBasicTAFReport(int.Parse(lblResultCreditinfoID.Text), nationalID, true);
                    }
                    else if (displayCompanyProfile)
                    {
                        DisplayCompanyProfile(int.Parse(lblResultCreditinfoID.Text), nationalID);
                    }
                }
                else
                {
                    dtgrCompanys.SelectedIndex = -1;
                    dtgrCompanys.DataSource = dsCompany;
                    dtgrCompanys.DataBind();
                    DisplayCompanySearchGrid();
                }
            }

            if (dsIndividual.Tables.Count > 0 && dsIndividual.Tables[0].Rows.Count >= 200)
            {
                this.lblIndividualLImit.Text = "    " + rm.GetString("lblIndividualSearchLimit", ci);
                this.lblIndividualLImit.Visible = true;
                // this.lblIndividualLImit
            }
            else
            {
                this.lblIndividualLImit.Visible = false;
            }

            if (dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count >= 200)
            {
                this.lblCompanyLImit.Text = "    " + rm.GetString("lblCompanySearchLimit", ci); ;
                this.lblCompanyLImit.Visible = true;
            }
            else
            {
                this.lblCompanyLImit.Visible = false;
            }
        }


        private void SetCompanyInfo(bool isCompany)
        {
            trTelephone.Visible = isCompany;
            trFax.Visible = isCompany;
            trWeb.Visible = isCompany;
            trEmail.Visible = isCompany;
            trLegalForm.Visible = isCompany;
            trCompanyStatus.Visible = isCompany;
            trLastArchived.Visible = isCompany;
            this.trVatNumbers.Visible = !isCompany;

        }


        private void SetCompany(bool isComp)
        {
            isCompany = isComp;
            lblIsCompany.Text = isComp.ToString();
            trAFS.Visible = false;
            //if(!isCompany) 
            //    trAFS.Visible = false;
            //else
            //    trAFS.Visible = true;


            SetCompanyInfo(isComp);
        }

        private void DisplayCompanySearchGrid()
        {
            txtNationalID.Text = "";
            trReportDisplay.Visible = false;
            trCompanyGridHeader.Visible = true;
            lblCompanyGridHeader.Text = rm.GetString("txtCompanies", ci) + ": " +
                                        ((DataSet)dtgrCompanys.DataSource).Tables[0].Rows.Count;
            lblCompanyGridHeader.Visible = true;
            trCompanySearchGrid.Visible = true;
        }

        private void HideCompanySearchGrid()
        {
            trCompanyGridHeader.Visible = false;
            trCompanySearchGrid.Visible = false;
            dtgrCompanys.DataSource = null;
            dtgrCompanys.DataBind();
            lblCompanyGridHeader.Visible = false;
        }

        private void DisplayIndividualSearchGrid()
        {
            txtNationalID.Text = "";
            trCompanyInfoRow.Visible = false;
            trIndividualGridHeader.Visible = true;
            lblIndividualGridHeader.Text = /*rm.GetString("txtCompanies", ci) +*/ "Individuals: " +
                                                                                  ((DataSet)dtgrIndividual.DataSource).
                                                                                      Tables[0].Rows.Count;
            lblIndividualGridHeader.Visible = true;
            trIndividualSearchGrid.Visible = true;
        }

        private void HideIndividualSearchGrid()
        {
            trIndividualGridHeader.Visible = false;
            trIndividualSearchGrid.Visible = false;
            dtgrIndividual.DataSource = null;
            dtgrIndividual.DataBind();
            lblIndividualGridHeader.Visible = false;
        }

        private void DisplayDetails(
            bool company,
            string creditinfoId,
            string nationalID,
            string name,
            string address,
            string city,
            string country,
            string postalCode,
            string note, string companyStatus, string legalForm
            )
        {
            Disclaimer(true);
            HideCompanySearchGrid();
            HideIndividualSearchGrid();
            trCompanyInfoRow.Visible = true;
            lblResultCreditinfoID.Text = creditinfoId == "" ? "-1" : creditinfoId;

            lblNationalIDLabel.Visible = true;
            lblNameLabel.Visible = true;
            lblAddressLabel.Visible = true;
            lblCityLabel.Visible = true;
            lblCountryLabel.Visible = true;
            lblPostalCodeLabel.Visible = true;
            

            lblNote.Text = note;
           
            lblResultNationalID.Text = nationalID;
            if (string.IsNullOrEmpty(lblResultNationalID.Text.Trim()))
                lblNationalIDLabel.Visible = false; 
            
            lblResultName.Text = name;
            if (string.IsNullOrEmpty(lblResultName.Text.Trim()))
              lblNameLabel.Visible = false;

            lblResultAddress.Text = address;
            if (string.IsNullOrEmpty(lblResultAddress.Text.Trim()))
                lblAddressLabel.Visible = false;

            lblResultCity.Text = city;

            if (string.IsNullOrEmpty(lblResultCity.Text.Trim()))
                this.lblCityLabel.Visible = false;
    
      
            lblResultCountry.Text = country;
            lblResultPostalCode.Text = postalCode;
            if(string.IsNullOrEmpty( lblResultCountry.Text.Trim()))
                lblCountryLabel.Visible = false;

            if (string.IsNullOrEmpty( lblResultPostalCode.Text.Trim()))
                lblPostalCodeLabel.Visible = false;
    
            txtName.Text = "";
            txtNationalID.Text = nationalID;
        
            //	this.lblResultPostcode.Text = postcode;
            lblDateOfReport.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();



            SetCompanyInfo(company);

            if (!string.IsNullOrEmpty(companyStatus))
            {
                lblResultCompanyStatus.Text = companyStatus;
            }
            else
                trCompanyStatus.Visible = false;

            lblResultLegalForm.Text = legalForm;
            if (string.IsNullOrEmpty(lblResultLegalForm.Text.Trim()))
                this.trLegalForm.Visible = false;
           
            if (company)
            {
                int ciid = -1;
                if (int.TryParse(creditinfoId, out ciid))
                    if (company)
                    {
                        this.getContactInfo(ciid);
                        this.getCompanyOperation(ciid);
                    }
                //trOrderStatementsUpdate.Visible = true;
                //trFinancialStatementSelect.Visible = true;

                trRegDate.Visible = true;
                var crFact = new CRFactory();
                var dt = crFact.GetMTCompanyRegDate(nationalID);

                if (dt != DateTime.MinValue)
                {
                    lblResultRegDate.Text = dt.ToShortDateString();
                    if (string.IsNullOrEmpty(lblResultRegDate.Text.Trim()))
                        this.trRegDate.Visible = false;
                }
                else
                    this.trRegDate.Visible = false;

                BoardSecretaryBLLC sec = crFact.GetMTBoardSecretary(nationalID);
                if (sec != null)
                {
                    trSecretary.Visible = true;
                    lblResultSecretary.Text = sec.SurNameNative + " " + sec.FirstNameNative + "<BR>ID no.: " +
                                              sec.NationalID;

                    var userID = int.Parse(Session["UserLoginID"].ToString());
                    if (userAdminFactory.HasUserAccessToProduct(
                            userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_BasicTafReport"))))
                    {
                        lblResultSecretary.Text = lblResultSecretary.Text + " <A HREF='FoSearch.aspx?BasicTAFReport=true&company=false&reqNationalID=" +
                                                     sec.NationalID + "'>Basic TAF Report</A>";
                    }
                    else
                    {
                        lblResultSecretary.Text = lblResultSecretary.Text + " <A HREF='FoSearch.aspx?TAFReport=true&company=false&reqNationalID=" +
                                                                          sec.NationalID + "'>TAF-report</A>";

                    }
                }
                else
                {
                    trSecretary.Visible = false;
                }
            }
            else
            {
                //trOrderStatementsUpdate.Visible = false;
                trRegDate.Visible = false;
                trSecretary.Visible = false;
                lblCountryLabel.Visible = false;
                lblPostalCodeLabel.Visible = false;
                trLastArchived.Visible = false;
                trMainActivity.Visible = false;

                int ciid = -1;
                if (int.TryParse(creditinfoId, out ciid))
                {
                    this.getVatNumbers(ciid);
                }
            }

            DisplayWatchButton(company);
            trAdditionalInformation.Visible = true;
            SetReportButtonVisability();
            if (lblResultCreditinfoID.Text.Length > 0)
            {
                int repRef = logUsage(
                    lblResultCreditinfoID.Text, CigConfig.Configure("lookupsettings.ProductKey_FOSearch"));
                if (repRef > 0)
                {
                    lblReportRefID.Text = repRef.ToString();
                }
            }
        }

        private void getCompanyOperation(int creditInfoId)
        {
            CPIFactory myFactory = new CPIFactory();
            var operation = myFactory.GetCompanyOperationAsDataSet(creditInfoId, true);

            trMainActivity.Visible = false;
            if (operation != null)
            {
                if (operation.Tables.Count > 0)
                {
                    if (operation.Tables[0].Rows.Count > 0)
                    {
                        trMainActivity.Visible = true;
                        StringBuilder sb = new StringBuilder();
                        foreach (DataRow row in operation.Tables[0].Rows)
                        {
                            sb.AppendLine(row["ShortDescriptionEN"].ToString() + "<br/>");
                        }

                        lblMainActivityResult.Text = sb.ToString();
                    }
                }
            }
        }

        private void getContactInfo(int creditinfoId)
        {
            uaFactory myFactory = new uaFactory();
            Company company = myFactory.GetCompany(creditinfoId, true);


            
            lblResultWeb.Text = company.URL;
            if (string.IsNullOrEmpty(lblResultWeb.Text.Trim()))
                trWeb.Visible = false;

            lblResultEmail.Text = company.Email;
            if (string.IsNullOrEmpty(lblResultEmail.Text.Trim()))
                trEmail.Visible = false;
            //if (company.Org_status_code != 0)
            //{
            //    var statusCode = rm.GetString("txtOrgStatus" + company.Org_status_code, ci);
            //    lblResultCompanyStatus.Text = (string.IsNullOrEmpty(statusCode)) ? company.Org_status_code.ToString() : statusCode;
            //}
            //else
            //    trCompanyStatus.Visible = false;
            //lblResultLegalForm.Text = company.LegalFormEn;
            //if (string.IsNullOrEmpty(lblResultLegalForm.Text.Trim()))
            //    this.trLegalForm.Visible = false;

            trTelephone.Visible = false;
            trFax.Visible = false;

            foreach(PhoneNumber num in company.Number)
            {
                if (num.NumberTypeID == 2)
                {
                    lblResultTelephone.Text = num.Number;
                    if (!string.IsNullOrEmpty(lblResultTelephone.Text.Trim()))
                        trTelephone.Visible = true;
                }
                if (num.NumberTypeID == 4)
                {
                    lblResultFax.Text = num.Number;
                    if (!string.IsNullOrEmpty(lblResultFax.Text.Trim()))
                    {
                        trFax.Visible = true;
                    }
                }
            }
        }

        /// <summary>
        /// Get vat numbers data and bind it to datagrid.
        /// </summary>
        /// <param name="creditInfoId"></param>
        private void getVatNumbers(int creditInfoId)
        {
            uaFactory myFactory = new uaFactory();

            IList<VatNumber> listOfVatNumbers = myFactory.GetVatNumbersByCreditinfoId(creditInfoId, true);

            if (listOfVatNumbers.Count == 0)
            {
                this.rptVatNumbers.Visible = false;
                this.trVatNumbers.Visible = false;
            }
            else
            {
                this.trVatNumbers.Visible = true;
                this.rptVatNumbers.Visible = true;
                this.rptVatNumbers.DataSource = listOfVatNumbers;
                this.rptVatNumbers.DataBind();
            }
        }

        private void DisplayDetailsFromDataSet(bool company, DataSet ds)
        {
            Disclaimer(true);
            var nationalID = ds.Tables[0].Rows[0]["Number"].ToString();
            var creditinfoId = ds.Tables[0].Rows[0]["CreditinfoId"].ToString();
            string name;
            string address;
            string city;
            string country;

            string url = string.Empty;
            string email = string.Empty;
            string postalCode = ds.Tables[0].Rows[0]["PostalCode"].ToString().Trim();

            string companyStatus = string.Empty;

            if (ds.Tables[0].Columns.IndexOf("CompanyState") != -1 && ds.Tables[0].Rows[0]["CompanyState"] != null && !string.IsNullOrEmpty(ds.Tables[0].Rows[0]["CompanyState"].ToString()))
                companyStatus = ds.Tables[0].Rows[0]["CompanyState"].ToString();
            
            string legalForm = string.Empty;
            if (ds.Tables[0].Columns.IndexOf("LegalForm") != -1 && ds.Tables[0].Rows[0]["LegalForm"] != null)
                legalForm = ds.Tables[0].Rows[0]["LegalForm"].ToString();
            

            var note = ds.Tables[0].Rows[0]["Note"].ToString();

            if (nativeCult)
            {
                name = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim() != "" ? ds.Tables[0].Rows[0]["NameNative"].ToString() : ds.Tables[0].Rows[0]["NameEN"].ToString();
                address = ds.Tables[0].Rows[0]["StreetNative"].ToString().Trim() != "" ? ds.Tables[0].Rows[0]["StreetNative"].ToString() : ds.Tables[0].Rows[0]["StreetEN"].ToString();
                city = ds.Tables[0].Rows[0]["CityNative"].ToString().Trim() != "" ? ds.Tables[0].Rows[0]["CityNative"].ToString() : ds.Tables[0].Rows[0]["CityEN"].ToString();
                country = ds.Tables[0].Rows[0]["CountryNative"].ToString().Trim() != "" ? ds.Tables[0].Rows[0]["CountryNative"].ToString() : ds.Tables[0].Rows[0]["CountryEN"].ToString();
            }
            else
            {
                name = ds.Tables[0].Rows[0]["NameEN"].ToString().Trim() != "" ? ds.Tables[0].Rows[0]["NameEN"].ToString() : ds.Tables[0].Rows[0]["NameNative"].ToString();
                address = ds.Tables[0].Rows[0]["StreetEN"].ToString().Trim() != "" ? ds.Tables[0].Rows[0]["StreetEN"].ToString() : ds.Tables[0].Rows[0]["StreetNative"].ToString();
                city = ds.Tables[0].Rows[0]["CityEN"].ToString().Trim() != "" ? ds.Tables[0].Rows[0]["CityEN"].ToString() : ds.Tables[0].Rows[0]["CityNative"].ToString();
                country = ds.Tables[0].Rows[0]["CountryEN"].ToString().Trim() != "" ? ds.Tables[0].Rows[0]["CountryEN"].ToString() : ds.Tables[0].Rows[0]["CountryNative"].ToString();
            }
            DisplayDetails(company, creditinfoId, nationalID, name, address, city, country, postalCode, note, companyStatus, legalForm);
        }

        private void DisplayWatchButton(bool company)
        {
            try
            {
                int userID = int.Parse(Session["UserLoginID"].ToString());
                if (
                    !userAdminFactory.HasUserAccessToProduct(
                         userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_Monitoring"))))
                {
                    lblSubjectIsOnMonitoring.Visible = false;
                    lbtnAddToMonitoring.Visible = false;
                }
                else
                {
                    if (IsAlreadyOnWatch(company))
                    {
                        lblSubjectIsOnMonitoring.Visible = true;
                        lbtnAddToMonitoring.Visible = false;
                    }
                    else
                    {
                        lblSubjectIsOnMonitoring.Visible = false;
                        lbtnAddToMonitoring.Visible = true;
                    }
                }
            }
            catch (NullReferenceException)
            {
                FormsAuthentication.SignOut();
                Response.AddHeader("Refresh", "0");
                Session.Abandon();
            }
        }

        private void lbtnBasicReport_Click(object sender, EventArgs e)
        {
            if (lblResultCreditinfoID.Text.Trim().Length > 0)
            {
                DisplayCompanyProfile(int.Parse(lblResultCreditinfoID.Text), lblResultNationalID.Text);
            }
            else
            {
                DisplayCompanyProfile(-1, lblResultNationalID.Text);
            }
        }

        private void lbtnCompanyReport_Click(object sender, EventArgs e) { DisplayCompanyReport(int.Parse(lblResultCreditinfoID.Text), lblResultNationalID.Text); }
        private void lbtnCreditinfoReport_Click(object sender, EventArgs e) { displayReport(ConfigurationSettings.AppSettings.Get("CreditInfoReportType")); }

        private void lbtnOrderReport_Click(object sender, EventArgs e)
        {
            OrderReport(
                rm.GetString("txtCreditInfoReport", ci),
                Convert.ToInt32(ConfigurationSettings.AppSettings.Get("CreditInfoReportTypeID")),
                true);
        }

        private void lbtnOrderCompanyReport_Click(object sender, EventArgs e)
        {
            OrderReport(
                rm.GetString("txtCompanyReport", ci),
                Convert.ToInt32(ConfigurationSettings.AppSettings.Get("CompanyReportTypeID")),
                false);
        }

        private void lbtnOrderFiscalUpdate_Click(object sender, EventArgs e)
        {
            OrderReport(
                rm.GetString("txtFinancialStatement", ci),
                Convert.ToInt32(ConfigurationSettings.AppSettings.Get("FinancialStatementsType")),
                true);
        }

        private void OrderReport(string name, int type, bool displayDeliveryTypes)
        {
            try
            {
                trDeliveryType.Visible = displayDeliveryTypes;
                lblOrderDeliveryTypeLabel.Visible = displayDeliveryTypes;
                lblOrderDispCompanyName.Text = lblResultName.Text;
                string nationalID = lblResultNationalID.Text;
                lblOrderDispCompanyNationalID.Text = nationalID;
                string userName = Session["UserLoginName"].ToString();

                lblReportOrder.Text = rm.GetString("txtReportOrder", ci)/* + " " + name*/;

                lblOrderProductID.Text = type.ToString();
                lblOrderProduct.Text = name;
                lblOrderDispUserName.Text = userName;

                auUsersBLLC auUser = userAdminFactory.GetSpecificUser(userName);
                if (auUser != null)
                {
                    string subscriberName;
                    if (nativeCult)
                    {
                        subscriberName = userAdminFactory.GetCreditInfoUserNativeName(auUser.SubscriberID);
                        if (subscriberName.Trim() == "")
                        {
                            subscriberName = userAdminFactory.GetCreditInfoUserENName(auUser.SubscriberID);
                        }
                        lblOrderDispSubscriber.Text = subscriberName;
                    }
                    else
                    {
                        subscriberName = userAdminFactory.GetCreditInfoUserENName(auUser.SubscriberID);
                        if (subscriberName.Trim() == "")
                        {
                            subscriberName = userAdminFactory.GetCreditInfoUserNativeName(auUser.SubscriberID);
                        }
                        lblOrderDispSubscriber.Text = subscriberName;
                    }
                }

                trOrderReport.Visible = true;
            }
            catch (NullReferenceException)
            {
                FormsAuthentication.SignOut();
                Response.AddHeader("Refresh", "0");
                Session.Abandon();
            }
        }

        private bool IsAlreadyOnWatch(bool isCompany1)
        {
            var myWatch = new WatchUniqueIDBLLC
                          {
                              CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]),
                              UserID = Convert.ToInt32(Session["UserLoginID"]),
                              WatchID = (-1),
                              UniqueID = lblResultNationalID.Text,
                              Sent = "False",
                              IsCompany = (isCompany1 ? "True" : "False"),
                              Created = DateTime.Now
                          };

            // UserID nota� til a� a�greina f�rslur � vakt � milli notenda... tekur vi� af CreditInfoID
            // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)

            // Athuga hvort skr�ning s� til fyrir...
            return creditWatchFactory.SearchUniqueWatch(myWatch);
        }

        private void lbtnAddToMonitoring_Click(object sender, EventArgs e)
        {
            cwFactory myFactory = new cwFactory();
            if (myFactory.CountWatch(Convert.ToInt32(Session["UserLoginID"])) >= Convert.ToInt32(Session["CreditWatch"]))
            {
                DisplayErrorMessage("Registration Limit Reached");
                return;
                //Server.Transfer("error.aspx?err=4");
            }

            if (IsAlreadyOnWatch(true /*rbtnlUserType.Items[1].Selected*/))
            {
                return;
            }

            var myWatch = new WatchUniqueIDBLLC
                          {
                              CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]),
                              UserID = Convert.ToInt32(Session["UserLoginID"]),
                              WatchID = (-1),
                              UniqueID = lblResultNationalID.Text,
                              Sent = "False",
                              IsCompany = "True",
                              Created = DateTime.Now
                          };

            // UserID nota� til a� a�greina f�rslur � vakt � milli notenda... tekur vi� af CreditInfoID
            // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)
            //if(rbtnlUserType.Items[1].Selected)
            //else
            //	myWatch.IsCompany = "False";

            if (!creditWatchFactory.TryToRegisterNewUniqueID(myWatch))
            {
                DisplayErrorMessage(
                    "An error occurred while registering to watchlist. Please notify the system administrator.");
            }
            else
            {
                DisplayMessage("Data subject added to monitoring");
                lbtnAddToMonitoring.Visible = false;
                lblSubjectIsOnMonitoring.Visible = true;
            }
        }

        private void lbtnDDD_Click(object sender, EventArgs e)
        {
            try
            {
                int ciid = int.Parse(lblResultCreditinfoID.Text);
                if (ciid != -1)
                {
                    Session["cwClaimSearchCreditInfoID"] = ciid;
                    //	Session["cwClaimSearchName"]=lblOrderDispCompanyName.Text;
                    Session["cwClaimSearchName"] = lblResultName.Text;
                    Session["cwClaimSearchAddress"] = lblResultAddress.Text;
                    Session["cwClaimSearchCity"] = lblResultCity.Text;
                    Response.Redirect("CreditWatch/cwClaimSearchDetails.aspx");
                }
            }
            catch (Exception exc)
            {
                Logger.WriteToLog("Error displaying DDD data for CIID " + lblResultCreditinfoID.Text, exc, true);
            }
        }

        private void ddlFS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFS.SelectedIndex > 0)
            {
                string afs_id = ddlFS.SelectedValue;
                try
                {
                    try
                    {
                        int ciid = int.Parse(lblResultCreditinfoID.Text); //lblResultNationalID.Text);
                        if (ciid > 0)
                        {
                            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
                            int userID = int.Parse(Session["UserLoginID"].ToString());
                            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                            var generator = new ReportGenerator(
                                rm, culture, userCreditInfoID, userID, ipAddress)
                                            {
                                                ReportType =
                                                    Convert.ToInt32(
                                                    ConfigurationSettings.AppSettings.Get("FinancialStatementsType"))
                                            };
                            var transformer = new XslTransform();
                            transformer.Load(
                                Server.MapPath(Request.ApplicationPath + "/CR/xsl/FOFinancialStatementDisplay.xslt"));
                            var sw = new StringWriter();
                            var doc = generator.getFinancialReport(ciid, afs_id);
                            if (doc != null)
                            {
                                transformer.Transform(doc, null, sw, null);
                                tdFinancialStatement.Visible = true;
                                tdFinancialStatement.InnerHtml = sw.ToString();
                            }
                            else
                            {
                                Logger.WriteToLog(
                                    "Error displaying financial data for CIID " + lblResultCreditinfoID.Text +
                                    " xmlDocument is null",
                                    true);
                                DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
                            }
                        }
                    }
                    catch (NullReferenceException)
                    {
                        FormsAuthentication.SignOut();
                        Response.AddHeader("Refresh", "0");
                        Session.Abandon();
                    }
                }
                catch (Exception exc)
                {
                    Logger.WriteToLog(
                        "Error displaying financial data for CIID " + lblResultCreditinfoID.Text, exc, true);
                    DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
                }
            }
        }

        private void DisplayErrorMessage(String errorMessage)
        {
            lblMessage.Text = errorMessage;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Visible = true;
        }

        private void DisplayMessage(String message)
        {
            lblMessage.Text = message;
            lblMessage.ForeColor = Color.Blue;
            lblMessage.Visible = true;
        }

        private void lbtnPaymentBehaviour_Click(object sender, EventArgs e)
        {
            try
            {
                int ciid = int.Parse(lblResultCreditinfoID.Text);
                if (ciid > 0)
                {
                    CompanyNaceCodeBLLC nace = cpiFactory.GetCompanyNaceCode(ciid);
                    if (nace != null)
                    {
                        Control c = LoadControl("new_user_controls/PaymentBehaviourControl.ascx");
                        ((PaymentBehaviourControl)c).generateChart(ciid, nace);
                        tdPaymentBahaviour.Controls.Add(c);
                        tdPaymentBahaviour.Visible = true;
                        logUsage(ciid.ToString(), CigConfig.Configure("lookupsettings.PaymentBehaviorID"));
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.WriteToLog(
                    "Error displaying payment behaviour data for CIID " + lblResultCreditinfoID.Text, exc, true);
            }
        }

        protected int logUsage(string ciid, string usageType)
        {
            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            if (usageType != null)
            {
                return crFactory.InsertInto_np_Usage(userCreditInfoID, int.Parse(usageType), ciid, 1, ipAddress, userID);
            }
            return 0;
        }

        private void lbtnTAFReport_Click(object sender, EventArgs e)
        {
            int userID = int.Parse(Session["UserLoginID"].ToString());

            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_BasicTafReport"))))
            {
                if (lblResultCreditinfoID.Text.Length > 0)
                {
                    DisplayBasicTAFReport(int.Parse(lblResultCreditinfoID.Text), lblResultNationalID.Text, isCompany);
                }
                else
                {
                    DisplayBasicTAFReport(-1, lblResultNationalID.Text, isCompany);
                }
            }
            else
            {
                //if(lblResultNationalID.Text.Trim()!="")
                //	{
                if (lblResultCreditinfoID.Text.Length > 0)
                {
                    DisplayTAFReport(int.Parse(lblResultCreditinfoID.Text), lblResultNationalID.Text, isCompany);
                }
                else
                {
                    DisplayTAFReport(-1, lblResultNationalID.Text, isCompany);
                }
                //}
            }
        }

        private void DisplayCompanyProfile(int ciid, string nationalID)
        {
            try
            {
                //if(nationalID!="")
                //{
                int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
                int userID = int.Parse(Session["UserLoginID"].ToString());
                var ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                var generator = new MTReportGenerator(rm, culture, userCreditInfoID, userID, ipAddress)
                                {
                                    ReportType = ReportGenerator.companyProfileReportID
                                };
                var transformer = new XslTransform();
                transformer.Load(Server.MapPath(Request.ApplicationPath + "/CR/xsl/mt/MTReports.xslt"));
                var sw = new StringWriter();
                var doc = generator.GetMTCompanyProfile(ciid, nationalID);
                if (doc != null)
                {
                    //trAdditionalInformation.Visible=false;
                    transformer.Transform(doc, null, sw, null);
                    trReportDisplay.Visible = true;
                    tdReportDisplay.InnerHtml = sw.ToString();
                }
                else
                {
                    Logger.WriteToLog(
                        "Error displaying company profile report for national id  " + lblResultNationalID.Text +
                        " xmlDocument is null",
                        true);
                    DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
                }
                //}
            }
            catch (Exception exc)
            {
                Logger.WriteToLog(
                    "Error displaying company profile report for national id  " + lblResultNationalID.Text, exc, true);
                DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
            }
        }

        private static String MaxDateFromDataSet(/*DataSet ds*/ string FinancialYear)
        {
            try
            {
                //DateTime created = ds.Tables[0].Rows[0]["Created"].ToString() != "" ? Convert.ToDateTime(ds.Tables[0].Rows[0]["Created"].ToString()) : DateTime.MinValue;
                //DateTime modified = ds.Tables[0].Rows[0]["Modified"].ToString() != "" ? Convert.ToDateTime(ds.Tables[0].Rows[0]["Modified"].ToString()) : DateTime.MinValue;
                //if (created == DateTime.MinValue && modified == DateTime.MinValue)
                //{
                //    return "";
                //}

                //if (created > modified)
                //{
                //    return "(Last Accounts Available: " + created.ToShortDateString() + ")";
                //}
                return "(Last Accounts Available: " + /*modified.ToShortDateString()*/FinancialYear + ")";
            }
            catch (Exception)
            {
                return "";
            }
        }

        private void DisplayCompanyReport(int ciid, string nationalID)
        {
            try
            {
                try
                {
                    var reportYears = cpiFactory.GetAvailableReportYearsAsDataSet(ciid);
                    //if(nationalID!="")
                    //{					

                    int fsireports;
                    try
                    {
                        fsireports = int.Parse(CigConfig.Configure("lookupsettings.FoSearch.FsiLimit"));
                    }
                    catch
                    {
                        fsireports = 3;
                    }
                    string afsIds = GetAFSIDs(reportYears, fsireports);

                    int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
                    int userID = int.Parse(Session["UserLoginID"].ToString());

                    string ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                    var generator = new MTReportGenerator(
                        rm, culture, userCreditInfoID, userID, ipAddress) { ReportType = ReportGenerator.companyReportID };
                    var transformer = new XslTransform();
                    transformer.Load(Server.MapPath(Request.ApplicationPath + "/CR/xsl/mt/MTReports.xslt"));
                    var sw = new StringWriter();
                    var doc = generator.GetMTCompanyReport(ciid, nationalID, afsIds);
                    if (doc != null)
                    {
                        //trAdditionalInformation.Visible=false;
                        //doc.Save(Server.MapPath(Request.ApplicationPath+"/test-report.xml"));
                        transformer.Transform(doc, null, sw, null);
                        trReportDisplay.Visible = true;
                        tdReportDisplay.InnerHtml = sw.ToString();
                    }
                    else
                    {
                        Logger.WriteToLog(
                            "Error displaying company report for national id  " + lblResultNationalID.Text +
                            " xmlDocument is null",
                            true);
                        DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
                    }
                    //}
                }
                catch (NullReferenceException)
                {
                    FormsAuthentication.SignOut();
                    Response.AddHeader("Refresh", "0");
                    Session.Abandon();
                }
            }
            catch (Exception exc)
            {
                Logger.WriteToLog(
                    "Error displaying company profile report for national id  " + lblResultNationalID.Text, exc, true);
                DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
            }
        }

        protected void DisplayBasicTAFReport(int ciid, string nationalID, bool company)
        {
            try
            {
                int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
                int userID = int.Parse(Session["UserLoginID"].ToString());
                string ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                var generator = new MTReportGenerator(rm, culture, userCreditInfoID, userID, ipAddress)
                                {
                                    ReportType =
                                        Convert.ToInt32(ConfigurationSettings.AppSettings.Get("BasicTAFReportType"))
                                };
                var transformer = new XslTransform();
                transformer.Load(Server.MapPath(Request.ApplicationPath + "/CR/xsl/mt/MTReports.xslt"));
                var sw = new StringWriter();
                XmlDocument doc = company ? generator.GetMTCompanyBasicTAFReport(ciid, nationalID) : generator.GetMTIndividualBasicTAFReport(ciid, nationalID);
                if (doc != null)
                {
                    //doc.Save(Server.MapPath(Request.ApplicationPath+"/test-basic.xml"));
                    transformer.Transform(doc, null, sw, null);
                    trReportDisplay.Visible = true;
                    tdReportDisplay.InnerHtml = sw.ToString();
                }
                else
                {
                    Logger.WriteToLog(
                        "Error displaying basic taf report for national id  " + lblResultNationalID.Text +
                        " xmlDocument is null",
                        true);
                    DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
                }
                //}
            }
            catch (Exception exc)
            {
                Logger.WriteToLog(
                    "Error displaying company profile report for national id  " + lblResultNationalID.Text, exc, true);
                DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
            }
        }

        protected void DisplayTAFReport(int ciid, string nationalID, bool company)
        {
            try
            {
                //if(nationalID!="")
                //{
                var userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
                var userID = int.Parse(Session["UserLoginID"].ToString());
                var ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                var generator = new MTReportGenerator(rm, culture, userCreditInfoID, userID, ipAddress)
                                {
                                    ReportType = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("TAFReportType"))
                                };
                var transformer = new XslTransform();
                transformer.Load(Server.MapPath(Request.ApplicationPath + "/CR/xsl/mt/MTReports.xslt"));
                var sw = new StringWriter();
                var doc = company ? generator.GetMTCompanyTAFReport(ciid, nationalID) : generator.GetMTIndividualTAFReport(ciid, nationalID);
                if (doc != null)
                {
                    transformer.Transform(doc, null, sw, null);
                    trReportDisplay.Visible = true;
                    tdReportDisplay.InnerHtml = sw.ToString();
                }
                else
                {
                    Logger.WriteToLog(
                        "Error displaying company profile report for national id  " + lblResultNationalID.Text +
                        " xmlDocument is null",
                        true);
                    DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
                }
                //}
            }
            catch (Exception exc)
            {
                Logger.WriteToLog(
                    "Error displaying company profile report for national id  " + lblResultNationalID.Text, exc, true);
                DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
            }
        }

        private void dtgrIndividual_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
            {
                return;
            }
            ((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect", ci);
            if (nativeCult)
            {
                // Name			
                e.Item.Cells[2].Text = e.Item.Cells[3].Text;
                if (e.Item.Cells[2].Text == "&nbsp;")
                {
                    e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                }
                // Address		
                ((LinkButton)e.Item.Cells[5].Controls[0]).Text = e.Item.Cells[6].Text;
                if (((LinkButton)e.Item.Cells[5].Controls[0]).Text == "&nbsp;")
                {
                    ((LinkButton)e.Item.Cells[5].Controls[0]).Text = e.Item.Cells[7].Text;
                }
                /*e.Item.Cells[5].Text = e.Item.Cells[6].Text; 
					if(e.Item.Cells[5].Text == "&nbsp;") 
						e.Item.Cells[5].Text = e.Item.Cells[7].Text;*/
                // City			
                e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                if (e.Item.Cells[9].Text == "&nbsp;")
                {
                    e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                }
            }
            else
            {
                //Name
                e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                if (e.Item.Cells[2].Text == "&nbsp;")
                {
                    e.Item.Cells[2].Text = e.Item.Cells[3].Text;
                }
                //Address
                ((LinkButton)e.Item.Cells[5].Controls[0]).Text = e.Item.Cells[7].Text;
                if (((LinkButton)e.Item.Cells[5].Controls[0]).Text == "&nbsp;")
                {
                    ((LinkButton)e.Item.Cells[5].Controls[0]).Text = e.Item.Cells[6].Text;
                }
                /*e.Item.Cells[5].Text = e.Item.Cells[7].Text;
					if(e.Item.Cells[5].Text == "&nbsp;") 
						e.Item.Cells[5].Text = e.Item.Cells[6].Text;*/
                //City
                e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                if (e.Item.Cells[9].Text == "&nbsp;")
                {
                    e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                }
            }
        }

        private void dtgrCompanys_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (!e.CommandName.Equals("Select"))
            {
                return;
            }
            string nationalID = e.Item.Cells[1].Text;
            if (!string.IsNullOrEmpty(nationalID))
            {
                Search(nationalID, "", "", "", false, false, false,string.Empty,false);
            }
        }

        private void dtgrIndividual_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string nationalID = e.Item.Cells[1].Text;
                if (!string.IsNullOrEmpty(nationalID))
                {
                    Search(nationalID, "", "", "", false, false, false,string.Empty,false);
                }
            }
            else if (e.CommandName.Equals("Address"))
            {
                string addr = ((LinkButton)e.Item.Cells[5].Controls[0]).Text;

                if (!string.IsNullOrEmpty(addr))
                {
                    string city = "";
                    if (!e.Item.Cells[9].Text.Equals("&nbsp;"))
                    {
                        city = e.Item.Cells[9].Text;
                    }
                    Search("", "", addr, city, false, false, true,string.Empty,false);
                }
            }
        }

        private void lblResultAddress_Click(object sender, EventArgs e)
        {
            if (lblResultAddress.Text == "")
            {
                return;
            }
            string city = "";
            if (lblResultCity.Text != "")
            {
                city = lblResultCity.Text;
            }
            Search("", "", lblResultAddress.Text, city, false, false, true,string.Empty,false);
        }

        // For the Vat individual display:
        protected string TransformText(object value)
        {
           return (value as string) == "Active" ? "Valid" : "Invalid";
        }

        protected string GetStyleForVat(object value)
        {
            var result = (value as string) == "Active" ? "color:green;" : "color:red;";
            result += "float: right;";
            result += "font-weight: bold;";
            return result;
        }


        #region: NotUsed

        /*private void Search(string nationalID, string name)
		{		
			HideCompanySearchGrid();
			UserAdmin.BLL.uaFactory uaFact = new UserAdmin.BLL.uaFactory();
			DataSet dsCompany;

			UserAdmin.BLL.CIUsers.Company comp = new UserAdmin.BLL.CIUsers.Company();
			comp.NameNative = name;		
			if(nationalID.Trim() != "")
			{
				UserAdmin.BLL.CIUsers.IDNumber idn = new UserAdmin.BLL.CIUsers.IDNumber();
				idn.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"].Trim());
				idn.Number = nationalID;
				comp.IDNumbers = new ArrayList();
				comp.IDNumbers.Add(idn);
			}

			if(CigConfig.Configure("lookupsettings.UserAdminNationalRegistry"].ToLower() == "true")
			{
				dsCompany = uaFact.FindCompanyInNationalAndCreditInfo(comp);
			}
			else
			{
				dsCompany = uaFact.FindCompany(comp);
			}

			if(dsCompany.Tables.Count<=0||dsCompany.Tables[0].Rows.Count<=0)
			{
				DisplayErrorMessage(rm.GetString("txtNoRecordsFound", ci));
				return;
			}

			//Check if only one company found and no individual - then display 
			if(dsCompany.Tables.Count>0&&dsCompany.Tables[0].Rows.Count==1)
			{
				DisplayDetailsFromDataSet(true, dsCompany);
				return;
			}

			if(dsCompany.Tables.Count>0&&dsCompany.Tables[0].Rows.Count>0)
			{
				dtgrCompanys.SelectedIndex=-1;
				dtgrCompanys.DataSource = dsCompany;
				dtgrCompanys.DataBind();
				DisplayCompanySearchGrid();
			}
		}*/
        /*	private void GetCompanyInfoFromNameOrAddress(string name, string address)
			{
				UserAdmin.BLL.CIUsers.Company company = new UserAdmin.BLL.CIUsers.Company();
				company.NameNative = name;
				UserAdmin.BLL.CIUsers.Address addr = new Address();
				addr.StreetNative = address;
				company.Address = new ArrayList();
				company.Address.Add(addr);
				DataSet ds;
				if(CigConfig.Configure("lookupsettings.FOCompanyNationalRegistry"] == "True")
					ds = userAdminFactory.FindCompanyInNationalAndCreditInfo(company);
				else
					ds = userAdminFactory.FindCompany(company);
				//now we have the company 
				if(ds==null||ds.Tables.Count<1||ds.Tables[0].Rows.Count<1)
				{
					//if null or no row display error
					CompanyNotFound();

				}
				else if(ds.Tables[0].Rows.Count==1)
				{
					//If we have the national id - then use it to display correct data
					string natId = ds.Tables[0].Rows[0]["Number"].ToString();
					if(natId.Trim() != "")
					{
						GetCompanyInfoFromNationalID(natId);
					}
					else
					{

						//If only one company - display details about that company
						trCompanyInfoRow.Visible=true;
						tdFinancialStatement.Visible=false;
						trAdditionalInformation.Visible=true;
						trCompanySearchGrid.Visible=false;
						ReportCompanyBLLC report = new ReportCompanyBLLC();
						report.NameNative = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
						report.NameEN = ds.Tables[0].Rows[0]["NameEN"].ToString().Trim();
						Address myAddress = new Address();
						myAddress.StreetNative = ds.Tables[0].Rows[0]["StreetNative"].ToString().Trim();
						myAddress.StreetEN = ds.Tables[0].Rows[0]["StreetEN"].ToString().Trim();
						myAddress.PostalCode = ds.Tables[0].Rows[0]["PostalCode"].ToString();

						report.Address = new ArrayList();
						report.Address.Add(myAddress);

						report.UniqueID = ds.Tables[0].Rows[0]["Number"].ToString();
						if(ds.Tables[0].Rows[0]["CreditInfoID"]!=null)
							report.CompanyCIID = int.Parse(ds.Tables[0].Rows[0]["CreditInfoID"].ToString());
						if(report.CompanyCIID<0)
							report.CompanyCIID = cpiFactory.GetCIIDByNationalID(report.UniqueID);
						DisplayCompanyInfo(report);
					}
				}
				else
				{
					//Display list in grid
					DataView view = ds.Tables[0].DefaultView;
					view.Sort = "NameNative";
					dtgrCompanys.DataSource = view;
					dtgrCompanys.DataBind();
					lblCompanyGridHeader.Text= rm.GetString("txtCompanies", ci) +": "+ds.Tables[0].Rows.Count;
					trCompanyInfoRow.Visible=false;
					tdFinancialStatement.Visible=false;
					trAdditionalInformation.Visible=false;
					trCompanySearchGrid.Visible=true;
					trOrderReceipe.Visible=false;
				}
			}*/
        /// <summary>
        /// Finds company with given national id and displays 
        /// informations about that company
        /// </summary>
        /// <param name="nationalID">The national id for the company</param>
        /*private void GetCompanyInfoFromNationalID(string nationalID)
		{
			ReportCompanyBLLC report = cpiFactory.GetCompanyReport(nationalID, -1, true);
			if(report==null||report.UniqueID==null)
			{
				//Report not found - try to find CIID for the national ID
				int ciid = cpiFactory.GetCIIDByNationalID(nationalID);
				if(ciid!=-1)
				{
					//OK - CIID found - try to find the company in user admin
					UserAdmin.BLL.CIUsers.Company company = userAdminFactory.GetCompany(ciid);
					if(company!=null&&company.CreditInfoID>-1)
					{
						//If company found in user admin - display that
						company.NationalID = nationalID;
						DisplayCompanyInfo(company);
					}
					else
					{
						//Strange not to find the company but only the CIID
						CompanyNotFound();
					}
				}
				else
				{
					if(CigConfig.Configure("lookupsettings.FOCompanyNationalRegistry"] == "True")
					{
						//Search in national registry
						UserAdmin.BLL.CIUsers.Company company = userAdminFactory.GetCompanyFromNationalRegistry(nationalID);
						if(company!=null&&company.NationalID!=null)
						{
							//If company found in user admin - display that
							DisplayCompanyInfo(company);
						}
						else
						{
							CompanyNotFound();
						}
					}
					else
					{
						CompanyNotFound();
					}
				}
			}
			else
			{
				DisplayCompanyInfo(report);
			}
		}*/
        /*private void GetIndividualInfoFromNameOrAddress(string name, string surName, string address)
		{
			UserAdmin.BLL.Debtor debtor = new UserAdmin.BLL.Debtor();
			debtor.FirstName = name;
			debtor.SurName = surName;
			UserAdmin.BLL.CIUsers.Address addr = new Address();
			addr.StreetNative = address;
			debtor.Address = new ArrayList();
			debtor.Address.Add(addr);
			DataSet ds;
			if(CigConfig.Configure("lookupsettings.FOIndividualNationalRegistry"] == "True")
				ds = userAdminFactory.FindCustomerInNationalAndCreditInfo(debtor);
			else
				ds = userAdminFactory.FindCustomer(debtor);
			//now we have the individual 
			if(ds==null||ds.Tables.Count<1||ds.Tables[0].Rows.Count<1)
			{
				//if null or no row display error
				IndividualNotFound();

			}
			else if(ds.Tables[0].Rows.Count==1)
			{
				//If only one individual - display details about that individual
				trCompanyInfoRow.Visible=true;
				tdFinancialStatement.Visible=false;
				trAdditionalInformation.Visible=true;
				trCompanySearchGrid.Visible=false;
				Indivitual individual = new Indivitual();
				individual.FirstNameNative = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
				individual.FirstNameEN = ds.Tables[0].Rows[0]["NameEN"].ToString().Trim();
				Address myAddress = new Address();
				myAddress.StreetNative = ds.Tables[0].Rows[0]["StreetNative"].ToString().Trim();
				myAddress.StreetEN = ds.Tables[0].Rows[0]["StreetEN"].ToString().Trim();
				myAddress.PostalCode = ds.Tables[0].Rows[0]["PostalCode"].ToString();

				individual.Address = new ArrayList();
				individual.Address.Add(myAddress);


				individual.NationalID = ds.Tables[0].Rows[0]["Number"].ToString();
				individual.CreditInfoID = cpiFactory.GetCIIDByNationalID(individual.NationalID);
				DisplayIndividualInfo(individual);
			}
			else
			{
				//Display list in grid
				dtgrCompanys.DataSource = ds;
				dtgrCompanys.DataBind();
				trCompanyInfoRow.Visible=false;
				tdFinancialStatement.Visible=false;
				trAdditionalInformation.Visible=false;
				trCompanySearchGrid.Visible=true;
				trOrderReceipe.Visible=false;
			}
		}*/
        /// <summary>
        /// Finds company with given national id and displays 
        /// informations about that company
        /// </summary>
        /// <param name="nationalID">The national id for the company</param>
        /*private void GetIndividualInfoFromNationalID(string nationalID)
		{
			UserAdmin.BLL.CIUsers.Indivitual individual = cpiFactory.GetIndivitualByNationalID(nationalID);
			if(individual != null && individual.NationalID != null)
			{
				//Report not found - try to find CIID for the national ID
				int ciid = cpiFactory.GetCIIDByNationalID(nationalID);
				if(ciid!=-1)
				{
					//OK - CIID found - try to find the individual in user admin
					individual = userAdminFactory.GetIndivitual(ciid);
					if(individual!=null&&individual.CreditInfoID>-1)
					{
						//If individual found in user admin - display that
						individual.NationalID = nationalID;
						DisplayIndividualInfo(individual);
					}
					else
					{
						//Strange not to find the individual but only the CIID
						IndividualNotFound();
					}
				}
				else
				{
					if(CigConfig.Configure("lookupsettings.FOIndividualNationalRegistry"] == "True")
					{
						//Search in national registry
						individual = userAdminFactory.GetIndivitualFromNationalRegistry(nationalID);
						if(individual!=null&&individual.NationalID!=null)
						{
							//If individual found in user admin - display that
							DisplayIndividualInfo(individual);
						}
						else
						{
							//individual not found
							IndividualNotFound();
						}
					}
					else
					{
						//individual not found
						IndividualNotFound();
					}
				}
			}
			else
			{
				DisplayIndividualInfo(individual);
			}
		}*/
        /*private void IndividualNotFound()
		{
			DisplayErrorMessage(rm.GetString("txtNoIndividualFound",ci));
			trCompanyInfoRow.Visible=false;
			tdFinancialStatement.Visible=false;
			trAdditionalInformation.Visible=false;
			trCompanySearchGrid.Visible=false;
			trOrderReceipe.Visible = false;
		}*/
        /*private void CompanyNotFound()
		{
			DisplayErrorMessage(rm.GetString("txtNoCompanyFound",ci));
			trCompanyInfoRow.Visible=false;
			tdFinancialStatement.Visible=false;
			trAdditionalInformation.Visible=false;
			trCompanySearchGrid.Visible=false;
			trCompanySearchGrid.Visible=false;
		}*/
        /*private void DisplayIndividualInfo(UserAdmin.BLL.CIUsers.Indivitual individual)
		{
			trCompanySearchGrid.Visible=false;
			trCompanyInfoRow.Visible=true;
			tdFinancialStatement.Visible=false;
			trAdditionalInformation.Visible=true;
			trOrderReceipe.Visible=false;
			this.lblResultNationalID.Text = individual.NationalID;
			this.lblResultCreditinfoID.Text = individual.CreditInfoID.ToString();
			if(IsAlreadyOnWatch(individual.NationalID, false))
			{
				lblSubjectIsOnMonitoring.Visible=true;
				lbtnAddToMonitoring.Visible=false;
			}
			else
			{
				lblSubjectIsOnMonitoring.Visible=false;
				lbtnAddToMonitoring.Visible=true;
			}
			if(nativeCult)
			{
				if(individual.FirstNameNative.Trim()!="")
					this.lblResultName.Text = individual.FirstNameNative+" "+individual.SurNameNative;	
				else
					this.lblResultName.Text = individual.FirstNameEN+" "+individual.SurNameEN;
				if(individual.Address!=null&&individual.Address.Count>0)
				{
					UserAdmin.BLL.CIUsers.Address address = (UserAdmin.BLL.CIUsers.Address)individual.Address[0];
					if(address!=null)
					{
						if(address.StreetNumber>0)
							lblResultAddress.Text = address.StreetNative+" "+address.StreetNumber;
						else
							lblResultAddress.Text = address.StreetNative;
						if(CigConfig.Configure("lookupsettings.currentVersion").ToString().Equals("czech"))
							lblResultCity.Text = address.PostalCode +"  "+address.CityNameNative;
						else
							lblResultCity.Text = address.PostalCode+"  "+address.CityNameNative;
					}
				}
				else
				{
					if(individual.SingleAddressNative.Trim()!="")
						lblResultAddress.Text = individual.SingleAddressNative;
					else
						lblResultAddress.Text = individual.SingleAddressEN;
				}
			}
			else
			{
				if(individual.FirstNameEN.Trim()!="")
					this.lblResultName.Text = individual.FirstNameEN+" "+individual.SurNameEN;
				else
					this.lblResultName.Text = individual.FirstNameNative+" "+individual.SurNameNative;	
				if(individual.Address!=null&&individual.Address.Count>0)
				{
					UserAdmin.BLL.CIUsers.Address address = (UserAdmin.BLL.CIUsers.Address)individual.Address[0];
					if(address!=null)
					{
						if(address.StreetNumber>0)
							lblResultAddress.Text = address.StreetEN+" "+address.StreetNumber;
						else
							lblResultAddress.Text = address.StreetEN;
						if(CigConfig.Configure("lookupsettings.currentVersion").ToString().Equals("czech"))
							lblResultCity.Text = formatPostalCodeForCzech(address.PostalCode) +"  "+address.CityNameEN;
						else
							lblResultCity.Text = address.PostalCode+"  "+address.CityNameEN;
					}
				}
				else
				{
					if(individual.SingleAddressEN.Trim()!="")
						lblResultAddress.Text = individual.SingleAddressEN;
					else
						lblResultAddress.Text = individual.SingleAddressNative;

				}
			}
			SetReportButtonVisability(false);
		}*/
        /*private void DisplayCompanyInfo(UserAdmin.BLL.CIUsers.Company company)
		{
			trCompanySearchGrid.Visible=false;
			trCompanyInfoRow.Visible=true;
			tdFinancialStatement.Visible=false;
			trAdditionalInformation.Visible=true;
			trOrderReceipe.Visible=false;
			this.lblResultNationalID.Text = company.NationalID;
			this.lblResultCreditinfoID.Text = company.CreditInfoID.ToString();
			if(IsAlreadyOnWatch(company.NationalID, true))
			{
				lblSubjectIsOnMonitoring.Visible=true;
				lbtnAddToMonitoring.Visible=false;
			}
			else
			{
				lblSubjectIsOnMonitoring.Visible=false;
				lbtnAddToMonitoring.Visible=true;
			}
			if(nativeCult)
			{
				this.lblResultName.Text = company.NameNative;				
				if(company.Address!=null&&company.Address.Count>0)
				{
					UserAdmin.BLL.CIUsers.Address address = (UserAdmin.BLL.CIUsers.Address)company.Address[0];
					if(address!=null)
					{
						if(address.StreetNumber>0)
							lblResultAddress.Text = address.StreetNative+" "+address.StreetNumber;
						else
							lblResultAddress.Text = address.StreetNative;
						lblResultCity.Text = address.PostalCode+"  "+address.CityNameNative;
					}
				}
			}
			else
			{
				this.lblResultName.Text = company.NameEN;
				if(company.Address!=null&&company.Address.Count>0)
				{
					UserAdmin.BLL.CIUsers.Address address = (UserAdmin.BLL.CIUsers.Address)company.Address[0];
					if(address!=null)
					{
						if(address.StreetNumber>0)
							lblResultAddress.Text = address.StreetEN+" "+address.StreetNumber;
						else
							lblResultAddress.Text = address.StreetEN;
						lblResultCity.Text = address.PostalCode+"  "+address.CityNameEN;
					}
				}
			}
			if(company.CreditInfoID>0)
			{
				FillFSDropDownBox(company.CreditInfoID);
			}

			SetReportButtonVisability(false);
		}*/
        /*private void DisplayCompanyInfo(ReportCompanyBLLC report)
		{
			trCompanySearchGrid.Visible=false;
			trAdditionalInformation.Visible=true;
			trCompanyInfoRow.Visible=true;
			tdFinancialStatement.Visible=false;
			trOrderReceipe.Visible=false;
			this.lblResultNationalID.Text = report.UniqueID;
			this.lblResultCreditinfoID.Text = report.CompanyCIID.ToString();

			if(IsAlreadyOnWatch(report.UniqueID, true))
			{
				lblSubjectIsOnMonitoring.Visible=true;
				lbtnAddToMonitoring.Visible=false;
			}
			else
			{
				lblSubjectIsOnMonitoring.Visible=false;
				lbtnAddToMonitoring.Visible=true;
			}
			if(nativeCult)
			{
				this.lblResultName.Text = report.NameNative;

				if(report.Address.Count > 0)
				{
					this.lblResultAddress.Text = ((Address)report.Address[0]).StreetNative;
					if(CigConfig.Configure("lookupsettings.currentVersion").ToString().Equals("czech"))
						this.lblResultCity.Text = formatPostalCodeForCzech(((Address)report.Address[0]).PostalCode) + "  "+((Address)report.Address[0]).CityNameNative;
					else
						this.lblResultCity.Text = ((Address)report.Address[0]).PostalCode+"  "+((Address)report.Address[0]).CityNameNative;
				}
			}
			else
			{
				this.lblResultName.Text = report.NameEN;

				if(report.Address.Count > 0)
				{
					this.lblResultAddress.Text = ((Address)report.Address[0]).StreetEN;
					if(CigConfig.Configure("lookupsettings.currentVersion").ToString().Equals("czech"))
						this.lblResultCity.Text = formatPostalCodeForCzech(((Address)report.Address[0]).PostalCode) + "  "+((Address)report.Address[0]).CityNameEN;
					else
						this.lblResultCity.Text = ((Address)report.Address[0]).PostalCode+"  "+((Address)report.Address[0]).CityNameEN;
				}
			}

			if(report.CompanyCIID<0)
			{
				SetReportButtonVisability(false);
			}
			else
			{
				//First check if report is ready
				if(report.StatusID==int.Parse(CigConfig.Configure("lookupsettings.cpiReportStatusReady"]))
				{
					if(CheckIfReportIsExpired(report.CompanyCIID))
					{
						DisplayErrorMessage(rm.GetString("txtReportHasExpired", ci));
						SetReportButtonVisability(false);
					}
					else
						SetReportButtonVisability(true);
				}
				else
					SetReportButtonVisability(false);
				FillFSDropDownBox(report.CompanyCIID);
			}
		}*/
        /*private string GetNaceCodeString(int ciid)
		{
			string naceString="";
			CompanyNaceCodeBLLC nace = cpiFactory.GetCompanyNaceCode(ciid);
			if(nace!=null)
			{
				naceString = nace.NaceCode+" ";
				if(nativeCult)
				{
					if(nace.DescriptionNative!=null&&nace.DescriptionNative!="")
						naceString+=nace.DescriptionNative;
					else
						naceString+=nace.DescriptionEN;
				}
				else
				{
					if(nace.DescriptionEN!=null&&nace.DescriptionEN!="")
						naceString+=nace.DescriptionEN;
					else
						naceString+=nace.DescriptionNative;
				}
				naceString+=", "+rm.GetString("txtCompaniesInNaceCode", ci)+": "+nace.NumberOfCompaniesInNaceCode;
			}
			return naceString;
		}*/

        #endregion

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.lbtnNatIdTAFReport.Click += new System.EventHandler(this.lbtnTAFReport_Click);
            this.lbtnAddToMonitoring.Click += new System.EventHandler(this.lbtnAddToMonitoring_Click);
            this.lblResultAddress.Click += new System.EventHandler(this.lblResultAddress_Click);
            this.lbtnTAFReport.Click += new System.EventHandler(this.lbtnTAFReport_Click);
            this.lbtnBasicReport.Click += new System.EventHandler(this.lbtnBasicReport_Click);
            this.lbtnCompanyReport.Click += new System.EventHandler(this.lbtnCompanyReport_Click);
            this.lbtnCreditinfoReport.Click += new System.EventHandler(this.lbtnCreditinfoReport_Click);
            this.lbtnOrderCompanyReport.Click += new System.EventHandler(this.lbtnOrderCompanyReport_Click);
            this.lbtnOrderReport.Click += new System.EventHandler(this.lbtnOrderReport_Click);
            this.ddlFS.SelectedIndexChanged += new System.EventHandler(this.ddlFS_SelectedIndexChanged);
            this.lbtnOrderFiscalUpdate.Click += new System.EventHandler(this.lbtnOrderFiscalUpdate_Click);
            this.lbtnDDD.Click += new System.EventHandler(this.lbtnDDD_Click);
            this.lbtnPaymentBehaviour.Click += new System.EventHandler(this.lbtnPaymentBehaviour_Click);
            this.btnOrderReport.Click += new System.EventHandler(this.btnOrderReport_Click);
            this.dtgrIndividual.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrIndividual_ItemCommand);
            this.dtgrIndividual.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrIndividual_ItemDataBound);
            this.dtgrCompanys.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrCompanys_ItemCommand);
            this.dtgrCompanys.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrCompanys_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}

#pragma warning restore 618,612
