#region

using System.Collections;
using Cig.Framework.Data.Common.Arguments.DbArguments;
using Cig.Framework.Data.DataAccess;
using NHibernate.Expression;
using ValueObjects.General;

#endregion

namespace DataAccess.Persistant.General {
    /// <summary>
    /// Summary description for SearchProxy.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class SearchProxy : PersistenceManager, ISearchProxy {
        #region ISearchProxy Members

        /// <summary>
        /// Search for individuals based on firstname and surname
        /// </summary>
        /// <param name="firstName">Firstname to search</param>
        /// <param name="surName">Surname to search</param>
        /// <returns>Retrieved list of individuals</returns>
        public IList FindIndividual(string firstName, string surName) { return FindIndividual(firstName, surName, null, null); }

        /// <summary>
        /// Search for individuals based on firstname, surname, address native and address En
        /// </summary>
        /// <param name="firstName">Firstname to search</param>
        /// <param name="surName">Surname to search</param>
        /// <param name="addressNative">Address native to search</param>
        /// <param name="addressEN">Address en to search</param>
        /// <returns>Retrieved list of individuals</returns>
        public IList FindIndividual(string firstName, string surName, string addressNative, string addressEN) {
            IList ciids = FindCIIDByName(firstName, surName);
            if (ciids == null || ciids.Count == 0) {
                return null;
            }
            int[] arr = new int[ciids.Count];
            for (int i = 0; i < ciids.Count; i++) {
                arr[i] = ((Word2CIID) ciids[i]).CreditinfoId;
            }
            DbListArgs args = new DbListArgs(typeof (Individual));
            if (addressNative != null || addressEN != null) {
                IList addresses = FindAddress(addressNative, addressEN, arr);
                int[] arrAddress = new int[addresses.Count];
                for (int i = 0; i < addresses.Count; i++) {
                    arrAddress[i] = ((Address) addresses[i]).CreditinfoId;
                }

                args.DbExpressions.Add(Expression.In("CreditinfoId", arrAddress));
            } else {
                args.DbExpressions.Add(Expression.In("CreditinfoId", arr));
            }

            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Search for individuals based on nationalId
        /// </summary>
        /// <param name="nationalId">National ID to search</param>
        /// <returns>Retrieved list of individuals</returns>
        public IList FindIndividualByNationalId(string nationalId) {
            IList ciids = FindCIIDByID(nationalId);
            if (ciids == null || ciids.Count == 0) {
                return null;
            }
            int[] arr = new int[ciids.Count];
            for (int i = 0; i < ciids.Count; i++) {
                arr[i] = ((IDNumber) ciids[i]).CreditinfoId;
            }
            DbListArgs args = new DbListArgs(typeof (Individual));
            args.DbExpressions.Add(Expression.In("CreditinfoId", arr));

            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Find one individual based on creditinfoId
        /// </summary>
        /// <param name="creditinfoId">Creditinfo Id to search</param>
        /// <returns>Retrieved individual</returns>
        public IIndividual FindIndividualByCreditinfoId(int creditinfoId) {
            DbLoadArgs args = new DbLoadArgs(typeof (Individual));
            args.DbId = creditinfoId;
            return (IIndividual) Execute(Load, args);
        }

        /// <summary>
        /// Search for company based on national Id
        /// </summary>
        /// <param name="nationalId">National Id to search</param>
        /// <returns>Retrieved list of companies</returns>
        public IList FindCompanyByNationalId(string nationalId) {
            IList ciids = FindCIIDByID(nationalId);
            if (ciids == null || ciids.Count == 0) {
                return null;
            }
            int[] arr = new int[ciids.Count];
            for (int i = 0; i < ciids.Count; i++) {
                arr[i] = ((IDNumber) ciids[i]).CreditinfoId;
            }
            DbListArgs args = new DbListArgs(typeof (Company));
            args.DbExpressions.Add(Expression.In("CreditinfoId", arr));

            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Find one company based on creditinfoId
        /// </summary>
        /// <param name="creditinfoId">Creditinfo Id to search</param>
        /// <returns>Retrieved company</returns>
        public ICompany FindCompanyByCreditinfoId(int creditinfoId) {
            DbLoadArgs args = new DbLoadArgs(typeof (Company));
            args.DbId = creditinfoId;
            return (ICompany) Execute(Load, args);
        }

        /// <summary>
        /// Search for company based on name
        /// </summary>
        /// <param name="name">Name to search</param>
        /// <returns>Retrieved list of companies</returns>
        public IList FindCompany(string name) { return FindCompany(name, null, null); }

        /// <summary>
        /// Search for company based on name, address native, address en
        /// </summary>
        /// <param name="name">Name to search</param>
        /// <param name="addressNative">Address native to search</param>
        /// <param name="addressEN">Address En to search</param>
        /// <returns>Retrieved list of companies</returns>
        public IList FindCompany(string name, string addressNative, string addressEN) {
            IList ciids = FindCIIDByName(name, true);
            if (ciids == null || ciids.Count == 0) {
                return null;
            }

            int[] arr = new int[ciids.Count];
            for (int i = 0; i < ciids.Count; i++) {
                arr[i] = ((Word2CIID) ciids[i]).CreditinfoId;
            }
            DbListArgs args = new DbListArgs(typeof (Company));
            if (addressNative != null || addressEN != null) {
                IList addresses = FindAddress(addressNative, addressEN, arr);
                int[] arrAddress = new int[addresses.Count];
                for (int i = 0; i < addresses.Count; i++) {
                    arrAddress[i] = ((Address) addresses[i]).CreditinfoId;
                }

                args.DbExpressions.Add(Expression.In("CreditinfoId", arrAddress));
            } else {
                args.DbExpressions.Add(Expression.In("CreditinfoId", arr));
            }
            return (IList) Execute(List, args);
        }

        #endregion

        /// <summary>
        /// Search for addresses based on street native and street En
        /// </summary>
        /// <param name="streetNative">Street native to search</param>
        /// <param name="streetEN">Street En to search</param>
        /// <returns>Retrieved list of Addresses</returns>
        public IList FindAddress(string streetNative, string streetEN) { return FindAddress(streetNative, streetEN, new int[0]); }

        /// <summary>
        /// Search for addresses based on street native, street En and ciids
        /// </summary>
        /// <param name="streetNative">Street native to search</param>
        /// <param name="streetEN">Street En to search</param>
        /// <param name="ciids">array of ciids to search</param>
        /// <returns>Retrieved list of Addresses</returns>
        public IList FindAddress(string streetNative, string streetEN, int[] ciids) {
            DbListArgs args = new DbListArgs(typeof (Address));
            if (streetNative != null) {
                args.DbExpressions.Add(Expression.Like("StreetNative", streetNative + "%"));
            }
            if (streetEN != null) {
                args.DbExpressions.Add(Expression.Like("StreetEN", streetEN + "%"));
            }
            if (ciids.Length > 0) {
                args.DbExpressions.Add(Expression.In("CreditinfoId", ciids));
            }
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Search for CIID by name
        /// </summary>
        /// <param name="name">Name to search</param>
        /// <param name="company">if this is company or individual name</param>
        /// <returns>Retrieved list of CIIDs</returns>
        protected IList FindCIIDByName(string name, bool company) {
            DbListArgs args = new DbListArgs(typeof (Word2CIID));
            args.DbExpressions.Add(Expression.Like("Word", name + "%"));
            if (company) {
                args.DbExpressions.Add(Expression.Eq("Type", "n"));
            } else {
                args.DbExpressions.Add(Expression.Eq("Type", "i"));
            }
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Search for CIID by first name and surname
        /// </summary>
        /// <param name="firstName">Firstname to search</param>
        /// <param name="surName">Surname to search</param>
        /// <returns>Retrieved list of CIIDs</returns>
        protected IList FindCIIDByName(string firstName, string surName) {
            DbListArgs args = new DbListArgs(typeof (Word2CIID));
            if ((firstName != null && firstName.Trim() != "") && //sgh fixing if search only by surname.
                (surName != null && surName.Trim() != "")) {
                args.DbExpressions.Add(
                    Expression.And(
                        Expression.Sql(
                            "Creditinfoid in(SELECT creditinfoid FROM np_Word2CIID WC WHERE WC.Word LIKE N'" + firstName +
                            "%')"),
                        Expression.Sql(
                            "Creditinfoid in(SELECT creditinfoid FROM np_Word2CIID WC WHERE WC.Word LIKE N'" + surName +
                            "%')")));
            } else if (surName != null && surName.Trim() != "") //sgh fixing if search only by surname.
            {
                args.DbExpressions.Add(Expression.Like("Word", surName + "%"));
            } else {
                args.DbExpressions.Add(Expression.Like("Word", firstName + "%"));
            }
            args.DbExpressions.Add(Expression.Eq("Type", "i"));

            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Search for CIID by Id
        /// </summary>
        /// <param name="id">Id to search</param>
        /// <returns>Retrieved list of CIIDs</returns>
        protected IList FindCIIDByID(string id) {
            DbListArgs args = new DbListArgs(typeof (IDNumber));
            args.DbExpressions.Add(Expression.Like("Number", id));
            return (IList) Execute(List, args);
        }
    }
}