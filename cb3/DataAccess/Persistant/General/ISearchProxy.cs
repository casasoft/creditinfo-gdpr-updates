#region

using System.Collections;
using ValueObjects.General;

#endregion

namespace DataAccess.Persistant.General {
    /// <summary>
    /// Summary description for ISearchProxy.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ISearchProxy {
        /// <summary>
        /// Search for individuals based on firstname and surname
        /// </summary>
        /// <param name="firstName">Firstname to search</param>
        /// <param name="surName">Surname to search</param>
        /// <returns>Retrieved list of individuals</returns>
        IList FindIndividual(string firstName, string surName);

        /// <summary>
        /// Search for individuals based on firstname, surname, address native and address En
        /// </summary>
        /// <param name="firstName">Firstname to search</param>
        /// <param name="surName">Surname to search</param>
        /// <param name="addressNative">Address native to search</param>
        /// <param name="addressEN">Address en to search</param>
        /// <returns>Retrieved list of individuals</returns>
        IList FindIndividual(string firstName, string surName, string addressNative, string addressEN);

        /// <summary>
        /// Search for individuals based on nationalId
        /// </summary>
        /// <param name="nationalId">National ID to search</param>
        /// <returns>Retrieved list of individuals</returns>
        IList FindIndividualByNationalId(string nationalId);

        /// <summary>
        /// Find one individual based on creditinfoId
        /// </summary>
        /// <param name="creditinfoId">Creditinfo Id to search</param>
        /// <returns>Retrieved individual</returns>
        IIndividual FindIndividualByCreditinfoId(int creditinfoId);

        /// <summary>
        /// Search for company based on name
        /// </summary>
        /// <param name="name">Name to search</param>
        /// <returns>Retrieved list of companies</returns>
        IList FindCompany(string name);

        /// <summary>
        /// Search for company based on name, address native, address en
        /// </summary>
        /// <param name="name">Name to search</param>
        /// <param name="addressNative">Address native to search</param>
        /// <param name="addressEN">Address En to search</param>
        /// <returns>Retrieved list of companies</returns>
        IList FindCompany(string name, string addressNative, string addressEN);

        /// <summary>
        /// Search for company based on national Id
        /// </summary>
        /// <param name="nationalId">National Id to search</param>
        /// <returns>Retrieved list of companies</returns>
        IList FindCompanyByNationalId(string nationalId);

        /// <summary>
        /// Find one company based on creditinfoId
        /// </summary>
        /// <param name="creditinfoId">Creditinfo Id to search</param>
        /// <returns>Retrieved company</returns>
        ICompany FindCompanyByCreditinfoId(int creditinfoId);
    }
}