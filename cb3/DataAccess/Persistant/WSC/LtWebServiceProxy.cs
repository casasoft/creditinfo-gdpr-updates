#region

using System.Xml;
using Cig.Framework.Data.DataAccess;
using DataAccess.LtThjodskra;
using DataAccess.LtVogin;
using ValueObjects.WSC;

#endregion

namespace DataAccess.Persistant.WSC {
    /// <summary>
    /// Summary description for NationalRegistry.
    /// </summary>
    public class LtWebServiceProxy : PersistenceManager, ILtWebServiceProxy {
        #region ILtWebServiceProxy Members

        /// <summary>
        /// Gets information about an individual/company from national regstry.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <returns>XmlDocument containing the information for national registry.</returns>
        public XmlDocument GetNationalRegistryInformation(IAuthenticator authenticator, string query, string user) {
            thjodskra registry = new thjodskra();
            XmlNode node = registry.ThjodskraKtRequest(authenticator.Username, authenticator.Password, query, user);
            XmlNodeReader reader = new XmlNodeReader(node);
            XmlDataDocument doc = new XmlDataDocument();
            doc.DataSet.ReadXml(reader);

            return doc;
        }

        /// <summary>
        /// Gets information about debts.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <returns>XmlDocument containing the information from debt registry.</returns>
        public XmlDocument GetDebtRegistryInformation(IAuthenticator authenticator, string query, string user) {
            Vogin registry = new Vogin();
            XmlNode node = registry.VoginKtRequest(authenticator.Username, authenticator.Password, query, user);
            XmlNodeReader reader = new XmlNodeReader(node);
            XmlDataDocument doc = new XmlDataDocument();
            doc.DataSet.ReadXml(reader);

            return doc;
        }

        #endregion
    }
}