#region

using System.Xml;
using ValueObjects.WSC;

#endregion

namespace DataAccess.Persistant.WSC {
    /// <summary>
    /// Summary description for INationalRegistry.
    /// </summary>
    public interface ILtWebServiceProxy {
        /// <summary>
        /// Gets information about an individual/company from national regstry.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <returns>XmlDocument containing the information for national registry.</returns>
        XmlDocument GetNationalRegistryInformation(IAuthenticator authenticator, string query, string user);

        /// <summary>
        /// Gets information about debts.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <returns>XmlDocument containing the information from debt registry.</returns>
        XmlDocument GetDebtRegistryInformation(IAuthenticator authenticator, string query, string user);
    }
}