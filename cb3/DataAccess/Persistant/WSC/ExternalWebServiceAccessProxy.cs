#region

using System;
using System.Collections;
using Cig.Framework.Base.Common.Logging;
using Cig.Framework.Data.Common.Arguments.DbArguments;
using Cig.Framework.Data.DataAccess;
using NHibernate.Expression;
using ValueObjects.WSC;

#endregion

namespace DataAccess.Persistant.WSC {
    /// <summary>
    /// Summary description for ExternalWebServiceAccessProxy.
    /// <para>Copyright (C) 2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
    /// </summary>
    public class ExternalWebServiceAccessProxy : PersistenceManager, IExternalWebServiceAccessProxy {
        #region IExternalWebServiceAccessProxy Members

        /// <summary>
        /// Returns the web service access with the given primary key.
        /// </summary>
        /// <param name="webServiceId">Unique identifier of the web service.</param>
        /// <param name="userId">Unique identifier of the user.</param>
        /// <returns>The access object with the given primary key.</returns>
        public IExternalWebServiceAccess Get(int webServiceId, int userId) {
            DbListArgs args = new DbListArgs(typeof (ExternalWebServiceAccess));
            args.DbExpressions.Add(Expression.Eq("WebServiceId", webServiceId));
            args.DbExpressions.Add(Expression.Eq("UserId", userId));
            return (IExternalWebServiceAccess) Execute(ListOne, args);
        }

        /// <summary>
        /// Returns all access objects for a specific web service.
        /// </summary>
        /// <param name="webServiceId">Unique identifier for the given web service.</param>
        /// <returns>A list of access objects.</returns>
        public IList GetByWebService(int webServiceId) {
            DbListArgs args = new DbListArgs(typeof (ExternalWebServiceAccess));
            args.DbExpressions.Add(Expression.Eq("WebServiceId", webServiceId));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Returns all access objects for a specific user.
        /// </summary>
        /// <param name="userId">Unique identifier for the given user.</param>
        /// <returns>A list of access objects.</returns>
        public IList GetByUser(int userId) {
            DbListArgs args = new DbListArgs(typeof (ExternalWebServiceAccess));
            args.DbExpressions.Add(Expression.Eq("UserId", userId));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Saves external web service access to the database.
        /// </summary>
        /// <param name="access">IExternalWebServiceAccess instance of the access to save.</param>
        /// <returns>True if successfully saved otherwise false.</returns>
        public bool SaveAccess(IExternalWebServiceAccess access) {
            Execute(Save, new DbPersistArgs(access));
            return true;
        }

        /// <summary>
        /// Deletes web service access with the given primary key.
        /// </summary>
        /// <param name="webServiceId">Unique identifier of the web service.</param>
        /// <param name="userId">Unique identifier of the user.</param>
        /// <returns>True if the delete is successful.</returns>
        public bool Remove(int webServiceId, int userId) {
            try {
                IExternalWebServiceAccess access = Get(webServiceId, userId);
                if (access != null) {
                    DbDeleteArgs args = new DbDeleteArgs();
                    args.DbObject = access;
                    Execute(Delete, args);
                }
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("Delete failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        #endregion
    }
}