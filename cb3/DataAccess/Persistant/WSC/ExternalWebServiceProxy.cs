#region

using System;
using Cig.Framework.Base.Common.Logging;
using Cig.Framework.Data.Common.Arguments.DbArguments;
using Cig.Framework.Data.DataAccess;
using NHibernate.Expression;
using ValueObjects.WSC;

#endregion

namespace DataAccess.Persistant.WSC {
    /// <summary>
    /// Summary description for ExternalWebServiceProxy.
    /// <para>Copyright (C) 2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
    /// </summary>
    public class ExternalWebServiceProxy : PersistenceManager, IExternalWebServiceProxy {
        #region IExternalWebServiceProxy Members

        /// <summary>
        /// Returns the web service with the given Id.
        /// </summary>
        /// <param name="webServiceId">Unique identifier of the web service to return</param>
        /// <returns>The web service with given unique identifier</returns>
        public IExternalWebService Get(int webServiceId) { return (IExternalWebService) Execute(Load, new DbLoadArgs(typeof (ExternalWebService), webServiceId)); }

        /// <summary>
        /// Returns the web service with the specified short code.
        /// </summary>
        /// <param name="shortCode">Short code of the web service.</param>
        /// <returns>The web service with given short code.</returns>
        public IExternalWebService Get(string shortCode) {
            DbListArgs args = new DbListArgs(typeof (ExternalWebService));
            args.DbExpressions.Add(Expression.Eq("ShortCode", shortCode));
            return (IExternalWebService) Execute(ListOne, args);
        }

        /// <summary>
        /// Saves external web service to the database.
        /// </summary>
        /// <param name="webService">Unique identifier of the web service to remove.</param>
        /// <returns>True if successfully saved otherwise false.</returns>
        public bool Save(IExternalWebService webService) {
            try {
                DbPersistArgs args = new DbPersistArgs();
                args.DbObject = webService;
                return (bool) Execute(SaveOrUpdate, args);
            } catch (Exception exception) {
                LogAndThrow.IocException("Save failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        /// <summary>
        /// Deletes web service with the specific unique identifier.
        /// </summary>
        /// <param name="webServiceId">Unique identi</param>
        /// <returns>True if the delete is successful.</returns>
        public bool Delete(int webServiceId) {
            try {
                IExternalWebService webService = Get(webServiceId);
                if (webService != null) {
                    DbDeleteArgs args = new DbDeleteArgs();
                    args.DbObject = webService;
                    Execute(Delete, args);
                }
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("Delete failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        #endregion
    }
}