#region

using System;
using System.Collections;
using Cig.Framework.Base.Common.Logging;
using Cig.Framework.Data.Common.Arguments.DbArguments;
using Cig.Framework.Data.DataAccess;
using NHibernate.Expression;
using ValueObjects.FSI.Templates;

#endregion

namespace DataAccess.Persistant.FSI {
    /// <summary>
    /// This proxy contains data functionality regarding ratios for accounts.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class RatiosProxy : PersistenceManager, IRatiosProxy {
        #region IRatiosProxy Members

        /// <summary>
        /// Returns a list of all ratios available in the system
        /// </summary>
        /// <returns>A list of all ratios available in the system</returns>
        public IList GetRatios() { return (IList) Execute(List, new DbListArgs(typeof (Ratio))); }

        /// <summary>
        /// Returns a list of all ratios assigned to given template
        /// </summary>
        /// <param name="templateID">Id of the template</param>
        /// <returns>A list of all ratios assigned to the template</returns>
        public IList GetRatios(int templateID) {
            var args = new DbListArgs(typeof (Ratio));
            args.DbExpressions.Add(Expression.Eq("TemplateID", templateID));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Returns the ratio with given id
        /// </summary>
        /// <param name="ratioID">Id of the ratio</param>
        /// <returns>The ratio with given id</returns>
        public IRatio GetRatio(int ratioID) {
            var args = new DbLoadArgs(typeof (Ratio)) {DbId = ratioID};
            return (IRatio) Execute(Load, args);
        }

        /// <summary>
        /// Returns a list of formula items for given ratio
        /// </summary>
        /// <param name="ratioID">The id of the ratio the have formula for</param>
        /// <returns>A list of formula items for given ratio</returns>
        public IList GetRatioFormula(int ratioID) {
            var args = new DbListArgs(typeof (RatioFormula));
            args.DbExpressions.Add(Expression.Eq("RatioID", ratioID));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Deletes the formula for given ratio
        /// </summary>
        /// <param name="ratioID">Id of the ratio to delete formula for</param>
        /// <returns>true if formula deleted successfully, false otherwise</returns>
        public bool DeleteRatioFormula(int ratioID) {
            try {
                IList formulas = GetRatioFormula(ratioID);
                session = sessionFactory.OpenSession();
                for (int i = 0; i < formulas.Count; i++) {
                    session.Delete(formulas[i]);
                }
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("DeleteRatioFormula failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        /// <summary>
        /// Saves given ratio to the database
        /// </summary>
        /// <param name="ratio">The ratio to save</param>
        /// <returns>True if ratio saved successfully, false otherwise</returns>
        public bool SaveRatio(IRatio ratio) {
            try {
                DeleteRatioFormula(ratio.ID);
                session = sessionFactory.OpenSession();
                if (ratio.ID == -1) {
                    session.Save(ratio);
                } else {
                    session.Update(ratio);
                }

                //Now save the formula
                for (int i = 0; i < ratio.Formula.Count; i++) {
                    session.Save(ratio.Formula[i]);
                }
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("SaveRatio failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        /// <summary>
        /// Deletes given ratio from the database
        /// </summary>
        /// <param name="ratio">The ratio to delete</param>
        /// <returns>True if deleted successfully, false otherwise</returns>
        public bool DeleteRatio(IRatio ratio) {
            try {
                session = sessionFactory.OpenSession();
                session.Delete(ratio);
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("DeleteRatio failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        #endregion
    }
}