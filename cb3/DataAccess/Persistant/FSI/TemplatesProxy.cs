#region

using System;
using System.Collections;
using Cig.Framework.Base.Common.Logging;
using Cig.Framework.Base.Configuration;
using Cig.Framework.Data.Common.Arguments.DbArguments;
using Cig.Framework.Data.DataAccess;
using NHibernate;
using NHibernate.Expression;
using ValueObjects.FSI.Templates;

#endregion

namespace DataAccess.Persistant.FSI {
    /// <summary>
    /// This proxy contains data functionality regarding templates for accounts.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class TemplatesProxy : PersistenceManager, ITemplatesProxy {
        /// <summary>
        /// DEfault constructor.
        /// </summary>
        static TemplatesProxy() { CigConfig.Configure(Type.GetType("DataAccess.Persistant.FSI.TemplatesProxy"), null); }

        #region Templates:

        /// <summary>
        /// Returns a list of all templates in the system.
        /// </summary>
        /// <returns>A list of all templates in the system.</returns>
        public IList GetTemplates() { return (IList) Execute(List, new DbListArgs(typeof (Template))); }

        /// <summary>
        /// Returns the template with given id
        /// </summary>
        /// <param name="templateID"></param>
        /// <returns>The template with given id</returns>
        public ITemplate GetTemplate(int templateID) {
            Template result = null;

            try {
                session = sessionFactory.OpenSession();
                result = (Template) session.Load(typeof (Template), templateID);
            } catch (Exception exception) {
                LogAndThrow.IocException("GetTemplate failed to return a value", exception, GetType());
            } finally {
                session.Flush();
                session.Close();
            }
            return result;
        }

        #endregion

        #region ITemplatesProxy Members

        /// <summary>
        /// Stores given template ratios in the database
        /// </summary>
        /// <param name="templateRatios">Templates ratios to save</param>
        /// <returns>True if saved successfully, false otherwise</returns>
        public bool SaveTemplateRatios(IList templateRatios) {
            session = sessionFactory.OpenSession();
            ITransaction tx = session.BeginTransaction();
            try {
                if (templateRatios.Count > 0) {
                    var ratio = (ITemplateRatio) templateRatios[0];
                    session.Delete("from TemplateRatio where TemplateID ='" + ratio.TemplateID + "'");
                    for (int i = 0; i < templateRatios.Count; i++) {
                        session.Save(templateRatios[i]);
                    }
                    tx.Commit();
                }
                return true;
            } catch (Exception exception) {
                tx.Rollback();
                LogAndThrow.IocException("SaveRatio failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Close();
            }
        }

        #endregion

        #region Fields:

        /// <summary>
        /// Returns a list of all fields for given templates
        /// </summary>
        /// <param name="templateID">Id of the template</param>
        /// <returns>All fields for given template</returns>
        public IList GetFields(int templateID) {
            var args = new DbListArgs(typeof (Field));
            args.DbExpressions.Add(Expression.Eq("TemplateID", templateID));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Returns the field with given id
        /// </summary>
        /// <param name="fieldID">Id of the field</param>
        /// <returns>The field with given id</returns>
        public IField GetField(int fieldID) {
            IField result = null;

            try {
                session = sessionFactory.OpenSession();
                result = (IField) session.Load(typeof (Field), fieldID);
            } catch (Exception exception) {
                LogAndThrow.IocException("GetField failed to return a value", exception, GetType());
            } finally {
                session.Flush();
                session.Close();
            }
            return result;
        }

        /// <summary>
        /// Returns a list of all available fields in the system
        /// </summary>
        /// <returns>A list of all available fields in the system</returns>
        public IList GetFields() { return (IList) Execute(List, new DbListArgs(typeof (Field))); }

        #endregion

        #region Operators:

        /// <summary>
        /// Returns a list of all available operators in the system
        /// </summary>
        /// <returns>A list of all available operators in the system</returns>
        public IList GetOperators() { return (IList) Execute(List, new DbListArgs(typeof (Operator))); }

        /// <summary>
        /// Returns the operator with given id
        /// </summary>
        /// <param name="operatorID">Id of the operator</param>
        /// <returns>The operator with given id</returns>
        public IOperator GetOperator(int operatorID) {
            IOperator result = null;

            try {
                session = sessionFactory.OpenSession();
                result = (IOperator) session.Load(typeof (Operator), operatorID);
            } catch (Exception exception) {
                LogAndThrow.IocException("GetField failed to return a value", exception, GetType());
            } finally {
                session.Flush();
                session.Close();
            }
            return result;
        }

        #endregion
    }
}