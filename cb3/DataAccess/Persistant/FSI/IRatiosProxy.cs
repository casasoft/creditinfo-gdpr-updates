#region

using System.Collections;
using ValueObjects.FSI.Templates;

#endregion

namespace DataAccess.Persistant.FSI {
    /// <summary>
    /// This interface describes functionality for fsi ratios handling.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IRatiosProxy {
        /// <summary>
        /// Returns a list of all ratios available in the system
        /// </summary>
        /// <returns>A list of all ratios available in the system</returns>
        IList GetRatios();

        /// <summary>
        /// Returns a list of all ratios assigned to given template
        /// </summary>
        /// <param name="templateID">Id of the template</param>
        /// <returns>A list of all ratios assigned to the template</returns>
        IList GetRatios(int templateID);

        /// <summary>
        /// Returns the ratio with given id
        /// </summary>
        /// <param name="ratioID">Id of the ratio</param>
        /// <returns>The ratio with given id</returns>
        IRatio GetRatio(int ratioID);

        /// <summary>
        /// Saves given ratio to the database
        /// </summary>
        /// <param name="ratio">The ratio to save</param>
        /// <returns>True if ratio saved successfully, false otherwise</returns>
        bool SaveRatio(IRatio ratio);

        /// <summary>
        /// Deletes given ratio from the database
        /// </summary>
        /// <param name="ratio">The ratio to delete</param>
        /// <returns>True if deleted successfully, false otherwise</returns>
        bool DeleteRatio(IRatio ratio);

        /// <summary>
        /// Returns a list of formula items for given ratio
        /// </summary>
        /// <param name="ratioID">The id of the ratio the have formula for</param>
        /// <returns>A list of formula items for given ratio</returns>
        IList GetRatioFormula(int ratioID);

        /// <summary>
        /// Deletes the formula for given ratio
        /// </summary>
        /// <param name="ratioID">Id of the ratio to delete formula for</param>
        /// <returns>true if formula deleted successfully, false otherwise</returns>
        bool DeleteRatioFormula(int ratioID);
    }
}