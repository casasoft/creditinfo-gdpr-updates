#region

using System.Collections;
using ValueObjects.FSI.Templates;

#endregion

namespace DataAccess.Persistant.FSI {
    /// <summary>
    /// This interface describes functionality for fsi template handling.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ITemplatesProxy {
        /// <summary>
        /// Returns a list of all templates in the system.
        /// </summary>
        /// <returns>A list of all templates in the system.</returns>
        IList GetTemplates();

        /// <summary>
        /// Returns the template with given id
        /// </summary>
        /// <param name="templateID"></param>
        /// <returns>The template with given id</returns>
        ITemplate GetTemplate(int templateID);

        /// <summary>
        /// Returns a list of all available fields in the system
        /// </summary>
        /// <returns>A list of all available fields in the system</returns>
        IList GetFields();

        /// <summary>
        /// Returns a list of all fields for given templates
        /// </summary>
        /// <param name="templateID">Id of the template</param>
        /// <returns>All fields for given template</returns>
        IList GetFields(int templateID);

        /// <summary>
        /// Returns the field with given id
        /// </summary>
        /// <param name="fieldID">Id of the field</param>
        /// <returns>The field with given id</returns>
        IField GetField(int fieldID);

        /// <summary>
        /// Returns a list of all available operators in the system
        /// </summary>
        /// <returns>A list of all available operators in the system</returns>
        IList GetOperators();

        /// <summary>
        /// Returns the operator with given id
        /// </summary>
        /// <param name="operatorID">Id of the operator</param>
        /// <returns>The operator with given id</returns>
        IOperator GetOperator(int operatorID);

        /// <summary>
        /// Stores given template ratios in the database
        /// </summary>
        /// <param name="templateRatios">Templates ratios to save</param>
        /// <returns>True if saved successfully, false otherwise</returns>
        bool SaveTemplateRatios(IList templateRatios);
    }
}