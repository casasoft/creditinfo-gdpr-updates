#region

using System.Collections;
using Cig.Framework.Data.Common.Arguments.DbArguments;
using Cig.Framework.Data.DataAccess;
using ValueObjects.Base;
using ValueObjects.General;
using ValueObjects.NP;

#endregion

namespace DataAccess.Persistant.NP {
    /// <summary>
    /// Summary description for LookupDataProxy.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class LookupDataProxy : PersistenceManager, ILookupDataProxy {
        #region ILookupDataProxy Members

        /// <summary>
        /// Get case statuses
        /// </summary>
        /// <returns>Retrieved case statuses</returns>
        public IList GetCaseStatuses() { return (IList) Execute(List, new DbListArgs(typeof (CaseStatus))); }

        /// <summary>
        /// Get information sources
        /// </summary>
        /// <returns>Retrieved information sources</returns>
        public IList GetInformationSources() { return (IList) Execute(List, new DbListArgs(typeof (CaseInformationSource))); }

        /// <summary>
        /// Get case types
        /// </summary>
        /// <returns>Retrieved case types</returns>
        public IList GetCaseTypes() { return (IList) Execute(List, new DbListArgs(typeof (CaseType))); }

        /// <summary>
        /// Get related party types
        /// </summary>
        /// <returns>Retrieved related party types</returns>
        public IList GetRelatedPartyTypes() { return (IList) Execute(List, new DbListArgs(typeof (RelatedPartyType))); }

        /// <summary>
        /// Get one ralated party type based on typeID
        /// </summary>
        /// <param name="typeID">typeID</param>
        /// <returns>Concrete related party type</returns>
        public ICigBaseLookupValueObject GetRelatedPartyType(int typeID) {
            DbLoadArgs args = new DbLoadArgs(typeof (RelatedPartyType));
            args.DbId = typeID;
            return (ICigBaseLookupValueObject) Execute(Load, args);
        }

        /// <summary>
        /// Retrieve all collateral value types
        /// </summary>
        /// <returns>IList of CollateralValueTypes</returns>
        public IList GetCollateralValueTypes() { return (IList) Execute(List, new DbListArgs(typeof (CollateralValueType))); }

        /// <summary>
        /// Retrieve all collateral types
        /// </summary>
        /// <returns>IList of CollateralTypes</returns>
        public IList GetCollateralTypes() { return (IList) Execute(List, new DbListArgs(typeof (CollateralType))); }

        /// <summary>
        /// Retrieve all cities
        /// </summary>
        /// <returns>IList of City</returns>
        public IList GetCities() { return (IList) Execute(List, new DbListArgs(typeof (City))); }

        /// <summary>
        /// Retrieve all postcodes
        /// </summary>
        /// <returns>IList of Postcode</returns>
        public IList GetPostcodes() { return (IList) Execute(List, new DbListArgs(typeof (Postcode))); }

        /// <summary>
        /// Retrieve all countries
        /// </summary>
        /// <returns>IList of Country</returns>
        public IList GetCountries() { return (IList) Execute(List, new DbListArgs(typeof (Country))); }

        /// <summary>
        /// Retrieve concrete collateral value type
        /// </summary>
        /// <param name="valueTypeID">collateral value type unique identifier</param>
        /// <returns>concrete collateral value type</returns>
        public ICigBaseLookupValueObject GetCollateralValueType(int valueTypeID) {
            DbLoadArgs args = new DbLoadArgs(typeof (CollateralValueType));
            args.DbId = valueTypeID;
            return (ICigBaseLookupValueObject) Execute(Load, args);
        }

        /// <summary>
        /// Retrieve concrete collateral type
        /// </summary>
        /// <param name="typeID">collateral type unique identifier</param>
        /// <returns>concrete collateral type</returns>
        public ICollateralType GetCollateralType(string typeID) {
            DbLoadArgs args = new DbLoadArgs(typeof (CollateralType));
            args.DbId = typeID;
            return (ICollateralType) Execute(Load, args);
        }

        /// <summary>
        /// Retrieve concrete City
        /// </summary>
        /// <param name="cityID">city unique identifier</param>
        /// <returns>concrete city</returns>
        public ICity GetCity(int cityID) {
            DbLoadArgs args = new DbLoadArgs(typeof (City));
            args.DbId = cityID;
            return (ICity) Execute(Load, args);
        }

        /// <summary>
        /// Retrieve concrete postcode
        /// </summary>
        /// <param name="postcodeID">postcode unique identifier</param>
        /// <returns>concrete postcode</returns>
        public IPostcode GetPostcode(int postcodeID) {
            DbLoadArgs args = new DbLoadArgs(typeof (Postcode));
            args.DbId = postcodeID;
            return (IPostcode) Execute(Load, args);
        }

        /// <summary>
        /// Retrive concrete country
        /// </summary>
        /// <param name="countryID">country unique identifier</param>
        /// <returns>concrete country</returns>
        public ICountry GetCountry(int countryID) {
            DbLoadArgs args = new DbLoadArgs(typeof (Country));
            args.DbId = countryID;
            return (ICountry) Execute(Load, args);
        }

        /// <summary>
        /// Retrives Case type.
        /// </summary>
        /// <param name="caseTypeID">Case type unique identifier.</param>
        /// <returns>The requested case type.</returns>
        public ICaseType GetCaseType(int caseTypeID) {
            DbLoadArgs args = new DbLoadArgs(typeof (CaseType));
            args.DbId = caseTypeID;
            return (ICaseType) Execute(Load, args);
        }

        /// <summary>
        /// Retrives Case status.
        /// </summary>
        /// <param name="caseStatusID">Case type unique identifier.</param>
        /// <returns>The requested case type.</returns>
        public CaseStatus GetCaseStatus(int caseStatusID) {
            DbLoadArgs args = new DbLoadArgs(typeof (CaseStatus));
            args.DbId = caseStatusID;
            return (CaseStatus) Execute(Load, args);
        }

        #endregion
    }
}