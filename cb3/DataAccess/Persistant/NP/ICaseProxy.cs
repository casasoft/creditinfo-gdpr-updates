#region

using System;
using System.Collections;
using ValueObjects.NP;

#endregion

namespace DataAccess.Persistant.NP {
    /// <summary>
    /// Interface for manipulating Case 
    /// </summary>
    public interface ICaseProxy {
        /// <summary>
        /// Returns all cases registered for the last given hours
        /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
        /// </summary>
        /// <param name="forTheLastHours">The hours to get cases for</param>
        /// <returns>List of cases that match the criteria</returns>
        IList GetCases(int forTheLastHours);

        /// <summary>
        /// Returns case by stautus Id
        /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
        /// </summary>
        /// <param name="statusId">The staus id to get cases for</param>
        /// <returns>List of cases that match the criteria</returns>
        IList GetCasesByStatusId(int statusId);

        /// <summary>
        /// Returns cases by contract number id
        /// </summary>
        /// <returns>List of cases that match the criteria</returns>
        IList GetCasesByCaseNumber(string caseNumber);

        /// <summary>
        /// Returns the case with given id
        /// </summary>
        /// <param name="caseID">Id of the case to return</param>
        /// <returns>The case with given id</returns>
        INPCase GetCase(int caseID);

        /// <summary>
        /// Gets the dynamic values of a certain case.
        /// </summary>
        /// <param name="caseID">Id of the case.</param>
        /// <returns>All dynamic fields of the Case.</returns>
        Hashtable GetDynamicValues(int caseID);

        /// <summary>
        /// Saves given case to the database
        /// </summary>
        /// <param name="theCase">The case to save</param>
        /// <returns>True if case saved successfully, false otherwise</returns>
        bool SaveCase(INPCase theCase);

        /// <summary>
        /// Saves related parties to the database
        /// </summary>
        /// <param name="relatedParties">The list of related parties</param>
        /// <returns>True if related parties saved successfully, false otherwise</returns>
        bool SaveRelatedParties(IList relatedParties);

        /// <summary>
        /// Deletes all related parties of a case.
        /// </summary>
        /// <param name="caseID">Id of the case.</param>
        /// <returns>True if successful.</returns>
        bool DeleteRelatedParties(int caseID);

        /// <summary>
        /// All related parties of a given case.
        /// </summary>
        /// <param name="caseID">ID of the case.</param>
        /// <returns>List of related parties of a case.</returns>
        IList GetRelatedParties(int caseID);

        /// <summary>
        /// Saves notes to the database
        /// </summary>
        /// <param name="notes">The list of notes</param>
        /// <returns>True if notes saved successfully, false otherwise</returns>
        bool SaveNotes(IList notes);

        /// <summary>
        /// Saves case fields of a given case to the database.
        /// </summary>
        /// <param name="fields">Fields to be saved.</param>
        /// <param name="caseID">ID of the case.</param>
        /// <returns>True if the save is successful.</returns>
        bool SaveCaseField(Hashtable fields, int caseID);

        /// <summary>
        /// Deletes case field of a selected Case.
        /// </summary>
        /// <param name="caseID">ID of the case.</param>
        /// <returns>True if the delete is successful.</returns>
        bool DeleteCaseField(int caseID);

        /// <summary>
        /// Saves collaterals to the database
        /// </summary>
        /// <param name="collaterals">The collaterals to save</param>
        /// <returns>True if collaterals saved successfully, false otherwise</returns>
        bool SaveCollaterals(IList collaterals);

        /// <summary>
        /// Delete all collaterals from the database
        /// </summary>
        /// <param name="caseId">Id of the case</param>
        /// <returns>True if collaterals deleted successfully, false otherwise</returns>
        bool DeleteCollaterals(int caseId);

        /// <summary>
        /// Retrieve collaterals based on case id
        /// </summary>
        /// <param name="caseId">Id of the case</param>
        /// <returns>List of collaterals for specified case</returns>
        IList GetCollaterals(int caseId);

        /// <summary>
        /// Searches for a given case in the database
        /// </summary>
        /// <param name="theCase">The case to search by</param>
        /// <param name="registeredFrom">registered from date</param>
        /// <param name="registeredTo">registered to date</param>
        /// <param name="updatedFrom">updated from date</param>
        /// <param name="updatedTo">updated to date</param>
        /// <param name="debtor">debtor id</param>		
        /// <returns>List of cases that match the criteria</returns>
        IList SearchCase(
            INPCase theCase,
            DateTime registeredFrom,
            DateTime registeredTo,
            DateTime updatedFrom,
            DateTime updatedTo,
            int debtor);

        /// <summary>
        /// Saves given case to the database with all included objects in one transaction
        /// </summary>
        /// <param name="theCase">The case to save</param>
        /// <returns>True if case saved successfully, false otherwise</returns>
        bool SaveCaseInTran(INPCase theCase);
    }
}