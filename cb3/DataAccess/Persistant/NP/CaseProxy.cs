#region

using System;
using System.Collections;
using System.Data.SqlClient;
using cb3.Audit;
using Cig.Framework.Base.Common.Logging;
using Cig.Framework.Base.Configuration;
using Cig.Framework.Data.Common.Arguments.DbArguments;
using Cig.Framework.Data.DataAccess;
using NHibernate;
using NHibernate.Expression;
using ValueObjects.NP;

#endregion

namespace DataAccess.Persistant.NP {
    /// <summary>
    /// Summary description for ClaimsProxy.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class CaseProxy : PersistenceManager, ICaseProxy {
        #region ICaseProxy Members

        /// <summary>
        /// Returns all cases registered for the last given hours
        /// </summary>
        /// <param name="forTheLastHours">The hours to get cases for</param>
        /// <returns>List of cases that match the criteria</returns>
        public IList GetCases(int forTheLastHours) {
            DbListArgs args = new DbListArgs(typeof (NPCase));
            DateTime dt = DateTime.Now;
            dt = dt.AddHours(-forTheLastHours);
            args.DbExpressions.Add(Expression.Ge("CaseDate", dt));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Returns cases by status id
        /// </summary>
        /// <param name="statusId">The staus id to get cases for</param>
        /// <returns>List of cases that match the criteria</returns>
        public IList GetCasesByStatusId(int statusId) {
            DbListArgs args = new DbListArgs(typeof (NPCase));
            args.DbExpressions.Add(Expression.Eq("StatusId", statusId));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Returns cases by contract number id
        /// </summary>
        /// <param name="statusId">The staus id to get cases for</param>
        /// <returns>List of cases that match the criteria</returns>
        public IList GetCasesByCaseNumber(string caseNumber) {
            DbListArgs args = new DbListArgs(typeof (NPCase));
            args.DbExpressions.Add(Expression.Eq("CaseNumber", caseNumber));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Returns the case with given id
        /// </summary>
        /// <param name="caseID">Id of the case to return</param>
        /// <returns>The case with given id</returns>
        public INPCase GetCase(int caseID) {
            DbLoadArgs args = new DbLoadArgs(typeof (NPCase));
            args.DbId = caseID;
            return (INPCase) Execute(Load, args);
        }

        /// <summary>
        /// Saves given case to the database
        /// </summary>
        /// <param name="theCase">The case to save</param>
        /// <returns>True if case saved successfully, false otherwise</returns>
        public bool SaveCase(INPCase theCase) {
            try {
                DbPersistArgs args = new DbPersistArgs();
                args.DbObject = theCase;
                return (bool) Execute(SaveOrUpdate, args);
            } catch (Exception exception) {
                LogAndThrow.IocException("SaveCase failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        /// <summary>
        /// Saves related parties to the database
        /// </summary>
        /// <param name="relatedParties">The list of related parties</param>
        /// <returns>True if related parties saved successfully, false otherwise</returns>
        public bool SaveRelatedParties(IList relatedParties) {
            try {
                for (int i = 0; i < relatedParties.Count; i++) {
                    DbPersistArgs args = new DbPersistArgs();
                    args.DbObject = relatedParties[i];
                    Execute(Save, args);
                }
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("SaveRelatedParties failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        /// <summary>
        /// Deletes all related parties of a case.
        /// </summary>
        /// <param name="caseID">Id of the case.</param>
        /// <returns>True if successful.</returns>
        public bool DeleteRelatedParties(int caseID) {
            try {
                IList relatedParties = GetRelatedParties(caseID);

                for (int i = 0; i < relatedParties.Count; i++) {
                    DbDeleteArgs args = new DbDeleteArgs(relatedParties[i]);
                    Execute(Delete, args);
                }
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("DeleteRelatedParties failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
            /*
			try
			{
				IList relatedParties = GetRelatedParties(caseID);
				session = sessionFactory.OpenSession();
				for(int i=0;i<relatedParties.Count;i++)
					session.Delete((IRelatedParty)relatedParties[i]);
				return true;
			} 
			catch (Exception exception)
			{
				LogAndThrow.IocException("DelateRelatedParties failed to return a value.", exception, this.GetType());	
				return false;
			}
			finally
			{
				session.Flush();
				session.Close();
				session.
			}
			*/
        }

        /// <summary>
        /// All related parties of a given case.
        /// </summary>
        /// <param name="caseID">ID of the case.</param>
        /// <returns>List of related parties of a case.</returns>
        public IList GetRelatedParties(int caseID) {
            DbListArgs args = new DbListArgs(typeof (RelatedParty));
            args.DbExpressions.Add(Expression.Eq("CaseId", caseID));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Saves notes to the database
        /// </summary>
        /// <param name="notes">The list of notes</param>
        /// <returns>True if notes saved successfully, false otherwise</returns>
        public bool SaveNotes(IList notes) {
            try {
                for (int i = 0; i < notes.Count; i++) {
                    DbPersistArgs args = new DbPersistArgs();
                    args.DbObject = notes[i];
                    Execute(SaveOrUpdate, args);
                }
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("SaveNotes failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        /// <summary>
        /// Saves collaterals to the database
        /// </summary>
        /// <param name="collaterals">The collaterals to save</param>
        /// <returns>True if collaterals saved successfully, false otherwise</returns>
        public bool SaveCollaterals(IList collaterals) {
            try {
                if (collaterals != null) {
                    for (int i = 0; i < collaterals.Count; i++) {
                        DbPersistArgs args = new DbPersistArgs();
                        args.DbObject = collaterals[i];
                        Execute(Save, args);
                    }
                }
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("SaveCollaterals failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        /// <summary>
        /// Delete all collaterals from the database
        /// </summary>
        /// <param name="caseId">Id of the case</param>
        /// <returns>True if collaterals deleted successfully, false otherwise</returns> 
        public bool DeleteCollaterals(int caseId) {
            try {
                IList collaterals = GetCollaterals(caseId);
                if (collaterals != null) {
                    for (int i = 0; i < collaterals.Count; i++) {
                        DbDeleteArgs args = new DbDeleteArgs();
                        args.DbObject = collaterals[i];
                        Execute(Delete, args);
                    }
                }
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("SaveCollaterals failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        /// <summary>
        /// Retrieve collaterals based on case id
        /// </summary>
        /// <param name="caseId">Id of the case</param>
        /// <returns>List of collaterals for specified case</returns>
        public IList GetCollaterals(int caseId) {
            DbListArgs args = new DbListArgs(typeof (Collateral));
            args.DbExpressions.Add(Expression.Eq("CaseID", caseId));

            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Saves case fields of a given case to the database.
        /// </summary>
        /// <param name="fields">Fields to be saved.</param>
        /// <param name="caseID">ID of the case.</param>
        /// <returns>True if the save is successful.</returns>
        public bool SaveCaseField(Hashtable fields, int caseID) {
            try {
                session = sessionFactory.OpenSession();
                SqlConnection myConn = (SqlConnection) session.Connection;

                string sqlQuery = "insert into c_np_CaseFields (CaseID, [";

                IEnumerator keys = fields.Keys.GetEnumerator();
                IEnumerator values = fields.Values.GetEnumerator();

                while (keys.MoveNext()) {
                    sqlQuery += keys.Current + "],[";
                }
                sqlQuery = sqlQuery.TrimEnd(new char[2] {',', '['});
                sqlQuery += ") values ('" + caseID + "','";

                while (values.MoveNext()) {
                    sqlQuery += values.Current + "','";
                }
                sqlQuery = sqlQuery.TrimEnd(new char[1] {'\''});
                sqlQuery = sqlQuery.TrimEnd(new char[1] {','});
                sqlQuery += ")";

                SqlCommand myCommand = new SqlCommand(sqlQuery);
                myCommand.Connection = myConn;
                new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
            } catch (Exception exception) {
                LogAndThrow.IocException("SaveCaseField failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }

            return true;
        }

        /// <summary>
        /// Deletes case field of a selected Case.
        /// </summary>
        /// <param name="caseID">ID of the case.</param>
        /// <returns>True if the delete is successful.</returns>
        public bool DeleteCaseField(int caseID) {
            try {
                session = sessionFactory.OpenSession();
                SqlConnection myConn = (SqlConnection) session.Connection;

                string sqlQuery = "delete from c_np_CaseFields where CaseID = " + caseID;

                SqlCommand myCommand = new SqlCommand(sqlQuery);
                myCommand.Connection = myConn;
                new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
            } catch (Exception exception) {
                LogAndThrow.IocException("DeleteCaseFieled failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }

            return true;
        }

        /// <summary>
        /// Gets the dynamic values of a certain case.
        /// </summary>
        /// <param name="caseID">Id of the case.</param>
        /// <returns>All dynamic fields of the Case.</returns>
        public Hashtable GetDynamicValues(int caseID) {
            Hashtable returnValues = new Hashtable();
            try {
                ArrayList arrColumns = new ArrayList();

                session = sessionFactory.OpenSession();
                SqlConnection myConn = (SqlConnection) session.Connection;

                string sqlColumnsQuery = "sp_columns @table_name = c_np_casefields";
                string sqlDataQuery = "select * from c_np_CaseFields where CaseID = " + caseID;

                SqlCommand myCommand = new SqlCommand(sqlColumnsQuery);
                myCommand.Connection = myConn;
                SqlDataReader reader = myCommand.ExecuteReader();

                while (reader.Read()) {
                    if (!reader.IsDBNull(3)) {
                        arrColumns.Add(reader.GetString(3));
                    } else {
                        arrColumns.Add("");
                    }
                }
                arrColumns.RemoveAt(0);
                reader.Close();

                myCommand = new SqlCommand(sqlDataQuery);
                myCommand.Connection = myConn;
                reader = myCommand.ExecuteReader();
                if (reader.Read()) {
                    for (int i = 0; i < arrColumns.Count; i++) {
                        int ord = reader.GetOrdinal(arrColumns[i].ToString());
                        string val = string.Empty;
                        if (!reader.IsDBNull(ord)) {
                            if (reader.GetFieldType(ord) == typeof (string)) {
                                val = reader.GetString(ord);
                            }
                        }
                        returnValues.Add(arrColumns[i].ToString(), val);
                    }
                }
                reader.Close();

                myCommand.Connection.Close();
            } catch (Exception) {
                //TODO: add exception handling //string drasl = "";
            } finally {
                session.Flush();
                session.Disconnect();
            }

            return returnValues;
        }

        /// <summary>
        /// Searches for a given case in the database
        /// </summary>
        /// <param name="theCase">The case to search by</param>
        /// <param name="registeredFrom">Case registered from</param>
        /// <param name="registeredTo">Case registered to</param>
        /// <param name="updatedFrom">Case updated from</param>
        /// <param name="updatedTo">Case updated to</param>
        /// <param name="debtor">CIG id of debtor</param>		
        /// <returns>List of cases that match the criteria</returns>
        public IList SearchCase(
            INPCase theCase,
            DateTime registeredFrom,
            DateTime registeredTo,
            DateTime updatedFrom,
            DateTime updatedTo,
            int debtor) {
            DbListArgs args = new DbListArgs(typeof (NPCase));

            if (theCase.CaseNumber != null && theCase.CaseNumber.Length > 0) {
                args.DbExpressions.Add(Expression.Eq("CaseNumber", theCase.CaseNumber));
            }
            if (theCase.StatusId > 0) {
                args.DbExpressions.Add(Expression.Eq("StatusId", theCase.StatusId));
            }
            if (theCase.CaseTypeId > 0) {
                args.DbExpressions.Add(Expression.Eq("CaseTypeId", theCase.CaseTypeId));
            } else {
                args.DbExpressions.Add(
                    Expression.Lt(
                        "CaseTypeId", int.Parse(CigConfig.Configure("lookupsettings.lookupsettings.PP.PositivePaymentTypeID"))));
            }
            if (theCase.InformationSourceId > 0) {
                args.DbExpressions.Add(Expression.Eq("InformationSourceId", theCase.InformationSourceId));
            }
            if (theCase.Id > 0) {
                args.DbExpressions.Add(Expression.Eq("Id", theCase.Id));
            }
            if (theCase.InsertedBy > 0) {
                args.DbExpressions.Add(Expression.Eq("InsertedBy", theCase.InsertedBy));
            }
            if (theCase.UpdatedBy > 0) {
                args.DbExpressions.Add(Expression.Eq("UpdatedBy", theCase.UpdatedBy));
            }
            if (registeredFrom != DateTime.MinValue) {
                args.DbExpressions.Add(Expression.Ge("Inserted", registeredFrom));
            }
            if (registeredTo != DateTime.MinValue) {
                args.DbExpressions.Add(Expression.Lt("Inserted", registeredTo.AddDays(1)));
            }
            if (updatedFrom != DateTime.MinValue) {
                args.DbExpressions.Add(Expression.Ge("Updated", updatedFrom));
            }
            if (updatedTo != DateTime.MinValue) {
                args.DbExpressions.Add(Expression.Lt("Updated", updatedTo.AddDays(1)));
            }

            //PP excluding
            //args.DbExpressions.Add(Expression.("CaseTypeId", int.Parse(Cig.Framework.Base.Configuration.CigConfig.Configure("lookupsettings.lookupsettings.PP.PositivePaymentTypeID"])));

            // Todo: Get values from config file.
            if (debtor > 0) {
                IList relatedParties = FindRelatedParties(debtor);
                if (relatedParties.Count < 1) {
                    return new ArrayList();
                }

                int[] arrParties = new int[relatedParties.Count];
                for (int i = 0; i < relatedParties.Count; i++) {
                    arrParties[i] = ((RelatedParty) relatedParties[i]).CaseId;
                }

                args.DbExpressions.Add(Expression.In("Id", arrParties));
            }

            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Saves given case to the database with all included objects in one transaction
        /// </summary>
        /// <param name="theCase">The case to save</param>
        /// <returns>True if case saved successfully, false otherwise</returns>
        public bool SaveCaseInTran(INPCase theCase) {
            ITransaction tran = null;
            try {
                //tran = this.StartTransaction();
                if (SaveCase(theCase, tran)) {
                    //Add a reference from the case to related parties and notes - ID
                    if (theCase.RelatedParties != null) {
                        IList relPart = theCase.RelatedParties;
                        for (int i = 0; i < relPart.Count; i++) {
                            ((IRelatedParty) relPart[i]).CaseId = theCase.Id;
                        }
                        if (DeleteRelatedParties(theCase.Id, tran)) {
                            SaveRelatedParties(relPart, tran);
                        } else {
                            return false;
                        }
                    }

                    if (theCase.Notes != null) {
                        IList notes = theCase.Notes;
                        for (int i = 0; i < notes.Count; i++) {
                            ((INote) notes[i]).CaseId = theCase.Id;
                        }

                        SaveNotes(notes, tran);
                    }

                    if (theCase.Collaterals != null) {
                        IList collaterals = theCase.Collaterals;
                        for (int i = 0; i < collaterals.Count; i++) {
                            ((ICollateral) collaterals[i]).CaseID = theCase.Id;
                        }
                        if (DeleteCollaterals(theCase.Id, tran)) {
                            SaveCollaterals(collaterals, tran);
                        } else {
                            return false;
                        }
                    }

                    /*if (theCase.DynamicValues != null) 
					{
						if(this.DeleteCaseField(theCase.Id, tran) && theCase.DynamicValues != null && theCase.DynamicValues.Count > 0)
						{
							this.SaveCaseField(theCase.DynamicValues, theCase.Id, theCase.InsertedBy, theCase.UpdatedBy, tran);
						}
					}*/

                    if (theCase.DynamicValues != null && theCase.DynamicValues.Count > 0) {
                        DeleteCaseField(theCase.Id, tran);

                        SaveCaseField(theCase.DynamicValues, theCase.Id, theCase.InsertedBy, theCase.UpdatedBy, tran);
                    }
                }
                //this.CommitTransaction(tran);
                return true;
            } catch (Exception exception) {
                LogAndThrow.IocException("DeleteRatioFormula failed to return a value", exception, GetType());
                return false;
            } finally {
                session.Flush();
                session.Close();
            }
        }

        #endregion

        #region Transactional methods to save Case and included objects

        /// <summary>
        /// Saves given case to the database
        /// </summary>
        /// <param name="theCase">The case to save</param>
        /// <param name="tran">Current transaction</param>
        /// <returns>True if case saved successfully, false otherwise</returns>
        private bool SaveCase(INPCase theCase, ITransaction tran) {
            DbPersistArgs args = new DbPersistArgs();
            args.DbObject = theCase;
            return (bool) Execute(SaveOrUpdate, args);
        }

        /// <summary>
        /// Deletes all related parties of a case.
        /// </summary>
        /// <param name="caseID">Id of the case.</param>
        /// <param name="tran">Current transaction</param>
        /// <returns>True if successful.</returns>
        private bool DeleteRelatedParties(int caseID, ITransaction tran) {
            IList relatedParties = GetRelatedParties(caseID, tran);

            for (int i = 0; i < relatedParties.Count; i++) {
                DbDeleteArgs args = new DbDeleteArgs(relatedParties[i]);
                Execute(Delete, args);
            }
            return true;
        }

        /// <summary>
        /// Saves all related parties
        /// </summary>
        /// <param name="relatedParties">The related parties to save</param>
        /// <param name="tran">Current transaction</param>
        /// <returns>True if related parties saved succesfully, false otherwise</returns>
        private bool SaveRelatedParties(IList relatedParties, ITransaction tran) {
            for (int i = 0; i < relatedParties.Count; i++) {
                DbPersistArgs args = new DbPersistArgs();
                args.DbObject = relatedParties[i];
                Execute(Save, args);
            }
            return true;
        }

        /// <summary>
        /// Saves all notes
        /// </summary>
        /// <param name="notes">The notes to save</param>
        /// <param name="tran">Current transaction</param>
        /// <returns>True if notes saved succesfully, false otherwise</returns>
        private bool SaveNotes(IList notes, ITransaction tran) {
            for (int i = 0; i < notes.Count; i++) {
                DbPersistArgs args = new DbPersistArgs();
                args.DbObject = notes[i];
                Execute(SaveOrUpdate, args);
            }
            return true;
        }

        /// <summary>
        /// Delete all collaterals from the database
        /// </summary>
        /// <param name="caseId">Id of the case</param>
        /// <param name="tran">Current transaction</param>
        /// <returns>True if collaterals deleted successfully, false otherwise</returns> 
        private bool DeleteCollaterals(int caseId, ITransaction tran) {
            IList collaterals = GetCollaterals(caseId, tran);
            if (collaterals != null) {
                for (int i = 0; i < collaterals.Count; i++) {
                    DbDeleteArgs args = new DbDeleteArgs();
                    args.DbObject = collaterals[i];
                    Execute(Delete, args);
                }
            }
            return true;
        }

        /// <summary>
        /// Saves collaterals to the database
        /// </summary>
        /// <param name="collaterals">The collaterals to save</param>
        /// <param name="tran">Current transaction</param>
        /// <returns>True if collaterals saved successfully, false otherwise</returns>
        private bool SaveCollaterals(IList collaterals, ITransaction tran) {
            if (collaterals != null) {
                for (int i = 0; i < collaterals.Count; i++) {
                    DbPersistArgs args = new DbPersistArgs();
                    args.DbObject = collaterals[i];
                    Execute(Save, args);
                }
            }
            return true;
        }

        /// <summary>
        /// Deletes case field of a selected Case.
        /// </summary>
        /// <param name="caseID">ID of the case.</param>
        /// <param name="tran">Current transaction</param>
        /// <returns>True if the delete is successful.</returns>
        private bool DeleteCaseField(int caseID, ITransaction tran) {
            try {
                session = session;
                SqlConnection myConn = (SqlConnection) session.Connection;

                string sqlQuery = "delete from c_np_CaseFields where CaseID = " + caseID;

                SqlCommand myCommand = new SqlCommand(sqlQuery);
                myCommand.Connection = myConn;
                //tran.Enlist(myCommand);
                new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
            } catch (Exception) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Saves case fields of a given case to the database.
        /// </summary>
        /// <param name="fields">Fields to be saved.</param>
        /// <param name="caseID">ID of the case.</param>
        ///  <param name="insertedBy">ID of the inserter.</param>
        ///   <param name="updatedBy">ID of the updater.</param>
        /// <param name="tran">Current transaction</param>
        /// <returns>True if the save is successful.</returns>
        private bool SaveCaseField(Hashtable fields, int caseID, int insertedBy, int updatedBy, ITransaction tran) {
            try {
                session = session;
                SqlConnection myConn = (SqlConnection) session.Connection;

                string sqlQuery = "insert into c_np_CaseFields (CaseID, Inserted, InsertedBy, Updated, UpdatedBy, [";

                IEnumerator keys = fields.Keys.GetEnumerator();
                IEnumerator values = fields.Values.GetEnumerator();

                while (keys.MoveNext()) {
                    //if (keys.Current.ToString().IndexOf("CaseID, Inserted, InsertedBy, Updated, UpdatedBy") < 0)
                    if (!(keys.Current.ToString().ToLower().Equals("caseid")
                          || keys.Current.ToString().ToLower().Equals("inserted")
                          || keys.Current.ToString().ToLower().Equals("insertedby")
                          || keys.Current.ToString().ToLower().Equals("updated")
                          || keys.Current.ToString().ToLower().Equals("updatedby"))) {
                        sqlQuery += keys.Current + "],[";
                    }
                }
                sqlQuery = sqlQuery.TrimEnd(new char[2] {',', '['});
                sqlQuery += ") values ('" + caseID + "', getDate(), " + insertedBy + ", getDate(), " + updatedBy + ",'";

                keys.Reset();
                while (keys.MoveNext()) {
                    //if (keys.Current.ToString().IndexOf("CaseID, Inserted, InsertedBy, Updated, UpdatedBy") < 0)
                    if (!(keys.Current.ToString().ToLower().Equals("caseid")
                          || keys.Current.ToString().ToLower().Equals("inserted")
                          || keys.Current.ToString().ToLower().Equals("insertedby")
                          || keys.Current.ToString().ToLower().Equals("updated")
                          || keys.Current.ToString().ToLower().Equals("updatedby"))) {
                        sqlQuery += fields[keys.Current] + "','";
                    }
                }
                sqlQuery = sqlQuery.TrimEnd(new char[1] {'\''});
                sqlQuery = sqlQuery.TrimEnd(new char[1] {','});
                sqlQuery += ")";

                SqlCommand myCommand = new SqlCommand(sqlQuery);
                myCommand.Connection = myConn;
                //tran.Enlist(myCommand);
                new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
            } catch (Exception ex) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// All related parties of a given case.
        /// </summary>
        /// <param name="caseID">ID of the case.</param>
        /// <param name="tran">Current transaction.</param>
        /// <returns>List of related parties of a case.</returns>
        private IList GetRelatedParties(int caseID, ITransaction tran) {
            DbListArgs args = new DbListArgs(typeof (RelatedParty));
            args.DbExpressions.Add(Expression.Eq("CaseId", caseID));
            return (IList) Execute(List, args);
        }

        /// <summary>
        /// Retrieve collaterals based on case id
        /// </summary>
        /// <param name="caseId">Id of the case</param>
        /// <param name="tran">Current transaction</param>
        /// <returns>List of collaterals for specified case</returns>
        private IList GetCollaterals(int caseId, ITransaction tran) {
            DbListArgs args = new DbListArgs(typeof (Collateral));
            args.DbExpressions.Add(Expression.Eq("CaseID", caseId));

            return (IList) Execute(List, args);
        }

        #endregion

        /// <summary>
        /// Find parties that are related to a certain case.
        /// </summary>
        /// <param name="CIID">CIID</param>
        /// <returns></returns>
        protected IList FindRelatedParties(int CIID) {
            DbListArgs args = new DbListArgs(typeof (RelatedParty));

            //args.DbExpressions.Add(Expression.And(Expression.Eq("CreditinfoId", CIID), Expression.Eq("TypeId", type)));
            args.DbExpressions.Add(Expression.Eq("CreditinfoId", CIID));

            return (IList) Execute(List, args);
        }
    }
}