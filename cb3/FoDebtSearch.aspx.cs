using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CR.BLL;
using CreditWatch.BLL;
using CreditWatch.BLL.Localization;
using CreditWatch.BLL.np_Usage;
using UserAdmin.BLL.CIUsers;

using Cig.Framework.Base.Configuration;

namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for FoDebtSearch.
    /// </summary>
    public class FoDebtSearch : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly CPIFactory cpiFactory = new CPIFactory();
        private readonly cwFactory creditWatchFactory = new cwFactory();
        private readonly CRFactory crFactory = new CRFactory();
        protected Button btnClear;
        protected Button btnFind;
        private string culture;
        private bool EN;
        protected HtmlTableRow Errorr;
        protected HtmlTableCell headLtDDD;
        protected HtmlTableRow headLtDDDLine;
        protected Label lblAddress;
        protected Label lblAddressLabel;
        protected HtmlTableRow lblAddressRow;
        protected Label lblCity;
        protected Label lblCityLabel;
        protected HtmlTableRow lblCityRow;
        protected Label lblCreditinfoIDLabel;
        protected Label lblDateOfReport;
        protected Label lblDateOfReportTitle;
        protected Label lblDisclaimer;
        protected HtmlTableRow lblDisclaimerLine;
        protected Label lblEmail;
        protected Label lblEmailLabel;
        protected HtmlTableRow lblEmailRow;
        protected Label lblEstablishment;
        protected Label lblEstablishmentLabel;
        protected HtmlTableRow lblEstablishmentRow;
        protected Label lblFax;
        protected Label lblFaxLabel;
        protected HtmlTableRow lblFaxRow;
        protected Label lblLERStatusLabel;
        protected Label lblLtClaims;
        protected Label lblName;
        protected Label lblNameLabel;
        protected Label lblNationalID;
        protected Label lblNationalID1;
        protected Label lblNationalIDLabel;
        protected Label lblNationalIDlbl;
        protected Label lblNoClaimRegistered;
        protected Label lblOfficeAddress;
        protected Label lblOfficeAddressLabel;
        protected HtmlTableRow lblOfficeAddressRow;
        protected Label lblOneMonth;
        protected HtmlTableRow lblOneMonthLine;
        protected Label lblOperationLabel;
        protected Label lblPhone;
        protected Label lblPhoneLabel;
        protected HtmlTableRow lblPhoneRow;
        protected Label lblRecent;
        protected HtmlTableRow lblRecentLine;
        protected Label lblRegistration;
        protected Label lblRegistrationLabel;
        protected HtmlTableRow lblRegistrationRow;
        protected Label lblResultAddress;
        protected Label lblResultCity;
        protected Label lblResultCreditinfoID;
        protected Label lblResultLERStatus;
        protected Label lblResultName;
        protected Label lblResultNationalID;
        protected Label lblResultOperation;
        protected Label lblResultSLERStatus;
        protected Label lblSearch;
        protected Label lblSixMonths;
        protected HtmlTableRow lblSixMonthsLine;
        protected Label lblSLERStatusLabel;
        protected Label lblStatusLER;
        protected Label lblStatusLERLabel;
        protected HtmlTableRow lblStatusLERRow;
        protected Label lblStatusSLER;
        protected Label lblStatusSLERLabel;
        protected HtmlTableRow lblStatusSLERRow;
        protected Label lblThreeMonths;
        protected HtmlTableRow lblThreeMonthsLine;
        protected Label lblTimeStamp;
        protected Label lblType;
        protected Label lblTypeLabel;
        protected HtmlTableRow lblTypeRow;
        protected Label ldlToday;
        protected HtmlTableRow Line;
        protected HtmlAnchor linkBack;
        protected HtmlTableRow linkBackLine;
        protected LinkButton Linkbutton1;
        protected HtmlTableRow ltDDDELine;
        protected HtmlTableRow ltDDDPersonRepeaterRow;
        protected HtmlTableRow ltDDDRepeaterRow;
        private bool nativeCult;
        protected RadioButtonList rbtnlUserType;
        protected Repeater repeatLtDDD;
        protected Repeater repeatPersonLtDDD;
        protected HtmlTableRow Tr1;
        protected HtmlTableRow Tr2;
        protected HtmlTableRow trCompanyInfoRow;
        protected TextBox txtNationalID;

        private void Page_Load(object sender, EventArgs e) {
            AddEnterEvent();
            culture = Thread.CurrentThread.CurrentCulture.Name;
            var nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            if (!Page.IsPostBack) {
                rbtnlUserType.Items[0].Selected = true; //EMI
                LocalizeText();
                trCompanyInfoRow.Visible = false;
            }
        }

        private void AddEnterEvent() {
            var frm = FindControl("FoSearchForm");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void LocalizeText() {
            rbtnlUserType.Items[0].Text = rm.GetString("txtIndividual", ci);
            rbtnlUserType.Items[1].Text = rm.GetString("txtCompany", ci);
            lblNationalID.Text = rm.GetString("txtCode", ci);
            btnFind.Text = rm.GetString("txtSearch", ci);
            btnClear.Text = rm.GetString("txtClear", ci);
            lblNationalIDLabel.Text = rm.GetString("txtCompanyCode", ci);
            lblNameLabel.Text = rm.GetString("txtName", ci);
            lblTypeLabel.Text = rm.GetString("lbType", ci);
            lblStatusLERLabel.Text = rm.GetString("txtLERStatusLabel", ci);
            lblStatusSLERLabel.Text = rm.GetString("txtSLERStatusLabel", ci);
            lblCityLabel.Text = rm.GetString("txtCity", ci);
            lblPhoneLabel.Text = rm.GetString("lbPhone", ci);
            lblFaxLabel.Text = rm.GetString("lbFax", ci);
            lblAddressLabel.Text = rm.GetString("txtAddress", ci);
            lblOfficeAddressLabel.Text = rm.GetString("lbOfficeAddress", ci);
            lblEmailLabel.Text = rm.GetString("lbEmail", ci);
            lblEstablishmentLabel.Text = rm.GetString("lbEstablished", ci);
            lblRegistrationLabel.Text = rm.GetString("lbRegistered", ci);
            lblLtClaims.Text = rm.GetString("lbClaim", ci);
            headLtDDDLine.Visible = false;
            ltDDDELine.Visible = false;
            ltDDDRepeaterRow.Visible = false;
            lblRecentLine.Visible = false;
            lblSixMonthsLine.Visible = false;
            lblThreeMonthsLine.Visible = false;
            lblOneMonthLine.Visible = false;
            linkBackLine.Visible = false;
            lblDisclaimerLine.Visible = false;
            Line.Visible = false;
            Errorr.Visible = false;

            lblSearch.Text = rm.GetString("txtSearch", ci);
            ldlToday.Text = rm.GetString("txtDateOfReport", ci) + ":";
            lblTimeStamp.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
        }

        /// <summary>
        /// Finds company with given national id and displays 
        /// informations about that company
        /// </summary>
        /// <param name="nationalID">The national id for the company</param>
        private void GetCompanyInfoFromNationalID(string nationalID) {
            try {
                if (EN) {
                    using (var mySet = creditWatchFactory.GetENSearchDetailsList(Convert.ToInt32(nationalID))) {
                        // Session["dgSearchDetailsList"] = mySet.Tables[0];
                        lblNoClaimRegistered.Visible = mySet.Tables[0].Rows.Count == 0;

                        if (!IsPostBack) {
                            if (!creditWatchFactory.CheckForEmployee(Session["UserLoginName"].ToString())) {
                                LogThis(mySet);
                            }
                        }
                    }
                }
                    // Fer alltaf hingaš vegna žess aš EN breytan er alltaf sett "false" žvķ enska leitin var tekin śt...
                else {
                    using (
                        DataSet mySet = creditWatchFactory.GetSplitUpNativeSearchDetailsList(
                            Convert.ToInt32(nationalID))) {
                        // hmm žarf ég aš tékka į hverri töflu fyrir sig?
                        if (mySet.Tables[6].Rows.Count == 0) {
                            NoClaimRegistered();
                            //lblNoClaimRegistered.Visible = true;
                        } else {
                            Errorr.Visible = false;
                        }
                        //lblNoClaimRegistered.Visible = false;

                        DataView myLtDDDView = mySet.Tables[6].DefaultView;

                        myLtDDDView.Sort = "claim_date DESC";

                        repeatLtDDD.DataSource = myLtDDDView;
                        repeatLtDDD.DataBind();

                        if (repeatLtDDD.Items.Count > 0) {
                            ltDDDELine.Visible = true;
                            ltDDDRepeaterRow.Visible = true;
                            headLtDDD.Visible = true;
                            headLtDDDLine.Visible = true;
                            repeatLtDDD.Visible = true;
                        } else {
                            ltDDDELine.Visible = false;
                            ltDDDRepeaterRow.Visible = false;
                            headLtDDD.Visible = false;
                            headLtDDDLine.Visible = false;
                            repeatLtDDD.Visible = false;
                        }

                        if (!IsPostBack) {
                            if (!creditWatchFactory.CheckForEmployee(Session["UserLoginName"].ToString())) {
                                LogThis(mySet);
                            }
                        }
                    }
                }
            } catch (Exception err) {
                // Villu hent nišur ķ skrį ķ gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwClaimList.aspx caught, message is : " + err.Message, true);
            }
        }

        /// <summary>
        /// Finds company with given national id and displays 
        /// informations about that company
        /// </summary>
        /// <param name="nationalID">The national id for the company</param>
        private void GetIndividualInfoFromNationalID(string nationalID) {
            using (DataSet mySet = creditWatchFactory.GetPersonSearchDetailsList(Convert.ToInt32(nationalID))) {
                // hmm žarf ég aš tékka į hverri töflu fyrir sig?
                if (mySet.Tables[0].Rows.Count == 0) {
                    NoClaimRegistered();
                    //lblNoClaimRegistered.Visible = true;
                } else {
                    Errorr.Visible = false;
                }
                //lblNoClaimRegistered.Visible = false;

                DataView myLtDDDView = mySet.Tables[0].DefaultView;

                myLtDDDView.Sort = "claim_date DESC";

                repeatPersonLtDDD.DataSource = myLtDDDView;
                repeatPersonLtDDD.DataBind();

                if (repeatPersonLtDDD.Items.Count > 0) {
                    ltDDDELine.Visible = true;
                    ltDDDPersonRepeaterRow.Visible = true;
                    headLtDDD.Visible = true;
                    headLtDDDLine.Visible = true;
                    repeatLtDDD.Visible = true;

                    trCompanyInfoRow.Visible = true;
                    lblNationalIDLabel.Visible = false;
                    lblNationalID1.Visible = false;
                    ldlToday.Visible = true;
                    ldlToday.Text = rm.GetString("lbToday", ci);
                    lblTimeStamp.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                    lblTypeRow.Visible = false;
                    lblStatusLERRow.Visible = false;
                    lblStatusSLERRow.Visible = false;
                    lblCityRow.Visible = false;
                    lblPhoneRow.Visible = false;
                    lblFaxRow.Visible = false;
                    lblAddressRow.Visible = false;
                    lblOfficeAddressRow.Visible = false;
                    lblEmailRow.Visible = false;
                    lblEstablishmentRow.Visible = false;
                    lblRegistrationRow.Visible = false;
                } else {
                    ltDDDELine.Visible = false;
                    ltDDDPersonRepeaterRow.Visible = false;
                    headLtDDD.Visible = false;
                    headLtDDDLine.Visible = false;
                    repeatLtDDD.Visible = false;
                }

                if (!IsPostBack) {
                    if (!creditWatchFactory.CheckForEmployee(Session["UserLoginName"].ToString())) {
                        LogThis(mySet);
                    }
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e) { ClearForm(); }

        private void ClearForm() {
            txtNationalID.Text = "";
            Errorr.Visible = false;
            //lblNoClaimRegistered.Visible=false;
            trCompanyInfoRow.Visible = false;

            headLtDDDLine.Visible = false;
            ltDDDELine.Visible = false;
            ltDDDRepeaterRow.Visible = false;
            lblRecentLine.Visible = false;
            lblSixMonthsLine.Visible = false;
            lblThreeMonthsLine.Visible = false;
            lblOneMonthLine.Visible = false;
            linkBackLine.Visible = false;
            lblDisclaimerLine.Visible = false;
            Line.Visible = false;
            //rbtnlUserType.Items[0].Selected=true;
            ltDDDPersonRepeaterRow.Visible = false;
        }

        private void btnFind_Click(object sender, EventArgs e) {
            //Clear old values
            lblNationalID1.Text = "";
            lblName.Text = "";
            lblTimeStamp.Text = "";
            lblType.Text = "";
            lblStatusLER.Text = "";
            lblStatusSLER.Text = "";
            lblCity.Text = "";
            lblPhone.Text = "";
            lblFax.Text = "";
            lblAddress.Text = "";
            lblOfficeAddress.Text = "";
            lblEmail.Text = "";
            lblEstablishment.Text = "";
            lblRegistration.Text = "";
            Errorr.Visible = false;
            //lblNoClaimRegistered.Visible=false;

            if (txtNationalID.Text.Trim() != "") {
                int ilgis = txtNationalID.Text.Trim().Length;
                if (rbtnlUserType.Items[0].Selected & ilgis.Equals(11)) {
                    int ciid = cpiFactory.GetCIIDByNationalID(txtNationalID.Text);
                    if (ciid != -1) {
                        GetIndividualInfoFromNationalID(Convert.ToString(ciid));
                    } else {
                        trCompanyInfoRow.Visible = true;
                        lblNationalIDLabel.Visible = false;
                        lblNationalID1.Visible = false;
                        ldlToday.Visible = true;
                        ldlToday.Text = rm.GetString("lbToday", ci);
                        lblTimeStamp.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                        lblTypeRow.Visible = false;
                        lblStatusLERRow.Visible = false;
                        lblStatusSLERRow.Visible = false;
                        lblCityRow.Visible = false;
                        lblPhoneRow.Visible = false;
                        lblFaxRow.Visible = false;
                        lblAddressRow.Visible = false;
                        lblOfficeAddressRow.Visible = false;
                        lblEmailRow.Visible = false;
                        lblEstablishmentRow.Visible = false;
                        lblRegistrationRow.Visible = false;

                        NoClaimRegistered();
                    }
                } else if (rbtnlUserType.Items[1].Selected & (ilgis.Equals(7) || ilgis.Equals(9))) {
                    CompanyLT theCompany = creditWatchFactory.GetCompanyReport(Convert.ToInt32(txtNationalID.Text));
                    if (theCompany != null) {
                        trCompanyInfoRow.Visible = true;

                        lblNationalIDLabel.Visible = true;
                        lblNationalID1.Visible = true;
                        lblNationalIDLabel.Text = rm.GetString("lbNationalID", ci);
                        lblNationalID1.Text = txtNationalID.Text;

                        ldlToday.Text = rm.GetString("lbToday", ci);
                        lblTimeStamp.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                        if (theCompany.NameNative != null) {
                            lblTypeRow.Visible = true;
                            lblNameLabel.Visible = true;
                            lblName.Visible = true;
                            lblNameLabel.Text = rm.GetString("lbName", ci);
                            lblName.Text = theCompany.NameNative;
                        } else {
                            lblNameLabel.Visible = false;
                        }

                        if (theCompany.TypeNative != null) {
                            lblTypeRow.Visible = true;
                            lblTypeLabel.Visible = true;
                            lblType.Visible = true;
                            lblTypeLabel.Text = rm.GetString("lbType", ci);
                            lblType.Text = theCompany.TypeNative;
                        } else {
                            lblTypeRow.Visible = false;
                        }

                        if (theCompany.LERStatusID > 0) {
                            lblStatusLERRow.Visible = true;
                            lblStatusLERLabel.Visible = true;
                            lblStatusLER.Visible = true;
                            lblStatusLERLabel.Text = rm.GetString("lbStatusLER", ci);
                            lblStatusLER.Text = crFactory.GetStatusAsString(theCompany.LERStatusID, nativeCult);
                        } else {
                            lblStatusLERRow.Visible = false;
                        }

                        if (theCompany.SLERStatusID > 0) {
                            lblStatusSLERRow.Visible = true;
                            lblStatusSLERLabel.Visible = true;
                            lblStatusSLER.Visible = true;
                            lblStatusSLERLabel.Text = rm.GetString("lbStatusSLER", ci);
                            lblStatusSLER.Text = crFactory.GetStatusAsString(theCompany.SLERStatusID, nativeCult);
                        } else {
                            lblStatusSLERRow.Visible = false;
                        }

                        if (theCompany.CityNative != null) {
                            lblCityRow.Visible = true;
                            lblCityLabel.Visible = true;
                            lblCity.Visible = true;
                            lblCityLabel.Text = rm.GetString("txtCity", ci);
                            lblCity.Text = theCompany.CityNative;
                        } else {
                            lblCityRow.Visible = false;
                        }

                        if (theCompany.Tel != null) {
                            lblPhoneRow.Visible = true;
                            lblPhoneLabel.Visible = true;
                            lblPhone.Visible = true;
                            lblPhoneLabel.Text = rm.GetString("lbPhone", ci);
                            lblPhone.Text = theCompany.Tel;
                        } else {
                            lblPhoneRow.Visible = false;
                        }

                        if (theCompany.Fax != null) {
                            lblFaxRow.Visible = true;
                            lblFaxLabel.Visible = true;
                            lblFax.Visible = true;
                            lblFaxLabel.Text = rm.GetString("lbFax", ci);
                            lblFax.Text = theCompany.Fax;
                        } else {
                            lblFaxRow.Visible = false;
                        }

                        if (theCompany.JurAddress != null) {
                            lblAddressRow.Visible = true;
                            lblAddressLabel.Visible = true;
                            lblAddress.Visible = true;
                            lblAddressLabel.Text = rm.GetString("lbAddress", ci);
                            lblAddress.Text = theCompany.JurAddress;
                        } else {
                            lblAddressRow.Visible = false;
                        }

                        if (theCompany.OfficeAddress != null) {
                            lblOfficeAddressRow.Visible = true;
                            lblOfficeAddressLabel.Visible = true;
                            lblOfficeAddress.Visible = true;
                            lblOfficeAddressLabel.Text = rm.GetString("lbOfficeAddress", ci);
                            lblOfficeAddress.Text = theCompany.OfficeAddress;
                        } else {
                            lblOfficeAddressRow.Visible = false;
                        }

                        if (theCompany.Email != null) {
                            lblEmailRow.Visible = true;
                            lblEmailLabel.Visible = true;
                            lblEmail.Visible = true;
                            lblEmailLabel.Text = rm.GetString("lbEmail", ci);
                            lblEmail.Text = theCompany.Email;
                        } else {
                            lblEmailRow.Visible = false;
                        }

                        if (theCompany.Established != null) {
                            lblEstablishmentRow.Visible = true;
                            lblEstablishmentLabel.Visible = true;
                            lblEstablishment.Visible = true;
                            lblEstablishmentLabel.Text = rm.GetString("lbEstablished", ci);
                            lblEstablishment.Text = Convert.ToDateTime(theCompany.Established).ToShortDateString();
                        } else {
                            lblEstablishmentRow.Visible = false;
                        }

                        if (theCompany.Registered != null) {
                            lblRegistrationRow.Visible = true;
                            lblRegistrationLabel.Visible = true;
                            lblRegistration.Visible = true;
                            lblRegistrationLabel.Text = rm.GetString("lbRegistered", ci);
                            lblRegistration.Text = Convert.ToDateTime(theCompany.Registered).ToShortDateString();
                        } else {
                            lblRegistrationRow.Visible = false;
                        }

                        if (theCompany.UniqueID != 0) {
                            //int ciid = cpiFactory.GetCIIDByNationalID(txtNationalID.Text);
                            int ciid = cpiFactory.GetCIIDByNationalID(theCompany.UniqueID.ToString());
                            GetCompanyInfoFromNationalID(Convert.ToString(ciid));
                        } else {
                            CompanyNotFound();
                        }

                        //int ciid = cpiFactory.GetCIIDByNationalID(txtNationalID.Text);
                        //GetCompanyInfoFromNationalID(Convert.ToString(ciid));	
                    } else {
                        CompanyNotFound();
                    }
                } else {
                    WrongInput();
                }
            }
        }

        private void rbtnlUserType_SelectedIndexChanged(object sender, EventArgs e) {
            //ClearForm();
            if (rbtnlUserType.Items[0].Selected) {
                txtNationalID.Text = "";
                Errorr.Visible = false;
                //lblNoClaimRegistered.Visible=false;
                trCompanyInfoRow.Visible = false;

                headLtDDDLine.Visible = false;
                ltDDDELine.Visible = false;
                ltDDDRepeaterRow.Visible = false;
                lblRecentLine.Visible = false;
                lblSixMonthsLine.Visible = false;
                lblThreeMonthsLine.Visible = false;
                lblOneMonthLine.Visible = false;
                linkBackLine.Visible = false;
                lblDisclaimerLine.Visible = false;
                Line.Visible = false;
            } else {
                txtNationalID.Text = "";
                Errorr.Visible = false;
                //lblNoClaimRegistered.Visible=false;
                trCompanyInfoRow.Visible = false;

                headLtDDDLine.Visible = false;
                ltDDDELine.Visible = false;
                ltDDDRepeaterRow.Visible = false;
                lblRecentLine.Visible = false;
                lblSixMonthsLine.Visible = false;
                lblThreeMonthsLine.Visible = false;
                lblOneMonthLine.Visible = false;
                linkBackLine.Visible = false;
                lblDisclaimerLine.Visible = false;
                Line.Visible = false;
            }
        }

        private void CompanyNotFound() {
            Errorr.Visible = true;
            lblNoClaimRegistered.Visible = true;
            lblNoClaimRegistered.Text = rm.GetString("txtNoCompanyFound", ci);
            //trCompanyInfoRow.Visible=false;
        }

        private void NoClaimRegistered() {
            Errorr.Visible = true;
            lblNoClaimRegistered.Visible = true;
            lblNoClaimRegistered.Text = rm.GetString("txtNoClaimRegistered", ci);
            //trCompanyInfoRow.Visible=false;
        }

        private void WrongInput() {
            Errorr.Visible = true;
            lblNoClaimRegistered.Visible = true;
            lblNoClaimRegistered.Text = rm.GetString("txtWrongInput", ci);
            //trCompanyInfoRow.Visible=false;
        }

        private void LogThis(DataSet mySet) {
            var myLog = new np_UsageBLLC();

            try {
                myLog.CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]);
                // UserID notaš til aš ašgreina fęrslur ķ loggun į milli notenda... tekur viš af CreditInfoID
                // Sem notast framvegis til žess aš ašgreina persónur og fyrirtęki (JBA - 14.10.2003)
                myLog.UserID = Convert.ToInt32(Session["UserLoginID"]);
                myLog.QueryType = 20;
                myLog.Query = Session["cwClaimSearchCreditInfoID"].ToString();
                myLog.ResultCount = mySet.Tables[0].Rows.Count;
                myLog.IP = Request.ServerVariables["REMOTE_ADDR"];

                creditWatchFactory.AddNewUsageLog(myLog);
            } catch (Exception err) {
                Logger.WriteToLog(
                    "Logging exception (probable 0 error on CreditInfoID) on cwClaimList.aspx caught, message is : " +
                    err.Message,
                    true);
                Server.Transfer("error.aspx?err=1");
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rbtnlUserType.SelectedIndexChanged += new System.EventHandler(this.rbtnlUserType_SelectedIndexChanged);
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}