@echo off

SET tooldir=C:\Build
SET dir=C:\CIG\CreditInfoGroup_mt\trunk\CreditInfoGroup_mt
SET dst=C:\Inetpub\wwwroot\cig.mt\
SET outputdir=%tooldir%\cig.mt

rmdir /S /Q "%outputdir%"
mkdir "%outputdir%"

REM Publish BackOffice

rmdir "%outputdir%\CreditInfoGroup_mt"
mkdir "%outputdir%\CreditInfoGroup_mt"

xcopy "%dir%\*.*" "%outputdir%\CreditInfoGroup_mt\" /F /Y /H /R /I /S /E

del "%outputdir%\CreditInfoGroup_mt\*.sln" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.suo" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.vssscc" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.cs" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.aspx.cs" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.aspx.resx" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.csproj" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.csproj.vspscc" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.csproj.webinfo" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.csproj.user" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt\*.scc" /S /F
del "%outputdir%\CreditInfoGroup_mt\*.pdb" /S /F
del "%outputdir%\CreditInfoGroup_mt\*.resx" /S /F
del "%outputdir%\CreditInfoGroup_mt\*.inf" /S /F

REM rmdir /S /Q "%outputdir%\CreditInfoGroup_mt\_UpgradeReport_Files"

del "%outputdir%\CreditInfoGroup_mt\CreditinfoGroup.xml"
del "%outputdir%\CreditInfoGroup_mt\*.bat"
del "%outputdir%\CreditInfoGroup_mt\UpgradeLog.XML"
del "%outputdir%\CreditInfoGroup_mt\Web.config.Malta"

REM Publish FrontOffice

rmdir "%outputdir%\CreditInfoGroup_mt_fo"
mkdir "%outputdir%\CreditInfoGroup_mt_fo"

xcopy "%dir%\*.*" "%outputdir%\CreditInfoGroup_mt_fo\" /F /Y /H /R /I /S /E

del "%outputdir%\CreditInfoGroup_mt_fo\*.sln" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.suo" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.vssscc" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.cs" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.aspx.cs" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.aspx.resx" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.csproj" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.csproj.vspscc" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.csproj.webinfo" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.csproj.user" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\*.scc" /S /F
del "%outputdir%\CreditInfoGroup_mt_fo\*.pdb" /S /F
del "%outputdir%\CreditInfoGroup_mt_fo\*.resx" /S /F
del "%outputdir%\CreditInfoGroup_mt_fo\*.inf" /S /F

rmdir /S /Q "%outputdir%\CreditInfoGroup_mt_fo\_ReSharper.CreditInfoGroup"
rmdir /S /Q "%outputdir%\CreditInfoGroup_mt_fo\_UpgradeReport_Files"

REM rmdir /S /Q "%outputdir%\CreditInfoGroup_mt\_UpgradeReport_Files"

del "%outputdir%\CreditInfoGroup_mt_fo\Default.aspx" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\error.aspx" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\Logon.aspx" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\Web.config.Malta" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\NoAuthorization.aspx" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\remove_fo.bat" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\UpgradeLog.XML" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\Billing\Billing.xml" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\DataPicker.aspx" /S /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\CPI\*.*" /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\CR\TemplateReport.aspx" /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\CR\SendReport.aspx" /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\CR\Report.aspx" /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\CPI\*.aspx" /F /Q
del "%outputdir%\CreditInfoGroup_mt_fo\NPayments\*.aspx" /F /Q

rmdir /S /Q "%outputdir%\CreditInfoGroup_mt\_ReSharper.CreditInfoGroup"
rmdir /S /Q "%outputdir%\CreditInfoGroup_mt\_UpgradeReport_Files"

REM CONFIGS

xcopy "%dir%\config\frontoffice\Web.config" "%outputdir%\CreditInfoGroup_mt_fo\" /F /Y /H /R /I /S /E
xcopy "%dir%\config\backoffice\Web.config" "%outputdir%\CreditInfoGroup_mt\" /F /Y /H /R /I /S /E

rem Deploy to server

REM rmdir /S /Q "%dst%\CreditInfoGroup_mt"
REM rmdir /S /Q "%dst%\CreditInfoGroup_mt_fo"

xcopy "%outputdir%\*" "%dst%" /F /Y /H /R /I /S /E

iisreset

pause
