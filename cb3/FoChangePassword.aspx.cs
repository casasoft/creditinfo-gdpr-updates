using System;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;
using UserAdmin.BLL;
using UserAdmin.BLL.auUsers;

using Cig.Framework.Base.Configuration;


namespace UserAdmin {
    /// <summary>
    /// Summary description for FoChangePassword.
    /// </summary>
    public class FoChangePassword : Page {
        protected Button btnChange;
        private CultureInfo ci;
        protected CompareValidator cpvPasswords;
        private string culture;
        private uaFactory factory;
        protected HyperLink hlMail;
        protected Label lblCurrentPasswordTitle;
        protected Label lblEmail;
        protected Label lblEmailTitle;
        protected Label lblHeader;
        protected Label lblMessage;
        protected Label lblNewPasswordTitle;
        protected Label lblUsername;
        protected Label lblUsernameTitle;
        protected Label lblVerifyPasswordTitle;
        protected RequiredFieldValidator rfvNewPassword;
        private ResourceManager rm;
        protected TextBox txtCurrentPassword;
        protected TextBox txtNewPassword;
        protected TextBox txtVerifyPassword;
        private auUsersBLLC user;
        protected ValidationSummary valSummary;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "3200";

            if (!User.IsInRole(Page.ID)) {
                Server.Transfer("FoNoAuthorization.aspx");
            }

            AddEnterEvent();
            culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {}

            factory = new uaFactory();
            lblMessage.Visible = false;

            if (!Page.IsPostBack) {
                LocalizeText();
            }

            LoadUser();
        }

        private void AddEnterEvent() {
            var frm = FindControl("frmChangePassword");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void LocalizeText() {
            // Labels
            lblHeader.Text = rm.GetString("txtChangePassword", ci);

            lblUsernameTitle.Text = rm.GetString("txtUserName", ci);
            lblEmailTitle.Text = rm.GetString("txtEmail", ci);

            lblCurrentPasswordTitle.Text = rm.GetString("txtPassword", ci);
            lblNewPasswordTitle.Text = rm.GetString("txtPasswordNew", ci);
            lblVerifyPasswordTitle.Text = rm.GetString("txtPasswordVerify", ci);

            // Buttons
            btnChange.Text = rm.GetString("txtChange", ci);

            // Validators
            cpvPasswords.ErrorMessage = rm.GetString("lblPasswordDontMatch", ci);
            rfvNewPassword.ErrorMessage = rm.GetString("lblPasswordShort", ci);
        }

        private void LoadUser() {
            user = factory.GetUser(int.Parse(Session["UserLoginID"].ToString()));

            lblUsername.Text = user.UserName;
            lblEmail.Text = user.Email;
        }

        private void btnChange_Click(object sender, EventArgs e) {
            if (factory.VerifyPassword(user.UserName, txtCurrentPassword.Text)) {
                string strPasswordHash = uaFactory.CreatePasswordHash(txtNewPassword.Text, user.Salt);
                factory.UpdateUserPassword(strPasswordHash, user.Salt, user.UserName);

                lblMessage.Text = rm.GetString("lblConfirmPasswordChanged", ci);
                lblMessage.CssClass = "confirm_text";
                lblMessage.Visible = true;
            } else {
                lblMessage.Text = rm.GetString("lblOldPasswordIncorrect", ci);
                lblMessage.CssClass = "error_text";
                lblMessage.Visible = true;
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}