using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using UserAdmin.Localization;

using Cig.Framework.Base.Configuration;
using System.Collections.Generic;

namespace UserAdmin
{
    /// <summary>
    /// Summary description for NewCIUserC.
    /// </summary>
    public class NewCIUserC : Page
    {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        private int _departmentID = -1;
        protected Button btCancel;
        protected Button btnCity2Search;
        protected Button btnCitySearch;
        protected Button btnContinue;
        protected Button btnSave;
        protected int creditInfoID = -1;
        protected CustomValidator cvCompanyEstablished;
        protected DropDownList ddCity1;
        protected DropDownList ddCity2;
        protected DropDownList ddCompanyNace;
        protected DropDownList ddIDNumberType1;
        protected DropDownList ddIDNumberType2;
        protected DropDownList ddOrgStatus;
        private bool EN;
        protected bool isCreatingSubscriber;
        protected bool isUpdatingUser;
        protected Label Label1;
        protected Label Label2;
        protected Label Label4;
        protected Label Label5;
        protected Label lbAddr1InfoEN;
        protected Label lbAddress1;
        protected Label lbAddress1EN;
        protected Label lbAddress1Info;
        protected Label lbAddress2;
        protected Label lbAddress2EN;
        protected Label lbAddress2Info;
        protected Label lbAddress2InfoEN;
        protected Label lbCity1;
        protected Label lbCity2;
        protected Label lbCompanyName;
        protected Label lbCompanyNameEN;
        protected Label lbCompFunction;
        protected Label lbCompURL;
        protected Label lbDebtorCIID;
        protected Label lbEmail;
        protected Label lbEstablished;
        protected Label lbFaxNumber;
        protected Label lbIDNumber1;
        protected Label lbIDNumber2;
        protected Label lblAddressOne;
        protected Label lblAddressTwo;
        protected Label lbLastContacted;
        protected Label lblCity2Search;
        protected Label lblCitySearch;
        protected Label lblError;
        protected Label lblIsSearchable;
        protected Label lblNote;
        protected Label lblOtherInfo;
        protected Label lblRegistered;
        protected Label lbNewCompany;
        protected Label lbOrg_status;
        protected Label lbPhoneNumber;
        protected Label lbPostalCode;
        protected Label lbPostalCode2;
        protected Label lbPostBox;
        protected Label lbPostBox2;
        protected Label lbType1;
        protected Label lbType2;
        protected string nationalID = "";
        protected HtmlForm newuser;
        protected bool newUser;
        protected RadioButton rbtIsSearchableFalse;
        protected RadioButton rbtIsSearchableTrue;
        protected RequiredFieldValidator rfvCompanyNameNative;
        protected RequiredFieldValidator rfvOrgStatus;
        protected RequiredFieldValidator rfvCompanyEstablished;
        protected RequiredFieldValidator rfvCity;
        protected int subscriberCIID = -1;
        protected TextBox tbAddress1EN;
        protected TextBox tbAddress1InfoEN;
        protected TextBox tbAddress1InfoN;
        protected TextBox tbAddress1Native;
        protected TextBox tbAddress2EN;
        protected TextBox tbAddress2InfoEN;
        protected TextBox tbAddress2InfoN;
        protected TextBox tbAddress2Native;
        protected TextBox tbCompanyCIID;
        protected TextBox tbCompanyEstablished;
        protected TextBox tbCompanyLContacted;
        protected TextBox tbCompanyNameEN;
        protected TextBox tbCompanyNameNative;
        protected TextBox tbCompanyURL;
        protected TextBox tbEmail;
        protected TextBox tbIDNumber1;
        protected TextBox tbIDNumber2;
        protected TextBox tbNote;
        protected TextBox tbPhoneNumber1;
        protected TextBox tbPhoneNumber2;
        protected TextBox tbPostalCode1;
        protected TextBox tbPostalCode2;
        protected TextBox tbPostBox1;
        protected TextBox tbPostBox2;
        protected TextBox tbRegistered;
        protected TextBox txtCity2Search;
        protected TextBox txtCitySearch;
        protected bool update;
        protected ValidationSummary ValidationSummary1;
        protected DataGrid dgVatNumbers;
        protected Panel tblVatNumbers;
        protected CheckBox chkdoNotDelete;

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                subscriberCIID = int.Parse(Request["SubscriberCIID"]);
            }
            catch
            {
                subscriberCIID = -1;
            }
            try
            {
                creditInfoID = int.Parse(Request["CreditInfoID"]);
            }
            catch
            {
                creditInfoID = -1;
            }
            try
            {
                _departmentID = int.Parse(Request["Department"]);
            }
            catch { }
            try
            {
                update = bool.Parse(Request["Update"]);
            }
            catch
            {
                update = false;
            }
            try
            {
                isUpdatingUser = bool.Parse(Request["UpdateUser"]);
            }
            catch
            {
                isUpdatingUser = false;
            }
            try
            {
                nationalID = Request["NationalID"];
            }
            catch
            {
                nationalID = "";
            }
            try
            {
                isCreatingSubscriber = bool.Parse(Request["CreateSubscriber"]);
            }
            catch
            {
                isCreatingSubscriber = false;
            }

            if (subscriberCIID == -1 && !isCreatingSubscriber && !isUpdatingUser)
            {
                newUser = true;
            }
            //Server.Transfer("FindSubscribers.aspx");

            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US"))
            {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            lblError.Visible = false;

            Page.ID = "112";
            if (!IsPostBack)
            {
                LocalizeText();
                InitDDBoxes();
                initControls(update);
            }
        }

        private void initControls(bool update)
        {
            btnContinue.Enabled = false;
            if (!newUser)
            {
                if (update)
                {
                    if (creditInfoID != -1)
                    {
                        Company myComp = myFactory.GetCompany(creditInfoID);
                        tbCompanyCIID.Text = creditInfoID.ToString();
                        tbCompanyNameNative.Text = myComp.NameNative;
                        tbCompanyNameEN.Text = myComp.NameEN;
                        tbEmail.Text = myComp.Email;
                        tbCompanyURL.Text = myComp.URL;
                        tbCompanyEstablished.Text = myComp.Established.ToString();
                        tbNote.Text = myComp.Note;
                        rbtIsSearchableTrue.Checked = myComp.IsSearchable;
                        rbtIsSearchableFalse.Checked = !myComp.IsSearchable;
                        chkdoNotDelete.Checked = myComp.DoNotDelete;
                        if ((myComp.Registered > DateTime.MinValue) && (myComp.Registered < DateTime.MaxValue))
                        {
                            tbRegistered.Text = myComp.Registered.ToShortDateString();
                        }
                        if (myComp.LastContacted > DateTime.MinValue)
                        {
                            tbCompanyLContacted.Text = myComp.LastContacted.ToString();
                        }

                        if (myComp.Org_status_code != -1)
                        {
                            try
                            {
                                ddOrgStatus.SelectedValue = myComp.Org_status_code.ToString();
                            }
                            catch { }
                        }

                        if (myComp.FuncID != "")
                        {
                            try
                            {
                                ddCompanyNace.SelectedValue = myComp.FuncID;
                            }
                            catch { }
                        }

                        initMultiItems(myComp);

                        if (!isUpdatingUser)
                        {
                            btnContinue.Enabled = true;
                        }

                        this.getVatNumbers(creditInfoID);
                    }
                }
                else
                {
                    if (nationalID != "")
                    {
                        //Load user data from national registry
                        if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True")
                        {
                            Company myComp = myFactory.GetCompanyFromNationalRegistry(nationalID);
                            tbIDNumber1.Text = myComp.NationalID;
                            ddIDNumberType1.SelectedIndex = 0;

                            tbCompanyNameNative.Text = myComp.NameNative;
                            if (myComp.Address.Count > 0)
                            {
                                var address = (Address)myComp.Address[0];
                                tbAddress1Native.Text = address.StreetNative;
                                tbPostalCode1.Text = address.PostalCode;
                            }
                            for (int i = 0; i < myComp.Number.Count; i++)
                            {
                                var number = (PhoneNumber)myComp.Number[0];
                                switch (number.NumberTypeID)
                                {
                                    case 1:
                                        tbPhoneNumber1.Text = number.Number;
                                        break;
                                    case 4:
                                        tbPhoneNumber2.Text = number.Number;
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void initMultiItems(CreditInfoUser myComp)
        {
            ArrayList arrAdd = myComp.Address;
            ArrayList arrIDNum = myComp.IDNumbers;
            ArrayList arrPNum = myComp.Number;
            Control frm = FindControl("_newuser");
            for (int i = 1; i <= arrAdd.Count; i++)
            {
                var myAddr = (Address)arrAdd[i - 1];

                foreach (Control ctrl in frm.Controls)
                {
                    if (ctrl is TextBox)
                    {
                        if ((ctrl).ID == "tbAddress" + i + "Native")
                        {
                            ((TextBox)ctrl).Text = myAddr.StreetNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "EN")
                        {
                            ((TextBox)ctrl).Text = myAddr.StreetEN;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "InfoN")
                        {
                            ((TextBox)ctrl).Text = myAddr.OtherInfoNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "InfoEN")
                        {
                            ((TextBox)ctrl).Text = myAddr.OtherInfoEN;
                        }
                        if ((ctrl).ID == "tbPostalCode" + i)
                        {
                            ((TextBox)ctrl).Text = myAddr.PostalCode;
                        }
                        if ((ctrl).ID == "tbPostBox" + i)
                        {
                            ((TextBox)ctrl).Text = myAddr.PostBox;
                        }
                    }
                    if (ctrl is DropDownList)
                    {
                        if ((ctrl).ID == "ddCity" + i)
                        {
                            int cityID = myAddr.CityID;
                            if (cityID > -1)
                            {
                                string cityName = myFactory.GetCityName(cityID, EN);
                                if (!string.IsNullOrEmpty(cityName))
                                {
                                    ((DropDownList)ctrl).Items.Add(new ListItem(cityName, cityID.ToString()));
                                    ((DropDownList)ctrl).SelectedValue = cityID.ToString();
                                }
                            }
                            //((DropDownList)ctrl).SelectedValue = myAddr.CityID.ToString();
                        }
                        //((DropDownList)ctrl).SelectedValue = myAddr.CityID.ToString();
                    }
                }
            }
            for (int i = 1; i <= arrIDNum.Count; i++)
            {
                var myIDNum = (IDNumber)arrIDNum[i - 1];

                foreach (Control ctrl in frm.Controls)
                {
                    if (ctrl is TextBox)
                    {
                        if ((ctrl).ID == "tbIDNumber" + i)
                        {
                            ((TextBox)ctrl).Text = myIDNum.Number;
                        }
                    }
                    if (ctrl is DropDownList)
                    {
                        if ((ctrl).ID == "ddIDNumberType" + i)
                        {
                            ((DropDownList)ctrl).SelectedValue = myIDNum.NumberTypeID.ToString();
                        }
                    }
                }
            }
            for (int i = 1; i <= arrPNum.Count; i++)
            {
                var myPNum = (PhoneNumber)arrPNum[i - 1];

                foreach (Control ctrl in frm.Controls)
                {
                    if (ctrl is TextBox)
                    {
                        if ((ctrl).ID == "tbPhoneNumber" + i)
                        {
                            ((TextBox)ctrl).Text = myPNum.Number;
                        }
                    }
                    if (ctrl is DropDownList)
                    {
                        if ((ctrl).ID == "ddPhoneNumber" + i + "Type")
                        {
                            ((DropDownList)ctrl).SelectedValue = myPNum.NumberTypeID.ToString();
                        }
                    }
                }
            }
        }

        private void InitDDBoxes()
        {
            BindIDNumberTypes();
            BindCompanyFunction();
            BindOrgStatus();
        }

        private void BindIDNumberTypes()
        {
            DataSet idNumberType = myFactory.GetIDNumberTypesAsDataSet();

            /*	this.ddNumberType1.DataSource = idNumberType;
			this.ddNumberType1.DataTextField = "TypeEN";
			this.ddNumberType1.DataBind();
		*/
            ddIDNumberType1.DataSource = idNumberType;
            ddIDNumberType1.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddIDNumberType1.DataValueField = "NumberTypeID";
            ddIDNumberType1.DataBind();

            ddIDNumberType2.DataSource = idNumberType;
            ddIDNumberType2.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddIDNumberType2.DataValueField = "NumberTypeID";
            ddIDNumberType2.DataBind();
        }

        private void BindCompanyFunction()
        {
            DataSet companyFunctionSet = myFactory.GetCompanyNaceCodes();
            if (EN)
            {
                companyFunctionSet.Tables[0].DefaultView.Sort = "DescriptionShortEN";
                ddCompanyNace.DataSource = companyFunctionSet.Tables[0].DefaultView;
                ddCompanyNace.DataTextField = "DescriptionShortEN";
            }
            else
            {
                companyFunctionSet.Tables[0].DefaultView.Sort = "DescriptionShortNative";
                ddCompanyNace.DataSource = companyFunctionSet.Tables[0].DefaultView;
                ddCompanyNace.DataTextField = "DescriptionShortNative";
            }
            ddCompanyNace.DataValueField = "FuncID";
            ddCompanyNace.DataBind();
            // if company function order key in config then order the company function list ...
            if (CigConfig.Configure("lookupsettings.orderedCompanyFunctionList") != null)
            {
                OrderCompanyFunctionList(CigConfig.Configure("lookupsettings.orderedCompanyFunctionList"));
            }
            // is this right!?
            Session["CompanyFunctionSet"] = companyFunctionSet;
        }

        private void OrderCompanyFunctionList(string orderList)
        {
            string[] strArray = orderList.Split(new[] { ',' });
            var liItems = new ListItem[orderList.Length];
            int index = 0;
            foreach (string id in strArray)
            {
                ListItem li = ddCompanyNace.Items.FindByValue(id); //.lbxBanks.Items.FindByValue(id);
                ddCompanyNace.Items.Remove(li);
                ddCompanyNace.Items.Insert(index, li);
                index++;
            }
        }

        private void BindOrgStatus()
        {
            DataSet orgStatusSet = myFactory.GetOrgStatus();
            ddOrgStatus.DataSource = orgStatusSet;
            ddOrgStatus.DataTextField = EN ? "nativeEN" : "nameNative";
            ddOrgStatus.DataValueField = "id";
            ddOrgStatus.DataBind();
        }

        private void BindCityList(DataSet citySet)
        {
            ddCity1.DataSource = citySet;
            ddCity1.DataTextField = EN ? "NameEN" : "NameNative";
            ddCity1.DataValueField = "CityID";
            ddCity1.DataBind();
        }

        private void BindCity2List(DataSet citySet)
        {
            ddCity2.DataSource = citySet;
            ddCity2.DataTextField = EN ? "NameEN" : "NameNative";
            ddCity2.DataValueField = "CityID";
            ddCity2.DataBind();
        }

        // get proper NaceCode by selected item from the combobox
        private String GetSelectedNaceCode(int index)
        {
            if (Session["CompanyFunctionSet"] != null)
            {
                var companyFunctionSet = (DataSet)Session["CompanyFunctionSet"];
                String temp = companyFunctionSet.Tables[0].Rows[index]["FuncID"].ToString();
                return temp;
            }
            return "ERROR";
        }

        public ArrayList GetIDNumbers()
        {
            IDNumber myIDNum;
            var aNumbers = new ArrayList();
            if (tbIDNumber1.Text != "")
            {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber1.Text,
                              NumberTypeID = int.Parse(ddIDNumberType1.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            if (tbIDNumber2.Text != "")
            {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber2.Text,
                              NumberTypeID = int.Parse(ddIDNumberType2.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            return aNumbers;
        }

        public ArrayList GetPhoneNumbers()
        {
            var myNum = new PhoneNumber();
            var aNumbers = new ArrayList();
            if (tbPhoneNumber1.Text != "")
            {
                myNum.Number = tbPhoneNumber1.Text;
                myNum.NumberTypeID = 2;
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber2.Text != "")
            {
                myNum = new PhoneNumber { Number = tbPhoneNumber2.Text, NumberTypeID = 4 };
                aNumbers.Add(myNum);
            }
            return aNumbers;
        }

        public ArrayList GetAddresses()
        {
            var myAddr = new Address();
            var address = new ArrayList();
            if (tbAddress1Native.Text != "" || tbAddress1EN.Text != "" || tbAddress1InfoN.Text != "" ||
                tbAddress1InfoEN.Text != "")
            {
                myAddr.CityID = int.Parse(ddCity1.SelectedItem.Value); //.SelectedIndex + 1;
                if (ddCity1.SelectedItem.Value.Trim() != "")
                {
                    myAddr.OtherInfoNative = tbAddress1InfoN.Text;
                }
                myAddr.OtherInfoEN = tbAddress1InfoEN.Text;
                myAddr.PostalCode = tbPostalCode1.Text;
                myAddr.PostBox = tbPostBox1.Text;
                myAddr.StreetNative = tbAddress1Native.Text;
                myAddr.StreetEN = tbAddress1EN.Text;
                address.Add(myAddr);
            }
            if (tbAddress2Native.Text != "" || tbAddress2EN.Text != "" || tbAddress2InfoN.Text != "" ||
                tbAddress2InfoEN.Text != "")
            {
                myAddr = new Address();
                if (ddCity2.SelectedItem.Value.Trim() != "")
                {
                    myAddr.CityID = int.Parse(ddCity2.SelectedItem.Value); //.SelectedIndex + 1;
                }
                myAddr.OtherInfoNative = tbAddress2InfoN.Text;
                myAddr.OtherInfoEN = tbAddress2InfoEN.Text;
                myAddr.PostalCode = tbPostalCode2.Text;
                myAddr.PostBox = tbPostBox2.Text;
                myAddr.StreetNative = tbAddress2Native.Text;
                myAddr.StreetEN = tbAddress2EN.Text;
                address.Add(myAddr);
            }
            return address;
        }

        /// <summary>
        /// Get vat numbers data and bind it to datagrid.
        /// </summary>
        /// <param name="creditInfoId"></param>
        private void getVatNumbers(int creditInfoId)
        {
            IList<VatNumber> listOfVatNumbers = this.myFactory.GetVatNumbersByCreditinfoId(creditInfoId);
            if (listOfVatNumbers.Count == 0)
            {
                this.tblVatNumbers.Visible = false;
            }
            else
            {
                this.tblVatNumbers.Visible = true;
                this.dgVatNumbers.DataSource = listOfVatNumbers;
                this.dgVatNumbers.DataBind();
            }
        }

        private void LocalizeText()
        {
            if (!isUpdatingUser)
            {
                if (isCreatingSubscriber)
                {
                    lbNewCompany.Text = rm.GetString("txtCreatingSubscriber", ci) + " - " +
                                        rm.GetString("txtNewCompany", ci);
                }
                else
                {
                    lbNewCompany.Text = rm.GetString("txtAddUser", ci) + " - " + rm.GetString("txtNewCompany", ci);
                }
            }
            else
            {
                if (isUpdatingUser)
                {
                    lbNewCompany.Text = rm.GetString("txtUpdatingUser", ci);
                }
                else if (isCreatingSubscriber)
                {
                    lbNewCompany.Text = rm.GetString("txtCreatingSubscriber", ci) + " - " +
                                        rm.GetString("txtUpdatingCompany", ci);
                }
                else
                {
                    lbNewCompany.Text = rm.GetString("txtAddUser", ci) + " - " + rm.GetString("txtUpdatingCompany", ci);
                }
            }

            if (newUser)
            {
                lbNewCompany.Text = rm.GetString("txtAddUser", ci) + " - " + rm.GetString("txtNewCompany", ci);
            }

            lbDebtorCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lbCompanyName.Text = rm.GetString("txtCompanyName", ci);
            lbCompanyNameEN.Text = rm.GetString("txtCompanyNameEN", ci);
            lbEstablished.Text = rm.GetString("txtEstablished", ci);
            lblRegistered.Text = rm.GetString("txtRegistered", ci);
            lbPostalCode.Text = rm.GetString("txtPostalCode1", ci);
            btCancel.Text = rm.GetString("txtCancel", ci);
            // need translation for email!
            lbEmail.Text = rm.GetString("txtEmail", ci);
            lbLastContacted.Text = rm.GetString("txtLastContacted", ci);
            lbCompURL.Text = rm.GetString("txtURL", ci);
            lbCompFunction.Text = rm.GetString("txtCompFunction", ci);
            lbPostalCode2.Text = rm.GetString("txtPostalCode2", ci);
            lbAddress2InfoEN.Text = rm.GetString("txtAddressInfoEN2", ci);
            lbPhoneNumber.Text = rm.GetString("txtPhoneNumber", ci);
            lbFaxNumber.Text = rm.GetString("txtFaxNumber", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            btnSave.Text = rm.GetString("txtSave", ci);

            lbAddress1.Text = rm.GetString("txtAddress1", ci);
            lbAddress1EN.Text = rm.GetString("txtAddressEN1", ci);
            lbCity1.Text = rm.GetString("txtCity1", ci);
            lbPostalCode.Text = rm.GetString("txtPostalCode", ci);
            lbPostBox.Text = rm.GetString("txtPostBox", ci);
            lbAddress1Info.Text = rm.GetString("txtAddressInfo1", ci);
            lbAddr1InfoEN.Text = rm.GetString("txtAddressInfoEN1", ci);
            lbAddress2.Text = rm.GetString("txtAddress2", ci);
            lbAddress2EN.Text = rm.GetString("txtAddressEN2", ci);
            lbCity2.Text = rm.GetString("txtCity2", ci);
            lbPostBox2.Text = rm.GetString("txtPostBox2", ci);
            lbAddress2Info.Text = rm.GetString("txtAddressInfo2", ci);
            lbIDNumber1.Text = rm.GetString("txtIDNumber1", ci);
            lbIDNumber2.Text = rm.GetString("txtIDNumber2", ci);
            lbType1.Text = rm.GetString("txtNumberType", ci);
            lbType2.Text = rm.GetString("txtNumberType", ci);
            /*
                        Requiredfieldvalidator1.ErrorMessage = rm.GetString("txtValueMissing", ci);
                        Requiredfieldvalidator2.ErrorMessage = rm.GetString("txtValueMissing", ci);
                        RequiredFieldValidator5.ErrorMessage = rm.GetString("txtValueMissing", ci);*/

            rfvCompanyNameNative.ErrorMessage = rm.GetString("txtValueMissing", ci); ;
            rfvOrgStatus.ErrorMessage = rm.GetString("txtValueMissing", ci); ;
            rfvCompanyEstablished.ErrorMessage = rm.GetString("txtValueMissing", ci); ;

            cvCompanyEstablished.ErrorMessage = rm.GetString("txtDateIncorrect", ci);
            rfvCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblCitySearch.Text = rm.GetString("txtSearch", ci);
            lblCity2Search.Text = rm.GetString("txtSearch", ci);
            lblNote.Text = rm.GetString("lblNote", ci);

            lblAddressOne.Text = rm.GetString("txtAddress1", ci);
            lblAddressTwo.Text = rm.GetString("txtAddress2", ci);
            lblOtherInfo.Text = rm.GetString("txtOtherInfo", ci);

            lblIsSearchable.Text = rm.GetString("txtIsSearchable", ci);
            rbtIsSearchableTrue.Text = rm.GetString("txtTrue", ci);
            rbtIsSearchableFalse.Text = rm.GetString("txtFalse", ci);
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            if (isUpdatingUser)
            {
                Server.Transfer("FindCIUser.aspx?UpdateUser=True");
            }
            else
            {
                Server.Transfer("FindSubscribers.aspx");
            }
        }

        public void DateValidate(object source, ServerValidateEventArgs value)
        {
            if (value.Value != "")
            {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try
                {
                    DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                }
                catch (Exception)
                {
                    value.IsValid = false;
                    return;
                }
            }
            else
            {
                value.IsValid = true;
            }
        }

        private static bool IsValidDate(string dateString)
        {
            if (dateString.Trim() == "")
            {
                return false;
            }
            IFormatProvider format = CultureInfo.CurrentCulture;
            try
            {
                DateTime.Parse(dateString.Trim(), format, DateTimeStyles.AllowWhiteSpaces);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            var myComp = new Company();
            var myFactory = new uaFactory();
            myComp.Type = CigConfig.Configure("lookupsettings.companyID");
            myComp.NameNative = tbCompanyNameNative.Text;
            myComp.NameEN = tbCompanyNameEN.Text;
            myComp.DoNotDelete = chkdoNotDelete.Checked;
            myComp.FuncID = GetSelectedNaceCode(ddCompanyNace.SelectedIndex);
            IFormatProvider format = CultureInfo.CurrentCulture;
            try
            {
                //	myClaim.ClaimDate = DateTime.Parse(myStringArray[6],format, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                myComp.Established = DateTime.Parse(
                    tbCompanyEstablished.Text, format, DateTimeStyles.AllowWhiteSpaces);
            }
            catch (Exception err)
            {
                Logger.WriteToLog(
                    "NewCIUserC.aspx : btRegCompany_Click : DateTime.Parse(this.tbCompanyEstablished.Text.." +
                    err.Message,
                    true);
                return;
            }

            try
            {
                myComp.LastContacted = DateTime.Parse(
                    tbCompanyLContacted.Text, format, DateTimeStyles.AllowWhiteSpaces);
            }
            catch { }

            if (IsValidDate(tbRegistered.Text))
            {
                myComp.Registered = DateTime.Parse(tbRegistered.Text, format, DateTimeStyles.AllowWhiteSpaces);
            }

            myComp.URL = tbCompanyURL.Text;
            myComp.Email = tbEmail.Text;
            myComp.IsSearchable = rbtIsSearchableTrue.Checked;
            myComp.Org_status_code = int.Parse(ddOrgStatus.SelectedValue);

            myComp.Note = tbNote.Text;

            if (myComp.FuncID.Equals("ERROR"))
            {
                // Transfer to errorPage
            }

            if (update)
            {
                myComp.CreditInfoID = int.Parse(tbCompanyCIID.Text);
            }

            // collections
            myComp.Address = GetAddresses();
            myComp.IDNumbers = GetIDNumbers();
            myComp.Number = GetPhoneNumbers();
            bool regOK = true;
            int creditInfoID = -1;
            if (!update)
            {
                creditInfoID = myFactory.AddCompany(myComp);
                if (creditInfoID < 0)
                {
                    lblError.CssClass = "error_text";
                    lblError.Text = rm.GetString("txtFailedCreatingCompany", ci);
                    lblError.Visible = true;
                    regOK = false;
                    return;
                }
            }
            else
            {
                myComp.LastUpdate = DateTime.Now;
                creditInfoID = myFactory.UpdateCompany(myComp);
                if (creditInfoID < 0)
                {
                    lblError.CssClass = "error_text";
                    lblError.Text = rm.GetString("txtFailedUpdatingCompany", ci);
                    lblError.Visible = true;
                    regOK = false;
                    return;
                }
            }

            lblError.CssClass = "confirm_text";
            lblError.Text = rm.GetString("txtCompanyInsertedUpdated", ci);
            lblError.Visible = true;

            // if should redirect to another page			
            if (regOK)
            {
                tbCompanyCIID.Text = creditInfoID.ToString();
                if (!isUpdatingUser)
                {
                    //btnContinue.Enabled=true;
                    if (isCreatingSubscriber)
                    {
                        Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + tbCompanyCIID.Text);
                    }
                    else if (newUser)
                    {
                        Server.Transfer("FindCIUser.aspx");
                    }
                    else
                    {
                        Server.Transfer(
                            "UserDetails.aspx?CreditInfoID=" + tbCompanyCIID.Text + "&SubscriberCIID=" +
                            subscriberCIID + "&Department=" + _departmentID);
                    }
                }

                if (isCreatingSubscriber)
                {
                    Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + tbCompanyCIID.Text);
                }
            }
            initControls(update);
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (isCreatingSubscriber)
                {
                    Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + tbCompanyCIID.Text);
                }
                else if (newUser)
                {
                    Server.Transfer("FindCIUser.aspx");
                }
                else
                {
                    Server.Transfer(
                        "UserDetails.aspx?CreditInfoID=" + tbCompanyCIID.Text + "&SubscriberCIID=" + subscriberCIID +
                        "&Department=" + _departmentID);
                }
            }
        }

        private void btnCitySearch_Click(object sender, EventArgs e)
        {
            if (txtCitySearch.Text.Trim() != "")
            {
                DataSet dsCity = myFactory.GetCityListAsDataSet(txtCitySearch.Text, EN);
                BindCityList(dsCity);
            }
        }

        private void btnCity2Search_Click(object sender, EventArgs e)
        {
            if (txtCity2Search.Text.Trim() != "")
            {
                DataSet dsCity = myFactory.GetCityListAsDataSet(txtCity2Search.Text, EN);
                BindCity2List(dsCity);
            }
        }


        protected void btDelete_Click(object sender, EventArgs e)
        {
            if (update)
            {
                var creditInfoID = int.Parse(tbCompanyCIID.Text);
                Company myComp = myFactory.GetCompany(creditInfoID);

                if (myComp != null && !myComp.DoNotDelete)
                {
                    if (myFactory.DeleteCompany(creditInfoID))
                    {
                        lblError.Text = "Company successfully deleted";
                        lblError.CssClass = "confirm_text";
                        lblError.Visible = true;
                    }
                    else
                    {
                        lblError.Text = "An Error Occured While deleting Company record. please check the logs for errors";
                        lblError.CssClass = "error_text";
                        lblError.Visible = true;
                    }



                }
                else
                {
                    // change this to resource
                    lblError.Text = "Company Record is set as  'Do Not Delete' therefore it can not be deleted or Company record do not exist";
                    lblError.CssClass = "error_text";
                    lblError.Visible = true;
                }
            }

        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cvCompanyEstablished.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.btnCitySearch.Click += new System.EventHandler(this.btnCitySearch_Click);
            this.btnCity2Search.Click += new System.EventHandler(this.btnCity2Search_Click);
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}