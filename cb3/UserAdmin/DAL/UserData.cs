using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using DataProtection;
using Logging.BLL;
using UserAdmin.BLL.auUsers;
using UserAdmin.BLL.npEmployee;
using cb3;
using cb3.Audit;
using cb3.Settings;
using cb3.UserAdmin.BLL.auUsers;

namespace UserAdmin.DAL {
    /// <summary>
    /// Summary description for UserData.
    /// </summary>
    public class UserData : BaseDALC {
        /// <summary>
        /// Fetches all users working in a specific department.
        /// </summary>
        /// <returns>A dataset containing users.</returns>
        public DataSet GetUsersAsDataSet() {
            var mySet = new DataSet();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand("SELECT * from au_users", myOleDbConn);

                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        public DataSet GetUsersBySubscriberAsDataSet(int SubscriberID) {
            var mySet = new DataSet();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    const string strSql = "SELECT au_users.* "
                                    +
                                    "FROM au_users INNER JOIN c_au_department ON au_users.DepartmentID=c_au_department.DepartmentID "
                                    + "WHERE c_au_department.CreditInfoID=?";

                    myAdapter.SelectCommand = new OleDbCommand(strSql, myOleDbConn);

                    // SubscriberCreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("SubscriberID", OleDbType.Integer)
                                      {
                                          Value = SubscriberID,
                                          Direction = ParameterDirection.Input
                                      };
                    myAdapter.SelectCommand.Parameters.Add(myParameter);

                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Fetches all users working in a specific department.
        /// </summary>
        /// <param name="DepartmentID">Unique ID for the department.</param>
        /// <returns>A dataset containing users.</returns>
        public DataSet GetUsersAsDataSet(int DepartmentID) {
            var mySet = new DataSet();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    const string strSql = "SELECT * "
                                    + "FROM au_users "
                                    + "WHERE DepartmentID = ?";
                    myAdapter.SelectCommand = new OleDbCommand(strSql, myOleDbConn);

                    // SubscriberCreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("DepartmentID", OleDbType.Integer)
                                      {
                                          Value = DepartmentID,
                                          Direction = ParameterDirection.Input
                                      };
                    myAdapter.SelectCommand.Parameters.Add(myParameter);

                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Fetches all users working in a specific department.
        /// </summary>
        /// <param name="DepartmentID">Unique ID for the department.</param>
        /// <param name="IsOpen">Is user open or closed.</param>
        /// <returns>A dataset containing users.</returns>
        public DataSet GetUsersAsDataSet(int DepartmentID, bool IsOpen) {
            var mySet = new DataSet();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    const string strSql = "SELECT * "
                                    + "FROM au_users "
                                    + "WHERE DepartmentID=? AND IsOpen=?";
                    myAdapter.SelectCommand = new OleDbCommand(strSql, myOleDbConn);

                    // SubscriberCreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("DepartmentID", OleDbType.Integer)
                                      {
                                          Value = DepartmentID,
                                          Direction = ParameterDirection.Input
                                      };
                    myAdapter.SelectCommand.Parameters.Add(myParameter);

                    myParameter = new OleDbParameter("IsOpen", OleDbType.WChar, 5)
                                  {
                                      Value = IsOpen,
                                      Direction = ParameterDirection.Input
                                  };
                    myAdapter.SelectCommand.Parameters.Add(myParameter);

                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        public DataSet GetUserTypes() {
            var dataSet = new DataSet();
            var strConn = DatabaseHelper.ConnectionString();
            using (var oleDbConn = new OleDbConnection(strConn)) {
                const string strSql = "SELECT UserType FROM au_Users GROUP BY UserType ORDER BY UserType;";
                var adapter = new OleDbDataAdapter();
                oleDbConn.Open();
                adapter.SelectCommand = new OleDbCommand(strSql, oleDbConn);
                adapter.Fill(dataSet);
                return dataSet;
            }
        }

        public DataSet FindUser(
            string strUsername,
            string strName,
            string strEmail,
            string strSubscriberName,
            string strUserType,
            int nIsOpen) {
            var mySet = new DataSet();
            var whereStatement = false;
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var strSql = "SELECT TOP 100 * FROM ua_UserNames_extended WHERE ";

                if (strUsername != "") {
                    strSql += " [UserName] LIKE '" + strUsername + "%'";
                    whereStatement = true;
                }

                if (strName != "") {
                    if (whereStatement) {
                        strSql += " AND";
                    }
                    strSql += " (NameNative LIKE '" + strName + "%' OR NameEN LIKE '" + strName + "%')";
                    whereStatement = true;
                }

                if (strEmail != "") {
                    if (whereStatement) {
                        strSql += " AND";
                    }
                    strSql += " Email like '" + strEmail + "%'";
                    whereStatement = true;
                }

                if (strSubscriberName != "") {
                    if (whereStatement) {
                        strSql += " AND";
                    }
                    strSql += " (SubscriberNameNative LIKE '" + strSubscriberName + "%' OR SubscriberNameEN LIKE '" +
                              strSubscriberName + "%')";
                    whereStatement = true;
                }

                if (strUserType != "") {
                    if (whereStatement) {
                        strSql += " AND";
                    }
                    strSql += " UserType='" + strUserType + "'";
                    whereStatement = true;
                }

                if (nIsOpen == 0) {
                    if (whereStatement) {
                        strSql += " AND";
                    }
                    strSql += " IsOpen='" + false + "'";
                    whereStatement = true;
                } else if (nIsOpen == 1) {
                    if (whereStatement) {
                        strSql += " AND";
                    }
                    strSql += " IsOpen='" + true + "'";
                    whereStatement = true;
                }

                // nothing to query by ... send it all back
                if (!whereStatement) {
                    strSql += " UserName LIKE('%')";
                }

                // Add order by
                strSql += " ORDER BY NameNative";

                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(strSql, myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        /// <summary>
        /// DEPRECATED - Search for user ids
        /// </summary>
        /// <param name="UserName">The user name</param>
        /// <param name="CreditInfoID">Creditinfo id</param>
        /// <param name="NameNative">Native name</param>
        /// <param name="NameEN">English name</param>
        /// <returns>The users as dataset</returns>
        public DataSet FindUser_OLD(string UserName, int CreditInfoID, string NameNative, string NameEN) {
            //string funcName = "FindUser(string UserName, int CreditInfoID, string NameNative, string nameEN)";
            var mySet = new DataSet();
            bool whereStatement = false;
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                string query = "SELECT TOP 100 * FROM ua_UserNames WHERE ";

                if (UserName != "") {
                    query += " UserName like '" + UserName + "%'";
                    whereStatement = true;
                }
                if (CreditInfoID > 0) {
                    query += " CreditInfoID = " + CreditInfoID + "";
                    whereStatement = true;
                }

                if (NameNative != "") {
                    query += " NameNative like '" + NameNative + "%'";
                    whereStatement = true;
                }

                if (NameEN != "") {
                    query += " NameEN like '" + NameEN + "%'";
                    whereStatement = true;
                }

                // nothing to query by ... send it all back
                if (!whereStatement) {
                    query += " UserName LIKE('%')";
                }

                // Add order by
                query += " ORDER BY NameNative";

                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        public auUsersBLLC GetSpecificUser(string UserName) {
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                var myOleDbCommand = new OleDbCommand(
                    "SELECT * FROM au_Users WHERE au_Users.UserName = ?", myOleDbConn);

                // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                var myParameter = new OleDbParameter("UserName", OleDbType.WChar)
                                  {
                                      Value = UserName,
                                      Direction = ParameterDirection.Input
                                  };
                myOleDbCommand.Parameters.Add(myParameter);

                var reader = myOleDbCommand.ExecuteReader();
                var myUser = new auUsersBLLC();

                if (!reader.Read()) {
                    return null;
                }
                myUser.ID = reader.GetInt32(0);
                myUser.CreditInfoID = reader.GetInt32(1);
                myUser.UserName = reader.GetString(2);

                if (!reader.IsDBNull(3)) {
                    myUser.SubscriberID = reader.GetInt32(3);
                }

                if (!reader.IsDBNull(4)) {
                    myUser.NationalID = reader.GetInt32(4);
                }

                if (!reader.IsDBNull(5)) {
                    myUser.Email = reader.GetString(5);
                }

                if (!reader.IsDBNull(6)) {
                    myUser.PasswordHash = reader.GetString(6);
                }

                if (!reader.IsDBNull(7)) {
                    myUser.Salt = reader.GetString(7);
                }

                if (!reader.IsDBNull(8)) {
                    myUser.Groups = reader.GetString(8);
                }

                if (!reader.IsDBNull(9)) {
                    myUser.UserType = reader.GetString(9);
                }

                if (!reader.IsDBNull(10)) {
                    myUser.CntCreditWatch = reader.GetInt32(10);
                }

                if (!reader.IsDBNull(11)) {
                    myUser.RegisteredBy = reader.GetInt32(11);
                }

                if (!reader.IsDBNull(12)) {
                    myUser.HasWebServices = reader.GetString(12);
                }

                if (!reader.IsDBNull(13)) {
                    myUser.isOpen = reader.GetString(13);
                }

                if (!reader.IsDBNull(14)) {
                    myUser.OpenUntil = reader.GetDateTime(14);
                }

                if (!reader.IsDBNull(15)) {
                    myUser.Created = reader.GetDateTime(15);
                }

                if (!reader.IsDBNull(16)) {
                    myUser.Updated = reader.GetDateTime(16);
                }

                if (!reader.IsDBNull(19)) {
                    myUser.DepartmentID = reader.GetInt32(19);
                }

                if (!reader.IsDBNull(20))
                {
                    myUser.lockUserAccount = reader.GetBoolean(20);
                }

                if (!reader.IsDBNull(21))
                {
                    myUser.GroupCode = reader.GetString(21);
                }

                reader.Close();
                return myUser;
            }
        }

        /// <summary>
        /// Fetches a specific user from the underlying data structure.
        /// </summary>
        /// <param name="UserID">Unique ID of the user.</param>
        /// <returns>User entity.</returns>
        public auUsersBLLC GetUser(int UserID) {
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                var myOleDbCommand = new OleDbCommand(
                    "SELECT * FROM au_Users WHERE au_Users.id = ?", myOleDbConn);

                // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                var myParameter = new OleDbParameter("id", OleDbType.Integer)
                                  {
                                      Value = UserID,
                                      Direction = ParameterDirection.Input
                                  };
                myOleDbCommand.Parameters.Add(myParameter);

                var reader = myOleDbCommand.ExecuteReader();
                var myUser = new auUsersBLLC();

                if (!reader.Read()) {
                    return null;
                }
                myUser.ID = reader.GetInt32(0);
                myUser.CreditInfoID = reader.GetInt32(1);
                myUser.UserName = reader.GetString(2);

                if (!reader.IsDBNull(3)) {
                    myUser.SubscriberID = reader.GetInt32(3);
                }

                if (!reader.IsDBNull(4)) {
                    myUser.NationalID = reader.GetInt32(4);
                }

                if (!reader.IsDBNull(5)) {
                    myUser.Email = reader.GetString(5);
                }

                if (!reader.IsDBNull(6)) {
                    myUser.PasswordHash = reader.GetString(6);
                }

                if (!reader.IsDBNull(7)) {
                    myUser.Salt = reader.GetString(7);
                }

                if (!reader.IsDBNull(8)) {
                    myUser.Groups = reader.GetString(8);
                }

                if (!reader.IsDBNull(9)) {
                    myUser.UserType = reader.GetString(9);
                }

                if (!reader.IsDBNull(10)) {
                    myUser.CntCreditWatch = reader.GetInt32(10);
                }

                if (!reader.IsDBNull(11)) {
                    myUser.RegisteredBy = reader.GetInt32(11);
                }

                if (!reader.IsDBNull(12)) {
                    myUser.HasWebServices = reader.GetString(12);
                }

                if (!reader.IsDBNull(13)) {
                    myUser.isOpen = reader.GetString(13);
                }

                if (!reader.IsDBNull(14)) {
                    myUser.OpenUntil = reader.GetDateTime(14);
                }

                if (!reader.IsDBNull(15)) {
                    myUser.Created = reader.GetDateTime(15);
                }

                if (!reader.IsDBNull(16)) {
                    myUser.Updated = reader.GetDateTime(16);
                }

                if (!reader.IsDBNull(19)) {
                    myUser.DepartmentID = reader.GetInt32(19);
                }

                reader.Close();
                return myUser;
            }
        }


        public auUserGroups GetUserGroup(int GroupID)
        {

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try {
                
                    myOleDbConn.Open();

                    var myOleDbCommand = new OleDbCommand(
                        "SELECT * FROM au_UserGroups WHERE au_UserGroups.ID = ?", myOleDbConn);
                
                    var myParameter = new OleDbParameter("ID", OleDbType.Integer)
                    {
                        Value = GroupID,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParameter);

                    var reader = myOleDbCommand.ExecuteReader();
                    var userGroup = new auUserGroups();

                    if (reader == null || !reader.Read())
                    {
                        return null;
                    }
                    userGroup.ID = reader.GetInt32(0);

                    if (!reader.IsDBNull(1))
                    {
                        userGroup.GroupName = reader.GetString(1);
                    }

                    if (!reader.IsDBNull(2))
                    {
                        userGroup.GroupCode = reader.GetString(2);
                    }

                    if (!reader.IsDBNull(3))
                    {
                        userGroup.GroupDesc = reader.GetString(3);
                    }

                    reader.Close();
                    return userGroup;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);

                }
            }
        }


        public int GetUserGroupID(string GroupCode)
        {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            if (string.IsNullOrEmpty(GroupCode))
            {
                return 0;
            }
            using (var myConnection = new OleDbConnection(myConnectionString))
            {
                try
                {
                    myConnection.Open();
                    myCommand = new OleDbCommand("SELECT ID FROM au_UserGroups WHERE GroupCode = ?", myConnection);


                    var myParameter = new OleDbParameter("GroupCode", OleDbType.WChar)
                    {
                        Value = GroupCode,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);

                    var reader = myCommand.ExecuteReader();

                    if (reader == null || !reader.Read())
                    {
                        return 0;
                    }

                    return (int)reader.GetInt32(0);
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.Message, true);
                    return 0;
                }
            }
        }


        public DataSet GetAllUserGroups()
        {

            var mySet = new DataSet();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    const string strSql = "SELECT* FROM au_UserGroups";
                    myAdapter.SelectCommand = new OleDbCommand(strSql, myOleDbConn);

                    myAdapter.Fill(mySet);
                    return mySet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }


        public int SaveUserGroup(auUserGroups auUserGroup)
        {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString))
            {
                myConnection.Open();
                var queryString = "";
                var isUpdate = false;
                

                try
                {

                    if (GetUserGroup(auUserGroup.ID) != null)
                    {
                        isUpdate = true;
                    }


                    if (isUpdate)
                    {
                        queryString = "Update au_UserGroups SET GroupName = ?, GroupCode = ?, GroupDesc = ? WHERE ID = " + auUserGroup.ID;
                    }
                    else
                    {
                        queryString = "INSERT INTO au_UserGroups (GroupName, GroupCode, GroupDesc) VALUES(?,?,?)";

                    }

                    myCommand =
                        new OleDbCommand(
                                queryString,
                                myConnection);
                    

                   var myParameter = new OleDbParameter("GroupName", OleDbType.WChar)
                    {
                        Value = auUserGroup.GroupName,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);

                    myParameter = new OleDbParameter("GroupCode", OleDbType.WChar)
                    {
                        Value = auUserGroup.GroupCode,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);

                    myParameter = new OleDbParameter("GroupDesc", OleDbType.WChar)
                    {
                        Value = auUserGroup.GroupDesc,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    if (!isUpdate) // if inserted then get the ID of the last insearted
                    {
                        using (OleDbCommand command = new OleDbCommand("SELECT top 1 ID from au_UserGroups order by ID desc", myConnection))
                        {
                            var reader = command.ExecuteReader();

                            if (reader == null || !reader.Read())
                            {
                                return 0;
                            }
                            var result = reader.GetInt32(0);
                            return result;
                        }

                    }
                    else
                    {
                        return auUserGroup.ID;
                    }
                    

                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.Message, true);
                    return 0;
                }
            }
        }


        
        public bool DeleteUserGroup(int GroupID)
        {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString))
            {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try
                {
                    // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("DELETE FROM au_UserGroups WHERE ID = ?", myConnection)
                        { Transaction = myTransaction };

                    // Fyrir Initials
                    var myParam = new OleDbParameter("ID", OleDbType.Integer)
                    {
                        Value = GroupID,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParam);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
            }

        }


        public void StoreAccountDetails(
            string userName,
            string passwordHash,
            string salt,
            string groups,
            int iCreditInfoID,
            int iCount) {
            // See "How To Use DPAPI (Machine Store) from ASP.NET" for information 
            // about securely storing connection strings.

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try {
                    // insert into np_Claims
                    var myOleDbCommand = new OleDbCommand(
                        "INSERT INTO au_users(UserName, PasswordHash, salt, groups, CreditInfoID, cntCreditWatch) VALUES(?,?,?,?,?,?)",
                        myOleDbConn) {Transaction = myTrans};

                    var myParam = new OleDbParameter("userName", OleDbType.WChar)
                                  {
                                      Value = userName,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("passwordHash", OleDbType.Char, 40)
                              {
                                  Value = passwordHash,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("salt", OleDbType.VarChar, 10)
                              {
                                  Value = salt,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("groups", OleDbType.WChar)
                              {
                                  Value = groups,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CrediInfoID", OleDbType.Integer)
                              {
                                  Value = iCreditInfoID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("cntCreditWatch", OleDbType.Integer)
                              {
                                  Value = iCount,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    myTrans.Commit();
                } catch (Exception ex) {
                    // Code to check for primary key violation (duplicate account name)
                    // or other database errors omitted for clarity
                    throw new Exception("Exception adding account. " + ex.Message);
                }
            }
        }

        public void UpdateUserPassword(string PasswordHash, string salt, string UserName) {
            // See "How To Use DPAPI (Machine Store) from ASP.NET" for information 
            // about securely storing connection strings.
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try {
                    // insert into au_Users
                    myOleDbCommand =
                        new OleDbCommand(
                            "Update au_Users SET PasswordHash = ?, salt = ? WHERE UserName = ?", myOleDbConn)
                        {Transaction = myTrans};

                    var myParam = new OleDbParameter("passwordHash", OleDbType.Char, 40)
                                  {
                                      Value = PasswordHash,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("salt", OleDbType.VarChar, 10)
                              {
                                  Value = salt,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("UserName", OleDbType.WChar)
                              {
                                  Value = UserName,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    myTrans.Commit();
                } catch (Exception ex) {
                    // Code to check for primary key violation (duplicate account name)
                    // or other database errors omitted for clarity
                    throw new Exception("Exception updating np_CreditInfoUser. " + ex.Message);
                }
            }
        }

        public void UpdateUserAccountDetails(auUsersBLLC myUser) {
            // See "How To Use DPAPI (Machine Store) from ASP.NET" for information 
            // about securely storing connection strings.
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try {
                    // insert into au_Users
                    myOleDbCommand =
                        new OleDbCommand(
                            "Update au_Users SET Email = ?, groups = ?, UserType = ?, cntCreditWatch = ?, HasWebServices = ?, IsOpen = ?, OpenUntil = ?, DateUpdated = ?, GroupCode = ?  WHERE UserName = ?",
                            myOleDbConn) {Transaction = myTrans};

                    var myParam = new OleDbParameter("Email", OleDbType.WChar)
                                  {
                                      Value = myUser.Email,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("groups", OleDbType.WChar)
                              {
                                  Value = myUser.Groups,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("UserType", OleDbType.WChar)
                              {
                                  Value = myUser.UserType,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("cntCreditWatch", OleDbType.Integer)
                              {
                                  Value = myUser.CntCreditWatch,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("HasWebServices", OleDbType.Char, 10)
                              {
                                  Value = myUser.HasWebServices,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("IsOpen", OleDbType.Char, 10)
                              {
                                  Value = myUser.isOpen,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("OpenUntil", OleDbType.Date)
                              {
                                  Value = myUser.OpenUntil,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DateUpdated", OleDbType.Date)
                              {
                                  Value = myUser.Updated,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("GroupCode", OleDbType.WChar)
                    {
                        Value = myUser.GroupCode,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("UserName", OleDbType.WChar)
                              {
                                  Value = myUser.UserName,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);



                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    myTrans.Commit();
                } catch (Exception ex) {
                    // Code to check for primary key violation (duplicate account name)
                    // or other database errors omitted for clarity
                    throw new Exception("Exception updating np_CreditInfoUser. " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Inserts a user entity into a data structure.
        /// </summary>
        /// <param name="myUser">auUsersBLLC entity.</param>
        public void StoreUserAccountDetails(auUsersBLLC myUser) {
            // See "How To Use DPAPI (Machine Store) from ASP.NET" for information 
            // about securely storing connection strings.
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try {
                    // insert into au_Users
                    myOleDbCommand =
                        new OleDbCommand(
                            "INSERT INTO au_users(CreditInfoID, UserName, SubscriberID, Email, PasswordHash, salt, groups, UserType, cntCreditWatch, RegisteredBy, HasWebServices, IsOpen, OpenUntil, DateUpdated, DepartmentID, GroupCode) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                            myOleDbConn) {Transaction = myTrans};

                    var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                  {
                                      Value = myUser.CreditInfoID,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("UserName", OleDbType.WChar)
                              {
                                  Value = myUser.UserName,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("SubscriberID", OleDbType.Integer)
                              {
                                  Value = myUser.SubscriberID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Email", OleDbType.WChar)
                              {
                                  Value = myUser.Email,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("passwordHash", OleDbType.Char, 40)
                              {
                                  Value = myUser.PasswordHash,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("salt", OleDbType.VarChar, 10)
                              {
                                  Value = myUser.Salt,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("groups", OleDbType.WChar)
                              {
                                  Value = myUser.Groups,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("UserType", OleDbType.WChar)
                              {
                                  Value = myUser.UserType,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("cntCreditWatch", OleDbType.Integer)
                              {
                                  Value = myUser.CntCreditWatch,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("RegisteredBy", OleDbType.Integer)
                              {
                                  Value = myUser.RegisteredBy,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("HasWebServices", OleDbType.Char, 10)
                              {
                                  Value = myUser.HasWebServices,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("IsOpen", OleDbType.Char, 10)
                              {
                                  Value = myUser.isOpen,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("OpenUntil", OleDbType.Date)
                              {
                                  Value = myUser.OpenUntil,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DateUpdated", OleDbType.Date)
                              {
                                  Value = myUser.Updated,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DepartmentID", OleDbType.Integer)
                              {
                                  Value = myUser.DepartmentID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("GroupCode", OleDbType.WChar)
                    {
                        Value = myUser.GroupCode,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    myTrans.Commit();
                } catch (Exception ex) {
                    // Code to check for primary key violation (duplicate account name)
                    // or other database errors omitted for clarity
                    throw new Exception("Exception adding account. " + ex.Message);
                }
            }
        }

        public void DeleteEmployeeDetails(string Initials) {
            OleDbCommand myCommand;
            var myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("DELETE FROM np_Employee WHERE Initials = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir Initials
                    var myParam = new OleDbParameter("Initials", OleDbType.WChar)
                                  {
                                      Value = Initials,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParam);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                }
            }
        }

        public void StoreEmployeeDetails(npEmployeeBLLC myEmp) {
            // See "How To Use DPAPI (Machine Store) from ASP.NET" for information 
            // about securely storing connection strings.
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try {
                    // insert into au_Users
                    myOleDbCommand =
                        new OleDbCommand(
                            "INSERT INTO np_Employee(Initials, CreditInfoID, LastUpdate) VALUES(?,?,?)", myOleDbConn)
                        {Transaction = myTrans};

                    var myParam = new OleDbParameter("Initials", OleDbType.WChar)
                                  {
                                      Value = myEmp.Initials,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Value = myEmp.CreditInfoID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("LastUpdate", OleDbType.Date)
                              {
                                  Value = myEmp.LastUpdated,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    myTrans.Commit();
                } catch (Exception ex) {
                    // Code to check for primary key violation (duplicate account name)
                    // or other database errors omitted for clarity
                    throw new Exception("Exception adding employee account. " + ex.Message);
                }
            }
        }

        public void UpdateCreditInfoUserTableWithInfo(string Email, int StatusID, int CreditInfoID) {
            // See "How To Use DPAPI (Machine Store) from ASP.NET" for information 
            // about securely storing connection strings.
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try {
                    // insert into au_Users
                    myOleDbCommand =
                        new OleDbCommand(
                            "Update np_CreditInfoUser SET Email = ?, StatusID = ? WHERE CreditInfoID = ?", myOleDbConn)
                        {Transaction = myTrans};

                    var myParam = new OleDbParameter("Email", OleDbType.WChar)
                                  {
                                      Value = Email,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("StatusID", OleDbType.Integer)
                              {
                                  Value = StatusID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Value = CreditInfoID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    myTrans.Commit();
                } catch (Exception ex) {
                    // Code to check for primary key violation (duplicate account name)
                    // or other database errors omitted for clarity
                    throw new Exception("Exception updating np_CreditInfoUser. " + ex.Message);
                }
            }
        }

        public static string CreateSalt(int size) {
            // Generate a cryptographic random number using the cryptographic
            // service provider
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);
            // Return a Base64 string representation of the random number
            return Convert.ToBase64String(buff);
        }

        public static string CreatePasswordHash(string pwd, string salt) {
            string saltAndPwd = string.Concat(pwd, salt);
            string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "SHA1");

            return hashedPwd;
        }

        public int GetCreditInfoID(string UserName) {
            int iRes;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT CreditInfoID FROM au_users WHERE UserName = '" + UserName + "'", myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    reader.Read(); // Advance to the one and only row

                    // Return output parameters from returned data stream
                    iRes = reader.GetInt32(0);
                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return iRes;
        }

        public int GetUserID(string UserName) {
            int iRes;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand = new OleDbCommand(
                        "SELECT id FROM au_users WHERE UserName = '" + UserName + "'", myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    reader.Read(); // Advance to the one and only row

                    // Return output parameters from returned data stream
                    iRes = reader.GetInt32(0);
                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return iRes;
        }

        public string GetUserName(int UserID) {
            string Result = "";
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand = new OleDbCommand(
                        "SELECT UserName FROM au_users WHERE id = '" + UserID + "'", myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        // Advance to the one and only row                        
                        // Return output parameters from returned data stream
                        if (!reader.IsDBNull(0)) {
                            Result = reader.GetString(0);
                        }
                    }
                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return Result;
        }

        public string GetUserNameByCIID(int nCIID) {
            string Result = "";
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT UserName FROM au_users WHERE CreditInfoID = '" + nCIID + "'", myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        // Advance to the one and only row                        
                        // Return output parameters from returned data stream
                        if (!reader.IsDBNull(0)) {
                            Result = reader.GetString(0);
                        }
                    }
                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return Result;
        }

        public int GetUserID(int CIID, int SubscriberID) {
            int iRes = -1;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT id FROM au_users WHERE CreditInfoID = " + CIID + " and SubscriberID = " +
                            SubscriberID,
                            myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        iRes = reader.GetInt32(0);
                    }

                    // Return output parameters from returned data stream

                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return iRes;
        }

        public string GetUserName(int CIID, int SubscriberID) {
            string Result;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT UserName FROM au_users WHERE CreditInfoID = " + CIID + " and SubscriberID = " +
                            SubscriberID,
                            myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    reader.Read(); // Advance to the one and only row

                    // Return output parameters from returned data stream
                    Result = reader.GetString(0);
                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return Result;
        }

        public string GetRoles(string username) {
            // Lookup code omitted for clarity
            // This code would typically look up the role list from a database
            // table.
            // If the user was being authenticated against Active Directory,
            // the Security groups and/or distribution lists that the user
            // belongs to may be used instead

            // This GetRoles method returns a pipe delimited string containing
            // roles rather than returning an array, because the string format
            // is convenient for storing in the authentication ticket /
            // cookie, as user data

            string roles = "none";
            OleDbCommand myOleDbCommand;

            // Get the salt and pwd from the database based on the user name.
            // See "How To: Use DPAPI (Machine Store) from ASP.NET," "How To:
            // Use DPAPI (User Store) from Enterprise Services," and "How To:
            // Create a DPAPI Library" for more information about how to use
            // DPAPI to securely store connection strings.
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand = new OleDbCommand(
                        "SELECT groups FROM au_users WHERE UserName = '" + username + "'", myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    reader.Read(); // Advance to the one and only row

                    // Return output parameters from returned data stream
                    roles = reader.GetString(0);
                    reader.Close();
                } catch (Exception) {}
            }

            return roles.Trim();
        }

        public int GetMaxCreditWatch(string UserName) {
            int iRes;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT cntCreditWatch FROM au_users WHERE UserName = '" + UserName + "'", myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    reader.Read(); // Advance to the one and only row

                    // Return output parameters from returned data stream
                    iRes = reader.GetInt32(0);
                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return iRes;
        }

        public bool VerifyPassword(string txtUserName, string txtPassword) {
            bool passwordMatch;
            OleDbCommand myOleDbCommand;

            // Get the salt and pwd from the database based on the user name.
            // See "How To: Use DPAPI (Machine Store) from ASP.NET," "How To:
            // Use DPAPI (User Store) from Enterprise Services," and "How To:
            // Create a DPAPI Library" for more information about how to use
            // DPAPI to securely store connection strings.
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                string dbPasswordHash = "";
                string salt = "";

                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT PasswordHash, salt FROM au_users WHERE UserName = '" + txtUserName + "'",
                            myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();

                    if (reader.Read()) // Advance to the one and only row
                    {
                        // Return output parameters from returned data stream
                        dbPasswordHash = reader.GetString(0);
                        salt = reader.GetString(1);
                    }
                    reader.Close();

                    // Now take the salt and the password entered by the user
                    // and concatenate them together.
                    string passwordAndSalt = string.Concat(txtPassword, salt);

                    // Now hash them
                    string hashedPasswordAndSalt =
                        FormsAuthentication.HashPasswordForStoringInConfigFile(passwordAndSalt, "SHA1");

                    // Now verify them.
                    passwordMatch = hashedPasswordAndSalt.Equals(dbPasswordHash);
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return passwordMatch;
        }

        public DataSet SearchUsers(
            string CreditInfoID, string FirstName, string Surname, string Address, string UserName, string OpenClosed) {
            var mySet = new DataSet();
            var myConnectionString = DatabaseHelper.ConnectionString();

            string query = "SELECT au_Users.*, np_Address.StreetNative, CASE WHEN np_Companys.NameNative > '' THEN np_Companys.NameNative "
                           + "ELSE np_Individual.FirstNameNative + ' ' + np_Individual.SurNameNative END AS [Name] "
                           + "FROM au_Users "
                           + "LEFT OUTER JOIN np_Companys ON au_Users.CreditInfoID = np_Companys.CreditInfoID "
                           + "LEFT OUTER JOIN np_Individual ON au_Users.CreditInfoID = np_Individual.CreditInfoID "
                           + "LEFT OUTER JOIN np_Address ON au_Users.CreditInfoID = np_Address.CreditInfoID "
                           + "WHERE ";

            // Fyrir CreditInfoID - Ef �a� er autt �� er leita� a� �llu	
            query += "au_Users.CreditInfoID LIKE('" + CreditInfoID + "%')";

            if (FirstName != "") {
                query += " AND ";

                query += "np_Companys.NameNative LIKE (N'" + FirstName + "%') OR np_Individual.FirstNameNative LIKE (N'" +
                         FirstName + "%')";
            }

            if (Surname != "") {
                query += " AND ";
                query += "np_Individual.SurNameNative LIKE (N'" + Surname + "%')";
            }

            if (Address != "") {
                query += " AND ";
                query += "np_Address.StreetNative LIKE (N'" + Address + "%')";
            }

            if (UserName != "") {
                query += " AND ";
                query += "au_users.UserName LIKE (N'" + UserName + "%')";
            }

            if (OpenClosed == "Open") {
                query += " AND ";
                query += "au_users.IsOpen = 'True'";
            }

            if (OpenClosed == "Closed") {
                query += " AND ";
                query += "au_users.IsOpen = 'False'";
            }

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    var myCommand = new OleDbCommand(query, myConnection);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        public bool IsUserOpen(int UserID) {
            bool iRes = false;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand = new OleDbCommand(
                        "SELECT IsOpen FROM au_users WHERE id = '" + UserID + "'", myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        iRes = Convert.ToBoolean(reader.GetString(0));
                    }

                    // Return output parameters from returned data stream

                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return iRes;
        }

        public bool IsUserExpiryDateOK(int UserID) {
            bool iRes = false;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand = new OleDbCommand(
                        "SELECT OpenUntil FROM au_users WHERE id = '" + UserID + "'", myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();

                    if (reader.Read()) {
                        iRes = reader.GetDateTime(0) > DateTime.Now;
                    }

                    // Return output parameters from returned data stream

                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return iRes;
        }


        public bool ChangePassword(auUsersBLLC user, string newPass)
        {


            var salt = CreateSalt(5);
            var passwordHash = CreatePasswordHash(newPass, salt);
            var passwordChangeDays = AuUserSettings.PASSWORD_CHANGE_DAYS;
            var openUntil = DateTime.Today.AddDays(passwordChangeDays);


            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                                "Update au_users SET PasswordHash= ? , salt = ?, OpenUntil = ?, DateUpdated = ? WHERE ID = ?", myOleDbConn)
                            { Transaction = myTrans };

                    var myParam = new OleDbParameter("PasswordHash", OleDbType.WChar)
                    {
                        Value = passwordHash,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("salt", OleDbType.WChar)
                    {
                        Value = salt,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("OpenUntil", OleDbType.DBDate)
                    {
                        Value = openUntil,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DateUpdated", OleDbType.DBDate)
                    {
                        Value = DateTime.Now,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("ID", OleDbType.Integer)
                    {
                        Value = user.ID,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);
                    
                    new AuditFactory(myOleDbCommand, user.ID).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    myTrans.Commit();


                }
                catch (Exception ex)
                {
                    throw new Exception("Exception updating new passowrd. " + ex.Message);
                }
            }

            return true;


        }


        public bool AccountLock(string userName, bool lockUserAccount)
        {
            // lock or unlock a user account 
            bool iRes = false;
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                                "Update au_users SET lockUserAccount= ?  WHERE UserName = ?", myOleDbConn)
                        { Transaction = myTrans };

                    var myParam = new OleDbParameter("lockUserAccount", OleDbType.Boolean)
                    {
                        Value = lockUserAccount,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);


                    myParam = new OleDbParameter("UserName", OleDbType.VarChar)
                    {
                        Value = userName,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess();
                    iRes = (myOleDbCommand.ExecuteNonQuery() > 0) ? true : false;
                    myTrans.Commit();

                }
                catch(Exception ex)
                {

                    throw new Exception("Error locking/unlocking user account" + ex.Message);

                }

            }

            

         return iRes;
        }


        public bool IsAccountLocked(int userId)
        {
            bool iRes = false;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                try
                {
                    myOleDbCommand = new OleDbCommand(
                        "SELECT lockUserAccount FROM au_users WHERE id = '" + userId + "'", myOleDbConn);
                    
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        iRes = (!reader.IsDBNull(0)) ? reader.GetBoolean(0) : false;
                    }
                    
                    reader.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return iRes;

        }


        public auSearchFilter GetSearchFilters(int filterId, bool isSubscriber)
        {
            

                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {

                var searchFilters = new auSearchFilter();
                try {
                    myOleDbConn.Open();

                    var myOleDbCommand =
                        new OleDbCommand("SELECT isInd, isIndVat, isComp, filterId, isSubscriber FROM au_SearchFilter WHERE au_SearchFilter.filterId = ? AND isSubscriber = ? ", myOleDbConn);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("filterId", OleDbType.Integer)
                    {
                        Value = filterId,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParameter);

                    myParameter = new OleDbParameter("isSubscriber", OleDbType.Integer)
                    {
                        Value = isSubscriber,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParameter);


                    var reader = myOleDbCommand.ExecuteReader();

                    if (reader == null || !reader.Read())
                    {
                        return null;
                    }
                    else
                    {

                        searchFilters.filterId = filterId;
                        searchFilters.isInd = reader.GetBoolean(0);
                        searchFilters.isIndVat = reader.GetBoolean(1);
                        searchFilters.isComp = reader.GetBoolean(2); 
                        searchFilters.isSubscriber = isSubscriber;

                    }
                    
                        reader.Close();
                    
                    }
                    catch (Exception ex)
                    {
                    Logger.WriteToLog(ex.Message, true);

                    }

                return searchFilters;
                }

        }


        /// <summary>
        ///  Uppf�rir tilvik af auSubscribers ni�ur � grunn. Tilvik inniheldur CreditInfoID og allar uppl�singar sem 
        ///  Subscriber taflan �arf � a� halda.
        /// </summary>
        public bool ModifySearchFilters(auSearchFilter searchFilter, bool isUpdate)
        {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString))
            {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();
                var queryString = "";
                if (GetSearchFilters(searchFilter.filterId, searchFilter.isSubscriber) == null) { isUpdate = false; }

                try
                {
                    if (isUpdate)
                    {
                        queryString = "Update au_SearchFilter SET isInd = ?, isIndVat = ?, isComp = ? WHERE filterId = ? AND isSubscriber = ?";
                    }
                    else
                    {
                        queryString = "INSERT INTO au_SearchFilter (isInd, isIndVat, isComp, filterId, isSubscriber) VALUES(?,?,?,?,?)";

                    }

                    myCommand =
                        new OleDbCommand(
                            queryString,
                            myConnection)
                        { Transaction = myTransaction };
                    
                    var myParameter = new OleDbParameter("isInd", OleDbType.Boolean)
                    {
                        Value = searchFilter.isInd,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);
                    
                    myParameter = new OleDbParameter("isIndVat", OleDbType.Boolean)
                    {
                        Value = searchFilter.isIndVat,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);
                    
                    myParameter = new OleDbParameter("isComp", OleDbType.Boolean)
                    {
                        Value = searchFilter.isComp,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);
                    
                    myParameter = new OleDbParameter("filterId", OleDbType.Integer)
                    {
                        Value = searchFilter.filterId,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);
                    
                    myParameter = new OleDbParameter("isSubscriber", OleDbType.Boolean)
                    {
                        Value = searchFilter.isSubscriber, 
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }


    }
}