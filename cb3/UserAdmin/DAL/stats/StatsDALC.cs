using System;
using System.Configuration;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using cb3;

//using UserAdmin;

namespace UserAdmin.DAL.stats {
    /// <summary>
    /// Summary description for StatsDALC.
    /// </summary>
    public class StatsDALC : BaseDALC {

        public int GetNumberOfRegistrationsAddedForPeriod(DateTime FromDate, DateTime ToDate) {
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                //TODO - Dagsetningame�h�ndlun er eitthva� f�tlu� - �arf a� sko�a
                var fromDate = FromDate.Year + "-" + FromDate.Month + "-" + FromDate.Day + " 00:00:00";
                var toDate = ToDate.Year + "-" + ToDate.Month + "-" + ToDate.Day + " 23:59:59";
                var myOleDbCommand =
                    new OleDbCommand(
                        "SELECT COUNT(*) AS Counted FROM np_Claims WHERE (RegDate BETWEEN '" + fromDate + "' AND '" +
                        toDate + "')",
                        myOleDbConn);
                myOleDbCommand.Connection.Open();
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                return 0;
            }
        }

        // Type stendur fyrir annars vegar einstaklinga og hins vegar l�ga�ila
        // 1 = l�ga�ili (company)
        // 2 = einstaklingur (individual)
        public int GetNumberOfRegistrationsAddedForPeriodAndType(DateTime FromDate, DateTime ToDate, int Type) {
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                string fromDate = FromDate.Year + "-" + FromDate.Month + "-" + FromDate.Day + " 00:00:00";
                string toDate = ToDate.Year + "-" + ToDate.Month + "-" + ToDate.Day + " 23:59:59";
                var myOleDbCommand =
                    new OleDbCommand(
                        "SELECT COUNT(DISTINCT np_Claims.CreditInfoID) AS Counted FROM np_Claims INNER JOIN np_CreditInfoUser ON np_Claims.CreditInfoID = np_CreditInfoUser.CreditInfoID WHERE (RegDate BETWEEN '" +
                        fromDate + "' AND '" + toDate + "') AND (np_CreditInfoUser.Type = " + Type + ")",
                        myOleDbConn);
                myOleDbCommand.Connection.Open();
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                return 0;
            }
        }
    }
}