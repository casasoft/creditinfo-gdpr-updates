using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using Logging.BLL;
using UserAdmin.BLL.auDepartments;
using cb3;
using cb3.Audit;

namespace UserAdmin.DAL.auDepartments {
    /// <summary>
    /// Summary description for auDepartmentsDALC.
    /// </summary>
    public class auDepartmentsDALC : BaseDALC {

        /// <summary>
        /// Writes a department entity to database.
        /// </summary>
        /// <param name="myDepartment">Department entity</param>
        public bool Insert(auDepartmentBLLC myDepartment) {
            if (myDepartment.CreditInfoID <= 0) {
                return false;
            }

            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp INSERT skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                            "INSERT INTO c_au_department(CreditInfoID, NameNative, NameEN, Postcode, StreetNative, StreetEN, Phone, Email, ContactPerson, IsOpen, IsHeadOffice, BillingAccount) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
                            myConnection) {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = myDepartment.CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir NameNative
                    myParameter = new OleDbParameter("NameNative", OleDbType.WChar)
                                  {
                                      Value = myDepartment.NameNative,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir NameEN
                    myParameter = new OleDbParameter("NameEN", OleDbType.WChar)
                                  {
                                      Value = myDepartment.NameEN,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Postcode
                    myParameter = new OleDbParameter("Postcode", OleDbType.WChar)
                                  {
                                      Value = myDepartment.Postcode,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir StreetNative
                    myParameter = new OleDbParameter("StreetNative", OleDbType.WChar)
                                  {
                                      Value = myDepartment.StreetNative,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir StreetEN
                    myParameter = new OleDbParameter("StreetEN", OleDbType.WChar)
                                  {
                                      Value = myDepartment.StreetEN,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Phone
                    myParameter = new OleDbParameter("Phone", OleDbType.WChar)
                                  {
                                      Value = myDepartment.Phone,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Email
                    myParameter = new OleDbParameter("Email", OleDbType.WChar)
                                  {
                                      Value = myDepartment.Email,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir ContactPerson
                    myParameter = new OleDbParameter("ContactPerson", OleDbType.WChar)
                                  {
                                      Value = myDepartment.ContactPerson,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsOpen
                    myParameter = new OleDbParameter("IsOpen", OleDbType.Boolean)
                                  {
                                      Value = myDepartment.IsOpen,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsHeadOffice
                    myParameter = new OleDbParameter("IsHeadOffice", OleDbType.Boolean)
                                  {
                                      Value = myDepartment.IsHeadOffice,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // For BillingAccount
                    myParameter = new OleDbParameter("BillingAccount", OleDbType.WChar)
                                  {
                                      Value = myDepartment.BillingAccount,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Updates a specfic Department entity.
        /// </summary>
        /// <param name="myDepartment">Department entity</param>
        public bool Update(auDepartmentBLLC myDepartment) {
            if (myDepartment.DepartmentID <= 0) {
                return false;
            }

            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp UPDATE skipun fyrir gagnagrunninn me� params...
                    string strSql = "UPDATE c_au_department "
                                    +
                                    "SET NameNative=?, NameEN=?, Postcode=?, StreetNative=?, StreetEN=?, Phone=?, Email=?, ContactPerson=?, IsOpen=?, IsHeadOffice=?, BillingAccount=? "
                                    + "WHERE DepartmentID=? ";
                    myCommand = new OleDbCommand(strSql, myConnection) {Transaction = myTransaction};

                    // Fyrir NameNative
                    var myParameter = new OleDbParameter("NameNative", OleDbType.WChar)
                                      {
                                          Value = myDepartment.NameNative,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir NameEN
                    myParameter = new OleDbParameter("NameEN", OleDbType.WChar)
                                  {
                                      Value = myDepartment.NameEN,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Postcode
                    myParameter = new OleDbParameter("Postcode", OleDbType.WChar)
                                  {
                                      Value = myDepartment.Postcode,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir StreetNative
                    myParameter = new OleDbParameter("StreetNative", OleDbType.WChar)
                                  {
                                      Value = myDepartment.StreetNative,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir StreetEN
                    myParameter = new OleDbParameter("StreetEN", OleDbType.WChar)
                                  {
                                      Value = myDepartment.StreetEN,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Phone
                    myParameter = new OleDbParameter("Phone", OleDbType.WChar)
                                  {
                                      Value = myDepartment.Phone,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Email
                    myParameter = new OleDbParameter("Email", OleDbType.WChar)
                                  {
                                      Value = myDepartment.Email,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir ContactPerson
                    myParameter = new OleDbParameter("ContactPerson", OleDbType.WChar)
                                  {
                                      Value = myDepartment.ContactPerson,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsOpen
                    myParameter = new OleDbParameter("IsOpen", OleDbType.Boolean)
                                  {
                                      Value = myDepartment.IsOpen,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsHeadOffice
                    myParameter = new OleDbParameter("IsHeadOffice", OleDbType.Boolean)
                                  {
                                      Value = myDepartment.IsHeadOffice,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // For BillingAccount
                    myParameter = new OleDbParameter("BillingAccount", OleDbType.WChar)
                                  {
                                      Value = myDepartment.BillingAccount,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir DepartmentID
                    myParameter = new OleDbParameter("DepartmentID", OleDbType.Integer)
                                  {
                                      Value = myDepartment.DepartmentID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();

                    if (!myDepartment.IsOpen) {
                        strSql = "UPDATE au_users SET IsOpen=? WHERE DepartmentID=?";
                        myCommand = new OleDbCommand(strSql, myConnection) {Transaction = myTransaction};

                        // Fyrir IsOpen
                        myParameter = new OleDbParameter("IsOpen", OleDbType.WChar, 5)
                                      {
                                          Value = myDepartment.IsOpen,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir DepartmentID
                        myParameter = new OleDbParameter("DepartmentID", OleDbType.Integer)
                                      {
                                          Value = myDepartment.DepartmentID,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    }
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Updates only the IsOpen attribute for a specific department
        /// </summary>
        /// <param name="myDepartment">Department entity</param>
        /// <returns>True/False</returns>
        public bool Enable(auDepartmentBLLC myDepartment) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp UPDATE skipun fyrir gagnagrunninn me� params...
                    const string strSql = "UPDATE c_au_department "
                                          + "SET IsOpen=? "
                                          + "WHERE DepartmentID=? ";
                    myCommand = new OleDbCommand(strSql, myConnection) {Transaction = myTransaction};

                    // Fyrir IsOpen
                    var myParameter = new OleDbParameter("IsOpen", OleDbType.Boolean)
                                      {
                                          Value = myDepartment.IsOpen,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir DepartmentID
                    myParameter = new OleDbParameter("DepartmentID", OleDbType.Integer)
                                  {
                                      Value = myDepartment.DepartmentID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Fetches all department entries for a specific subscriber(CIID). 
        /// </summary>
        /// <param name="CIID">CreditInfoID identifying a subscriber.</param>
        /// <returns>Set of department data.</returns>
        public DataSet GetDepartmentsAsDataSet(int CIID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();

            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    var sbSql = new StringBuilder();
                    sbSql.Append(
                        "SELECT c_au_department.DepartmentID, c_au_department.NameNative, c_au_department.NameEN, c_au_department.Phone, "
                        +
                        "c_au_department.Email, c_au_department.ContactPerson, c_au_department.IsOpen, Users.UserCount, c_au_department.BillingAccount ");
                    sbSql.Append(
                        "FROM c_au_department LEFT OUTER JOIN (SELECT COUNT(id) AS UserCount, DepartmentID FROM au_users WHERE IsOpen='True' GROUP BY DepartmentID) Users ON c_au_department.DepartmentID = Users.DepartmentID ");
                    sbSql.Append("WHERE c_au_department.CreditInfoID=" + CIID);
                    var myCommand = new OleDbCommand(sbSql.ToString(), myConnection);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Fetches all department entries for a specific subscriber(CIID). 
        /// </summary>
        /// <param name="CIID">CreditInfoID identifying a subscriber.</param>
        /// <param name="IsOpen">Is department open or closed.</param>
        /// <returns>Set of department data.</returns>
        public DataSet GetDepartmentsAsDataSet(int CIID, bool IsOpen) {
            // �tb� t�mt dataset
            var mySet = new DataSet();

            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    var sbSql = new StringBuilder();
                    sbSql.Append(
                        "SELECT c_au_department.DepartmentID, c_au_department.NameNative, c_au_department.NameEN, c_au_department.Phone, "
                        +
                        "c_au_department.Email, c_au_department.ContactPerson, c_au_department.IsOpen, Users.UserCount, c_au_department.BillingAccount ");
                    sbSql.Append(
                        "FROM c_au_department LEFT OUTER JOIN (SELECT COUNT(id) AS UserCount, DepartmentID FROM au_users WHERE IsOpen='True' GROUP BY DepartmentID) Users ON c_au_department.DepartmentID = Users.DepartmentID ");
                    if (IsOpen) {
                        sbSql.Append("WHERE c_au_department.CreditInfoID=" + CIID + " AND c_au_department.IsOpen=1");
                    } else {
                        sbSql.Append("WHERE c_au_department.CreditInfoID=" + CIID + " AND c_au_department.IsOpen=0");
                    }
                    var myCommand = new OleDbCommand(sbSql.ToString(), myConnection);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Fetches one specific deparment from database.
        /// </summary>
        /// <param name="DepartmentID">Unique ID for a department</param>
        /// <returns>Department entity</returns>
        public auDepartmentBLLC Get(int DepartmentID) {
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                var myOleDbCommand = new OleDbCommand(
                    "SELECT * FROM c_au_department WHERE DepartmentID=?", myOleDbConn);

                // DepartmentID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                var myParameter = new OleDbParameter("DepartmentID", OleDbType.Integer)
                                  {
                                      Value = DepartmentID,
                                      Direction = ParameterDirection.Input
                                  };
                myOleDbCommand.Parameters.Add(myParameter);

                var reader = myOleDbCommand.ExecuteReader();
                var department = new auDepartmentBLLC();

                if (!reader.Read()) {
                    return null;
                }
                department.DepartmentID = reader.GetInt32(0);
                department.CreditInfoID = reader.GetInt32(1);

                if (!reader.IsDBNull(2)) {
                    department.NameNative = reader.GetString(2);
                }
                if (!reader.IsDBNull(3)) {
                    department.NameEN = reader.GetString(3);
                }
                if (!reader.IsDBNull(4)) {
                    department.Postcode = reader.GetString(4);
                }
                if (!reader.IsDBNull(5)) {
                    department.StreetNative = reader.GetString(5);
                }
                if (!reader.IsDBNull(6)) {
                    department.StreetEN = reader.GetString(6);
                }
                if (!reader.IsDBNull(7)) {
                    department.Phone = reader.GetString(7);
                }
                if (!reader.IsDBNull(8)) {
                    department.Email = reader.GetString(8);
                }
                if (!reader.IsDBNull(9)) {
                    department.ContactPerson = reader.GetString(9);
                }

                department.IsOpen = reader.GetBoolean(10);
                department.IsHeadOffice = reader.GetBoolean(11);

                if (!reader.IsDBNull(12)) {
                    department.BillingAccount = reader.GetString(12);
                }

                reader.Close();
                return department;
            }
        }

        /// <summary>
        /// Check if a department that a user belongs to is open.
        /// </summary>
        /// <param name="UserID">Unique ID of an user.</param>
        /// <returns>Open(true)/Closed(false).</returns>
        public bool IsOpen(int UserID) {
            bool iRes = false;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    var strSql = "SELECT c_au_department.IsOpen "
                                    +
                                    "FROM c_au_department INNER JOIN au_users ON c_au_department.DepartmentID = au_users.DepartmentID "
                                    + "WHERE au_users.id=" + UserID;
                    myOleDbCommand = new OleDbCommand(strSql, myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            iRes = reader.GetBoolean(0);
                        }
                    }

                    // Return output parameters from returned data stream

                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return iRes;
        }
    }
}