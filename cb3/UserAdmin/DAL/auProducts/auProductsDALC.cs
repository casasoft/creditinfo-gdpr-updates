using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using Logging.BLL;
using UserAdmin.BLL.auProducts;
using cb3;
using System.Collections.Generic;
using cb3.Audit;

namespace UserAdmin.DAL.auProducts {
    /// <summary>
    /// Summary description for auProductsDALC.
    /// </summary>
    public class auProductsDALC : BaseDALC {

        public DataSet GetAllTheOpenProducts(bool isMenuNeeded) {
            // �tb� t�mt dataset
            var mySet = new DataSet();

            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    var strMenu = "";
                    if (isMenuNeeded) {
                        strMenu = " AND Menu='True'";
                    }
                    myConnection.Open();
                    var myCommand = new OleDbCommand(
                        "SELECT * FROM au_Products WHERE IsOpen = 'True'" + strMenu +
                        " ORDER BY ProductID, ParentID",
                        myConnection);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        public DataSet ReadUserProducts(int UserID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();

            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand("SELECT * FROM au_ProductToUser WHERE UserId = ?", myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }


        public DataSet ReadGroupProducts(int GroupID)
        {
            // �tb� t�mt dataset
            var mySet = new DataSet();

            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString))
            {
                try
                {
                    myConnection.Open();
                    myCommand = new OleDbCommand("SELECT * FROM au_ProductToGroup WHERE GroupID = ?", myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("GroupID", OleDbType.Integer)
                    {
                        Value = GroupID,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter { SelectCommand = myCommand };

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }


      

        public bool WriteUserProducts(auProductsBLLC[] myProductArray, int UserID) {
            if (!DeleteUserProducts(UserID)) {
                return false;
            }

            OleDbCommand myCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                OleDbTransaction myTransaction = null;
                try {
                    for (var i = 0; i < myProductArray.Length; i++) {
                        if (myProductArray[i] == null) {
                            continue;
                        }
                        // insert into au_ProductToUser
                        myTransaction = myOleDbConn.BeginTransaction();
                        myCommand = new OleDbCommand(
                            "INSERT INTO au_ProductToUser(UserID, ProductID) VALUES(?,?)", myOleDbConn)
                                    {Transaction = myTransaction};

                        var myParam = new OleDbParameter("UserID", OleDbType.Integer)
                                                 {
                                                     Value = myProductArray[i].UserID,
                                                     Direction = ParameterDirection.Input
                                                 };
                        myCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ProductID", OleDbType.Integer)
                                  {
                                      Value = myProductArray[i].ProductID,
                                      Direction = ParameterDirection.Input
                                  };
                        myCommand.Parameters.Add(myParam);

                        new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                        myTransaction.Commit();
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    if (myTransaction != null) {
                        myTransaction.Rollback();
                    }
                    return false;
                }
            }
            return true;
        }


        public bool WriteGroupProducts(auProductsBLLC[] myProductArray, int GroupID)
        {
            if (!DeleteGroupProducts(GroupID))
            {
                return false;
            }

            OleDbCommand myCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                OleDbTransaction myTransaction = null;
                try
                {
                    for (var i = 0; i < myProductArray.Length; i++)
                    {
                        if (myProductArray[i] == null)
                        {
                            continue;
                        }
                        // insert into au_ProductToUser
                        myTransaction = myOleDbConn.BeginTransaction();
                        myCommand = new OleDbCommand(
                                "INSERT INTO au_ProductToGroup(GroupID, ProductID) VALUES(?,?)", myOleDbConn)
                            { Transaction = myTransaction };

                        var myParam = new OleDbParameter("GroupID", OleDbType.Integer)
                        {
                            Value = myProductArray[i].UserID,
                            Direction = ParameterDirection.Input
                        };
                        myCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ProductID", OleDbType.Integer)
                        {
                            Value = myProductArray[i].ProductID,
                            Direction = ParameterDirection.Input
                        };
                        myCommand.Parameters.Add(myParam);

                        new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                        myTransaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.Message, true);
                    if (myTransaction != null)
                    {
                        myTransaction.Rollback();
                    }
                    return false;
                }
            }
            return true;
        }



        private static bool DeleteUserProducts(int UserID) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("DELETE FROM au_ProductToUser WHERE UserID = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir Initials
                    var myParam = new OleDbParameter("UserID", OleDbType.Integer)
                                  {
                                      Value = UserID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParam);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                    return true;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
            }
        }

        public bool DeleteGroupProducts(int GroupID)
        {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString))
            {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try
                {
                    myCommand = new OleDbCommand("DELETE FROM au_ProductToGroup WHERE GroupID = ?", myConnection)
                    { Transaction = myTransaction };
                    
                    var myParam = new OleDbParameter("GroupID", OleDbType.Integer)
                    {
                        Value = GroupID,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParam);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
            }
        }

        public bool HasUserAccessToProduct(int UserID, int ProductID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();

            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand =
                        new OleDbCommand(
                            "SELECT Count(*) AS COUNTER FROM au_ProductToUser WHERE UserId = ? AND ProductID = ?",
                            myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // ProductID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("ProductID", OleDbType.Integer)
                                  {
                                      Value = ProductID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return (mySet.Tables[0].Rows[0]["COUNTER"].ToString() == "1");
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return false;
                }
            }
        }

        public string GetParentID(string strProductID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand("SELECT ParentID FROM au_products WHERE ProductID = ?", myConnection);

                    // ProductID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("ProductID", OleDbType.Integer)
                                      {
                                          Value = strProductID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet.Tables[0].Rows[0]["ParentID"].ToString();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        public IList<string> GetSearchAndTafReportsDesctiptions()
        {
            var connString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(connString))
            {
                try
                {
                    var setHelper = new DataSet();
                    myConnection.Open();
                    var myCommand = new OleDbCommand(
                        @"select ProductDescriptionNative from au_Products
                        where ProductNameNative = 'TAF-report'  or ProductNameEN = 'Monitoring'", 
                        myConnection);
                    var myAdapter = new OleDbDataAdapter { SelectCommand = myCommand };
                    myAdapter.Fill(setHelper);

                    var results = new List<string>();
                    results.Add(setHelper.Tables[0].Rows[0][0].ToString());
                    results.Add(setHelper.Tables[0].Rows[1][0].ToString());

                    return results;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }
    }
}