using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Text;
using DataProtection;
using Logging.BLL;
using UserAdmin.BLL.auSubscribers;
using UserAdmin.Localization;
using cb3;
using cb3.Audit;
using Cig.Framework.Base.Configuration;


namespace UserAdmin.DAL.auSubscribers {
    /// <summary>
    /// Summary description for auSubscribersDALC.
    /// </summary>
    public class auSubscribersDALC : BaseDALC {

        /// <summary>
        ///  Skrifar tilvik af auSubscribers ni�ur � grunn. Tilvik inniheldur CreditInfoID og allar uppl�singar sem 
        ///  Subscriber taflan �arf � a� halda.
        /// </summary>
        public bool AddNewSubscriber(auSubscribersBLLC mySubscriber) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp INSERT skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                            "INSERT INTO au_Subscribers (CreditInfoID, RegisteredBy, SubscriberNickName, NationalID, MaxUsers, IsOpen, Updated, Created, isPrintBlocked) VALUES(?,?,?,?,?,?,?,?,?)",
                            myConnection) {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = mySubscriber.CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir RegisteredBy
                    myParameter = new OleDbParameter("RegisteredBy", OleDbType.Integer)
                                  {
                                      Value = mySubscriber.RegisteredBy,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir SubscriberNickName
                    myParameter = new OleDbParameter("SubscriberNickName", OleDbType.WChar)
                                  {
                                      Value = mySubscriber.SubscriberNickName,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir NationalID
                    myParameter = new OleDbParameter("NationalID", OleDbType.Integer)
                                  {
                                      Value = mySubscriber.NationalID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir MaxUsers
                    myParameter = new OleDbParameter("MaxUsers", OleDbType.Integer)
                                  {
                                      Value = mySubscriber.MaxUsers,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir isOpen
                    myParameter = new OleDbParameter("isOpen", OleDbType.Char, 10)
                                  {
                                      Value = mySubscriber.isOpen,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Updated
                    myParameter = new OleDbParameter("Updated", OleDbType.Date)
                                  {
                                      Value = mySubscriber.Updated,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Created (13.11.2003 GPG b�tti vi�)
                    myParameter = new OleDbParameter("Created", OleDbType.Date)
                                  {
                                      Value = mySubscriber.Created,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);


                    myParameter = new OleDbParameter("isPrintBlocked", OleDbType.Boolean)
                    {
                        Value = mySubscriber.isPrintBlocked,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);


                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();

                    // Create the default department
                    myCommand =
                        new OleDbCommand(
                            "INSERT INTO c_au_department(CreditInfoID, NameNative, NameEN, IsOpen, IsHeadOffice) VALUES(?,?,?,?,?)",
                            myConnection) {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                  {
                                      Value = mySubscriber.CreditInfoID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir NameNative
                    var ciTemp = new CultureInfo(CigConfig.Configure("lookupsettings.nativeCulture"));
                    var rm = CIResource.CurrentManager;
                    myParameter = new OleDbParameter("NameNative", OleDbType.WChar)
                                  {
                                      Value = rm.GetString("txtDepartmentDefault", ciTemp),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir NameEN
                    ciTemp = new CultureInfo("en-US");
                    myParameter = new OleDbParameter("NameEN", OleDbType.WChar)
                                  {
                                      Value = rm.GetString("txtDepartmentDefault", ciTemp),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsOpen
                    myParameter = new OleDbParameter("IsOpen", OleDbType.Boolean)
                                  {
                                      Value = mySubscriber.isOpen,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsHeadOffice
                    myParameter = new OleDbParameter("IsHeadOffice", OleDbType.Boolean)
                                  {
                                      Value = true,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();

                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        ///  Uppf�rir tilvik af auSubscribers ni�ur � grunn. Tilvik inniheldur CreditInfoID og allar uppl�singar sem 
        ///  Subscriber taflan �arf � a� halda.
        /// </summary>
        public bool UpdateSubscriber(auSubscribersBLLC mySubscriber) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp UPDATE skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                            "Update au_Subscribers SET SubscriberNickName = ?, NationalID = ?, MaxUsers = ?, IsOpen = ?, Updated = ?, isPrintBlocked = ? WHERE CreditInfoID = ?",
                            myConnection) {Transaction = myTransaction};

                    // Fyrir SubscriberNickName
                    var myParameter = new OleDbParameter("SubscriberNickName", OleDbType.WChar)
                                      {
                                          Value = mySubscriber.SubscriberNickName,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir NationalID
                    myParameter = new OleDbParameter("NationalID", OleDbType.Integer)
                                  {
                                      Value = mySubscriber.NationalID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir MaxUsers
                    myParameter = new OleDbParameter("MaxUsers", OleDbType.Integer)
                                  {
                                      Value = mySubscriber.MaxUsers,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir isOpen
                    myParameter = new OleDbParameter("isOpen", OleDbType.Char, 10)
                                  {
                                      Value = mySubscriber.isOpen,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Updated
                    myParameter = new OleDbParameter("Updated", OleDbType.Date)
                                  {
                                      Value = mySubscriber.Updated,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    myParameter = new OleDbParameter("isPrintBlocked", OleDbType.Boolean, 10)
                    {
                        Value = mySubscriber.isPrintBlocked,
                        Direction = ParameterDirection.Input
                    };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir CreditInfoID
                    myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                  {
                                      Value = mySubscriber.CreditInfoID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Skilar einum tilteknum Subscriber eftir CreditInfoID
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetOneSubscriberByCreditInfoIDAsDataSet(int CreditInfoID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();

            OleDbCommand myCommand;
            var myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand(
                        "SELECT * FROM au_Subscribers WHERE au_Subscribers.CreditInfoID = ?", myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        public DataSet GetAllSubscribersAsDataSet() {
            // �tb� t�mt dataset
            var mySet = new DataSet();

            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    var myCommand = new OleDbCommand(
                        "SELECT au_Subscribers.*, CASE WHEN np_Companys.NameNative > '' THEN np_Companys.NameNative ELSE np_Individual.FirstNameNative + ' ' + np_Individual.SurNameNative END AS [Name] FROM au_Subscribers LEFT OUTER JOIN np_Companys ON au_Subscribers.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Individual ON au_Subscribers.CreditInfoID = np_Individual.CreditInfoID",
                        myConnection);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Skilar einum tilteknum Subscriber eftir CreditInfoID
        /// </summary>
        /// <returns>DataSet</returns>
        public auSubscribersBLLC GetOneSubscriberByCreditInfoIDAsSubscriber(int CreditInfoID) {
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                var myOleDbCommand =
                    new OleDbCommand("SELECT * FROM au_Subscribers WHERE au_Subscribers.CreditInfoID = ?", myOleDbConn);

                // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                  {
                                      Value = CreditInfoID,
                                      Direction = ParameterDirection.Input
                                  };
                myOleDbCommand.Parameters.Add(myParameter);

                var reader = myOleDbCommand.ExecuteReader();
                var mySubscriber = new auSubscribersBLLC();

                if (!reader.Read()) {
                    return null;
                }
                mySubscriber.ID = reader.GetInt32(0);
                mySubscriber.CreditInfoID = reader.GetInt32(1);

                if (!reader.IsDBNull(2)) {
                    mySubscriber.RegisteredBy = reader.GetInt32(2);
                }

                if (!reader.IsDBNull(3)) {
                    mySubscriber.SubscriberNickName = reader.GetString(3);
                }

                if (!reader.IsDBNull(4)) {
                    mySubscriber.NationalID = reader.GetInt32(4);
                }

                if (!reader.IsDBNull(5)) {
                    mySubscriber.MaxUsers = reader.GetInt32(5);
                }

                mySubscriber.isOpen = reader.GetString(6);
                mySubscriber.Updated = reader.GetDateTime(7);
                mySubscriber.Created = reader.GetDateTime(8);
                if (!reader.IsDBNull(9))
                {
                    mySubscriber.isPrintBlocked = reader.GetBoolean(9);
                }

                /* Spurning um a� setja inn collection fyrir notendur undir subscriber...
					 * 
					myIndi.Address = GetAddressList(myIndi.CreditInfoID);
					myIndi.Number = GetPhoneNumberList(myIndi.CreditInfoID);
					myIndi.IDNumbers = GetIDNumberList(myIndi.CreditInfoID);
					 *
					 */
                reader.Close();
                return mySubscriber;
            }
        }

        public int GetSubscriberUserLimit(int CreditInfoID) {
            int Result = -1;

            OleDbCommand myCommand;

            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myConnection.Open();
                try {
                    myCommand =
                        new OleDbCommand(
                            "SELECT MaxUsers FROM au_Subscribers WHERE CreditInfoID = '" + CreditInfoID + "'",
                            myConnection);

                    OleDbDataReader reader = myCommand.ExecuteReader();

                    if (reader.Read()) {
                        Result = reader.GetInt32(0);
                    }

                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return Result;
        }

        /// <summary>
        /// Fetched the number of open users already created for a specific susbscriber.
        /// </summary>
        /// <param name="CreditInfoID">Unique CIID of the subscriber.</param>
        /// <returns>Number of users.</returns>
        public int GetSubscriberUserCount(int CreditInfoID) {
            int Result = -1;

            OleDbCommand myCommand;
            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myConnection.Open();
                try {
                    string strSql = "SELECT COUNT(au_users.id) AS NrOfUsers "
                                    +
                                    "FROM c_au_department INNER JOIN au_users ON c_au_department.DepartmentID = au_users.DepartmentID "
                                    + "WHERE au_users.IsOpen='True' AND c_au_department.CreditInfoID=" + CreditInfoID;
                    myCommand = new OleDbCommand(strSql, myConnection);
                    var reader = myCommand.ExecuteReader();
                    if (reader.Read()) {
                        Result = reader.GetInt32(0);
                    }
                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return Result;
        }

        public bool IsSubscriberOpen(int UserID) {
            bool iRes = false;
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT dbo.au_Subscribers.IsOpen FROM dbo.au_Subscribers INNER JOIN dbo.au_users ON dbo.au_Subscribers.CreditInfoID = dbo.au_users.SubscriberID WHERE dbo.au_users.id = '" +
                            UserID + "'",
                            myOleDbConn);

                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        iRes = Convert.ToBoolean(reader.GetString(0));
                    }

                    // Return output parameters from returned data stream

                    reader.Close();
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return iRes;
        }

        public DataSet SearchSubscribers(
            string CreditInfoID,
            string NationalID,
            string FirstName,
            string Surname,
            string Address,
            string RegisteredBy,
            string OpenClosed) {
            var mySet = new DataSet();
            bool whereStatement = false;
            string myConnectionString = DatabaseHelper.ConnectionString();

            string query = "SELECT au_Subscribers.*, np_IDNumbers.Number NID, np_Address.StreetNative, au_users.UserName, CASE WHEN np_Companys.NameNative > '' THEN np_Companys.NameNative "
                           + "ELSE np_Individual.FirstNameNative + ' ' + np_Individual.SurNameNative END AS [Name] "
                           + "FROM au_Subscribers "
                           + "LEFT OUTER JOIN np_Companys ON au_Subscribers.CreditInfoID = np_Companys.CreditInfoID "
                           +
                           "LEFT OUTER JOIN np_Individual ON au_Subscribers.CreditInfoID = np_Individual.CreditInfoID "
                           + "LEFT OUTER JOIN np_Address ON au_Subscribers.CreditInfoID = np_Address.CreditInfoID "
                           + "LEFT OUTER JOIN au_users ON au_Subscribers.RegisteredBy = au_users.id "
                           + "LEFT OUTER JOIN np_IDNumbers ON au_Subscribers.CreditInfoID = np_IDNumbers.CreditInfoID "
                           + "WHERE ";

            // Fyrir CreditInfoID - Ef �a� er autt �� er leita� a� �llu	
            query += "au_Subscribers.CreditInfoID LIKE('" + CreditInfoID + "%')";
            whereStatement = true;
            // Fyrir national id - Ef �a� er autt �� er leita� a� �llu (23.03 GPG Ef �etta Where skilyr�i er sett er ni�ursta�an a�eins �eir
            // suscriberar sem eru me� nationalID skr��
            if (!string.IsNullOrEmpty(NationalID)) {
                query += " and np_IDNumbers.Number LIKE('" + NationalID + "%')";
            }
            if (FirstName != "") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "np_Companys.NameNative LIKE (N'" + FirstName + "%') OR np_Individual.FirstNameNative LIKE (N'" +
                         FirstName + "%')";
                whereStatement = true;
            }

            if (Surname != "") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "np_Individual.SurNameNative LIKE (N'" + Surname + "%')";
                whereStatement = true;
            }

            if (Address != "") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "np_Address.StreetNative LIKE (N'" + Address + "%')";
                whereStatement = true;
            }

            if (RegisteredBy != "") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "au_users.UserName LIKE (N'" + RegisteredBy + "%')";
                whereStatement = true;
            }

            if (OpenClosed == "Open") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "au_Subscribers.IsOpen = 'True'";
                whereStatement = true;
            }

            if (OpenClosed == "Closed") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "au_Subscribers.IsOpen = 'False'";
                whereStatement = true;
            }

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    var myCommand = new OleDbCommand(query, myConnection);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }
    }
}