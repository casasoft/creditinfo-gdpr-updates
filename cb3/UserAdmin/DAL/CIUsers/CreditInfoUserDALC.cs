using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using cb3;

using Cig.Framework.Base.Configuration;
using System.Collections.Generic;

using System.Data.SqlClient;
using cb3.Audit;

namespace UserAdmin.DAL.CIUsers
{
    /// <summary>
    /// Summary description for DebtorDALC.
    /// </summary>
    public class CreditInfoUserDALC : BaseDALC
    {
        private const string className = "CreditInfoUserDALC";
        // From NPayments user part
        /// <summary>
        /// Takes debtor CIID and returns (Debtor) instance
        /// </summary>
        /// <param name="DebtorID">The debtors CIID</param>
        /// <returns>Instance of debtor</returns>
        public Debtor GetIndivitual(string DebtorID)
        {
            const string funcName = "GetIndivitual(string DebtorID)";

            var myDebtor = new Debtor();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var strSql =
                        "SELECT * FROM np_Individual FULL OUTER JOIN dbo.np_Notes ON dbo.np_Individual.CreditInfoID = dbo.np_Notes.CreditInfoID WHERE CreditInfoID =" +
                        Int32.Parse(DebtorID) + "";
                    var myOleDbCommand = new OleDbCommand(strSql, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    while (reader.NextResult())
                    {
                        myDebtor.CreditInfo = reader.GetInt32(1);
                        myDebtor.Type = reader.GetString(2);
                        myDebtor.FirstName = reader.GetString(3);
                        myDebtor.FirstNameEN = reader.GetString(4);
                        myDebtor.SurName = reader.GetString(5);
                        myDebtor.SurNameEN = reader.GetString(6);
                        myDebtor.ProfessionNameNative = reader.GetString(7);
                        myDebtor.ProfessionNameEN = reader.GetString(8);
                        myDebtor.ProfessionDescriptionEN = reader.GetString(9);
                        myDebtor.ProfessionDescriptionNative = reader.GetString(10);
                        myDebtor.EducationEN = reader.GetString(11);
                        myDebtor.EducationNative = reader.GetString(12);
                        myDebtor.EducationDescriptionEN = reader.GetString(13);
                        myDebtor.EducationDescriptionNative = reader.GetString(14);
                        myDebtor.Note = reader.GetString(17);
                        // get the collections
                        myDebtor.Address = GetAddressList(myDebtor.CreditInfo);
                        myDebtor.Number = GetPhoneNumberList(myDebtor.CreditInfo);
                        myDebtor.IDNumber = GetIDNumberList(myDebtor.CreditInfo);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
                return myDebtor;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DebtorID"></param>
        /// <returns></returns>
        public Debtor GetDDDIndivitual(string DebtorID)
        {
            const string funcName = "GetIndivitual(string DebtorID)";


            var myDebtor = new Debtor();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    //	myOleDbConn.Open();
                    // begin transaction...
                    //	var myTrans = myOleDbConn.BeginTransaction();
                    /*	myOleDbCommand = new OleDbCommand("UPDATE np_Claims SET StatusID = 3, AnnouncementDate = ? WHERE ID = ?" ,myOleDbConn);
					myOleDbCommand.Transaction = myTrans;
					var myParam = new OleDbParameter("AnnouncementDate",OleDbType.Date);
					myParam.Value = DateTime.Now;
					myParam.Direction = ParameterDirection.Input;
					myOleDbCommand.Parameters.Add(myParam);
					myParam = new OleDbParameter("ClaimID",OleDbType.Integer);
					myParam.Value = claimID;
					myParam.Direction = ParameterDirection.Input;
					myOleDbCommand.Parameters.Add(myParam);
					new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
				*/
                    myOleDbConn.Open();
                    var myTrans = myOleDbConn.BeginTransaction();
                    // SELECT * FROM np_Individual RIGHT OUTER JOIN np_Claims ON np_Claims.CreditinfoID = np_Individual.CreditinfoID WHERE np_Individual.CreditInfoID = 2077280845 AND np_Claims.StatusID= 4
                    var myOleDbCommand = new OleDbCommand(
                        "SELECT * FROM np_Individual RIGHT OUTER JOIN np_Claims ON np_Claims.CreditinfoID = np_Individual.CreditinfoID WHERE np_Individual.CreditInfoID = ? AND np_Claims.StatusID= 4",
                        myOleDbConn) { Transaction = myTrans };
                    var reader = myOleDbCommand.ExecuteReader();

                    /*	var strSql = "SELECT * FROM np_Individual WHERE CreditInfoID ="+ Int32.Parse(DebtorID) +"";
					var myOleDbCommand = new OleDbCommand(strSql,myOleDbConn);
					var reader = myOleDbCommand.ExecuteReader();
				*/
                    while (reader.NextResult())
                    {
                        myDebtor.CreditInfo = reader.GetInt32(1);
                        myDebtor.Type = reader.GetString(2);
                        myDebtor.FirstName = reader.GetString(3);
                        myDebtor.FirstNameEN = reader.GetString(4);
                        myDebtor.SurName = reader.GetString(5);
                        myDebtor.SurNameEN = reader.GetString(6);
                        myDebtor.ProfessionNameNative = reader.GetString(7);
                        myDebtor.ProfessionNameEN = reader.GetString(8);
                        myDebtor.ProfessionDescriptionEN = reader.GetString(9);
                        myDebtor.ProfessionDescriptionNative = reader.GetString(10);
                        myDebtor.EducationEN = reader.GetString(11);
                        myDebtor.EducationNative = reader.GetString(12);
                        myDebtor.EducationDescriptionEN = reader.GetString(13);
                        myDebtor.EducationDescriptionNative = reader.GetString(14);

                        // get the collections
                        myDebtor.Address = GetAddressList(myDebtor.CreditInfo);
                        myDebtor.Number = GetPhoneNumberList(myDebtor.CreditInfo);
                        myDebtor.IDNumber = GetIDNumberList(myDebtor.CreditInfo);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
                return myDebtor;
            }
        }

        /// <summary>
        /// Takes national id and returns individual information from national registry
        /// </summary>
        /// <param name="NationalID">The national id</param>
        /// <returns>Instance of individual</returns>
        public Indivitual GetIndivitualFromNationalRegistry(string NationalID)
        {
            const string funcName = "GetIndivitualFromNationalRegistry(string NationalID)";

            var myDebtor = new Indivitual();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT IDNumber, Name, PostNumber, Address FROM ros_nationalinterfaceview WHERE IDNumber ='" +
                            NationalID + "'",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        myDebtor.NationalID = reader.GetString(0);

                        var uName = reader.GetString(1).Trim();
                        var arrName = uName.Split(' ');
                        if (arrName.Length > 0)
                        {
                            if (arrName.Length == 1)
                            {
                                myDebtor.FirstNameNative = arrName[0];
                            }
                            else
                            {
                                myDebtor.SurNameNative = arrName[arrName.Length - 1];
                                myDebtor.FirstNameNative = uName.Substring(
                                    0, uName.Length - (myDebtor.SurNameNative.Length + 1));
                            }
                        }

                        var address = new Address();
                        if (!reader.IsDBNull(2))
                        {
                            address.PostalCode = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            address.StreetNative = reader.GetString(3);
                        }
                        myDebtor.Address.Add(address);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
                return myDebtor;
            }
        }

        /// <summary>
        /// This function takes national id and returns instance of company
        /// </summary>
        /// <param name="NationalID">The compananies national id</param>
        /// <returns>Instance of company</returns>
        public Company GetCompanyFromNationalRegistry(string NationalID)
        {
            const string funcName = "GetCompanyFromNationalRegstry (string NationalID)";

            var myComp = new Company();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "select IDNumber, Name, PostalCode, Address, PhoneNumber, FaxNumber from ros_NationalCompanyInterfaceView where IDNumber = '" +
                            NationalID + "'",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        //myComp.NationalID = reader.GetString(0);
                        myComp.NationalID = reader.GetValue(0).ToString();
                        myComp.Type = CigConfig.Configure("lookupsettings.companyID");
                        if (!reader.IsDBNull(1))
                        {
                            myComp.NameNative = reader.GetString(1);
                        }
                        var address = new Address();
                        if (!reader.IsDBNull(2))
                        {
                            address.PostalCode = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            address.StreetNative = reader.GetString(3);
                        }
                        myComp.Address.Add(address);

                        if (!reader.IsDBNull(4))
                        {
                            var number = new PhoneNumber { Number = reader.GetString(4), NumberTypeID = 1 };

                            myComp.Number.Add(number);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            var fax = new PhoneNumber { Number = reader.GetString(4), NumberTypeID = 4 };
                            myComp.Number.Add(fax);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
                return myComp;
            }
        }

        public Company GetLithuaniaCompanyFromNationalRegistry(string NationalID)
        {
            const string funcName = "GetCompanyFromNationalRegstry (string NationalID)";

            var myComp = new Company();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "select IDNumber, Name, PostalCode, Address, PhoneNumber, FaxNumber, LERStatus_ID, SLERStatus_ID, CAST(SUBSTRING(CAST(City_ID AS VARCHAR),1,2) AS INT) from ros_NationalCompanyInterfaceView where IDNumber = '" +
                            NationalID + "'",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        //myComp.NationalID = reader.GetString(0);
                        myComp.NationalID = reader.GetValue(0).ToString();
                        myComp.Type = CigConfig.Configure("lookupsettings.companyID");
                        if (!reader.IsDBNull(1))
                        {
                            myComp.NameNative = reader.GetString(1);
                        }
                        var address = new Address();
                        if (!reader.IsDBNull(2))
                        {
                            address.PostalCode = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            address.StreetNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            address.CityID = reader.GetInt32(8);
                        }
                        myComp.Address.Add(address);

                        if (!reader.IsDBNull(4))
                        {
                            var number = new PhoneNumber { Number = reader.GetValue(4).ToString(), NumberTypeID = 1 };
                            //number.Number = reader.GetString(4);
                            myComp.Number.Add(number);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            var fax = new PhoneNumber { Number = reader.GetString(5), NumberTypeID = 4 };
                            myComp.Number.Add(fax);
                        }
                        /*	if(!reader.IsDBNull(6))
								myComp.LERStatusID = reader.GetInt32(6);
							if(!reader.IsDBNull(7))
								myComp.SLERStatusID = reader.GetInt32(7);*/
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
                return myComp;
            }
        }

        // from NPayments section
        /// <summary>
        /// This function takes internal CIID and returns instance of individual
        /// </summary>
        /// <param name="creditInfoID">The indivitual internal CIID</param>
        /// <returns>Instance of indivitual</returns>
        public Indivitual GetIndivitual(int creditInfoID) { return GetIndivitual(creditInfoID, false); }

        // from NPayments section
        /// <summary>
        /// This function takes internal CIID and returns instance of individual
        /// </summary>
        /// <param name="creditInfoID">The indivitual internal CIID</param>
        /// <param name="isSearchable">True will only get searchable individual</param>
        /// <returns>Instance of indivitual</returns>
        public Indivitual GetIndivitual(int creditInfoID, bool isSearchable)
        {
            const string funcName = "GetIndivitual(int creditInfoID)";

            var myIndi = new Indivitual();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    //	var myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                 //   string strSql = "SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, case when len(rtrim(ind.SurNameNative)) = 0 then ''else '(' + rtrim(ind.SurNameNative) + ')'end as SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative, ciu.Email,edu.EducationID,prof.ProfessionID, cre.IsSearchable FROM np_Individual ind LEFT OUTER JOIN np_Profession prof ON ind.ProfessionID = prof.ProfessionID LEFT OUTER JOIN np_Education edu ON ind.EducationID = edu.EducationID, np_CreditInfoUser cre, np_CreditInfoUser ciu WHERE cre.CreditInfoID = ind.CreditInfoID AND ciu.CreditInfoID = ind.CreditInfoID AND cre.CreditInfoID = " +

                    string strSql = "SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative, ciu.Email,edu.EducationID,prof.ProfessionID, cre.IsSearchable, ind.IsPEP, ind.IsGDPRConsent, ind.DoNotDelete FROM np_Individual ind LEFT OUTER JOIN np_Profession prof ON ind.ProfessionID = prof.ProfessionID LEFT OUTER JOIN np_Education edu ON ind.EducationID = edu.EducationID, np_CreditInfoUser cre, np_CreditInfoUser ciu WHERE cre.CreditInfoID = ind.CreditInfoID AND ciu.CreditInfoID = ind.CreditInfoID AND cre.CreditInfoID = " +


                                    creditInfoID + " ";
                    if (isSearchable)
                    {
                        strSql += "AND cre.IsSearchable='True' ";
                    }
                    var myOleDbCommand = new OleDbCommand(strSql, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        myIndi.CreditInfoID = reader.GetInt32(0);
                        myIndi.Type = reader.GetString(1);
                        if (!reader.IsDBNull(2))
                        {
                            myIndi.FirstNameNative = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myIndi.FirstNameEN = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            string surName = reader.GetString(4);
                            if (!string.IsNullOrEmpty(surName))
                            {
                                if (!surName.StartsWith("(") && !surName.EndsWith(")"))
                                    surName = string.Format("({0})", surName);
                            }

                            myIndi.SurNameNative = surName;
                        }
                        if (!reader.IsDBNull(5))
                        {
                            myIndi.SurNameEN = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            myIndi.ProfessionNameNative = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            myIndi.ProfessionNameEN = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            myIndi.ProfessionDescriptionEN = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9))
                        {
                            myIndi.ProfessionDescriptionNative = reader.GetString(9);
                        }
                        if (!reader.IsDBNull(10))
                        {
                            myIndi.EducationEN = reader.GetString(10);
                        }
                        if (!reader.IsDBNull(11))
                        {
                            myIndi.EducationNative = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12))
                        {
                            myIndi.EducationDescriptionEN = reader.GetString(12);
                        }
                        if (!reader.IsDBNull(13))
                        {
                            myIndi.EducationDescriptionNative = reader.GetString(13);
                        }
                        if (!reader.IsDBNull(14))
                        {
                            myIndi.Email = reader.GetString(14);
                        }
                        if (!reader.IsDBNull(15))
                        {
                            myIndi.EducationID = reader.GetInt32(15);
                        }
                        if (!reader.IsDBNull(16))
                        {
                            myIndi.ProfessionID = reader.GetInt32(16);
                        }
                        if (!reader.IsDBNull(17))
                        {
                            myIndi.IsSearchable = bool.Parse(reader.GetString(17));
                        }
                        if (!reader.IsDBNull(18))
                        {
                            myIndi.IsPEP = reader.GetBoolean(18);
                        }
                        if (!reader.IsDBNull(19))
                        {
                            myIndi.IsGDPRConsent = reader.GetBoolean(19);
                        }
                        if (!reader.IsDBNull(20))
                        {
                            myIndi.DoNotDelete = reader.GetBoolean(20);
                        }

                        

                        // get the collections
                        myIndi.Address = GetAddressList(myIndi.CreditInfoID);
                        myIndi.Number = GetPhoneNumberList(myIndi.CreditInfoID);
                        myIndi.IDNumbers = GetIDNumberList(myIndi.CreditInfoID);
                        myIndi.Note = GetNote(myIndi.CreditInfoID);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
                return myIndi;
            }
        }

        // from NPayments section
        /// <summary>
        /// This function takes internal CIID and returns instance of company
        /// </summary>
        /// <param name="creditInfoID">The compananies internal CIID</param>
        /// <returns>Instance of company</returns>
        public Company GetCompany(int creditInfoID) { return GetCompany(creditInfoID, false); }

        // from NPayments section
        /// <summary>
        /// This function takes internal CIID and returns instance of company
        /// </summary>
        /// <param name="creditInfoID">The compananies internal CIID</param>
        /// <param name="isSearchable">True will remove all nonSearchable companies, False will discard IsSearchable field.</param>
        /// <returns>Instance of company</returns>
        public Company GetCompany(int creditInfoID, bool isSearchable)
        {
            const string funcName = "GetCompany (int creditInfoID)";

            var myComp = new Company();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var strSql =
                       string.Format(@"SELECT DISTINCT np_Companys.CreditInfoID, np_Companys.NameNative, np_Companys.NameEN, np_Companys.Established, 
                        np_Companys.LastContacted,np_Companys.URL,np_Companys.FuncID, np_Companys.org_status_code,np_Companys.org_name_status_code, np_Companys.org_status_date, 
                        np_Companys.LastUpdate, np_CreditInfoUser.Email, cpi_NaceCodes.DescriptionNative, cpi_NaceCodes.DescriptionEN, cpi_NaceCodes.ShortDescriptionNative, cpi_NaceCodes.ShortDescriptionEN, np_Companys.Registered, np_CreditInfoUser.IsSearchable 
                        , cpi_RegistrationForm.NameNative as LegalFormNative, cpi_RegistrationForm.NameEn as LegalFormNameEn, np_Companys.DoNotDelete
                        FROM np_Companys LEFT OUTER JOIN np_CreditInfoUser ON np_Companys.CreditInfoID = np_CreditInfoUser.CreditInfoID 
                        LEFT OUTER JOIN cpi_NaceCodes ON np_Companys.FuncID = cpi_NaceCodes.NaceCodeID COLLATE SQL_Latin1_General_CP1_CI_AI 
                        LEFT OUTER JOIN cpi_CompanyReport on np_Companys.CreditInfoID = cpi_CompanyReport.CompanyCIID
                        LEFT OUTER JOIN cpi_RegistrationForm on cpi_CompanyReport.RegistrationFormID = cpi_RegistrationForm.FormID
                        WHERE np_Companys.CreditInfoID = {0}", creditInfoID);
                    if (isSearchable)
                    {
                        strSql += "AND np_CreditInfoUser.IsSearchable='True' ";
                    }
                    var myOleDbCommand = new OleDbCommand(strSql, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        myComp.CreditInfoID = reader.GetInt32(0);
                        myComp.Type = CigConfig.Configure("lookupsettings.companyID");
                        if (!reader.IsDBNull(1))
                        {
                            myComp.NameNative = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            myComp.NameEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myComp.Established = reader.GetDateTime(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myComp.LastContacted = reader.GetDateTime(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            myComp.URL = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            myComp.FuncID = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            myComp.Org_status_code = reader.GetInt32(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            myComp.Org_name_status_code = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9))
                        {
                            myComp.Org_status_date = reader.GetDateTime(9);
                        }
                        if (!reader.IsDBNull(10))
                        {
                            myComp.LastUpdate = reader.GetDateTime(10);
                        }
                        if (!reader.IsDBNull(11))
                        {
                            myComp.Email = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12))
                        {
                            myComp.FuncDescriptionNative = reader.GetString(12).Trim();
                        }
                        if (!reader.IsDBNull(13))
                        {
                            myComp.FuncDescriptionEN = reader.GetString(13).Trim();
                        }
                        if (!reader.IsDBNull(14))
                        {
                            myComp.FuncDescriptionShortNative = reader.GetString(14).Trim();
                        }
                        if (!reader.IsDBNull(15))
                        {
                            myComp.FuncDescriptionShortEN = reader.GetString(15).Trim();
                        }
                        if (!reader.IsDBNull(16))
                        {
                            myComp.Registered = reader.GetDateTime(16);
                        }
                        if (!reader.IsDBNull(17))
                        {
                            myComp.IsSearchable = bool.Parse(reader.GetString(17));
                        }

                        if (!reader.IsDBNull(18))
                        {
                            myComp.LegalFormNative = reader.GetString(18).ToString();
                        }

                        if (!reader.IsDBNull(19))
                        {
                            myComp.LegalFormEn = reader.GetString(19).ToString();
                        }

                        if (!reader.IsDBNull(20))
                        {
                            myComp.DoNotDelete = reader.GetBoolean(20);
                        }

                        // get the collections
                        myComp.Address = GetAddressList(myComp.CreditInfoID);
                        myComp.Number = GetPhoneNumberList(myComp.CreditInfoID);
                        myComp.IDNumbers = GetIDNumberList(myComp.CreditInfoID);
                        myComp.Note = GetNote(myComp.CreditInfoID);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
                return myComp;
            }
        }

        // from NPayments section
        /// <summary>
        /// This function takes creditinfoID and return Addresses as ArrayList
        /// </summary>
        /// <param name="CreditInfo">The requested users internal system ID</param>
        /// <returns>ArrayList of Adress instances</returns>
        public ArrayList GetAddressList(int CreditInfo)
        {
            const string funcName = "GetAddressList(int CreditInfo)";

            var myArr = new ArrayList();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT addr.ID,addr.StreetNative,addr.StreetEN, addr.StreetNumber,addr.CityID, city.NameNative, city.NameEN,city.LocationNative,city.LocationEN, addr.PostalCode, addr.PostBox,addr.OtherInfoNative,addr.OtherInfoEN, addr.IsTradingAddress, addr.CountryId FROM np_Address addr, np_City city WHERE CreditInfoID = " +
                            CreditInfo + " AND addr.CityID = city.CityID",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var myAddress = new Address { AddressID = reader.GetInt32(0) };
                        if (!reader.IsDBNull(1))
                        {
                            myAddress.StreetNative = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            myAddress.StreetEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myAddress.StreetNumber = reader.GetInt32(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myAddress.CityID = reader.GetInt32(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            myAddress.CityNameNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            myAddress.CityNameEN = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            myAddress.CityLocationNative = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            myAddress.CityLocationEN = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9))
                        {
                            myAddress.PostalCode = reader.GetString(9);
                        }
                        if (!reader.IsDBNull(10))
                        {
                            myAddress.PostBox = reader.GetString(10);
                        }
                        if (!reader.IsDBNull(11))
                        {
                            myAddress.OtherInfoNative = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12))
                        {
                            myAddress.OtherInfoEN = reader.GetString(12);
                        }
                        if (!reader.IsDBNull(13))
                        {
                            myAddress.IsTradingAddress = Convert.ToBoolean(reader.GetString(13));
                        }
                        if (!reader.IsDBNull(14))
                        {
                            myAddress.CountryId = reader.GetInt32(14);
                        }

                        myArr.Add(myAddress);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }

                return myArr;
            }
        }

        // from NPayments section
        /// <summary>
        /// This function search for all phone number per given creditinfoID
        /// </summary>
        /// <param name="CreditInfo">The requsted users internal creditinfoID</param>
        /// <returns>ArrayList of PhoneNumber instances</returns>
        public ArrayList GetPhoneNumberList(int CreditInfo)
        {
            const string funcName = "GetPhoneNumberList(int CreditInfo)";

            var myArr = new ArrayList();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT pnum.ID, pnum.Number, ptype.TypeEN, ptype.TypeNative,pnum.NumberTypeID FROM np_PNumbers pnum, np_PNumberTypes ptype WHERE CreditInfoID = " +
                            CreditInfo + " AND pnum.NumberTypeID = ptype.NumberTypeID",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var myNumber = new PhoneNumber { NumberID = reader.GetInt32(0), Number = reader.GetString(1) };
                        if (!reader.IsDBNull(2))
                        {
                            myNumber.NumberTypeEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myNumber.NumberTypeNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myNumber.NumberTypeID = reader.GetInt32(4);
                        }

                        myArr.Add(myNumber);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }

                return myArr;
            }
        }

        // from NPayments section
        /// <summary>GetIDNumberList gets a IDNumber list belonging to customer. </summary>
        /// <param name="CreditInfo">The customers number.</param>
        /// <returns>Returns ArrayList of numbers.</returns>
        public string GetNote(int CreditInfo)
        {
            const string funcName = "GetNote(int CreditInfo)";

            string myNote = "";
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();

                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT np_Notes.CreditInfoID, np_Notes.Note FROM np_Notes WHERE np_Notes.CreditInfoID = " +
                            CreditInfo,
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            myNote = reader.GetString(1);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }

                return myNote;
            }
        }

        // from NPayments section
        /// <summary>GetIDNumberList gets a IDNumber list belonging to customer. </summary>
        /// <param name="CreditInfo">The customers number.</param>
        /// <returns>Returns ArrayList of numbers.</returns>
        public ArrayList GetIDNumberList(int CreditInfo)
        {
            const string funcName = "GetIDNumberList(int CreditInfo)";

            var myArr = new ArrayList();
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();

                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT idnum.ID ,idnum.Number, idtypes.TypeEN, idtypes.TypeNative,idnum.NumberTypeID FROM np_IDNumbers idnum, np_NumberTypes idtypes WHERE CreditInfoID = " +
                            CreditInfo + " AND idnum.NumberTypeID = idtypes.NumberTypeID",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        var myNumber = new IDNumber { IDNumberID = reader.GetInt32(0), Number = reader.GetString(1) };
                        if (!reader.IsDBNull(2))
                        {
                            myNumber.NumberTypeEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            myNumber.NumberTypeNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            myNumber.NumberTypeID = reader.GetInt32(4);
                        }

                        myArr.Add(myNumber);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }

                return myArr;
            }
        }

        public DataSet GetOrgStatus()
        {
            const string funcName = "GetOrgStatus()";
            var theSet = new DataSet();

            using (var theOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    var theAdapter = new OleDbDataAdapter();
                    theOleDbConn.Open();
                    theAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_org_status", theOleDbConn);
                    theAdapter.Fill(theSet);
                    return theSet;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    return null;
                }
            }
        }

        // from NPayments section
        /// <summary>AddNewCustomer adds a new istance of Debtor to the database. </summary>
        /// <param name="theIndi">Instance of indivudual.</param>
        /// <returns>Returns true if ok otherwise false.</returns>
        public int AddNewIndivitual(Indivitual theIndi)
        {
            const string funcName = "AddNewIndivitual(Indivitual theIndi)";
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                int creditInfoID;
                try
                {
                    // 	creditInfoID = GetLatestCreditInfoID(); no longer is use
                    myOleDbCommand =
                        new OleDbCommand("INSERT INTO np_CreditInfoUser(Type,Email,IsSearchable) VALUES(?,?,?)", myOleDbConn)
                        {
                            Transaction = myTrans
                        };
                    var myParam = new OleDbParameter("Type", OleDbType.Char, 10)
                                  {
                                      Value = theIndi.Type,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Email", OleDbType.WChar)
                              {
                                  Value = theIndi.Email,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("IsSearchable", OleDbType.Char, 5)
                              {
                                  Value = theIndi.IsSearchable,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    // n� � s��ast innsetta ID
                    myOleDbCommand.Parameters.Clear();
                    myOleDbCommand.CommandText = "SELECT @@IDENTITY";
                    creditInfoID = Convert.ToInt32(myOleDbCommand.ExecuteScalar());

                    myOleDbCommand.Parameters.Clear();
                    myOleDbCommand.CommandText =
                        "INSERT INTO np_Individual(CreditInfoID,FirstNameNative,FirstNameEN,SurNameNative,SurNameEN,ProfessionID,EducationID,IsPEP, IsGDPRConsent, DoNotDelete) VALUES(?,?,?,?,?,?,?,?,?,?)";

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = creditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert FirstName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "FirstNameNative",
                        "FirstNameEN",
                        ParameterDirection.Input,
                        theIndi.FirstNameNative,
                        theIndi.FirstNameEN,
                        OleDbType.WChar);

                    // Insert FirstName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "SurNameNative",
                        "SurNameEN",
                        ParameterDirection.Input,
                        theIndi.SurNameNative,
                        theIndi.SurNameEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("ProfessionID", OleDbType.Integer) { Direction = ParameterDirection.Input };
                    if (theIndi.ProfessionID == 0)
                    {
                        myParam.Value = null;
                    }
                    else
                    {
                        myParam.Value = theIndi.ProfessionID;
                    }
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("EducationID", OleDbType.Integer) { Direction = ParameterDirection.Input };
                    if (theIndi.EducationID == 0)
                    {
                        myParam.Value = null;
                    }
                    else
                    {
                        myParam.Value = theIndi.EducationID;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                     myParam = new OleDbParameter("IsPEP", OleDbType.Boolean)
                        {
                            Value = theIndi.IsPEP,
                            Direction = ParameterDirection.Input
                        };
                    myOleDbCommand.Parameters.Add(myParam);
                    
                    myParam = new OleDbParameter("IsGDPRConsent", OleDbType.Boolean)
                    {
                        Value = theIndi.IsGDPRConsent,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DoNotDelete", OleDbType.Boolean)
                    {
                        Value = theIndi.DoNotDelete,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);


                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    if (theIndi.Number.Count > 0)
                    {
                        InsertPhoneNumbers(theIndi.Number, myOleDbCommand, creditInfoID);
                    }
                    if (theIndi.Address.Count > 0)
                    {
                        InsertAddress(theIndi.Address, myOleDbCommand, creditInfoID);
                    }
                    if (theIndi.IDNumbers.Count > 0)
                    {
                        InsertIDNumbers(theIndi.IDNumbers, myOleDbCommand, creditInfoID);
                    }

                    InsertNote(theIndi.Note, myOleDbCommand, creditInfoID);

                    myTrans.Commit();
                }
                catch (OleDbException oleEx)
                {
                    Logger.WriteToLog(className + " : " + funcName, oleEx, true);
                    myTrans.Rollback();
                    if (oleEx.Message.StartsWith("The statement has been terminated.\r\nViolation of UNIQUE KEY constraint 'IX_np_IDNumbers'."))
                    {
                        return -2;
                    }
                    return -1;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    myTrans.Rollback();
                    return -1;
                }
                return creditInfoID;
            }
        }

        public bool DeleteIndivitual(int creditInfoID)
        {
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();

                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    myOleDbCommand =
                        new OleDbCommand(" ", myOleDbConn)
                        {
                            Transaction = myTrans
                        };

                    // delete all individual related records using CreditInfoID
                    var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                    {
                        Direction = ParameterDirection.Input,
                        Value = creditInfoID
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    //----- individual record deletion ----//
                    DeleteAndArchive("np_Individual_FullName", myOleDbCommand);
                    DeleteAndArchive("NP_INDIVIDUAL", myOleDbCommand);
                    DeleteAndArchive("np_PNumbers", myOleDbCommand);
                    DeleteAndArchive("NP_ADDRESS", myOleDbCommand);
                    DeleteAndArchive("NP_IDNUMBERS", myOleDbCommand);
                    DeleteAndArchive("NP_NOTES", myOleDbCommand);



                    
                    myTrans.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    myTrans.Rollback();
                    Logger.WriteToLog(className + " : " + "DeleteIndivitual", ex, true);
                    return false;
                }
            }

        }


        public bool DeleteCompany(int creditInfoID)
        {
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();

                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    myOleDbCommand =
                        new OleDbCommand(" ", myOleDbConn)
                        {
                            Transaction = myTrans
                        };

                    // delete all individual related records using CreditInfoID
                    var myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer)
                    {
                        Direction = ParameterDirection.Input,
                        Value = creditInfoID
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    //----- individual record deletion ----//
                    DeleteAndArchive("cpi_BoardMembers", myOleDbCommand);
                    DeleteAndArchive("cpi_CompaniesCustomerType", myOleDbCommand);
                    DeleteAndArchive("cpi_CompaniesNaceCodes", myOleDbCommand);
                    DeleteAndArchive("cpi_CompanyBanks", myOleDbCommand);
                    DeleteAndArchive("cpi_CompanyNegativePayment", myOleDbCommand);
                    DeleteAndArchive("cpi_CompanyPrincipals", myOleDbCommand);
                    DeleteAndArchive("cpi_ExportCountries", myOleDbCommand);
                    DeleteAndArchive("cpi_HistoryOperationReview", myOleDbCommand);
                    DeleteAndArchive("cpi_ImportCountries", myOleDbCommand);
                    DeleteAndArchive("cpi_MarketingPosition", myOleDbCommand);
                    DeleteAndArchive("cpi_Owners", myOleDbCommand);
                    DeleteAndArchive("cpi_RealEstates", myOleDbCommand);
                    DeleteAndArchive("cpi_StaffCount", myOleDbCommand);
                    DeleteAndArchive("cpi_CompanyReport", myOleDbCommand);


                    // ----- deletion of NP_CREDITINFOUSER foreign key dependency ----//
                    myOleDbCommand.Parameters.Clear();
                     myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                    {
                        Direction = ParameterDirection.Input,
                        Value = creditInfoID
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    DeleteAndArchive("np_Companys", myOleDbCommand);


                    myTrans.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    myTrans.Rollback();
                    Logger.WriteToLog(className + " : " + "DeleteIndivitual", ex, true);
                    return false;
                }
            }

        }


        private bool DeleteAndArchive(string tableName, OleDbCommand myOleDbCommand)
        {
            
            try
            {
            if(myOleDbCommand.Parameters.Count == 1) // paramater for the delete must be defined
            {



                    // create archive table if it does not exist
                    myOleDbCommand.CommandText = $@"IF NOT EXISTS(SELECT TABLE_NAME
                                                FROM information_schema.TABLES
                                            WHERE TABLE_SCHEMA = 'archive' and TABLE_NAME = '{tableName}')

                                            BEGIN
                                                EXEC sp_executesql N'SELECT TOP 0 * INTO archive.{tableName} FROM {tableName}';
                                            END";
                    myOleDbCommand.ExecuteNonQuery();


                    // archive the record to be deleted 
                    myOleDbCommand.CommandText = $@"declare @colsUnpivot nvarchar(MAX) 
                                                 declare @query nvarchar(MAX)
                                                 declare @incrementID nvarchar(30)
                                                 set  @incrementID = (select top 1 name from sys.identity_columns where OBJECT_NAME(object_id) = '{tableName}')

                                                 select @colsUnpivot = stuff((select DISTINCT ','+quotename(C.name) 
                                                    FROM sys.columns c JOIN sys.types y ON y.system_type_id = c.system_type_id
                                                    WHERE c.object_id = OBJECT_ID('{tableName}') and C.name != ISNULL(@incrementID, 'id')  and y.name NOT IN ('timestamp') 
                                                    for xml path('')), 1, 1, '') 

                                                 set @query  = 'INSERT INTO archive.{tableName}  ('+ @colsUnpivot +')  SELECT '+ @colsUnpivot +' 
                                                    FROM {tableName} WHERE {myOleDbCommand.Parameters[0].ParameterName} = {myOleDbCommand.Parameters[0].Value}' 

                                                 exec sp_executesql @query; ";
                    
                myOleDbCommand.ExecuteNonQuery();
                    

                    //deletion of the data
                    myOleDbCommand.CommandText = $"DELETE FROM {tableName} WHERE {myOleDbCommand.Parameters[0].ParameterName} = ? ";

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    
            }
            else
            {
                throw new Exception("No Delete parameter defined or more than one parameter defined" );
            }
                    
                return true;
            }
            catch (Exception ex)
            {
            throw new Exception($"Error ocurred During Delete and Archive. on table:{tableName}  please check log:  "+ ex.Message);
            }

        }


        // from UserAdmin section (identical to NPayments part
        /// <summary>
        /// Insert phonenumbers
        /// </summary>
        /// <param name="theNumber">ArrayList of phonenumber instances</param>
        /// <param name="myOleDbCommand">The command to use for execute (using transaction)</param>
        /// <param name="creditInfoID">The given user's internal system ID</param>
        public void InsertPhoneNumbers(ArrayList theNumber, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "INSERT INTO np_PNumbers(NumberTypeID,Number,CreditInfoID) VALUES(?,?,?)";

            for (int i = 0; i < theNumber.Count; i++)
            {
                myOleDbCommand.Parameters.Clear();
                var myNum = (PhoneNumber)theNumber[i];
                var myParam = new OleDbParameter("PNumberTypeID", OleDbType.Integer)
                                         {
                                             Direction = ParameterDirection.Input,
                                             Value = myNum.NumberTypeID
                                         };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Number", OleDbType.WChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myNum.Number
                          };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                          {
                              Direction = ParameterDirection.Input,
                              Value = creditInfoID
                          };
                myOleDbCommand.Parameters.Add(myParam);
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            }
        }

        // from UserAdmin section (identical to NPayments part
        /// <summary>
        /// Insert phonenumbers
        /// </summary>
        /// <param name="note">xxx</param>
        /// <param name="myOleDbCommand">The command to use for execute (using transaction)</param>
        /// <param name="creditInfoID">The given user's internal system ID</param>
        public void InsertNote(string note, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "INSERT INTO np_Notes(CreditInfoID,Note) VALUES(?,?)";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("Note", OleDbType.VarWChar)
                      {
                          Direction = ParameterDirection.Input,
                          Value = note
                      };
            myOleDbCommand.Parameters.Add(myParam);

            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        // the UserAdmin version (indentical to NPayments section
        /// <summary>
        /// Inserts addresses
        /// </summary>
        /// <param name="theAddress"></param>
        /// <param name="myOleDbCommand"></param>
        /// <param name="creditInfoID"></param>
        public void InsertAddress(ArrayList theAddress, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            //const string funcName = "InsertAddress(ArrayList theAddress,OleDbCommand myOleDbCommand, int creditInfoID)";
            myOleDbCommand.CommandText =
                "INSERT INTO np_Address(StreetNative,StreetEN,CityID,PostalCode,PostBox,OtherInfoNative,OtherInfoEN,CreditInfoID,IsTradingAddress, CountryID) VALUES(?,?,?,?,?,?,?,?,?,?)";

            for (int i = 0; i < theAddress.Count; i++)
            {
                myOleDbCommand.Parameters.Clear();
                var myAddr = (Address)theAddress[i];

                // Insert Street Native and EN strings
                CreateTransliteratedParameterFromString(
                    myOleDbCommand,
                    "StreetNative",
                    "StreetEN",
                    ParameterDirection.Input,
                    myAddr.StreetNative,
                    myAddr.StreetEN,
                    OleDbType.WChar);

                var myParam = new OleDbParameter("CityID", OleDbType.Integer)
                                         {
                                             Direction = ParameterDirection.Input,
                                             Value = myAddr.CityID
                                         };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("PostalCode", OleDbType.VarChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myAddr.PostalCode
                          };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("PostBox", OleDbType.VarChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myAddr.PostBox
                          };
                myOleDbCommand.Parameters.Add(myParam);

                // Insert OtherInfo Native and EN strings
                CreateParameterFromString(
                    myOleDbCommand,
                    "OtherInfoNative",
                    "OtherInfoEN",
                    ParameterDirection.Input,
                    myAddr.OtherInfoNative,
                    myAddr.OtherInfoEN,
                    OleDbType.WChar);

                myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                          {
                              Direction = ParameterDirection.Input,
                              Value = creditInfoID
                          };
                myOleDbCommand.Parameters.Add(myParam);

                myParam = new OleDbParameter("IsTradingAddress", OleDbType.VarChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myAddr.IsTradingAddress.ToString()
                          };
                myOleDbCommand.Parameters.Add(myParam);

                myParam = new OleDbParameter("CountryID", OleDbType.Integer)
                {
                    Direction = ParameterDirection.Input,
                    Value = myAddr.CountryId
                };
                myOleDbCommand.Parameters.Add(myParam);

                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            }
        }

        // the UserAdmin version (indentival to NPayments version)
        public void InsertIDNumbers(ArrayList theIDNumbers, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            //const string funcName = "InsertIDNumbers(ArrayList theIDNumbers,OleDbCommand myOleDbCommand, int creditInfoID)";
            myOleDbCommand.CommandText = "INSERT INTO np_IDNumbers(NumberTypeID,Number,CreditInfoID) VALUES(?,?,?)";
            for (int i = 0; i < theIDNumbers.Count; i++)
            {
                myOleDbCommand.Parameters.Clear();
                var myNum = (IDNumber)theIDNumbers[i];
                var myParam = new OleDbParameter("PNumberTypeID", OleDbType.Integer)
                                         {
                                             Direction = ParameterDirection.Input,
                                             Value = myNum.NumberTypeID
                                         };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Number", OleDbType.WChar)
                          {
                              Direction = ParameterDirection.Input,
                              Value = myNum.Number
                          };
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                          {
                              Direction = ParameterDirection.Input,
                              Value = creditInfoID
                          };
                myOleDbCommand.Parameters.Add(myParam);
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            }
        }

        // The UserAdmin version (identical to NPayments version)
        public void UdatePhoneNumbers(ArrayList theNumber, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            DeletePhoneNumbers(myOleDbCommand, creditInfoID);
            InsertPhoneNumbers(theNumber, myOleDbCommand, creditInfoID);
        }

        // The UserAdmin version
        public void DeletePhoneNumbers(OleDbCommand myOleDbCommand, int creditInfoID)
        {
            myOleDbCommand.CommandText = "DELETE FROM np_PNumbers WHERE CreditInfoID = ?";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        // The UserAdmin version (identical to NPayments)
        public void DeleteNote(OleDbCommand myOleDbCommand, int creditInfoID)
        {
            //const string funcName = "DeleteAddress(OleDbCommand myOleDbCommand, int creditInfoID)";
            myOleDbCommand.CommandText = "DELETE FROM np_Notes WHERE CreditInfoID = ?";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                     {
                                         Direction = ParameterDirection.Input,
                                         Value = creditInfoID
                                     };
            myOleDbCommand.Parameters.Add(myParam);
            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        // The UserAdmin version (identical to NPayments)
        public void DeleteAddress(OleDbCommand myOleDbCommand, int creditInfoID)
        {
            //const string funcName = "DeleteAddress(OleDbCommand myOleDbCommand, int creditInfoID)";
            myOleDbCommand.CommandText = "DELETE FROM np_Address WHERE CreditInfoID = ?";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                      {
                          Direction = ParameterDirection.Input,
                          Value = creditInfoID
                      };
            myOleDbCommand.Parameters.Add(myParam);
            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        // The UserAdmin version (identical to NPayments)
        public void UdateNote(string note, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            DeleteNote(myOleDbCommand, creditInfoID);
            InsertNote(note, myOleDbCommand, creditInfoID);
        }

        // The UserAdmin version (identical to NPayments)
        public void UdateAddress(ArrayList theAddress, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            DeleteAddress(myOleDbCommand, creditInfoID);
            InsertAddress(theAddress, myOleDbCommand, creditInfoID);
        }

        // The UserAdmin version (identical to NPayments)
        public void UdateIDNumbers(ArrayList theIDNumbers, OleDbCommand myOleDbCommand, int creditInfoID)
        {
            DeleteIDNumbers(myOleDbCommand, creditInfoID);
            InsertIDNumbers(theIDNumbers, myOleDbCommand, creditInfoID);
        }

        // The UserAdmin version...
        public void DeleteIDNumbers(OleDbCommand myOleDbCommand, int creditInfoID)
        {
            //const string funcName = "DeleteIDNumbers(OleDbCommand myOleDbCommand, int creditInfoID)";
            myOleDbCommand.CommandText = "DELETE FROM np_IDNumbers WHERE CreditInfoID = ?";

            myOleDbCommand.Parameters.Clear();
            var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                          {
                              Direction = ParameterDirection.Input,
                              Value = creditInfoID
                          };
            myOleDbCommand.Parameters.Add(myParam);
            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        // The NPayments version
        /// <summary>
        /// This function returns number types as DataSet
        /// </summary>
        /// <returns></returns>
        public DataSet GetIDNumberTypesAsDataSet()
        {
            const string funcName = "GetIDNumberTypesAsDataSet()";
            var mySet = new DataSet();

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_NumberTypes", myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return mySet;
        }

        // The NPayments version
        /// <summary>
        /// This function returns Phonenumbertypes as DataSet
        /// </summary>
        /// <returns>Available phonenumber types as DataSet</returns>
        public DataSet GetPNumberTypesAsDataSet()
        {
            const string funcName = "GetPNumberTypesAsDataSet()";
            var mySet = new DataSet();

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_PNumberTypes", myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
            }
            return mySet;
        }

        // The NPayments version
        /// <summary>
        /// Gets a list of available professions
        /// </summary>
        /// <returns>Available professions as DataSet</returns>
        public DataSet GetProfessioListAsDataSet()
        {
            const string funcName = "GetProfessioListAsDataSet()";
            var mySet = new DataSet();

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_Profession", myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return mySet;
        }

        // The NPayments version 
        /// <summary>
        /// This function returns all available education as DataSet
        /// </summary>
        /// <returns>List of available education as DataSet</returns>
        public DataSet GetEducationListAsDataSet()
        {
            const string funcName = "GetEducationListAsDataSet()";
            var mySet = new DataSet();

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_Education", myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return mySet;
        }

        // The UserAdmin version
        /// <summary>
        /// Gets list of cities
        /// </summary>
        /// <returns>List of available cities</returns>
        public DataSet GetCityListAsDataSet()
        {
            var mySet = new DataSet();

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_City", myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        // The NPayments version 
        /// <summary>
        /// Gets list of cities
        /// </summary>
        /// <param name="en">Wether the list should be ordered by en or native column</param>
        /// <returns>List of available cities</returns>
        public DataSet GetCityListAsDataSet(bool en)
        {
            const string funcName = "GetCityListAsDataSet(bool en)";
            var mySet = new DataSet();

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = en ? new OleDbCommand("SELECT * FROM np_City ORDER BY NameEN", myOleDbConn) : new OleDbCommand(
                                                                                                                                "SELECT * FROM np_City ORDER BY NameNative", myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return mySet;
        }

        /// <summary>
        /// Gets list of cities that starts with given cityName
        /// </summary>
        /// <param name="cityName">Name that the city shoult stats with</param>
        /// <param name="en">Wether the list should be ordered by en or native column</param>
        /// <returns>List of available cities</returns>
        public DataSet GetCityListAsDataSet(string cityName, bool en)
        {
            const string funcName = "GetCityListAsDataSet(bool en)";
            var mySet = new DataSet();

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = en ? new OleDbCommand(
                                                       "SELECT TOP 200 * FROM np_City WHERE NameEN LIKE '" + cityName + "%' ORDER BY NameEN",
                                                       myOleDbConn) : new OleDbCommand(
                                                                          "SELECT TOP 200 * FROM np_City WHERE NameNative LIKE N'" + cityName +
                                                                          "%' ORDER BY NameNative",
                                                                          myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return mySet;
        }

        public string GetCityName(int cityID, bool en)
        {
            const string funcName = "GetCityName(int cityID, bool en)";

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    myOleDbConn.Open();
                    string query = "select NameNative, NameEN from np_CITY WHERE CityID = " + cityID;
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        string nameNative = "";
                        string nameEN = "";
                        if (!reader.IsDBNull(0))
                        {
                            nameNative = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            nameEN = reader.GetString(1);
                        }
                        if (en)
                        {
                            if (nameEN == null || nameEN.Trim() == "")
                            {
                                return nameNative;
                            }
                            return nameEN;
                        }
                        return nameNative == null || nameNative.Trim() == "" ? nameEN : nameNative;
                    }
                    return null;
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
        }

        public DataSet FindCustomerInNationalAndCreditInfo(Debtor newDebtor, bool exactAddress)
        {
            const string funcName = "FindCustomerInNationalAndCreditInfo(Debtor newDebtor)";

            var mySet = new DataSet();

            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string maxHits = CigConfig.Configure("lookupsettings.UserAdminMaxHits").Trim();
                string query = "Select TOP " + maxHits +
                               " CreditInfoID, CASE WHEN NationalName IS NOT NULL THEN rtrim(NationalName COLLATE SQL_Latin1_General_CP1_CI_AI) ELSE rtrim(FirstNameNative+' '+LastNameNative) END AS NameNative, rtrim(FirstNameEN+' '+LastNameEN) NameEN, CASE WHEN NationalAddress IS NOT NULL THEN rtrim(NationalAddress COLLATE SQL_Latin1_General_CP1_CI_AI) ELSE rtrim(AddressNative) END AS StreetNative, rtrim(AddressEN) StreetEN, " +
                               "IDNumber Number, rtrim(CityNative) CityNative, rtrim(CityEN) CityEN, NationalPostNumber PostalCode, '0' IsCompany, rtrim(CountryNative) AS CountryNative, rtrim(CountryEN) AS CountryEN, Note from ros_CIIDNationalIndividualView where ";
                if (!string.IsNullOrEmpty(newDebtor.IDNumber1))
                {
                    //	query += "AND ";
                    query += "IDNumber = ('" + newDebtor.IDNumber1 + "%') ";
                    whereStatement = true;
                }
                if (newDebtor.CreditInfo > 0)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "CreditInfoID = " + newDebtor.CreditInfo + " ";
                    whereStatement = true;
                }

                if (newDebtor.FirstName != "" && newDebtor.SurName != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "((FirstNameNative LIKE(N'" + newDebtor.FirstName + "%') AND LastNameNative LIKE(N'%" +
                             newDebtor.SurName + "%')) OR (FirstNameEN LIKE(N'" + newDebtor.FirstName +
                             "%') AND LastNameEN LIKE(N'%" + newDebtor.SurName + "%')) OR NationalName Like(N'" +
                             newDebtor.FirstName + "%" + newDebtor.SurName + "%'))";
                    whereStatement = true;
                }
                else
                {
                    if (newDebtor.FirstName != "")
                    {
                        string likeString = newDebtor.FirstName.Replace("'", "[''�`]");
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        query += "(FirstNameNative LIKE(N'" + likeString + "%') OR FirstNameEN LIKE(N'" + likeString +
                                 "%') OR NationalName Like(N'" + likeString + "%')) ";
                        whereStatement = true;
                    }
                    if (newDebtor.SurName != "")
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        query += "(LastNameNative LIKE(N'" + newDebtor.SurName + "%') OR LastNameEN LIKE(N'" +
                                 newDebtor.SurName + "%') OR NationalName LIKE(N'%" + newDebtor.SurName + "%')) ";
                        whereStatement = true;
                    }
                }

                if (newDebtor.Address != null && newDebtor.Address.Count > 0)
                {
                    var address = (Address)newDebtor.Address[0];
                    if (!string.IsNullOrEmpty(address.StreetNative))
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        if (exactAddress)
                        {
                            query += "(AddressNative =(N'" + address.StreetNative + "') OR AddressEN LIKE(N'" +
                                     address.StreetNative + "') OR NationalAddress LIKE(N'" + address.StreetNative +
                                     "')) ";
                        }
                        else
                        {
                            query += "(AddressNative LIKE(N'" + address.StreetNative + "%') OR AddressEN LIKE(N'" +
                                     address.StreetNative + "%') OR NationalAddress LIKE(N'" + address.StreetNative +
                                     "%')) ";
                        }
                        whereStatement = true;
                    }

                    if (!string.IsNullOrEmpty(address.CityNameNative))
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }

                        if (exactAddress)
                        {
                            query += "(CityNative =(N'" + address.CityNameNative + "') OR CityEN =(N'" +
                                                             address.CityNameNative + "')) ";
                        }
                        else
                        {
                            query += "(CityNative LIKE(N'" + address.CityNameNative + "%') OR CityEN LIKE(N'" +
                                     address.CityNameNative + "%')) ";
                        }

                        whereStatement = true;
                    }
                }

                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    query += "IDNumber LIKE('%') ";
                }

                //Add order by
                query += " ORDER BY NationalName";

                try
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName + " " + query, e, true);
                }
            }
            return ClearDuplicatedCIID(mySet);
        }

        // The userAdmin version (identical to NPayments version)
        /// <summary>
        /// Search for company by given search criteria
        /// </summary>
        /// <param name="myComp">Instance of company</param>
        /// <param name="exactAddress"></param>
        /// <param name="tradingAddress"></param>
        /// <returns>List of companies matching the search criteria</returns>
        public DataSet FindMaltaCompany(Company myComp, bool c, bool tradingAddress)
        {
            return this.FindMaltaCompany(myComp,  c, tradingAddress,string.Empty);
        }
        /// <summary>
        /// Extended FindMaltaCompany.
        /// </summary>
        /// <param name="myComp"></param>
        /// <param name="exactAddress"></param>
        /// <param name="tradingAddress"></param>
        /// <param name="vatNumber"></param>
        /// <returns></returns>
        public DataSet FindMaltaCompany(Company myComp, bool exactAddress, bool tradingAddress, string vatNumber)
        {
            return this.FindMaltaCompany(myComp, exactAddress, tradingAddress, string.Empty,false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="myComp"></param>
        /// <param name="exactAddress"></param>
        /// <param name="tradingAddress"></param>
        /// <param name="vatNumber"></param>
        /// <param name="showVat"></param>
        /// <returns></returns>
        public DataSet FindMaltaCompany(Company myComp, bool exactAddress, bool tradingAddress, string vatNumber,bool showVat)
        {
            //const string funcName = "FindCompany(Company myComp)";
            var mySet = new DataSet();
            bool whereStatement = false;


            string maxHits = CigConfig.Configure("lookupsettings.UserAdminMaxHits").Trim();

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                //string query = "SELECT DISTINCT np_Companys.CreditInfoID, np_Companys.NameNative, np_Companys.NameEN, np_Companys.Established, np_IDNumbers.Number FROM np_Companys LEFT OUTER JOIN np_IDNumbers ON np_Companys.CreditInfoID = np_IDNumbers.CreditInfoID WHERE ";

                var qBuilder = new StringBuilder();
                qBuilder.Append(
                    "SELECT TOP " + maxHits +
                    " np_Companys.CreditInfoID, rtrim(np_Companys.NameNative) NameNative, rtrim(np_Companys.NameEN) NameEN, ");
                qBuilder.Append(
                    "rtrim(np_Address.StreetNative) StreetNative, rtrim(np_Address.StreetEN) StreetEn, np_IDNumbers.Number, ");
                qBuilder.Append(
                    @"rtrim(np_City.NameNative) CityNative, rtrim(np_City.NameEN) CityEN, np_Address.PostalCode, '1' IsCompany, 
                    np_Companys.org_name_status_code, cpi_Countries.NameNative As CountryNative, cpi_Countries.NameEN As CountryEN, 
                    np_IDNumbers.NumberTypeID, np_Notes.Note, lmt_companystates.state as CompanyState, lmt_companyclassification.classification as LegalForm ");
                qBuilder.Append("FROM np_Companys ");
                if (myComp.IsSearchable)
                {
                    qBuilder.Append(
                        "INNER JOIN np_CreditInfoUser ON np_Companys.CreditInfoID = np_CreditInfoUser.CreditInfoID ");
                    qBuilder.Append(
                        "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID ");
                    qBuilder.Append("LEFT OUTER JOIN np_Address ON np_Companys.CreditInfoID = np_Address.CreditInfoID ");
                    qBuilder.Append("LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID ");
                    qBuilder.Append("LEFT OUTER JOIN cpi_Countries ON np_Address.CountryID = cpi_Countries.CountryID ");
                    qBuilder.Append("LEFT OUTER JOIN np_Notes ON np_Companys.CreditInfoID = np_Notes.CreditInfoID ");
                }
                else
                {
                    qBuilder.Append("LEFT OUTER JOIN np_Address ON np_Companys.CreditInfoID = np_Address.CreditInfoID ");
                    qBuilder.Append(
                        "LEFT OUTER JOIN np_IDNumbers ON np_Companys.CreditInfoID = np_IDNumbers.CreditInfoID ");
                    qBuilder.Append("LEFT Outer JOIN np_City on np_Address.CityID = np_City.CityID ");
                    qBuilder.Append("LEFT Outer JOIN cpi_Countries on np_Address.CountryID = cpi_Countries.CountryID ");
                    qBuilder.Append("LEFT OUTER JOIN np_Notes ON np_Companys.CreditInfoID = np_Notes.CreditInfoID ");
                }
                qBuilder.Append(@" LEFT OUTER JOIN lmt_companies ON np_IDNumbers.Number = lmt_companies.COMPANYID
                        LEFT OUTER JOIN lmt_companystates ON lmt_companies.companystatusid = lmt_companystates.statusid ");

                qBuilder.Append(@" LEFT OUTER JOIN lmt_companyclassification ON lmt_companies.companyclassificationid = lmt_companyclassification.classificationid ");

                qBuilder.Append("WHERE ");
                if (!string.IsNullOrEmpty(myComp.NationalID))
                {
                    qBuilder.Append("np_IDNumbers.Number =(N'" + myComp.NationalID + "') ");
                    whereStatement = true;
                }
                if (myComp.CreditInfoID > 0)
                {
                    if (whereStatement)
                    {
                        qBuilder.Append("AND ");
                    }
                    qBuilder.Append("np_Companys.CreditInfoID = " + myComp.CreditInfoID + " ");
                    whereStatement = true;
                }

                if (myComp.Address.Count > 0)
                {
                    var address = (Address)myComp.Address[0];
                    if (!string.IsNullOrEmpty(address.StreetNative))
                    {
                        if (whereStatement)
                        {
                            qBuilder.Append("AND ");
                        }

                        if (exactAddress)
                        {
                            qBuilder.Append(
                                "(StreetNative =(N'" + address.StreetNative + "') OR StreetEN =(N'" +
                                address.StreetNative + "')) ");
                        }
                        else
                        {
                            qBuilder.Append(
                                "(StreetNative LIKE(N'" + address.StreetNative + "%') OR StreetEN LIKE(N'" +
                                address.StreetNative + "%')) ");
                        }
                        whereStatement = true;
                    }

                    if (!string.IsNullOrEmpty(address.CityNameNative))
                    {
                        if (whereStatement)
                        {
                            qBuilder.Append("AND ");
                        }
                        if (exactAddress)
                        {
                            qBuilder.Append(
                                "(np_City.NameNative =(N'" + address.CityNameNative + "') OR np_City.NameEN =(N'" +
                                address.CityNameNative + "')) ");
                        }
                        else
                        {
                            qBuilder.Append(
                                "(np_City.NameNative LIKE(N'" + address.CityNameNative + "%') OR np_City.NameEN LIKE(N'" +
                                address.CityNameNative + "%')) ");
                        }
                        whereStatement = true;
                    }
                }

                if (myComp.IsSearchable)
                {
                    if (whereStatement)
                    {
                        qBuilder.Append("AND ");
                    }

                    qBuilder.Append("np_CreditInfoUser.IsSearchable = 'True' ");
                    whereStatement = true;
                }

                if (myComp.NameNative != "")
                {
                    string likeString = myComp.NameNative.Replace("'", "[''�`]");
                    if (whereStatement)
                    {
                        qBuilder.Append("AND ");
                    }

                    qBuilder.Append(
                        "exists (select * from np_word2ciid where np_Companys.creditinfoid=np_word2ciid.creditinfoid and np_word2ciid.type='n' ");
                    qBuilder.Append("and np_word2ciid.word like (N'" + likeString + "%')) ");
                    whereStatement = true;
                }

                if (whereStatement)
                {
                    qBuilder.Append(" AND ");
                }

                if (tradingAddress)
                {
                    qBuilder.Append("np_Address.IsTradingAddress = 'True' ");
                }
                else
                {
                    qBuilder.Append("np_Address.IsTradingAddress = 'False' ");
                }

                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    qBuilder.Append(" np_IDNumbers.Number LIKE('%')");
                }

                //Check if vat number is searched
                if (!string.IsNullOrEmpty(vatNumber))
                {
                    string showString = "0";
                    if (showVat)
                    {
                        showString = "1";
                    }

                    if (!whereStatement)
                    {
                        qBuilder.Append("WHERE (np_Companys.CreditInfoID in (SELECT creditinfoid FROM np_VatNumbers WHERE Number = N'" + vatNumber + "' AND show = " + showString + ") ) ");
                    }
                    else
                    {
                        qBuilder.Append("AND (np_Companys.CreditInfoID in (SELECT creditinfoid FROM np_VatNumbers WHERE Number = N'" + vatNumber + "' AND show = " + showString + "))");
                    }
                }

                qBuilder.Append(" ORDER BY np_Companys.CreditInfoID, np_IDNumbers.NumberTypeID desc ");

                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(qBuilder.ToString(), myOleDbConn);
                try
                {
                    myAdapter.Fill(mySet);
                }
                catch (Exception exc)
                {
                    Logger.WriteToLog("Error in FindCompany - sql is " + qBuilder, exc, true);
                }
                return ClearDuplicatedCIID(mySet);
            }
        }

        /// <summary>
        /// Find without vat number parameter.
        /// </summary>
        /// <param name="myComp"></param>
        /// <param name="exactAddress"></param>
        /// <returns></returns>
        public DataSet FindCompany(Company myComp, bool exactAddress)
        {
            return this.FindCompany(myComp, exactAddress, string.Empty);
        }

        // The userAdmin version (identical to NPayments version)
        /// <summary>
        /// Search for company by given search criteria
        /// </summary>
        /// <param name="myComp">Instance of company</param>
        /// <returns>List of companies matching the search criteria</returns>
        public DataSet FindCompany(Company myComp, bool exactAddress, string vatNumber)
        {
            //const string funcName = "FindCompany(Company myComp)";
            var mySet = new DataSet();
            bool whereStatement = false;


            string maxHits = CigConfig.Configure("lookupsettings.UserAdminMaxHits").Trim();

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                //string query = "SELECT DISTINCT np_Companys.CreditInfoID, np_Companys.NameNative, np_Companys.NameEN, np_Companys.Established, np_IDNumbers.Number FROM np_Companys LEFT OUTER JOIN np_IDNumbers ON np_Companys.CreditInfoID = np_IDNumbers.CreditInfoID WHERE ";

                var qBuilder = new StringBuilder();
                qBuilder.Append(
                    "SELECT TOP " + maxHits +
                    " np_Companys.CreditInfoID, rtrim(np_Companys.NameNative) NameNative, rtrim(np_Companys.NameEN) NameEN, ");
                qBuilder.Append(
                    "rtrim(np_Address.StreetNative) StreetNative, rtrim(np_Address.StreetEN) StreetEn, np_IDNumbers.Number, ");
                qBuilder.Append(
                    "rtrim(np_City.NameNative) CityNative, rtrim(np_City.NameEN) CityEN, np_Address.PostalCode, '1' IsCompany, np_Companys.org_name_status_code, cpi_Countries.NameNative As CountryNative, cpi_Countries.NameEN As CountryEN, np_IDNumbers.NumberTypeID, np_Notes.Note ");
                qBuilder.Append("FROM np_Companys ");
                if (myComp.IsSearchable)
                {
                    qBuilder.Append(
                        "INNER JOIN np_CreditInfoUser ON np_Companys.CreditInfoID = np_CreditInfoUser.CreditInfoID ");
                    qBuilder.Append(
                        "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID ");
                    qBuilder.Append("LEFT OUTER JOIN np_Notes ON np_Companys.CreditInfoID = np_Notes.CreditInfoID ");
                    qBuilder.Append("LEFT OUTER JOIN np_Address ON np_Companys.CreditInfoID = np_Address.CreditInfoID ");
                    qBuilder.Append("LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID ");
                    qBuilder.Append("LEFT OUTER JOIN cpi_Countries ON np_Address.CountryID = cpi_Countries.CountryID ");
                }
                else
                {
                    qBuilder.Append("LEFT OUTER JOIN np_Address ON np_Companys.CreditInfoID = np_Address.CreditInfoID ");
                    qBuilder.Append("LEFT OUTER JOIN np_Notes ON np_Companys.CreditInfoID = np_Notes.CreditInfoID ");
                    qBuilder.Append(
                        "LEFT OUTER JOIN np_IDNumbers ON np_Companys.CreditInfoID = np_IDNumbers.CreditInfoID ");
                    qBuilder.Append("LEFT Outer JOIN np_City on np_Address.CityID = np_City.CityID ");
                    qBuilder.Append("LEFT Outer JOIN cpi_Countries on np_Address.CountryID = cpi_Countries.CountryID ");
                }
                qBuilder.Append("WHERE ");
                if (!string.IsNullOrEmpty(myComp.NationalID))
                {
                    qBuilder.Append("np_IDNumbers.Number =(N'" + myComp.NationalID + "') ");
                    whereStatement = true;
                }
                if (myComp.CreditInfoID > 0)
                {
                    if (whereStatement)
                    {
                        qBuilder.Append("AND ");
                    }
                    qBuilder.Append("np_Companys.CreditInfoID = " + myComp.CreditInfoID + " ");
                    whereStatement = true;
                }

                if (myComp.Address.Count > 0)
                {
                    var address = (Address)myComp.Address[0];
                    if (!string.IsNullOrEmpty(address.StreetNative))
                    {
                        if (whereStatement)
                        {
                            qBuilder.Append("AND ");
                        }

                        if (exactAddress)
                        {
                            qBuilder.Append(
                                "(StreetNative =(N'" + address.StreetNative + "') OR StreetEN =(N'" +
                                address.StreetNative + "')) ");
                        }
                        else
                        {
                            qBuilder.Append(
                                "(StreetNative LIKE(N'" + address.StreetNative + "%') OR StreetEN LIKE(N'" +
                                address.StreetNative + "%')) ");
                        }
                        whereStatement = true;
                    }

                    if (!string.IsNullOrEmpty(address.CityNameNative))
                    {
                        if (whereStatement)
                        {
                            qBuilder.Append("AND ");
                        }
                        qBuilder.Append(
                            "(np_City.NameNative LIKE(N'" + address.CityNameNative + "%') OR np_City.NameEN LIKE(N'" +
                            address.CityNameNative + "%')) ");
                        whereStatement = true;
                    }
                }

                if (myComp.IsSearchable)
                {
                    if (whereStatement)
                    {
                        qBuilder.Append("AND ");
                    }

                    qBuilder.Append("np_CreditInfoUser.IsSearchable = 'True' ");
                    whereStatement = true;
                }

                if (myComp.NameNative != "")
                {
                    string likeString = myComp.NameNative.Replace("'", "[''�`]");
                    if (whereStatement)
                    {
                        qBuilder.Append("AND ");
                    }

                    qBuilder.Append(
                        "exists (select * from np_word2ciid where np_Companys.creditinfoid=np_word2ciid.creditinfoid and np_word2ciid.type='n' ");
                    qBuilder.Append("and np_word2ciid.word like (N'" + likeString + "%')) ");
                    whereStatement = true;
                }

                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    qBuilder.Append(" np_IDNumbers.Number LIKE('%')");
                }

                //Check if vat number is searched
                if (!string.IsNullOrEmpty(vatNumber))
                {
                    if (!whereStatement)
                    {
                        qBuilder.Append("AND (np_Companys.CreditInfoID in (SELECT creditinfoid FROM np_VatNumbers WHERE Number = N'" + vatNumber + "') ) ");
                    }
                    else
                    {
                        qBuilder.Append("AND (np_Companys.CreditInfoID in (SELECT creditinfoid FROM np_VatNumbers WHERE Number = N'" + vatNumber + "'))");
                    }
                }

                qBuilder.Append(" ORDER BY np_Companys.CreditInfoID, np_IDNumbers.NumberTypeID desc ");

                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(qBuilder.ToString(), myOleDbConn);
                try
                {
                    myAdapter.Fill(mySet);
                }
                catch (Exception exc)
                {
                    Logger.WriteToLog("Error in FindCompany - sql is " + qBuilder, exc, true);
                }
                return ClearDuplicatedCIID(mySet);
            }
        }

        public string GetCompanyStatusByCIID(int CIID)
        {
            const string funcName = "GetCompanyStatusByCIID(int CIID)";

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT org_name_status_code FROM np_Companys WHERE CreditInfoId = " + CIID + "",
                            myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            return reader.GetString(0);
                        }
                        return "-1";
                    }
                    return "-1";
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName + " " + err.Message, true);
                return "-1";
            }
        }

        public DataSet FindCompanyInNationalAndCreditInfo(Company myComp, bool exactAddress)
        {
            //const string funcName = "FindCompanyInNationalAndCreditInfo(CompanyBLLC theCompany)";
            var mySet = new DataSet();


            bool whereStatement = false;

            string maxHits = CigConfig.Configure("lookupsettings.UserAdminMaxHits").Trim();

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "Select TOP " + maxHits +
                               " CreditInfoID, CASE WHEN NationalName IS NOT NULL THEN rtrim(NationalName COLLATE SQL_Latin1_General_CP1_CI_AI) ELSE rtrim(NameNative) END AS NameNative, rtrim(NameEN) NameEN, CASE WHEN NationalAddress IS NOT NULL THEN rtrim(NationalAddress COLLATE SQL_Latin1_General_CP1_CI_AI) ELSE rtrim(AddressNative) END AS StreetNative, rtrim(AddressEN) StreetEN, " +
                               "CASE WHEN NationalIDNumber IS NOT NULL THEN NationalIDNumber COLLATE SQL_Latin1_General_CP1_CI_AI ELSE IDNumber END AS Number, rtrim(CityNative) CityNative, rtrim(CityEN) CityEN, NationalPostalCode PostalCode, '1' IsCompany, rtrim(CountryNative) AS CountryNative, rtrim(CountryEN) AS CountryEN, rtrim(Regdate) AS RegistrationDate,Note from ros_CIIDNationalCompanyView where ";

                int nationalID = int.Parse(CigConfig.Configure("lookupsettings.nationalID").Trim());
                if (myComp.IDNumbers.Count > 0)
                {
                    if (((IDNumber)myComp.IDNumbers[0]).NumberTypeID == nationalID)
                    {
                        query += "(NationalIDNumber =('" + ((IDNumber)myComp.IDNumbers[0]).Number +
                                 "') OR IDNumber = '" + ((IDNumber)myComp.IDNumbers[0]).Number +
                                 "') AND NumberTypeID = " + nationalID + " "; // 1 for ID-card number
                        whereStatement = true;
                    }
                }
                else if (!string.IsNullOrEmpty(myComp.NationalID)) // GENGUR EKKI! (�tg�fa 22 Svenni) 040205 GPG reyna a� pl�gga �essu inn aftur 
                {
                    query += "(NationalIDNumber =('" + myComp.NationalID + "') OR IDNumber = '" + myComp.NationalID +
                             "') AND NumberTypeID = " + nationalID + " "; // 1 for ID-card number
                    whereStatement = true;
                }

                if (myComp.CreditInfoID > 0)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "CreditInfoID = " + myComp.CreditInfoID + " ";
                    whereStatement = true;
                }
                if (myComp.NameNative != "")
                {
                    string likeString = myComp.NameNative.Replace("'", "[''�`]");
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "(NameNative LIKE(N'%" + likeString + "%') OR NameEN LIKE(N'%" + likeString +
                             "%') OR NationalName LIKE(N'%" + likeString + "%')) ";
                    whereStatement = true;
                }

                if (myComp.Address.Count > 0)
                {
                    var address = (Address)myComp.Address[0];
                    if (!string.IsNullOrEmpty(address.StreetNative))
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        if (exactAddress)
                        {
                            query += "(NationalAddress =(N'" + address.StreetNative + "') OR AddressNative =(N'" +
                                     address.StreetNative + "') OR AddressEN =(N'" + address.StreetNative + "')) ";
                        }
                        else
                        {
                            query += "(NationalAddress LIKE(N'" + address.StreetNative + "%') OR AddressNative LIKE(N'" +
                                     address.StreetNative + "%') OR AddressEN LIKE(N'" + address.StreetNative + "%')) ";
                        }

                        whereStatement = true;
                    }

                    if (!string.IsNullOrEmpty(address.CityNameNative))
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        query += "(CityNative LIKE(N'" + address.CityNameNative + "%') OR CityEN LIKE(N'" +
                                 address.CityNameNative + "%')) ";
                        whereStatement = true;
                    }
                }
                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    query += "IDNumber LIKE('%')";
                }

                //Add order by
                query += " ORDER BY NameNative";

                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                try
                {
                    myAdapter.Fill(mySet);
                }
                catch (Exception exc)
                {
                    Logger.WriteToLog("Error in FindCompanyInNationalAndCreditinfo - sql is " + query, exc, true);
                }
                return ClearDuplicatedCIID(mySet);
            }
        }

        // The UserAdmin version (identical to NPayments version
        /// <summary>FindCustomer search for Customer in DB. </summary>
        /// <param name="newDebtor">Instance of Debtor.</param>
        /// <param name="exactAddress"></param>
        /// <returns>Returns true if ok otherwise false.</returns>
        public DataSet FindCustomer(Debtor newDebtor, bool exactAddress)
        {
            return this.FindCustomer(newDebtor, exactAddress, string.Empty);
        }
        // The UserAdmin version (identical to NPayments version
        /// <summary>FindCustomer search for Customer in DB. </summary>
        /// <param name="newDebtor">Instance of Debtor.</param>
        /// <param name="exactAddress"></param>
         /// <returns>Returns true if ok otherwise false.</returns>
        public DataSet FindCustomer(Debtor newDebtor, bool exactAddress, string vatNumber)
        {
            return this.FindCustomer(newDebtor, exactAddress, vatNumber,true);
        }

        // The UserAdmin version (identical to NPayments version
        /// <summary>FindCustomer search for Customer in DB. </summary>
        /// <param name="newDebtor">Instance of Debtor.</param>
        /// <param name="exactAddress"></param>
        /// <param name="showVat">Show flag from Vat numbers.</param>
        /// <returns>Returns true if ok otherwise false.</returns>
        public DataSet FindCustomer(Debtor newDebtor, bool exactAddress, string vatNumber, bool showVat)
        {
            //	const string funcName = "FindCustomer(Debtor newDebtor)";
            var mySet = new DataSet();
            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string maxHits = CigConfig.Configure("lookupsettings.UserAdminMaxHits").Trim();
                string query = "SELECT DISTINCT TOP " + maxHits +
                               " np_Individual.CreditInfoID, " +
                              // " rtrim(np_Individual.FirstNameNative + case when len(rtrim(np_Individual.SurNameNative)) = 0 then ''else ' (' + rtrim(np_Individual.SurNameNative) + ')'end) NameNative, " +
                              // "rtrim(np_Individual.FirstNameNative + case when len(rtrim(np_Individual.SurNameNative)) = 0 then ''else ' (' + rtrim(np_Individual.SurNameNative) + ')'end) NameEn, " + 
                              " rtrim(np_Individual.FirstNameNative + " +
                               " case when len(rtrim(np_Individual.SurNameNative)) = 0 then '' else  " +
                               " case when charindex('(', rtrim(np_Individual.SurNameNative)) = 0 then ' (' else ' ' end + rtrim(np_Individual.SurNameNative) +  " +
                               " case when charindex(')', rtrim(np_Individual.SurNameNative)) = len(np_Individual.SurNameNative) then '' else ')' end end ) NameNative,  " +
                               " rtrim(np_Individual.FirstNameNative + case when len(rtrim(np_Individual.SurNameNative)) = 0 then '' else  " +
                               " case when charindex('(', rtrim(np_Individual.SurNameNative)) = 0 then ' (' else ' ' end +  " +
                               " rtrim(np_Individual.SurNameNative) + case when charindex(')', rtrim(np_Individual.SurNameNative)) = len(np_Individual.SurNameNative) then ''  " +
                               " else ')' end end )  NameEn, ISNULL(IsPEP, 0) as IsPEP,  " +
                              " rtrim(np_Address.StreetNative) StreetNative, rtrim(np_Address.StreetEN) StreetEN,np_IDNumbers.Number, " +
                               "rtrim(np_City.NameNative) CityNative, rtrim(np_City.NameEN) CityEN, np_Address.PostalCode, '0' IsCompany, cpi_Countries.NameNative As CountryNative, cpi_Countries.NameEN As CountryEN, np_IDNumbers.NumberTypeID,np_Notes.Note " +
                               "FROM np_Individual ";

                if (newDebtor.IsSearchable)
                {
                    query += "INNER JOIN np_CreditInfoUser ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID "
                             +
                             "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID "
                             + "LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID "
                             + "LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID "
                             + "LEFT OUTER JOIN np_Notes ON np_Individual.CreditInfoID = np_Notes.CreditInfoID "
                             + "LEFT OUTER JOIN cpi_Countries ON np_Address.CountryID = cpi_Countries.CountryID WHERE ";
                }
                else
                {
                    query +=
                        "LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN " +
                        "np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID " +
                        "LEFT Outer JOIN np_City on np_Address.CityID = np_City.CityID " +
                        "LEFT OUTER JOIN np_Notes ON np_Individual.CreditInfoID = np_Notes.CreditInfoID " +
                        "LEFT Outer JOIN cpi_Countries on np_Address.CountryID = cpi_Countries.CountryID WHERE ";
                }

                Dictionary<string, object> queryParam = new Dictionary<string, object>();
                if (!string.IsNullOrEmpty(newDebtor.IDNumber1))
                {
                    //query += "np_IDNumbers.Number = (N'" + newDebtor.IDNumber1 + "') ";
                    query += "np_IDNumbers.Number = (?) ";
                    queryParam.Add("@Param1", newDebtor.IDNumber1);
                    whereStatement = true;
                }
                if (newDebtor.CreditInfo > 0)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                //    query += "np_Individual.CreditInfoID = " + newDebtor.CreditInfo + " ";
                    query += "np_Individual.CreditInfoID = ? ";
                    queryParam.Add("@Param2", newDebtor.CreditInfo);
                    whereStatement = true;
                }

                if (!string.IsNullOrEmpty(newDebtor.FirstName))
                {
                    string likeString = newDebtor.FirstName.Replace("'", "[''�`]");
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query +=
                        "exists (select * from np_word2ciid where np_Individual.creditinfoid=np_word2ciid.creditinfoid and np_word2ciid.type='i' ";
                   // query += "and np_word2ciid.word like (N'" + likeString + "%')) ";
                    query += "and np_word2ciid.word like (?)) ";

                    queryParam.Add("@Param3", likeString + '%');
                    //query += "(np_Individual.FirstNameNative LIKE(N'"+newDebtor.FirstName+"%') OR np_Individual.FirstNameEN LIKE(N'"+newDebtor.FirstName+"%')) ";
                    whereStatement = true;
                }

                if (newDebtor.SurName != null)
                {
                    if (newDebtor.SurName != "")
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        //query += "(np_Individual.SurNameNative LIKE(N'"+newDebtor.SurName+"%') OR np_Individual.SurNameEN LIKE(N'"+newDebtor.SurName+"%')) ";
                        query +=
                            "exists (select * from np_word2ciid where np_Individual.creditinfoid=np_word2ciid.creditinfoid and np_word2ciid.type='i' ";
                     //   query += "and np_word2ciid.word like (N'" + newDebtor.SurName + "%')) ";
                        query += "and np_word2ciid.word like (?)) ";

                        queryParam.Add("@Param4", newDebtor.SurName + '%');

                        whereStatement = true;
                    }
                }
                if (newDebtor.Address != null && newDebtor.Address.Count > 0)
                {
                    var address = (Address)newDebtor.Address[0];
                    if (!string.IsNullOrEmpty(address.StreetNative))
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        if (exactAddress)
                        {
                       //     query += "(StreetNative = (N'" + address.StreetNative + "') OR StreetEN =(N'" +
                       //              address.StreetNative + "')) ";
                            query += "(StreetNative = (?) OR StreetEN =(?)) ";

                            queryParam.Add("@Param5", address.StreetNative);
                            queryParam.Add("@Param6", address.StreetNative);

                        }
                        else
                        {
                        //    query += "(StreetNative LIKE(N'" + address.StreetNative + "%') OR StreetEN LIKE(N'" +
                        //             address.StreetNative + "%')) ";

                            query += "(StreetNative LIKE(?) OR StreetEN LIKE(?)) ";

                            queryParam.Add("@Param5", address.StreetNative + '%');
                            queryParam.Add("@Param6", address.StreetNative + '%');

                        }
                        whereStatement = true;
                    }
                    if (!string.IsNullOrEmpty(address.CityNameNative))
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        if (exactAddress)
                        {
                           // query += "(np_City.NameNative = (N'" + address.CityNameNative +
                           //          "') OR np_City.NameEN = (N'" + address.CityNameNative + "')) ";

                            query += "(np_City.NameNative = (?) OR np_City.NameEN = (?)) ";
                            queryParam.Add("@Param7", address.CityNameNative);
                            queryParam.Add("@Param8", address.CityNameNative);

                        }
                        else
                        {
                         //   query += "(np_City.NameNative LIKE(N'" + address.CityNameNative +
                         //          "%') OR np_City.NameEN LIKE(N'" + address.CityNameNative + "%')) ";

                            query += "(np_City.NameNative LIKE(?) OR np_City.NameEN LIKE(?)) ";

                            queryParam.Add("@Param7", address.CityNameNative + '%');
                            queryParam.Add("@Param8", address.CityNameNative + '%');
                        }

                        whereStatement = true;
                    }
                }

                if (newDebtor.showPEP)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }

                    query += "np_Individual.IsPEP = 1 ";
                    whereStatement = true;
                }

                if (newDebtor.IsSearchable)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }

                    query += "np_CreditInfoUser.IsSearchable='True' ";
                    whereStatement = true;
                }

                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    query += "np_IDNumbers.Number LIKE('%') ";
                }

                //Check if vat number is searched
                if (!string.IsNullOrEmpty(vatNumber))
                {
                    string showString = "0";
                    if (showVat)
                    {
                        showString = "1";
                    }

                    if (!whereStatement)
                    {
                        //query += "AND (np_Individual.CreditInfoID in (SELECT creditinfoid FROM np_VatNumbers WHERE Number = N'" + vatNumber + "' AND show = " + showString + ") ) ";

                        query += "AND (np_Individual.CreditInfoID in (SELECT creditinfoid FROM np_VatNumbers WHERE Number = ? AND show = ?) ) ";

                        queryParam.Add("@Param9", vatNumber);
                        queryParam.Add("@Param10", showString);
                    }
                    else
                    {
                       // query += "AND (np_Individual.CreditInfoID in (SELECT creditinfoid FROM np_VatNumbers WHERE Number = N'" + vatNumber + "' AND show = " + showString + "))";
                        query += "AND (np_Individual.CreditInfoID in (SELECT creditinfoid FROM np_VatNumbers WHERE Number = ? AND show = ?))";


                        queryParam.Add("@Param9", vatNumber);
                        queryParam.Add("@Param10", showString);
                    }
                }
                else if (showVat)
                {
                    query += "AND (np_Individual.CreditInfoID in (SELECT creditinfoid FROM np_VatNumbers WHERE show = 1 ))";

                    

                }

                query += " ORDER BY np_Individual.CreditInfoID, np_IDNumbers.NumberTypeID,StreetNative, StreetEN desc";

             
                OleDbCommand cmd = new OleDbCommand(query, myOleDbConn);
                foreach (var param in queryParam.Keys)
                    cmd.Parameters.AddWithValue(param, queryParam[param]);
                
                var myAdapter = new OleDbDataAdapter(cmd);
                myOleDbConn.Open();

                try
                {
                    myAdapter.Fill(mySet);
                }
                catch (Exception exc)
                {
                    Logger.WriteToLog("Error in FindCustomer - sql is " + query, exc, true);
                }
                return ClearDuplicatedCIID(mySet);
            }
        }

        protected static DataSet ClearDuplicatedCIIDByTradingAddress(DataSet ds)
        {
            if (ds.Tables.Count > 0)
            {
                var arrCiids = new ArrayList();
                for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--)
                {
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    // what about empty creditinfoids like when searching Cyprus whitelist?
                    if (creditInfoId == "")
                    {
                        continue;
                    }
                    for (int j = arrCiids.Count - 1; j >= 0; j--)
                    {
                        if (!((string)arrCiids[j]).Equals(creditInfoId)) { }
                        else
                        {
                            ds.Tables[0].Rows.Remove(ds.Tables[0].Rows[i]);
                            break;
                        }
                    }
                    arrCiids.Add(creditInfoId);
                }
            }
            return ds;
        }


        // The UserAdmin version (identical to NPayments version
        /// <summary>FindCustomer search for Customer in DB. </summary>
        /// <param name="newDebtor">Instance of Debtor.</param>
        /// <param name="exactAddress"></param>
        /// <returns>Returns true if ok otherwise false.</returns>
        public DataSet FindDDDCustomer(Debtor newDebtor, bool exactAddress)
        {
            //	const string funcName = "FindCustomer(Debtor newDebtor)";
            var mySet = new DataSet();

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string maxHits = CigConfig.Configure("lookupsettings.UserAdminMaxHits").Trim();
                string query = "SELECT DISTINCT TOP " + maxHits +
                               " np_Individual.CreditInfoID, rtrim(np_Individual.FirstNameNative+' '+np_Individual.SurNameNative) NameNative, " +
                               "rtrim(np_Individual.FirstNameEN+' '+np_Individual.SurNameEN) NameEN, rtrim(np_Address.StreetNative) StreetNative, rtrim(np_Address.StreetEN) StreetEN,np_IDNumbers.Number, " +
                               "rtrim(np_City.NameNative) CityNative, rtrim(np_City.NameEN) CityEN, np_Address.PostalCode, '0' IsCompany, cpi_Countries.NameNative As CountryNative, cpi_Countries.NameEN As CountryEN, np_IDNumbers.NumberTypeID " +
                               "FROM np_Individual RIGHT OUTER JOIN np_Claims ON np_Claims.CreditinfoID = np_Individual.CreditInfoID ";

                if (newDebtor.IsSearchable)
                {
                    query += "INNER JOIN np_CreditInfoUser ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID "
                             +
                             "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID "
                             + "LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID "
                             + "LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID "
                             + "LEFT OUTER JOIN cpi_Countries ON np_Address.CountryID = cpi_Countries.CountryID WHERE ";
                }
                else
                {
                    query +=
                        "LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN " +
                        "np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID " +
                        "LEFT Outer JOIN np_City on np_Address.CityID = np_City.CityID " +
                        "LEFT Outer JOIN cpi_Countries on np_Address.CountryID = cpi_Countries.CountryID WHERE ";
                }

                bool whereStatement = false;
                if (!string.IsNullOrEmpty(newDebtor.IDNumber1))
                {
                    query += "np_IDNumbers.Number = (N'" + newDebtor.IDNumber1 + "') ";
                    whereStatement = true;
                }
                // only claims with registerd status
                query += "np_Claims.StatusID = 4 ";
                whereStatement = true;
                if (newDebtor.CreditInfo > 0)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "np_Individual.CreditInfoID = " + newDebtor.CreditInfo + " ";
                    whereStatement = true;
                }
                if (newDebtor.FirstName != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query +=
                        "exists (select * from np_word2ciid where np_Individual.creditinfoid=np_word2ciid.creditinfoid and np_word2ciid.type='i' ";
                    query += "and np_word2ciid.word like (N'" + newDebtor.FirstName + "%')) ";

                    //query += "(np_Individual.FirstNameNative LIKE(N'"+newDebtor.FirstName+"%') OR np_Individual.FirstNameEN LIKE(N'"+newDebtor.FirstName+"%')) ";
                    whereStatement = true;
                }
                if (newDebtor.SurName != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    //query += "(np_Individual.SurNameNative LIKE(N'"+newDebtor.SurName+"%') OR np_Individual.SurNameEN LIKE(N'"+newDebtor.SurName+"%')) ";
                    query +=
                        "exists (select * from np_word2ciid where np_Individual.creditinfoid=np_word2ciid.creditinfoid and np_word2ciid.type='i' ";
                    query += "and np_word2ciid.word like (N'" + newDebtor.SurName + "%')) ";
                    whereStatement = true;
                }
                if (newDebtor.Address != null && newDebtor.Address.Count > 0)
                {
                    var address = (Address)newDebtor.Address[0];
                    if (!string.IsNullOrEmpty(address.StreetNative))
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        if (exactAddress)
                        {
                            query += "(StreetNative = (N'" + address.StreetNative + "') OR StreetEN =(N'" +
                                     address.StreetNative + "')) ";
                        }
                        else
                        {
                            query += "(StreetNative LIKE(N'" + address.StreetNative + "%') OR StreetEN LIKE(N'" +
                                     address.StreetNative + "%')) ";
                        }
                        whereStatement = true;
                    }
                    if (!string.IsNullOrEmpty(address.CityNameNative))
                    {
                        if (whereStatement)
                        {
                            query += "AND ";
                        }
                        query += "(np_City.NameNative LIKE(N'" + address.CityNameNative +
                                 "%') OR np_City.NameEN LIKE(N'" + address.CityNameNative + "%')) ";
                        whereStatement = true;
                    }
                }
                if (newDebtor.IsSearchable)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }

                    query += "np_CreditInfoUser.IsSearchable='True' ";
                    whereStatement = true;
                }

                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    query += "np_IDNumbers.Number LIKE('%') ";
                }

                query += " ORDER BY np_Individual.CreditInfoID, np_IDNumbers.NumberTypeID desc ";

                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                try
                {
                    myAdapter.Fill(mySet);
                }
                catch (Exception exc)
                {
                    Logger.WriteToLog("Error in FindCustomer - sql is " + query, exc, true);
                }
                return ClearDuplicatedCIID(mySet);
            }
        }

        protected static DataSet ClearDuplicatedCIID(DataSet ds)
        {
            if (ds.Tables.Count > 0)
            {
                var arrCiids = new ArrayList();
                for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--)
                {
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    // what about empty creditinfoids like when searching Cyprus whitelist?
                    if (creditInfoId == "")
                    {
                        continue;
                    }
                    for (int j = arrCiids.Count - 1; j >= 0; j--)
                    {
                        if (!((string)arrCiids[j]).Equals(creditInfoId)) { }
                        else
                        {
                            ds.Tables[0].Rows.Remove(ds.Tables[0].Rows[i]);
                            break;
                        }
                    }
                    arrCiids.Add(creditInfoId);
                }
            }
            return ds;
        }

        // The UserAdmin version (identical to NPayments version)
        public string GetCIUserType(int creditInfoID)
        {
            //const string funcName = "GetCIUserType(int creditInfoID)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                var myOleDbCommand =
                    new OleDbCommand(
                        "SELECT Type FROM np_CreditInfoUser WHERE CreditInfoId = " + creditInfoID + "", myOleDbConn);
                myOleDbCommand.Connection.Open();
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        return reader.GetString(0);
                    }
                    return "-1";
                }
                return "-1";
            }
        }

        /// <summary>
        /// Returns nationalID matching given CIID
        /// </summary>
        /// <param name="CIID">The creditinfoID to search for</param>
        /// <returns>NationalID if found -1 otherwise</returns>
        public string getNationalIDByCIID(int CIID)
        {

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                var myOleDbCommand =
                    new OleDbCommand(
                        "SELECT Number FROM np_IDNumbers WHERE CreditInfoId = " + CIID + " AND NumberTypeID = 1",
                        myOleDbConn);
                myOleDbCommand.Connection.Open();
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        return reader.GetString(0);
                    }
                    return "-1";
                }
                return "-1";
            }
        }


        // The NPayments version ...
        /// <summary>
        /// Adds company to the database
        /// </summary>
        /// <param name="myComp">Instance of company</param>
        /// <returns>The registed companies internal system ID</returns>
        public int AddCompany(Company myComp)
        {
            const string funcName = "AddCompany(Company myComp)";
            OleDbCommand myOleDbCommand;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                int creditInfoID;
                try
                {
                    // creditInfoID = GetLatestCreditInfoID(); // no longer in use
                    myOleDbCommand =
                        new OleDbCommand(
                            "INSERT INTO np_CreditInfoUser(Type,Email,IsSearchable) VALUES(?,?,?)", myOleDbConn) { Transaction = myTrans };
                    var myParam = new OleDbParameter("Type", OleDbType.Char, 10)
                                  {
                                      Value = myComp.Type,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Email", OleDbType.WChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = myComp.Email
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("IsSearchable", OleDbType.Char, 5)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = myComp.IsSearchable
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    // n� � s��ast innsetta ID
                    myOleDbCommand.Parameters.Clear();
                    myOleDbCommand.CommandText = "SELECT @@IDENTITY";
                    creditInfoID = Convert.ToInt32(myOleDbCommand.ExecuteScalar());

                    myOleDbCommand.Parameters.Clear();
                    if ((myComp.Registered > DateTime.MinValue) && (myComp.Registered < DateTime.MaxValue))
                    {
                        myOleDbCommand.CommandText =
                            "INSERT INTO np_Companys(CreditInfoID,NameNative,NameEN,Established,LastContacted,URL,FuncID,DoNotDelete,Registered) VALUES(?,?,?,?,?,?,?,?,?)";
                    }
                    else
                    {
                        myOleDbCommand.CommandText =
                            "INSERT INTO np_Companys(CreditInfoID,NameNative,NameEN,Established,LastContacted,URL,FuncID,DoNotDelete) VALUES(?,?,?,?,?,?,?,?)";
                    }
                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = creditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Name Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "NameNative",
                        "NameEN",
                        ParameterDirection.Input,
                        myComp.NameNative,
                        myComp.NameEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("Established", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = myComp.Established
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("LastContacted", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = myComp.LastContacted
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("URL", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = myComp.URL
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("FuncID", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = myComp.FuncID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DoNotDelete", OleDbType.Boolean)
                    {
                        Direction = ParameterDirection.Input,
                        Value = myComp.DoNotDelete
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    if ((myComp.Registered > DateTime.MinValue) && (myComp.Registered < DateTime.MaxValue))
                    {
                        myParam = new OleDbParameter("Registered", OleDbType.Date)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = myComp.Registered
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                    }

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    if (myComp.Number.Count > 0)
                    {
                        InsertPhoneNumbers(myComp.Number, myOleDbCommand, creditInfoID);
                    }
                    if (myComp.Address.Count > 0)
                    {
                        InsertAddress(myComp.Address, myOleDbCommand, creditInfoID);
                    }
                    if (myComp.IDNumbers.Count > 0)
                    {
                        InsertIDNumbers(myComp.IDNumbers, myOleDbCommand, creditInfoID);
                    }

                    InsertNote(myComp.Note, myOleDbCommand, creditInfoID);

                    myTrans.Commit();
                }
                catch (OleDbException oleEx)
                {
                    Logger.WriteToLog(className + " : " + funcName, oleEx, true);
                    myTrans.Rollback();
                    if (
                        oleEx.Message.StartsWith(
                            "The statement has been terminated.\r\nViolation of UNIQUE KEY constraint 'IX_np_IDNumbers'."))
                    {
                        return -2;
                    }
                    return -1;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    myTrans.Rollback();
                    return -1;
                }
                return creditInfoID;
            }
        }

        // The UserAdmin version (identical to NPayments version)
        public DataSet GetCompanyNaceCodes()
        {
            //const string funcName = "GetCompanyNaceCodes()";
            var mySet = new DataSet();

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                //myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_CompanyFunction",myOleDbConn);
                myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_CompanyFunction", myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        // The NPayments version ...
        /// <summary>
        /// Updates indivitual
        /// </summary>
        /// <param name="theIndi">Instance of indivitual</param>
        /// <returns>The updeted indivitual insternal system id of ok, otherwise -1</returns>
        public int UpdateIndivitual(Indivitual theIndi)
        {
            const string funcName = "UpdateIndivitual(Indivitual theIndi)";
            OleDbCommand myOleDbCommand;
            int creditInfoID = theIndi.CreditInfoID;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    // Update np_CreditInfoUser
                    myOleDbCommand =
                        new OleDbCommand(
                            "Update np_CreditInfoUser SET Email = ?, IsSearchable=? WHERE CreditInfoID = ?", myOleDbConn) { Transaction = myTrans };

                    var myParam = new OleDbParameter("Email", OleDbType.WChar);
                    if (theIndi.Email != string.Empty)
                    {
                        myParam.Value = theIndi.Email;
                    }
                    else
                    {
                        myParam.Value = DBNull.Value;
                    }
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("IsSearchable", OleDbType.Char, 5)
                              {
                                  Value = theIndi.IsSearchable,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Value = theIndi.CreditInfoID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    myOleDbCommand =
                        new OleDbCommand(
                            "UPDATE np_Individual SET FirstNameNative = ?,FirstNameEN = ?,SurNameNative = ?,SurNameEN = ?,ProfessionID = ?,EducationID = ?, LastUpdate = ?, IsPEP = ?, IsGDPRConsent = ?, DoNotDelete = ?, Processed = 0 WHERE CreditInfoID = ?",

                            myOleDbConn) { Transaction = myTrans };

                    // Insert FirstName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "FirstNameNative",
                        "FirstNameEN",
                        ParameterDirection.Input,
                        theIndi.FirstNameNative,
                        theIndi.FirstNameEN,
                        OleDbType.WChar);

                    // Insert SurName Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "SurNameNative",
                        "SurNameEN",
                        ParameterDirection.Input,
                        theIndi.SurNameNative,
                        theIndi.SurNameEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("ProfessionID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theIndi.ProfessionID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("EducationID", OleDbType.Integer) { Direction = ParameterDirection.Input, Value = theIndi.EducationID };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("LastUpdate", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theIndi.LastUpdate
                              };
                    myOleDbCommand.Parameters.Add(myParam);


                    myParam = new OleDbParameter("IsPEP", OleDbType.Boolean)
                    {
                        Value = theIndi.IsPEP,
                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);
                    
                    myParam = new OleDbParameter("IsGDPRConsent", OleDbType.Boolean)
                    {
                        Value = theIndi.IsGDPRConsent,

                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DoNotDelete", OleDbType.Boolean)
                    {
                        Value = theIndi.DoNotDelete,

                        Direction = ParameterDirection.Input
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theIndi.CreditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);


                    
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    UdateNote(theIndi.Note, myOleDbCommand, theIndi.CreditInfoID);

                    if (theIndi.Number.Count > 0)
                    {
                        UdatePhoneNumbers(theIndi.Number, myOleDbCommand, theIndi.CreditInfoID);
                    }
                    if (theIndi.Address.Count > 0)
                    {
                        UdateAddress(theIndi.Address, myOleDbCommand, theIndi.CreditInfoID);
                    }
                    if (theIndi.IDNumbers.Count > 0)
                    {
                        UdateIDNumbers(theIndi.IDNumbers, myOleDbCommand, theIndi.CreditInfoID);
                    }

                    myTrans.Commit();
                }
                catch (OleDbException oleEx)
                {
                    Logger.WriteToLog(className + " : " + funcName, oleEx, true);
                    myTrans.Rollback();
                    if (
                        oleEx.Message.StartsWith(
                            "The statement has been terminated.\r\nViolation of UNIQUE KEY constraint 'IX_np_IDNumbers'"))
                    {
                        return -2;
                    }
                    return -1;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    myTrans.Rollback();
                    return -1;
                }
                return creditInfoID;
            }
        }

        // The UserAdmin version (indentical to the NPayments version)
        /// <summary>
        /// Updates company info
        /// </summary>
        /// <param name="theComp">Instance of company</param>
        /// <returns>The companies internal system ID if OK, otherwise -1 </returns>
        public int UpdateCompany(Company theComp)
        {
            const string funcName = "UpdateCompany(Company theComp)";
            OleDbCommand myOleDbCommand;
            int creditInfoID = theComp.CreditInfoID;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    // Update np_CreditInfoUser
                    myOleDbCommand =
                        new OleDbCommand(
                            "Update np_CreditInfoUser SET Email = ?, IsSearchable=? WHERE CreditInfoID = ?", myOleDbConn) { Transaction = myTrans };

                    var myParam = new OleDbParameter("Email", OleDbType.WChar);
                    if (theComp.Email != string.Empty)
                    {
                        myParam.Value = theComp.Email;
                    }
                    else
                    {
                        myParam.Value = DBNull.Value;
                    }
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("IsSearchable", OleDbType.Char, 5)
                              {
                                  Value = theComp.IsSearchable,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Value = theComp.CreditInfoID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    if ((theComp.Registered > DateTime.MinValue) && (theComp.Registered < DateTime.MaxValue))
                    {
                        myOleDbCommand =
                            new OleDbCommand(
                                "UPDATE np_Companys SET NameNative = ?,NameEN = ?,Established = ?,LastContacted = ?,URL = ?,FuncID = ?, org_status_code = ?, LastUpdate = ?, DoNotDelete = ?, Registered = ? " +
                                "WHERE CreditInfoID = ?",
                                myOleDbConn);
                    }
                    else
                    {
                        myOleDbCommand =
                            new OleDbCommand(
                                "UPDATE np_Companys SET NameNative = ?,NameEN = ?,Established = ?,LastContacted = ?,URL = ?,FuncID = ?, org_status_code = ?, LastUpdate = ?, DoNotDelete = ? " +
                                "WHERE CreditInfoID = ?",
                                myOleDbConn);
                    }
                    myOleDbCommand.Transaction = myTrans;

                    // Insert Name Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "NameNative",
                        "NameEN",
                        ParameterDirection.Input,
                        theComp.NameNative,
                        theComp.NameEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("Established", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.Established
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("LastContacted", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.LastContacted
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("URL", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.URL
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("FuncID", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.FuncID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("org_status_code", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.Org_status_code
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("LastUpdate", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.LastUpdate
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DoNotDelete", OleDbType.Boolean)
                    {
                        Direction = ParameterDirection.Input,
                        Value = theComp.DoNotDelete
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    if ((theComp.Registered > DateTime.MinValue) && (theComp.Registered < DateTime.MaxValue))
                    {
                        myParam = new OleDbParameter("Registered", OleDbType.Date)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = theComp.Registered
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                    }

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.CreditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    // Update virkar ekki � multiple atri�i vantar einkv�mt ID � �au		
                    if (theComp.Number.Count > 0)
                    {
                        UdatePhoneNumbers(theComp.Number, myOleDbCommand, theComp.CreditInfoID);
                    }
                    if (theComp.Address.Count > 0)
                    {
                        UdateAddress(theComp.Address, myOleDbCommand, theComp.CreditInfoID);
                    }
                    if (theComp.IDNumbers.Count > 0)
                    {
                        UdateIDNumbers(theComp.IDNumbers, myOleDbCommand, theComp.CreditInfoID);
                    }

                    UdateNote(theComp.Note, myOleDbCommand, theComp.CreditInfoID);

                    myTrans.Commit();
                }
                catch (OleDbException oleEx)
                {
                    Logger.WriteToLog(className + " : " + funcName, oleEx, true);
                    myTrans.Rollback();
                    if (
                        oleEx.Message.StartsWith(
                            "The statement has been terminated.\r\nViolation of UNIQUE KEY constraint 'IX_np_IDNumbers'."))
                    {
                        return -2;
                    }
                    return -1;
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(funcName, e, true);
                    myTrans.Rollback();
                    return -1;
                }
                return creditInfoID;
            }
        }

        // This is a "new" function from NPayments
        /// <summary>
        /// Update "limited" company info. Used when updating from BasicInformation in the CPI part
        /// </summary>
        /// <param name="theComp">Instance of Company</param>
        /// <returns>the updated CIID or -1 if failes</returns>
        public int UpdateCompanyFromBasicInfo(Company theComp)
        {
            const string funcName = "UpdateCompanyFromBasicInfo(Company theComp)";
            OleDbCommand myOleDbCommand;
            int creditInfoID = theComp.CreditInfoID;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();
                try
                {
                    // Update np_CreditInfoUser
                    myOleDbCommand = new OleDbCommand(
                        "Update np_CreditInfoUser SET Email = ? WHERE CreditInfoID = ?", myOleDbConn) { Transaction = myTrans };

                    var myParam = new OleDbParameter("Email", OleDbType.WChar);
                    if (theComp.Email != string.Empty)
                    {
                        myParam.Value = theComp.Email;
                    }
                    else
                    {
                        myParam.Value = DBNull.Value;
                    }
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Value = theComp.CreditInfoID,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    if ((theComp.Registered > DateTime.MinValue) && (theComp.Registered < DateTime.MaxValue))
                    {
                        myOleDbCommand =
                            new OleDbCommand(
                                "UPDATE np_Companys SET NameNative = ?,NameEN = ?,Established = ?,LastContacted = ?,URL = ?, org_status_code = ?, LastUpdate = ?, Registered = ? " +
                                "WHERE CreditInfoID = ?",
                                myOleDbConn);
                    }
                    else
                    {
                        myOleDbCommand =
                            new OleDbCommand(
                                "UPDATE np_Companys SET NameNative = ?,NameEN = ?,Established = ?,LastContacted = ?,URL = ?, org_status_code = ?, LastUpdate = ? " +
                                "WHERE CreditInfoID = ?",
                                myOleDbConn);
                    }
                    myOleDbCommand.Transaction = myTrans;

                    // Insert Name Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "NameNative",
                        "NameEN",
                        ParameterDirection.Input,
                        theComp.NameNative,
                        theComp.NameEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("Established", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.Established
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("LastContacted", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.LastContacted
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("URL", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.URL
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("org_status_code", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.Org_status_code
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("LastUpdate", OleDbType.Date)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.LastUpdate
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    if ((theComp.Registered > DateTime.MinValue) && (theComp.Registered < DateTime.MaxValue))
                    {
                        myParam = new OleDbParameter("Registered", OleDbType.Date)
                                  {
                                      Direction = ParameterDirection.Input,
                                      Value = theComp.Registered
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                    }

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.CreditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    // Update email in CreditInfoUser as well ...
                    myOleDbCommand.CommandText = "UPDATE np_CreditInfoUser SET Email = ? WHERE CreditInfoID = ?";
                    myOleDbCommand.Parameters.Clear();

                    myParam = new OleDbParameter("Email", OleDbType.VarChar)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = theComp.Email
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = creditInfoID
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    if (theComp.Number.Count > 0)
                    {
                        UdatePhoneNumbers(theComp.Number, myOleDbCommand, theComp.CreditInfoID);
                    }
                    if (theComp.Address.Count > 0)
                    {
                        UdateAddress(theComp.Address, myOleDbCommand, theComp.CreditInfoID);
                    }
                    if (theComp.IDNumbers.Count > 0)
                    {
                        UdateIDNumbers(theComp.IDNumbers, myOleDbCommand, theComp.CreditInfoID);
                    }

                    UdateNote(theComp.Note, myOleDbCommand, theComp.CreditInfoID);

                    myTrans.Commit();
                }
                catch (Exception e)
                {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    myTrans.Rollback();
                    return -1;
                }
                return creditInfoID;
            }
        }

        // The UserAdmin version (identical to NPayments version)
        /// <summary>
        /// Search the whitelist by given search criteria
        /// </summary>
        /// <param name="myIndi">Instance of Indivitual</param>
        /// <returns>List of indivituals matching the search criteria as DataSet</returns>
        public DataSet SearchWhitelist(Indivitual myIndi)
        {
            //const string funcName = "SearchWhitelist(Indivitual myIndi)";
            var mySet = new DataSet();
            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "SELECT TOP 100 * FROM np_PersonWhitelist wl WHERE ";

                if (myIndi.FirstNameNative != "")
                {
                    query += "wl.NameSurname LIKE(N'%" + myIndi.FirstNameNative + "%')";
                    whereStatement = true;
                }
                if (myIndi.FathersName != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "wl.FathersName LIKE(N'%" + myIndi.FathersName + "%')";
                    whereStatement = true;
                }
                if (myIndi.DateOfBirth != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "wl.DateOfBirth = " + myIndi.DateOfBirth + " ";
                    whereStatement = true;
                }
                if (myIndi.WhitelistAddress != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "wl.Address LIKE(N'%" + myIndi.WhitelistAddress + "%') ";
                    whereStatement = true;
                }
                if (myIndi.WhitelistIDNumber != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "wl.IDNumber LIKE('" + myIndi.WhitelistIDNumber + "%') ";
                    whereStatement = true;
                }

                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    query += "wl.IDNumber LIKE('%')";
                }

                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        // The UserAdmin version (identical to NPayments version)
        /// <summary>
        /// Searches to Companies House data
        /// </summary>
        /// <param name="myComp">Instance of company</param>
        /// <returns>List of companies matching the search criteria as DataSet</returns>
        public DataSet SearchCompaniesHouse(Company myComp)
        {
            //const string funcName = "SearchCompaniesHouse(Company myComp)";
            var mySet = new DataSet();
            bool whereStatement = false;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query = "SELECT TOP 300 * FROM np_CompaniesHouse ch WHERE ";

                if (myComp.NameNative != "")
                {
                    query += "ch.Org_name LIKE(N'" + myComp.NameNative + "%')";
                    whereStatement = true;
                }
                if (myComp.NationalID != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "ch.RegistrationNumber LIKE(N'" + myComp.NationalID + "%')";
                    whereStatement = true;
                }
                if (myComp.Org_name_status_code != "")
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "ch.Org_name_status_code LIKE(N'" + myComp.Org_name_status_code + "%') ";
                    whereStatement = true;
                }
                if (myComp.Org_status_code != 0)
                {
                    if (whereStatement)
                    {
                        query += "AND ";
                    }
                    query += "ch.Org_status_code = " + myComp.Org_status_code + " ";
                    whereStatement = true;
                }

                // nothing to query by ... send it all back
                if (!whereStatement)
                {
                    query += "ch.RegistrationNumber LIKE('%')";
                }

                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        // The UserAdmin version (identical to NPayments version) 
        /// <summary>
        /// Checks wether ID number is registed
        /// </summary>
        /// <param name="number">ID number to search for</param>
        /// <param name="type">ID number type to search for</param>
        /// <returns>true if registed, false otherwise</returns>
        public bool IsIDRegisted(string number, int type)
        {
            //const string funcName = "IsIDRegisted (string number, int type)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                //	var myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                var myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM np_IDNumbers WHERE np_IDNumbers.Number = '" + number +
                        "' AND np_IDNumbers.NumberTypeID = " + type + "",
                        myOleDbConn);
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read())
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Gets CreditInfoUserNameNative
        /// </summary>
        /// <param name="CreditInfoID">The users internal system ID</param>
        /// <returns>The users name in native</returns>
        public string GetCreditInfoUserNativeName(int CreditInfoID)
        {
            string Result = "";
            string myCommandString = "SELECT CASE WHEN dbo.np_Individual.FirstNameNative IS NOT NULL"
                                     +
                                     " THEN dbo.np_Individual.FirstNameNative + ' ' + COALESCE(dbo.np_Individual.SurNameNative, '')"
                                     + " ELSE dbo.np_Companys.NameNative END AS [Name]"
                                     + " FROM dbo.np_CreditInfoUser LEFT OUTER JOIN dbo.np_Companys"
                                     + " ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID"
                                     + " LEFT OUTER JOIN dbo.np_Individual"
                                     + " ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID"
                                     + " WHERE  dbo.np_CreditInfoUser.CreditInfoID = '" + CreditInfoID + "'";

            OleDbCommand myCommand;

            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myConnection.Open();
                try
                {
                    myCommand = new OleDbCommand(myCommandString, myConnection);

                    var reader = myCommand.ExecuteReader();

                    if (reader.Read())
                    {
                        Result = reader.IsDBNull(0) ? "" : reader.GetString(0);
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("GetCreditInfoUserNativeName. <br> Ex: " + ex.Message);
                }
            }

            return Result;
        }

        /// <summary>
        /// Gets CreditInfo user name in english
        /// </summary>
        /// <param name="CreditInfoID">The users internal system ID</param>
        /// <returns>The users name in english</returns>
        public string GetCreditInfoUserENName(int CreditInfoID)
        {
            string Result = "";
            string myCommandString = "SELECT CASE WHEN dbo.np_Individual.FirstNameEN IS NOT NULL"
                                     + " THEN dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN"
                                     + " ELSE dbo.np_Companys.NameEN END AS [Name]"
                                     + " FROM dbo.np_CreditInfoUser LEFT OUTER JOIN dbo.np_Companys"
                                     + " ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID"
                                     + " LEFT OUTER JOIN dbo.np_Individual"
                                     + " ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID"
                                     + " WHERE  dbo.np_CreditInfoUser.CreditInfoID = '" + CreditInfoID + "'";

            OleDbCommand myCommand;

            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myConnection.Open();
                try
                {
                    myCommand = new OleDbCommand(myCommandString, myConnection);

                    var reader = myCommand.ExecuteReader();

                    if (reader.Read())
                    {
                        Result = reader.IsDBNull(0) ? "" : reader.GetString(0);
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                }
            }

            return Result;
        }

        // from NPayments version 
        /// <summary>
        /// Get city ID matching cityname
        /// </summary>
        /// <param name="cityName">The city name</param>
        /// <returns>City id if found, -1 otherwise</returns>
        public int GetCityID(string cityName)
        {
            //const string funcName = "GetCityID(string cityName)";

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                //	var myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                var myOleDbCommand =
                    new OleDbCommand(
                        "SELECT CityID FROM dbo.np_City WHERE (NameNative = N'" + cityName + "')", myOleDbConn);
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read())
                {
                    return reader.GetInt32(0);
                }
                return -1;
            }
        }

        /// <summary>
        /// Gets user by nationalID
        /// </summary>
        /// <param name="nationalID">The users national ID</param>
        /// <returns>Instance of indivitual</returns>
        public Indivitual GetIndivitualByNationalID(string nationalID) { return GetIndivitualByNationalID(nationalID, false); }

        /// <summary>
        /// Gets user by nationalID
        /// </summary>
        /// <param name="nationalID">The users national ID</param>
        /// <param name="isSearchable">True will remove all nonSearchable indiviuals, False will discard IsSearchable field.</param>
        /// <returns>Instance of indivitual</returns>
        public Indivitual GetIndivitualByNationalID(string nationalID, bool isSearchable)
        {
            const string funcName = "GetIndivitualByNationalID(string nationalID)";
            var theIndi = new Indivitual();

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                string query =
                    "SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                    "np_Individual.SurNameNative, np_Individual.SurNameEN, np_Address.StreetNative, np_Address.StreetEN,np_IDNumbers.Number,np_City.NameNative,np_City.NameEN ";

                if (isSearchable)
                {
                    query += "FROM dbo.np_Individual "
                             +
                             "INNER JOIN np_CreditInfoUser ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID "
                             +
                             "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID "
                             + "LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID "
                             + "LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID ";
                }
                else
                {
                    query +=
                        "FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID " +
                        "LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID  ";
                }
                query += "WHERE np_IDNumbers.Number = '" + nationalID + "' AND np_IDNumbers.NumberTypeID = 1 ";

                if (isSearchable)
                {
                    query += "AND np_CreditInfoUser.IsSearchable = 'True' ";
                }

                var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                myOleDbCommand.Connection.Open();
                var reader = myOleDbCommand.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            theIndi.CreditInfoID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            theIndi.FirstNameNative = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            theIndi.FirstNameEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            theIndi.SurNameNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            theIndi.SurNameEN = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            theIndi.SingleAddressNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            theIndi.SingleAddressEN = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            theIndi.NationalID = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            theIndi.SingleCityNative = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9))
                        {
                            theIndi.SingleCityEN = reader.GetString(9);
                        }
                    }
                }
                catch (Exception err)
                {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                }
                finally
                {
                    reader.Close();
                }
            }
            return theIndi;
        }

        /// <summary>
        /// Gets user by nationalID
        /// </summary>
        /// <param name="nationalID">The users national ID</param>
        /// <param name="isSearchable">True will remove all nonSearchable indiviuals, False will discard IsSearchable field.</param>
        /// <returns>Instance of indivitual</returns>
        public Indivitual GetDDDIndivitualByNationalID(string nationalID, bool isSearchable)
        {
            const string funcName = "GetIndivitualByNationalID(string nationalID)";
            var theIndi = new Indivitual();

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                var query =
                    "SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                    "np_Individual.SurNameNative, np_Individual.SurNameEN, np_Address.StreetNative, np_Address.StreetEN,np_IDNumbers.Number,np_City.NameNative,np_City.NameEN ";

                if (isSearchable)
                {
                    query += "FROM dbo.np_Individual "
                             +
                             "INNER JOIN np_CreditInfoUser ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID "
                             +
                             "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID "
                             + "LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID "
                             + "LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID ";
                }
                else
                {
                    query +=
                        "FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID " +
                        "LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID  ";
                }
                query += "RIGHT OUTER JOIN np_Claims ON np_Claims.CreditinfoID = np_Individual.CreditinfoID ";
                query += "WHERE np_IDNumbers.Number = '" + nationalID +
                         "' AND np_IDNumbers.NumberTypeID = 1 AND np_Claims.StatusID = 4 ";

                if (isSearchable)
                {
                    query += "AND np_CreditInfoUser.IsSearchable = 'True' ";
                }

                var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                myOleDbCommand.Connection.Open();
                var reader = myOleDbCommand.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            theIndi.CreditInfoID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1))
                        {
                            theIndi.FirstNameNative = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            theIndi.FirstNameEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            theIndi.SurNameNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            theIndi.SurNameEN = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            theIndi.SingleAddressNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            theIndi.SingleAddressEN = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7))
                        {
                            theIndi.NationalID = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8))
                        {
                            theIndi.SingleCityNative = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9))
                        {
                            theIndi.SingleCityEN = reader.GetString(9);
                        }
                    }
                }
                catch (Exception err)
                {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                }
                finally
                {
                    reader.Close();
                }
            }
            return theIndi;
        }

        /// <summary>
        /// This function returns all employees as DataSet
        /// </summary>
        /// <returns>All employees as DataSet</returns>
        public DataSet GetEmployeesAsDataSet()
        {
            const string funcName = "GetEmployeesAsDataSet()";
            var mySet = new DataSet();

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_Employee", myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return mySet;
        }

        /// <summary>
        /// Takes nationalID and finds matching CIID and returns if found 
        /// </summary>
        /// <param name="nationalID">The companies nationalID</param>
        /// <returns>The companies internal system ID (CIID) or -1 if not found</returns>	
        public int GetCIIDByNationalID(string nationalID)
        {
            const string funcName = "GetCIIDByNationalID(string nationalID)";

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    myOleDbConn.Open();
                    // �essi virkar r�tt
                    //SELECT DISTINCT np_CreditInfoUser.CreditInfoID FROM np_CreditInfoUser LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE np_IDNumbers.Number = '1111111110' AND np_IDNumbers.NumberTypeID = 1
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT DISTINCT np_CreditInfoUser.CreditInfoID FROM np_CreditInfoUser LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE np_IDNumbers.Number = '" +
                            nationalID + "' AND np_IDNumbers.NumberTypeID = 1",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        return reader.GetInt32(0);
                    }
                    return -1;
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return -1;
            }
        }

        public DataSet GetPostcodesByCityAsDataSet(int cityID)
        {
            const string funcName = "GetPostcodesByCityAsDataSet(int cityID)";
            var mySet = new DataSet();

            try
            {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand =
                        new OleDbCommand(
                            "SELECT * FROM np_Postcode JOIN np_City ON np_City.NameNative = np_Postcode.CityNative WHERE np_City.CityID = " +
                            cityID + "",
                            myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return mySet;
        }

                /// <summary>
        /// Return list of Vat numbers by credit info id.
        /// </summary>
        /// <param name="creditinfoId"></param>
        /// <returns></returns>
        public IList<BLL.CIUsers.VatNumber> GetVatNumbersByCreditinfoId(int creditinfoId,bool showOnly)
        {
            IList<BLL.CIUsers.VatNumber> retValue = new List<BLL.CIUsers.VatNumber>();
            try
            {
                using (var oleDbConnection = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {

                    var oleDbDataAdapter = new OleDbDataAdapter();
                    string sqlString = "SELECT * FROM np_VatNumbers WHERE CreditInfoID = " + creditinfoId.ToString();
                    if (showOnly)
                    {
                        sqlString += " AND Show = 1 ";
                    }

                    sqlString += "ORDER BY Number";

                    oleDbDataAdapter.SelectCommand = new OleDbCommand(sqlString);
                    //oleDbDataAdapter.SelectCommand.Parameters.Add(new OleDbParameter("@CreditInfoID", OleDbType.Integer)).Value = creditinfoId;
                    try
                    {
                        oleDbDataAdapter.SelectCommand.Connection = oleDbConnection;
                        oleDbDataAdapter.SelectCommand.Connection.Open();
                        OleDbDataReader reader = oleDbDataAdapter.SelectCommand.ExecuteReader();
                        while (reader.Read())
                        {
                            BLL.CIUsers.VatNumber vatNumber = new VatNumber();
                            vatNumber.Id = reader.GetInt32(reader.GetOrdinal("ID"));
                            vatNumber.CreditInfoID = reader.GetInt32(reader.GetOrdinal("CreditInfoID"));
                            vatNumber.Number = reader[reader.GetOrdinal("Number")].ToString();

                            vatNumber.TraderName = reader.IsDBNull(reader.GetOrdinal("TraderName")) ? string.Empty : reader[reader.GetOrdinal("TraderName")].ToString();
                            vatNumber.Building = reader.IsDBNull(reader.GetOrdinal("Building")) ? string.Empty : reader[reader.GetOrdinal("Building")].ToString();
                            vatNumber.Address = reader.IsDBNull(reader.GetOrdinal("Address")) ? string.Empty : reader[reader.GetOrdinal("Address")].ToString();
                            vatNumber.Street = reader.IsDBNull(reader.GetOrdinal("Street")) ? string.Empty : reader[reader.GetOrdinal("Street")].ToString();
                            vatNumber.City = reader.IsDBNull(reader.GetOrdinal("City")) ? string.Empty : reader[reader.GetOrdinal("City")].ToString();
                            vatNumber.PostCode = reader.IsDBNull(reader.GetOrdinal("PostCode")) ? string.Empty : reader[reader.GetOrdinal("PostCode")].ToString();
                            vatNumber.Tel = reader.IsDBNull(reader.GetOrdinal("Tel")) ? string.Empty : reader[reader.GetOrdinal("Tel")].ToString();
                            vatNumber.Fax = reader.IsDBNull(reader.GetOrdinal("Fax")) ? string.Empty : reader[reader.GetOrdinal("Fax")].ToString();
                            vatNumber.Note = reader.IsDBNull(reader.GetOrdinal("Note")) ? string.Empty : reader[reader.GetOrdinal("Note")].ToString();
                            vatNumber.Show = reader.GetBoolean(reader.GetOrdinal("Show"));
                            retValue.Add(vatNumber);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        if (oleDbDataAdapter.SelectCommand != null)
                        {
                            if (oleDbDataAdapter.SelectCommand.Connection != null)
                            {
                                oleDbDataAdapter.SelectCommand.Connection.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception exa)
            {
                Logger.WriteToLog(className + " : " + "GetVatNumbersByCreditinfoId(int creditinfoId,bool showOnly)", exa, true);
            }

            return retValue;
        }

        /// <summary>
        /// Return list of Vat numbers by credit info id.
        /// </summary>
        /// <param name="creditinfoId"></param>
        /// <returns></returns>
        public IList<BLL.CIUsers.VatNumber> GetVatNumbersByCreditinfoId(int creditinfoId)
        {
            return this.GetVatNumbersByCreditinfoId(creditinfoId, false);           
        }
    
    }
}
