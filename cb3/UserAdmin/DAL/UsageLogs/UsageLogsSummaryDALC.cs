using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using Logging.BLL;
using cb3;

namespace UserAdmin.DAL.UsageLogs {
    /// <summary>
    /// Summary description for UsageLogsSummaryDALC.
    /// </summary>
    public class UsageLogsSummaryDALC : BaseDALC {

        public DataSet GetLoggingTypeAsDataSet() {
            var mySet = new DataSet();
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_UsageTypes", myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        public DataSet GetUsageData(
            string FromDate,
            string ToDate,
            string UserName,
            string LoggingType,
            string SubscriberID,
            string CreditInfoID,
            string FirstName,
            string SurName) {
            var mySet = new DataSet();
            bool whereStatement;
            string myConnectionString = DatabaseHelper.ConnectionString();

            string query = "SELECT TOP 100 PERCENT au_users.UserName, "
                           + "np_UsageTypes.typeNative, COUNT(np_Usage.result_count) AS OperationRequest, "
                           +
                           "SUM(np_Usage.result_count) AS OperationResultsSum, CASE WHEN np_Companys.NameNative > '' THEN "
                           +
                           "np_Companys.NameNative ELSE np_Individual.FirstNameNative + ' ' + np_Individual.SurNameNative "
                           + "END AS [Name] "
                           + "FROM dbo.np_UsageTypes "
                           + "LEFT JOIN dbo.np_Usage ON dbo.np_UsageTypes.id = dbo.np_Usage.query_type "
                           + "LEFT OUTER JOIN dbo.au_Subscribers "
                           +
                           "LEFT JOIN dbo.np_Companys ON dbo.au_Subscribers.CreditInfoID = dbo.np_Companys.CreditInfoID "
                           + "LEFT JOIN dbo.au_users ON dbo.au_Subscribers.CreditInfoID = dbo.au_users.SubscriberID "
                           +
                           "LEFT JOIN dbo.np_IDNumbers ON dbo.au_Subscribers.CreditInfoID = dbo.np_IDNumbers.CreditInfoID "
                           +
                           "LEFT OUTER JOIN dbo.np_Individual ON dbo.au_Subscribers.CreditInfoID = dbo.np_Individual.CreditInfoID ON dbo.np_Usage.UserID = dbo.au_users.id "
                           + " WHERE ";

            // Fyrir FromDate og ToDate
            if (FromDate != "" && ToDate != "") {
                query += "dbo.np_Usage.created BETWEEN '" + FromDate + "' AND '" + ToDate + "' ";
                whereStatement = true;
            } else if (FromDate != "") {
                query += "dbo.np_Usage.created >= '" + FromDate + "'";
                whereStatement = true;
            } else if (ToDate != "") {
                query += "dbo.np_Usage.created <= '" + ToDate + "'";
                whereStatement = true;
            } else {
                query += "dbo.np_Usage.created > '1900-01-01 00:00:00'";
                whereStatement = true;
            }

            // UserName
            if (UserName != "") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "dbo.au_users.UserName LIKE (N'" + UserName + "%')";
                whereStatement = true;
            }

            if (Convert.ToInt32(LoggingType) > 0) {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "dbo.np_Usage.query_type LIKE (N'" + LoggingType + "%')";
                whereStatement = true;
            }

            if (SubscriberID != "") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "np_IDNumbers.Number LIKE (N'" + SubscriberID + "%')";
                whereStatement = true;
            }

            if (CreditInfoID != "") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "np_IDNumbers.CreditInfoID LIKE (N'" + CreditInfoID + "%')";
                whereStatement = true;
            }

            if (FirstName != "") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "np_Companys.NameNative LIKE (N'" + FirstName + "%') OR np_Individual.FirstNameNative LIKE (N'" +
                         FirstName + "%')";
                whereStatement = true;
            }

            if (SurName != "") {
                if (whereStatement) {
                    query += " AND ";
                }

                query += "np_Individual.SurNameNative LIKE (N'" + SurName + "%')";
                whereStatement = true;
            }

            query += "GROUP BY au_users.UserName, np_UsageTypes.typeNative, np_Companys.NameNative, np_Individual.FirstNameNative, np_Individual.SurNameNative "
                     + "ORDER BY au_users.UserName, np_UsageTypes.typeNative";

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    var myCommand = new OleDbCommand(query, myConnection);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        public DataSet GetUsageLogSummaryForUserAsDataSet(string FromDate, string ToDate, string UserName) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    const string myCommandString = "SELECT a.UserName, b.typeNative, "
                                                   + "COUNT(c.result_count) AS OperationRequest, SUM(c.result_count) "
                                                   + "AS OperationResultsSum FROM dbo.np_UsageTypes b INNER JOIN "
                                                   + "dbo.np_Usage c ON b.id = c.query_type INNER JOIN dbo.au_users a "
                                                   + "ON c.UserID = a.id WHERE (c.created BETWEEN ? "
                                                   + "AND ?) AND a.UserName LIKE ? GROUP BY b.typeNative, a.UserName "
                                                   + "ORDER BY a.UserName, b.typeNative";
                    //myCommand = new OleDbCommand("SELECT a.UserName, b.typeNative, COUNT(c.result_count) AS OperationRequest, SUM(c.result_count) AS OperationResultsSum FROM dbo.np_UsageTypes b INNER JOIN dbo.np_Usage c ON b.id = c.query_type INNER JOIN dbo.au_users a ON c.Creditinfoid = a.CreditInfoID WHERE (c.created BETWEEN '?' AND '?') GROUP BY b.typeNative, a.UserName ORDER BY a.UserName, b.typeNative", myConnection);
                    myCommand = new OleDbCommand(myCommandString, myConnection);

                    // FromDate sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("FromDate", OleDbType.Date)
                                      {
                                          Value = Convert.ToDateTime(FromDate),
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // ToDate sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("ToDate", OleDbType.Date)
                                  {
                                      Value = Convert.ToDateTime(ToDate),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // ToDate sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("UserName", OleDbType.WChar)
                                  {
                                      Value = UserName,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }
    }
}