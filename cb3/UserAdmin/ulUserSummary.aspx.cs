using System;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserAdmin.BLL;
using UserAdmin.Localization;

namespace UserAdmin {
    /// <summary>
    /// Summary description for ulUserSummary.
    /// </summary>
    public partial class ulUserSummary : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private bool EN;
        private uaFactory myFactory;

        protected void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
            Page.ID = "108";
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();

            myFactory = new uaFactory();
            AddEnterEvent();

            if (!IsPostBack) {
                DateTime dt = DateTime.Now;
                dt = dt.AddDays(1 - dt.Day);
                txtFromDate.Text = dt.ToShortDateString();
                DateTime dt2 = dt.AddDays(DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month) - 1);
                txtToDate.Text = dt2.ToShortDateString();

                //txtFromDate.Text = DateTime.Today.Year.ToString()+"-"+DateTime.Today.Month.ToString()+"-"+DateTime.Today.Day.ToString();
                //txtToDate.Text = DateTime.Today.Year.ToString()+"-"+DateTime.Today.Month.ToString()+"-"+DateTime.Today.Day.ToString();

                ddlLoggingType.DataSource = myFactory.GetLoggingTypeAsDataSet();

                ddlLoggingType.DataTextField = EN ? "typeEN" : "typeNative";

                ddlLoggingType.DataValueField = "id";
                ddlLoggingType.DataBind();

                // B�tt vi� �annig a� h�gt s� a� sj� allar notkunartegundir...
                var itm = new ListItem("All", "0") {Selected = true};
                ddlLoggingType.Items.Add(itm);

                /*this.ddlLoggingType.Items.Add("All");
				this.ddlLoggingType.Items[3].Value = "0";
				this.ddlLoggingType.Items[3].Selected = true;*/
            }

            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();
        }

        protected void btnExecute_Click(object sender, EventArgs e) {
            /*string FromDate = "";
			string ToDate = "";
			
			if (txtFromDate.Text != "")
				FromDate = txtFromDate.Text + " 00:00:00";

			if (txtToDate.Text != "")
				ToDate = txtToDate.Text + " 23:59:59";*/
            DateTime FromDate;
            DateTime ToDate;

            string fromDate = "";
            string toDate = "";

            // Setja textastrengi, au�veldara a� vinna me� ��...
            if (txtFromDate.Text != "") {
                FromDate = DateTime.Parse(txtFromDate.Text);
                fromDate = FromDate.Year + "-" + FromDate.Month + "-" + FromDate.Day + " 00:00:00";
            }

            if (txtToDate.Text != "") {
                ToDate = DateTime.Parse(txtToDate.Text);
                ToDate = ToDate.AddHours(23);
                ToDate = ToDate.AddMinutes(59);
                ToDate = ToDate.AddSeconds(59);
                toDate = ToDate.Year + "-" + ToDate.Month + "-" + ToDate.Day + " 23:59:59";
            }

            string UserName = txtUserName.Text;
            string LoggingType = ddlLoggingType.SelectedValue;
            string SubscriberID = txtSubscriberID.Text;
            string CreditInfoID = txtCreditInfoID.Text;
            string FirstName = txtFirstName.Text;
            string SurName = txtSurName.Text;

            using (
                DataSet mySet = myFactory.GetUsageData(
                    fromDate, toDate, UserName, LoggingType, SubscriberID, CreditInfoID, FirstName, SurName)) {
                dgUsageSummary.DataSource = mySet;
                dgUsageSummary.DataBind();

                int iSumOfTimes = 0;
                int iSumOfResults = 0;

                foreach (DataRow myRow in mySet.Tables[0].Rows) {
                    iSumOfTimes += Convert.ToInt32(myRow[2].ToString());
                    iSumOfResults += Convert.ToInt32(myRow[3].ToString());
                }
                lblSumOfTimes.Text = iSumOfTimes.ToString();
                lblSumOfResults.Text = iSumOfResults.ToString();
            }
        }

        private void LocalizeText() {
            //15
            lblAccessLogTotal.Text = rm.GetString("txtAccessLogTotal", ci);
            lblCreditInfoID.Text = rm.GetString("txtCreditInfoID", ci);
            lblFirstName.Text = rm.GetString("txtFirstName", ci);
            lblFromDate.Text = rm.GetString("txtFromDate", ci);
            lblLoggingType.Text = rm.GetString("txtLoggingType", ci);
            lblLogInfo.Text = rm.GetString("txtLogInfo", ci);
            lblPageHeader.Text = rm.GetString("txtUsageSummary", ci);
            lblResultLogTotal.Text = rm.GetString("txtResultLogTotal", ci);
            lblSubscriberID.Text = rm.GetString("txtSubscriberID", ci);
            lblSubscriberInfo.Text = rm.GetString("txtSubscriberInfo", ci);
            lblSurName.Text = rm.GetString("txtSurName", ci);
            lblToDate.Text = rm.GetString("txtToDate", ci);
            lblUserName.Text = rm.GetString("txtUserName", ci);
            btnExecute.Text = rm.GetString("txtExecute", ci);
            btnPrint.Text = rm.GetString("txtPrint", ci);
        }

        protected void btnPrint_Click(object sender, EventArgs e) {
            const string myScript = "<script language=Javascript>window.print();</script>";
            Page.RegisterClientScriptBlock("alert", myScript);
        }

        private void AddEnterEvent() {
            var frm = FindControl("UsageSummary");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is RadioButtonList) {
                    ((RadioButtonList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is RadioButtonList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { }

        #endregion
    }
}