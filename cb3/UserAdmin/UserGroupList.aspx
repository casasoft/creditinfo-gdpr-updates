﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserGroupList.aspx.cs" Inherits="cb3.UserAdmin.UserGroup" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>List User Group</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="pragma" content="no-cache">
    <link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="ListUserGroup" runat="server">
    <table width="997" height="600" align="center" border="0">
    <tr>
        <td colspan="4">
            <uc1:head id="Head1" runat="server"></uc1:head>
        </td>
    </tr>
    <tr valign="top">
    <td width="1"></td>
    <td>
    <table width="100%" height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <uc1:language id="Language1" runat="server"></uc1:language>
        </td>
        <td></td>
        <td align="right">
            <ucl:options id="Options1" runat="server"></ucl:options>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
    </tr>
    <tr>
        <td height="10"></td>
    </tr>
    <tr>
        <td></td>
        <td>
            <uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
        </td>
        <td align="right">
            <ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
        </td>
    </tr>
    <tr>
        <td height="10"></td>
    </tr>
    <tr valign="top">
        <td width="150" valign="top" align="left">
            <table width="98%">
                <tr>
                    <td>
                        <uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
                    </td>
                </tr>
            </table>
        </td>
        <td colspan="2">
            <!-- Main Body Starts -->
            <table width="100%">
                <tr>
                    <td>
                        <table class="grid_table" cellspacing="0" cellpadding="0">
                            <tr>
                                <th>
                                    <asp:label id="lblPageHeader" runat="server" cssclass="HeadMain">User Groups</asp:label>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <table class="datagrid" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div class="TA" id="UserGroupList" runat="server" style="OVERFLOW-X: auto; OVERFLOW: auto; WIDTH: 100%; HEIGHT: auto">
                                                    <asp:datagrid id="dgUserGroupList" runat="server" allowsorting="True" autogeneratecolumns="False"
                                                                  gridlines="None" cssclass="grid">
                                                        <footerstyle cssclass="grid_footer"></footerstyle>
                                                        <selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
                                                        <alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
                                                        <itemstyle cssclass="grid_item"></itemstyle>
                                                        <headerstyle cssclass="grid_header"></headerstyle>
                                                        <columns>
                                                            <asp:ButtonColumn Text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;" CommandName="Edit">
                                                        <itemstyle cssclass="leftpadding">
                                                        </ItemStyle>
                                                        </asp:ButtonColumn>
                                                            <asp:BoundColumn DataField="ID" SortExpression="Name" HeaderText="Group ID">
                                                                <itemstyle cssclass="padding"></itemstyle>
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="GroupName" SortExpression="Name" HeaderText="Group Name">
                                                                <itemstyle cssclass="padding"></itemstyle>
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="GroupCode" SortExpression="UserName" HeaderText="Group Code">
                                                                <itemstyle cssclass="padding"></itemstyle>
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn Visible="False" DataField="GroupDesc" HeaderText="Group Description">
                                                                <itemstyle cssclass="padding"></itemstyle>
                                                            </asp:BoundColumn>
                                                        </columns>
                                                        <pagerstyle cssclass="grid_pager"></pagerstyle>
                                                    </asp:datagrid>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="empty_table" cellspacing="0">
                                                    <tr>
                                                        <td align="left">
                                                        </td>
                                                        <td align="right">
                                                            <asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel" ></asp:button><asp:button id="btnNew" runat="server" cssclass="confirm_button" text="New" OnClick="btnNew_Click" ></asp:button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
            </table>
            <!-- Main Body Ends -->
        </td>
    </tr>
    <tr>
        <td height="20"></td>
    </tr>
    <tr>
        <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
    </tr>
    </table>
    </td>
    <td width="2"></td>
    </tr>
    <tr>
        <td align="center" colspan="4">
            <uc1:footer id="Footer1" runat="server"></uc1:footer>
            <uc1:secureuseradmin id="SecureUserAdmin1" runat="server"></uc1:secureuseradmin>
        </td>
    </tr>
    </table>
    </form>
</body>
</html>
