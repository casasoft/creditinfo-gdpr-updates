<%@ Page language="c#" Codebehind="DepartmentDetails.aspx.cs" AutoEventWireup="True" Inherits="UserAdmin.DepartmentDetails" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en" >
<html>
  <head>
		<title>Subscriber details</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>function checkEnterKey() 
			{    
				if (event.keyCode == 13) 
				{        
					event.cancelBubble = true;
					event.returnValue = false;
					SubscriberDetails.btnRegister.click(); 
				}
			}</script>
</head>
	<body ms_positioning="GridLayout">
		<form id="SubscriberDetails" name="SubscriberDetails" action="" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblDepartmentDetailsHeader" runat="server"></asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblNameNT" runat="server" ></asp:label>
																		<br>
																		<asp:textbox id="txtNameNT" runat="server" ></asp:textbox>
																	</td>
																	<td>
																		<asp:label id="lblNameEN" runat="server" ></asp:label>
																		<br>
																		<asp:textbox id="txtNameEN" runat="server" ></asp:textbox>
																	</td>
																	<td><asp:Label id="lblPhone" runat="server"></asp:Label><br><asp:TextBox id="txtPhone" runat="server"></asp:TextBox>
																	</td>
																	<td><asp:label id="lblEmail" runat="server"></asp:label><br><asp:textbox id="txtEmail" runat="server"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblStreetNT" runat="server" ></asp:label>
																		<br>
																		<asp:textbox id="txtStreetNT" runat="server" ></asp:textbox>
																	</td>
																	<td>
																		<asp:label id="lblStreetEN" runat="server" ></asp:label>
																		<br>
																		<asp:textbox id="txtStreetEN" runat="server" ></asp:textbox>
																	</td>
																	<td><asp:label id="lblPostcode" runat="server"></asp:label><br><asp:textbox id="txtPostCode" runat="server"></asp:textbox>
																	</td>
																	<td><asp:Label id="lblContactPerson" runat="server"></asp:Label><br><asp:TextBox id="txtContactPerson" runat="server"></asp:TextBox>
																	</td>
																</tr>																	
																<tr>
																	<td colspan="2"><asp:Label id="lblBillingAccount" runat="server"></asp:Label><br><asp:TextBox id="txtBillingAccount" runat="server"></asp:TextBox>
																	</td>
																	<td><asp:Label id="lblIsHeadOffice" runat="server"></asp:Label><br><asp:CheckBox id="chbIsHeadOffice" runat="server" CssClass="radio"></asp:CheckBox>
																	</td>
																	<td><asp:Label id="lblIsOpen" runat="server"></asp:Label><br><asp:checkbox id="chbIsOpen" runat="server" checked="True" cssclass="radio"></asp:checkbox>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END -->
											</td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tblUsers" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td><asp:radiobutton id="rbOpen" runat="server" cssclass="radio" AutoPostBack="True" Checked="True" groupname="IsOpen" text="[Open]" oncheckedchanged="IsOpen_CheckedChanged"></asp:radiobutton>&nbsp; 
<asp:radiobutton id="rbClosed" runat="server" cssclass="radio" AutoPostBack="True" groupname="IsOpen" text="[Closed]" oncheckedchanged="IsOpen_CheckedChanged"></asp:radiobutton>&nbsp; 
<asp:radiobutton id="rbBoth" runat="server" cssclass="radio" AutoPostBack="True" groupname="IsOpen" text="[Both]" oncheckedchanged="IsOpen_CheckedChanged"></asp:radiobutton></td>
														<td align="right">
															<asp:label id="lblUsersIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th colspan="2">
															<asp:label id="lblUsersHeader" runat="server"></asp:label>
														</th>
													</tr>
													<tr>
														<td colspan="2">
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dgUsers" runat="server" autogeneratecolumns="False" gridlines="None" cssclass="grid">
<footerstyle cssclass="grid_footer">
</FooterStyle>

<selecteditemstyle cssclass="grid_selecteditem">
</SelectedItemStyle>

<alternatingitemstyle cssclass="grid_alternatingitem">
</AlternatingItemStyle>

<itemstyle cssclass="grid_item">
</ItemStyle>

<headerstyle cssclass="grid_header">
</HeaderStyle>

<columns>
<asp:ButtonColumn Text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;" CommandName="Edit">
<itemstyle cssclass="leftpadding">
</ItemStyle>
</asp:ButtonColumn>
<asp:BoundColumn Visible="False" DataField="id">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="CreditInfoID" HeaderText="CIID">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="UserName" HeaderText="Username">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="Email" HeaderText="Email">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="UserType" HeaderText="UserType">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="cntCreditWatch" HeaderText="WatchLimit">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="IsOpen" HeaderText="Open">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="DateCreated" HeaderText="Created">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
</Columns>

<pagerstyle cssclass="grid_pager">
</PagerStyle>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>										
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td>
															<asp:label id="lblMessage" runat="server" width="100%" visible="False" cssclass="error_text"></asp:label>
														</td>
														<td align="right"><asp:Button id="btnNewUser" runat="server" CssClass="gray_button" onclick="btnNewUser_Click"></asp:Button>
															<asp:button id="btnInsert" runat="server" cssclass="confirm_button" onclick="btnInsert_Click"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>										
									</table>																		
									
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
						<uc1:secureuseradmin id="SecureUserAdmin1" runat="server"></uc1:secureuseradmin>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
