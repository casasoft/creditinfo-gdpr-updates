<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>

<%@ Page Language="c#" CodeBehind="NewCIUser.aspx.cs" AutoEventWireup="false" Inherits="UserAdmin.NewDebtor" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>New Debtor</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="pragma" content="no-cache">
    <link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        BR.pageEnd
        {
            page-break-after: always;
        }
    </style>

    <script language="javascript">
        function checkEnterKey() {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                newuser.btRebDebtor.click();
            }
        }

        function SetFormFocus() {
            document._newuser.tbDebtorFirstName.focus();
        }

    </script>

</head>
<body bgcolor="#ffffff" onload="SetFormFocus()" ms_positioning="GridLayout">
    <form id="_newuser" name="_newuser" action="" method="post" runat="server">
    <table height="600" width="997" align="center" border="0">
        <tr>
            <td colspan="4">
                <uc1:head ID="Head1" runat="server"></uc1:head>
            </td>
        </tr>
        <tr valign="top">
            <td width="1">
            </td>
            <td>
                <table height="100%" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <uc1:language ID="Language1" runat="server"></uc1:language>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <ucl:options ID="Options1" runat="server"></ucl:options>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#000000" colspan="3" height="1">
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <uc1:sitePositionBar ID="SitePositionBar1" runat="server"></uc1:sitePositionBar>
                        </td>
                        <td align="right">
                            <ucl:UserInfo ID="UserInfo1" runat="server"></ucl:UserInfo>
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td valign="top" align="left" width="150">
                            <table width="98%">
                                <tr>
                                    <td>
                                        <uc1:panelBar ID="PanelBar1" runat="server"></uc1:panelBar>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2">
                            <!-- Main Body Starts -->
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lbNewIndividual" runat="server" CssClass="HeadMain">New Individual</asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 33%">
                                                                <asp:Label ID="lbDebtorFirstName" runat="server">First name</asp:Label><br>
                                                                <asp:TextBox ID="tbDebtorFirstName" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox><asp:Label
                                                                    ID="Label1" runat="server" CssClass="error_text">*</asp:Label><asp:RequiredFieldValidator
                                                                        ID="RequiredFieldValidator2" runat="server" Display="None" ControlToValidate="tbDebtorFirstName"
                                                                        ErrorMessage="Value missing!"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td style="width: 33%">
                                                                <asp:Label ID="lbFirstNameEN" runat="server">First name(EN)</asp:Label><br>
                                                                <asp:TextBox ID="tbFirstNameEN" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblIsSearchable" runat="server">[IsSearchable]</asp:Label><br>
                                                                <asp:RadioButton ID="rbtIsSearchableTrue" runat="server" CssClass="radio" Text="[Yes]"
                                                                    GroupName="IsSearchable" Checked="True"></asp:RadioButton>&nbsp;<asp:RadioButton
                                                                        ID="rbtIsSearchableFalse" runat="server" CssClass="radio" Text="[No]" GroupName="IsSearchable">
                                                                    </asp:RadioButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbDebtorSurName" runat="server">Surname</asp:Label><br>
                                                                <asp:TextBox ID="tbDebtorSurName" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox><asp:Label
                                                                    ID="Label2" runat="server" CssClass="error_text">*</asp:Label><asp:RequiredFieldValidator
                                                                        ID="RequiredFieldValidator1" runat="server" Display="None" ControlToValidate="tbDebtorSurName"
                                                                        ErrorMessage="Value missing!" Enabled="False"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbSurnameEN" runat="server">Surname(EN)</asp:Label><br>
                                                                <asp:TextBox ID="tbSurnameEN" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbEmail" runat="server">Email</asp:Label><br>
                                                                <asp:TextBox ID="tbEmail" runat="server" MaxLength="30"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23"><asp:label id="lblpep" runat="server">Is PEP</asp:label><br>
                                                                <asp:checkbox id=chkIsPEP runat="server" cssclass="radio" ></asp:checkbox></td>
                                                            
                                                            <td height="23"><asp:label id="lblGDPRConsent" runat="server">Is GDPR Consented</asp:label><br>
                                                                <asp:checkbox id=chkIsGDPRConsent runat="server" cssclass="radio" ></asp:checkbox></td>
                                                            
                                                            <td height="23"><asp:label id="lbldonotdelete" runat="server">Do Not Delete</asp:label><br>
                                                                <asp:checkbox id=chkdoNotDelete runat="server" cssclass="radio" ></asp:checkbox></td>
                                                            



                                                            <td height="23">
                                                                <asp:Label ID="lbDebtorCIID" runat="server" Visible="False">CreditInfoID</asp:Label><asp:TextBox
                                                                    ID="tbDebtorCIID" runat="server" Visible="False" ReadOnly="True"></asp:TextBox>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblAddressOne" runat="server" CssClass="HeadMain"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress1" runat="server">Address1</asp:Label><br>
                                                                <asp:TextBox ID="tbAddress1" runat="server" MaxLength="150" EnableViewState="False"></asp:TextBox><asp:Label
                                                                    ID="Label3" runat="server" CssClass="error_text">*</asp:Label><asp:RequiredFieldValidator
                                                                        ID="RequiredFieldValidator3" runat="server" Display="None" ControlToValidate="tbAddress1"
                                                                        ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress1EN" runat="server">Address1(EN)</asp:Label><br>
                                                                <asp:TextBox ID="tbAddress1EN" runat="server" MaxLength="150" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress1Info" runat="server">Address1 Info</asp:Label><br>
                                                                <asp:TextBox ID="tbAddress1Info" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbAddress1InfoEN" runat="server">Address1 Info(EN)</asp:Label><br>
                                                                <asp:TextBox ID="tbAddress1InfoEN" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbPostalCode" runat="server">PostalCode1</asp:Label><br>
                                                                <asp:TextBox ID="tbPostalCode1" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbPostBox" runat="server">PostBox1</asp:Label><br>
                                                                <asp:TextBox ID="tbPostBox1" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lbCity1" runat="server">City1</asp:Label><asp:Label ID="lblCitySearch"
                                                                    runat="server">Search</asp:Label><br>
                                                                <asp:DropDownList ID="ddCity1" runat="server">
                                                                </asp:DropDownList>
                                                                &nbsp;
                                                                <asp:TextBox ID="txtCitySearch" runat="server" MaxLength="10" Width="50px"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnCitySearch" runat="server" CssClass="popup" CausesValidation="False"
                                                                    Text="..."></asp:Button><asp:Label ID="Label4" runat="server" CssClass="error_text">*</asp:Label><asp:RequiredFieldValidator
                                                                        ID="rfvCity" runat="server" Display="None" ControlToValidate="ddCity1" ErrorMessage="Value missing!"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 380px" height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblAddressTwo" runat="server" CssClass="HeadMain"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress2" runat="server">Address2</asp:Label><br>
                                                                <asp:TextBox ID="tbAddress2" runat="server" MaxLength="150" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress2EN" runat="server">Address2(EN)</asp:Label><br>
                                                                <asp:TextBox ID="tbAddress2EN" runat="server" MaxLength="150" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress2Info" runat="server">Address2 Info</asp:Label><br>
                                                                <asp:TextBox ID="tbAddress2Info" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbAddress2InfoEN" runat="server">Address2 Info(EN)</asp:Label><br>
                                                                <asp:TextBox ID="tbAddress2InfoEN" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="ltPostalCode2" runat="server">PostalCode2</asp:Label><br>
                                                                <asp:TextBox ID="tbPostalCode2" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbPostBox2" runat="server">PostBox2</asp:Label><br>
                                                                <asp:TextBox ID="tbPostBox2" runat="server" MaxLength="50" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lbCity2" runat="server">City2</asp:Label><asp:Label ID="lblCity2Search"
                                                                    runat="server">Search</asp:Label><br>
                                                                <asp:DropDownList ID="ddCity2" runat="server">
                                                                </asp:DropDownList>
                                                                &nbsp;
                                                                <asp:TextBox ID="txtCity2Search" runat="server" MaxLength="10" Width="50px"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnCity2Search" runat="server" CssClass="popup" CausesValidation="False"
                                                                    Text="..."></asp:Button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblOtherInfo" runat="server"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 180px">
                                                                <asp:Label ID="lbProfession" runat="server">Profession</asp:Label><br>
                                                                <asp:DropDownList ID="ddProfession" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 180px">
                                                                <asp:Label ID="lbEducation" runat="server">Education</asp:Label><br>
                                                                <asp:DropDownList ID="ddEducation" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 180px">
                                                                <asp:Label ID="lblNote" runat="server">Note</asp:Label><br>
                                                                <asp:TextBox ID="tbNote" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 180px">
                                                                <asp:Label ID="lbIDNumber2" runat="server">IDNumber 1</asp:Label><br>
                                                                <asp:TextBox ID="tbIDNumber1" runat="server" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 180px">
                                                                <asp:Label ID="lbIDNumberType" runat="server">Type</asp:Label><br>
                                                                <asp:DropDownList ID="ddNumberType1" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 180px">
                                                                <asp:Label ID="lbDebtorIDNumber3" runat="server">IDNumber 2</asp:Label><br>
                                                                <asp:TextBox ID="tbIDNumber2" runat="server" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbIDNumberType2" runat="server">Type</asp:Label><br>
                                                                <asp:DropDownList ID="ddNumberType2" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 143px">
                                                                <asp:Label ID="lbPhoneNumber1" runat="server">Phone number 1</asp:Label><br>
                                                                <asp:TextBox ID="tbPhoneNumber1" runat="server" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 164px">
                                                                <asp:Label ID="lbPhonNumber1Type" runat="server">Type</asp:Label><br>
                                                                <asp:DropDownList ID="ddPhoneNumber1Type" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 140px">
                                                                <asp:Label ID="lbPhoneNumber2" runat="server">Phone number 2</asp:Label><br>
                                                                <asp:TextBox ID="tbPhoneNumber2" runat="server" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbPhonenumber2Type" runat="server">Type</asp:Label><br>
                                                                <asp:DropDownList ID="ddPhoneNumber2Type" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 143px">
                                                                <asp:Label ID="lbPhoneNumber3" runat="server">Phone number 3</asp:Label><br>
                                                                <asp:TextBox ID="tbPhoneNumber3" runat="server" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 164px">
                                                                <asp:Label ID="lbPhoneNumber3Type" runat="server">Type</asp:Label><br>
                                                                <asp:DropDownList ID="ddPhoneNumber3Type" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 140px">
                                                                <asp:Label ID="lbPhoneNumber4" runat="server">Phone number 4</asp:Label><br>
                                                                <asp:TextBox ID="tbPhoneNumber4" runat="server" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbPhoneNumber4Type" runat="server">Type</asp:Label><br>
                                                                <asp:DropDownList ID="ddPhoneNumber4Type" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="tblVatNumbers" runat="server">
                                            <table class="grid_table" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblHeaderVatNumbers" runat="server">Vat numbers</asp:Label>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="fields" id="Table1" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <table class="datagrid" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:DataGrid ID="dgVatNumbers" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                    AllowSorting="True" GridLines="None">
                                                                                    <FooterStyle CssClass="grid_footer"></FooterStyle>
                                                                                    <SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
                                                                                    <AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
                                                                                    <ItemStyle CssClass="grid_item"></ItemStyle>
                                                                                    <HeaderStyle CssClass="grid_header"></HeaderStyle>
                                                                                    <Columns>
                                                                                        <asp:BoundColumn DataField="Id" SortExpression="Id" HeaderText="Id">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="TraderName" SortExpression="TraderName" HeaderText="Trader name">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="MTNumber" SortExpression="MTNumber" HeaderText="Vat number">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        
                                                                                        <asp:BoundColumn DataField="Building" SortExpression="Building" HeaderText="Building">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="Address">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Street" SortExpression="Street" HeaderText="Street">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="City" SortExpression="City" HeaderText="City">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="PostCode" SortExpression="PostCode" HeaderText="Post code">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Tel" SortExpression="Tel" HeaderText="Tel">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Fax" SortExpression="Fax" HeaderText="Fax">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Note" SortExpression="Note" HeaderText="Note">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="ShowTextFlag" SortExpression="ShowTextFlag" HeaderText="Show">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                    </Columns>
                                                                                    <PagerStyle CssClass="grid_pager"></PagerStyle>
                                                                                </asp:DataGrid>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="23">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>                                            
                                            </asp:Panel>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="10">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="empty_table" cellspacing="0">
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lbUserUpdated" runat="server" CssClass="confirm_text" Visible="False">The user has been updated</asp:Label><br>
                                                        <asp:Label ID="lblError" runat="server" CssClass="error_text"></asp:Label><br>
                                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="203px" Height="32px">
                                                        </asp:ValidationSummary>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Button ID="btDelete" runat="server" CssClass="cancel_button" CausesValidation="False"
                                                                    Text="Delete" OnClick="btDelete_Click"></asp:Button>
                                                        <asp:Button ID="btCancel" runat="server" CssClass="cancel_button" CausesValidation="False"
                                                            Text="cancel"></asp:Button><asp:Button ID="btnContinue" runat="server" CssClass="gray_button"
                                                                Visible="False" Text="Continue"></asp:Button><asp:Button ID="btnSave" runat="server"
                                                                    CssClass="confirm_button" Text="Save"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                            </table>
                            <!-- Main Body Ends -->
                        </td>
                    </tr>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#000000" colspan="3" height="1">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="2">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <uc1:footer ID="Footer1" runat="server"></uc1:footer>
                <uc1:SecureUserAdmin ID="SecureUserAdmin1" runat="server"></uc1:SecureUserAdmin>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
