using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.auDepartments;
using UserAdmin.Localization;

namespace UserAdmin {
    /// <summary>
    /// Summary description for DepartmentDetails.
    /// </summary>
    public partial class DepartmentDetails : Page {
        private static CultureInfo ci;
        private static ResourceManager rm;
        private readonly uaFactory _factory = new uaFactory();
        // My Variables
//		private bool _isNativeCulture = false;
        private int _departmentID = -1;
        private int _subscriberCIID = -1;

        /// Html Controls
        protected HtmlTable tblUsers;

        protected void Page_Load(object sender, EventArgs e) {
            Page.ID = "118";
            GetCultureInfo();

            try {
                _departmentID = int.Parse(Request["Department"]);
            } catch {}
            try {
                _subscriberCIID = int.Parse(Request["SubscriberCIID"]);
            } catch {
                Server.Transfer("FindSubscribers.aspx");
            }

            if (!IsPostBack) {
                LocalizeText();
                LocalizeDataGrid();
                LoadData();
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dgUsers.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgUsers_ItemCommand);
            this.dgUsers.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCurrencyList_ItemDataBound);
        }

        #endregion

        #region EventHandlers

        private void dgCurrencyList_ItemDataBound(object sender, DataGridItemEventArgs e) {
            var grid = (DataGrid) sender;
            WebDesign.CreateExplanationIcons(grid.Columns, lblUsersIcons, rm, ci);

            if (e.Item.Cells[7].Text.Trim().Equals("False")) {
                e.Item.ForeColor = Color.Gray;
            }
        }

        protected void btnInsert_Click(object sender, EventArgs e) {
            lblMessage.Visible = true;

            var department = new auDepartmentBLLC
                             {
                                 NameNative = txtNameNT.Text,
                                 NameEN = txtNameEN.Text,
                                 StreetNative = txtStreetNT.Text,
                                 StreetEN = txtStreetEN.Text,
                                 Postcode = txtPostCode.Text,
                                 Phone = txtPhone.Text,
                                 Email = txtEmail.Text,
                                 ContactPerson = txtContactPerson.Text,
                                 IsHeadOffice = chbIsHeadOffice.Checked,
                                 IsOpen = chbIsOpen.Checked,
                                 CreditInfoID = _subscriberCIID,
                                 BillingAccount = txtBillingAccount.Text
                             };

            if (_departmentID < 0) // IsInsert
            {
                if (_factory.InsertDepartment(department)) {
                    Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + _subscriberCIID);
                } else {
                    lblMessage.Text = rm.GetString("txtFailedSaveDepartment", ci);
                }
            } else // IsUpdate
            {
                department.DepartmentID = _departmentID;
                if (_factory.UpdateDepartment(department)) {
                    Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + _subscriberCIID);
                } else {
                    lblMessage.Text = rm.GetString("txtFailedSaveDepartment", ci);
                }
            }
        }

        protected void btnNewUser_Click(object sender, EventArgs e) {
            Session["SubscriberMaxUsers"] = _factory.GetSubscriberUserLimit(_subscriberCIID);
            string sUrl = "FindCIUser.aspx?Department=" + _departmentID + "&SubscriberCIID=" + _subscriberCIID;
            Server.Transfer(sUrl);
        }

        private void dgUsers_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Edit")) {
                string creditInfoID = e.Item.Cells[2].Text.Trim();
                string strUserID = e.Item.Cells[1].Text.Trim();
                Server.Transfer(
                    "UserDetails.aspx?CreditInfoID=" + creditInfoID + "&SubscriberCIID=" + _subscriberCIID + "&UserID=" +
                    strUserID);
            }
        }

        protected void IsOpen_CheckedChanged(object sender, EventArgs e) { LoadDataIntoGrid(); }

        #endregion

        #region Other functions

        private static void GetCultureInfo() {
//			string culture = Thread.CurrentThread.CurrentCulture.Name;
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
//			string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
//			if(culture.Equals(nativeCulture))
//				_isNativeCulture = true;
        }

        private void LocalizeText() {
            // Lables
            lblContactPerson.Text = rm.GetString("txtContactPerson", ci);
            lblDepartmentDetailsHeader.Text = rm.GetString("txtDepartments", ci);
            lblEmail.Text = rm.GetString("txtEmail", ci);
            lblIsHeadOffice.Text = rm.GetString("txtHeadOffice", ci);
            lblIsOpen.Text = rm.GetString("txtIsOpen", ci);
            lblNameEN.Text = rm.GetString("txtCompanyNameEN", ci);
            lblNameNT.Text = rm.GetString("txtName", ci);
            lblPhone.Text = rm.GetString("txtPhoneNumber", ci);
            lblPostcode.Text = rm.GetString("txtPostalCode", ci);
            lblStreetEN.Text = rm.GetString("txtAddressEN", ci);
            lblStreetNT.Text = rm.GetString("txtAddress", ci);
            lblUsersHeader.Text = rm.GetString("txtUsers", ci);
            lblBillingAccount.Text = rm.GetString("lblBillingAccount", ci);
            // Buttons
            btnInsert.Text = rm.GetString("txtSave", ci);
            btnNewUser.Text = rm.GetString("txtAddUser", ci);
            // RadioButtons
            rbOpen.Text = rm.GetString("txtOpen", ci);
            rbClosed.Text = rm.GetString("txtClosed", ci);
            rbBoth.Text = rm.GetString("txtBoth", ci);
        }

        private void LocalizeDataGrid() {
            dgUsers.Columns[2].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dgUsers.Columns[3].HeaderText = rm.GetString("txtUserName", ci);
            dgUsers.Columns[4].HeaderText = rm.GetString("txtEmail", ci);
            dgUsers.Columns[5].HeaderText = rm.GetString("txtUserType", ci);
            dgUsers.Columns[6].HeaderText = rm.GetString("txtWatchLimit", ci);
            dgUsers.Columns[7].HeaderText = rm.GetString("txtOpen", ci);
            dgUsers.Columns[8].HeaderText = rm.GetString("txtCreated", ci);
        }

        private void LoadData() {
            tblUsers.Visible = false;
            btnNewUser.Visible = false;

            if (_departmentID > -1) {
                /// Loading Department Info.
                auDepartmentBLLC department = _factory.GetDepartment(_departmentID);
                txtNameNT.Text = department.NameNative;
                txtNameEN.Text = department.NameEN;
                txtStreetNT.Text = department.StreetNative;
                txtStreetEN.Text = department.StreetEN;
                txtPostCode.Text = department.Postcode;
                txtPhone.Text = department.Phone;
                txtEmail.Text = department.Email;
                txtContactPerson.Text = department.ContactPerson;
                txtBillingAccount.Text = department.BillingAccount;
                chbIsHeadOffice.Checked = department.IsHeadOffice;
                chbIsOpen.Checked = department.IsOpen;

                LoadDataIntoGrid();

                if (department.IsOpen) {
                    btnNewUser.Visible = CheckMaxUsers();
                }
            }
        }

        private void LoadDataIntoGrid() {
            // Users DataGrid.
            DataSet mySet;
            if (rbOpen.Checked || rbClosed.Checked) {
                mySet = _factory.GetUsersAsDataSet(_departmentID, rbOpen.Checked);
            } else {
                mySet = _factory.GetUsersAsDataSet(_departmentID);
            }
            dgUsers.DataSource = mySet;
            dgUsers.DataBind();
            Session["dgUsers"] = mySet.Tables[0];

            dgUsers.ShowFooter = dgUsers.Items.Count < 1;

            tblUsers.Visible = true;
        }

        private bool CheckMaxUsers() {
            var nMaxUsers = _factory.GetSubscriberUserLimit(_subscriberCIID);
            if (nMaxUsers == 0) {
                return true;
            }
            var nCurrentUserCount = _factory.GetSubscriberUserCount(_subscriberCIID);
            return nCurrentUserCount < nMaxUsers;
        }

        #endregion
    }
}