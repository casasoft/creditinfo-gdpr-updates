<%@ Page language="c#" Codebehind="FindCIUser.aspx.cs" AutoEventWireup="True" Inherits="UserAdmin.FindDebtor" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>Find user</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta http-equiv="expires" content="-1">
		<meta http-equiv="pragma" content="no-cache">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>
			function checkEnterKey() 
			{    
				if (event.keyCode == 13) 
				{        
					event.cancelBubble = true;
					event.returnValue = false;
					frmFindUser.btSearchDebtor.click(); 
				}
			} 
			function SetFormFocus()
				{
					document.frmFindUser.tbIDNumber.focus();
				}
		</script>
</head>
	<body bgcolor="#ffffff" onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="frmFindUser" name="frmFindUser" action="" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblPageHeader" runat="server" cssclass="HeadMain">Individual search</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td colspan="2" style="WIDTH: 498px">
																		<br>
																		<asp:radiobutton id="rbtnIndividual" runat="server" groupname="CustomerType" checked="True" autopostback="True"
																			cssclass="radio" oncheckedchanged="rbtnIndividual_CheckedChanged"></asp:radiobutton>
																		<asp:image id="imgIndividual" runat="server" imageurl="../img/individual.gif"></asp:image>
																		<asp:radiobutton id="rbtnCompany" runat="server" groupname="CustomerType" autopostback="True" cssclass="radio" oncheckedchanged="rbtnCompany_CheckedChanged"></asp:radiobutton>
																		<asp:image id="imgCompany" runat="server" imageurl="../img/company.gif"></asp:image>
																	</td>
																</tr>
																<tr>
																	<td style="WIDTH: 320px">
																		<asp:label id="lbIDNumber" runat="server" width="150px">IDNumber</asp:label>&nbsp;
<asp:Label id="lblNumberType" runat="server">[NumberType]</asp:Label>
																		<br>
																		<asp:textbox id="tbIDNumber" runat="server"></asp:textbox>&nbsp;
																		<asp:dropdownlist id="ddIDNumberType" runat="server" onselectedindexchanged="ddIDNumberType_SelectedIndexChanged"></asp:dropdownlist>
																	</td>
																	<td style="WIDTH: 170px">
																		<asp:label id="lbDebtorFirstName" runat="server">First name</asp:label>
																		<br>
																		<asp:textbox id="tbDebtorFirstName" runat="server"></asp:textbox>
																	</td>
																	<td style="WIDTH: 170px">
																		<asp:label id="lbDebtorSurName" runat="server">Surname</asp:label>
																		<br>
																		<asp:textbox id="tbDebtorSurName" runat="server"></asp:textbox>
																	</td>
																	<td valign="bottom" align="right">
																		<asp:button id="btSearchDebtor" runat="server" cssclass="search_button" text="search" onclick="btSearchDebtor_Click"></asp:button>
																	</td>
																</tr>
																<tr colspan="3">
																	<td style="WIDTH: 170px">
																		<asp:label id="lbVatNumber" runat="server">Vat number</asp:label>
																		<br>
																		<asp:textbox id="tdVatNumber" runat="server"></asp:textbox>
																	</td>
																	<td style="WIDTH: 170px"></td>
																	<td style="WIDTH: 170px"></td>
																</tr>
																<tr colspan="3">
																	<td height="23" style="WIDTH: 395px" colspan="3"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td height="10"></td>
													</tr>												
													<tr>
														<td align="right">
															<asp:label id="lblDatagridIcons" runat="server"></asp:label>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblUserListHeader" runat="server">[VANTAR]</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dgDebtors" runat="server" width="100%" autogeneratecolumns="False" allowsorting="True"
																			gridlines="None">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;" commandname="Select">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;" commandname="Update">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/new.gif&quot; alt=&quot;New user&quot; border=&quot;0&quot;&gt;" commandname="newuser"></asp:buttoncolumn>
																				<asp:boundcolumn datafield="CreditInfoID" sortexpression="CreditInfoID" headertext="CreditInfoID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameNative" sortexpression="NameNative" headertext="Name">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameEN" sortexpression="NameEN" headertext="NameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="StreetNative" sortexpression="StreetNative" headertext="Street(N)">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="StreetEN" sortexpression="StreetEN" headertext="Street EN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Number" sortexpression="Number" headertext="National ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="CityNative" sortexpression="CityNative" headertext="CityNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="CityEN" sortexpression="CityEN" headertext="CityEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="PostalCode" sortexpression="PostalCode" headertext="PostalCode">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:label id="lblErrorMessage" runat="server" cssclass="error_text" visible="False">Error</asp:label>
														</td>
														<td align="right">
															<asp:button id="btCancel" runat="server" cssclass="cancel_button" text="cancel" onclick="btCancel_Click"></asp:button><asp:button id="btnNew" runat="server" cssclass="confirm_button" text="New" onclick="btnNew_Click"></asp:button>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
						<uc1:secureuseradmin id="SecureUserAdmin1" runat="server"></uc1:secureuseradmin>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
