﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.auSubscribers;
using UserAdmin.BLL.auUsers;
using UserAdmin.Localization;

using Cig.Framework.Base.Configuration;


namespace UserAdmin
{
    /// <summary>
    /// Til þess að þessi síða sé nothæf þarf að skila inn á hana 2 session variables, einni með CreditInfoID og hinni
    /// með nafni.
    /// </summary>
    public partial class SubscriberDetails : Page
    {
        public static CultureInfo ci;
        private static bool isUpdate;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        private int _totalUsers;
        private int CIID = -1;
        private bool nativeCult;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CIID = int.Parse(Request["SubscriberCIID"]);
            }
            catch
            {
                CIID = -1;
            }

            if (CIID == -1)
            {
                Server.Transfer("FindSubscribers.aspx");
            }

            lblError.Visible = false;

            String culture = Thread.CurrentThread.CurrentCulture.Name;

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            String nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture))
            {
                nativeCult = true;
            }

            Page.ID = "116";
            LocalizeText();
            LocalizeGridHeader();
            AddEnterEvent();

            if (!IsPostBack)
            {
                InitInfoForForm();
                GetDataSetThingy();
            }
        }

        private void AddEnterEvent()
        {
            Control frm = FindControl("SubscriberDetails");
            foreach (Control ctrl in frm.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is CheckBox)
                {
                    ((CheckBox)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void LocalizeText()
        {
            // Lables
            lblCreatedLabel.Text = rm.GetString("txtDateCreated", ci);
            lblCreditInfoLabel.Text = rm.GetString("txtCreditInfoID", ci);
            lblMaxUsers.Text = rm.GetString("txtMaxUsers", ci);
            lblUpdatedLabel.Text = rm.GetString("txtDateUpdated", ci);
            lblNameLabel.Text = rm.GetString("txtName", ci);
            lblregisteredLabel.Text = rm.GetString("txtRegisteredBy", ci);
            lblPageHeader.Text = rm.GetString("txtSubscriberDetails", ci);
            lblIsOpen.Text = rm.GetString("txtIsOpen", ci);
            lblError.Text = " - " + rm.GetString("txtRegistrationNotFinished", ci);
            lblDeparmentsHeader.Text = rm.GetString("txtDepartments", ci);
            // Buttons
            btnRegister.Text = rm.GetString("txtSave", ci);
            btnSubscriberFirstUser.Text = rm.GetString("txtMakeSubscriberFirstUser", ci);
            btnDepartmentNew.Text = rm.GetString("txtDeparmentNew", ci);
            // RadioButtons
            rbOpen.Text = rm.GetString("txtOpen", ci);
            rbClosed.Text = rm.GetString("txtClosed", ci);
            rbBoth.Text = rm.GetString("txtBoth", ci);
        }

        private void LocalizeGridHeader()
        {
            if (nativeCult)
            {
                dgDepartments.Columns[3].Visible = true;
                dgDepartments.Columns[4].Visible = false;
            }
            else
            {
                dgDepartments.Columns[3].Visible = false;
                dgDepartments.Columns[4].Visible = true;
            }
            dgDepartments.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dgDepartments.Columns[4].HeaderText = rm.GetString("txtName", ci);
            dgDepartments.Columns[5].HeaderText = rm.GetString("txtContactPerson", ci);
            dgDepartments.Columns[6].HeaderText = rm.GetString("txtPhoneNumber", ci);
            dgDepartments.Columns[7].HeaderText = rm.GetString("txtEmail", ci);
            dgDepartments.Columns[9].HeaderText = rm.GetString("txtUsers", ci);
        }

        private void InitInfoForForm()
        {
            if (CIID <= -1)
            {
                return;
            }
            lblCreditInfoID.Text = CIID.ToString();
            lblName.Text = nativeCult ? myFactory.GetCreditInfoUserNativeName(Convert.ToInt32(lblCreditInfoID.Text)) : myFactory.GetCreditInfoUserENName(Convert.ToInt32(lblCreditInfoID.Text));

            auSubscribersBLLC mySubscriber = myFactory.GetOneSubscriberByCreditInfoIDAsSubscriber(CIID);

            if (mySubscriber == null)
            {
                isUpdate = false;
                lblError.Text = " - " + rm.GetString("txtRegistrationNotFinished", ci);
                lblError.Visible = true;
                //tdUserButton.Visible=false;

                lblCreated.Text = DateTime.Now.ToString();
                lblUpdated.Text = DateTime.Now.ToString();
                lblregisteredBy.Text = myFactory.GetUserName(Convert.ToInt32(Session["UserLoginID"]));
            }
            else
            {
                isUpdate = true;
                lblError.Visible = false;
                //tdUserButton.Visible=true;

                lblCreated.Text = mySubscriber.Created.ToString();
                lblUpdated.Text = mySubscriber.Updated.ToString();
                lblregisteredBy.Text = myFactory.GetUserName(mySubscriber.RegisteredBy);
                chkbxIsOpen.Checked = Convert.ToBoolean(mySubscriber.isOpen);
                txtMaxUsers.Text = mySubscriber.MaxUsers.ToString();
                chkIsPrintBlocked.Checked = mySubscriber.isPrintBlocked;

                var searchFilters = myFactory.GetSearchFilters(CIID, true);
                if (searchFilters == null) //set all to checked if there was no record for search filters
                {
                    chkIsInd.Checked = true;
                    chkIsComp.Checked = true;
                    chkIsIndVat.Checked = true;
                }
                else
                {
                    chkIsInd.Checked = searchFilters.isInd;
                    chkIsComp.Checked = searchFilters.isComp;
                    chkIsIndVat.Checked = searchFilters.isIndVat;

                }
            }
        }

        private void ResetForm()
        {
            if (isUpdate)
            {
                lblError.Visible = false;
                //tdUserButton.Visible=true;

                auSubscribersBLLC mySubscriber;
                if (CIID > -1)
                {
                    lblName.Text = nativeCult ? myFactory.GetCreditInfoUserNativeName(Convert.ToInt32(lblCreditInfoID.Text)) : myFactory.GetCreditInfoUserENName(Convert.ToInt32(lblCreditInfoID.Text));
                    mySubscriber = myFactory.GetOneSubscriberByCreditInfoIDAsSubscriber(CIID);
                    lblCreated.Text = mySubscriber.Created.ToString();
                    lblUpdated.Text = mySubscriber.Updated.ToString();
                    lblregisteredBy.Text = myFactory.GetUserName(mySubscriber.RegisteredBy);
                    chkbxIsOpen.Checked = Convert.ToBoolean(mySubscriber.isOpen);
                    chkIsPrintBlocked.Checked = mySubscriber.isPrintBlocked;
                    txtMaxUsers.Text = mySubscriber.MaxUsers.ToString();
                }
            }
            else
            {
                lblError.Text = " - " + rm.GetString("txtRegistrationNotFinished", ci);
                lblError.Visible = true;
                //tdUserButton.Visible=true;
            }
            GetDataSetThingy();
        }

        private void GetDataSetThingy()
        {
            btnDepartmentNew.Visible = false;
            btnSubscriberFirstUser.Visible = false;

            if (isUpdate)
            {
                DataSet mySet;
                if (rbOpen.Checked || rbClosed.Checked)
                {
                    mySet = myFactory.GetDepartmentsAsDataSet(CIID, rbOpen.Checked);
                }
                else
                {
                    mySet = myFactory.GetDepartmentsAsDataSet(CIID);
                }
                dgDepartments.DataSource = mySet;
                dgDepartments.DataBind();
                Session["dgDepartments"] = mySet.Tables[0];

                if (mySet.Tables[0].Rows.Count > 0)
                {
                    dgDepartments.Columns[1].Visible = CheckMaxUsers();
                }

                if (chkbxIsOpen.Checked)
                {
                    btnDepartmentNew.Visible = true;
                }
            }
        }

        private bool CheckMaxUsers()
        {
            int nMaxUsers = myFactory.GetSubscriberUserLimit(CIID);
            if (nMaxUsers == 0)
            {
                return false;
            }
            int nCurrentUserCount = myFactory.GetSubscriberUserCount(CIID);
            if (nCurrentUserCount >= nMaxUsers)
            {
                return false;
            }

            return true;
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            var mySubscriber = new auSubscribersBLLC();

            var searchFilter = new auSearchFilter()
            {
                isInd = chkIsInd.Checked,
                isComp = chkIsComp.Checked,
                isIndVat = chkIsIndVat.Checked,
                isSubscriber = true,
                filterId = CIID
            };

            if (isUpdate)
            {
                mySubscriber.CreditInfoID = Convert.ToInt32(lblCreditInfoID.Text);
                mySubscriber.isOpen = chkbxIsOpen.Checked.ToString();
                mySubscriber.Updated = DateTime.Now;
                mySubscriber.isPrintBlocked = chkIsPrintBlocked.Checked;
                try
                {
                    mySubscriber.MaxUsers = Convert.ToInt32(txtMaxUsers.Text);
                }
                catch (Exception)
                {
                    mySubscriber.MaxUsers = 0;
                }

                if (myFactory.UpdateSubscriber(mySubscriber))
                {
                    if(!myFactory.ModifySearchFilters(searchFilter, true))
                    {
                        lblError.Text = rm.GetString("txtErrorRecordfilter", ci);
                        lblError.Visible = true;
                    }

                    isUpdate = true;
                    ResetForm();
                }
                else
                {
                    lblError.Text = rm.GetString("txtErrorRegistering", ci);
                    lblError.Visible = true;
                }
            }
            else
            {
                mySubscriber.CreditInfoID = Convert.ToInt32(lblCreditInfoID.Text);
                mySubscriber.RegisteredBy = Convert.ToInt32(Session["UserLoginID"]);
                mySubscriber.isOpen = chkbxIsOpen.Checked.ToString();
                mySubscriber.isPrintBlocked = chkIsPrintBlocked.Checked;
                if (txtMaxUsers.Text != "")
                {
                    mySubscriber.MaxUsers = Convert.ToInt32(txtMaxUsers.Text);
                }
                mySubscriber.Created = DateTime.Now;
                mySubscriber.Updated = DateTime.Now;

                if (myFactory.AddNewSubscriber(mySubscriber))
                {
                    if (!myFactory.ModifySearchFilters(searchFilter, true))
                    {
                        lblError.Text = rm.GetString("txtErrorRecordfilter", ci);
                        lblError.Visible = true;
                    }

                    isUpdate = true;
                    ResetForm();
                }
                else
                {
                    lblError.Text = rm.GetString("txtErrorRegistering", ci);
                    lblError.Visible = true;
                }
            }
        }

        protected void btnSubscriberFirstUser_Click(object sender, EventArgs e) { Server.Transfer("UserDetails.aspx?SetSubscriberFirstUser=true&SubscriberCIID=" + lblCreditInfoID.Text); }

        private void dgDepartments_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals(DataGrid.EditCommandName))
            {
                Server.Transfer(
                    "DepartmentDetails.aspx?Department=" + e.Item.Cells[2].Text + "&SubscriberCIID=" +
                    lblCreditInfoID.Text);
            }
            else if (e.CommandName.Equals("newuser"))
            {
                var nSubscriberID = int.Parse(lblCreditInfoID.Text);
                try
                {
                    if (!string.IsNullOrEmpty(e.Item.Cells[2].Text))
                    {
                        int nDepartmentID = int.Parse(e.Item.Cells[2].Text);
                        var myFactory2 = new uaFactory();
                        Session["SubscriberMaxUsers"] = myFactory2.GetSubscriberUserLimit(nSubscriberID);
                        var sUrl = "FindCIUser.aspx?Department=" + nDepartmentID + "&SubscriberCIID=" + nSubscriberID;
                        Server.Transfer(sUrl);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteToLog("SubscriberDetails.dgDepartments_ItemCommand(..): " + ex.Message, false);
                }
            }
        }

        private void dgDepartments_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            var grid = (DataGrid)sender;
            WebDesign.CreateExplanationIcons(grid.Columns, lblDeparmentsIcons, rm, ci);

            if (e.Item.ItemType.Equals(ListItemType.Footer))
            {
                e.Item.Cells[9].Text = _totalUsers.ToString();
            }
            else if (!e.Item.ItemType.Equals(ListItemType.Footer) && !e.Item.ItemType.Equals(ListItemType.Header))
            {
                if (e.Item.Cells[8].Text.Equals("False"))
                {
                    e.Item.Cells[1].Controls[0].Visible = false;
                    e.Item.ForeColor = Color.Gray;
                    e.Item.Cells[9].Text = "0";
                }
                else
                {
                    if (e.Item.Cells[9].Text.Equals("&nbsp;"))
                    {
                        e.Item.Cells[9].Text = "0";
                    }
                    else
                    {
                        try
                        {
                            _totalUsers += int.Parse(e.Item.Cells[9].Text);
                        }
                        catch { }
                    }
                }
            }
        }

        protected void btnDepartmentNew_Click(object sender, EventArgs e)
        {
            var myFactory3 = new uaFactory();
            Session["SubscriberMaxUsers"] = myFactory3.GetSubscriberUserLimit(CIID);
            Server.Transfer("DepartmentDetails.aspx?SubscriberCIID=" + CIID);
        }

        protected void IsOpen_CheckedChanged(object sender, EventArgs e) { GetDataSetThingy(); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgDepartments.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDepartments_ItemCommand);
            this.dgDepartments.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgDepartments_ItemDataBound);
        }

        #endregion
    }
}