<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Page language="c#" Codebehind="FindUser.aspx.cs" AutoEventWireup="True" Inherits="UserAdmin.FindUser" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>Find user</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta http-equiv="expires" content="-1">
		<meta http-equiv="pragma" content="no-cache">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>
			function checkEnterKey() 
			{    
				if (event.keyCode == 13) 
				{        
					event.cancelBubble = true;
					event.returnValue = false;
					frmFindUserName.btSearch.click(); 
				}
			} 
			function SetFormFocus()
				{
					document.frmFindUserName.txtUserName.focus();
				}
		</script>
</head>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="frmFindUserName" name="frmFindUserName" action="" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td id="blas" colspan="2" runat="server">
									<!-- Main Body -->
									<table width="100%">
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblPageHeader" runat="server"> PageHeader</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td><asp:label id="lblUserName" runat="server">User name</asp:label><br>
																		<asp:textbox id="txtUserName" runat="server"></asp:textbox></td>
																	<td><asp:label id="lblName" runat="server">First name</asp:label><br>
																		<asp:textbox id="txtName" runat="server"></asp:textbox></td>
																	<td><asp:label id="lblIsOpen" runat="server">[IsOpen]</asp:label><br>
																		<asp:radiobutton id="rbOpen" runat="server" text="[Open]" groupname="IsOpen" cssclass="radio"></asp:radiobutton>&nbsp;
																		<asp:radiobutton id="rbClosed" runat="server" text="[Closed]" groupname="IsOpen" cssclass="radio"></asp:radiobutton>&nbsp;
																		<asp:radiobutton id="rbBoth" runat="server" text="[Both]" groupname="IsOpen" checked="True" cssclass="radio"></asp:radiobutton></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblSubscriberName" runat="server">[SubscriberName]</asp:label><br>
																		<asp:textbox id="txtSubscriberName" runat="server"></asp:textbox></td>
																	<td><asp:label id="lblUserEmail" runat="server">Email</asp:label><br>
																		<asp:textbox id="txtUserEmail" runat="server"></asp:textbox></td>
																	<td><asp:label id="lblUserType" runat="server">[UserType]</asp:label><br>
																		<asp:dropdownlist id="ddlUserType" runat="server"></asp:dropdownlist></td>
																	<td valign="bottom"><asp:button id="btSearch" runat="server" cssclass="search_button" text="search" onclick="btSearch_Click"></asp:button></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr id="outerGridTableInfo" runat="server">
											<td align="right">
												<div>
													<asp:label id="lblDatagridIcons" runat="server"></asp:label></div>
											</td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<th>
															<asp:label id="lblGridHeader" runat="server">GridHeader</asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td><asp:datagrid id="dgUsers" runat="server" width="100%" autogeneratecolumns="False" allowsorting="True"
																			gridlines="None">
<footerstyle cssclass="grid_footer">
</FooterStyle>

<selecteditemstyle cssclass="grid_selecteditem">
</SelectedItemStyle>

<alternatingitemstyle cssclass="grid_alternatingitem">
</AlternatingItemStyle>

<itemstyle cssclass="grid_item">
</ItemStyle>

<headerstyle cssclass="grid_header">
</HeaderStyle>

<columns>
<asp:ButtonColumn Text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;" CommandName="Edit">
<itemstyle cssclass="leftpadding">
</ItemStyle>
</asp:ButtonColumn>
<asp:BoundColumn Visible="False" DataField="CreditInfoID" SortExpression="CreditInfoID" HeaderText="CreditInfoID">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="UserName" SortExpression="UserName" HeaderText="UserName">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="NameNative" SortExpression="NameNative" HeaderText="NameNative">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="NameEN" SortExpression="NameEN" HeaderText="NameEN">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="StreetNative" SortExpression="StreetNative" HeaderText="Street(N)">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="StreetEN" SortExpression="StreetEN" HeaderText="Street EN">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="Email" SortExpression="Email" HeaderText="Email">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="IsOpen" SortExpression="IsOpen" HeaderText="IsOpen">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="SubscriberID" SortExpression="SubscriberID" HeaderText="SubscriberID">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="SubscriberNameNative" SortExpression="SubscriberNameNative" HeaderText="Subscriber">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="SubscriberNameEN" SortExpression="SubscriberNameEN" HeaderText="Subscriber">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="id"></asp:BoundColumn>
<asp:BoundColumn DataField="DepartmentNT" SortExpression="DepartmentNT" HeaderText="Department">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="DepartmentEN" SortExpression="DepartmentEN" HeaderText="Department">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
</Columns>

<pagerstyle cssclass="grid_pager">
</PagerStyle>
																		</asp:datagrid></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left"><asp:label id="lblErrorMessage" runat="server" cssclass="error_text" width="100%" visible="False">Error</asp:label></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- Main Body --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer><uc1:secureuseradmin id="SecureUserAdmin1" runat="server"></uc1:secureuseradmin></td>
				</tr>
			</table>
		</form>
	</body>
</html>
