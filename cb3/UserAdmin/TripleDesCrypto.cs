/* 
 * $Header: /CIG_System/CreditInfoGroup/UserAdmin/DAL/TripleDesCrypto.cs 2     27.02.06 9:35 Gudjon $
 * $Log: /CIG_System/CreditInfoGroup/UserAdmin/DAL/TripleDesCrypto.cs $ 
 * 
 * 2     27.02.06 9:35 Gudjon
 * Support for application center added.
 */
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace UserAdmin.DAL
{
	/// <summary>
	/// Class that handles TripleDes encryption/decryption
	/// <para>Copyright (C) 2006 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
	/// </summary>
	public partial class TripleDesCrypto
	{
		private static byte[] key = {60,21,217,24,4,60,58,1,208,134,95,162,108,198,34,52,57,222,248,97,48,164,24,168};
		private static byte[] iv = {22,153,20,175,188,5,44,80};

		/// <summary>
		/// Default constructor.
		/// </summary>
		public TripleDesCrypto()
		{
		}

		/// <summary>
		/// Encrypts given text
		/// </summary>
		/// <param name="dataToEncrypt">Text to encrypt</param>
		/// <returns>Encrypted text</returns>
		public static string Encrypt(string dataToEncrypt)
		{   
			SymmetricAlgorithm mCryptoService = new TripleDESCryptoServiceProvider();

			//Create byte arrays to hold original, encrypted, and decrypted data.

			byte[] plainByte = Encoding.ASCII.GetBytes(dataToEncrypt);

			// Set private key
			mCryptoService.Key = key;
			mCryptoService.IV =iv;
   
			// Encryptor object
			ICryptoTransform cryptoTransform = mCryptoService.CreateEncryptor();
   
			// Memory stream object
			MemoryStream ms = new MemoryStream();

			// Crpto stream object
			CryptoStream cs = new CryptoStream(ms, cryptoTransform, 
				CryptoStreamMode.Write);

			// Write encrypted byte to memory stream
			cs.Write(plainByte, 0, plainByte.Length);
			cs.FlushFinalBlock();

			// Get the encrypted byte length
			byte[] cryptoByte = ms.ToArray();

			return Convert.ToBase64String(cryptoByte);
  
		}

		/// <summary>
		/// Decrypts given text
		/// </summary>
		/// <param name="dataToDecrypt">Text to decrypt</param>
		/// <returns>Decrypted text</returns>
		public static string Decrypt(string dataToDecrypt)
		{   
			SymmetricAlgorithm mCryptoService = new TripleDESCryptoServiceProvider();

			//Create byte arrays to hold original, encrypted, and decrypted data.
			byte[] plainByte = Convert.FromBase64String(dataToDecrypt);

			// Set private key
			mCryptoService.Key = key;
			mCryptoService.IV =iv;
   
			// Encryptor object
			ICryptoTransform cryptoTransform = mCryptoService.CreateDecryptor();
   
			// Memory stream object
			MemoryStream ms = new MemoryStream();

			// Crpto stream object
			CryptoStream cs = new CryptoStream(ms, cryptoTransform, 
				CryptoStreamMode.Write);

			// Write encrypted byte to memory stream
			cs.Write(plainByte, 0, plainByte.Length);
			cs.FlushFinalBlock();

			// Get the encrypted byte length
			byte[] cryptoByte = ms.ToArray();

			return Encoding.ASCII.GetString(cryptoByte);
  
		}
	}
}
