using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using Telerik.WebControls;
using UserAdmin.BLL;
using UserAdmin.BLL.auDepartments;
using UserAdmin.BLL.auProducts;
using UserAdmin.BLL.auUsers;
using UserAdmin.BLL.npEmployee;
using UserAdmin.Localization;

using Cig.Framework.Base.Configuration;

namespace UserAdmin {
    /// <summary>
    /// Summary description for UserDetails.
    /// </summary>
    public partial class UserDetails : Page {
        public static CultureInfo ci;
        private static bool isEmp;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        private int _departmentID = -1;
        private int creditInfoID = -1;
        private bool nativeCult;
        private bool PasswordChange;
        private int subscriberCIID = -1;
        private bool subscriberToUser;
        private int userID = -1;
        //public user_controls.CIGTree tree = new UserAdmin.user_controls.CIGTree();
        public int SubscriberCIID { get { return subscriberCIID; } }

        protected void Page_Load(object sender, EventArgs e) {
            /*tree.AddNodeDiv("NodeText", user_controls.CIGTree.mrNodeType.mrParent, "1", null);
			tree.BuildTree();*/

            //this.Page.ID = "113";
            //Page.ID = Request.QueryString["action"];
            if (Context.User.IsInRole("115")) {
                Page.ID = "115";
                internalTree.Visible = true;
                lblInternal.Visible = true;
                tdInternal.Visible = true;
                tdInternal.Style.Clear();
                tdInternal.Style.Add("WIDTH", "50%");
                externalTree.Visible = true;
                lblExternal.Visible = true;
            } else if (Context.User.IsInRole("114")) {
                Page.ID = "114";
                internalTree.Visible = true;
                lblInternal.Visible = true;
                tdInternal.Visible = true;
                tdInternal.Style.Clear();
                tdInternal.Style.Add("WIDTH", "50%");
                externalTree.Visible = false;
                lblExternal.Visible = false;
            } else if (Context.User.IsInRole("113")) {
                Page.ID = "113";
                internalTree.Visible = false;
                lblInternal.Visible = false;
                tdInternal.Visible = false;
                tdInternal.Style.Clear();
                tdInternal.Style.Add("WIDTH", "0px");
                externalTree.Visible = true;
                lblExternal.Visible = true;
            } else {
                Page.ID = "113";
                internalTree.Visible = false;
                lblInternal.Visible = false;
                tdInternal.Visible = false;
                tdInternal.Style.Clear();
                tdInternal.Style.Add("WIDTH", "0px");
                externalTree.Visible = false;
                lblExternal.Visible = false;
            }

            try {
                subscriberCIID = int.Parse(Request["SubscriberCIID"]);
            } catch {
                subscriberCIID = -1;
            }
            try {
                creditInfoID = int.Parse(Request["CreditInfoID"]);
            } catch {
                creditInfoID = -1;
            }
            try {
                _departmentID = int.Parse(Request["Department"]);
            } catch {}
            try {
                userID = int.Parse(Request["UserID"]);
            } catch {
                userID = -1;
            }
            try {
                subscriberToUser = bool.Parse(Request["SetSubscriberFirstUser"]);
            } catch {
                subscriberToUser = false;
            }

            if (subscriberCIID == -1) {
                Server.Transfer("FindSubscribers.aspx");
            }

            var culture = Thread.CurrentThread.CurrentCulture.Name;

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            var nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");

            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            LocalizeText();

            if (IsPostBack) {
                return;
            }
            if (subscriberToUser) {
                lblCreditInfoID.Text = subscriberCIID.ToString();
                NewUser();
            } else {
                //int userID = myFactory.GetUserID(creditInfoID, subscriberCIID);
                if (userID == -1) //New user for this subscriber
                {
                    lblCreditInfoID.Text = creditInfoID.ToString();
                    NewUser();
                } else {
                    txtUsername.Text = myFactory.GetUserName(userID);
                    lblCreditInfoID.Text = creditInfoID.ToString();
                    //myFactory.GetCreditInfoID(Request.QueryString["sysusername"].ToString()).ToString();
                    OldUser(lblCreditInfoID.Text, txtUsername.Text);

                    var searchFilters = myFactory.GetSearchFilters(userID, false);
                    if (searchFilters == null) //set all to checked if there was no record for search filters
                    {
                        chkIsInd.Checked = true;
                        chkIsComp.Checked = true;
                        chkIsIndVat.Checked = true;
                    }
                    else
                    {
                        chkIsInd.Checked = searchFilters.isInd;
                        chkIsComp.Checked = searchFilters.isComp;
                        chkIsIndVat.Checked = searchFilters.isIndVat;

                    }
                }
            }
            GetProducts();
        }

        private void LocalizeText() {
            lblPageHeader.Text = rm.GetString("txtUserDetails", ci);
            lblSubscriberHeader.Text = rm.GetString("txtSubscriber", ci);
            lblDepartmentHeader.Text = rm.GetString("txtDepartment", ci);
            lblCreatedLabel.Text = rm.GetString("txtCreated", ci);
            lblCreditInfoLabel.Text = rm.GetString("txtCreditInfoID", ci);
            lblIsOpen.Text = rm.GetString("txtStatus", ci);
            lblNameLabel.Text = rm.GetString("txtName", ci);
            lblOpenUntil.Text = rm.GetString("txtOpenUntil", ci);
            lblEmail.Text = rm.GetString("txtEmail", ci);
            lblPassword.Text = rm.GetString("txtPassword", ci);
            lblregisteredLabel.Text = rm.GetString("txtRegisteredBy", ci);
            lblFinishRegistration.Text = rm.GetString("txtRegistrationNotFinished", ci);
            lblUpdatedLabel.Text = rm.GetString("txtUpdated", ci);
            lblUserName.Text = rm.GetString("txtUserName", ci);
            lblUserRights.Text = rm.GetString("txtUserRights", ci);
            lblUserSettings.Text = rm.GetString("txtUserSettings", ci);
            lblUserType.Text = rm.GetString("txtUserType", ci);
            lblWatchLimit.Text = rm.GetString("txtWatchLimit", ci);
            lblWebService.Text = rm.GetString("txtWebService", ci);
            btnBack.Text = rm.GetString("txtBack", ci);
            btnRegister.Text = rm.GetString("txtSave", ci);
            ddlUserType.Items[0].Text = rm.GetString("txtRegularUser", ci);
            ddlUserType.Items[1].Text = rm.GetString("txtEmployee", ci);

            lblInternal.Text = rm.GetString("txtInternal", ci);
            lblExternal.Text = rm.GetString("txtExternal", ci);

            rfPassword.ErrorMessage = rm.GetString("txtPasswordMustBeSet", ci);
            rfUserName.ErrorMessage = rm.GetString("txtUsernameMustBeSet", ci);

            rbOpen.Text = rm.GetString("txtOpen", ci);
            rbClosed.Text = rm.GetString("txtClosed", ci);

            rbLock.Text = rm.GetString("txtLock", ci);
            rbUnlock.Text = rm.GetString("txtUnlock", ci);
        }

        private void NewUser() {
            lblFinishRegistration.Visible = true;
            lblName.Text = nativeCult ? myFactory.GetCreditInfoUserNativeName(Convert.ToInt32(lblCreditInfoID.Text)) : myFactory.GetCreditInfoUserENName(Convert.ToInt32(lblCreditInfoID.Text));
            rfPassword.Enabled = true;
            lblCreated.Text = DateTime.Now.ToString();
            lblUpdated.Text = DateTime.Now.ToString();
            lblregisteredBy.Text = myFactory.GetUserName(Convert.ToInt32(Session["UserLoginID"]));
            txtOpenUntil.Text = Convert.ToString(DateTime.Now + TimeSpan.FromDays(30000));

            LoadSubscriberAndDepartmentData(subscriberCIID, _departmentID);
        }

        private void OldUser(string CIID, string userID) {
            lblFinishRegistration.Visible = false;
            lblName.Text = nativeCult ? myFactory.GetCreditInfoUserNativeName(Convert.ToInt32(lblCreditInfoID.Text)) : myFactory.GetCreditInfoUserENName(Convert.ToInt32(lblCreditInfoID.Text));

            try {
                auUsersBLLC myUser = myFactory.GetSpecificUser(userID);

                LoadSubscriberAndDepartmentData(myUser.SubscriberID, myUser.DepartmentID);

                lblCreated.Text = myUser.Created.ToString();
                lblUpdated.Text = myUser.Updated.ToString();
                lblregisteredBy.Text = myFactory.GetUserName(myUser.RegisteredBy);

                txtUsername.Text = myUser.UserName;
                txtUsername.ReadOnly = true;

                txtPassword.Text = "*****";
                rfPassword.Enabled = false;
                chkbxWebServices.Checked = Convert.ToBoolean(myUser.HasWebServices);
                txtCreditWatchLimit.Text = myUser.CntCreditWatch.ToString();
                txtEmail.Text = myUser.Email;
                txtGroupCode.Text = myUser.GroupCode;

                // Fyrir UserType �arf a� finna texta sem "matchar" og setja "Selected Item"...
                ddlUserType.SelectedItem.Selected = false;
                for (int index = 0; index < ddlUserType.Items.Count; index++) {
                    if (ddlUserType.Items[index].Text == myUser.UserType) {
                        ddlUserType.Items[index].Selected = true;
                    }
                }

                // Ef um starfsmann er a� r��a...
                if (ddlUserType.SelectedItem.Value == "1") {
                    isEmp = true;
                }

                if (Convert.ToBoolean(myUser.isOpen)) {
                    rbOpen.Checked = Convert.ToBoolean(myUser.isOpen);
                } else {
                    rbClosed.Checked = true;
                }


                if (Convert.ToBoolean(myUser.lockUserAccount))
                {
                    rbLock.Checked = Convert.ToBoolean(myUser.lockUserAccount);
                }
                else
                {
                    rbUnlock.Checked = true;
                }

                if (!CheckMaxUsers() && rbClosed.Checked) {
                    rbOpen.Enabled = false;
                    rbClosed.Enabled = false;
                }

                txtOpenUntil.Text = myUser.OpenUntil.ToShortDateString();
            } catch (Exception e) {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on UserDetails.aspx caught, message is : " + e.Message, true);
            }
        }

        private void LoadSubscriberAndDepartmentData(int SubscriberCIID, int DepartmentID) {
            auDepartmentBLLC department = myFactory.GetDepartment(DepartmentID);
            if (nativeCult) {
                lblSubscriber.Text = subscriberCIID + " - " + myFactory.GetCreditInfoUserNativeName(SubscriberCIID);
                lblDepartment.Text = department.NameNative;
            } else {
                lblSubscriber.Text = subscriberCIID + " - " + myFactory.GetCreditInfoUserENName(SubscriberCIID);
                lblDepartment.Text = department.NameEN;
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                const string strGroups = "";
                var myUser = new auUsersBLLC
                             {
                                 CreditInfoID = Convert.ToInt32(lblCreditInfoID.Text),
                                 SubscriberID = subscriberCIID,
                                 DepartmentID = _departmentID,
                                 UserName = txtUsername.Text,
                                 Salt = uaFactory.CreateSalt(5)
                             };

                myUser.PasswordHash = uaFactory.CreatePasswordHash(txtPassword.Text, myUser.Salt);
                myUser.Email = txtEmail.Text;
                myUser.Groups = strGroups;
                myUser.CntCreditWatch = Convert.ToInt32(txtCreditWatchLimit.Text);
                myUser.HasWebServices = chkbxWebServices.Checked.ToString();
                myUser.UserType = ddlUserType.SelectedItem.ToString();
                myUser.isOpen = rbOpen.Checked.ToString();
                myUser.OpenUntil = Convert.ToDateTime(txtOpenUntil.Text);
                myUser.RegisteredBy = Convert.ToInt32(Session["UserLoginID"]);
                myUser.Created = DateTime.Now;
                myUser.Updated = DateTime.Now;
                myUser.GroupCode = txtGroupCode.Text.Trim();

                var searchFilter = new auSearchFilter()
                {
                    isInd = chkIsInd.Checked,
                    isComp = chkIsComp.Checked,
                    isIndVat = chkIsIndVat.Checked,
                    isSubscriber = false,
                    filterId = myFactory.GetUserID(txtUsername.Text)
                };

                // Athuga hvort um n�ja skr�ningu er a� r��a e�a hvort veri� er a� uppf�ra notanda...
                //if (Request.QueryString["new"] == "true")
                if (lblFinishRegistration.Visible) {
                    bool transfer = false;
                    try {
                        myFactory.StoreUserAccountDetails(myUser);
                        myFactory.UpdateCreditInfoUserTableWithInfo(myUser.Email, 1, myUser.CreditInfoID);
                        myFactory.ModifySearchFilters(searchFilter, false);
                        transfer = true;
                        Session["newuser"] = null;
                        Session["sysuserid"] = null;
                        if (myUser.UserType == "Employee") {
                            myFactory.DeleteEmployeeDetails(myUser.UserName);
                            var myEmp = new npEmployeeBLLC
                                        {
                                            CreditInfoID = myUser.CreditInfoID,
                                            Initials = myUser.UserName,
                                            LastUpdated = DateTime.Now
                                        };
                            myFactory.StoreEmployeeDetails(myEmp);
                        }

                        WriteUserProducts();
                    } catch (Exception Err) {
                        // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                        Logger.WriteToLog(
                            "Exception on UserDetails.aspx caught, message is : " + Err.Message + " " + Err.GetType(),
                            true);
                        transfer = false;

                        string myScript = "<script language=Javascript>alert('" +
                                          rm.GetString("txtUnableToRegisterUser", ci) + "');</script>";
                        Page.RegisterClientScriptBlock("alert", myScript);
                        return;
                    }
                    isEmp = false;

                    if (transfer) {
                        Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + subscriberCIID);
                    }
                } else {
                    try {
                        myFactory.UpdateUserAccountDetails(myUser);
                        myFactory.UpdateCreditInfoUserTableWithInfo(myUser.Email, 1, myUser.CreditInfoID);
                        myFactory.ModifySearchFilters(searchFilter, true);
                        if (PasswordChange) {
                            myFactory.UpdateUserPassword(myUser.PasswordHash, myUser.Salt, myUser.UserName);
                        }

                        if (rbLock.Checked || rbUnlock.Checked)
                        {
                            myFactory.AccountLock(myUser.UserName, rbLock.Checked);

                        }

                        WriteUserProducts();
                    } catch (Exception Err) {
                        // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                        Logger.WriteToLog("Exception on UserDetails.aspx caught, message is : " + Err.Message, true);
                    }
                    isEmp = false;
                    Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + subscriberCIID);
                }
            }
            WriteUserProducts();
        }

        protected void txtPassword_TextChanged(object sender, EventArgs e) { PasswordChange = true; }

        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e) {
            if (isEmp) {
                if (ddlUserType.SelectedItem.Value != "1") {
                    myFactory.DeleteEmployeeDetails(txtUsername.Text);
                }
            } else {
                if (ddlUserType.SelectedItem.Value == "1") {
                    myFactory.DeleteEmployeeDetails(txtUsername.Text);
                    var myEmp = new npEmployeeBLLC
                                {
                                    CreditInfoID = Convert.ToInt32(lblCreditInfoID.Text),
                                    Initials = txtUsername.Text,
                                    LastUpdated = DateTime.Now
                                };
                    myFactory.StoreEmployeeDetails(myEmp);
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e) { Server.Transfer(Request.UrlReferrer.PathAndQuery); }

        private void GetProducts() {
            DataSet mySet = myFactory.GetAllTheOpenProducts(false);
            DataView myView = mySet.Tables[0].DefaultView;

            myView.RowFilter = "IsBaseProduct = 'True' and IsInner = 'True'";

            IDictionary internalNodeTable = new Hashtable(myView.Count);
            //GenerateMenuTree(myView, internalNode, internalNodeTable, internalTreeView);
            //internalNodeTable = new Hashtable(myView.Count);
            GenerateTreeView(myView, internalNodeTable, internalTree);

            myView.RowFilter = "IsBaseProduct = 'True' and IsInner = 'False'";

            IDictionary externalNodeTable = new Hashtable(myView.Count);
            //GenerateMenuTree(myView, externalNode, externalNodeTable, externalTreeview);
            //externalNodeTable = new Hashtable(myView.Count);
            GenerateTreeView(myView, externalNodeTable, externalTree);

            if (internalTree.Nodes.Count == 0) {
                internalTree.Visible = false;
                lblInternal.Visible = false;
                tdInternal.Visible = false;
            }
            if (externalTree.Nodes.Count == 0) {
                externalTree.Visible = false;
                lblExternal.Visible = false;
            }

            if (txtUsername.Text != "") {
                int myUserID = myFactory.GetUserID(txtUsername.Text);
                mySet = myFactory.ReadUserProducts(myUserID);

                for (int i = 0; i < mySet.Tables[0].Rows.Count; i++) {
                    string key = mySet.Tables[0].Rows[i]["ProductID"].ToString();
                    if (internalNodeTable.Contains(key)) {
                        ((RadTreeNode) internalNodeTable[key]).Checked = true;
                    }

                    if (externalNodeTable.Contains(key)) {
                        ((RadTreeNode) externalNodeTable[key]).Checked = true;
                    }
                }
            }
        }

        private void GenerateTreeView(DataView myView, IDictionary nodeTable, RadTreeView tree) {
            for (int i = 0; i < myView.Count; i++) {
                var node = new RadTreeNode
                                   {
                                       ID = myView[i]["productID"].ToString(),
                                       Text =
                                           (nativeCult
                                                ? myView[i]["ProductNameNative"].ToString()
                                                : myView[i]["ProductNameEN"].ToString())
                                   };
                //node.Expanded = true;

                nodeTable.Add(myView[i]["productID"].ToString(), node);

                if (myView[i]["ParentID"] == DBNull.Value || Convert.ToInt32(myView[i]["ParentID"]) == 0) {
                    tree.AddNode(node);
                } else {
                    var parent = (RadTreeNode) nodeTable[myView[i]["ParentID"].ToString()];
                    if (parent != null) {
                        parent.AddNode(node);
                    } else {
                        tree.AddNode(node);
                    }
                }
            }
        }

        private static void GetCheckedNodeList(IList nodeList, RadTreeNodeCollection nodeCollection) {
            foreach (RadTreeNode node in nodeCollection) {
                if (node.Checked) {
                    nodeList.Add(node);
                }

                if (node.Nodes != null) {
                    GetCheckedNodeList(nodeList, node.Nodes);
                }
            }
        }

        private void WriteUserProducts() {
            IList list = new ArrayList();
            GetCheckedNodeList(list, internalTree.Nodes);
            GetCheckedNodeList(list, externalTree.Nodes);

            int myUserID = myFactory.GetUserID(txtUsername.Text);
            var myProductArray = new auProductsBLLC[list.Count];

            int counter = 0;

            foreach (RadTreeNode node in list) {
                myProductArray[counter] = new auProductsBLLC {UserID = myUserID, ProductID = Convert.ToInt32(node.ID)};
                counter++;
            }

            // Skrifa r�ttindi ni�ur � grunn...
            myFactory.WriteUserProducts(myProductArray, myUserID);
        }

        private bool CheckMaxUsers() {
            int nMaxUsers = myFactory.GetSubscriberUserLimit(SubscriberCIID);
            if (nMaxUsers == 0) {
                return true;
            }
            int nCurrentUserCount = myFactory.GetSubscriberUserCount(SubscriberCIID);
            return nCurrentUserCount < nMaxUsers;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
        }

        #endregion
    }
}