using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserAdmin.BLL;
using UserAdmin.Localization;

namespace UserAdmin {
    /// <summary>
    /// Summary description for Stats.
    /// </summary>
    public partial class Stats : Page {
        // B�ta vi� v�sun � uaFactory - Gert um lei� og instance ver�ur til af �essum klasa.
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();

        protected void Page_Load(object sender, EventArgs e) {
            Page.ID = "107";

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();

            // Fyrir Enter lykilinn...
            AddEnterEvent();
            if (!IsPostBack) {
                // Veri� a� setja dagsetningar � textaboxin. Fr� byrjun til loka �essa m�na�ar og fr� byrjun �rs.
                DateTime dt = DateTime.Now;
                dt = dt.AddDays(1 - dt.Day);
                txtFromDate.Text = dt.ToShortDateString();
                DateTime dt2 = dt.AddDays(DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month) - 1);
                txtToDate.Text = dt2.ToShortDateString();
                dt = dt.AddMonths(1 - dt.Month);
                txtBeginningDate.Text = dt.ToShortDateString();
            }
        }

        private void LocalizeText() {
            lblBeginningPeriod.Text = rm.GetString("txtBeginningOfRegistrationPeriod", ci);
            lblBeginToDateName.Text = rm.GetString("txtBeginDate", ci) + " - " + rm.GetString("txtToDate", ci);
            lblDifferenceName.Text = rm.GetString("txtDifference", ci);
            lblfromDate.Text = rm.GetString("txtFromDate", ci);
            lblFromDateToDateName.Text = rm.GetString("txtFromDate", ci) + " - " + rm.GetString("txtToDate", ci);
            lblNumberOfRegistrations.Text = rm.GetString("txtNumberOfRegistrations", ci);
            lblPageHeader.Text = rm.GetString("txtStatistic", ci);
            lblToDate.Text = rm.GetString("txtToDate", ci);
            lblTotalNumOfCompanies.Text = rm.GetString("txtNumOfCompanies", ci);
            lblTotalNumOfIndi.Text = rm.GetString("txtNumOfIndi", ci);
            lblQuery.Text = rm.GetString("txtQuery", ci);
            lblResult.Text = rm.GetString("txtResultForSelectedPeriod", ci);
            lblDescription.Text = rm.GetString("txtDescription", ci);
            btnExecute.Text = rm.GetString("txtExecute", ci);
            btnPrint.Text = rm.GetString("txtPrint", ci);
        }

        // Prenta
        protected void btnPrint_Click(object sender, EventArgs e) {
            const string myScript = "<script language=Javascript>window.print();</script>";
#pragma warning disable 612,618
            Page.RegisterClientScriptBlock("alert", myScript);
#pragma warning restore 612,618
        }

        protected void btnExecute_Click(object sender, EventArgs e) {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            DateTime BeginningDate = DateTime.Now;

            // Setja textastrengi, au�veldara a� vinna me� ��...
            if (txtFromDate.Text != "") {
                FromDate = DateTime.Parse(txtFromDate.Text);
            }

            if (txtToDate.Text != "") {
                ToDate = DateTime.Parse(txtToDate.Text);
                ToDate = ToDate.AddHours(23);
                ToDate = ToDate.AddMinutes(59);
                ToDate = ToDate.AddSeconds(59);
            }

            if (txtBeginningDate.Text != "") {
                BeginningDate = DateTime.Parse(txtBeginningDate.Text);
            }

            // Claims
            lblRegBeginning.Text = (myFactory.GetNumberOfRegistrationsAddedForPeriod(BeginningDate, ToDate)).ToString();
            lblFromToReg.Text = (myFactory.GetNumberOfRegistrationsAddedForPeriod(FromDate, ToDate)).ToString();
            lblDiffReg.Text =
                Convert.ToString(Convert.ToInt32(lblRegBeginning.Text) - Convert.ToInt32(lblFromToReg.Text));

            // Companies (type = 1)
            lblCompBeginning.Text =
                (myFactory.GetNumberOfRegistrationsAddedForPeriodAndType(BeginningDate, ToDate, 1)).ToString();
            lblFromToComp.Text =
                (myFactory.GetNumberOfRegistrationsAddedForPeriodAndType(FromDate, ToDate, 1)).ToString();
            lblDiffComp.Text =
                Convert.ToString(Convert.ToInt32(lblCompBeginning.Text) - Convert.ToInt32(lblFromToComp.Text));

            // Individuals (type =2)
            lblIndiBeginning.Text =
                (myFactory.GetNumberOfRegistrationsAddedForPeriodAndType(BeginningDate, ToDate, 2)).ToString();
            lblFromToIndi.Text =
                (myFactory.GetNumberOfRegistrationsAddedForPeriodAndType(FromDate, ToDate, 2)).ToString();
            lblDiffIndi.Text =
                Convert.ToString(Convert.ToInt32(lblIndiBeginning.Text) - Convert.ToInt32(lblFromToIndi.Text));

            lblBeginToDateName.Text = Convert.ToDateTime(BeginningDate).ToShortDateString() + " - " +
                                      Convert.ToDateTime(ToDate).ToShortDateString();
            lblFromDateToDateName.Text = Convert.ToDateTime(FromDate).ToShortDateString() + " - " +
                                         Convert.ToDateTime(ToDate).ToShortDateString();
        }

        private void AddEnterEvent() {
            var frm = FindControl("frmStatsMain");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { }

        #endregion
    }
}