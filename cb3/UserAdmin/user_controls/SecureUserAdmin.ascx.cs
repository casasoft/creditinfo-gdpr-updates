using System;
using System.Web.UI;

namespace UserAdmin.user_controls {
    /// <summary>
    ///		Summary description for SecureUserAdmin.
    /// </summary>
    public partial class SecureUserAdmin : UserControl {
        protected void Page_Load(object sender, EventArgs e) {
            if (!Context.User.IsInRole("100")) {
                Server.Transfer("../NoAuthorization.aspx");
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { }

        #endregion
    }
}