using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserAdmin.Localization;

namespace UserAdmin.user_controls {
    /// <summary>
    ///		Summary description for header.
    /// </summary>
    public partial class header : UserControl {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected HyperLink hlUserSummary;

        protected void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();

            if (Context.User.IsInRole("Credit Watch") || Context.User.IsInRole("Administration")) {
                hlCreditWatch.Visible = true;
            } else {
                hlCreditWatch.Visible = false;
            }

            if (Context.User.IsInRole("Negative Payments") || Context.User.IsInRole("Administration")) {
                hlNegativePayments.Visible = true;
            } else {
                hlNegativePayments.Visible = false;
            }

            hlUserAdmin.Visible = Context.User.IsInRole("Administration");

            if (Context.User.IsInRole("Claim Search") || Context.User.IsInRole("Administration")) {
                hlSearchClaims.Visible = true;
            } else {
                hlSearchClaims.Visible = false;
            }
        }

        protected void imbtEN_Click(object sender, ImageClickEventArgs e) {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            Session["culture"] = culture;
            // forcing reload to get the proper Culture in global.asax
            Response.AddHeader("Refresh", "0");
        }

        protected void imgbtCYP_Click(object sender, ImageClickEventArgs e) {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("el-GR", true);
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            Session["culture"] = culture;
            // forcing reload to get the proper Culture in global.asax
            Response.AddHeader("Refresh", "0");
        }

        private void LocalizeText() {
            hlNegativePayments.Text = rm.GetString("txtNegativePayments", ci);
            hlCreditWatch.Text = rm.GetString("txtCreditWatch", ci);
            hlSearchClaims.Text = rm.GetString("txtSearchClaims", ci);
            hlUserAdmin.Text = rm.GetString("txtUserAdmin", ci);
            lnkLogout.Text = rm.GetString("txtLogOut", ci);
        }

        protected void lnkLogout_Click(object sender, EventArgs e) {
            FormsAuthentication.SignOut();
            Response.AddHeader("Refresh", "0");
            Server.Transfer("../Default.aspx");
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { }

        #endregion
    }
}