<%@ Control Language="c#" AutoEventWireup="True" Codebehind="header.ascx.cs" Inherits="UserAdmin.user_controls.header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<HTML>
	<HEAD>
		<title>head</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="http://www.lt.is/cy_styles.css" type="text/css" rel="stylesheet">
		<style type="text/css"> br.pageEnd {page-break-after: always}
		</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<table id="Table3" borderColor="#0000ff" cellSpacing="0" cellPadding="0" width="600" align="center"
			bgColor="#ffffff" border="0">
			<TBODY>
				<tr>
					<td vAlign="top" align="left" height="50">
						<IMG src="/CreditInfoGroup_lt/img/logo_cyprus.png">
					</td>
				</tr>
				<tr>
					<td vAlign="top" align="center" height="98%">
						<table id="Table1" width="600" align="center" cellSpacing="0" cellPadding="0">
							<tr>
								<td align="center">
									<asp:HyperLink id="hlNegativePayments" runat="server" NavigateUrl="../../NPayments/ClaimOverView.aspx">
									Negative payments</asp:HyperLink></td>
								<TD align="center">
									<asp:HyperLink id="hlCreditWatch" NavigateUrl="../../CreditWatch/cwOverView.aspx" runat="server">
									Credit Watch</asp:HyperLink></TD>
								<td align="center">
									<asp:HyperLink id="hlUserAdmin" runat="server" NavigateUrl="../TypeSpecify.aspx">
									User Admin</asp:HyperLink></td>
								<TD align="center">
									<asp:hyperlink id="hlSearchClaims" NavigateUrl="../../CreditWatch/cwClaimSearch.aspx" runat="server">Search Claims</asp:hyperlink></TD>
							</tr>
							<TR>
								<TD class="dark-row" align="center" height="33" rowSpan="2">
									<asp:HyperLink id="hlUserControl" NavigateUrl="../TypeSpecify.aspx" runat="server" CssClass="subject">User control</asp:HyperLink></TD>
								<TD class="dark-row" align="center" height="33" rowSpan="2">
									<asp:HyperLink id="hlUsageSummary" NavigateUrl="../ulUserSummary.aspx" runat="server" CssClass="subject">Usage summary</asp:HyperLink></TD>
								<TD class="dark-row" align="center" height="33" rowSpan="2"></TD>
								<TD class="dark-row" align="center" height="33" rowSpan="2"></TD>
							</TR>
						</table>
						<table class="list" id="Table2" cellSpacing="0" cellPadding="2" width="600" align="center">
							<TBODY>
								<tr class="dark-row">
									<td align="center" style="WIDTH: 36px">
										<asp:imagebutton id="imbtEN" runat="server" CausesValidation="False" Height="20px" ImageUrl="../../CreditInfoGroup/img/flag_uk.png" onclick="imbtEN_Click"></asp:imagebutton></td>
									<td align="left">
										<asp:imagebutton id="imgbtCYP" runat="server" CausesValidation="False" Height="20px" ImageUrl="/CreditInfoGroup_lt/CreditInfoGroup/img/flag_cy.png" onclick="imgbtCYP_Click"></asp:imagebutton>&nbsp;
										<asp:LinkButton id="lnkLogout" runat="server" onclick="lnkLogout_Click">Logout</asp:LinkButton></td>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
			</TBODY>
		</table>
	</body>
</HTML>
