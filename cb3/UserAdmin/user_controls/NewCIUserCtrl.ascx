<%@ Control Language="c#" AutoEventWireup="True" Codebehind="NewCIUserCtrl.ascx.cs" Inherits="UserAdmin.user_controls.NewCIUserCtrl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<SCRIPT language="javascript"> 
		function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						document.UserHandlingForm.NewCIUserCtrl_btnSave.click(); 
					}
				} 
		
		function SetFormFocus()
		{
			document.UserHandlingForm.NewCIUserCtrl_tbDebtorFirstName.focus();
		}
</SCRIPT>
<body bgColor="#ffffff" onload="SetFormFocus()" MS_POSITIONING="GridLayout">
	<form id="newuser" name="newuser" action="" method="post">
		<table cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td style="BACKGROUND-IMAGE: url(../img/mainback.gif)"><table cellSpacing="0" cellPadding="0" width="600" align="center" border="0">
						<tr>
							<!--	<td class="mainShadeLeft" style="WIDTH: 568px" vAlign="top" align="right" width="568"><IMG alt="" src="../img/spacer.gif" width="6"></td> -->
							<td>
								<table cellSpacing="0" cellPadding="0" width="919" align="center" border="0">
									<!--	<tr>
										<td class="PageTitle" bgColor="#951e16" colSpan="1">
										</td>
									</tr> -->
									<tr>
										<!--	<td style="WIDTH: 568px" vAlign="top" width="568" bgColor="#ced0c5"></td> -->
										<!-- <td class="betweensides"></td> -->
										<td style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px"
											vAlign="top" width="550" bgColor="#ffffff">
											<table id="FormTable" width="100%">
												<tr>
													<td>
														<!--<P><asp:label id="lbNewIndividual" runat="server" CssClass="HeadMain">New Individual</asp:label></P>
														<P> -->
														<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbPersonInformation" runat="server" CssClass="HeadLists">Person Info</asp:label></TD>
																<TD></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"></TD>
																<TD></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbDebtorCIID" runat="server">CreditInfoID</asp:label></TD>
																<TD><asp:Label id="lblIsSearchable" runat="server">[IsSearchable]</asp:Label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbDebtorCIID" runat="server" ReadOnly="True" EnableViewState="False" Width="140px"></asp:textbox></TD>
																<TD><asp:RadioButton id="rbtIsSearchableTrue" CssClass="radio" runat="server" Text="[Yes]" GroupName="IsSearchable" Checked="True"></asp:RadioButton>&nbsp;<asp:RadioButton id="rbtIsSearchableFalse" CssClass="radio" runat="server" Text="[No]" GroupName="IsSearchable"></asp:RadioButton></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbDebtorFirstName" runat="server">First name</asp:label></TD>
																<TD><asp:label id="lbFirstNameEN" runat="server">First name(EN)</asp:label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbDebtorFirstName" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox><asp:requiredfieldvalidator id="rfvDebtorFirstName" runat="server" ErrorMessage="Value missing!" ControlToValidate="tbDebtorFirstName">*</asp:requiredfieldvalidator></TD>
																<TD><asp:textbox id="tbFirstNameEN" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbDebtorSurName" runat="server">Surname</asp:label></TD>
																<TD><asp:label id="lbSurnameEN" runat="server">Surname(EN)</asp:label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbDebtorSurName" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox><asp:requiredfieldvalidator id="rfvDebtorSurName" runat="server" ErrorMessage="Value missing!" ControlToValidate="tbDebtorSurName"
																		Enabled="False">*</asp:requiredfieldvalidator></TD>
																<TD><asp:textbox id="tbSurnameEN" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbEmail" runat="server">Email</asp:label></TD>
																<TD></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbEmail" runat="server" Width="225px" MaxLength="30"></asp:textbox></TD>
																<TD></TD>
															</tr>
															<tr>
																<td style="HEIGHT: 2px" colSpan="4">
																	<hr noShade SIZE="1">
																</td>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"></TD>
																<TD></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbAddress1" runat="server">Address1</asp:label></TD>
																<TD><asp:label id="lbAddress1EN" runat="server">Address1(EN)</asp:label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbAddress1" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox><asp:requiredfieldvalidator id="rfvAddress" runat="server" ErrorMessage="RequiredFieldValidator"
																		ControlToValidate="tbAddress1">*</asp:requiredfieldvalidator></TD>
																<TD><asp:textbox id="tbAddress1EN" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbAddress1Info" runat="server">Address1 Info</asp:label></TD>
																<TD><asp:label id="lbAddress1InfoEN" runat="server">Address1 Info(EN)</asp:label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbAddress1Info" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox></TD>
																<TD><asp:textbox id="tbAddress1InfoEN" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbPostalCode" runat="server">PostalCode1</asp:label></TD>
																<TD><asp:label id="lbPostBox" runat="server">PostBox1</asp:label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbPostalCode1" runat="server" EnableViewState="False" Width="140px" MaxLength="50"></asp:textbox></TD>
																<TD><asp:textbox id="tbPostBox1" runat="server" EnableViewState="False" Width="140px" MaxLength="50"></asp:textbox></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbCity1" runat="server">City1</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<asp:label id="lblCitySearch" runat="server">Search</asp:label></TD>
																<TD></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:dropdownlist id="ddCity1" runat="server" Width="128px"></asp:dropdownlist>
																	<asp:RequiredFieldValidator id="rfvCity" runat="server" ControlToValidate="ddCity1" ErrorMessage="Value missing!">*</asp:RequiredFieldValidator>
																	<asp:textbox id="txtCitySearch" runat="server" Width="40px" MaxLength="10"></asp:textbox>
																	<asp:Button id="btnCitySearch" runat="server" Width="26px" Text="..." CausesValidation="False"
																		CssClass="popup" onclick="btnCitySearch_Click"></asp:Button></TD>
																<TD></TD>
															</tr>
															<tr>
																<td style="HEIGHT: 2px" colSpan="4">
																	<hr noShade SIZE="1">
																</td>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"></TD>
																<TD></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbAddress2" runat="server">Address2</asp:label></TD>
																<TD><asp:label id="lbAddress2EN" runat="server">Address2(EN)</asp:label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbAddress2" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox></TD>
																<TD><asp:textbox id="tbAddress2EN" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbAddress2Info" runat="server">Address2 Info</asp:label></TD>
																<TD><asp:label id="lbAddress2InfoEN" runat="server">Address2 Info(EN)</asp:label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbAddress2Info" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox></TD>
																<TD><asp:textbox id="tbAddress2InfoEN" runat="server" EnableViewState="False" Width="225px" MaxLength="50"></asp:textbox></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="ltPostalCode2" runat="server">PostalCode2</asp:label></TD>
																<TD><asp:label id="lbPostBox2" runat="server">PostBox2</asp:label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:textbox id="tbPostalCode2" runat="server" EnableViewState="False" Width="140px" MaxLength="50"></asp:textbox></TD>
																<TD><asp:textbox id="tbPostBox2" runat="server" EnableViewState="False" Width="140px" MaxLength="50"></asp:textbox></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbCity2" runat="server">City2</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<asp:label id="lblCity2Search" runat="server">Search</asp:label></TD>
																<TD></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:dropdownlist id="ddCity2" runat="server" Width="128px"></asp:dropdownlist>
																	<asp:RequiredFieldValidator id="rfvCity2" runat="server" ControlToValidate="ddCity2" ErrorMessage="Value missing!"
																		Enabled="False">*</asp:RequiredFieldValidator>
																	<asp:textbox id="txtCity2Search" runat="server" Width="40px" MaxLength="10"></asp:textbox>
																	<asp:Button id="btnCity2Search" runat="server" Width="26px" Text="..." CausesValidation="False"
																		CssClass="popup" onclick="btnCity2Search_Click"></asp:Button></TD>
																<TD></TD>
															</tr>
															<tr>
																<td style="HEIGHT: 2px" colSpan="4">
																	<hr noShade SIZE="1">
																</td>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:label id="lbProfession" runat="server">Profession</asp:label></TD>
																<TD><asp:label id="lbEducation" runat="server">Education</asp:label></TD>
															</tr>
															<tr>
																<TD style="WIDTH: 310px"><asp:dropdownlist id="ddProfession" runat="server" Width="225px"></asp:dropdownlist></TD>
																<TD><asp:dropdownlist id="ddEducation" runat="server" Width="225px"></asp:dropdownlist></TD>
															</tr>
														</TABLE>
														<P></P>
														<P>
															<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
																<tr>
																	<TD style="WIDTH: 143px"><asp:label id="lbIDNumber2" runat="server">IDNumber 1</asp:label></TD>
																	<TD style="WIDTH: 164px"><asp:label id="lbIDNumberType" runat="server">Type</asp:label></TD>
																	<TD style="WIDTH: 140px"><asp:label id="lbDebtorIDNumber3" runat="server">IDNumber 2</asp:label></TD>
																	<TD><asp:label id="lbIDNumberType2" runat="server">Type</asp:label></TD>
																</tr>
																<tr>
																	<TD style="WIDTH: 143px"><asp:textbox id="tbIDNumber1" runat="server" EnableViewState="False" Width="140px"></asp:textbox></TD>
																	<TD style="WIDTH: 164px"><asp:dropdownlist id="ddNumberType1" runat="server"></asp:dropdownlist></TD>
																	<TD style="WIDTH: 140px"><asp:textbox id="tbIDNumber2" runat="server" EnableViewState="False" Width="140px"></asp:textbox></TD>
																	<TD><asp:dropdownlist id="ddNumberType2" runat="server"></asp:dropdownlist></TD>
																</tr>
																<tr>
																	<TD style="WIDTH: 143px"></TD>
																	<TD style="WIDTH: 164px"></TD>
																	<TD style="WIDTH: 140px"></TD>
																	<TD></TD>
																</tr>
																<tr>
																	<TD style="WIDTH: 143px"><asp:label id="lbPhoneNumber1" runat="server">Phone number 1</asp:label></TD>
																	<TD style="WIDTH: 164px"><asp:label id="lbPhonNumber1Type" runat="server">Type</asp:label></TD>
																	<TD style="WIDTH: 140px"><asp:label id="lbPhoneNumber2" runat="server">Phone number 2</asp:label></TD>
																	<TD><asp:label id="lbPhonenumber2Type" runat="server">Type</asp:label></TD>
																</tr>
																<tr>
																	<TD style="WIDTH: 143px"><asp:textbox id="tbPhoneNumber1" runat="server" EnableViewState="False" Width="140px"></asp:textbox></TD>
																	<TD style="WIDTH: 164px"><asp:dropdownlist id="ddPhoneNumber1Type" runat="server"></asp:dropdownlist></TD>
																	<TD style="WIDTH: 140px"><asp:textbox id="tbPhoneNumber2" runat="server" EnableViewState="False" Width="140px"></asp:textbox></TD>
																	<TD><asp:dropdownlist id="ddPhoneNumber2Type" runat="server"></asp:dropdownlist></TD>
																</tr>
																<tr>
																	<TD style="WIDTH: 143px"><asp:label id="lbPhoneNumber3" runat="server">Phone number 3</asp:label></TD>
																	<TD style="WIDTH: 164px"><asp:label id="lbPhoneNumber3Type" runat="server">Type</asp:label></TD>
																	<TD style="WIDTH: 140px"><asp:label id="lbPhoneNumber4" runat="server">Phone number 4</asp:label></TD>
																	<TD><asp:label id="lbPhoneNumber4Type" runat="server">Type</asp:label></TD>
																</tr>
																<tr>
																	<TD style="WIDTH: 143px"><asp:textbox id="tbPhoneNumber3" runat="server" EnableViewState="False" Width="140px"></asp:textbox></TD>
																	<TD style="WIDTH: 164px"><asp:dropdownlist id="ddPhoneNumber3Type" runat="server"></asp:dropdownlist></TD>
																	<TD style="WIDTH: 140px"><asp:textbox id="tbPhoneNumber4" runat="server" EnableViewState="False" Width="140px"></asp:textbox></TD>
																	<TD><asp:dropdownlist id="ddPhoneNumber4Type" runat="server"></asp:dropdownlist></TD>
																</tr>
															</TABLE>
														</P>
														<P>
															<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
																<tr>
																	<TD>
																		<P><asp:label id="lbUserUpdated" runat="server" Visible="False" ForeColor="Red">The user has been updated</asp:label>&nbsp;&nbsp;&nbsp;
																			<asp:label id="lblError" runat="server" ForeColor="Red"></asp:label></P>
																	</TD>
																</tr>
																<tr>
																	<TD style="HEIGHT: 15px">
																		<HR noShade SIZE="1">
																		&nbsp;</TD>
																</tr>
																<!--	<tr>
																	<TD style="HEIGHT: 15px" bgColor="#951e16"></TD>
																</tr>
																<tr>
																	<TD style="HEIGHT: 15px"></TD>
																</tr>
															-->
																<tr>
																	<TD style="HEIGHT: 15px">
																		<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<tr>
																				<TD>
																					<P>
																						<asp:validationsummary id="ValidationSummary1" runat="server" Width="203px" Height="32px"></asp:validationsummary></P>
																				</TD>
																				<TD vAlign="top" align="right">
																					<TABLE>
																						<tr>
																							<TD vAlign="top">
																								<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px">
																									<asp:button id="btnSave" runat="server" CssClass="confirm_button" Text="Save" onclick="btnSave_Click"></asp:button></DIV>
																							</TD>
																							<TD vAlign="top">
																								<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px">
																									<asp:button id="btCancel" runat="server" CssClass="cancel_button" Text="cancel" CausesValidation="False" onclick="btCancel_Click"></asp:button></DIV>
																							</TD>
																						</tr>
																					</TABLE>
																					<asp:Literal id="Literal1" runat="server"></asp:Literal>
																				</TD>
																			</tr>
																		</TABLE>
																	</TD>
																</tr>
															</TABLE>
														</P>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<!--	<tr>
										<TD style="WIDTH: 568px" width="568" bgColor="#e2e2e2" height="1"><IMG height="1" src="" width="153"></TD>
										<TD><IMG height="1" src="" width="10"></TD>
										<TD width="766" bgColor="#e2e2e2" height="1"><IMG height="1" src="" width="758"></TD>
									</tr>
								-->
								</table>
							</td>
							<!-- <TD class="mainShadeRight"><IMG alt="" src="img/spacer.gif" width="6"></TD> -->
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
