<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NewCIUserCCtrl.ascx.cs" Inherits="UserAdmin.user_controls.NewCIUserCCtrl_ascx" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						document.UserHandlingForm.NewCIUserCCtrl_btnSave.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.UserHandlingForm.NewCIUserCCtrl_tbCompanyNameNative.focus();
				}

</SCRIPT>
<body bgColor="#ffffff" onload="SetFormFocus()" MS_POSITIONING="GridLayout">
	<form id="newuser" name="newuser" action="" method="post">
		<table cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td style="BACKGROUND-IMAGE: url(../img/mainback.gif)"><table cellSpacing="0" cellPadding="0" width="600" align="center" border="0">
						<tr>
							<!--	<td class="mainShadeLeft" style="WIDTH: 568px" vAlign="top" align="right" width="568"><IMG alt="" src="../img/spacer.gif" width="6"></td> -->
							<td>
								<table cellSpacing="0" cellPadding="0" width="919" align="center" border="0">
									<!--	<tr>
										<td class="PageTitle" bgColor="#951e16" colSpan="3">
										</td>
									</tr>
									-->
									<tr>
										<!--	<td class="betweensides"></td> -->
										<td style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px"
											vAlign="top" width="500" bgColor="#ffffff">
											<table width="100%">
												<tr>
													<td>
														<!--	<P><asp:label id="lbNewCompany" runat="server" CssClass="HeadMain">New Company</asp:label></P>
														<P>
													-->
														<TABLE id="FormTable" cellSpacing="1" cellPadding="1" width="100%" border="0">
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbCompanyInformation" CssClass="HeadLists" runat="server">Company Info</asp:label></TD>
																<TD></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"></TD>
																<TD></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbDebtorCIID" runat="server">CreditInfoID</asp:label></TD>
																<TD><asp:Label id="lblIsSearchable" runat="server">[IsSearchable]</asp:Label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 310px"><asp:textbox id="tbCompanyCIID" CssClass="rammi" runat="server" Width="140px" ReadOnly="True"
																		EnableViewState="False"></asp:textbox></TD>
																<TD><asp:RadioButton id="rbtIsSearchableTrue" runat="server" CssClass="radio" Text="[Yes]" Checked="True" GroupName="IsSearchable"></asp:RadioButton>&nbsp;<asp:RadioButton id="rbtIsSearchableFalse" runat="server" CssClass="radio" Text="[No]" GroupName="IsSearchable"></asp:RadioButton></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbCompanyName" runat="server">Name</asp:label></TD>
																<TD><asp:label id="lbCompanyNameEN" runat="server"> Name(EN)</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:textbox id="tbCompanyNameNative" CssClass="rammi" runat="server" Width="225px" EnableViewState="False"></asp:textbox><asp:requiredfieldvalidator id="reqValCompNameNative" runat="server" ControlToValidate="tbCompanyNameNative"
																		ErrorMessage="Value missing!">*</asp:requiredfieldvalidator></TD>
																<TD><asp:textbox id="tbCompanyNameEN" CssClass="rammi" runat="server" Width="225px" EnableViewState="False"></asp:textbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbEstablished" runat="server">Established</asp:label></TD>
																<TD><asp:label id="lblRegistered" runat="server">Registered</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:textbox id="tbCompanyEstablished" CssClass="rammi" runat="server" Width="176px" EnableViewState="False"></asp:textbox>&nbsp;<INPUT onclick="PopupPicker('NewCIUserCCtrl_tbCompanyEstablished', 250, 250);" type="button"
																		value="..." class="popup"><asp:customvalidator id="cvCompanyEstablished" CssClass="rammi" runat="server" ControlToValidate="tbCompanyEstablished"
																		ErrorMessage="Date input incorrect!">*</asp:customvalidator><asp:requiredfieldvalidator id="reqValCompEstablish" runat="server" ControlToValidate="tbCompanyEstablished"
																		ErrorMessage="Value missing!">*</asp:requiredfieldvalidator></TD>
																<TD><asp:textbox id="tbRegistered" CssClass="rammi" runat="server" Width="176px" EnableViewState="False"></asp:textbox>&nbsp;<INPUT onclick="PopupPicker('NewCIUserCCtrl_tbRegistered', 250, 250);" type="button" value="..."
																		class="popup"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px">&nbsp;<asp:label id="lbLastContacted" runat="server">Last Contacted</asp:label></TD>
																<TD></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:textbox id="tbCompanyLContacted" CssClass="rammi" runat="server" Width="176px" EnableViewState="False"></asp:textbox><INPUT onclick="PopupPicker('NewCIUserCCtrl_tbCompanyLContacted', 250, 250);" type="button"
																		value="..." class="popup">
																	<asp:customvalidator id="cvLastContacted" CssClass="rammi" runat="server" ControlToValidate="tbCompanyLContacted"
																		ErrorMessage="Date inpur incorrect!">*</asp:customvalidator></TD>
																<TD></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbCompFunction" runat="server">Company function</asp:label></TD>
																<TD><asp:label id="lbOrg_status" runat="server">Org status</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px; HEIGHT: 13px"><asp:dropdownlist id="ddCompanyNace" CssClass="rammi" runat="server" Width="225px"></asp:dropdownlist></TD>
																<TD style="HEIGHT: 13px"><asp:dropdownlist id="ddOrgStatus" runat="server" Width="225px"></asp:dropdownlist><asp:requiredfieldvalidator id="reqValOrgStatus" runat="server" ControlToValidate="ddOrgStatus" ErrorMessage="Value missing!">*</asp:requiredfieldvalidator></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbEmail" runat="server">Email</asp:label></TD>
																<TD><asp:label id="lbCompURL" runat="server">URL</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px; HEIGHT: 21px"><asp:textbox id="tbEmail" CssClass="rammi" runat="server" Width="225px"></asp:textbox></TD>
																<TD style="HEIGHT: 21px"><asp:textbox id="tbCompanyURL" CssClass="rammi" runat="server" Width="225px" EnableViewState="False"></asp:textbox></TD>
															</TR>
															<TR>
																<td style="HEIGHT: 2px" colSpan="4">
																	<hr noShade SIZE="1">
																</td>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"></TD>
																<TD></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbAddress1" runat="server">Address1</asp:label></TD>
																<TD><asp:label id="lbAddress1EN" runat="server">Address1(EN)</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:textbox id="tbAddress1Native" CssClass="rammi" runat="server" Width="225px"></asp:textbox></TD>
																<TD><asp:textbox id="tbAddress1EN" CssClass="rammi" runat="server" Width="225px"></asp:textbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbAddress1Info" runat="server">Address1 Info</asp:label></TD>
																<TD><asp:label id="lbAddr1InfoEN" runat="server">Address1 Info(EN)</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:textbox id="tbAddress1InfoN" CssClass="rammi" runat="server" Width="225px"></asp:textbox></TD>
																<TD><asp:textbox id="tbAddress1InfoEN" CssClass="rammi" runat="server" Width="225px" EnableViewState="False"></asp:textbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbPostalCode" runat="server">PostalCode1</asp:label></TD>
																<TD><asp:label id="lbPostBox" runat="server">PostBox1</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:textbox id="tbPostalCode1" CssClass="rammi" runat="server" Width="140px"></asp:textbox></TD>
																<TD><asp:textbox id="tbPostBox1" CssClass="rammi" runat="server" Width="140px"></asp:textbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbCity1" runat="server">City1</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
																	&nbsp;
																	<asp:label id="lblCitySearch" runat="server">Search</asp:label></TD>
																<TD></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:dropdownlist id="ddCity1" CssClass="rammi" runat="server" Width="128px"></asp:dropdownlist><asp:requiredfieldvalidator id="rfvCity" runat="server" ControlToValidate="ddCity1" ErrorMessage="Value missing!">*</asp:requiredfieldvalidator><asp:textbox id="txtCitySearch" runat="server" Width="40px" MaxLength="10"></asp:textbox><asp:button id="btnCitySearch" runat="server" Width="26px" CausesValidation="False" Text="..."
																		CssClass="popup"></asp:button></TD>
																<TD></TD>
															</TR>
															<TR>
																<td style="HEIGHT: 29px" colSpan="4">
																	<hr noShade SIZE="1">
																</td>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"></TD>
																<TD></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbAddress2" runat="server">Address2</asp:label></TD>
																<TD><asp:label id="lbAddress2EN" runat="server">Address2 (EN)</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:textbox id="tbAddress2Native" CssClass="rammi" runat="server" Width="225px" EnableViewState="False"></asp:textbox></TD>
																<TD><asp:textbox id="tbAddress2EN" CssClass="rammi" runat="server" Width="225px"></asp:textbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbAddress2Info" runat="server">Address2 Info</asp:label></TD>
																<TD><asp:label id="lbAddress2InfoEN" runat="server">Address2 Info (EN)</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:textbox id="tbAddress2InfoN" CssClass="rammi" runat="server" Width="225px"></asp:textbox></TD>
																<TD><asp:textbox id="tbAddress2InfoEN" CssClass="rammi" runat="server" Width="225px"></asp:textbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbPostalCode2" runat="server">Postal Code2</asp:label></TD>
																<TD><asp:label id="lbPostBox2" runat="server">PostBox2</asp:label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:textbox id="tbPostalCode2" CssClass="rammi" runat="server" Width="140px"></asp:textbox></TD>
																<TD><asp:textbox id="tbPostBox2" CssClass="rammi" runat="server" Width="140px"></asp:textbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:label id="lbCity2" runat="server">City2</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<asp:label id="lblCity2Search" runat="server">Search</asp:label></TD>
																<TD></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 311px"><asp:dropdownlist id="ddCity2" CssClass="rammi" runat="server" Width="128px"></asp:dropdownlist><asp:requiredfieldvalidator id="rfvCity2" runat="server" ControlToValidate="ddCity2" ErrorMessage="Value missing!"
																		Enabled="False">*</asp:requiredfieldvalidator><asp:textbox id="txtCity2Search" runat="server" Width="40px" MaxLength="10"></asp:textbox><asp:button id="btnCity2Search" runat="server" Width="26px" CausesValidation="False" Text="..."
																		CssClass="popup"></asp:button></TD>
																<TD></TD>
															</TR>
															<TR>
																<td style="HEIGHT: 2px" colSpan="4">
																	<hr noShade SIZE="1">
																</td>
															</TR>
														</TABLE>
														<P></P>
														<P>
															<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
																<TR>
																	<TD style="WIDTH: 141px"><asp:label id="lbIDNumber1" runat="server">IDNumber 1</asp:label></TD>
																	<TD style="WIDTH: 166px"><asp:label id="lbType1" runat="server">Type</asp:label></TD>
																	<TD style="WIDTH: 143px"><asp:label id="lbIDNumber2" runat="server">IDNumber 2</asp:label></TD>
																	<TD><asp:label id="lbType2" runat="server">Type</asp:label></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 141px"><asp:textbox id="tbIDNumber1" CssClass="rammi" runat="server" Width="140px"></asp:textbox></TD>
																	<TD style="WIDTH: 166px"><asp:dropdownlist id="ddIDNumberType1" CssClass="rammi" runat="server"></asp:dropdownlist></TD>
																	<TD style="WIDTH: 143px"><asp:textbox id="tbIDNumber2" CssClass="rammi" runat="server" Width="140px"></asp:textbox></TD>
																	<TD><asp:dropdownlist id="ddIDNumberType2" CssClass="rammi" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 141px"></TD>
																	<TD style="WIDTH: 166px"></TD>
																	<TD style="WIDTH: 143px"></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 141px"><asp:label id="lbPhoneNumber" runat="server">Phone number</asp:label></TD>
																	<TD style="WIDTH: 166px"></TD>
																	<TD style="WIDTH: 143px"><asp:label id="lbFaxNumber" runat="server">Fax number</asp:label></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 141px"><asp:textbox id="tbPhoneNumber1" CssClass="rammi" runat="server" Width="140px"></asp:textbox></TD>
																	<TD style="WIDTH: 166px"></TD>
																	<TD style="WIDTH: 143px"><asp:textbox id="tbPhoneNumber2" CssClass="rammi" runat="server" Width="140px"></asp:textbox></TD>
																	<TD></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 141px" colSpan="6"><asp:label id="lblError" runat="server" Width="400px" ForeColor="Red"></asp:label></TD>
																</TR>
															</TABLE>
														</P>
														<p>
															<table id="Table1" borderColor="#0000ff" height="100%" cellSpacing="0" cellPadding="0"
																width="100%" align="center" bgColor="#ffffff" border="0">
																<tr>
																	<td vAlign="top" align="center">
																		<table id="Table2" cellSpacing="0" cellPadding="2" width="100%">
																			<TR>
																				<TD style="HEIGHT: 32px" colSpan="4" height="32">
																					<HR noShade SIZE="1">
																				</TD>
																			</TR>
																			<!--	<TR>
																				<TD style="HEIGHT: 15px" bgColor="#951e16" colSpan="4" height="20"></TD>
																			</TR>
																		-->
																			<!--	<TR>
																				<TD style="HEIGHT: 15px" colSpan="4" height="32"></TD>
																			</TR>
																		-->
																			<TR>
																				<TD style="WIDTH: 194px"><asp:validationsummary id="ValidationSummary1" CssClass="rammi" runat="server" Width="168px"></asp:validationsummary></TD>
																				<td vAlign="top" align="right">
																					<table cellPadding="0" border="0">
																						<tr>
																							<td>
																								<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnSave" CssClass="confirm_button" runat="server" Text="Save"></asp:button></div>
																							</td>
																							<td>
																								<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btCancel" CssClass="cancel_button" runat="server" CausesValidation="False" Text="cancel"></asp:button></div>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</TR>
																			<!--<TR class="input">
																				<TD style="WIDTH: 199px; HEIGHT: 16px" colSpan="2"></TD>
																				<TD style="HEIGHT: 16px" colSpan="2"></TD>
																			</TR>
																			--></table>
																	</td>
																</tr>
															</table>
														</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<!--<TR>
										<TD style="WIDTH: 568px" width="568" bgColor="#e2e2e2" height="1"><IMG height="1" src="" width="153"></TD>
										<TD><img height="1" src="" width="10"></TD>
										<td width="766" bgColor="#e2e2e2" height="1"><img height="1" src="" width="758"></td>
									</TR>
								--></table>
							</td>
							<!--	<td class="mainShadeRight"><IMG alt="" src="img/spacer.gif" width="6"></td> --></tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
</body>
