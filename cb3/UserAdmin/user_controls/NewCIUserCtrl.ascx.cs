using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using UserAdmin.Localization;

using Cig.Framework.Base.Configuration;

namespace UserAdmin.user_controls {
    /// <summary>
    ///		Summary description for NewCIUserCtrl.
    /// </summary>
    public partial class NewCIUserCtrl : UserControl {
        // added
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        protected int creditInfoID = -1;
        private bool EN;
        protected bool isCreatingSubscriber;
        protected bool isUpdatingUser;
        protected string nationalID = "";
        protected bool newUser;
        public string redirectPage = "";
        protected int subscriberCIID = -1;
        protected bool update;

        protected void Page_Load(object sender, EventArgs e) {
            if (Request["SubscriberCIID"] != null) {
                subscriberCIID = int.Parse(Request["SubscriberCIID"]);
            } else {
                subscriberCIID = -1;
            }

            if (Request["CreditInfoID"] != null) {
                creditInfoID = int.Parse(Request["CreditInfoID"]);
            } else {
                creditInfoID = -1;
            }

            update = Request["Update"] != null && bool.Parse(Request["Update"]);

            isUpdatingUser = Request["UpdateUser"] != null && bool.Parse(Request["UpdateUser"]);

            nationalID = Request["NationalID"] ?? "";

            isCreatingSubscriber = Request["CreateSubscriber"] != null && bool.Parse(Request["CreateSubscriber"]);

            if (subscriberCIID == -1 && !isCreatingSubscriber && !isUpdatingUser) {
                newUser = true;
            }

            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            lblError.Visible = false;
            lbUserUpdated.Visible = false;
            AddEnterEvent();
            Page.ID = "111";

            if (!IsPostBack) {
                InitDDBoxes();
                initControls(update);
            }
            LocalizeText();
        }

        private void initControls(bool update) {
            if (!newUser) {
                if (update) {
                    if (creditInfoID != -1) {
                        Indivitual myIndi = myFactory.GetIndivitual(creditInfoID);
                        tbDebtorCIID.Text = creditInfoID.ToString();
                        tbDebtorSurName.Text = myIndi.SurNameNative;
                        tbSurnameEN.Text = myIndi.SurNameEN;
                        tbEmail.Text = myIndi.Email;
                        tbDebtorFirstName.Text = myIndi.FirstNameNative;
                        tbFirstNameEN.Text = myIndi.FirstNameEN;
                        if (myIndi.ProfessionID != 0) {
                            ddProfession.SelectedValue = myIndi.ProfessionID.ToString();
                        } else 
                            // stilla h�r � default N/A gildi
                            if (myIndi.EducationID != 0) {
                                ddEducation.SelectedValue = myIndi.EducationID.ToString();
                            } else {
                                // stilla h�r � default N/A gildi
                                initMultiItems(myIndi);
                            }
                    }
                } else {
                    if (nationalID != "") {
                        //Load user data from national registry
                        if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True") {
                            var myIndi = myFactory.GetIndivitualFromNationalRegistry(nationalID);
                            tbIDNumber1.Text = myIndi.NationalID;
                            ddNumberType1.SelectedIndex = 0;

                            tbDebtorFirstName.Text = myIndi.FirstNameNative;
                            tbDebtorSurName.Text = myIndi.SurNameNative;
                            if (myIndi.Address.Count > 0) {
                                var address = (Address) myIndi.Address[0];
                                tbAddress1.Text = address.StreetNative;
                                tbPostalCode1.Text = address.PostalCode;
                            }
                        }
                    }
                }
            }
        }

        private void initMultiItems(CreditInfoUser myIndi) {
            var arrAdd = myIndi.Address;
            var arrIDNum = myIndi.IDNumbers;
            var arrPNum = myIndi.Number;
            var frm = FindControl("UserHandlingForm");
            for (int i = 1; i <= arrAdd.Count; i++) {
                var myAddr = (Address) arrAdd[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbAddress" + i) {
                            ((TextBox) ctrl).Text = myAddr.StreetNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "EN") {
                            ((TextBox) ctrl).Text = myAddr.StreetEN;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "Info") {
                            ((TextBox) ctrl).Text = myAddr.OtherInfoNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "InfoEN") {
                            ((TextBox) ctrl).Text = myAddr.OtherInfoEN;
                        }
                        if ((ctrl).ID == "tbPostalCode" + i) {
                            ((TextBox) ctrl).Text = myAddr.PostalCode;
                        }
                        if ((ctrl).ID == "tbPostBox" + i) {
                            ((TextBox) ctrl).Text = myAddr.PostBox;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID != "ddCity" + i) {
                        continue;
                    }
                    int cityID = myAddr.CityID;
                    if (cityID <= -1) {
                        continue;
                    }
                    string cityName = myFactory.GetCityName(cityID, EN);
                    if (string.IsNullOrEmpty(cityName)) {
                        continue;
                    }
                    ((DropDownList) ctrl).Items.Add(new ListItem(cityName, cityID.ToString()));
                    ((DropDownList) ctrl).SelectedValue = cityID.ToString();
                }
            }
            for (int i = 1; i <= arrIDNum.Count; i++) {
                var myIDNum = (IDNumber) arrIDNum[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbIDNumber" + i) {
                            ((TextBox) ctrl).Text = myIDNum.Number;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID == "ddNumberType" + i) {
                        ((DropDownList) ctrl).SelectedValue = myIDNum.NumberTypeID.ToString();
                    }
                }
            }
            for (int i = 1; i <= arrPNum.Count; i++) {
                var myPNum = (PhoneNumber) arrPNum[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbPhoneNumber" + i) {
                            ((TextBox) ctrl).Text = myPNum.Number;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID == "ddPhoneNumber" + i + "Type") {
                        ((DropDownList) ctrl).SelectedValue = myPNum.NumberTypeID.ToString();
                    }
                }
            }
        }

        private void InitDDBoxes() {
            var myFactory2 = new uaFactory();
            BindPhoneNumberTypes(myFactory2);
            BindIDNumberTypes(myFactory2);
            BindEducationTypes(myFactory2);
            BindProfessionTypes(myFactory2);
        }

        private void BindPhoneNumberTypes(uaFactory myFactory2) {
            var phoneTypeSet = myFactory2.GetPNumberTypesAsDataSet();
            ddPhoneNumber1Type.DataSource = phoneTypeSet;
            ddPhoneNumber1Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber1Type.DataValueField = "NumberTypeID";
            ddPhoneNumber1Type.DataBind();

            ddPhoneNumber2Type.DataSource = phoneTypeSet;
            ddPhoneNumber2Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber2Type.DataValueField = "NumberTypeID";
            ddPhoneNumber2Type.DataBind();

            ddPhoneNumber3Type.DataSource = phoneTypeSet;
            ddPhoneNumber3Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber3Type.DataValueField = "NumberTypeID";
            ddPhoneNumber3Type.DataBind();

            ddPhoneNumber4Type.DataSource = phoneTypeSet;
            ddPhoneNumber4Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber4Type.DataValueField = "NumberTypeID";
            ddPhoneNumber4Type.DataBind();
        }

        private void BindIDNumberTypes(uaFactory myFactory2) {
            var idNumberType = myFactory2.GetIDNumberTypesAsDataSet();

            ddNumberType1.DataSource = idNumberType;
            ddNumberType1.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddNumberType1.DataValueField = "NumberTypeID";
            ddNumberType1.DataBind();

            ddNumberType2.DataSource = idNumberType;
            ddNumberType2.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddNumberType2.DataValueField = "NumberTypeID";
            ddNumberType2.DataBind();
        }

        private void BindEducationTypes(uaFactory myFactory) {
            var educationTypeSet = myFactory.GetEducationListAsDataSet();
            ddEducation.DataSource = educationTypeSet;

            ddEducation.DataTextField = EN ? "NameEN" : "NameNative";

            ddEducation.DataValueField = "EducationID";
            ddEducation.DataBind();
        }

        private void BindProfessionTypes(uaFactory myFactory) {
            var professionTypeSet = myFactory.GetProfessioListAsDataSet();
            ddProfession.DataSource = professionTypeSet;
            ddProfession.DataTextField = EN ? "NameEN" : "NameNative";
            ddProfession.DataValueField = "ProfessionID";
            ddProfession.DataBind();
        }

        private void BindCityList(DataSet citySet) {
            ddCity1.DataSource = citySet;
            ddCity1.DataTextField = EN ? "NameEN" : "NameNative";

            ddCity1.DataValueField = "CityID";
            ddCity1.DataBind();
        }

        private void BindCity2List(DataSet citySet) {
            ddCity2.DataSource = citySet;
            ddCity2.DataTextField = EN ? "NameEN" : "NameNative";

            ddCity2.DataValueField = "CityID";
            ddCity2.DataBind();
        }

        public ArrayList GetAddresses() {
            var myAddr = new Address();
            var address = new ArrayList();
            if (tbAddress1.Text != "" || tbAddress1EN.Text != "" || tbAddress1Info.Text != "" ||
                tbAddress1InfoEN.Text != "") {
                if (ddCity1.SelectedItem.Value.Trim() != "") {
                    myAddr.CityID = int.Parse(ddCity1.SelectedItem.Value);
                }
                myAddr.OtherInfoNative = tbAddress1Info.Text;
                myAddr.OtherInfoEN = tbAddress1InfoEN.Text;
                myAddr.PostalCode = tbPostalCode1.Text;
                myAddr.PostBox = tbPostBox1.Text;
                myAddr.StreetNative = tbAddress1.Text;
                myAddr.StreetEN = tbAddress1EN.Text;
                address.Add(myAddr);
            }
            if (tbAddress2.Text != "" || tbAddress2EN.Text != "" || tbAddress2Info.Text != "" ||
                tbAddress2InfoEN.Text != "") {
                myAddr = new Address();
                if (ddCity2.SelectedItem.Value.Trim() != "") {
                    myAddr.CityID = int.Parse(ddCity2.SelectedItem.Value);
                }
                myAddr.OtherInfoNative = tbAddress2Info.Text;
                myAddr.OtherInfoEN = tbAddress2InfoEN.Text;
                myAddr.PostalCode = tbPostalCode2.Text;
                myAddr.PostBox = tbPostBox2.Text;
                myAddr.StreetNative = tbAddress2.Text;
                myAddr.StreetEN = tbAddress2EN.Text;
                address.Add(myAddr);
            }
            return address;
        }

        public ArrayList GetIDNumbers() {
            IDNumber myIDNum;
            var aNumbers = new ArrayList();
            if (tbIDNumber1.Text != "") {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber1.Text,
                              NumberTypeID = int.Parse(ddNumberType1.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            if (tbIDNumber2.Text != "") {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber2.Text,
                              NumberTypeID = int.Parse(ddNumberType2.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            return aNumbers;
        }

        public ArrayList GetPhoneNumbers() {
            var myNum = new PhoneNumber();
            var aNumbers = new ArrayList();
            if (tbPhoneNumber1.Text != "") {
                myNum.Number = tbPhoneNumber1.Text;
                myNum.NumberTypeID = int.Parse(ddPhoneNumber1Type.SelectedItem.Value);
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber2.Text != "") {
                myNum = new PhoneNumber
                        {
                            Number = tbPhoneNumber2.Text,
                            NumberTypeID = int.Parse(ddPhoneNumber2Type.SelectedItem.Value)
                        };
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber3.Text != "") {
                myNum = new PhoneNumber
                        {
                            Number = tbPhoneNumber3.Text,
                            NumberTypeID = int.Parse(ddPhoneNumber3Type.SelectedItem.Value)
                        };
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber4.Text != "") {
                myNum = new PhoneNumber
                        {
                            Number = tbPhoneNumber4.Text,
                            NumberTypeID = int.Parse(ddPhoneNumber4Type.SelectedItem.Value)
                        };
                aNumbers.Add(myNum);
            }
            return aNumbers;
        }

        private void LocalizeText() {
            lbDebtorSurName.Text = rm.GetString("txtSurName", ci);
            lbDebtorFirstName.Text = rm.GetString("txtFirstName", ci);
            lbPersonInformation.Text = rm.GetString("txtPersonInformation", ci);
            btnSave.Text = rm.GetString("txtSave", ci);
            btCancel.Text = rm.GetString("txtCancel", ci);
            lbIDNumber2.Text = rm.GetString("txtIDNumber2", ci);
            lbIDNumberType.Text = rm.GetString("txtNumberType", ci);
            lbDebtorIDNumber3.Text = rm.GetString("txtIDNumber3", ci);
            lbIDNumberType2.Text = rm.GetString("txtNumberType", ci);
            lbSurnameEN.Text = rm.GetString("txtSurnameEN", ci);
            lbFirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lbPostalCode.Text = rm.GetString("txtPostalCode", ci);
            lbPostBox.Text = rm.GetString("txtPostBox", ci);
            lbAddress2.Text = rm.GetString("txtAddress2", ci);
            lbCity2.Text = rm.GetString("txtCity2", ci);
            lbAddress1.Text = rm.GetString("txtAddress1", ci);
            ltPostalCode2.Text = rm.GetString("txtPostalCode2", ci);
            lbProfession.Text = rm.GetString("txtProfession", ci);
            lbEducation.Text = rm.GetString("txtEducation", ci);
            lbPostBox2.Text = rm.GetString("txtPostBox2", ci);
            lbAddress2Info.Text = rm.GetString("txtAddressInfo2", ci);
            lbAddress1Info.Text = rm.GetString("txtAddressInfo1", ci);
            lbPhoneNumber1.Text = rm.GetString("txtPhoneNumber1", ci);

            lbPhonNumber1Type.Text = rm.GetString("txtNumberType", ci);
            lbPhoneNumber2.Text = rm.GetString("txtPhoneNumber2", ci);
            lbPhonenumber2Type.Text = rm.GetString("txtNumberType", ci);
            lbPhoneNumber3.Text = rm.GetString("txtPhoneNumber3", ci);
            lbPhoneNumber3Type.Text = rm.GetString("txtNumberType", ci);
            lbPhoneNumber4.Text = rm.GetString("txtPhoneNumber4", ci);
            lbPhoneNumber4Type.Text = rm.GetString("txtNumberType", ci);
            lbDebtorCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lbAddress1EN.Text = rm.GetString("txtAddressEN1", ci);
            lbCity1.Text = rm.GetString("txtCity1", ci);
            lbAddress1InfoEN.Text = rm.GetString("txtAddressInfoEN1", ci);
            lbAddress2EN.Text = rm.GetString("txtAddressEN2", ci);
            lbAddress2InfoEN.Text = rm.GetString("txtAddressInfoEN2", ci);
            rfvAddress.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvDebtorFirstName.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvDebtorSurName.ErrorMessage = rm.GetString("txtValueMissing", ci);
/*                
            RequiredFieldValidator1.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator3.ErrorMessage = rm.GetString("txtValueMissing", ci);*/
            lbUserUpdated.Text = rm.GetString("txtUserUpdated", ci);
            lbUserUpdated.Text = rm.GetString("txtNewUserCreated", ci);
            rfvCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvCity2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblCitySearch.Text = rm.GetString("txtSearch", ci);
            lblCity2Search.Text = rm.GetString("txtSearch", ci);
            lbEmail.Text = rm.GetString("txtEmail", ci);

            lblIsSearchable.Text = rm.GetString("txtIsSearchable", ci);
            rbtIsSearchableTrue.Text = rm.GetString("txtTrue", ci);
            rbtIsSearchableFalse.Text = rm.GetString("txtFalse", ci);
        }

        private void AddEnterEvent() {
            foreach (Control ctrl in Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {
                return;
            }
            var myFactory2 = new uaFactory();
            var theIndi = new Indivitual
                          {
                              Type = CigConfig.Configure("lookupsettings.individualID"),
                              EducationID = int.Parse(ddEducation.SelectedItem.Value),
                              FirstNameNative = tbDebtorFirstName.Text,
                              FirstNameEN = tbFirstNameEN.Text,
                              SurNameNative = tbDebtorSurName.Text,
                              SurNameEN = tbSurnameEN.Text,
                              Email = tbEmail.Text,
                              IsSearchable = rbtIsSearchableTrue.Checked,
                              ProfessionID = int.Parse(ddProfession.SelectedItem.Value)
                          };

            if (update) {
                theIndi.CreditInfoID = int.Parse(tbDebtorCIID.Text);
            }

            // collections
            theIndi.Address = GetAddresses();
            theIndi.IDNumbers = GetIDNumbers();
            theIndi.Number = GetPhoneNumbers();
            bool regOK = true;

            if (!update) {
                creditInfoID = myFactory2.addNewIndivitual(theIndi);
                if (creditInfoID == -2) {
                    lblError.Text = rm.GetString("txtFailedCreate_ViolationUniqueConstraint_IX_np_IDNumbers", ci);
                    lblError.Visible = true;
                    regOK = false;
                    return;
                } else if (creditInfoID < 0) {
                    lblError.Text = rm.GetString("txtFailedCreatingUser", ci);
                    lblError.Visible = true;
                    regOK = false;
                    return;
                }
                lbUserUpdated.Text = rm.GetString("txtNewUserCreated", ci);
                lbUserUpdated.Visible = true;
            } else {
                theIndi.LastUpdate = DateTime.Now;
                creditInfoID = myFactory2.UpdateIndivitual(theIndi);
                if (creditInfoID == -2) {
                    lblError.Text = rm.GetString("txtFailedCreate_ViolationUniqueConstraint_IX_np_IDNumbers", ci);
                    lblError.Visible = true;
                    regOK = false;
                    return;
                } else if (creditInfoID < 0) {
                    lblError.Text = rm.GetString("txtFailedUpdatingUser", ci);
                    lblError.Visible = true;
                    regOK = false;
                    return;
                }
                lbUserUpdated.Text = rm.GetString("txtUserUpdated", ci);
                lbUserUpdated.Visible = true;
            }

            if (regOK) {
                tbDebtorCIID.Text = creditInfoID.ToString();

                // adding javascript for submitting and closing the popup window
                var sbJscript = new StringBuilder();
                sbJscript.Append("<script language=\"JavaScript\">");
                sbJscript.Append("function SetCIID(){");
                sbJscript.Append("tsearch = window.location.search.substr(1);");
                sbJscript.Append("params = tsearch.split(\"&\");");
                sbJscript.Append("if(params.length > 0) { ctl = params[0].substring(4);");
                sbJscript.Append("nationalfieldname = params[1].substring(14);");
                sbJscript.Append("firstname = params[2].substring(11);");
                sbJscript.Append("surname = params[3].substring(11);}");
                sbJscript.Append(
                    "if (window.opener.document.getElementById(ctl) != null) window.opener.document.getElementById(ctl).value = " +
                    creditInfoID + ";");
                for (int i = 0; i < theIndi.IDNumbers.Count; i++) {
                    var myIDNum = (IDNumber) theIndi.IDNumbers[i];
                    if (myIDNum.NumberTypeID != int.Parse(CigConfig.Configure("lookupsettings.nationalID"))) {} else {
                        sbJscript.Append("if(nationalfieldname != '')");
                        sbJscript.Append(
                            "if (window.opener.document.getElementById(nationalfieldname) != null) window.opener.document.getElementById(nationalfieldname).value = \"" +
                            myIDNum.Number + "\";");
                        break;
                    }
                }
                sbJscript.Append(
                    "if (window.opener.document.getElementById(firstname) != null) window.opener.document.getElementById(firstname).value = \"" +
                    tbDebtorFirstName.Text + "\";");
                sbJscript.Append("if(surname != '')");
                sbJscript.Append(
                    "if (window.opener.document.getElementById(surname) != null) window.opener.document.getElementById(surname).value = \"" +
                    tbDebtorSurName.Text + "\";");
                sbJscript.Append("self.close();}");
                sbJscript.Append("SetCIID();");
                sbJscript.Append("</script>");
                Response.Write(sbJscript.ToString());
            }

            initControls(update);
        }

        protected void btnCitySearch_Click(object sender, EventArgs e) {
            if (txtCitySearch.Text.Trim() == "") {
                return;
            }
            var dsCity = myFactory.GetCityListAsDataSet(txtCitySearch.Text, EN);
            BindCityList(dsCity);
        }

        protected void btnCity2Search_Click(object sender, EventArgs e) {
            if (txtCity2Search.Text.Trim() == "") {
                return;
            }
            var dsCity = myFactory.GetCityListAsDataSet(txtCity2Search.Text, EN);
            BindCity2List(dsCity);
        }

        protected void btCancel_Click(object sender, EventArgs e) {
            var sbJscript = new StringBuilder();
            sbJscript.Append("<script language=\"JavaScript\">");
            sbJscript.Append("function Leave(){");
            sbJscript.Append("self.close();}");
            sbJscript.Append("Leave();");
            sbJscript.Append("</script>");
            Response.Write(sbJscript.ToString());
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { }

        #endregion
    }
}