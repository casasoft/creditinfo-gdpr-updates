using System;
using System.Configuration;
using System.Web.UI;
using Cig.Framework.Base.Configuration;

namespace UserAdmin {
    /// <summary>
    /// Summary description for ExtUserHandling.
    /// </summary>
    public partial class ExtUserHandling : Page {
        protected void Page_Load(object sender, EventArgs e) {
            Control uc;
            var strType = Request.QueryString["Type"];

            if (strType == "") {
                strType = ViewState["CurrentPage"].ToString();
            }

            ViewState["CurrentPage"] = strType;

            if (CigConfig.Configure("lookupsettings.individualID").Equals(strType)) {
//"Individual":
                uc = LoadControl("user_controls/NewCIUserCtrl.ascx");
                uc.ID = "NewCIUserCtrl";
            } else if (CigConfig.Configure("lookupsettings.companyID").Equals(strType)) {
                uc = LoadControl("user_controls/NewCIUserCCtrl.ascx");
                uc.ID = "NewCIUserCCtrl";
                //	this.Page.ID = "706";
            } else {
                uc = LoadControl("user_controls/NewCIUserCtrl.ascx");
                uc.ID = "NewCIUserCtrl";
            }

            blas.Controls.Add(uc);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { }

        #endregion
    }
}