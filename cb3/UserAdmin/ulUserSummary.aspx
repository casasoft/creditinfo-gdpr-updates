<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="ulUserSummary.aspx.cs" AutoEventWireup="True" Inherits="UserAdmin.ulUserSummary" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Usage summary</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						UsageSummary.btnExecute.click();
					}
				} 
		
				function SetFormFocus()
				{
					document.UsageSummary.txtUserName.focus();
				}
		</script>
	</HEAD>
	<body onload="SetFormFocus()" MS_POSITIONING="GridLayout">
		<form id="UsageSummary" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td style="BACKGROUND-IMAGE: url(../img/mainback.gif)">
						<table cellSpacing="0" cellPadding="0" width="919" align="center" border="0">
							<tr>
								<td style="WIDTH: 568px" vAlign="top" width="568" align="right" class="mainShadeLeft"><IMG alt="" src="../img/spacer.gif" width="6"></td>
								<td>
									<table cellSpacing="0" cellPadding="0" width="919" align="center" border="0">
										<tr>
											<td class="PageTitle" colSpan="3" bgColor="#951e16">
												<table borderColor="#0" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<tr>
														<td>
															<uc1:sitePositionBar id="SitePositionBar1" runat="server"></uc1:sitePositionBar></td>
														<td align="right"><uc1:language id="Language1" runat="server"></uc1:language></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td style="WIDTH: 568px" vAlign="top" width="568" bgColor="#ced0c5">
												<uc1:panelBar id="PanelBar1" runat="server"></uc1:panelBar></td>
											<td class="betweensides"></td>
											<td style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px"
												vAlign="top" width="766" bgColor="#ffffff">
												<table width="100%">
													<tr>
														<td>&nbsp;
															<asp:label id="lblPageHeader" runat="server" CssClass="HeadMain">Usage summary</asp:label>
															<p>
																<table class="subject" id="tbMain" cellSpacing="0" cellPadding="0" width="100%" align="center"
																	border="0">
																	<tr>
																		<td class="HeadLists">
																			<asp:Label id="lblLogInfo" runat="server">LOG INFO</asp:Label></td>
																	</tr>
																	<tr>
																		<td>
																			<hr>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<table id="tblQuery" cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD style="WIDTH: 253px"><asp:label id="lblFromDate" runat="server">From Date</asp:label></TD>
																					<TD style="WIDTH: 228px"><asp:label id="lblUserName" runat="server">Username</asp:label></TD>
																					<TD></TD>
																				</TR>
																				<tr>
																					<td style="WIDTH: 253px"><asp:textbox id="txtFromDate" runat="server" Width="176px"></asp:textbox>&nbsp;<INPUT onclick="PopupPicker('txtFromDate', 250, 250);" type="button" value="..."></td>
																					<td style="WIDTH: 228px"><asp:textbox id="txtUserName" runat="server" Width="225px"></asp:textbox></td>
																					<td></td>
																				</tr>
																				<TR>
																					<TD style="WIDTH: 253px"><asp:label id="lblToDate" runat="server">To Date</asp:label></TD>
																					<TD style="WIDTH: 228px"><asp:label id="lblLoggingType" runat="server">Logging type</asp:label></TD>
																					<TD></TD>
																				</TR>
																				<tr>
																					<td style="WIDTH: 253px"><asp:textbox id="txtToDate" runat="server" Width="176px"></asp:textbox>&nbsp;<INPUT onclick="PopupPicker('txtToDate', 250, 250);" type="button" value="..."></td>
																					<td style="WIDTH: 228px"><asp:dropdownlist id="ddlLoggingType" runat="server" Width="225px">
																							<asp:ListItem Value="0" Selected="True">All</asp:ListItem>
																						</asp:dropdownlist></td>
																					<td></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td height="20"></td>
																	</tr>
																	<tr>
																		<td class="HeadLists">
																			<asp:Label id="lblSubscriberInfo" runat="server">SUBSCRIBER INFO</asp:Label></td>
																	</tr>
																	<tr>
																		<td>
																			<hr>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<table id="tblSubscriberInfo" cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD style="WIDTH: 255px; HEIGHT: 14px"><asp:label id="lblSubscriberID" runat="server" Width="144px">Subscr. ID (national)</asp:label></TD>
																					<TD style="WIDTH: 230px; HEIGHT: 14px"><asp:label id="lblFirstName" runat="server">Subscr. Firstname</asp:label></TD>
																					<TD style="HEIGHT: 14px"></TD>
																				</TR>
																				<tr>
																					<td style="WIDTH: 255px; HEIGHT: 19px"><asp:textbox id="txtSubscriberID" runat="server" Width="140px"></asp:textbox></td>
																					<td style="WIDTH: 230px; HEIGHT: 19px"><asp:textbox id="txtFirstName" runat="server" Width="225px"></asp:textbox></td>
																					<td style="HEIGHT: 19px"></td>
																				</tr>
																				<TR>
																					<TD style="WIDTH: 255px">
																						<asp:Label id="lblCreditInfoID" runat="server">CreditInfoID</asp:Label></TD>
																					<TD style="WIDTH: 230px">
																						<asp:Label id="lblSurName" runat="server">Subscr. Surname</asp:Label></TD>
																					<TD></TD>
																				</TR>
																				<tr>
																					<td style="WIDTH: 255px">
																						<asp:textbox id="txtCreditInfoID" runat="server" Width="140px"></asp:textbox></td>
																					<td style="WIDTH: 230px">
																						<asp:textbox id="txtSurName" runat="server" Width="225px"></asp:textbox></td>
																					<td></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
																				<TR>
																					<TD style="HEIGHT: 15px"></TD>
																				</TR>
																				<TR>
																					<TD style="HEIGHT: 15px" bgColor="#951e16"></TD>
																				</TR>
																				<TR>
																					<TD style="HEIGHT: 15px"></TD>
																				</TR>
																				<tr>
																					<td align="right">
																						<table cellPadding="0" border="0">
																							<tr>
																								<td>
																									<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:Button id="btnExecute" runat="server" Text="Execute" CssClass="RegisterButton" onclick="btnExecute_Click"></asp:Button></div>
																								</td>
																								<td>
																									<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:Button id="btnPrint" runat="server" Text="Print" CssClass="RegisterButton" onclick="btnPrint_Click"></asp:Button></div>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																				<TR>
																					<TD style="HEIGHT: 15px"></TD>
																				</TR>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<table id="tbSumForResults" cellSpacing="0" cellPadding="0" width="100%" align="center"
																				border="0">
																				<tr>
																					<td style="WIDTH: 140px">
																						<asp:Label id="lblAccessLogTotal" runat="server">Access log TOTAL:</asp:Label></td>
																					<td>
																						<asp:Label id="lblSumOfTimes" runat="server"></asp:Label></td>
																				</tr>
																				<tr>
																					<td style="WIDTH: 140px">
																						<asp:Label id="lblResultLogTotal" runat="server">Results log TOTAL:</asp:Label></td>
																					<td>
																						<asp:Label id="lblSumOfResults" runat="server"></asp:Label></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<hr>
																		</td>
																	</tr>
																	<tr>
																		<td align="center">
																			<asp:DataGrid id="dgUsageSummary" runat="server" CellPadding="4" BackColor="White" BorderWidth="1px"
																				BorderStyle="None" BorderColor="Gray" AutoGenerateColumns="False" HorizontalAlign="Center"
																				Width="100%">
																				<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
																				<AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
																				<ItemStyle ForeColor="Black" BackColor="White"></ItemStyle>
																				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#895A4F"></HeaderStyle>
																				<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
																				<Columns>
																					<asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="Subscriber"></asp:BoundColumn>
																					<asp:BoundColumn DataField="UserName" SortExpression="UserName" ReadOnly="True" HeaderText="User">
																						<ItemStyle HorizontalAlign="Center"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="typeNative" SortExpression="typeNative" ReadOnly="True" HeaderText="Logging Type">
																						<ItemStyle HorizontalAlign="Center"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="OperationRequest" SortExpression="OperationRequest" ReadOnly="True" HeaderText="No. of times">
																						<ItemStyle HorizontalAlign="Center"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="OperationResultsSum" ReadOnly="True" HeaderText="No. of results returned">
																						<ItemStyle HorizontalAlign="Center"></ItemStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
																			</asp:DataGrid>
																		</td>
																	</tr>
																</table>
															</p>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td style="WIDTH: 568px" width="568" bgColor="#e2e2e2" height="1"><img height="1" src="" width="153"></td>
											<td><img height="1" src="" width="10"></td>
											<td width="766" bgColor="#e2e2e2" height="1"><img height="1" src="" width="758"></td>
										</tr>
									</table>
								</td>
								<td class="mainShadeRight"><IMG alt="" src="img/spacer.gif" width="6"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer>
						<uc1:SecureUserAdmin id="SecureUserAdmin1" runat="server"></uc1:SecureUserAdmin></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
