using System;
using System.Data;
using UserAdmin.BLL.auDepartments;
using UserAdmin.BLL.auProducts;
using UserAdmin.BLL.auSubscribers;
using UserAdmin.BLL.auUsers;
using UserAdmin.BLL.CIUsers;
using UserAdmin.BLL.npEmployee;
using UserAdmin.DAL;
using UserAdmin.DAL.auDepartments;
using UserAdmin.DAL.auProducts;
using UserAdmin.DAL.auSubscribers;
using UserAdmin.DAL.CIUsers;
using UserAdmin.DAL.stats;
using UserAdmin.DAL.UsageLogs;
using UserAdmin.BLL;

using System.Collections.Generic;
using cb3.UserAdmin.BLL.auUsers;

namespace UserAdmin.BLL {
    /// <summary>
    /// Summary description for Factory.
    /// </summary>
    public class uaFactory {
        private readonly CreditInfoUserDALC _myCreditInfoUserDALC;
        private readonly auDepartmentsDALC _myDepartmentDALC;
        private readonly auProductsDALC _myProductsDALC;
        private readonly StatsDALC _myStats;
        private readonly auSubscribersDALC _mySubscriberDALC;
        private readonly UsageLogsSummaryDALC _myUsageLogDALC;
        private readonly UserData _myUserDataDALC;

        /// <summary>
        /// Constructor.
        /// </summary>
        public uaFactory() {
            //
            // TODO: Add constructor logic here
            //

            _myUsageLogDALC = new UsageLogsSummaryDALC();
            _myCreditInfoUserDALC = new CreditInfoUserDALC();
            _myUserDataDALC = new UserData();
            _mySubscriberDALC = new auSubscribersDALC();
            _myProductsDALC = new auProductsDALC();
            _myDepartmentDALC = new auDepartmentsDALC();
            _myStats = new StatsDALC();
        }

        #region usage

        public DataSet GetUsageLogSummaryForUserAsDataSet(string FromDate, string ToDate, string UserName) { return _myUsageLogDALC.GetUsageLogSummaryForUserAsDataSet(FromDate, ToDate, UserName); }

        public DataSet GetUsageData(
            string FromDate,
            string ToDate,
            string UserName,
            string LoggingType,
            string SubscriberID,
            string CreditInfoID,
            string FirstName,
            string SurName) {
            return _myUsageLogDALC.GetUsageData(
                FromDate, ToDate, UserName, LoggingType, SubscriberID, CreditInfoID, FirstName, SurName);
        }

        public DataSet GetLoggingTypeAsDataSet() { return _myUsageLogDALC.GetLoggingTypeAsDataSet(); }

        #endregion

        #region user / subscriber

        public string GetCreditInfoUserNativeName(int CreditInfoID) { return _myCreditInfoUserDALC.GetCreditInfoUserNativeName(CreditInfoID); }
        public string GetCreditInfoUserENName(int CreditInfoID) { return _myCreditInfoUserDALC.GetCreditInfoUserENName(CreditInfoID); }
        public DataSet FindCustomer(Debtor myDebtor) { return _myCreditInfoUserDALC.FindCustomer(myDebtor, false); }

        public DataSet FindCustomer(Debtor myDebtor,string vatNumber) { return _myCreditInfoUserDALC.FindCustomer(myDebtor, false,vatNumber); }

        /// <summary>
        /// Gets list of customer as DataSet
        /// </summary>
        /// <param name="myDebtor">Debtor searchable info</param>
        /// <returns>Customers DataSet</returns>
        public DataSet FindDDDCustomer(Debtor myDebtor) { return _myCreditInfoUserDALC.FindDDDCustomer(myDebtor, false); }

        /// <summary>
        /// Returns DDD "only" individual
        /// </summary>
        /// <param name="nationalID">The individuals National ID</param>
        /// <returns></returns>
        public Indivitual GetDDDIndivitualByNationalIDAsIndi(string nationalID) { return _myCreditInfoUserDALC.GetDDDIndivitualByNationalID(nationalID, false); }

        /*	public DataSet FindDDDCustomer(Debtor myDebtor)
			{
				return _myCreditInfoUserDALC.FindDDDCustomer(myDebtor, false);
			}
		*/
        public DataSet FindCompany(Company myComp) { return _myCreditInfoUserDALC.FindCompany(myComp, false); }
        /// <summary>
        /// Method for searching by VatNumbers.
        /// </summary>
        /// <param name="myComp"></param>
        /// <param name="vatNumber"></param>
        /// <returns></returns>
        public DataSet FindCompany(Company myComp, string vatNumber) { return _myCreditInfoUserDALC.FindCompany(myComp, false, vatNumber); }

        public DataSet FindCustomerInNationalAndCreditInfo(Debtor myDebtor) { return _myCreditInfoUserDALC.FindCustomerInNationalAndCreditInfo(myDebtor, false); }
        public DataSet FindCompanyInNationalAndCreditInfo(Company myComp) { return _myCreditInfoUserDALC.FindCompanyInNationalAndCreditInfo(myComp, false); }
        public DataSet FindCustomer(Debtor myDebtor, bool exactAddress) { return _myCreditInfoUserDALC.FindCustomer(myDebtor, exactAddress); }
        public DataSet FindCustomer(Debtor myDebtor, bool exactAddress, string vatNumber) { return _myCreditInfoUserDALC.FindCustomer(myDebtor, exactAddress, vatNumber); }
        public DataSet FindCustomer(Debtor myDebtor, bool exactAddress, string vatNumber, bool showVat) { return _myCreditInfoUserDALC.FindCustomer(myDebtor, exactAddress, vatNumber, showVat); }


        public DataSet FindMaltaCompany(Company myComp, bool exactAddress, bool tradingAddress) { return _myCreditInfoUserDALC.FindMaltaCompany(myComp, exactAddress, tradingAddress); }
        public DataSet FindMaltaCompany(Company myComp, bool exactAddress, bool tradingAddress,string vatNumber) { return _myCreditInfoUserDALC.FindMaltaCompany(myComp, exactAddress, tradingAddress,vatNumber); }
        public DataSet FindMaltaCompany(Company myComp, bool exactAddress, bool tradingAddress, string vatNumber, bool showVat) { return _myCreditInfoUserDALC.FindMaltaCompany(myComp, exactAddress, tradingAddress, vatNumber,  showVat); }


        public DataSet FindCompany(Company myComp, bool exactAddress) { return _myCreditInfoUserDALC.FindCompany(myComp, exactAddress); }
        public DataSet FindCustomerInNationalAndCreditInfo(Debtor myDebtor, bool exactAddress) { return _myCreditInfoUserDALC.FindCustomerInNationalAndCreditInfo(myDebtor, exactAddress); }
        public DataSet FindCompanyInNationalAndCreditInfo(Company myComp, bool exactAddress) { return _myCreditInfoUserDALC.FindCompanyInNationalAndCreditInfo(myComp, exactAddress); }
        public DataSet GetIDNumberTypesAsDataSet() { return _myCreditInfoUserDALC.GetIDNumberTypesAsDataSet(); }
        public int UpdateCompany(Company theComp) { return _myCreditInfoUserDALC.UpdateCompany(theComp); }
        public Company GetCompany(int creditInfoID) { return _myCreditInfoUserDALC.GetCompany(creditInfoID); }
        public Company GetCompany(int creditInfoID, bool isSearchable) { return _myCreditInfoUserDALC.GetCompany(creditInfoID, isSearchable); }
        public int UpdateIndivitual(Indivitual indi) { return _myCreditInfoUserDALC.UpdateIndivitual(indi); }
        public int addNewIndivitual(Indivitual theIndi) { return _myCreditInfoUserDALC.AddNewIndivitual(theIndi); }
        public bool DeleteIndivitual(int creditInfoID) { return _myCreditInfoUserDALC.DeleteIndivitual(creditInfoID); }
        public bool DeleteCompany(int creditInfoID) { return _myCreditInfoUserDALC.DeleteCompany(creditInfoID); }
        public DataSet GetCompanyNaceCodes() { return _myCreditInfoUserDALC.GetCompanyNaceCodes(); }
        public DataSet GetOrgStatus() { return _myCreditInfoUserDALC.GetOrgStatus(); }
        public DataSet GetCityListAsDataSet() { return _myCreditInfoUserDALC.GetCityListAsDataSet(); }
        public DataSet GetCityListAsDataSet(bool EN) { return _myCreditInfoUserDALC.GetCityListAsDataSet(EN); }
        public DataSet GetCityListAsDataSet(string cityName, bool en) { return _myCreditInfoUserDALC.GetCityListAsDataSet(cityName, en); }
        public string GetCityName(int cityID, bool en) { return _myCreditInfoUserDALC.GetCityName(cityID, en); }
        public int AddCompany(Company myComp) { return _myCreditInfoUserDALC.AddCompany(myComp); }
        public DataSet GetProfessioListAsDataSet() { return _myCreditInfoUserDALC.GetProfessioListAsDataSet(); }
        public Debtor GetIndivitual(String DebtorID) { return _myCreditInfoUserDALC.GetIndivitual(DebtorID); }



        /// <summary>
        /// Searches and returns entities based on DDDOnly param
        /// </summary>
        /// <param name="DebtorID">The debtors CIID</param>
        /// <param name="DDDOnly">Wether to search for DDD only subjects</param>
        /// <returns>Debtor instance</returns>
        public Debtor GetIndivitual(String DebtorID, bool DDDOnly) {
            return DDDOnly ? _myCreditInfoUserDALC.GetDDDIndivitual(DebtorID) : _myCreditInfoUserDALC.GetIndivitual(DebtorID);
        }

        public Indivitual GetIndivitualFromNationalRegistry(string NationalID) { return _myCreditInfoUserDALC.GetIndivitualFromNationalRegistry(NationalID); }
        public Company GetCompanyFromNationalRegistry(string NationalID) { return _myCreditInfoUserDALC.GetCompanyFromNationalRegistry(NationalID); }
        public Indivitual GetIndivitual(int creditInfoID) { return _myCreditInfoUserDALC.GetIndivitual(creditInfoID); }
        public Indivitual GetIndivitual(int creditInfoID, bool isSearchable) { return _myCreditInfoUserDALC.GetIndivitual(creditInfoID, isSearchable); }
        public DataSet GetPNumberTypesAsDataSet() { return _myCreditInfoUserDALC.GetPNumberTypesAsDataSet(); }
        public DataSet GetEducationListAsDataSet() { return _myCreditInfoUserDALC.GetEducationListAsDataSet(); }
        public DataSet SearchWhitelist(Indivitual myIndi) { return _myCreditInfoUserDALC.SearchWhitelist(myIndi); }
        public bool IsIDRegisted(String number, int type) { return _myCreditInfoUserDALC.IsIDRegisted(number, type); }
        public DataSet SearchCompaniesHouse(Company myComp) { return _myCreditInfoUserDALC.SearchCompaniesHouse(myComp); }
        public string GetCompanyStatusByCIID(int CIID) { return _myCreditInfoUserDALC.GetCompanyStatusByCIID(CIID); }

        /// <summary>
        /// Return list of Vat numbers by credit info id.
        /// </summary>
        /// <param name="creditinfoId"></param>
        /// <returns></returns>
        public IList<CIUsers.VatNumber> GetVatNumbersByCreditinfoId(int creditinfoId)
        {
            return this._myCreditInfoUserDALC.GetVatNumbersByCreditinfoId(creditinfoId);
        }

        /// <summary>
        /// Return list of Vat numbers by credit info id.
        /// </summary>
        /// <param name="creditinfoId"></param>
        /// <returns></returns>
        public IList<CIUsers.VatNumber> GetVatNumbersByCreditinfoId(int creditinfoId,bool showOnly)
        {
            return this._myCreditInfoUserDALC.GetVatNumbersByCreditinfoId(creditinfoId, showOnly);
        }


        #endregion

        #region uaUser

        public bool IsUserOpen(int UserID) { return _myUserDataDALC.IsUserOpen(UserID); }
        public bool IsUserExpiryDateOK(int UserID) { return _myUserDataDALC.IsUserExpiryDateOK(UserID); }
        public bool ChangePassword(auUsersBLLC user, string newPass) { return _myUserDataDALC.ChangePassword(user, newPass); }
        public bool AccountLock(string userName, bool lockUserAccount) { return _myUserDataDALC.AccountLock(userName, lockUserAccount); }
        public bool IsAccountLocked(int userId) { return _myUserDataDALC.IsAccountLocked(userId); }

        public DataSet SearchUsers(
            string CreditInfoID, string FirstName, string Surname, string Address, string UserName, string OpenClosed) { return _myUserDataDALC.SearchUsers(CreditInfoID, FirstName, Surname, Address, UserName, OpenClosed); }

        /// <summary>
        /// Inserts a user entity into a data structure.
        /// </summary>
        /// <param name="myUser">auUsersBLLC entity.</param>
        public void StoreUserAccountDetails(auUsersBLLC myUser) { _myUserDataDALC.StoreUserAccountDetails(myUser); }

        public void StoreEmployeeDetails(npEmployeeBLLC myEmp) { _myUserDataDALC.StoreEmployeeDetails(myEmp); }
        public void DeleteEmployeeDetails(string Initials) { _myUserDataDALC.DeleteEmployeeDetails(Initials); }

        /// <summary>
        /// Fetches a specific user from the underlying data structure.
        /// </summary>
        /// <param name="UserID">Unique ID of the user.</param>
        /// <returns>User entity.</returns>
        public auUsersBLLC GetUser(int UserID) { return _myUserDataDALC.GetUser(UserID); }

        public auUsersBLLC GetSpecificUser(string UserName) { return _myUserDataDALC.GetSpecificUser(UserName); }
        public auUserGroups GetUserGroup(int GroupID) { return _myUserDataDALC.GetUserGroup(GroupID); }
        public int GetUserGroupID(string GroupCode) { return _myUserDataDALC.GetUserGroupID(GroupCode); }
        public DataSet GetAllUserGroups() { return _myUserDataDALC.GetAllUserGroups(); }
        public int SaveUserGroup(auUserGroups auUserGroup) { return _myUserDataDALC.SaveUserGroup(auUserGroup); }
        public bool DeleteUserGroup(int GroupID) { return _myUserDataDALC.DeleteUserGroup(GroupID); }
        public string GetRoles(string username) { return _myUserDataDALC.GetRoles(username); }
        public void UpdateUserAccountDetails(auUsersBLLC myUser) { _myUserDataDALC.UpdateUserAccountDetails(myUser); }
        public void UpdateUserPassword(string PasswordHash, string salt, string UserName) { _myUserDataDALC.UpdateUserPassword(PasswordHash, salt, UserName); }
        public void UpdateCreditInfoUserTableWithInfo(string Email, int StatusID, int CreditInfoID) { _myUserDataDALC.UpdateCreditInfoUserTableWithInfo(Email, StatusID, CreditInfoID); }
        public static string CreateSalt(int size) { return UserData.CreateSalt(size); }
        public static string CreatePasswordHash(string pwd, string salt) { return UserData.CreatePasswordHash(pwd, salt); }
        public int GetCreditInfoID(string UserName) { return _myUserDataDALC.GetCreditInfoID(UserName); }
        public int GetUserID(string UserName) { return _myUserDataDALC.GetUserID(UserName); }
        public int GetUserID(int CIID, int SubscriberID) { return _myUserDataDALC.GetUserID(CIID, SubscriberID); }
        public string GetUserName(int CIID, int SubscriberID) { return _myUserDataDALC.GetUserName(CIID, SubscriberID); }
        public string GetUserName(int UserID) { return _myUserDataDALC.GetUserName(UserID); }
        public string GetUserNameByCIID(int CIID) { return _myUserDataDALC.GetUserNameByCIID(CIID); }
        public DataSet GetUsersAsDataSet() { return _myUserDataDALC.GetUsersAsDataSet(); }
        public DataSet GetUsersBySubscriberAsDataSet(int SubscriberID) { return _myUserDataDALC.GetUsersBySubscriberAsDataSet(SubscriberID); }
        public auSearchFilter GetSearchFilters(int filterId, bool isSubscriber) { return _myUserDataDALC.GetSearchFilters(filterId, isSubscriber); }
        public bool ModifySearchFilters(auSearchFilter searchFilter, bool isUpdate) { return _myUserDataDALC.ModifySearchFilters(searchFilter, isUpdate); }

        /// <summary>
        /// Fetches all users working in a specific department.
        /// </summary>
        /// <param name="DepartmentID">Unique ID for the department.</param>
        /// <returns>A dataset containing users.</returns>
        public DataSet GetUsersAsDataSet(int DepartmentID) { return _myUserDataDALC.GetUsersAsDataSet(DepartmentID); }

        /// <summary>
        /// Fetches all users working in a specific department.
        /// </summary>
        /// <param name="DepartmentID">Unique ID for the department.</param>
        /// <param name="IsOpen">Is user open or closed.</param>
        /// <returns>A dataset containing users.</returns>
        public DataSet GetUsersAsDataSet(int DepartmentID, bool IsOpen) { return _myUserDataDALC.GetUsersAsDataSet(DepartmentID, IsOpen); }

        public DataSet FindUser_OLD(string UserName, int CreditInfoID, string NameNative, string NameEN) { return _myUserDataDALC.FindUser_OLD(UserName, CreditInfoID, NameNative, NameEN); }

        public DataSet FindUser(
            string strUsername,
            string strName,
            string strEmail,
            string strSubscriberName,
            string strUserType,
            int nIsOpen) { return _myUserDataDALC.FindUser(strUsername, strName, strEmail, strSubscriberName, strUserType, nIsOpen); }

        public DataSet GetUserTypes() { return _myUserDataDALC.GetUserTypes(); }
        public int GetMaxCreditWatch(string UserName) { return _myUserDataDALC.GetMaxCreditWatch(UserName); }
        public bool VerifyPassword(string txtUserName, string txtPassword) { return _myUserDataDALC.VerifyPassword(txtUserName, txtPassword); }

        public void StoreAccountDetails(
            string userName,
            string passwordHash,
            string salt,
            string groups,
            int iCreditInfoID,
            int iCount) { _myUserDataDALC.StoreAccountDetails(userName, passwordHash, salt, groups, iCreditInfoID, iCount); }

        public DataSet GetPostcodesByCityAsDataSet(int cityID) { return _myCreditInfoUserDALC.GetPostcodesByCityAsDataSet(cityID); }

        #endregion

        #region uaSubscriber

        public bool IsSubscriberOpen(int UserID) { return _mySubscriberDALC.IsSubscriberOpen(UserID); }

        public DataSet SearchSubscribers(
            string CreditInfoID,
            string NationalID,
            string FirstName,
            string Surname,
            string Address,
            string RegisteredBy,
            string OpenClosed) {
            return _mySubscriberDALC.SearchSubscribers(
                CreditInfoID, NationalID, FirstName, Surname, Address, RegisteredBy, OpenClosed);
        }

        public DataSet GetAllSubscribersAsDataSet() { return _mySubscriberDALC.GetAllSubscribersAsDataSet(); }
        public bool AddNewSubscriber(auSubscribersBLLC mySubscriber) { return _mySubscriberDALC.AddNewSubscriber(mySubscriber); }
        public bool UpdateSubscriber(auSubscribersBLLC mySubscriber) { return _mySubscriberDALC.UpdateSubscriber(mySubscriber); }
        public DataSet GetOneSubscriberByCreditInfoIDAsDataSet(int CreditInfoID) { return _mySubscriberDALC.GetOneSubscriberByCreditInfoIDAsDataSet(CreditInfoID); }
        public auSubscribersBLLC GetOneSubscriberByCreditInfoIDAsSubscriber(int CreditInfoID) { return _mySubscriberDALC.GetOneSubscriberByCreditInfoIDAsSubscriber(CreditInfoID); }
        public int GetSubscriberUserLimit(int CreditInfoID) { return _mySubscriberDALC.GetSubscriberUserLimit(CreditInfoID); }

        /// <summary>
        /// Fetched the number of open users already created for a specific susbscriber.
        /// </summary>
        /// <param name="CreditInfoID">Unique CIID of the subscriber.</param>
        /// <returns>Number of users.</returns>
        public int GetSubscriberUserCount(int CreditInfoID) { return _mySubscriberDALC.GetSubscriberUserCount(CreditInfoID); }

        #endregion

        #region auDepartments

        /// <summary>
        /// Writes a new department entry to database.
        /// </summary>
        /// <param name="myDepartment">Department entity</param>
        /// <returns>True/False</returns>
        public bool InsertDepartment(auDepartmentBLLC myDepartment) { return _myDepartmentDALC.Insert(myDepartment); }

        /// <summary>
        /// Updates an existing department entry in database.
        /// </summary>
        /// <param name="myDepartment">Department entity</param>
        /// <returns>True/False</returns>
        public bool UpdateDepartment(auDepartmentBLLC myDepartment) { return _myDepartmentDALC.Update(myDepartment); }

        /// <summary>
        /// Updates only the IsOpen attribute for a specific department
        /// </summary>
        /// <param name="myDepartment">Department entity</param>
        /// <returns>True/False</returns>
        public bool EnableDepartment(auDepartmentBLLC myDepartment) { return _myDepartmentDALC.Enable(myDepartment); }

        /// <summary>
        /// Updates only the IsOpen attribute for a specific department
        /// </summary>
        /// <param name="DepartmentID">Unique ID for department</param>
        /// <param name="Enable">True to enable department, False to disable department</param>
        /// <returns>True/False</returns>
        public bool EnableDepartment(int DepartmentID, bool Enable) {
            auDepartmentBLLC department = new auDepartmentBLLC();
            department.DepartmentID = DepartmentID;
            department.IsOpen = Enable;
            return _myDepartmentDALC.Enable(department);
        }

        /// <summary>
        /// Fetches one specific department entry.
        /// </summary>
        /// <param name="DepartmentID">Unique ID for department.</param>
        /// <returns>Department entity</returns>
        public auDepartmentBLLC GetDepartment(int DepartmentID) { return _myDepartmentDALC.Get(DepartmentID); }

        /// <summary>
        /// Fetches all department entries for a specific subscriber(CIID). 
        /// </summary>
        /// <param name="CIID">CreditInfoID identifying a subscriber.</param>
        /// <returns>Set of department data.</returns>
        public DataSet GetDepartmentsAsDataSet(int CIID) { return _myDepartmentDALC.GetDepartmentsAsDataSet(CIID); }

        /// <summary>
        /// Fetches all department entries for a specific subscriber(CIID). 
        /// </summary>
        /// <param name="CIID">CreditInfoID identifying a subscriber.</param>
        /// <param name="IsOpen">Is department open or closed.</param>
        /// <returns>Set of department data.</returns>
        public DataSet GetDepartmentsAsDataSet(int CIID, bool IsOpen) { return _myDepartmentDALC.GetDepartmentsAsDataSet(CIID, IsOpen); }

        /// <summary>
        /// Check if a department that a user belongs to is open.
        /// </summary>
        /// <param name="UserID">Unique ID of an user.</param>
        /// <returns>Open(true)/Closed(false).</returns>
        public bool IsDepartmentOpen(int UserID) { return _myDepartmentDALC.IsOpen(UserID); }

        #endregion

        #region auProducts

        public DataSet GetAllTheOpenProducts() { return _myProductsDALC.GetAllTheOpenProducts(true); }
        public DataSet GetAllTheOpenProducts(bool isMenu) { return _myProductsDALC.GetAllTheOpenProducts(isMenu); }
        public bool WriteUserProducts(auProductsBLLC[] myProductArray, int UserID) { return _myProductsDALC.WriteUserProducts(myProductArray, UserID); }
        public bool WriteGroupProducts(auProductsBLLC[] myProductArray, int GroupID) { return _myProductsDALC.WriteGroupProducts(myProductArray, GroupID); }
        public bool DeleteGroupProducts(int GroupID) { return _myProductsDALC.DeleteGroupProducts(GroupID); }
        public DataSet ReadUserProducts(int UserID) { return _myProductsDALC.ReadUserProducts(UserID); }
        public DataSet ReadGroupProducts(int GroupID) { return _myProductsDALC.ReadGroupProducts(GroupID); }
        public bool HasUserAccessToProduct(int UserID, int ProductID) { return _myProductsDALC.HasUserAccessToProduct(UserID, ProductID); }
        public string GetParentID(string strProductID) { return _myProductsDALC.GetParentID(strProductID); }
        public IList<string> GetSearchAndTafDescriptions() { return _myProductsDALC.GetSearchAndTafReportsDesctiptions(); }

        
        #endregion

        #region stats

        public int GetNumberOfRegistrationsAddedForPeriod(DateTime FromDate, DateTime ToDate) { return _myStats.GetNumberOfRegistrationsAddedForPeriod(FromDate, ToDate); }
        public int GetNumberOfRegistrationsAddedForPeriodAndType(DateTime FromDate, DateTime ToDate, int Type) { return _myStats.GetNumberOfRegistrationsAddedForPeriodAndType(FromDate, ToDate, Type); }

        #endregion
    }
}