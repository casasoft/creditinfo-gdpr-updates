namespace UserAdmin.BLL.auDepartments {
    /// <summary>
    /// Summary description for auDepartmentBLLC.
    /// </summary>
    public class auDepartmentBLLC {
        // private members
        /// <summary>
        /// Gets or sets unique ID for a department.
        /// </summary>		
        public int DepartmentID { get; set; }

        /// <summary>
        /// Gets or sets unique CIID of the subscriber which this department belongs to.
        /// </summary>
        public int CreditInfoID { get; set; }

        /// <summary>
        /// Gets or sets native name.
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// Gets or sets english name.
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Gets or sets deparments postcode.
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// Gets or sets native name of street.
        /// </summary>
        public string StreetNative { get; set; }

        /// <summary>
        /// Gets or sets english name of street.
        /// </summary>
        public string StreetEN { get; set; }

        /// <summary>
        /// Gets or sets departments phone number.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets departments e-mail address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the contact person for the department.
        /// </summary>
        public string ContactPerson { get; set; }

        /// <summary>
        /// Gets or sets the open/closed status of the department.
        /// </summary>
        public bool IsOpen { get; set; }

        /// <summary>
        /// Gets or sets a bool indicating if this deparment is at the subscribers head office.
        /// </summary>
        public bool IsHeadOffice { get; set; }

        /// <summary>
        /// Gets or sets the billing account reference.
        /// </summary>
        public string BillingAccount { get; set; }
    }
}