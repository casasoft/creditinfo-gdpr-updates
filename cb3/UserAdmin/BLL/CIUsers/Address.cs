using System;

namespace UserAdmin.BLL.CIUsers {
    [Serializable]
    /// <summary>
        /// Summary description for Address.
        /// </summary>
    public class Address {
        public int AddressID { get; set; }
        public String StreetNative { get; set; }
        public String StreetEN { get; set; }
        public String CountryNative { get; set; }
        public String CountryEN { get; set; }
        public int? CountryId { get; set; }
        public int StreetNumber { get; set; }
        public int CityID { get; set; }
        public String CityNameNative { get; set; }
        public String CityNameEN { get; set; }
        public String CityLocationNative { get; set; }
        public String CityLocationEN { get; set; }
        public String PostalCode { get; set; }
        public String PostBox { get; set; }
        public String OtherInfoNative { get; set; }
        public String OtherInfoEN { get; set; }
        public int CreditInfoID { get; set; }
        public bool IsTradingAddress { get; set; }
    }
}