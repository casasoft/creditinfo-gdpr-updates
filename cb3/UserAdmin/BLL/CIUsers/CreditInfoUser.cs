using System;
using System.Collections;

namespace UserAdmin.BLL.CIUsers {
    /// <summary>
    /// This is the base class for CreditInfoUsers.
    /// </summary>
    public abstract class CreditInfoUser {
        // 0 is Indivitual, 1 is Company
        public CreditInfoUser() {
            CreditInfoID = -1;
            Address = new ArrayList();
            Number = new ArrayList();
            IDNumbers = new ArrayList();
            //
            // TODO: Add constructor logic here
            //
        }

        public int CreditInfoID { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastUpdate { get; set; }
        public String Type { get; set; }
        public String Email { get; set; }
        public ArrayList Address { get; set; }
        public ArrayList Number { get; set; }
        public ArrayList IDNumbers { get; set; }
    }
}