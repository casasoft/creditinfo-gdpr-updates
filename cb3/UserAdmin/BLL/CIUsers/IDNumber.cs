namespace UserAdmin.BLL.CIUsers {
    /// <summary>
    /// Summary description for IDNumber.
    /// </summary>
    public class IDNumber {
        public int IDNumberID { get; set; }
        public string Number { get; set; }
        public int NumberTypeID { get; set; }
        public int CreditInfoID { get; set; }
        public string NumberTypeNative { get; set; }
        public string NumberTypeEN { get; set; }
    }
}