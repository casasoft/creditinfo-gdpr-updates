using System;

namespace UserAdmin.BLL.CIUsers {
    /// <summary>
    /// Summary description for Company.
    /// </summary>
    public class Company : CreditInfoUser {
        /// <summary>
        /// The companies functional description written in english language
        /// </summary>
        private string _funcDescriptioEN;

        /// <summary>
        /// The companies functional description written in native language
        /// </summary>
        private string _funcDescriptionNative;

        /// <summary>
        /// Gets/set the _nameNative attribute
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// Gets/set the _nameEN attribute
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Gets/set the _established attribute
        /// </summary>
        public DateTime Established { get; set; }

        /// <summary>
        /// Gets/set the _registered attribute
        /// </summary>
        public DateTime Registered { get; set; }

        /// <summary>
        /// Gets/set the _lastContacted attribute
        /// </summary>
        public DateTime LastContacted { get; set; }

        /// <summary>
        /// Gets/set the _URL attribute
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// Gets/set the _funcID attribute
        /// </summary>
        public string FuncID { get; set; }

        /// <summary>
        /// Gets/set the __funcDescriptionShortNative attribute
        /// </summary>
        public string FuncDescriptionShortNative { get; set; }

        /// <summary>
        /// Gets/set the _funcDescriptionShortEN attribute
        /// </summary>
        public string FuncDescriptionShortEN { get; set; }

        /// <summary>
        /// Gets/set the _funcDescriptionNative attribute
        /// </summary>
        public string FuncDescriptionNative { get { return _funcDescriptionNative; } set { _funcDescriptionNative = value; } }

        /// <summary>
        /// Gets/set the _funcDescriptionNative attribute
        /// </summary>
        public string FuncDescriptionEN { get { return _funcDescriptionNative; } set { _funcDescriptioEN = value; } }

        /// <summary>
        /// Gets/set the _nationalID attribute
        /// </summary>
        public string NationalID { get; set; }

        /// <summary>
        /// Gets/set the _org_status_code attribute
        /// </summary>
        public int Org_status_code { get; set; }

        /// <summary>
        /// Gets/set the _org_name_status_code attribute
        /// </summary>
        public string Org_name_status_code { get; set; }

        /// <summary>
        /// Gets/set the _org_status_date attribute
        /// </summary>
        public DateTime Org_status_date { get; set; }

        /// <summary>
        /// Get/sets the single Address Native attribute
        /// </summary>
        public string SingleAddressNative { get; set; }

        /// <summary>
        /// Get/set the Single Address EN attribute
        /// </summary>
        public string SingleAddressEN { get; set; }

        /// <summary>
        /// Gets/sets the _isSearchable attribute
        /// </summary>
        public bool IsSearchable { get; set; }

        /// <summary>
        /// Gets/set the _note attribute
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Gets/set the _note attribute
        /// </summary>
        public bool DoNotDelete { get; set; }

        /// <summary>
        /// The companies functional description written in english language
        /// </summary>
        public string funcDescriptioEN { get { return _funcDescriptioEN; } }


        public string LegalFormNative { get; set; }
        public string LegalFormEn { get; set; }
      
    }
}