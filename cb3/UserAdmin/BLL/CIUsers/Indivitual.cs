using System;

namespace UserAdmin.BLL.CIUsers {
    /// <summary>
    /// Summary description for Indivitual.
    /// </summary>
    public class Indivitual : CreditInfoUser {
        /// <summary>
        /// Gets/sets the Firstnamenative attribute
        /// </summary>
        public String FirstNameNative { get; set; }

        /// <summary>
        /// Gets/sets the _firstNameEN attribute
        /// </summary>
        public String FirstNameEN { get; set; }

        /// <summary>
        /// Gets/sets the _surNameNative attribute
        /// </summary>
        public String SurNameNative { get; set; }

        /// <summary>
        /// Gets/sets the _surNameEN attribute
        /// </summary>
        public String SurNameEN { get; set; }

        /// <summary>
        /// Gets/sets the _professionID attribute
        /// </summary>
        public int ProfessionID { get; set; }

        /// <summary>
        /// Gets/sets the _educationID attribute
        /// </summary>
        public int EducationID { get; set; }

        /// <summary>
        /// Gets/sets the _idNumber attribute
        /// </summary>
        public int IDNumber { get; set; }

        /// <summary>
        /// Gets/sets the _professionNameEN attribute
        /// </summary>
        public String ProfessionNameEN { get; set; }

        /// <summary>
        /// Gets/sets the _professionNameNative attribute
        /// </summary>
        public String ProfessionNameNative { get; set; }

        /// <summary>
        /// Gets/sets the _professionDescriptionEN attribute
        /// </summary>
        public String ProfessionDescriptionEN { get; set; }

        /// <summary>
        /// Gets/sets the _professionDescriptionNative attribute
        /// </summary>
        public String ProfessionDescriptionNative { get; set; }

        /// <summary>
        /// Gets/sets the _educationEN attribute
        /// </summary>
        public String EducationEN { get; set; }

        /// <summary>
        /// Gets/sets the _educationNative attribute
        /// </summary>
        public String EducationNative { get; set; }

        /// <summary>
        /// Gets/sets the _educationDescriptionEN attribute
        /// </summary>
        public String EducationDescriptionEN { get; set; }

        /// <summary>
        /// Gets/sets the _educationDescriptionNative attribute
        /// </summary>
        public String EducationDescriptionNative { get; set; }

        /// <summary>
        /// Gets/sets the _fathersName attribute
        /// </summary>
        public String FathersName { get; set; }

        /// <summary>
        /// Gets/sets the _dateOfBirth attribute
        /// </summary>
        public String DateOfBirth { get; set; }

        /// <summary>
        /// Gets/sets the _whitelistAddress attribute
        /// </summary>
        public String WhitelistAddress { get; set; }

        /// <summary>
        /// Gets/sets the _whitelistIDNumber attribute
        /// </summary>
        public String WhitelistIDNumber { get; set; }

        /// <summary>
        /// Gets/sets the _whitelistCity attribute
        /// </summary>
        public String WhitelistCity { get; set; }

        /// <summary>
        /// Gets/sets the _singleAddressNative attribute
        /// </summary>
        public String SingleAddressNative { get; set; }

        /// <summary>
        /// Gets/sets the _singleAddressEN attribute
        /// </summary>
        public String SingleAddressEN { get; set; }

        /// <summary>
        /// Gets/sets the _nationalID attribute
        /// </summary>
        public String NationalID { get; set; }

        /// <summary>
        /// Gets/sets the _singleCityNative attribute
        /// </summary>
        public String SingleCityNative { get; set; }

        /// <summary>
        /// Gets/sets the _singleCityEN attribute
        /// </summary>
        public String SingleCityEN { get; set; }

        /// <summary>
        /// Gets/sets the _isSearchable attribute
        /// </summary>
        public bool IsSearchable { get; set; }

        /// <summary>
        /// Gets/sets the IsPEP attribute
        /// </summary>
        public bool IsPEP { get; set; }

        /// <summary>
        /// Gets/sets the IsGDPRConsent attribute
        /// </summary>
        public bool IsGDPRConsent { get; set; }

        /// Gets/sets the DoNotDelete attribute
        /// </summary>
        public bool DoNotDelete { get; set; }

        /// <summary>
        /// Gets/sets the _note attribute
        /// </summary>
        public String Note { get; set; }
    }
}