namespace UserAdmin.BLL.CIUsers {
    /// <summary>
    /// Summary description for VatNumber.
    /// </summary>
    public class VatNumber {
        public int Id { get; set; }
        public int CreditInfoID { get; set; }
        public string Number { get; set; }
        public string TraderName { get; set; }
        public string Building { get; set; }
        public string Address { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Note { get; set; }
        public bool Show { get; set; }
        /// <summary>
        /// Return formated address as string with coma separators and spaces.
        /// </summary>
        public string FullAddressString 
        {
            get 
            {
                string retVal = (string.IsNullOrEmpty(this.Building) ? string.Empty : this.Building + ", ") ;
                retVal += (string.IsNullOrEmpty(this.Address) ? string.Empty : this.Address + ", ") ;
                retVal += (string.IsNullOrEmpty(this.Street) ? string.Empty : this.Street + ", ") ;
                retVal += (string.IsNullOrEmpty(this.City) ? string.Empty : this.City + " ") ;
          //      retVal += (string.IsNullOrEmpty(this.PostCode) ? string.Empty : this.PostCode);
                retVal = retVal.Trim();
                if(retVal.EndsWith(","))
                {
                    retVal = retVal.Remove(retVal.Length - 1);
                }
                return retVal;
            }
        }
        /// <summary>
        /// Return text presentation for Showing.
        /// </summary>
        public string ShowTextFlag { get { if (this.Show) return "Yes"; else return "Not shown"; } }
        /// <summary>
        /// Return VAT number with prefif MT.
        /// </summary>
        public string MTNumber { get 
        {
            if (!string.IsNullOrEmpty(this.Number))
            {
                return /*"MT " + */this.Number;
            }
            else
            {
                return string.Empty;
            }
        } 
        }
    }
}