using System;

namespace UserAdmin.BLL.CIUsers {
    /// <summary>
    /// Summary description for PhoneNumber.
    /// </summary>
    [Serializable]
    public class PhoneNumber {
        public int NumberID { get; set; }
        public int NumberTypeID { get; set; }
        public string Number { get; set; }
        public int CreditInfoID { get; set; }
        public string NumberTypeNative { get; set; }
        public string NumberTypeEN { get; set; }
    }
}