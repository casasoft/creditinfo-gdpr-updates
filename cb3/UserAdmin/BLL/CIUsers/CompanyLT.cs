using System;

namespace UserAdmin.BLL.CIUsers {
    /// <summary>
    /// Summary description for CompanyLT.
    /// </summary>
    public class CompanyLT : CreditInfoUser {
        /// <summary>
        /// The companies functional description written in native language
        /// </summary>
        private string _funcDescriptionNative;

        //private DateTime registered;
        //private DateTime registered;
        /// <summary>
        /// Gets/set the _nameNative attribute
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// Gets/set the _nameEN attribute
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Gets/set the _established attribute
        /// </summary>
        public string Established { get; set; }

        /// <summary>
        /// Gets/set the _lastContacted attribute
        /// </summary>
        public DateTime LastContacted { get; set; }

        /// <summary>
        /// Gets/set the _URL attribute
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// Gets/set the _funcID attribute
        /// </summary>
        public string FuncID { get; set; }

        /// <summary>
        /// Gets/set the __funcDescriptionShortNative attribute
        /// </summary>
        public string FuncDescriptionShortNative { get; set; }

        /// <summary>
        /// Gets/set the _funcDescriptionShortEN attribute
        /// </summary>
        public string FuncDescriptionShortEN { get; set; }

        /// <summary>
        /// Gets/set the _funcDescriptionNative attribute
        /// </summary>
        public string FuncDescriptionNative { get { return _funcDescriptionNative; } set { _funcDescriptionNative = value; } }

        /// <summary>
        /// Gets/set the _funcDescriptionNative attribute
        /// </summary>
        public string FuncDescriptionEN { get { return _funcDescriptionNative; } set { _funcDescriptionNative = value; } }

        /// <summary>
        /// Gets/set the _nationalID attribute
        /// </summary>
        public string NationalID { get; set; }

        /// <summary>
        /// Gets/set the _newnationalID attribute
        /// </summary>
        public string NewNationalID { get; set; }

        /// <summary>
        /// Gets/set the _org_status_code attribute
        /// </summary>
        public int Org_status_code { get; set; }

        /// <summary>
        /// Gets/set the _org_name_status_code attribute
        /// </summary>
        public string Org_name_status_code { get; set; }

        /// <summary>
        /// Gets/set the _org_status_date attribute
        /// </summary>
        public DateTime Org_status_date { get; set; }

        /// <summary>
        /// Get/sets the single Address Native attribute
        /// </summary>
        public string SingleAddressNative { get; set; }

        /// <summary>
        /// Get/set the Single Address EN attribute
        /// </summary>
        public string SingleAddressEN { get; set; }

        /// <summary>
        /// Gets/sets the registrationFormID attribute
        /// </summary>
        public int RegistrationFormID { get; set; }

        public int LERStatusID { get; set; }
        public int SLERStatusID { get; set; }
        public int UniqueID { get; set; }
        public string TypeNative { get; set; }
        public string TypeEN { get; set; }
        public string LERStatusNative { get; set; }
        public string LERStatusEN { get; set; }
        public string SLERStatusNative { get; set; }
        public string SLERStatusEN { get; set; }
        public string CityNative { get; set; }
        public string CityEN { get; set; }
        public new string Email { get; set; }
        public string Tel { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string JurAddress { get; set; }
        public string OfficeAddress { get; set; }
        public string Director { get; set; }
        public string DirectorAddress { get; set; }
        public string DirectorPhone { get; set; }
        public string Registered { get; set; }
        public string NaceCodeName { get; set; }
        public string NaceCodeNameEN { get; set; }
        public string WWW { get; set; }
    }
}