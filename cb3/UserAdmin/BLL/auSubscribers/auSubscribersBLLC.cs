using System;

namespace UserAdmin.BLL.auSubscribers {
    /// <summary>
    /// Summary description for auSubscribersBLLC.
    /// </summary>
    public class auSubscribersBLLC {
        public int ID { get; set; }
        public int CreditInfoID { get; set; }
        public int RegisteredBy { get; set; }
        public string SubscriberNickName { get; set; }
        public int NationalID { get; set; }
        public int MaxUsers { get; set; }
        public string isOpen { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
        public bool isPrintBlocked { get; set; }
    }
}