using System;

namespace UserAdmin.BLL.auProducts {
    /// <summary>
    /// Summary description for auProducts.
    /// </summary>
    public class auProductsBLLC {
        public int UserID { get; set; }
        public int ProductID { get; set; }
        public DateTime Created { get; set; }
    }
}