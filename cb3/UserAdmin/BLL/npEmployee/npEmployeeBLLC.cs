using System;

namespace UserAdmin.BLL.npEmployee {
    /// <summary>
    /// Summary description for npEmployee.
    /// </summary>
    public class npEmployeeBLLC {
        public string Initials { get; set; }
        public int CreditInfoID { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}