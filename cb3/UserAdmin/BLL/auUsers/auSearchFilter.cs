﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserAdmin.BLL.auUsers
{
    public class auSearchFilter
    {

        public bool isInd { get; set; }
        public bool isIndVat { get; set; }
        public bool isComp { get; set; }

        public int filterId { get; set; }
        public bool isSubscriber { get; set; }

    }
}