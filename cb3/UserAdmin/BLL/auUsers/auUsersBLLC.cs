using System;

namespace UserAdmin.BLL.auUsers {
    /// <summary>
    /// Summary description for auUsersBLLC.
    /// </summary>
    public class auUsersBLLC {
        public int ID { get; set; }
        public int CreditInfoID { get; set; }
        public string UserName { get; set; }
        public int SubscriberID { get; set; }

        /// <summary>
        /// Unique ID of the department that the user belongs to.
        /// </summary>
        public int DepartmentID { get; set; }

        public int NationalID { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
        public string Groups { get; set; }
        public string UserType { get; set; }
        public int CntCreditWatch { get; set; }
        public int RegisteredBy { get; set; }
        public string HasWebServices { get; set; }
        public string isOpen { get; set; }
        public DateTime OpenUntil { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool lockUserAccount { get; set; }
        public string GroupCode { get; set; }
    }
}