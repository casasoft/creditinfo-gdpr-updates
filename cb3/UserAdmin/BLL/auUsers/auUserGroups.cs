﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.UserAdmin.BLL.auUsers
{
    public class auUserGroups
    {
        public int ID { get; set; }
        public string GroupName { get; set; }
        public string GroupCode { get; set; }
        public string GroupDesc { get; set; }
    }
}