using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using UserAdmin.Localization;

using Cig.Framework.Base.Configuration;
using System.Collections.Generic;

namespace UserAdmin {
    /// <summary>
    /// Summary description for NewDebtor.
    /// </summary>
    public class NewDebtor : Page {
        //private bool newRegistration = true;
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        private int _departmentID = -1;
        protected Button btCancel;
        protected Button btnCity2Search;
        protected Button btnCitySearch;
        protected Button btnContinue;
        protected Button btnSave;
        protected int creditInfoID = -1;
        protected DropDownList ddCity1;
        protected DropDownList ddCity2;
        protected DropDownList ddEducation;
        protected DropDownList ddNumberType1;
        protected DropDownList ddNumberType2;
        protected DropDownList ddPhoneNumber1Type;
        protected DropDownList ddPhoneNumber2Type;
        protected DropDownList ddPhoneNumber3Type;
        protected DropDownList ddPhoneNumber4Type;
        protected DropDownList ddProfession;
        private bool EN;
        protected bool isCreatingSubscriber;
        protected bool isUpdatingUser;
        protected Label Label1;
        protected Label Label2;
        protected Label Label3;
        protected Label Label4;
        protected Label lbAddress1;
        protected Label lbAddress1EN;
        protected Label lbAddress1Info;
        protected Label lbAddress1InfoEN;
        protected Label lbAddress2;
        protected Label lbAddress2EN;
        protected Label lbAddress2Info;
        protected Label lbAddress2InfoEN;
        protected Label lbCity1;
        protected Label lbCity2;
        protected Label lbDebtorCIID;
        protected Label lbDebtorFirstName;
        protected Label lbDebtorIDNumber3;
        protected Label lbDebtorSurName;
        protected Label lbEducation;
        protected Label lbEmail;
        protected Label lbFirstNameEN;
        protected Label lbIDNumber2;
        protected Label lbIDNumberType;
        protected Label lbIDNumberType2;
        protected Label lblAddressOne;
        protected Label lblAddressTwo;
        protected Label lblCity2Search;
        protected Label lblCitySearch;
        protected Label lblError;
        protected Label lblIsSearchable;
        protected Label lblNote;
        protected Label lblOtherInfo;
        protected Label lbNewIndividual;
        protected Label lbPhoneNumber1;
        protected Label lbPhoneNumber2;
        protected Label lbPhonenumber2Type;
        protected Label lbPhoneNumber3;
        protected Label lbPhoneNumber3Type;
        protected Label lbPhoneNumber4;
        protected Label lbPhoneNumber4Type;
        protected Label lbPhonNumber1Type;
        protected Label lbPostalCode;
        protected Label lbPostBox;
        protected Label lbPostBox2;
        protected Label lbProfession;
        protected Label lbSurnameEN;
        protected Label lbUserUpdated;
        protected Label ltPostalCode2;
        protected string nationalID = "";
        protected bool newUser;
        protected RadioButton rbtIsSearchableFalse;
        protected RadioButton rbtIsSearchableTrue;
        public String redirectPage = "";
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected RequiredFieldValidator rfvCity;
        protected int subscriberCIID = -1;
        protected TextBox tbAddress1;
        protected TextBox tbAddress1EN;
        protected TextBox tbAddress1Info;
        protected TextBox tbAddress1InfoEN;
        protected TextBox tbAddress2;
        protected TextBox tbAddress2EN;
        protected TextBox tbAddress2Info;
        protected TextBox tbAddress2InfoEN;
        protected TextBox tbDebtorCIID;
        protected TextBox tbDebtorFirstName;
        protected TextBox tbDebtorSurName;
        protected TextBox tbEmail;
        protected TextBox tbFirstNameEN;
        protected TextBox tbIDNumber1;
        protected TextBox tbIDNumber2;
        protected TextBox tbNote;
        protected TextBox tbPhoneNumber1;
        protected TextBox tbPhoneNumber2;
        protected TextBox tbPhoneNumber3;
        protected TextBox tbPhoneNumber4;
        protected TextBox tbPostalCode1;
        protected TextBox tbPostalCode2;
        protected TextBox tbPostBox1;
        protected TextBox tbPostBox2;
        protected TextBox tbSurnameEN;
        protected TextBox txtCity2Search;
        protected TextBox txtCitySearch;
        protected CheckBox chkIsPEP;
        protected CheckBox chkIsGDPRConsent;
        protected CheckBox chkdoNotDelete;
        protected bool update;
        protected ValidationSummary ValidationSummary1;
        protected DataGrid dgVatNumbers;
        protected Panel tblVatNumbers;

        private void Page_Load(object sender, EventArgs e) {
            try {
                subscriberCIID = int.Parse(Request["SubscriberCIID"]);
            } catch {
                subscriberCIID = -1;
            }
            try {
                creditInfoID = int.Parse(Request["CreditInfoID"]);
            } catch {
                creditInfoID = -1;
            }
            try {
                _departmentID = int.Parse(Request["Department"]);
            } catch {}
            try {
                update = bool.Parse(Request["Update"]);
            } catch {
                update = false;
            }
            try {
                isUpdatingUser = bool.Parse(Request["UpdateUser"]);
            } catch {
                isUpdatingUser = false;
            }
            try {
                nationalID = Request["NationalID"];
            } catch {
                nationalID = "";
            }
            try {
                isCreatingSubscriber = bool.Parse(Request["CreateSubscriber"]);
            } catch {
                isCreatingSubscriber = false;
            }

            if (subscriberCIID == -1 && !isCreatingSubscriber && !isUpdatingUser) {
                newUser = true;
            }
            //Server.Transfer("FindSubscribers.aspx");

            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            lblError.Visible = false;
            lbUserUpdated.Visible = false;
            AddEnterEvent();
            Page.ID = "111";

            if (!IsPostBack) {
                InitDDBoxes();
                initControls(update);
            }
            LocalizeText();
        }

        private void initControls(bool update)
        {
            btnContinue.Enabled = subscriberCIID > -1;
            if (newUser)
            {
                return;
            }
            if (update)
            {
                if (creditInfoID != -1)
                {
                    Indivitual myIndi = myFactory.GetIndivitual(creditInfoID);
                    tbDebtorCIID.Text = creditInfoID.ToString();
                    tbDebtorSurName.Text = myIndi.SurNameNative;
                    tbSurnameEN.Text = myIndi.SurNameEN;
                    tbEmail.Text = myIndi.Email;
                    tbDebtorFirstName.Text = myIndi.FirstNameNative;
                    tbFirstNameEN.Text = myIndi.FirstNameEN;
                    tbNote.Text = myIndi.Note;
                    rbtIsSearchableTrue.Checked = myIndi.IsSearchable;
                    rbtIsSearchableFalse.Checked = !myIndi.IsSearchable;
                    chkIsPEP.Checked = myIndi.IsPEP;
                    chkIsGDPRConsent.Checked = myIndi.IsGDPRConsent;
                    chkdoNotDelete.Checked = myIndi.DoNotDelete;
                    if (myIndi.ProfessionID != 0)
                    {
                        ddProfession.SelectedValue = myIndi.ProfessionID.ToString();
                    }
                    //else 
                    // stilla h�r � default N/A gildi
                    if (myIndi.EducationID != 0)
                    {
                        ddEducation.SelectedValue = myIndi.EducationID.ToString();
                    }
                    //else 
                    // stilla h�r � default N/A gildi
                    initMultiItems(myIndi);

                    if (!isUpdatingUser)
                    {
                        btnContinue.Enabled = true;
                    }

                    this.getVatNumbers(creditInfoID);
                }
            }
            else
            {
                if (nationalID != "")
                {
                    //Load user data from national registry
                    if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True")
                    {
                        var myIndi = myFactory.GetIndivitualFromNationalRegistry(nationalID);
                        tbIDNumber1.Text = myIndi.NationalID;
                        ddNumberType1.SelectedIndex = 0;

                        tbDebtorFirstName.Text = myIndi.FirstNameNative;
                        tbDebtorSurName.Text = myIndi.SurNameNative;
                        if (myIndi.Address.Count > 0)
                        {
                            var address = (Address)myIndi.Address[0];
                            tbAddress1.Text = address.StreetNative;
                            tbPostalCode1.Text = address.PostalCode;
                        }
                    }
                }
            }
        }

        private void initMultiItems(CreditInfoUser myIndi) {
            var arrAdd = myIndi.Address;
            var arrIDNum = myIndi.IDNumbers;
            var arrPNum = myIndi.Number;
            var frm = FindControl("_newuser");
            for (int i = 1; i <= arrAdd.Count; i++) {
                var myAddr = (Address) arrAdd[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbAddress" + i) {
                            ((TextBox) ctrl).Text = myAddr.StreetNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "EN") {
                            ((TextBox) ctrl).Text = myAddr.StreetEN;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "Info") {
                            ((TextBox) ctrl).Text = myAddr.OtherInfoNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "InfoEN") {
                            ((TextBox) ctrl).Text = myAddr.OtherInfoEN;
                        }
                        if ((ctrl).ID == "tbPostalCode" + i) {
                            ((TextBox) ctrl).Text = myAddr.PostalCode;
                        }
                        if ((ctrl).ID == "tbPostBox" + i) {
                            ((TextBox) ctrl).Text = myAddr.PostBox;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID != "ddCity" + i) {
                        continue;
                    }
                    var cityID = myAddr.CityID;
                    if (cityID <= -1) {
                        continue;
                    }
                    var cityName = myFactory.GetCityName(cityID, EN);
                    if (string.IsNullOrEmpty(cityName)) {
                        continue;
                    }
                    ((DropDownList) ctrl).Items.Add(new ListItem(cityName, cityID.ToString()));
                    ((DropDownList) ctrl).SelectedValue = cityID.ToString();
                    //((DropDownList)ctrl).SelectedValue = myAddr.CityID.ToString();
                    //	((DropDownList)ctrl).SelectedValue = myAddr.CityID.ToString();
                }
            }
            for (int i = 1; i <= arrIDNum.Count; i++) {
                var myIDNum = (IDNumber) arrIDNum[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbIDNumber" + i) {
                            ((TextBox) ctrl).Text = myIDNum.Number;
                        }
                    }
                    if (ctrl is DropDownList) {
                        if ((ctrl).ID == "ddNumberType" + i) {
                            ((DropDownList) ctrl).SelectedValue = myIDNum.NumberTypeID.ToString();
                        }
                    }
                }
            }
            for (int i = 1; i <= arrPNum.Count; i++) {
                var myPNum = (PhoneNumber) arrPNum[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbPhoneNumber" + i) {
                            ((TextBox) ctrl).Text = myPNum.Number;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID == "ddPhoneNumber" + i + "Type") {
                        ((DropDownList) ctrl).SelectedValue = myPNum.NumberTypeID.ToString();
                    }
                }
            }
        }

        private void InitDDBoxes() {
            var myFactory5 = new uaFactory();
            BindPhoneNumberTypes(myFactory5);
            BindIDNumberTypes(myFactory5);
            BindEducationTypes(myFactory5);
            BindProfessionTypes(myFactory5);
            //BindCityList(myFactory);
        }

        private void BindPhoneNumberTypes(uaFactory myFactory) {
            var phoneTypeSet = myFactory.GetPNumberTypesAsDataSet();
            ddPhoneNumber1Type.DataSource = phoneTypeSet;
            ddPhoneNumber1Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber1Type.DataValueField = "NumberTypeID";
            ddPhoneNumber1Type.DataBind();

            ddPhoneNumber2Type.DataSource = phoneTypeSet;
            ddPhoneNumber2Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber2Type.DataValueField = "NumberTypeID";
            ddPhoneNumber2Type.DataBind();

            ddPhoneNumber3Type.DataSource = phoneTypeSet;
            ddPhoneNumber3Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber3Type.DataValueField = "NumberTypeID";
            ddPhoneNumber3Type.DataBind();

            ddPhoneNumber4Type.DataSource = phoneTypeSet;
            ddPhoneNumber4Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber4Type.DataValueField = "NumberTypeID";
            ddPhoneNumber4Type.DataBind();
        }

        private void BindIDNumberTypes(uaFactory myFactory) {
            var idNumberType = myFactory.GetIDNumberTypesAsDataSet();

            ddNumberType1.DataSource = idNumberType;
            ddNumberType1.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddNumberType1.DataValueField = "NumberTypeID";
            ddNumberType1.DataBind();

            ddNumberType2.DataSource = idNumberType;
            ddNumberType2.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddNumberType2.DataValueField = "NumberTypeID";
            ddNumberType2.DataBind();
        }

        private void BindEducationTypes(uaFactory myFactory) {
            var educationTypeSet = myFactory.GetEducationListAsDataSet();
            ddEducation.DataSource = educationTypeSet;

            ddEducation.DataTextField = EN ? "NameEN" : "NameNative";

            ddEducation.DataValueField = "EducationID";
            ddEducation.DataBind();
        }

        private void BindProfessionTypes(uaFactory myFactory) {
            var professionTypeSet = myFactory.GetProfessioListAsDataSet();
            ddProfession.DataSource = professionTypeSet;
            ddProfession.DataTextField = EN ? "NameEN" : "NameNative";
            ddProfession.DataValueField = "ProfessionID";
            ddProfession.DataBind();
        }

        private void BindCityList(DataSet citySet) {
            ddCity1.DataSource = citySet;
            ddCity1.DataTextField = EN ? "NameEN" : "NameNative";

            ddCity1.DataValueField = "CityID";
            ddCity1.DataBind();
        }

        private void BindCity2List(DataSet citySet) {
            ddCity2.DataSource = citySet;
            ddCity2.DataTextField = EN ? "NameEN" : "NameNative";

            ddCity2.DataValueField = "CityID";
            ddCity2.DataBind();
        }

        public ArrayList GetAddresses() {
            var myAddr = new Address();
            var address = new ArrayList();
            if (tbAddress1.Text != "" || tbAddress1EN.Text != "" || tbAddress1Info.Text != "" ||
                tbAddress1InfoEN.Text != "") {
                if (ddCity1.SelectedItem.Value.Trim() != "") {
                    myAddr.CityID = int.Parse(ddCity1.SelectedItem.Value);
                }
                myAddr.OtherInfoNative = tbAddress1Info.Text;
                myAddr.OtherInfoEN = tbAddress1InfoEN.Text;
                myAddr.PostalCode = tbPostalCode1.Text;
                myAddr.PostBox = tbPostBox1.Text;
                myAddr.StreetNative = tbAddress1.Text;
                myAddr.StreetEN = tbAddress1EN.Text;
               // myAddr.IsTradingAddress = false;
                address.Add(myAddr);
            }
            if (tbAddress2.Text != "" || tbAddress2EN.Text != "" || tbAddress2Info.Text != "" ||
                tbAddress2InfoEN.Text != "") {
                myAddr = new Address();

                if (ddCity2.SelectedItem != null)
                {
                    if (ddCity2.SelectedItem.Value.Trim() != "")
                    {
                        myAddr.CityID = int.Parse(ddCity2.SelectedItem.Value);
                    }
                }
                myAddr.OtherInfoNative = tbAddress2Info.Text;
                myAddr.OtherInfoEN = tbAddress2InfoEN.Text;
                myAddr.PostalCode = tbPostalCode2.Text;
                myAddr.PostBox = tbPostBox2.Text;
                myAddr.StreetNative = tbAddress2.Text;
                myAddr.StreetEN = tbAddress2EN.Text;
               // myAddr.IsTradingAddress = true;
                address.Add(myAddr);
            }
            return address;
        }

        public ArrayList GetIDNumbers() {
            var myIDNum = new IDNumber();
            var aNumbers = new ArrayList();
            if (tbIDNumber1.Text != "") {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber1.Text,
                              NumberTypeID = int.Parse(ddNumberType1.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            if (tbIDNumber2.Text != "") {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber2.Text,
                              NumberTypeID = int.Parse(ddNumberType2.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            return aNumbers;
        }

        public ArrayList GetPhoneNumbers() {
            var myNum = new PhoneNumber();
            var aNumbers = new ArrayList();
            if (tbPhoneNumber1.Text != "") {
                myNum.Number = tbPhoneNumber1.Text;
                myNum.NumberTypeID = int.Parse(ddPhoneNumber1Type.SelectedItem.Value);
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber2.Text != "") {
                myNum = new PhoneNumber
                        {
                            Number = tbPhoneNumber2.Text,
                            NumberTypeID = int.Parse(ddPhoneNumber2Type.SelectedItem.Value)
                        };
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber3.Text != "") {
                myNum = new PhoneNumber
                        {
                            Number = tbPhoneNumber3.Text,
                            NumberTypeID = int.Parse(ddPhoneNumber3Type.SelectedItem.Value)
                        };
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber4.Text != "") {
                myNum = new PhoneNumber
                        {
                            Number = tbPhoneNumber4.Text,
                            NumberTypeID = int.Parse(ddPhoneNumber4Type.SelectedItem.Value)
                        };
                aNumbers.Add(myNum);
            }
            return aNumbers;
        }

        private void LocalizeText() {
            if (!isUpdatingUser) //newRegistration)
            {
                if (isCreatingSubscriber) {
                    lbNewIndividual.Text = rm.GetString("txtCreatingSubscriber", ci) + " - " +
                                           rm.GetString("txtNewIndividual", ci);
                } else {
                    lbNewIndividual.Text = rm.GetString("txtAddUser", ci) + " - " + rm.GetString("txtNewIndividual", ci);
                }
            } else {
                if (isUpdatingUser) {
                    lbNewIndividual.Text = rm.GetString("txtUpdatingUser", ci);
                } else if (isCreatingSubscriber) {
                    lbNewIndividual.Text = rm.GetString("txtCreatingSubscriber", ci) + " - " +
                                           rm.GetString("txtUpdatingUser", ci);
                } else {
                    lbNewIndividual.Text = rm.GetString("txtAddUser", ci) + " - " + rm.GetString("txtUpdatingUser", ci);
                }
            }

            if (newUser) {
                lbNewIndividual.Text = rm.GetString("txtAddUser", ci) + " - " + rm.GetString("txtNewIndividual", ci);
            }

            lbDebtorSurName.Text = rm.GetString("txtSurName", ci);
            lbDebtorFirstName.Text = rm.GetString("txtFirstName", ci);
            btnSave.Text = rm.GetString("txtSave", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            btCancel.Text = rm.GetString("txtCancel", ci);
            lbIDNumber2.Text = rm.GetString("txtIDNumber2", ci);
            lbIDNumberType.Text = rm.GetString("txtNumberType", ci);
            lbDebtorIDNumber3.Text = rm.GetString("txtIDNumber3", ci);
            lbIDNumberType2.Text = rm.GetString("txtNumberType", ci);
            lbSurnameEN.Text = rm.GetString("txtSurnameEN", ci);
            lbFirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lbPostalCode.Text = rm.GetString("txtPostalCode", ci);
            lbPostBox.Text = rm.GetString("txtPostBox", ci);
            lbAddress2.Text = rm.GetString("txtAddress2", ci);
            lbCity2.Text = rm.GetString("txtCity2", ci);
            lbAddress1.Text = rm.GetString("txtAddress1", ci);
            ltPostalCode2.Text = rm.GetString("txtPostalCode2", ci);
            lbProfession.Text = rm.GetString("txtProfession", ci);
            lbEducation.Text = rm.GetString("txtEducation", ci);
            lbPostBox2.Text = rm.GetString("txtPostBox2", ci);
            lbAddress2Info.Text = rm.GetString("txtAddressInfo2", ci);
            lbAddress1Info.Text = rm.GetString("txtAddressInfo1", ci);
            lbPhoneNumber1.Text = rm.GetString("txtPhoneNumber1", ci);

            lbPhonNumber1Type.Text = rm.GetString("txtNumberType", ci);
            lbPhoneNumber2.Text = rm.GetString("txtPhoneNumber2", ci);
            lbPhonenumber2Type.Text = rm.GetString("txtNumberType", ci);
            lbPhoneNumber3.Text = rm.GetString("txtPhoneNumber3", ci);
            lbPhoneNumber3Type.Text = rm.GetString("txtNumberType", ci);
            lbPhoneNumber4.Text = rm.GetString("txtPhoneNumber4", ci);
            lbPhoneNumber4Type.Text = rm.GetString("txtNumberType", ci);
            lbDebtorCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lbAddress1EN.Text = rm.GetString("txtAddressEN1", ci);
            lbCity1.Text = rm.GetString("txtCity1", ci);
            lbAddress1InfoEN.Text = rm.GetString("txtAddressInfoEN1", ci);
            lbAddress2EN.Text = rm.GetString("txtAddressEN2", ci);
            lbAddress2InfoEN.Text = rm.GetString("txtAddressInfoEN2", ci);
            RequiredFieldValidator1.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator3.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lbUserUpdated.Text = rm.GetString("txtUserUpdated", ci);
            lbUserUpdated.Text = rm.GetString("txtNewUserCreated", ci);
            rfvCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblCitySearch.Text = rm.GetString("txtSearch", ci);
            lblCity2Search.Text = rm.GetString("txtSearch", ci);
            lbEmail.Text = rm.GetString("txtEmail", ci);
            lblNote.Text = rm.GetString("lblNote", ci);

            lblAddressOne.Text = rm.GetString("txtAddress1", ci);
            lblAddressTwo.Text = rm.GetString("txtAddress2", ci);
            lblOtherInfo.Text = rm.GetString("txtOtherInfo", ci);

            lblIsSearchable.Text = rm.GetString("txtIsSearchable", ci);
            rbtIsSearchableTrue.Text = rm.GetString("txtTrue", ci);
            rbtIsSearchableFalse.Text = rm.GetString("txtFalse", ci);
        }

        private void AddEnterEvent() {
            Control frm = FindControl("_newuser");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            var myFactory = new uaFactory();
            var theIndi = new Indivitual
                          {
                              Type = CigConfig.Configure("lookupsettings.individualID"),
                              EducationID = int.Parse(ddEducation.SelectedItem.Value),
                              FirstNameNative = tbDebtorFirstName.Text,
                              FirstNameEN = tbFirstNameEN.Text,
                              SurNameNative = tbDebtorSurName.Text,
                              SurNameEN = tbSurnameEN.Text,
                              Email = tbEmail.Text,
                              IsSearchable = rbtIsSearchableTrue.Checked,
                              Note = tbNote.Text,
                              ProfessionID = int.Parse(ddProfession.SelectedItem.Value),
                              IsPEP = chkIsPEP.Checked,
                              IsGDPRConsent = chkIsGDPRConsent.Checked,
                              DoNotDelete = chkdoNotDelete.Checked
            };

            if (update)
            {
                theIndi.CreditInfoID = int.Parse(tbDebtorCIID.Text);
            }

            // collections
            theIndi.Address = GetAddresses();
            theIndi.IDNumbers = GetIDNumbers();
            theIndi.Number = GetPhoneNumbers();
            bool regOK = true;
            if (!update)
            {
                creditInfoID = myFactory.addNewIndivitual(theIndi);
                if (creditInfoID < 0)
                {
                    lblError.Text = rm.GetString("txtFailedCreatingUser", ci);
                    lblError.Visible = true;
                    regOK = false;
                    return;
                }
                lbUserUpdated.Text = rm.GetString("txtNewUserCreated", ci);
                lbUserUpdated.Visible = true;
            }
            else
            {
                theIndi.LastUpdate = DateTime.Now;
                creditInfoID = myFactory.UpdateIndivitual(theIndi);
                if (creditInfoID < 0)
                {
                    lblError.Text = rm.GetString("txtFailedUpdatingUser", ci);
                    lblError.Visible = true;
                    regOK = false;
                    return;
                }
                lbUserUpdated.Text = rm.GetString("txtUserUpdated", ci);
                lbUserUpdated.Visible = true;
            }

            if (regOK)
            {
                tbDebtorCIID.Text = creditInfoID.ToString();
                if (subscriberCIID > -1)
                {
                    //	this.btnContinue.Enabled = true;
                    if (isCreatingSubscriber)
                    {
                        Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + tbDebtorCIID.Text);
                    }
                    else if (newUser)
                    {
                        Server.Transfer("FindCIUser.aspx");
                    }
                    else
                    {
                        Server.Transfer(
                            "UserDetails.aspx?CreditInfoID=" + tbDebtorCIID.Text + "&SubscriberCIID=" +
                            subscriberCIID + "&Department=" + _departmentID);
                    }
                }
                else if (!isUpdatingUser)
                {
                    if (isCreatingSubscriber)
                    {
                        Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + tbDebtorCIID.Text);
                    }

                    btnContinue.Enabled = true;
                }
            }

            initControls(update);
        }

        private void btnContinue_Click(object sender, EventArgs e) {
            if (isCreatingSubscriber) {
                Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + tbDebtorCIID.Text);
            } else if (newUser) {
                Server.Transfer("FindCIUser.aspx");
            } else {
                Server.Transfer(
                    "UserDetails.aspx?CreditInfoID=" + tbDebtorCIID.Text + "&SubscriberCIID=" + subscriberCIID +
                    "&Department=" + _departmentID);
            }
        }

        private void btCancel_Click_1(object sender, EventArgs e) {
            if (isUpdatingUser) {
                Server.Transfer("FindCIUser.aspx?UpdateUser=True");
            } else {
                Server.Transfer("FindSubscribers.aspx");
            }
        }

        private void btnCitySearch_Click(object sender, EventArgs e) {
            if (txtCitySearch.Text.Trim() != "") {
                DataSet dsCity = myFactory.GetCityListAsDataSet(txtCitySearch.Text, EN);
                BindCityList(dsCity);
            }
        }

        private void btnCity2Search_Click(object sender, EventArgs e) {
            if (txtCity2Search.Text.Trim() != "") {
                DataSet dsCity = myFactory.GetCityListAsDataSet(txtCity2Search.Text, EN);
                BindCity2List(dsCity);
            }
        }

        /// <summary>
        /// Get vat numbers data and bind it to datagrid.
        /// </summary>
        /// <param name="creditInfoId"></param>
        private void getVatNumbers(int creditInfoId)
        {
            IList<VatNumber> listOfVatNumbers = this.myFactory.GetVatNumbersByCreditinfoId(creditInfoId);
            if (listOfVatNumbers.Count == 0)
            {
                this.tblVatNumbers.Visible = false;
            }
            else
            {
                this.tblVatNumbers.Visible = true;
                this.dgVatNumbers.Visible = true;
                this.dgVatNumbers.DataSource = listOfVatNumbers;
                this.dgVatNumbers.DataBind();
            }

        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCitySearch.Click += new System.EventHandler(this.btnCitySearch_Click);
            this.btnCity2Search.Click += new System.EventHandler(this.btnCity2Search_Click);
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click_1);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

        protected void btDelete_Click(object sender, EventArgs e)
        {
            if (update)
            {
                var creditInfoID = int.Parse(tbDebtorCIID.Text);
                Indivitual myIndi = myFactory.GetIndivitual(creditInfoID);

                if(myIndi != null && !myIndi.DoNotDelete)
                {
                    if (myFactory.DeleteIndivitual(creditInfoID))
                    {
                        lbUserUpdated.Text = "Individual successfully deleted";
                        lbUserUpdated.Visible = true;
                    }
                    else
                    {
                        lblError.Text = "An Error Occured While deleting individual record. please check the logs for errors";
                        lblError.Visible = true;
                    }



                }
                else
                {
                    // change this to resource
                    lblError.Text = "Individual Record is set as  'Do Not Delete' therefore it can not be deleted or Individual record do not exist";
                    lblError.Visible = true;
                }
            }

        }
    }
}