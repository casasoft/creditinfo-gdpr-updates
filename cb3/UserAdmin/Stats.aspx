<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Page language="c#" Codebehind="Stats.aspx.cs" AutoEventWireup="True" Inherits="UserAdmin.Stats" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Stats</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						frmStatsMain.btnExecute.click();
					}
				} 
		
				function SetFormFocus()
				{
					document.frmStatsMain.txtFromDate.focus();
				}
		</script>
	</head>
	<body ms_positioning="GridLayout" onload="SetFormFocus()">
		<form id="frmStatsMain" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblPageHeader" runat="server">Statistics</asp:label>-
															<asp:label id="lblQuery" runat="server">Query</asp:label></th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblfromDate" runat="server">From date</asp:label>
																		<br>
																		<asp:textbox id="txtFromDate" runat="server"></asp:textbox>
																		<input onclick="PopupPicker('txtFromDate', 250, 250);" type="button" value="...">
																	</td>
																	<td>
																		<asp:label id="lblToDate" runat="server">To Date</asp:label>
																		<br>
																		<asp:textbox id="txtToDate" runat="server"></asp:textbox>
																		<input onclick="PopupPicker('txtToDate', 250, 250);" type="button" value="...">
																	</td>
																	<td>
																		<asp:label id="lblBeginningPeriod" runat="server">Beginning of registration period</asp:label>
																		<br>
																		<asp:textbox id="txtBeginningDate" runat="server"></asp:textbox>
																		<input onclick="PopupPicker('txtBeginningDate', 250, 250);" type="button" value="...">
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<th>
															<asp:label id="lblResult" runat="server">RESULTS FOR SELECTED PERIOD</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr height="22">
																	<td rowspan="4" width="10"></td>
																	<td>
																		<b>
																			<asp:label id="lblDescription" runat="server">Label</asp:label>
																		</b>
																	</td>
																	<td>
																		<asp:label id="lblBeginToDateName" runat="server">Begin date - To date</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblFromDateToDateName" runat="server">From date - To date</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblDifferenceName" runat="server">Diff.</asp:label>
																	</td>
																</tr>
																<tr height="22">
																	<td>
																		<asp:label id="lblNumberOfRegistrations" runat="server">Number of registrations:</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblRegBeginning" runat="server" width="50px">0</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblFromToReg" runat="server" width="50px">0</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblDiffReg" runat="server" width="50px">0</asp:label>
																	</td>
																</tr>
																<tr height="22">
																	<td>
																		<asp:label id="lblTotalNumOfCompanies" runat="server">Total num. of (distinct) companies:</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblCompBeginning" runat="server" width="50px">0</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblFromToComp" runat="server" width="50px">0</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblDiffComp" runat="server" width="50px">0</asp:label>
																	</td>
																</tr>
																<tr height="22">
																	<td>
																		<asp:label id="lblTotalNumOfIndi" runat="server">Total num. of (distinct) individuals:</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblIndiBeginning" runat="server" width="50px">0</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblFromToIndi" runat="server" width="50px">0</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblDiffIndi" runat="server" width="50px">0</asp:label>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left"></td>
														<td align="right">
															<asp:button id="btnPrint" runat="server" cssclass="gray_button" text="Print" onclick="btnPrint_Click"></asp:button><asp:button id="btnExecute" runat="server" cssclass="confirm_button" text="Execute" onclick="btnExecute_Click"></asp:button>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
						<uc1:secureuseradmin id="SecureUserAdmin1" runat="server"></uc1:secureuseradmin>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
