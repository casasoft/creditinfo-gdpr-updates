﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cb3.UserAdmin.BLL.auUsers;
using Cig.Framework.Base.Configuration;
using Telerik.WebControls;
using UserAdmin.BLL;
using UserAdmin.BLL.auProducts;
using UserAdmin.Localization;

namespace cb3.UserAdmin
{
    public partial class UserGroup1 : System.Web.UI.Page
    {


        private static CultureInfo ci;
        private static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        private bool nativeCult;
        private bool newUserGroup;
        private int groupID;


        protected void Page_Load(object sender, EventArgs e)
        {
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            lblUserRights.Text = rm.GetString("txtUserRights", ci);
            lblInternal.Text = rm.GetString("txtInternal", ci);
            lblExternal.Text = rm.GetString("txtExternal", ci);
            lblMsg.Text = "";
            lblMsg.Visible = false;

            if (Context.User.IsInRole("20001")) 
            {
                Page.ID = "20001";
                internalGroupTree.Visible = true;
                lblInternal.Visible = true;
                tdInternal.Visible = true;
                tdInternal.Style.Clear();
                tdInternal.Style.Add("WIDTH", "50%");
                externalGroupTree.Visible = true;
                lblExternal.Visible = true;
            }

            if (IsPostBack)
            {
                return;
            }

            if (!string.IsNullOrEmpty(Request["newUserGroup"]) && Request["newUserGroup"] == "True")
            {
                newUserGroup = true;
            }


            if (!string.IsNullOrEmpty(Request["GroupID"]))
            {
                groupID = Convert.ToInt32(Request["GroupID"]);
                ID.Value = Request["GroupID"];
                txtGrpCode.ReadOnly = true;
            }



            var culture = Thread.CurrentThread.CurrentCulture.Name;
            var nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");

            if (culture.Equals(nativeCulture))
            {
                nativeCult = true;
            }

            LoadUserGroups();
            GetProducts();

        }

        



        private void LoadUserGroups()
        {
            //ID.Value = "2";  /// testing purpose
            //newUserGroup = false;
            if (groupID!=0 && !newUserGroup)
            {

                var userGroup = myFactory.GetUserGroup(groupID);
                if (userGroup != null)
                {
                    txtName.Text = userGroup.GroupName;
                    txtGrpCode.Text = userGroup.GroupCode;
                    txtGrpDesc.Text = userGroup.GroupDesc;

                }
                else
                {

                    lblMsg.Visible = true;
                    lblMsg.Text = "Error Occured while Loading User Group";
                }
                

            }

        }
        
        private void GetProducts()
        {
            DataSet mySet = myFactory.GetAllTheOpenProducts(false);
            DataView myView = mySet.Tables[0].DefaultView;

            myView.RowFilter = "IsBaseProduct = 'True' and IsInner = 'True'";

            IDictionary internalNodeTable = new Hashtable(myView.Count);
            GenerateTreeView(myView, internalNodeTable, internalGroupTree);

            myView.RowFilter = "IsBaseProduct = 'True' and IsInner = 'False'";

            IDictionary externalNodeTable = new Hashtable(myView.Count);
            GenerateTreeView(myView, externalNodeTable, externalGroupTree);
            var test = externalGroupTree.Nodes;

            if (internalGroupTree.Nodes.Count == 0)
            {
                internalGroupTree.Visible = false;
                lblInternal.Visible = false;
                tdInternal.Visible = false;
            }
            if (externalGroupTree.Nodes.Count == 0)
            {
                externalGroupTree.Visible = false;
                lblExternal.Visible = false;
            }

            if (groupID != 0 && !newUserGroup)
            {
                mySet = myFactory.ReadGroupProducts(groupID);
                if(mySet != null && mySet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < mySet.Tables[0].Rows.Count; i++)
                    {
                        string key = mySet.Tables[0].Rows[i]["ProductID"].ToString();
                        if (internalNodeTable.Contains(key))
                        {
                            ((RadTreeNode)internalNodeTable[key]).Checked = true;
                        }

                        if (externalNodeTable.Contains(key))
                        {
                            ((RadTreeNode)externalNodeTable[key]).Checked = true;
                        }
                    }
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Error Occured while Loading Products";

                }
                
            }
        }

        private void GenerateTreeView(DataView myView, IDictionary nodeTable, RadTreeView tree)
        {
            for (int i = 0; i < myView.Count; i++)
            {
                var node = new RadTreeNode
                {
                    ID = myView[i]["productID"].ToString(),
                    Text =
                    (nativeCult
                        ? myView[i]["ProductNameNative"].ToString()
                        : myView[i]["ProductNameEN"].ToString())
                };
                //node.Expanded = true;

                nodeTable.Add(myView[i]["productID"].ToString(), node);

                if (myView[i]["ParentID"] == DBNull.Value || Convert.ToInt32(myView[i]["ParentID"]) == 0)
                {
                    tree.AddNode(node);
                }
                else
                {
                    var parent = (RadTreeNode)nodeTable[myView[i]["ParentID"].ToString()];
                    if (parent != null)
                    {
                        parent.AddNode(node);
                    }
                    else
                    {
                        tree.AddNode(node);
                    }
                }
            }
        }

        private static void GetCheckedNodeList(IList nodeList, RadTreeNodeCollection nodeCollection)
        {
            foreach (RadTreeNode node in nodeCollection)
            {
                if (node.Checked)
                {
                    nodeList.Add(node);
                }

                if (node.Nodes != null)
                {
                    GetCheckedNodeList(nodeList, node.Nodes);
                }
            }
        }


        private bool WriteGroupProducts(int GroupID)
        {
            IList list = new ArrayList();
            GetCheckedNodeList(list, internalGroupTree.Nodes);
            GetCheckedNodeList(list, externalGroupTree.Nodes);

            if(list.Count == 0)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "No User right was checked";
                return false;
            }

            var myProductArray = new auProductsBLLC[list.Count];

            int counter = 0;

            foreach (RadTreeNode node in list)
            {
                myProductArray[counter] = new auProductsBLLC { UserID = GroupID, ProductID = Convert.ToInt32(node.ID) };
                counter++;
            }

            if (!myFactory.WriteGroupProducts(myProductArray, GroupID))
            {
                return false;
            }

            return true;
        }





        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var groupID = (!string.IsNullOrEmpty(ID.Value)) ? Convert.ToInt32(ID.Value) : 0;
            

            var userGroup = new auUserGroups()
            {
                GroupName = txtName.Text,
                GroupCode = txtGrpCode.Text,
                GroupDesc = txtGrpDesc.Text,
                ID = groupID
            };

            // get all User groups and confirm the User group code does not exsit before
            var userGroups = myFactory.GetAllUserGroups();
            if (userGroups.Tables.Count > 0 && userGroups.Tables[0].Rows.Count > 0 && userGroup.ID == 0)
            {
                foreach (DataRow item in userGroups.Tables[0].Rows)
                {
                    if ((string)item["GroupCode"] == userGroup.GroupCode)
                    {
                        lblMsg.Visible = true;
                        lblMsg.Text = "Group Code Already Exist";
                        break;


                    }

                }
            }


            if (!lblMsg.Visible)
            {

                var result = myFactory.SaveUserGroup(userGroup);
                if (result != 0)
                {
                    if (!WriteGroupProducts(result))
                    {
                        lblMsg.Visible = true;
                        lblMsg.Text = "Error Occured while updating the group products";
                    }

                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Error Occured while updating User group";
                }
            }


            if(!lblMsg.Visible) { Response.Redirect("UserGroupList.aspx"); } // if no visible message 
        }

        

        protected void Delete_Click(object sender, EventArgs e)
        {
            var groupID = (!string.IsNullOrEmpty(ID.Value)) ? Convert.ToInt32(ID.Value) : 0;

            if(groupID != 0)
            {

                if (myFactory.DeleteUserGroup(groupID))
                {
                    if (!myFactory.DeleteGroupProducts(groupID))
                    {
                        lblMsg.Visible = true;
                        lblMsg.Text = "Error Occured while deleting the User Products";

                    }

                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Error Occured while deleting the User Group";
                }
            }

            if (!lblMsg.Visible) { Response.Redirect("UserGroupList.aspx"); } // if no visible message 


        }
    }
}