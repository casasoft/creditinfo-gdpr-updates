<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Page language="c#" Codebehind="FindSubscribers.aspx.cs" AutoEventWireup="True" Inherits="UserAdmin.FindSubscribers" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Find Subscribers</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						FindSubscribers.btnSearch.click();
					}
				} 
		
				function SetFormFocus()
				{
					document.FindSubscribers.txtNationalID.focus();
				}
		</script>
	</HEAD>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="FindSubscribers" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblPageHeader" runat="server" cssclass="HeadMain">Find subscribers</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<asp:label id="lblIDNumber" runat="server" visible="False">Creditinfo ID</asp:label>
																<asp:textbox id="txtID" runat="server" visible="False"></asp:textbox>
																<tr>
																	<td>
																		<asp:label id="lblNationalID" runat="server">National ID</asp:label>
																		<br>
																		<asp:textbox id="txtNationalID" runat="server"></asp:textbox>
																	</td>
																	<td>
																		<asp:label id="lblFirstName" runat="server">Firstn. / Comp. name</asp:label>
																		<br>
																		<asp:textbox id="txtFirstName" runat="server"></asp:textbox>
																	</td>
																	<td>
																		<asp:label id="lblSurName" runat="server">Surname</asp:label>
																		<br>
																		<asp:textbox id="txtSurName" runat="server"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblAddres" runat="server">Address</asp:label>
																		<br>
																		<asp:textbox id="txtAddress" runat="server"></asp:textbox>
																	</td>
																	<td>
																		<asp:label id="lblRegisterdBy" runat="server">Registered by</asp:label>
																		<br>
																		<asp:textbox id="txtRegisteredBy" runat="server"></asp:textbox>
																	</td>
																	<td>
																		<asp:label id="lblOpenClosed" runat="server"> [IsOpen]</asp:label>
																		<br>
																		<asp:radiobutton id="rbOpen" runat="server" text="[Open]" groupname="IsOpen" cssclass="radio"></asp:radiobutton>&nbsp;
																		<asp:radiobutton id="rbClosed" runat="server" text="[Closed]" groupname="IsOpen" cssclass="radio"></asp:radiobutton>&nbsp;
																		<asp:radiobutton id="rbBoth" runat="server" text="[Both]" groupname="IsOpen" checked="True" cssclass="radio"></asp:radiobutton>
																	</td>
																</tr>
																<tr>
																	<td colspan="2"></td>
																	<td align="right">
																		<asp:button id="btnSearch" runat="server" cssclass="search_button" text="Search" onclick="btnSearch_Click"></asp:button>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr id="outerGridTableInfo" runat="server">
											<td align="right">
												<div>
													<asp:label id="lblDatagridIcons" runat="server"></asp:label></div>
											</td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<th>
															<asp:label id="lblDataGridHeader" runat="server"></asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divNameSearch" runat="server" style="OVERFLOW-X: auto; OVERFLOW: auto; WIDTH: 100%; HEIGHT: auto">
																			<asp:datagrid id="dgSubscriberList" runat="server" allowsorting="True" autogeneratecolumns="False"
																				gridlines="None" cssclass="grid">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																						CommandName="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:ButtonColumn>
																					<asp:ButtonColumn Text="&lt;img src=&quot;../img/new.gif&quot; alt=&quot;New department&quot; border=&quot;0&quot;&gt;"
																						CommandName="newuser"></asp:ButtonColumn>
																					<asp:BoundColumn DataField="NID" SortExpression="NID" HeaderText="NationalID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="CreditInfoID" SortExpression="CreditInfoID" HeaderText="ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="Name">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="StreetNative" SortExpression="StreetNative" HeaderText="Address">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="UserName" SortExpression="UserName" HeaderText="Reg. By">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="RegisteredBy" SortExpression="RegisteredBy" HeaderText="Reg. by">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="SubscriberNickName" SortExpression="SubscriberNickName"
																						HeaderText="Nickname">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="NationalID" SortExpression="NationalID" HeaderText="NationalID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="MaxUsers" SortExpression="MaxUsers" HeaderText="Max users">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="IsOpen" SortExpression="IsOpen" HeaderText="Open">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="Updated" SortExpression="Updated" HeaderText="Updated" DataFormatString="{0:d}">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="Created" SortExpression="Created" HeaderText="Created" DataFormatString="{0:d}">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
														</td>
														<td align="right">
															<asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel" onclick="btnCancel_Click"></asp:button><asp:button id="btnNew" runat="server" cssclass="confirm_button" text="New" onclick="btnNew_Click"></asp:button>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
						<uc1:secureuseradmin id="SecureUserAdmin1" runat="server"></uc1:secureuseradmin>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
