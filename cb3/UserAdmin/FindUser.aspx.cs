using System;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.Localization;

namespace UserAdmin
{
	/// <summary>
	/// Summary description for FindUser.
	/// </summary>
	public partial class FindUser : Page
	{

		public static ResourceManager rm;
		public static CultureInfo ci;
		private bool EN=false;
	
		protected void Page_Load(object sender, EventArgs e)
		{
			this.Page.ID = "104";

			String culture = Thread.CurrentThread.CurrentCulture.Name;
			if (culture.Equals("en-US"))
				EN=true;

			rm = CIResource.CurrentManager;
			ci = Thread.CurrentThread.CurrentCulture;

			AddEnterEvent();
			LocalizeText();
			SetColumnsByCulture();			

			if(!Page.IsPostBack)
			{
				SetDropDownLists();

				outerGridTable.Visible = false;
				outerGridTableInfo.Visible = false;
			}
		}

		private void SetDropDownLists()
		{
			uaFactory myFactory = new uaFactory();
			ddlUserType.DataSource = myFactory.GetUserTypes();
			ddlUserType.DataValueField = "UserType";
			ddlUserType.DataTextField = "UserType";
			ddlUserType.DataBind();

			ListItem listItem = new ListItem();
			listItem.Value = "";
			listItem.Text = rm.GetString("txtNothingChosen", ci);
			listItem.Selected = true;
			ddlUserType.Items.Insert(0, listItem);
		}

		// display correct datagrid comlumns by locale
		private void SetColumnsByCulture()
		{
			if(EN) 
			{
				this.dgUsers.Columns[0].Visible = true;//Select
				this.dgUsers.Columns[1].Visible = false; //CIID
				this.dgUsers.Columns[2].Visible = true; //User name
				this.dgUsers.Columns[3].Visible = false;//Name
				this.dgUsers.Columns[4].Visible = true;//NameEN
				this.dgUsers.Columns[5].Visible = false;//Street
				this.dgUsers.Columns[6].Visible = false;//StreetEN
				this.dgUsers.Columns[7].Visible = true;//Email
				this.dgUsers.Columns[8].Visible = true;//IsOpen
				this.dgUsers.Columns[9].Visible = false;//Subscriber id
				this.dgUsers.Columns[10].Visible = false;//Subscriber
				this.dgUsers.Columns[11].Visible = true;//SubscriberEN
				this.dgUsers.Columns[13].Visible = false;//DepartmentNT
				this.dgUsers.Columns[14].Visible = true;//DepartmentEN
			}
			else 
			{
				this.dgUsers.Columns[0].Visible = true;//Select
				this.dgUsers.Columns[1].Visible = false; //CIID
				this.dgUsers.Columns[2].Visible = true; //User name
				this.dgUsers.Columns[3].Visible = true;//Name
				this.dgUsers.Columns[4].Visible = false;//NameEN
				this.dgUsers.Columns[5].Visible = false;//Street
				this.dgUsers.Columns[6].Visible = false;//StreetEN
				this.dgUsers.Columns[7].Visible = true;//Email
				this.dgUsers.Columns[8].Visible = true;//IsOpen
				this.dgUsers.Columns[9].Visible = false;//Subscriber id
				this.dgUsers.Columns[10].Visible = true;//Subscriber
				this.dgUsers.Columns[11].Visible = false;//SubscriberEN
				this.dgUsers.Columns[13].Visible = true;//DepartmentNT
				this.dgUsers.Columns[14].Visible = false;//DepartmentEN
			}
		}

		private void LocalizeText()
		{
			this.lblPageHeader.Text = rm.GetString("txtUserSearch",ci);
			this.lblGridHeader.Text = rm.GetString("txtUsers", ci);
			this.lblName.Text = rm.GetString("txtName",ci);
			this.lblUserName.Text = rm.GetString("txtUserName",ci);
			this.btSearch.Text = rm.GetString("txtSearch",ci);
			this.lblErrorMessage.Text = rm.GetString("txtUserNotFound", ci);
			this.lblUserEmail.Text = rm.GetString("txtEmail",ci);
			lblIsOpen.Text = rm.GetString("txtStatus", ci);
			rbOpen.Text = rm.GetString("txtOpen", ci);
			rbClosed.Text = rm.GetString("txtClosed", ci);
			rbBoth.Text = rm.GetString("txtBoth", ci);
			lblUserType.Text = rm.GetString("txtUserType", ci);
			lblSubscriberName.Text = rm.GetString("txtSubscriber", ci);
		}

		private void SetColumnsHeaderByCulture()
		{
			this.dgUsers.Columns[1].HeaderText = rm.GetString("txtCreditInfoID",ci);
			this.dgUsers.Columns[2].HeaderText = rm.GetString("txtUserName",ci);
			this.dgUsers.Columns[3].HeaderText = rm.GetString("txtName",ci);
			this.dgUsers.Columns[4].HeaderText = rm.GetString("txtName",ci);
			this.dgUsers.Columns[5].HeaderText = rm.GetString("txtAddress",ci);
			this.dgUsers.Columns[6].HeaderText = rm.GetString("txtAddress",ci);
			this.dgUsers.Columns[7].HeaderText = rm.GetString("txtEmail",ci);
			this.dgUsers.Columns[8].HeaderText = rm.GetString("txtIsOpen",ci);	
			dgUsers.Columns[10].HeaderText = rm.GetString("txtSubscriber", ci);
			dgUsers.Columns[11].HeaderText = rm.GetString("txtSubscriber", ci);

			dgUsers.Columns[13].HeaderText = rm.GetString("txtDepartment", ci);
			dgUsers.Columns[14].HeaderText = rm.GetString("txtDepartment", ci);
		}	

		private void AddEnterEvent() 
		{
			txtUserName.Attributes.Add("onkeypress", "checkEnterKey();");
			txtName.Attributes.Add("onkeypress", "checkEnterKey();");
			txtUserEmail.Attributes.Add("onkeypress", "checkEnterKey();");
		    txtSubscriberName.Attributes.Add("onkeypress", "checkEnterKey();");
		/*	Control frm = FindControl("frmFindUserName");
			foreach(Control ctrl in frm.Controls)
			{
				if(ctrl is TextBox)
				{
					((TextBox)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");;
				}
			}*/
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgUsers.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgUsers_ItemCommand);
			this.dgUsers.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgUsers_ItemDataBound);

		}
		#endregion

		protected void btSearch_Click(object sender, EventArgs e)
		{
			this.lblErrorMessage.Visible = false;

			uaFactory myFactory = new uaFactory();
			DataSet customerSet = null;
			
			int nIsOpen;
			if(rbOpen.Checked)
			{
				nIsOpen = 1;
			}
			else if(rbClosed.Checked)
			{
				nIsOpen = 0;
			}
			else
			{
				nIsOpen = -1;
			}

			//customerSet = myFactory.FindUser(txtUserName.Text, -1, txtName.Text, null);			
			customerSet = myFactory.FindUser(txtUserName.Text, txtName.Text, txtUserEmail.Text, txtSubscriberName.Text, ddlUserType.SelectedValue, nIsOpen);

			if(customerSet.Tables.Count>0&&customerSet.Tables[0].Rows.Count > 0) 
			{
				this.dgUsers.DataSource = customerSet.Tables[0].DefaultView;
				SetColumnsHeaderByCulture();
				this.dgUsers.DataBind();
				
				if(dgUsers.Items.Count > 0)
				{
					outerGridTable.Visible = true;
					outerGridTableInfo.Visible = true;
				}
			}
			else 
			{
				this.dgUsers.DataSource = null;
				SetColumnsHeaderByCulture();
				this.dgUsers.DataBind();	
				this.lblErrorMessage.Text = rm.GetString("txtUserNotFound", ci);
				this.lblErrorMessage.Visible = true;
			}
		}

		private void dgUsers_ItemCommand(object source, DataGridCommandEventArgs e)
		{		
			if(e.CommandName.Equals("Edit")) 
			{
				string creditInfoID = e.Item.Cells[1].Text.Trim();
				string subscriberCIID = e.Item.Cells[9].Text.Trim(); 
				string strUserID = e.Item.Cells[12].Text.Trim();
				Server.Transfer("UserDetails.aspx?CreditInfoID="+creditInfoID+"&SubscriberCIID="+subscriberCIID + "&UserID=" + strUserID);
			}
		}

		private void dgUsers_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			// Name of user

			if (e.Item.ItemType == ListItemType.Item || 
				e.Item.ItemType == ListItemType.AlternatingItem)
			{
				if(e.Item.Cells[3].Text == null || e.Item.Cells[3].Text.Equals("&nbsp;"))
					e.Item.Cells[3].Text = e.Item.Cells[4].Text;
				if(e.Item.Cells[4].Text == null || e.Item.Cells[4].Text.Equals("&nbsp;"))
					e.Item.Cells[4].Text = e.Item.Cells[3].Text;

				// SubscriberName
				if(e.Item.Cells[10].Text == null || e.Item.Cells[10].Text.Equals("&nbsp;"))
					e.Item.Cells[10].Text = e.Item.Cells[11].Text;
				if(e.Item.Cells[11].Text == null || e.Item.Cells[11].Text.Equals("&nbsp;"))
					e.Item.Cells[11].Text = e.Item.Cells[10].Text;

				//boolean values		

				if(!EN)
				{
					if(e.Item.Cells[8].Text.Trim().ToLower() == "true")
					{
						e.Item.Cells[8].Text = rm.GetString("txtTrue",ci);					
					}
					else
					{
						e.Item.Cells[8].Text = rm.GetString("txtFalse",ci);					
					}
				}
			
				WebDesign.CreateExplanationIcons(dgUsers.Columns, lblDatagridIcons, rm, ci);
			}
		}
	}
}
