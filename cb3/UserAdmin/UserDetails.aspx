<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<%@ Page language="c#" Codebehind="UserDetails.aspx.cs" AutoEventWireup="True" Inherits="UserAdmin.UserDetails" %>
<%@ Register TagPrefix="radt" Namespace="Telerik.WebControls" Assembly="RadTreeView" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en" >
<html>
  <head>
		<title>User details</title>
<script language=JavaScript src="../DatePicker.js"></script>

<meta content="Microsoft Visual Studio .NET 7.1" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><LINK href="../css/CIGStyles.css" type=text/css rel=stylesheet >
<script>

			function UpdateAllChildren(nodes, checked)
			{
				var i;
				for (i=0; i<nodes.length; i++)
				{
						if (checked)       
							nodes[i].Check();       
						else      
							nodes[i].UnCheck(); 
					      
						if (nodes[i].Nodes.length > 0)      
							UpdateAllChildren(nodes[i].Nodes, checked);      
				}
			}

			function AfterCheck(node)
			{
				if (!node.Checked && node.Parent != null)
					node.Parent.UnCheck();   
				      
				var siblingCollection = (node.Parent != null) ? node.Parent.Nodes : node.TreeView.Nodes;
				   
				var allChecked = false;
				for (var i=0; i<siblingCollection.length; i++)
				{
					if(siblingCollection[i].Checked)
					{
						allChecked = true;
						break;
					}
				}
				
				if (allChecked && node.Parent != null)
				{
					node.Parent.Check();
				}
				   
				UpdateAllChildren(node.Nodes, node.Checked);
			}

		</script>
</head>
<body ms_positioning="GridLayout">
<form id=UserDetails name=UserDetails method=post 
runat="server">
<table height=600 width=997 align=center border=0>
  <tr>
    <td colSpan=4><uc1:head id=Head1 runat="server"></uc1:head></td></tr>
  <tr vAlign=top>
    <td width=1></td>
    <td>
      <table height="100%" cellSpacing=0 cellPadding=0 width="100%" 
      >
        <tr>
          <td><uc1:language id=Language1 runat="server"></uc1:language></td>
          <td></td>
          <td align=right><ucl:options id=Options1 runat="server"></ucl:options></td></tr>
        <tr>
          <td align=center bgColor=#000000 colSpan=3 height=1 
          ></td></tr>
        <tr>
          <td height=10></td></tr>
        <tr>
          <td></td>
          <td><uc1:sitepositionbar id=SitePositionBar1 runat="server"></uc1:sitepositionbar></td>
          <td align=right><ucl:userinfo id=UserInfo1 runat="server"></ucl:userinfo></td></tr>
        <tr>
          <td height=10></td></tr>
        <tr vAlign=top>
          <td vAlign=top align=left width=150>
            <table width="98%">
              <tr>
                <td><uc1:panelbar id=PanelBar1 runat="server"></uc1:panelbar></td></tr></table></td>
          <td colSpan=2>
									<!-- Main Body Starts -->
            <table width="100%">
              <tr>
                <td>
                  <table class=grid_table cellSpacing=0 cellPadding=0 
                  >
                    <tr>
                      <th><asp:label id=lblPageHeader runat="server" cssclass="HeadMain">User details</asp:label></th></tr>
                    <tr>
                      <td>
                        <table class=fields id=tbMain cellSpacing=0 
                        cellPadding=0>
                          <tr>
                            <td><asp:Label id="lblSubscriberHeader" runat="server"></asp:Label><br><asp:Label id="lblSubscriber" runat="server"></asp:Label></td>
                            <td><asp:Label id="lblDepartmentHeader" runat="server"></asp:Label><br><asp:Label id="lblDepartment" runat="server"></asp:Label></td>
                           
                            <td style="WIDTH: 25%"><asp:label id=lblNameLabel runat="server">Name</asp:label><br 
                              ><asp:label id=lblName runat="server"></asp:label></td>
</tr>
                          <tr>                              
                            <td style="WIDTH: 25%"><asp:label id=lblCreatedLabel runat="server">Date Created</asp:label><br 
                              ><asp:label id=lblCreated runat="server"></asp:label></td>
                            <td style="WIDTH: 25%"><asp:label id=lblregisteredLabel runat="server">Registered by</asp:label><br 
                              ><asp:label id=lblregisteredBy runat="server"></asp:label></td>
                            <td><asp:label id=lblUpdatedLabel runat="server">Date Updated</asp:label><br 
                              ><asp:label id=lblUpdated runat="server"></asp:label></td></tr>
                          <tr>
                            <td height=23><asp:label id=lblCreditInfoLabel runat="server" visible="False">CreditInfo ID</asp:label><asp:label id=lblCreditInfoID runat="server" visible="False"></asp:label></td></tr></table></td></tr></table></td></tr>
              <tr>
                <td height=10></td></tr>
              <tr>
                <td>
                  <table class=grid_table cellSpacing=0 cellPadding=0 
                  >
                    <tr>
                      <th><asp:label id=lblUserSettings runat="server">User settings</asp:label></th></tr>
                    <tr>
                      <td>
                        <table class=fields id=tbSettings1 cellSpacing=0 
                        cellPadding=0>
                          <tr>
                            <td style="WIDTH: 25%"><asp:label id=lblUserName runat="server">Username</asp:label><br 
                              ><asp:textbox id=txtUsername runat="server"></asp:textbox><asp:requiredfieldvalidator id=rfUserName runat="server" errormessage="Username must be set" controltovalidate="txtUsername">*</asp:requiredfieldvalidator></td>
                            <td style="WIDTH: 25%"><asp:label id=lblUserType runat="server">User Type</asp:label><br 
                              ><asp:dropdownlist id=ddlUserType runat="server" onselectedindexchanged="ddlUserType_SelectedIndexChanged">
																			<asp:listitem value="0" selected="True">Regular User</asp:listitem>
																			<asp:listitem value="1">Employee</asp:listitem>
																		</asp:dropdownlist></td>
                            <td style="WIDTH: 25%"><asp:label id=lblPassword runat="server">Password</asp:label><br 
                              ><asp:textbox id=txtPassword runat="server" textmode="Password" ontextchanged="txtPassword_TextChanged"></asp:textbox><asp:requiredfieldvalidator id=rfPassword runat="server" errormessage="Password must be set" controltovalidate="txtPassword">*</asp:requiredfieldvalidator></td>
                            <td><asp:label id=lblIsOpen runat="server">[Status]</asp:label><br 
                              ><asp:radiobutton id=rbOpen runat="server" cssclass="radio" checked="True" groupname="IsOpen"></asp:radiobutton><asp:radiobutton id=rbClosed runat="server" cssclass="radio" groupname="IsOpen"></asp:radiobutton></td></tr>
                          <tr>
                            <td><asp:label id=lblEmail runat="server">Email</asp:label><br 
                              ><asp:textbox id=txtEmail runat="server"></asp:textbox></td>
                            <td><asp:label id=lblOpenUntil runat="server">Open until</asp:label><br 
                              ><asp:textbox id=txtOpenUntil runat="server"></asp:textbox><input class=popup onclick="PopupPicker('txtOpenUntil', 250, 250);" type=button value=...> 
                            </td>
                            <td><asp:label id=lblWatchLimit runat="server">Credit Watch limit</asp:label><br 
                              ><asp:textbox id=txtCreditWatchLimit runat="server">1000</asp:textbox></td>
                              
                              <td><asp:label id=lblAccountLocked runat="server">Account lock</asp:label><br 
                              ><asp:radiobutton id=rbUnlock runat="server" cssclass="radio" checked="True" groupname="AccountLocked"></asp:radiobutton><asp:radiobutton id=rbLock runat="server" cssclass="radio" groupname="AccountLocked"></asp:radiobutton></td>
                          
                            <td><asp:label id=lblWebService runat="server" visible="False">WebServices</asp:label><br 
                              ><asp:checkbox id=chkbxWebServices runat="server" cssclass="radio" visible="False"></asp:checkbox></td>

                          </tr>
                            
                            <tr>
                                <td><asp:label id=GroupCode runat="server">Group Code</asp:label><br 
                                                                                         ><asp:textbox id=txtGroupCode runat="server"></asp:textbox></td>                                                                            ><asp:checkbox id=Checkbox1 runat="server" cssclass="radio" visible="False"></asp:checkbox></td>

                            </tr>
                            

                             <tr>
                                                            <td><asp:label id=Label8 runat="server">Show Users the records for:</asp:label></td>

                                                            <td><asp:label id=lblIsInd runat="server">Show Individual records</asp:label><br 
                                                                                                                                         ><asp:checkbox id=chkIsInd runat="server" cssclass="radio"></asp:checkbox></td>

                                                            <td><asp:label id=lblIsIndVat runat="server">Show Individual with Vat Number records</asp:label><br 
                                                                                                                                                            ><asp:checkbox id=chkIsIndVat runat="server" cssclass="radio"></asp:checkbox></td>

                                                            <td><asp:label id=lblIsComp runat="server">Show Company records</asp:label><br 
                                                                                                                                       ><asp:checkbox id=chkIsComp runat="server" cssclass="radio"></asp:checkbox></td>
                                                        </tr>
                          <tr>
                            <td height=23 
                        ></td></tr></table></td></tr></table></td></tr>
              <tr>
                <td height=10></td></tr>
              <tr>
                <td>
                  <table class=grid_table cellSpacing=0 cellPadding=0 
                  >
                    <tr>
                      <th><asp:label id=lblUserRights runat="server">User rights</asp:label></th></tr>
                    <tr>
                      <td>
                        <table class=fields id=tbTrees cellSpacing=0 
                        cellPadding=0>
                          <tr>
                            <td id=tdInternal style="WIDTH: 50%" vAlign=top 
                             runat="server"><asp:label id=lblInternal runat="server">Internal</asp:label><br 
                              >
                              <div class=tree><radt:radtreeview id=internalTree runat="server" causesvalidation="False" checkboxes="True" afterclientcheck="AfterCheck"></radt:radtreeview></div></td>
                            <td vAlign=top><asp:label id=lblExternal runat="server">External</asp:label><br 
                              >
                              <div class=tree><radt:radtreeview id=externalTree runat="server" causesvalidation="False" checkboxes="True" afterclientcheck="AfterCheck"></radt:radtreeview></div></td></tr>
                          <tr>
                            <td height=23 
                        ></td></tr></table></td></tr></table></td></tr>
              <tr>
                <td height=10></td></tr>
              <tr>
                <td>
                  <table class=empty_table cellSpacing=0>
                    <tr>
                      <td align=left><asp:label id=lblFinishRegistration runat="server" visible="False" forecolor="Red">- registration not finished</asp:label><asp:validationsummary id=ValidationSummary1 runat="server"></asp:validationsummary></td>
                      <td align=right><asp:button id=btnBack runat="server" cssclass="cancel_button" causesvalidation="False" text="Back" onclick="btnBack_Click"></asp:button><asp:button id=btnRegister runat="server" cssclass="confirm_button" text="Register" onclick="btnRegister_Click"></asp:button></td></tr></table></td></tr></table>
									<!-- Main Body Ends --></td></tr>
        <tr>
          <td height=20></td></tr>
        <tr>
          <td align=center bgColor=#000000 colSpan=3 height=1 
          ></td></tr>
        <tr>
          <td align=center colSpan=4><uc1:footer id=Footer1 runat="server"></uc1:footer><uc1:secureuseradmin id=SecureUserAdmin1 runat="server"></uc1:secureuseradmin></td></tr></table></td></tr></table></form>
	</body>
</html>
