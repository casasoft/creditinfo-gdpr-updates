﻿<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="SubscriberDetails.aspx.cs" AutoEventWireup="True" Inherits="UserAdmin.SubscriberDetails" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Subscriber details</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name=GENERATOR>
    <meta content=C# name=CODE_LANGUAGE>
    <meta content=JavaScript name=vs_defaultClientScript>
    <meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><LINK href="../css/CIGStyles.css" type=text/css rel=stylesheet >
    <script>
        function checkEnterKey() 
        {    
            if (event.keyCode == 13) 
            {        
                event.cancelBubble = true;
                event.returnValue = false;
                SubscriberDetails.btnRegister.click(); 
            }
        } 
    </script>
</head>
<body ms_positioning="GridLayout">
<form id=SubscriberDetails name=SubscriberDetails action="" method=post runat="server">
    <table height=600 width=997 align=center border=0>
        <tr>
            <td colSpan=4><uc1:head id=Head1 runat="server"></uc1:head></td></tr>
        <tr vAlign=top>
            <td width=1></td>
            <td>
                <table height="100%" cellSpacing=0 cellPadding=0 width="100%" 
                >
                    <tr>
                        <td><uc1:language id=Language1 runat="server"></uc1:language></td>
                        <td></td>
                        <td align=right><ucl:options id=Options1 runat="server"></ucl:options></td></tr>
                    <tr>
                        <td align=center bgColor=#000000 colSpan=3 height=1 
                        ></td></tr>
                    <tr>
                        <td height=10></td></tr>
                    <tr>
                        <td></td>
                        <td><uc1:sitepositionbar id=SitePositionBar1 runat="server"></uc1:sitepositionbar></td>
                        <td align=right><ucl:userinfo id=UserInfo1 runat="server"></ucl:userinfo></td></tr>
                    <tr>
                        <td height=10></td></tr>
                    <tr vAlign=top>
                        <td vAlign=top align=left width=150>
                            <table width="98%">
                                <tr>
                                    <td><uc1:panelbar id=PanelBar1 runat="server"></uc1:panelbar></td></tr></table></td>
                        <td colSpan=2>
                            <!-- Main Body Starts -->
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table class=grid_table cellSpacing=0 cellPadding=0 
                                        >
                                            <tr>
                                                <th><asp:label id=lblPageHeader runat="server" cssclass="HeadMain">Subscriber details</asp:label></th></tr>
                                            <tr>
                                                <td>
                                                    <table class=fields id=tbMain cellSpacing=0 
                                                           cellPadding=0>
                                                        <tr><asp:label id=lblCreditInfoLabel runat="server" visible="False">CreditInfo ID</asp:label><asp:label id=lblCreditInfoID runat="server" visible="False"></asp:label>
                                                            <td style="WIDTH: 25%"><asp:label id=lblNameLabel runat="server">Name</asp:label><br 
                                                                                                                                             ><asp:label id=lblName runat="server"></asp:label></td>
                                                            <td style="WIDTH: 25%"><asp:label id=lblCreatedLabel runat="server">Date Created</asp:label><br 
                                                                                                                                                        ><asp:label id=lblCreated runat="server"></asp:label></td>
                                                            <td style="WIDTH: 25%"><asp:label id=lblregisteredLabel runat="server">Registered by</asp:label><br 
                                                                                                                                                            ><asp:label id=lblregisteredBy runat="server"></asp:label></td>
                                                            <td><asp:label id=lblUpdatedLabel runat="server">Date Updated</asp:label><br 
                                                                                                                                     ><asp:label id=lblUpdated runat="server"></asp:label></td></tr>
                                                        <tr>
                                                            <td><asp:label id=lblIsOpen runat="server">Open (check) Closed (uncheck)</asp:label><br 
                                                                                                                                                ><asp:checkbox id=chkbxIsOpen runat="server" cssclass="radio" checked="True"></asp:checkbox></td>
                                                            <td><asp:label id=lblMaxUsers runat="server">Max. Users</asp:label><br 
                                                                                                                               ><asp:textbox id=txtMaxUsers runat="server" tooltip="Integer value" maxlength="10">0</asp:textbox></td>
                                                            <td><asp:label id=lblIsPrintBlocked runat="server">Block print screen and copy paste for users</asp:label><br 
                                                                                                                                             ><asp:checkbox id=chkIsPrintBlocked runat="server" cssclass="radio" checked="True"></asp:checkbox></td>
                                                        </tr>

                                                        <tr>
                                                            <td><asp:label id=Label8 runat="server">Show Users the records for:</asp:label></td>

                                                            <td><asp:label id=lblIsInd runat="server">Show Individual records</asp:label><br 
                                                                                                                                         ><asp:checkbox id=chkIsInd runat="server" cssclass="radio"></asp:checkbox></td>

                                                            <td><asp:label id=lblIsIndVat runat="server">Show Individual with Vat Number records</asp:label><br 
                                                                                                                                                            ><asp:checkbox id=chkIsIndVat runat="server" cssclass="radio"></asp:checkbox></td>

                                                            <td><asp:label id=lblIsComp runat="server">Show Company records</asp:label><br 
                                                                                                                                       ><asp:checkbox id=chkIsComp runat="server" cssclass="radio"></asp:checkbox></td>
                                                        </tr>

                                                        <tr>
                                                            <td height=23 
                                                            ></td></tr></table></td></tr></table></td></tr>
                                <tr>
                                    <td>
                                        <table class=grid_table id=tblDepartments cellSpacing=0 
                                               cellPadding=0 runat="server">
                                            <tr>
                                                <td height=10></td></tr>
                                            <tr>
                                                <td><asp:radiobutton id="rbOpen" runat="server" cssclass="radio" text="[Open]" groupname="IsOpen" Checked="True" AutoPostBack="True" oncheckedchanged="IsOpen_CheckedChanged"></asp:radiobutton>&nbsp; 
                                                    <asp:radiobutton id="rbClosed" runat="server" cssclass="radio" text="[Closed]" groupname="IsOpen" AutoPostBack="True" oncheckedchanged="IsOpen_CheckedChanged"></asp:radiobutton>&nbsp; 
                                                    <asp:radiobutton id="rbBoth" runat="server" cssclass="radio" text="[Both]" groupname="IsOpen" AutoPostBack="True" oncheckedchanged="IsOpen_CheckedChanged"></asp:radiobutton></td>
                                                <td align=right>
                                                    <asp:label id=lblDeparmentsIcons runat="server"></asp:label></td></tr>
                                            <tr>
                                                <th colspan="2"><asp:label id=lblDeparmentsHeader runat="server">Users</asp:label></th></tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table class=datagrid cellSpacing=0 cellPadding=0 
                                                    >
                                                        <tr>
                                                            <td><asp:datagrid id=dgDepartments runat="server" gridlines="None" allowsorting="True" cellpadding="4" backcolor="White" borderwidth="1px" borderstyle="None" bordercolor="Gray" width="100%" autogeneratecolumns="False" showfooter="True">
                                                                <footerstyle cssclass="grid_footer">
                                                                </FooterStyle>

                                                                <selecteditemstyle cssclass="grid_selecteditem">
                                                                </SelectedItemStyle>

                                                                <alternatingitemstyle cssclass="grid_alternatingitem">
                                                                </AlternatingItemStyle>

                                                                <itemstyle cssclass="grid_item">
                                                                </ItemStyle>

                                                                <headerstyle cssclass="grid_header">
                                                                </HeaderStyle>

                                                                <columns>
                                                                    <asp:ButtonColumn Text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;" CommandName="Edit">
                                                                        <itemstyle cssclass="leftpadding">
                                                                        </ItemStyle>
                                                                    </asp:ButtonColumn>
                                                                    <asp:ButtonColumn Text="&lt;img src=&quot;../img/new.gif&quot; alt=&quot;New user&quot; border=&quot;0&quot;&gt;" CommandName="newuser">
                                                                        <itemstyle cssclass="nopadding">
                                                                        </ItemStyle>
                                                                    </asp:ButtonColumn>
                                                                    <asp:BoundColumn Visible="False" DataField="DepartmentID">
                                                                        <itemstyle cssclass="padding">
                                                                        </ItemStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="NameNative" HeaderText="[Name]">
                                                                        <itemstyle cssclass="padding">
                                                                        </ItemStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="NameEN" HeaderText="[Name]">
                                                                        <itemstyle cssclass="padding">
                                                                        </ItemStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ContactPerson" HeaderText="[Contact]">
                                                                        <itemstyle cssclass="padding">
                                                                        </ItemStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Phone" HeaderText="[Phone]">
                                                                        <itemstyle cssclass="padding">
                                                                        </ItemStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Email" HeaderText="[Email]">
                                                                        <itemstyle cssclass="padding">
                                                                        </ItemStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn Visible="False" DataField="IsOpen">
                                                                        <itemstyle cssclass="padding">
                                                                        </ItemStyle>
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="UserCount" HeaderText="[UserCount]">
                                                                        <itemstyle cssclass="padding">
                                                                        </ItemStyle>
                                                                    </asp:BoundColumn>
                                                                </Columns>

                                                                <pagerstyle cssclass="grid_pager">
                                                                </PagerStyle>
                                                            </asp:datagrid></td></tr></table></td>
                                            </tr></table></td></tr>
                                <tr>
                                    <td height=10></td></tr>
                                <tr>
                                    <td>
                                        <table class="empty_table" cellspacing="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:label id="lblError" runat="server" cssclass="error_text"></asp:label>
                                                </td>
                                                <td align="right"><asp:Button id="btnDepartmentNew" runat="server" Text="New Department" CssClass="gray_button" onclick="btnDepartmentNew_Click"></asp:Button><asp:button id="btnSubscriberFirstUser" runat="server" cssclass="gray_button" text="Subscriber first" visible="False" onclick="btnSubscriberFirstUser_Click"></asp:button><asp:button id="btnRegister" runat="server" cssclass="confirm_button" text="Register" causesvalidation="False" onclick="btnRegister_Click"></asp:button>
                                                </td>
                                            </tr>
                                        </table></td></tr></table>
                            <!-- Main Body Ends --></td></tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
                    </tr></table></td>
            <td width="2"></td></tr>
        <tr>
            <td align="center" colspan="4">
                <uc1:footer id="Footer1" runat="server"></uc1:footer>
                <uc1:secureuseradmin id="SecureUserAdmin1" runat="server"></uc1:secureuseradmin>
            </td>
        </tr></table></form>
</body>
</html>
