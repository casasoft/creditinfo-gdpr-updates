using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.Localization;

namespace UserAdmin
{
    /// <summary>
    /// Summary description for FindSubscribers.
    /// </summary>
    public partial class FindSubscribers : Page
    {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        private const string pageName = "FindSubscribers.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            AddEnterEvent();
            Page.ID = "101";

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            LocalizeText();
            localizeGridHeader();

            if (!Page.IsPostBack)
            {
                outerGridTable.Visible = false;
                outerGridTableInfo.Visible = false;
            }
        }

        private void LocalizeText()
        {
            lblAddres.Text = rm.GetString("txtAddress", ci);
            lblFirstName.Text = rm.GetString("txtFirstNameCompanyName", ci);
            lblIDNumber.Text = rm.GetString("txtCreditInfoID", ci);
            lblPageHeader.Text = rm.GetString("txtFindSubscriber", ci);
            lblRegisterdBy.Text = rm.GetString("txtRegisteredBy", ci);
            lblSurName.Text = rm.GetString("txtSurName", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);
            btnNew.Text = rm.GetString("txtNew", ci);
            lblNationalID.Text = rm.GetString("txtNationalID", ci);

            //this.lblOpenClosed.Text=rm.GetString("txtOpenClosed", ci);
            //this.ddOpenClosed.Items[0].Text=rm.GetString("txtBoth", ci);
            //this.ddOpenClosed.Items[1].Text=rm.GetString("txtOpen", ci);
            //this.ddOpenClosed.Items[2].Text=rm.GetString("txtClosed", ci);
            lblOpenClosed.Text = rm.GetString("txtStatus", ci);
            rbOpen.Text = rm.GetString("txtOpen", ci);
            rbClosed.Text = rm.GetString("txtClosed", ci);
            rbBoth.Text = rm.GetString("txtBoth", ci);

            lblDataGridHeader.Text = rm.GetString("txtResult", ci);
        }

        private void localizeGridHeader()
        {
            dgSubscriberList.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dgSubscriberList.Columns[3].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dgSubscriberList.Columns[4].HeaderText = rm.GetString("txtName", ci);
            dgSubscriberList.Columns[5].HeaderText = rm.GetString("txtAddress", ci);
            dgSubscriberList.Columns[6].HeaderText = rm.GetString("txtRegisteredBy", ci);
            //this.dgSubscriberList.Columns[9].HeaderText = rm.GetString("txtMaxUsers", ci);
            dgSubscriberList.Columns[11].HeaderText = rm.GetString("txtOpen", ci);
            dgSubscriberList.Columns[12].HeaderText = rm.GetString("txtUpdated", ci);
            dgSubscriberList.Columns[13].HeaderText = rm.GetString("txtCreated", ci);
        }

        private void dgSubscriberList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals(DataGrid.SelectCommandName))
            {
                try
                {
                    Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + e.Item.Cells[3].Text);
                }
                catch (ThreadAbortException)
                {
                    //?? happens randomly when Server.Transfer is invoked
                }
                catch (Exception err)
                {
                    Logger.WriteToLog(
                        pageName + "::dgSubscriberList_ItemCommand - Error clicking update " + err.Message, false);
                }
            }
            else if (e.CommandName.Equals("newuser"))
            {
                try
                {
                    if (!string.IsNullOrEmpty(e.Item.Cells[3].Text))
                    {
                        int nCigID = int.Parse(e.Item.Cells[3].Text);
                        var myFactory3 = new uaFactory();

                        Session["SubscriberMaxUsers"] = myFactory3.GetSubscriberUserLimit(nCigID);
                        //Server.Transfer("FindCIUser.aspx?SubscriberCIID=" + nCigID);
                        Server.Transfer("DepartmentDetails.aspx?SubscriberCIID=" + e.Item.Cells[3].Text);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteToLog("" + ex.Message, false);
                }
            }
        }

        private void dgSubscriberList_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            var myTable = (DataTable)Session["dgSubscriberList"];
            var myView = myTable.DefaultView;

            myView.Sort = e.SortExpression;
            dgSubscriberList.DataSource = myView;
            dgSubscriberList.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string CreditInfoID = txtID.Text;
            string nationalID = txtNationalID.Text;
            string FirstName = txtFirstName.Text;
            string Surname = txtSurName.Text;
            string Address = txtAddress.Text;
            string RegisteredBy = txtRegisteredBy.Text;
            string OpenClosed; //= ddOpenClosed.SelectedValue.ToString();
            if (rbOpen.Checked)
            {
                OpenClosed = "Open";
            }
            else OpenClosed = rbClosed.Checked ? "Closed" : "";

            var mySet = myFactory.SearchSubscribers(
                CreditInfoID, nationalID, FirstName, Surname, Address, RegisteredBy, OpenClosed);
            dgSubscriberList.DataSource = mySet;
            dgSubscriberList.DataBind();
            Session["dgSubscriberList"] = mySet.Tables[0];

            if (dgSubscriberList.Items.Count > 0)
            {
                outerGridTable.Visible = true;
                outerGridTableInfo.Visible = true;
            }
            else
            {
                outerGridTable.Visible = false;
                outerGridTableInfo.Visible = false;
            }
        }

        private void AddEnterEvent()
        {
            Control frm = FindControl("FindSubscribers");
            foreach (Control ctrl in frm.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is RadioButtonList)
                {
                    ((RadioButtonList)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtID.Text = "";
            txtFirstName.Text = "";
            txtSurName.Text = "";
            txtAddress.Text = "";
            txtRegisteredBy.Text = "";
            rbBoth.Checked = true;
        }

        private void dgSubscriberList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            /*if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
			{
				((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
			}*/

            if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
            {
                if (e.Item.Cells[11].Text.Trim().Equals("False"))
                {
                    e.Item.ForeColor = Color.Gray;
                    e.Item.Cells[1].Controls[0].Visible = false;
                }
            }
            WebDesign.CreateExplanationIcons(dgSubscriberList.Columns, lblDatagridIcons, rm, ci);
        }

        protected void btnNew_Click(object sender, EventArgs e) { Server.Transfer("FindCIUser.aspx?CreateSubscriber=True"); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgSubscriberList.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgSubscriberList_ItemCommand);
            this.dgSubscriberList.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgSubscriberList_SortCommand);
            this.dgSubscriberList.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSubscriberList_ItemDataBound);
        }

        #endregion
    }
}