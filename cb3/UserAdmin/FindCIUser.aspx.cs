using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using UserAdmin.Localization;

using Cig.Framework.Base.Configuration;

namespace UserAdmin
{
    /// <summary>
    /// Summary description for FindCIUser.
    /// </summary>
    public partial class FindDebtor : Page
    {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private int _departmentID = -1;
        protected int creditInfoID = -1;
        private bool EN;
        protected HtmlForm Form2;
        protected bool isCreatingSubscriber;
        protected bool isUpdatingUser;
        protected int subscriberCIID = -1;
        protected Table Table1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["CreateSubscriber"] != null &&
                Request.QueryString["CreateSubscriber"].Equals("True"))
            {
                Page.ID = "105";
            }
            else if (Request.QueryString["UpdateUser"] != null && Request.QueryString["UpdateUser"].Equals("True"))
            {
                Page.ID = "106";
            }
            else
            {
                Page.ID = "103";
            }

            try
            {
                subscriberCIID = int.Parse(Request["SubscriberCIID"]);
            }
            catch
            {
                subscriberCIID = -1;
            }
            try
            {
                _departmentID = int.Parse(Request["Department"]);
            }
            catch { }

            try
            {
                isCreatingSubscriber = bool.Parse(Request["CreateSubscriber"]);
            }
            catch
            {
                isCreatingSubscriber = false;
            }
            try
            {
                isUpdatingUser = bool.Parse(Request["UpdateUser"]);
            }
            catch
            {
                isUpdatingUser = false;
            }
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US"))
            {
                EN = true;
            }

            if (subscriberCIID == -1 && !isCreatingSubscriber && !isUpdatingUser)
            {
                Server.Transfer("FindSubscribers.aspx");
            }

            if (subscriberCIID != -1 || isCreatingSubscriber)
            {
                dgDebtors.Columns[0].Visible = true;
            }
            else
            {
                dgDebtors.Columns[0].Visible = false;
                dgDebtors.Columns[1].ItemStyle.CssClass = "leftpadding";
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            AddEnterEvent();

            if (!IsPostBack)
            {
                ViewState["url"] = Request.UrlReferrer.AbsoluteUri;
                InitDDLists();
            }

            LocalizeText();
            SetColumnsByCulture();
            if (isUpdatingUser)
            {
                //btnNew.Enabled=false;
                dgDebtors.Columns[0].Visible = false;
            }

            if (dgDebtors.Visible && dgDebtors.Items.Count > 0)
            {
                outerGridTable.Visible = true;
            }
            else
            {
                outerGridTable.Visible = false;
            }
        }

        protected void btSearchDebtor_Click(object sender, EventArgs e)
        {
            lblErrorMessage.Visible = false;

            var myFactory = new uaFactory();
            DataSet customerSet;

            if (rbtnIndividual.Checked) //We are looking for an individual
            {
                if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True" && !isUpdatingUser)
                {
                    customerSet = myFactory.FindCustomerInNationalAndCreditInfo(GetDebtorInfoFromPage());
                }
                else
                {
                    customerSet = myFactory.FindCustomer(GetDebtorInfoFromPage(),this.tdVatNumber.Text.Trim());
                }
                dgDebtors.Columns[2].Visible = false;
            }
            else //We are looking for a company
            {
                if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True" && !isUpdatingUser)
                {
                    customerSet = myFactory.FindCompanyInNationalAndCreditInfo(GetCompanyInfoFromPage());
                }
                else
                {
                    customerSet = myFactory.FindCompany(GetCompanyInfoFromPage(),this.tdVatNumber.Text.Trim());
                }
                dgDebtors.Columns[2].Visible = false;
            }

            if (customerSet.Tables.Count > 0 && customerSet.Tables[0].Rows.Count > 0)
            {
                //this.dgDebtors.DataSource=null;

                var myView = customerSet.Tables[0].DefaultView;
                myView.Sort = EN ? "NameEN" : "NameNative";
                dgDebtors.DataSource = myView;
                SetColumnsHeaderByCulture();
                Session["dgDebtors"] = customerSet.Tables[0];
                dgDebtors.DataBind();

                outerGridTable.Visible = true;
            }
            else
            {
                dgDebtors.DataSource = null;
                SetColumnsHeaderByCulture();
                dgDebtors.DataBind();
                lblErrorMessage.Text = rbtnCompany.Checked ? rm.GetString("txtCompanyNotFound", ci) : rm.GetString("txtIndividualNotFound", ci);
                lblErrorMessage.Visible = true;
            }
        }

        private Company GetCompanyInfoFromPage()
        {
            var theCompany = new Company { NationalID = tbIDNumber.Text, NameNative = tbDebtorFirstName.Text };

            /*try
			{
				if (this.tbCIID.Text != "")
					theCompany.CreditInfoID = int.Parse(this.tbCIID.Text);
			}
			catch{}*/

            theCompany.Address.Add(GetAddressFromPage());
            return theCompany;
        }

        private Debtor GetDebtorInfoFromPage()
        {
            var myDebtor = new Debtor
                           {
                               IDNumber1 = tbIDNumber.Text,
                               IDNumber2Type = int.Parse(ddIDNumberType.SelectedItem.Value),
                               SurName = tbDebtorSurName.Text,
                               FirstName = tbDebtorFirstName.Text,
                               Address = new ArrayList
                                         {
                                             GetAddressFromPage
                                                 (
                                                 )
                                         }
                           };

            /*try
			{
				if (this.tbCIID.Text != "")
					myDebtor.CreditInfo = int.Parse(this.tbCIID.Text);
			}
			catch{}*/

            return myDebtor;
        }

        private static Address GetAddressFromPage()
        {
            var theAddress = new Address();
            //theAddress.StreetNative = this.txtAddressNative.Text;
            return theAddress;
        }

        private void InitDDLists()
        {
            var myFactory = new uaFactory();
            InitIDNumberType(myFactory);
        }

        private void InitIDNumberType(uaFactory myFactory)
        {
            var idNumberType = myFactory.GetIDNumberTypesAsDataSet();

            ddIDNumberType.DataSource = idNumberType;
            ddIDNumberType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddIDNumberType.DataValueField = "NumberTypeID";
            ddIDNumberType.DataBind();
        }

        private void dgDebtors_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.Item.Cells[3].Text == "")
                {
                    creditInfoID = -1;
                }
                else
                {
                    creditInfoID = int.Parse(e.Item.Cells[3].Text);
                }
            }
            catch // catching int.Parse(e.Item.Cells[3].Text) when sorting
            {
                creditInfoID = -1;
            }
            string idNumber = e.Item.Cells[7].Text.Trim();

            if (e.CommandName.Equals("Update"))
            {
                if (rbtnIndividual.Checked)
                {
                    if (isUpdatingUser)
                    {
                        Server.Transfer("NewCIUser.aspx?Update=True&CreditInfoID=" + creditInfoID + "&UpdateUser=True");
                    }
                    else if (isCreatingSubscriber)
                    {
                        if (creditInfoID == -1 && idNumber != "")
                        {
                            Server.Transfer(
                                "NewCIUser.aspx?Update=False&CreditInfoID=" + creditInfoID + "&NationalID=" + idNumber +
                                "&CreateSubscriber=True");
                        }
                        else
                        {
                            Server.Transfer(
                                "NewCIUser.aspx?Update=True&CreditInfoID=" + creditInfoID + "&CreateSubscriber=True");
                        }
                    }
                    else
                    {
                        if (creditInfoID == -1 && idNumber != "")
                        {
                            Server.Transfer(
                                "NewCIUser.aspx?Update=False&CreditInfoID=" + creditInfoID + "&NationalID=" + idNumber +
                                "&SubscriberCIID=" + subscriberCIID);
                        }
                        else
                        {
                            Server.Transfer(
                                "NewCIUser.aspx?Update=true&CreditInfoID=" + creditInfoID + "&SubscriberCIID=" +
                                subscriberCIID);
                        }
                    }
                    Session.Remove("dgDebtors");
                }
                else
                {
                    if (isUpdatingUser)
                    {
                        Server.Transfer("NewCIUserC.aspx?Update=True&CreditInfoID=" + creditInfoID + "&UpdateUser=True");
                    }
                    else if (isCreatingSubscriber)
                    {
                        if (creditInfoID == -1 && idNumber != "")
                        {
                            Server.Transfer(
                                "NewCIUserC.aspx?Update=False&CreditInfoID=" + creditInfoID + "&NationalID=" + idNumber +
                                "&CreateSubscriber=True");
                        }
                        else
                        {
                            Server.Transfer(
                                "NewCIUserC.aspx?Update=True&CreditInfoID=" + creditInfoID + "&CreateSubscriber=True");
                        }
                    }
                    else
                    {
                        if (creditInfoID == -1 && idNumber != "")
                        {
                            Server.Transfer(
                                "NewCIUserC.aspx?Update=False&CreditInfoID=" + creditInfoID + "&NationalID=" + idNumber +
                                "&SubscriberCIID=" + subscriberCIID);
                        }
                        else
                        {
                            Server.Transfer(
                                "NewCIUserC.aspx?Update=true&CreditInfoID=" + creditInfoID + "&SubscriberCIID=" +
                                subscriberCIID);
                        }
                    }
                    Session.Remove("dgDebtors");
                }
            }
            else if (e.CommandName.Equals("Select"))
            {
                if (rbtnIndividual.Checked)
                {
                    if (isCreatingSubscriber)
                    {
                        if (creditInfoID == -1 && idNumber != "")
                        {
                            Server.Transfer(
                                "NewCIUser.aspx?Update=False&CreditInfoID=" + creditInfoID + "&NationalID=" + idNumber +
                                "&CreateSubscriber=True");
                        }
                        else
                        {
                            Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + creditInfoID);
                        }
                    }
                    else if (subscriberCIID != -1)
                    {
                        if (creditInfoID == -1 && idNumber != "")
                        {
                            Server.Transfer(
                                "NewCIUser.aspx?Update=False&CreditInfoID=" + creditInfoID + "&NationalID=" + idNumber +
                                "&SubscriberCIID=" + subscriberCIID + "&Department=" + _departmentID);
                        }
                        else
                        {
                            Server.Transfer(
                                "UserDetails.aspx?new=false&comp=true&CreditInfoID=" + creditInfoID + "&SubscriberCIID=" +
                                subscriberCIID + "&Department=" + _departmentID);
                        }
                    }
                    Session.Remove("dgDebtors");
                }
                else
                {
                    if (isCreatingSubscriber)
                    {
                        if (creditInfoID == -1 && idNumber != "")
                        {
                            Server.Transfer(
                                "NewCIUserC.aspx?Update=False&CreditInfoID=" + creditInfoID + "&NationalID=" + idNumber +
                                "&CreateSubscriber=True");
                        }
                        else
                        {
                            Server.Transfer("SubscriberDetails.aspx?SubscriberCIID=" + creditInfoID);
                        }
                    }
                    else if (subscriberCIID != -1)
                    {
                        if (creditInfoID == -1 && idNumber != "")
                        {
                            Server.Transfer(
                                "NewCIUserC.aspx?Update=False&CreditInfoID=" + creditInfoID + "&NationalID=" + idNumber +
                                "&SubscriberCIID=" + subscriberCIID + "&Department=" + _departmentID);
                        }
                        else
                        {
                            Server.Transfer(
                                "UserDetails.aspx?new=false&comp=true&CreditInfoID=" + creditInfoID + "&SubscriberCIID=" +
                                subscriberCIID + "&Department=" + _departmentID);
                        }
                    }
                    Session.Remove("dgDebtors");
                }
            }
            else if (e.CommandName.Equals("newuser"))
            {
                var myFactory = new uaFactory();
                Session["SubscriberMaxUsers"] = myFactory.GetSubscriberUserLimit(creditInfoID);
                Server.Transfer("FindCIUser.aspx?SubscriberCIID=" + creditInfoID);
                Session.Remove("dgDebtors");
            }
        }

        // display correct datagrid comlumns by locale
        private void SetColumnsByCulture()
        {
            if (EN)
            {
                dgDebtors.Columns[0].Visible = true; //Select
                dgDebtors.Columns[1].Visible = true; //Update
                dgDebtors.Columns[2].Visible = true; //New user
                dgDebtors.Columns[3].Visible = true; //CIID
                dgDebtors.Columns[4].Visible = false; //Name
                dgDebtors.Columns[5].Visible = true; //NameEN
                dgDebtors.Columns[6].Visible = false; //Street
                dgDebtors.Columns[7].Visible = true; //StreetEN
                dgDebtors.Columns[8].Visible = true; //IDNumber
                dgDebtors.Columns[9].Visible = false; //City
                dgDebtors.Columns[10].Visible = true; //CityEN
                dgDebtors.Columns[11].Visible = true; //PostalCode
            }
            else
            {
                dgDebtors.Columns[0].Visible = true; //Select
                dgDebtors.Columns[1].Visible = true; //Update
                dgDebtors.Columns[2].Visible = true; //New user
                dgDebtors.Columns[3].Visible = true; //CIID
                dgDebtors.Columns[4].Visible = true; //Name
                dgDebtors.Columns[5].Visible = false; //NameEN
                dgDebtors.Columns[6].Visible = true; //Street
                dgDebtors.Columns[7].Visible = false; //StreetEN
                dgDebtors.Columns[8].Visible = true; //IDNumber
                dgDebtors.Columns[9].Visible = true; //City
                dgDebtors.Columns[10].Visible = false; //CityEN
                dgDebtors.Columns[11].Visible = true; //PostalCode
            }
        }

        private void LocalizeText()
        {
            if (rbtnCompany.Checked)
            {
                if (isUpdatingUser)
                {
                    lblPageHeader.Text = /*rm.GetString("txtUpdatingUser",ci) + " - " +*/
                        rm.GetString("txtCompanySearch", ci);
                }
                else lblPageHeader.Text = isCreatingSubscriber ? rm.GetString("txtCompanySearch", ci) : rm.GetString(
                                                                                                            "txtCompanySearch", ci);
            }
            else
            {
                if (isUpdatingUser)
                {
                    lblPageHeader.Text = /*rm.GetString("txtUpdatingUser",ci) + " - " +*/
                        rm.GetString("txtIndividualSearch", ci);
                }
                else lblPageHeader.Text = isCreatingSubscriber ? rm.GetString("txtIndividualSearch", ci) : rm.GetString(
                                                                                                               "txtIndividualSearch", ci);
            }
            rbtnIndividual.Text = ""; //rm.GetString("txtIndividual",ci);
            imgIndividual.AlternateText = rm.GetString("txtIndividual", ci);
            rbtnCompany.Text = ""; //rm.GetString("txtCompany",ci);
            imgCompany.AlternateText = rm.GetString("txtCompany", ci);

            btCancel.Text = rm.GetString("txtCancel", ci);
            lbDebtorFirstName.Text = rm.GetString("txtFirstName", ci);
            lbDebtorSurName.Text = rm.GetString("txtSurName", ci);
            //this.lbIDNumber.Text = rm.GetString("txtNumberType",ci);
            lbIDNumber.Text = rm.GetString("txtIDNumber", ci);
            lblNumberType.Text = rm.GetString("txtNumberType", ci);
            btSearchDebtor.Text = rm.GetString("txtSearch", ci);
            //this.lbSearchBy.Text = rm.GetString("txtSearchBy",ci);			
            btnNew.Text = rm.GetString("txtNewUser", ci);

            lblUserListHeader.Text = rm.GetString("txtResult", ci);
        }

        private void SetColumnsHeaderByCulture()
        {
            dgDebtors.Columns[3].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dgDebtors.Columns[4].HeaderText = rm.GetString("txtName", ci);
            dgDebtors.Columns[5].HeaderText = rm.GetString("txtName", ci);
            dgDebtors.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dgDebtors.Columns[7].HeaderText = rm.GetString("txtAddress", ci);
            dgDebtors.Columns[8].HeaderText = rm.GetString("txtIDNumber", ci);
            dgDebtors.Columns[9].HeaderText = rm.GetString("txtCity", ci);
            dgDebtors.Columns[10].HeaderText = rm.GetString("txtCity", ci);
            dgDebtors.Columns[11].HeaderText = rm.GetString("txtPostalCode", ci);
        }

        private void AddEnterEvent()
        {
            var frm = FindControl("frmFindUser");
            foreach (Control ctrl in frm.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList)
                {
                    ((DropDownList)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void dgDebtors_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            if (Session["dgDebtors"] == null)
            {
                return;
            }
            var myTable = (DataTable)Session["dgDebtors"];
            var myView = myTable.DefaultView;

            myView.Sort = e.SortExpression;
            dgDebtors.DataSource = myView;
            dgDebtors.DataBind();
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            Session.Remove("dgDebtors");
            var url = ViewState["url"] as string;
            if (url != null)
            {
                Response.Redirect(url);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Session.Remove("dgDebtors");
            if (rbtnIndividual.Checked)
            {
                if (isCreatingSubscriber)
                {
                    Server.Transfer("NewCIUser.aspx?CreateSubscriber=True");
                }
                else if (subscriberCIID != -1)
                {
                    Server.Transfer("NewCIUser.aspx?Department=" + _departmentID + "&SubscriberCIID=" + subscriberCIID);
                }
                else
                {
                    Response.Redirect("NewCIUser.aspx");
                }
            }
            else
            {
                if (isCreatingSubscriber)
                {
                    Server.Transfer("NewCIUserC.aspx?CreateSubscriber=True");
                }
                else if (subscriberCIID != -1)
                {
                    Server.Transfer("NewCIUserC.aspx?Department=" + _departmentID + "&SubscriberCIID=" + subscriberCIID);
                }
                else
                {
                    Response.Redirect("NewCIUserC.aspx");
                }
            }
        }

        protected void rbtnIndividual_CheckedChanged(object sender, EventArgs e)
        {
            //this.lbDebtorSurName.Visible = true;
            //this.tbDebtorSurName.Visible = true;
            tbDebtorSurName.Enabled = true;

            //this.ddIDNumberType.Visible=true;
            tbDebtorSurName.Visible = true;
            lbDebtorSurName.Visible = true;

            lbDebtorFirstName.Text = rm.GetString("txtFirstName", ci);
            lblErrorMessage.Text = rm.GetString("txtIndividualNotFound", ci);

            if (isUpdatingUser)
            {
                lblPageHeader.Text = rm.GetString("txtUpdatingUser", ci) + " - " +
                                     rm.GetString("txtIndividualSearch", ci);
            }
            else if (isCreatingSubscriber)
            {
                lblPageHeader.Text = rm.GetString("txtCreatingSubscriber", ci) + " - " +
                                     rm.GetString("txtIndividualSearch", ci);
            }
            else
            {
                lblPageHeader.Text = rm.GetString("txtAddUser", ci) + " - " + rm.GetString("txtIndividualSearch", ci);
            }
            lblErrorMessage.Visible = false;
            dgDebtors.DataSource = null;
            dgDebtors.DataBind();

            outerGridTable.Visible = false;
        }

        protected void rbtnCompany_CheckedChanged(object sender, EventArgs e)
        {
            //this.lbDebtorSurName.Visible = false;
            //this.tbDebtorSurName.Visible = false;

            //this.ddIDNumberType.Visible=false;
            tbDebtorSurName.Visible = false;
            lbDebtorSurName.Visible = false;

            tbDebtorSurName.Enabled = false;
            lbDebtorFirstName.Text = rm.GetString("txtCompanyName", ci);
            lblErrorMessage.Text = rm.GetString("txtCompanyNotFound", ci);

            if (isUpdatingUser)
            {
                lblPageHeader.Text = rm.GetString("txtUpdatingUser", ci) + " - " + rm.GetString("txtCompanySearch", ci);
            }
            else if (isCreatingSubscriber)
            {
                lblPageHeader.Text = rm.GetString("txtCreatingSubscriber", ci) + " - " +
                                     rm.GetString("txtCompanySearch", ci);
            }
            else
            {
                lblPageHeader.Text = rm.GetString("txtAddUser", ci) + " - " + rm.GetString("txtIndividualSearch", ci);
            }
            lblErrorMessage.Visible = false;
            dgDebtors.DataSource = null;
            dgDebtors.DataBind();

            outerGridTable.Visible = false;
        }

        private void dgDebtors_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            /*if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
			{
				((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
				((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtUpdate",ci);
			}*/

            //CreateExplanationIcons(dgDebtors.Columns, lblDatagridIcons);
            WebDesign.CreateExplanationIcons(dgDebtors.Columns, lblDatagridIcons, rm, ci);
        }

        protected void ddIDNumberType_SelectedIndexChanged(object sender, EventArgs e) { }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgDebtors.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDebtors_ItemCommand);
            this.dgDebtors.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgDebtors_SortCommand);
            this.dgDebtors.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgDebtors_ItemDataBound);
        }

        #endregion
    }
}