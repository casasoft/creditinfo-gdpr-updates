<%@ Page Language="c#" CodeBehind="NewCIUserC.aspx.cs" AutoEventWireup="false" Inherits="UserAdmin.NewCIUserC" %>

<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>New CIUserC</title>

    <script language="JavaScript" src="../DatePicker.js"></script>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        BR.pageEnd
        {
            page-break-after: always;
        }
    </style>

    <script language="javascript">
        function checkEnterKey() {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                newuser.btnSave.click();
            }
        }

        function SetFormFocus() {
            document._newuser.tbCompanyNameNative.focus();
        }

    </script>

</head>
<body bgcolor="#ffffff" onload="SetFormFocus()" ms_positioning="GridLayout">
    <form id="_newuser" name="newuser" action="" method="post" runat="server">
    <table width="997" height="600" align="center" border="0">
        <tr>
            <td colspan="4">
                <uc1:head ID="Head1" runat="server"></uc1:head>
            </td>
        </tr>
        <tr valign="top">
            <td width="1">
            </td>
            <td>
                <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <uc1:language ID="Language1" runat="server"></uc1:language>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <ucl:options ID="Options1" runat="server"></ucl:options>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" height="1" bgcolor="#000000">
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <uc1:sitePositionBar ID="SitePositionBar1" runat="server"></uc1:sitePositionBar>
                        </td>
                        <td align="right">
                            <ucl:UserInfo ID="UserInfo1" runat="server"></ucl:UserInfo>
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td width="150" valign="top" align="left">
                            <table width="98%">
                                <tr>
                                    <td>
                                        <uc1:panelBar ID="PanelBar1" runat="server"></uc1:panelBar>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2">
                            <!-- Main Body Starts -->
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lbNewCompany" runat="server">New Company</asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <asp:Label ID="lbDebtorCIID" runat="server" Visible="False">CreditInfoID</asp:Label>
                                                        <asp:TextBox ID="tbCompanyCIID" runat="server" CssClass="rammi" ReadOnly="True" Visible="False"></asp:TextBox>
                                                        <tr>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbCompanyName" runat="server">Name</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbCompanyNameNative" runat="server" EnableViewState="False"></asp:TextBox>
                                                                <asp:Label ID="Label1" runat="server" CssClass="error_text">*</asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvCompanyNameNative" runat="server" ControlToValidate="tbCompanyNameNative"
                                                                    ErrorMessage="Value missing!" Display="None"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbCompanyNameEN" runat="server">Name(EN)</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbCompanyNameEN" runat="server" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbCompFunction" runat="server">Company function</asp:Label>
                                                                <br>
                                                                <asp:DropDownList ID="ddCompanyNace" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbOrg_status" runat="server">Org status</asp:Label>
                                                                <br>
                                                                <asp:DropDownList ID="ddOrgStatus" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="Label4" runat="server" CssClass="error_text">*</asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvOrgStatus" runat="server" ErrorMessage="Value missing!"
                                                                    ControlToValidate="ddOrgStatus" Display="None"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbEstablished" runat="server">Established</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbCompanyEstablished" runat="server" EnableViewState="False"></asp:TextBox>&nbsp;
                                                                <input onclick="PopupPicker('tbCompanyEstablished', 250, 250);" type="button" value="..."
                                                                    class="popup">
                                                                <asp:Label ID="Label2" runat="server" CssClass="error_text">*</asp:Label>
                                                                <asp:CustomValidator ID="cvCompanyEstablished" runat="server" ErrorMessage="Date input incorrect!"
                                                                    ControlToValidate="tbCompanyEstablished" Display="None"></asp:CustomValidator>
                                                                <asp:RequiredFieldValidator ID="rfvCompanyEstablished" runat="server" ErrorMessage="Value missing!"
                                                                    ControlToValidate="tbCompanyEstablished" Display="None"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRegistered" runat="server">Registered</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbRegistered" runat="server" EnableViewState="False"></asp:TextBox>&nbsp;
                                                                <input onclick="PopupPicker('tbRegistered', 250, 250);" type="button" value="..."
                                                                    class="popup">
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbLastContacted" runat="server">Last Contacted</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbCompanyLContacted" runat="server" EnableViewState="False" ReadOnly="True"></asp:TextBox>
                                                                &nbsp;<input class="popup" onclick="PopupPicker('tbCompanyLContacted', 250, 250);"
                                                                    type="button" value="...">
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblIsSearchable" runat="server">[IsSearchable]</asp:Label><br>
                                                                <asp:RadioButton ID="rbtIsSearchableTrue" runat="server" Checked="True" GroupName="IsSearchable"
                                                                    Text="[Yes]" CssClass="radio"></asp:RadioButton>&nbsp;<asp:RadioButton ID="rbtIsSearchableFalse"
                                                                        runat="server" GroupName="IsSearchable" Text="[No]" CssClass="radio"></asp:RadioButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Label ID="lbEmail" runat="server">Email</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbEmail" runat="server" CssClass="long_input"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lbCompURL" runat="server">URL</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbCompanyURL" runat="server" CssClass="long_input" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td height="23"><asp:label id="lbldonotdelete" runat="server">Do Not Delete</asp:label><br>
                                                                <asp:checkbox id=chkdoNotDelete runat="server" cssclass="radio" ></asp:checkbox></td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblAddressOne" runat="server"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress1" runat="server">Address1</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbAddress1Native" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress1EN" runat="server">Address1(EN)</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbAddress1EN" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress1Info" runat="server">Address1 Info</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbAddress1InfoN" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbAddr1InfoEN" runat="server">Address1 Info(EN)</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbAddress1InfoEN" runat="server" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 63px">
                                                                <asp:Label ID="lbPostalCode" runat="server">PostalCode1</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbPostalCode1" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td style="height: 63px">
                                                                <asp:Label ID="lbPostBox" runat="server">PostBox1</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbPostBox1" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td style="height: 63px" colspan="2">
                                                                <asp:Label ID="lbCity1" runat="server">City1</asp:Label>
                                                                <asp:Label ID="lblCitySearch" runat="server">Search</asp:Label>
                                                                <br>
                                                                <asp:DropDownList ID="ddCity1" runat="server" CssClass="rammi">
                                                                </asp:DropDownList>
                                                                &nbsp;
                                                                <asp:TextBox ID="txtCitySearch" runat="server" MaxLength="10" Width="50px"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnCitySearch" runat="server" Text="..." CausesValidation="False"
                                                                    CssClass="popup"></asp:Button>
                                                                <asp:Label ID="Label5" runat="server" CssClass="error_text">*</asp:Label>
                                                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddCity1"
                                                                    ErrorMessage="Value missing!" Display="None"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblAddressTwo" runat="server"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress2" runat="server">Address2</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbAddress2Native" runat="server" EnableViewState="False"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress2EN" runat="server">Address2 (EN)</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbAddress2EN" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lbAddress2Info" runat="server">Address2 Info</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbAddress2InfoN" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbAddress2InfoEN" runat="server">Address2 Info (EN)</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbAddress2InfoEN" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbPostalCode2" runat="server">Postal Code2</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbPostalCode2" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbPostBox2" runat="server">PostBox2</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbPostBox2" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lbCity2" runat="server">City2</asp:Label>
                                                                <asp:Label ID="lblCity2Search" runat="server">Search</asp:Label>
                                                                <br>
                                                                <asp:DropDownList ID="ddCity2" runat="server" CssClass="rammi">
                                                                </asp:DropDownList>
                                                                &nbsp;
                                                                <asp:TextBox ID="txtCity2Search" runat="server" MaxLength="10" Width="50px"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnCity2Search" runat="server" Text="..." CausesValidation="False"
                                                                    CssClass="popup"></asp:Button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblOtherInfo" runat="server"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="width: 180px">
                                                                <asp:Label ID="lbIDNumber1" runat="server">IDNumber 1</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbIDNumber1" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 180px">
                                                                <asp:Label ID="lbType1" runat="server">Type</asp:Label>
                                                                <br>
                                                                <asp:DropDownList ID="ddIDNumberType1" runat="server" CssClass="rammi">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 180px">
                                                                <asp:Label ID="lbIDNumber2" runat="server">IDNumber 2</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbIDNumber2" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td style="height: 57px">
                                                                <asp:Label ID="lbType2" runat="server">Type</asp:Label>
                                                                <br>
                                                                <asp:DropDownList ID="ddIDNumberType2" runat="server" CssClass="rammi">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbPhoneNumber" runat="server">Phone number</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbPhoneNumber1" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbFaxNumber" runat="server">Fax number</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbPhoneNumber2" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lblNote" runat="server">Note</asp:Label>
                                                                <br>
                                                                <asp:TextBox ID="tbNote" runat="server" CssClass="rammi"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="tblVatNumbers" runat="server">
                                             <table class="grid_table" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblHeaderVatNumbers" runat="server">Vat numbers</asp:Label>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="fields" id="Table1" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <table class="datagrid" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:DataGrid ID="dgVatNumbers" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                    AllowSorting="True" GridLines="None">
                                                                                    <FooterStyle CssClass="grid_footer"></FooterStyle>
                                                                                    <SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
                                                                                    <AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
                                                                                    <ItemStyle CssClass="grid_item"></ItemStyle>
                                                                                    <HeaderStyle CssClass="grid_header"></HeaderStyle>
                                                                                    <Columns>
                                                                                        <asp:BoundColumn DataField="Id" SortExpression="Id" HeaderText="Id">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                         <asp:BoundColumn DataField="TraderName" SortExpression="TraderName" HeaderText="Trader name">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="MTNumber" SortExpression="MTNumber" HeaderText="Vat number">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Building" SortExpression="Building" HeaderText="Building">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="Address">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Street" SortExpression="Street" HeaderText="Street">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="City" SortExpression="City" HeaderText="City">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="PostCode" SortExpression="PostCode" HeaderText="Post code">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Tel" SortExpression="Tel" HeaderText="Tel">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Fax" SortExpression="Fax" HeaderText="Fax">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Note" SortExpression="Note" HeaderText="Note">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="ShowTextFlag" SortExpression="ShowTextFlag" HeaderText="Show">
                                                                                            <ItemStyle CssClass="padding"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                    </Columns>
                                                                                    <PagerStyle CssClass="grid_pager"></PagerStyle>
                                                                                </asp:DataGrid>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="23">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>                                           
                                            </asp:Panel>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="10">
                                        </td>
                                    </tr>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="empty_table" cellspacing="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lblError" runat="server" Width="400px" CssClass="error_text"></asp:Label>
                                                    <br>
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="rammi" Width="168px">
                                                    </asp:ValidationSummary>
                                                </td>
                                                <td align="right">
                                                    <asp:Button ID="btDelete" runat="server" CssClass="cancel_button" CausesValidation="False"
                                                                Text="Delete" OnClick="btDelete_Click"></asp:Button>
                                                    <asp:Button ID="btCancel" runat="server" CssClass="cancel_button" Text="cancel">
                                                    </asp:Button><asp:Button ID="btnContinue" runat="server" CssClass="gray_button" Text="Continue"
                                                        Visible="False"></asp:Button><asp:Button ID="btnSave" runat="server" CssClass="confirm_button"
                                                            Text="Save"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- Main Body Ends -->
                        </td>
                    </tr>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" height="1" bgcolor="#000000">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="2">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <uc1:footer ID="Footer1" runat="server"></uc1:footer>
                <uc1:SecureUserAdmin ID="SecureUserAdmin1" runat="server"></uc1:SecureUserAdmin>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
