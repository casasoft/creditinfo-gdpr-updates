﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserAdmin.BLL;

namespace cb3.UserAdmin
{
    public partial class UserGroup : System.Web.UI.Page
    {

        private readonly uaFactory myFactory = new uaFactory();


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ID = "102";
            ListUserGroups();
            

        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgUserGroupList.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgUserGroupList_ItemCommand);
            //this.dgUserGroupList.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgUsers_ItemDataBound);

        }
        #endregion


        private void ListUserGroups()
        {
            var userGroupsSet = myFactory.GetAllUserGroups();

            if (userGroupsSet.Tables.Count > 0 && userGroupsSet.Tables[0].Rows.Count > 0)
            {
                this.dgUserGroupList.DataSource = userGroupsSet.Tables[0].DefaultView;
                this.dgUserGroupList.DataBind();

            }
        }



        protected void btnNew_Click(object sender, EventArgs e) { Server.Transfer("UserGroup.aspx?newUserGroup=True"); }



        protected void dgUserGroupList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Edit"))
            {
                string GroupID = e.Item.Cells[1].Text.Trim();
                Server.Transfer("UserGroup.aspx?newUserGroup=true&GroupID=" + GroupID);
            }

        }


    }
}