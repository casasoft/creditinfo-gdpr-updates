﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserGroup.aspx.cs" Inherits="cb3.UserAdmin.UserGroup1" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="radt" Namespace="Telerik.WebControls" Assembly="RadTreeView" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
    <title>User Group</title>
    <script language="JavaScript" src="../DatePicker.js"></script>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
    
    <script>

        function UpdateAllChildren(nodes, checked)
        {
            var i;
            for (i=0; i<nodes.length; i++)
            {
                if (checked)       
                    nodes[i].Check();       
                else      
                    nodes[i].UnCheck(); 
					      
                if (nodes[i].Nodes.length > 0)      
                    UpdateAllChildren(nodes[i].Nodes, checked);      
            }
        }

        function AfterCheck(node)
        {
            if (!node.Checked && node.Parent != null)
                node.Parent.UnCheck();   
				      
            var siblingCollection = (node.Parent != null) ? node.Parent.Nodes : node.TreeView.Nodes;
				   
            var allChecked = false;
            for (var i=0; i<siblingCollection.length; i++)
            {
                if(siblingCollection[i].Checked)
                {
                    allChecked = true;
                    break;
                }
            }
				
            if (allChecked && node.Parent != null)
            {
                node.Parent.Check();
            }
				   
            UpdateAllChildren(node.Nodes, node.Checked);
        }

    </script>

</HEAD>
<body ms_positioning="GridLayout">
<form id="UserGroup" name="PendingCase" method="post" runat="server">
    <table width="997" height="600" align="center" border="0">
        <tr>
            <td colspan="4">
                <uc1:head id="Head1" runat="server"></uc1:head>
            </td>
        </tr>
        <tr valign="top">
            <td width="1"></td>
            <td>
                <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <uc1:language id="Language1" runat="server"></uc1:language>
                        </td>
                        <td></td>
                        <td align="right">
                            <ucl:options id="Options1" runat="server"></ucl:options>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
                        </td>
                        <td align="right">
                            <ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
                        </td>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr valign="top">
                        <td width="150" valign="top" align="left">
                            <table width="98%">
                                <tr>
                                    <td>
                                        <uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2">
                            <!-- Main Body Starts -->
                            <table width="100%">
                                <tr>
                                    <td>
                                        <!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
                                        <table class="grid_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
                                                        <asp:HiddenField ID="ID" runat="server" value="" />
                                                        <tr>
                                                            <td style="WIDTH: 25%">
                                                                <asp:label id="lblName" runat="server">Group Name</asp:label>
                                                                <br>
                                                                <asp:textbox id="txtName" runat="server"></asp:textbox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Group Name cannot be blank" ControlToValidate="txtName" ForeColor="Red"></asp:RequiredFieldValidator>  
                                                            </td>
                                                            <TD style="WIDTH: 25%">
                                                                <asp:label id="lblGrpCode" runat="server">Group Code</asp:label><BR>
                                                                <asp:textbox id="txtGrpCode" runat="server"></asp:textbox></TD>
                                                            <td style="WIDTH: 25%">
                                                                <asp:label id="lblGrpDesc" runat="server">Group Description</asp:label>
                                                                <br>
                                                                <asp:textbox id="txtGrpDesc" runat="server"></asp:textbox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="23"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    <table class=grid_table cellSpacing=0 cellPadding=0 
                                                    >
                                                        <tr>
                                                            <th><asp:label id=lblUserRights runat="server">User rights</asp:label></th></tr>
                                                        <tr>
                                                            <td>
                                                                <table class=fields id=tbTrees cellSpacing=0 
                                                                       cellPadding=0>
                                                                    <tr>
                                                                        <td id=tdInternal style="WIDTH: 50%" vAlign=top 
                                                                            runat="server"><asp:label id=lblInternal runat="server">Internal</asp:label><br 
                                                                                                                                                        >
                                                                            <div class=tree><radt:radtreeview id=internalGroupTree runat="server" causesvalidation="False" checkboxes="True" afterclientcheck="AfterCheck"></radt:radtreeview></div></td>
                                                                        <td vAlign=top><asp:label id=lblExternal runat="server">External</asp:label><br 
                                                                                                                                                    >
                                                                            <div class=tree><radt:radtreeview id=externalGroupTree runat="server" causesvalidation="False" checkboxes="True" afterclientcheck="AfterCheck"></radt:radtreeview></div></td></tr>
                                                                    <tr>
                                                                        <td height=23 
                                                                        ></td></tr></table></td></tr></table></td></tr>
                                        </table>
                                        <!-- END // GRID TABLE WITHOUT STEPS // END -->
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
                                        <table class="empty_table" cellspacing="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:label id="lblMsg" runat="server" visible="False" cssclass="error_text"></asp:label>
                                                </td>
                                                <td align="right">
                                                 <asp:button id="btnSubmit" runat="server" cssclass="confirm_button" text="submit" OnClick="btnSubmit_Click"></asp:button>
                                                    <asp:button id="Delete" runat="server" cssclass="cancel_button" text="delete" OnClick="Delete_Click" ></asp:button>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
                                    </td>
                                </tr>
                            </table>
                            <!-- Main Body Ends -->
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
                    </tr>
                </table>
            </td>
            <td width="2"></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <uc1:footer id="Footer1" runat="server"></uc1:footer>
            </td>
        </tr>
    </table>
</form>
</body>
</HTML>

