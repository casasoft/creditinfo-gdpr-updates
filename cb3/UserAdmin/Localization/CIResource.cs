using System;
using System.Reflection;
using System.Resources;
using System.Web;

namespace UserAdmin.Localization {
    /// <summary>
    /// Summary description for CIResource.
    /// </summary>
    public class CIResource {
        private const string ApplicationVariableName = "UserAdmin";

        /// <summary>
        /// <para>The CurrentManager property returns and caches the shared ResourceManager instance.
        /// </para>
        /// </summary>
        public static ResourceManager CurrentManager {
            get {
                var context = HttpContext.Current;
                if (null == context) {
                    throw new ArgumentException("Global_NoHttpContext");
                }

                var mgr = new ResourceManager(
                    "cb3.UserAdmin.resources.strings", Assembly.GetExecutingAssembly(), null);

                    // Add to the cache
                    context.Cache.Insert(ApplicationVariableName, mgr);
                return mgr;
            }
        }
    }
}