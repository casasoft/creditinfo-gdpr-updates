﻿using System.Web;
using System.Web.Caching;
using System;

namespace cb3.CacheBase
{
    public class HttpCacheAdapter : ICache
    {
        public Cache HttpCache { get { return HttpContext.Current.Cache; } }

        public T Get<T>(string key) where T : class
        {
            return (HttpCache[key] as T);
        }

        public T GetOrLoad<T>(string key, Func<T> loadFunc) where T : class
        {
            if (this.HasKey(key))
            {
                return this.Get<T>(key);
            }
            else
            {
                var data = loadFunc();
                this.Store(key, data);
                return data;
            }
        }

        public T GetOrLoad<T>(string key, Func<T> loadFunc, TimeSpan duration) where T : class
        {
            if (this.HasKey(key))
            {
                return this.Get<T>(key);
            }
            else
            {
                var data = loadFunc();
                this.Store(key, data, duration);
                return data;
            }
        }

        public T? GetOrLoadValue<T>(string key, Func<T?> loadFunc) where T : struct
        {
            if (this.HasKey(key))
            {
                return this.GetValue<T>(key);
            }
            else
            {
                var data = loadFunc();
                this.Store(key, data);
                return data;
            }
        }

        public T? GetOrLoadValue<T>(string key, Func<T?> loadFunc, TimeSpan duration) where T : struct
        {
            if (this.HasKey(key))
            {
                return this.GetValue<T>(key);
            }
            else
            {
                var data = loadFunc();
                this.Store(key, data, duration);
                return data;
            }
        }

        public T? GetValue<T>(string key) where T : struct
        {
            try
            {
                return (T)HttpCache[key];
            }
            catch (Exception)
            {
                return null as T?;
            }
        }

        public bool HasKey(string key)
        {
            return HttpCache[key] != null;
        }

        public void Store(string key, object item)
        {
            HttpCache[key] = item;
        }

        public void Store(string key, object item, TimeSpan duration)
        {
            var expiration = DateTime.Now.Add(duration);
            if (item == null)
            {
                HttpCache.Add(key, DBNull.Value, null, expiration, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
            }
            else
                HttpCache.Add(key, item, null, expiration, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
        }
    }
}