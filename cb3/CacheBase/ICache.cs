﻿using System;

namespace cb3.CacheBase
{
    public interface ICache
    {
        T Get<T>(string key) where T : class;

        /// <summary> Get methods for value types / structs </summary>
        Nullable<T> GetValue<T>(string key) where T : struct;

        bool HasKey(string key);

        void Store(string key, object item);

        void Store(string key, object item, TimeSpan duration);

        T GetOrLoad<T>(string key, Func<T> loadFunc) where T : class;

        T GetOrLoad<T>(string key, Func<T> loadFunc, TimeSpan duration) where T : class;

        T? GetOrLoadValue<T>(string key, Func<T?> loadFunc) where T : struct;

        T? GetOrLoadValue<T>(string key, Func<T?> loadFunc, TimeSpan duration) where T : struct;
    }
}