using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Routing;
using DataProtection;
using Logging.BLL;
using UserAdmin.DAL;
using cb3;
using cb3.Settings;

using Cig.Framework.Base.Configuration;
using System.Web.Routing;
using UserAdmin.BLL;

namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for Global.
    /// </summary>
    public class Global : HttpApplication {
        public Global() { InitializeComponent(); }
        protected void Application_Start(Object sender, EventArgs e) {
            RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_Start(Object sender, EventArgs e) {
            Session["LoginTryCount"] = AuUserSettings.NUMBER_OF_WRONG_PASSWORD_ATTEMPT;
            Session["Defculture"] = CigConfig.Configure("lookupsettings.nativeCulture");
        }

        private static void RegisterRoutes(RouteCollection routes)
        {
            var handler = new DocumentRouteHandler();
            routes.Add("ImagesRoute1", new Route("Document/{DocumentId}", handler));
            routes.Add("ImagesRoute2", new Route("CrossBorder/Document/{DocumentId}", handler));
          //  routes.Add("ImagesRoute3", new Route("Malta/CrossBorder/Document/{DocumentId}", handler));
        }


        protected void Application_BeginRequest(Object sender, EventArgs e) {
            //Haukur - 30.05.2005 - Did not work well - somtimes https - sometimes http - moved into config
            /*string AbsUri  = Request.Url.AbsoluteUri.ToString();
			string RawUrl  = Request.RawUrl.ToString();
			string CorrPath = AbsUri.Substring(0, AbsUri.Length - RawUrl.Length);
			string AppPath = Request.ApplicationPath;
			if(AppPath == "/")
				Application.Add("AppPath",CorrPath);
			else
				Application.Add("AppPath",AppPath);*/

            Application.Add("AppPath", CigConfig.Configure("lookupsettings.AppPath"));
        }

        protected void Application_EndRequest(Object sender, EventArgs e) { }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e) {
            if (Request.IsAuthenticated) {
                IIdentity user = Context.User.Identity;
                Context.User = new GenericPrincipal(user, GetProducts(user.Name).Split(new[] {'|'}));
            }
        }

        protected void Application_Error(Object sender, EventArgs e) {
            // Senda p�st � error@lt.is ef �me�h�ndlu� villa kemur upp � kerfinu...

            String myErrorMessage = "The error description is as follows :\n\n " + Server.GetLastError() +
                                    " \n\nStacktrace is \n\n" + Server.GetLastError().StackTrace;
            const string mySubject = "CreditInfoGroup Unhandled Error";

            Logger.WriteToLog(mySubject + "\n\n" + myErrorMessage, true);

            if (Server.GetLastError().ToString().IndexOf("Object reference not set to an instance of an object") > -1) {
                if (CigConfig.Configure("lookupsettings.logonURL") != null) {
                    Server.Transfer(CigConfig.Configure("lookupsettings.logonURL"));
                }
            }
            //HelpFunctions myFunctions = new HelpFunctions();

            //myFunctions.SendMail (myErrorMessage, mySubject);
        }

        protected void Session_End(Object sender, EventArgs e) { }
        protected void Application_End(Object sender, EventArgs e) { }

        protected void Application_AcquireRequestState(Object sender, EventArgs e) {
            String culture;

            // Grip � "locale" sem kemur � gegn um LISA vefinn...
            if (Request.Params["locale"] != null) {
                var locale = Request.Params["locale"];
                var currLocale = "el-GR";
                if (locale != null) {
                    currLocale = locale.Equals("EN") ? "en-US" : "el-GR";
                }
                Session["culture"] = currLocale;
                culture = currLocale;
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            }

            try {
                if (Session["culture"] != null) {
                    culture = (String) Session["culture"];
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
                }
            } catch (Exception) {
                // Gera e-� g�fulegt h�r
            }
        }

        private static string GetProducts(string UserName) {
            // Tek upp productID �annig a� �etta s� "country independent"...
            var roles = "";

            // Get the user and check if user has a group, if it has a group then use the role on the group
            var myFactory =  new uaFactory();
            var user = myFactory.GetSpecificUser(UserName);
            if(user != null && !string.IsNullOrEmpty(user.GroupCode))
            {
                var GroupID = myFactory.GetUserGroupID(user.GroupCode);
                if(GroupID != 0)
                {
                   var userProductsSet = myFactory.ReadGroupProducts(GroupID);
                    if(userProductsSet.Tables.Count > 0 && userProductsSet.Tables[0].Rows.Count > 0)
                    {
                        foreach(DataRow row in userProductsSet.Tables[0].Rows)
                        {
                            if (roles != "")
                            {
                                roles += "|";
                            }

                            roles += row["ProductID"].ToString();
                        }
                        return roles.Trim();

                    }

                }

            }


            OleDbCommand myOleDbCommand;
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT dbo.au_Products.ProductID FROM dbo.au_Products INNER JOIN dbo.au_ProductToUser ON dbo.au_Products.ProductID = dbo.au_ProductToUser.ProductID INNER JOIN dbo.au_users ON dbo.au_ProductToUser.UserID = dbo.au_users.id WHERE UserName = '" +
                            UserName + "'",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) // Advance to the one and only row
                    {
                        // Return output parameters from returned data stream
                        if (roles != "") {
                            roles += "|";
                        }

                        roles += (reader.GetInt32(0)).ToString();
                    }
                    reader.Close();
                } catch (Exception) {}
            }
            return roles.Trim();
        }

        #region Web Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { }

        #endregion
    }
}