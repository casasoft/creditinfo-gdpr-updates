using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;

namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for FoError.
    /// </summary>
    public class FoError : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected HyperLink hlMail;
        protected Label lblApology;
        protected Label lblEmail;
        protected Label lblError;
        protected Label lbRquestFailure;

        private void Page_Load(object sender, EventArgs e) {
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();
        }

        private void LocalizeText() {
            lbRquestFailure.Text = rm.GetString("lbRquestFailure", ci);
            lblError.Text = rm.GetString("lblError", ci);
            lblApology.Text = rm.GetString("lblApology", ci);
            lblEmail.Text = rm.GetString("lblEmail", ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}