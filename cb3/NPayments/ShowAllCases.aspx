<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="ShowAllCases.aspx.cs" AutoEventWireup="false" Inherits="NPayments.ShowAllCases" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>ShowAllCases</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css"> BR.pageEnd { PAGE-BREAK-AFTER: always } </style>
		<script>
			function checkEnterKey() 
			{    
				if (event.keyCode == 13) 
				{        
					event.cancelBubble = true;
					event.returnValue = false;
					fclaim.btSearchDebtor.click(); 
				}
			} 
			function SetFormFocus()
				{
					document.fclaim.tbDCIIDNumber.focus();
				}
		</script>
</head>
	<body>
		<form id="fclaim" name="findclaim" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head2" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language2" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="Sitepositionbar2" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelBar id="Panelbar2" runat="server"></uc1:panelBar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:Label id="lbAllCases" runat="server">All cases per customer</asp:Label>
														</th>
													</tr>
													<tr>
														<td>
															<TABLE class="fields" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<asp:label id="lblCreditInfoID" runat="server">CreditInfoID</asp:label></TD>
																	<TD>
																		<asp:Label id="lblCreditInfoIDValue" runat="server">Label</asp:Label></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblFirstName" runat="server">First name</asp:label></TD>
																	<TD>
																		<asp:Label id="lblFirstNameValue" runat="server">Label</asp:Label></TD>
																</TR>
																<TR>
																	<TD>
																		<asp:label id="lblSurName" runat="server">Surname</asp:label></TD>
																	<TD>
																		<asp:Label id="lblSurNameValue" runat="server">Label</asp:Label></TD>
																</TR>
															</TABLE>
														</td>
													</tr>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<TABLE class="grid_table" id="SearchGrid" cellSpacing="0" cellPadding="0" runat="server">
													<tr>
														<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<TR>
														<TH>
															<asp:label id="lblDgResults" runat="server"></asp:label></TH></TR>
													<TR>
														<TD id="Td2" width="100%" runat="server">
															<div class="TA" id="divAllCases" style="OVERFLOW-X: auto; OVERFLOW: auto; HEIGHT: auto"
																runat="server">
																<asp:datagrid id="dgClaim" runat="server" CssClass="rammi" ForeColor="Black" GridLines="None"
																	CellPadding="4" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE"
																	AutoGenerateColumns="False" AllowSorting="True">
																	<footerstyle cssclass="grid_footer"></FooterStyle>
																	<selecteditemstyle cssclass="grid_selecteditem"></SelectedItemStyle>
																	<alternatingitemstyle cssclass="grid_alternatingitem"></AlternatingItemStyle>
																	<itemstyle cssclass="grid_item"></ItemStyle>
																	<headerstyle cssclass="grid_header"></HeaderStyle>
																	<pagerstyle cssclass="grid_pager"></pagerstyle>
																	<columns>
																		<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;" CommandName="Select">
																			<itemstyle cssclass="leftpadding"></ItemStyle>
																		</asp:ButtonColumn>
																		<asp:BoundColumn DataField="CreditInfoID" SortExpression="CreditInfoID" HeaderText="CreditInfoID">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="Claim ID">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="RegDate" SortExpression="RegDate" HeaderText="Registed">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Initials" SortExpression="Initials" HeaderText="Employee">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn HeaderText="Owner">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="ClaimOwnerCIID" HeaderText="Owner CIID">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="RegisterCommentNative" SortExpression="RegisterCommentNative" HeaderText="Comment(N)">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="RegisterCommentEN" SortExpression="RegisterCommentEN" HeaderText="Comment (E)">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="NameNative" SortExpression="NameNative" HeaderText="Source(N)">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="NameEN" SortExpression="NameEN" HeaderText="Source (E)">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="StateNative" SortExpression="StateNative" HeaderText="State(N)">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="StateEN" SortExpression="StateEN" HeaderText="State (E)">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ClaimDate" SortExpression="ClaimDate" HeaderText="Claim Date">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="CaseID" SortExpression="CaseID" HeaderText="CaseID">
																			<itemstyle cssclass="padding"></ItemStyle>
																		</asp:BoundColumn>
																	</Columns>
																</asp:datagrid>
															</div>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
