<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="NewCIUserC.aspx.cs" AutoEventWireup="false" Inherits="NPayments.NewCIUserC" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>NewCIUserC</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css"> BR.pageEnd { PAGE-BREAK-AFTER: always } </style>
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						newuser.btRegCompany.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.newuser.tbCompanyEstablished.focus();
				}

		</SCRIPT>
	</HEAD>
	<body ms_positioning="GridLayout" onload="SetFormFocus()">
		<form id="newuser" name="newuser" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbNewCompany" runat="server">New Company</asp:label>-
															<asp:label id="lbCompanyInformation" runat="server">Company Info</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 263px">
																		<asp:label id="lbDebtorCIID" runat="server">CreditInfoID</asp:label>
																		<br>
																		<asp:textbox id="tbCompanyCIID" runat="server" enableviewstate="False" readonly="True"></asp:textbox>
																	</td>
																	<TD style="WIDTH: 253px">
																		<asp:label id="lbCompFunction" runat="server">Company function</asp:label><BR>
																		<asp:dropdownlist id="ddCompanyNace" runat="server"></asp:dropdownlist></TD>
																	<td>
																		<asp:label id="lbOrg_status" runat="server">Org status</asp:label><BR>
																		<asp:dropdownlist id="ddOrgStatus" runat="server"></asp:dropdownlist></td>
																</tr>
																<tr>
																	<td style="WIDTH: 263px">
																		<asp:label id="lbCompanyName" runat="server">Name</asp:label>
																		<br>
																		<asp:textbox id="tbCompanyNameNative" runat="server" enableviewstate="False"></asp:textbox>
																		<asp:requiredfieldvalidator id="rfvName" runat="server" errormessage="RequiredFieldValidator" controltovalidate="tbCompanyNameNative">*</asp:requiredfieldvalidator>
																	</td>
																	<TD style="WIDTH: 253px">
																		<asp:label id="lbCompanyNameEN" runat="server">Name(EN)</asp:label><BR>
																		<asp:textbox id="tbCompanyNameEN" runat="server" enableviewstate="False"></asp:textbox></TD>
																	<td>
																		<asp:label id="lbEstablished" runat="server">Established</asp:label><BR>
																		<asp:textbox id="tbCompanyEstablished" runat="server" enableviewstate="False"></asp:textbox>&nbsp;<INPUT class="popup" onclick="PopupPicker('tbCompanyEstablished', 250, 250);" type="button"
																			value="...">&nbsp;
																		<asp:customvalidator id="cvCompanyEstablished" runat="server" controltovalidate="tbCompanyEstablished"
																			errormessage="Date input incorrect!" cssclass="rammi">*</asp:customvalidator>
																		<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" controltovalidate="tbCompanyEstablished"
																			errormessage="Value missing!">*</asp:requiredfieldvalidator>
																	</td>
																</tr>
																<tr>
																	<td style="WIDTH: 263px">
																		<asp:label id="lblRegistered" runat="server">Registered</asp:label><BR>
																		<asp:textbox id="tbRegistered" runat="server" enableviewstate="False"></asp:textbox>&nbsp;<INPUT class="popup" onclick="PopupPicker('tbRegistered', 250, 250);" type="button" value="...">
																	</td>
																	<TD style="WIDTH: 253px">
																		<asp:label id="lbLastContacted" runat="server">Last Contacted</asp:label><BR>
																		<asp:textbox id="tbCompanyLContacted" runat="server" enableviewstate="False"></asp:textbox>&nbsp;<INPUT class="popup" onclick="PopupPicker('tbCompanyLContacted', 250, 250);" type="button"
																			value="...">&nbsp;
																		<asp:customvalidator id="cvLastContacted" runat="server" controltovalidate="tbCompanyLContacted" errormessage="Date inpur incorrect!"
																			cssclass="rammi">*</asp:customvalidator></TD>
																	<TD>
																		<asp:label id="lbCompURL" runat="server">URL</asp:label><BR>
																		<asp:textbox id="tbCompanyURL" runat="server" enableviewstate="False"></asp:textbox></TD>
																</tr>
																<TR>
																	<TD style="WIDTH: 263px">
																		<asp:label id="lbEmail" runat="server">Email</asp:label><BR>
																		<asp:textbox id="tbEmail" runat="server"></asp:textbox></TD>
																	<TD style="WIDTH: 253px"></TD>
																	<TD></TD>
																</TR>
																<tr>
																	<td height="23" style="WIDTH: 263px"></td>
																	<TD style="WIDTH: 253px" height="23"></TD>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblAddressInfo" runat="server">[AddressInfo]</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 189px">
																		<asp:label id="lbAddress1" runat="server">Address1</asp:label>
																		<br>
																		<asp:textbox id="tbAddress1Native" runat="server"></asp:textbox>
																		<asp:requiredfieldvalidator id="rfvAddress" runat="server" errormessage="RequiredFieldValidator" controltovalidate="tbAddress1Native">*</asp:requiredfieldvalidator>
																	</td>
																	<TD style="HEIGHT: 63px">
																		<asp:label id="lbAddress1EN" runat="server">Address1(EN)</asp:label><BR>
																		<asp:textbox id="tbAddress1EN" runat="server"></asp:textbox></TD>
																	<td style="HEIGHT: 63px" colSpan="2">
																		<asp:label id="lbCity1" runat="server">City1</asp:label>
																		<asp:label id="lblCitySearch" runat="server">Search</asp:label>
																		<br>
																		<asp:dropdownlist id="ddCity1" runat="server"></asp:dropdownlist>&nbsp;
																		<asp:textbox id="txtCitySearch" runat="server" width="40px" maxlength="10"></asp:textbox>
																		<asp:button id="btnCitySearch" runat="server" text="..." causesvalidation="False" cssclass="popup"></asp:button>&nbsp;
																		<asp:requiredfieldvalidator id="rfvCity" runat="server" controltovalidate="ddCity1" errormessage="Value missing!">*</asp:requiredfieldvalidator>
																	</td>
																</tr>
																<tr>
																	<td style="WIDTH: 189px">
																		<asp:label id="lbAddress1Info" runat="server">Address1 Info</asp:label>
																		<br>
																		<asp:textbox id="tbAddress1InfoN" runat="server"></asp:textbox>
																	</td>
																	<TD>
																		<asp:label id="lbAddr1InfoEN" runat="server">Address1 Info(EN)</asp:label><BR>
																		<asp:textbox id="tbAddress1InfoEN" runat="server" enableviewstate="False"></asp:textbox></TD>
																	<td>
																		<asp:label id="lbPostalCode" runat="server">PostalCode1</asp:label>
																		<br>
																		<asp:textbox id="tbPostalCode1" runat="server"></asp:textbox>
																	</td>
																	<TD>
																		<asp:label id="lbPostBox" runat="server">PostBox1</asp:label><BR>
																		<asp:textbox id="tbPostBox1" runat="server"></asp:textbox></TD>
																</tr>
																<tr>
																	<td height="23" style="WIDTH: 189px"></td>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblAddressInfo2" runat="server">[AddressInfo]</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 189px">
																		<asp:label id="lbAddress2" runat="server">Address2</asp:label>
																		<br>
																		<asp:textbox id="tbAddress2Native" runat="server" enableviewstate="False"></asp:textbox>
																	</td>
																	<TD>
																		<asp:label id="lbAddress2EN" runat="server">Address2 (EN)</asp:label><BR>
																		<asp:textbox id="tbAddress2EN" runat="server"></asp:textbox></TD>
																	<TD colSpan="2">
																		<asp:label id="lbCity2" runat="server">City2</asp:label>
																		<asp:label id="lblCity2Search" runat="server">Search</asp:label><BR>
																		<asp:dropdownlist id="ddCity2" runat="server"></asp:dropdownlist>&nbsp;
																		<asp:textbox id="txtCity2Search" runat="server" maxlength="10" width="40px"></asp:textbox>&nbsp;
																		<asp:button id="btnCity2Search" runat="server" cssclass="popup" causesvalidation="False" text="..."></asp:button>&nbsp;
																		<asp:requiredfieldvalidator id="rfvCity2" runat="server" controltovalidate="ddCity2" errormessage="Value missing!"
																			enabled="False">*</asp:requiredfieldvalidator></TD>
																</tr>
																<tr>
																	<td style="WIDTH: 189px">
																		<asp:label id="lbAddress2Info" runat="server">Address2 Info</asp:label>
																		<br>
																		<asp:textbox id="tbAddress2InfoN" runat="server"></asp:textbox>
																	</td>
																	<TD>
																		<asp:label id="lbAddress2InfoEN" runat="server">Address2 Info (EN)</asp:label><BR>
																		<asp:textbox id="tbAddress2InfoEN" runat="server"></asp:textbox></TD>
																	<TD>
																		<asp:label id="lbPostalCode2" runat="server">Postal Code2</asp:label><BR>
																		<asp:textbox id="tbPostalCode2" runat="server"></asp:textbox></TD>
																	<td>
																		<asp:label id="lbPostBox2" runat="server">PostBox2</asp:label>
																		<br>
																		<asp:textbox id="tbPostBox2" runat="server"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23" style="WIDTH: 189px"></td>
																	<TD height="23"></TD>
																	<TD height="23"></TD>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblAdditionalInfo" runat="server">[AdditionalInfo]</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 187px">
																		<asp:label id="lbIDNumber1" runat="server">IDNumber 1</asp:label>
																		<br>
																		<asp:textbox id="tbIDNumber1" runat="server"></asp:textbox>
																	</td>
																	<td style="WIDTH: 188px">
																		<asp:label id="lbType1" runat="server">Type</asp:label>
																		<br>
																		<asp:dropdownlist id="ddIDNumberType1" runat="server"></asp:dropdownlist>
																	</td>
																	<td style="WIDTH: 186px">
																		<asp:label id="lbIDNumber2" runat="server">IDNumber 2</asp:label>
																		<br>
																		<asp:textbox id="tbIDNumber2" runat="server"></asp:textbox>
																	</td>
																	<td>
																		<asp:label id="lbType2" runat="server">Type</asp:label>
																		<br>
																		<asp:dropdownlist id="ddIDNumberType2" runat="server"></asp:dropdownlist>
																	</td>
																</tr>
																<tr>
																	<td style="WIDTH: 187px">
																		<asp:label id="lbPhoneNumber" runat="server">Phone number</asp:label>
																		<br>
																		<asp:textbox id="tbPhoneNumber1" runat="server"></asp:textbox>
																	</td>
																	<td style="WIDTH: 188px">
																		<asp:label id="lbFaxNumber" runat="server">Fax number</asp:label>
																		<br>
																		<asp:textbox id="tbPhoneNumber2" runat="server"></asp:textbox>
																	</td>
																	<td height="23" style="WIDTH: 187px">
																		<asp:label id="lbNote" runat="server">Note</asp:label>
																		<br>
																		<asp:textbox id="tbNote" runat="server"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23" style="WIDTH: 187px">
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:label id="lblMsg" runat="server" cssclass="error_text">Label</asp:label>
															<asp:validationsummary id="ValidationSummary1" runat="server" width="216px"></asp:validationsummary>
															<asp:textbox id="tbPageID" runat="server" visible="False" height="12px"></asp:textbox>
														</td>
														<td align="right">
															<asp:button id="btCancel" runat="server" cssclass="cancel_button" text="cancel" CausesValidation="False"></asp:button><asp:button id="btRegCompany" runat="server" cssclass="confirm_button" text="submit"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
