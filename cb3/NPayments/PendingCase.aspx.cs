using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPayments.BLL;
using NPayments.BLL.Localization;
using NPayments.BLL.PendingCases;

using Cig.Framework.Base.Configuration;


namespace NPayments {
    /// <summary>
    /// Summary description for PendingCase.
    /// </summary>
    public class PendingCase : Page {
        protected Button btnCloseCase;
        protected Button btnNew;
        protected Button btnSubmit;
        private CultureInfo ci;
        protected NPaymentsFactory claimFactory;
        private string culture;
        protected DropDownList ddAdjudicator;
        protected DropDownList ddCurrency;
        protected DropDownList ddInfoSource;
        protected Label lbComment;
        protected Label lblAdjudicator;
        protected Label lblCaseID;
        protected Label lblCaseNumber;
        protected Label lblDateRegistered;
        protected Label lblDefendantID;
        protected Label lblDefendantName;
        protected Label lblIDOfTheClaim;
        protected Label lblInfoSource;
        protected Label lblMsg;
        protected Label lblPengindCase;
        protected Label lblPlaintiffID;
        protected Label lblPlaintiffName;
        protected Label lblStatus;
        protected Label lblValueOfSuit;
        private bool nativeCult;
        private ResourceManager rm;
        protected TextBox txtCaseNumber;
        protected TextBox txtComment;
        protected TextBox txtDefendantID;
        protected TextBox txtDefendantName;
        protected TextBox txtPlaintiffID;
        protected TextBox txtPlaintiffName;
        protected TextBox txtRegDate;
        protected TextBox txtValueOfSuit;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            Page.ID = "252";

            claimFactory = new NPaymentsFactory();

            btnCloseCase.Visible = false;

            if (!Page.IsPostBack) {
                InitDDBoxes();
                LocalizeText();

                if (Request["Update"] != null && Request["Update"].Trim().ToLower() == "true") {
                    if (Request["pcID"] != null && Request["pcID"].Trim() != "") {
                        LoadPendingCase(Request["pcID"]);
                        return;
                    }
                }
                InitNew();
            }
        }

        private void InitNew() {
            lblCaseID.Text = "";
            lblStatus.Text = "";
            lblIDOfTheClaim.Text = "";

            lblCaseID.Visible = false;
            lblStatus.Visible = false;

            txtDefendantID.Text = "";
            txtDefendantName.Text = "";
            txtPlaintiffID.Text = "";
            txtPlaintiffName.Text = "";

            txtRegDate.Text = DateTime.Now.ToShortDateString();
            txtCaseNumber.Text = "";
            txtValueOfSuit.Text = "";

            ddInfoSource.SelectedIndex = 0;
            ddAdjudicator.SelectedIndex = 0;
            txtComment.Text = "";

            lblPengindCase.Text = rm.GetString("txtNew", ci) + " " + rm.GetString("txtPendingCase", ci);

            btnCloseCase.Visible = false;
        }

        private void LocalizeText() {
            lblDefendantID.Text = rm.GetString("txtDefendantID", ci);
            lblDefendantName.Text = rm.GetString("txtDefendantName", ci);
            lblPlaintiffID.Text = rm.GetString("txtPlaintiffID", ci);
            lblPlaintiffName.Text = rm.GetString("txtPlaintiffName", ci);
            lblCaseNumber.Text = rm.GetString("txtCaseNumber", ci);
            lblInfoSource.Text = rm.GetString("txtInformationSource", ci);
            lblDateRegistered.Text = rm.GetString("txtDateRegistered", ci);
            lblAdjudicator.Text = rm.GetString("txtAdjudicator", ci);
            //lblPengindCase.Text = rm.GetString("txtPendingCase", ci);
            lblValueOfSuit.Text = rm.GetString("txtValueOfSuit", ci);
            lbComment.Text = rm.GetString("lbComment", ci);
            btnSubmit.Text = rm.GetString("btSubmit", ci);
            btnNew.Text = rm.GetString("txtNew", ci);
        }

        public void InitDDBoxes() {
            ddCurrency.DataSource = claimFactory.GetCurrencyListAsDataSet();
            ddCurrency.DataTextField = "CurrencyCode";
            ddCurrency.DataBind();

            ddInfoSource.DataSource = claimFactory.GetInformationSourceListAsDataSet();
            ddInfoSource.DataTextField = nativeCult ? "NameNative" : "NameEN";

            ddInfoSource.DataValueField = "InformationSourceID";
            ddInfoSource.DataBind();

            DataTable adjTable = claimFactory.GetAdjudicatorsAsDataTable();
            adjTable.DefaultView.Sort = nativeCult ? "AdjudicatorNameNative" : "AdjudicatorNameEN";

            ddAdjudicator.DataSource = adjTable;
            ddAdjudicator.DataValueField = "ID";
            ddAdjudicator.DataTextField = nativeCult ? "AdjudicatorNameNative" : "AdjudicatorNameEN";
            ddAdjudicator.DataBind();
        }

        protected void LoadPendingCase(string id) {
            var pCase = new PendingCaseBLLC();
            if (!pCase.Load(id)) {
                return;
            }
            //txtPCID.Text = id;
            txtDefendantID.Text = pCase.DefendantID;
            txtDefendantName.Text = pCase.DefendantName;
            txtPlaintiffID.Text = pCase.PlaintiffID;
            txtPlaintiffName.Text = pCase.PlaintiffName;

            txtRegDate.Text = pCase.DateOfWrit.ToShortDateString();
            txtCaseNumber.Text = pCase.WritNumber;
            txtValueOfSuit.Text = pCase.ValueOfSuit.ToString();

            ddInfoSource.SelectedValue = pCase.Court.ToString();
            ddAdjudicator.SelectedValue = pCase.Adjudicator.ToString();
            if (!string.IsNullOrEmpty(pCase.CurrencyCode)) {
                ddCurrency.SelectedValue = pCase.CurrencyCode;
            }
            txtComment.Text = pCase.Comments;

            lblCaseID.Text = rm.GetString("txtID", ci) + ": " + id;
            lblIDOfTheClaim.Text = id;
            if (nativeCult) {
                lblStatus.Text = rm.GetString("lbStatus", ci) + ": " + pCase.StateTextNative;
            } else {
                lblStatus.Text = pCase.StateTextEN;
            }

            lblCaseID.Visible = true;
            lblStatus.Visible = true;

            if (pCase.IsRegistered) {
                btnCloseCase.Visible = true;
            }
        }

        protected void DisplayMsg(string msg) {
            lblMsg.Text = msg;
            lblMsg.Visible = true;
            lblMsg.ForeColor = Color.Blue;
        }

        protected void DisplayError(string msg) {
            lblMsg.Text = msg;
            lblMsg.Visible = true;
            lblMsg.ForeColor = Color.Red;
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            PendingCaseBLLC pCase = GetCaseFromPage();

            if (pCase.Save()) {
                DisplayMsg("Claim inserted/updated");
            } else {
                DisplayMsg("Error inserting/updating claim");
            }
        }

        private PendingCaseBLLC GetCaseFromPage() {
            var pCase = new PendingCaseBLLC();
            if (lblIDOfTheClaim.Text.Trim() != "") {
                pCase.ID = int.Parse(lblIDOfTheClaim.Text.Trim());
            }
            pCase.DefendantID = txtDefendantID.Text;
            pCase.DefendantName = txtDefendantName.Text;
            pCase.PlaintiffID = txtPlaintiffID.Text;
            pCase.PlaintiffName = txtPlaintiffName.Text;
            pCase.Court = int.Parse(ddInfoSource.SelectedValue);
            pCase.Adjudicator = int.Parse(ddAdjudicator.SelectedValue);
            pCase.DateOfWrit = DateTime.Parse(txtRegDate.Text);
            pCase.WritNumber = txtCaseNumber.Text;
            float suitValue = 0;
            if (!float.TryParse(txtValueOfSuit.Text, out suitValue))
            { 
                suitValue = 0;
            }
            pCase.ValueOfSuit = suitValue;
            pCase.Comments = txtComment.Text;
            pCase.CurrencyCode = ddCurrency.SelectedValue;
            return pCase;
        }

        private void btnCloseCase_Click(object sender, EventArgs e) {
            if (lblCaseID.Text.Trim() != "") {
                PendingCaseBLLC pCase = GetCaseFromPage();
                if (pCase != null && pCase.ID > 0) {
                    pCase.CloseCase();
                    Server.Transfer("FindPendingCase.aspx");
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e) { InitNew(); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCloseCase.Click += new EventHandler(this.btnCloseCase_Click);
            this.btnNew.Click += new EventHandler(this.btnNew_Click);
            this.btnSubmit.Click += new EventHandler(this.btnSubmit_Click);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}