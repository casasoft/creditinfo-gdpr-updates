using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for ShowAllCases.
    /// </summary>
    public class ShowAllCases : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly NPaymentsFactory myFact = new NPaymentsFactory();
        private bool company;
        protected DataGrid dgClaim;
        protected HtmlGenericControl divAllCases;
        private bool EN;
        protected Label lbAllCases;
        // localization use
        protected Label lblCreditInfoID;
        protected Label lblCreditInfoIDValue;
        protected Label lblDatagridIcons;
        protected Label lblDgResults;
        protected Label lblFirstName;
        protected Label lblFirstNameValue;
        protected Label lblSurName;
        protected Label lblSurNameValue;
        protected HtmlTable SearchGrid;
        protected HtmlTableCell Td2;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "216";

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            if (!Page.IsPostBack) {
                String creditInfoID = Session["CreditInfoID"].ToString();
                if (!(string.IsNullOrEmpty(creditInfoID))) {
                    Session.Remove("CreditInfoID");
                    lblCreditInfoIDValue.Text = creditInfoID;
                    //First we need to check if ciid is a company or individual
                    string type = myFact.GetCIUserType(int.Parse(creditInfoID));
                    if (type.Trim().Equals("-1")) // no type found
                    {
                        type = CigConfig.Configure("lookupsettings.individualID"); // then set to individual as default
                    }
                    if (type.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                        company = false;
                        Indivitual indi = myFact.GetIndivitual(int.Parse(creditInfoID));
                        if (EN) {
                            lblFirstNameValue.Text = indi.FirstNameEN;
                            lblSurNameValue.Text = indi.SurNameEN;
                        } else {
                            lblFirstNameValue.Text = indi.FirstNameNative;
                            lblSurNameValue.Text = indi.SurNameNative;
                        }
                    } else {
                        company = true;
                        Company comp = myFact.GetCompany(int.Parse(creditInfoID));
                        lblFirstNameValue.Text = comp.NameNative;
                        lblSurNameValue.Text = comp.NameEN;
                    }

                    DataSet claimSet = myFact.GetAllClaimsByCIIDAsDataset(creditInfoID);
                    dgClaim.DataSource = claimSet.Tables[0].DefaultView;
                    SetColumnsHeaderByCulture();
                    dgClaim.DataBind();
                }
            }

            LocalizeText();
            SetColumnsByCulture();
        }

        private void LocalizeText() {
            lbAllCases.Text = rm.GetString("txtAllCasesPerCustomer", ci);
            lblCreditInfoID.Text = rm.GetString("lbDebtorID", ci);
            if (company) {
                lblFirstName.Text = rm.GetString("txtCompany", ci);
                lblSurName.Text = rm.GetString("txtCompany(EN)", ci);
            } else {
                lblFirstName.Text = rm.GetString("txtFirstname", ci);
                lblSurName.Text = rm.GetString("txtSurname", ci);
            }
        }

        private void SetColumnsByCulture() {
            if (EN) {
                dgClaim.Columns[0].Visible = true;
                dgClaim.Columns[1].Visible = true;
                dgClaim.Columns[2].Visible = true;
                dgClaim.Columns[3].Visible = true;
                dgClaim.Columns[4].Visible = true;
                dgClaim.Columns[5].Visible = true;
                dgClaim.Columns[6].Visible = false;
                dgClaim.Columns[7].Visible = false;
                dgClaim.Columns[8].Visible = true;
                dgClaim.Columns[9].Visible = false;
                dgClaim.Columns[10].Visible = true;
                dgClaim.Columns[11].Visible = false;
                dgClaim.Columns[12].Visible = true;
                dgClaim.Columns[13].Visible = true;
                dgClaim.Columns[14].Visible = true;
            } else {
                dgClaim.Columns[0].Visible = true;
                dgClaim.Columns[1].Visible = true;
                dgClaim.Columns[2].Visible = true;
                dgClaim.Columns[3].Visible = true;
                dgClaim.Columns[4].Visible = true;
                dgClaim.Columns[5].Visible = true;
                dgClaim.Columns[6].Visible = false;
                dgClaim.Columns[7].Visible = true;
                dgClaim.Columns[8].Visible = false;
                dgClaim.Columns[9].Visible = true;
                dgClaim.Columns[10].Visible = false;
                dgClaim.Columns[11].Visible = true;
                dgClaim.Columns[12].Visible = false;
                dgClaim.Columns[13].Visible = true;
                dgClaim.Columns[14].Visible = true;
            }
        }

        private void SetColumnsHeaderByCulture() {
            dgClaim.Columns[1].HeaderText = rm.GetString("lbDebtorID", ci);
            dgClaim.Columns[2].HeaderText = rm.GetString("lbClaim", ci);
            dgClaim.Columns[3].HeaderText = rm.GetString("txtRegistration", ci);
            dgClaim.Columns[4].HeaderText = rm.GetString("txtEmployee", ci);
            dgClaim.Columns[5].HeaderText = rm.GetString("lbClaimOwnerName", ci);
            dgClaim.Columns[6].HeaderText = rm.GetString("lbClaimOwnerNameEN", ci);
            dgClaim.Columns[7].HeaderText = rm.GetString("lbComment", ci);
            dgClaim.Columns[8].HeaderText = rm.GetString("lbComment", ci);
            dgClaim.Columns[9].HeaderText = rm.GetString("lbInfoOrigin", ci);
            dgClaim.Columns[10].HeaderText = rm.GetString("lbInfoOrigin", ci);
            dgClaim.Columns[11].HeaderText = rm.GetString("lbStatus", ci);
            dgClaim.Columns[12].HeaderText = rm.GetString("lbStatus", ci);
            dgClaim.Columns[13].HeaderText = rm.GetString("lbClaimDate", ci); // vantar h�r ���ingu
            dgClaim.Columns[14].HeaderText = rm.GetString("lbCaseID", ci);
        }

        private void dgClaim_ItemCommand(object source, DataGridCommandEventArgs e) {
            try {
                if (e.CommandName == "Select") {
                    int claimID = int.Parse(e.Item.Cells[2].Text);
                    int creditInfoID = int.Parse(e.Item.Cells[1].Text);
                    int caseID = int.Parse(e.Item.Cells[14].Text);

                    Session["ClaimID"] = claimID;
                    Session["CreditInfoID"] = creditInfoID;
                    // Transfer to current page 
                    Server.Transfer(
                        CigConfig.Configure("lookupsettings.redirectpageID" + caseID) + "?update=true&pageid=11");
                }
            } catch (ThreadAbortException) {
                // when using Server.Trandsfer this is an "normal" exception
            } catch (Exception err) // catch int.Parse error
            {
                Logger.WriteToLog("ShowAllCases.aspx, dgClaim_ItemCommand Error ", err, true);
            }
        }

        private void dgClaim_ItemDataBound(object sender, DataGridItemEventArgs e) {
            try {
                //Get the claim owner
                int claimOwnerCIID = int.Parse(e.Item.Cells[6].Text);
                e.Item.Cells[5].Text = GetDebtorName(claimOwnerCIID);
            } catch {
                // just using this for check if it is a valid integer, no need to do some special handling here...
            }
        }

        /// <summary>
        /// Get debtors name for each case in caseOverview. Quick and dirty solution though...
        /// </summary>
        private String GetDebtorName(int creditInfoID) {
            String type = myFact.GetCIUserType(creditInfoID);
            String debtorName = "";
            if (type.Trim().Equals("-1")) // no type found
            {
                type = CigConfig.Configure("lookupsettings.individualID"); // then set to individual as default
            }
            if (type.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                Indivitual myIndi = myFact.GetIndivitual(creditInfoID);
                if (EN) {
                    if (myIndi.FirstNameEN == null || myIndi.FirstNameEN.Trim() == "") {
                        debtorName = myIndi.FirstNameNative;
                        debtorName += " " + myIndi.SurNameNative;
                    } else {
                        debtorName = myIndi.FirstNameEN;
                        debtorName += " " + myIndi.SurNameEN;
                    }
                } else {
                    if (myIndi.FirstNameNative == null || myIndi.FirstNameNative.Trim() == "") {
                        debtorName = myIndi.FirstNameEN;
                        debtorName += " " + myIndi.SurNameEN;
                    } else {
                        debtorName = myIndi.FirstNameNative;
                        debtorName += " " + myIndi.SurNameNative;
                    }
                }
            } else if (type.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                // company
                Company myComp = myFact.GetCompany(creditInfoID);
                if (EN) {
                    if (myComp.NameEN == null || myComp.NameEN.Trim() == "") {
                        debtorName = myComp.NameNative;
                    } else {
                        debtorName = myComp.NameEN;
                    }
                } else {
                    if (myComp.NameNative == null || myComp.NameNative.Trim() == "") {
                        debtorName = myComp.NameEN;
                    } else {
                        debtorName = myComp.NameNative;
                    }
                }
            }
            return debtorName;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dgClaim.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgClaim_ItemCommand);
            this.dgClaim.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgClaim_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}