<%@ Page language="c#" Codebehind="CryptPage.aspx.cs" AutoEventWireup="false" Inherits="NPayments.CryptPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CryptPage</title>
	</HEAD>
	<BODY>
		&lt;
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<form id="Form1" method="post" runat="server">
			<asp:TextBox id="txtDataToEncrypt" style="Z-INDEX: 101; LEFT: 184px; POSITION: absolute; TOP: 175px"
				runat="server" Width="236px"></asp:TextBox>
			<asp:Label id="lbEncryptedData" style="Z-INDEX: 108; LEFT: 194px; POSITION: absolute; TOP: 208px"
				runat="server">Encryped Data</asp:Label>
			<asp:Label id="lbDecryptedData" style="Z-INDEX: 107; LEFT: 193px; POSITION: absolute; TOP: 261px"
				runat="server">Decrypted Data</asp:Label>
			<asp:TextBox id="txtEncryptedData" style="Z-INDEX: 102; LEFT: 185px; POSITION: absolute; TOP: 233px"
				runat="server" Width="232px"></asp:TextBox>
			<asp:TextBox id="txtDecryptedData" style="Z-INDEX: 103; LEFT: 184px; POSITION: absolute; TOP: 290px"
				runat="server" Width="235px"></asp:TextBox>
			<asp:Button id="btnEncrypt" style="Z-INDEX: 104; LEFT: 232px; POSITION: absolute; TOP: 331px"
				runat="server" Text="Encrypt"></asp:Button>
			<asp:Button id="btnDecrypt" style="Z-INDEX: 105; LEFT: 300px; POSITION: absolute; TOP: 331px"
				runat="server" Text="Decrypt"></asp:Button>
			<asp:Label id="lbDataToEncrypt" style="Z-INDEX: 106; LEFT: 190px; POSITION: absolute; TOP: 151px"
				runat="server">Data to encrypt</asp:Label>
			<asp:Label id="lblError" style="Z-INDEX: 109; LEFT: 375px; POSITION: absolute; TOP: 334px"
				runat="server">Label</asp:Label>
			<asp:Button id="btnDecryptConfig" style="Z-INDEX: 110; LEFT: 190px; POSITION: absolute; TOP: 375px"
				runat="server" Text="Decrypt string from config file"></asp:Button>
		</form>
	</BODY>
</HTML>
