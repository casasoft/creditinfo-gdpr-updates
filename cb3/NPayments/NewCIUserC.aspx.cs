using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for NewCIUserC.
    /// </summary>
    public class NewCIUserC : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly NPaymentsFactory myFact = new NPaymentsFactory();
        protected Button btCancel;
        protected Button btnCity2Search;
        protected Button btnCitySearch;
        protected Button btRegCompany;
        protected CustomValidator cvCompanyEstablished;
        protected CustomValidator cvLastContacted;
        protected DropDownList ddCity1;
        protected DropDownList ddCity2;
        protected DropDownList ddCompanyNace;
        protected DropDownList ddIDNumberType1;
        protected DropDownList ddIDNumberType2;
        protected DropDownList ddOrgStatus;
        private bool EN;
        protected Label lbAddr1InfoEN;
        protected Label lbAddress1;
        protected Label lbAddress1EN;
        protected Label lbAddress1Info;
        protected Label lbAddress2;
        protected Label lbAddress2EN;
        protected Label lbAddress2Info;
        protected Label lbAddress2InfoEN;
        protected Label lbCity1;
        protected Label lbCity2;
        protected Label lbCompanyInformation;
        protected Label lbCompanyName;
        protected Label lbCompanyNameEN;
        protected Label lbCompFunction;
        protected Label lbCompURL;
        protected Label lbDebtorCIID;
        protected Label lbEmail;
        protected Label lbEstablished;
        protected Label lbFaxNumber;
        protected Label lbIDNumber1;
        protected Label lbIDNumber2;
        protected Label lblAdditionalInfo;
        protected Label lblAddressInfo;
        protected Label lblAddressInfo2;
        protected Label lbLastContacted;
        protected Label lblCity2Search;
        protected Label lblCitySearch;
        protected Label lblMsg;
        protected Label lblRegistered;
        protected Label lbNewCompany;
        protected Label lbNote;
        protected Label lbOrg_status;
        protected Label lbPhoneNumber;
        protected Label lbPostalCode;
        protected Label lbPostalCode2;
        protected Label lbPostBox;
        protected Label lbPostBox2;
        protected Label lbType1;
        protected Label lbType2;
        private bool newRegistration = true;
        protected HtmlForm newuser;
        private string redirectPage = "";
        protected RequiredFieldValidator Requiredfieldvalidator1;
        protected RequiredFieldValidator rfvAddress;
        protected RequiredFieldValidator rfvCity;
        protected RequiredFieldValidator rfvCity2;
        protected RequiredFieldValidator rfvName;
        protected TextBox tbAddress1EN;
        protected TextBox tbAddress1InfoEN;
        protected TextBox tbAddress1InfoN;
        protected TextBox tbAddress1Native;
        protected TextBox tbAddress2EN;
        protected TextBox tbAddress2InfoEN;
        protected TextBox tbAddress2InfoN;
        protected TextBox tbAddress2Native;
        protected TextBox tbCompanyCIID;
        protected TextBox tbCompanyEstablished;
        protected TextBox tbCompanyLContacted;
        protected TextBox tbCompanyNameEN;
        protected TextBox tbCompanyNameNative;
        protected TextBox tbCompanyURL;
        protected TextBox tbEmail;
        protected TextBox tbIDNumber1;
        protected TextBox tbIDNumber2;
        protected TextBox tbNote;
        protected TextBox tbPageID;
        protected TextBox tbPhoneNumber1;
        protected TextBox tbPhoneNumber2;
        protected TextBox tbPostalCode1;
        protected TextBox tbPostalCode2;
        protected TextBox tbPostBox1;
        protected TextBox tbPostBox2;
        protected TextBox tbRegistered;
        protected TextBox txtCity2Search;
        protected TextBox txtCitySearch;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            lblMsg.Visible = false;
            Page.ID = "213";
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            AddEnterEvent();
            LocalizeText();

            if (!IsPostBack) {
                InitDDBoxes();
                if (Session["Update"] != null) {
                    newRegistration = false;
                    initControls(true);
                } else {
                    initControls(false);
                }
            }
            if (Request.Params["pageid"] != null) {
                SetRedirectPage(int.Parse(Request.Params["pageid"]));
                tbPageID.Text = Request.Params["pageid"];
            }
            if (tbPageID.Text != "") {
                SetRedirectPage(int.Parse(tbPageID.Text));
            }
        }

        private void initControls(bool update) {
            if (update) {
                if (Session["CreditInfoID"] != null) {
                    var creditInfoID = (int) Session["CreditInfoID"];
                    var myComp = myFact.GetCompany(creditInfoID);
                    tbCompanyCIID.Text = creditInfoID.ToString();
                    tbCompanyNameNative.Text = myComp.NameNative;
                    tbCompanyNameEN.Text = myComp.NameNative;
                    tbEmail.Text = myComp.Email;
                    tbCompanyURL.Text = myComp.URL;
                    tbCompanyEstablished.Text = myComp.Established.ToShortDateString();
                    if (myComp.LastContacted > DateTime.MinValue) {
                        tbCompanyLContacted.Text = myComp.LastContacted.ToShortDateString();
                    }
                    if ((myComp.Registered > DateTime.MinValue) && (myComp.Registered < DateTime.MaxValue)) {
                        tbRegistered.Text = myComp.Registered.ToShortDateString();
                    }
                    if (myComp.Org_status_code != -1) {
                        try {
                            ddOrgStatus.SelectedValue = myComp.Org_status_code.ToString();
                        } catch {}
                    }

                    if (myComp.FuncID != "") {
                        ddCompanyNace.SelectedValue = myComp.FuncID;
                    }

                    initMultiItems(myComp);

                    btRegCompany.Text = "Update";
                }
            } else if (Session["AddCompany"] != null) {
                if (bool.Parse(Session["AddCompany"].ToString())) {
                    if (Session["NationalID"] != null) {
                        tbIDNumber1.Text = Session["NationalID"].ToString();
                    }
                    if (Session["NameNative"] != null) {
                        tbCompanyNameNative.Text = Session["NameNative"].ToString();
                    }
                    if (Session["NameEN"] != null) {
                        tbCompanyNameEN.Text = Session["NameEN"].ToString();
                    }
                    if (Session["StreetNative"] != null) {
                        tbAddress1Native.Text = Session["StreetNative"].ToString();
                    }
                    if (Session["StreetEN"] != null) {
                        tbAddress1EN.Text = Session["StreetEN"].ToString();
                    }
                    //Clear those session variables
                    Session.Remove("NationalID");
                    Session.Remove("NameNative");
                    Session.Remove("NameEN");
                    Session.Remove("StreetNative");
                    Session.Remove("StreetEN");
                    Session.Remove("AddCompany");
                }
            } else if (Session["AddClaimOwner"] != null) {
                if (bool.Parse(Session["AddClaimOwner"].ToString())) {
                    if (Session["NationalID"] != null) {
                        tbIDNumber1.Text = Session["NationalID"].ToString();
                    }
                    if (Session["NameNative"] != null) {
                        tbCompanyNameNative.Text = Session["NameNative"].ToString();
                    }
                    if (Session["NameEN"] != null) {
                        tbCompanyNameEN.Text = Session["NameEN"].ToString();
                    }
                    if (Session["StreetNative"] != null) {
                        tbAddress1Native.Text = Session["StreetNative"].ToString();
                    }
                    if (Session["StreetEN"] != null) {
                        tbAddress1EN.Text = Session["StreetEN"].ToString();
                    }
                    //Clear those session variables
                    Session.Remove("NationalID");
                    Session.Remove("NameNative");
                    Session.Remove("NameEN");
                    Session.Remove("StreetNative");
                    Session.Remove("StreetEN");
                    Session.Remove("AddClaimOwner");
                    Session["ClaimOwnerInfo"] = "true";
                }
            }
        }

        private void initMultiItems(CreditInfoUser myComp) {
            var arrAdd = myComp.Address;
            var arrIDNum = myComp.IDNumbers;
            var arrPNum = myComp.Number;
            var frm = FindControl("newuser");
            for (var i = 1; i <= arrAdd.Count; i++) {
                var myAddr = (Address) arrAdd[i - 1]; // �etta krassar (cast exception...)

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbAddress" + i + "Native") {
                            ((TextBox) ctrl).Text = myAddr.StreetNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "EN") {
                            ((TextBox) ctrl).Text = myAddr.StreetEN;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "InfoN") {
                            ((TextBox) ctrl).Text = myAddr.OtherInfoNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "InfoEN") {
                            ((TextBox) ctrl).Text = myAddr.OtherInfoEN;
                        }
                        if ((ctrl).ID == "tbPostalCode" + i) {
                            ((TextBox) ctrl).Text = myAddr.PostalCode;
                        }
                        if ((ctrl).ID == "tbPostBox" + i) {
                            ((TextBox) ctrl).Text = myAddr.PostBox;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID != "ddCity" + i) {
                        continue;
                    }
                    int cityID = myAddr.CityID;
                    if (cityID <= -1) {
                        continue;
                    }
                    string cityName = myFact.GetCityName(cityID, EN);
                    if (string.IsNullOrEmpty(cityName)) {
                        continue;
                    }
                    ((DropDownList) ctrl).Items.Add(new ListItem(cityName, cityID.ToString()));
                    ((DropDownList) ctrl).SelectedValue = cityID.ToString();
                    //((DropDownList)ctrl).SelectedValue = myAddr.CityID.ToString();
                }
            }
            for (int i = 1; i <= arrIDNum.Count; i++) {
                var myIDNum = (IDNumber) arrIDNum[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbIDNumber" + i) {
                            ((TextBox) ctrl).Text = myIDNum.Number;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID == "ddIDNumberType" + i) {
                        ((DropDownList) ctrl).SelectedValue = myIDNum.NumberTypeID.ToString();
                    }
                }
            }
            for (int i = 1; i <= arrPNum.Count; i++) {
                var myPNum = (PhoneNumber) arrPNum[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbPhoneNumber" + i) {
                            ((TextBox) ctrl).Text = myPNum.Number;
                        }
                    }
                    if (ctrl is DropDownList) {
                        if ((ctrl).ID == "ddPhoneNumber" + i + "Type") {
                            ((DropDownList) ctrl).SelectedValue = myPNum.NumberTypeID.ToString();
                        }
                    }
                }
            }
        }

        private void InitDDBoxes() {
            BindIDNumberTypes();
            BindCompanyFunction();
            //BindCityList();
            BindOrgStatus();
        }

        private void BindOrgStatus() {
            var orgStatusSet = myFact.GetOrgStatus();
            ddOrgStatus.DataSource = orgStatusSet;
            ddOrgStatus.DataTextField = EN ? "nativeEN" : "nameNative";
            ddOrgStatus.DataValueField = "id";
            ddOrgStatus.DataBind();
        }

        private void BindIDNumberTypes() {
            var idNumberType = myFact.GetIDNumberTypesAsDataSet();

            ddIDNumberType1.DataSource = idNumberType;
            ddIDNumberType1.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddIDNumberType1.DataValueField = "NumberTypeID";
            ddIDNumberType1.DataBind();

            ddIDNumberType2.DataSource = idNumberType;
            ddIDNumberType2.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddIDNumberType2.DataValueField = "NumberTypeID";
            ddIDNumberType2.DataBind();
        }

        private void BindCompanyFunction() {
            var companyFunctionSet = myFact.GetCompanyNaceCodes();
            ddCompanyNace.DataSource = companyFunctionSet;
            ddCompanyNace.DataTextField = EN ? "DescriptionShortEN" : "DescriptionShortNative";
            ddCompanyNace.DataValueField = "FuncID";
            ddCompanyNace.DataBind();
            try {
                ddCompanyNace.SelectedValue = "000000";
            } catch {
                //
            }
            // is this right!?
            Session["CompanyFunctionSet"] = companyFunctionSet;
        }

        private void BindCityList(DataSet citySet) {
            ddCity1.DataSource = citySet;
            ddCity1.DataTextField = EN ? "NameEN" : "NameNative";
            ddCity1.DataValueField = "CityID";
            ddCity1.DataBind();
            //this.ddCity1.SelectedValue = "1";			
        }

        private void BindCity2List(DataSet citySet) {
            ddCity2.DataSource = citySet;
            ddCity2.DataTextField = EN ? "NameEN" : "NameNative";
            ddCity2.DataValueField = "CityID";
            ddCity2.DataBind();
        }

        // get proper NaceCode by selected item from the combobox
        private string GetSelectedNaceCode(int index) {
            if (Session["CompanyFunctionSet"] != null) {
                var companyFunctionSet = (DataSet) Session["CompanyFunctionSet"];
                var temp = companyFunctionSet.Tables[0].Rows[index]["FuncID"].ToString();
                return temp;
            }
            return "ERROR";
        }

        public ArrayList GetIDNumbers() {
            IDNumber myIDNum;
            var aNumbers = new ArrayList();
            if (tbIDNumber1.Text != "") {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber1.Text,
                              NumberTypeID = int.Parse(ddIDNumberType1.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            if (tbIDNumber2.Text != "") {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber2.Text,
                              NumberTypeID = int.Parse(ddIDNumberType2.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            return aNumbers;
        }

        public ArrayList GetPhoneNumbers() {
            var myNum = new PhoneNumber();
            var aNumbers = new ArrayList();
            if (tbPhoneNumber1.Text != "") {
                myNum.Number = tbPhoneNumber1.Text;
                myNum.NumberTypeID = 2;
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber2.Text != "") {
                myNum = new PhoneNumber {Number = tbPhoneNumber2.Text, NumberTypeID = 4};
                aNumbers.Add(myNum);
            }
            return aNumbers;
        }

        public ArrayList GetAddresses() {
            var myAddr = new Address();
            var address = new ArrayList();
            if (tbAddress1Native.Text != "" || tbAddress1EN.Text != "" || tbAddress1InfoN.Text != "" ||
                tbAddress1InfoEN.Text != "") {
                if (ddCity1.SelectedItem.Value.Trim() != "") {
                    myAddr.CityID = int.Parse(ddCity1.SelectedItem.Value); //.SelectedIndex + 1;
                }
                myAddr.OtherInfoNative = tbAddress1InfoN.Text;
                myAddr.OtherInfoEN = tbAddress1InfoEN.Text;
                myAddr.PostalCode = tbPostalCode1.Text;
                myAddr.PostBox = tbPostBox1.Text;
                myAddr.StreetNative = tbAddress1Native.Text;
                myAddr.StreetEN = tbAddress1EN.Text;
                address.Add(myAddr);
            }
            if (tbAddress2Native.Text != "" || tbAddress2EN.Text != "" || tbAddress2InfoN.Text != "" ||
                tbAddress2InfoEN.Text != "") {
                myAddr = new Address();
                if (ddCity2.SelectedItem.Value.Trim() != "") {
                    myAddr.CityID = int.Parse(ddCity2.SelectedItem.Value); //.SelectedIndex + 1;
                }
                myAddr.OtherInfoNative = tbAddress2InfoN.Text;
                myAddr.OtherInfoEN = tbAddress2InfoEN.Text;
                myAddr.PostalCode = tbPostalCode2.Text;
                myAddr.PostBox = tbPostBox2.Text;
                myAddr.StreetNative = tbAddress2Native.Text;
                myAddr.StreetEN = tbAddress2EN.Text;
                address.Add(myAddr);
            }
            return address;
        }

        public void SetRedirectPage(int index) { redirectPage = CigConfig.Configure("lookupsettings.redirectpageID" + index); }

        private void LocalizeText() {
            lbDebtorCIID.Text = rm.GetString("lbDebtorID", ci);
            lbCompanyName.Text = rm.GetString("lbCompanyName", ci);
            lbCompanyNameEN.Text = rm.GetString("lbCompanyNameEN", ci);
            lbEstablished.Text = rm.GetString("lbEstablished", ci);
            lblRegistered.Text = rm.GetString("txtRegistered", ci);
            lbPostalCode.Text = rm.GetString("lbPostalCode", ci);
            btCancel.Text = rm.GetString("btnCancel", ci);
            lbEmail.Text = rm.GetString("txtEmail", ci);
            lbLastContacted.Text = rm.GetString("lbLastContacted", ci);
            lbCompURL.Text = rm.GetString("lbCompURL", ci);
            lbCompFunction.Text = rm.GetString("lbCompFunction", ci);
            lbCompanyInformation.Text = rm.GetString("lbCompanyInformation", ci);
            lbPostalCode2.Text = rm.GetString("lbPostalCode2", ci);
            lbAddress2InfoEN.Text = rm.GetString("lbAddress2InfoEN", ci);
            lbPhoneNumber.Text = rm.GetString("lbPhoneNumber", ci);
            lbFaxNumber.Text = rm.GetString("lbFaxNumber", ci);
            btRegCompany.Text = rm.GetString("btSubmit", ci);
            lbAddress1.Text = rm.GetString("lbAddress1", ci);
            lbAddress1EN.Text = rm.GetString("lbAddress1EN", ci);
            lbCity1.Text = rm.GetString("lbCity1", ci);
            lbPostBox.Text = rm.GetString("lbPostBox", ci);
            lbAddress1Info.Text = rm.GetString("lbAddress1Info", ci);
            lbAddr1InfoEN.Text = rm.GetString("lbAddress1InfoEN", ci);
            lbAddress2.Text = rm.GetString("lbAddress2", ci);
            lbAddress2EN.Text = rm.GetString("lbAddress2EN", ci);
            lbCity2.Text = rm.GetString("lbCity2", ci);
            lbPostBox2.Text = rm.GetString("lbPostBox2", ci);
            lbAddress2Info.Text = rm.GetString("lbAddress2Info", ci);
            lbIDNumber1.Text = rm.GetString("lbIDNumber1", ci);
            lbIDNumber2.Text = rm.GetString("lbIDNumber2", ci);
            lbType1.Text = rm.GetString("lbType", ci);
            lbType2.Text = rm.GetString("lbType", ci);
            lbNewCompany.Text = rm.GetString("txtNewCompany", ci);
            rfvName.ErrorMessage = rm.GetString("RequiredFieldValidator1", ci);
            rfvAddress.ErrorMessage = rm.GetString("RequiredFieldValidator1", ci);
            lbOrg_status.Text = rm.GetString("txtOrgStatusCode", ci);
            cvLastContacted.ErrorMessage = rm.GetString("CustomValidator2", ci);
            cvCompanyEstablished.ErrorMessage = rm.GetString("CustomValidator2", ci);
            Requiredfieldvalidator1.ErrorMessage = rm.GetString("RequiredFieldValidator1", ci);
            rfvCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvCity2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblCitySearch.Text = rm.GetString("txtSearch", ci);
            lblCity2Search.Text = rm.GetString("txtSearch", ci);
            lbNote.Text = rm.GetString("lblNote", ci);
        }

        private static bool IsValidDate(string dateString) {
            if (dateString.Trim() == "") {
                return false;
            }
            IFormatProvider format = CultureInfo.CurrentCulture;
            try {
                DateTime.Parse(dateString.Trim(), format, DateTimeStyles.AllowWhiteSpaces);
                return true;
            } catch (Exception) {
                return false;
            }
        }

        private void btRegCompany_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                try {
                    var myComp = new Company();
                    var myFact1 = new NPaymentsFactory();
                    myComp.Type = CigConfig.Configure("lookupsettings.companyID");
                    myComp.NameNative = tbCompanyNameNative.Text;
                    myComp.NameEN = tbCompanyNameEN.Text;
                    myComp.FuncID = GetSelectedNaceCode(ddCompanyNace.SelectedIndex);
                    myComp.Established = DateTime.Parse(tbCompanyEstablished.Text);
                    if (IsValidDate(tbRegistered.Text)) {
                        myComp.Registered = DateTime.Parse(tbRegistered.Text);
                    }
                    myComp.URL = tbCompanyURL.Text;
                    myComp.Email = tbEmail.Text;
                    myComp.Org_status_code = int.Parse(ddOrgStatus.SelectedValue);
                    myComp.Note = tbNote.Text;

                    if (myComp.FuncID.Equals("ERROR")) {
                        // Transfer to errorPage
                    }

                    // check if update
                    if (Session["Update"] != null) {
                        newRegistration = false;
                    }

                    try {
                        if (!newRegistration) {
                            myComp.CreditInfoID = int.Parse(tbCompanyCIID.Text);
                        }
                    } catch (Exception err) {
                        Logger.WriteToLog("NewCIUserC.aspx : btRegCompany if(!newRegistration) ", err, true);
                    }

                    // collections
                    try {
                        myComp.Address = GetAddresses();
                    } catch (Exception err) {
                        Logger.WriteToLog("NewCIUserC.aspx : btRegCompany, myComp.Address = GetAddresses() ", err, true);
                    }
                    try {
                        myComp.IDNumbers = GetIDNumbers();
                    } catch (Exception err) {
                        Logger.WriteToLog(
                            "NewCIUserC.aspx : btRegCompany : myComp.IDNumbers = GetIDNumbers(); ", err, true);
                    }
                    try {
                        myComp.Number = GetPhoneNumbers();
                    } catch (Exception err) {
                        Logger.WriteToLog(
                            "NewCIUserC.aspx : btRegCompany : myComp.Number = GetPhoneNumbers(); ", err, true);
                    }

                    bool regOK = true;
                    int creditInfoID = -1;
                    if (newRegistration) {
                        //Check if id numbers are already registered
                        if (myComp.IDNumbers != null) {
                            var idns = myComp.IDNumbers;
                            for (int i = 0; i < idns.Count; i++) {
                                var idn = (IDNumber) idns[i];
                                if (!myFact1.IsIDRegisted(idn.Number, idn.NumberTypeID)) {
                                    continue;
                                }
                                DisplayErrorMsg("txtIDNumberAlreadyRegistered");
                                return;
                            }
                        }
                        creditInfoID = myFact1.AddCompany(myComp);
                        if (creditInfoID < 0) {
                            //string error = "FAILED"; // Gera e-� g�fulegt h�r....
                            regOK = false;
                            DisplayErrorMsg("txtFailedInsertingCompany");
                            return;
                        }
                    } else {
                        myComp.LastUpdate = DateTime.Now;
                        creditInfoID = myFact1.UpdateCompany(myComp);
                        if (creditInfoID < 0) {
                            //string error = "FAILED"; // Gera e-� g�fulegt h�r....
                            regOK = false;
                            DisplayErrorMsg("txtFailedUpdatingCompany");
                            return;
                        }
                    }

                    // if should redirect to another page

                    if (redirectPage != "" && regOK) {
                        if (Session["ClaimOwnerInfo"] != null && Session["ClaimOwnerInfo"].ToString() == "true") {
                            Session["CreditInfoID"] = creditInfoID;
                            Session["ClaimOwnerNameEN"] = tbCompanyNameEN.Text;
                            Session["ClaimOwnerNameNative"] = tbCompanyNameNative.Text;
                        } else {
                            Session["CreditInfoID"] = creditInfoID;
                            Session["CompanyNameEN"] = tbCompanyNameEN.Text;
                            Session["CompanyNameNative"] = tbCompanyNameNative.Text;
                            Session["txtNationalID"] = tbIDNumber1.Text;
                        }

                        //	Server.Transfer(redirectPage +"?company=true");
                        Response.Redirect(redirectPage + "?company=true");
                    } else {
                        DisplayMsg("txtCompanyInsertedUpdated");
                    }
                } catch (ThreadAbortException) {
                    // when using Server.Transfer this is an "normal" exception
                } catch (Exception err) {
                    Logger.WriteToLog("NewCIUserC.aspx, btRebDebtor_Click.", err, true);
                    DisplayErrorMsg("errMsg2");
                }
            }
        }

        protected void DisplayErrorMsg(string msg) {
            lblMsg.Text = rm.GetString(msg, ci);
            lblMsg.ForeColor = Color.Red;
            lblMsg.Visible = true;
        }

        protected void DisplayMsg(string msg) {
            lblMsg.Text = rm.GetString(msg, ci);
            lblMsg.ForeColor = Color.Blue;
            lblMsg.Visible = true;
        }

        private void AddEnterEvent() {
            Control frm = FindControl("newuser");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void btnCitySearch_Click(object sender, EventArgs e) {
            if (txtCitySearch.Text.Trim() != "") {
                DataSet dsCity = myFact.GetCityListAsDataSet(txtCitySearch.Text, EN);
                BindCityList(dsCity);
            }
        }

        private void btnCity2Search_Click(object sender, EventArgs e) {
            if (txtCity2Search.Text.Trim() != "") {
                DataSet dsCity = myFact.GetCityListAsDataSet(txtCity2Search.Text, EN);
                BindCity2List(dsCity);
            }
        }

        private void btCancel_Click(object sender, EventArgs e) { }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCitySearch.Click += new System.EventHandler(this.btnCitySearch_Click);
            this.btnCity2Search.Click += new System.EventHandler(this.btnCity2Search_Click);
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            this.btRegCompany.Click += new System.EventHandler(this.btRegCompany_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}