using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NPayments.BLL;
using NPayments.BLL.Localization;
using NPayments.BLL.PendingCases;

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for GeneratePendingCaseLetter.
    /// </summary>
    public class GeneratePendingCaseLetter : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected Button btnGenLetter;
        protected Button btnUncheckAll;
        protected Button btUpdateStatus;
        protected DataGrid dgLetterPendingCases;
        private bool EN;
        protected Label lbLetterClaims;
        protected Label lblTotalNumberOfHits;
        public NPaymentsFactory myFact = new NPaymentsFactory();
        protected HtmlTable outerGridTable;

        private void Page_Load(object sender, EventArgs e) {
            //LetterGenerator myLG = new LetterGenerator();
            //String path = Server.MapPath("docs/NotificationLetterGR.txt");

            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            Page.ID = "253";

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (!Page.IsPostBack) {
                LocalizeText();
                LoadData();
            }
        }

        private void LocalizeText() {
            lblTotalNumberOfHits.Text = rm.GetString("txtTotalNumberOfHits", ci);
            lbLetterClaims.Text = rm.GetString("txtAllPengingCasesWithStatusLetter", ci);
            btnGenLetter.Text = rm.GetString("txtGenerateLetters", ci);
            btnUncheckAll.Text = rm.GetString("txtUncheckAll", ci);
            btUpdateStatus.Text = rm.GetString("txtUpdateStatus", ci);
        }

        private void LoadData() {
            DataTable dtLetters = myFact.GetAllLetterPendingCasesAsDataTable();
            dgLetterPendingCases.DataSource = dtLetters;
            dgLetterPendingCases.DataBind();

            lblTotalNumberOfHits.Text += dtLetters.Rows.Count;
        }

        private void btUpdateStatus_Click(object sender, EventArgs e) {
            CheckBox cbRegularLetter;
            for (int i = 0; i < dgLetterPendingCases.Items.Count; i++) {
                cbRegularLetter = (CheckBox) dgLetterPendingCases.Items[i].Cells[13].Controls[1];

                if (!cbRegularLetter.Checked) {
                    continue;
                }
                var id = dgLetterPendingCases.Items[i].Cells[0].Text;
                if (id == null || id.Trim() == "") {
                    continue;
                }
                var pCase = new PendingCaseBLLC {ID = int.Parse(id)};
                pCase.RegisterCase();
            }
            LoadData();
        }

        private void btnUncheckAll_Click(object sender, EventArgs e) {
            CheckBox cbRegularLetter;

            foreach (DataGridItem dgi in dgLetterPendingCases.Items) {
                cbRegularLetter = (CheckBox) dgi.Cells[13].Controls[1];
                cbRegularLetter.Checked = false;
            }
        }

        private void btnGenLetter_Click(object sender, EventArgs e) {
            CheckBox cbRegularLetter;
            var arrLetter = new ArrayList();

            foreach (DataGridItem dgi in dgLetterPendingCases.Items) {
                cbRegularLetter = (CheckBox) dgi.Cells[13].Controls[1];
                if (!cbRegularLetter.Checked) {
                    continue;
                }
                var pCase = new PendingCaseBLLC();
                if (!pCase.Load(dgi.Cells[0].Text)) {
                    continue;
                }
                if (!pCase.LoadAddressInfo()) {
                    pCase.LoadCompanyAddressInfo();
                }
                arrLetter.Add(pCase);
            }
            if (arrLetter.Count <= 0) {
                return;
            }
            Session["arrLetter"] = arrLetter;
            Session["PendingCaseLetter"] = "true";
            Server.Transfer(CigConfig.Configure("lookupsettings.displayletterpage"));
        }

        private void dgLetterPendingCases_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer) {
                return;
            }
            if (EN) {
                //Set court - EN if available, else native
                if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                    e.Item.Cells[5].Text = e.Item.Cells[7].Text;
                } else {
                    e.Item.Cells[5].Text = e.Item.Cells[6].Text;
                }

                //Set adjudicator - EN if available, else native
                if (e.Item.Cells[10].Text.Trim() != "" && e.Item.Cells[10].Text != "&nbsp;") {
                    e.Item.Cells[8].Text = e.Item.Cells[10].Text;
                } else {
                    e.Item.Cells[8].Text = e.Item.Cells[9].Text;
                }
            } else {
                //Set court - Native if available, else EN
                if (e.Item.Cells[6].Text.Trim() != "" && e.Item.Cells[6].Text != "&nbsp;") {
                    e.Item.Cells[5].Text = e.Item.Cells[6].Text;
                } else {
                    e.Item.Cells[5].Text = e.Item.Cells[7].Text;
                }

                //Set adjudicator - Native if available, else EN
                if (e.Item.Cells[9].Text.Trim() != "" && e.Item.Cells[9].Text != "&nbsp;") {
                    e.Item.Cells[8].Text = e.Item.Cells[9].Text;
                } else {
                    e.Item.Cells[8].Text = e.Item.Cells[10].Text;
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dgLetterPendingCases.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgLetterPendingCases_ItemDataBound);
            this.btnUncheckAll.Click += new System.EventHandler(this.btnUncheckAll_Click);
            this.btUpdateStatus.Click += new System.EventHandler(this.btUpdateStatus_Click);
            this.btnGenLetter.Click += new System.EventHandler(this.btnGenLetter_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}