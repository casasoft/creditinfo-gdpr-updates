<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="ClaimOverView.aspx.cs" AutoEventWireup="false" Inherits="NPayments.ClaimOverView" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ClaimOverView</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="claimoverview" name="claimoverview" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head2" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language2" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="Sitepositionbar2" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table cellspacing="0" cellpadding="0" align="center" width="100%">
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right">
															<div><asp:label id="lblDatagridIcons" runat="server">Select</asp:label></div>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lbAllRegClaims24h" runat="server">Registered claims for the last 24 hours</asp:label></th></tr>
													<tr>
														<td id="tdNameSearchGrid" width="100%" runat="server">
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divDataGrid" style="OVERFLOW-X: auto; OVERFLOW: auto; HEIGHT: auto"
																			runat="server">
																			<asp:datagrid id="DgClaimsOverView" runat="server" autogeneratecolumns="False" gridlines="None"
																				allowsorting="False">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="ID" headertext="ClaimID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CreditInfoID" headertext="Deb. CIID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn Visible="True" headertext="Debtor">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn Visible="True" headertext="Owner">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerCIID" headertext="Owner">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="TypeNative"  headertext="Type">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="TypeEN"  headertext="Type">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="NameNative"  headertext="Source">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="NameEN"  headertext="Source">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="RegDate"  headertext="RegDate">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="StateNative" headertext="State">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="StateEN" headertext="State">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn Visible="False" datafield="CaseID"  headertext="CaseID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StatusID" headertext="StatusID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CBA" headertext="CBA">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="DebtorUserType" headertext="DebtorUserType"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimantUserType" headertext="ClaimantUserType"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="DebtorFirstNameNative"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="DebtorSurNameNative"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="DebtorFirstNameEN"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="DebtorSurNameEN"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="DebtorCompanyNameNative"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="DebtorCompanyNameEN"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerFirstNameNative"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerSurNameNative"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerFirstNameEN"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerSurNameEN"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerCompanyNameNative"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerCompanyNameEN"></asp:boundcolumn>
																					<asp:boundcolumn datafield="CaseNr" headertext="CaseNr">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																			</asp:datagrid>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbColorDefinitions" runat="server">Color Definitions</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td bgcolor="#cfcfcf" style="WIDTH: 25%" align="center">
																		<asp:label id="lbLetterColor" runat="server">Letter</asp:label>
																	</td>
																	<td bgcolor="#cdc673" style="WIDTH: 25%" align="center">
																		<asp:label id="lbColorRegistered" runat="server">Registrated</asp:label>
																	</td>
																	<td bgcolor="#cd661d" style="WIDTH: 25%" align="center">
																		<asp:label id="lbColorCancelled" runat="server">Cancelled - De-registered</asp:label>
																	</td>
																	<td bgcolor="#9ac0cd" align="center">
																		<asp:label id="lbColorBouchedCBA" runat="server">Bounched cheque with CBA</asp:label>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
