#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Localization;
using Debtor=UserAdmin.BLL.Debtor;

#endregion

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for FindDebtor.
    /// </summary>
    public class FindDebtor : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected Button btCancel;
        protected Button btSearchDebtor;
        protected DropDownList ddIDNumberType;
        protected DataGrid dgDebtors;
        private bool EN;
        protected HtmlForm Form2;
        protected Label lbCIID;
        protected Label lbDebtorFirstName;
        protected Label lbDebtorSurName;
        protected Label lbIDNumber;
        protected Label lbIndividualSearch;
        protected Label lblDatagridIcons;
        protected Label lblDgResults;
        protected Label lblError;
        protected Label lbNumberType;
        protected Label lbSearchBy;
        public String redirectPage = "";
        protected HtmlTable SearchGrid;
        protected TextBox tbCIID;
        protected TextBox tbDebtorFirstName;
        protected TextBox tbDebtorSurName;
        protected TextBox tbIDNumber;
        protected HtmlTableCell tdNameSearchGrid;
        protected HtmlTableRow trDgResults;

        private void Page_Load(object sender, EventArgs e) {
            lblError.Visible = false;
            Page.ID = "210";
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            trDgResults.Visible = false;

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            AddEnterEvent();
            if (!IsPostBack) {
                InitDDLists();
            }

            LocalizeText();
            SetColumnsByCulture();
            if (Request.Params["pageid"] != null) {
                SetRedirectPage(int.Parse(Request.Params["pageid"]));
                //this.hlSearchWL.NavigateUrl="SearchWhitelist.aspx?pageid=" + Request.Params["pageid"];
            }
        }

        private void btSearchDebtor_Click(object sender, EventArgs e) {
            dgDebtors.CurrentPageIndex = 0;
            lblError.Visible = false;
            //this.hlSearchWL.Visible = false;
            if (!dgDebtors.Visible) {
                dgDebtors.Visible = true;
            }
            var myFact = new NPaymentsFactory();
            var myDebtor = new Debtor();
            var query = true;
            if (tbCIID.Text != "") {
                myDebtor.CreditInfo = int.Parse(tbCIID.Text);
            }
            myDebtor.IDNumber1 = tbIDNumber.Text;
            myDebtor.IDNumber2Type = int.Parse(ddIDNumberType.SelectedItem.Value);
            myDebtor.SurName = tbDebtorSurName.Text;
            myDebtor.FirstName = tbDebtorFirstName.Text;

            if ((myDebtor.IDNumber1.Equals("")) && (tbCIID.Text.Equals("")) && (myDebtor.SurName.Equals("")) &&
                (myDebtor.FirstName.Equals(""))) {
                query = false;
            }

            try {
                if (query) {
                    DataSet customerSet = myFact.FindCustomer(myDebtor);
                    if (customerSet.Tables[0].Rows.Count > 0) {
                        dgDebtors.DataSource = customerSet.Tables[0].DefaultView;
                        SetColumnsHeaderByCulture();
                        Session["dgDebtors"] = customerSet.Tables[0];
                        dgDebtors.DataBind();

                        trDgResults.Visible = true;
                    } else {
                        if (dgDebtors.Items.Count > 0) {
                            dgDebtors.Visible = false;
                        }
                        lblError.Text = rm.GetString("txtIndividualNotFound", ci);
                        lblError.Visible = true;
                        //this.hlSearchWL.Visible = true;
                    }
                } else {
                    lblError.Text = rm.GetString("errMsg3", ci);
                    lblError.Visible = true;
                }
            } catch (Exception err) {
                Logger.WriteToLog("FindCIUser.aspx " + "btSearchDebtor_Click ", err, true);
            }
        }

        private void InitDDLists() {
            var myFact = new NPaymentsFactory();
            var idNumberType = myFact.GetIDNumberTypesAsDataSet();

            ddIDNumberType.DataSource = idNumberType;
            ddIDNumberType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddIDNumberType.DataValueField = "NumberTypeID";
            try {
                ddIDNumberType.DataBind();
            } catch (Exception err) {
                Logger.WriteToLog("FindCIUser.aspx " + "InitDDLists ", err, true);
            }
        }

        // display correct datagrid comlumns by locale
        private void SetColumnsByCulture() {
            if (EN) {
                dgDebtors.Columns[0].Visible = true;
                dgDebtors.Columns[1].Visible = true;
                dgDebtors.Columns[2].Visible = true;
                dgDebtors.Columns[3].Visible = false;
                dgDebtors.Columns[4].Visible = true;
                dgDebtors.Columns[5].Visible = false;
                dgDebtors.Columns[6].Visible = true;
            } else {
                dgDebtors.Columns[0].Visible = true;
                dgDebtors.Columns[1].Visible = true;
                dgDebtors.Columns[2].Visible = true;
                dgDebtors.Columns[3].Visible = true;
                dgDebtors.Columns[4].Visible = false;
                dgDebtors.Columns[5].Visible = true;
                dgDebtors.Columns[6].Visible = false;
            }
        }

        private void LocalizeText() {
            btCancel.Text = rm.GetString("btnCancel", ci);
            lbDebtorFirstName.Text = rm.GetString("lbName", ci);
            lbDebtorSurName.Text = rm.GetString("txtSurname", ci);
            lbIDNumber.Text = rm.GetString("lbIDNumber", ci);
            btSearchDebtor.Text = rm.GetString("txtSearch", ci);
            lbCIID.Text = rm.GetString("lbDebtorID", ci);
            lbNumberType.Text = rm.GetString("lbIDNumberType", ci);
            lbSearchBy.Text = rm.GetString("lbSearchBy", ci);
            lbIndividualSearch.Text = rm.GetString("lbIndividualSearch", ci);
        }

        private void SetColumnsHeaderByCulture() {
            dgDebtors.Columns[2].HeaderText = rm.GetString("lbDebtorID", ci);
            dgDebtors.Columns[3].HeaderText = rm.GetString("lbName", ci);
            dgDebtors.Columns[5].HeaderText = rm.GetString("lbAddress", ci);
        }

        public void SetRedirectPage(int index) { redirectPage = CigConfig.Configure("lookupsettings.redirectpageID" + index); }

        private void AddEnterEvent() {
            var frm = FindControl("vansk");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btSearchDebtor.Click += new System.EventHandler(this.btSearchDebtor_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}