#region

using System;
using System.Configuration;
using NPayments.DAL.PendingCases;

#endregion

namespace NPayments.BLL.PendingCases {
    /// <summary>
    /// Summary description for PendingCase.
    /// </summary>
    [Serializable]
    public class PendingCaseBLLC {
        protected int adjudicator;
        protected string adjudicatorEN;
        protected string adjudicatorNative;
        protected string comments;
        protected int court;
        protected string courtEN;
        protected string courtNative;
        private string currencyCode;
        protected DateTime dateOfWrit;
        protected string defendantID;
        protected string defendantName;
        protected int id = -1;
        protected string location;
        protected PendingCasesDALC pcDal;
        protected string plaintiffID;
        protected string plaintiffName;
        protected string stateTextEN;
        protected string stateTextNative;
        protected int statusClosed = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("PendingCaseStatusClosed"));
        protected int statusID;
        protected int statusLetter = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("PendingCaseStatusLetter"));

        protected int statusRegistered =
            Convert.ToInt32(ConfigurationSettings.AppSettings.Get("PendingCaseStatusRegistered"));

        protected string street;
        protected string streetFloor;
        protected float valueOfSuit;
        protected string writNumber;
        public int ID { get { return id; } set { id = value; } }
        public int StatusID { get { return statusID; } set { statusID = value; } }
        public bool IsClosed { get { return statusID == statusClosed; } }
        public bool IsLetter { get { return statusID == statusLetter; } }
        public bool IsRegistered { get { return statusID == statusRegistered; } }
        public string DefendantID { get { return defendantID; } set { defendantID = value; } }
        public string DefendantName { get { return defendantName; } set { defendantName = value; } }
        public string Street { get { return street; } set { street = value; } }
        public string StreetFloor { get { return streetFloor; } set { streetFloor = value; } }
        public string Location { get { return location; } set { location = value; } }
        public string PlaintiffID { get { return plaintiffID; } set { plaintiffID = value; } }
        public string PlaintiffName { get { return plaintiffName; } set { plaintiffName = value; } }
        public string StateTextEN { get { return stateTextEN; } set { stateTextEN = value; } }
        public string StateTextNative { get { return stateTextNative; } set { stateTextNative = value; } }
        public int Court { get { return court; } set { court = value; } }
        public string CourtEN { get { return courtEN; } set { courtEN = value; } }
        public string CourtNative { get { return courtNative; } set { courtNative = value; } }
        public int Adjudicator { get { return adjudicator; } set { adjudicator = value; } }
        public string AdjudicatorNative { get { return adjudicatorNative; } set { adjudicatorNative = value; } }
        public string AdjudicatorEN { get { return adjudicatorEN; } set { adjudicatorEN = value; } }
        public DateTime DateOfWrit { get { return dateOfWrit; } set { dateOfWrit = value; } }
        public string WritNumber { get { return writNumber; } set { writNumber = value; } }
        public float ValueOfSuit { get { return valueOfSuit; } set { valueOfSuit = value; } }
        public string Comments { get { return comments; } set { comments = value; } }
        public string CurrencyCode { get { return currencyCode; } set { currencyCode = value; } }
        public bool CloseCase() { return GetDAL().UpdatePendingCaseStatus(id.ToString(), statusClosed); }
        public bool RegisterCase() { return GetDAL().UpdatePendingCaseStatus(id.ToString(), statusRegistered); }

        public bool Load(string sId) {
            PendingCaseBLLC pc = GetDAL().GetPendingCase(sId);
            if (pc != null) {
                id = pc.ID;
                defendantID = pc.DefendantID;
                defendantName = pc.DefendantName;
                plaintiffID = pc.plaintiffID;
                plaintiffName = pc.PlaintiffName;
                court = pc.Court;
                courtNative = pc.courtNative;
                courtEN = pc.courtEN;
                adjudicator = pc.Adjudicator;
                adjudicatorEN = pc.adjudicatorEN;
                adjudicatorNative = pc.adjudicatorNative;
                dateOfWrit = pc.DateOfWrit;
                writNumber = pc.WritNumber;
                valueOfSuit = pc.valueOfSuit;
                comments = pc.Comments;
                statusID = pc.StatusID;
                stateTextNative = pc.StateTextNative;
                stateTextEN = pc.StateTextEN;
                currencyCode = pc.CurrencyCode;
                return true;
            } else {
                return false;
            }
        }

        public bool IsAvailableClaimWithSameCaseNumberAndSource() { return GetDAL().IsAvailableClaimWithSameCaseNumberAndSource(id); }
        public bool LoadAddressInfo() { return GetDAL().AddAddressInfo(this); }
        public bool LoadCompanyAddressInfo() { return GetDAL().AddCompanyAddressInfo(this); }
        public bool Save() { return GetDAL().AddPendingCase(this); }

        private PendingCasesDALC GetDAL() {
            if (pcDal == null) {
                pcDal = new PendingCasesDALC();
            }
            return pcDal;
        }
    }
}