#region

using System;
using System.Collections;
using NPayments.DAL.Claims;

#endregion

namespace NPayments.BLL.Claims {
    /// <summary>
    /// Summary description for Claim.
    /// </summary>
    public class Claim {
        private bool _bankruptie;
        private ArrayList _bankruptiesWSameCourtCaseID;
        private bool _caseAgainstCompanie;
        private int _creditInfoID;
        // np_Case table
        // np_InformationSource table
        // np_Status table
        private DateTime _delayDate;
        private DateTime _onHold;
        private DateTime _paymentDate;
        private String _RegisterInternalCommentEN;
        private int _statusID;
        //Cyprus specific
        // Central Bank Registry
        public int ClaimID { get; set; }
        public int CreditInfoID { get { return _creditInfoID; } set { _creditInfoID = value; } }
        public int ClaimOwnerCIID { get; set; }
        public int ClaimTypeID { get; set; }
        public String ClaimTypeEN { get; set; }
        public String ClaimTypeNative { get; set; }
        public String ClaimDescriptionEN { get; set; }
        public String ClaimDescriptionNative { get; set; }
        public String ClaimValidMonths { get; set; }
        public String ClaimValidComment { get; set; }
        public int InformationSourceID { get; set; }
        public String InformationNameEN { get; set; }
        public String InformationNameNative { get; set; }
        public DateTime RegDate { get; set; }
        public DateTime OnHold { get { return _onHold; } set { _onHold = value; } }
        public int StatusID { get { return _statusID; } set { _statusID = value; } }
        public String StateEN { get; set; }
        public String StateNative { get; set; }
        public String StatusDescriptionEN { get; set; }
        public String StatusDescriptionNative { get; set; }
        public String CaseNumber { get; set; }
        public String CurrencySymbol { get; set; }
        public String CurrencyEN { get; set; }
        public double Amount { get; set; }
        public decimal DAmount { get; set; }
        public DateTime ClaimDate { get; set; }
        public bool DoNotDelete { get; set; }

        public DateTime AnnouncementDate // when the letter is sent
        { get; set; }

        public DateTime UnregistedDate { get; set; }
        public DateTime DelayDate { get { return _delayDate; } set { _delayDate = value; } }
        public DateTime PaymentDate { get { return _paymentDate; } set { _paymentDate = value; } }
        public DateTime SearchFromDate { get; set; }
        public DateTime SearchToDate { get; set; }
        public DateTime LastUpdate { get; set; }
        public int RegisterID { get; set; }
        public String RegisterInitials { get; set; }
        public String Valid { get; set; }
        public String GazatteYear { get; set; }
        public String GazettePage { get; set; }
        public String AppliedForByDebtor { get; set; }
        public String RegisterCommentNative { get; set; }
        public String RegisterCommentEN { get; set; }
        public String Agent { get; set; }
        public int ChequeIssuedBank { get; set; }
        public String CaseDecisionType { get; set; }
        public String CBA { get; set; }
        public String RegisterInternalCommentNative { get; set; }
        public String RegisterInternalCommentEN { get { return RegisterInternalCommentEN; } set { _RegisterInternalCommentEN = value; } }
        public bool Bankruptie { get { return _bankruptie; } set { _bankruptie = value; } }
        public bool CaseAgainstCompanie { get { return _caseAgainstCompanie; } set { _caseAgainstCompanie = value; } }
        public ArrayList BankruptiesWSameCourtCaseID { get { return _bankruptiesWSameCourtCaseID; } set { _bankruptiesWSameCourtCaseID = value; } }
        public ArrayList CasesAgainstCompaniesWSameCourtCaseID { get; set; }
        /* Setja state � claimi� m.v. eftirfarandi reglu
		 * 1. Ef 0 m�l eru � skr� hj� vi�komandi �� sta�an = br�f
		 * 2. Ef 1+ m�l eru � bi� �� sta�an = bi�
		 * 3. Ef br�f < 1 �r �� sta�an = skr��
		 * 4. Ef dags uppgert sta�an = afskr��
		 * 5. Ef dags fresta� > currdate �� sta�an = fresta�
		 * 6. Annars br�f
		 * * [status t�purnar eru �essar; 1=New registration,2=Letter,3=On hold,4=Registrated,5=Postponed,6=De-registred (Cancelled),7=Update]
		 */

        public void SetClaimStatus() {
            bool letterSentLastYear = false;
            var myDAL = new ClaimDALC();

            // Ef fyrirt�ki er me� �egar skr�� bankruptie m�l �� er ekki sent br�f
            if (_bankruptie) {
                _bankruptiesWSameCourtCaseID = myDAL.GetAllCaseInstancesPerCourtCaseID(
                    CaseNumber, InformationSourceID, true);
                if (DelayDate > DateTime.Now) {
                    foreach (Claim claim in _bankruptiesWSameCourtCaseID) {
                        claim.StatusID = 5;
                        claim.PaymentDate = _paymentDate;
                        claim.DelayDate = _delayDate;
                    }
                    _statusID = 5;
                    return;
                }

                if (PaymentDate > DateTime.MinValue) {
                    foreach (Claim claim in _bankruptiesWSameCourtCaseID) {
                        claim.StatusID = 6;
                        claim.DelayDate = _delayDate;
                        claim.PaymentDate = _paymentDate;
                    }
                    _statusID = 6;
                    return;
                }
                if (myDAL.AlreadyBankruptieRegistered(_creditInfoID)) {
                    foreach (Claim claim in _bankruptiesWSameCourtCaseID) {
                        claim.DelayDate = _delayDate;
                        claim.PaymentDate = _paymentDate;
                        claim.StatusID = 4;
                    }
                    StatusID = 4;
                } else {
                    StatusID = 2;
                }
                return;
            }
            if (_caseAgainstCompanie) {
                if (DelayDate > DateTime.Now) {
                    _statusID = 5;
                    return;
                }

                if (PaymentDate > DateTime.MinValue) {
                    _statusID = 6;
                    return;
                }
                _statusID = 4;
                return;
            }
            if (DelayDate > DateTime.Now) {
                _statusID = 5;
                return;
            }

            if (PaymentDate > DateTime.MinValue) {
                _statusID = 6;
                return;
            }

            var claimArr = new ArrayList(myDAL.GetAllClaimsByCIID(_creditInfoID));
            if (claimArr.Count == 0) // hefur ekkert m�l skr�� m�li� f�r �� "Letter" status
            {
                _statusID = 2;
                return;
            }

            foreach (Claim claim in claimArr) {
                if (claim.OnHold > DateTime.MinValue) {
                    _statusID = 3;
                    _onHold = DateTime.Now;
                    return;
                }
                if (claim.AnnouncementDate <= DateTime.MinValue) {
                    continue;
                }
                if (claim.AnnouncementDate <= (DateTime.Now - new TimeSpan(365, 0, 0, 0))) {
                    continue;
                }
                letterSentLastYear = true;
                _statusID = 4;
                return;
                // move this to the database as job
            }
            // so far no critera match, check if lettes has been send within the last 12 months. 
            _statusID = letterSentLastYear ? 4 : 2;
        }

        /* Setja state � claimi� m.v. eftirfarandi reglu
		 * 1. Ef 0 m�l eru � skr� hj� vi�komandi �� sta�an = br�f
		 * 2. Ef 1+ m�l eru � bi� �� sta�an = bi�
		 * 3. Ef br�f < 1 �r �� sta�an = skr��
		 * 4. Ef dags uppgert sta�an = afskr��
		 * 5. Ef dags fresta� > currdate �� sta�an = fresta�
		 * 6. Annars br�f
		 * * [status t�purnar eru �essar; 1=New registration,2=Letter,3=On hold,4=Registrated,5=Postponed,6=De-registred (Cancelled),7=Update]
		 */

        public void SetMTClaimStatus(string ddBoxValue, string beforeUpdateValue) {
            //First - check is status has been changed with dd box
            if (ddBoxValue == beforeUpdateValue) {
                if (DelayDate > DateTime.Now) {
                    _statusID = 5;
                    return;
                }

                if (PaymentDate > DateTime.MinValue) {
                    _statusID = 6;
                    return;
                }

                if (_statusID == 3 || _statusID == 4 || _statusID == 7) //dont change
                {
                    return;
                }

                _statusID = 2;
            } else {
                //changed with dd box - then...

                if (DelayDate > DateTime.Now) {
                    _statusID = 5;
                    return;
                }

                if (PaymentDate > DateTime.MinValue) {
                    _statusID = 6;
                    return;
                }

                if (ddBoxValue == "4") {
                    _statusID = 4;
                    return;
                }

                _statusID = int.Parse(ddBoxValue);
            }
        }
    }
}