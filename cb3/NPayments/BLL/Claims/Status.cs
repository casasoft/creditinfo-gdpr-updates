#region

using System;

#endregion

namespace NPayments.BLL.Claims {
    /// <summary>
    /// Summary description for Status.
    /// </summary>
    public class Status {
        public int StatusID { get; set; }
        public String StateEN { get; set; }
        public String StateNative { get; set; }
        public String DescriptionEN { get; set; }
        public String DescriptionNative { get; set; }
    }
}