#region

using System;
using System.Collections;
using System.IO;
using NPayments.BLL.PendingCases;

#endregion

namespace NPayments.BLL.Letter {
    /// <summary>
    /// Summary description for LetterGenerator.
    /// </summary>
    public class PendingCaseLetterGenerator {
        public string Generate(string path) {
            var objReader = new StreamReader(path);
            string docTemplate = "";
            var arrText = new ArrayList();

            string sLine = objReader.ReadToEnd();
            if (sLine != null) {
                arrText.Add(sLine);
                docTemplate += sLine;
            }

            objReader.Close();

            return docTemplate;
        }

        public string Generate(string path, PendingCaseBLLC pCase) {
            var objReader = new StreamReader(path);
            string docTemplate = "";
            var arrText = new ArrayList();

            //	while (sLine != null)
            //	{
            //sLine = objReader.ReadLine();
            string sLine = objReader.ReadToEnd();
            if (sLine != null) {
                arrText.Add(sLine);
                docTemplate += sLine;
            }
            //	}
            objReader.Close();

            /*		foreach (string sOutput in arrText)
						Console.WriteLine(sOutput);
					Console.ReadLine();
			*/
            return GenerateLetters(docTemplate, pCase);
        }

        public string GenerateLetters(string docTemplate, PendingCaseBLLC pCase) {
            string[] myStringArr = {
                                       "%NAME%", "%ADDRESS1%", "%ADDRESS2%", "%TOWN%", "%CIREFERENCE%", "%INFOSOURCE%",
                                       "%INFOSOURCENative%", "%DATEOFREGISTRATION%", "%PLAINTIFFNAME%", "%CASENUMBER%",
                                       "%ADJUDICATOR%", "%ADJUDICATORNative%", "%DESCRIPTIONEN%", "%DESCRIPTIONNative%",
                                       "%DATEOFLETTER%"
                                   };
            string[] myReplaceValues = {
                                           pCase.DefendantName, pCase.Street, pCase.StreetFloor, pCase.Location,
                                           pCase.ID.ToString(), pCase.CourtEN, pCase.CourtNative,
                                           pCase.DateOfWrit.ToShortDateString(), pCase.PlaintiffName, pCase.WritNumber,
                                           pCase.AdjudicatorEN, pCase.AdjudicatorNative, "Pending Claim", "Talba Pendenti",
                                           DateTime.Now.ToShortDateString()
                                       };
            int count = 0;
            foreach (string searchString in myStringArr) {
                docTemplate = docTemplate.Replace(searchString, myReplaceValues[count]);
                count++;
            }
            return docTemplate;
        }
    }
}