#region

using System;

#endregion

namespace NPayments.BLL.Letter {
    /// <summary>
    /// Summary description for Letter.
    /// </summary>
    [Serializable]
    public class Letter {
        private String _caseDate = "";
        private String _caseNo = "";
        private String _claimOwnerName = "";
        private String _debtor_Ref = "";
        private String _debtorAddress = "";
        private String _debtorFirstName = "";
        private String _debtorSurName = "";
        private String _debtorTown = "";
        private String _informationSource = "";
        private String _informationSourceEN = "";
        private DateTime _letterDate = DateTime.Now;
        private String _postcode = "";
        private bool _regularLetter = true;
        private String _type = "";
        private String _typeEN = "";
        public String DebtorFirstName { get { return _debtorFirstName; } set { _debtorFirstName = value; } }
        public String DebtorSurName { get { return _debtorSurName; } set { _debtorSurName = value; } }
        public String DebtorAddress { get { return _debtorAddress; } set { _debtorAddress = value; } }
        public String Postcode { get { return _postcode; } set { _postcode = value; } }
        public String DebtorTown { get { return _debtorTown; } set { _debtorTown = value; } }
        public String Debtor_Ref { get { return _debtor_Ref; } set { _debtor_Ref = value; } }
        public String ClaimOwnerName { get { return _claimOwnerName; } 
            set { 
            _claimOwnerName = value; } 
        
        }
        public String InformationSource { get { return _informationSource; } set { _informationSource = value; } }
        public String InformationSourceEN { get { return _informationSourceEN; } set { _informationSourceEN = value; } }
        public String CaseNo { get { return _caseNo; } set { _caseNo = value; } }
        public String CaseDate { get { return _caseDate; } set { _caseDate = value; } }
        public String Type { get { return _type; } set { _type = value; } }
        public String TypeEN { get { return _typeEN; } set { _typeEN = value; } }
        public bool BankruptieLetter { get; set; }
        public bool RegularLetter { get { return _regularLetter; } set { _regularLetter = value; } }
        public DateTime LetterDate { get { return _letterDate; } set { _letterDate = value; } }
    }
}