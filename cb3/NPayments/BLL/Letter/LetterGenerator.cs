#region

using System.Collections;
using System.IO;

#endregion

namespace NPayments.BLL.Letter
{
    /// <summary>
    /// Summary description for LetterGenerator.
    /// </summary>
    public class LetterGenerator
    {
        public string Generate(string path)
        {
            var objReader = new StreamReader(path);
            string docTemplate = "";
            var arrText = new ArrayList();

            string sLine = objReader.ReadToEnd();
            if (sLine != null)
            {
                arrText.Add(sLine);
                docTemplate += sLine;
            }

            objReader.Close();

            return docTemplate;
        }

        public string Generate(string path, Letter genLetter)
        {
            var objReader = new StreamReader(path);
            string docTemplate = "";
            var arrText = new ArrayList();

            //	while (sLine != null)
            //	{
            //sLine = objReader.ReadLine();
            string sLine = objReader.ReadToEnd();
            if (sLine != null)
            {
                arrText.Add(sLine);
                docTemplate += sLine;
            }
            //	}
            objReader.Close();

            /*		foreach (string sOutput in arrText)
				Console.WriteLine(sOutput);
			Console.ReadLine();
	*/
            return GenerateLetters(docTemplate, genLetter);
        }

        public string GenerateLetters(string docTemplate, Letter genLetter)
        {
            string[] myStringArr = {
                                       "DebtorFirstName", "DebtorSurName", "DebtorAddress", "Postcode", "DebtorTown",
                                       "HeadDate", "Debtor_Ref", "InformationSource", "CaseNo", "CaseDate", "Type"
                                   };
            string[] myReplaceValues = {
                                           genLetter.DebtorFirstName, genLetter.DebtorSurName, genLetter.DebtorAddress,
                                           genLetter.Postcode, genLetter.DebtorTown,
                                           genLetter.LetterDate.ToShortDateString(), genLetter.Debtor_Ref,
                                           genLetter.InformationSource, genLetter.CaseNo, genLetter.CaseDate,
                                           genLetter.Type
                                       };

            if (GenerateLetter.IsMalta)
            {
                myStringArr = new[]
                              {
                                  "%DebtorFirstName%", "%DebtorSurName%", "%DebtorAddress%", "%Postcode%", "%DebtorTown%",
                                  "%HeadDate%", "%Debtor_Ref%", "%InformationSourceNative%", "%InformationSourceEN%",
                                  "%CaseNo%", "%CaseDate%", "%TypeNative%", "%TypeEN%", "%Claim_Owner%"
                              };
                myReplaceValues = new[]
                                  {
                                      genLetter.DebtorFirstName, genLetter.DebtorSurName, genLetter.DebtorAddress,
                                      genLetter.Postcode, genLetter.DebtorTown, genLetter.LetterDate.ToShortDateString(),
                                      genLetter.Debtor_Ref, genLetter.InformationSource, genLetter.InformationSourceEN,
                                      genLetter.CaseNo, genLetter.CaseDate, genLetter.Type, genLetter.TypeEN,
                                      genLetter.ClaimOwnerName
                                  };
            }
            int count = 0;
            foreach (string searchString in myStringArr)
            {
                docTemplate = docTemplate.Replace(searchString, myReplaceValues[count]);
                count++;
            }
            return docTemplate;
        }
    }
}