#region

using System;
using System.Collections;

#endregion

namespace NPayments.BLL {
    /// <summary>
    /// Summary description for Debtor.
    /// </summary>
    public class Debtor {
        public int CreditInfo { get; set; }
        public String Type { get; set; }
        public DateTime Created { get; set; }
        public String FirstName { get; set; }
        public String FirstNameEN { get; set; }
        public String SurName { get; set; }
        public String SurNameEN { get; set; }
        public String ProfessionNameNative { get; set; }
        public String ProfessionNameEN { get; set; }
        public String ProfessionDescriptionNative { get; set; }
        public String ProfessionDescriptionEN { get; set; }
        public String EducationNative { get; set; }
        public String EducationEN { get; set; }
        public String EducationDescriptionNative { get; set; }
        public String EducationDescriptionEN { get; set; }
        public int IDNumber2Type { get; set; }
        public int IDNumber3Type { get; set; }
        public int ProfessionID { get; set; }
        public int EducationID { get; set; }
        public int NIDNumber { get; set; }
        public String IDNumber1 { get; set; }
        public String IDNumber2 { get; set; }
        public String IDNumber3 { get; set; }
        public ArrayList Address { get; set; }
        public ArrayList Number { get; set; }
        public ArrayList IDNumber { get; set; }
    }
}