#region

using System;
using System.Collections;
using System.Data;
using Billing.DAL;
using NPayments.BLL.Claims;
using NPayments.BLL.PendingCases;
using NPayments.DAL.Claims;
using NPayments.DAL.PendingCases;
using UserAdmin.BLL.CIUsers;
using UserAdmin.DAL.CIUsers;
// using NPayments.DAL.CIUsers;
// using NPayments.BLL.CIUsers;

#endregion

namespace NPayments.BLL {
    /// <summary>
    /// Summary description for NPaymentsFactory.
    /// </summary>
    public class NPaymentsFactory {
        private readonly ClaimDALC _myClaimDALC;
        // moved all the NPayment user handling function over to UserAdmin
        //	private NPayments.DAL.CIUsers.CreditInfoUserDALC _myCreditInfoUserDALC;
        private readonly CreditInfoUserDALC _myCreditInfoUserDALC;
        private readonly BillingDALC myBillingDAL = new BillingDALC();

        public NPaymentsFactory() {
            //
            // TODO: Add constructor logic here
            //
            _myClaimDALC = new ClaimDALC();
            _myCreditInfoUserDALC = new CreditInfoUserDALC();
        }

        #region Pending Cases

        public DataTable GetAdjudicatorsAsDataTable() {
            var pcDalc = new PendingCasesDALC();
            return pcDalc.GetAdjudicatorsAsDataTable();
        }

        public DataTable GetAllPendingCaseStatusAsDataTable() {
            var pcDalc = new PendingCasesDALC();
            return pcDalc.GetAllPendingCaseStatusAsDataTable();
        }

        public DataTable GetAllLetterPendingCasesAsDataTable() {
            var pcDalc = new PendingCasesDALC();
            return pcDalc.GetAllLetterCasesAsDataTable();
        }

        public DataTable FindPendingCase(PendingCaseBLLC pCase, DateTime dFrom, DateTime dTo) {
            var pcDalc = new PendingCasesDALC();
            return pcDalc.FindPendingCase(pCase, dFrom, dTo);
        }

        public PendingCaseBLLC GetPendingCase(string id) {
            var pcDalc = new PendingCasesDALC();
            return pcDalc.GetPendingCase(id);
        }

        public bool AddPendingCase(PendingCaseBLLC pCase) {
            var pcDalc = new PendingCasesDALC();
            return pcDalc.AddPendingCase(pCase);
        }

        public bool UpdatePendingCaseStatus(string id, int status) {
            var pcDalc = new PendingCasesDALC();
            return pcDalc.UpdatePendingCaseStatus(id, status);
        }

        #endregion

        public bool FillInfoSourceAndType(Letter.Letter letter) { return _myClaimDALC.FillInfoSourceAndType(letter); }
        public DataSet GetCityListAsDataSet(string cityName, bool en) { return _myCreditInfoUserDALC.GetCityListAsDataSet(cityName, en); }
        public string GetCityName(int cityID, bool en) { return _myCreditInfoUserDALC.GetCityName(cityID, en); }
        public Indivitual GetIndivitual(int creditInfoID) { return _myCreditInfoUserDALC.GetIndivitual(creditInfoID); }
        public int addNewIndivitual(Indivitual theIndi) { return _myCreditInfoUserDALC.AddNewIndivitual(theIndi); }
        public int UpdateIndivitual(Indivitual indi) { return _myCreditInfoUserDALC.UpdateIndivitual(indi); }
        public DataSet GetIDNumberTypesAsDataSet() { return _myCreditInfoUserDALC.GetIDNumberTypesAsDataSet(); }
        public DataSet GetOrgStatus() { return _myCreditInfoUserDALC.GetOrgStatus(); }
        public DataSet GetPNumberTypesAsDataSet() { return _myCreditInfoUserDALC.GetPNumberTypesAsDataSet(); }
        public DataSet GetProfessioListAsDataSet() { return _myCreditInfoUserDALC.GetProfessioListAsDataSet(); }
        public DataSet GetEducationListAsDataSet() { return _myCreditInfoUserDALC.GetEducationListAsDataSet(); }
        public DataSet GetCityListAsDataSet(bool en) { return _myCreditInfoUserDALC.GetCityListAsDataSet(en); }
        public DataSet FindCustomer(UserAdmin.BLL.Debtor myDebtor) { return _myCreditInfoUserDALC.FindCustomer(myDebtor, false); }
        public DataSet FindCustomerInNationalAndCreditInfo(UserAdmin.BLL.Debtor myDebtor) { return _myCreditInfoUserDALC.FindCustomerInNationalAndCreditInfo(myDebtor, false); }
        public String GetCIUserType(int creditInfoID) { return _myCreditInfoUserDALC.GetCIUserType(creditInfoID); }
        // whitelist
        public DataSet SearchWhitelist(Indivitual myIndi) { return _myCreditInfoUserDALC.SearchWhitelist(myIndi); }
        // Companys
        public int AddCompany(Company myComp) { return _myCreditInfoUserDALC.AddCompany(myComp); }
        public DataSet GetCompanyNaceCodes() { return _myCreditInfoUserDALC.GetCompanyNaceCodes(); }
        public DataSet FindCompany(Company myComp) { return _myCreditInfoUserDALC.FindCompany(myComp, false); }
        public DataSet FindCompanyInNationalAndCreditInfo(Company myComp) { return _myCreditInfoUserDALC.FindCompanyInNationalAndCreditInfo(myComp, false); }
        public Company GetCompany(int creditInfoID) { return _myCreditInfoUserDALC.GetCompany(creditInfoID); }
        public int UpdateCompany(Company theComp) { return _myCreditInfoUserDALC.UpdateCompany(theComp); }
        public DataSet SearchCompaniesHouse(Company myComp) { return _myCreditInfoUserDALC.SearchCompaniesHouse(myComp); }
        // Comp && individuals
        public bool IsIDRegisted(String number, int type) { return _myCreditInfoUserDALC.IsIDRegisted(number, type); }
        public int GetCityID(String cityName) { return _myCreditInfoUserDALC.GetCityID(cityName); }
        // Claims part
        public DataTable GetCurrencyListAsDataSet() { return myBillingDAL.GetCurrencyListAsDataTable(); }

        public DataSet GetInformationSourceListAsDataSet() {
            var myClaimDALC = new ClaimDALC();
            return myClaimDALC.GetInformationSourceListAsDataSet();
        }

        public DataSet GetCaseTypesAsDataSet(String CaseID) {
            var myClaimDALC = new ClaimDALC();
            return myClaimDALC.GetCaseTypesAsDataSet(CaseID);
        }

        public DataSet GetCaseTypesAsDataSet() {
            var myClaimDALC = new ClaimDALC();
            return myClaimDALC.GetCaseTypesAsDataSet();
        }

        public bool AddNewClaim(Claim myClaim) {
            var myClaimDALC = new ClaimDALC();
            return myClaimDALC.AddNewClaim(myClaim);
        }

        public bool UpdateClaim(Claim myClaim) {
            var myClaimDALC = new ClaimDALC();
            return myClaimDALC.UpdateClaim(myClaim);
        }

        public Status GetStatus(int statusID) {
            var myClaimDALC = new ClaimDALC();
            return myClaimDALC.GetStatus(statusID);
        }

        public DataSet GetStatusListAsDataSet() { return _myClaimDALC.GetStatusListAsDataSet(); }
        public DataSet FindClaim(Claim searchClaim) { return _myClaimDALC.FindClaim(searchClaim); }
        public Claim GetClaim(int claimID) { return _myClaimDALC.GetClaim(claimID); }
        public DataSet GetClaimOverViewListAsDataSet() { return _myClaimDALC.GetClaimOverViewListAsDataSet(); }
        public DataSet GetLetterClaimOverViewListAsDataSet() { return _myClaimDALC.GetLetterClaimOverViewListAsDataSet(); }
        public int UpdateClaimLetterStatus(ArrayList myArr) { return _myClaimDALC.UpdateClaimLetterStatus(myArr); }
        public DataSet GetAllClaimsByCIIDAsDataset(String CreditInfoID) { return _myClaimDALC.GetAllClaimsByCIIDAsDataset(CreditInfoID); }
        public ArrayList GetAllClaimsByCIID(int CreditInfoID) { return _myClaimDALC.GetAllClaimsByCIID(CreditInfoID); }
        public bool AlreadyBankruptieRegistered(int creditInfoID) { return _myClaimDALC.AlreadyBankruptieRegistered(creditInfoID); }
        public ArrayList GetAllCaseInstancesPerCourtCaseID(String courtCaseID, int informationSourceID, bool individual) { return _myClaimDALC.GetAllCaseInstancesPerCourtCaseID(courtCaseID, informationSourceID, individual); }
// other
        public DataSet GetIssuedBanksAsDataSet(bool en) { return _myClaimDALC.GetIssuedBanksAsDataSet(en); }
        public DataSet GetCaseDecisionsTypesAsDataSet() { return _myClaimDALC.GetCaseDecisionsTypesAsDataSet(); }
        public ArrayList GetCaseTypesAsArrayList(int caseType) { return _myClaimDALC.GetCaseTypesAsArrayList(caseType); }
    }
}