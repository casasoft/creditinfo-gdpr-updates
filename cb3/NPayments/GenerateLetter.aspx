<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="GenerateLetter.aspx.cs" AutoEventWireup="false" Inherits="NPayments.GenerateLetter" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>GenerateLetter</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body ms_positioning="GridLayout">
		<form id="claimoverview" name="claimoverview" action="" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<th>
															<asp:label id="lbLetterClaims" runat="server">All claims with status "Letter"</asp:label>&nbsp;-
															<asp:Label id="lblTotalNumberOfNPIHits" runat="server"></asp:Label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divDataGrid" style="OVERFLOW-X: auto; OVERFLOW: hidden; WIDTH: 100%; HEIGHT: auto"
																			runat="server">
																			<asp:datagrid id="dgLetterClaims" runat="server" cssclass="grid" autogeneratecolumns="False" gridlines="None">
																				<FooterStyle CssClass="grid_footer"></FooterStyle>
																				<SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
																				<AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
																				<ItemStyle CssClass="grid_item"></ItemStyle>
																				<HeaderStyle CssClass="grid_header"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="ID" HeaderText="Claim ID">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="CreditInfoID" HeaderText="Deb. CIID">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Owner">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="ClaimOwnerCIID" HeaderText="OwnerNative">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" HeaderText="OwnerEN">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Type">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="TypeNative" HeaderText="TypeNative">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="TypeEN" HeaderText="TypeEN">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Source">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="NameNative" HeaderText="SourceNative">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="NameEN" HeaderText="SourceEN">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="RegDate" HeaderText="RegDate">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="ClaimDate" HeaderText="Claim date">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn HeaderText="State">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="StateNative" HeaderText="StateNative">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="StateEN" HeaderText="StateEN">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="CaseNr" HeaderText="Case No">
																						<ItemStyle CssClass="padding"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Regular Letter">
																						<ItemStyle CssClass="padding"></ItemStyle>
																						<ItemTemplate>
																							<asp:checkbox id="chkRegularLetter" runat="server" checked="True" cssclass="radio"></asp:checkbox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn HeaderText="Bankruptcy Letter">
																						<ItemStyle CssClass="padding"></ItemStyle>
																						<ItemTemplate>
																							<asp:checkbox id="chkBankruptieLetter" runat="server" cssclass="radio"></asp:checkbox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:boundcolumn visible="False" datafield="ClaimantUserType" headertext="ClaimantUserType"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerFirstNameNative"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerSurNameNative"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerFirstNameEN"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerSurNameEN"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerCompanyNameNative"></asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="ClaimOwnerCompanyNameEN"></asp:boundcolumn>
																				</Columns>
																				<PagerStyle CssClass="grid_pager"></PagerStyle>
																			</asp:datagrid>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
														</td>
														<td align="right">
															<asp:button id="btnUncheckAll" runat="server" text="Uncheck all" cssclass="gray_button"></asp:button><asp:button id="btUpdateStatus" runat="server" text="Update status" cssclass="gray_button"></asp:button><asp:button id="btnGenLetter" runat="server" text="Generate Letters" cssclass="confirm_button"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
