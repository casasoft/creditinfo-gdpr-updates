using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Claims;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;
using Debtor=UserAdmin.BLL.Debtor;
using Image=System.Web.UI.WebControls.Image;

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for MTNewClaim.
    /// </summary>
    public class MTNewClaim : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected Button btnClaimOwnerClear;
        protected Button btnCreateClaimOwner;
        protected Button btnCreateDebtor;
        protected Button btnDebtorClear;
        protected Button btnSearchClaimOwner;
        protected Button btnSearchDebtor;
        protected Button btnShowAll;
        protected Button btnSubmitAndClear;
        protected Button btSubmit;
        private int claimStatus;
        private bool company;
        protected CustomValidator CustomValidator1;
        protected CustomValidator CustomValidator2;
        protected CustomValidator CustomValidator3;
        protected DropDownList ddCurrency;
        protected DropDownList ddlCaseType;
        protected DropDownList ddlClaimStatus;
        protected DropDownList ddlSourceOfInfo;
        protected DataGrid dgClaimOwner;
        protected DataGrid dgDebtors;
        protected HtmlGenericControl divClaimOwner;
        protected HtmlGenericControl divNameSearch;
        private bool EN;
        protected Image imgCompanyClaimOwner;
        protected Image imgCompanyDebtor;
        protected Image imgIndividualClaimOwner;
        protected Image imgIndividualDebtor;
        protected Label lbCaseID;
        protected Label lbClaim;
        protected Label lbClaimAmount;
        protected Label lbClaimOwnerName;
        protected Label lbComment;
        protected Label lbDates;
        protected Label lbDebtor;
        protected Label lbDebtorFirstName;
        protected Label lbDebtorID;
        protected Label lbInternalComment;
        protected Label lblClaimID;
        protected Label lblClaimIDText;
        protected Label lblClaimOwner;
        protected Label lblClaimOwnerCIID;
        protected Label lblClaimOwnerNameSearchDatagridHeader;
        protected Label lblClaimOwnerNameSearchDatagridIcons;
        protected Label lblClaimOwnerNationalID;
        protected Label lblClaimStatus;
        protected Label lblClaimType;
        protected Label lblMsg;
        protected Label lblNameSearchDatagridHeader;
        protected Label lblNameSearchDatagridIcons;
        protected Label lblNationalID;
        protected Label lblNewClaim;
        protected Label lblSourceofInfo;
        protected Label lbOnHold;
        protected Label lbOther;
        protected Label lbPayment;
        protected Label lbRegisted;
        protected Label lbRegister;
        protected Label lbRegistration;
        protected Label lbStatus;
        private NPaymentsFactory myFact = new NPaymentsFactory();
        private bool newRegistration = true;
        protected RadioButton rbtCompany;
        protected RadioButton rbtIndividual;
        protected RadioButton rbtnCompanyClaimOwner;
        protected RadioButton rbtnIndividualClaimOwner;
        private string redirectPage = "";
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected RequiredFieldValidator RequiredFieldValidator4;
        protected RequiredFieldValidator RequiredFieldValidator5;
        protected RequiredFieldValidator rfvCIID;
        protected RequiredFieldValidator rfvClaimOwnerCIID;
        protected HtmlTable tableClaimOwnerNameSearchGrid;
        protected HtmlTable tableNameSearchGrid;
        protected TextBox tbPageID;
        protected TextBox txtCaseID;
        protected TextBox txtClaimAmount;
        protected TextBox txtClaimDate;
        protected TextBox txtClaimDelayedDate;
        protected TextBox txtClaimOwnerCIID;
        protected TextBox txtClaimOwnerNationalID;
        protected TextBox txtClaimOwnerNative;
        protected TextBox txtClaimStatus;
        protected TextBox txtComment;
        protected TextBox txtDebtorFirstName;
        protected TextBox txtDebtorID;
        protected TextBox txtInternalComment;
        protected TextBox txtNationalID;
        protected TextBox txtPaymentDate;
        protected TextBox txtRegDate;
        protected TextBox txtRegister;
        protected TextBox txtStatus;
        protected ValidationSummary ValidationSummary1;
        protected CheckBox chkdoNotDelete;

        private void Page_Load(object sender, EventArgs e) {
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            Page.ID = "218";

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            tableClaimOwnerNameSearchGrid.Visible = false;
            tableNameSearchGrid.Visible = false;

            if (Request.QueryString["update"] != null) {
                newRegistration = false;
            }

            if (Request.QueryString["company"] != null) {
                company = true;
            }

            AddEnterEvent();

            lblMsg.Visible = false;

            if (ViewState["ClaimStatus"] != null) {
                claimStatus = (int) ViewState["ClaimStatus"];
            }

            if (!Page.IsPostBack) {
                InitDDBoxes();
                LocalizeText();

                tableNameSearchGrid.Visible = false;
                tableClaimOwnerNameSearchGrid.Visible = false;
                InitControls();
                SetColumnsHeaderByCulture();
                //SetColumnsByCulture();
                rbtIndividual.Checked = true;
                Session.Remove("ClaimStatus");
            }

            if (newRegistration) {
                claimStatus = 1;
                txtRegDate.Text = DateTime.Now.ToString();
            } else {
                if (Request.Params["pageid"] != null) {
                    SetRedirectPage(int.Parse(Request.Params["pageid"]));
                    tbPageID.Text = Request.Params["pageid"];
                }
            }
            if (tbPageID.Text != "") {
                SetRedirectPage(int.Parse(tbPageID.Text));
            }
        }

        private void InitControls() {
            int creditInfoID = -1;
            if (newRegistration) {
                InitHeader(true, -1);
                //Set name of logged in user in the Register box
                PrepareControlsForNewRegistration();
                if (Session["ClaimOwnerInfo"] != null && Session["ClaimOwnerInfo"].ToString() == "true") {
                    if (Session["CreditInfoID"] != null) {
                        txtClaimOwnerCIID.Text = Session["CreditInfoID"].ToString();
                    }
                    if (Session["ClaimOwnerNameNative"] != null) {
                        txtClaimOwnerNative.Text = Session["ClaimOwnerNameNative"].ToString().Trim();
                    }
                    if (Session["ClaimOwnerNameEN"] != null && txtClaimOwnerNative.Text == "") {
                        txtClaimOwnerNative.Text = Session["ClaimOwnerNameEN"].ToString().Trim();
                    }

                    if (Session["tbDebtorFirstName"] != null) {
                        txtDebtorFirstName.Text = Session["tbDebtorFirstName"].ToString();
                    }
                    if (Session["tbDebtorSurname"] != null) {
                        txtDebtorFirstName.Text += " " + Session["tbDebtorSurname"];
                    }
                    if (Session["tbDebtorID"] != null) {
                        int.Parse(Session["tbDebtorID"].ToString());
                        txtDebtorID.Text = Session["tbDebtorID"].ToString();
                    }
                    if (Session["txtNationalID"] != null) {
                        txtNationalID.Text = Session["txtNationalID"].ToString();
                    }

                    Session.Remove("tbDebtorFirstName");
                    Session.Remove("tbDebtorSurname");
                    Session.Remove("tbDebtorID");
                    Session.Remove("txtNationalID");

                    Session.Remove("ClaimOwnerInfo");
                    Session.Remove("CreditInfoID");
                    Session.Remove("ClaimOwnerNameNative");
                    Session.Remove("ClaimOwnerNameEN");
                } else {
                    if (Session["CreditInfoID"] != null) {
                        creditInfoID = (int) Session["CreditInfoID"];
                    }
                    if (company) {
                        rbtCompany.Checked = true;
                        if (Session["CompanyNameNative"] != null) {
                            txtDebtorFirstName.Text = Session["CompanyNameNative"].ToString();
                        }
                        Session.Remove("CreditInfoID");
                        Session.Remove("CompanyNameNative");
                        Session.Remove("CompanyNameEN");
                    } else {
                        rbtIndividual.Checked = true;
                        if (Session["DebtorFirstName"] != null) {
                            txtDebtorFirstName.Text = Session["DebtorFirstName"].ToString();
                        }
                        if (Session["DebtorSurName"] != null) {
                            txtDebtorFirstName.Text += " " + Session["DebtorSurName"];
                        }
                        Session.Remove("CreditInfoID");
                        Session.Remove("DebtorFirstName");
                        Session.Remove("DebtorSurName");
                    }

                    if (creditInfoID != -1) {
                        txtDebtorID.Text = creditInfoID.ToString();
                    }

                    if (Session["txtClaimOwnerCIID"] != null) {
                        txtClaimOwnerCIID.Text = Session["txtClaimOwnerCIID"].ToString();
                    }
                    if (Session["tbClaimOwnerNative"] != null) {
                        txtClaimOwnerNative.Text = Session["tbClaimOwnerNative"].ToString();
                    }
                    if (Session["txtClaimOwnerNationalID"] != null) {
                        txtClaimOwnerNationalID.Text = Session["txtClaimOwnerNationalID"].ToString();
                    }

                    if (Session["txtNationalID"] != null) {
                        txtNationalID.Text = Session["txtNationalID"].ToString();
                    }

                    Session.Remove("txtClaimOwnerCIID");
                    Session.Remove("tbCreditorNameNative");
                    Session.Remove("txtNationalID");
                }
            } else {
                int claimID = -1;
                if (Session["claimID"] != null) {
                    claimID = (int) Session["ClaimID"];
                }
                if (Session["CreditInfoID"] != null) {}

                Claim myClaim = myFact.GetClaim(claimID);
                btSubmit.Text = rm.GetString("txtUpdate", ci);
                InitHeader(false, myClaim.ClaimID);

                ddlClaimStatus.SelectedValue = myClaim.StatusID.ToString();
                //Keep status to check if dd box is used to change status
                txtClaimStatus.Text = myClaim.StatusID.ToString();

                txtDebtorID.Text = myClaim.CreditInfoID.ToString();

                chkdoNotDelete.Checked = myClaim.DoNotDelete;

                //Get Debtor info
                if (myClaim.CreditInfoID > -1) {
                    string debtorType = myFact.GetCIUserType(myClaim.CreditInfoID);
                    if (debtorType.Trim().Equals("-1")) // no type found
                    {
                        debtorType = CigConfig.Configure("lookupsettings.individualID");
                            // then set to individual as default
                    }
                    if (debtorType.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                        rbtIndividual.Checked = true;
                        Indivitual myIndi = myFact.GetIndivitual(myClaim.CreditInfoID);
                        txtNationalID.Text = GetNationalID(myIndi.IDNumbers);
                        if (EN) {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameEN)) {
                                txtDebtorFirstName.Text = myIndi.FirstNameEN;
                                txtDebtorFirstName.Text += " " + myIndi.SurNameEN;
                            } else {
                                txtDebtorFirstName.Text = myIndi.FirstNameNative;
                                txtDebtorFirstName.Text += " " + myIndi.SurNameNative;
                            }
                        } else {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameNative)) {
                                txtDebtorFirstName.Text = myIndi.FirstNameNative;
                                txtDebtorFirstName.Text += " " + myIndi.SurNameNative;
                            } else {
                                txtDebtorFirstName.Text = myIndi.FirstNameEN;
                                txtDebtorFirstName.Text += " " + myIndi.SurNameEN;
                            }
                        }
                    } else if (debtorType.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                        // company
                        rbtCompany.Checked = true;
                        var myComp = myFact.GetCompany(myClaim.CreditInfoID);
                        txtNationalID.Text = GetNationalID(myComp.IDNumbers);
                        if (EN) {
                            txtDebtorFirstName.Text = !string.IsNullOrEmpty(myComp.NameEN) ? myComp.NameEN : myComp.NameNative;
                        } else {
                            txtDebtorFirstName.Text = !string.IsNullOrEmpty(myComp.NameNative) ? myComp.NameNative : myComp.NameEN;
                        }
                    }
                }

                txtClaimOwnerCIID.Text = myClaim.ClaimOwnerCIID.ToString();
                if (myClaim.ClaimOwnerCIID > 0) {
                    string claimOwnerType = myFact.GetCIUserType(myClaim.ClaimOwnerCIID);
                    if (claimOwnerType.Trim().Equals("-1")) // no type found
                    {
                        claimOwnerType = CigConfig.Configure("lookupsettings.individualID");
                            // then set to individual as default
                    }
                    if (claimOwnerType.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                        rbtnIndividualClaimOwner.Checked = true;
                        var myIndi = myFact.GetIndivitual(myClaim.ClaimOwnerCIID);
                        if (EN) {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameEN)) {
                                txtClaimOwnerNative.Text = myIndi.FirstNameEN + " " + myIndi.SurNameEN;
                            } else {
                                txtClaimOwnerNative.Text = myIndi.FirstNameNative + " " + myIndi.SurNameNative;
                            }
                        } else {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameNative)) {
                                txtClaimOwnerNative.Text = myIndi.FirstNameNative + " " + myIndi.SurNameNative;
                            } else {
                                txtClaimOwnerNative.Text = myIndi.FirstNameEN + " " + myIndi.SurNameEN;
                            }
                        }
                        if (myIndi.IDNumbers.Count > 0) {
                            var idn = (IDNumber) myIndi.IDNumbers[0];
                            txtClaimOwnerNationalID.Text = idn.Number;
                        }
                    } else if (claimOwnerType.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                        // company
                        rbtnCompanyClaimOwner.Checked = true;
                        var myComp = myFact.GetCompany(myClaim.ClaimOwnerCIID);
                        if (EN) {
                            txtClaimOwnerNative.Text = !string.IsNullOrEmpty(myComp.NameEN) ? myComp.NameEN : myComp.NameNative;
                        } else {
                            txtClaimOwnerNative.Text = !string.IsNullOrEmpty(myComp.NameNative) ? myComp.NameNative : myComp.NameEN;
                        }
                        txtClaimOwnerNationalID.Text = myComp.NationalID;
                    }
                }
                ddlSourceOfInfo.SelectedValue = myClaim.InformationSourceID.ToString();
                txtRegDate.Text = myClaim.RegDate.ToString();
                if (myClaim.ClaimDate > DateTime.MinValue) {
                    txtClaimDate.Text = myClaim.ClaimDate.ToShortDateString();
                }
                if (myClaim.DelayDate > DateTime.MinValue) {
                    txtClaimDelayedDate.Text = myClaim.DelayDate.ToShortDateString();
                }
                if (myClaim.PaymentDate > DateTime.MinValue) {
                    txtPaymentDate.Text = myClaim.PaymentDate.ToShortDateString();
                }
                txtClaimAmount.Text = myClaim.DAmount.ToString();

                try {
                    ddCurrency.SelectedValue = myClaim.CurrencySymbol;
                } catch (Exception) {}

                ddlCaseType.SelectedValue = myClaim.ClaimTypeID.ToString();
                txtCaseID.Text = myClaim.CaseNumber;
                txtStatus.Text = myClaim.StateEN;
                ViewState["ClaimStatus"] = myClaim.StatusID;
                txtRegister.Text = myClaim.RegisterInitials;
                txtComment.Text = myClaim.RegisterCommentNative;
                txtInternalComment.Text = myClaim.RegisterInternalCommentNative;
                Session.Remove("ClaimID");
                Session.Remove("CreditInfoID");
            }
        }

        private void OrderCurrencyList(string orderList) {
            var strArray = orderList.Split(new[] {','});
            int index = 0;
            foreach (string id in strArray) {
                var li = ddCurrency.Items.FindByValue(id);
                ddCurrency.Items.Remove(li);
                ddCurrency.Items.Insert(index, li);
                index++;
            }
        }

        private void InitDDBoxes() {
            //Currency
            ddCurrency.DataSource = myFact.GetCurrencyListAsDataSet();
            ddCurrency.DataTextField = "CurrencyCode";
            ddCurrency.DataValueField = "CurrencyCode";
            ddCurrency.DataBind();
            try {
                ddCurrency.SelectedValue = "EUR";
            } catch (Exception) {
//Ignore - just try if this is in currency
            }

            if (CigConfig.Configure("lookupsettings.orderCurrencyList") != null) {
                OrderCurrencyList(CigConfig.Configure("lookupsettings.orderCurrencyList"));
            }

            ddlClaimStatus.DataSource = myFact.GetStatusListAsDataSet();
            ddlClaimStatus.DataTextField = "StateEN";
            ddlClaimStatus.DataValueField = "StatusID";
            ddlClaimStatus.DataBind();
            ddlClaimStatus.SelectedValue = "2";

            // Source of info
            ddlSourceOfInfo.DataSource = myFact.GetInformationSourceListAsDataSet();
            ddlSourceOfInfo.DataTextField = EN ? "NameEN" : "NameNative";

            ddlSourceOfInfo.DataValueField = "InformationSourceID";
            ddlSourceOfInfo.DataBind();
            var theItem = new ListItem("N/A", "0");
            ddlSourceOfInfo.Items.Insert(0, theItem);

            // type
            ddlCaseType.DataSource = myFact.GetCaseTypesAsDataSet(CigConfig.Configure("lookupsettings.MT_CaseType"));
            ddlCaseType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddlCaseType.DataValueField = "CaseTypeID";
            ddlCaseType.DataBind();
        }

        private bool SaveClaim() {
            var myClaim = new Claim();
            myFact = new NPaymentsFactory();
            if (!Page.IsValid) {
                return false;
            }
            try {
                myClaim.CreditInfoID = int.Parse(txtDebtorID.Text);
                myClaim.ClaimOwnerCIID = int.Parse(txtClaimOwnerCIID.Text);
                try {
                    myClaim.ClaimTypeID = int.Parse(ddlCaseType.SelectedItem.Value);
                } catch (Exception err) {
                    Logger.WriteToLog(
                        "MTNewClaim.aspx, SaveClaim(), myClaim.ClaimTypeID = int.Parse(this.ddlCaseType.SelectedItem.Value) ",
                        err,
                        true);
                }
                myClaim.StatusID = claimStatus; //int.Parse(this.tbStatus.Text); // athuga me� value h�r!
                myClaim.CaseNumber = txtCaseID.Text;
                myClaim.CurrencySymbol = ddCurrency.SelectedItem.Text;
                myClaim.CurrencyEN = ""; // unneccessery field?
                try {
                    myClaim.DAmount = Convert.ToDecimal(txtClaimAmount.Text);
                } catch (Exception err) {
                    Logger.WriteToLog(
                        "MTNewClaim.aspx, SaveClaim(), myClaim.DAmount = Convert.ToDecimal(this.txtClaimAmount.Text) ",
                        err,
                        true);
                }
                myClaim.RegisterCommentNative = txtComment.Text;
                myClaim.RegisterInternalCommentNative = txtInternalComment.Text;
                myClaim.Agent = "";
                myClaim.CBA = "False";
                myClaim.DoNotDelete = chkdoNotDelete.Checked;
                myClaim.ChequeIssuedBank = 1;
                if (ddlSourceOfInfo.SelectedIndex != 0) {
                    myClaim.InformationSourceID = int.Parse(ddlSourceOfInfo.SelectedItem.Value);
                }

                IFormatProvider format = CultureInfo.CurrentCulture;
                if (txtRegDate.Text != "") {
                    myClaim.RegDate = DateTime.Parse(txtRegDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                }
                if (txtClaimDate.Text != "") {
                    myClaim.ClaimDate = DateTime.Parse(txtClaimDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                }
                if (txtClaimDelayedDate.Text != "") {
                    myClaim.DelayDate = DateTime.Parse(
                        txtClaimDelayedDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                }
                if (txtPaymentDate.Text != "") {
                    myClaim.PaymentDate = DateTime.Parse(
                        txtPaymentDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                }

                //			myClaim.RegisterID = 9001; // taka �etta �r session �egar loggin mechanisminn ver�ur � lagi
                try {
                    myClaim.RegisterID = (int) Session["UserCreditInfoID"];
                } catch (Exception err) {
                    Logger.WriteToLog(
                        "MTNewClaim.aspx, SaveClaim(), myClaim.RegisterID = (int) Session[\"UserCreditInfoID\"] ",
                        err,
                        true);
                }

                //Check if status set with dd box is 5 or 6 - then dates are needed
                if (ddlClaimStatus.SelectedValue != txtClaimStatus.Text) {
                    if (ddlClaimStatus.SelectedValue == "5") {
                        if (myClaim.DelayDate <= DateTime.Now) {
                            //Can not do that - inform
                            DisplayError("Delay date is requires if \"Postponed\" status is selected");
                            return false;
                        }
                    }

                    if (ddlClaimStatus.SelectedValue == "6") {
                        if (myClaim.PaymentDate <= DateTime.MinValue) {
                            //Can not do that - inform
                            DisplayError("Payment date is requires if \"De-registered\" status is selected");
                            return false;
                        }
                    }
                }

                // now we need to set status of claim
                myClaim.SetMTClaimStatus(ddlClaimStatus.SelectedValue, txtClaimStatus.Text);
            } catch (Exception err) {
                Logger.WriteToLog("Exception on MTNewClaim.aspx caught, message is : " + err.Message, true);
                DisplayError(rm.GetString("errMsg2", ci));
                return false;
            }
            if (newRegistration) {
                if (!myFact.AddNewClaim(myClaim)) {
                    Logger.WriteToLog("MTNewClaim.aspx, SaveClaim(), if(!myFact.AddNewClaim(myClaim))", true);
                    DisplayError(rm.GetString("errMsg1", ci));
                    return false;
                }
                DisplayMsg(rm.GetString("txtClaimRegistered", ci));
                ClearSessionVariables();
                PrepareControlsForNewRegistration();
            } else {
                myClaim.LastUpdate = DateTime.Now;
                myClaim.ClaimID = int.Parse(lblClaimID.Text);
                if (!myFact.UpdateClaim(myClaim)) {
                    Logger.WriteToLog("MTNewClaim.aspx, SaveClaim(), if(!myFact.UpdateClaim(myClaim)) ", true);
                    DisplayError(rm.GetString("errMsg1", ci));
                    return false;
                }
                //		ClearForm();
                DisplayMsg(rm.GetString("txtClaimUpdated", ci));
                //If redirect-page is set then redirect
                if (redirectPage != "") {
                    Response.Redirect(redirectPage);
                }
                // halda g�mlu gildunum ...�etta �arf �� a� pr�fa betur!!
                //		ClearSessionVariables();
                //		PrepareControlsForNewRegistration();
            }
            //ClearForm(false);
            return true;
        }

        private void btnClaimOwnerClear_Click(object sender, EventArgs e) {
            txtClaimOwnerCIID.Text = "";
            txtClaimOwnerNative.Text = "";
            txtClaimOwnerNationalID.Text = "";
            dgClaimOwner.DataSource = null;
            dgClaimOwner.DataBind();
            tableClaimOwnerNameSearchGrid.Visible = false;
        }

        private void btnCustomerClear_Click(object sender, EventArgs e) {
            txtDebtorFirstName.Text = "";
            txtDebtorID.Text = "";
            txtNationalID.Text = "";
            dgDebtors.DataSource = null;
            dgDebtors.DataBind();
            tableNameSearchGrid.Visible = false;
        }

        # region Utility methods

        private void InitHeader(bool newClaim, int claimID) {
            if (newClaim) {
                lblClaimID.Visible = false;
                lblClaimIDText.Visible = false;
                lblNewClaim.Text = rm.GetString("txtNewClaim", ci);
            } else {
                lblClaimID.Text = claimID.ToString();
                lblClaimIDText.Text = rm.GetString("lbClaimID", ci);
                lblNewClaim.Text = rm.GetString("txtUpdateClaim", ci);
                lblClaimID.Visible = true;
                lblClaimIDText.Visible = true;
            }
        }

        private static string GetNationalID(IList idNumbers) {
            if (idNumbers != null) {
                for (int i = 0; i < idNumbers.Count; i++) {
                    var idn = (IDNumber) idNumbers[i];
                    if (idn.NumberTypeID == int.Parse(CigConfig.Configure("lookupsettings.nationalID"))) {
                        return idn.Number;
                    }
                }
            }
            return "";
        }

        public void SetRedirectPage(int index) { redirectPage = CigConfig.Configure("lookupsettings.redirectpageID" + index); }

        private void AddEnterEvent() {
            Control frm = FindControl("MTNewClaim");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    if ((ctrl).ID == "txtInternalComment" || (ctrl).ID == "txtComment") {
                        continue;
                    }
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void DisplayMsg(string msg) {
            lblMsg.Text = msg;
            lblMsg.ForeColor = Color.Blue;
            lblMsg.Visible = true;
        }

        private void DisplayError(string err) {
            lblMsg.Text = err;
            lblMsg.ForeColor = Color.Red;
            lblMsg.Visible = true;
        }

        public void DateValidate(object source, ServerValidateEventArgs value) {
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        private void ClearForm(bool clearAll) {
            if (clearAll) {
                txtClaimOwnerCIID.Text = "";
                txtClaimOwnerNative.Text = "";
                txtClaimOwnerNationalID.Text = "";
                txtClaimDate.Text = "";
                txtClaimAmount.Text = "";
                txtCaseID.Text = "";
            }
            txtDebtorID.Text = "";
            txtDebtorFirstName.Text = "";
            txtNationalID.Text = "";
            txtStatus.Text = "";
            txtRegister.Text = "";
            txtComment.Text = "";
            txtClaimDelayedDate.Text = "";
            txtPaymentDate.Text = "";
            txtInternalComment.Text = "";
            tableNameSearchGrid.Visible = false;
        }

        private void ClearSessionVariables() {
            Session["CreditInfoID"] = null;
            Session["DebtorFirstName"] = null;
            Session["DebtorSurName"] = null;
            Session["ClaimID"] = null;
        }

        private void PrepareControlsForNewRegistration() {
            newRegistration = true;
            claimStatus = 1;
            txtRegDate.Text = DateTime.Now.ToString();
            Status myStatus = myFact.GetStatus(claimStatus);
            txtStatus.Text = myStatus.StateEN;
            if (Session["UserLoginName"] != null) {
                txtRegister.Text = Session["UserLoginName"].ToString();
            }
        }

        private void LocalizeText() {
            btSubmit.Text = rm.GetString("txtSubmitAndNew", ci);
            btnShowAll.Text = rm.GetString("txtShowAllCases", ci);
            btnSubmitAndClear.Text = rm.GetString("txtSubmitAndClear", ci);
            btnSearchClaimOwner.Text = rm.GetString("txtSearch", ci);
            btnSearchDebtor.Text = rm.GetString("txtSearch", ci);

            lbDebtorID.Text = rm.GetString("lbDebtorID", ci);
            lbDates.Text = rm.GetString("lbDates", ci);
            lbRegisted.Text = rm.GetString("lbRegisted", ci);
            lbClaim.Text = rm.GetString("lbClaim", ci);
            lbOnHold.Text = rm.GetString("lbOnHold", ci);
            lbPayment.Text = rm.GetString("lbPayment", ci);
            lbOther.Text = rm.GetString("lbOther", ci);
            lbClaimAmount.Text = rm.GetString("lbClaimAmount", ci);
            lbCaseID.Text = rm.GetString("lbCaseID", ci);
            lbRegistration.Text = rm.GetString("lbRegistration", ci);
            lbStatus.Text = rm.GetString("lbStatus", ci);
            lbRegister.Text = rm.GetString("lbRegister", ci);
            lbComment.Text = rm.GetString("lbComment", ci);
            lblSourceofInfo.Text = rm.GetString("txtInformationSource", ci);
            lbDebtorFirstName.Text = rm.GetString("lbName", ci);
            lblClaimOwnerNationalID.Text = rm.GetString("lbNationalID", ci);
            lbClaimOwnerName.Text = rm.GetString("lbClaimOwnerName", ci);
            lblClaimType.Text = rm.GetString("lbType", ci);
            lblNewClaim.Text = rm.GetString("lbClaim", ci);
            lbDebtor.Text = rm.GetString("txtDebtor", ci);
            lbInternalComment.Text = rm.GetString("lbInternalComment", ci);
            lblNationalID.Text = rm.GetString("lbNationalID", ci);
            lblClaimOwnerCIID.Text = rm.GetString("txtClaimOwnerCIID", ci);
            lblClaimOwner.Text = rm.GetString("txtClaimOwner", ci);
            lblNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblClaimOwnerNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);

            RequiredFieldValidator3.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator4.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator5.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvCIID.ErrorMessage = rm.GetString("txtValueMissing", ci);

            CustomValidator1.ErrorMessage = rm.GetString("CustomValidator1", ci);
            CustomValidator2.ErrorMessage = rm.GetString("CustomValidator2", ci);
            CustomValidator3.ErrorMessage = rm.GetString("CustomValidator3", ci);

            imgIndividualDebtor.AlternateText = rm.GetString("txtIndividual", ci);
            imgCompanyDebtor.AlternateText = rm.GetString("txtCompany", ci);
            imgIndividualClaimOwner.AlternateText = rm.GetString("txtIndividual", ci);
            imgCompanyClaimOwner.AlternateText = rm.GetString("txtCompany", ci);
        }

        private void SetColumnsHeaderByCulture() {
            dgDebtors.Columns[1].HeaderText = rm.GetString("lbNationalID", ci);
            dgDebtors.Columns[2].HeaderText = rm.GetString("lbName", ci);
            dgDebtors.Columns[3].HeaderText = rm.GetString("lbAddress", ci);
            dgDebtors.Columns[4].HeaderText = rm.GetString("lbCity", ci);
            dgDebtors.Columns[11].HeaderText = rm.GetString("lbCIID", ci);
        }

        #endregion

        # region Search

        private void btnSearchDebtor_Click(object sender, EventArgs e) {
            int ciid = -1;

            try {
                ciid = int.Parse(txtDebtorID.Text);
            } catch (Exception) {
                ciid = -1;
            }

            if (rbtCompany.Checked) {
                SearchForCompany(ciid, txtNationalID.Text, txtDebtorFirstName.Text);
            } else {
                SearchForIndividual(ciid, txtNationalID.Text, txtDebtorFirstName.Text);
            }
        }

        private void btnSearchClaimOwner_Click(object sender, EventArgs e) {
            int ciid = -1;

            try {
                ciid = int.Parse(txtClaimOwnerCIID.Text.Trim());
            } catch (Exception) {
                ciid = -1;
            }

            SearchForClaimOwner(ciid, txtClaimOwnerNationalID.Text, txtClaimOwnerNative.Text);
        }

        /*	private void btnCreateDebtor_Click(object sender, EventArgs e)
		{
			if(rbtCompany.Checked)
				Response.Redirect("NewCIUserC.aspx?pageid=1");
			else
				Response.Redirect("NewCIUser.aspx?pageid=1");
		}

		private void btnCreateClaimOwner_Click(object sender, EventArgs e)
		{
			if(this.rbtnCompanyClaimOwner.Checked)
				NewClaimOwnerCompany();
			else
				NewClaimOwnerIndividual();
		}*/

        protected void SearchForIndividual(int ciid, string nationalID, string firstName) {
            tableClaimOwnerNameSearchGrid.Visible = false;
            var myDebt = new Debtor
                         {
                             CreditInfo = ciid,
                             IDNumber1 = nationalID,
                             IDNumber2Type = 1,
                             FirstName = firstName
                         };

            try {
                var ds = CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True" ? myFact.FindCustomerInNationalAndCreditInfo(myDebt) : myFact.FindCustomer(myDebt);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        //if creditinfoid
                        string sCreditInfoID = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                        if (sCreditInfoID != "") {
                            //this.tdNameSearchGrid.Visible=false;

                            tableNameSearchGrid.Visible = false;
                            txtDebtorFirstName.Text = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            //this.txtCompanyNameEN.Text=ds.Tables[0].Rows[0]["NameEN"].ToString();
                            txtDebtorID.Text = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                            txtNationalID.Text = ds.Tables[0].Rows[0]["Number"].ToString();
                        } else {
                            dgDebtors.DataSource = ds;
                            dgDebtors.DataBind();
                            tableNameSearchGrid.Visible = true;

                            divNameSearch.Style["HEIGHT"] = "auto"; //dtgrNameSearch.Height.ToString();
                            divNameSearch.Style["OVERFLOW"] = "auto";
                            divNameSearch.Style["OVERFLOW-X"] = "auto";
                        }
                    } else {
                        dgDebtors.DataSource = ds;
                        dgDebtors.DataBind();
                        tableNameSearchGrid.Visible = true;
                        //this.tdNameSearchGrid.Visible=true;

                        int gridRows = dgDebtors.Items.Count;
                        divNameSearch.Style["HEIGHT"] = gridRows < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll")) ? "auto" : "164px";
                        divNameSearch.Style["OVERFLOW"] = "auto";
                        divNameSearch.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    tableNameSearchGrid.Visible = false;
                    txtDebtorFirstName.Text = firstName;
                    txtNationalID.Text = nationalID;
                    txtDebtorID.Text = ciid == -1 ? "" : ciid.ToString();
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        protected void SearchForCompany(int ciid, string nationalID, string companyName) {
            tableClaimOwnerNameSearchGrid.Visible = false;
            var myComp = new Company {CreditInfoID = ciid, NationalID = nationalID, NameNative = companyName};

            try {
                var ds = CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True" ? myFact.FindCompanyInNationalAndCreditInfo(myComp) : myFact.FindCompany(myComp);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        tableNameSearchGrid.Visible = false;
                        txtDebtorFirstName.Text = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim() + " " +
                                                  ds.Tables[0].Rows[0]["NameEN"].ToString().Trim();
                        txtDebtorID.Text = ds.Tables[0].Rows[0]["CreditInfoID"].ToString();
                        txtNationalID.Text = ds.Tables[0].Rows[0]["Number"].ToString().Trim();
                    } else {
                        dgDebtors.DataSource = ds;
                        dgDebtors.DataBind();
                        tableNameSearchGrid.Visible = true;
                        int gridRows = dgDebtors.Items.Count;
                        divNameSearch.Style["HEIGHT"] = gridRows < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll")) ? "auto" : "164px";
                        divNameSearch.Style["OVERFLOW"] = "auto";
                        divNameSearch.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    tableNameSearchGrid.Visible = false;
                    txtDebtorFirstName.Text = companyName;
                    txtDebtorID.Text = ciid == -1 ? "" : ciid.ToString();
                    txtNationalID.Text = nationalID;
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        protected void SearchForClaimOwner(int ciid, string nationalID, string name) {
            tableNameSearchGrid.Visible = false;
            var myDebt = new Debtor {CreditInfo = ciid, IDNumber1 = nationalID, IDNumber2Type = 1, FirstName = name};

            var myComp = new Company {CreditInfoID = ciid, NationalID = nationalID, NameNative = name};

            try {
                DataSet ds;
                if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True") {
                    ds = rbtnCompanyClaimOwner.Checked ? myFact.FindCompanyInNationalAndCreditInfo(myComp) : myFact.FindCustomerInNationalAndCreditInfo(myDebt);
                } else {
                    ds = rbtnCompanyClaimOwner.Checked ? myFact.FindCompany(myComp) : myFact.FindCustomer(myDebt);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        //if creditinfoid
                        string sCreditInfoID = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                        if (sCreditInfoID != "") {
                            //this.tdNameSearchGrid.Visible=false;

                            tableClaimOwnerNameSearchGrid.Visible = false;
                            txtClaimOwnerNative.Text = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            txtClaimOwnerCIID.Text = sCreditInfoID;
                            txtClaimOwnerNationalID.Text = ds.Tables[0].Rows[0]["Number"].ToString().Trim();
                        } else {
                            dgClaimOwner.DataSource = ds;
                            dgClaimOwner.DataBind();
                            tableClaimOwnerNameSearchGrid.Visible = true;

                            divNameSearch.Style["HEIGHT"] = "auto"; //dtgrNameSearch.Height.ToString();
                            divNameSearch.Style["OVERFLOW"] = "auto";
                            divNameSearch.Style["OVERFLOW-X"] = "auto";
                        }
                    } else {
                        dgClaimOwner.DataSource = ds;
                        dgClaimOwner.DataBind();
                        tableClaimOwnerNameSearchGrid.Visible = true;

                        int gridRows = dgClaimOwner.Items.Count;
                        divClaimOwner.Style["HEIGHT"] = gridRows < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll")) ? "auto" : "164px";
                        divClaimOwner.Style["OVERFLOW"] = "auto";
                        divClaimOwner.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    tableClaimOwnerNameSearchGrid.Visible = false;
                    txtClaimOwnerNative.Text = name;
                    txtClaimOwnerNationalID.Text = nationalID;
                    txtClaimOwnerCIID.Text = ciid == -1 ? "" : ciid.ToString();
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        #endregion

        # region EventHandlers

        private void btnSubmitAndClear_Click(object sender, EventArgs e) {
            if (SaveClaim()) {
                ClearForm(true);
            }
        }

        private void btSubmit_Click(object sender, EventArgs e) {
            if (Request.Params["pageid"] != null) {
                if (Request.Params["pageid"].Equals("11")) {
                    Session["CreditInfoID"] = txtDebtorID.Text;
                }
            }
            if (SaveClaim()) {
                ClearForm(false);
            }
        }

        private void btnShowAll_Click(object sender, EventArgs e) {
            if (txtDebtorID.Text != "") {
                Session["CreditInfoID"] = txtDebtorID.Text;
                Server.Transfer("ShowAllCases.aspx");
            }
        }

        #endregion

        # region Grid methods

        private void dgDebtors_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (!e.CommandName.Equals("Select")) {
                return;
            }
            int ciid = -1;
            try {
                ciid = int.Parse(e.Item.Cells[11].Text);
            } catch (Exception) {
                ciid = -1;
            }
            if (ciid == -1) {
                //Store claim owner in session if available
                if (txtClaimOwnerCIID.Text != "") {
                    Session["txtClaimOwnerCIID"] = txtClaimOwnerCIID.Text;
                }
                if (txtClaimOwnerNative.Text != "") {
                    Session["tbClaimOwnerNative"] = txtClaimOwnerNative.Text;
                }

                if (rbtIndividual.Checked) {
                    //No ciid - then create the individual
                    string firstName = "";
                    string surName = "";
                    string name = e.Item.Cells[2].Text.Trim();
                    if (name == "&nbsp;") {
                        name = "";
                    }
                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            firstName = arrName[0];
                        } else {
                            surName = arrName[arrName.Length - 1];
                            firstName = name.Substring(0, name.Length - (surName.Length + 1));
                        }
                    }
                    Session["AddIndividual"] = true;
                    Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                    Session["FirstName"] = firstName;
                    Session["SurName"] = surName;
                    if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                        Session["StreetNative"] = e.Item.Cells[7].Text.Trim();
                    }
                    if (e.Item.Cells[8].Text.Trim() != "&nbsp;") {
                        Session["StreetEN"] = e.Item.Cells[8].Text.Trim();
                    }
                    if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                        Session["City"] = e.Item.Cells[4].Text.Trim();
                    }
                    Server.Transfer("NewCIUser.aspx?pageid=2");
                } else {
                    Session["AddCompany"] = true;
                    Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                    if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                        Session["NameNative"] = e.Item.Cells[5].Text.Trim();
                    }
                    if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                        Session["NameEN"] = e.Item.Cells[6].Text.Trim();
                    }
                    if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                        Session["StreetNative"] = e.Item.Cells[7].Text.Trim();
                    }
                    if (e.Item.Cells[8].Text.Trim() != "&nbsp;") {
                        Session["StreetEN"] = e.Item.Cells[8].Text.Trim();
                    }
                    if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                        Session["City"] = e.Item.Cells[4].Text.Trim();
                    }
                    Server.Transfer("NewCIUserC.aspx?pageid=2");
                }
            } else {
                if (rbtIndividual.Checked) {
                    SearchForIndividual(ciid, "", "");
                } else {
                    SearchForCompany(ciid, "", "");
                }
            }
        }

        private void dgDebtors_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (EN) {
                    //Set name - EN if available, else native
                    if (e.Item.Cells[6].Text != "" && e.Item.Cells[6].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[6].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    }

                    //Set street - EN if available, else native
                    if (e.Item.Cells[8].Text != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    }
                    //City
                    if (e.Item.Cells[10].Text != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[4].Text = e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[4].Text = e.Item.Cells[9].Text;
                    }
                } else {
                    //Set name - Native if available, else EN
                    if (e.Item.Cells[5].Text != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[6].Text;
                    }

                    //Set street - Native if available, else EN
                    if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[8].Text;
                    }

                    //City
                    if (e.Item.Cells[9].Text != "" && e.Item.Cells[9].Text != "&nbsp;") {
                        e.Item.Cells[4].Text = e.Item.Cells[9].Text;
                    } else {
                        e.Item.Cells[4].Text = e.Item.Cells[10].Text;
                    }
                }
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
                var grid = (DataGrid) sender;
                var label = grid.ID.Equals(dgDebtors.ID) ? lblNameSearchDatagridIcons : lblClaimOwnerNameSearchDatagridIcons;

                WebDesign.CreateExplanationIcons(grid.Columns, label, rm, ci);
            }
        }

        private void dgClaimOwner_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (!e.CommandName.Equals("Select")) {
                return;
            }
            int ciid = -1;
            if (e.Item.Cells[11].Text != "") {
                try {
                    ciid = int.Parse(e.Item.Cells[11].Text);
                } catch (Exception) {
                    ciid = -1;
                }
            }
            if (ciid == -1) {
                //Then ciid is not found - then we need to create the user - redirect the user
                //to NewCIUserC.aspx or NewCIUser.aspx

                //If company already selected - then store in session					
                if (txtNationalID.Text != "") {
                    Session["txtNationalID"] = txtNationalID.Text;
                }
                if (txtDebtorFirstName.Text != "") {
                    Session["tbDebtorFirstName"] = txtDebtorFirstName.Text;
                }
                if (txtDebtorID.Text != "") {
                    Session["tbDebtorID"] = txtDebtorID.Text;
                }

                Session["AddClaimOwner"] = true;
                if (e.Item.Cells[1].Text.Trim() != "&nbsp;") {
                    Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                }
                if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                    Session["StreetNative"] = e.Item.Cells[7].Text.Trim();
                }
                if (e.Item.Cells[8].Text.Trim() != "&nbsp;") {
                    Session["StreetEN"] = e.Item.Cells[8].Text.Trim();
                }
                if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                    Session["City"] = e.Item.Cells[4].Text.Trim();
                }
                if (e.Item.Cells[12].Text.Trim() == "1") {
                    if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                        Session["NameNative"] = e.Item.Cells[5].Text.Trim();
                    }
                    if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                        Session["NameEN"] = e.Item.Cells[6].Text.Trim();
                    }
                    Server.Transfer("NewCIUserC.aspx?pageid=2");
                } else {
                    if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                        string name = e.Item.Cells[5].Text.Trim();
                        string[] arrName = name.Split(' ');
                        if (arrName.Length > 0) {
                            if (arrName.Length == 1) {
                                Session["FirstName"] = arrName[0];
                            } else {
                                string surName = arrName[arrName.Length - 1];
                                Session["SurName"] = surName;
                                Session["FirstName"] = name.Substring(0, name.Length - (surName.Length + 1));
                            }
                        }
                    }
                    Server.Transfer("NewCIUser.aspx?pageid=2");
                }
            } else {
                txtClaimOwnerCIID.Text = ciid.ToString();
                txtClaimOwnerNative.Text = e.Item.Cells[2].Text.Trim();
            }
        }

        #endregion

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearchDebtor.Click += new System.EventHandler(this.btnSearchDebtor_Click);
            this.dgDebtors.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDebtors_ItemCommand);
            this.dgDebtors.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgDebtors_ItemDataBound);
            this.btnDebtorClear.Click += new System.EventHandler(this.btnCustomerClear_Click);
            this.btnSearchClaimOwner.Click += new System.EventHandler(this.btnSearchClaimOwner_Click);
            this.dgClaimOwner.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgClaimOwner_ItemCommand);
            this.dgClaimOwner.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgDebtors_ItemDataBound);
            this.btnClaimOwnerClear.Click += new System.EventHandler(this.btnClaimOwnerClear_Click);
            this.CustomValidator1.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.CustomValidator2.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.CustomValidator3.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
            this.btnSubmitAndClear.Click += new System.EventHandler(this.btnSubmitAndClear_Click);
            this.btSubmit.Click += new System.EventHandler(this.btSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}