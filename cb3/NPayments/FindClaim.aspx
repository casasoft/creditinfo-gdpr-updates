<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="FindClaim.aspx.cs" AutoEventWireup="false" Inherits="NPayments.FindClaim" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>FindClaim</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<script>
			function checkEnterKey() 
			{    
				if (event.keyCode == 13) 
				{        
					event.cancelBubble = true;
					event.returnValue = false;
					fclaim.btSearchDebtor.click(); 
				}
			} 
			function SetFormFocus()
				{
					document.fclaim.tbDNationalID.focus();
				}
		</script>
	</HEAD>
	<body onload="SetFormFocus()">
		<form id="fclaim" name="findclaim" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language2" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="Sitepositionbar2" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table id="list" width="100%">
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblDebtorSearch" runat="server">[Debtor search]</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td colSpan="1"><asp:radiobutton id="rbtIndividual" runat="server" checked="True" groupname="DebtorType" autopostback="True"
																			cssclass="radio"></asp:radiobutton><asp:image id="imgIndividual" runat="server" imageurl="../img/individual.gif"></asp:image>&nbsp;
																		<asp:radiobutton id="rbtCompany" runat="server" groupname="DebtorType" autopostback="True" cssclass="radio"></asp:radiobutton><asp:image id="imgCompany" runat="server" imageurl="../img/company.gif"></asp:image></td>
                                                                    
																    <td><asp:label id="lblpep" runat="server">Show Only PEP Records</asp:label><br>
																        <asp:checkbox id=chkShowPEP runat="server" cssclass="radio" ></asp:checkbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lbDNatNum" runat="server"> Debtor id</asp:label><br>
																		<asp:textbox id="tbDNationalID" runat="server"></asp:textbox><asp:textbox id="tbDCIIDNumber" runat="server" Width="32px" Visible="False"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblFirstName" runat="server">Firstname</asp:label><br>
																		<asp:textbox id="txtFirstName" runat="server"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblSurName" runat="server">SurName</asp:label><br>
																		<asp:textbox id="txtSurName" runat="server"></asp:textbox></td>
																	<td align="right"><asp:button id="btnDebtorClear" runat="server" cssclass="cancel_button" CausesValidation="False"
																			text="Clear"></asp:button>&nbsp;<asp:button id="btnSearchDebtor" runat="server" cssclass="search_button" text="Search"></asp:button></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr id="trDebtorGrid" runat="server">
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblDatagridIcons2" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblDebtorGridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<div class="datagrid" id="divNameSearch" style="OVERFLOW-X: auto; OVERFLOW: auto; HEIGHT: 164px"
																runat="server"><asp:datagrid id="dgDebtors" runat="server" cssclass="grid" width="100%" forecolor="Black" allowsorting="True"
																	gridlines="None" backcolor="White" borderstyle="None" bordercolor="#DEDFDE" autogeneratecolumns="False">
																	<footerstyle cssclass="grid_footer"></footerstyle>
																	<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																	<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																	<itemstyle cssclass="grid_item"></itemstyle>
																	<headerstyle cssclass="grid_header"></headerstyle>
																	<pagerstyle cssclass="grid_pager"></pagerstyle>
																	<columns>
																		<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																			commandname="Select">
																			<itemstyle cssclass="leftpadding"></itemstyle>
																		</asp:buttoncolumn>
																		<asp:boundcolumn datafield="Number" sortexpression="Number" headertext="National ID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Name">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Street">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameNative" sortexpression="NameNative" headertext="NameNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameEN" sortexpression="NameEN" headertext="Name EN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetNative" sortexpression="StreetNative" headertext="Street(N)">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetEN" sortexpression="StreetEN" headertext="Street EN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn datafield="CreditInfoID" sortexpression="CreditInfoID" headertext="CreditInfoID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="CityNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="CityEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																	</columns>
																</asp:datagrid></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblClaimOwnerSearch" runat="server">[ClaimOwner search]</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td colSpan="4"><asp:radiobutton id="rbtnIndividualClaimOwner" runat="server" checked="True" groupname="ClaimantType"
																			cssclass="radio" AutoPostBack="True"></asp:radiobutton><asp:image id="imgIndividualClaimOwner" runat="server" imageurl="../img/individual.gif"></asp:image>&nbsp;
																		<asp:radiobutton id="rbtnCompanyClaimOwner" runat="server" groupname="ClaimantType" cssclass="radio"
																			AutoPostBack="True"></asp:radiobutton><asp:image id="imgCompanyClaimOwner" runat="server" imageurl="../img/company.gif"></asp:image></td>
																</tr>
																<tr>
																	<td style="WIDTH: 25%"><br>
																		<asp:label id="lblClaimOwnerNationalID" runat="server">Claim owner id</asp:label><br>
																		<asp:textbox id="txtClaimOwnerNationalID" runat="server"></asp:textbox><asp:textbox id="txtClaimOwnerCIID" runat="server" Width="39px" Visible="False"></asp:textbox></td>
																	<td style="WIDTH: 25%"><br>
																		<asp:label id="lbClaimOwner" runat="server"> ClaimOwner</asp:label><br>
																		<asp:textbox id="txtClaimOwner" runat="server"></asp:textbox></td>
																	<td style="WIDTH: 25%"><br>
																		<asp:label id="lblClaimOwnerSurname" runat="server"> ClaimOwner surname</asp:label><br>
																		<asp:textbox id="txtClaimOwnerSurname" runat="server"></asp:textbox></td>
																	<td align="right"><asp:button id="btnClaimOwnerClear" runat="server" cssclass="cancel_button" CausesValidation="False"
																			text="Clear"></asp:button>&nbsp;<asp:button id="btnSearchClaim" runat="server" cssclass="search_button" text="Search"></asp:button></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr id="trClaimOwnerGrid" runat="server">
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblClaimOwnerGridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<div class="datagrid" id="divClaimOwner" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																runat="server"><asp:datagrid id="dgClaimOwner" runat="server" cssclass="grid" width="100%" forecolor="Black"
																	allowsorting="True" gridlines="None" backcolor="White" borderstyle="None" bordercolor="#DEDFDE" autogeneratecolumns="False">
																	<footerstyle cssclass="grid_footer"></footerstyle>
																	<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																	<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																	<itemstyle cssclass="grid_item"></itemstyle>
																	<headerstyle cssclass="grid_header"></headerstyle>
																	<pagerstyle cssclass="grid_pager"></pagerstyle>
																	<columns>
																		<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																			commandname="Select">
																			<itemstyle cssclass="leftpadding"></itemstyle>
																		</asp:buttoncolumn>
																		<asp:boundcolumn datafield="Number" sortexpression="Number" headertext="National ID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Name">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Street">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameNative" sortexpression="NameNative" headertext="NameNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameEN" sortexpression="NameEN" headertext="NameEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetNative" sortexpression="StreetNative" headertext="StreetNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetEN" sortexpression="StreetEN" headertext="StreetEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn datafield="CreditInfoID" sortexpression="CreditInfoID" headertext="CreditInfoID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="IsCompany" headertext="IsCompany">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="CityNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="CityEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																	</columns>
																</asp:datagrid></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lbClaimSearch" runat="server">Claim search</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td><asp:label id="lbClaimID" runat="server">Claim ID</asp:label><br>
																		<asp:textbox id="tbClaimID" runat="server" maxlength="12"></asp:textbox></td>
																	<td><asp:label id="lbRegDateFrom" runat="server">Registed date (from)</asp:label><br>
																		<asp:textbox id="tbRegDateFrom" runat="server"></asp:textbox><INPUT class="popup" onclick="PopupPicker('tbRegDateFrom', 250, 250);" type="button" value="...">
																		<asp:customvalidator id="CustomValidator1" runat="server" errormessage="Wrong Date input!" controltovalidate="tbRegDateFrom">*</asp:customvalidator><br>
																		&nbsp;</td>
																	<td colSpan="2"><asp:label id="lbRegDateTo" runat="server">Registed date (to)</asp:label><br>
																		<asp:textbox id="tbRegDateTo" runat="server"></asp:textbox><INPUT class="popup" onclick="PopupPicker('tbRegDateTo', 250, 250);" type="button" value="...">
																		<asp:customvalidator id="CustomValidator2" runat="server" errormessage="Wrong Date input!" controltovalidate="tbRegDateTo">*</asp:customvalidator><br>
																	</td>
																	<td><asp:label id="lbCaseNumber" runat="server">Case Number</asp:label><br>
																		<asp:textbox id="tbCourtCaseNumber" runat="server"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lbState" runat="server">State</asp:label><br>
																		<asp:dropdownlist id="ddState" runat="server"></asp:dropdownlist></td>
																	<td><asp:label id="lbCourt" runat="server">Court</asp:label><br>
																		<asp:dropdownlist id="ddInformationSource" runat="server"></asp:dropdownlist></td>
																	<td colSpan="2"><asp:label id="lbCaseType" runat="server">Case type</asp:label><br>
																		<asp:dropdownlist id="ddCaseType" runat="server"></asp:dropdownlist></td>
																	<td align="right"><asp:button id="btnFindClaimClear" runat="server" cssclass="cancel_button" CausesValidation="False"
																			text="Clear"></asp:button>&nbsp;<asp:button id="btSearchDebtor" runat="server" cssclass="search_button" text="Search"></asp:button></td>
																</tr>
																<tr>
																	<td height="23"></td>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr id="trClaimSearch" runat="server">
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblDatagridIcons3" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblDgResults3" runat="server"></asp:label></th></tr>
													<tr>
														<td id="Td5" width="100%" runat="server">
															<div class="datagrid" id="divClaims" runat="server"><asp:datagrid id="dgClaim" runat="server" forecolor="Black" allowsorting="True" gridlines="None"
																	backcolor="White" borderstyle="None" bordercolor="#DEDFDE" autogeneratecolumns="False" cellpadding="4" borderwidth="1px" pagesize="20">
																	<FooterStyle CssClass="grid_footer"></FooterStyle>
																	<SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
																	<AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
																	<ItemStyle CssClass="grid_item"></ItemStyle>
																	<HeaderStyle CssClass="grid_header"></HeaderStyle>
																	<Columns>
																		<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																			CommandName="Select">
																			<ItemStyle CssClass="leftpadding"></ItemStyle>
																		</asp:ButtonColumn>
																		<asp:BoundColumn DataField="CreditInfoID" SortExpression="CreditInfoID" HeaderText="CreditInfoID">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn HeaderText="Deb.Name">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="Claim ID">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="RegDate" SortExpression="RegDate" HeaderText="Registed">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="Initials" SortExpression="Initials" HeaderText="Employee">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn HeaderText="Owner">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="ClaimOwnerCIID" HeaderText="Owner (EN)">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="RegisterCommentNative" SortExpression="RegisterCommentNative"
																			HeaderText="Comment(N)">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="RegisterCommentEN" SortExpression="RegisterCommentEN"
																			HeaderText="Comment (E)">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="NameNative" SortExpression="NameNative" HeaderText="Source(N)">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="NameEN" SortExpression="NameEN" HeaderText="Source (E)">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="StateNative" SortExpression="StateNative" HeaderText="State(N)">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="StateEN" SortExpression="StateEN" HeaderText="State (E)">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn DataField="ClaimDate" SortExpression="ClaimDate" HeaderText="Claim Date" DataFormatString="{0:d}">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="CaseID" SortExpression="CaseID" HeaderText="CaseID">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="StatusID" HeaderText="StatusID">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="CBA" HeaderText="CBA">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn HeaderText="Type">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="TypeNative" SortExpression="TypeNative" HeaderText="Type(N)">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="TypeEN" SortExpression="TypeEN" HeaderText="Type(E)">
																			<ItemStyle CssClass="padding"></ItemStyle>
																		</asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="DebtorUserType" HeaderText="DebtorUserType"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="ClaimantUserType" HeaderText="ClaimantUserType"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="DebtorFirstNameNative"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="DebtorSurNameNative"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="DebtorFirstNameEN"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="DebtorSurNameEN"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="DebtorCompanyNameNative"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="DebtorCompanyNameEN"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="ClaimOwnerFirstNameNative"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="ClaimOwnerSurNameNative"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="ClaimOwnerFirstNameEN"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="ClaimOwnerSurNameEN"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="ClaimOwnerCompanyNameNative"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="ClaimOwnerCompanyNameEN"></asp:BoundColumn>
																	</Columns>
																	<PagerStyle CssClass="grid_pager"></PagerStyle>
																</asp:datagrid></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table width="100%">
													<tr>
														<td><asp:validationsummary id="ValidationSummary1" runat="server" width="288px"></asp:validationsummary><asp:label id="lblMsg" runat="server" forecolor="Red" visible="False">Label</asp:label></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lbColorDefinitions" runat="server">Color Definitions</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td style="WIDTH: 25%" align="center" bgColor="#cfcfcf"><asp:label id="lbLetterColor" runat="server">Letter</asp:label></td>
																	<td style="WIDTH: 25%" align="center" bgColor="#cdc673"><asp:label id="lbColorRegisteret" runat="server">Registrated</asp:label></td>
																	<td style="WIDTH: 25%" align="center" bgColor="#cd661d"><asp:label id="lbColorCancelled" runat="server">Cancelled - De-registered</asp:label></td>
																	<td align="center" bgColor="#9ac0cd"><asp:label id="lbColorBouchedCBA" runat="server">Bounched cheque with CBA</asp:label></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
