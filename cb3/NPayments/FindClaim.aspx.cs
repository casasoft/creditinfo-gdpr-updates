#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Claims;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;
using Debtor=UserAdmin.BLL.Debtor;
using Image=System.Web.UI.WebControls.Image;

#endregion

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for FindClaim.
    /// </summary>
    public class FindClaim : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly NPaymentsFactory myFact = new NPaymentsFactory();
        protected Button btCancel;
        protected Button btnClaimOwnerClear;
        protected Button btnDebtorClear;
        protected Button btnFindClaimClear;
        protected Button btnSearchClaim;
        protected Button btnSearchDebtor;
        protected Button btSearchDebtor;
        protected CustomValidator CustomValidator1;
        protected CustomValidator CustomValidator2;
        protected DropDownList ddCaseType;
        protected DropDownList ddInformationSource;
        protected DropDownList ddState;
        protected DataGrid dgClaim;
        protected DataGrid dgClaimOwner;
        protected DataGrid dgDebtors;
        protected HtmlGenericControl divClaimOwner;
        protected HtmlGenericControl divClaims;
        protected HtmlGenericControl divNameSearch;
        private bool EN;
        protected HtmlForm fclaim;
        protected Image imgCompany;
        protected Image imgCompanyClaimOwner;
        protected Image imgIndividual;
        protected Image imgIndividualClaimOwner;
        protected Label lbCaseNumber;
        protected Label lbCaseType;
        protected Label lbClaimID;
        protected Label lbClaimOwner;
        protected Label lbClaimSearch;
        protected Label lbColorBouchedCBA;
        protected Label lbColorCancelled;
        protected Label lbColorDefinitions;
        protected Label lbColorRegisteret;
        protected Label lbCourt;
        protected Label lbDNatNum;
        protected Label lblClaimOwnerGridHeader;
        protected Label lblClaimOwnerNationalID;
        protected Label lblClaimOwnerSearch;
        protected Label lblClaimOwnerSurname;
        protected Label lblDatagridIcons;
        protected Label lblDatagridIcons2;
        protected Label lblDatagridIcons3;
        protected Label lblDebtorGridHeader;
        protected Label lblDebtorSearch;
        protected Label lblDgResults3;
        protected Label lbLetterColor;
        protected Label lblFirstName;
        protected Label lblMsg;
        protected Label lblSurName;
        protected Label lbRegDateFrom;
        protected Label lbRegDateTo;
        protected Label lbState;
        protected RadioButton rbtCompany;
        protected RadioButton rbtIndividual;
        protected RadioButton rbtnCompanyClaimOwner;
        protected RadioButton rbtnIndividualClaimOwner;
        protected HtmlTable SearchGrid;
        protected HtmlTable Table1;
        protected HtmlTable Table2;
        protected TextBox tbClaimID;
        protected TextBox tbCourtCaseNumber;
        protected TextBox tbDCIIDNumber;
        protected TextBox tbDNationalID;
        protected TextBox tbRegDateFrom;
        protected TextBox tbRegDateTo;
        protected HtmlTableCell Td2;
        protected HtmlTableCell Td3;
        protected HtmlTableCell Td4;
        protected HtmlTableCell Td5;
        protected HtmlTableCell tdNameSearchGrid;
        protected HtmlTableRow trClaimOwnerGrid;
        protected HtmlTableRow trClaimSearch;
        protected HtmlTableRow trDebtorGrid;
        protected TextBox txtClaimOwner;
        protected TextBox txtClaimOwnerCIID;
        protected TextBox txtClaimOwnerNationalID;
        protected TextBox txtClaimOwnerSurname;
        protected TextBox txtFirstName;
        protected TextBox txtSurName;
        protected ValidationSummary ValidationSummary1;
        protected CheckBox chkShowPEP;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "204";
            lblMsg.Visible = false;
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            trClaimSearch.Visible = false;

            int creditinfoID = 0;
            if (Session["CreditInfoID"] != null) {
                string redPage = Request.QueryString["URL"];
                if (redPage != null) {
                    creditinfoID = (int) Session["CreditInfoID"];
                    if (string.IsNullOrEmpty(tbDCIIDNumber.Text)) {
                        tbDCIIDNumber.Text = Convert.ToString(creditinfoID.ToString());
                    }
                }
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            // Add <ENTER> event to text controls
            AddEnterEvent();
            SetColumnsByCulture();
            SetNameSearchColumnsByCulture();

            if (!IsPostBack) {
                SetClaimOwnerSearchColumnsHeaderByCulture();
                SetNameSearchColumnsHeaderByCulture();
                LocalizeText();
                trClaimOwnerGrid.Visible = false;
                trDebtorGrid.Visible = false;
                InitDDLists();
            }
        }

        private void InitDDLists() {
            ddCaseType.DataSource = myFact.GetCaseTypesAsDataSet();
            ddCaseType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddCaseType.DataValueField = "CaseTypeID";
            ddCaseType.DataBind();
            if (EN) {
                ddCaseType.Items.Insert(0, "Any type");
            } else {
                ddCaseType.Items.Insert(0, "Any type(N)"); // need translation...
            }

            ddState.DataSource = myFact.GetStatusListAsDataSet();
            ddState.DataTextField = EN ? "StateEN" : "StateNative";
            ddState.DataValueField = "StatusID";
            ddState.DataBind();
            if (EN) {
                ddState.Items.Insert(0, "Any state");
            } else {
                ddState.Items.Insert(0, "Any state(N)"); // need translation...
            }

            ddInformationSource.DataSource = myFact.GetInformationSourceListAsDataSet();
            ddInformationSource.DataTextField = EN ? "NameEN" : "NameNative";
            ddInformationSource.DataValueField = "InformationSourceID";
            ddInformationSource.DataBind();
            if (EN) {
                ddInformationSource.Items.Insert(0, "Any source");
            } else {
                ddInformationSource.Items.Insert(0, "Any source(N)"); // need translation...
            }
        }

        private void btSearchDebtor_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {} else {
                var myFact1 = new NPaymentsFactory();
                dgClaim.CurrentPageIndex = 0;
                var myClaim = new Claim();
                var query = false;

                try {
                    if (tbDCIIDNumber.Text != "" && tbDCIIDNumber != null) {
                        myClaim.CreditInfoID = int.Parse(tbDCIIDNumber.Text);
                        query = true;
                    }
                    if (tbClaimID.Text != "" && tbClaimID != null) {
                        myClaim.ClaimID = int.Parse(tbClaimID.Text);
                        query = true;
                    }
                    if (tbRegDateFrom.Text != "" && tbRegDateFrom != null) {
                        myClaim.SearchFromDate = DateTime.Parse(tbRegDateFrom.Text);
                        query = true;
                    }
                    if (tbRegDateTo.Text != "" && tbRegDateTo != null) {
                        myClaim.SearchToDate = DateTime.Parse(tbRegDateTo.Text);
                        query = true;
                    }
                    if (!string.IsNullOrEmpty(txtClaimOwnerCIID.Text)) {
                        myClaim.ClaimOwnerCIID = int.Parse(txtClaimOwnerCIID.Text);
                        query = true;
                    }
                    if (!string.IsNullOrEmpty(tbCourtCaseNumber.Text)) {
                        myClaim.CaseNumber = tbCourtCaseNumber.Text;
                        query = true;
                    }
                    if (ddCaseType.SelectedIndex > 0) {
                        myClaim.ClaimTypeID = int.Parse(ddCaseType.SelectedItem.Value);
                    }
                    if (ddState.SelectedIndex > 0) {
                        myClaim.StatusID = int.Parse(ddState.SelectedItem.Value);
                    }
                    if ((myClaim.ClaimTypeID != 0) || (myClaim.StatusID != 0)) {
                        query = true;
                    }
                    if (ddInformationSource.SelectedIndex > 0) {
                        myClaim.InformationSourceID = Convert.ToInt32(ddInformationSource.SelectedValue);
                        query = true;
                    }

                    if (!query) {
                        DisplayErrorMessage(rm.GetString("errMsg3", ci));
                    } else {
                        DataSet claimSet = myFact1.FindClaim(myClaim);
                        if (claimSet.Tables.Count > 0) {
                            dgClaim.DataSource = claimSet.Tables[0].DefaultView;
                            SetColumnsHeaderByCulture();
                            dgClaim.DataBind();
                            Session["dsClaimSet"] = claimSet.Tables[0];

                            trClaimSearch.Visible = true;
                        }
                    }
                } catch (ThreadAbortException) {
                    //bug?? happens randomly when Server.Transfer is invoked
                } catch (Exception err) {
                    Logger.WriteToLog("Exception on FindClaim.aspx caught, message is : " + err.Message, true);
                    DisplayErrorMessage(rm.GetString("errMsg2", ci));
                    return;
                }
            }
        }

        private void DisplayErrorMessage(string errorMessage) {
            lblMsg.Text = errorMessage;
            lblMsg.ForeColor = Color.Red;
            lblMsg.Visible = true;
        }

        private void DisplayMessage(string message) {
            lblMsg.Text = message;
            lblMsg.ForeColor = Color.Blue;
            lblMsg.Visible = true;
        }

        public void DateValidate(object source, ServerValidateEventArgs value) {
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        private void dgClaim_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName == "Select") {
                int creditInfoID = -1;
                int claimID = -1;
                int caseTypeID = 0;

                try {
                    creditInfoID = int.Parse(e.Item.Cells[1].Text);
                    claimID = int.Parse(e.Item.Cells[3].Text);
                } catch (Exception err) {
                    Logger.WriteToLog(
                        "FindClaim.aspx + dgClaim_ItemCommand,  creditInfoID = int.Parse(e.Item.Cells[1].Text) CaseID = " +
                        creditInfoID + " :: " + err.Message,
                        true);
                    Logger.WriteToLog(
                        "FindClaim.aspx + dgClaim_ItemCommand,  claimID = int.Parse(e.Item.Cells[3].Text) claimID = " +
                        claimID + " :: " + err.Message,
                        true);
                }
                try {
                    caseTypeID = int.Parse(e.Item.Cells[15].Text);
                } catch (Exception err) {
                    Logger.WriteToLog(
                        "FindClaim.aspx + dgClaim_ItemCommand,  caseTypeID = int.Parse(e.Item.Cells[14].Text) CaseID = " +
                        claimID + " :: " + err.Message,
                        true);
                }

                Session["CreditInfoID"] = creditInfoID;
                Session["ClaimID"] = claimID;
                try {
                    Server.Transfer(
                        CigConfig.Configure("lookupsettings.redirectpageID" + caseTypeID) + "?update=true&pageid=12");
                } catch (ThreadAbortException) {
                    //bug?? happens randomly when Server.Transfer is invoked
                } catch (Exception err) {
                    Logger.WriteToLog(
                        "FindClaim.aspx + dgClaim_ItemCommand,  Server.Transfer(CigConfig.Configure(\"redirectpageID\"+caseTypeID]+ \"?update=true\")" +
                        " :: " + err.Message,
                        true);
                }
            }
        }

        private void LocalizeText() {
            btSearchDebtor.Text = rm.GetString("txtSearch", ci);
            btnSearchClaim.Text = rm.GetString("txtSearch", ci);
            btnSearchDebtor.Text = rm.GetString("txtSearch", ci);

            CustomValidator1.ErrorMessage = rm.GetString("CustomValidator1", ci);
            CustomValidator2.ErrorMessage = rm.GetString("CustomValidator2", ci);

            lbClaimSearch.Text = rm.GetString("lbClaimSearch", ci);
            lbDNatNum.Text = rm.GetString("lbNationalID", ci);
            lbCaseType.Text = rm.GetString("lbType", ci);
            lbRegDateFrom.Text = rm.GetString("lbRegDateFrom", ci);
            lbRegDateTo.Text = rm.GetString("lbRegDateTo", ci);
            lbState.Text = rm.GetString("txtState", ci);
            lbClaimID.Text = rm.GetString("lbClaimID", ci);
            lbClaimOwner.Text = rm.GetString("txtFirstname", ci);
            lblClaimOwnerSurname.Text = rm.GetString("txtSurname", ci);
            lbCourt.Text = rm.GetString("txtInformationSource", ci);
            lbCaseNumber.Text = rm.GetString("txtCaseNumber", ci);
            lbColorDefinitions.Text = rm.GetString("txtColorDefinitions", ci);
            lbLetterColor.Text = rm.GetString("txtLetter", ci);
            lbColorRegisteret.Text = rm.GetString("txtRegistered", ci);
            lbColorCancelled.Text = rm.GetString("txtCancelledDeRegistered", ci);
            lbColorBouchedCBA.Text = rm.GetString("txtBounchedChequeWithCBA", ci);
            lblFirstName.Text = rm.GetString("txtFirstname", ci);
            lblSurName.Text = rm.GetString("txtSurname", ci);
            lblClaimOwnerNationalID.Text = rm.GetString("lbNationalID", ci);

            lblDebtorSearch.Text = rm.GetString("txtDebtorSearch", ci);
            lblDebtorGridHeader.Text = rm.GetString("txtDebtorResults", ci);
            lblClaimOwnerSearch.Text = rm.GetString("txtClaimantSearch", ci);
            lblClaimOwnerGridHeader.Text = rm.GetString("txtClaimantResults", ci);
            lblDgResults3.Text = rm.GetString("lblDgResults", ci);

            imgIndividual.AlternateText = rm.GetString("txtIndividual", ci);
            imgCompany.AlternateText = rm.GetString("txtCompany", ci);
            imgCompanyClaimOwner.AlternateText = rm.GetString("txtCompany", ci);
            imgIndividualClaimOwner.AlternateText = rm.GetString("txtIndividual", ci);
        }

        private void SetColumnsByCulture() {
            if (EN) {
                dgClaim.Columns[0].Visible = true;
                dgClaim.Columns[1].Visible = true;
                dgClaim.Columns[2].Visible = true;
                dgClaim.Columns[3].Visible = true;
                dgClaim.Columns[4].Visible = false;
                dgClaim.Columns[5].Visible = true;
                dgClaim.Columns[6].Visible = true;
                dgClaim.Columns[7].Visible = false;
                //this.dgClaim.Columns[8].Visible = false;
                //this.dgClaim.Columns[9].Visible = true;
                dgClaim.Columns[10].Visible = false;
                dgClaim.Columns[11].Visible = true;
                dgClaim.Columns[12].Visible = false;
                dgClaim.Columns[13].Visible = true;
            } else {
                dgClaim.Columns[0].Visible = true;
                dgClaim.Columns[1].Visible = true;
                dgClaim.Columns[2].Visible = true;
                dgClaim.Columns[3].Visible = true;
                dgClaim.Columns[4].Visible = true;
                dgClaim.Columns[5].Visible = false;
                dgClaim.Columns[6].Visible = true;
                dgClaim.Columns[7].Visible = false;
                //this.dgClaim.Columns[8].Visible = true;
                //this.dgClaim.Columns[9].Visible = false;
                dgClaim.Columns[10].Visible = true;
                dgClaim.Columns[11].Visible = false;
                dgClaim.Columns[12].Visible = true;
                dgClaim.Columns[13].Visible = false;
            }
        }

        private void SetColumnsHeaderByCulture() {
            dgClaim.Columns[1].HeaderText = rm.GetString("lbDebtorID", ci);
            dgClaim.Columns[2].HeaderText = rm.GetString("lbName", ci);
            dgClaim.Columns[3].HeaderText = rm.GetString("lbClaim", ci);
            dgClaim.Columns[4].HeaderText = rm.GetString("txtRegistration", ci);
            dgClaim.Columns[5].HeaderText = rm.GetString("txtEmployee", ci);
            dgClaim.Columns[6].HeaderText = rm.GetString("lbClaimOwnerName", ci);
            //this.dgClaim.Columns[8].HeaderText = rm.GetString("lbComment",ci);
            //this.dgClaim.Columns[9].HeaderText = rm.GetString("lbComment",ci);
            dgClaim.Columns[10].HeaderText = rm.GetString("lbInfoOrigin", ci);
            dgClaim.Columns[11].HeaderText = rm.GetString("lbInfoOrigin", ci);
            dgClaim.Columns[12].HeaderText = rm.GetString("lbStatus", ci);
            dgClaim.Columns[13].HeaderText = rm.GetString("lbStatus", ci);
            dgClaim.Columns[14].HeaderText = rm.GetString("lbClaimDate", ci);
            dgClaim.Columns[15].HeaderText = rm.GetString("lbCaseID", ci);
        }

        private void AddEnterEvent() {
            Control frm = FindControl("fclaim");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void dgClaim_SortCommand(object source, DataGridSortCommandEventArgs e) {
            var myTable = (DataTable) Session["dsClaimSet"];
            var myView = myTable.DefaultView;

            myView.Sort = e.SortExpression;
            dgClaim.DataSource = myView;
            dgClaim.DataBind();
            trClaimSearch.Visible = true;
        }

        private void dgClaim_PageIndexChanged(object source, DataGridPageChangedEventArgs e) {
            dgClaim.CurrentPageIndex = e.NewPageIndex;

            var myTable = (DataTable) Session["dsClaimSet"];
            var myView = myTable.DefaultView;

            dgClaim.DataSource = myView;
            dgClaim.DataBind();
        }

        private void dgClaim_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (e.Item.Cells[16].Text == "2") {
                    e.Item.BackColor = Color.FromName("#cfcfcf");
                } else if (e.Item.Cells[16].Text == "4") {
                    e.Item.BackColor = Color.FromName("#cdc673");
                } else if (e.Item.Cells[16].Text == "6") {
                    e.Item.BackColor = Color.FromName("#cd661d");
                }
                if (e.Item.Cells[17].Text == "True") {
                    for (int i = 0; i < 5; i++) {
                        e.Item.Cells[i].BackColor = Color.FromName("#9ac0cd");
                    }
                }
                if (e.Item.Cells[21].Text.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                    if (EN) {
                        if (e.Item.Cells[25].Text != "" && e.Item.Cells[25].Text != "&nbsp;") {
                            e.Item.Cells[2].Text = e.Item.Cells[25].Text;
                            if (e.Item.Cells[26].Text != "" && e.Item.Cells[26].Text != "&nbsp;") {
                                e.Item.Cells[2].Text += " " + e.Item.Cells[26].Text;
                            }
                        } else {
                            e.Item.Cells[2].Text = e.Item.Cells[23].Text;
                            if (e.Item.Cells[24].Text != "" && e.Item.Cells[24].Text != "&nbsp;") {
                                e.Item.Cells[2].Text += " " + e.Item.Cells[24].Text;
                            }
                        }
                    } else {
                        if (e.Item.Cells[23].Text != "" && e.Item.Cells[23].Text != "&nbsp;") {
                            e.Item.Cells[2].Text = e.Item.Cells[23].Text;
                            if (e.Item.Cells[24].Text != "" && e.Item.Cells[24].Text != "&nbsp;") {
                                e.Item.Cells[2].Text += " " + e.Item.Cells[24].Text;
                            }
                        } else {
                            e.Item.Cells[2].Text = e.Item.Cells[25].Text;
                            if (e.Item.Cells[26].Text != "" && e.Item.Cells[26].Text != "&nbsp;") {
                                e.Item.Cells[2].Text += " " + e.Item.Cells[26].Text;
                            }
                        }
                    }
                } else {
                    if (EN) {
                        if (e.Item.Cells[28].Text != "" && e.Item.Cells[28].Text != "&nbsp;") {
                            e.Item.Cells[2].Text = e.Item.Cells[28].Text;
                        } else {
                            e.Item.Cells[2].Text = e.Item.Cells[27].Text;
                        }
                    } else {
                        if (e.Item.Cells[27].Text != "" && e.Item.Cells[27].Text != "&nbsp;") {
                            e.Item.Cells[2].Text = e.Item.Cells[27].Text;
                        } else {
                            e.Item.Cells[2].Text = e.Item.Cells[28].Text;
                        }
                    }
                }

                if (e.Item.Cells[22].Text.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                    if (EN) {
                        if (e.Item.Cells[31].Text != "" && e.Item.Cells[31].Text != "&nbsp;") {
                            e.Item.Cells[6].Text = e.Item.Cells[31].Text;
                            if (e.Item.Cells[32].Text != "" && e.Item.Cells[32].Text != "&nbsp;") {
                                e.Item.Cells[6].Text += " " + e.Item.Cells[32].Text;
                            }
                        } else {
                            e.Item.Cells[6].Text = e.Item.Cells[29].Text;
                            if (e.Item.Cells[30].Text != "" && e.Item.Cells[30].Text != "&nbsp;") {
                                e.Item.Cells[6].Text += " " + e.Item.Cells[30].Text;
                            }
                        }
                    } else {
                        if (e.Item.Cells[29].Text != "" && e.Item.Cells[29].Text != "&nbsp;") {
                            e.Item.Cells[6].Text = e.Item.Cells[29].Text;
                            if (e.Item.Cells[30].Text != "" && e.Item.Cells[30].Text != "&nbsp;") {
                                e.Item.Cells[6].Text += " " + e.Item.Cells[30].Text;
                            }
                        } else {
                            e.Item.Cells[6].Text = e.Item.Cells[31].Text;
                            if (e.Item.Cells[32].Text != "" && e.Item.Cells[32].Text != "&nbsp;") {
                                e.Item.Cells[6].Text += " " + e.Item.Cells[32].Text;
                            }
                        }
                    }
                } else {
                    if (EN) {
                        if (e.Item.Cells[34].Text != "" && e.Item.Cells[34].Text != "&nbsp;") {
                            e.Item.Cells[6].Text = e.Item.Cells[34].Text;
                        } else {
                            e.Item.Cells[6].Text = e.Item.Cells[33].Text;
                        }
                    } else {
                        if (e.Item.Cells[33].Text != "" && e.Item.Cells[33].Text != "&nbsp;") {
                            e.Item.Cells[6].Text = e.Item.Cells[33].Text;
                        } else {
                            e.Item.Cells[6].Text = e.Item.Cells[34].Text;
                        }
                    }
                }

                if (EN) {
                    //Set TYPE - EN if available, else native
                    if (e.Item.Cells[20].Text != "" && e.Item.Cells[20].Text != "&nbsp;") {
                        e.Item.Cells[18].Text = e.Item.Cells[20].Text;
                    } else {
                        e.Item.Cells[18].Text = e.Item.Cells[19].Text;
                    }
                } else {
                    //Set TYPE - Native if available, else EN
                    if (e.Item.Cells[19].Text != "" && e.Item.Cells[19].Text != "&nbsp;") {
                        e.Item.Cells[18].Text = e.Item.Cells[19].Text;
                    } else {
                        e.Item.Cells[18].Text = e.Item.Cells[20].Text;
                    }
                }
            }

            WebDesign.CreateExplanationIcons(dgClaim.Columns, lblDatagridIcons3, rm, ci);
        }

        /*/// <summary>
		/// Get debtors name for each case in caseOverview. Quick and dirty solution though...
		/// </summary>
		private string GetDebtorName(int creditInfoID)
		{
			string type = myFact.GetCIUserType(creditInfoID);
			string debtorName = "";
			if(type.Trim().Equals("-1")) // no type found
				type = CigConfig.Configure("lookupsettings.individualID"); // then set to individual as default
			if(type.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) 
			{
				Indivitual myIndi = myFact.GetIndivitual(creditInfoID);
				if(EN) 
				{
					if(myIndi.FirstNameEN!=null&&myIndi.FirstNameEN.Trim()!="")
					{
						debtorName = myIndi.FirstNameEN;
						debtorName += " " + myIndi.SurNameEN;
					}
					else
					{
						debtorName = myIndi.FirstNameNative;
						debtorName += " " + myIndi.SurNameNative;
					}
				} 
				else  
				{
					if(myIndi.FirstNameNative!=null&&myIndi.FirstNameNative.Trim()!="")
					{
						debtorName = myIndi.FirstNameNative;
						debtorName += " " + myIndi.SurNameNative;
					}
					else
					{
						debtorName = myIndi.FirstNameEN;
						debtorName += " " + myIndi.SurNameEN;
					}
				}

			}
			else if(type.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) 
			{ // company
				Company myComp = myFact.GetCompany(creditInfoID);
				if(EN)
				{
					if(myComp.NameEN!=null&&myComp.NameEN.Trim() != "")
						debtorName = myComp.NameEN;
					else
						debtorName = myComp.NameNative;
				} 
				else 
				{
					if(myComp.NameNative!=null&&myComp.NameNative.Trim()!="")
						debtorName = myComp.NameNative;
					else
						debtorName = myComp.NameEN;
				}
			}
			return debtorName;
		}*/

        private void SetNameSearchColumnsByCulture() {
            dgDebtors.Columns[0].Visible = true;
            dgDebtors.Columns[1].Visible = true;
            dgDebtors.Columns[2].Visible = true;
            dgDebtors.Columns[3].Visible = true;
            dgDebtors.Columns[4].Visible = false;
            dgDebtors.Columns[5].Visible = false;
            dgDebtors.Columns[6].Visible = false;
            dgDebtors.Columns[7].Visible = false;
        }

        private void SetNameSearchColumnsHeaderByCulture() {
            dgDebtors.Columns[1].HeaderText = rm.GetString("lbNationalID", ci);
            dgDebtors.Columns[2].HeaderText = rm.GetString("lbName", ci);
            dgDebtors.Columns[3].HeaderText = rm.GetString("lbAddress", ci);
            dgDebtors.Columns[8].HeaderText = rm.GetString("lbCIID", ci);
        }

        private void SetClaimOwnerSearchColumnsHeaderByCulture() {
            dgClaimOwner.Columns[1].HeaderText = rm.GetString("lbNationalID", ci);
            dgClaimOwner.Columns[2].HeaderText = rm.GetString("lbName", ci);
            dgClaimOwner.Columns[3].HeaderText = rm.GetString("lbAddress", ci);
            dgClaimOwner.Columns[8].HeaderText = rm.GetString("lbCIID", ci);
        }

        protected void SearchForDebtor(string nationalID, string firstName, string surName) {
            trClaimOwnerGrid.Visible = false;
            try {
                DataSet ds;
                if (rbtIndividual.Checked) {
                    var myDebt = new Debtor
                                 {
                                     CreditInfo = (-1),
                                     IDNumber1 = nationalID,
                                     IDNumber2Type = 1,
                                     FirstName = firstName,
                                     SurName = surName,
                                     showPEP = chkShowPEP.Checked
                    };
                    ds = myFact.FindCustomer(myDebt);
                } else {
                    var myComp = new Company
                                     {
                                         CreditInfoID = (-1),
                                         NationalID = nationalID,
                                         NameNative = firstName
                                     };
                    ds = myFact.FindCompany(myComp);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        trDebtorGrid.Visible = false;
                        tbDCIIDNumber.Text = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                        tbDNationalID.Text = ds.Tables[0].Rows[0]["Number"].ToString().Trim();
                        //set the name of the company or person in the textboxes
                        if (rbtCompany.Checked) {
                            txtFirstName.Text = EN ? ds.Tables[0].Rows[0]["NameEN"].ToString().Trim() : ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                        } else {
                            if (EN) {
                                string name = ds.Tables[0].Rows[0]["NameEN"].ToString().Trim();

                                var last = name.LastIndexOf(" ") + 1;

                                txtFirstName.Text = name.Substring(0, last - 1);
                                txtSurName.Text = name.Substring(last);
                            } else {
                                string name = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();

                                int last = name.LastIndexOf(" ") + 1;

                                txtFirstName.Text = name.Substring(0, last - 1);
                                txtSurName.Text = name.Substring(last);
                            }
                        }
                        //this.btSearchDebtor_Click(this, null);
                        return;
                    }
                    dgDebtors.DataSource = ds;
                    dgDebtors.DataBind();
                    trDebtorGrid.Visible = true;
                    //this.tdNameSearchGrid.Visible=true;

                    int gridRows = dgDebtors.Items.Count;
                    divNameSearch.Style["HEIGHT"] = gridRows <= 10 ? "auto" : "200px";
                } else {
                    trDebtorGrid.Visible = false;
                    tbDNationalID.Text = nationalID;
                    txtFirstName.Text = firstName;
                    txtSurName.Text = surName;
                    DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        private void rbtIndividual_CheckedChanged(object sender, EventArgs e) {
            if (rbtIndividual.Checked) {
                lblSurName.Visible = true;
                txtSurName.Visible = true;
                lblFirstName.Text = rm.GetString("txtFirstname", ci);
                lblSurName.Text = rm.GetString("txtSurname", ci);
                txtSurName.Text = "";
                txtFirstName.Text = "";

                //			lbtSearchSurName.Visible=true;
                dgDebtors.DataSource = null;
                dgDebtors.DataBind();
                trDebtorGrid.Visible = false;
            }
        }

        private void rbtCompany_CheckedChanged(object sender, EventArgs e) {
            if (rbtCompany.Checked) {
                lblFirstName.Text = rm.GetString("txtCompany", ci);
                lblSurName.Visible = false;
                txtSurName.Visible = false;
                txtFirstName.Text = "";
                //			lbtSearchSurName.Visible=false;
                dgDebtors.DataSource = null;
                dgDebtors.DataBind();
                trDebtorGrid.Visible = false;
            }
        }

        private void rbtnIndividualClaimOwner_CheckedChanged(object sender, EventArgs e) {
            if (rbtnIndividualClaimOwner.Checked) {
                lblClaimOwnerSurname.Visible = true;
                txtClaimOwnerSurname.Visible = true;
                lbClaimOwner.Text = rm.GetString("txtFirstname", ci);
                lblClaimOwnerSurname.Text = rm.GetString("txtSurname", ci);
                txtClaimOwnerSurname.Text = "";
                txtClaimOwner.Text = "";
                dgClaimOwner.DataSource = null;
                dgClaimOwner.DataBind();
                trClaimOwnerGrid.Visible = false;
            }
        }

        private void rbtnCompanyClaimOwner_CheckedChanged(object sender, EventArgs e) {
            if (rbtnCompanyClaimOwner.Checked) {
                lbClaimOwner.Text = rm.GetString("txtCompany", ci);
                lblClaimOwnerSurname.Visible = false;
                txtClaimOwnerSurname.Visible = false;
                txtClaimOwner.Text = "";
                dgClaimOwner.DataSource = null;
                dgClaimOwner.DataBind();
                trClaimOwnerGrid.Visible = false;
            }
        }

        private void dgDebtors_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                tbDCIIDNumber.Text = e.Item.Cells[8].Text;
                tbDNationalID.Text = e.Item.Cells[1].Text;
                string name = e.Item.Cells[2].Text;
                if (rbtCompany.Checked) {
                    txtFirstName.Text = name;
                } else {
                    if (name != null && name.Trim() != "" && name.Trim() != "&nbsp;") {
                        string[] arrName = name.Split(' ');
                        if (arrName.Length > 0) {
                            if (arrName.Length == 1) {
                                txtFirstName.Text = arrName[0];
                            } else {
                                txtSurName.Text = arrName[arrName.Length - 1];
                                txtFirstName.Text = name.Substring(0, name.Length - (txtSurName.Text.Length + 1));
                            }
                        }
                    }
                }
                //btSearchDebtor_Click(this, null);
            }
        }

        private void dgDebtors_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (EN) {
                    //Set name - EN if available, else native
                    if (e.Item.Cells[5].Text != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    }

                    //Set street - EN if available, else native
                    if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    }

                    //Add city to street
                    if (e.Item.Cells[10].Text != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[3].Text += ", " + e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[3].Text += ", " + e.Item.Cells[9].Text;
                    }
                } else {
                    //Set name - Native if available, else EN
                    if (e.Item.Cells[4].Text != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    }

                    //Set street - Native if available, else EN
                    if (e.Item.Cells[6].Text != "" && e.Item.Cells[6].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    }

                    //Add city to street
                    if (e.Item.Cells[9].Text != "" && e.Item.Cells[9].Text != "&nbsp;") {
                        e.Item.Cells[3].Text += ", " + e.Item.Cells[9].Text;
                    } else {
                        e.Item.Cells[3].Text += ", " + e.Item.Cells[10].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
                WebDesign.CreateExplanationIcons(dgDebtors.Columns, lblDatagridIcons2, rm, ci);
            }
        }

        protected void SearchForClaimOwner(string nationalID, string firstName, string surName) {
            trDebtorGrid.Visible = false;
            var myDebt = new Debtor
                         {
                             CreditInfo = (-1),
                             IDNumber1 = nationalID,
                             IDNumber2Type = 1,
                             FirstName = firstName,
                             SurName = surName,
                             showPEP = chkShowPEP.Checked
                         };

            var myComp = new Company {CreditInfoID = (-1), NationalID = nationalID, NameNative = firstName};

            try {
                var ds = rbtnCompanyClaimOwner.Checked ? myFact.FindCompany(myComp) : myFact.FindCustomer(myDebt);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        //if creditinfoid
                        string sCreditInfoID = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                        if (sCreditInfoID != "") {
                            //this.tdNameSearchGrid.Visible=false;

                            trClaimOwnerGrid.Visible = false;
                            //this.tbClaimOwner.Text = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            txtClaimOwnerCIID.Text = sCreditInfoID;
                            txtClaimOwnerNationalID.Text = ds.Tables[0].Rows[0]["Number"].ToString().Trim();

                            if (rbtCompany.Checked) {
                                txtClaimOwner.Text = EN ? ds.Tables[0].Rows[0]["NameEN"].ToString().Trim() : ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            } else {
                                if (EN) {
                                    string name = ds.Tables[0].Rows[0]["NameEN"].ToString().Trim();

                                    int last = name.LastIndexOf(" ") + 1;
                                    txtClaimOwner.Text = name;//name.Substring(0, last - 1);
                                    txtClaimOwnerSurname.Text = name.Substring(last);
                                } else {
                                    string name = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();

                                    int last = name.LastIndexOf(" ") + 1;
                                    txtClaimOwner.Text = name;//name.Substring(0, last - 1);
                                    txtClaimOwnerSurname.Text = name.Substring(last);
                                }
                            }
                        } else {
                            dgClaimOwner.DataSource = ds;
                            dgClaimOwner.DataBind();
                            trClaimOwnerGrid.Visible = true;

                            divClaimOwner.Style["HEIGHT"] = "auto"; //dtgrNameSearch.Height.ToString();
                            divClaimOwner.Style["OVERFLOW"] = "auto";
                            divClaimOwner.Style["OVERFLOW-X"] = "auto";
                        }
                    } else {
                        dgClaimOwner.DataSource = ds;
                        dgClaimOwner.DataBind();
                        trClaimOwnerGrid.Visible = true;

                        int gridRows = dgClaimOwner.Items.Count;
                        divClaimOwner.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                        divClaimOwner.Style["OVERFLOW"] = "auto";
                        divClaimOwner.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    trClaimOwnerGrid.Visible = false;
                    txtClaimOwner.Text = firstName;
                    txtClaimOwnerNationalID.Text = nationalID;
                    txtClaimOwnerSurname.Text = nationalID;
                    //if(ciid==-1)
                    //this.txtClaimOwnerCIID.Text="";
                    //else
                    //this.txtClaimOwnerCIID.Text = ciid.ToString();

                    DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        private void dgClaimOwner_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (EN) {
                    //Set name - EN if available, else native
                    if (e.Item.Cells[5].Text != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    }

                    //Set street - EN if available, else native
                    if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    }

                    //Add city to street
                    if (e.Item.Cells[11].Text != "" && e.Item.Cells[11].Text != "&nbsp;") {
                        e.Item.Cells[3].Text += ", " + e.Item.Cells[11].Text;
                    } else {
                        e.Item.Cells[3].Text += ", " + e.Item.Cells[10].Text;
                    }
                } else {
                    //Set name - Native if available, else EN
                    if (e.Item.Cells[4].Text != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    }

                    //Set street - Native if available, else EN
                    if (e.Item.Cells[6].Text != "" && e.Item.Cells[6].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    }

                    //Add city to street
                    if (e.Item.Cells[10].Text != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[3].Text += ", " + e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[3].Text += ", " + e.Item.Cells[11].Text;
                    }
                }
            }

            WebDesign.CreateExplanationIcons(dgClaimOwner.Columns, lblDatagridIcons, rm, ci);
        }

        private void dgClaimOwner_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                int ciid = -1;
                if (e.Item.Cells[8].Text != "") {
                    try {
                        ciid = int.Parse(e.Item.Cells[8].Text);
                    } catch (Exception) {
                        ciid = -1;
                    }
                }
                if (ciid != -1) {
                    txtClaimOwnerCIID.Text = ciid.ToString();
                    if (rbtnCompanyClaimOwner.Checked) {
                        txtClaimOwner.Text = e.Item.Cells[2].Text;
                    } else {
                        string name = e.Item.Cells[2].Text;
                        if (name != null && name.Trim() != "" && name.Trim() != "&nbsp;") {
                            string[] arrName = name.Split(' ');
                            if (arrName.Length > 0) {
                                if (arrName.Length == 1) {
                                    txtClaimOwner.Text = arrName[0];
                                } else {
                                    txtClaimOwnerSurname.Text = arrName[arrName.Length - 1];
                                    txtClaimOwner.Text = name.Substring(0, name.Length - (txtSurName.Text.Length + 1));
                                }
                            }
                        }
                    }

                    txtClaimOwnerNationalID.Text = e.Item.Cells[1].Text.Trim();
                    //	trClaimOwnerGrid.Visible=false;
                }
            }
        }

        private void btnSearchDebtor_Click(object sender, EventArgs e) { SearchForDebtor(tbDNationalID.Text, txtFirstName.Text, txtSurName.Text); }
        private void btnSearchClaim_Click(object sender, EventArgs e) { SearchForClaimOwner(txtClaimOwnerNationalID.Text, txtClaimOwner.Text, txtClaimOwnerSurname.Text); }

        private void btnDebtorClear_Click(object sender, EventArgs e) {
            txtFirstName.Text = "";
            txtSurName.Text = "";
            tbDCIIDNumber.Text = "";
            dgDebtors.DataSource = null;
            dgDebtors.DataBind();
            trDebtorGrid.Visible = false;
            this.tbDNationalID.Text = string.Empty;
            this.tbDCIIDNumber.Text = string.Empty;
        }

        private void btnClaimOwnerClear_Click(object sender, EventArgs e) {
            txtClaimOwner.Text = "";
            txtClaimOwnerCIID.Text = "";
            dgClaimOwner.DataSource = null;
            dgClaimOwner.DataBind();
            trClaimOwnerGrid.Visible = false;
            this.txtClaimOwnerSurname.Text = string.Empty;
            this.txtClaimOwnerNationalID.Text = string.Empty;
            this.txtClaimOwnerCIID.Text = string.Empty;
        }

        private void btnFindClaimClear_Click(object sender, EventArgs e) {
            tbRegDateFrom.Text = "";
            tbRegDateTo.Text = "";
            tbCourtCaseNumber.Text = "";
            ddCaseType.SelectedIndex = 0;
            ddInformationSource.SelectedIndex = 0;
            ddState.SelectedIndex = 0;
            dgClaim.DataSource = null;
            dgClaim.DataBind();
            trClaimSearch.Visible = false;
            this.tbClaimID.Text = string.Empty;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rbtIndividual.CheckedChanged += new System.EventHandler(this.rbtIndividual_CheckedChanged);
            this.rbtCompany.CheckedChanged += new System.EventHandler(this.rbtCompany_CheckedChanged);
            this.btnDebtorClear.Click += new System.EventHandler(this.btnDebtorClear_Click);
            this.btnSearchDebtor.Click += new System.EventHandler(this.btnSearchDebtor_Click);
            this.dgDebtors.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDebtors_ItemCommand);
            this.dgDebtors.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgDebtors_ItemDataBound);
            this.rbtnIndividualClaimOwner.CheckedChanged +=
                new System.EventHandler(this.rbtnIndividualClaimOwner_CheckedChanged);
            this.rbtnCompanyClaimOwner.CheckedChanged +=
                new System.EventHandler(this.rbtnCompanyClaimOwner_CheckedChanged);
            this.btnClaimOwnerClear.Click += new System.EventHandler(this.btnClaimOwnerClear_Click);
            this.btnSearchClaim.Click += new System.EventHandler(this.btnSearchClaim_Click);
            this.dgClaimOwner.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgClaimOwner_ItemCommand);
            this.dgClaimOwner.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgClaimOwner_ItemDataBound);
            this.CustomValidator1.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.btnFindClaimClear.Click += new System.EventHandler(this.btnFindClaimClear_Click);
            this.btSearchDebtor.Click += new System.EventHandler(this.btSearchDebtor_Click);
            this.dgClaim.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgClaim_ItemCommand);
            this.dgClaim.PageIndexChanged +=
                new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgClaim_PageIndexChanged);
            this.dgClaim.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgClaim_SortCommand);
            this.dgClaim.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgClaim_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

        
    }
}