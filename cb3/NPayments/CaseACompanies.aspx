<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="CaseACompanies.aspx.cs" AutoEventWireup="false" Inherits="NPayments.CaseACompanys" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CaseACompanys</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<script>
			function checkEnterKey() 
			{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						vansk.btSubmit.click(); 
					}
			} 
			function SetFormFocus()
				{
					document.vansk.tbCreditorNameNative.focus();
				}
		</script>
	</HEAD>
	<body onload="SetFormFocus()">
		<form id="vansk" name="vansk" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table id="tbDefault" width="100%">
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbDebtor" runat="server" font-bold="True">Debtor</asp:label><asp:textbox id="tbClaimID" runat="server" visible="False"></asp:textbox>
															<asp:label id="lbClaimID" runat="server" visible="False">ClaimID</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellspacing="0" cellpadding="0" border="0">
																<tr>
																	<td>
																		<table>
																			<tr>
																				<td style="WIDTH: 224px"><asp:label id="lblCIID" runat="server">Debtor CIID</asp:label><br>
																					<asp:textbox id="txtCompanyCIID" runat="server" maxlength="12"></asp:textbox><asp:requiredfieldvalidator id="rfvCIID" runat="server" controltovalidate="txtCompanyCIID" errormessage="Value is missing!">*</asp:requiredfieldvalidator></td>
																				<td style="WIDTH: 280px"><asp:label id="lblNationalID" runat="server">National ID</asp:label><br>
																					<asp:textbox id="txtCompanyNationalID" runat="server"></asp:textbox></td>
																				<td style="WIDTH: 280px"><asp:label id="lblCompanyName" runat="server">Company name</asp:label><br>
																					<asp:textbox id="txtCompanyName" runat="server"></asp:textbox></td>
																				<td style="WIDTH: 280px"><asp:label id="lblCompanyNameEN" runat="server">Company name (EN)</asp:label><br>
																					<asp:textbox id="txtCompanyNameEN" runat="server" readonly="True"></asp:textbox></td>
																			</tr>
																			<tr>
																				<td style="WIDTH: 224px" colspan="2"><asp:label id="lbInfoOrigin" runat="server">Information source</asp:label><br>
																					<asp:dropdownlist id="ddInformationSourceList" runat="server" Width="280px"></asp:dropdownlist></td>
																				<td style="WIDTH: 224px"></td>
																				<td style="WIDTH: 224px" align="right"><asp:button id="btnSearch" runat="server" causesvalidation="False" cssclass="search_button"
																						text="Search"></asp:button>&nbsp; <input onclick="GoNordurNidur('txtCompanyCIID','txtCompanyNationalID','txtCompanyName','',1, 670, 900);"
																						type="button" value="..." class="popup"></td>
																			</tr>
																			<tr>
																				<td style="WIDTH: 224px" colspan="2" height="23"></td>
																				<td style="WIDTH: 224px" height="23"></td>
																				<td style="WIDTH: 224px" align="right" height="23"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<!-- // -->
										<!--tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td align="right">
											</td>
										</tr-->
										<asp:button id="btnCreate" runat="server" causesvalidation="False" cssclass="confirm_button"
											text="New" visible="False"></asp:button>
										<!-- // -->
										<tr>
											<td>
												<table class="grid_table" id="trNameSearchGrid" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right">
															<div><img alt="Select" src="../img/select.gif" align="middle">&nbsp;=&nbsp;<asp:label id="Label3" runat="server">Select</asp:label></div>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblDgResults" runat="server" cssclass="HeadMain">Ord</asp:label></th></tr>
													<tr>
														<td id="tdNameSearchGrid" width="100%" runat="server">
															<div class="datagrid" id="divNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																runat="server"><asp:datagrid id="dgCompanies" runat="server" forecolor="Black" gridlines="None" cellpadding="4"
																	backcolor="White" borderwidth="1px" borderstyle="None" bordercolor="#DEDFDE" autogeneratecolumns="False"
																	allowsorting="True" width="100%">
																	<footerstyle cssclass="grid_footer"></footerstyle>
																	<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																	<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																	<itemstyle cssclass="grid_item"></itemstyle>
																	<headerstyle cssclass="grid_header"></headerstyle>
																	<columns>
																		<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																			commandname="Select">
																			<itemstyle cssclass="leftpadding"></itemstyle>
																		</asp:buttoncolumn>
																		<asp:boundcolumn datafield="Number" sortexpression="Number" headertext="National ID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Name">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Street">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameNative" sortexpression="NameNative" headertext="NameNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameEN" sortexpression="NameEN" headertext="NameEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetNative" sortexpression="StreetNative" headertext="StreetNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetEN" sortexpression="StreetEN" headertext="StreetEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn datafield="CreditInfoID" sortexpression="CreditInfoID" headertext="CreditInfoID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																	</columns>
																	<pagerstyle cssclass="grid_pager"></pagerstyle>
																</asp:datagrid></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblClaimOwner" runat="server" font-bold="True">Claim owner</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellspacing="0" cellpadding="0" border="0">
																<tr>
																	<td colspan="3" style="HEIGHT: 37px"><asp:radiobutton id="rbtnIndividual" runat="server" cssclass="radio" checked="True" groupname="Type"></asp:radiobutton>
																		<asp:image id="imgIndividual" runat="server" imageurl="../img/individual.gif"></asp:image>&nbsp;
																		<asp:radiobutton id="rbtnCompany" runat="server" cssclass="radio" groupname="Type"></asp:radiobutton>
																		<asp:image id="imgCompany" runat="server" imageurl="../img/company.gif"></asp:image></td>
																</tr>
																<tr>
																	<td style="WIDTH: 327px"><asp:label id="lblClaimOwnerCIID" runat="server">Claim owner CIID</asp:label><br>
																		<asp:textbox id="txtClaimOwnerCIID" runat="server"></asp:textbox><asp:requiredfieldvalidator id="rfvClaimOwnerCIID" runat="server" controltovalidate="txtClaimOwnerCIID" errormessage="Value missing!">*</asp:requiredfieldvalidator></td>
																	<td><asp:label id="lbCreditorOwnerNative" runat="server"> Claim owner</asp:label><br>
																		<asp:textbox id="tbCreditorNameNative" runat="server" maxlength="50"></asp:textbox></td>
																	<td align="right"><asp:button id="btnSearchClaim" runat="server" causesvalidation="False" cssclass="search_button"
																			text="Search"></asp:button>&nbsp; <input onclick="GoNordurNidur('txtClaimOwnerCIID','','tbCreditorNameNative','',rbtnIndividual, 670, 900);"
																			type="button" value="..." class="popup"></td>
																</tr>
																<tr>
																	<td style="WIDTH: 327px" height="23">&nbsp;</td>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<!-- // -->
										<!--tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td align="right"></td>
										</tr-->
										<asp:button id="btnCreateNewClaim" runat="server" causesvalidation="False" cssclass="confirm_button"
											text="Create" visible="False"></asp:button>
										<!-- // -->
										<tr>
											<td>
												<table class="grid_table" id="trClaimOwnerNameSearchGrid" cellspacing="0" cellpadding="0"
													runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right">
															<div><img alt="Select" src="../img/select.gif" align="middle">&nbsp;=&nbsp;<asp:label id="Label1" runat="server">Select</asp:label></div>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															&nbsp;
															<asp:label id="lblDgResults2" runat="server">Label</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<div class="datagrid" id="divClaimOwner" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																runat="server"><asp:datagrid id="dgClaimOwner" runat="server" cssclass="rammi" forecolor="Black" gridlines="None"
																	cellpadding="4" backcolor="White" borderwidth="1px" borderstyle="None" bordercolor="#DEDFDE" autogeneratecolumns="False"
																	allowsorting="True" width="100%">
																	<footerstyle cssclass="grid_footer"></footerstyle>
																	<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																	<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																	<itemstyle cssclass="grid_item"></itemstyle>
																	<headerstyle cssclass="grid_header"></headerstyle>
																	<pagerstyle cssclass="grid_pager"></pagerstyle>
																	<columns>
																		<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																			commandname="Select">
																			<itemstyle cssclass="leftpadding"></itemstyle>
																		</asp:buttoncolumn>
																		<asp:boundcolumn datafield="Number" sortexpression="Number" headertext="National ID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Name">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Street">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameNative" sortexpression="NameNative" headertext="NameNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameEN" sortexpression="NameEN" headertext="NameEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetNative" sortexpression="StreetNative" headertext="StreetNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetEN" sortexpression="StreetEN" headertext="StreetEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn datafield="CreditInfoID" sortexpression="CreditInfoID" headertext="CreditInfoID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="IsCompany" headertext="IsCompany">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																	</columns>
																</asp:datagrid></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbDates" runat="server">Dates</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellspacing="0" cellpadding="0" border="0">
																<tr>
																	<td style="WIDTH: 221px; HEIGHT: 15px"><asp:label id="lbRegisted" runat="server">Registed</asp:label><br>
																		<asp:textbox id="tbRegDate" runat="server" enabled="False"></asp:textbox></td>
																	<td style="WIDTH: 223px; HEIGHT: 15px"><asp:label id="lbClaim" runat="server">Claim</asp:label><br>
																		<asp:textbox id="tbClaimDate" runat="server"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbClaimDate', 250, 250);" type="button" value="...">
																		<asp:requiredfieldvalidator id="RequiredFieldValidator5" runat="server" controltovalidate="tbClaimDate" errormessage="Value missing!">*</asp:requiredfieldvalidator><asp:customvalidator id="CustomValidator1" runat="server" controltovalidate="tbClaimDate" errormessage="Date input incorrect!"
																			cssclass="rammi">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td style="WIDTH: 221px; HEIGHT: 15px"><asp:label id="lbOnHold" runat="server">Delayed</asp:label><br>
																		<asp:textbox id="tbClaimDelayedDate" runat="server"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbClaimDelayedDate', 250, 250);" type="button"
																			value="...">
																		<asp:customvalidator id="CustomValidator2" runat="server" controltovalidate="tbClaimDelayedDate" errormessage="Date input incorrect">*</asp:customvalidator></td>
																	<td style="WIDTH: 223px; HEIGHT: 15px"><asp:label id="lbPayment" runat="server">Payment</asp:label><br>
																		<asp:textbox id="tbPaymentDate" runat="server"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbPaymentDate', 250, 250);" type="button" value="...">
																		<asp:customvalidator id="CustomValidator3" runat="server" controltovalidate="tbPaymentDate" errormessage="Date input incorrect">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td style="WIDTH: 471px" colspan="2" height="23">&nbsp;&nbsp;</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbOther" runat="server">Other info</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellspacing="0" cellpadding="0" border="0">
																<tr>
																	<td style="WIDTH: 390px"><asp:label id="lbStatusType" runat="server">StatusType</asp:label><br>
																		<asp:dropdownlist id="ddType" runat="server" Width="368px"></asp:dropdownlist>
																	</td>
																	<td><asp:label id="lbGazetteYear" runat="server">Gazzette Year</asp:label><br>
																		<asp:textbox id="tbGazetteYear" runat="server"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td style="WIDTH: 390px"><asp:label id="lbCaseID" runat="server">Case number</asp:label><br>
																		<asp:textbox id="tbCaseID" runat="server"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" controltovalidate="tbCaseID" errormessage="Value missing!">*</asp:requiredfieldvalidator>
																	</td>
																	<td><asp:label id="lbGazettePage" runat="server">Gazzette page</asp:label><br>
																		<asp:textbox id="tbGazettePage" runat="server"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23" style="WIDTH: 390px"></td>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbRegistration" runat="server">Registration</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellspacing="0" cellpadding="0" border="0">
																<tr class="input">
																	<td style="WIDTH: 370px; HEIGHT: 14px"><asp:label id="lbStatus" runat="server">Status</asp:label><br>
																		<asp:textbox id="tbStatus" runat="server" readonly="True"></asp:textbox></td>
																	<td style="WIDTH: 1px; HEIGHT: 14px"></td>
																	<td style="WIDTH: 158px; HEIGHT: 14px" colspan="2"><asp:label id="lbRegister" runat="server"> Register</asp:label><br>
																		<asp:textbox id="tbRegister" runat="server" readonly="True"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 370px; HEIGHT: 20px"><asp:label id="lbComment" runat="server">Comment</asp:label><br>
																		<asp:textbox id="tbComment" runat="server" maxlength="200" width="306px" tooltip="NOTE! This comment will be publicly viewable."
																			height="103px" textmode="MultiLine"></asp:textbox></td>
																	<td style="WIDTH: 1px; HEIGHT: 20px"></td>
																	<td style="HEIGHT: 20px" colspan="2"><asp:label id="lbInternalComment" runat="server">Internal Comment</asp:label><br>
																		<asp:textbox id="tbInternalComment" runat="server" maxlength="200" width="306px" height="103px"
																			textmode="MultiLine"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 370px" height="23"></td>
																	<td style="WIDTH: 1px" height="23"></td>
																	<td colspan="2" height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td><asp:textbox id="tbPageID" runat="server" visible="False" height="9px"></asp:textbox></td>
											<td></td>
										</tr>
										<tr>
											<td>
												<table width="100%">
													<tr valign="top">
														<td>
															<asp:validationsummary id="ValidationSummary1" runat="server" width="152px"></asp:validationsummary><asp:label id="lblMsg" runat="server" visible="False" forecolor="Blue" CssClass="confirm_text">New Claim has been registered</asp:label>
														</td>
														<td align="right">
															<asp:button id="btnShowAll" runat="server" causesvalidation="False" cssclass="gray_button" text="Show all"></asp:button><asp:button id="btnSubmitAndClear" runat="server" cssclass="confirm_button" text="submit &amp; clear"></asp:button><asp:button id="btSubmit" runat="server" cssclass="confirm_button" text="submit &amp; new"></asp:button>
														</td>
													</tr>
												</table>
											</td>
											<td></td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
