#region

using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using NPayments.BLL.Letter;
using NPayments.BLL.PendingCases;

#endregion

using Cig.Framework.Base.Configuration;

namespace NPayments
{
    /// <summary>
    /// Summary description for DisplayLetter.
    /// </summary>
    public class DisplayLetter : Page
    {
        private void Page_Load(object sender, EventArgs e)
        {
            var arrLetter = (ArrayList)Session["arrLetter"];
            if (Session["PendingCaseLetter"] != null && Session["PendingCaseLetter"].ToString().ToLower() == "true")
            {
                Session.Remove("PendingCaseLetter");
                GenPendingCaseLetter(arrLetter);
            }
            else
            {
                GenLetter(arrLetter);
            }
        }

        public void GenLetter(ArrayList arrLetters)
        {
            var myLG = new LetterGenerator();
            //ArrayList strArray = new ArrayList();
            /*
			foreach(Letter genLetter in arrLetters) 
			{
				if(genLetter.RegularLetter)
					path = Server.MapPath(CigConfig.Configure("lookupsettings.regularnotifivationletter"]);
				else if(genLetter.BankruptieLetter)
					path = Server.MapPath(CigConfig.Configure("lookupsettings.bankrutienotificationletter"]);
				else 
					continue;

					//strArray.Add(myLG.Generate(path,genLetter));
					Response.Write(myLG.Generate(path,genLetter));

			}
			*/

            Response.Write(
                myLG.Generate(Server.MapPath(CigConfig.Configure("lookupsettings.headerfornotificationletter"))));
            for (int i = 0; i < arrLetters.Count; i++)
            {
                var genLetter = (Letter)arrLetters[i];
                var path = "";
                if (genLetter.RegularLetter)
                {
                    path = Server.MapPath(CigConfig.Configure("lookupsettings.regularnotifivationletter"));
                }
                else if (genLetter.BankruptieLetter)
                {
                    path = Server.MapPath(CigConfig.Configure("lookupsettings.bankrutienotificationletter"));
                }
                else
                {
                    continue;
                }
                if (i < arrLetters.Count - 1)
                {
                    Response.Write(myLG.Generate(path, genLetter));
                    Response.Write("<br clear=all style='page-break-before:always'>");
                }
                else
                {
                    Response.Write(myLG.Generate(path, genLetter));
                }
            }
            Response.Write(
                myLG.Generate(Server.MapPath(CigConfig.Configure("lookupsettings.footerfornotificationletter"))));
        }

        public void GenPendingCaseLetter(ArrayList arrLetters)
        {
            var myLG = new PendingCaseLetterGenerator();
            //ArrayList strArray = new ArrayList();

            Response.Write(
                myLG.Generate(Server.MapPath(CigConfig.Configure("lookupsettings.headerfornotificationletter"))));

            int i = 1;
            foreach (PendingCaseBLLC pCase in arrLetters)
            {
                string path = Server.MapPath(CigConfig.Configure("lookupsettings.pendingcasenotificationletter"));

                //strArray.Add(myLG.Generate(path,pCase));
                Response.Write(myLG.Generate(path, pCase));

                if (i < arrLetters.Count)
                {
                    Response.Write("<br clear=all style='page-break-before:always'>");
                }

                i++;
            }

            Response.Write(
                myLG.Generate(Server.MapPath(CigConfig.Configure("lookupsettings.footerfornotificationletter"))));
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}