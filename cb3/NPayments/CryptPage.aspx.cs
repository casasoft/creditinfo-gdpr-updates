#region

using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataProtection;

#endregion

namespace NPayments {
    /// <summary>
    /// Summary description for CryptPage.
    /// </summary>
    public class CryptPage : Page {
        protected Button btnDecrypt;
        protected Button btnDecryptConfig;
        protected Button btnEncrypt;
        protected Label lbDataToEncrypt;
        protected Label lbDecryptedData;
        protected Label lbEncryptedData;
        protected Label lblError;
        protected TextBox txtDataToEncrypt;
        protected TextBox txtDecryptedData;
        protected TextBox txtEncryptedData;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
        }

        private void btnEncrypt_Click(object sender, EventArgs e) {
            var dp = new DataProtector(DataProtector.Store.USE_MACHINE_STORE);
            try {
                var dataToEncrypt =
                    Encoding.ASCII.GetBytes(txtDataToEncrypt.Text);
                // Not passing optional entropy in this example
                // Could pass random value (stored by the application) for added
                // security when using DPAPI with the machine store.
                txtEncryptedData.Text =
                    Convert.ToBase64String(dp.Encrypt(dataToEncrypt, null));
            } catch (Exception ex) {
                lblError.ForeColor = Color.Red;
                lblError.Text = "Exception.<br>" + ex.Message;
                return;
            }
            lblError.Text = "";
        }

        private void btnDecrypt_Click(object sender, EventArgs e) {
            var dp = new DataProtector(DataProtector.Store.USE_MACHINE_STORE);
            try {
                byte[] dataToDecrypt =
                    Convert.FromBase64String(txtEncryptedData.Text);
                // Optional entropy parameter is null. 
                // If entropy was used within the Encrypt method, the same entropy
                // parameter must be supplied here
                txtDecryptedData.Text =
                    Encoding.ASCII.GetString(dp.Decrypt(dataToDecrypt, null));
            } catch (Exception ex) {
                lblError.ForeColor = Color.Red;
                lblError.Text = "Exception.<br>" + ex.Message;
                return;
            }
            lblError.Text = "";
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}