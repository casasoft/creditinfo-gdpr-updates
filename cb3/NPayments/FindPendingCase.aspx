<%@ Page language="c#" Codebehind="FindPendingCase.aspx.cs" AutoEventWireup="false" Inherits="NPayments.FindPendingCase" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Find Pending Case</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						FindPendingCase.btnFind.click(); 
					}
				} 
				function SetFormFocus()
				{
					document.FindPendingCase.txtDefendantID.focus();
				}
		</SCRIPT>
	</HEAD>
	<body onload="SetFormFocus()">
		<form id="FindPendingCase" name="FindPendingCase" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language2" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="Sitepositionbar2" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblFindPendingCase" runat="server"> Find pending Case</asp:label></th></tr>
													<tr>
														<td>
															<TABLE class="fields" cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD style="WIDTH: 25%"><asp:label id="lblDefendantID" runat="server">Defendant ID</asp:label><BR>
																		<asp:textbox id="txtDefendantID" runat="server" MaxLength="12"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblDefendantName" runat="server"> Defendant name</asp:label><BR>
																		<asp:textbox id="txtDefendantName" runat="server"></asp:textbox></TD>
																	<TD style="WIDTH: 25%"><asp:label id="lblPlaintiffID" runat="server"> Plaintiff ID</asp:label><BR>
																		<asp:textbox id="txtPlaintiffID" runat="server"></asp:textbox></TD>
																	<TD><asp:label id="lblPlaintiffName" runat="server">Plaintiff name</asp:label><BR>
																		<asp:textbox id="txtPlaintiffName" runat="server" MaxLength="30"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 62px"><asp:label id="lblCaseNumber" runat="server">Case number</asp:label><BR>
																		<asp:textbox id="txtCaseNumber" runat="server"></asp:textbox></TD>
																	<TD style="HEIGHT: 62px" colSpan="2"><asp:label id="lblInfoSource" runat="server">Information source</asp:label><BR>
																		<asp:dropdownlist id="ddInfoSource" runat="server" CssClass="long_input"></asp:dropdownlist></TD>
																	<TD style="HEIGHT: 90px"><asp:label id="lblCaseStatus" Runat="server">Case status</asp:label><br>
																		<asp:dropdownlist id="ddCaseStatus" Runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 46px"><asp:label id="lblDateRegistered" runat="server">Registed</asp:label><br>
																		<asp:textbox id="txtRegDate" runat="server"></asp:textbox><INPUT class="popup" onclick="PopupPicker('txtRegDate', 250, 250);" type="button" value="..."></TD>
																	<TD style="HEIGHT: 46px" colSpan="2">
																		<P><asp:label id="lblAdjudicator" runat="server"> Adjudicator</asp:label><BR>
																			<asp:dropdownlist id="ddAdjudicator" runat="server" CssClass="long_input"></asp:dropdownlist></P>
																	</TD>
																	<TD style="HEIGHT: 46px">
																		<asp:label id="lblSearchByRef" Runat="server">Search by reference</asp:label><br>
																		<asp:TextBox Runat="server" ID="txtReference"></asp:TextBox>
																	</TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 46px">
																		<asp:label id="lblDateFrom" Runat="server">Date from</asp:label><br>
																		<asp:textbox id="txtDateFrom" runat="server"></asp:textbox><INPUT class="popup" onclick="PopupPicker('txtDateFrom', 250, 250);" type="button" value="...">
																	</TD>
																	<TD>
																		<asp:label id="lblDateTo" Runat="server">Date to</asp:label><br>
																		<asp:textbox id="txtDateTo" runat="server"></asp:textbox><INPUT class="popup" onclick="PopupPicker('txtDateTo', 250, 250);" type="button" value="...">
																	</TD>
																	<TD>&nbsp;</TD>
																	<TD>&nbsp;</TD>
																</TR>
																<TR>
																	<TD><asp:textbox id="txtPCID" runat="server" Height="9px" Visible="False"></asp:textbox></TD>
																	<TD align="right"></TD>
																	<td align="right"></td>
																	<TD align="right"><asp:button id="btnFind" runat="server" CssClass="search_button" Text="Find"></asp:button></TD>
																</TR>
																<TR>
																	<TD align="right" height="23"></TD>
																</TR>
															</TABLE>
														</td>
													</tr>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<TR>
											<td align="right" colSpan="4">
												<table width="100%">
													<tr vAlign="top">
														<td style="WIDTH: 177px" colSpan="2"><asp:validationsummary id="ValidationSummary1" runat="server" Height="26px" Width="360px"></asp:validationsummary><asp:label id="lblMsg" runat="server" Visible="False" ForeColor="Red">New Claim has been registered</asp:label></td>
														<td align="right"><asp:button id="btnClear" runat="server" CssClass="cancel_button" Text="Clear" CausesValidation="False"></asp:button></td>
													</tr>
												</table>
											</td>
										</TR>
										<tr>
											<td height="10"></td>
										</tr>
										<tr id="trSearchGrid" runat="server">
											<td id="Td1">
												<TABLE class="grid_table" id="SearchGrid" cellSpacing="0" cellPadding="0" runat="server">
													<tr>
														<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<TR>
														<TH>
															<asp:label id="lblDgResults" runat="server"></asp:label></TH></TR>
													<TR>
														<TD id="Td2" width="100%" runat="server">
															<table class="datagrid">
																<tr>
																	<td><asp:datagrid id="dgPendingCases" runat="server" AllowPaging="True" PageSize="20" AutoGenerateColumns="False"
																			GridLines="None" AllowSorting="True">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																			<columns>
																				<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																					CommandName="Select">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:ButtonColumn>
																				<asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DefendantID" HeaderText="Defendant ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DefendantName" HeaderText="Defendant Name">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="PlaintiffID" HeaderText="Plaintiff ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="PlaintiffName" HeaderText="Plaintiff Name">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn HeaderText="Court">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="CourtNative" HeaderText="CourtNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="CourtEN" HeaderText="CourtEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn HeaderText="Adjudicator">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="AdjudicatorNative" HeaderText="AdjudicatorNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="AdjudicatorEN" HeaderText="AdjudicatorEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DateOfWrit" HeaderText="DateOfWrit" DataFormatString="{0:d}">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="WritNumber" HeaderText="WritNumber">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn HeaderText="State"></asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="StateNative" HeaderText="StateNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="StateEN" HeaderText="StateEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																			</columns>
																		</asp:datagrid></td>
																</tr>
															</table>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
