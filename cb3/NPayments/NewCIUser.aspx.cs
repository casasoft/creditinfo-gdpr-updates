using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for NewDebtor.
    /// </summary>
    public class NewDebtor : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly NPaymentsFactory myFact = new NPaymentsFactory();
        protected Button btCancel;
        protected Button btnCity2Search;
        protected Button btnCitySearch;
        protected Button btRebDebtor;
        protected DropDownList ddCity1;
        protected DropDownList ddCity2;
        protected DropDownList ddEducation;
        protected DropDownList ddNumberType1;
        protected DropDownList ddNumberType2;
        protected DropDownList ddPhoneNumber1Type;
        protected DropDownList ddPhoneNumber2Type;
        protected DropDownList ddPhoneNumber3Type;
        protected DropDownList ddPhoneNumber4Type;
        protected DropDownList ddProfession;
        private bool EN;
        protected Label lbAdditionalInfo;
        protected Label lbAddress1;
        protected Label lbAddress1EN;
        protected Label lbAddress1Header;
        protected Label lbAddress1Info;
        protected Label lbAddress1InfoEN;
        protected Label lbAddress2;
        protected Label lbAddress2EN;
        protected Label lbAddress2Header;
        protected Label lbAddress2Info;
        protected Label lbAddress2InfoEN;
        protected Label lbCity1;
        protected Label lbCity2;
        protected Label lbDebtorCIID;
        protected Label lbDebtorFirstName;
        protected Label lbDebtorIDNumber3;
        protected Label lbDebtorSurName;
        protected Label lbEducation;
        protected Label lbEmail;
        protected Label lbFirstNameEN;
        protected Label lbIDNumber2;
        protected Label lbIDNumberType;
        protected Label lbIDNumberType2;
        protected Label lblCity2Search;
        protected Label lblCitySearch;
        protected Label lblNote;
        protected Label lbMsg;
        protected Label lbNewIndividual;
        protected Label lbPersonInformation;
        protected Label lbPhoneNumber1;
        protected Label lbPhoneNumber2;
        protected Label lbPhonenumber2Type;
        protected Label lbPhoneNumber3;
        protected Label lbPhoneNumber3Type;
        protected Label lbPhoneNumber4;
        protected Label lbPhoneNumber4Type;
        protected Label lbPhonNumber1Type;
        protected Label lbPostalCode;
        protected Label lbPostBox;
        protected Label lbPostBox2;
        protected Label lbProfession;
        protected Label lbSurnameEN;
        protected Label ltPostalCode2;
        private bool newRegistration = true;
        protected HtmlForm newuser;
        public String redirectPage = "";
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected RequiredFieldValidator rfvCity;
        protected RequiredFieldValidator rfvCity2;
        protected TextBox tbAddress1;
        protected TextBox tbAddress1EN;
        protected TextBox tbAddress1Info;
        protected TextBox tbAddress1InfoEN;
        protected TextBox tbAddress2;
        protected TextBox tbAddress2EN;
        protected TextBox tbAddress2Info;
        protected TextBox tbAddress2InfoEN;
        protected TextBox tbDebtorCIID;
        protected TextBox tbDebtorFirstName;
        protected TextBox tbDebtorSurName;
        protected TextBox tbEmail;
        protected TextBox tbFirstNameEN;
        protected TextBox tbIDNumber1;
        protected TextBox tbIDNumber2;
        protected TextBox tbNote;
        protected TextBox tbPageID;
        protected TextBox tbPhoneNumber1;
        protected TextBox tbPhoneNumber2;
        protected TextBox tbPhoneNumber3;
        protected TextBox tbPhoneNumber4;
        protected TextBox tbPostalCode1;
        protected TextBox tbPostalCode2;
        protected TextBox tbPostBox1;
        protected TextBox tbPostBox2;
        protected TextBox tbSurnameEN;
        protected TextBox txtCity2Search;
        protected TextBox txtCitySearch;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            lbMsg.Visible = false;
            Page.ID = "212";
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();

            if (!IsPostBack) {
                InitDDBoxes();
                if (Session["Update"] != null) {
                    newRegistration = false;
                    initControls(true);
                } else {
                    initControls(false);
                }
            }
            if (Request.Params["pageid"] != null) {
                SetRedirectPage(int.Parse(Request.Params["pageid"]));
                tbPageID.Text = Request.Params["pageid"];
            }
            if (tbPageID.Text != "") {
                SetRedirectPage(int.Parse(tbPageID.Text));
            }

            LocalizeText();
            //pageID = int.Parse(Request.Params["pageid"]);
        }

        private void initControls(bool update) {
            if (update) {
                if (Session["CreditInfoID"] != null) {
                    var creditInfoID = (int) Session["CreditInfoID"];
                    var myIndi = myFact.GetIndivitual(creditInfoID);
                    tbDebtorCIID.Text = creditInfoID.ToString();
                    tbDebtorSurName.Text = myIndi.SurNameNative;
                    tbSurnameEN.Text = myIndi.SurNameEN;
                    tbEmail.Text = myIndi.Email;
                    tbDebtorFirstName.Text = myIndi.FirstNameNative;
                    tbFirstNameEN.Text = myIndi.FirstNameEN;
                    tbNote.Text = myIndi.Note;
                    if (myIndi.ProfessionID != 0) {
                        ddProfession.SelectedValue = myIndi.ProfessionID.ToString();
                    } else 
                        // stilla h�r � default N/A gildi
                        if (myIndi.EducationID != 0) {
                            ddEducation.SelectedValue = myIndi.EducationID.ToString();
                        } else {
                            // stilla h�r � default N/A gildi

                            initMultiItems(myIndi);
                        }
                    btRebDebtor.Text = "Update";
                }
            } else if (Session["AddIndividual"] != null) {
                if (bool.Parse(Session["AddIndividual"].ToString())) {
                    if (Session["NationalID"] != null) {
                        tbIDNumber1.Text = Session["NationalID"].ToString();
                    }
                    if (Session["FirstName"] != null) {
                        tbDebtorFirstName.Text = Session["FirstName"].ToString();
                    }
                    if (Session["SurName"] != null) {
                        tbDebtorSurName.Text = Session["SurName"].ToString();
                    }
                    if (Session["StreetNative"] != null) {
                        tbAddress1.Text = Session["StreetNative"].ToString();
                    }
                    if (Session["StreetEN"] != null) {
                        tbAddress1EN.Text = Session["StreetEN"].ToString();
                    }
                    if (Session["Note"] != null) {
                        tbNote.Text = Session["Note"].ToString();
                    }
                    if (Session["City"] != null) {
                        string cityName = Session["City"].ToString();
                        int cityId = myFact.GetCityID(cityName);
                        if (cityId > -1) {
                            ddCity1.Items.Add(new ListItem(cityName, cityId.ToString()));
                            ddCity1.SelectedValue = cityId.ToString();
                        }
                    }

                    Session.Remove("NationalID");
                    Session.Remove("FirstName");
                    Session.Remove("SurName");
                    Session.Remove("City");
                    Session.Remove("StreetNative");
                    Session.Remove("StreetEN");
                    Session.Remove("AddIndividual");
                    Session.Remove("Note");
                }
            } else if (Session["AddClaimOwner"] != null) {
                if (bool.Parse(Session["AddClaimOwner"].ToString())) {
                    if (Session["NationalID"] != null) {
                        tbIDNumber1.Text = Session["NationalID"].ToString();
                    }
                    if (Session["FirstName"] != null) {
                        tbDebtorFirstName.Text = Session["FirstName"].ToString();
                    }
                    if (Session["SurName"] != null) {
                        tbDebtorSurName.Text = Session["SurName"].ToString();
                    }
                    if (Session["StreetNative"] != null) {
                        tbAddress1.Text = Session["StreetNative"].ToString();
                    }
                    if (Session["StreetEN"] != null) {
                        tbAddress1EN.Text = Session["StreetEN"].ToString();
                    }
                    //Clear those session variables
                    Session.Remove("NationalID");
                    Session.Remove("FirstName");
                    Session.Remove("SurName");
                    Session.Remove("StreetNative");
                    Session.Remove("StreetEN");
                    Session.Remove("AddClaimOwner");
                    Session["ClaimOwnerInfo"] = "true";
                }
            }
        }

        private void initMultiItems(CreditInfoUser myIndi) {
            var arrAdd = myIndi.Address;
            var arrIDNum = myIndi.IDNumbers;
            var arrPNum = myIndi.Number;
            var frm = FindControl("newuser");
            for (var i = 1; i <= arrAdd.Count; i++) {
                var myAddr = (Address) arrAdd[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbAddress" + i) {
                            ((TextBox) ctrl).Text = myAddr.StreetNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "EN") {
                            ((TextBox) ctrl).Text = myAddr.StreetEN;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "Info") {
                            ((TextBox) ctrl).Text = myAddr.OtherInfoNative;
                        }
                        if ((ctrl).ID == "tbAddress" + i + "InfoEN") {
                            ((TextBox) ctrl).Text = myAddr.OtherInfoEN;
                        }
                        if ((ctrl).ID == "tbPostalCode" + i) {
                            ((TextBox) ctrl).Text = myAddr.PostalCode;
                        }
                        if ((ctrl).ID == "tbPostBox" + i) {
                            ((TextBox) ctrl).Text = myAddr.PostBox;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID != "ddCity" + i) {
                        continue;
                    }
                    var cityID = myAddr.CityID;
                    if (cityID <= -1) {
                        continue;
                    }
                    var cityName = myFact.GetCityName(cityID, EN);
                    if (string.IsNullOrEmpty(cityName)) {
                        continue;
                    }
                    ((DropDownList) ctrl).Items.Add(new ListItem(cityName, cityID.ToString()));
                    ((DropDownList) ctrl).SelectedValue = cityID.ToString();
                    //((DropDownList)ctrl).SelectedValue = myAddr.CityID.ToString();
                }
            }
            for (int i = 1; i <= arrIDNum.Count; i++) {
                var myIDNum = (IDNumber) arrIDNum[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbIDNumber" + i) {
                            ((TextBox) ctrl).Text = myIDNum.Number;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID == "ddNumberType" + i) {
                        ((DropDownList) ctrl).SelectedValue = myIDNum.NumberTypeID.ToString();
                    }
                }
            }
            for (int i = 1; i <= arrPNum.Count; i++) {
                var myPNum = (PhoneNumber) arrPNum[i - 1];

                foreach (Control ctrl in frm.Controls) {
                    if (ctrl is TextBox) {
                        if ((ctrl).ID == "tbPhoneNumber" + i) {
                            ((TextBox) ctrl).Text = myPNum.Number;
                        }
                    }
                    if (!(ctrl is DropDownList)) {
                        continue;
                    }
                    if ((ctrl).ID == "ddPhoneNumber" + i + "Type") {
                        ((DropDownList) ctrl).SelectedValue = myPNum.NumberTypeID.ToString();
                    }
                }
            }
        }

        public void SetRedirectPage(int index) { redirectPage = CigConfig.Configure("lookupsettings.redirectpageID" + index); }

        private void InitDDBoxes() {
            var myFact1 = new NPaymentsFactory();
            BindPhoneNumberTypes(myFact1);
            BindIDNumberTypes(myFact1);
            BindEducationTypes(myFact1);
            BindProfessionTypes(myFact1);
            //BindCityList(myFact);
        }

        private void BindPhoneNumberTypes(NPaymentsFactory myFact1) {
            var phoneTypeSet = myFact1.GetPNumberTypesAsDataSet();
            ddPhoneNumber1Type.DataSource = phoneTypeSet;
            ddPhoneNumber1Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber1Type.DataValueField = "NumberTypeID";
            ddPhoneNumber1Type.DataBind();

            ddPhoneNumber2Type.DataSource = phoneTypeSet;
            ddPhoneNumber2Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber2Type.DataValueField = "NumberTypeID";
            ddPhoneNumber2Type.DataBind();

            ddPhoneNumber3Type.DataSource = phoneTypeSet;
            ddPhoneNumber3Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber3Type.DataValueField = "NumberTypeID";
            ddPhoneNumber3Type.DataBind();

            ddPhoneNumber4Type.DataSource = phoneTypeSet;
            ddPhoneNumber4Type.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddPhoneNumber4Type.DataValueField = "NumberTypeID";
            ddPhoneNumber4Type.DataBind();
        }

        private void BindIDNumberTypes(NPaymentsFactory myFact1) {
            var idNumberType = myFact1.GetIDNumberTypesAsDataSet();

            ddNumberType1.DataSource = idNumberType;
            ddNumberType1.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddNumberType1.DataValueField = "NumberTypeID";
            ddNumberType1.DataBind();

            ddNumberType2.DataSource = idNumberType;
            ddNumberType2.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddNumberType2.DataValueField = "NumberTypeID";
            ddNumberType2.DataBind();
        }

        private void BindEducationTypes(NPaymentsFactory myFact1) {
            var educationTypeSet = myFact1.GetEducationListAsDataSet();
            ddEducation.DataSource = educationTypeSet;

            ddEducation.DataTextField = EN ? "NameEN" : "NameNative";

            ddEducation.DataValueField = "EducationID";
            ddEducation.DataBind();
        }

        private void BindProfessionTypes(NPaymentsFactory myFact1) {
            var professionTypeSet = myFact1.GetProfessioListAsDataSet();
            ddProfession.DataSource = professionTypeSet;
            ddProfession.DataTextField = EN ? "NameEN" : "NameNative";
            ddProfession.DataValueField = "ProfessionID";
            ddProfession.DataBind();
        }

        private void BindCityList(DataSet citySet) {
            ddCity1.DataSource = citySet;
            ddCity1.DataTextField = EN ? "NameEN" : "NameNative";

            ddCity1.DataValueField = "CityID";
            ddCity1.DataBind();
            //this.ddCity1.SelectedValue = "1";
            // ra�a h�r eftir itemum �annig a� r��in ver�i svona
            // 797,796,795,798,799,906			
        }

        private void BindCity2List(DataSet citySet) {
            ddCity2.DataSource = citySet;
            ddCity2.DataTextField = EN ? "NameEN" : "NameNative";

            ddCity2.DataValueField = "CityID";
            ddCity2.DataBind();
            //this.ddCity2.SelectedValue = "1";
            // Order  the items in the way Marina wanted them to be
            //bool update = true;
            //bool whitelist = true;
            //int[] valueArr = {906,799,798,795,796,797};
            //if(Session["Update"] == null)
            //	update = false;
            //if(Session["WhiteListPerson"] == null)
            //	whitelist = false;
        }

        private void btRebDebtor_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {} else {
                try {
                    var myFact1 = new NPaymentsFactory();
                    //BLL.Debtor myDebtor = new BLL.Debtor();
                    var theIndi = new Indivitual
                                  {
                                      Type = CigConfig.Configure("lookupsettings.individualID")
                                  };
                    try {
                        theIndi.EducationID = int.Parse(ddEducation.SelectedItem.Value);
                    } catch (Exception err) {
                        Logger.WriteToLog("NewCIUser.aspx : btRebDebtor() theIndi.EducationID " + err.Message, true);
                        theIndi.EducationID = 0;
                    }
                    theIndi.FirstNameNative = tbDebtorFirstName.Text;
                    theIndi.FirstNameEN = tbFirstNameEN.Text;
                    theIndi.SurNameNative = tbDebtorSurName.Text;
                    theIndi.SurNameEN = tbSurnameEN.Text;
                    theIndi.Email = tbEmail.Text;
                    theIndi.Note = tbNote.Text;

                    // check if update
                    if (Session["Update"] != null) {
                        newRegistration = false;
                    }

                    try {
                        theIndi.ProfessionID = int.Parse(ddProfession.SelectedItem.Value);
                    } catch (Exception err) {
                        Logger.WriteToLog("NewCIUser.aspx : btRebDebtor() theIndi.ProfessionID " + err.Message, true);
                        theIndi.ProfessionID = 0;
                    }

                    if (!newRegistration) {
                        theIndi.CreditInfoID = int.Parse(tbDebtorCIID.Text);
                    }

                    // collections
                    try {
                        theIndi.Address = GetAddresses();
                    } catch (Exception err) {
                        Logger.WriteToLog(
                            "NewCIUser.aspx : btRebDebtor() theIndi.Address = GetAddresses() " + err.Message, true);
                    }
                    try {
                        theIndi.IDNumbers = GetIDNumbers();
                    } catch (Exception err) {
                        Logger.WriteToLog(
                            "NewCIUser.aspx : btRebDebtor() theIndi.IDNumbers = GetIDNumbers(); " + err.Message, true);
                    }
                    try {
                        theIndi.Number = GetPhoneNumbers();
                    } catch (Exception err) {
                        Logger.WriteToLog(
                            "NewCIUser.aspx : btRebDebtor() theIndi.Number = GetPhoneNumbers(); " + err.Message, true);
                    }

                    bool regOK = true;
                    int creditInfoID = -1;
                    if (newRegistration) {
                        //Check if ID number is already registered
                        if (theIndi.IDNumbers != null) {
                            var idns = theIndi.IDNumbers;
                            for (int i = 0; i < idns.Count; i++) {
                                var idn = (IDNumber) idns[i];
                                if (!myFact1.IsIDRegisted(idn.Number, idn.NumberTypeID)) {
                                    continue;
                                }
                                DisplayErrorMsg("txtIDNumberAlreadyRegistered");
                                return;
                            }
                        }

                        creditInfoID = myFact1.addNewIndivitual(theIndi);
                        if (creditInfoID < 0) {
                            //String error = "FAILED"; // Gera e-� g�fulegt h�r....
                            regOK = false;
                            DisplayErrorMsg("txtFailedInsertingUser");
                            return;
                        }
                        DisplayMsg("lblUserRegistered");
                    } else {
                        theIndi.LastUpdate = DateTime.Now;
                        creditInfoID = myFact1.UpdateIndivitual(theIndi);
                        if (creditInfoID < 0) {
                            //String error = "FAILED"; // Gera e-� g�fulegt h�r....
                            regOK = false;
                            DisplayErrorMsg("txtFailedUpdatingUser");
                            return;
                        }
                        DisplayMsg("txtUserUpdated");
                    }

                    // if should redirct to another page
                    if (redirectPage != "" && regOK) {
                        if (Session["ClaimOwnerInfo"] != null && Session["ClaimOwnerInfo"].ToString() == "true") {
                            Session["CreditInfoID"] = creditInfoID;
                            Session["ClaimOwnerNameEN"] = tbFirstNameEN.Text + " " + tbSurnameEN.Text;
                            Session["ClaimOwnerNameNative"] = tbDebtorFirstName.Text + " " + tbDebtorSurName.Text;
                        } else {
                            Session["CreditInfoID"] = creditInfoID;
                            Session["DebtorFirstName"] = tbDebtorFirstName.Text;
                            Session["DebtorSurName"] = tbDebtorSurName.Text;
                            Session["txtNationalID"] = tbIDNumber1.Text;
                        }

                        //	Server.Transfer(redirectPage);
                        Response.Redirect(redirectPage);
                    }
                } catch (ThreadAbortException) {
                    // when using Server.Transfer this is an "normal" exception
                } catch (Exception err) {
                    Logger.WriteToLog("NewCIUser.aspx, btRebDebtor_Click. Message = " + err.Message, true);
                    DisplayErrorMsg("errMsg2");
                }
            }
        }

        protected void DisplayErrorMsg(string msg) {
            lbMsg.Text = rm.GetString(msg, ci);
            lbMsg.ForeColor = Color.Red;
            lbMsg.Visible = true;
        }

        protected void DisplayMsg(string msg) {
            lbMsg.Text = rm.GetString(msg, ci);
            lbMsg.ForeColor = Color.Blue;
            lbMsg.Visible = true;
        }

        public ArrayList GetAddresses() {
            var myAddr = new Address();
            var address = new ArrayList();
            if (tbAddress1.Text != "" || tbAddress1EN.Text != "" || tbAddress1Info.Text != "" ||
                tbAddress1InfoEN.Text != "") {
                if (ddCity1.SelectedItem.Value.Trim() != "") {
                    myAddr.CityID = int.Parse(ddCity1.SelectedItem.Value);
                }
                myAddr.OtherInfoNative = tbAddress1Info.Text;
                myAddr.OtherInfoEN = tbAddress1InfoEN.Text;
                myAddr.PostalCode = tbPostalCode1.Text;
                myAddr.PostBox = tbPostBox1.Text;
                myAddr.StreetNative = tbAddress1.Text;
                myAddr.StreetEN = tbAddress1EN.Text;
                address.Add(myAddr);
            }
            if (tbAddress2.Text != "" || tbAddress2EN.Text != "" || tbAddress2Info.Text != "" ||
                tbAddress2InfoEN.Text != "") {
                myAddr = new Address();
                if (ddCity2.SelectedItem.Value.Trim() != "") {
                    myAddr.CityID = int.Parse(ddCity2.SelectedItem.Value);
                }
                myAddr.OtherInfoNative = tbAddress2Info.Text;
                myAddr.OtherInfoEN = tbAddress2InfoEN.Text;
                myAddr.PostalCode = tbPostalCode2.Text;
                myAddr.PostBox = tbPostBox2.Text;
                myAddr.StreetNative = tbAddress2.Text;
                myAddr.StreetEN = tbAddress1EN.Text;
                address.Add(myAddr);
            }
            return address;
        }

        public ArrayList GetIDNumbers() {
            IDNumber myIDNum;
            var aNumbers = new ArrayList();
            if (tbIDNumber1.Text != "") {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber1.Text,
                              NumberTypeID = int.Parse(ddNumberType1.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            if (tbIDNumber2.Text != "") {
                myIDNum = new IDNumber
                          {
                              Number = tbIDNumber2.Text,
                              NumberTypeID = int.Parse(ddNumberType2.SelectedItem.Value)
                          };
                aNumbers.Add(myIDNum);
            }
            return aNumbers;
        }

        public ArrayList GetPhoneNumbers() {
            var myNum = new PhoneNumber();
            var aNumbers = new ArrayList();
            if (tbPhoneNumber1.Text != "") {
                myNum.Number = tbPhoneNumber1.Text;
                myNum.NumberTypeID = int.Parse(ddPhoneNumber1Type.SelectedItem.Value);
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber2.Text != "") {
                myNum = new PhoneNumber
                        {
                            Number = tbPhoneNumber2.Text,
                            NumberTypeID = int.Parse(ddPhoneNumber2Type.SelectedItem.Value)
                        };
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber3.Text != "") {
                myNum = new PhoneNumber
                        {
                            Number = tbPhoneNumber3.Text,
                            NumberTypeID = int.Parse(ddPhoneNumber3Type.SelectedItem.Value)
                        };
                aNumbers.Add(myNum);
            }
            if (tbPhoneNumber4.Text != "") {
                myNum = new PhoneNumber
                        {
                            Number = tbPhoneNumber4.Text,
                            NumberTypeID = int.Parse(ddPhoneNumber4Type.SelectedItem.Value)
                        };
                aNumbers.Add(myNum);
            }
            return aNumbers;
        }

        private void LocalizeText() {
            lbDebtorSurName.Text = rm.GetString("txtSurname", ci);
            lbDebtorFirstName.Text = rm.GetString("txtFirstname", ci);
            lbPersonInformation.Text = rm.GetString("lbPersonInformation", ci);
            btRebDebtor.Text = rm.GetString("btSubmit", ci);
            btCancel.Text = rm.GetString("btnCancel", ci);
            lbIDNumber2.Text = rm.GetString("lbIDNumber2", ci);
            lbIDNumberType.Text = rm.GetString("lbIDNumberType", ci);
            lbDebtorIDNumber3.Text = rm.GetString("lbDebtorIDNumber3", ci);
            lbIDNumberType2.Text = rm.GetString("lbIDNumberType", ci);
            lbSurnameEN.Text = rm.GetString("lbSurnameEN", ci);
            lbFirstNameEN.Text = rm.GetString("lbFirstNameEN", ci);
            lbPostalCode.Text = rm.GetString("lbPostalCode", ci);
            lbPostBox.Text = rm.GetString("lbPostBox", ci);
            lbAddress2Header.Text = rm.GetString("lbAddress2", ci);
            lbAddress2.Text = rm.GetString("lbAddress2", ci);
            lbCity2.Text = rm.GetString("lbCity2", ci);
            lbAddress1Header.Text = rm.GetString("lbAddress1", ci);
            lbAddress1.Text = rm.GetString("lbAddress1", ci);
            ltPostalCode2.Text = rm.GetString("lbPostalCode2", ci);
            lbProfession.Text = rm.GetString("lbProfession", ci);
            lbEducation.Text = rm.GetString("lbEducation", ci);
            lbPostBox2.Text = rm.GetString("lbPostBox2", ci);
            lbAddress2Info.Text = rm.GetString("lbAddress2Info", ci);
            lbAddress1Info.Text = rm.GetString("lbAddress1Info", ci);
            lbPhoneNumber1.Text = rm.GetString("lbPhoneNumber1", ci);
            lbPhonNumber1Type.Text = rm.GetString("lbIDNumberType", ci);
            lbPhoneNumber2.Text = rm.GetString("lbPhoneNumber2", ci);
            lbPhonenumber2Type.Text = rm.GetString("lbIDNumberType", ci);
            lbPhoneNumber3.Text = rm.GetString("lbPhoneNumber3", ci);
            lbPhoneNumber3Type.Text = rm.GetString("lbIDNumberType", ci);
            lbPhoneNumber4.Text = rm.GetString("lbPhoneNumber4", ci);
            lbPhoneNumber4Type.Text = rm.GetString("lbIDNumberType", ci);
            lbDebtorCIID.Text = rm.GetString("lbDebtorID", ci);
            lbAddress1EN.Text = rm.GetString("lbAddress1EN", ci);
            lbCity1.Text = rm.GetString("lbCity1", ci);
            lbAddress1InfoEN.Text = rm.GetString("lbAddress1InfoEN", ci);
            lbAddress2EN.Text = rm.GetString("lbAddress2EN", ci);
            lbAddress2InfoEN.Text = rm.GetString("lbAddress2InfoEN", ci);
            RequiredFieldValidator1.ErrorMessage = rm.GetString("RequiredFieldValidator1", ci);
            RequiredFieldValidator2.ErrorMessage = rm.GetString("RequiredFieldValidator1", ci);
            RequiredFieldValidator3.ErrorMessage = rm.GetString("RequiredFieldValidator1", ci);
            lbNewIndividual.Text = rm.GetString("txtNewIndividual", ci);
            lbEmail.Text = rm.GetString("txtEmail", ci);
            lbAdditionalInfo.Text = rm.GetString("lbAdditionalInfo", ci);
            lblNote.Text = rm.GetString("lblNote", ci);
            rfvCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvCity2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblCitySearch.Text = rm.GetString("txtSearch", ci);
            lblCity2Search.Text = rm.GetString("txtSearch", ci);
        }

        private void AddEnterEvent() {
            var frm = FindControl("newuser");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void btnCitySearch_Click(object sender, EventArgs e) {
            if (txtCitySearch.Text.Trim() == "") {
                return;
            }
            var dsCity = myFact.GetCityListAsDataSet(txtCitySearch.Text, EN);
            BindCityList(dsCity);
        }

        private void btnCity2Search_Click(object sender, EventArgs e) {
            if (txtCity2Search.Text.Trim() == "") {
                return;
            }
            var dsCity = myFact.GetCityListAsDataSet(txtCity2Search.Text, EN);
            BindCity2List(dsCity);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCitySearch.Click += new System.EventHandler(this.btnCitySearch_Click);
            this.btnCity2Search.Click += new System.EventHandler(this.btnCity2Search_Click);
            this.btRebDebtor.Click += new System.EventHandler(this.btRebDebtor_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}