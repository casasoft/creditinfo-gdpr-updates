#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Localization;

#endregion

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for ClaimOverView.
    /// </summary>
    public class ClaimOverView : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected DataGrid DgClaimsOverView;
        protected HtmlGenericControl divDataGrid;
        private bool EN;
        protected Label lbAllRegClaims24h;
        protected Label lbColorBouchedCBA;
        protected Label lbColorCancelled;
        protected Label lbColorDefinitions;
        protected Label lbColorRegistered;
        protected Label lblDatagridIcons;
        protected Label lbLetterColor;
        protected HtmlTableCell tdNameSearchGrid;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
            Page.ID = "201";
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();

            var myFact = new NPaymentsFactory();
            var mySet = myFact.GetClaimOverViewListAsDataSet();
            Session["dsClaimOverView"] = mySet.Tables[0];
            //DataSet myLetterSet = myFact.GetLetterClaimOverViewListAsDataSet();
            DgClaimsOverView.DataSource = mySet;
            SetColumnsByCulture();
            DgClaimsOverView.DataBind();

            // gera samsvarandi bind fyrir letters datagriddiğ			
        }

        private void LocalizeText() {
            lbAllRegClaims24h.Text = rm.GetString("txtRegClaims24h", ci);
            lbColorDefinitions.Text = rm.GetString("txtColorDefinitions", ci);
            lbLetterColor.Text = rm.GetString("txtLetter", ci);
            lbColorRegistered.Text = rm.GetString("txtRegistered", ci);
            lbColorCancelled.Text = rm.GetString("txtCancelledDeRegistered", ci);
            lbColorBouchedCBA.Text = rm.GetString("txtBounchedChequeWithCBA", ci);
        }

        private void SetColumnsByCulture() {
            if (EN) {
                DgClaimsOverView.Columns[0].Visible = true;
                DgClaimsOverView.Columns[1].Visible = true;
                DgClaimsOverView.Columns[2].Visible = true;
                DgClaimsOverView.Columns[3].Visible = true;
                DgClaimsOverView.Columns[4].Visible = true;
                DgClaimsOverView.Columns[5].Visible = false;
                DgClaimsOverView.Columns[6].Visible = false;
                DgClaimsOverView.Columns[7].Visible = true;
                DgClaimsOverView.Columns[8].Visible = false;
                DgClaimsOverView.Columns[9].Visible = true;
                DgClaimsOverView.Columns[10].Visible = true;
                DgClaimsOverView.Columns[11].Visible = false;
                DgClaimsOverView.Columns[12].Visible = true;
            } else {
                DgClaimsOverView.Columns[0].Visible = true;
                DgClaimsOverView.Columns[1].Visible = true;
                DgClaimsOverView.Columns[2].Visible = true;
                DgClaimsOverView.Columns[3].Visible = true;
                DgClaimsOverView.Columns[4].Visible = true;
                DgClaimsOverView.Columns[5].Visible = false;
                DgClaimsOverView.Columns[6].Visible = true;
                DgClaimsOverView.Columns[7].Visible = false;
                DgClaimsOverView.Columns[8].Visible = true;
                DgClaimsOverView.Columns[9].Visible = false;
                DgClaimsOverView.Columns[10].Visible = true;
                DgClaimsOverView.Columns[11].Visible = true;
                DgClaimsOverView.Columns[12].Visible = false;

                SetColumnsHeaderByCulture();
            }
        }

        private void SetColumnsHeaderByCulture() {
            DgClaimsOverView.Columns[1].HeaderText = rm.GetString("lbClaimID", ci);
            DgClaimsOverView.Columns[2].HeaderText = rm.GetString("lbDebtorID", ci);
            DgClaimsOverView.Columns[3].HeaderText = rm.GetString("lbName", ci);
            DgClaimsOverView.Columns[4].HeaderText = rm.GetString("lbClaimOwnerName", ci);
            DgClaimsOverView.Columns[5].HeaderText = rm.GetString("lbClaimOwnerName", ci);
            DgClaimsOverView.Columns[6].HeaderText = rm.GetString("lbType", ci);
            DgClaimsOverView.Columns[7].HeaderText = rm.GetString("lbType", ci);
            DgClaimsOverView.Columns[8].HeaderText = rm.GetString("lbInfoOrigin", ci);
            DgClaimsOverView.Columns[10].HeaderText = rm.GetString("txtRegDate", ci);
            DgClaimsOverView.Columns[11].HeaderText = rm.GetString("txtState", ci);
            DgClaimsOverView.Columns[12].HeaderText = rm.GetString("txtState", ci);
            DgClaimsOverView.Columns[30].HeaderText = rm.GetString("lbCaseID", ci);
        }

        private void DgClaimsOverView_ItemCommand(object source, DataGridCommandEventArgs e) {
            try {
                if (e.CommandName == "Select") {
                    int claimID = int.Parse(e.Item.Cells[1].Text);
                    int creditInfoID = int.Parse(e.Item.Cells[2].Text);
                    int caseID = int.Parse(e.Item.Cells[13].Text);
                    //Remove the dataset from session
                    Session.Remove("dsClaimOverView");
                    Session["ClaimID"] = claimID;
                    Session["CreditInfoID"] = creditInfoID;
                    Session["ClaimStatus"] = int.Parse(e.Item.Cells[14].Text);
                    // Transfer to current page 
                    Server.Transfer(
                        CigConfig.Configure("lookupsettings.redirectpageID" + caseID) + "?update=true&pageid=10");
                }
            } catch (ThreadAbortException) {
                // when using Server.Trandsfer this is an "normal" exception
            } catch (Exception err) // catch int.Parse error when sorting ...
            {
                Logger.WriteToLog("ClaimOverView.aspx, DgClaimsOverView_ItemCommand " + err.Message, true);
            }
        }

        private void DgClaimsOverView_SortCommand(object source, DataGridSortCommandEventArgs e) {
            try {
                var myTable = (DataTable) Session["dsClaimOverView"];
                var myView = myTable.DefaultView;
                myView.Sort = e.SortExpression;
                DgClaimsOverView.DataSource = myView;
                DgClaimsOverView.DataBind();
            } catch (Exception err) {
                Logger.WriteToLog("ClaimOverView.aspx, DgClaimsOverView_SortCommand " + err.Message, true);
            }
        }

        private void DgClaimsOverView_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (e.Item.Cells[14].Text == "2") {
                    e.Item.BackColor = Color.FromName("#cfcfcf");
                } else if (e.Item.Cells[14].Text == "4") {
                    e.Item.BackColor = Color.FromName("#cdc673");
                } else if (e.Item.Cells[14].Text == "6") {
                    e.Item.BackColor = Color.FromName("#cd661d");
                }
                if (e.Item.Cells[14].Text == "True") {
                    e.Item.Cells[5].BackColor = Color.FromName("#9ac0cd");
                    e.Item.Cells[6].BackColor = Color.FromName("#9ac0cd");
                }
                //String creditInfoID = e.Item.Cells[2].Text;
                // get debtors name
                try {
                    //int nCreditInfoID = int.Parse(creditInfoID);
                    //e.Item.Cells[3].Text = GetDebtorName(nCreditInfoID, e.Item.Cells[16].Text);
                    if (e.Item.Cells[16].Text.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                        if (EN) {
                            if (e.Item.Cells[20].Text != "" && e.Item.Cells[20].Text != "&nbsp;") {
                                e.Item.Cells[3].Text = e.Item.Cells[20].Text;
                                if (e.Item.Cells[21].Text != "" && e.Item.Cells[21].Text != "&nbsp;") {
                                    e.Item.Cells[3].Text += " " + e.Item.Cells[21].Text;
                                }
                            } else {
                                e.Item.Cells[3].Text = e.Item.Cells[18].Text;
                                if (e.Item.Cells[19].Text != "" && e.Item.Cells[19].Text != "&nbsp;") {
                                    e.Item.Cells[3].Text += " " + e.Item.Cells[19].Text;
                                }
                            }
                        } else {
                            if (e.Item.Cells[18].Text != "" && e.Item.Cells[18].Text != "&nbsp;") {
                                e.Item.Cells[3].Text = e.Item.Cells[18].Text;
                                if (e.Item.Cells[19].Text != "" && e.Item.Cells[19].Text != "&nbsp;") {
                                    e.Item.Cells[3].Text += " " + e.Item.Cells[19].Text;
                                }
                            } else {
                                e.Item.Cells[3].Text = e.Item.Cells[20].Text;
                                if (e.Item.Cells[21].Text != "" && e.Item.Cells[21].Text != "&nbsp;") {
                                    e.Item.Cells[3].Text += " " + e.Item.Cells[21].Text;
                                }
                            }
                        }
                    } else {
                        if (EN) {
                            if (e.Item.Cells[23].Text != "" && e.Item.Cells[23].Text != "&nbsp;") {
                                e.Item.Cells[3].Text = e.Item.Cells[23].Text;
                            } else {
                                e.Item.Cells[3].Text = e.Item.Cells[22].Text;
                            }
                        } else {
                            if (e.Item.Cells[22].Text != "" && e.Item.Cells[22].Text != "&nbsp;") {
                                e.Item.Cells[3].Text = e.Item.Cells[22].Text;
                            } else {
                                e.Item.Cells[3].Text = e.Item.Cells[23].Text;
                            }
                        }
                    }
                } catch {
                    // just using this for check if it is a valid integer, no need to do some special handling here...
                }
                try {
                    //Get the claim owner
                    //int claimOwnerCIID = int.Parse(e.Item.Cells[5].Text);
                    //e.Item.Cells[4].Text = GetDebtorName(claimOwnerCIID, e.Item.Cells[17].Text);
                    if (e.Item.Cells[17].Text.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                        if (EN) {
                            if (e.Item.Cells[26].Text != "" && e.Item.Cells[26].Text != "&nbsp;") {
                                e.Item.Cells[4].Text = e.Item.Cells[26].Text;
                                if (e.Item.Cells[27].Text != "" && e.Item.Cells[27].Text != "&nbsp;") {
                                    e.Item.Cells[4].Text += " " + e.Item.Cells[27].Text;
                                }
                            } else {
                                e.Item.Cells[4].Text = e.Item.Cells[24].Text;
                                if (e.Item.Cells[25].Text != "" && e.Item.Cells[25].Text != "&nbsp;") {
                                    e.Item.Cells[4].Text += " " + e.Item.Cells[25].Text;
                                }
                            }
                        } else {
                            if (e.Item.Cells[24].Text != "" && e.Item.Cells[24].Text != "&nbsp;") {
                                e.Item.Cells[4].Text = e.Item.Cells[24].Text;
                                if (e.Item.Cells[25].Text != "" && e.Item.Cells[25].Text != "&nbsp;") {
                                    e.Item.Cells[4].Text += " " + e.Item.Cells[25].Text;
                                }
                            } else {
                                e.Item.Cells[4].Text = e.Item.Cells[26].Text;
                                if (e.Item.Cells[27].Text != "" && e.Item.Cells[27].Text != "&nbsp;") {
                                    e.Item.Cells[4].Text += " " + e.Item.Cells[27].Text;
                                }
                            }
                        }
                    } else {
                        if (EN) {
                            if (e.Item.Cells[29].Text != "" && e.Item.Cells[29].Text != "&nbsp;") {
                                e.Item.Cells[4].Text = e.Item.Cells[29].Text;
                            } else {
                                e.Item.Cells[4].Text = e.Item.Cells[28].Text;
                            }
                        } else {
                            if (e.Item.Cells[28].Text != "" && e.Item.Cells[28].Text != "&nbsp;") {
                                e.Item.Cells[4].Text = e.Item.Cells[28].Text;
                            } else {
                                e.Item.Cells[4].Text = e.Item.Cells[29].Text;
                            }
                        }
                    }
                } catch {
                    // just using this for check if it is a valid integer, no need to do some special handling here...
                }
                WebDesign.CreateExplanationIcons(DgClaimsOverView.Columns, lblDatagridIcons, rm, ci);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.DgClaimsOverView.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DgClaimsOverView_ItemCommand);
            this.DgClaimsOverView.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.DgClaimsOverView_SortCommand);
            this.DgClaimsOverView.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.DgClaimsOverView_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

        /// <summary>
        /// Get debtors name for each case in caseOverview. Quick and dirty solution though...
        /// </summary>
        /*private String GetDebtorName(int creditInfoID, string type)
		{
			//String type = myFact.GetCIUserType(creditInfoID);
			String debtorName = "";
			if(type.Trim().Equals("-1")) // no type found
				type = CigConfig.Configure("lookupsettings.individualID"); // then set to individual as default
			if(type.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) 
			{
				Indivitual myIndi = myFact.GetIndivitual(creditInfoID);
				if(EN) 
				{
					if(myIndi.FirstNameEN==null||myIndi.FirstNameEN.Trim()=="")
					{
						debtorName = myIndi.FirstNameNative;
						debtorName += " " + myIndi.SurNameNative;						
					}
					else
					{
						debtorName = myIndi.FirstNameEN;
						debtorName += " " + myIndi.SurNameEN;						
					}
				} 
				else  
				{
					if(myIndi.FirstNameNative==null||myIndi.FirstNameNative.Trim()=="")
					{
						debtorName = myIndi.FirstNameEN;
						debtorName += " " + myIndi.SurNameEN;						
					}
					else
					{
						debtorName = myIndi.FirstNameNative;
						debtorName += " " + myIndi.SurNameNative;
						
					}
				}
			}
			else if(type.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) 
			{ // company
				Company myComp = myFact.GetCompany(creditInfoID);
				if(EN)
				{
					if(myComp.NameEN==null||myComp.NameEN.Trim()=="")
						debtorName = myComp.NameNative;
					else
						debtorName = myComp.NameEN;
				}
				else 
				{
					if(myComp.NameNative==null||myComp.NameNative.Trim()=="")
						debtorName = myComp.NameEN;
					else
						debtorName = myComp.NameNative;
				}
			}
			return debtorName;
		}*/
    }
}