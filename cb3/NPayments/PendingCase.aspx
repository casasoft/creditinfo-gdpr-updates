<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="PendingCase.aspx.cs" AutoEventWireup="false" Inherits="NPayments.PendingCase" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PendingCase</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						PendingCase.btnSubmit.click(); 
					}
				} 
				function SetFormFocus()
				{
					document.PendingCase.txtDefendantID.focus();
				}
		</SCRIPT>
	</HEAD>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="PendingCase" name="PendingCase" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblPengindCase" runat="server">Pending case</asp:label>
															<asp:label id="lblCaseID" runat="server" visible="False">ClaimID</asp:label>
															<asp:label id="lblStatus" runat="server" visible="False">ClaimID</asp:label>
															<asp:label id="lblIDOfTheClaim" runat="server" visible="False">-1</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 25%">
																		<asp:label id="lblDefendantID" runat="server">Defendant ID</asp:label>
																		<br>
																		<asp:textbox id="txtDefendantID" runat="server" maxlength="12"></asp:textbox>
																	</td>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblDefendantName" runat="server">Defendant name</asp:label><BR>
																		<asp:textbox id="txtDefendantName" runat="server" Enabled="False"></asp:textbox></TD>
																	<td style="WIDTH: 25%">
																		<asp:label id="lblPlaintiffID" runat="server">Plaintiff ID</asp:label>
																		<br>
																		<asp:textbox id="txtPlaintiffID" runat="server"></asp:textbox>
																	</td>
																	<TD>
																		<asp:label id="lblPlaintiffName" runat="server">Plaintiff name</asp:label><BR>
																		<asp:textbox id="txtPlaintiffName" runat="server" maxlength="30" Enabled="False"></asp:textbox></TD>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblCaseNumber" runat="server">Case number</asp:label>
																		<br>
																		<asp:textbox id="txtCaseNumber" runat="server"></asp:textbox>
																	</td>
																	<TD>
																		<asp:label id="lblDateRegistered" runat="server">Registed</asp:label><BR>
																		<asp:textbox id="txtRegDate" runat="server"></asp:textbox>&nbsp; <input onclick="PopupPicker('txtRegDate', 250, 250);" type="button" value="..." class="popup"></TD>
																	<td colSpan="2">
																		<asp:label id="lblValueOfSuit" runat="server">Value of suit</asp:label>
																		<br>
																		<asp:textbox id="txtValueOfSuit" runat="server" maxlength="10"></asp:textbox>&nbsp;<asp:dropdownlist id="ddCurrency" runat="server" CssClass="short_input"></asp:dropdownlist>
																	</td>
																</tr>
																<tr>
																	<td colSpan="2">
																		<asp:label id="lblInfoSource" runat="server">Information source</asp:label><BR>
																		<asp:dropdownlist id="ddInfoSource" runat="server" cssclass="long_input"></asp:dropdownlist>
																	</td>
																	<td colSpan="2">
																		<asp:label id="lblAdjudicator" runat="server">Adjudicator</asp:label>
																		<br>
																		<asp:dropdownlist id="ddAdjudicator" runat="server" cssclass="long_input"></asp:dropdownlist>
																	</td>
																</tr>
																<tr>
																	<td colSpan="4">
																		<asp:label id="lbComment" runat="server">Comment</asp:label><BR>
																		<asp:textbox id="txtComment" runat="server" maxlength="200" tooltip="NOTE! This comment will be publicly viewable."
																			height="140px" textmode="MultiLine" width="100%"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:label id="lblMsg" runat="server" visible="False" cssclass="error_text">New Claim has been registered</asp:label>
															<asp:validationsummary id="ValidationSummary1" runat="server" width="360px" height="26px"></asp:validationsummary>
														</td>
														<td align="right">
															<asp:button id="btnCloseCase" runat="server" cssclass="cancel_button" text="Close case"></asp:button><asp:button id="btnNew" runat="server" cssclass="gray_button" text="New" causesvalidation="False"></asp:button><asp:button id="btnSubmit" runat="server" cssclass="confirm_button" text="submit"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
