#region

using System;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Localization;
using NPayments.BLL.PendingCases;

#endregion

using Cig.Framework.Base.Configuration;


namespace NPayments {
    public class FindPendingCase : Page {
        protected Button btnClear;
        protected Button btnFind;
        private CultureInfo ci;
        protected NPaymentsFactory claimFactory = new NPaymentsFactory();
        private string culture;
        protected DropDownList ddAdjudicator;
        protected DropDownList ddCaseStatus;
        protected DropDownList ddInfoSource;
        protected DataGrid dgPendingCases;
        protected Label lblAdjudicator;
        protected Label lblCaseNumber;
        protected Label lblCaseStatus;
        protected Label lblDatagridIcons;
        protected Label lblDateFrom;
        protected Label lblDateRegistered;
        protected Label lblDateTo;
        protected Label lblDefendantID;
        protected Label lblDefendantName;
        protected Label lblDgResults;
        protected Label lblFindPendingCase;
        protected Label lblInfoSource;
        protected Label lblMsg;
        protected Label lblPlaintiffID;
        protected Label lblPlaintiffName;
        protected Label lblSearchByRef;
        private bool nativeCult;
        private ResourceManager rm;
        protected HtmlTable SearchGrid;
        protected HtmlTableCell Td2;
        protected HtmlTableRow trSearchGrid;
        protected TextBox txtCaseNumber;
        protected TextBox txtDateFrom;
        protected TextBox txtDateTo;
        protected TextBox txtDefendantID;
        protected TextBox txtDefendantName;
        protected TextBox txtPCID;
        protected TextBox txtPlaintiffID;
        protected TextBox txtPlaintiffName;
        protected TextBox txtReference;
        protected TextBox txtRegDate;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            Page.ID = "251";

            trSearchGrid.Visible = false;

            if (!Page.IsPostBack) {
                InitDDBoxes();
                LocalizeText();
                LocalizeGridHeader();
            }
        }

        private void LocalizeText() {
            lblDefendantID.Text = rm.GetString("txtDefendantID", ci);
            lblDefendantName.Text = rm.GetString("txtDefendantName", ci);
            lblPlaintiffID.Text = rm.GetString("txtPlaintiffID", ci);
            lblPlaintiffName.Text = rm.GetString("txtPlaintiffName", ci);
            lblCaseNumber.Text = rm.GetString("txtCaseNumber", ci);
            lblInfoSource.Text = rm.GetString("txtInformationSource", ci);
            lblDateRegistered.Text = rm.GetString("txtDateRegistered", ci);
            lblAdjudicator.Text = rm.GetString("txtAdjudicator", ci);
            lblFindPendingCase.Text = rm.GetString("txtFindPendingCase", ci);
            btnFind.Text = rm.GetString("txtSearch", ci);
            btnClear.Text = rm.GetString("txtClear", ci);
            lblCaseStatus.Text = rm.GetString("txtStatus", ci);
            lblSearchByRef.Text = rm.GetString("txtCIReference", ci);
            lblDateFrom.Text = rm.GetString("txtBeginDate", ci);
            lblDateTo.Text = rm.GetString("txtEndDate", ci);
            lblDgResults.Text = rm.GetString("lblDgResults", ci);
        }

        private void LocalizeGridHeader() {
            dgPendingCases.Columns[2].HeaderText = rm.GetString("txtDefendantID", ci);
            dgPendingCases.Columns[3].HeaderText = rm.GetString("txtDefendantName", ci);
            dgPendingCases.Columns[4].HeaderText = rm.GetString("txtPlaintiffID", ci);
            dgPendingCases.Columns[5].HeaderText = rm.GetString("txtPlaintiffName", ci);
            dgPendingCases.Columns[6].HeaderText = rm.GetString("txtInformationSource", ci);
            dgPendingCases.Columns[9].HeaderText = rm.GetString("txtAdjudicator", ci);
            dgPendingCases.Columns[12].HeaderText = rm.GetString("txtDateRegistered", ci);
            dgPendingCases.Columns[13].HeaderText = rm.GetString("txtCaseNumber", ci);
        }

        private void btnFind_Click(object sender, EventArgs e) {
            var pCase = new PendingCaseBLLC
                        {
                            DefendantID = txtDefendantID.Text,
                            DefendantName = txtDefendantName.Text,
                            PlaintiffID = txtPlaintiffID.Text,
                            PlaintiffName = txtPlaintiffName.Text
                        };
            if (ddCaseStatus.SelectedIndex > 0) {
                pCase.StatusID = int.Parse(ddCaseStatus.SelectedValue);
            } else {
                pCase.StatusID = -1;
            }

            if (ddInfoSource.SelectedIndex > 0) {
                pCase.Court = int.Parse(ddInfoSource.SelectedValue);
            } else {
                pCase.Court = -1;
            }

            if (ddAdjudicator.SelectedIndex > 0) {
                pCase.Adjudicator = int.Parse(ddAdjudicator.SelectedValue);
            } else {
                pCase.Adjudicator = -1;
            }

            if (txtReference.Text.Length > 0) {
                try {
                    pCase.ID = int.Parse(txtReference.Text);
                } catch (Exception) {
                    pCase.ID = -1;
                }
            }

            pCase.DateOfWrit = txtRegDate.Text.Trim() != "" ? DateTime.Parse(txtRegDate.Text) : DateTime.MinValue;
            pCase.WritNumber = txtCaseNumber.Text;

            var dFrom = DateTime.MinValue;
            var dTo = DateTime.MinValue;
            if (txtDateFrom.Text.Trim() != "") {
                dFrom = DateTime.Parse(txtDateFrom.Text);
            }

            if (txtDateTo.Text.Trim() != "") {
                dTo = DateTime.Parse(txtDateTo.Text);
            }

            var dt = claimFactory.FindPendingCase(pCase, dFrom, dTo);
            if (dt == null || dt.Rows.Count <= 0) {
                return;
            }
            dgPendingCases.DataSource = dt;
            dgPendingCases.DataBind();
            trSearchGrid.Visible = true;
        }

        private void HideSearchGrid() {
            dgPendingCases.DataSource = null;
            dgPendingCases.DataBind();
            trSearchGrid.Visible = false;
        }

        public void InitDDBoxes() {
            //Case Status dropdwon
            ddCaseStatus.DataSource = claimFactory.GetAllPendingCaseStatusAsDataTable();
            ddCaseStatus.DataValueField = "StatusId";
            ddCaseStatus.DataTextField = nativeCult ? "StateNative" : "StateEn";
            ddCaseStatus.DataBind();
            ddCaseStatus.Items.Insert(0, "Any status");

            //Information source dropdown
            ddInfoSource.DataSource = claimFactory.GetInformationSourceListAsDataSet();
            ddInfoSource.DataTextField = nativeCult ? "NameNative" : "NameEN";

            ddInfoSource.DataValueField = "InformationSourceID";
            ddInfoSource.DataBind();
            ddInfoSource.Items.Insert(0, "Any Source");

            //Adjudicator
            var adjTable = claimFactory.GetAdjudicatorsAsDataTable();
            adjTable.DefaultView.Sort = nativeCult ? "AdjudicatorNameNative" : "AdjudicatorNameEN";

            ddAdjudicator.DataSource = adjTable;
            ddAdjudicator.DataValueField = "ID";
            ddAdjudicator.DataTextField = nativeCult ? "AdjudicatorNameNative" : "AdjudicatorNameEN";

            ddAdjudicator.DataBind();
            ddAdjudicator.Items.Insert(0, "Any Adjudicator");
        }

        private void dgPendingCases_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (!e.CommandName.Equals("Select")) {
                return;
            }
            int id = -1;

            if (e.Item.Cells[1].Text != "") {
                try {
                    id = int.Parse(e.Item.Cells[1].Text);
                } catch (Exception) {
                    id = -1;
                }
            }
            if (id != -1) {
                Server.Transfer("PendingCase.aspx?Update=true&pcID=" + id);
            }
        }

        private void dgPendingCases_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set info source - Native if available, else EN
                    if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }

                    //Set adjudictator - Native if available, else EN
                    if (e.Item.Cells[10].Text != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    }

                    //Set state - Native if available, else EN
                    if (e.Item.Cells[15].Text != "" && e.Item.Cells[15].Text != "&nbsp;") {
                        e.Item.Cells[14].Text = e.Item.Cells[15].Text;
                    } else {
                        e.Item.Cells[14].Text = e.Item.Cells[16].Text;
                    }
                } else {
                    //Set info source - EN if available, else native
                    if (e.Item.Cells[8].Text != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }

                    //Set adjudictator - EN if available, else native
                    if (e.Item.Cells[11].Text != "" && e.Item.Cells[11].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    }

                    //Set state - Native if available, else EN
                    if (e.Item.Cells[16].Text != "" && e.Item.Cells[16].Text != "&nbsp;") {
                        e.Item.Cells[14].Text = e.Item.Cells[16].Text;
                    } else {
                        e.Item.Cells[14].Text = e.Item.Cells[15].Text;
                    }
                }

                //Check if available claim with same case and source - the mark the case with color
                var pCase = new PendingCaseBLLC {ID = int.Parse(e.Item.Cells[1].Text)};
                if (pCase.IsAvailableClaimWithSameCaseNumberAndSource()) {
                    e.Item.BackColor = Color.Red;
                }
            }

            WebDesign.CreateExplanationIcons(dgPendingCases.Columns, lblDatagridIcons, rm, ci);
        }

        private void btnClear_Click(object sender, EventArgs e) {
            txtDefendantID.Text = "";
            txtDefendantName.Text = "";
            txtPlaintiffID.Text = "";
            txtPlaintiffName.Text = "";

            txtRegDate.Text = "";
            txtCaseNumber.Text = "";

            ddInfoSource.SelectedIndex = 0;
            ddAdjudicator.SelectedIndex = 0;
            HideSearchGrid();
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.dgPendingCases.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgPendingCases_ItemCommand);
            this.dgPendingCases.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgPendingCases_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}