<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="FindCIUser.aspx.cs" AutoEventWireup="false" Inherits="NPayments.FindDebtor" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>FindDebtor</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta http-equiv="expires" content="-1">
		<meta http-equiv="pragma" content="no-cache">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>
			function checkEnterKey() 
			{    
				if (event.keyCode == 13) 
				{        
					event.cancelBubble = true;
					event.returnValue = false;
					vansk.btSearchDebtor.click(); 
				}
			} 
			function SetFormFocus()
				{
					document.vansk.tbIDNumber.focus();
				}
		</script>
	</HEAD>
	<body onload="SetFormFocus()">
		<form id="vansk" name="vansk" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options2" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="Userinfo2" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lbIndividualSearch" runat="server">Individual search</asp:label></th></tr>
													<tr>
														<td>
															<TABLE class="fields" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td style="HEIGHT: 19px" colSpan="4"><b><asp:label id="lbSearchBy" runat="server">Search by:</asp:label></b></td>
																</tr>
																<TR>
																	<td style="WIDTH: 382px; HEIGHT: 41px" colSpan="2">
																		<TABLE id="Table1" style="WIDTH: 254px" cellSpacing="1" cellPadding="1" width="254" border="0">
																			<TR>
																				<TD style="WIDTH: 134px"><asp:label id="lbIDNumber" runat="server"> IDNumber</asp:label><BR>
																					<asp:textbox id="tbIDNumber" runat="server"></asp:textbox></TD>
																				<TD><asp:label id="lbNumberType" runat="server">IDNumberType</asp:label><BR>
																					<asp:dropdownlist id="ddIDNumberType" runat="server"></asp:dropdownlist></TD>
																			</TR>
																		</TABLE>
																	<td style="WIDTH: 5px; HEIGHT: 41px" colSpan="1">
																		<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="300" border="0">
																			<TR>
																				<TD><asp:label id="lbCIID" runat="server">CreditInfoID</asp:label><BR>
																					<asp:textbox id="tbCIID" runat="server"></asp:textbox></TD>
																			</TR>
																		</TABLE>
																	</td>
																<TR>
																	<td style="WIDTH: 382px; HEIGHT: 10px" colSpan="2"><asp:label id="lbDebtorFirstName" runat="server">First name</asp:label><BR>
																		<asp:textbox id="tbDebtorFirstName" runat="server"></asp:textbox></td>
																	<td style="HEIGHT: 10px" colSpan="2"><asp:label id="lbDebtorSurName" runat="server">Surname</asp:label><BR>
																		<asp:textbox id="tbDebtorSurName" runat="server" MaxLength="12"></asp:textbox></td>
																</TR>
																<TR>
																	<TD style="WIDTH: 382px; HEIGHT: 23px" colSpan="2"></TD>
																	<TD style="HEIGHT: 23px" align="right" colSpan="2"><asp:button id="btSearchDebtor" runat="server" Text="search" CssClass="search_button"></asp:button></TD>
																</TR>
																<TR>
																	<TD colSpan="4" height="23"></TD>
																</TR>
															</TABLE>
														</td>
													</tr>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table width="100%">
													<tr>
														<td>
															<asp:label id="lblError" runat="server" Visible="False" ForeColor="Red">Label</asp:label></td>
														<td style="HEIGHT: 20px" align="right"><asp:button id="btCancel" runat="server" Text="cancel" CssClass="cancel_button"></asp:button></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr id="trDgResults" runat="server">
											<td>
												<TABLE class="grid_table" id="SearchGrid" cellSpacing="0" cellPadding="0" runat="server">
													<tr>
														<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<TR>
														<TH>
															<asp:label id="lblDgResults" runat="server"></asp:label></TH></TR>
													<TR>
														<TD id="tdNameSearchGrid" width="100%" runat="server"><asp:datagrid id="dgDebtors" runat="server" Width="100%" ForeColor="Black" CssClass="rammi" GridLines="None"
																CellPadding="4" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" AutoGenerateColumns="False" AllowSorting="True">
																<FooterStyle CssClass="grid_footer"></FooterStyle>
																<SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
																<AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
																<ItemStyle CssClass="grid_item"></ItemStyle>
																<HeaderStyle CssClass="grid_header"></HeaderStyle>
																<pagerstyle cssclass="grid_pager"></pagerstyle>
																<Columns>
																	<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																		CommandName="Select">
																		<ItemStyle CssClass="leftpadding"></ItemStyle>
																	</asp:ButtonColumn>
																	<asp:ButtonColumn Text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;"
																		CommandName="Update">
																		<ItemStyle CssClass="leftpadding"></ItemStyle>
																	</asp:ButtonColumn>
																	<asp:BoundColumn DataField="CreditInfoID" SortExpression="CreditInfoID" HeaderText="CreditInfoID">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="NameNative" SortExpression="NameNative" HeaderText="Name">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="NameEN" SortExpression="NameEN" HeaderText="Name EN">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="StreetNative" SortExpression="StreetNative" HeaderText="Street(N)">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="StreetEN" SortExpression="StreetEN" HeaderText="Street EN">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="Number" SortExpression="Number" HeaderText="National ID">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																</Columns>
															</asp:datagrid></TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><uc1:footer id="Footer2" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
