<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="NewChDcAof.aspx.cs" AutoEventWireup="false" Inherits="NPayments.NewChDcAof" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Creditinfo</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						vansk.btSubmit.click(); 
					}
				} 
				function SetFormFocus()
				{
					document.vansk.tbClaimOwnerNative.focus();
				}
		</script>
	</HEAD>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="vansk" name="vansk" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbChequesAndOther" runat="server">Cheques and other</asp:label><asp:label id="lbClaimID" runat="server" visible="False">ClaimID</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td><asp:label id="lbDebtor" runat="server" font-bold="True">Debtor</asp:label></td>
																</tr>
																<tr>
																	<td colspan="4"><asp:radiobutton id="rbtIndividual" runat="server" autopostback="True" groupname="DebtorType" cssclass="radio"></asp:radiobutton>
																		<asp:image id="imgIndividual" runat="server" imageurl="../img/individual.gif"></asp:image>&nbsp;<asp:radiobutton id="rbtCompany" runat="server" autopostback="True" groupname="DebtorType" cssclass="radio"></asp:radiobutton>
																		<asp:image id="imgCompany" runat="server" imageurl="../img/company.gif"></asp:image></td>
																</tr>
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lbDebtorID" runat="server">Debtor CIID</asp:label><br>
																		<asp:textbox id="tbDebtorID" runat="server" maxlength="12"></asp:textbox><asp:requiredfieldvalidator id="rfvCIID" runat="server" errormessage="Value is missing!" controltovalidate="tbDebtorID">*</asp:requiredfieldvalidator></td>
																	<td style="WIDTH: 25%"><asp:label id="lblNationalID" runat="server">National ID</asp:label><br>
																		<asp:textbox id="txtNationalID" runat="server" maxlength="12"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lbDebtorFirstName" runat="server">Debtor first name</asp:label><br>
																		<asp:textbox id="tbDebtorFirstName" runat="server"></asp:textbox></td>
																	<td><asp:label id="lbDebtorSurname" runat="server">Debtor Surname</asp:label><br>
																		<asp:textbox id="tbDebtorSurname" runat="server"></asp:textbox></td>
																</tr>
																<tr>
																	<td></td>
																	<td align="right" colspan="3"><asp:button id="btnSearchDebtor" runat="server" text="Search" cssclass="search_button" causesvalidation="False"></asp:button>&nbsp;
																		<input onclick="GoNordurNidur('tbDebtorID','txtNationalID','tbDebtorFirstName','tbDebtorSurname',document.vansk.rbtIndividual, 670, 900);"
																			type="button" value="..." class="popup"></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										<!-- setja her t�flu med nameresults -->
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableNameSearchGrid" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dgDebtors" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False"
																				allowsorting="True">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="Number" sortexpression="Number" headertext="National ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Name">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Street">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameNative" sortexpression="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameEN" sortexpression="NameEN" headertext="Name EN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetNative" sortexpression="StreetNative" headertext="Street(N)">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetEN" sortexpression="StreetEN" headertext="Street EN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CreditInfoID" sortexpression="CreditInfoID" headertext="CreditInfoID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td>
									<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
									<table class="grid_table" cellspacing="0" cellpadding="0">
										<tr>
											<th>
												<asp:label id="lblClaimOwner" runat="server">Claim owner</asp:label></th></tr>
										<tr>
											<td>
												<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
													<tr>
														<td colspan="2"><asp:radiobutton id="rbtnIndividualClaimOwner" runat="server" groupname="ClaimType" checked="True"
																cssclass="radio"></asp:radiobutton>
															<asp:image id="imgIndividualClaimOwner" runat="server" imageurl="../img/individual.gif"></asp:image>&nbsp;<asp:radiobutton id="rbtnCompanyClaimOwner" runat="server" groupname="ClaimType" cssclass="radio"></asp:radiobutton>
															<asp:image id="imgCompanyClaimOwner" runat="server" imageurl="../img/company.gif"></asp:image></td>
													</tr>
													<tr>
														<td style="WIDTH: 50%"><asp:label id="lblClaimOwnerCIID" runat="server">Claim owner CIID</asp:label><br>
															<asp:textbox id="txtClaimOwnerCIID" runat="server"></asp:textbox><asp:requiredfieldvalidator id="rfvClaimOwnerCIID" runat="server" errormessage="Value missing!" controltovalidate="txtClaimOwnerCIID">*</asp:requiredfieldvalidator></td>
														<td><asp:label id="lbClaimOwnerName" runat="server">Claim Owner</asp:label><br>
															<asp:textbox id="tbClaimOwnerNative" runat="server" maxlength="30"></asp:textbox></td>
													</tr>
													<tr>
														<td></td>
														<td align="right"><asp:button id="btnSearchClaimOwner" runat="server" text="Search" cssclass="search_button" causesvalidation="False"></asp:button>&nbsp;
															<input onclick="GoNordurNidur('txtClaimOwnerCIID','','tbClaimOwnerNative','',document.vansk.rbtnIndividualClaimOwner, 670, 900);"
																type="button" value="..." class="popup"></td>
													</tr>
													<tr>
														<td height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td>
									<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
									<table class="grid_table" cellspacing="0" cellpadding="0">
										<tr>
											<th>
												<asp:label id="lblInfo" runat="server">Info</asp:label></th></tr>
										<tr>
											<td>
												<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
													<tr>
														<td style="WIDTH: 50%"><asp:label id="lbAgentID" runat="server">Agent</asp:label><br>
															<asp:textbox id="tbAgent" runat="server"></asp:textbox></td>
														<td><asp:label id="lbType" runat="server">Type</asp:label><asp:dropdownlist id="ddType" runat="server"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
							</tr>
							<tr>
							</tr>
							<tr>
								<td>
									<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
									<table class="grid_table" id="tableClaimOwnerNameSearchGrid" cellspacing="0" cellpadding="0"
										runat="server">
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td align="right"><asp:label id="lblClaimOwnerNameSearchDatagridIcons" runat="server"></asp:label></td>
										</tr>
										<tr>
											<td height="5"></td>
										</tr>
										<tr>
											<th>
												<asp:label id="lblClaimOwnerNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
										<tr>
											<td>
												<table class="datagrid" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<div class="TA" id="divClaimOwner" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																runat="server"><asp:datagrid id="dgClaimOwner" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False"
																	allowsorting="True">
																	<footerstyle cssclass="grid_footer"></footerstyle>
																	<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																	<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																	<itemstyle cssclass="grid_item"></itemstyle>
																	<headerstyle cssclass="grid_header"></headerstyle>
																	<columns>
																		<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;"
																			commandname="Select">
																			<itemstyle cssclass="leftpadding"></itemstyle>
																		</asp:buttoncolumn>
																		<asp:boundcolumn datafield="Number" sortexpression="Number" headertext="National ID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Name">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn headertext="Street">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameNative" sortexpression="NameNative" headertext="NameNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="NameEN" sortexpression="NameEN" headertext="NameEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetNative" sortexpression="StreetNative" headertext="StreetNative">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="StreetEN" sortexpression="StreetEN" headertext="StreetEN">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn datafield="CreditInfoID" sortexpression="CreditInfoID" headertext="CreditInfoID">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																		<asp:boundcolumn datafield="IsCompany" headertext="IsCompany">
																			<itemstyle cssclass="padding"></itemstyle>
																		</asp:boundcolumn>
																	</columns>
																	<pagerstyle cssclass="grid_pager"></pagerstyle>
																</asp:datagrid></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END // TABLE FOR DATAGRID // END --></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td>
									<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
									<table class="grid_table" cellspacing="0" cellpadding="0">
										<tr>
											<th>
												<asp:label id="lbDates" runat="server">Dates</asp:label></th></tr>
										<tr>
											<td>
												<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
													<tr>
														<td style="WIDTH: 25%"><asp:label id="lbRegisted" runat="server">Registed</asp:label><br>
															<asp:textbox id="tbRegDate" runat="server" enabled="False"></asp:textbox></td>
														<td style="WIDTH: 25%"><asp:label id="lbOnHold" runat="server">Delayed</asp:label><br>
															<asp:textbox id="tbClaimDelayedDate" runat="server"></asp:textbox>&nbsp;<input onclick="PopupPicker('tbClaimDelayedDate', 250, 250);" type="button" value="..."
																class="popup">
															<asp:customvalidator id="CustomValidator2" runat="server" errormessage="Date input incorrect" controltovalidate="tbClaimDelayedDate"
																display="Dynamic">*</asp:customvalidator></td>
														<td style="WIDTH: 25%"><asp:label id="lbClaim" runat="server">Claim</asp:label><br>
															<asp:textbox id="tbClaimDate" runat="server"></asp:textbox>&nbsp;<input onclick="PopupPicker('tbClaimDate', 250, 250);" type="button" value="..." class="popup">
															<asp:requiredfieldvalidator id="RequiredFieldValidator5" runat="server" errormessage="Value missing!" controltovalidate="tbClaimDate"
																display="Dynamic">*</asp:requiredfieldvalidator><asp:customvalidator id="CustomValidator1" runat="server" errormessage="Date input incorrect!" controltovalidate="tbClaimDate"
																display="Dynamic">*</asp:customvalidator></td>
														<td><asp:label id="lbPayment" runat="server">Payment</asp:label><br>
															<asp:textbox id="tbPaymentDate" runat="server"></asp:textbox>&nbsp;<input onclick="PopupPicker('tbPaymentDate', 250, 250);" type="button" value="..." class="popup">
															<asp:customvalidator id="CustomValidator3" runat="server" errormessage="Date input incorrect" controltovalidate="tbPaymentDate"
																display="Dynamic">*</asp:customvalidator></td>
													</tr>
													<tr>
														<td height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td>
									<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
									<table class="grid_table" cellspacing="0" cellpadding="0">
										<tr>
											<th>
												<asp:label id="lbOther" runat="server">Other info</asp:label></th></tr>
										<tr>
											<td>
												<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
													<tr>
														<td style="WIDTH: 25%"><asp:label id="lbClaimAmount" runat="server">Claim amount</asp:label><br>
															<asp:textbox id="tbClaimAmount" runat="server" maxlength="10" width="100px"></asp:textbox>&nbsp;<asp:dropdownlist id="ddCurrency" runat="server" cssclass="short_input"></asp:dropdownlist><asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" errormessage="Value missing!" controltovalidate="tbClaimAmount"
																display="Dynamic">*</asp:requiredfieldvalidator></td>
														<td style="WIDTH: 25%"><asp:label id="lbBank" runat="server">Issued Bank</asp:label><br>
															<asp:dropdownlist id="ddIssueBanks" runat="server"></asp:dropdownlist></td>
														<td style="WIDTH: 25%"><asp:label id="lbCaseID" runat="server">Case number</asp:label><br>
															<asp:textbox id="tbCaseID" runat="server"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" errormessage="Value missing!" controltovalidate="tbCaseID">*</asp:requiredfieldvalidator></td>
														<td><asp:checkbox id="chbCBR" runat="server" text="CBR" cssclass="radio"></asp:checkbox></td>
													</tr>
													<tr>
														<td height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td>
									<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
									<table class="grid_table" cellspacing="0" cellpadding="0">
										<tr>
											<th>
												<asp:label id="lbRegistration" runat="server">Registration</asp:label></th></tr>
										<tr>
											<td>
												<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
													<tr>
														<td style="WIDTH: 50%"><asp:label id="lbStatus" runat="server">Status</asp:label><br>
															<asp:textbox id="tbStatus" runat="server" readonly="True"></asp:textbox>
														</td>
														<td><asp:label id="lbRegister" runat="server">Register</asp:label><br>
															<asp:textbox id="tbRegister" runat="server" readonly="True"></asp:textbox></td>
													</tr>
													<tr>
														<td><asp:label id="lbComment" runat="server">Comment</asp:label><br>
															<asp:textbox id="tbComment" runat="server" maxlength="200" tooltip="NOTE! This comment will be publicly viewable."
																height="103px" textmode="MultiLine" width="306px"></asp:textbox></td>
														<td>
															<asp:label id="lbInternalComment" runat="server">InternalComment</asp:label>
															<br>
															<asp:textbox id="tbInternalComment" runat="server" width="306px" textmode="MultiLine" height="103px"></asp:textbox>
														</td>
													</tr>
													<tr>
														<td height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td>
									<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
									<table class="empty_table" cellspacing="0">
										<tr>
											<td align="left">
												<asp:label id="lblMsg" runat="server" visible="False" cssclass="confirm_text" ForeColor="Blue">New Claim has been registered</asp:label>
												<asp:validationsummary id="ValidationSummary1" runat="server" width="256px" height="26px"></asp:validationsummary>
												<asp:textbox id="tbPageID" runat="server" visible="False" height="9px"></asp:textbox>
											</td>
											<td align="right">
												<asp:button id="btnShowAll" runat="server" text="cancel" causesvalidation="False" cssclass="gray_button"></asp:button><asp:button id="btnSubmitAndClear" runat="server" text="submit &amp; clear" cssclass="confirm_button"></asp:button><asp:button id="btSubmit" runat="server" text="submit &amp; new" cssclass="confirm_button"></asp:button>
											</td>
										</tr>
									</table>
									<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
								</td>
							</tr>
						</table>
						<!-- Main Body Ends --></td>
				</tr>
				<tr>
					<td height="20"></td>
				</tr>
				<tr>
					<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
				</tr>
			</table>
			</td>
			<td width="2"></td>
			</tr>
			<tr>
				<td align="center" colspan="4">
					<uc1:footer id="Footer1" runat="server"></uc1:footer>
				</td>
			</tr>
			</table>
		</form>
	</body>
</HTML>
