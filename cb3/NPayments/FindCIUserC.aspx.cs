#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;

#endregion

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for FindCIUserC.
    /// </summary>
    public class FindCIUserC : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected Button btCancel;
        protected Button btSearchDebtor;
        protected DataGrid dgDebtors;
        private bool EN;
        protected HtmlForm Form2;
        protected Label lbCIID;
        protected Label lbCompanyName;
        protected Label lbCompanySearch;
        protected Label lblDatagridIcons;
        protected Label lblDgResults;
        protected Label lblError;
        protected Label lbNationalID;
        protected Label lbSearchBy;
        private NPaymentsFactory myFact = new NPaymentsFactory();
        public String redirectPage = "";
        protected HtmlTable SearchGrid;
        protected TextBox tbCIID;
        protected TextBox tbNameNative;
        protected TextBox tbNationalIDNumber;
        protected HtmlTableCell tdNameSearchGrid;
        protected HtmlTableRow trDgResults;

        private void Page_Load(object sender, EventArgs e) {
            lblError.Visible = false;
            Page.ID = "211";
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            trDgResults.Visible = false;

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            AddEnterEvent();
            LocalizeText();
            SetColumnsByCulture();
            if (Request.QueryString["pageid"] != null) {
                SetRedirectPage(int.Parse(Request.QueryString["pageid"]));
                //this.hlSearchCH.NavigateUrl = "SearchCHouse.aspx?pageid=" + Request.QueryString["pageid"];
            }
        }

        private void btSearchDebtor_Click(object sender, EventArgs e) {
            dgDebtors.CurrentPageIndex = 0;
            myFact = new NPaymentsFactory();
            var myComp = new Company();
            bool query = true;
            if (tbCIID.Text != "") {
                myComp.CreditInfoID = int.Parse(tbCIID.Text);
            }
            myComp.NationalID = tbNationalIDNumber.Text;
            myComp.NameNative = tbNameNative.Text;

            if (tbCIID.Text.Equals("") && myComp.NationalID.Equals("") && myComp.NameNative.Equals("")) {
                query = false;
            }

            try {
                if (query) {
                    DataSet customerSet = myFact.FindCompany(myComp);
                    dgDebtors.DataSource = customerSet.Tables[0].DefaultView;
                    if (!EN) {
                        SetColumnsHeaderByCulture();
                    }
                    Session["dgCompTable"] = customerSet.Tables[0];
                    dgDebtors.DataBind();

                    trDgResults.Visible = true;
                } else {
                    lblError.Text = rm.GetString("errMsg3", ci);
                    lblError.Visible = true;
                }
            } catch (Exception err) {
                Logger.WriteToLog("FindCIUser.aspx ", err, true);
            }
        }

        // display datagrid comlumns that match the locale
        private void SetColumnsByCulture() {
            if (EN) {
                dgDebtors.Columns[0].Visible = true;
                dgDebtors.Columns[1].Visible = true;
                dgDebtors.Columns[2].Visible = true;
                dgDebtors.Columns[4].Visible = true;
                dgDebtors.Columns[3].Visible = false;
                dgDebtors.Columns[5].Visible = true;
            } else {
                dgDebtors.Columns[0].Visible = true;
                dgDebtors.Columns[1].Visible = true;
                dgDebtors.Columns[2].Visible = true;
                dgDebtors.Columns[4].Visible = false;
                dgDebtors.Columns[3].Visible = true;
                dgDebtors.Columns[5].Visible = true;
            }
        }

        private void SetColumnsHeaderByCulture() {
            dgDebtors.Columns[2].HeaderText = rm.GetString("lbNationalID", ci);
            dgDebtors.Columns[4].HeaderText = rm.GetString("txtCompany(EN)", ci);
            dgDebtors.Columns[3].HeaderText = rm.GetString("txtCompany", ci);
            dgDebtors.Columns[5].HeaderText = rm.GetString("lbDebtorID", ci);
        }

        private void LocalizeText() {
            lbNationalID.Text = rm.GetString("lbNationalID", ci);
            lbCIID.Text = rm.GetString("lbDebtorID", ci);
            lbCompanyName.Text = rm.GetString("txtCompany", ci);
            btSearchDebtor.Text = rm.GetString("txtSearch", ci);
            btCancel.Text = rm.GetString("btnCancel", ci);
            lbSearchBy.Text = rm.GetString("lbSearchBy", ci);
            lbCompanySearch.Text = rm.GetString("txtCompanySearch", ci);
        }

        public void SetRedirectPage(int index) { redirectPage = CigConfig.Configure("lookupsettings.redirectpageID" + index); }

        private void AddEnterEvent() {
            var frm = FindControl("findcuser");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btSearchDebtor.Click += new System.EventHandler(this.btSearchDebtor_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}