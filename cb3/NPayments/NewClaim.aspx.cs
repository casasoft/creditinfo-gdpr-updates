using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Claims;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;
using Debtor=UserAdmin.BLL.Debtor;
using Image=System.Web.UI.WebControls.Image;

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for _Default.
    /// </summary>
    public class _Default : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly NPaymentsFactory myFact = new NPaymentsFactory();
        protected Button btnSearchClaimOwner;
        protected Button btnSearchDebtor;
        protected Button btnShowAll;
        protected Button btnSubmitAndClear;
        protected Button btSubmit;
        private int claimStatus;
        protected CustomValidator CustomValidator1;
        protected CustomValidator CustomValidator2;
        protected CustomValidator CustomValidator3;
        protected DropDownList ddCurrency;
        protected DropDownList ddInformationSourceList;
        protected DropDownList ddType;
        protected DataGrid dgClaimOwner;
        protected DataGrid dgDebtors;
        protected HtmlGenericControl divClaimOwner;
        protected HtmlGenericControl divDebtors;
        private bool EN;
        protected HyperLink hlNewDebtor;
        protected Image imgCompany;
        protected Image imgIndividual;
        protected Label lbAgentID;
        protected Label lbBankruptieH;
        protected Label lbCaseID;
        protected Label lbClaim;
        protected Label lbClaimAmount;
        protected Label lbClaimID;
        protected Label lbClaimOwnerName;
        protected Label lbComment;
        protected Label lbDates;
        protected Label lbDebtor;
        protected Label lbDebtorFirstName;
        protected Label lbDebtorID;
        protected Label lbDebtorSurname;
        protected Label lbGazettePage;
        protected Label lbGazetteYear;
        protected Label lbInfoOrigin;
        protected Label lbInternalComment;
        protected Label lblClaimOwner;
        protected Label lblClaimOwnerCIID;
        protected Label lblDatagridIcons;
        protected Label lblDatagridIcons2;
        protected Label lblDgResults;
        protected Label lblDgResults2;
        protected Label lblInfo;
        protected Label lblMessage;
        protected Label lblMsg;
        protected Label lblNationalID;
        protected Label lbOnHold;
        protected Label lbOther;
        protected Label lbPayment;
        protected Label lbRegisted;
        protected Label lbRegister;
        protected Label lbRegistration;
        protected Label lbStatus;
        protected Label lbType;
        private bool newRegistration = true;
        protected RadioButton rbtnCompanyClaimOwner;
        protected RadioButton rbtnIndividualClaimOwner;
        private string redirectPage = "";
        protected RequiredFieldValidator RequiredFieldValidator5;
        protected RequiredFieldValidator rfvCIID;
        protected RequiredFieldValidator rfvClaimOwnerCIID;
        protected HtmlTable SearchGrid;
        protected HtmlTable Table1;
        protected TextBox tbAgent;
        protected TextBox tbCaseID;
        protected TextBox tbClaimAmount;
        protected TextBox tbClaimDate;
        protected TextBox tbClaimDelayedDate;
        protected TextBox tbClaimOwnerNative;
        protected TextBox tbComment;
        protected TextBox tbDebtorFirstName;
        protected TextBox tbDebtorID;
        protected TextBox tbDebtorSurname;
        protected TextBox tbGazettePage;
        protected TextBox tbGazetteYear;
        protected TextBox tbInternalComment;
        protected TextBox tbPageID;
        protected TextBox tbPaymentDate;
        protected TextBox tbRegDate;
        protected TextBox tbRegister;
        protected TextBox tbStatus;
        protected HtmlTableCell Td2;
        protected HtmlTableRow trClaimOwnerNameSearchGrid;
        protected HtmlTableRow trDebtorNameSearchGrid;
        protected TextBox txtClaimOwnerCIID;
        protected TextBox txtHiddenClaimID;
        protected TextBox txtNationalID;
        protected ValidationSummary ValidationSummary1;
        protected HtmlForm vansk;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "208";
            lblMsg.Visible = false;
            lblMessage.Visible = false;

            tbClaimOwnerNative.Attributes.Add("onkeypress", "testEnterKey();");

            if (Request.QueryString["head"] != null) {
                ClearSessionVariables();
            }

            // check the current culture
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            // Add <ENTER> event
            AddEnterEvent();

            if (Request.QueryString["update"] != null) {
                newRegistration = false;
            }

            if (Session["ClaimStatus"] != null) {
                claimStatus = (int) Session["ClaimStatus"];
            }
            if (!IsPostBack) {
                trDebtorNameSearchGrid.Visible = false;
                trClaimOwnerNameSearchGrid.Visible = false;
                InitControls();
                SetColumnsHeaderByCulture();
                SetColumnsByCulture();
                //	Session.Remove("ClaimStatus");
            }
            if (newRegistration) {
                claimStatus = 1;
            } else {
                if (Request.Params["pageid"] != null) {
                    SetRedirectPage(int.Parse(Request.Params["pageid"]));
                    tbPageID.Text = Request.Params["pageid"];
                }
            }
            if (tbPageID.Text != "") {
                SetRedirectPage(int.Parse(tbPageID.Text));
            }
        }

        private void InitControls() {
            int creditInfoID = -1;
            if (newRegistration) {
                claimStatus = 1;
                tbRegDate.Text = DateTime.Now.ToShortDateString();
                var myStatus = myFact.GetStatus(claimStatus);
                tbStatus.Text = EN ? myStatus.StateEN : myStatus.StateNative;

                if (Session["ClaimOwnerInfo"] != null && Session["ClaimOwnerInfo"].ToString() == "true") {
                    if (Session["CreditInfoID"] != null) {
                        txtClaimOwnerCIID.Text = Session["CreditInfoID"].ToString();
                    }
                    if (Session["ClaimOwnerNameNative"] != null) {
                        tbClaimOwnerNative.Text = Session["ClaimOwnerNameNative"].ToString().Trim();
                    }
                    if (Session["ClaimOwnerNameEN"] != null && tbClaimOwnerNative.Text == "") {
                        tbClaimOwnerNative.Text = Session["ClaimOwnerNameEN"].ToString().Trim();
                    }

                    if (Session["tbDebtorFirstName"] != null) {
                        tbDebtorFirstName.Text = Session["tbDebtorFirstName"].ToString();
                    }
                    if (Session["tbDebtorSurname"] != null) {
                        tbDebtorSurname.Text = Session["tbDebtorSurname"].ToString();
                    }
                    if (Session["tbDebtorID"] != null) {
                        creditInfoID = int.Parse(Session["tbDebtorID"].ToString());
                        tbDebtorID.Text = Session["tbDebtorID"].ToString();
                    }
                    if (Session["txtNationalID"] != null) {
                        txtNationalID.Text = Session["txtNationalID"].ToString();
                    }

                    Session.Remove("tbDebtorFirstName");
                    Session.Remove("tbDebtorSurname");
                    Session.Remove("tbDebtorID");
                    Session.Remove("txtNationalID");

                    Session.Remove("ClaimOwnerInfo");
                    Session.Remove("CreditInfoID");
                    Session.Remove("ClaimOwnerNameNative");
                    Session.Remove("ClaimOwnerNameEN");
                } else {
                    if (Session["CreditInfoID"] != null) {
                        creditInfoID = (int) Session["CreditInfoID"];
                    }
                    if (Session["DebtorFirstName"] != null) {
                        tbDebtorFirstName.Text = Session["DebtorFirstName"].ToString();
                    }
                    if (Session["DebtorSurName"] != null) {
                        tbDebtorSurname.Text = Session["DebtorSurName"].ToString();
                    }
                    if (creditInfoID != -1) {
                        tbDebtorID.Text = creditInfoID.ToString();
                    }

                    if (Session["txtClaimOwnerCIID"] != null) {
                        txtClaimOwnerCIID.Text = Session["txtClaimOwnerCIID"].ToString();
                    }
                    if (Session["tbClaimOwnerNative"] != null) {
                        tbClaimOwnerNative.Text = Session["tbClaimOwnerNative"].ToString();
                    }
                    Session.Remove("txtClaimOwnerCIID");
                    Session.Remove("tbCreditorNameNative");

                    Session.Remove("CreditInfoID");
                    Session.Remove("DebtorFirstName");
                    Session.Remove("DebtorSurName");
                }
            } else {
                btnSubmitAndClear.Visible = false;
                int claimID = -1;
                if (Session["claimID"] != null) {
                    claimID = (int) Session["ClaimID"];
                }
                if (Session["CreditInfoID"] != null) {
                    creditInfoID = (int) Session["CreditInfoID"];
                }
                txtHiddenClaimID.Text = claimID.ToString();
                Claim myClaim = myFact.GetClaim(claimID);
                btSubmit.Text = rm.GetString("txtUpdate", ci);
                lbClaimID.Text = rm.GetString("lbClaimID", ci) + ": " + myClaim.ClaimID;
                lbClaimID.Visible = true;
                tbDebtorID.Text = myClaim.CreditInfoID.ToString();

                txtClaimOwnerCIID.Text = myClaim.ClaimOwnerCIID.ToString();
                if (myClaim.ClaimOwnerCIID > 0) {
                    string claimOwnerType = myFact.GetCIUserType(myClaim.ClaimOwnerCIID);
                    if (claimOwnerType.Trim().Equals("-1")) // no type found
                    {
                        claimOwnerType = CigConfig.Configure("lookupsettings.individualID");
                            // then set to individual as default
                    }
                    if (claimOwnerType.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                        var myIndi = myFact.GetIndivitual(myClaim.ClaimOwnerCIID);
                        rbtnCompanyClaimOwner.Checked = false;
                        rbtnIndividualClaimOwner.Checked = true;
                        if (EN) {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameEN)) {
                                tbClaimOwnerNative.Text = myIndi.FirstNameEN + " " + myIndi.SurNameEN;
                            } else {
                                tbClaimOwnerNative.Text = myIndi.FirstNameNative + " " + myIndi.SurNameNative;
                            }
                        } else {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameNative)) {
                                tbClaimOwnerNative.Text = myIndi.FirstNameNative + " " + myIndi.SurNameNative;
                            } else {
                                tbClaimOwnerNative.Text = myIndi.FirstNameEN + " " + myIndi.SurNameEN;
                            }
                        }
                    } else if (claimOwnerType.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                        // company
                        var myComp = myFact.GetCompany(myClaim.ClaimOwnerCIID);
                        rbtnCompanyClaimOwner.Checked = true;
                        rbtnIndividualClaimOwner.Checked = false;
                        if (EN) {
                            tbClaimOwnerNative.Text = !string.IsNullOrEmpty(myComp.NameEN) ? myComp.NameEN : myComp.NameNative;
                        } else {
                            tbClaimOwnerNative.Text = !string.IsNullOrEmpty(myComp.NameNative) ? myComp.NameNative : myComp.NameEN;
                        }
                    }
                }
                ddInformationSourceList.SelectedValue = myClaim.InformationSourceID.ToString();
                tbRegDate.Text = myClaim.RegDate.ToString();
                if (myClaim.ClaimDate > DateTime.MinValue) {
                    tbClaimDate.Text = myClaim.ClaimDate.ToShortDateString();
                }
                if (myClaim.DelayDate > DateTime.MinValue) {
                    tbClaimDelayedDate.Text = myClaim.DelayDate.ToShortDateString();
                }
                if (myClaim.PaymentDate > DateTime.MinValue) {
                    tbPaymentDate.Text = myClaim.PaymentDate.ToShortDateString();
                }
                tbClaimAmount.Text = myClaim.DAmount.ToString();
                ddCurrency.Items.Add(myClaim.CurrencySymbol);
                ddType.SelectedValue = myClaim.ClaimTypeID.ToString();
                tbCaseID.Text = myClaim.CaseNumber;
                tbGazetteYear.Text = myClaim.GazatteYear;
                tbGazettePage.Text = myClaim.GazettePage;
                tbStatus.Text = myClaim.StateEN;
                Session["ClaimStatus"] = myClaim.StatusID;
                tbRegister.Text = myClaim.RegisterInitials;
                tbComment.Text = myClaim.RegisterCommentNative;
                tbInternalComment.Text = myClaim.RegisterInternalCommentNative;
                tbAgent.Text = myClaim.Agent;
                Session.Remove("ClaimID");
                Session.Remove("CreditInfoID");
            }
            // h�r vantar �t np_Indivitual t�flu
            string type = myFact.GetCIUserType(creditInfoID);
            if (type.Trim().Equals("-1")) // no type found
            {
                type = CigConfig.Configure("lookupsettings.individualID"); // then set to individual as default
            }
            if (type.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                Indivitual myIndi = myFact.GetIndivitual(creditInfoID);
                if (EN) {
                    if (myIndi.FirstNameEN == null || myIndi.FirstNameEN.Trim() == "") {
                        tbDebtorFirstName.Text = myIndi.FirstNameNative;
                        tbDebtorSurname.Text = myIndi.SurNameNative;
                    } else {
                        tbDebtorFirstName.Text = myIndi.FirstNameEN;
                        tbDebtorSurname.Text = myIndi.SurNameEN;
                    }
                } else {
                    if (myIndi.FirstNameNative == null || myIndi.FirstNameNative.Trim() == "") {
                        tbDebtorFirstName.Text = myIndi.FirstNameEN;
                        tbDebtorSurname.Text = myIndi.SurNameEN;
                    } else {
                        tbDebtorFirstName.Text = myIndi.FirstNameNative;
                        tbDebtorSurname.Text = myIndi.SurNameNative;
                    }
                }
            }
            LocalizeText();
            InitDDBoxes();
        }

        private void ClearSessionVariables() {
            Session["CreditInfoID"] = null;
            Session["DebtorFirstName"] = null;
            Session["DebtorSurName"] = null;
            Session["CompNameEN"] = null;
            Session["CompNameNative"] = null;
            Session["ClaimStatus"] = null;
        }

        public void SetRedirectPage(int index) { redirectPage = CigConfig.Configure("lookupsettings.redirectpageID" + index); }

        public void InitDDBoxes() {
            ddCurrency.DataSource = myFact.GetCurrencyListAsDataSet();
            ddCurrency.DataTextField = "CurrencyCode";
            ddCurrency.DataBind();
            //	this.ddCurrency.Items.Insert(0,"CYP");
            //	this.ddCurrency.Items.Insert(1,"EUR");
            // if currency order key in config then order the currency list ...
            if (CigConfig.Configure("lookupsettings.orderCurrencyList") != null) {
                OrderCurrencyList(CigConfig.Configure("lookupsettings.orderCurrencyList"));
            }

            ddInformationSourceList.DataSource = myFact.GetInformationSourceListAsDataSet();
            ddInformationSourceList.DataTextField = EN ? "NameEN" : "NameNative";

            ddInformationSourceList.DataValueField = "InformationSourceID";
            ddInformationSourceList.DataBind();

            ddType.DataSource = myFact.GetCaseTypesAsDataSet(CigConfig.Configure("lookupsettings.ct_bankruptie"));
            try {
                ddType.DataTextField = EN ? "TypeEN" : "TypeNative";

                ddType.DataValueField = "CaseTypeID";
                ddType.DataBind();
            } catch (Exception err) // when switching from native to EN the DataValueField throws exeption. However the functionality seem ok
            {
                Logger.WriteToLog("NewClaim.aspx-InitDDBoxes()ddType.ValueField " + err.Message, true);
            }
            if (!newRegistration) {
                return;
            }
            // Select N/A as default
            int index = ddType.Items.IndexOf(ddType.Items.FindByText("N/A"));
            if (index > 0) {
                ddType.SelectedIndex = index;
            }
        }

        private void OrderCurrencyList(string orderList) {
            var strArray = orderList.Split(new[] {','});
            var index = 0;
            foreach (string id in strArray) {
                var li = ddCurrency.Items.FindByValue(id);
                ddCurrency.Items.Remove(li);
                ddCurrency.Items.Insert(index, li);
                index++;
            }
        }

        private void btSubmit_Click(object sender, EventArgs e) {
            var myClaim = new Claim();
            var myFact1 = new NPaymentsFactory();

            int maxLength;
            try {
                maxLength = int.Parse(CigConfig.Configure("lookupsettings.NPI.Comment.MaxSize"));
            } catch {
                maxLength = 512;
            }

            if (tbComment.Text.Length > maxLength || tbInternalComment.Text.Length > maxLength) {
                lblMsg.Text = rm.GetString("txtCommentTooLong", ci);
                return;
            }

            if (!Page.IsValid) {
                return;
            }
            try {
                myClaim.CreditInfoID = int.Parse(tbDebtorID.Text);
                myClaim.ClaimOwnerCIID = int.Parse(txtClaimOwnerCIID.Text);
                myClaim.ClaimTypeID = int.Parse(ddType.SelectedItem.Value); //.SelectedIndex + 1;
                myClaim.InformationSourceID = int.Parse(ddInformationSourceList.SelectedItem.Value);
                myClaim.CaseNumber = tbCaseID.Text;
                myClaim.CurrencySymbol = ddCurrency.SelectedItem.Text;
                if (tbClaimAmount.Text != "") {
                    myClaim.DAmount = Convert.ToDecimal(tbClaimAmount.Text);
                }
                myClaim.RegisterCommentNative = tbComment.Text;
                myClaim.RegisterInternalCommentNative = tbInternalComment.Text;
                myClaim.Agent = tbAgent.Text;

                IFormatProvider format = CultureInfo.CurrentCulture;
                if (tbRegDate.Text != "") {
                    myClaim.RegDate = DateTime.Parse(tbRegDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                }
                if (tbClaimDate.Text != "") {
                    myClaim.ClaimDate = DateTime.Parse(tbClaimDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                }
                if (tbClaimDelayedDate.Text != "") {
                    myClaim.DelayDate = DateTime.Parse(
                        tbClaimDelayedDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                }
                if (tbPaymentDate.Text != "") {
                    myClaim.PaymentDate = DateTime.Parse(
                        tbPaymentDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                }
                try {
                    myClaim.RegisterID = (int) Session["UserCreditInfoID"];
                } catch (Exception error) {
                    Logger.WriteToLog(
                        "Exception on NewClaim.aspx caught, when getting (int) Session[\"UserCreditInfoID\"] : ",
                        error,
                        true);
                    lblMsg.Text = rm.GetString("errMsg2", ci);
                    lblMsg.Visible = true;
                    return;
                }
                if (tbGazetteYear.Text != "") {
                    myClaim.GazatteYear = tbGazetteYear.Text;
                }
                if (tbGazettePage.Text != "") {
                    myClaim.GazettePage = tbGazettePage.Text;
                }

                myClaim.Bankruptie = true;
                if (claimStatus != 0) {
                    // myClaim.StatusID = 4; // All bankruptie cases get registrated status right away
                    myClaim.SetClaimStatus();
                    // claimStatus; //int.Parse(this.tbStatus.Text); // athuga me� value h�r!
                } else {
                    myClaim.StatusID = int.Parse(CigConfig.Configure("lookupsettings.formerstatusid"));
                }
            } catch (Exception err) {
                Logger.WriteToLog("Exception on NewClaim.aspx caught, ", err, true);
                lblMsg.Text = rm.GetString("errMsg2", ci);
                lblMsg.Visible = true;
                return;
            }
            if (newRegistration) {
                if (!myFact1.AddNewClaim(myClaim)) {
                    lblMsg.Text = rm.GetString("errMsg1", ci);
                    lblMsg.Visible = true;
                    return;
                }
                lblMsg.Text = rm.GetString("txtClaimRegistered", ci);
                lblMsg.Visible = true;
                ClearSessionVariables();
                PrepareControlsForNewRegistration();
            } else {
                myClaim.LastUpdate = DateTime.Now;
                myClaim.ClaimID = int.Parse(txtHiddenClaimID.Text); //Session["ClaimID"];
                if (!myFact1.UpdateClaim(myClaim)) {
                    lblMsg.Text = rm.GetString("errMsg1", ci);
                    lblMsg.Visible = true;
                    return;
                }
                lblMsg.Text = rm.GetString("txtClaimUpdated", ci);
                lblMsg.Visible = true;
                //If redirect-page is set then redirect
                if (!string.IsNullOrEmpty(redirectPage)) {
                    Response.Redirect(redirectPage);
                }
            }
            ClearForm(false);
        }

        public void DateValidate(object source, ServerValidateEventArgs value) {
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime date = DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        private void ClearForm(bool clearDebtor) {
            if (clearDebtor) {
                tbDebtorID.Text = "";
                tbDebtorSurname.Text = "";
                tbDebtorFirstName.Text = "";
                txtNationalID.Text = "";
            }
            tbClaimDate.Text = "";
            tbClaimDelayedDate.Text = "";
            tbPaymentDate.Text = "";
            tbClaimAmount.Text = "";
            tbCaseID.Text = "";
            tbGazetteYear.Text = "";
            tbGazettePage.Text = "";
            tbStatus.Text = "";
            tbRegister.Text = "";
            tbComment.Text = "";
            tbClaimOwnerNative.Text = "";
            txtClaimOwnerCIID.Text = "";
            tbAgent.Text = "";
            tbInternalComment.Text = "";
            trDebtorNameSearchGrid.Visible = false;
            trClaimOwnerNameSearchGrid.Visible = false;
            Session.Remove("ClaimStatus");
        }

        private void PrepareControlsForNewRegistration() {
            newRegistration = true;
            claimStatus = 1;
            tbRegDate.Text = DateTime.Now.ToString();
            Status myStatus = myFact.GetStatus(claimStatus);
            tbStatus.Text = myStatus.StateEN;
        }

        private void LocalizeText() {
            lbDebtorID.Text = rm.GetString("lbDebtorID", ci);
            lbAgentID.Text = rm.GetString("lbAgentID", ci);
            lbDates.Text = rm.GetString("lbDates", ci);
            lbRegisted.Text = rm.GetString("lbRegisted", ci);
            lbClaim.Text = rm.GetString("lbClaim", ci);
            lbOnHold.Text = rm.GetString("lbOnHold", ci);
            lbPayment.Text = rm.GetString("lbPayment", ci);
            lbOther.Text = rm.GetString("lbOther", ci);
            lbClaimAmount.Text = rm.GetString("lbClaimAmount", ci);
            lbCaseID.Text = rm.GetString("lbCaseID", ci);
            lbRegistration.Text = rm.GetString("lbRegistration", ci);
            lbStatus.Text = rm.GetString("lbStatus", ci);
            lbRegister.Text = rm.GetString("lbRegister", ci);
            lbComment.Text = rm.GetString("lbComment", ci);
            btSubmit.Text = rm.GetString("txtSubmitAndNew", ci);
//			this.hlNewDebtor.Text = rm.GetString("hlNewDebtor",ci);

            btnSearchClaimOwner.Text = rm.GetString("txtSearch", ci);
            btnSearchDebtor.Text = rm.GetString("txtSearch", ci);
            lblClaimOwner.Text = rm.GetString("txtClaimOwner", ci);

            imgCompany.AlternateText = rm.GetString("txtCompany", ci);
            imgIndividual.AlternateText = rm.GetString("txtIndividual", ci);

            lbDebtorSurname.Text = rm.GetString("txtSurname", ci);
            lbDebtorFirstName.Text = rm.GetString("txtFirstname", ci);
            lbClaimOwnerName.Text = rm.GetString("lbClaimOwnerName", ci);
            lblClaimOwnerCIID.Text = rm.GetString("txtClaimOwnerCIID", ci);
            lbInfoOrigin.Text = rm.GetString("lbInfoOrigin", ci);
            lbGazetteYear.Text = rm.GetString("lbGazetteYear", ci);
            lbGazettePage.Text = rm.GetString("lbGazettePage", ci);
            lbInternalComment.Text = rm.GetString("lbInternalComment", ci);
            lbType.Text = rm.GetString("lbType", ci);
            RequiredFieldValidator5.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvClaimOwnerCIID.ErrorMessage = rm.GetString("txtValueMissing", ci);
            CustomValidator1.ErrorMessage = rm.GetString("CustomValidator1", ci);
            CustomValidator2.ErrorMessage = rm.GetString("CustomValidator2", ci);
            CustomValidator3.ErrorMessage = rm.GetString("CustomValidator3", ci);
            btnShowAll.Text = rm.GetString("txtShowAllCases", ci);
            lbBankruptieH.Text = rm.GetString("txtBankrupt", ci);
            lbDebtor.Text = rm.GetString("txtDebtor", ci);
            lblNationalID.Text = rm.GetString("lbNationalID", ci);
            btnSubmitAndClear.Text = rm.GetString("txtSubmitAndClear", ci);
            rfvCIID.ErrorMessage = rm.GetString("txtValueMissing", ci);

            lblDgResults.Text = rm.GetString("txtResults", ci);
            lblDgResults2.Text = rm.GetString("txtResults", ci);
        }

        private void AddEnterEvent() {
            var frm = FindControl("vansk");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void btnShowAll_Click(object sender, EventArgs e) {
            if (tbDebtorID.Text != "") {
                Session["CreditInfoID"] = tbDebtorID.Text;
                Server.Transfer("ShowAllCases.aspx");
            }
        }

        private void SetColumnsByCulture() {
            dgDebtors.Columns[0].Visible = true;
            dgDebtors.Columns[1].Visible = true;
            dgDebtors.Columns[2].Visible = true;
            dgDebtors.Columns[3].Visible = true;
            dgDebtors.Columns[4].Visible = false;
            dgDebtors.Columns[5].Visible = false;
            dgDebtors.Columns[6].Visible = false;
            dgDebtors.Columns[7].Visible = false;
            dgDebtors.Columns[8].Visible = true;
        }

        private void SetColumnsHeaderByCulture() {
            dgDebtors.Columns[1].HeaderText = rm.GetString("lbNationalID", ci);
            dgDebtors.Columns[2].HeaderText = rm.GetString("lbName", ci);
            dgDebtors.Columns[3].HeaderText = rm.GetString("lbAddress", ci);
            dgDebtors.Columns[8].HeaderText = rm.GetString("lbDebtorID", ci);
        }

        protected void SearchForIndividual(int ciid, string nationalID, string firstName, string surName) {
            trClaimOwnerNameSearchGrid.Visible = false;
            var myDebt = new Debtor
                         {
                             CreditInfo = ciid,
                             IDNumber1 = nationalID,
                             IDNumber2Type = 1,
                             FirstName = firstName,
                             SurName = surName
                         };

            try {
                var ds = CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True" ? myFact.FindCustomerInNationalAndCreditInfo(myDebt) : myFact.FindCustomer(myDebt);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        //if the creditinfoid is available
                        string sCreditInfoID = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                        if (sCreditInfoID != "") {
                            trDebtorNameSearchGrid.Visible = false;
                            string name = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            string[] arrName = name.Split(' ');
                            if (arrName.Length > 0) {
                                if (arrName.Length == 1) {
                                    tbDebtorFirstName.Text = arrName[0];
                                } else {
                                    tbDebtorSurname.Text = arrName[arrName.Length - 1];
                                    tbDebtorFirstName.Text = name.Substring(
                                        0, name.Length - (tbDebtorSurname.Text.Length + 1));
                                }
                            }
                            //this.txtCompanyNameEN.Text=ds.Tables[0].Rows[0]["NameEN"].ToString();
                            tbDebtorID.Text = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                            txtNationalID.Text = ds.Tables[0].Rows[0]["Number"].ToString();
                        } else {
                            trDebtorNameSearchGrid.Visible = true;
                            dgDebtors.DataSource = ds;
                            dgDebtors.DataBind();
                            divDebtors.Visible = true;
                            divDebtors.Style["HEIGHT"] = "auto";
                            divDebtors.Style["OVERFLOW"] = "auto";
                            divDebtors.Style["OVERFLOW-X"] = "auto";
                        }
                    } else {
                        dgDebtors.DataSource = ds;
                        dgDebtors.DataBind();
                        trDebtorNameSearchGrid.Visible = true;
                        int gridRows = dgDebtors.Items.Count;
                        if (gridRows < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll"))) {
                            divDebtors.Style.Remove("HEIGHT");
                        } else {
                            divDebtors.Style["HEIGHT"] = "164px";
                        }
                        divDebtors.Style["OVERFLOW"] = "auto";
                        divDebtors.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                    trDebtorNameSearchGrid.Visible = false;
                    tbDebtorFirstName.Text = firstName;
                    tbDebtorSurname.Text = surName;
                    txtNationalID.Text = nationalID;
                    tbDebtorID.Text = ciid == -1 ? "" : ciid.ToString();
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewClaim.aspx ", err, true);
            }
        }

        private void dgDebtors_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                int ciid = -1;
                try {
                    ciid = int.Parse(e.Item.Cells[8].Text);
                } catch (Exception) {
                    ciid = -1;
                }
                if (ciid == -1) {
                    //Store claim owner in session if available
                    if (txtClaimOwnerCIID.Text != "") {
                        Session["txtClaimOwnerCIID"] = txtClaimOwnerCIID.Text;
                    }
                    if (tbClaimOwnerNative.Text != "") {
                        Session["tbClaimOwnerNative"] = tbClaimOwnerNative.Text;
                    }

                    //No ciid - then create the individual
                    string firstName = "";
                    string surName = "";
                    string name = e.Item.Cells[4].Text.Trim();
                    if (name == "&nbsp;") {
                        name = "";
                    }
                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            firstName = arrName[0];
                        } else {
                            surName = arrName[arrName.Length - 1];
                            firstName = name.Substring(0, name.Length - (surName.Length + 1));
                        }
                    }
                    Session["AddIndividual"] = true;
                    Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                    Session["FirstName"] = firstName;
                    Session["SurName"] = surName;
                    if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                        Session["StreetNative"] = e.Item.Cells[6].Text.Trim();
                    }
                    if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                        Session["StreetEN"] = e.Item.Cells[7].Text.Trim();
                    }
                    Server.Transfer("NewCIUser.aspx?pageid=1");
                } else {
                    SearchForIndividual(ciid, "", "", "");
                }
            }
        }

        private void btnSubmitAndClear_Click(object sender, EventArgs e) {
            btSubmit_Click(this, null);
            ClearForm(true);
        }

        private void dgDebtors_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (EN) {
                    //Set name - EN if available, else native
                    if (e.Item.Cells[5].Text != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    }

                    //Set street - EN if available, else native
                    if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    }
                } else {
                    //Set name - Native if available, else EN
                    if (e.Item.Cells[4].Text != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    }

                    //Set street - Native if available, else EN
                    if (e.Item.Cells[6].Text != "" && e.Item.Cells[6].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
                WebDesign.CreateExplanationIcons(dgDebtors.Columns, lblDatagridIcons, rm, ci);
            }
        }

        protected void SearchForClaimOwner(int ciid, string firstName) {
            trDebtorNameSearchGrid.Visible = false;
            var myDebt = new Debtor {CreditInfo = ciid};

            string[] arrName = firstName.Split(' ');
            if (arrName.Length > 0) {
                if (arrName.Length == 1) {
                    myDebt.FirstName = arrName[0];
                } else {
                    myDebt.SurName = arrName[arrName.Length - 1];
                    myDebt.FirstName = firstName.Substring(0, firstName.Length - (myDebt.SurName.Length + 1));
                }
            }

            var myComp = new Company {CreditInfoID = ciid, NameNative = firstName};

            try {
                DataSet ds;
                if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True") {
                    ds = rbtnCompanyClaimOwner.Checked ? myFact.FindCompanyInNationalAndCreditInfo(myComp) : myFact.FindCustomerInNationalAndCreditInfo(myDebt);
                } else {
                    ds = rbtnIndividualClaimOwner.Checked ? myFact.FindCustomer(myDebt) : myFact.FindCompany(myComp);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        //if creditinfoid
                        string sCreditInfoID = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                        if (sCreditInfoID != "") {
                            //this.tdNameSearchGrid.Visible=false;

                            trClaimOwnerNameSearchGrid.Visible = false;
                            tbClaimOwnerNative.Text = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            txtClaimOwnerCIID.Text = sCreditInfoID;
                        } else {
                            dgClaimOwner.DataSource = ds;
                            dgClaimOwner.DataBind();
                            trClaimOwnerNameSearchGrid.Visible = true;

                            divDebtors.Style["HEIGHT"] = "auto"; //dtgrNameSearch.Height.ToString();
                            divDebtors.Style["OVERFLOW"] = "auto";
                            divDebtors.Style["OVERFLOW-X"] = "auto";
                        }
                    } else {
                        dgClaimOwner.DataSource = ds;
                        dgClaimOwner.DataBind();
                        trClaimOwnerNameSearchGrid.Visible = true;

                        int gridRows = dgClaimOwner.Items.Count;
                        divClaimOwner.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                        divClaimOwner.Style["OVERFLOW"] = "auto";
                        divClaimOwner.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                    trClaimOwnerNameSearchGrid.Visible = false;
                    tbClaimOwnerNative.Text = firstName;
                    txtClaimOwnerCIID.Text = ciid == -1 ? "" : ciid.ToString();
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        private void DisplayMessage(string message) {
            lblMessage.Text = message;
            lblMessage.ForeColor = Color.Blue;
            lblMessage.Visible = true;
        }

        private void dgClaimOwner_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (EN) {
                    //Set name - EN if available, else native
                    if (e.Item.Cells[5].Text != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    }

                    //Set street - EN if available, else native
                    if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    }
                } else {
                    //Set name - Native if available, else EN
                    if (e.Item.Cells[4].Text != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    }

                    //Set street - Native if available, else EN
                    if (e.Item.Cells[6].Text != "" && e.Item.Cells[6].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
                WebDesign.CreateExplanationIcons(dgClaimOwner.Columns, lblDatagridIcons2, rm, ci);
            }
        }

        private void dgClaimOwner_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                int ciid = -1;
                if (e.Item.Cells[8].Text != "") {
                    try {
                        ciid = int.Parse(e.Item.Cells[8].Text);
                    } catch (Exception) {
                        ciid = -1;
                    }
                }
                if (ciid == -1) {
                    //Then ciid is not found - then we need to create the user - redirect the user
                    //to NewCIUserC.aspx or NewCIUser.aspx

                    //If individual already selected - then store in session					
                    if (txtNationalID.Text != "") {
                        Session["txtNationalID"] = txtNationalID.Text;
                    }
                    if (tbDebtorSurname.Text != "") {
                        Session["tbDebtorSurname"] = tbDebtorSurname.Text;
                    }
                    if (tbDebtorFirstName.Text != "") {
                        Session["tbDebtorFirstName"] = tbDebtorFirstName.Text;
                    }
                    if (tbDebtorID.Text != "") {
                        Session["tbDebtorID"] = tbDebtorID.Text;
                    }

                    Session["AddClaimOwner"] = true;
                    if (e.Item.Cells[1].Text.Trim() != "&nbsp;") {
                        Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                    }
                    if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                        Session["StreetNative"] = e.Item.Cells[6].Text.Trim();
                    }
                    if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                        Session["StreetEN"] = e.Item.Cells[7].Text.Trim();
                    }
                    // �etta krassar ef um einstakling er a� r��a
                    //	if(e.Item.Cells[9].Text.Trim()=="1")
                    if (rbtnCompanyClaimOwner.Checked) {
                        if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                            Session["NameNative"] = e.Item.Cells[4].Text.Trim();
                        }
                        if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                            Session["NameEN"] = e.Item.Cells[5].Text.Trim();
                        }
                        Server.Transfer("NewCIUserC.aspx?pageid=1");
                    } else {
                        if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                            string name = e.Item.Cells[4].Text.Trim();
                            string[] arrName = name.Split(' ');
                            if (arrName.Length > 0) {
                                if (arrName.Length == 1) {
                                    Session["FirstName"] = arrName[0];
                                } else {
                                    string surName = arrName[arrName.Length - 1];
                                    Session["SurName"] = surName;
                                    Session["FirstName"] = name.Substring(0, name.Length - (surName.Length + 1));
                                }
                            }
                        }
                        Server.Transfer("NewCIUser.aspx?pageid=1");
                    }
                } else {
                    txtClaimOwnerCIID.Text = ciid.ToString();
                    tbClaimOwnerNative.Text = e.Item.Cells[2].Text.Trim();
                }
            }
        }

        private void btnSearchClaimOwner_Click(object sender, EventArgs e) {
            int ciid = -1;

            try {
                ciid = int.Parse(txtClaimOwnerCIID.Text);
            } catch (Exception) {
                ciid = -1;
            }

            SearchForClaimOwner(ciid, tbClaimOwnerNative.Text);
        }

        private void btnSearchDebtor_Click(object sender, EventArgs e) {
            int ciid = -1;

            try {
                ciid = int.Parse(tbDebtorID.Text);
            } catch (Exception) {
                ciid = -1;
            }

            SearchForIndividual(ciid, txtNationalID.Text, tbDebtorFirstName.Text, tbDebtorSurname.Text);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearchDebtor.Click += new System.EventHandler(this.btnSearchDebtor_Click);
            this.dgDebtors.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDebtors_ItemCommand);
            this.dgDebtors.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgDebtors_ItemDataBound);
            this.btnSearchClaimOwner.Click += new System.EventHandler(this.btnSearchClaimOwner_Click);
            this.dgClaimOwner.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgClaimOwner_ItemCommand);
            this.dgClaimOwner.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgClaimOwner_ItemDataBound);
            this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
            this.btnSubmitAndClear.Click += new System.EventHandler(this.btnSubmitAndClear_Click);
            this.btSubmit.Click += new System.EventHandler(this.btSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}