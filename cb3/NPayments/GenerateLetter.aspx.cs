#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NPayments.BLL;
using NPayments.BLL.Letter;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;

#endregion

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for GenerateLetter.
    /// </summary>
    public class GenerateLetter : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected Button btnGenLetter;
        protected Button btnUncheckAll;
        protected Button btUpdateStatus;
        protected DataGrid dgLetterClaims;
        protected HtmlGenericControl divDataGrid;
        private bool EN;
        protected Label lblDatagridIcons;
        protected Label lbLetterClaims;
        protected Label lblTotalNumberOfNPIHits;
        public NPaymentsFactory myFact = new NPaymentsFactory();
        protected HtmlTable outerGridTable;

        /// <summary>
        /// Checks if the running version is for Malta
        /// </summary>
        /// <returns>True if the version is for Malta, false otherwise</returns>
        public static bool IsMalta {
            get {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                    if (CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                        return true;
                    }
                }
                return false;
            }
        }

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "209";

            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            LocalizeText();

            if (IsPostBack) {
                return;
            }
            var myFact1 = new NPaymentsFactory();
            var myLetterSet = myFact1.GetLetterClaimOverViewListAsDataSet();
            dgLetterClaims.DataSource = myLetterSet;
            SetColumnsByCulture();
            dgLetterClaims.DataBind();

            if (myLetterSet != null) {
                lblTotalNumberOfNPIHits.Text += myLetterSet.Tables[0].Rows.Count;
            }
        }

        private void LocalizeText() {
            lbLetterClaims.Text = rm.GetString("txtAllClaimsWithStatusLetter", ci);
            btnGenLetter.Text = rm.GetString("txtGenerateLetters", ci);
            btUpdateStatus.Text = rm.GetString("txtUpdateStatus", ci);
            btnUncheckAll.Text = rm.GetString("txtUncheckAll", ci);
            lblTotalNumberOfNPIHits.Text = rm.GetString("txtTotalNumberOfNPIHits", ci);
        }

        private void SetColumnsHeaderByCulture() {
            dgLetterClaims.Columns[0].HeaderText = rm.GetString("lbClaimID", ci);
            dgLetterClaims.Columns[1].HeaderText = rm.GetString("lbDebtorID", ci);
            dgLetterClaims.Columns[2].HeaderText = rm.GetString("lbClaimOwnerName", ci);
            dgLetterClaims.Columns[5].HeaderText = rm.GetString("lbType", ci);
            dgLetterClaims.Columns[8].HeaderText = rm.GetString("lbInfoOrigin", ci);
            dgLetterClaims.Columns[11].HeaderText = rm.GetString("txtRegDate", ci);
            dgLetterClaims.Columns[12].HeaderText = rm.GetString("lbClaimDate", ci);
            dgLetterClaims.Columns[13].HeaderText = rm.GetString("txtState", ci);
            dgLetterClaims.Columns[16].HeaderText = rm.GetString("lbCaseID", ci);
        }

        private void SetColumnsByCulture() {
            // letter status grid
            /*this.dgLetterClaims.Columns[0].Visible = true;
			this.dgLetterClaims.Columns[1].Visible = true;
			this.dgLetterClaims.Columns[2].Visible = true;
			this.dgLetterClaims.Columns[3].Visible = false;
			this.dgLetterClaims.Columns[4].Visible = false;
			this.dgLetterClaims.Columns[5].Visible = true;
			this.dgLetterClaims.Columns[6].Visible = false;
			this.dgLetterClaims.Columns[7].Visible = false;
			this.dgLetterClaims.Columns[8].Visible = true;
			this.dgLetterClaims.Columns[9].Visible = false;
			this.dgLetterClaims.Columns[10].Visible = false; // taka state �t
			this.dgLetterClaims.Columns[11].Visible = true; //      " "
			this.dgLetterClaims.Columns[12].Visible = true;
			this.dgLetterClaims.Columns[13].Visible = false;
			this.dgLetterClaims.Columns[14].Visible = false;
			this.dgLetterClaims.Columns[15].Visible = false;
			this.dgLetterClaims.Columns[16].Visible = true;
			this.dgLetterClaims.Columns[17].Visible = true;
			this.dgLetterClaims.Columns[18].Visible = true;*/
            if (IsMalta) {
                dgLetterClaims.Columns[18].Visible = false;
            }

            SetColumnsHeaderByCulture();
        }

        private void btnGenLetter_Click(object sender, EventArgs e) {
            CheckBox cbBankruptieLetter;
            CheckBox cbRegularLetter;
            Letter genLetter;
            var arrLetter = new ArrayList();

            //Order by: debtorID, ClaimId
            
            foreach (DataGridItem dgi in dgLetterClaims.Items) {
                genLetter = new Letter
                            {
                                Debtor_Ref = dgi.Cells[0].Text,
                                ClaimOwnerName = dgi.Cells[2].Text,
                                Type = dgi.Cells[4].Text,
                                InformationSource = dgi.Cells[8].Text,
                                CaseDate = dgi.Cells[12].Text,
                                CaseNo = dgi.Cells[16].Text
                            };
                cbRegularLetter = (CheckBox) dgi.Cells[17].Controls[1];
                genLetter.RegularLetter = cbRegularLetter.Checked;
                cbBankruptieLetter = (CheckBox) dgi.Cells[18].Controls[1];
                genLetter.BankruptieLetter = cbBankruptieLetter.Checked;
                if (IsMalta) {
                    //Only regulare letter for malta
                    cbBankruptieLetter.Checked = false;
                    //We need to fill in the native and en data for info source and type
                    myFact.FillInfoSourceAndType(genLetter);
                }
                if (cbRegularLetter.Checked || cbBankruptieLetter.Checked) {
                    AddDebtorInfo(genLetter, dgi.Cells[1].Text);
                    arrLetter.Add(genLetter);
                }
                //AddDebtorInfo(genLetter, dgi.Cells[1].Text);
                //arrLetter.Add(genLetter);				
            }
            if (arrLetter.Count > 0) {
                Session["arrLetter"] = arrLetter;
                Server.Transfer(CigConfig.Configure("lookupsettings.displayletterpage"));
            }
        }

        private static void AddDebtorInfo(Letter genLetter, string CreditInfoID) {
            var myFact = new NPaymentsFactory();
            string userType = myFact.GetCIUserType(int.Parse(CreditInfoID));

            if (userType.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                var myIndi = myFact.GetIndivitual(int.Parse(CreditInfoID));
                if (myIndi.Address.Count > 0) {
                    var myAddress = (Address) myIndi.Address[0];
                    genLetter.DebtorAddress = myAddress.StreetNative;
                    if (myAddress.StreetNumber > 0) {
                        genLetter.DebtorAddress += myAddress.StreetNumber;
                    }

                    genLetter.DebtorTown = myAddress.CityNameNative;
                    genLetter.Postcode = myAddress.PostalCode;
                } else {
                    genLetter.DebtorAddress = "NOT REGISTERED";
                    genLetter.DebtorTown = "NOT REGISTERED";
                    genLetter.Postcode = "NOT REGISTERED";
                }
                genLetter.DebtorFirstName = myIndi.FirstNameNative;
                if (!string.IsNullOrEmpty(myIndi.SurNameNative))
                {

                    if (myIndi.SurNameNative.IndexOf("(") == -1 && myIndi.SurNameNative.IndexOf(")") == -1)
                        genLetter.DebtorSurName = " (" + myIndi.SurNameNative + ")";
                    else
                        genLetter.DebtorSurName = myIndi.SurNameNative;
                }
                else
                {
                    genLetter.DebtorSurName = string.Empty;
                }
            } else if (userType.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                // company
                var myComp = myFact.GetCompany(int.Parse(CreditInfoID));
                if (myComp.Address.Count > 0) {
                    var myAddress = (Address) myComp.Address[0];
                    genLetter.DebtorAddress = myAddress.StreetNative;
                    if (myAddress.StreetNumber > 0) {
                        genLetter.DebtorAddress += myAddress.StreetNumber;
                    }
                    genLetter.DebtorTown = myAddress.CityNameNative;
                    genLetter.Postcode = myAddress.PostalCode;
                } else {
                    genLetter.DebtorAddress = "NOT REGISTERED";
                    genLetter.DebtorTown = "NOT REGISTERED";
                    genLetter.Postcode = "NOT REGISTERED";
                }
                genLetter.DebtorFirstName = myComp.NameNative;
            }
        }

        private void btUpdateStatus_Click(object sender, EventArgs e) {
            var myArr = new ArrayList();
            CheckBox cbBankruptieLetter;
            CheckBox cbRegularLetter;

            for (int i = 0; i < dgLetterClaims.Items.Count; i++) {
                cbRegularLetter = (CheckBox) dgLetterClaims.Items[i].Cells[17].Controls[1];
                cbBankruptieLetter = (CheckBox) dgLetterClaims.Items[i].Cells[18].Controls[1];
                if (IsMalta) //Only regulare letter for malta
                {
                    cbBankruptieLetter.Checked = false;
                }

                if (cbRegularLetter.Checked || cbBankruptieLetter.Checked) {
                    myArr.Add(dgLetterClaims.Items[i].Cells[0].Text);
                }
            }
            var myFact1 = new NPaymentsFactory();
            myFact1.UpdateClaimLetterStatus(myArr);

            var myLetterSet = myFact1.GetLetterClaimOverViewListAsDataSet();
            dgLetterClaims.DataSource = myLetterSet;
            SetColumnsByCulture();
            dgLetterClaims.DataBind();
        }

        private void btnUncheckAll_Click(object sender, EventArgs e) {
            CheckBox cbBankruptieLetter;
            CheckBox cbRegularLetter;

            foreach (DataGridItem dgi in dgLetterClaims.Items) {
                cbRegularLetter = (CheckBox) dgi.Cells[17].Controls[1];
                cbRegularLetter.Checked = false;
                cbBankruptieLetter = (CheckBox) dgi.Cells[18].Controls[1];
                cbBankruptieLetter.Checked = false;
            }
        }

        private void dgCLetterClaims_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (EN) {
                    //Set Type - EN if available, else native
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[5].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[5].Text = e.Item.Cells[6].Text;
                    }

                    //Set Source - EN if available, else native
                    if (e.Item.Cells[10].Text.Trim() != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[8].Text = e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[8].Text = e.Item.Cells[9].Text;
                    }

                    //Set State - EN if available, else native
                    if (e.Item.Cells[15].Text.Trim() != "" && e.Item.Cells[15].Text != "&nbsp;") {
                        e.Item.Cells[13].Text = e.Item.Cells[15].Text;
                    } else {
                        e.Item.Cells[13].Text = e.Item.Cells[14].Text;
                    }
                } else {
                    //Set Type - Native if available, else EN
                    if (e.Item.Cells[6].Text.Trim() != "" && e.Item.Cells[6].Text != "&nbsp;") {
                        e.Item.Cells[5].Text = e.Item.Cells[6].Text;
                    } else {
                        e.Item.Cells[5].Text = e.Item.Cells[7].Text;
                    }

                    //Set Source - Native if available, else EN
                    if (e.Item.Cells[9].Text.Trim() != "" && e.Item.Cells[9].Text != "&nbsp;") {
                        e.Item.Cells[8].Text = e.Item.Cells[9].Text;
                    } else {
                        e.Item.Cells[8].Text = e.Item.Cells[10].Text;
                    }

                    //Set State - Native if available, else EN
                    if (e.Item.Cells[14].Text.Trim() != "" && e.Item.Cells[14].Text != "&nbsp;") {
                        e.Item.Cells[13].Text = e.Item.Cells[14].Text;
                    } else {
                        e.Item.Cells[13].Text = e.Item.Cells[15].Text;
                    }
                }
                /*try
				{
					//Get the claim owner
					int claimOwnerCIID = int.Parse(e.Item.Cells[3].Text);
					e.Item.Cells[2].Text = GetDebtorName(claimOwnerCIID);
				}
				catch
				{
					// just using this for check if it is a valid integer, no need to do some special handling here...
				}*/
                try {
                    //Get the claim owner
                    //int claimOwnerCIID = int.Parse(e.Item.Cells[5].Text);
                    //e.Item.Cells[4].Text = GetDebtorName(claimOwnerCIID, e.Item.Cells[17].Text);
                    if (e.Item.Cells[19].Text.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                        if (EN) {
                            if (e.Item.Cells[22].Text != "" && e.Item.Cells[22].Text != "&nbsp;") {
                                e.Item.Cells[2].Text = e.Item.Cells[22].Text;
                                if (e.Item.Cells[23].Text != "" && e.Item.Cells[23].Text != "&nbsp;") {
                                    e.Item.Cells[2].Text += " " + e.Item.Cells[23].Text;
                                }
                            } else {
                                e.Item.Cells[2].Text = e.Item.Cells[20].Text;
                                if (e.Item.Cells[21].Text != "" && e.Item.Cells[21].Text != "&nbsp;") {
                                    e.Item.Cells[2].Text += " " + e.Item.Cells[21].Text;
                                }
                            }
                        } else {
                            if (e.Item.Cells[20].Text != "" && e.Item.Cells[20].Text != "&nbsp;") {
                                e.Item.Cells[2].Text = e.Item.Cells[20].Text;
                                if (e.Item.Cells[21].Text != "" && e.Item.Cells[21].Text != "&nbsp;") {
                                    e.Item.Cells[2].Text += " " + e.Item.Cells[21].Text;
                                }
                            } else {
                                e.Item.Cells[2].Text = e.Item.Cells[22].Text;
                                if (e.Item.Cells[23].Text != "" && e.Item.Cells[23].Text != "&nbsp;") {
                                    e.Item.Cells[2].Text += " " + e.Item.Cells[23].Text;
                                }
                            }
                        }
                    } else {
                        if (EN) {
                            if (e.Item.Cells[25].Text != "" && e.Item.Cells[25].Text != "&nbsp;") {
                                e.Item.Cells[2].Text = e.Item.Cells[25].Text;
                            } else {
                                e.Item.Cells[2].Text = e.Item.Cells[24].Text;
                            }
                        } else {
                            if (e.Item.Cells[24].Text != "" && e.Item.Cells[24].Text != "&nbsp;") {
                                e.Item.Cells[2].Text = e.Item.Cells[24].Text;
                            } else {
                                e.Item.Cells[2].Text = e.Item.Cells[25].Text;
                            }
                        }
                    }
                } catch {
                    // just using this for check if it is a valid integer, no need to do some special handling here...
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dgLetterClaims.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCLetterClaims_ItemDataBound);
            this.btnUncheckAll.Click += new System.EventHandler(this.btnUncheckAll_Click);
            this.btUpdateStatus.Click += new System.EventHandler(this.btUpdateStatus_Click);
            this.btnGenLetter.Click += new System.EventHandler(this.btnGenLetter_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

        /// <summary>
        /// Get debtors name for each case in caseOverview. Quick and dirty solution though...
        /// </summary>
        /*private string GetDebtorName(int creditInfoID)
		{
			string type = myFact.GetCIUserType(creditInfoID);
			string debtorName = "";
			if (type.Trim().Equals("-1")) // no type found
				type = CigConfig.Configure("lookupsettings.individualID"); // then set to individual as default
			if (type.Trim().Equals(CigConfig.Configure("lookupsettings.individualID")))
			{
				Indivitual myIndi = myFact.GetIndivitual(creditInfoID);
				if (EN)
				{
					if (myIndi.FirstNameEN == null || myIndi.FirstNameEN.Trim() == "")
					{
						debtorName = myIndi.FirstNameNative;
						debtorName += " " + myIndi.SurNameNative;
					}
					else
					{
						debtorName = myIndi.FirstNameEN;
						debtorName += " " + myIndi.SurNameEN;
					}
				}
				else
				{
					if (myIndi.FirstNameNative == null || myIndi.FirstNameNative.Trim() == "")
					{
						debtorName = myIndi.FirstNameEN;
						debtorName += " " + myIndi.SurNameEN;
					}
					else
					{
						debtorName = myIndi.FirstNameNative;
						debtorName += " " + myIndi.SurNameNative;

					}
				}
			}
			else if (type.Trim().Equals(CigConfig.Configure("lookupsettings.companyID")))
			{ // company
				Company myComp = myFact.GetCompany(creditInfoID);
				if (EN)
				{
					if (myComp.NameEN == null || myComp.NameEN.Trim() == "")
						debtorName = myComp.NameNative;
					else
						debtorName = myComp.NameEN;
				}
				else
				{
					if (myComp.NameNative == null || myComp.NameNative.Trim() == "")
						debtorName = myComp.NameEN;
					else
						debtorName = myComp.NameNative;
				}
			}
			return debtorName;
		}*/
    }
}