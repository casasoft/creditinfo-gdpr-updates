<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="GeneratePendingCaseLetter.aspx.cs" AutoEventWireup="false" Inherits="NPayments.GeneratePendingCaseLetter" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Generate Pending Case Letter</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body ms_positioning="GridLayout">
		<form id="GeneratePendingCaseLetter" name="GeneratePendingCaseLetter" action="" method="post"
			runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellSpacing="0" cellPadding="0" runat="server">
													<tr>
														<th>
															<asp:label id="lbLetterClaims" runat="server">All Pending Cases with status "Letter"</asp:label>&nbsp;-
															<asp:label id="lblTotalNumberOfHits" runat="server">Label</asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellSpacing="0" cellPadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dgLetterPendingCases" runat="server" cssclass="grid" autogeneratecolumns="False"
																			gridlines="None">
																			<FooterStyle CssClass="grid_footer"></FooterStyle>
																			<SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
																			<AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
																			<ItemStyle CssClass="grid_item"></ItemStyle>
																			<HeaderStyle CssClass="grid_header"></HeaderStyle>
																			<Columns>
																				<asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DefendantID" HeaderText="Defendant ID">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DefendantName" HeaderText="Defendant Name">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="PlaintiffID" HeaderText="Plaintiff ID">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="PlaintiffName" HeaderText="Plaintiff Name">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn HeaderText="Court">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="CourtNative" HeaderText="CourtNative">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="CourtEN" HeaderText="CourtEN">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn HeaderText="Adjudicator">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="AdjudicatorNative" HeaderText="AdjudicatorNative">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="AdjudicatorEN" HeaderText="AdjudicatorEN">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DateOfWrit" HeaderText="DateOfWrit" DataFormatString="{0:d}">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="WritNumber" HeaderText="WritNumber">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:TemplateColumn HeaderText="Regular Letter">
																					<ItemStyle CssClass="padding"></ItemStyle>
																					<ItemTemplate>
																						<asp:checkbox id="chkRegularLetter" runat="server" checked="True" cssclass="radio"></asp:checkbox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																			</Columns>
																			<PagerStyle CssClass="grid_pager"></PagerStyle>
																		</asp:datagrid></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"></td>
														<td align="right"><asp:button id="btnUncheckAll" runat="server" cssclass="gray_button" text="Uncheck all"></asp:button><asp:button id="btUpdateStatus" runat="server" cssclass="gray_button" text="Update status"></asp:button><asp:button id="btnGenLetter" runat="server" text="Generate Letters" cssclass="confirm_button"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
