<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="FindCIUserC.aspx.cs" AutoEventWireup="false" Inherits="NPayments.FindCIUserC" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>FindCIUserC</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>
			function checkEnterKey() 
			{    
				if (event.keyCode == 13) 
				{        
					event.cancelBubble = true;
					event.returnValue = false;
					findcuser.btSearchDebtor.click(); 
				}
			} 
			function SetFormFocus()
				{
					document.findcuser.tbNationalIDNumber.focus();
				}
		</script>
	</HEAD>
	<body onload="SetFormFocus()">
		<form id="findcuser" name="vansk" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language2" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="Sitepositionbar2" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lbCompanySearch" runat="server">Company search</asp:label></th></tr>
													<tr>
														<td>
															<TABLE class="fields" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td style="HEIGHT: 15px" colSpan="4"><b><asp:label id="lbSearchBy" runat="server">Search by:</asp:label></b></td>
																</tr>
																<tr>
																	<TD style="WIDTH: 434px; HEIGHT: 17px"><asp:label id="lbNationalID" runat="server"> National ID</asp:label><BR>
																		<asp:textbox id="tbNationalIDNumber" runat="server"></asp:textbox>
																	<td style="HEIGHT: 17px" colSpan="2"><asp:label id="lbCIID" runat="server">CreditInfoID</asp:label><BR>
																		<asp:textbox id="tbCIID" runat="server"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 434px; HEIGHT: 15px"><asp:label id="lbCompanyName" runat="server">Name</asp:label><BR>
																		<asp:textbox id="tbNameNative" runat="server" Width="200px"></asp:textbox></td>
																	<td style="HEIGHT: 16px" colSpan="2" align="right"><asp:button id="btSearchDebtor" runat="server" Text="Search" CssClass="search_button"></asp:button></td>
																</tr>
																<TR>
																	<TD colSpan="4" height="23"></TD>
																</TR>
															</TABLE>
														</td>
													</tr>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table width="100%">
													<tr>
														<td style="WIDTH: 662px; HEIGHT: 20px"><asp:label id="lblError" runat="server" Visible="False" ForeColor="Red">Label</asp:label></td>
														<td style="HEIGHT: 20px" align="right"><asp:button id="btCancel" runat="server" Text="Cancel" CssClass="cancel_button"></asp:button></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr id="trDgResults" runat="server">
											<td>
												<TABLE class="grid_table" id="SearchGrid" cellSpacing="0" cellPadding="0" runat="server">
													<tr>
														<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<TR>
														<TH>
															<asp:label id="lblDgResults" runat="server"></asp:label></TH></TR>
													<TR>
														<TD id="tdNameSearchGrid" width="100%" runat="server"><asp:datagrid id="dgDebtors" runat="server" Width="100%" ForeColor="Black" CssClass="rammi" AllowSorting="True"
																AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="4" GridLines="None">
																<FooterStyle CssClass="grid_footer"></FooterStyle>
																<SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
																<AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
																<ItemStyle CssClass="grid_item"></ItemStyle>
																<HeaderStyle CssClass="grid_header"></HeaderStyle>
																<pagerstyle cssclass="grid_pager"></pagerstyle>
																<Columns>
																	<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																		CommandName="Select">
																		<ItemStyle CssClass="leftpadding"></ItemStyle>
																	</asp:ButtonColumn>
																	<asp:ButtonColumn Text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;"
																		CommandName="Update">
																		<ItemStyle CssClass="leftpadding"></ItemStyle>
																	</asp:ButtonColumn>
																	<asp:BoundColumn DataField="Number" SortExpression="Number" HeaderText="National ID">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="NameNative" SortExpression="NameNative" HeaderText="Name">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="NameEN" SortExpression="NameEN" HeaderText="NameEN">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="CreditInfoID" SortExpression="CreditInfoID" HeaderText="CreditInfoID">
																		<ItemStyle CssClass="padding"></ItemStyle>
																	</asp:BoundColumn>
																</Columns>
															</asp:datagrid></TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
