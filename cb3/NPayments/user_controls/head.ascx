<%@ Control Language="c#" AutoEventWireup="false" Codebehind="head.ascx.cs" Inherits="NPayments.user_controls.head" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<HTML>
	<HEAD>
		<title>head</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="http://www.lt.is/cy_styles.css" type="text/css" rel="stylesheet">
		<style type="text/css"> br.pageEnd {page-break-after: always}
		</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<table id="Table3" borderColor="#0000ff" cellSpacing="0" cellPadding="0" width="600" align="center"
			bgColor="#ffffff" border="0">
			<TBODY>
				<tr>
					<td vAlign="top" align="left" height="50">&nbsp;&nbsp;<IMG src="/CreditInfoGroup_lt/img/logo_cyprus.png">
					</td>
				</tr>
				<tr>
					<td vAlign="top" align="center" height="98%">
						<table id="Table1" width="400" align="center">
							<tr>
								<td align="center">
									<asp:HyperLink id="hlHome" NavigateUrl="../../Default.aspx" runat="server">Home</asp:HyperLink><A></A></td>
								<td align="center"><asp:hyperlink id="hlOverview" runat="server" NavigateUrl="../ClaimOverView.aspx">Overview</asp:hyperlink></td>
								<td align="center"><asp:hyperlink id="hlSearch" runat="server" NavigateUrl="../FindClaim.aspx">Search</asp:hyperlink></td>
								<td align="center">
									<asp:LinkButton id="lbnLogout" runat="server">Logout</asp:LinkButton></td>
							</tr>
						</table>
						<table class="list" id="Table2" cellSpacing="0" cellPadding="2" width="600" align="center">
							<TBODY>
								<tr class="dark-row">
									<td align="center">&nbsp;
										<asp:imagebutton id="imbtEN" runat="server" ImageUrl="../../img/flag_uk.png" Height="20px" CausesValidation="False"></asp:imagebutton></td>
									<td align="center"><asp:imagebutton id="imgbtCYP" runat="server" ImageUrl="../../img/flag_cy.png" Height="20px" CausesValidation="False"></asp:imagebutton></td>
									<td align="center"><asp:hyperlink id="hlCheques" runat="server" NavigateUrl="../NewChDcAof.aspx?head=true">cheques/c. d./a.o.d.</asp:hyperlink></td>
									<td align="center"><asp:hyperlink id="hlCaseACompanies" runat="server" NavigateUrl="../CaseACompanies.aspx?head=true">Case Decisons v. Companys</asp:hyperlink></td>
									<td align="center"><asp:hyperlink id="hlBankruptie" runat="server" NavigateUrl="../NewClaim.aspx?head=true">Bankruptie</asp:hyperlink></td>
									<td align="center"><asp:hyperlink id="hlLetters" runat="server" NavigateUrl="../GenerateLetter.aspx">Letters</asp:hyperlink></td>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
			</TBODY>
		</table>
	</body>
</HTML>
