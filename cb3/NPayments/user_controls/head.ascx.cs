#region

using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPayments.BLL.Localization;

#endregion

namespace NPayments.user_controls {
    /// <summary>
    ///		Summary description for head.
    /// </summary>
    public class head : UserControl {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected HyperLink hlBankruptie;
        protected HyperLink hlCaseACompanies;
        protected HyperLink hlCheques;
        protected HyperLink hlHome;
        protected HyperLink hlLetters;
        protected HyperLink hlOverview;
        protected HyperLink hlSearch;
        protected ImageButton imbtEN;
        protected ImageButton imgbtCYP;
        protected LinkButton lbnLogout;

        private void Page_Load(object sender, EventArgs e) {
            if (!(Context.User.IsInRole("Administration") || Context.User.IsInRole("Negative Payments"))) {
                Server.Transfer("../NoAuthorization.aspx");
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();
        }

        private void imbtEN_Click(object sender, ImageClickEventArgs e) {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            Session["culture"] = culture;
            // forcing reload to get the proper Culture in global.asax
            Response.AddHeader("Refresh", "0");
        }

        private void imgbtCYP_Click(object sender, ImageClickEventArgs e) {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("el-GR", true);
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            Session["culture"] = culture;
            // forcing reload to get the proper Culture in global.asax
            Response.AddHeader("Refresh", "0");
        }

        private void LocalizeText() {
            hlOverview.Text = rm.GetString("txtOverview", ci);
            hlSearch.Text = rm.GetString("txtSearch", ci);
            hlCheques.Text = rm.GetString("lbChequesAndOther", ci);
            hlBankruptie.Text = rm.GetString("txtBankrupt", ci);
            hlCaseACompanies.Text = rm.GetString("txtCaseACompanies", ci);
            hlLetters.Text = rm.GetString("txtLetters", ci);
            lbnLogout.Text = rm.GetString("txtLogOut", ci);
        }

        private void lbnLogout_Click(object sender, EventArgs e) {
            FormsAuthentication.SignOut();
            Response.AddHeader("Refresh", "0");
            Server.Transfer("../Default.aspx");
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lbnLogout.Click += new System.EventHandler(this.lbnLogout_Click);
            this.imbtEN.Click += new System.Web.UI.ImageClickEventHandler(this.imbtEN_Click);
            this.imgbtCYP.Click += new System.Web.UI.ImageClickEventHandler(this.imgbtCYP_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}