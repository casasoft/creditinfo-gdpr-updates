using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Claims;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;
using Debtor=UserAdmin.BLL.Debtor;

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for NewChDcAof.
    /// </summary>
    public class NewChDcAof : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected Button btnSearchClaimOwner;
        protected Button btnSearchDebtor;
        protected Button btnShowAll;
        protected Button btnSubmitAndClear;
        protected Button btSubmit;
        protected CheckBox chbCBR;
        private int claimStatus;
        private bool company;
        protected CustomValidator CustomValidator1;
        protected CustomValidator CustomValidator2;
        protected CustomValidator CustomValidator3;
        protected DropDownList ddCurrency;
        protected DropDownList ddIssueBanks;
        protected DropDownList ddType;
        protected DataGrid dgClaimOwner;
        protected DataGrid dgDebtors;
        protected HtmlGenericControl divClaimOwner;
        protected HtmlGenericControl divNameSearch;
        private bool EN;
        protected Image imgCompany;
        protected Image imgCompanyClaimOwner;
        protected Image imgIndividual;
        protected Image imgIndividualClaimOwner;
        protected Label lbAgentID;
        protected Label lbBank;
        protected Label lbCaseID;
        protected Label lbChequesAndOther;
        protected Label lbClaim;
        protected Label lbClaimAmount;
        protected Label lbClaimID;
        protected Label lbClaimOwnerName;
        protected Label lbComment;
        protected Label lbDates;
        protected Label lbDebtor;
        protected Label lbDebtorFirstName;
        protected Label lbDebtorID;
        protected Label lbDebtorSurname;
        protected Label lbInternalComment;
        protected Label lblClaimOwner;
        protected Label lblClaimOwnerCIID;
        protected Label lblClaimOwnerNameSearchDatagridHeader;
        protected Label lblClaimOwnerNameSearchDatagridIcons;
        protected Label lblInfo;
        protected Label lblMsg;
        protected Label lblNameSearchDatagridHeader;
        protected Label lblNameSearchDatagridIcons;
        protected Label lblNationalID;
        protected Label lbOnHold;
        protected Label lbOther;
        protected Label lbPayment;
        protected Label lbRegisted;
        protected Label lbRegister;
        protected Label lbRegistration;
        protected Label lbStatus;
        protected Label lbType;
        private NPaymentsFactory myFact = new NPaymentsFactory();
        private bool newRegistration = true;
        protected RadioButton rbtCompany;
        protected RadioButton rbtIndividual;
        protected RadioButton rbtnCompanyClaimOwner;
        protected RadioButton rbtnIndividualClaimOwner;
        private string redirectPage = "";
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected RequiredFieldValidator RequiredFieldValidator4;
        protected RequiredFieldValidator RequiredFieldValidator5;
        protected RequiredFieldValidator rfvCIID;
        protected RequiredFieldValidator rfvClaimOwnerCIID;
        protected HtmlTable tableClaimOwnerNameSearchGrid;
        protected HtmlTable tableNameSearchGrid;
        protected TextBox tbAgent;
        protected TextBox tbCaseID;
        protected TextBox tbClaimAmount;
        protected TextBox tbClaimDate;
        protected TextBox tbClaimDelayedDate;
        protected TextBox tbClaimOwnerNative;
        protected TextBox tbComment;
        protected TextBox tbDebtorFirstName;
        protected TextBox tbDebtorID;
        protected TextBox tbDebtorSurname;
        protected TextBox tbInternalComment;
        protected TextBox tbPageID;
        protected TextBox tbPaymentDate;
        protected TextBox tbRegDate;
        protected TextBox tbRegister;
        protected TextBox tbStatus;
        protected TextBox txtClaimOwnerCIID;
        protected TextBox txtNationalID;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "205";
            lblMsg.Visible = false;
            if (Request.QueryString["head"] != null) {
                ClearSessionVariables();
            }
            // check the current culture
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (Request.QueryString["update"] != null) {
                newRegistration = false;
            }

            if (Request.QueryString["company"] != null) {
                company = true;
            }

            AddEnterEvent();

            if (Session["ClaimStatus"] != null) {
                claimStatus = (int) Session["ClaimStatus"];
            }
            if (!IsPostBack) {
                tableNameSearchGrid.Visible = false;
                tableClaimOwnerNameSearchGrid.Visible = false;
                InitControls();
                SetColumnsHeaderByCulture();
                SetColumnsByCulture();
                //	rbtIndividual.Checked=true;
                //	Session.Remove("ClaimStatus");
            }
            if (newRegistration) {
                claimStatus = 1;
            } else {
                if (Request.QueryString["pageid"] != null) {
                    SetRedirectPage(int.Parse(Request.QueryString["pageid"]));
                    tbPageID.Text = Request.QueryString["pageid"];
                }
            }
            if (tbPageID.Text != "") {
                SetRedirectPage(int.Parse(tbPageID.Text));
            }
        }

        private void InitControls() {
            int creditInfoID = -1;
            InitDDBoxes();
            if (newRegistration) {
                claimStatus = 1;
                tbRegDate.Text = DateTime.Now.ToShortDateString();
                var myStatus = myFact.GetStatus(claimStatus);
                tbStatus.Text = EN ? myStatus.StateEN : myStatus.StateNative;

                if (Session["ClaimOwnerInfo"] != null && Session["ClaimOwnerInfo"].ToString() == "true") {
                    if (Session["CreditInfoID"] != null) {
                        txtClaimOwnerCIID.Text = Session["CreditInfoID"].ToString();
                    }
                    if (Session["ClaimOwnerNameNative"] != null) {
                        tbClaimOwnerNative.Text = Session["ClaimOwnerNameNative"].ToString().Trim();
                    }
                    if (Session["ClaimOwnerNameEN"] != null && tbClaimOwnerNative.Text == "") {
                        tbClaimOwnerNative.Text = Session["ClaimOwnerNameEN"].ToString().Trim();
                    }

                    if (Session["tbDebtorFirstName"] != null) {
                        tbDebtorFirstName.Text = Session["tbDebtorFirstName"].ToString();
                    }
                    if (Session["tbDebtorSurname"] != null) {
                        tbDebtorSurname.Text = Session["tbDebtorSurname"].ToString();
                    }
                    if (Session["tbDebtorID"] != null) {
                        creditInfoID = int.Parse(Session["tbDebtorID"].ToString());
                        tbDebtorID.Text = Session["tbDebtorID"].ToString();
                    }
                    if (Session["txtNationalID"] != null) {
                        txtNationalID.Text = Session["txtNationalID"].ToString();
                    }

                    Session.Remove("tbDebtorFirstName");
                    Session.Remove("tbDebtorSurname");
                    Session.Remove("tbDebtorID");
                    Session.Remove("txtNationalID");

                    Session.Remove("ClaimOwnerInfo");
                    Session.Remove("CreditInfoID");
                    Session.Remove("ClaimOwnerNameNative");
                    Session.Remove("ClaimOwnerNameEN");
                } else {
                    if (Session["CreditInfoID"] != null) {
                        creditInfoID = (int) Session["CreditInfoID"];
                    }
                    if (company) {
                        rbtCompany.Checked = true;
                        rbtCompany_CheckedChanged(this, null);
                        if (Session["CompanyNameNative"] != null) {
                            tbDebtorFirstName.Text = Session["CompanyNameNative"].ToString();
                        }
                        if (Session["CompanyNameEN"] != null) {
                            tbDebtorSurname.Text = Session["CompanyNameEN"].ToString();
                        }
                        Session.Remove("CreditInfoID");
                        Session.Remove("CompanyNameNative");
                        Session.Remove("CompanyNameEN");
                    } else {
                        rbtIndividual.Checked = true;
                        rbtIndividual_CheckedChanged(this, null);
                        if (Session["DebtorFirstName"] != null) {
                            tbDebtorFirstName.Text = Session["DebtorFirstName"].ToString();
                        }
                        if (Session["DebtorSurName"] != null) {
                            tbDebtorSurname.Text = Session["DebtorSurName"].ToString();
                        }
                        Session.Remove("CreditInfoID");
                        Session.Remove("DebtorFirstName");
                        Session.Remove("DebtorSurName");
                    }

                    if (creditInfoID != -1) {
                        tbDebtorID.Text = creditInfoID.ToString();
                    }

                    if (Session["txtClaimOwnerCIID"] != null) {
                        txtClaimOwnerCIID.Text = Session["txtClaimOwnerCIID"].ToString();
                    }
                    if (Session["tbClaimOwnerNative"] != null) {
                        tbClaimOwnerNative.Text = Session["tbClaimOwnerNative"].ToString();
                    }
                    Session.Remove("txtClaimOwnerCIID");
                    Session.Remove("tbCreditorNameNative");
                }
            } else {
                btnSubmitAndClear.Enabled = false;
                var claimID = (int) Session["ClaimID"];
                creditInfoID = (int) Session["CreditInfoID"];
                var myClaim = myFact.GetClaim(claimID);
                btSubmit.Text = rm.GetString("txtUpdate", ci);
                lbClaimID.Text = rm.GetString("lbClaimID", ci) + " : " + myClaim.ClaimID;
                lbClaimID.Visible = true;
                tbDebtorID.Text = myClaim.CreditInfoID.ToString();
                txtClaimOwnerCIID.Text = myClaim.ClaimOwnerCIID.ToString();
                if (myClaim.ClaimOwnerCIID > 0) {
                    string claimOwnerType = myFact.GetCIUserType(myClaim.ClaimOwnerCIID);
                    if (claimOwnerType.Trim().Equals("-1")) // no type found
                    {
                        claimOwnerType = CigConfig.Configure("lookupsettings.individualID");
                            // then set to individual as default
                    }
                    if (claimOwnerType.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                        Indivitual myIndi = myFact.GetIndivitual(myClaim.ClaimOwnerCIID);
                        rbtnCompanyClaimOwner.Checked = false;
                        rbtnIndividualClaimOwner.Checked = true;
                        if (EN) {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameEN)) {
                                tbClaimOwnerNative.Text = myIndi.FirstNameEN + " " + myIndi.SurNameEN;
                            } else {
                                tbClaimOwnerNative.Text = myIndi.FirstNameNative + " " + myIndi.SurNameNative;
                            }
                        } else {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameNative)) {
                                tbClaimOwnerNative.Text = myIndi.FirstNameNative + " " + myIndi.SurNameNative;
                            } else {
                                tbClaimOwnerNative.Text = myIndi.FirstNameEN + " " + myIndi.SurNameEN;
                            }
                        }
                    } else if (claimOwnerType.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                        // company
                        var myComp = myFact.GetCompany(myClaim.ClaimOwnerCIID);
                        rbtnCompanyClaimOwner.Checked = true;
                        rbtnIndividualClaimOwner.Checked = false;
                        if (EN) {
                            tbClaimOwnerNative.Text = !string.IsNullOrEmpty(myComp.NameEN) ? myComp.NameEN : myComp.NameNative;
                        } else {
                            tbClaimOwnerNative.Text = !string.IsNullOrEmpty(myComp.NameNative) ? myComp.NameNative : myComp.NameEN;
                        }
                    }
                }
                tbRegDate.Text = myClaim.RegDate.ToString();
                if (myClaim.ChequeIssuedBank != 0) {
                    ddIssueBanks.SelectedValue = myClaim.ChequeIssuedBank.ToString();
                }
                ddCurrency.SelectedValue = myClaim.CurrencySymbol;
                ddType.SelectedValue = myClaim.ClaimTypeID.ToString();
                if (myClaim.ClaimDate > DateTime.MinValue) {
                    tbClaimDate.Text = myClaim.ClaimDate.ToShortDateString();
                }
                if (myClaim.DelayDate > DateTime.MinValue) {
                    tbClaimDelayedDate.Text = myClaim.DelayDate.ToShortDateString();
                }
                if (myClaim.PaymentDate > DateTime.MinValue) {
                    tbPaymentDate.Text = myClaim.PaymentDate.ToShortDateString();
                }
                tbClaimAmount.Text = myClaim.DAmount.ToString();
                ddCurrency.Items.Add(myClaim.CurrencySymbol);
                tbCaseID.Text = myClaim.CaseNumber;
                tbStatus.Text = myClaim.StateEN;
                Session["ClaimStatus"] = myClaim.StatusID;
                tbRegister.Text = myClaim.RegisterInitials;
                tbComment.Text = myClaim.RegisterCommentNative;
                tbInternalComment.Text = myClaim.RegisterInternalCommentNative;
                tbAgent.Text = myClaim.Agent;
                if (myClaim.CBA != null) {
                    chbCBR.Checked = bool.Parse(myClaim.CBA);
                }

                if (newRegistration) {
                    Session.Remove("ClaimID");
                }
                Session.Remove("CreditInfoID");
            }

            if (creditInfoID == -1) {
                LocalizeText();
                return;
            }

            // the debtors part
            string type = myFact.GetCIUserType(creditInfoID);
            if (type.Trim().Equals("-1")) // no type found
            {
                type = CigConfig.Configure("lookupsettings.individualID"); // then set to individual as default
            }
            if (type.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                Indivitual myIndi = myFact.GetIndivitual(creditInfoID);
                rbtCompany.Checked = false;
                rbtIndividual.Checked = true;
                if (EN) {
                    if (myIndi.FirstNameEN == null || myIndi.FirstNameEN.Trim() == "") {
                        tbDebtorFirstName.Text = myIndi.FirstNameNative;
                        tbDebtorSurname.Text = myIndi.SurNameNative;
                    } else {
                        tbDebtorFirstName.Text = myIndi.FirstNameEN;
                        tbDebtorSurname.Text = myIndi.SurNameEN;
                    }
                } else {
                    if (myIndi.FirstNameNative == null || myIndi.FirstNameNative.Trim() == "") {
                        tbDebtorFirstName.Text = myIndi.FirstNameEN;
                        tbDebtorSurname.Text = myIndi.SurNameEN;
                    } else {
                        tbDebtorFirstName.Text = myIndi.FirstNameNative;
                        tbDebtorSurname.Text = myIndi.SurNameNative;
                    }
                }
            } else if (type.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                // company
                rbtCompany.Checked = true;
                rbtIndividual.Checked = false;
                Company myComp = myFact.GetCompany(creditInfoID);
                if (myComp.NameEN != null) {
                    lbDebtorSurname.Text = myComp.NameEN;
                }
                tbDebtorFirstName.Text = myComp.NameNative;
            }
            LocalizeText();
            //		InitDDBoxes();
        }

        public void SetRedirectPage(int index) { redirectPage = CigConfig.Configure("lookupsettings.redirectpageID" + index); }

        public void InitDDBoxes() {
            //Currency
            ddCurrency.DataSource = myFact.GetCurrencyListAsDataSet();
            ddCurrency.DataTextField = "CurrencyCode";
            ddCurrency.DataValueField = "CurrencyCode";
            ddCurrency.DataBind();
            //	this.ddCurrency.Items.Insert(0,"CYP");
            //	this.ddCurrency.Items.Insert(1,"EUR");
            // if currency order key in config then order the currency list ...
            if (CigConfig.Configure("lookupsettings.orderCurrencyList") != null) {
                OrderCurrencyList(CigConfig.Configure("lookupsettings.orderCurrencyList"));
            }

            // IssuedBanks
            ddIssueBanks.DataSource = myFact.GetIssuedBanksAsDataSet(EN);
            ddIssueBanks.DataTextField = EN ? "NameEN" : "NameNative";

            ddIssueBanks.DataValueField = "BankID";
            ddIssueBanks.DataBind();
            var theItem = new ListItem("N/A", "0");
            ddIssueBanks.Items.Insert(0, theItem);

            // types
            ddType.DataSource = myFact.GetCaseTypesAsDataSet(CigConfig.Configure("lookupsettings.ct_chequesandothers"));
            ddType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddType.DataValueField = "CaseTypeID";
            ddType.DataBind();

            if (!newRegistration) {
                return;
            }
            var index = ddType.Items.IndexOf(ddType.Items.FindByText("N/A"));
            if (index > 0) {
                ddType.SelectedIndex = index;
            }
        }

        private void OrderCurrencyList(string orderList) {
            var strArray = orderList.Split(new[] {','});
            var index = 0;
            foreach (string id in strArray) {
                var li = ddCurrency.Items.FindByValue(id);
                ddCurrency.Items.Remove(li);
                ddCurrency.Items.Insert(index, li);
                index++;
            }
        }

        private void btSubmit_Click(object sender, EventArgs e) {
            var myClaim = new Claim();
            myFact = new NPaymentsFactory();
            if (Page.IsValid) {
                try {
                    myClaim.CreditInfoID = int.Parse(tbDebtorID.Text);
                    myClaim.ClaimOwnerCIID = int.Parse(txtClaimOwnerCIID.Text);
                    try {
                        myClaim.ClaimTypeID = int.Parse(ddType.SelectedItem.Value);
                    } catch (Exception err) {
                        Logger.WriteToLog(
                            "NewChDcAof,aspx, btSubmit, myClaim.ClaimTypeID = int.Parse(this.ddType.SelectedItem.Value) " +
                            err.Message,
                            true);
                    }
                    myClaim.StatusID = claimStatus; //int.Parse(this.tbStatus.Text); // athuga me� value h�r!
                    myClaim.CaseNumber = tbCaseID.Text;
                    myClaim.CurrencySymbol = ddCurrency.SelectedItem.Text;
                    myClaim.CurrencyEN = ""; // unneccessery field?
                    try {
                        myClaim.DAmount = Convert.ToDecimal(tbClaimAmount.Text);
                    } catch (Exception err) {
                        Logger.WriteToLog(
                            "NewChDcAof,aspx, btSubmit, myClaim.DAmount = Convert.ToDecimal(this.tbClaimAmount.Text) " +
                            err.Message,
                            true);
                    }
                    myClaim.RegisterCommentNative = tbComment.Text;
                    myClaim.RegisterInternalCommentNative = tbInternalComment.Text;
                    myClaim.Agent = tbAgent.Text;
                    myClaim.CBA = chbCBR.Checked.ToString();
                    if (ddIssueBanks.SelectedIndex != 0) {
                        myClaim.ChequeIssuedBank = int.Parse(ddIssueBanks.SelectedItem.Value);
                    }
                    try {
                        myClaim.InformationSourceID =
                            int.Parse(CigConfig.Configure("lookupsettings.neutralnformationsourceid"));
                    } catch (Exception err) {
                        Logger.WriteToLog(
                            "NewChDcAof,aspx, btSubmit, mmyClaim.InformationSourceID = int.Parse(ConfigurationSettings " +
                            err.Message,
                            true);
                    }

                    IFormatProvider format = CultureInfo.CurrentCulture;
                    if (tbRegDate.Text != "") {
                        myClaim.RegDate = DateTime.Parse(tbRegDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbClaimDate.Text != "") {
                        myClaim.ClaimDate = DateTime.Parse(tbClaimDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbClaimDelayedDate.Text != "") {
                        myClaim.DelayDate = DateTime.Parse(
                            tbClaimDelayedDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbPaymentDate.Text != "") {
                        myClaim.PaymentDate = DateTime.Parse(
                            tbPaymentDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    //			myClaim.RegisterID = 9001; // taka �etta �r session �egar loggin mechanisminn ver�ur � lagi
                    try {
                        myClaim.RegisterID = (int) Session["UserCreditInfoID"];
                    } catch (Exception err) {
                        Logger.WriteToLog(
                            "NewChDcAof,aspx, btSubmit, myClaim.RegisterID = (int) Session[\"UserCreditInfoID\"] " +
                            err.Message,
                            true);
                    }
                    if (claimStatus != 0) {
                        //	myClaim.StatusID = 
                        myClaim.SetClaimStatus();
                            // claimStatus; //int.Parse(this.tbStatus.Text); // athuga me� value h�r!
                    } else {
                        myClaim.StatusID = int.Parse(CigConfig.Configure("lookupsettings.formerstatusid"));
                    }
                } catch (Exception err) {
                    Logger.WriteToLog("Exception on NewChDAof.aspx caught, message is : " + err.Message, true);
                    lblMsg.Text = rm.GetString("errMsg2", ci);
                    lblMsg.Visible = true;
                    return;
                }
                if (newRegistration) {
                    if (!myFact.AddNewClaim(myClaim)) {
                        Logger.WriteToLog("NewChDcAof,aspx, btSubmit, if(!myFact.AddNewClaim(myClaim))", true);
                        lblMsg.Text = rm.GetString("errMsg1", ci);
                        lblMsg.Visible = true;
                        return;
                    }
                    lblMsg.Text = rm.GetString("txtClaimRegistered", ci);
                    lblMsg.Visible = true;
                    ClearSessionVariables();
                    PrepareControlsForNewRegistration();
                } else {
                    myClaim.LastUpdate = DateTime.Now;
                    myClaim.ClaimID = (int) Session["ClaimID"];
                    if (!myFact.UpdateClaim(myClaim)) {
                        Logger.WriteToLog("NewChDcAof,aspx, btSubmit, if(!myFact.UpdateClaim(myClaim)) ", true);
                        lblMsg.Text = rm.GetString("errMsg1", ci);
                        lblMsg.Visible = true;
                        return;
                    }
                    //		ClearForm();
                    lblMsg.Text = rm.GetString("txtClaimUpdated", ci);
                    lblMsg.Visible = true;
                    //If redirect-page is set then redirect
                    if (!string.IsNullOrEmpty(redirectPage)) {
                        Response.Redirect(redirectPage);
                    }
                    // halda g�mlu gildunum ...�etta �arf �� a� pr�fa betur!!
                    //		ClearSessionVariables();
                    //		PrepareControlsForNewRegistration();
                }
                ClearForm(false);
            } else // if(Page.IsValid)
            {
                Logger.WriteToLog("NewChDcAof.aspx Page.IsValid = false", true);
            }
        }

        public void DateValidate(object source, ServerValidateEventArgs value) {
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        private void ClearForm(bool clearDebtor) {
            if (clearDebtor) {
                tbDebtorID.Text = "";
                tbDebtorFirstName.Text = "";
                tbDebtorSurname.Text = "";
                txtNationalID.Text = "";
            }
            tbClaimOwnerNative.Text = "";
            txtClaimOwnerCIID.Text = "";
            tbAgent.Text = "";
            tbClaimDate.Text = "";
            tbClaimAmount.Text = "";
            tbCaseID.Text = "";
            tbStatus.Text = "";
            tbRegister.Text = "";
            tbComment.Text = "";
            tbClaimDelayedDate.Text = "";
            tbPaymentDate.Text = "";
            tbInternalComment.Text = "";
            tableNameSearchGrid.Visible = false;
            tableClaimOwnerNameSearchGrid.Visible = false;
            Session.Remove("ClaimStatus");
        }

        private void ClearSessionVariables() {
            Session["ClaimStatus"] = null;
            Session["CreditInfoID"] = null;
            Session["DebtorFirstName"] = null;
            Session["DebtorSurName"] = null;
            Session["ClaimID"] = null;
        }

        private void PrepareControlsForNewRegistration() {
            newRegistration = true;
            claimStatus = 1;
            tbRegDate.Text = DateTime.Now.ToString();
            Status myStatus = myFact.GetStatus(claimStatus);
            tbStatus.Text = myStatus.StateEN;
        }

        private void LocalizeText() {
            lbDebtorID.Text = rm.GetString("lbDebtorID", ci);
            lbAgentID.Text = rm.GetString("lbAgentID", ci);
            lbDates.Text = rm.GetString("lbDates", ci);
            lbRegisted.Text = rm.GetString("lbRegisted", ci);
            lbClaim.Text = rm.GetString("lbClaim", ci);
            lbOnHold.Text = rm.GetString("lbOnHold", ci);
            lbPayment.Text = rm.GetString("lbPayment", ci);
            lbOther.Text = rm.GetString("lbOther", ci);
            lbClaimAmount.Text = rm.GetString("lbClaimAmount", ci);
            lbCaseID.Text = rm.GetString("lbCaseID", ci);
            lbRegistration.Text = rm.GetString("lbRegistration", ci);
            lbStatus.Text = rm.GetString("lbStatus", ci);
            lbRegister.Text = rm.GetString("lbRegister", ci);
            lbComment.Text = rm.GetString("lbComment", ci);
            lbType.Text = rm.GetString("lbType", ci);
            btSubmit.Text = rm.GetString("txtSubmitAndNew", ci);
            if (company) {
                lbDebtorFirstName.Text = rm.GetString("txtCompany", ci);
                lbDebtorSurname.Text = rm.GetString("lbCompanyNameEN", ci);
            } else {
                lbDebtorFirstName.Text = rm.GetString("txtFirstname", ci);
                lbDebtorSurname.Text = rm.GetString("txtSurname", ci);
            }
            lbClaimOwnerName.Text = rm.GetString("lbClaimOwnerName", ci);
            lbBank.Text = rm.GetString("lbBank", ci);
            RequiredFieldValidator3.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator4.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator5.ErrorMessage = rm.GetString("txtValueMissing", ci);
            CustomValidator1.ErrorMessage = rm.GetString("CustomValidator1", ci);
            btnShowAll.Text = rm.GetString("txtShowAllCases", ci);
            chbCBR.Text = rm.GetString("lbCBR", ci);
            lbChequesAndOther.Text = rm.GetString("lbChequesAndOther", ci);
            lbDebtor.Text = rm.GetString("txtDebtor", ci);
            CustomValidator2.Text = rm.GetString("CustomValidator2", ci);
            CustomValidator3.Text = rm.GetString("CustomValidator3", ci);
            lbInternalComment.Text = rm.GetString("lbInternalComment", ci);
            lblNationalID.Text = rm.GetString("lbNationalID", ci);
            btnSubmitAndClear.Text = rm.GetString("txtSubmitAndClear", ci);
            rfvCIID.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblClaimOwnerCIID.Text = rm.GetString("txtClaimOwnerCIID", ci);
            lblClaimOwner.Text = rm.GetString("txtClaimOwner", ci);
            btnSearchClaimOwner.Text = rm.GetString("txtSearch", ci);
            btnSearchDebtor.Text = rm.GetString("txtSearch", ci);
            lblClaimOwnerNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);

            //this.rbtIndividual.Text = rm.GetString("txtIndividual",ci);
            imgIndividual.AlternateText = rm.GetString("txtIndividual", ci);
            imgIndividualClaimOwner.AlternateText = rm.GetString("txtIndividual", ci);
            //this.rbtCompany.Text = rm.GetString("txtCompany",ci);
            imgCompany.AlternateText = rm.GetString("txtCompany", ci);
            imgCompanyClaimOwner.AlternateText = rm.GetString("txtCompany", ci);
        }

        private void AddEnterEvent() {
            Control frm = FindControl("vansk");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (!(ctrl is DropDownList)) {
                    continue;
                }
                ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
            }
        }

        private void btnShowAll_Click(object sender, EventArgs e) {
            if (tbDebtorID.Text != "") {
                Session["CreditInfoID"] = tbDebtorID.Text;
                Server.Transfer("ShowAllCases.aspx");
            }
        }

        private void SetColumnsByCulture() {
            dgDebtors.Columns[0].Visible = true;
            dgDebtors.Columns[1].Visible = true;
            dgDebtors.Columns[2].Visible = true;
            dgDebtors.Columns[3].Visible = true;
            dgDebtors.Columns[4].Visible = false;
            dgDebtors.Columns[5].Visible = false;
            dgDebtors.Columns[6].Visible = false;
            dgDebtors.Columns[7].Visible = false;
            dgDebtors.Columns[8].Visible = true;
        }

        private void SetColumnsHeaderByCulture() {
            dgDebtors.Columns[1].HeaderText = rm.GetString("lbNationalID", ci);
            dgDebtors.Columns[2].HeaderText = rm.GetString("lbName", ci);
            dgDebtors.Columns[3].HeaderText = rm.GetString("lbAddress", ci);
            dgDebtors.Columns[8].HeaderText = rm.GetString("lbDebtorID", ci);
        }

        protected void SearchForIndividual(int ciid, string nationalID, string firstName, string surName) {
            tableClaimOwnerNameSearchGrid.Visible = false;
            var myDebt = new Debtor
                         {
                             CreditInfo = ciid,
                             IDNumber1 = nationalID,
                             IDNumber2Type = 1,
                             FirstName = firstName,
                             SurName = surName
                         };

            try {
                var ds = CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True" ? myFact.FindCustomerInNationalAndCreditInfo(myDebt) : myFact.FindCustomer(myDebt);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        //if creditinfoid
                        string sCreditInfoID = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                        if (sCreditInfoID != "") {
                            //this.tdNameSearchGrid.Visible=false;

                            tableNameSearchGrid.Visible = false;
                            string name = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            string[] arrName = name.Split(' ');
                            if (arrName.Length > 0) {
                                if (arrName.Length == 1) {
                                    tbDebtorFirstName.Text = arrName[0];
                                } else {
                                    tbDebtorSurname.Text = arrName[arrName.Length - 1];
                                    tbDebtorFirstName.Text = name.Substring(
                                        0, name.Length - (tbDebtorSurname.Text.Length + 1));
                                }
                            }
                            //this.txtCompanyNameEN.Text=ds.Tables[0].Rows[0]["NameEN"].ToString();
                            tbDebtorID.Text = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                            txtNationalID.Text = ds.Tables[0].Rows[0]["Number"].ToString();
                        } else {
                            dgDebtors.DataSource = ds;
                            dgDebtors.DataBind();
                            tableNameSearchGrid.Visible = true;

                            divNameSearch.Style["HEIGHT"] = "auto"; //dtgrNameSearch.Height.ToString();
                            divNameSearch.Style["OVERFLOW"] = "auto";
                            divNameSearch.Style["OVERFLOW-X"] = "auto";
                        }
                    } else {
                        dgDebtors.DataSource = ds;
                        dgDebtors.DataBind();
                        tableNameSearchGrid.Visible = true;
                        //this.tdNameSearchGrid.Visible=true;

                        var gridRows = dgDebtors.Items.Count;
                        divNameSearch.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                        divNameSearch.Style["OVERFLOW"] = "auto";
                        divNameSearch.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    tableNameSearchGrid.Visible = false;
                    tbDebtorFirstName.Text = firstName;
                    tbDebtorSurname.Text = surName;
                    txtNationalID.Text = nationalID;
                    tbDebtorID.Text = ciid == -1 ? "" : ciid.ToString();
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        protected void SearchForCompany(int ciid, string nationalID, string companyName) {
            tableClaimOwnerNameSearchGrid.Visible = false;
            var myComp = new Company {CreditInfoID = ciid, NationalID = nationalID, NameNative = companyName};

            try {
                var ds = CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True" ? myFact.FindCompanyInNationalAndCreditInfo(myComp) : myFact.FindCompany(myComp);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        tableNameSearchGrid.Visible = false;
                        tbDebtorFirstName.Text = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                        tbDebtorSurname.Text = ds.Tables[0].Rows[0]["NameEN"].ToString().Trim();
                        tbDebtorID.Text = ds.Tables[0].Rows[0]["CreditInfoID"].ToString();
                        txtNationalID.Text = ds.Tables[0].Rows[0]["Number"].ToString().Trim();
                    } else {
                        dgDebtors.DataSource = ds;
                        dgDebtors.DataBind();
                        tableNameSearchGrid.Visible = true;
                        int gridRows = dgDebtors.Items.Count;
                        divNameSearch.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                        divNameSearch.Style["OVERFLOW"] = "auto";
                        divNameSearch.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    tableNameSearchGrid.Visible = false;
                    tbDebtorFirstName.Text = companyName;
                    tbDebtorSurname.Text = "";
                    tbDebtorID.Text = ciid == -1 ? "" : ciid.ToString();
                    txtNationalID.Text = nationalID;
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        private void rbtIndividual_CheckedChanged(object sender, EventArgs e) {
            if (rbtIndividual.Checked) {
                lbDebtorFirstName.Text = rm.GetString("txtFirstname", ci);
                lbDebtorSurname.Text = rm.GetString("txtSurname", ci);
                //			lbtSurNameSearch.Visible=true;
                dgDebtors.DataSource = null;
                dgDebtors.DataBind();
                tableNameSearchGrid.Visible = false;
                ClearForm(true);
            }
        }

        private void rbtCompany_CheckedChanged(object sender, EventArgs e) {
            if (rbtCompany.Checked) {
                lbDebtorFirstName.Text = rm.GetString("txtCompany", ci);
                lbDebtorSurname.Text = rm.GetString("lbCompanyNameEN", ci);
                //			lbtSurNameSearch.Visible=false;
                dgDebtors.DataSource = null;
                dgDebtors.DataBind();
                tableNameSearchGrid.Visible = false;
                ClearForm(true);
            }
        }

        private void dgDebtors_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                int ciid = -1;
                try {
                    ciid = int.Parse(e.Item.Cells[8].Text);
                } catch (Exception) {
                    ciid = -1;
                }
                if (ciid == -1) {
                    //Store claim owner in session if available
                    if (txtClaimOwnerCIID.Text != "") {
                        Session["txtClaimOwnerCIID"] = txtClaimOwnerCIID.Text;
                    }
                    if (tbClaimOwnerNative.Text != "") {
                        Session["tbClaimOwnerNative"] = tbClaimOwnerNative.Text;
                    }

                    if (rbtIndividual.Checked) {
                        //No ciid - then create the individual
                        string firstName = "";
                        string surName = "";
                        string name = e.Item.Cells[4].Text.Trim();
                        if (name == "&nbsp;") {
                            name = "";
                        }
                        string[] arrName = name.Split(' ');
                        if (arrName.Length > 0) {
                            if (arrName.Length == 1) {
                                firstName = arrName[0];
                            } else {
                                surName = arrName[arrName.Length - 1];
                                firstName = name.Substring(0, name.Length - (surName.Length + 1));
                            }
                        }
                        Session["AddIndividual"] = true;
                        Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                        Session["FirstName"] = firstName;
                        Session["SurName"] = surName;
                        if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                            Session["StreetNative"] = e.Item.Cells[6].Text.Trim();
                        }
                        if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                            Session["StreetEN"] = e.Item.Cells[7].Text.Trim();
                        }
                        Server.Transfer("NewCIUser.aspx?pageid=2");
                    } else {
                        Session["AddCompany"] = true;
                        Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                        if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                            Session["NameNative"] = e.Item.Cells[4].Text.Trim();
                        }
                        if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                            Session["NameEN"] = e.Item.Cells[5].Text.Trim();
                        }
                        if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                            Session["StreetNative"] = e.Item.Cells[6].Text.Trim();
                        }
                        if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                            Session["StreetEN"] = e.Item.Cells[7].Text.Trim();
                        }
                        Server.Transfer("NewCIUserC.aspx?pageid=2");
                    }
                } else {
                    if (rbtIndividual.Checked) {
                        SearchForIndividual(ciid, "", "", "");
                    } else {
                        SearchForCompany(ciid, "", "");
                    }
                }
            }
        }

        private void btnSubmitAndClear_Click(object sender, EventArgs e) {
            btSubmit_Click(this, null);
            ClearForm(true);
        }

        private void dgDebtors_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (EN) {
                    //Set name - EN if available, else native
                    if (e.Item.Cells[5].Text != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    }

                    //Set street - EN if available, else native
                    if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    }
                } else {
                    //Set name - Native if available, else EN
                    if (e.Item.Cells[4].Text != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    }

                    //Set street - Native if available, else EN
                    if (e.Item.Cells[6].Text != "" && e.Item.Cells[6].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
                var grid = (DataGrid) sender;
                var label = grid.ID.Equals(dgDebtors.ID) ? lblNameSearchDatagridIcons : lblClaimOwnerNameSearchDatagridIcons;

                WebDesign.CreateExplanationIcons(grid.Columns, label, rm, ci);
            }
        }

        protected void SearchForClaimOwner(int ciid, string firstName) {
            tableNameSearchGrid.Visible = false;
            var myDebt = new Debtor {CreditInfo = ciid};

            string[] arrName = firstName.Split(' ');
            if (arrName.Length > 0) {
                if (arrName.Length == 1) {
                    myDebt.FirstName = arrName[0];
                } else {
                    myDebt.SurName = arrName[arrName.Length - 1];
                    myDebt.FirstName = firstName.Substring(0, firstName.Length - (myDebt.SurName.Length + 1));
                }
            }

            var myComp = new Company {CreditInfoID = ciid, NameNative = firstName};

            try {
                DataSet ds;
                if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True") {
                    ds = rbtnCompanyClaimOwner.Checked ? myFact.FindCompanyInNationalAndCreditInfo(myComp) : myFact.FindCustomerInNationalAndCreditInfo(myDebt);
                } else {
                    ds = rbtnCompanyClaimOwner.Checked ? myFact.FindCompany(myComp) : myFact.FindCustomer(myDebt);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        //if creditinfoid
                        string sCreditInfoID = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                        if (sCreditInfoID != "") {
                            //this.tdNameSearchGrid.Visible=false;

                            tableClaimOwnerNameSearchGrid.Visible = false;
                            tbClaimOwnerNative.Text = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            txtClaimOwnerCIID.Text = sCreditInfoID;
                        } else {
                            dgClaimOwner.DataSource = ds;
                            dgClaimOwner.DataBind();
                            tableClaimOwnerNameSearchGrid.Visible = true;

                            divNameSearch.Style["HEIGHT"] = "auto"; //dtgrNameSearch.Height.ToString();
                            divNameSearch.Style["OVERFLOW"] = "auto";
                            divNameSearch.Style["OVERFLOW-X"] = "auto";
                        }
                    } else {
                        dgClaimOwner.DataSource = ds;
                        dgClaimOwner.DataBind();
                        tableClaimOwnerNameSearchGrid.Visible = true;

                        int gridRows = dgClaimOwner.Items.Count;
                        divClaimOwner.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                        divClaimOwner.Style["OVERFLOW"] = "auto";
                        divClaimOwner.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    tableClaimOwnerNameSearchGrid.Visible = false;
                    tbClaimOwnerNative.Text = firstName;
                    txtClaimOwnerCIID.Text = ciid == -1 ? "" : ciid.ToString();
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        private void dgClaimOwner_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (EN) {
                    //Set name - EN if available, else native
                    if (e.Item.Cells[5].Text != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    }

                    //Set street - EN if available, else native
                    if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    }
                } else {
                    //Set name - Native if available, else EN
                    if (e.Item.Cells[4].Text != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    }

                    //Set street - Native if available, else EN
                    if (e.Item.Cells[6].Text != "" && e.Item.Cells[6].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
                WebDesign.CreateExplanationIcons(dgClaimOwner.Columns, lblClaimOwnerNameSearchDatagridIcons, rm, ci);
            }
        }

        private void dgClaimOwner_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                int ciid = -1;
                if (e.Item.Cells[8].Text != "") {
                    try {
                        ciid = int.Parse(e.Item.Cells[8].Text);
                    } catch (Exception) {
                        ciid = -1;
                    }
                }
                if (ciid == -1) {
                    //Then ciid is not found - then we need to create the user - redirect the user
                    //to NewCIUserC.aspx or NewCIUser.aspx

                    //If company already selected - then store in session					
                    if (txtNationalID.Text != "") {
                        Session["txtNationalID"] = txtNationalID.Text;
                    }
                    if (tbDebtorSurname.Text != "") {
                        Session["tbDebtorSurname"] = tbDebtorSurname.Text;
                    }
                    if (tbDebtorFirstName.Text != "") {
                        Session["tbDebtorFirstName"] = tbDebtorFirstName.Text;
                    }
                    if (tbDebtorID.Text != "") {
                        Session["tbDebtorID"] = tbDebtorID.Text;
                    }

                    Session["AddClaimOwner"] = true;
                    if (e.Item.Cells[1].Text.Trim() != "&nbsp;") {
                        Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                    }
                    if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                        Session["StreetNative"] = e.Item.Cells[6].Text.Trim();
                    }
                    if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                        Session["StreetEN"] = e.Item.Cells[7].Text.Trim();
                    }
                    if (e.Item.Cells[9].Text.Trim() == "1") {
                        if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                            Session["NameNative"] = e.Item.Cells[4].Text.Trim();
                        }
                        if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                            Session["NameEN"] = e.Item.Cells[5].Text.Trim();
                        }
                        Server.Transfer("NewCIUserC.aspx?pageid=2");
                    } else {
                        if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                            string name = e.Item.Cells[4].Text.Trim();
                            string[] arrName = name.Split(' ');
                            if (arrName.Length > 0) {
                                if (arrName.Length == 1) {
                                    Session["FirstName"] = arrName[0];
                                } else {
                                    string surName = arrName[arrName.Length - 1];
                                    Session["SurName"] = surName;
                                    Session["FirstName"] = name.Substring(0, name.Length - (surName.Length + 1));
                                }
                            }
                        }
                        Server.Transfer("NewCIUser.aspx?pageid=2");
                    }
                } else {
                    txtClaimOwnerCIID.Text = ciid.ToString();
                    tbClaimOwnerNative.Text = e.Item.Cells[2].Text.Trim();
                }
            }
        }

        private void NewClaimOwnerCompany() {
            //If company already selected - then store in session					
            if (txtNationalID.Text != "") {
                Session["txtNationalID"] = txtNationalID.Text;
            }
            if (tbDebtorSurname.Text != "") {
                Session["tbDebtorSurname"] = tbDebtorSurname.Text;
            }
            if (tbDebtorFirstName.Text != "") {
                Session["tbDebtorFirstName"] = tbDebtorFirstName.Text;
            }
            if (tbDebtorID.Text != "") {
                Session["tbDebtorID"] = tbDebtorID.Text;
            }

            Session["AddClaimOwner"] = true;
            if (tbClaimOwnerNative.Text.Trim() != "") {
                Session["NameNative"] = tbClaimOwnerNative.Text.Trim();
            }
            Server.Transfer("NewCIUserC.aspx?pageid=2");
        }

        private void NewClaimOwnerIndividual() {
            //If company already selected - then store in session					
            if (txtNationalID.Text != "") {
                Session["txtNationalID"] = txtNationalID.Text;
            }
            if (tbDebtorSurname.Text != "") {
                Session["tbDebtorSurname"] = tbDebtorSurname.Text;
            }
            if (tbDebtorFirstName.Text != "") {
                Session["tbDebtorFirstName"] = tbDebtorFirstName.Text;
            }
            if (tbDebtorID.Text != "") {
                Session["tbDebtorID"] = tbDebtorID.Text;
            }

            Session["AddClaimOwner"] = true;

            if (tbClaimOwnerNative.Text.Trim() != "") {
                string name = tbClaimOwnerNative.Text.Trim();
                string[] arrName = name.Split(' ');
                if (arrName.Length > 0) {
                    if (arrName.Length == 1) {
                        Session["FirstName"] = arrName[0];
                    } else {
                        string surName = arrName[arrName.Length - 1];
                        Session["SurName"] = surName;
                        Session["FirstName"] = name.Substring(0, name.Length - (surName.Length + 1));
                    }
                }
            }
            Server.Transfer("NewCIUser.aspx?pageid=2");
        }

        private void btnSearchDebtor_Click(object sender, EventArgs e) {
            int ciid = -1;

            try {
                ciid = int.Parse(tbDebtorID.Text);
            } catch (Exception) {
                ciid = -1;
            }

            if (rbtCompany.Checked) {
                SearchForCompany(ciid, txtNationalID.Text, tbDebtorFirstName.Text);
            } else {
                SearchForIndividual(ciid, txtNationalID.Text, tbDebtorFirstName.Text, tbDebtorSurname.Text);
            }
        }

        private void btnSearchClaimOwner_Click(object sender, EventArgs e) {
            int ciid = -1;

            try {
                ciid = int.Parse(txtClaimOwnerCIID.Text.Trim());
            } catch (Exception) {
                ciid = -1;
            }

            SearchForClaimOwner(ciid, tbClaimOwnerNative.Text);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rbtIndividual.CheckedChanged += new System.EventHandler(this.rbtIndividual_CheckedChanged);
            this.rbtCompany.CheckedChanged += new System.EventHandler(this.rbtCompany_CheckedChanged);
            this.btnSearchDebtor.Click += new System.EventHandler(this.btnSearchDebtor_Click);
            this.dgDebtors.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDebtors_ItemCommand);
            this.dgDebtors.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgDebtors_ItemDataBound);
            this.btnSearchClaimOwner.Click += new System.EventHandler(this.btnSearchClaimOwner_Click);
            this.dgClaimOwner.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgClaimOwner_ItemCommand);
            this.dgClaimOwner.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgClaimOwner_ItemDataBound);
            this.CustomValidator2.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.CustomValidator1.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.CustomValidator3.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
            this.btnSubmitAndClear.Click += new System.EventHandler(this.btnSubmitAndClear_Click);
            this.btSubmit.Click += new System.EventHandler(this.btSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}