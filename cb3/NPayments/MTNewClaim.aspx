<%@ Page language="c#" Codebehind="MTNewClaim.aspx.cs" AutoEventWireup="false" Inherits="NPayments.MTNewClaim" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>New Claim</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						MTNewClaim.btSubmit.click(); 
					}
				} 
				function SetFormFocus()
				{
					document.MTNewClaim.txtClaimOwnerNative.focus();
				}
		</script>
	</HEAD>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="MTNewClaim" name="MTNewClaim" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblNewClaim" runat="server">Claim</asp:label>&nbsp;<asp:label id="lblClaimIDText" runat="server" visible="False">ClaimID</asp:label>&nbsp;<asp:label id="lblClaimID" runat="server" visible="False">ClaimID</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td><asp:label id="lbDebtor" runat="server" font-bold="True">Debtor</asp:label></td>
																</tr>
																<tr>
																	<td colspan="4"><asp:radiobutton id="rbtIndividual" runat="server" cssclass="radio" autopostback="True" groupname="DebtorType"></asp:radiobutton>
																		<asp:image id="imgIndividualDebtor" runat="server" imageurl="../img/individual.gif"></asp:image>&nbsp;<asp:radiobutton id="rbtCompany" runat="server" cssclass="radio" autopostback="True" groupname="DebtorType"></asp:radiobutton>
																		<asp:image id="imgCompanyDebtor" runat="server" imageurl="../img/company.gif"></asp:image></td>
																</tr>
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lbDebtorID" runat="server">Debtor CIID</asp:label><br>
																		<asp:textbox id="txtDebtorID" runat="server" maxlength="12"></asp:textbox><asp:requiredfieldvalidator id="rfvCIID" runat="server" errormessage="Value is missing!" controltovalidate="txtDebtorID">*</asp:requiredfieldvalidator></td>
																	<td style="WIDTH: 25%"><asp:label id="lblNationalID" runat="server">National ID</asp:label><br>
																		<asp:textbox id="txtNationalID" runat="server" maxlength="12"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lbDebtorFirstName" runat="server">Debtor first name</asp:label><br>
																		<asp:textbox id="txtDebtorFirstName" runat="server"></asp:textbox></td>
																	<td><br>
																		<asp:button id="btnSearchDebtor" runat="server" cssclass="search_button" text="Search" causesvalidation="False"></asp:button></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableNameSearchGrid" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dgDebtors" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False"
																				allowsorting="True">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="Number" sortexpression="Number" headertext="National ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Name">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Street">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="City">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameNative" sortexpression="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameEN" sortexpression="NameEN" headertext="Name EN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetNative" sortexpression="StreetNative" headertext="Street(N)">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetEN" sortexpression="StreetEN" headertext="Street EN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityNative" sortexpression="CityNative" headertext="City(N)">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityEN" sortexpression="CityEN" headertext="City EN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CreditInfoID" sortexpression="CreditInfoID" headertext="CreditInfoID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td align="right"><asp:button id="btnDebtorClear" runat="server" cssclass="cancel_button" text="Clear" CausesValidation="False"></asp:button>&nbsp;<INPUT class="confirm_button" onclick="GoNordurNidur('txtDebtorID','txtNationalID','txtDebtorFirstName','txtDebtorSurname',document.MTNewClaim.rbtIndividual, 670, 900);"
													type="button" value="..."></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblClaimOwner" runat="server" font-bold="True">Claim owner</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td colspan="3"><asp:radiobutton id="rbtnIndividualClaimOwner" runat="server" cssclass="radio" groupname="ClaimType"
																			checked="True"></asp:radiobutton>
																		<asp:image id="imgIndividualClaimOwner" runat="server" imageurl="../img/individual.gif"></asp:image>&nbsp;<asp:radiobutton id="rbtnCompanyClaimOwner" runat="server" cssclass="radio" groupname="ClaimType"></asp:radiobutton>
																		<asp:image id="imgCompanyClaimOwner" runat="server" imageurl="../img/company.gif"></asp:image></td>
																</tr>
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lblClaimOwnerCIID" runat="server">Claim owner CIID</asp:label><br>
																		<asp:textbox id="txtClaimOwnerCIID" runat="server"></asp:textbox><asp:requiredfieldvalidator id="rfvClaimOwnerCIID" runat="server" errormessage="Value missing!" controltovalidate="txtClaimOwnerCIID">*</asp:requiredfieldvalidator></td>
																	<TD style="WIDTH: 25%">
																		<asp:label id="lblClaimOwnerNationalID" runat="server">National ID</asp:label><br>
																		<asp:textbox id="txtClaimOwnerNationalID" runat="server" maxlength="12"></asp:textbox></TD>
																	<td style="WIDTH: 25%"><asp:label id="lbClaimOwnerName" runat="server">Claim Owner</asp:label><br>
																		<asp:textbox id="txtClaimOwnerNative" runat="server" maxlength="30"></asp:textbox></td>
																	<td><br>
																		<asp:button id="btnSearchClaimOwner" runat="server" cssclass="search_button" text="Search" causesvalidation="False"></asp:button></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableClaimOwnerNameSearchGrid" cellspacing="0" cellpadding="0"
													runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblClaimOwnerNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblClaimOwnerNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divClaimOwner" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dgClaimOwner" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False"
																				allowsorting="True">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="Number" sortexpression="Number" headertext="National ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Name">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Street">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="City">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameNative" sortexpression="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameEN" sortexpression="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetNative" sortexpression="StreetNative" headertext="StreetNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetEN" sortexpression="StreetEN" headertext="StreetEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityNative" sortexpression="CityNative" headertext="City(N)">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityEN" sortexpression="CityEN" headertext="City EN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="CreditInfoID" sortexpression="CreditInfoID" headertext="CreditInfoID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="IsCompany" headertext="IsCompany">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td align="right">
												<asp:button id="btnClaimOwnerClear" runat="server" cssclass="cancel_button" text="Clear" CausesValidation="False"></asp:button>&nbsp;<INPUT class="confirm_button" onclick="GoNordurNidur('txtClaimOwnerCIID','','txtClaimOwnerNative','',document.MTNewClaim.rbtnIndividualClaimOwner, 670, 900);"
													type="button" value="..."></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbDates" runat="server">Dates</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lbRegisted" runat="server">Registed</asp:label><br>
																		<asp:textbox id="txtRegDate" runat="server" Enabled="False"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lbClaim" runat="server">Claim</asp:label><br>
																		<asp:textbox id="txtClaimDate" runat="server"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('txtClaimDate', 250, 250);" type="button" value="..."><asp:customvalidator id="CustomValidator1" runat="server" errormessage="Date input incorrect!" controltovalidate="txtClaimDate"
																			display="Dynamic">*</asp:customvalidator><asp:requiredfieldvalidator id="RequiredFieldValidator5" runat="server" errormessage="Value missing!" controltovalidate="txtClaimDate"
																			display="Dynamic">*</asp:requiredfieldvalidator></td>
																	<td style="WIDTH: 25%"><asp:label id="lbOnHold" runat="server">Delayed</asp:label><br>
																		<asp:textbox id="txtClaimDelayedDate" runat="server"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('txtClaimDelayedDate', 250, 250);" type="button"
																			value="..."><asp:customvalidator id="CustomValidator2" runat="server" errormessage="Date input incorrect" controltovalidate="txtClaimDelayedDate"
																			display="Dynamic">*</asp:customvalidator></td>
																	<td><asp:label id="lbPayment" runat="server">Payment</asp:label><br>
																		<asp:textbox id="txtPaymentDate" runat="server"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('txtPaymentDate', 250, 250);" type="button" value="..."><asp:customvalidator id="CustomValidator3" runat="server" errormessage="Date input incorrect" controltovalidate="txtPaymentDate"
																			display="Dynamic">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td style="WIDTH: 137px" height="23"></td>
																	<td style="WIDTH: 192px" height="23"></td>
																	<td style="WIDTH: 219px" height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbOther" runat="server">Other info</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 50%" colSpan="2"><asp:label id="lbClaimAmount" runat="server">Claim amount</asp:label><br>
																		<asp:textbox id="txtClaimAmount" runat="server" maxlength="10" Width="104px"></asp:textbox>&nbsp;
																		<asp:dropdownlist id="ddCurrency" runat="server" cssclass="short_input"></asp:dropdownlist><asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" errormessage="Value missing!" controltovalidate="txtClaimAmount">*</asp:requiredfieldvalidator>
																		<asp:TextBox id="txtClaimStatus" runat="server" Width="24px" Visible="False"></asp:TextBox></td>
																	<td><asp:label id="lblSourceofInfo" runat="server">Source of information</asp:label><br>
																		<asp:dropdownlist id="ddlSourceOfInfo" runat="server" cssclass="long_input"></asp:dropdownlist></td>
																</tr>
																<tr>
																	<td style="HEIGHT: 57px"><asp:label id="lbCaseID" runat="server">Case number</asp:label><br>
																		<asp:textbox id="txtCaseID" runat="server" Width="104px"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" errormessage="Value missing!" controltovalidate="txtCaseID">*</asp:requiredfieldvalidator></td>
																	<td style="HEIGHT: 57px">
																		<asp:label id="lblClaimStatus" runat="server">Claim status</asp:label>
																		<asp:dropdownlist id="ddlClaimStatus" runat="server" cssclass="input"></asp:dropdownlist></td>
																	<td style="HEIGHT: 57px"><asp:label id="lblClaimType" runat="server">Claim type</asp:label><br>
																		<asp:dropdownlist id="ddlCaseType" runat="server" cssclass="long_input"></asp:dropdownlist></td>
																</tr>
                                                                <tr>
																	
															            <td style="HEIGHT: 57px"><asp:label id="lbldonotdelete" runat="server"> Do Not Delete</asp:label><br>
															                <asp:checkbox id=chkdoNotDelete runat="server" cssclass="radio" ></asp:checkbox></td>
															            <tr>
																</tr>
																<tr>
																	<td style="WIDTH: 376px" height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lbRegistration" runat="server">Registration</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 50%"><asp:label id="lbStatus" runat="server">Status</asp:label><br>
																		<asp:textbox id="txtStatus" runat="server" readonly="True"></asp:textbox></td>
																	<td><asp:label id="lbRegister" runat="server">Register</asp:label><br>
																		<asp:textbox id="txtRegister" runat="server" readonly="True"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lbComment" runat="server">Comment</asp:label><br>
																		<asp:textbox id="txtComment" runat="server" maxlength="200" width="100%" tooltip="NOTE! This comment will be publicly viewable."
																			height="103px" textmode="MultiLine"></asp:textbox></td>
																	<td><asp:label id="lbInternalComment" runat="server">InternalComment</asp:label><br>
																		<asp:textbox id="txtInternalComment" runat="server" width="100%" height="103px" textmode="MultiLine"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left"><asp:label id="lblMsg" runat="server" visible="False" cssclass="error_text">New Claim has been registered</asp:label><asp:validationsummary id="ValidationSummary1" runat="server" width="256px" height="26px"></asp:validationsummary><asp:textbox id="tbPageID" runat="server" visible="False" height="9px"></asp:textbox></td>
														<td align="right">
															<asp:button id="btnShowAll" runat="server" cssclass="gray_button" text="Show all" causesvalidation="False"></asp:button><asp:button id="btnSubmitAndClear" runat="server" cssclass="confirm_button" text="submit &amp; clear"></asp:button><asp:button id="btSubmit" runat="server" cssclass="confirm_button" text="submit &amp; new"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
