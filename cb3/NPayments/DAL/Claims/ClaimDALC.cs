#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using Logging.BLL;
using NPayments.BLL.Claims;
using NPayments.BLL.Letter;
using cb3;

#endregion

using Cig.Framework.Base.Configuration;
using cb3.Audit;

namespace NPayments.DAL.Claims {
    /// <summary>
    /// Summary description for ClaimDALC.
    /// </summary>
    public class ClaimDALC : BaseDALC {
        private const string className = "ClaimDALC";
        private readonly string connection;
        public ClaimDALC() { connection = DatabaseHelper.ConnectionString(); }

        /// <summary>
        /// Fills out information source and type fields of the letter object provided
        /// </summary>
        /// <param name="letter">The letter to fill out</param>
        /// <returns>True if successfullt filled the letter, false otherwise</returns>
        public bool FillInfoSourceAndType(Letter letter) {
            const string funcName = "FillInfoSourceAndType(NPayments.BLL.Letter.Letter letter) ";
            var qBuilder =
                new StringBuilder(
                    "select np_InformationSource.NameNative AS ISNative, np_InformationSource.NameEN AS ISEN, ");
            qBuilder.Append("np_CaseTypes.TypeNative, np_CaseTypes.TypeEN ");
            qBuilder.Append("from np_Claims ");
            qBuilder.Append(
                "LEFT JOIN np_InformationSource ON np_Claims.InformationSourceID = np_InformationSource.InformationSourceID ");
            qBuilder.Append(
                "LEFT JOIN np_CaseTypes ON np_Claims.ClaimTypeID = np_CaseTypes.CaseTypeID where np_Claims.id = ");
            qBuilder.Append(letter.Debtor_Ref);
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myOleDbCommand = new OleDbCommand(qBuilder.ToString(), myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            letter.InformationSource = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            letter.InformationSourceEN = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            letter.Type = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            letter.TypeEN = reader.GetString(3);
                        }
                        return true;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName, e, true);
            }
            return false;
        }

        public DataSet GetCurrencyListAsDataSet() {
            var mySet = new DataSet();
            const string funcName = "GetCurrencyListAsDataSet";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_Currency", myOleDbConn);
                    myAdapter.Fill(mySet);
                    return mySet;
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName + e.Message, true);
            }
            return mySet;
        }

        public DataSet GetInformationSourceListAsDataSet() {
            var mySet = new DataSet();
            const string funcName = "GetInformationSourceListAsDataSet";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_InformationSource", myOleDbConn);
                    myAdapter.Fill(mySet);
                    return mySet;
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName + e.Message, true);
            }
            return mySet;
        }

        public DataSet GetCaseTypesAsDataSet(string CaseID) {
            string sTemp = CaseID;
            string[] tempArr = sTemp.Split(new[] {',', ' '});
            int count = tempArr.Length;
            //	type1 = int.Parse(tempArr[0]);
            //	type2 = int.Parse(tempArr[1]);
            var mySet = new DataSet();
            const string funcName = "GetCaseTypesAsDataSet";
            string SQL = "SELECT * FROM np_CaseTypes WHERE ";
            for (int i = 0; i < count; i++) {
                if (i > 0) {
                    SQL += "OR ";
                }
                SQL += "CaseID = " + tempArr[i] + " ";
            }
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    //	myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_CaseTypes WHERE CaseID = "+ CaseID +"",myOleDbConn);
                    myAdapter.SelectCommand = new OleDbCommand(SQL, myOleDbConn);
                    myAdapter.Fill(mySet);
                    return mySet;
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName + e.Message, true);
            }
            return mySet;
        }

        public DataSet GetCaseTypesAsDataSet() {
            var mySet = new DataSet();
            const string funcName = "GetCaseTypesAsDataSet";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_CaseTypes", myOleDbConn);
                    myAdapter.Fill(mySet);
                    return mySet;
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName + e.Message, true);
            }
            return mySet;
        }

        public bool AddNewClaim(Claim myClaim) {
            const string funcName = "AddNewClaim";
            OleDbCommand myOleDbCommand;
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    myOleDbConn.Open();
                    // begin transaction...
                    var myTrans = myOleDbConn.BeginTransaction();
                    try {
                        // insert into np_Claims
                        myOleDbCommand =
                            new OleDbCommand(
                                "INSERT INTO np_Claims(CreditInfoID,ClaimOwnerCIID,ClaimTypeID," +
                                "InformationSourceID,StatusID,CaseNr,CurrencyID,Amount,ClaimDate," +
                                "AnnouncementDate,UnregistedDate,DelayDate,PaymentDate,RegisterID,Valid,GazetteYear,GazettePage,AppliedForByDebtor," +
                                "RegisterCommentNative,RegisterCommentEN,RegisterInternalCommentNative,Agent,ChequeIssuedBank,CaseDecisionsType, CBA, DoNotDelete) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                                myOleDbConn) {Transaction = myTrans};
                        var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = myClaim.CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("ClaimOwnerCIID", OleDbType.Integer)
                                  {
                                      Value = myClaim.ClaimOwnerCIID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("ClaimTypeID", OleDbType.Integer)
                                  {
                                      Value = myClaim.ClaimTypeID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        //If InformationSource is 0 - then get the lowest index from the database and set as default
                        if (myClaim.InformationSourceID <= 0) {
                            myClaim.InformationSourceID = GetLowestIndexFromInformationSource();
                        }

                        myParam = new OleDbParameter("InformationSourceID", OleDbType.Integer)
                                  {
                                      Value = myClaim.InformationSourceID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
/*
						myParam = new OleDbParameter("RegDate",OleDbType.Date);
						myParam.Value = myClaim.RegDate;
						myParam.Direction = ParameterDirection.Input;
						myOleDbCommand.Parameters.Add(myParam);
*/
                        myParam = new OleDbParameter("StatusID", OleDbType.Integer)
                                  {
                                      Value = myClaim.StatusID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("CaseNr", OleDbType.VarChar)
                                  {
                                      Value = myClaim.CaseNumber,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("CurrencyID", OleDbType.VarChar)
                                  {
                                      Value = myClaim.CurrencySymbol,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("Amount", OleDbType.Decimal)
                                  {
                                      Value = myClaim.DAmount,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("ClaimDate", OleDbType.Date)
                                  {
                                      Value = myClaim.ClaimDate,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("AnnouncementDate", OleDbType.Date);
                        if (myClaim.AnnouncementDate > DateTime.MinValue) {
                            myParam.Value = myClaim.AnnouncementDate;
                        } else {
                            myParam.Value = null;
                        }
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("UnregistedDate", OleDbType.Date);
                        if (myClaim.UnregistedDate > DateTime.MinValue) {
                            myParam.Value = myClaim.UnregistedDate;
                        } else {
                            myParam.Value = null;
                        }
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("DelayDate", OleDbType.Date);
                        if (myClaim.DelayDate > DateTime.MinValue) {
                            myParam.Value = myClaim.DelayDate;
                        } else {
                            myParam.Value = null;
                        }
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("PaymentDate", OleDbType.Date);
                        if (myClaim.PaymentDate > DateTime.MinValue) {
                            myParam.Value = myClaim.PaymentDate;
                        } else {
                            myParam.Value = null;
                        }
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("RegisterID", OleDbType.Integer)
                                  {
                                      Value = myClaim.RegisterID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("Valid", OleDbType.Boolean)
                                  {
                                      Value = myClaim.Valid,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("GazetteYear", OleDbType.Integer)
                                  {
                                      Value = myClaim.GazatteYear,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("GazettePage", OleDbType.Integer)
                                  {
                                      Value = myClaim.GazettePage,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("AppliedByForByDebtor", OleDbType.VarChar)
                                  {
                                      Value = (myClaim.AppliedForByDebtor ?? ""),
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        // Insert Native and EN strings
                        CreateParameterFromString(
                            myOleDbCommand,
                            "RegisterCommentNative",
                            "RegisterCommentEN",
                            ParameterDirection.Input,
                            myClaim.RegisterCommentNative,
                            myClaim.RegisterCommentEN,
                            OleDbType.WChar);

                        myParam = new OleDbParameter("RegisterInternalCommentNative", OleDbType.WChar)
                                  {
                                      Value = myClaim.RegisterInternalCommentNative,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("Agent", OleDbType.WChar)
                                  {
                                      Value = myClaim.Agent,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        //If CheckIssueBank is 0 - then get the lowest index from the database and set as default
                        if (myClaim.ChequeIssuedBank <= 0) {
                            myClaim.ChequeIssuedBank = GetLowestIndexFromCheckIssueBanks();
                        }

                        myParam = new OleDbParameter("ChequeIsssuedBank", OleDbType.Integer)
                                  {
                                      Value = myClaim.ChequeIssuedBank,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("CaseDecisionsType", OleDbType.VarChar)
                                  {
                                      Value = myClaim.CaseDecisionType,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("CBA", OleDbType.VarChar)
                                  {
                                      Value = myClaim.CBA,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DoNotDelete", OleDbType.Boolean)
                        {
                            Value = myClaim.DoNotDelete,
                            Direction = ParameterDirection.Input
                        };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                        myTrans.Commit();
                    } catch (Exception e) {
                        Logger.WriteToLog(className + " : " + funcName + " : " + e.Message, true);
                        myTrans.Rollback();
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName + e.Message, true);
                return false;
            }
            return true;
        }

        public bool UpdateClaim(Claim myClaim) {
            const string funcName = "UpdateClaim(Claim myClaim)";
            OleDbCommand myOleDbCommand;
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    myOleDbConn.Open();
                    // begin transaction...
                    var myTrans = myOleDbConn.BeginTransaction();
                    try {
                        // update np_Claims
                        myOleDbCommand = myClaim.AnnouncementDate > DateTime.MinValue ? new OleDbCommand(
                                                                                            "UPDATE np_Claims SET CreditInfoID = ?,ClaimOwnerCIID = ?,ClaimTypeID = ?," +
                                                                                            "InformationSourceID = ?,RegDate = ?,StatusID = ?,CaseNr = ?,CurrencyID = ?,Amount = ?,ClaimDate = ?," +
                                                                                            "AnnouncementDate = ?,UnregistedDate = ?,DelayDate = ?,PaymentDate = ?,RegisterID = ?,Valid = ?,GazetteYear = ?,GazettePage = ?,AppliedForByDebtor = ?," +
                                                                                            "RegisterCommentNative = ?,RegisterCommentEN = ?, LastUpdate = ?, ChequeIssuedBank = ?, CaseDecisionsType = ?,Agent = ?, CBA = ?, RegisterInternalCommentNative = ? WHERE ID = ?",
                                                                                            myOleDbConn) : new OleDbCommand(
                                                                                                               "UPDATE np_Claims SET CreditInfoID = ?,ClaimOwnerCIID = ?,ClaimTypeID = ?," +
                                                                                                               "InformationSourceID = ?,RegDate = ?,StatusID = ?,CaseNr = ?,CurrencyID = ?,Amount = ?,ClaimDate = ?," +
                                                                                                               "UnregistedDate = ?,DelayDate = ?,PaymentDate = ?,RegisterID = ?,Valid = ?,GazetteYear = ?,GazettePage = ?,AppliedForByDebtor = ?," +
                                                                                                               "RegisterCommentNative = ?,RegisterCommentEN = ?, LastUpdate = ?, ChequeIssuedBank = ?, CaseDecisionsType = ?,Agent = ?, CBA = ?, RegisterInternalCommentNative = ?, DoNotDelete = ? WHERE ID = ?",
                                                                                                               myOleDbConn);
                        myOleDbCommand.Transaction = myTrans;
                        var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = myClaim.CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("ClaimOwnerCIID", OleDbType.Integer)
                                  {
                                      Value = myClaim.ClaimOwnerCIID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("ClaimTypeID", OleDbType.Integer)
                                  {
                                      Value = myClaim.ClaimTypeID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        //If InformationSource is 0 - then get the lowest index from the database and set as default
                        if (myClaim.InformationSourceID <= 0) {
                            myClaim.InformationSourceID = GetLowestIndexFromInformationSource();
                        }

                        myParam = new OleDbParameter("InformationSourceID", OleDbType.Integer)
                                  {
                                      Value = myClaim.InformationSourceID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("RegDate", OleDbType.Date)
                                  {
                                      Value = myClaim.RegDate,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("StatusID", OleDbType.Integer)
                                  {
                                      Value = myClaim.StatusID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("CaseNr", OleDbType.VarChar)
                                  {
                                      Value = myClaim.CaseNumber,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("CurrencyID", OleDbType.VarChar)
                                  {
                                      Value = myClaim.CurrencySymbol,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("Amount", OleDbType.Decimal)
                                  {
                                      Value = myClaim.DAmount,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("ClaimDate", OleDbType.Date)
                                  {
                                      Value = myClaim.ClaimDate,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        if (myClaim.AnnouncementDate > DateTime.MinValue) {
                            myParam = new OleDbParameter("AnnouncementDate", OleDbType.Date)
                                      {
                                          Value = myClaim.AnnouncementDate,
                                          Direction = ParameterDirection.Input
                                      };
                            myOleDbCommand.Parameters.Add(myParam);
                        }

                        myParam = new OleDbParameter("UnregistedDate", OleDbType.Date);
                        if (myClaim.UnregistedDate > DateTime.MinValue) {
                            myParam.Value = myClaim.UnregistedDate;
                        } else {
                            myParam.Value = null;
                        }
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("DelayDate", OleDbType.Date);
                        if (myClaim.DelayDate > DateTime.MinValue) {
                            myParam.Value = myClaim.DelayDate;
                        } else {
                            myParam.Value = null;
                        }
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("PaymentDate", OleDbType.Date);
                        if (myClaim.PaymentDate > DateTime.MinValue) {
                            myParam.Value = myClaim.PaymentDate;
                        } else {
                            myParam.Value = null;
                        }
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("RegisterID", OleDbType.Integer)
                                  {
                                      Value = myClaim.RegisterID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("Valid", OleDbType.Boolean)
                                  {
                                      Value = myClaim.Valid,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("GazetteYear", OleDbType.Integer)
                                  {
                                      Value = myClaim.GazatteYear,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("GazettePage", OleDbType.Integer)
                                  {
                                      Value = myClaim.GazettePage,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("AppliedByForByDebtor", OleDbType.VarChar);
                        if (myClaim.AppliedForByDebtor != "") {
                            myParam.Value = myClaim.AppliedForByDebtor ?? "";
                        } else {
                            myParam.Value = "";
                        }
                        myParam.Direction = ParameterDirection.Input;
                        myOleDbCommand.Parameters.Add(myParam);

                        // Insert Native and EN strings
                        CreateParameterFromString(
                            myOleDbCommand,
                            "RegisterCommentNative",
                            "RegisterCommentEN",
                            ParameterDirection.Input,
                            myClaim.RegisterCommentNative,
                            myClaim.RegisterCommentEN,
                            OleDbType.WChar);

                        myParam = new OleDbParameter("LastUpdate", OleDbType.Date)
                                  {
                                      Value = myClaim.LastUpdate,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        //If CheckIssueBank is 0 - then get the lowest index from the database and set as default
                        if (myClaim.ChequeIssuedBank <= 0) {
                            myClaim.ChequeIssuedBank = GetLowestIndexFromCheckIssueBanks();
                        }

                        myParam = new OleDbParameter("ChequeIssuedBank", OleDbType.Integer)
                                  {
                                      Value = myClaim.ChequeIssuedBank,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("CaseDecisionType", OleDbType.VarChar)
                                  {
                                      Value = myClaim.CaseDecisionType,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("Agent", OleDbType.WChar)
                                  {
                                      Value = myClaim.Agent,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("CBA", OleDbType.WChar)
                                  {
                                      Value = myClaim.CBA,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        myParam = new OleDbParameter("RegisterInternalCommentNative", OleDbType.WChar)
                                  {
                                      Value = myClaim.RegisterInternalCommentNative,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DoNotDelete", OleDbType.Boolean)
                        {
                            Value = myClaim.DoNotDelete,
                            Direction = ParameterDirection.Input
                        };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ID", OleDbType.Integer)
                                  {
                                      Value = myClaim.ClaimID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                        if (myClaim.BankruptiesWSameCourtCaseID != null) {
                            if (myClaim.BankruptiesWSameCourtCaseID.Count > 0) {
                                foreach (Claim claim in myClaim.BankruptiesWSameCourtCaseID) {
                                    UpdateClaimStatus(myOleDbConn, myTrans, claim);
                                }
                            }
                        }

                        if (myClaim.CasesAgainstCompaniesWSameCourtCaseID != null) {
                            if (myClaim.CasesAgainstCompaniesWSameCourtCaseID.Count > 0) {
                                foreach (Claim claim in myClaim.CasesAgainstCompaniesWSameCourtCaseID) {
                                    UpdateClaimStatus(myOleDbConn, myTrans, claim);
                                }
                            }
                        }

                        myTrans.Commit();
                    } catch (Exception e) {
                        Logger.WriteToLog(className + " : " + funcName + " : " + e.Message, true);
                        myTrans.Rollback();
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName + e.Message, true);
                return false;
            }
            return true;
        }

        public void UpdateClaimStatus(OleDbConnection myOleDbConn, OleDbTransaction myTrans, Claim claim) {
            var myOleDbCommand =
                new OleDbCommand(
                    "UPDATE np_Claims SET StatusID = ?, DelayDate = ?, PaymentDate = ? WHERE ID = ?", myOleDbConn)
                {Transaction = myTrans};
            var myParam = new OleDbParameter("StatusID", OleDbType.Integer)
                          {
                              Value = claim.StatusID,
                              Direction = ParameterDirection.Input
                          };
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("DelayDate", OleDbType.Date);
            if ((claim.DelayDate == DateTime.MinValue) || (claim.DelayDate.Year == 1899)) {
                myParam.Value = null;
            } else {
                myParam.Value = claim.DelayDate;
            }
            myParam.Direction = ParameterDirection.Input;
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("PaymentDate", OleDbType.Date);
            if ((claim.PaymentDate == DateTime.MinValue) || (claim.PaymentDate.Year == 1899)) {
                myParam.Value = null;
            } else {
                myParam.Value = claim.PaymentDate;
            }
            myParam.Direction = ParameterDirection.Input;
            myOleDbCommand.Parameters.Add(myParam);
            myParam = new OleDbParameter("ID", OleDbType.Integer)
                      {
                          Value = claim.ClaimID,
                          Direction = ParameterDirection.Input
                      };
            myOleDbCommand.Parameters.Add(myParam);

            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        public int GetLowestIndexFromInformationSource() {
            const string funcName = "GetLowestIndexFromInformationSource()";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myOleDbCommand =
                        new OleDbCommand("SELECT min(InformationSourceID) FROM np_InformationSource", myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            return reader.GetInt32(0);
                        }
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName, e, true);
                return 0;
            }
            return 0;
        }

        public int GetLowestIndexFromCheckIssueBanks() {
            const string funcName = "GetLowestIndexFromCheckIssueBanks()";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myOleDbCommand = new OleDbCommand(
                        "select min(bankID) from np_chequeissuebanks", myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            return reader.GetInt32(0);
                        }
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName, e, true);
                return 0;
            }
            return 0;
        }

        public Status GetStatus(int statusID) {
            const string funcName = "GetStatus(int statusID)";
            var myStatus = new Status();
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT StateEN,StateNative,DescriptionEN,DescriptionNative FROM np_Status WHERE StatusID =" +
                            statusID + "",
                            myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            myStatus.StateEN = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            myStatus.StateNative = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            myStatus.DescriptionEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            myStatus.DescriptionNative = reader.GetString(3);
                        }
                    }
                    return myStatus;
                }
            } catch (Exception e) {
                myStatus.StatusID = -1;
                Logger.WriteToLog(className + ": " + funcName, e, true);
            }
            return myStatus;
        }

        public DataSet GetStatusListAsDataSet() {
            const string funcName = "GetStatusListAsDataSet()";
            var mySet = new DataSet();
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM np_Status ORDER BY StatusID", myOleDbConn);
                    myAdapter.Fill(mySet);
                    return mySet;
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName + e.Message, true);
            }
            return mySet;
        }

        protected static DataSet ClearDuplicates(DataSet ds, int index) {
            if (ds.Tables.Count > 0) {
                var arrCiids = new ArrayList();
                for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                    string creditInfoId = ds.Tables[0].Rows[i][index].ToString();
                    if (creditInfoId == "") {
                        continue;
                    }
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (((string) arrCiids[j]).Equals(creditInfoId)) {
                            ds.Tables[0].Rows.Remove(ds.Tables[0].Rows[i]);
                            break;
                        }
                    }
                    arrCiids.Add(creditInfoId);
                }
            }
            return ds;
        }

        /// <summary>FindClaim takes an instance of Claim and search by given values. </summary>
        /// <param name="searchClaim">Instance of Claim.</param>
        /// <returns>DataSet.</returns>
        public DataSet FindClaim(Claim searchClaim) {
            var mySet = new DataSet();
            bool whereStatement = false;
            const string funcName = "FindClaim(Claim searchClaim)";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    string maxHits = CigConfig.Configure("lookupsettings.UserAdminMaxHits").Trim();
                    string query = "SELECT DISTINCT TOP " + maxHits +
                                   " cl.ID, cl.CreditInfoID, cl.RegDate, au_users.UserName AS Initials, " +
                                   "cl.ClaimOwnerCIID, cl.RegisterCommentNative,cl.RegisterCommentEN,np_InformationSource.NameNative, " +
                                   "np_InformationSource.NameEN, np_Status.StateNative, np_Status.StateEN, cl.ClaimDate, cl.ChequeIssuedBank, " +
                                   "np_CaseTypes.CaseID,cl.StatusID,cl.CBA, np_CaseTypes.TypeNative, np_CaseTypes.TypeEN,  " +
                                   "ciua.Type As DebtorUserType, ciub.Type As ClaimantUserType, " +
                                   "inda.FirstNameNative As DebtorFirstNameNative, case when len(rtrim(inda.SurNameNative)) = 0 then ''else '(' + rtrim(inda.SurNameNative) + ')'end  As DebtorSurNameNative, " +
                                   "inda.FirstNameEN As DebtorFirstNameEN, inda.SurnameEN As DebtorSurNameEN, " +
                                   "coma.NameNative AS DebtorCompanyNameNative, coma.NameEN AS DebtorCompanyNameEN, " +
                                   "indb.FirstNameNative As ClaimOwnerFirstNameNative, indb.SurnameNative As ClaimOwnerSurNameNative, " +
                                   "indb.FirstNameEN As ClaimOwnerFirstNameEN, indb.SurnameEN As ClaimOwnerSurNameEN, " +
                                   "comb.NameNative AS ClaimOwnerCompanyNameNative, comb.NameEN AS ClaimOwnerCompanyNameEN " +
                                   "FROM np_Claims cl LEFT OUTER JOIN au_users ON au_users.CreditInfoID = cl.RegisterID " +
                                   "LEFT OUTER JOIN np_InformationSource ON cl.InformationSourceID = np_InformationSource.InformationSourceID " +
                                   "LEFT OUTER JOIN np_Status ON cl.StatusID = np_Status.StatusID " +
                                   "LEFT OUTER JOIN np_CaseTypes ON cl.ClaimTypeID = np_CaseTypes.CaseTypeID " +
                                   "LEFT JOIN np_Creditinfouser ciua on cl.Creditinfoid = ciua.creditinfoid " +
                                   "LEFT JOIN np_individual inda on cl.Creditinfoid =inda.Creditinfoid " +
                                   "LEFT JOIN np_companys coma on cl.Creditinfoid =coma.Creditinfoid " +
                                   "LEFT JOIN np_Creditinfouser ciub on cl.ClaimOwnerCIID = ciub.creditinfoid " +
                                   "LEFT JOIN np_individual indb on cl.ClaimOwnerCIID =indb.Creditinfoid " +
                                   "LEFT JOIN np_companys comb on cl.ClaimOwnerCIID =comb.Creditinfoid ";

                    if (searchClaim.ClaimID != 0) {
                        if (whereStatement) {
                            query += "AND ";
                        } else {
                            query += "WHERE ";
                        }
                        query += "cl.ID = " + searchClaim.ClaimID + " ";
                        whereStatement = true;
                    }
                    if (searchClaim.CreditInfoID != 0) {
                        if (whereStatement) {
                            query += "AND ";
                        } else {
                            query += "WHERE ";
                        }
                        query += "cl.CreditInfoID = " + searchClaim.CreditInfoID + " ";
                        whereStatement = true;
                    }
                    if (searchClaim.ClaimOwnerCIID != 0) {
                        if (whereStatement) {
                            query += "AND ";
                        } else {
                            query += "WHERE ";
                        }
                        query += "cl.ClaimOwnerCIID = " + searchClaim.ClaimOwnerCIID + " ";
                        whereStatement = true;
                    }
                    if (searchClaim.SearchFromDate != DateTime.MinValue) // default setting is MinValue 
                    {
                        if (whereStatement) {
                            query += "AND ";
                        } else {
                            query += "WHERE ";
                        }
                        query += "cl.RegDate >= '" + searchClaim.SearchFromDate.ToString("yyyy-MM-dd") +
                                 " 00:00:00:000' ";
                        whereStatement = true;
                    }

                    if (searchClaim.SearchToDate != DateTime.MinValue) // default setting is MinValue 
                    {
                        if (whereStatement) {
                            query += "AND ";
                        } else {
                            query += "WHERE ";
                        }
                        query += "cl.RegDate <= '" + searchClaim.SearchToDate.ToString("yyyy-MM-dd") +
                                 " 23:59:59:999'  ";
                        whereStatement = true;
                    }

                    if (searchClaim.ClaimTypeID != 0) {
                        if (whereStatement) {
                            query += "AND ";
                        } else {
                            query += "WHERE ";
                        }
                        query += "cl.ClaimTypeID = " + searchClaim.ClaimTypeID + " ";
                        whereStatement = true;
                    }
                    if (searchClaim.StatusID != 0) {
                        if (whereStatement) {
                            query += "AND ";
                        } else {
                            query += "WHERE ";
                        }
                        query += "cl.StatusID = " + searchClaim.StatusID + " ";
                        whereStatement = true;
                    }
                    if (!string.IsNullOrEmpty(searchClaim.CaseNumber)) {
                        if (whereStatement) {
                            query += "AND ";
                        } else {
                            query += "WHERE ";
                        }
                        query += "cl.CaseNr = '" + searchClaim.CaseNumber + "' ";
                        whereStatement = true;
                    }
                    if (searchClaim.InformationSourceID != 0) {
                        if (whereStatement) {
                            query += "AND ";
                        } else {
                            query += "WHERE ";
                        }
                        query += "cl.InformationSourceID = " + searchClaim.InformationSourceID + " ";
                        whereStatement = true;
                    }

                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + ": " + funcName + err.Message, true);
            }
            return ClearDuplicates(mySet, 0);
        }

        /// <summary>GetClaim return one instance of claim based on given ClaimID. </summary>
        /// <returns>Claim.</returns>
        public Claim GetClaim(int claimID) {
            var myClaim = new Claim();
            const string funcName = "GetClaim(int claimID)";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    //	var myOleDbCommand = new OleDbCommand("SELECT  * FROM np_Claims WHERE np_Claims.ID = "+ claimID +" ",myOleDbConn);
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT DISTINCT np_Claims.ID,np_Claims.CreditInfoID, np_Claims.ClaimOwnerCIID, np_Claims.ClaimTypeID, " +
                            "np_Claims.InformationSourceID,np_Claims.RegDate,np_Claims.StatusID,np_Claims.CaseNr,np_Claims.CurrencyID,np_Claims.Amount,np_Claims.ClaimDate,np_Claims.AnnouncementDate,np_Claims.UnregistedDate, " +
                            "np_Claims.DelayDate,np_Claims.PaymentDate,np_Claims.RegisterID,np_Claims.Valid,np_Claims.GazetteYear,np_Claims.GazettePage,np_Claims.AppliedForByDebtor,np_Claims.RegisterCommentNative,np_Claims.RegisterCommentEN, " +
                            "au_users.UserName,np_Status.StateNative, np_Status.StateEN, np_Claims.ChequeIssuedBank, np_Claims.CaseDecisionsType, np_Claims.Agent, np_Claims.CBA, np_Claims.RegisterInternalCommentNative, np_Claims.DoNotDelete FROM np_Claims LEFT OUTER JOIN np_Status ON np_Claims.StatusID = np_Status.StatusID " +
                            "LEFT OUTER JOIN au_users ON np_Claims.RegisterID = au_users.CreditInfoID " +
                            "WHERE np_Claims.ID =  " + claimID + "",
                            myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();

                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            myClaim.ClaimID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            myClaim.CreditInfoID = reader.GetInt32(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            myClaim.ClaimOwnerCIID = reader.GetInt32(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            myClaim.ClaimTypeID = reader.GetInt32(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            myClaim.InformationSourceID = reader.GetInt32(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            myClaim.RegDate = reader.GetDateTime(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            myClaim.StatusID = reader.GetInt32(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            myClaim.CaseNumber = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            myClaim.CurrencySymbol = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            myClaim.DAmount = reader.GetDecimal(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            myClaim.ClaimDate = reader.GetDateTime(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            myClaim.AnnouncementDate = reader.GetDateTime(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            myClaim.UnregistedDate = reader.GetDateTime(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            myClaim.DelayDate = reader.GetDateTime(13);
                        }
                        if (!reader.IsDBNull(14)) {
                            myClaim.PaymentDate = reader.GetDateTime(14);
                        }
                        if (!reader.IsDBNull(15)) {
                            myClaim.RegisterID = reader.GetInt32(15);
                        }
                        if (!reader.IsDBNull(16)) {
                            myClaim.Valid = reader.GetString(16);
                        }
                        if (!reader.IsDBNull(17)) {
                            myClaim.GazatteYear = reader.GetString(17);
                        }
                        if (!reader.IsDBNull(18)) {
                            myClaim.GazettePage = reader.GetString(18);
                        }
                        if (!reader.IsDBNull(19)) {
                            myClaim.AppliedForByDebtor = reader.GetString(19);
                        }
                        if (!reader.IsDBNull(20)) {
                            myClaim.RegisterCommentNative = reader.GetString(20);
                        }
                        if (!reader.IsDBNull(21)) {
                            myClaim.RegisterCommentEN = reader.GetString(21);
                        }
                        if (!reader.IsDBNull(22)) {
                            myClaim.RegisterInitials = reader.GetString(22);
                        }
                        if (!reader.IsDBNull(23)) {
                            myClaim.StateNative = reader.GetString(23);
                        }
                        if (!reader.IsDBNull(24)) {
                            myClaim.StateEN = reader.GetString(24);
                        }
                        if (!reader.IsDBNull(25)) {
                            myClaim.ChequeIssuedBank = reader.GetInt32(25);
                        }
                        if (!reader.IsDBNull(26)) {
                            myClaim.CaseDecisionType = reader.GetString(26);
                        }
                        if (!reader.IsDBNull(27)) {
                            myClaim.Agent = reader.GetString(27);
                        }
                        if (!reader.IsDBNull(28)) {
                            myClaim.CBA = reader.GetString(28);
                        }
                        if (!reader.IsDBNull(29)) {
                            myClaim.RegisterInternalCommentNative = reader.GetString(29);
                        }
                        if (!reader.IsDBNull(30))
                        {
                            myClaim.DoNotDelete = reader.GetBoolean(30);
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + ": " + funcName + err.Message, true);
            }

            return myClaim;
        }

        public DataSet GetClaimOverViewListAsDataSet() {
            var mySet = new DataSet();
            const string funcName = "GetClaimOverViewList";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    /*string query = "SELECT DISTINCT cl.ID,cl.CreditInfoID, cl.ClaimOwnerNameNative, cl.ClaimOwnerNameEN,  cs.TypeNative,cs.TypeEN, isource.NameNative,isource.NameEN,cl.RegDate," +
						"cl.StatusID,cl.CaseNr,cl.CurrencyID,cl.Amount,cl.ClaimDate,cl.AnnouncementDate,cl.UnregistedDate,cl.DelayDate,cl.PaymentDate,cl.RegisterID,cl.Valid,cl.GazetteYear,"+
						"cl.GazettePage,cl.AppliedForByDebtor,cl.RegisterCommentNative,cl.RegisterCommentEN,emp.initials,st.StateNative, st.StateEN, cl.ChequeIssuedBank, cs.CaseID, cl.CBA FROM np_Claims cl, np_Status st, np_Employee emp,np_InformationSource isource, np_CaseTypes cs " +
						"WHERE cl.RegDate >=  ? AND cl.RegDate <= ? AND cl.RegisterID = emp.CreditInfoID AND cl.ClaimTypeID = cs.CaseTypeID AND cl.StatusID = st.StatusID AND cl.InformationSourceID = isource.InformationSourceID AND cl.ClaimTypeID = cs.CaseTypeID " +
						"ORDER BY cl.ID DESC";	*/
                    // case when len(rtrim(inda.SurNameNative)) = 0 then ''else '(' + rtrim(inda.SurNameNative) + ')'end  As DebtorSurNameNative,
                
                    const string query = "SELECT DISTINCT cl.ID,cl.CreditInfoID, cl.ClaimOwnerCIID, " +
                                         "cs.TypeNative,cs.TypeEN, isource.NameNative,isource.NameEN,cl.RegDate,cl.StatusID,cl.CaseNr, " +
                                         "cl.CurrencyID,cl.Amount,cl.ClaimDate,cl.AnnouncementDate,cl.UnregistedDate,cl.DelayDate, " +
                                         "cl.PaymentDate,cl.RegisterID,cl.Valid,cl.GazetteYear,cl.GazettePage,cl.AppliedForByDebtor, " +
                                         "cl.RegisterCommentNative,cl.RegisterCommentEN,emp.initials,st.StateNative, st.StateEN, " +
                                         "cl.ChequeIssuedBank, cs.CaseID, cl.CBA, ciua.Type As DebtorUserType, ciub.Type As ClaimantUserType, " +
                                         "inda.FirstNameNative As DebtorFirstNameNative, case when len(rtrim(inda.SurNameNative)) = 0 then ''else '(' + rtrim(inda.SurNameNative) + ')'end As DebtorSurNameNative, " +
                                         "inda.FirstNameEN As DebtorFirstNameEN, inda.SurnameEN As DebtorSurNameEN, " +
                                         "coma.NameNative AS DebtorCompanyNameNative, coma.NameEN AS DebtorCompanyNameEN, " +
                                         "indb.FirstNameNative As ClaimOwnerFirstNameNative, case when len(rtrim(indb.SurNameNative)) = 0 then ''else '(' + rtrim(indb.SurNameNative) + ')'end As ClaimOwnerSurNameNative, " +
                                         "indb.FirstNameEN As ClaimOwnerFirstNameEN, indb.SurnameEN As ClaimOwnerSurNameEN, " +
                                         "comb.NameNative AS ClaimOwnerCompanyNameNative, comb.NameEN AS ClaimOwnerCompanyNameEN " +
                                         "FROM np_Claims cl left join np_Status st on cl.StatusID = st.StatusID " +
                                         "left join np_Employee emp on cl.RegisterID = emp.CreditInfoID left join np_InformationSource isource " +
                                         "on cl.InformationSourceID = iSource.InformationSourceID left join np_CaseTypes cs on cl.ClaimTypeID = cs.CaseTypeID " +
                                         "LEFT JOIN np_Creditinfouser ciua on cl.Creditinfoid = ciua.creditinfoid " +
                                         "LEFT JOIN np_individual inda on cl.Creditinfoid =inda.Creditinfoid " +
                                         "LEFT JOIN np_companys coma on cl.Creditinfoid =coma.Creditinfoid " +
                                         "LEFT JOIN np_Creditinfouser ciub on cl.ClaimOwnerCIID = ciub.creditinfoid " +
                                         "LEFT JOIN np_individual indb on cl.ClaimOwnerCIID =indb.Creditinfoid " +
                                         "LEFT JOIN np_companys comb on cl.ClaimOwnerCIID =comb.Creditinfoid " +
                                         "WHERE cl.RegDate >=  ? AND cl.RegDate <= ? ORDER BY cl.RegDate DESC";
//						"WHERE cl.RegDate >=  '2007-01-01' AND cl.RegDate <= '2008-01-01'  ORDER BY cl.RegDate DESC";

                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                    var myParam = new OleDbParameter("RegDate", OleDbType.Date)
                                  {
                                      Value = (DateTime.Now - new TimeSpan(1, 0, 0, 0)),
                                      Direction = ParameterDirection.Input
                                  };

                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("RegDate", OleDbType.Date)
                              {
                                  Value = DateTime.Now,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myAdapter.SelectCommand = myOleDbCommand;
                    myAdapter.Fill(mySet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + ": " + funcName + err.Message, true);
            }
            return ClearDuplicates(mySet, 0);
        }

        public DataSet GetLetterClaimOverViewListAsDataSet() {
            var mySet = new DataSet();
            const string funcName = "GetLetterClaimOverViewListAsDataSet";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    /*string query = "SELECT DISTINCT cl.ID,cl.CreditInfoID, cl.ClaimOwnerNameNative, cl.ClaimOwnerNameEN,  cs.TypeNative,cs.TypeEN, isource.NameNative,isource.NameEN,cl.RegDate,cl.ClaimDate, " +
						"cl.StatusID,cl.CaseNr,cl.CurrencyID,cl.Amount,cl.ClaimDate,cl.AnnouncementDate,cl.UnregistedDate,cl.DelayDate,cl.PaymentDate,cl.RegisterID,cl.Valid,cl.GazetteYear,"+
						"cl.GazettePage,cl.AppliedForByDebtor,cl.RegisterCommentNative,cl.RegisterCommentEN,emp.initials,st.StateNative, st.StateEN, cl.ChequeIssuedBank, cs.CaseID FROM np_Claims cl, np_Status st, np_Employee emp,np_InformationSource isource, np_CaseTypes cs " +
						"WHERE cl.RegisterID = emp.CreditInfoID AND cl.ClaimTypeID = cs.CaseTypeID AND cl.StatusID = st.StatusID AND cl.InformationSourceID = isource.InformationSourceID AND cl.ClaimTypeID = cs.CaseTypeID AND cl.StatusID = 2";	
*/ 
                    // case when len(rtrim(inda.SurNameNative)) = 0 then ''else '(' + rtrim(inda.SurNameNative) + ')'end  As DebtorSurNameNative,
                    const string query = "SELECT DISTINCT cl.ID,cl.CreditInfoID, cl.ClaimOwnerCIID,  cs.TypeNative,cs.TypeEN, isource.NameNative,isource.NameEN, " +
                                         "cl.RegDate,cl.ClaimDate, cl.StatusID,cl.CaseNr,cl.CurrencyID,cl.Amount,cl.ClaimDate,cl.AnnouncementDate,cl.UnregistedDate,cl.DelayDate, " +
                                         "cl.PaymentDate,cl.RegisterID,cl.Valid,cl.GazetteYear,cl.GazettePage,cl.AppliedForByDebtor,cl.RegisterCommentNative,cl.RegisterCommentEN, " +
                                         "emp.initials,st.StateNative, st.StateEN, cl.ChequeIssuedBank, cs.CaseID, cl.StatusID, " +
                                         "ciub.Type As ClaimantUserType, " +
                                         "indb.FirstNameNative As ClaimOwnerFirstNameNative, case when len(rtrim(indb.SurNameNative)) = 0 then ''else '(' + rtrim(indb.SurNameNative) + ')' end  As ClaimOwnerSurNameNative, " +
                                         "indb.FirstNameEN As ClaimOwnerFirstNameEN, indb.SurnameEN As ClaimOwnerSurNameEN, " +
                                         "comb.NameNative AS ClaimOwnerCompanyNameNative, comb.NameEN AS ClaimOwnerCompanyNameEN " +
                                         "FROM np_Claims cl left join np_Status st on cl.StatusID = st.StatusID " +
                                         "left join np_Employee emp on cl.RegisterID = emp.CreditInfoID left join np_InformationSource isource on cl.InformationSourceID = isource.InformationSourceID " +
                                         "left join np_CaseTypes cs on cl.ClaimTypeID = cs.CaseTypeID " +
                                         "LEFT JOIN np_Creditinfouser ciub on cl.ClaimOwnerCIID = ciub.creditinfoid " +
                                         "LEFT JOIN np_individual indb on cl.ClaimOwnerCIID =indb.Creditinfoid " +
                                         "LEFT JOIN np_companys comb on cl.ClaimOwnerCIID =comb.Creditinfoid " +
                                         "WHERE cl.StatusID = 2 ORDER BY cl.CreditInfoID";

                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var myParam = new OleDbParameter("RegDate", OleDbType.Date)
                                  {
                                      Value = (DateTime.Now - new TimeSpan(1, 0, 0, 0)),
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("RegDate", OleDbType.Date)
                              {
                                  Value = DateTime.Now,
                                  Direction = ParameterDirection.Input
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    try {
                        myAdapter.SelectCommand = myOleDbCommand;
                        myAdapter.Fill(mySet);
                    } catch (Exception e) {
                        Logger.WriteToLog(funcName + e.Message, true);
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + ": " + funcName + err.Message, true);
            }
            return ClearDuplicates(mySet, 0);
        }

        // this is specially addet for Cyprus
        public DataSet GetIssuedBanksAsDataSet(bool en) {
            var mySet = new DataSet();
            const string funcName = "GetIssuedBanksAsDataSet";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    string query = "";
                    query = en ? "SELECT * FROM np_ChequeIssueBanks ORDER BY NameEN" : "SELECT * FROM np_ChequeIssueBanks ORDER BY NameNative";

                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + ": " + funcName + err.Message, true);
            }
            return mySet;
        }

        // this is specially addet for Cyprus
        public DataSet GetCaseDecisionsTypesAsDataSet() {
            var mySet = new DataSet();
            const string funcName = "GetCaseDecisionsTypesAsDataSet";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    const string query = "SELECT * FROM np_CaseDecisionsTypes";
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + ": " + funcName + err.Message, true);
            }
            return mySet;
        }

        public ArrayList GetAllClaimsByCIID(int creditInfoID) {
            Claim myClaim;
            const string funcName = "GetAllClaimsByCIID(string creditInfoID)";
            var claimArr = new ArrayList();
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    //	var myOleDbCommand = new OleDbCommand("SELECT  * FROM np_Claims WHERE np_Claims.ID = "+ claimID +" ",myOleDbConn);
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT DISTINCT np_Claims.ID,np_Claims.CreditInfoID, np_Claims.ClaimTypeID, " +
                            "np_Claims.RegDate,np_Claims.StatusID,np_Claims.ClaimDate,np_Claims.AnnouncementDate,np_Claims.UnregistedDate, " +
                            "np_Claims.DelayDate,np_Claims.PaymentDate,np_Claims.OnHold  FROM np_Claims " +
                            "WHERE np_Claims.CreditInfoID =  " + creditInfoID + "",
                            myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        myClaim = new Claim();
                        if (!reader.IsDBNull(0)) {
                            myClaim.ClaimID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            myClaim.CreditInfoID = reader.GetInt32(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            myClaim.ClaimTypeID = reader.GetInt32(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            myClaim.RegDate = reader.GetDateTime(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            myClaim.StatusID = reader.GetInt32(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            myClaim.ClaimDate = reader.GetDateTime(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            myClaim.AnnouncementDate = reader.GetDateTime(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            myClaim.UnregistedDate = reader.GetDateTime(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            myClaim.DelayDate = reader.GetDateTime(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            myClaim.PaymentDate = reader.GetDateTime(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            myClaim.OnHold = reader.GetDateTime(10);
                        }

                        claimArr.Add(myClaim);
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + ": " + funcName + err.Message, true);
            }
            return claimArr;
        }

        public int UpdateClaimLetterStatus(ArrayList myArr) {
            const string funcName = "UpdateClaimLetterStatus(ArrayList myArr)";
            OleDbCommand myOleDbCommand;
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    myOleDbConn.Open();
                    // begin transaction...
                    var myTrans = myOleDbConn.BeginTransaction();
                    for (int i = 0; i < myArr.Count; i++) {
                        int claimID = int.Parse(myArr[i].ToString());
                        try {
                            // update Claim status np_Claims
                            myOleDbCommand =
                                new OleDbCommand(
                                    "UPDATE np_Claims SET StatusID = 3, AnnouncementDate = ? WHERE ID = ?", myOleDbConn)
                                {Transaction = myTrans};
                            var myParam = new OleDbParameter("AnnouncementDate", OleDbType.Date)
                                          {
                                              Value = DateTime.Now,
                                              Direction = ParameterDirection.Input
                                          };
                            myOleDbCommand.Parameters.Add(myParam);
                            myParam = new OleDbParameter("ClaimID", OleDbType.Integer)
                                      {
                                          Value = claimID,
                                          Direction = ParameterDirection.Input
                                      };
                            myOleDbCommand.Parameters.Add(myParam);
                            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                        } catch (Exception e) {
                            Logger.WriteToLog(className + " : " + funcName + " : " + e.Message, true);
                            myTrans.Rollback();
                            return -1;
                        }
                    }
                    myTrans.Commit();
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName + e.Message, true);
                return -1;
            }
            return 1;
        }

        public DataSet GetAllClaimsByCIIDAsDataset(string CreditInfoID) {
            var mySet = new DataSet();
            const string funcName = "GetAllClaimsByCIIDAsDataset(string CreditInfoID)";
            try {
                int creditInfoID = int.Parse(CreditInfoID);
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    //	var myOleDbCommand = new OleDbCommand("SELECT  * FROM np_Claims WHERE np_Claims.ID = "+ claimID +" ",myOleDbConn);
                    string query =
                        "SELECT np_Claims.ID, np_Claims.CreditInfoID, np_Claims.RegDate, au_users.UserName AS Initials, np_Claims.ClaimOwnerCIID, " +
                        "np_Claims.RegisterCommentNative,np_Claims.RegisterCommentEN,np_InformationSource.NameNative,np_InformationSource.NameEN, " +
                        "np_Status.StateNative, np_Status.StateEN, np_Claims.ClaimDate, np_Claims.ChequeIssuedBank, np_CaseTypes.CaseID FROM " +
                        "np_Claims LEFT OUTER JOIN au_users ON au_users.CreditInfoID = np_Claims.RegisterID " +
                        "LEFT OUTER JOIN np_InformationSource ON np_Claims.InformationSourceID = np_InformationSource.InformationSourceID LEFT OUTER JOIN np_Status ON np_Claims.StatusID = np_Status.StatusID LEFT OUTER JOIN np_CaseTypes ON np_Claims.ClaimTypeID = np_CaseTypes.CaseTypeID " +
                        "WHERE np_Claims.CreditInfoID =  " + creditInfoID + "";

                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(funcName + " : " + err.Message, true);
            }
            return ClearDuplicates(mySet, 0);
        }

        public DataSet GetAllClaimsWithClaimOwnerNameNativeByCIIDAsDataSet(int creditInfoID) {
            var mySet = new DataSet();
            const string funcName = "GetAllClaimsWithClaimOwnerNameNativeByCIIDAsDataSet(int creditInfoID)";
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    string query =
                        "SELECT np_Claims.ID, np_Claims.CreditInfoID, np_Claims.RegDate,np_Claims.Amount,np_Claims.CaseNr, au_users.UserName AS Initials, np_Claims.ClaimOwnerCIID, " +
                        " ClaimOwnerName = " +
                        "(SELECT CASE WHEN dbo.np_Individual.FirstNameNative IS NOT NULL THEN " +
                        "dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative " +
                        "ELSE dbo.np_Companys.NameNative END AS [Name] FROM dbo.np_CreditInfoUser " +
                        "LEFT OUTER JOIN dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID " +
                        "LEFT OUTER JOIN dbo.np_Individual ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID " +
                        "WHERE  dbo.np_CreditInfoUser.CreditInfoID = np_Claims.ClaimOwnerCIID)," +
                        "np_Claims.RegisterCommentNative,np_Claims.RegisterCommentEN,np_InformationSource.NameNative,np_InformationSource.NameEN, np_Status.StateNative," +
                        "np_Status.StateEN, np_Claims.ClaimDate, np_Claims.ChequeIssuedBank, np_CaseTypes.CaseID,np_CaseTypes.TypeEN,np_CaseTypes.TypeNative FROM np_Claims " +
                        "LEFT OUTER JOIN au_users ON au_users.CreditInfoID = np_Claims.RegisterID " +
                        "LEFT OUTER JOIN np_InformationSource ON np_Claims.InformationSourceID = np_InformationSource.InformationSourceID " +
                        "LEFT OUTER JOIN np_Status ON np_Claims.StatusID = np_Status.StatusID LEFT OUTER JOIN np_CaseTypes ON np_Claims.ClaimTypeID = np_CaseTypes.CaseTypeID " +
                        "WHERE np_Claims.CreditInfoID = " + creditInfoID + "";

                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(funcName + " : " + err.Message, true);
            }
            return ClearDuplicates(mySet, 0);
        }

        public DataSet GetAllActiveClaimsWithClaimOwnerNameNativeByCIIDAsDataSet(int creditInfoID) {
            var mySet = new DataSet();
            const string funcName = "GetAllActiveClaimsWithClaimOwnerNameNativeByCIIDAsDataSet(int creditInfoID)";
            int registrationStatus = 4;
            // hmm we need to check if the claims have registration status
            if (CigConfig.Configure("lookupsettings.RegisteredClaimID") != null) {
                registrationStatus = int.Parse(CigConfig.Configure("lookupsettings.RegisteredClaimID"));
            }
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    string query =
                        "SELECT np_Claims.ID, np_Claims.CreditInfoID, np_Claims.RegDate,np_Claims.Amount,np_Claims.CaseNr, au_users.UserName AS Initials, np_Claims.ClaimOwnerCIID, " +
                        " ClaimOwnerName = " +
                        "(SELECT CASE WHEN dbo.np_Individual.FirstNameNative IS NOT NULL THEN " +
                        "dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative " +
                        "ELSE dbo.np_Companys.NameNative END AS [Name] FROM dbo.np_CreditInfoUser " +
                        "LEFT OUTER JOIN dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID " +
                        "LEFT OUTER JOIN dbo.np_Individual ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID " +
                        "WHERE  dbo.np_CreditInfoUser.CreditInfoID = np_Claims.ClaimOwnerCIID)," +
                        "np_Claims.RegisterCommentNative,np_Claims.RegisterCommentEN,np_InformationSource.NameNative,np_InformationSource.NameEN, np_Status.StateNative," +
                        "np_Status.StateEN, np_Claims.ClaimDate, np_Claims.ChequeIssuedBank, np_CaseTypes.CaseID,np_CaseTypes.TypeEN,np_CaseTypes.TypeNative FROM np_Claims " +
                        "LEFT OUTER JOIN au_users ON au_users.CreditInfoID = np_Claims.RegisterID " +
                        "LEFT OUTER JOIN np_InformationSource ON np_Claims.InformationSourceID = np_InformationSource.InformationSourceID " +
                        "LEFT OUTER JOIN np_Status ON np_Claims.StatusID = np_Status.StatusID LEFT OUTER JOIN np_CaseTypes ON np_Claims.ClaimTypeID = np_CaseTypes.CaseTypeID " +
                        "WHERE np_Claims.CreditInfoID = " + creditInfoID + " AND np_Claims.StatusID = " +
                        registrationStatus + "";

                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(funcName + " : " + err.Message, true);
            }
            return ClearDuplicates(mySet, 0);
        }

        public ArrayList GetCaseTypesAsArrayList(int caseType) {
            const string funcName = "GetCaseTypesAsArrayList(int caseType)";
            var caseTypeArr = new ArrayList();
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    //	var myOleDbCommand = new OleDbCommand("SELECT  * FROM np_Claims WHERE np_Claims.ID = "+ claimID +" ",myOleDbConn);
                    var myOleDbCommand = new OleDbCommand(
                        "SELECT caseTypeID " +
                        "WHERE caseID =  " + caseType + "",
                        myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            caseTypeArr.Add(reader.GetInt32(0));
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(funcName + " : " + err.Message, true);
            }
            return caseTypeArr;
        }

        public bool AlreadyBankruptieRegistered(int creditInfoID) {
            const string funcName = "AlreadyBankruptieRegistered(int creditInfoID)";
            bool hasBankruptieRegisterd = false;
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT np_Claims.ID, np_Claims.CreditInfoID  from np_Claims,np_CaseTypes " +
                            "WHERE np_Claims.ClaimTypeID = np_CaseTypes.CaseTypeID AND np_Claims.CreditInfoID = " +
                            creditInfoID + " AND np_CaseTypes.CaseID = 1",
                            myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();

                    if (reader.Read()) {
                        hasBankruptieRegisterd = true;
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(funcName + " : " + err.Message, true);
            }
            return hasBankruptieRegisterd;
        }

        public ArrayList GetAllCaseInstancesPerCourtCaseID(string courtCaseID, int informationSourceID, bool individual) {
            const string funcName = "GetAllCaseInstancesPerCourtCaseID(string courtCaseID, int nformationSourceID, bool individual)";
            var myClaimArr = new ArrayList();
            Claim myClaim;
            try {
                using (var myOleDbConn = new OleDbConnection(connection)) {
                    var myOleDbCommand = individual ? new OleDbCommand(
                       "SELECT DISTINCT np_Claims.ID,np_Claims.CreditInfoID, np_Claims.ClaimTypeID, np_Claims.CaseNr, " +
                       "np_Claims.RegDate,np_Claims.StatusID,np_Claims.ClaimDate,np_Claims.AnnouncementDate,np_Claims.UnregistedDate, " +
                       "np_Claims.DelayDate,np_Claims.PaymentDate,np_Claims.OnHold  FROM np_Claims " +
                       "WHERE np_Claims.CaseNr =  '" + courtCaseID + "' AND InformationSourceID = " +
                       informationSourceID + "",
                       myOleDbConn) : new OleDbCommand(
                                          "SELECT DISTINCT np_Claims.ID,np_Claims.CreditInfoID, np_Claims.ClaimTypeID, np_Claims.CaseNr, " +
                                          "np_Claims.RegDate,np_Claims.StatusID,np_Claims.ClaimDate,np_Claims.AnnouncementDate,np_Claims.UnregistedDate, " +
                                          "np_Claims.DelayDate,np_Claims.PaymentDate,np_Claims.OnHold  FROM np_Claims " +
                                          "WHERE np_Claims.CaseNr =  '" + courtCaseID + "' AND InformationSourceID = " +
                                          informationSourceID + "" +
                                          " AND CaseDecisionsType != ''",
                                          myOleDbConn);
                    myOleDbCommand.Connection.Open();
                    var reader = myOleDbCommand.ExecuteReader();

                    while (reader.Read()) {
                        myClaim = new Claim();
                        if (!reader.IsDBNull(0)) {
                            myClaim.ClaimID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            myClaim.CreditInfoID = reader.GetInt32(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            myClaim.ClaimTypeID = reader.GetInt32(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            myClaim.CaseNumber = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            myClaim.RegDate = reader.GetDateTime(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            myClaim.StatusID = reader.GetInt32(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            myClaim.ClaimDate = reader.GetDateTime(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            myClaim.AnnouncementDate = reader.GetDateTime(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            myClaim.UnregistedDate = reader.GetDateTime(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            myClaim.DelayDate = reader.GetDateTime(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            myClaim.PaymentDate = reader.GetDateTime(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            myClaim.OnHold = reader.GetDateTime(11);
                        }

                        myClaimArr.Add(myClaim);
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(funcName + " : " + err.Message, true);
            }
            return myClaimArr;
        }
    }
}