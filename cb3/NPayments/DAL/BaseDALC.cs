#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using Logging.BLL;
using cb3;
using cb3.Audit;

#endregion

namespace NPayments.DAL {
    /// <summary>
    /// Summary description for BaseDALC.
    /// </summary>
    [Serializable]
    public class BaseDALC {

        /// <summary>
        /// Used for class name in error handling
        /// </summary>
        private string strClassName;

        /// <summary>
        /// Used for function name in error handling.
        /// </summary>
        private string strFunctionName;

        public BaseDALC() {
        }

        public string ClassName { get { return strClassName; } set { strClassName = value; } }
        public string FunctionName { get { return strFunctionName; } set { strFunctionName = value; } }

        /// <summary>
        /// OpenConnection opens a connection to a database
        /// </summary>
        /// <remarks>
        /// This function is defined as virtual. This means that child classes can override this function
        /// if any of them would like to use another connection method or connetion string. The calling of this
        /// function should be followed with a connection close and dispose statements for the returned connection.
        /// </remarks>
        /// <returns>OleDbConnection - Open connection</returns>
        public virtual OleDbConnection OpenConnection() {
            var connection = new OleDbConnection(DatabaseHelper.ConnectionString());
            connection.Open();

            return connection;
        }

        /// <summary>
        /// LoadData returns data according to the supplied SQL statement
        /// </summary>
        /// <remarks>
        /// LoadData is used to return data from the database. The supplied SQL statement is used to select data. If
        /// some error occurs then the function returns "null".
        /// </remarks>
        /// <param name="strSQL">The SQL string (usually a SELECT statement) that is used.</param>
        /// <returns>DataTable.</returns>
        public DataTable LoadDataTable(string strSQL) {
            DataTable dt = null;
            OleDbConnection connection = null;
            OleDbCommand command = null;
            OleDbDataAdapter adapter = null;

            try {
                connection = OpenConnection();

                command = new OleDbCommand(strSQL, connection);
                adapter = new OleDbDataAdapter(command);

                dt = new DataTable();
                adapter.Fill(dt);
                adapter.FillSchema(dt, SchemaType.Source);
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                throw;
            } finally {
                if (adapter != null) {
                    adapter.Dispose();
                }
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return dt;
        }

        protected bool ExecuteModifyStatement(string sql) {
            OleDbConnection connection = null;
            OleDbCommand command = null;

            try {
                connection = OpenConnection();

                command = new OleDbCommand(sql, connection);
                new AuditFactory(command).PerformAuditProcess(); command.ExecuteNonQuery();

                connection.Close();

                return true;
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                return false;
            } finally {
                if (command != null) {
                    command.Dispose();
                }
                if (connection != null) {
                    connection.Dispose();
                }
            }
        }

        protected DataSet ExecuteSelectStatement(string query) {
            var ds = new DataSet();
            var myAdapter = new OleDbDataAdapter();
            var myOleConn = OpenConnection();
            try {
                myAdapter.SelectCommand = new OleDbCommand(query, myOleConn);
                myAdapter.Fill(ds);
                return ds;
            } catch (Exception ex) {
                Logger.WriteToLog(strClassName + " : " + strFunctionName + ex.Message, true);
                return ds;
            } finally {
                if (myOleConn != null) {
                    myOleConn.Close();
                }
            }
        }


        /// <summary>
        /// Creates a parameter for an INSERT/UPDATE statement. For EN and Native strings. If both strings have values then they're both
        /// created as parameters. If only one has values then it's text is set, by default, into both fields in the db.
        /// </summary>
        /// <remarks>Creates a parameter for an INSERT/UPDATE statement.</remarks>
        /// <param name="command">OleDbCommand - parameter parent command</param>
        /// <param name="strNameNative">Variable name - both for source column and variable</param>
        /// <param name="strNameEN">Variable name - both for source column and variable</param>
        /// <param name="oValueNative">The entered native value</param>
        /// <param name="oValueEN">The entered EN value</param>
        /// <param name="paramDirect">Parameter direction</param>
        /// <param name="type">Column datatype</param>
        /// <returns></returns>
        public void CreateParameterFromString(
            OleDbCommand command,
            string strNameNative,
            string strNameEN,
            ParameterDirection paramDirect,
            object oValueNative,
            object oValueEN,
            OleDbType type) {
            OleDbParameter paramNative = command.Parameters.Add(strNameNative, type);
            OleDbParameter paramEN = command.Parameters.Add(strNameEN, type);
            paramNative.Direction = paramDirect;
            paramEN.Direction = paramDirect;

            // If both values are null then we'll enter them both as null to the database
            if (oValueNative == null && oValueEN == null) {
                paramNative.Value = DBNull.Value;
                paramEN.Value = DBNull.Value;
                return;
            }

            // If either value (but not both) is null we'd like to convert it into an empty string
            if (oValueNative == null) {
                oValueNative = "";
            }

            if (oValueEN == null) {
                oValueEN = "";
            }

            // Check the stings and switch them if either is an empty string.
            if (oValueNative.ToString().Trim() != string.Empty) {
                paramNative.Value = oValueNative;
            } else if (oValueEN.ToString().Trim() != string.Empty) {
                paramNative.Value = oValueEN;
                // oValueNative = oValueEN;
            }

            if (oValueEN.ToString().Trim() != string.Empty) {
                paramEN.Value = oValueEN;
            } else if (oValueNative.ToString().Trim() != string.Empty) {
                paramEN.Value = oValueNative;
                // oValueEN = oValueNative;
            }
        }
    }
}