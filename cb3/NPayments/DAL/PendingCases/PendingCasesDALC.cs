#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using cb3.Audit;
using Logging.BLL;
using NPayments.BLL.PendingCases;

#endregion

namespace NPayments.DAL.PendingCases {
    /// <summary>
    /// Summary description for PendingCasesDALC.
    /// </summary>
    [Serializable]
    public class PendingCasesDALC : BaseDALC {
        public PendingCasesDALC() { ClassName = "PendingCasesDALC "; }

        public DataTable GetAdjudicatorsAsDataTable() {
            FunctionName = "DataTable GetAdjudicatorsAsDataTable()";

            const string strCommand = "SELECT * FROM mt_Adjudicators";
            return LoadDataTable(strCommand);
        }

        public DataTable GetAllLetterCasesAsDataTable() {
            FunctionName = "DataTable GetAllLetterCasesAsDataTable()";
            var statusLetter = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("PendingCaseStatusLetter"));

            var strCommand = "SELECT * FROM mt_v_PendingCases where StatusID = " + statusLetter;
            return LoadDataTable(strCommand);
        }

        public DataTable FindPendingCase(PendingCaseBLLC pCase, DateTime dFrom, DateTime dTo) {
            bool where = false;
//			string query = "select * from mt_v_PendingCases where ";
            string sql = "select * from mt_v_PendingCases";
            string query = "";

            if (pCase.DefendantID != null && pCase.DefendantID.Trim() != "") {
                query += "DefendantID = '" + pCase.DefendantID + "' ";
                where = true;
            }

            if (pCase.DefendantName != null && pCase.DefendantName.Trim() != "") {
                if (where) {
                    query += "AND ";
                }
                query += "DefendantName LIKE '" + pCase.DefendantName + "%' ";
                where = true;
            }

            if (pCase.PlaintiffID != null && pCase.PlaintiffID.Trim() != "") {
                if (where) {
                    query += "AND ";
                }
                query += "PlaintiffID = '" + pCase.PlaintiffID + "' ";
                where = true;
            }

            if (pCase.PlaintiffName != null && pCase.PlaintiffName.Trim() != "") {
                if (where) {
                    query += "AND ";
                }
                query += "PlaintiffName LIKE '" + pCase.PlaintiffName + "%' ";
                where = true;
            }

            if (pCase.WritNumber != null && pCase.WritNumber.Trim() != "") {
                if (where) {
                    query += "AND ";
                }
                query += "WritNumber = '" + pCase.WritNumber + "' ";
                where = true;
            }

            if (pCase.Court > -1) {
                if (where) {
                    query += "AND ";
                }
                query += "Court = '" + pCase.Court + "' ";
                where = true;
            }

            if (pCase.Adjudicator > -1) {
                if (where) {
                    query += "AND ";
                }
                query += "Adjudicator = '" + pCase.Adjudicator + "' ";
                where = true;
            }

            if (pCase.ID > -1) {
                if (where) {
                    query += "AND ";
                }
                query += "ID = '" + pCase.ID + "' ";
                where = true;
            }

            if (pCase.StatusID > -1) {
                if (where) {
                    query += "AND ";
                }
                query += "StatusId = '" + pCase.StatusID + "' ";
                where = true;
            }

            //en-GB dates are date/month/year, but database wants month/date/year
            if (pCase.DateOfWrit != DateTime.MinValue) {
                if (where) {
                    query += "AND ";
                }
                query += "DateOfWrit = '" + pCase.DateOfWrit.ToString("MM/dd/yyyy") + "' ";
                where = true;
            }

            // from, no to
            if (dFrom != DateTime.MinValue && dTo == DateTime.MinValue) {
                if (where) {
                    query += "AND ";
                }
                query += "DateOfWrit >= '" + dFrom.ToString("MM/dd/yyyy") + "' ";
                where = true;
            } else
                // from and to
                if (dFrom != DateTime.MinValue && dTo != DateTime.MinValue) {
                    if (where) {
                        query += "AND ";
                    }
                    query += "DateOfWrit >= '" + dFrom.ToString("MM/dd/yyyy") + "' and DateOfWrit <= '" +
                             dTo.ToString("MM/dd/yyyy") + "' ";
                    where = true;
                } else
                    // to, no from
                    if (dFrom == DateTime.MinValue && dTo != DateTime.MinValue) {
                        if (where) {
                            query += "AND ";
                        }
                        query += "DateOfWrit <= '" + dTo.ToString("MM/dd/yyyy") + "' ";
                        where = true;
                    }

            if (where) {
                sql += " where " + query;
            }

            return LoadDataTable(sql);
//			return LoadDataTable(query);
        }

        public PendingCaseBLLC GetPendingCase(string id) {
            var query = "select * from mt_v_PendingCases where ID = " + id;
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                var pc = new PendingCaseBLLC
                         {
                             ID = int.Parse(id),
                             DefendantID = ds.Tables[0].Rows[0]["DefendantID"].ToString(),
                             DefendantName = ds.Tables[0].Rows[0]["DefendantName"].ToString(),
                             PlaintiffID = ds.Tables[0].Rows[0]["PlaintiffID"].ToString(),
                             PlaintiffName = ds.Tables[0].Rows[0]["PlaintiffName"].ToString(),
                             Court = int.Parse(ds.Tables[0].Rows[0]["Court"].ToString()),
                             CourtNative = ds.Tables[0].Rows[0]["CourtNative"].ToString(),
                             CourtEN = ds.Tables[0].Rows[0]["CourtEN"].ToString(),
                             Adjudicator = int.Parse(ds.Tables[0].Rows[0]["Adjudicator"].ToString()),
                             AdjudicatorNative = ds.Tables[0].Rows[0]["AdjudicatorNative"].ToString(),
                             AdjudicatorEN = ds.Tables[0].Rows[0]["AdjudicatorEN"].ToString(),
                             DateOfWrit = DateTime.Parse(ds.Tables[0].Rows[0]["DateOfWrit"].ToString()),
                             WritNumber = ds.Tables[0].Rows[0]["WritNumber"].ToString(),
                             ValueOfSuit = float.Parse(ds.Tables[0].Rows[0]["ValueOfSuit"].ToString()),
                             Comments = ds.Tables[0].Rows[0]["Comments"].ToString(),
                             StatusID = int.Parse(ds.Tables[0].Rows[0]["StatusID"].ToString()),
                             StateTextNative = ds.Tables[0].Rows[0]["StateNative"].ToString(),
                             StateTextEN = ds.Tables[0].Rows[0]["StateEN"].ToString(),
                             CurrencyCode = ds.Tables[0].Rows[0]["CurrencyCode"].ToString()
                         };
                return pc;
            }
            return null;
        }

        public DataTable GetAllPendingCaseStatusAsDataTable() {
            FunctionName = "DataTable GetAllLetterCasesAsDataTable()";

            const string strCommand = "SELECT * FROM mt_PendingCaseStatus ORDER BY StatusID";
            return LoadDataTable(strCommand);
        }

        public bool UpdatePendingCaseStatus(string id, int status) {
            FunctionName = "UpdatePendingCaseStatus(string id, int status)";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        string query = "UPDATE mt_PendingCases SET StatusID = ? WHERE ID = " + id;
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("StatusID", OleDbType.Integer)
                                      {
                                          Value = status,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);
                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public bool AddPendingCase(PendingCaseBLLC pCase) {
            return IsExistingPendingCase(pCase.ID) ? UpdatePendingCase(pCase) : InsertPendingCase(pCase);
        }

        public bool AddAddressInfo(PendingCaseBLLC pCase) {
            string query = "select * from mt_v_Individual where ssno = '" + pCase.DefendantID + "'"; //'267081M'";
            DataSet ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                pCase.Street = ds.Tables[0].Rows[0]["street"].ToString();
                pCase.Location = ds.Tables[0].Rows[0]["location"].ToString();
                pCase.StreetFloor = ds.Tables[0].Rows[0]["street_floor"].ToString();
                return true;
            }
            return false;
        }

        public bool AddCompanyAddressInfo(PendingCaseBLLC pCase) {
            string query =
                "select StreetNative AS street, NameNative AS location from np_Address, np_IDNumbers, np_City " +
                "where Number = '" + pCase.DefendantID + "' " +
                "and np_Address.CreditInfoID = np_IDNumbers.CreditInfoID " +
                "and np_Address.CityID = np_City.CityID " +
                "and np_Address.IsTradingAddress = 'False'";

            DataSet ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                pCase.Street = ds.Tables[0].Rows[0]["street"].ToString();
                pCase.Location = ds.Tables[0].Rows[0]["location"].ToString();
                return true;
            }
            return false;
        }

        protected bool UpdatePendingCase(PendingCaseBLLC pCase) {
            FunctionName = "InsertPendingCase(PendingCaseBLLC pCase)";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        var query = "UPDATE mt_PendingCases SET DefendantID=?, DefendantName=?, PlaintiffID=?, " +
                                       "PlaintiffName=?, Court=?, Adjudicator=?, DateOfWrit=?, WritNumber=?, ValueOfSuit=?, Comments=?, CurrencyCode=? WHERE ID = " +
                                       pCase.ID;
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("DefendantID", OleDbType.VarChar)
                                      {
                                          Value = pCase.DefendantID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DefendantName", OleDbType.VarChar)
                                  {
                                      Value = pCase.DefendantName,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("PlaintiffID", OleDbType.VarChar)
                                  {
                                      Value = pCase.PlaintiffID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("PlaintiffName", OleDbType.VarChar)
                                  {
                                      Value = pCase.PlaintiffName,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("Court", OleDbType.Integer)
                                  {
                                      Value = pCase.Court,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("Adjudicator", OleDbType.Integer)
                                  {
                                      Value = pCase.Adjudicator,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DateOfWrit", OleDbType.Date)
                                  {
                                      Value = pCase.DateOfWrit,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("WritNumber", OleDbType.VarChar)
                                  {
                                      Value = pCase.WritNumber,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ValueOfSuit", OleDbType.Double)
                                  {
                                      Value = pCase.ValueOfSuit,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("Comments", OleDbType.VarChar)
                                  {
                                      Value = pCase.Comments,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("CurrencyCode", OleDbType.VarChar, 3)
                                  {
                                      Value = pCase.CurrencyCode,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        protected bool InsertPendingCase(PendingCaseBLLC pCase) {
            FunctionName = "InsertPendingCase(PendingCaseBLLC pCase)";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        const string query = "INSERT INTO mt_PendingCases(DefendantID, DefendantName, PlaintiffID, " +
                                             "PlaintiffName, Court, Adjudicator, DateOfWrit, WritNumber, ValueOfSuit, Comments, CurrencyCode) " +
                                             "VALUES " +
                                             "(?,?,?,?,?,?,?,?,?,?,?)";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("DefendantID", OleDbType.VarChar)
                                      {
                                          Value = pCase.DefendantID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DefendantName", OleDbType.VarChar)
                                  {
                                      Value = pCase.DefendantName,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("PlaintiffID", OleDbType.VarChar)
                                  {
                                      Value = pCase.PlaintiffID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("PlaintiffName", OleDbType.VarChar)
                                  {
                                      Value = pCase.PlaintiffName,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("Court", OleDbType.Integer)
                                  {
                                      Value = pCase.Court,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("Adjudicator", OleDbType.Integer)
                                  {
                                      Value = pCase.Adjudicator,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DateOfWrit", OleDbType.Date)
                                  {
                                      Value = pCase.DateOfWrit,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("WritNumber", OleDbType.VarChar)
                                  {
                                      Value = pCase.WritNumber,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ValueOfSuit", OleDbType.Double)
                                  {
                                      Value = pCase.ValueOfSuit,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("Comments", OleDbType.VarChar)
                                  {
                                      Value = pCase.Comments,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("CurrencyCode", OleDbType.VarChar, 3)
                                  {
                                      Value = pCase.CurrencyCode,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);
                        
                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public bool IsAvailableClaimWithSameCaseNumberAndSource(int id) {
            FunctionName = "IsAvailableClaimWithSameCaseNumberAndSource(int id)";
            try {
                var pCase = GetPendingCase(id.ToString());
                if (pCase != null) {
                    using (var myOleDbConn = OpenConnection()) {
                        var myOleDbCommand =
                            new OleDbCommand(
                                "select * from np_Claims where CaseNr = '" + pCase.WritNumber +
                                "' and InformationSourceID = '" + pCase.Court + "'",
                                myOleDbConn);
                        var reader = myOleDbCommand.ExecuteReader();
                        if (reader.Read()) {
                            return true;
                        }
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName, e, true);
            }
            return false;
        }

        protected bool IsExistingPendingCase(int id) {
            FunctionName = "IsExistingPendingCase(int id)";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    var myOleDbCommand = new OleDbCommand(
                        "SELECT * from mt_PendingCases where ID = " + id, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return true;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName, e, true);
            }
            return false;
        }
    }
}