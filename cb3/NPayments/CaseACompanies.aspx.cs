#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Claims;
using NPayments.BLL.Localization;
using UserAdmin.BLL.CIUsers;
using Debtor=UserAdmin.BLL.Debtor;

#endregion

using Cig.Framework.Base.Configuration;

namespace NPayments {
    /// <summary>
    /// Summary description for CaseACompanys.
    /// </summary>
    public class CaseACompanys : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly NPaymentsFactory myFact = new NPaymentsFactory();
        protected Button btnCreate;
        protected Button btnCreateNewClaim;
        protected Button btnSearch;
        protected Button btnSearchClaim;
        protected Button btnShowAll;
        protected Button btnSubmitAndClear;
        protected Button btSubmit;
        private int claimStatus;
        protected CustomValidator CustomValidator1;
        protected CustomValidator CustomValidator2;
        protected CustomValidator CustomValidator3;
        protected DropDownList ddInformationSourceList;
        protected DropDownList ddType;
        protected DataGrid dgClaimOwner;
        protected DataGrid dgCompanies;
        protected HtmlGenericControl divClaimOwner;
        protected HtmlGenericControl divNameSearch;
        private bool EN;
        protected Image imgCompany;
        protected Image imgIndividual;
        protected Label Label1;
        protected Label Label3;
        protected Label lbCaseID;
        protected Label lbClaim;
        protected Label lbClaimID;
        protected Label lbComment;
        protected Label lbCreditorOwnerNative;
        protected Label lbDates;
        protected Label lbDebtor;
        protected Label lbGazettePage;
        protected Label lbGazetteYear;
        protected Label lbInfoOrigin;
        protected Label lbInternalComment;
        protected Label lblCIID;
        protected Label lblClaimOwner;
        protected Label lblClaimOwnerCIID;
        protected Label lblCompanyName;
        protected Label lblCompanyNameEN;
        protected Label lblDgResults;
        protected Label lblDgResults2;
        protected Label lblMsg;
        protected Label lblNationalID;
        protected Label lbOnHold;
        protected Label lbOther;
        protected Label lbPayment;
        protected Label lbRegisted;
        protected Label lbRegister;
        protected Label lbRegistration;
        protected Label lbStatus;
        protected Label lbStatusType;
        private bool newRegistration = true;
        protected RadioButton rbtnCompany;
        protected RadioButton rbtnIndividual;
        public string redirectPage = "";
        protected RequiredFieldValidator RequiredFieldValidator4;
        protected RequiredFieldValidator RequiredFieldValidator5;
        protected RequiredFieldValidator rfvCIID;
        protected RequiredFieldValidator rfvClaimOwnerCIID;
        protected TextBox tbCaseID;
        protected TextBox tbClaimDate;
        protected TextBox tbClaimDelayedDate;
        protected TextBox tbClaimID;
        protected TextBox tbComment;
        protected TextBox tbCreditorNameNative;
        protected TextBox tbGazettePage;
        protected TextBox tbGazetteYear;
        protected TextBox tbInternalComment;
        protected TextBox tbPageID;
        protected TextBox tbPaymentDate;
        protected TextBox tbRegDate;
        protected TextBox tbRegister;
        protected TextBox tbStatus;
        protected HtmlTableCell tdNameSearchGrid;
        protected HtmlTable trClaimOwnerNameSearchGrid;
        protected HtmlTable trNameSearchGrid;
        protected TextBox txtClaimOwnerCIID;
        protected TextBox txtCompanyCIID;
        protected TextBox txtCompanyName;
        protected TextBox txtCompanyNameEN;
        protected TextBox txtCompanyNationalID;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            lblMsg.Visible = false;
            Page.ID = "202";
            if (Request.QueryString["head"] != null) {
                ClearSessionVariables();
            }
            // check the current culture
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            AddEnterEvent();

            if (Request.QueryString["update"] != null) {
                newRegistration = false;
            }

            if (Session["ClaimStatus"] != null) {
                claimStatus = (int) Session["ClaimStatus"];
            }

            if (!IsPostBack) {
                tbPageID.Text = "";
                trNameSearchGrid.Visible = false;
                trClaimOwnerNameSearchGrid.Visible = false;
                InitControls();
                SetColumnsHeaderByCulture();
                SetColumnsByCulture();
                //	Session.Remove("ClaimStatus");
            }

            if (newRegistration) {
                claimStatus = 1;
            } else {
                if (Request.Params["pageid"] != null) {
                    SetRedirectPage(int.Parse(Request.Params["pageid"]));
                    tbPageID.Text = Request.Params["pageid"];
                }
            }
            if (tbPageID.Text != "") {
                SetRedirectPage(int.Parse(tbPageID.Text));
            }
        }

        private void InitControls() {
            int creditInfoID = -1;
            InitDDBoxes();
            if (newRegistration) {
                claimStatus = 1;
                tbRegDate.Text = DateTime.Now.ToShortDateString();
                var myStatus = myFact.GetStatus(claimStatus);
                tbStatus.Text = EN ? myStatus.StateEN : myStatus.StateNative;

                if (Session["ClaimOwnerInfo"] != null && Session["ClaimOwnerInfo"].ToString() == "true") {
                    if (Session["CreditInfoID"] != null) {
                        txtClaimOwnerCIID.Text = Session["CreditInfoID"].ToString();
                    }
                    if (Session["ClaimOwnerNameNative"] != null) {
                        tbCreditorNameNative.Text = Session["ClaimOwnerNameNative"].ToString().Trim();
                    }
                    if (Session["ClaimOwnerNameEN"] != null && tbCreditorNameNative.Text == "") {
                        tbCreditorNameNative.Text = Session["ClaimOwnerNameEN"].ToString().Trim();
                    }

                    if (Session["txtCompanyName"] != null) {
                        txtCompanyName.Text = Session["txtCompanyName"].ToString();
                    }
                    if (Session["txtCompanyNameEN"] != null) {
                        txtCompanyNameEN.Text = Session["txtCompanyNameEN"].ToString();
                    }
                    if (Session["txtCompanyNationalID"] != null) {
                        txtCompanyNationalID.Text = Session["txtCompanyNationalID"].ToString();
                    }
                    if (Session["txtCompanyCIID"] != null) {
                        creditInfoID = int.Parse(Session["txtCompanyCIID"].ToString());
                        txtCompanyCIID.Text = Session["txtCompanyCIID"].ToString();
                    }

                    Session.Remove("txtCompanyName");
                    Session.Remove("txtCompanyNameEN");
                    Session.Remove("txtCompanyNationalID");
                    Session.Remove("txtCompanyCIID");

                    Session.Remove("ClaimOwnerInfo");
                    Session.Remove("CreditInfoID");
                    Session.Remove("ClaimOwnerNameNative");
                    Session.Remove("ClaimOwnerNameEN");
                } else {
                    if (Session["CreditInfoID"] != null) {
                        creditInfoID = int.Parse(Session["CreditInfoID"].ToString());
                    }
                    if (Session["CompanyNameNative"] != null) {
                        txtCompanyName.Text = Session["CompanyNameNative"].ToString();
                    }
                    if (Session["CompanyNameEN"] != null) {
                        txtCompanyNameEN.Text = Session["CompanyNameEN"].ToString();
                    }
                    if (creditInfoID != -1) {
                        txtCompanyCIID.Text = creditInfoID.ToString();
                    }

                    if (Session["txtClaimOwnerCIID"] != null) {
                        txtClaimOwnerCIID.Text = Session["txtClaimOwnerCIID"].ToString();
                    }
                    if (Session["tbCreditorNameNative"] != null) {
                        tbCreditorNameNative.Text = Session["tbCreditorNameNative"].ToString();
                    }
                    Session.Remove("txtClaimOwnerCIID");
                    Session.Remove("tbCreditorNameNative");
                }
                Session.Remove("CreditInfoID");
                Session.Remove("CompanyNameNative");
                Session.Remove("CompanyNameEN");
            } else {
                btnSubmitAndClear.Enabled = false;
                //Check for redirect page 
                var claimID = (int) Session["ClaimID"];
                creditInfoID = (int) Session["CreditInfoID"];
                Claim myClaim = myFact.GetClaim(claimID);
                tbClaimID.Text = claimID.ToString();
                btSubmit.Text = rm.GetString("txtUpdate", ci);
                lbClaimID.Text = rm.GetString("lbClaimID", ci) + " : " + myClaim.ClaimID;
                lbClaimID.Visible = true;
                txtCompanyCIID.Text = myClaim.CreditInfoID.ToString();

                txtClaimOwnerCIID.Text = myClaim.ClaimOwnerCIID.ToString();
                if (myClaim.ClaimOwnerCIID > 0) {
                    string claimOwnerType = myFact.GetCIUserType(myClaim.ClaimOwnerCIID);
                    if (claimOwnerType.Trim().Equals("-1")) // no type found
                    {
                        claimOwnerType = CigConfig.Configure("lookupsettings.individualID");
                            // then set to individual as default
                    }
                    if (claimOwnerType.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                        var myIndi = myFact.GetIndivitual(myClaim.ClaimOwnerCIID);
                        rbtnCompany.Checked = false;
                        rbtnIndividual.Checked = true;
                        if (EN) {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameEN)) {
                                tbCreditorNameNative.Text = myIndi.FirstNameEN + " " + myIndi.SurNameEN;
                            } else {
                                tbCreditorNameNative.Text = myIndi.FirstNameNative + " " + myIndi.SurNameNative;
                            }
                        } else {
                            if (!string.IsNullOrEmpty(myIndi.FirstNameNative)) {
                                tbCreditorNameNative.Text = myIndi.FirstNameNative + " " + myIndi.SurNameNative;
                            } else {
                                tbCreditorNameNative.Text = myIndi.FirstNameEN + " " + myIndi.SurNameEN;
                            }
                        }
                    } else if (claimOwnerType.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                        // company
                        var myComp = myFact.GetCompany(myClaim.ClaimOwnerCIID);
                        if (EN) {
                            tbCreditorNameNative.Text = !string.IsNullOrEmpty(myComp.NameEN) ? myComp.NameEN : myComp.NameNative;
                        } else {
                            tbCreditorNameNative.Text = !string.IsNullOrEmpty(myComp.NameNative) ? myComp.NameNative : myComp.NameEN;
                        }
                    }
                }

                ddInformationSourceList.SelectedValue = myClaim.InformationSourceID.ToString();
                tbRegDate.Text = myClaim.RegDate.ToString();
                if (myClaim.ClaimDate > DateTime.MinValue) {
                    tbClaimDate.Text = myClaim.ClaimDate.ToShortDateString();
                }
                if (myClaim.DelayDate > DateTime.MinValue) {
                    tbClaimDelayedDate.Text = myClaim.DelayDate.ToShortDateString();
                }
                if (myClaim.PaymentDate > DateTime.MinValue) {
                    tbPaymentDate.Text = myClaim.PaymentDate.ToShortDateString();
                }
                ddType.SelectedValue = myClaim.CaseDecisionType; // .ClaimTypeID.ToString();
                tbCaseID.Text = myClaim.CaseNumber;
                tbGazetteYear.Text = myClaim.GazatteYear;
                tbGazettePage.Text = myClaim.GazettePage;
                tbStatus.Text = myClaim.StateEN;
                Session["ClaimStatus"] = myClaim.StatusID;
                tbRegister.Text = myClaim.RegisterInitials;
                tbComment.Text = myClaim.RegisterCommentNative;
                tbInternalComment.Text = myClaim.RegisterInternalCommentNative;
                Session.Remove("ClaimID");
                Session.Remove("CreditInfoID");
            }
            if (creditInfoID == -1) {
                LocalizeText();
                return;
            }
            string type = myFact.GetCIUserType(creditInfoID);
            if (type.Trim().Equals("-1")) // no type found
            {
                type = CigConfig.Configure("lookupsettings.individualID"); // then set to individual as default
            }
            if (type.Trim().Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                Indivitual myIndi = myFact.GetIndivitual(creditInfoID);
                txtCompanyName.Text = myIndi.FirstNameNative;
                txtCompanyNameEN.Text = myIndi.FirstNameEN;
            } else if (type.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                // company
                var myComp = myFact.GetCompany(creditInfoID);
                txtCompanyName.Text = myComp.NameNative;
                txtCompanyNameEN.Text = myComp.NameEN;
            }
            LocalizeText();
            //			InitDDBoxes();
        }

        public void SetRedirectPage(int index) { redirectPage = CigConfig.Configure("lookupsettings.redirectpageID" + index); }

        public void InitDDBoxes() {
            ddInformationSourceList.DataSource = myFact.GetInformationSourceListAsDataSet();
            ddInformationSourceList.DataTextField = EN ? "NameEN" : "NameNative";

            ddInformationSourceList.DataValueField = "InformationSourceID";
            ddInformationSourceList.DataBind();

            ddType.DataSource = myFact.GetCaseDecisionsTypesAsDataSet();
            ddType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddType.DataValueField = "StatusType";
            ddType.DataBind();

            if (newRegistration) {
                // Select N/A as the default one
                int index = ddType.Items.IndexOf(ddType.Items.FindByText("N/A"));
                if (index > 0) {
                    ddType.SelectedIndex = index;
                }
            }
        }

        private void btSubmit_Click(object sender, EventArgs e) { submit(false); }

        private void submit(bool clear) {
            var myClaim = new Claim();
            var myFact1 = new NPaymentsFactory();
            if (Page.IsValid) {
                try {
                    myClaim.CreditInfoID = int.Parse(txtCompanyCIID.Text);
                    myClaim.ClaimOwnerCIID = int.Parse(txtClaimOwnerCIID.Text);
                    myClaim.ClaimTypeID = int.Parse(CigConfig.Configure("lookupsettings.ct_casecompanies"));
                    myClaim.InformationSourceID = int.Parse(ddInformationSourceList.SelectedValue);
                        // .SelectedIndex + 1;
                    myClaim.CaseNumber = tbCaseID.Text;
                    myClaim.RegisterCommentNative = tbComment.Text;
                    myClaim.RegisterInternalCommentNative = tbInternalComment.Text;
                    myClaim.CaseDecisionType = ddType.SelectedItem.Value;

                    IFormatProvider format = CultureInfo.CurrentCulture;
                    if (tbRegDate.Text != "") {
                        myClaim.RegDate = DateTime.Parse(tbRegDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbClaimDate.Text != "") {
                        myClaim.ClaimDate = DateTime.Parse(tbClaimDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbClaimDelayedDate.Text != "") {
                        myClaim.DelayDate = DateTime.Parse(
                            tbClaimDelayedDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbPaymentDate.Text != "") {
                        myClaim.PaymentDate = DateTime.Parse(
                            tbPaymentDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }

                    try {
                        myClaim.RegisterID = (int) Session["UserCreditInfoID"];
                    } catch (Exception err) {
                        myClaim.RegisterID = 2077280554;
                        Logger.WriteToLog(
                            "CaseACompanies,aspx, btSubmit, myClaim.RegisterID = (int) Session[\"UserCreditInfoID\"] " +
                            err.Message,
                            true);
                    }

                    if (tbGazetteYear.Text != "") {
                        myClaim.GazatteYear = tbGazetteYear.Text;
                    }
                    if (tbGazettePage.Text != "") {
                        myClaim.GazettePage = tbGazettePage.Text;
                    }
                    myClaim.RegisterCommentNative = tbComment.Text;
                    myClaim.RegisterInternalCommentNative = tbInternalComment.Text;
                    if (claimStatus != 0) {
                        // myClaim.StatusID = 4; // Cases against companies get Registrated status no matter what
                        myClaim.CaseAgainstCompanie = true;
                        myClaim.SetClaimStatus();
                            // claimStatus; //int.Parse(this.tbStatus.Text); // athuga me� value h�r!
                    } else {
                        myClaim.StatusID = int.Parse(CigConfig.Configure("lookupsettings.formerstatusid"));
                    }
                } catch (Exception err) {
                    Logger.WriteToLog("Exception on NewClaim.aspx caught, message is : " + err.Message, true);
                    lblMsg.Text = rm.GetString("errMsg2", ci);
                    lblMsg.Visible = true;
                    return;
                }
                if (newRegistration) {
                    if (!myFact1.AddNewClaim(myClaim)) {
                        lblMsg.Text = rm.GetString("errMsg1", ci);
                        lblMsg.Visible = true;
                        return;
                    }
                    lblMsg.Text = rm.GetString("txtClaimRegistered", ci);
                    lblMsg.Visible = true;
                    ClearSessionVariables();
                    PrepareControlsForNewRegistration();
                } else {
                    myClaim.LastUpdate = DateTime.Now;
                    myClaim.ClaimID = int.Parse(tbClaimID.Text);
                    if (!myFact1.UpdateClaim(myClaim)) {
                        lblMsg.Text = rm.GetString("errMsg12", ci);
                        lblMsg.Visible = true;
                        return;
                    }
                    lblMsg.Text = rm.GetString("txtClaimUpdated", ci);
                    lblMsg.Visible = true;
                    // halda gildunum
                    //	ClearSessionVariables();
                    //	PrepareControlsForNewRegistration();
                    //If redirect-page is set then redirect
                    if (!string.IsNullOrEmpty(redirectPage)) {
                        Response.Redirect(redirectPage);
                    }
                }
                ClearForm(clear);
            } else // if(Page.IsValid)
            {
                Logger.WriteToLog("NewChDcAof.aspx Page.IsValid = false", true);
            }
        }

        private void btnSubmitAndClear_Click(object sender, EventArgs e) { submit(true); }

        public void DateValidate(object source, ServerValidateEventArgs value) {
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        private void ClearForm(bool clearDebtor) {
            if (clearDebtor) {
                txtCompanyName.Text = "";
                txtCompanyNameEN.Text = "";
                txtCompanyCIID.Text = "";
                txtCompanyNationalID.Text = "";
            }
            ddInformationSourceList.SelectedIndex = 0;
            tbClaimDate.Text = "";
            tbClaimDelayedDate.Text = "";
            tbPaymentDate.Text = "";
            ddType.SelectedIndex = 0;
            tbCaseID.Text = "";
            tbGazetteYear.Text = "";
            tbGazettePage.Text = "";
            tbStatus.Text = "";
            tbRegister.Text = "";
            tbComment.Text = "";
            txtClaimOwnerCIID.Text = "";
            tbCreditorNameNative.Text = "";
            //tbCreditorNameEN.Text="";
            tbClaimID.Text = "";
            tbInternalComment.Text = "";
            trNameSearchGrid.Visible = false;
            trClaimOwnerNameSearchGrid.Visible = false;
            Session.Remove("ClaimStatus");
        }

        private void PrepareControlsForNewRegistration() {
            newRegistration = true;
            claimStatus = 1;
            tbRegDate.Text = DateTime.Now.ToString();
            Status myStatus = myFact.GetStatus(claimStatus);
            tbStatus.Text = myStatus.StateEN;
            trNameSearchGrid.Visible = false;
            trClaimOwnerNameSearchGrid.Visible = false;
        }

        private void LocalizeText() {
            lblCIID.Text = rm.GetString("lbDebtorID", ci);
            lblNationalID.Text = rm.GetString("lbNationalID", ci);
            lbDates.Text = rm.GetString("lbDates", ci);
            lbRegisted.Text = rm.GetString("lbRegisted", ci);
            lbClaim.Text = rm.GetString("lbClaim", ci);
            lbOnHold.Text = rm.GetString("lbOnHold", ci);
            lbPayment.Text = rm.GetString("lbPayment", ci);
            lbOther.Text = rm.GetString("lbOther", ci);
            lbCaseID.Text = rm.GetString("lbCaseID", ci);
            lbRegistration.Text = rm.GetString("lbRegistration", ci);
            lbStatus.Text = rm.GetString("lbStatus", ci);
            lbRegister.Text = rm.GetString("lbRegister", ci);
            lbComment.Text = rm.GetString("lbComment", ci);
            btSubmit.Text = rm.GetString("txtSubmitAndNew", ci);
            lblCompanyName.Text = rm.GetString("txtCompany", ci);
            lblCompanyNameEN.Text = rm.GetString("txtCompany(EN)", ci);
            lbInfoOrigin.Text = rm.GetString("lbInfoOrigin", ci);
            lbGazetteYear.Text = rm.GetString("lbGazetteYear", ci);
            lbGazettePage.Text = rm.GetString("lbGazettePage", ci);
            lbInternalComment.Text = rm.GetString("lbInternalComment", ci);
            RequiredFieldValidator4.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator5.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvClaimOwnerCIID.ErrorMessage = rm.GetString("txtValueMissing", ci);
            CustomValidator1.Text = rm.GetString("CustomValidator1", ci);
            CustomValidator2.Text = rm.GetString("CustomValidator2", ci);
            CustomValidator3.Text = rm.GetString("CustomValidator3", ci);
            btnShowAll.Text = rm.GetString("txtShowAllCases", ci);
            lbStatusType.Text = rm.GetString("lbStatusType", ci);
            lbDebtor.Text = rm.GetString("txtDebtor", ci);
            lbCreditorOwnerNative.Text = rm.GetString("lbClaimOwnerName", ci);
            lblClaimOwnerCIID.Text = rm.GetString("txtClaimOwnerCIID", ci);
            btnSubmitAndClear.Text = rm.GetString("txtSubmitAndClear", ci);
            rfvCIID.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblClaimOwner.Text = rm.GetString("txtClaimOwner", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);
            btnCreate.Text = rm.GetString("txtCreateNew", ci);
            btnCreateNewClaim.Text = rm.GetString("txtCreateNew", ci);
            btnSearchClaim.Text = rm.GetString("txtSearch", ci);

            //this.rbtnCompany.Text = rm.GetString("txtCompany",ci);
            imgCompany.AlternateText = rm.GetString("txtCompany", ci);
            //this.rbtnIndividual.Text = rm.GetString("txtIndividual",ci);
            imgIndividual.AlternateText = rm.GetString("txtIndividual", ci);

            lblDgResults.Text = rm.GetString("lblDgResults", ci);
            lblDgResults2.Text = rm.GetString("lblDgResults", ci);
        }

        public void ClearSessionVariables() {
            Session["ClaimStatus"] = null;
            Session["CreditInfoID"] = null;
            Session["DebtorFirstName"] = null;
            Session["DebtorSurName"] = null;
            Session["ClaimID"] = null;
        }

        private void AddEnterEvent() {
            var frm = FindControl("vansk");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
                if (ctrl is DropDownList) {
                    ((DropDownList) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void btnShowAll_Click(object sender, EventArgs e) {
            if (txtCompanyCIID.Text != "") {
                Session["CreditInfoID"] = txtCompanyCIID.Text;
                Server.Transfer("ShowAllCases.aspx");
            }
        }

        private void SetColumnsByCulture() {
            dgCompanies.Columns[0].Visible = true;
            dgCompanies.Columns[1].Visible = true;
            dgCompanies.Columns[2].Visible = true;
            dgCompanies.Columns[3].Visible = true;
            dgCompanies.Columns[4].Visible = false;
            dgCompanies.Columns[5].Visible = false;
            dgCompanies.Columns[6].Visible = false;
            dgCompanies.Columns[7].Visible = false;
            dgCompanies.Columns[8].Visible = true;
        }

        private void SetColumnsHeaderByCulture() {
            dgCompanies.Columns[1].HeaderText = rm.GetString("lbNationalID", ci);
            dgCompanies.Columns[2].HeaderText = rm.GetString("txtCompany", ci);
            dgCompanies.Columns[3].HeaderText = rm.GetString("lbAddress", ci);
            dgCompanies.Columns[8].HeaderText = rm.GetString("lbDebtorID", ci);
        }

        protected void SearchForCompany(int ciid, string nationalID, string companyName) {
            trClaimOwnerNameSearchGrid.Visible = false;
            var myComp = new Company {CreditInfoID = ciid, NameNative = companyName};

            if (nationalID.Length > 0) {
                var myID = new IDNumber {Number = nationalID};
                try {
                    myID.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID").Trim());
                } catch (Exception) {
                    //hmmmm
                }
                myComp.IDNumbers.Add(myID);
            }

            try {
                var ds = CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True" ? myFact.FindCompanyInNationalAndCreditInfo(myComp) : myFact.FindCompany(myComp);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        //if creditinfoid
                        string sCreditInfoID = ds.Tables[0].Rows[0]["CreditInfoID"].ToString();
                        if (sCreditInfoID != "") {
                            trNameSearchGrid.Visible = false;
                            txtCompanyName.Text = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            txtCompanyNameEN.Text = ds.Tables[0].Rows[0]["NameEN"].ToString().Trim();
                            txtCompanyCIID.Text = ds.Tables[0].Rows[0]["CreditInfoID"].ToString();
                            txtCompanyNationalID.Text = ds.Tables[0].Rows[0]["Number"].ToString().Trim();
                        } else {
                            dgCompanies.DataSource = ds;
                            dgCompanies.DataBind();
                            trNameSearchGrid.Visible = true;

                            divNameSearch.Style["HEIGHT"] = "auto";
                            divNameSearch.Style["OVERFLOW"] = "auto";
                            divNameSearch.Style["OVERFLOW-X"] = "auto";
                        }
                    } else {
                        dgCompanies.DataSource = ds;
                        dgCompanies.DataBind();
                        trNameSearchGrid.Visible = true;

                        int gridRows = dgCompanies.Items.Count;
                        divNameSearch.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                        divNameSearch.Style["OVERFLOW"] = "auto";
                        divNameSearch.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    trNameSearchGrid.Visible = false;
                    txtCompanyName.Text = companyName;
                    txtCompanyNameEN.Text = "";
                    txtCompanyCIID.Text = ciid == -1 ? "" : ciid.ToString();
                    txtCompanyNationalID.Text = nationalID;
                }
            } catch (Exception err) {
                Logger.WriteToLog("CaseACompanies.aspx ", err, true);
            }
        }

        protected void SearchForClaimOwner(int ciid, string firstName, bool company) {
            trNameSearchGrid.Visible = false;
            var myDebt = new Debtor {CreditInfo = ciid};

            var arrName = firstName.Split(' ');
            if (arrName.Length > 0) {
                if (arrName.Length == 1) {
                    myDebt.FirstName = arrName[0];
                } else {
                    myDebt.SurName = arrName[arrName.Length - 1];
                    myDebt.FirstName = firstName.Substring(0, firstName.Length - (myDebt.SurName.Length + 1));
                }
            }

            var myComp = new Company {CreditInfoID = ciid, NameNative = firstName};

            try {
                DataSet ds;
                if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True") {
                    ds = company ? myFact.FindCompanyInNationalAndCreditInfo(myComp) : myFact.FindCustomerInNationalAndCreditInfo(myDebt);
                } else {
                    ds = company ? myFact.FindCompany(myComp) : myFact.FindCustomer(myDebt);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                    if (ds.Tables[0].Rows.Count == 1) {
                        //Only one result - then dont display grid and set value in fields
                        //if creditinfoid
                        string sCreditInfoID = ds.Tables[0].Rows[0]["CreditInfoID"].ToString().Trim();
                        if (sCreditInfoID != "") {
                            //this.tdNameSearchGrid.Visible=false;

                            trClaimOwnerNameSearchGrid.Visible = false;
                            tbCreditorNameNative.Text = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                            txtClaimOwnerCIID.Text = sCreditInfoID;
                        } else {
                            dgClaimOwner.DataSource = ds;
                            dgClaimOwner.DataBind();
                            trClaimOwnerNameSearchGrid.Visible = true;

                            divNameSearch.Style["HEIGHT"] = "auto"; //dtgrNameSearch.Height.ToString();
                            divNameSearch.Style["OVERFLOW"] = "auto";
                            divNameSearch.Style["OVERFLOW-X"] = "auto";
                        }
                    } else {
                        dgClaimOwner.DataSource = ds;
                        dgClaimOwner.DataBind();
                        trClaimOwnerNameSearchGrid.Visible = true;

                        int gridRows = dgClaimOwner.Items.Count;
                        divClaimOwner.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                        divClaimOwner.Style["OVERFLOW"] = "auto";
                        divClaimOwner.Style["OVERFLOW-X"] = "auto";
                    }
                } else {
                    trClaimOwnerNameSearchGrid.Visible = false;
                    tbCreditorNameNative.Text = firstName;
                    txtClaimOwnerCIID.Text = ciid == -1 ? "" : ciid.ToString();
                }
            } catch (Exception err) {
                Logger.WriteToLog("NewChDcAof.aspx ", err, true);
            }
        }

        private void dgCompanies_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (!e.CommandName.Equals("Select")) {
                return;
            }
            int ciid = -1;
            if (e.Item.Cells[8].Text != "") {
                try {
                    ciid = int.Parse(e.Item.Cells[8].Text);
                } catch (Exception) {
                    ciid = -1;
                }
            }
            if (ciid == -1) {
                //Then ciid is not found - then we need to create the user - redirect the user
                //to NewCIUserC.aspx

                //Store claim owner in session if available
                if (txtClaimOwnerCIID.Text != "") {
                    Session["txtClaimOwnerCIID"] = txtClaimOwnerCIID.Text;
                }
                if (tbCreditorNameNative.Text != "") {
                    Session["tbCreditorNameNative"] = tbCreditorNameNative.Text;
                }

                Session["AddCompany"] = true;
                if (e.Item.Cells[1].Text.Trim() != "&nbsp;") {
                    Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                }
                if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                    Session["NameNative"] = e.Item.Cells[4].Text.Trim();
                }
                if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                    Session["NameEN"] = e.Item.Cells[5].Text.Trim();
                }
                if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                    Session["StreetNative"] = e.Item.Cells[6].Text.Trim();
                }
                if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                    Session["StreetEN"] = e.Item.Cells[7].Text.Trim();
                }
                Server.Transfer("NewCIUserC.aspx?pageid=3");
            } else {
                SearchForCompany(ciid, "", "");
            }
        }

        private void dgCompanies_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer) {
                return;
            }
            if (EN) {
                //Set name - EN if available, else native
                if (e.Item.Cells[5].Text != "" && e.Item.Cells[5].Text != "&nbsp;") {
                    e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                } else {
                    e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                }

                //Set street - EN if available, else native
                if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                    e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                } else {
                    e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                }
            } else {
                //Set name - Native if available, else EN
                if (e.Item.Cells[4].Text != "" && e.Item.Cells[4].Text != "&nbsp;") {
                    e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                } else {
                    e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                }

                //Set street - Native if available, else EN
                if (e.Item.Cells[6].Text != "" && e.Item.Cells[6].Text != "&nbsp;") {
                    e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                } else {
                    e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                }
            }
        }

        private void dgClaimOwner_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer) {
                return;
            }
            if (EN) {
                //Set name - EN if available, else native
                if (e.Item.Cells[5].Text != "" && e.Item.Cells[5].Text != "&nbsp;") {
                    e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                } else {
                    e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                }

                //Set street - EN if available, else native
                if (e.Item.Cells[7].Text != "" && e.Item.Cells[7].Text != "&nbsp;") {
                    e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                } else {
                    e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                }
            } else {
                //Set name - Native if available, else EN
                if (e.Item.Cells[4].Text != "" && e.Item.Cells[4].Text != "&nbsp;") {
                    e.Item.Cells[2].Text = e.Item.Cells[4].Text;
                } else {
                    e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                }

                //Set street - Native if available, else EN
                if (e.Item.Cells[6].Text != "" && e.Item.Cells[6].Text != "&nbsp;") {
                    e.Item.Cells[3].Text = e.Item.Cells[6].Text;
                } else {
                    e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                }
            }
        }

        private void dgClaimOwner_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (!e.CommandName.Equals("Select")) {
                return;
            }
            int ciid = -1;
            if (e.Item.Cells[8].Text != "") {
                try {
                    ciid = int.Parse(e.Item.Cells[8].Text);
                } catch (Exception) {
                    ciid = -1;
                }
            }
            if (ciid == -1) {
                //Then ciid is not found - then we need to create the user - redirect the user
                //to NewCIUserC.aspx or NewCIUser.aspx

                //If company already selected - then store in session					
                if (txtCompanyName.Text != "") {
                    Session["txtCompanyName"] = txtCompanyName.Text;
                }
                if (txtCompanyNameEN.Text != "") {
                    Session["txtCompanyNameEN"] = txtCompanyNameEN.Text;
                }
                if (txtCompanyNationalID.Text != "") {
                    Session["txtCompanyNationalID"] = txtCompanyNationalID.Text;
                }
                if (txtCompanyCIID.Text != "") {
                    Session["txtCompanyCIID"] = txtCompanyCIID.Text;
                }

                Session["AddClaimOwner"] = true;
                if (e.Item.Cells[1].Text.Trim() != "&nbsp;") {
                    Session["NationalID"] = e.Item.Cells[1].Text.Trim();
                }
                if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                    Session["StreetNative"] = e.Item.Cells[6].Text.Trim();
                }
                if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                    Session["StreetEN"] = e.Item.Cells[7].Text.Trim();
                }
                if (e.Item.Cells[9].Text.Trim() == "1") {
                    if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                        Session["NameNative"] = e.Item.Cells[4].Text.Trim();
                    }
                    if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                        Session["NameEN"] = e.Item.Cells[5].Text.Trim();
                    }
                    Server.Transfer("NewCIUserC.aspx?pageid=3");
                } else {
                    if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                        string name = e.Item.Cells[4].Text.Trim();
                        string[] arrName = name.Split(' ');
                        if (arrName.Length > 0) {
                            if (arrName.Length == 1) {
                                Session["FirstName"] = arrName[0];
                            } else {
                                string surName = arrName[arrName.Length - 1];
                                Session["SurName"] = surName;
                                Session["FirstName"] = name.Substring(0, name.Length - (surName.Length + 1));
                            }
                        }
                    }
                    Server.Transfer("NewCIUser.aspx?pageid=3");
                }
            } else {
                txtClaimOwnerCIID.Text = ciid.ToString();
                tbCreditorNameNative.Text = e.Item.Cells[2].Text.Trim();
            }
        }

        private void NewClaimOwnerIndividual() {
            //If company already selected - then store in session					
            if (txtCompanyName.Text != "") {
                Session["txtCompanyName"] = txtCompanyName.Text;
            }
            if (txtCompanyNameEN.Text != "") {
                Session["txtCompanyNameEN"] = txtCompanyNameEN.Text;
            }
            if (txtCompanyNationalID.Text != "") {
                Session["txtCompanyNationalID"] = txtCompanyNationalID.Text;
            }
            if (txtCompanyCIID.Text != "") {
                Session["txtCompanyCIID"] = txtCompanyCIID.Text;
            }

            Session["AddClaimOwner"] = true;

            if (tbCreditorNameNative.Text.Trim() != "") {
                string name = tbCreditorNameNative.Text.Trim();
                string[] arrName = name.Split(' ');
                if (arrName.Length > 0) {
                    if (arrName.Length == 1) {
                        Session["FirstName"] = arrName[0];
                    } else {
                        string surName = arrName[arrName.Length - 1];
                        Session["SurName"] = surName;
                        Session["FirstName"] = name.Substring(0, name.Length - (surName.Length + 1));
                    }
                }
            }
            Server.Transfer("NewCIUser.aspx?pageid=3");
        }

        private void NewClaimOwnerCompany() {
            //If company already selected - then store in session					
            if (txtCompanyName.Text != "") {
                Session["txtCompanyName"] = txtCompanyName.Text;
            }
            if (txtCompanyNameEN.Text != "") {
                Session["txtCompanyNameEN"] = txtCompanyNameEN.Text;
            }
            if (txtCompanyNationalID.Text != "") {
                Session["txtCompanyNationalID"] = txtCompanyNationalID.Text;
            }
            if (txtCompanyCIID.Text != "") {
                Session["txtCompanyCIID"] = txtCompanyCIID.Text;
            }

            Session["AddClaimOwner"] = true;
            if (tbCreditorNameNative.Text.Trim() != "") {
                Session["NameNative"] = tbCreditorNameNative.Text.Trim();
            }
            Server.Transfer("NewCIUserC.aspx?pageid=3");
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            int ciid = -1;

            if (txtCompanyCIID.Text.Trim().Length > 0) {
                try {
                    ciid = int.Parse(txtCompanyCIID.Text);
                } catch (Exception) {
                    ciid = -1;
                }
            }

            SearchForCompany(ciid, txtCompanyNationalID.Text, txtCompanyName.Text);
        }

        private void btnCreate_Click(object sender, EventArgs e) { NewClaimOwnerCompany(); }

        private void btnSearchClaim_Click(object sender, EventArgs e) {
            if (tbCreditorNameNative.Text.Trim() != "") {
                int ciid = -1;

                if (txtClaimOwnerCIID.Text.Trim().Length > 0) {
                    try {
                        ciid = int.Parse(txtClaimOwnerCIID.Text);
                    } catch (Exception) {
                        ciid = -1;
                    }
                }

                //SearchClaimOwner
                if (rbtnCompany.Checked) {
                    SearchForClaimOwner(ciid, tbCreditorNameNative.Text, true);
                } else {
                    SearchForClaimOwner(ciid, tbCreditorNameNative.Text, false);
                }
            }
        }

        private void btnCreateNewClaim_Click(object sender, EventArgs e) {
            if (rbtnCompany.Checked) {
                NewClaimOwnerCompany();
            } else {
                NewClaimOwnerIndividual();
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            this.dgCompanies.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCompanies_ItemCommand);
            this.dgCompanies.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCompanies_ItemDataBound);
            this.btnSearchClaim.Click += new System.EventHandler(this.btnSearchClaim_Click);
            this.btnCreateNewClaim.Click += new System.EventHandler(this.btnCreateNewClaim_Click);
            this.dgClaimOwner.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgClaimOwner_ItemCommand);
            this.dgClaimOwner.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgClaimOwner_ItemDataBound);
            this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
            this.btnSubmitAndClear.Click += new System.EventHandler(this.btnSubmitAndClear_Click);
            this.btSubmit.Click += new System.EventHandler(this.btSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}