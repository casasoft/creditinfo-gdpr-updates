<%@ Page language="c#" Codebehind="NewCIUser.aspx.cs" AutoEventWireup="false" Inherits="NPayments.NewDebtor" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>NewDebtor</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						newuser.btRebDebtor.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.newuser.tbDebtorFirstName.focus();
				}

		</SCRIPT>
	</HEAD>
	<body onload="SetFormFocus()">
		<form id="newuser" name="newuser" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language2" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="Sitepositionbar2" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td></td>
										</tr>
										<tr>
											<td>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lbNewIndividual" runat="server">New Individual</asp:label>&nbsp;-
															<asp:label id="lbPersonInformation" runat="server">Person Info</asp:label></th></tr>
													<tr>
														<td>
															<TABLE class="fields" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td style="WIDTH: 2px; HEIGHT: 16px" colSpan="2"><asp:label id="lbDebtorCIID" runat="server">CreditInfoID</asp:label><BR>
																		<asp:textbox id="tbDebtorCIID" runat="server" ReadOnly="True" EnableViewState="False"></asp:textbox></td>
																	<TD style="WIDTH: 186px; HEIGHT: 16px"></TD>
																	<td style="HEIGHT: 16px" colSpan="2"><asp:label id="lbEmail" runat="server">Email</asp:label><BR>
																		<asp:textbox id="tbEmail" runat="server" MaxLength="30"></asp:textbox></td>
																	<TD style="HEIGHT: 16px"></TD>
																</tr>
																<tr>
																	<td style="WIDTH: 2px; HEIGHT: 23px" colSpan="2"><asp:label id="lbDebtorFirstName" runat="server">First name</asp:label><BR>
																		<asp:textbox id="tbDebtorFirstName" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Value missing!" ControlToValidate="tbDebtorFirstName">*</asp:requiredfieldvalidator></td>
																	<TD style="WIDTH: 186px; HEIGHT: 17px"><asp:label id="lbDebtorSurName" runat="server">Surname</asp:label><BR>
																		<asp:textbox id="tbDebtorSurName" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Value missing!" ControlToValidate="tbDebtorSurName">*</asp:requiredfieldvalidator></TD>
																	<td style="HEIGHT: 17px" colSpan="2"><asp:label id="lbFirstNameEN" runat="server">First name(EN)</asp:label><BR>
																		<asp:textbox id="tbFirstNameEN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></td>
																	<TD style="HEIGHT: 17px"><asp:label id="lbSurnameEN" runat="server">Surname(EN)</asp:label><BR>
																		<asp:textbox id="tbSurnameEN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																</tr>
																<TR>
																	<TD style="WIDTH: 2px" colSpan="2" height="23"></TD>
																	<TD style="WIDTH: 186px" height="23"></TD>
																	<TD colSpan="2" height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</TABLE>
														</td>
													</tr>
												</TABLE>
											</td>
										</tr>
										<TR>
											<td height="10"></td>
										</TR>
										<tr>
											<td>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															&nbsp;
															<asp:label id="lbAddress1Header" runat="server">Label</asp:label></th></tr>
													<tr>
														<td>
															<TABLE class="fields" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td style="WIDTH: 105px" colSpan="2"><asp:label id="lbAddress1" runat="server">Address1</asp:label><BR>
																		<asp:textbox id="tbAddress1" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator"
																			ControlToValidate="tbAddress1">*</asp:requiredfieldvalidator></td>
																	<TD style="WIDTH: 187px"><asp:label id="lbAddress1EN" runat="server">Address1(EN)</asp:label><BR>
																		<asp:textbox id="tbAddress1EN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<td style="WIDTH: 207px" colSpan="2"><asp:label id="lbCity1" runat="server">City1</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		<asp:label id="lblCitySearch" runat="server">Search</asp:label><BR>
																		<asp:dropdownlist id="ddCity1" runat="server"></asp:dropdownlist><asp:textbox id="txtCitySearch" runat="server" MaxLength="10" CssClass="short_input"></asp:textbox>&nbsp;<asp:button id="btnCitySearch" runat="server" Width="26px" CssClass="popup" Text="..." CausesValidation="False"></asp:button>
																		<asp:requiredfieldvalidator id="rfvCity" runat="server" ErrorMessage="Value missing!" ControlToValidate="ddCity1">*</asp:requiredfieldvalidator></td>
																</tr>
																<tr>
																	<td style="WIDTH: 105px" colSpan="2" height="17"><asp:label id="lbAddress1Info" runat="server">Address1 Info</asp:label><BR>
																		<asp:textbox id="tbAddress1Info" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></td>
																	<TD style="WIDTH: 187px" height="17"><asp:label id="lbAddress1InfoEN" runat="server">Address1 Info(EN)</asp:label><BR>
																		<asp:textbox id="tbAddress1InfoEN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<td style="WIDTH: 181px" height="17"><asp:label id="lbPostalCode" runat="server">PostalCode1</asp:label><BR>
																		<asp:textbox id="tbPostalCode1" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></td>
																	<TD height="17"><asp:label id="lbPostBox" runat="server">PostBox1</asp:label><BR>
																		<asp:textbox id="tbPostBox1" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																</tr>
																<TR>
																	<TD style="WIDTH: 105px" colSpan="2" height="23"></TD>
																	<TD style="WIDTH: 187px" height="23"></TD>
																	<TD style="WIDTH: 181px" height="23"></TD>
																	<TD height="23"></TD>
																</TR>
															</TABLE>
														</td>
													</tr>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															&nbsp;
															<asp:label id="lbAddress2Header" runat="server">Label</asp:label></th></tr>
													<tr>
														<td>
															<TABLE class="fields" cellSpacing="0" cellPadding="0" border="0">
																<TR class="input">
																	<TD style="WIDTH: 179px" colSpan="2"><asp:label id="lbAddress2" runat="server">Address2</asp:label><BR>
																		<asp:textbox id="tbAddress2" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox>
																	<TD style="WIDTH: 182px"><asp:label id="lbAddress2EN" runat="server">Address2(EN)</asp:label><BR>
																		<asp:textbox id="tbAddress2EN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 163px" colSpan="2"><asp:label id="lbCity2" runat="server">City2</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
																		&nbsp;
																		<asp:label id="lblCity2Search" runat="server">Search</asp:label><BR>
																		<asp:dropdownlist id="ddCity2" runat="server"></asp:dropdownlist>&nbsp;
																		<asp:requiredfieldvalidator id="rfvCity2" runat="server" ErrorMessage="Value missing!" ControlToValidate="ddCity2"
																			Enabled="False">*</asp:requiredfieldvalidator><asp:textbox id="txtCity2Search" runat="server" MaxLength="10" CssClass="short_input"></asp:textbox>&nbsp;<asp:button id="btnCity2Search" runat="server" CssClass="popup" Text="..." CausesValidation="False"></asp:button></TD>
																</TR>
																<TR class="input">
																	<TD style="WIDTH: 179px" colSpan="2"><asp:label id="lbAddress2Info" runat="server">Address2 Info</asp:label><BR>
																		<asp:textbox id="tbAddress2Info" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox>
																	<TD style="WIDTH: 182px"><asp:label id="lbAddress2InfoEN" runat="server">Address2 Info(EN)</asp:label><BR>
																		<asp:textbox id="tbAddress2InfoEN" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD style="WIDTH: 190px"><asp:label id="ltPostalCode2" runat="server">PostalCode2</asp:label><BR>
																		<asp:textbox id="tbPostalCode2" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																	<TD><asp:label id="lbPostBox2" runat="server">PostBox2</asp:label><BR>
																		<asp:textbox id="tbPostBox2" runat="server" EnableViewState="False" MaxLength="50"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 179px" colSpan="2" height="23"></TD>
																	<TD style="WIDTH: 182px" height="23"></TD>
																	<TD style="WIDTH: 163px" colSpan="2" height="23"></TD>
																</TR>
															</TABLE>
														</td>
													</tr>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<TABLE class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															&nbsp;
															<asp:label id="lbAdditionalInfo" runat="server">Label</asp:label></th></tr>
													<tr>
														<td>
															<TABLE class="fields" cellSpacing="0" cellPadding="0" border="0">
																<TR class="input">
																	<TD style="WIDTH: 135px; HEIGHT: 18px"><asp:label id="lbProfession" runat="server">Profession</asp:label><BR>
																		<asp:dropdownlist id="ddProfession" runat="server"></asp:dropdownlist>
																	<TD style="WIDTH: 225px"><asp:label id="lbEducation" runat="server">Education</asp:label><BR>
																		<asp:dropdownlist id="ddEducation" runat="server"></asp:dropdownlist></TD>
																	<TD style="WIDTH: 135px"><asp:label id="lblNote" runat="server">Note</asp:label><BR>
																		<asp:textbox id="tbNote" runat="server"></asp:textbox></TD>
																</TR>
																<TR class="input">
																	<TD style="WIDTH: 135px; HEIGHT: 17px"><asp:label id="lbIDNumber2" runat="server">IDNumber 1</asp:label><BR>
																		<asp:textbox id="tbIDNumber1" runat="server" EnableViewState="False"></asp:textbox>
																	<TD style="WIDTH: 225px; HEIGHT: 17px"><asp:label id="lbIDNumberType" runat="server">Type</asp:label><BR>
																		<asp:dropdownlist id="ddNumberType1" runat="server"></asp:dropdownlist>
																	<TD style="HEIGHT: 17px"><asp:label id="lbDebtorIDNumber3" runat="server">IDNumber 2</asp:label><BR>
																		<asp:textbox id="tbIDNumber2" runat="server" EnableViewState="False"></asp:textbox></TD>
																	<TD style="HEIGHT: 17px"><asp:label id="lbIDNumberType2" runat="server">Type</asp:label><BR>
																		<asp:dropdownlist id="ddNumberType2" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR class="input">
																	<TD style="WIDTH: 135px; HEIGHT: 43px"><asp:label id="lbPhoneNumber1" runat="server">Phone number 1</asp:label><BR>
																		<asp:textbox id="tbPhoneNumber1" runat="server" EnableViewState="False"></asp:textbox>
																	<TD style="WIDTH: 225px; HEIGHT: 43px"><asp:label id="lbPhonNumber1Type" runat="server">Type</asp:label><BR>
																		<asp:dropdownlist id="ddPhoneNumber1Type" runat="server"></asp:dropdownlist>
																	<TD style="HEIGHT: 43px"><asp:label id="lbPhoneNumber2" runat="server">Phone number 2</asp:label><BR>
																		<asp:textbox id="tbPhoneNumber2" runat="server" EnableViewState="False"></asp:textbox></TD>
																	<TD style="HEIGHT: 43px"><asp:label id="lbPhonenumber2Type" runat="server">Type</asp:label><BR>
																		<asp:dropdownlist id="ddPhoneNumber2Type" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR class="input">
																	<TD style="WIDTH: 135px; HEIGHT: 18px"><asp:label id="lbPhoneNumber3" runat="server">Phone number 3</asp:label><BR>
																		<asp:textbox id="tbPhoneNumber3" runat="server" EnableViewState="False"></asp:textbox>
																	<TD style="WIDTH: 225px; HEIGHT: 18px"><asp:label id="lbPhoneNumber3Type" runat="server">Type</asp:label><BR>
																		<asp:dropdownlist id="ddPhoneNumber3Type" runat="server"></asp:dropdownlist>
																	<TD style="HEIGHT: 18px"><asp:label id="lbPhoneNumber4" runat="server">Phone number 4</asp:label><BR>
																		<asp:textbox id="tbPhoneNumber4" runat="server" EnableViewState="False"></asp:textbox></TD>
																	<TD style="HEIGHT: 18px"><asp:label id="lbPhoneNumber4Type" runat="server">Type</asp:label><BR>
																		<asp:dropdownlist id="ddPhoneNumber4Type" runat="server"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD colSpan="4" height="23"><asp:textbox id="tbPageID" runat="server" Height="9px" Visible="False"></asp:textbox></TD>
																</TR>
															</TABLE>
														</td>
													</tr>
												</TABLE>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table width="100%">
													<tr valign="top">
														<td colSpan="2"><asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary>
															<P><asp:label id="lbMsg" runat="server" Visible="False" ForeColor="Red">New user has been registered</asp:label></P>
														</td>
														<td align="right" colSpan="2">
															<asp:button id="btCancel" runat="server" CssClass="cancel_button" Text="cancel" CausesValidation="False"></asp:button><asp:button id="btRebDebtor" runat="server" CssClass="confirm_button" Text="submit"></asp:button>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
