using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CR.BLL;
using CR.Localization;
using CreditInfoGroup.new_user_controls;
using Logging.BLL;
using WebSupergoo.ABCpdf4;

using Cig.Framework.Base.Configuration;


namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for PaymentReport.
    /// </summary>
    public class PaymentReport : Page {
        private readonly CPIFactory cpiFactory = new CPIFactory();
        private readonly CRFactory crFactory = new CRFactory();
        private CultureInfo ci;
        private string culture;
        protected Literal liPaymentBehaviour;
        private ResourceManager rm;
        protected HtmlTableCell showControl;

        private void Page_Load(object sender, EventArgs e) {
            culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            string reportFormat = "HTML";
            if (culture.Equals(nativeCulture)) {}
            if (Session["ReportFormat"] != null) {
                reportFormat = Session["ReportFormat"].ToString();
            }
            try {
                int ciid = int.Parse(Session["PaymentBehaviourCIID"].ToString());

                if (ciid > 0) {
                    CompanyNaceCodeBLLC nace = cpiFactory.GetCompanyNaceCode(ciid);
                    if (nace != null) {
                        Control c = LoadControl("new_user_controls/PaymentBehaviourControl.ascx");
                        liPaymentBehaviour.Text = rm.GetString("txtPaymentBehaviour", ci);
                        //liPaymentBehaviour.Text += Session["PaymentBehaviourName"].ToString();

                        ((PaymentBehaviourControl) c).generateChart(ciid, nace);
                        showControl.Controls.Add(c);
                        showControl.Visible = true;

                        switch (reportFormat) {
                            case "HTML":
                                break;
                            case "PDF":
                                string fileDir = CigConfig.Configure("lookupsettings.TMPFilesLocation");
                                    //"C:/Inetpub/wwwroot/BulkSampleCSharp/files/"; // get 
                                string file = fileDir + Util.CreateUniqueString() + ".html";

                                var mygen = new HTMLGenerator();
                                Render(mygen.RenderHere);
                                mygen.WriteHTMLFile(file);

                                XSettings.License = CigConfig.Configure("lookupsettings.AbcPDFLicence");
                                var theDoc = new Doc();

                                var theID = theDoc.AddImageUrl("file:///" + file, true, 620, false);

                                while (true) {
                                    theDoc.FrameRect();
                                    if (theDoc.GetInfo(theID, "Truncated") != "1") {
                                        break;
                                    }
                                    theDoc.Page = theDoc.AddPage();
                                    theID = theDoc.AddImageToChain(theID);
                                }

                                for (int i = 1; i <= theDoc.PageCount; i++) {
                                    theDoc.PageNumber = i;
                                    theDoc.Flatten();
                                }

                                Response.ContentType = "application/pdf";
                                theDoc.Save(Response.OutputStream);
                                theDoc.Clear();
                                Response.Flush();
                                Response.End();
                                break;
                        }

                        logUsage(CigConfig.Configure("lookupsettings.PaymentBehaviourID"));
                    }
                }
            } catch (Exception exc) {
                Logger.WriteToLog(
                    "Error displaying payment behaviour data for CIID " + Session["PaymentBehaviourCIID"], exc, true);
            }
        }

        protected void logUsage(string usageType) {
            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            if (usageType != null) {
                crFactory.InsertInto_np_Usage(userCreditInfoID, int.Parse(usageType), "", 1, ipAddress, userID);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ID = "PaymentRep";
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }

    /// <summary>
    /// Html generator
    /// </summary>
    public class HTMLGenerator {
        private readonly StringWriter html;
        private readonly MyHtmlTextWriter htmlWriter;
        private readonly string newUrl;
        // override the HtmlTextWriter to reach the constructor
        // the constructor in the base class is protected 
        // constructor initializes stringwriter and htmlwriter based on that
        // initialize Url 
        internal HTMLGenerator() {
            html = new StringWriter();
            htmlWriter = new MyHtmlTextWriter(html);
            newUrl = HttpContext.Current.Request.Url.AbsolutePath;
            newUrl = newUrl.Replace(".aspx", ".htm");
        }

        internal HtmlTextWriter RenderHere { get { return htmlWriter; } }

        /// <summary>
        /// Writes html file
        /// </summary>
        /// <param name="fullFilePath">Full File Path</param>
        internal void WriteHTMLFile(string fullFilePath) {
            // Stringreader reads output rendered by asp.net
            // Stringwriter writes html output file
            string text1 = string.Empty;

            bool removeApplicationPath;
            try {
                removeApplicationPath = bool.Parse(CigConfig.Configure("lookupsettings.RemoveApplicationPath"));
            } catch {
                removeApplicationPath = false;
            }

            if (HttpContext.Current != null && removeApplicationPath) {
                text1 = HttpContext.Current.Request.ApplicationPath;
            }

            text1 = text1 + "/WebCharts";
            // Quick fix for domains
            text1 = text1.Replace("//", "/");

            var sr = new StringReader(html.ToString().Replace(text1, "../../WebCharts"));
            var sw = new StringWriter();

            // Read from input
            string htmlLine = sr.ReadLine();
            while (htmlLine != null) {
                // Filter out ASp.net specific tags
                if (! ((htmlLine.IndexOf("<form") > 0) ||
                       (htmlLine.IndexOf("__VIEWSTATE") > 0) ||
                       (htmlLine.IndexOf("</form>") > 0))) {
                    sw.WriteLine(htmlLine);
                }
                //				if (! (
                //					(htmlLine.IndexOf("__VIEWSTATE") > 0)
                //					 ))
                //				{sw.WriteLine(htmlLine);}

                htmlLine = sr.ReadLine();
            }
            //	StreamReader fr = new StreamReader(HttpContext.Current.Server.MapPath("~/design/reportHeader.htm"));

            // If failes here, it means you don't have store/reports folder.
            // It's in sourcesafe.

            // Write contents stringwriter to html file
            var fs = new StreamWriter(fullFilePath, false, Encoding.UTF8);
            //fs.Write(fr.ReadToEnd());
            fs.Write(sw.ToString());
            //fs.Write("</body>");
            //fs.Write("</html>");
            fs.Close();
        }

        #region Nested type: MyHtmlTextWriter

        private class MyHtmlTextWriter : HtmlTextWriter {
            internal MyHtmlTextWriter(TextWriter tw) : base(tw) { }
        }

        #endregion
    }
}