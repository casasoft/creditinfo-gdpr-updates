using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using skstk.BLL;
using cb3;

namespace skstk.DAL {
    /// <summary>
    /// SubscribersDALC is used to get data from the several sources, both tables and views, regarding subscribers
    /// and their status in regard to skstk (Dept information system).
    /// </summary>
    /// <remarks>
    /// SubscriberDALC uses the following sources for data:
    /// <list type="bullet">
    /// <item>
    /// <description>au_Subscribers (table)</description>
    /// </item>
    /// <item>
    /// <description>skstk_SubscriberDetails (view)</description>
    /// </item>
    /// <item>
    /// <description>skstk_SubscriberInformation (view)</description>
    /// </item>
    /// </list>
    /// The views contain both native and english verison strings. The User Interface must manipulate what fields
    /// are shown and what conditions, if any, apply.
    /// </remarks>
    public class SubscribersDALC {

        /// <summary>
        /// Returns detailed subscriber information for all subscribers that are open and have equal or lower 
        /// access levels then the current user. Detailed information is based on the skstk_SubscriberDetials view.
        /// </summary>
        /// <remarks>
        /// The properties of the skstk_SubscriberDetails view are:
        /// <list type="bullet">
        /// <item>SubscriberID</item>
        /// <item>CreditInfoID</item>
        /// <item>IsOpen</item>
        /// <item>NameNative</item>
        /// <item>NameEN</item>
        /// <item>StreetNative</item>
        /// <item>StreetEN</item>
        /// <item>CityNative</item>
        /// <item>CityEN</item>
        /// <item>AccessLevel</item>
        /// <item>AccessDescriptionNative</item>
        /// <item>AccessDescriptionEN</item>
        /// <item>ServerSite</item>
        /// <item>LastErrorMsgCode</item>
        /// <item>ErrorMessageNative</item>
        /// <item>ErrorMessageEN</item>
        /// <item>skstkCreateDate</item>
        /// </list>
        /// </remarks>
        /// <param name="AccessLevel">Current user access level (inhereted from subscriber)</param>
        /// <returns>Datatable - A table containing information for all accessible subscribers</returns>
        public DataTable GetAccessibleSubscribers(int AccessLevel) {
            var myTable = new DataTable();
            OleDbCommand myCommand;

            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myConnection.Open();
                    myCommand =
                        new OleDbCommand(
                            "SELECT * FROM skstk_SubscriberDetails WHERE AccessLevel = ? AND IsOpen = 'True'",
                            myConnection);

                    // Set AccessLevel as input parameter for the command...
                    var myParameter = new OleDbParameter("AccessLevel", OleDbType.Integer)
                                      {
                                          Value = AccessLevel,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Make new adapter that uses the select command...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Fill DataSet with results and return it...
                    myAdapter.Fill(myTable);
                    return myTable;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    throw;
                }
            }
        }
    }
}