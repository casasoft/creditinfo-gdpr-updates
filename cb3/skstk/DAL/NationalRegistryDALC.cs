using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using skstk.BLL;
using cb3;

namespace skstk.DAL {
    /// <summary>
    /// NationalRegistryDALC is used to get data from the skstk_NationalRegistryCheck view that can be found in
    /// the CreditInfoGroup_IS database.
    /// </summary>
    /// <remarks>
    /// The skstk_NationalRegistryCheck view should contain the following fields:
    /// <list type="bullet">
    /// <item>
    /// <description>Kennitala (NationalID)</description>
    /// </item>
    /// <item>
    /// <description>Nafn (Name)</description>
    /// </item>
    /// /// <item>
    /// <description>Heimilisfang (Address)</description>
    /// </item>
    /// /// <item>
    /// <description>CreditInfoID (CreditInfoID)</description>
    /// </item>
    /// /// <item>
    /// <description>IsCompany (IsCompany)</description>
    /// </item>
    /// </list>
    /// The view is defined for Icelandic setup and uses the Icelandic national registry. It has to be set up for
    /// other counties if this class should be available to them.
    /// </remarks>
    public class NationalRegistryDALC {

        /// <summary>
        /// GetNationalRegistryEntity method returns one subject from National Registry, if it exists.
        /// </summary>
        /// <remarks>
        /// A DataRepeater can be used to display information returned by this method. Since the return value is a
        /// DataTable, it can be set as the DataSource for a DataRepeater, a DataList or a DataGrid. This method
        /// will never return more then one result embedded in the DataTable.
        /// </remarks>
        /// <param name="NationalID">National Registry ID that is unique for each listed entity.</param>
        /// <returns>DataTable - Unique Entity (maximum one row)</returns>
        public DataTable GetNationalRegistryEntity(string NationalID) {
            var myTable = new DataTable();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand(
                        "SELECT * FROM skstk_NationalRegistryCheck WHERE kennitala = ?", myConnection);

                    // Set NationalID as input parameter for the command...
                    var myParameter = new OleDbParameter("NationalID", OleDbType.Char)
                                      {
                                          Value = NationalID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Make new adapter that uses the select command...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Fill DataSet with results and return it...
                    myAdapter.Fill(myTable);
                    return myTable;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    throw;
                }
            }
        }
    }
}