using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using skstk.BLL;
using cb3;

namespace skstk.DAL {
    /// <summary>
    /// UsersDALC is used to get data from the several sources, both tables and views, regarding users
    /// and their status in regard to skstk (Dept information system).
    /// </summary>
    /// <remarks>
    /// UsersDALC uses the following sources for data:
    /// <list type="bullet">
    /// <item>
    /// <description>skstk_UserDetails (view)</description>
    /// </item>
    /// </list>
    /// The views might contain both native and english verison strings. The User Interface must manipulate what fields
    /// are shown and what conditions, if any, apply.
    /// </remarks>
    public class UsersDALC {


        /// <summary>
        /// Returns skstk (Dept information system) access rights for the current user. Access rights are represented
        /// as an integer. Low numbers represent lower access rights then high numbers. User inherit access right from
        /// their subscriber.
        /// </summary>
        /// <remarks>
        /// Access rights are structured in such a way that users that have a high access number can "see" Subscriber
        /// details for those subscribers that have the same access number or less. Thus a user that inherits access
        /// rights = 1000 can see subscribers with access rights equal to or less than 1000.
        /// </remarks>
        /// <param name="UserID">Current user ID (Can retrieve from session variable ["UserLoginID"]</param>
        /// <returns>Integer = Access rights. High number equals more access. Low number equals less access.</returns>
        public int GetUserAccessRights(int UserID) {
            int iRes;
            OleDbCommand myOleDbCommand;
            OleDbDataReader reader = null;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                try {
                    // Uses sktsk_UserDetails view to retrieve User access according to the given User ID (exact match)
                    myOleDbCommand =
                        new OleDbCommand(
                            "SELECT AccessLevel FROM skstk_UserDetails WHERE ID = '" + UserID + "'", myOleDbConn);

                    reader = myOleDbCommand.ExecuteReader();
                    reader.Read(); // Advance to the one and only row

                    // Return output parameters from returned data stream
                    iRes = reader.GetInt32(0);
                } catch (Exception ex) {
                    throw new Exception("Connecting to SQL server. <br> Ex: " + ex.Message);
                } finally {
                    // Close the reader if it is open...
                    if (reader != null) {
                        if (!reader.IsClosed) {
                            reader.Close();
                        }
                    }
                }
            }

            return iRes;
        }

        /// <summary>
        /// Returns detailed user information with subscriber info. Returned information
        /// is based on skstk_UserDetials and skstk_SubscriberInformation views which are joined
        /// in the view used, skstk_UserDetailsWithNamePlusSubscriberInformation.
        /// </summary>
        /// <remarks>
        /// The properties of the skstk_UserDetailsWithNamePlusSubscriberInformation view are:
        /// <list type="bullet">
        /// <item>id</item>
        /// <item>CreditInfoID</item>
        /// <item>UserName</item>
        /// <item>SubscriberID</item>
        /// <item>Email</item>
        /// <item>UserType</item>
        /// <item>HasWebServices</item>
        /// <item>IsOpen</item>
        /// <item>AccessLevel</item>
        /// <item>Site</item>
        /// <item>LastErrorState</item>
        /// <item>NameNative</item>
        /// <item>NameEN</item>
        /// <item>SubscriberNameNative</item>
        /// <item>SubscriberNameEN</item>
        /// <item>SubscriberStreetNative</item>
        /// <item>SubscriberStreetEN</item>
        /// <item>SubscriberCityNative</item>
        /// <item>SubscriberCityEN</item>
        /// </list>
        /// </remarks>
        /// <param name="UserID">Current user ID</param>
        /// <returns>Datatable - A table containing information for the current user</returns>
        public DataTable GetUserDetailsWithNamePlusSubscriberInformation(int UserID) {
            var myTable = new DataTable();
            OleDbCommand myCommand;

            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myConnection.Open();
                    myCommand =
                        new OleDbCommand(
                            "SELECT * FROM skstk_UserDetailsWithNamePlusSubscriberInformation WHERE id = ?",
                            myConnection);

                    // Set UserID as input parameter for the command...
                    var myParameter = new OleDbParameter("id", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Make new adapter that uses the select command...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Fill DataSet with results and return it...
                    myAdapter.Fill(myTable);
                    return myTable;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    throw;
                }
            }
        }
    }
}