using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using skstk.BLL;
using skstk.Localization;

namespace skstk {
    /// <summary>
    /// SkstkQueryRunAndResults class - SkstkQueryRunAndResults.aspx CodeBehind. Query and Result Web Form for skstk.
    /// </summary>
    /// <remarks>
    /// SkstkQueryRunAndResults form is the form that queries the skstk Server webservices. Each participant has one
    /// such service set up. The form also displays the result from the queries after forming the results in a readable
    /// and printable way. Functionality is:
    /// <list type="bullet">
    /// <item>Query skstkServer webservices</item>
    /// <description></description>
    /// <item>Form results in DataSet and display on screen</item>
    /// <description></description>
    /// <item>Print results on a local printer</item>
    /// <description></description>
    /// </list>
    /// </remarks>
    public class skstkQueryRunAndResults : Page {
        /// <summary>Static CultureInfo object
        /// <seealso cref="System.Globalization.CultureInfo"/>
        /// <seealso cref="System.Threading.Thread"/>
        /// <seealso cref="System.Threading.Thread.CurrentThread"/></summary>
        /// <remarks>Variable that holds the CurrentCulture for the CurrentThread. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		ci = Thread.CurrentThread.CurrentCulture;
        ///		}
        /// </code></remarks>
        public static CultureInfo ci;

        /// <summary>True if culture = "en-US"</summary>
        /// <remarks>True if culture = "en-US"</remarks>
        protected static bool EN;

        /// <summary>Initializes a new static instance of the BLL.skstkFactory class
        /// <seealso cref="skstk.BLL.skstkFactory"/></summary>
        /// <remarks>Static myFactory is used to access methods on the DAL and BLL lairs. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		DataTable myTable = new DataTable();
        ///		myTable = myFactory.GetNationalRegistryEntity(NationalID);
        ///		}
        /// </code></remarks>
        public static skstkFactory myFactory = new skstkFactory();

        /// <summary>Static ResourceManager object
        /// <seealso cref="System.Resources.ResourceManager"/>
        /// <seealso cref="skstk.Localization.CIResource"/></summary>
        /// <remarks>Used to hold the return value of the CIResource.CurrentManager property. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		rm = CIResource.CurrentManager;
        ///		}
        /// </code></remarks>
        public static ResourceManager rm;

        /// <summary>Pageheader label</summary>
        /// <remarks>Pageheader label</remarks>
        protected Label lblConfidential;

        /// <summary>Content label</summary>
        protected Label lblContent;

        /// <summary>Department information label</summary>
        protected Label lblDeptInformation;

        /// <summary>Upper content label</summary>
        protected Label lblUpperConent;

        /// <summary>Department information repeater</summary>
        protected Repeater repeatDeptInformation;

        /// <summary>DataRepeater - Display results from skstk_UserDetailsWithNamePlusSubscriberInformation</summary>
        /// <remarks>DataRepeater - Display results from skstk_UserDetailsWithNamePlusSubscriberInformation</remarks>
        protected Repeater repeatUserInfo;

        protected HtmlTable Table2;
        protected HtmlTable Table3;
        protected HtmlTable Table4;
        protected HtmlTable Table5;

        /// <summary>Called when page is loaded. Used to initialize the page and set default values</summary>
        /// <remarks>Sets rm and ci, static variables. Calls LocalizeText method.</remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void Page_Load(object sender, EventArgs e) {
            // For multilanguage control...
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            var culture = Thread.CurrentThread.CurrentCulture.Name;
            EN = culture.Equals("en-US");

            LocalizeText();

            if (IsPostBack) {
                return;
            }
            // Setja �ll upphafsgildi sem �arf a� s�kja beint � gagnagrunn (CreditInfoGroup_IS)
            // en ekki �au g�gn sem s�tt eru fr� vef�j�nustum...
            SetInitialData();
            QueryData();
        }

        /// <summary>Called each time the page is loaded. Localizes all displayed text</summary>
        /// <remarks>
        /// Uses the rm.GetString method to set language according to the current
        /// culture settings. Resource files must be available for each languge that can be set. Example:
        /// <code>
        ///		public void LocalizeText()
        ///		{
        ///			this.lblSkstk.Text = rm.GetString("lblSkstk",ci);
        ///		}
        /// </code>
        /// </remarks>
        private void LocalizeText() {
            // Variable used to replace Placeholder variables that are placed in the resource files
            // The placeholders can be identified by the "@" symbol in the resource file strings...

            lblConfidential.Text = rm.GetString("lblConfidential", ci);
            lblContent.Text = rm.GetString("lblContent", ci);
            lblDeptInformation.Text = rm.GetString("lblDeptInformation", ci);

            if (rm == null) {
                return;
            }
            var VariableReplaceString = rm.GetString("lblUpperConent", ci).Replace(
                "@DateTimeVar", DateTime.Now.ToLongDateString());
            lblUpperConent.Text = VariableReplaceString;
        }

        private void SetInitialData() {
            try {
                repeatUserInfo.DataSource =
                    myFactory.GetUserDetailsWithNamePlusSubscriberInformation(Convert.ToInt32(Session["UserLoginID"]));
                repeatUserInfo.DataBind();
            } catch {}
        }

        private void QueryData() {
            repeatDeptInformation.DataSource = Session["GetNationalRegistryEntity"];
            repeatDeptInformation.DataBind();
        }

        #region Web Form Designer generated code

        /// <summary>
        /// Event handler that is called when the page is initialized
        /// </summary>
        /// <param name="e">The event arguments</param>
        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}