<%@ Register TagPrefix="uc1" TagName="head" Src="../user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SecureSkstk" Src="user_controls/SecureSkstk.ascx" %>
<%@ Page language="c#" Codebehind="skstkMain.aspx.cs" AutoEventWireup="false" EnableEventValidation="false" Inherits="skstk.skstkMain" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Skuldastöğukerfi</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/cy_styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="popup.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="skstkMain" method="post" runat="server">
			<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="600" align="center" border="0">
				<TR>
					<TD><uc1:head id="Head1" runat="server"></uc1:head><uc1:secureskstk id="SecureSkstk1" runat="server"></uc1:secureskstk></TD>
				</TR>
				<TR>
					<TD class="pageheader"><asp:label id="lblSkstk" runat="server">Skuldastöğukerfi</asp:label></TD>
				</TR>
				<TR>
					<TD>
						<HR noShade SIZE="1">
						&nbsp;</TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD>
						<TABLE class="subject" id="tblQuery" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="120"><asp:label id="lblNAtionalID" runat="server"></asp:label></TD>
								<TD width="200"><asp:textbox id="txtID" runat="server" Width="160px" CssClass="rammi"></asp:textbox></TD>
								<TD><asp:button id="btnAuthenticate" runat="server" Width="80px" CssClass="button"></asp:button><asp:button id="btnQuery" runat="server" Width="80px" CssClass="button" Visible="False"></asp:button>&nbsp;
									<asp:button id="btnCancel" runat="server" Width="80px" CssClass="button" Visible="False"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD><asp:comparevalidator id="validateInteger" runat="server" CssClass="subject" ErrorMessage="CompareValidator"
							ControlToValidate="txtID" Operator="DataTypeCheck" Type="Integer"></asp:comparevalidator><br>
						<asp:rangevalidator id="validateRange" runat="server" CssClass="subject" ErrorMessage="RangeValidator"
							ControlToValidate="txtID" Type="Double" MaximumValue="9999999999" MinimumValue="0101000001"></asp:rangevalidator></TD>
				</TR>
				<TR>
					<TD class="sectionheader"><STRONG><font size="2"><asp:label id="lblNationalRegistryInfo" runat="server"></asp:label></font></STRONG></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblResults" runat="server"></asp:label><asp:repeater id="repeatResults" runat="server">
							<ItemTemplate>
								<table runat="server" id="tblRepeatResults" class="list" BORDER="0" CELLSPACING="0" CELLPADDING="3"
									width="100%">
									<tr>
										<td rowspan="3"></td>
										<td colspan="4" class="listhead">
											<b>
												<%#rm.GetString("repeatKT",ci)%>
												: </b>
											<%#DataBinder.Eval(Container.DataItem, "kennitala")%>
										</td>
									</tr>
									<tr>
										<td class="dark-row" valign="top"><b><%#rm.GetString("repeatName",ci)%></b><br>
											<%#DataBinder.Eval(Container.DataItem, "Nafn")%>
										</td>
										<td class="dark-row" valign="top"><b><%#rm.GetString("repeatAddress",ci)%></b><br>
											<%#DataBinder.Eval(Container.DataItem, "Heimilisfang")%>
										</td>
										<td class="dark-row" valign="top"><b><%#rm.GetString("repeatIsCompany",ci)%></b><br>
											<%#DataBinder.Eval(Container.DataItem, "IsCompany")%>
										</td>
									</tr>
								</table>
							</ItemTemplate>
						</asp:repeater></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD class="sectionheader"><STRONG><font size="2"><asp:label id="lblInformationPrioviders" runat="server"></asp:label></font></STRONG></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="dgProviders" runat="server" Width="100%" CssClass="rammi" AllowSorting="True"
							AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" BackColor="White"
							CellPadding="4" GridLines="Vertical" ForeColor="Black">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
							<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
							<ItemStyle BackColor="#F7F7DE"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
							<FooterStyle BackColor="#CCCC99"></FooterStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="SubscriberID" SortExpression="SubscriberID" ReadOnly="True"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="CreditInfoID" SortExpression="CreditInfoID"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="IsOpen" SortExpression="IsOpen"></asp:BoundColumn>
								<asp:BoundColumn DataField="NameNative" SortExpression="NameNative" HeaderText="Nafn"></asp:BoundColumn>
								<asp:BoundColumn DataField="NameEN" SortExpression="NameEN" HeaderText="Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="StreetNative" SortExpression="StreetNative" HeaderText="Heimilisf."></asp:BoundColumn>
								<asp:BoundColumn DataField="StreetEN" SortExpression="StreetEN" HeaderText="Address"></asp:BoundColumn>
								<asp:BoundColumn DataField="CityNative" SortExpression="CityNative" HeaderText="P&#243;stnr."></asp:BoundColumn>
								<asp:BoundColumn DataField="CityEN" SortExpression="CityEN" HeaderText="City"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="AccessLevel" SortExpression="AccessLevel"></asp:BoundColumn>
								<asp:BoundColumn DataField="AccessDescriptionNative" SortExpression="AccessDescriptionNative" HeaderText="Tegund a&#240;gengis"></asp:BoundColumn>
								<asp:BoundColumn DataField="AccessDescriptionEN" SortExpression="AccessDescriptionEN" HeaderText="Access type"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="ServerSite" SortExpression="ServerSite"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="LastErrorMsgCode" SortExpression="LastErrorMsgCode"></asp:BoundColumn>
								<asp:BoundColumn DataField="ErrorMessageNative" SortExpression="ErrorMessageNative" HeaderText="S&#237;&#240;ast &#254;ekkta sta&#240;a"></asp:BoundColumn>
								<asp:BoundColumn DataField="ErrorMessageEN" SortExpression="ErrorMessageEN" HeaderText="Last known status"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="skstkCreateDate" SortExpression="skstkCreateDate"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD><br>
						<asp:button id="btnPrint" runat="server" CssClass="button" ToolTip="Print partisipant list"
							Text="Print"></asp:button></TD>
				</TR>
				<TR>
					<TD><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
