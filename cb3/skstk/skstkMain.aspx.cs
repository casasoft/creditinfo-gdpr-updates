using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using skstk.BLL;
using skstk.Localization;

namespace skstk {
    /// <summary>
    /// skstkMain class - skstkMain.aspx CodeBehind. Default Web Form for skstk.
    /// </summary>
    /// <remarks>
    /// skstkMain form is the main form for the skstk application. It should be used for NationalID checks, Dept
    /// information queries and status information for other subscribers that can be seen by the logged in
    /// subscriber. Functionality is:
    /// <list type="bullet">
    /// <item>National Identification ID validation</item>
    /// <description>Validators used to check user input before National Registry is searched</description>
    /// <item>National Registry Search</item>
    /// <description>User input is used to search through the National Registry</description>
    /// <item>Dept information query</item>
    /// <description>If National Registry Seach is positive, the user can proceed to enquire about dept info</description>
    /// <item>Subscriber details list</item>
    /// <description>List of subscribers that are queried for dept information by the user. Contains status info.</description>
    /// </list>
    /// </remarks>
    public class skstkMain : Page {
        /// <summary>Static CultureInfo object
        /// <seealso cref="System.Globalization.CultureInfo"/>
        /// <seealso cref="System.Threading.Thread"/>
        /// <seealso cref="System.Threading.Thread.CurrentThread"/></summary>
        /// <remarks>Variable that holds the CurrentCulture for the CurrentThread. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		ci = Thread.CurrentThread.CurrentCulture;
        ///		}
        /// </code></remarks>
        public static CultureInfo ci;

        /// <summary>True if culture = "en-US"</summary>
        /// <remarks>True if culture = "en-US"</remarks>
        private static bool EN;

        /// <summary>Initializes a new static instance of the BLL.skstkFactory class
        /// <seealso cref="skstk.BLL.skstkFactory"/></summary>
        /// <remarks>Static myFactory is used to access methods on the DAL and BLL lairs. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		DataTable myTable = new DataTable();
        ///		myTable = myFactory.GetNationalRegistryEntity(NationalID);
        ///		}
        /// </code></remarks>
        public static skstkFactory myFactory = new skstkFactory();

        /// <summary>Initializes a new static instance of the BLL.HelpFunctions class
        /// <seealso cref="skstk.BLL.HelpFunctions"/></summary>
        /// <remarks>Static HelpFunctions class is used to access methods that are designed to help the
        /// system in regard to flexibility and usability. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		string Subject = "Mail sent by the HelpFunctions Class via smtp";
        ///		string Message = "This is the message";
        ///		myHelpFunctions.SendMail(Message, Subject);
        ///		}
        /// </code></remarks>
        public static HelpFunctions myHelpFunctions = new HelpFunctions();

        /// <summary>Static ResourceManager object
        /// <seealso cref="System.Resources.ResourceManager"/>
        /// <seealso cref="skstk.Localization.CIResource"/></summary>
        /// <remarks>Used to hold the return value of the CIResource.CurrentManager property. Example:
        /// <code>
        ///		private void Page_Load(object sender, System.EventArgs e)
        ///		{
        ///		rm = CIResource.CurrentManager;
        ///		}
        /// </code></remarks>
        public static ResourceManager rm;

        /// <summary>Triggers National Registry Search</summary>
        /// <remarks>Triggers National Registry Search</remarks>
        protected Button btnAuthenticate;

        /// <summary>Triggers cancelation of Dept query</summary>
        /// <remarks>Triggers cancelation of Dept query</remarks>
        protected Button btnCancel;

        /// <summary>Print button</summary>
        protected Button btnPrint;

        /// <summary>Triggers Dept Information Query</summary>
        /// <remarks>Triggers Dept Information Query</remarks>
        protected Button btnQuery;

        /// <summary>DataGrid for subscribers that the user can "see"</summary>
        /// <remarks>DataGrid for subscribers that the user can "see"</remarks>
        protected DataGrid dgProviders;

        /// <summary>Header label</summary>
        /// <remarks>Header label</remarks>
        protected Label lblInformationPrioviders;

        /// <summary>Field explanation label</summary>
        /// <remarks>Field explanation label for txtID texbox</remarks>
        protected Label lblNAtionalID;

        /// <summary>Header label</summary>
        /// <remarks>Header label</remarks>
        protected Label lblNationalRegistryInfo;

        /// <summary>Message if National Registry query returns null</summary>
        /// <remarks>Message if National Registry query returns null</remarks>
        protected Label lblResults;

        /// <summary>Pageheader label</summary>
        /// <remarks>Pageheader label</remarks>
        protected Label lblSkstk;

        /// <summary>DataRepeater - Display results from National Registry</summary>
        /// <remarks>DataRepeater - Display results from National Registry</remarks>
        protected Repeater repeatResults;

        /// <summary>Table that consumes the tblRepeatResults variable</summary>
        /// <remarks>Table that consumes the tblRepeatResults variable</remarks>
        protected Repeater tblRepeatResults;

        /// <summary>Textbox for National ID input</summary>
        /// <remarks>Textbox for National ID input</remarks>
        protected TextBox txtID;

        /// <summary>Integer validator for National ID textbox</summary>
        /// <remarks>Integer validator for National ID textbox</remarks>
        protected CompareValidator validateInteger;

        /// <summary>Range validator for National ID textbox</summary>
        /// <remarks>Range validator for National ID textbox</remarks>
        protected RangeValidator validateRange;

        /// <summary>Called when page is loaded. Used to initialize the page and set default values</summary>
        /// <remarks>Sets rm and ci, static variables. Calls LocalizeText method.</remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "600";
            // For multilanguage control...
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            string culture = Thread.CurrentThread.CurrentCulture.Name;

            EN = culture.Equals("en-US");

            if (!IsPostBack) {
                GetUserAccess();
                GetSubscriberDetails();
            }

            LocalizeText();
            SetColumnsByCulture();
        }

        /// <summary>Called each time the page is loaded. Localizes all displayed text</summary>
        /// <remarks>
        /// Uses the rm.GetString method to set language according to the current
        /// culture settings. Resource files must be available for each languge that can be set. Example:
        /// <code>
        ///		public void LocalizeText()
        ///		{
        ///			this.lblSkstk.Text = rm.GetString("lblSkstk",ci);
        ///		}
        /// </code>
        /// </remarks>
        public void LocalizeText() {
            lblSkstk.Text = rm.GetString("lblSkstk", ci);
            lblNAtionalID.Text = rm.GetString("lblNAtionalID", ci);
            btnAuthenticate.Text = rm.GetString("btnAuthenticate", ci);
            btnQuery.Text = rm.GetString("btnQuery", ci);
            btnCancel.Text = rm.GetString("btnCancel", ci);
            lblNationalRegistryInfo.Text = rm.GetString("lblNationalRegistryInfo", ci);
            lblInformationPrioviders.Text = rm.GetString("lblInformationPrioviders", ci);
            validateInteger.Text = rm.GetString("validateInteger", ci);
            validateRange.Text = rm.GetString("validateRange", ci);
        }

        /// <summary>
        /// Authenticate user National ID input
        /// </summary>
        /// <remarks>
        /// Uses BLL.skstkFactory to access the DAL. If entity it found then the user can proceed to get dept
        /// information for the entity returned. Otherwise the user will have to try to authenticate a different ID.
        /// </remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnAuthenticate_Click(object sender, EventArgs e) {
            if (txtID.Text == "") {
                return;
            }

            DataTable myTable = null;

            try {
                myTable = myFactory.GetNationalRegistryEntity(txtID.Text);
            } catch (Exception err) {
                // Display error message if data connection failed. Send mail message
                lblResults.Text = rm.GetString("lblResults_Error", ci);
                myHelpFunctions.SendMail(
                    Session["UserLoginID"] + " " + err.Message,
                    "CreditInfo_IS: skstkMain.btnAuthenticate_click, db connection");
            }

            // If results were found...
            if (myTable == null) {
                return;
            }
            if (myTable.Rows.Count > 0) {
                // Hide National Registry search button and display dept info and cacel button...
                btnAuthenticate.Visible = false;
                btnQuery.Visible = true;
                btnCancel.Visible = true;
                txtID.ReadOnly = true;
                txtID.BackColor = Color.Gray;

                // No message...
                lblResults.Text = "";

                // Set repeater
                repeatResults.DataSource = myTable;
                // Throw to session. This DataTable will also be used when results for the query subject
                // are displayed.
                Session["GetNationalRegistryEntity"] = myTable;
                repeatResults.DataBind();
            } else {
                lblResults.Text = rm.GetString("lblResults_NoResults", ci);
            }
        }

        /// <summary>
        /// Transfers the user to the Query run and results page.
        /// </summary>
        /// <remarks>
        /// Query Run and Results page calls the skstkClient WebService. Call is made with a username and a password. 
        /// Both strings are encoded. Encoding key is also sent. Information sent about skstkServer webservices to 
        /// be queried.
        /// </remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnQuery_Click(object sender, EventArgs e) {
            const string myScript = "<script language=Javascript>CreateWnd('skstkQueryRunAndResults.aspx', 800, 800, true);</script>";
            Page.RegisterClientScriptBlock("skstkQuery", myScript);
        }

        /// <summary>
        /// Cancel query request - allows the user to input (try) another national ID.
        /// </summary>
        /// <remarks>
        /// Resets the form. Unlocks the txtID textbox and sets it's backroundcolor to white. Shows the
        /// Authenticate button and hides itself along with the query button. Clears the txtID textbox.
        /// </remarks>
        /// <param name="sender">object</param>
        /// <param name="e">System.EventArgs</param>
        private void btnCancel_Click(object sender, EventArgs e) {
            btnAuthenticate.Visible = true;
            btnQuery.Visible = false;
            btnCancel.Visible = false;
            txtID.ReadOnly = false;
            txtID.BackColor = Color.White;
            txtID.Text = "";

            repeatResults.DataSource = null;
            repeatResults.DataBind();
        }

        /// <summary>
        /// Gets user access rights to other skstk agents according to inherited access rights from the subscriber
        /// that the current user belongs to.
        /// </summary>
        /// <example>
        /// <code>
        /// Session["skstkAccessLevel"] = myFactory.GetUserAccessRights((int)Session["UserLoginID"]);
        /// </code>
        /// </example>
        /// <remarks>
        /// Access rights determine who the user can "ask" for dept information. Access level is returned as an integer
        /// from the method GetUserAccessRights which is called through skstkFactory. Access level returned is stored
        /// in Session["skstkAccessLevel"].
        /// <seealso cref="skstk.BLL.skstkFactory.GetUserAccessRights"/>
        /// </remarks>
        private void GetUserAccess() { Session["skstkAccessLevel"] = myFactory.GetUserAccessRights(Convert.ToInt32(Session["UserLoginID"])); }

        /// <summary>
        /// Sets a DataGrid control (dgProviders) datasource = (DataTable)accessible subscribers according to
        /// the current user's acces level.
        /// </summary>
        /// <example>
        /// The following codeblock shows how the method works (actual code):
        /// <code>
        /// DataTable myTable = myFactory.GetAccessibleSubscribers((int)Session["skstkAccessLevel"]);
        /// dgProviders.DataSource = myTable;
        /// dgProviders.DataBind();
        /// Session["skstkSubscriberList"] = myTable;
        /// myTable.Dispose();
        /// </code>
        /// </example>
        /// <remarks>
        /// Subscriber information is retrieved from the skstk_SubscriberDetails view that should reside
        /// in the system database. For further information see:
        /// <see cref="skstk.DAL.SubscribersDALC"/> 
        /// <seealso cref="skstk.BLL.skstkFactory.GetAccessibleSubscribers"/>
        /// </remarks>
        private void GetSubscriberDetails() {
            try {
                DataTable myTable = myFactory.GetAccessibleSubscribers((int) Session["skstkAccessLevel"]);
                dgProviders.DataSource = myTable;
                dgProviders.DataBind();
                Session["skstkSubscriberList"] = myTable;
                myTable.Dispose();
            } catch (Exception e) {
                // Display error message if data connection failed. Error message is red. Send mail message
                lblInformationPrioviders.Text = rm.GetString("lblInformationPrioviders_Error", ci);
                lblInformationPrioviders.ForeColor = Color.Red;
                myHelpFunctions.SendMail(
                    Session["UserLoginID"] + " " + e.Message,
                    "CreditInfo_IS: skstkMain.btnAuthenticate_click, db connection");
            }
        }

        /// <summary>Sets column visibility for the dgProviders DataGrid. Visibility is decided by current culture</summary>
        /// <remarks>
        /// Sets column visibility for the dgProviders DataGrid. Visibility is decided by current culture.
        /// <see cref="skstk.skstkMain.EN"/>
        /// <see cref="skstk.skstkMain.dgProviders"/>
        /// </remarks>
        private void SetColumnsByCulture() {
            if (EN) {
                dgProviders.Columns[3].Visible = false;
                dgProviders.Columns[4].Visible = true;
                dgProviders.Columns[5].Visible = false;
                dgProviders.Columns[6].Visible = true;
                dgProviders.Columns[7].Visible = false;
                dgProviders.Columns[8].Visible = true;
                dgProviders.Columns[10].Visible = false;
                dgProviders.Columns[11].Visible = true;
                dgProviders.Columns[14].Visible = false;
                dgProviders.Columns[15].Visible = true;
            } else {
                dgProviders.Columns[3].Visible = true;
                dgProviders.Columns[4].Visible = false;
                dgProviders.Columns[5].Visible = true;
                dgProviders.Columns[6].Visible = false;
                dgProviders.Columns[7].Visible = true;
                dgProviders.Columns[8].Visible = false;
                dgProviders.Columns[10].Visible = true;
                dgProviders.Columns[11].Visible = false;
                dgProviders.Columns[14].Visible = true;
                dgProviders.Columns[15].Visible = false;

                SetColumnsHeaderByCulture();
            }
        }

        /// <summary>Empty - Headers set in code (on DataGrid object)</summary>
        /// <remarks>Empty - Headers set in code (on DataGrid object)</remarks>
        private static void SetColumnsHeaderByCulture() { }

        private void dgProviders_SortCommand(object source, DataGridSortCommandEventArgs e) {
            var myTable = (DataTable) Session["skstkSubscriberList"];
            var myView = myTable.DefaultView;

            myView.Sort = e.SortExpression;
            dgProviders.DataSource = myView;
            dgProviders.DataBind();
        }

        #region Web Form Designer generated code

        /// <summary>
        /// OnInit called when the page is initialized.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnAuthenticate.Click += new System.EventHandler(this.btnAuthenticate_Click);
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.dgProviders.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgProviders_SortCommand);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}