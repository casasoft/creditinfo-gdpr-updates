using System;
using System.Web.UI;

namespace skstk.user_controls {
    /// <summary>
    /// SecureSkstk class - skstkMain.ascx CodeBehind. Security User Control for skstk project.
    /// </summary>
    /// <example>
    /// This User control is used by declaring it in the HTML part of each web form that resides in the
    /// skstk assembly.
    /// </example>
    /// <remarks>
    /// Secures the web forms of the skstk assembly by cheking the roles for the current user. The
    /// roles decide which products the user can access (according to the CreditInfoGroup product database).
    /// Roles for the CreditInfoGroup_IS system are defined as:
    /// <list type="bullet">
    /// <item>Role 100:		Administration</item>
    /// <item>Role 200:		Negative Payments</item>
    /// <item>Role 300:		Credit Watch</item>
    /// <item>Role 400:		Claim search</item>
    /// <item>Role 500:		Development</item>
    /// <item>Role 600:		Dept Information System (skstk)</item>
    /// </list>
    /// </remarks>
    public class SecureSkstk : UserControl {
        private void Page_Load(object sender, EventArgs e) {
            /*
			Role 100:		Administration
			Role 200:		Negative Payments
			Role 300:		Credit Watch
			Role 400:		Claim search
			Role 500:		Development
			Role 600:		Dept Information System (skstk)
			*/

            if (!(Context.User.IsInRole("100") || Context.User.IsInRole("600"))) {
                Server.Transfer("../NoAuthorization.aspx");
            }
        }

        #region Web Form Designer generated code

        /// <summary>
        /// Event handler that is called when the control is initialized
        /// </summary>
        /// <param name="e">The event arguments</param>
        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}