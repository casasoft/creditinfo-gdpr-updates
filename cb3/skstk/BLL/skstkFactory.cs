using System.Data;
using skstk.DAL;

namespace skstk.BLL {
    /// <summary>Factory classes are used to access all "public" methods found on the Data Access Lair.</summary>
    /// <remarks>
    /// The Factory class is also used to tie together data and functionality (DAL and BLL).
    /// Most of the methods that are defined in the factory class are used to encapsulate DAL functions. These
    /// methods very seldom add any new functionality or feature. When that happens the method description should
    /// mention this.
    /// <para>
    /// The Factory class can also be used to overload method names, even for methods that truly reside
    /// in other classes. It represents a solid front for the user and closes down any direct access to the DAL.
    /// </para>
    /// </remarks>
    public class skstkFactory {
        /// <summary>
        /// Instance of NationalRegistryDALC class. It is used to connect to a view that describes the
        /// National Registry.
        /// <seealso cref="skstk.DAL.NationalRegistryDALC"/>
        /// </summary>
        /// <remarks>Factory objects access the DAL through instanciated objects of DAL classes.</remarks>
        private readonly NationalRegistryDALC _myNationalRegistryDALC;

        /// <summary>
        /// Instance of SubscribersDALC class. It is used to connect to Subscriber information that is valid and 
        /// relevant to the skstk system (Dept Information System).
        /// <seealso cref="skstk.DAL.SubscribersDALC"/>
        /// </summary>
        /// <remarks>Factory objects access the DAL through instanciated objects of DAL classes.</remarks>
        private readonly SubscribersDALC _mySubscribersDALC;

        /// <summary>
        /// Instance of UsersDALC class. It is used to connect to user information that is valid and relevant
        /// to the skstk system (Dept Information System).
        /// <seealso cref="skstk.DAL.UsersDALC"/>
        /// </summary>
        /// <remarks>Factory objects access the DAL through instanciated objects of DAL classes.</remarks>
        private readonly UsersDALC _myUsersDALC;

        /// <summary>
        /// Default Constructor logic - Instanciate DAL and BLL classes for Factory access.
        /// </summary>
        /// <remarks>
        /// skstkFactory objects create the following DAL and BLL ojects:
        /// <list type="bullet">
        /// <item>_myNationalRegistyDALC
        /// <description>Instance of NationalRegistryDALC</description>
        /// </item>
        /// <item>_myUsersDALC
        /// <description>Instance of UsersDALC</description>
        /// </item>
        /// <item>_mySubscribersDALC
        /// <description>Instance of SubscribersDALC</description>
        /// </item>
        /// </list>
        /// </remarks>
        public skstkFactory() {
            _myNationalRegistryDALC = new NationalRegistryDALC();
            _myUsersDALC = new UsersDALC();
            _mySubscribersDALC = new SubscribersDALC();
        }

        /// <summary>
        /// NationalRegistryDALC - No added functionality.
        /// <see cref="skstk.DAL.NationalRegistryDALC.GetNationalRegistryEntity"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.skstkFactory mySkstkFactory = new BLL.skstkFactory();
        ///		string UniqueID = "1234567890";
        ///		DataTable myTable = new DataTable();
        ///		myTable = mySkstkFactory.GetNationalRegistryEntity(UniqueID);
        /// </code>
        /// <seealso cref="skstk.DAL.NationalRegistryDALC.GetNationalRegistryEntity"/>
        /// </remarks>
        /// <param name="NationalID">Unique National ID</param>
        /// <returns>Datatable - one record only (unique)</returns>
        public DataTable GetNationalRegistryEntity(string NationalID) { return _myNationalRegistryDALC.GetNationalRegistryEntity(NationalID); }

        /// <summary>
        /// UsersDALC - No added functionality.
        /// <see cref="skstk.DAL.UsersDALC.GetUserAccessRights"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.skstkFactory mySkstkFactory = new BLL.skstkFactory();
        ///		int UserID = (int)Session["UserLoginID"];
        ///		int AccessRights = mySkstkFactory.GetUserAccessRights(UserID);
        /// </code>
        /// <seealso cref="skstk.DAL.UsersDALC.GetUserAccessRights"/>
        /// </remarks>
        /// <param name="UserID">Current user ID (Can retrieve from session variable ["UserLoginID"]</param>
        /// <returns>Integer = Access rights. High number equals more access. Low number equals less access.</returns>
        public int GetUserAccessRights(int UserID) { return _myUsersDALC.GetUserAccessRights(UserID); }

        /// <summary>
        /// UsersDALC - No added functionality.
        /// <see cref="skstk.DAL.UsersDALC.GetUserDetailsWithNamePlusSubscriberInformation"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.skstkFactory mySkstkFactory = new BLL.skstkFactory();
        ///		int UserID = (int)Session["UserLoginID"];
        ///		DataTable UserDetails = mySkstkFactory.GetUserDetailsWithNamePlusSubscriberInformation(UserID);
        /// </code>
        /// <seealso cref="skstk.DAL.UsersDALC.GetUserDetailsWithNamePlusSubscriberInformation"/>
        /// </remarks>
        /// <param name="UserID">Current user ID (Can retrieve from session variable ["UserLoginID"]</param>
        /// <returns>Datatable - A table containing information for the current user</returns>
        public DataTable GetUserDetailsWithNamePlusSubscriberInformation(int UserID) { return _myUsersDALC.GetUserDetailsWithNamePlusSubscriberInformation(UserID); }

        /// <summary>
        /// SubscribersDALC - No added functionality.
        /// <see cref="skstk.DAL.SubscribersDALC.GetAccessibleSubscribers"/>
        /// </summary>
        /// <remarks>
        /// Example:
        /// <code>
        ///		BLL.skstkFactory mySkstkFactory = new BLL.skstkFactory();
        ///		int AccessLevel = (int)Session["skstkAccessLevel"];
        ///		DataTable myTable = new DataTable();
        ///		myTable = mySkstkFactory.GetAccessableSubscribers(AccessLevel);
        /// </code>
        /// <seealso cref="skstk.DAL.SubscribersDALC.GetAccessibleSubscribers"/>
        /// </remarks>
        /// <param name="AccessLevel">Current user access level (inhereted from subscriber)</param>
        /// <returns>Datatable - A table containing information for all accessible subscribers</returns>
        public DataTable GetAccessibleSubscribers(int AccessLevel) { return _mySubscribersDALC.GetAccessibleSubscribers(AccessLevel); }
    }
}