using System;
using System.IO;

namespace skstk.BLL {
    /// <summary>
    /// Logger Class is used to log exceptions to a file.
    /// </summary>
    /// <remarks>
    /// The file exists on the local computer that caused the exception. Two kinds of files are available. They
    /// both reside in the TMP folder of the local machine.
    /// <list type="bullet">
    /// <item>Error file</item>
    /// <description>Error file is used to log system errors</description>
    /// <item>Debug file</item>
    /// <description>Debuf file is used for debugging purposes only</description>
    /// </list>
    /// </remarks>
    public class Logger {
        /// <summary>
        /// debugFilePath uses a System.Environment path reference to build a for the debug.txt file.
        /// </summary>
        /// <remarks>
        /// Example: C:\Documents and Settings\IS-623B\ASPNET\Local Settings\Temp\debug.txt
        /// </remarks>
        private static readonly string debugFilePath = Environment.GetEnvironmentVariable("TMP") + @"\debug.txt";

        /// <summary>
        /// errorFilePath uses a System.Environment path reference to build a for the error.txt file.
        /// </summary>
        /// <remarks>
        /// Example: C:\Documents and Settings\IS-623B\ASPNET\Local Settings\Temp\error.txt
        /// </remarks>
        private static readonly string errorFilePath = Environment.GetEnvironmentVariable("TMP") + @"\error.txt";

        /// <summary>
        /// Method is used to write to logfile. Logfiles are stored locally on the machine that caused the
        /// method to be executed.
        /// <seealso cref="skstk.BLL.Logger.errorFilePath"/>
        /// <seealso cref="skstk.BLL.Logger.debugFilePath"/>
        /// </summary>
        /// <example> The following example shows how to call the WriteToLog method from within a 
        /// "try - catch" statement.
        /// <code>
        ///		catch(Exception e)
        ///		{
        ///			BLL.Logger.WriteToLog(e.Message, true);
        ///			return null;
        ///		}
        /// </code>
        /// </example>
        /// <remarks>
        /// This method can also be used for debugging purposes where the intent is to run the
        /// system for some period of time and review the log files with intervals while the system runs.
        /// <para>The logged message includes a DateTime stamp.</para>
        /// </remarks>
        /// <param name="input">The string that is logged to file.</param>
        /// <param name="error">True = ErrorFile, False = DebugFile</param>
        public static void WriteToLog(string input, bool error) {
            var filePath = error ? errorFilePath : debugFilePath;

            var logFile = new FileInfo(filePath);

            if (logFile.Exists) {
                if (logFile.Length >= 100000) {
                    File.Delete(filePath);
                }
            }

            var fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            var w = new StreamWriter(fs);
            w.BaseStream.Seek(0, SeekOrigin.End);

            w.Write("\nLog Entry : ");
            w.Write(
                "{0} {1} \n\n",
                DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());

            w.Write(input + "\n");
            w.Write("------------------------------------\n");

            w.Flush();

            w.Close();
        }
    }
}