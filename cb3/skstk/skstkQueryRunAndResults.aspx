<%@ Page language="c#" Codebehind="skstkQueryRunAndResults.aspx.cs" AutoEventWireup="false" EnableEventValidation="false" Inherits="skstk.skstkQueryRunAndResults" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>skstkQueryRunAndResults</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../styles/cy_styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMainQuery" cellSpacing="0" cellPadding="0" width="600" align="center" border="0">
				<tr>
					<td style="HEIGHT: 60px" vAlign="top"><IMG src="../img/logo.gif"></td>
				</tr>
				<TR>
					<TD class="dark-row" align="center"><asp:label id="lblConfidential" runat="server" ForeColor="Red" Font-Bold="True"></asp:label></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:repeater id="repeatUserInfo" runat="server">
							<ItemTemplate>
								<table runat="server" id="tblRepeatUserInfo" class="subject" BORDER="0" CELLSPACING="0"
									CELLPADDING="0" width="100%">
									<tr>
										<td colspan="2">
											<!--if setning s�r um a� meta hvort birta eigi �slensk heiti e�a ensk-->
											<%if (EN)%>
											<%#DataBinder.Eval(Container.DataItem, "SubscriberNameEN")%>
											<%else%>
											<%#DataBinder.Eval(Container.DataItem, "SubscriberNameNative")%>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<!--if setning s�r um a� meta hvort birta eigi �slensk heiti e�a ensk-->
											<%if (EN)%>
											<%#DataBinder.Eval(Container.DataItem, "NameEN")%>
											<%else%>
											<%#DataBinder.Eval(Container.DataItem, "NameNative")%>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<!--if setning s�r um a� meta hvort birta eigi �slensk heiti e�a ensk-->
											<%if (EN)%>
											<%#DataBinder.Eval(Container.DataItem, "SubscriberStreetEN")%>
											<%else%>
											<%#DataBinder.Eval(Container.DataItem, "SubscriberStreetNative")%>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<!--if setning s�r um a� meta hvort birta eigi �slensk heiti e�a ensk-->
											<%if (EN)%>
											<%#DataBinder.Eval(Container.DataItem, "SubscriberCityEN")%>
											<%else%>
											<%#DataBinder.Eval(Container.DataItem, "SubscriberCityNative")%>
										</td>
									</tr>
									<tr>
										<td colspan="2" align="right">
											<%#rm.GetString("queryRepeatPlace",ci) + ", " + DateTime.Now.ToLongDateString()%>
										</td>
									</tr>
								</table>
							</ItemTemplate>
						</asp:repeater></TD>
				<TR>
					<TD><br>
						<br>
					</TD>
				</TR>
				<TR>
					<TD class="subject" style="HEIGHT: 51px"><asp:label id="lblContent" runat="server" Font-Bold="True"></asp:label>&nbsp;
						<asp:label id="lblDeptInformation" runat="server"></asp:label>
						<p></p>
						<asp:label id="lblUpperConent" runat="server"></asp:label>
						<p></p>
					</TD>
				</TR>
				<TR>
					<TD><asp:repeater id="repeatDeptInformation" runat="server">
							<ItemTemplate>
								<table runat="server" id="Table1" BORDER="0" CELLSPACING="0" CELLPADDING="0" width="100%">
									<tr>
										<td align="center" style="FONT-WEIGHT: bold">
											<%#DataBinder.Eval(Container.DataItem, "Nafn")%>
											, kt:&nbsp;
											<%#DataBinder.Eval(Container.DataItem, "Kennitala")%>
										</td>
									</tr>
									<tr>
										<td align="center">
											<%#DataBinder.Eval(Container.DataItem, "Heimilisfang")%>
										</td>
									</tr>
								</table>
							</ItemTemplate>
						</asp:repeater></TD>
				</TR>
				<TR>
					<TD><br>
					</TD>
				</TR>
				<TR>
					<TD><STRONG>Global Bank of the World</STRONG></TD>
				</TR>
				<TR>
					<TD>
						<table id="Table2" cellSpacing="1" cellPadding="0" width="95%" align="right" border="0"
							runat="server">
							<tr>
								<td><FONT size="2"><EM>Type</EM></FONT></td>
								<td><FONT size="2"><EM>Issue date</EM></FONT></td>
								<td><FONT size="2"><EM>Loan period</EM></FONT></td>
								<td><FONT size="2"><EM>Role</EM></FONT></td>
								<td><FONT size="2"><EM>Amount Remaining</EM></FONT></td>
								<td><FONT size="2"><EM>Defaulted</EM></FONT></td>
							</tr>
							<tr>
								<td><FONT size="2">Bond</FONT></td>
								<td><FONT size="2">01.01.2004</FONT></td>
								<td><FONT size="2">24 months</FONT></td>
								<td><FONT size="2">Deptor</FONT></td>
								<td><FONT size="2">1.200.000 (ISK)</FONT></td>
								<td><FONT size="2">200.000 (ISK)</FONT></td>
							</tr>
							<tr>
								<td colSpan="6">
									<hr>
								</td>
							</tr>
							<tr>
								<td><FONT size="2"><EM>Type</EM></FONT></td>
								<td><FONT size="2"><EM>Issue date</EM></FONT></td>
								<td><FONT size="2"><EM>Loan period</EM></FONT></td>
								<td><FONT size="2"><EM>Role</EM></FONT></td>
								<td><FONT size="2"><EM>Amount Remaining</EM></FONT></td>
								<td><FONT size="2"><EM>Defaulted</EM></FONT></td>
							</tr>
							<tr>
								<td><FONT size="2">Check</FONT></td>
								<td><FONT size="2">01.01.2003</FONT></td>
								<td><FONT size="2">48 months</FONT></td>
								<td><FONT size="2">Voucher</FONT></td>
								<td><FONT size="2">5.250.000 (ISK)</FONT></td>
								<td><FONT size="2">3.000.000 (ISK)</FONT></td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD><br>
					</TD>
				</TR>
				<TR>
					<TD><STRONG>The Big Bank</STRONG></TD>
				</TR>
				<TR>
					<TD>
						<table id="Table3" cellSpacing="1" cellPadding="0" width="95%" align="right" border="0"
							runat="server">
							<tr>
								<td><FONT size="2"><EM>Type</EM></FONT></td>
								<td><FONT size="2"><EM>Issue date</EM></FONT></td>
								<td><FONT size="2"><EM>Loan period</EM></FONT></td>
								<td><FONT size="2"><EM>Role</EM></FONT></td>
								<td><FONT size="2"><EM>Amount Remaining</EM></FONT></td>
								<td><FONT size="2"><EM>Defaulted</EM></FONT></td>
							</tr>
							<tr>
								<td><FONT size="2">Bond</FONT></td>
								<td><FONT size="2">01.01.2004</FONT></td>
								<td><FONT size="2">24 months</FONT></td>
								<td><FONT size="2">Deptor</FONT></td>
								<td><FONT size="2">1.200.000 (ISK)</FONT></td>
								<td><FONT size="2">200.000 (ISK)</FONT></td>
							</tr>
							<tr>
								<td colSpan="6">
									<hr>
								</td>
							</tr>
							<tr>
								<td><FONT size="2"><EM>Type</EM></FONT></td>
								<td><FONT size="2"><EM>Issue date</EM></FONT></td>
								<td><FONT size="2"><EM>Loan period</EM></FONT></td>
								<td><FONT size="2"><EM>Role</EM></FONT></td>
								<td><FONT size="2"><EM>Amount Remaining</EM></FONT></td>
								<td><FONT size="2"><EM>Defaulted</EM></FONT></td>
							</tr>
							<tr>
								<td><FONT size="2">Check</FONT></td>
								<td><FONT size="2">01.01.2003</FONT></td>
								<td><FONT size="2">48 months</FONT></td>
								<td><FONT size="2">Voucher</FONT></td>
								<td><FONT size="2">5.250.000 (ISK)</FONT></td>
								<td><FONT size="2">3.000.000 (ISK)</FONT></td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD><br>
					</TD>
				</TR>
				<TR>
					<TD><STRONG>CreditInfo Defaulting Dept Database</STRONG></TD>
				</TR>
				<TR>
					<TD>
						<table runat="server" id="Table4" BORDER="0" CELLSPACING="1" CELLPADDING="0" width="95%"
							align="right">
							<tr>
								<td><FONT size="2">No records found.</FONT></td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD><br>
					</TD>
				</TR>
				<TR>
					<TD><STRONG>Loan Application History</STRONG></TD>
				</TR>
				<TR>
					<TD>
						<table runat="server" id="Table5" BORDER="0" CELLSPACING="1" CELLPADDING="0" width="95%"
							align="right">
							<tr>
								<td><FONT size="2"><EM>Application Date</EM></FONT></td>
								<td><FONT size="2"><EM>Response</EM></FONT></td>
								<td><FONT size="2"><EM>Reason</EM></FONT></td>
							</tr>
							<tr>
								<td><FONT size="2">1.1.2003</FONT></td>
								<td><FONT size="2">Accepted</FONT></td>
								<td><FONT size="2">Credit OK</FONT></td>
							</tr>
							<tr>
								<td colSpan="3">
									<hr>
								</td>
							</tr>
							<tr>
								<td><FONT size="2"><EM>Application Date</EM></FONT></td>
								<td><FONT size="2"><EM>Response</EM></FONT></td>
								<td><FONT size="2"><EM>Reason</EM></FONT></td>
							</tr>
							<tr>
								<td><FONT size="2">1.1.2004</FONT></td>
								<td><FONT size="2">Accepted</FONT></td>
								<td><FONT size="2">Credit OK</FONT></td>
							</tr>
							<tr>
								<td colSpan="3">
									<hr>
								</td>
							</tr>
							<tr>
								<td><FONT size="2"><EM>Application Date</EM></FONT></td>
								<td><FONT size="2"><EM>Response</EM></FONT></td>
								<td><FONT size="2"><EM>Reason</EM></FONT></td>
							</tr>
							<tr>
								<td><FONT size="2">1.6.2004</FONT></td>
								<td><FONT size="2">Rejected</FONT></td>
								<td><FONT size="2">Bad Dept Information</FONT></td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD><br><br><br>
					</TD>
				</TR>
				<TR>
					<TD class="subject" >
						<P>According to rules that regard the IDS system it is not allowed to make queries 
							about Dept Information history without the written consent of the query 
							subject. You might be asked, at any given time, to supply this consent to 
							verify the legality of this query.</P>
						<P>Any information found in this report is regarded as highly confidential. The 
							query subscriber is fully and solely responsible for any misuse of this 
							information.</P>
						<P>CreditInfo Group does not guarantee the validity of this report.</P>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
