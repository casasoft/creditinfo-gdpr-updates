using System;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.BLL;
using CreditInfoGroup.Localization;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.auSubscribers;
using UserAdmin.BLL.auUsers;

using Cig.Framework.Base.Configuration;

namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for Logon.
    /// </summary>
    public class FoLogon : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected Button btCancel;
        protected Button btnLogon;
        protected Label lblMessage;
        protected Label lbLogin;
        protected Label lbPassword;
        protected Label lbUserName;
        private uaFactory myUserAdminFactory;
        protected TextBox tbPassword;
        protected TextBox tbUserName;
        // Fyrir multilanguage d�mi. 
        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here

            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();
            // Fyrir multilanguage d�mi...
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            myUserAdminFactory = new uaFactory();
            LocalizeText();
        }

        private bool IsAuthenticated(string username, string password) {
            // Lookup code omitted for clarity
            // This code would typically validate the user name and password
            // combination against a SQL database or Active Directory
            // Simulate an authenticated user

            bool passwordVerified = false;
            try {
                passwordVerified = myUserAdminFactory.VerifyPassword(tbUserName.Text, tbPassword.Text);
            } catch (Exception ex) {
                Logger.WriteToLog("FoLogon.aspx, IsAuthenticated() :" + ex.Message, true);
                lblMessage.Text = rm.GetString("lblLoginFailedMessage", ci);
            }
            return passwordVerified;
        }

        private void btnLogon_Click(object sender, EventArgs e) {
            try {
                string userName = tbUserName.Text;
                bool isAuthenticated = IsAuthenticated(userName, tbPassword.Text);

                if (isAuthenticated) {
                    int userLogInID = myUserAdminFactory.GetUserID(userName);
                    // Athuga hvort a� notandi s� opinn. Ef ekki �� hefur hann ekki a�gang
                    if (!myUserAdminFactory.IsUserOpen(userLogInID)) {
                        lblMessage.Text = "User account is closed";
                        return;
                    }

                    // Athuga hvort a� notandi s� �trunninn (Open until). Ef hann er �trunninn - Enginn a�gangur
                    if (!myUserAdminFactory.IsUserExpiryDateOK(userLogInID)) {
                        lblMessage.Text = "User account has expired";
                        return;
                    }

                    // Athuga hvort a� �skrifandi s� loka�ur. Ef hann er loka�ur �� er loka� � alla skr��a notendur
                    // Undir honum.
                    if (!myUserAdminFactory.IsSubscriberOpen(userLogInID)) {
                        lblMessage.Text = "Subscriber account is closed";
                        return;
                    }

                    //OK - the user is ok and ready to use the web
                    Session.Add("UserCreditInfoID", myUserAdminFactory.GetCreditInfoID(userName));
                    Session.Add("CreditWatch", myUserAdminFactory.GetMaxCreditWatch(userName));
                    Session.Add("UserLoginID", userLogInID.ToString());
                    Session["UserLoginName"] = userName;

                    //Load user info (name and subscriber name into session
                    auUsersBLLC user = myUserAdminFactory.GetSpecificUser(userName);
                    auSubscribersBLLC subscriber =
                        myUserAdminFactory.GetOneSubscriberByCreditInfoIDAsSubscriber(user.SubscriberID);

                    string nativeUserName = myUserAdminFactory.GetCreditInfoUserNativeName(user.CreditInfoID);
                    string nativeSubscriberName = myUserAdminFactory.GetCreditInfoUserNativeName(
                        subscriber.CreditInfoID);
                    // if no native name the get the english one
                    if (nativeUserName.Trim() == "") {
                        nativeUserName = myUserAdminFactory.GetCreditInfoUserENName(user.CreditInfoID);
                    }
                    if (nativeSubscriberName.Trim() == "") {
                        nativeSubscriberName = myUserAdminFactory.GetCreditInfoUserENName(user.CreditInfoID);
                    }
                    Session.Add("NativeUserName", nativeUserName);
                    Session.Add("NativeSubscriberName", nativeSubscriberName);

                    string enUserName = myUserAdminFactory.GetCreditInfoUserENName(user.CreditInfoID);
                    string enSubscriberName = myUserAdminFactory.GetCreditInfoUserENName(subscriber.CreditInfoID);
                    // if no english name then get the native one
                    if (enUserName.Trim() == "") {
                        enUserName = myUserAdminFactory.GetCreditInfoUserNativeName(user.CreditInfoID);
                    }
                    if (enSubscriberName.Trim() == "") {
                        enSubscriberName = myUserAdminFactory.GetCreditInfoUserNativeName(user.CreditInfoID);
                    }

                    Session.Add("ENUserName", enUserName);
                    Session.Add("ENSubscriberName", enSubscriberName);

                    // Redirect the user to the originally requested page
                    // Response.Redirect(FormsAuthentication.GetRedirectUrl(tbUserName.Text, false));
                    FormsAuthentication.RedirectFromLoginPage(tbUserName.Text, false);
                } else {
                    var TryCount = (int) Session["LoginTryCount"];

                    if (TryCount <= 1) {
                        string myMessage = "User failed to log in three times. User is coming from IP address: " +
                                           Request.ServerVariables["REMOTE_ADDR"] + "\n\nUser tried to use name: " +
                                           tbUserName.Text + " and password: " + tbPassword.Text;
                        string mySubject = "CreditInfoGroup Login Failed (" +
                                           CigConfig.Configure("lookupsettings.rootURL") + ")";

                        var myFunctions = new HelpFunctions();
                        myFunctions.SendMail(myMessage, mySubject);

                        lblMessage.Text = rm.GetString("lblLoginFailedMessage", ci);
                    } else {
                        Session["LoginTryCount"] = --TryCount;
                        lblMessage.Text = rm.GetString("lblLoginTryAgainMessage", ci) + TryCount;
                    }
                }
            } catch (Exception ex) {
                //				if ( 0 == lblMessage.Text.Length)
                lblMessage.Text = ex.Message;
            }
        }

        private void LocalizeText() {
            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                lbLogin.Text = CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus") ? rm.GetString("lbLTCyprusHeaderLogin", ci) : rm.GetString("lbLTLogin", ci);
            } else {
                lbLogin.Text = rm.GetString("lbLTLogin", ci);
            }
            lbPassword.Text = rm.GetString("txtPassword", ci);
            lbUserName.Text = rm.GetString("txtUserName", ci);
            btnLogon.Text = rm.GetString("txtLogon", ci);
            btCancel.Text = rm.GetString("txtCancel", ci);
        }

        private void AddEnterEvent() {
            tbUserName.Attributes.Add("onkeypress", "checkEnterKey();");
            tbPassword.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnLogon.Click += new System.EventHandler(this.btnLogon_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}