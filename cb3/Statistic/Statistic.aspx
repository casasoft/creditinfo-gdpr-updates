<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="Statistic.aspx.cs" AutoEventWireup="false" Inherits="Statistic.Statistic"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
		<title>Customer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<!--#include file=include\olap.js -->
  </head>
	<body onload="InitializePage();initializePivotTable(document.Form1.ddlCubes.value)" ms_positioning="GridLayout">
		<form id="Form1" method="post" runat="server">
			<div id="service" style="BEHAVIOR: url(webservice.htc)"></div>
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->

									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblStatistic" runat="server">Statistic</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:dropdownlist id="ddlCubes" runat="server"></asp:dropdownlist>
																		<input id="tbCreditInfoID" type="hidden" name="tbCreditInfoID" runat="server">
																		<asp:button id="btnConnect" runat="server" cssclass="confirm_button" text="Connect"></asp:button>
																	</td>
																</tr>
																<tr valign="top">
																	<td width="50%">
																		<asp:label id="lblName" runat="server" visible="False">Name</asp:label>
																		<br>
																		<asp:textbox id="tbName" runat="server" visible="False"></asp:textbox><br>
																		<button id="BUTTON1" onclick="SaveReport()" type="button" runat="server" class=confirm_button>Save Report</button>
																	</td>
																	<td width="50%">
																		<asp:listbox id="lbSavedReports" runat="server" visible="False" width="200px" rows="5"></asp:listbox>
																		<br>
																		<button id="BUTTON2" onclick="LoadSavedReport()" type="button" runat="server" class=confirm_button>Load Report</button>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<th>
															<asp:label id="lblPivotHeader" runat="server"></asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
                              <object id=PivotTable1 
                              style="WIDTH: 502px; HEIGHT: 217px" height=217 
                              width=502 
                              classid=clsid:0002E552-0000-0000-C000-000000000046>
	<param NAME="XMLData" VALUE='<xml xmlns:x="urn:schemas-microsoft-com:office:excel">&#13;&#10; <x:PivotTable>&#13;&#10;  <x:OWCVersion>10.0.0.5605         </x:OWCVersion>&#13;&#10;  <x:DisplayScreenTips/>&#13;&#10;  <x:CubeProvider>msolap.2</x:CubeProvider>&#13;&#10;  <x:CacheDetails/>&#13;&#10;  <x:PivotView>&#13;&#10;   <x:IsNotFiltered/>&#13;&#10;  </x:PivotView>&#13;&#10; </x:PivotTable>&#13;&#10;</xml>'>
	</object>
																		<br>
																		<button id="Button3" style="FONT-SIZE: 8pt; WIDTH: 130px; FONT-FAMILY: Arial; HEIGHT: 21px" onclick="LoadChart()" type="button" runat="server">Show chart</button>
																		<br>
                              <object id=ChartSpace1 
                              style="VISIBILITY: hidden; WIDTH: 100%; HEIGHT: 350px" 
                              classid=CLSID:0002E556-0000-0000-C000-000000000046>
	<param NAME="XMLData" VALUE='<xml xmlns:x="urn:schemas-microsoft-com:office:excel">&#13;&#10; <x:ChartSpace>&#13;&#10;  <x:OWCVersion>10.0.0.5605         </x:OWCVersion>&#13;&#10;  <x:Width>21775</x:Width>&#13;&#10;  <x:Height>9260</x:Height>&#13;&#10;  <x:Palette>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#8080FF</x:Entry>&#13;&#10;   <x:Entry>#802060</x:Entry>&#13;&#10;   <x:Entry>#FFFFA0</x:Entry>&#13;&#10;   <x:Entry>#A0E0E0</x:Entry>&#13;&#10;   <x:Entry>#600080</x:Entry>&#13;&#10;   <x:Entry>#FF8080</x:Entry>&#13;&#10;   <x:Entry>#008080</x:Entry>&#13;&#10;   <x:Entry>#C0C0FF</x:Entry>&#13;&#10;   <x:Entry>#000080</x:Entry>&#13;&#10;   <x:Entry>#FF00FF</x:Entry>&#13;&#10;   <x:Entry>#80FFFF</x:Entry>&#13;&#10;   <x:Entry>#0080FF</x:Entry>&#13;&#10;   <x:Entry>#FF8080</x:Entry>&#13;&#10;   <x:Entry>#C0FF80</x:Entry>&#13;&#10;   <x:Entry>#FFC0FF</x:Entry>&#13;&#10;   <x:Entry>#FF80FF</x:Entry>&#13;&#10;  </x:Palette>&#13;&#10;  <x:DefaultFont>Arial</x:DefaultFont>&#13;&#10; </x:ChartSpace>&#13;&#10;</xml>'>
	<param NAME="ScreenUpdating" VALUE="-1">
	<param NAME="EnableEvents" VALUE="-1">
	</object>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
									</table>

									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
