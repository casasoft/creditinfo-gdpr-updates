using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Xsl;
using Logging.BLL;
using Statistic.BLL;
using Statistic.Localization;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;

using Cig.Framework.Base.Configuration;

namespace Statistic.user_controls {
    /// <summary>
    ///	Summary description for RequestReport.
    /// <para>Copyright (C) 2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
    /// </summary>
    public class RequestReport : UserControl {
        private static CultureInfo ci;
        private static ResourceManager rm;

        /// <summary>Button to search.</summary>
        protected Button btnSearch;

        /// <summary>Button to search for CIG entities.</summary>
        protected Button btSearchCigEntity;

        /// <summary>Check box to filter out employees.</summary>
        protected CheckBox cbxFilterOutEmployees;

        /// <summary>Validator to check if date from field contains a date.</summary>
        protected CompareValidator cpvDateFrom;

        /// <summary>Validator to ensure that date from is before data to.</summary>
        protected CompareValidator cpvDates;

        /// <summary>Validator to check if date to field contains a date.</summary>
        protected CompareValidator cpvDateTo;

        /// <summary>drop drown list for types of id number.</summary>
        protected DropDownList ddIDNumberType;

        /// <summary>Data grid to Creditinfo entities.</summary>
        protected DataGrid dgrCigEntities;

        private bool EN;

        /// <summary>Image for company.</summary>
        protected Image imgCompany;

        /// <summary>Image for individual.</summary>
        protected Image imgIndividual;

        /// <summary>Label for required *.</summary>
        protected Label Label2;

        /// <summary>Label for required *.</summary>
        protected Label Label6;

        /// <summary>Label for Ciid.</summary>
        protected Label lblCiid;

        /// <summary>Label for the value of Creditinfo Id.</summary>
        protected Label lblCiidValue;

        /// <summary>Label for data grid header</summary>
        protected Label lblDatagridHeader;

        /// <summary>Label for datagrid icons.</summary>
        protected Label lblDatagridIcons;

        /// <summary>Label for  data from.</summary>
        protected Label lblDateFrom;

        /// <summary>Label for date to.</summary>
        protected Label lblDateTo;

        /// <summary>Label for first name.</summary>
        protected Label lblFirstName;

        /// <summary>Label for id number.</summary>
        protected Label lblIDNumber;

        /// <summary>Label for type of id number.</summary>
        protected Label lblNumberType;

        /// <summary>Label for page header.</summary>
        protected Label lblPageHeader;

        /// <summary>Lable for report search header.</summary>
        protected Label lblReportSearch;

        /// <summary>Label for surname.</summary>
        protected Label lblSurname;

        /// <summary>label for user list header.</summary>
        protected Label lblUserListHeader;

        /// <summary>Radio button for company.</summary>
        protected RadioButton rbtnCompany;

        /// <summary>Radio button for individual.</summary>
        protected RadioButton rbtnIndividual;

        /// <summary>Validator to ensure date from is not empty.</summary>
        protected RequiredFieldValidator rfvDateFrom;

        /// <summary>Validator to ensure date to is not empty.</summary>
        protected RequiredFieldValidator rfvDateTo;

        /// <summary>Text box for first name.</summary>/summary>
        protected TextBox tbFirstName;

        /// <summary>Text box for Id number.</summary>
        protected TextBox tbIDNumber;

        /// <summary>HTML table for results of CIG entity search.</summary>
        protected HtmlTable tblCigEntities;

        /// <summary>HTML table for date fields.</summary>
        protected HtmlTable tblDateFields;

        /// <summary>HTML table for result data grid.</summary>
        protected HtmlTable tblResults;

        /// <summary>Text box for surname.</summary>
        protected TextBox tbSurname;

        /// <summary>Text box for date from.</summary>
        protected TextBox txtDateFrom;

        /// <summary>Text box for date to.</summary>
        protected TextBox txtDateTo;

        /// <summary>Validation summary</summary>
        protected ValidationSummary ValidationSummary;

        /// <summary>Xml for request.</summary>
        protected Xml xmlRequests;

        private void Page_Load(object sender, EventArgs e) {
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            tblCigEntities.Visible = false;

            if (!IsPostBack) {
                tblDateFields.Visible = false;
                tblResults.Visible = false;

                SetColumnsHeaderByCulture();

                LoadDefaults();
                InitIDNumberType();
            }

            LocalizeText();
        }

        /// <summary>
        /// Loads text fields with texts in a specific culture.
        /// </summary>
        private void LocalizeText() {
            btnSearch.Text = rm.GetString("btnSearch", ci);
            btSearchCigEntity.Text = rm.GetString("btnSearch", ci);

            cbxFilterOutEmployees.Text = rm.GetString("lblFilterOutEmployees", ci);

            cpvDateFrom.ErrorMessage = rm.GetString("valDateFromCompare", ci);
            cpvDates.ErrorMessage = rm.GetString("valDatesCompare", ci);
            cpvDateTo.ErrorMessage = rm.GetString("valDateToCompare", ci);

            lblDatagridHeader.Text = rm.GetString("lblResults", ci);
            lblDateFrom.Text = rm.GetString("lblDateFrom", ci);
            lblDateTo.Text = rm.GetString("lblDateTo", ci);
            lblIDNumber.Text = rm.GetString("lblIdNumber", ci);
            lblNumberType.Text = rm.GetString("lblType", ci);
            lblFirstName.Text = rm.GetString("lblFirstName", ci);
            lblSurname.Text = rm.GetString("lblSurname", ci);
            lblPageHeader.Text = rm.GetString("lblRequestReport", ci);
            lblCiid.Text = rm.GetString("lblCiid", ci);
            lblUserListHeader.Text = rm.GetString("txtResults", ci);
            lblReportSearch.Text = rm.GetString("lblReportSearch", ci);

            rfvDateFrom.ErrorMessage = rm.GetString("valDateFromRequired", ci);
            rfvDateTo.ErrorMessage = rm.GetString("valDateToRequired", ci);
        }

        private void LoadDefaults() {
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            var factory = new StatisticFactory();
            var transformer = new XslTransform();
            xmlRequests.Document = factory.GetRequestReport(
                lblCiidValue.Text,
                DateTime.Parse(txtDateFrom.Text),
                DateTime.Parse(txtDateTo.Text),
                cbxFilterOutEmployees.Checked,
                EN);

            if (xmlRequests.Document.DocumentElement == null) {} else {
                // Adding culture and resources.
                var element = xmlRequests.Document.CreateElement("Settings");
                if (EN) {
                    var attribute = xmlRequests.Document.CreateAttribute("IsEnglish");
                    attribute.Value = EN.ToString();
                    element.Attributes.Append(attribute);
                }
                var child = xmlRequests.Document.CreateElement("txtDate");
                child.InnerText = rm.GetString("lblDate", ci);
                element.AppendChild(child);

                child = xmlRequests.Document.CreateElement("txtSubscriber");
                child.InnerText = rm.GetString("lblSubscriber", ci);
                element.AppendChild(child);

                child = xmlRequests.Document.CreateElement("txtUser");
                child.InnerText = rm.GetString("lblUser", ci);
                element.AppendChild(child);

                child = xmlRequests.Document.CreateElement("txtType");
                child.InnerText = rm.GetString("lblType", ci);
                element.AppendChild(child);

                xmlRequests.Document.DocumentElement.InsertBefore(
                    element, xmlRequests.Document.DocumentElement.FirstChild);

                transformer.Load(
                    Server.MapPath(
                        CigConfig.Configure("lookupsettings.backRootName") +
                        CigConfig.Configure("lookupsettings.XSL_Stats_RequestReport")));
                xmlRequests.Transform = transformer;

                tblResults.Visible = true;
            }
        }

        private Debtor GetDebtorInfoFromPage() {
            var myDebtor = new Debtor
                              {
                                  IDNumber1 = tbIDNumber.Text,
                                  IDNumber2Type = int.Parse(ddIDNumberType.SelectedItem.Value),
                                  SurName = tbSurname.Text,
                                  FirstName = tbFirstName.Text
                              };
            return myDebtor;
        }

        private Company GetCompanyInfoFromPage() {
            var theCompany = new Company {NationalID = tbIDNumber.Text, NameNative = tbFirstName.Text};
            return theCompany;
        }

        private void SetColumnsHeaderByCulture() {
            dgrCigEntities.Columns[1].HeaderText = rm.GetString("lblCiid", ci);
            dgrCigEntities.Columns[2].HeaderText = rm.GetString("lblName", ci);
            dgrCigEntities.Columns[3].HeaderText = rm.GetString("lblName", ci);
            dgrCigEntities.Columns[4].HeaderText = rm.GetString("lblAddress", ci);
            dgrCigEntities.Columns[5].HeaderText = rm.GetString("lblAddress", ci);
            dgrCigEntities.Columns[6].HeaderText = rm.GetString("lblIdNumber", ci);
            dgrCigEntities.Columns[7].HeaderText = rm.GetString("lblCity", ci);
            dgrCigEntities.Columns[8].HeaderText = rm.GetString("lblCity", ci);
            dgrCigEntities.Columns[9].HeaderText = rm.GetString("lblPostalCode", ci);
        }

        private void SetColumnsByCulture() {
            if (EN) {
                dgrCigEntities.Columns[0].Visible = true; //Select
                dgrCigEntities.Columns[1].Visible = true; //CIID
                dgrCigEntities.Columns[2].Visible = false; //Name
                dgrCigEntities.Columns[3].Visible = true; //NameEN
                dgrCigEntities.Columns[4].Visible = false; //Street
                dgrCigEntities.Columns[5].Visible = true; //StreetEN
                dgrCigEntities.Columns[6].Visible = true; //IDNumber
                dgrCigEntities.Columns[7].Visible = false; //City
                dgrCigEntities.Columns[8].Visible = true; //CityEN
                dgrCigEntities.Columns[9].Visible = true; //PostalCode
            } else {
                dgrCigEntities.Columns[0].Visible = true; //Select
                dgrCigEntities.Columns[1].Visible = true; //CIID
                dgrCigEntities.Columns[2].Visible = true; //Name
                dgrCigEntities.Columns[3].Visible = false; //NameEN
                dgrCigEntities.Columns[4].Visible = true; //Street
                dgrCigEntities.Columns[5].Visible = false; //StreetEN
                dgrCigEntities.Columns[6].Visible = true; //IDNumber
                dgrCigEntities.Columns[7].Visible = true; //City
                dgrCigEntities.Columns[8].Visible = false; //CityEN
                dgrCigEntities.Columns[9].Visible = true; //PostalCode
            }
        }

        private void btSearchCigEntity_Click(object sender, EventArgs e) {
            tblDateFields.Visible = false;
            tblResults.Visible = false;
            dgrCigEntities.SelectedIndex = -1;
            //this.lblErrorMessage.Visible = false;

            var myFactory = new uaFactory();
            DataSet customerSet = null;

            if (rbtnIndividual.Checked) //We are looking for an individual
            {
                customerSet = myFactory.FindCustomer(GetDebtorInfoFromPage());
                dgrCigEntities.Columns[2].Visible = false;
            } else //We are looking for a company
            {
                customerSet = myFactory.FindCompany(GetCompanyInfoFromPage());
                dgrCigEntities.Columns[2].Visible = false;
            }

            if (customerSet.Tables.Count > 0 && customerSet.Tables[0].Rows.Count > 0) {
                //this.dgDebtors.DataSource=null;

                var myView = customerSet.Tables[0].DefaultView;
                myView.Sort = EN ? "NameEN" : "NameNative";
                dgrCigEntities.DataSource = myView;
                dgrCigEntities.DataKeyField = "CreditInfoID";
                dgrCigEntities.DataBind();
                tblCigEntities.Visible = true;
            } else {
                dgrCigEntities.DataSource = null;
                dgrCigEntities.DataBind();
                tblCigEntities.Visible = false;
//				if(rbtnCompany.Checked)
//					this.lblErrorMessage.Text = rm.GetString("txtCompanyNotFound", ci);
//				else
//					this.lblErrorMessage.Text = rm.GetString("txtIndividualNotFound", ci);
//				this.lblErrorMessage.Visible = true;
            }
            SetColumnsByCulture();
        }

        private void rbtnIndividual_CheckedChanged(object sender, EventArgs e) {
            tbSurname.Visible = true;
            lblSurname.Visible = true;
            lblFirstName.Text = rm.GetString("lblFirstName", ci);

            dgrCigEntities.DataSource = null;
            dgrCigEntities.DataBind();
            tblCigEntities.Visible = false;
        }

        private void rbtnCompany_CheckedChanged(object sender, EventArgs e) {
            tbSurname.Visible = false;
            lblSurname.Visible = false;
            lblFirstName.Text = rm.GetString("lblName", ci);

            dgrCigEntities.DataSource = null;
            dgrCigEntities.DataBind();
            tblCigEntities.Visible = false;
        }

        private void InitIDNumberType() {
            var myFactory = new uaFactory();
            var idNumberType = myFactory.GetIDNumberTypesAsDataSet();

            ddIDNumberType.DataSource = idNumberType;
            ddIDNumberType.DataTextField = EN ? "TypeEN" : "TypeNative";

            ddIDNumberType.DataValueField = "NumberTypeID";
            ddIDNumberType.DataBind();
        }

        private void dgrCigEntities_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (!e.CommandName.ToLower().Equals(DataGrid.SelectCommandName.ToLower())) {
                return;
            }
            tblDateFields.Visible = true;
            lblCiidValue.Text = dgrCigEntities.DataKeys[e.Item.ItemIndex].ToString();
        }

        private void dgrCigEntities_ItemDataBound(object sender, DataGridItemEventArgs e) {
            // Name
            if (e.Item.Cells[2].Text != null && e.Item.Cells[2].Text.Equals("&nbsp;")) {
                e.Item.Cells[2].Text = e.Item.Cells[3].Text;
            }
            if (e.Item.Cells[3].Text != null && e.Item.Cells[3].Text.Equals("&nbsp;")) {
                e.Item.Cells[3].Text = e.Item.Cells[2].Text;
            }
            // Address
            if (e.Item.Cells[4].Text != null && e.Item.Cells[4].Text.Equals("&nbsp;")) {
                e.Item.Cells[4].Text = e.Item.Cells[5].Text;
            }
            if (e.Item.Cells[5].Text != null && e.Item.Cells[5].Text.Equals("&nbsp;")) {
                e.Item.Cells[5].Text = e.Item.Cells[4].Text;
            }
            // City
            if (e.Item.Cells[7].Text != null && e.Item.Cells[7].Text.Equals("&nbsp;")) {
                e.Item.Cells[7].Text = e.Item.Cells[8].Text;
            }
            if (e.Item.Cells[8].Text != null && e.Item.Cells[8].Text.Equals("&nbsp;")) {
                e.Item.Cells[8].Text = e.Item.Cells[7].Text;
            }

            WebDesign.CreateExplanationIcons(dgrCigEntities.Columns, lblDatagridIcons, rm, ci, 1);
        }

        #region Web Form Designer generated code

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rbtnIndividual.CheckedChanged += new System.EventHandler(this.rbtnIndividual_CheckedChanged);
            this.rbtnCompany.CheckedChanged += new System.EventHandler(this.rbtnCompany_CheckedChanged);
            this.btSearchCigEntity.Click += new System.EventHandler(this.btSearchCigEntity_Click);
            this.dgrCigEntities.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgrCigEntities_ItemCommand);
            this.dgrCigEntities.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrCigEntities_ItemDataBound);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}