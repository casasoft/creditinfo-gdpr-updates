<%@ Control Language="c#" AutoEventWireup="false" Codebehind="RequestReport.ascx.cs" Inherits="Statistic.user_controls.RequestReport" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<script language=JavaScript src="CheckEnterKey.js"></script>

<script language=JavaScript src="../DatePicker.js"></script>

<script language=javascript> 
	function checkEnterKey(path) 
	{    
		if (event.keyCode == 13) 
		{        
			event.cancelBubble = true;
			event.returnValue = false;
			//document.BillingMain.FindCtrl1_btnSearch.click(); 
		}
	} 
	
	function SetFormFocus()
	{
		//document.BillingMain.FindCtrl1_txtIdNumber.focus();
	}
</script>

<body onload=SetFormFocus()>
<form id=Search method=post>
<table width="100%">
  <tr>
    <td>
      <table class=grid_table cellSpacing=0 cellPadding=0 
        >
        <tr>
          <th><asp:label id=lblPageHeader runat="server" cssclass="HeadMain">Individual search</asp:label></th></tr>
        <tr>
          <td>
            <table class=fields id=tbCriteria cellSpacing=0 cellPadding=0 
            >
              <tr>
                <td colSpan=2><br 
                  ><asp:radiobutton id=rbtnIndividual runat="server" cssclass="radio" checked="True" autopostback="True" groupname="CustomerType"></asp:radiobutton><asp:image id=imgIndividual runat="server" imageurl="../../img/individual.gif"></asp:image>&nbsp;<asp:radiobutton id=rbtnCompany runat="server" cssclass="radio" autopostback="True" groupname="CustomerType"></asp:radiobutton><asp:image id=imgCompany runat="server" imageurl="../../img/company.gif"></asp:image></td></tr>
              <tr>
                <td><asp:label id=lblIDNumber runat="server" width="155px"></asp:label><asp:label id=lblNumberType runat="server"></asp:label><br 
                  ><asp:textbox id=tbIDNumber runat="server"></asp:textbox>&nbsp;<asp:dropdownlist id=ddIDNumberType runat="server"></asp:dropdownlist></td>
                <td><asp:label id=lblFirstName runat="server"></asp:label><br 
                  ><asp:textbox id=tbFirstName runat="server"></asp:textbox></td>
                <td><asp:label id=lblSurname runat="server"></asp:label><br 
                  ><asp:textbox id=tbSurname runat="server"></asp:textbox></td>
                <td vAlign=bottom align=right><asp:button id=btSearchCigEntity runat="server" cssclass="search_button" text="search"></asp:button></td></tr>
              <tr colspan="3">
                <td style="WIDTH: 395px" colSpan=3 height=23 
                ></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=grid_table id=tblCigEntities cellSpacing=0 cellPadding=0 
       runat="server">
        <tr>
          <td height=10></td></tr>
        <tr>
          <td align=right><asp:label id=lblDatagridIcons runat="server"></asp:label></td></tr>
        <tr>
          <td height=5></td></tr>
        <tr>
          <th><asp:label id=lblUserListHeader runat="server"></asp:label></th></tr>         
        <tr>
          <td>
            <table class=datagrid cellSpacing=0 cellPadding=0 
            >
              <tr>
                <td><asp:datagrid id=dgrCigEntities runat="server" width="100%" autogeneratecolumns="False" allowsorting="True" gridlines="None">
<footerstyle cssclass="grid_footer">
</FooterStyle>

<selecteditemstyle cssclass="grid_selecteditem">
</SelectedItemStyle>

<alternatingitemstyle cssclass="grid_alternatingitem">
</AlternatingItemStyle>

<itemstyle cssclass="grid_item">
</ItemStyle>

<headerstyle cssclass="grid_header">
</HeaderStyle>

<columns>
<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;" CommandName="Select">
<itemstyle cssclass="leftpadding">
</ItemStyle>
</asp:ButtonColumn>
<asp:BoundColumn DataField="CreditInfoID" SortExpression="CreditInfoID" HeaderText="CreditInfoID">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="NameNative" SortExpression="NameNative" HeaderText="Name">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="NameEN" SortExpression="NameEN" HeaderText="NameEN">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="StreetNative" SortExpression="StreetNative" HeaderText="Street(N)">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="StreetEN" SortExpression="StreetEN" HeaderText="Street EN">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="Number" SortExpression="Number" HeaderText="National ID">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="CityNative" SortExpression="CityNative" HeaderText="CityNative">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="CityEN" SortExpression="CityEN" HeaderText="CityEN">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="PostalCode" SortExpression="PostalCode" HeaderText="PostalCode">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
</Columns>

<pagerstyle cssclass="grid_pager">
</PagerStyle>
											</asp:datagrid></td></tr></table></td></tr></table></td></tr>
  <tr>
          <td height=10></td></tr> 
  <tr>
    <td>
      <table class=grid_table cellSpacing=0 cellPadding=0 id="tblDateFields" runat="server"
        >
        <tr>
          <th style="HEIGHT: 19px"><asp:label id=lblReportSearch runat="server">[PageHeader]</asp:label></th></tr>
        <tr>
          <td>
            <table class=fields cellSpacing=0 cellPadding=0>
              <tr>
                <td><asp:Label id="lblCiid" runat="server"></asp:Label><br><asp:Label id="lblCiidValue" runat="server"></asp:Label>
                </td>
                <td>
											<asp:label id="lblDateFrom" runat="server">[Date from]</asp:label>
											<br>
											<asp:textbox id="txtDateFrom" runat="server"></asp:textbox>
											<input class="popup" onclick="PopupPicker('RequestReport_txtDateFrom', 250, 250);" type="button" value="...">
											<asp:label id="Label6" runat="server" cssclass="error_text">*</asp:label>
											<asp:requiredfieldvalidator id="rfvDateFrom" runat="server" controltovalidate="txtDateFrom" errormessage="requiredBeginDate" display="None">*</asp:requiredfieldvalidator>
											<asp:comparevalidator id="cpvDateFrom" runat="server" controltovalidate="txtDateFrom" errormessage="CompareDateFrom" display="None" operator="DataTypeCheck" type="Date">*</asp:comparevalidator></td>
										<td>
											<asp:label id="lblDateTo" runat="server">[Date to]</asp:label>
											<br>
											<asp:textbox id="txtDateTo" runat="server"></asp:textbox>
											<input class="popup" onclick="PopupPicker('RequestReport_txtDateTo', 250, 250);" type="button" value="...">
											<asp:label id="Label2" runat="server" cssclass="error_text">*</asp:label>
											<asp:requiredfieldvalidator id="rfvDateTo" runat="server" controltovalidate="txtDateTo" errormessage="requiredBeginDate" display="None">*</asp:requiredfieldvalidator>
											<asp:comparevalidator id="cpvDateTo" runat="server" controltovalidate="txtDateTo" errormessage="CompareEndDate" display="None" operator="DataTypeCheck" type="Date">*</asp:comparevalidator>
											<asp:comparevalidator id="cpvDates" runat="server" controltovalidate="txtDateTo" errormessage="CompareValidator" display="None" operator="GreaterThanEqual" type="Date" controltocompare="txtDateFrom">*</asp:comparevalidator>
										</td>
										<td valign="bottom" align="right">
											<asp:button id="btnSearch" runat="server" cssclass="search_button"></asp:button>
										</td>
									</tr>
									<tr>
										<td>
											<asp:checkbox id="cbxFilterOutEmployees" runat="server" checked="True" visible="False" cssclass="radio"></asp:checkbox>
										</td>										
									</tr>
									<tr>
										<td height="23">
										</td>
									</tr></table></td></tr></table></td></tr>
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td>
					<table class="grid_table" id="tblResults" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<th>
								<asp:label id="lblDatagridHeader" runat="server"></asp:label>
							</th>
						</tr>
						<tr>
							<td>
								<table class="fields" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<asp:xml id="xmlRequests" runat="server"></asp:xml>
										</td>
									</tr>
									<tr>
										<td height="23"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td>
					<table class="empty_table" cellspacing="0">
						<tr valign="top">
							<td>
								<asp:validationsummary id="ValidationSummary" runat="server" width="100%" displaymode="List"></asp:validationsummary>
							</td>
						</tr>
					</table>
				</td>
			</tr></table></form>
</body>
