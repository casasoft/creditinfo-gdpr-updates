using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Statistic.BLL;
using Statistic.Localization;

using Cig.Framework.Base.Configuration;

namespace Statistic.user_controls {
    /// <summary>
    ///		Summary description for UsageReport.
    /// </summary>
    public class UsageReport : UserControl {
        protected static CultureInfo ci;
        protected static ResourceManager rm;
        private bool _isBillable;
        protected Button btnSearch;
        protected CheckBox chbNonUsage;
        protected CompareValidator cpvDateFrom;
        protected CompareValidator cpvDates;
        protected CompareValidator cpvDateTo;
        protected DataGrid dgSubscriberList;
        protected DataGrid dgSubscriberNonUsageList;
        protected bool EN;
        protected Label Label1;
        protected Label Label6;
        protected Label lblBillable;
        protected Label lblDatagridIcons;
        protected Label lblDatagridNonUsageIcons;
        protected Label lblDateFrom;
        protected Label lblDateTo;
        protected Label lblPageHeader;
        protected Label lblProductCount;
        protected Label lblProductName;
        protected Label lblProductsCount;
        protected Label lblProductsNumberOf;
        protected Label lblSubscriberActive;
        protected Label lblSubscriberCount;
        protected Label lblSubscriberGridHeader;
        protected Label lblSubscriberIDNumber;
        protected Label lblSubscriberName;
        protected Label lblSubscriberNonUsageGridHeader;
        protected Label lblSubscriberNumberOf;
        protected Label lblTypeCount;
        protected Label lblTypeNumberOf;
        protected Label lblUsageCounts;
        protected Label lblUserActive;
        protected Label lblUserCount;
        protected Label lblUserIDNumber;
        protected Label lblUserName;
        protected Label lblUserNumberOf;
        protected Label lblUserUsername;
        protected RadioButtonList rblBillable;
        protected RadioButtonList rblSubscriberActive;
        protected RadioButtonList rblUserActive;
        protected RadioButton rbtSearchByAll;
        protected RadioButton rbtSearchBySubscriber;
        protected RadioButton rbtSearchByUser;
        protected RequiredFieldValidator rfvDateFrom;
        protected RequiredFieldValidator rfvDateTo;
        protected HtmlTable tableNonUsageSubscribers;
        protected HtmlTable tableSubscribers;
        protected HtmlTable tableUsageCounts;
        protected TextBox txtDateFrom;
        protected TextBox txtDateTo;
        protected TextBox txtSubscriberIDNumber;
        protected TextBox txtSubscriberName;
        protected TextBox txtUserIDNumber;
        protected TextBox txtUserName;
        protected TextBox txtUserUsername;
        protected ValidationSummary ValidationSummary;

        private void Page_Load(object sender, EventArgs e) {
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                EN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (!IsPostBack) {
                tableNonUsageSubscribers.Visible = false;
                tableUsageCounts.Visible = false;
                tableSubscribers.Visible = false;

                LoadDefaults();
            }

            try {
                _isBillable = Convert.ToBoolean(CigConfig.Configure("lookupsettings.IsBillableActive"));
            } catch {
                _isBillable = false;
            }
            lblBillable.Visible = _isBillable;
            rblBillable.Visible = _isBillable;

            LocalizeText();
        }



        /// <summary>
        /// Enables the appropriate labels and fields according to which RadioButton is checked.
        /// </summary>
        private void SearchBy_CheckedChanged() {

            if (this.rbtSearchByAll.Checked)
            {
                this.chbNonUsage.Enabled = false;
            }

            lblSubscriberIDNumber.Enabled = rbtSearchBySubscriber.Checked;
            txtSubscriberIDNumber.Enabled = rbtSearchBySubscriber.Checked;
            lblSubscriberName.Enabled = rbtSearchBySubscriber.Checked;
            txtSubscriberName.Enabled = rbtSearchBySubscriber.Checked;
            if (!rbtSearchBySubscriber.Checked) {
                txtSubscriberName.Text = "";
                txtSubscriberIDNumber.Text = "";
            }
            else
            {
                this.chbNonUsage.Enabled = true;
            }

            lblUserIDNumber.Enabled = rbtSearchByUser.Checked;
            txtUserIDNumber.Enabled = rbtSearchByUser.Checked;
            lblUserName.Enabled = rbtSearchByUser.Checked;
            txtUserName.Enabled = rbtSearchByUser.Checked;
            lblUserUsername.Enabled = rbtSearchByUser.Checked;
            txtUserUsername.Enabled = rbtSearchByUser.Checked;
            if (!rbtSearchByUser.Checked)
            {
                txtUserIDNumber.Text = "";
                txtUserName.Text = "";
                txtUserUsername.Text = "";
            }
            else
            {
                this.chbNonUsage.Enabled = true;
            }
           // chbNonUsage.Enabled = rbtSearchByAll.Checked;
            if (chbNonUsage.Enabled == false) 
            {
                chbNonUsage.Checked = false;
            }

            this.chbNonUsage_CheckedChanged(this.chbNonUsage, new EventArgs());
        }

        /// <summary>
        /// Loads text fields with texts in a specific culture.
        /// </summary>
        private void LocalizeText() {
            // Buttons
            btnSearch.Text = rm.GetString("btnSearch", ci);

            // Labels
            lblPageHeader.Text = rm.GetString("lblUsageReport", ci);

            lblSubscriberIDNumber.Text = rm.GetString("lblIdNumber", ci);
            lblSubscriberName.Text = rm.GetString("lblName", ci);

            lblUserIDNumber.Text = rm.GetString("lblIdNumber", ci);
            lblUserName.Text = rm.GetString("lblNameOfUser", ci);
            lblUserUsername.Text = rm.GetString("lblUsername", ci);

            lblDateFrom.Text = rm.GetString("lblDateFrom", ci);
            lblDateTo.Text = rm.GetString("lblDateTo", ci);
            lblUserActive.Text = rm.GetString("lblUsers", ci);
            lblSubscriberActive.Text = rm.GetString("lblSubscribers", ci);
            lblBillable.Text = rm.GetString("lblBillable", ci);

            lblUsageCounts.Text = rm.GetString("lblNumberOf", ci);
            lblSubscriberNumberOf.Text = rm.GetString("lblSubscriber", ci);
            lblUserNumberOf.Text = rm.GetString("lblUsers", ci);
            lblTypeNumberOf.Text = rm.GetString("lblType", ci);
            lblProductsNumberOf.Text = rm.GetString("lblProduct", ci);

            lblSubscriberGridHeader.Text = rm.GetString("lblResults", ci);
            lblSubscriberNonUsageGridHeader.Text = rm.GetString("lblResults", ci);

            chbNonUsage.Text = rm.GetString("chbNonUsage", ci);

            // RabioButtons & RadioButtonLists
            rbtSearchByAll.Text = rm.GetString("lblAll", ci);
            rbtSearchBySubscriber.Text = rm.GetString("lblSubscribers", ci);
            rbtSearchByUser.Text = rm.GetString("lblUsers", ci);

            rblUserActive.Items[0].Text = rm.GetString("lblOpen", ci);
            rblUserActive.Items[1].Text = rm.GetString("lblClosed", ci);
            rblUserActive.Items[2].Text = rm.GetString("lblBoth", ci);

            rblSubscriberActive.Items[0].Text = rm.GetString("lblOpen", ci);
            rblSubscriberActive.Items[1].Text = rm.GetString("lblClosed", ci);
            rblSubscriberActive.Items[2].Text = rm.GetString("lblBoth", ci);

            rblBillable.Items[0].Text = rm.GetString("lblTrue", ci);
            rblBillable.Items[1].Text = rm.GetString("lblFalse", ci);
            rblBillable.Items[2].Text = rm.GetString("lblBoth", ci);

            // Datagrid
            dgSubscriberList.Columns[0].HeaderText = rm.GetString("lblIdNumber", ci);
            dgSubscriberList.Columns[1].HeaderText = rm.GetString("lblName", ci);
            dgSubscriberList.Columns[2].HeaderText = rm.GetString("lblName", ci);
            dgSubscriberList.Columns[3].HeaderText = rm.GetString("lblUsername", ci);
            dgSubscriberList.Columns[4].HeaderText = rm.GetString("lblType", ci);
            dgSubscriberList.Columns[5].HeaderText = rm.GetString("lblType", ci);
            dgSubscriberList.Columns[6].HeaderText = rm.GetString("lblNumberOf", ci);

            //Datagrid non usage

            dgSubscriberNonUsageList.Columns[0].HeaderText = rm.GetString("lblIdNumber", ci);
            dgSubscriberNonUsageList.Columns[1].HeaderText = rm.GetString("lblName", ci);
            dgSubscriberNonUsageList.Columns[2].HeaderText = rm.GetString("lblName", ci);
            dgSubscriberNonUsageList.Columns[3].HeaderText = rm.GetString("lblUsername", ci);

            // Validators
            rfvDateFrom.ErrorMessage = rm.GetString("valDateFromRequired", ci);
            rfvDateTo.ErrorMessage = rm.GetString("valDateToRequired", ci);
            cpvDateFrom.ErrorMessage = rm.GetString("valDateFromCompare", ci);
            cpvDateTo.ErrorMessage = rm.GetString("valDateToCompare", ci);
            cpvDates.ErrorMessage = rm.GetString("valDatesCompare", ci);
        }

        private void LoadDefaults() {
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            var factory = new StatisticFactory();
            DataSet statsData = null;
            DataSet subscriberData = null;

            XBool IsBillable = null;
            if (rblBillable.Items[0].Selected)
            {
                IsBillable = new XBool(true);
            }
            else if (rblBillable.Items[1].Selected)
            {
                IsBillable = new XBool(false);
            }

            XBool IsSubscriberActive = null;
            if (rblSubscriberActive.Items[0].Selected)
            {
                IsSubscriberActive = new XBool(true);
            }
            else if (rblSubscriberActive.Items[1].Selected)
            {
                IsSubscriberActive = new XBool(false);
            }

            XBool IsUserActive = null;
            if (rblUserActive.Items[0].Selected)
            {
                IsUserActive = new XBool(true);
            }
            else if (rblUserActive.Items[1].Selected)
            {
                IsUserActive = new XBool(false);
            }

            // Get the result totals.
            if (rbtSearchByAll.Checked)
            {
                if (chbNonUsage.Checked)
                {
                    if (IsSubscriberActive == null && IsUserActive == null)
                    {
                        subscriberData = factory.GetNonUsageSubscribers(
                            DateTime.Parse(txtDateFrom.Text), DateTime.Parse(txtDateTo.Text).AddDays(1));
                    }
                    else
                    {
                        subscriberData = factory.GetNonUsageSubscribers(
                            DateTime.Parse(txtDateFrom.Text),
                            DateTime.Parse(txtDateTo.Text).AddDays(1),
                            IsSubscriberActive,
                            IsUserActive);
                    }
                }
                else
                {
                    if (IsSubscriberActive == null && IsUserActive == null)
                    {
                        statsData = factory.GetUsageCounts(
                            DateTime.Parse(txtDateFrom.Text), DateTime.Parse(txtDateTo.Text).AddDays(1), IsBillable);
                        subscriberData = factory.GetUsageSubscribers(
                            DateTime.Parse(txtDateFrom.Text), DateTime.Parse(txtDateTo.Text).AddDays(1), IsBillable);
                    }
                    else
                    {
                        statsData = factory.GetUsageCounts(
                            DateTime.Parse(txtDateFrom.Text),
                            DateTime.Parse(txtDateTo.Text).AddDays(1),
                            IsBillable,
                            IsSubscriberActive,
                            IsUserActive);
                        subscriberData = factory.GetUsageSubscribers(
                            DateTime.Parse(txtDateFrom.Text),
                            DateTime.Parse(txtDateTo.Text).AddDays(1),
                            IsBillable,
                            IsSubscriberActive,
                            IsUserActive);
                    }
                }
            }
            else if (rbtSearchBySubscriber.Checked)
            {
                if (chbNonUsage.Checked)
                {
                    //if (IsSubscriberActive == null && IsUserActive == null)
                    //{
                        subscriberData = factory.GetNonUsageBySubscribers(
                            DateTime.Parse(txtDateFrom.Text), 
                            DateTime.Parse(txtDateTo.Text).AddDays(1), 
                            IsBillable, 
                            IsSubscriberActive, 
                            IsUserActive);
                    //}
                    //else
                    //{
                    //    subscriberData = factory.GetNonUsageSubscribers(
                    //        DateTime.Parse(txtDateFrom.Text),
                    //        DateTime.Parse(txtDateTo.Text).AddDays(1),
                    //        IsSubscriberActive,
                    //        IsUserActive);
                    //}
                }
                else
                {
                    statsData = factory.GetUsageCounts(
                        DateTime.Parse(txtDateFrom.Text),
                        DateTime.Parse(txtDateTo.Text).AddDays(1),
                        IsBillable,
                        IsSubscriberActive,
                        IsUserActive,
                        txtSubscriberIDNumber.Text,
                        txtSubscriberName.Text);
                    subscriberData = factory.GetUsageSubscribers(
                        DateTime.Parse(txtDateFrom.Text),
                        DateTime.Parse(txtDateTo.Text).AddDays(1),
                        IsBillable,
                        IsSubscriberActive,
                        IsUserActive,
                        txtSubscriberIDNumber.Text,
                        txtSubscriberName.Text);
                }
            }
            else if (rbtSearchByUser.Checked)
            {
                if (chbNonUsage.Checked)
                {
                    //if (IsSubscriberActive == null && IsUserActive == null)
                    //{
                    //    subscriberData = factory.GetNonUsageSubscribersUsers(
                    //        DateTime.Parse(txtDateFrom.Text), 
                    //        DateTime.Parse(txtDateTo.Text).AddDays(1), 
                    //        IsSubscriberActive,
                    //        IsUserActive);
                    //}
                    //else
                    //{
                        subscriberData = factory.GetNonUsageSubscribersUsers(
                            DateTime.Parse(txtDateFrom.Text),
                            DateTime.Parse(txtDateTo.Text).AddDays(1),
                            IsSubscriberActive,
                            IsUserActive,
                            IsBillable);
                    //}
                }
                else
                {
                    statsData = factory.GetUsageCounts(
                        DateTime.Parse(txtDateFrom.Text),
                        DateTime.Parse(txtDateTo.Text).AddDays(1),
                        IsBillable,
                        IsSubscriberActive,
                        IsUserActive,
                        txtUserIDNumber.Text,
                        txtUserName.Text,
                        txtUserUsername.Text);
                    subscriberData = factory.GetUsageSubscribers(
                        DateTime.Parse(txtDateFrom.Text),
                        DateTime.Parse(txtDateTo.Text).AddDays(1),
                        IsBillable,
                        IsSubscriberActive,
                        IsUserActive,
                        txtUserIDNumber.Text,
                        txtUserName.Text,
                        txtUserUsername.Text);
                }
            }

            if (statsData != null)
            {
                // Make the stats visable and load with data.
                tableUsageCounts.Visible = true;
                lblSubscriberCount.Text = statsData.Tables[0].Rows[0]["subscriber_count"].ToString();
                lblUserCount.Text = statsData.Tables[0].Rows[0]["user_count"].ToString();
                lblTypeCount.Text = statsData.Tables[0].Rows[0]["type_count"].ToString();
                lblProductsCount.Text = statsData.Tables[0].Rows[0]["product_count"].ToString();

                lblProductName.Text = "";
                lblProductCount.Text = "";

                if (statsData.Tables[1] != null)
                {
                    foreach (DataRow row in statsData.Tables[1].Rows)
                    {
                        lblProductCount.Text += row[0] + "<br/>";
                        if (EN)
                        {
                            lblProductName.Text += row[1] + "<br/>";
                        }
                        else
                        {
                            lblProductName.Text += row[2] + "<br/>";
                        }
                    }
                }
            }
            if (subscriberData != null && chbNonUsage.Checked)
            {
                dgSubscriberNonUsageList.DataSource = subscriberData;
                dgSubscriberNonUsageList.DataBind();

                if (dgSubscriberNonUsageList.Items != null && dgSubscriberNonUsageList.Items.Count > 0)
                {
                    tableNonUsageSubscribers.Visible = true;
                }
                else
                {
                    tableNonUsageSubscribers.Visible = false;
                }

                dgSubscriberNonUsageList.Columns[0].Visible = true;
                dgSubscriberNonUsageList.Columns[1].Visible = false;
                dgSubscriberNonUsageList.Columns[2].Visible = true;
                if (this.rbtSearchByUser.Checked)
                {
                    dgSubscriberNonUsageList.Columns[3].Visible = true;
                }
                else
                {
                    dgSubscriberNonUsageList.Columns[3].Visible = false;
                }

                tableSubscribers.Visible = false;
            }
            else if (subscriberData != null)
            {
                tableNonUsageSubscribers.Visible = false;
                dgSubscriberList.DataSource = subscriberData;
                dgSubscriberList.DataBind();

                if (dgSubscriberList.Items != null && dgSubscriberList.Items.Count > 0)
                {
                    tableSubscribers.Visible = true;
                }
                else
                {
                    tableSubscribers.Visible = false;
                }

                if (EN)
                {
                    dgSubscriberList.Columns[1].Visible = true;
                    dgSubscriberList.Columns[2].Visible = false;
                    dgSubscriberList.Columns[4].Visible = true;
                    dgSubscriberList.Columns[5].Visible = false;
                }
                else
                {
                    dgSubscriberList.Columns[1].Visible = false;
                    dgSubscriberList.Columns[2].Visible = true;
                    dgSubscriberList.Columns[4].Visible = false;
                    dgSubscriberList.Columns[5].Visible = true;
                }
            }
            else
            {
                tableSubscribers.Visible = false;
            }
        }

        private void dgSubscriberList_ItemDataBound(object sender, DataGridItemEventArgs e) {
            // Put NameEN = NameNative if Name is null
            if (string.IsNullOrEmpty(e.Item.Cells[1].Text) || e.Item.Cells[1].Text.Equals("&nbsp;")) {
                e.Item.Cells[1].Text = e.Item.Cells[2].Text;
            }
            // Put NameNative = NameEN if Name is null
            if (string.IsNullOrEmpty(e.Item.Cells[2].Text) || e.Item.Cells[2].Text.Equals("&nbsp;")) {
                e.Item.Cells[2].Text = e.Item.Cells[1].Text;
            }

            e.Item.Cells[1].Text += " | " + e.Item.Cells[3].Text;
            e.Item.Cells[2].Text += " | " + e.Item.Cells[3].Text;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rbtSearchByAll.CheckedChanged += new EventHandler(this.rbtSearchByAll_CheckedChanged);

            this.rbtSearchByAll.CheckedChanged += new System.EventHandler(this.rbtSearchByAll_CheckedChanged);
            this.rbtSearchBySubscriber.CheckedChanged +=
                new System.EventHandler(this.rbtSearchBySubscriber_CheckedChanged);
            this.rbtSearchByUser.CheckedChanged += new System.EventHandler(this.rbtSearchByUser_CheckedChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            //this.dgSubscriberNonUsageList.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSubscriberList_ItemDataBound);
            this.dgSubscriberList.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSubscriberList_ItemDataBound);

            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

        /// <summary>
        /// Non usage checkbox checked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chbNonUsage_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {
                if (this.rbtSearchBySubscriber.Checked)
                {
                    this.txtSubscriberIDNumber.Enabled = false;
                    this.txtSubscriberName.Enabled = false;
                    this.lblSubscriberIDNumber.Enabled = false;
                    this.lblSubscriberName.Enabled = false;                    
                }

                if (this.rbtSearchByUser.Checked)
                {
                    this.txtUserIDNumber.Enabled = false;
                    this.txtUserName.Enabled = false;
                    this.txtUserUsername.Enabled = false;
                    this.lblUserIDNumber.Enabled = false;
                    this.lblUserName.Enabled = false;
                    this.lblUserUsername.Enabled = false;
                }
            }
            else
            {
                if (this.rbtSearchBySubscriber.Checked)
                {
                    this.txtSubscriberIDNumber.Enabled = true;
                    this.txtSubscriberName.Enabled = true;
                    this.lblSubscriberIDNumber.Enabled = true;
                    this.lblSubscriberName.Enabled = true;      
                }

                if (this.rbtSearchByUser.Checked)
                {
                    this.txtUserIDNumber.Enabled = true;
                    this.txtUserName.Enabled = true;
                    this.txtUserUsername.Enabled = true;
                    this.lblUserIDNumber.Enabled = true;
                    this.lblUserName.Enabled = true;
                    this.lblUserUsername.Enabled = true;
                }
            }
        }

 
        protected void rbtSearchByAll_CheckedChanged(object sender, EventArgs e)
        {
            SearchBy_CheckedChanged();
        }


        private void rbtSearchBySubscriber_CheckedChanged(object sender, EventArgs e) { SearchBy_CheckedChanged(); }
        private void rbtSearchByUser_CheckedChanged(object sender, EventArgs e) { SearchBy_CheckedChanged(); }

    }
}