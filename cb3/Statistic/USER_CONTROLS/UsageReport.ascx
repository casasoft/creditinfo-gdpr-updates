<%@ Control Language="c#" AutoEventWireup="false" Codebehind="UsageReport.ascx.cs" Inherits="Statistic.user_controls.UsageReport" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="JavaScript" src="CheckEnterKey.js"></script>
<script language="JavaScript" src="../DatePicker.js"></script>
<script language="javascript"> 
	function checkEnterKey(path) 
	{    
		if (event.keyCode == 13) 
		{        
			event.cancelBubble = true;
			event.returnValue = false;
			//document.BillingMain.FindCtrl1_btnSearch.click(); 
		}
	} 
	
	function SetFormFocus()
	{
		//document.BillingMain.FindCtrl1_txtIdNumber.focus();
	}
</script>
<body onload="SetFormFocus()">
	<form id="Search" method="post">
		<table width="100%">

			<tr>
				<td>
					<table class="grid_table" cellspacing="0" cellpadding="0">
    
						<tr>
							<th>
								<asp:label id="lblPageHeader" runat="server">[PageHeader]</asp:label></th></tr>
						<tr>
							<td>
								<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
            
									<tr>
										<td style="WIDTH: 27%" valign="top"><asp:radiobutton id="rbtSearchByAll" 
                                                runat="server" text="[All users]" groupname="SearchBy" autopostback="True"
												checked="True" cssclass="radio" ></asp:radiobutton></td>
										<td style="WIDTH: 27%"></td>
										<td style="WIDTH: 23%"></td>
										<td></td>
									</tr>
									<tr>
										<td valign="top"><asp:radiobutton id="rbtSearchBySubscriber" runat="server" 
                                                text="[Subscriber]" groupname="SearchBy"
												autopostback="True" cssclass="radio" ></asp:radiobutton></td>
										<td><asp:label id="lblSubscriberIDNumber" runat="server" enabled="False">[ID number]</asp:label><br>
											<asp:textbox id="txtSubscriberIDNumber" runat="server" enabled="False"></asp:textbox></td>
										<td><asp:label id="lblSubscriberName" runat="server" enabled="False">[Name]</asp:label><br>
											<asp:textbox id="txtSubscriberName" runat="server" enabled="False"></asp:textbox></td>
										<td></td>
									</tr>
									<tr>
										<td valign="top"><asp:radiobutton id="rbtSearchByUser" runat="server" text="[User]" 
                                                groupname="SearchBy" autopostback="True"
												cssclass="radio" ></asp:radiobutton></td>
										<td><asp:label id="lblUserIDNumber" runat="server" enabled="False">[ID number]</asp:label><br>
											<asp:textbox id="txtUserIDNumber" runat="server" enabled="False"></asp:textbox></td>
										<td><asp:label id="lblUserName" runat="server" enabled="False">[Name]</asp:label><br>
											<asp:textbox id="txtUserName" runat="server" enabled="False"></asp:textbox></td>
										<td><asp:label id="lblUserUsername" runat="server" enabled="False">[Username]</asp:label><br>
											<asp:textbox id="txtUserUsername" runat="server" enabled="False"></asp:textbox></td>
									</tr>
									<tr>
										<td><asp:label id="lblDateFrom" runat="server">[Date from]</asp:label><br>
											<asp:textbox id="txtDateFrom" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('UsageReport_txtDateFrom', 250, 250);" type="button"
												value="...">&nbsp;
											<asp:label id="Label6" runat="server" cssclass="error_text">*</asp:label><asp:requiredfieldvalidator id="rfvDateFrom" runat="server" display="None" controltovalidate="txtDateFrom" errormessage="requiredBeginDate">*</asp:requiredfieldvalidator><asp:comparevalidator id="cpvDateFrom" runat="server" display="None" controltovalidate="txtDateFrom" errormessage="CompareDateFrom"
												type="Date" operator="DataTypeCheck">*</asp:comparevalidator></td>
										<td><asp:label id="lblDateTo" runat="server">[Date to]</asp:label><br>
											<asp:textbox id="txtDateTo" runat="server"></asp:textbox><input class="popup" onclick="PopupPicker('UsageReport_txtDateTo', 250, 250);" type="button"
												value="...">&nbsp;
											<asp:label id="Label1" runat="server" cssclass="error_text">*</asp:label><asp:requiredfieldvalidator id="rfvDateTo" runat="server" display="None" controltovalidate="txtDateTo" errormessage="requiredBeginDate">*</asp:requiredfieldvalidator><asp:comparevalidator id="cpvDateTo" runat="server" display="None" controltovalidate="txtDateTo" errormessage="CompareEndDate"
												type="Date" operator="DataTypeCheck">*</asp:comparevalidator><asp:comparevalidator id="cpvDates" runat="server" display="None" controltovalidate="txtDateTo" errormessage="CompareValidator"
												type="Date" operator="GreaterThanEqual" controltocompare="txtDateFrom">*</asp:comparevalidator></td>
										<td><asp:label id="lblUserActive" runat="server"> 
                  [User]</asp:label><br>
											<asp:radiobuttonlist id="rblUserActive" runat="server" cssclass="radio" font-size="X-Small" height="8px"
												width="80%" repeatdirection="Horizontal">
												<asp:listitem value="Open">Open</asp:listitem>
												<asp:listitem value="Closed">Closed</asp:listitem>
												<asp:listitem value="Both" selected="True">Both</asp:listitem>
											</asp:radiobuttonlist></td>
										<td><asp:label id="lblSubscriberActive" runat="server">[Subscriber]</asp:label><br>
											<asp:radiobuttonlist id="rblSubscriberActive" runat="server" cssclass="radio" font-size="X-Small" height="8px"
												width="80%" repeatdirection="Horizontal">
												<asp:listitem value="Open">Open</asp:listitem>
												<asp:listitem value="Closed">Closed</asp:listitem>
												<asp:listitem value="Both" selected="True">Both</asp:listitem>
											</asp:radiobuttonlist></td>
									</tr>
									<tr>
										<td><asp:label id="lblBillable" runat="server" visible="False">[Billable]</asp:label><br>
											<asp:radiobuttonlist id="rblBillable" runat="server" cssclass="radio" repeatdirection="Horizontal" visible="False">
												<asp:listitem value="True">[True]</asp:listitem>
												<asp:listitem value="False">[False]</asp:listitem>
												<asp:listitem value="null" selected="True">[Both]</asp:listitem>
											</asp:radiobuttonlist></td>
										<td></td>
										<td></td>
										<td></td>
									<tr>
										<td>
										<asp:CheckBox ID="chbNonUsage" Runat="server" cssclass="radio" Text="[Non Usage]" 
                                              AutoPostBack="true"  oncheckedchanged="chbNonUsage_CheckedChanged" 
                                                Enabled="False"></asp:CheckBox>
										</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									</tr>
									<tr>
										<td style="HEIGHT: 24px" align="right" colspan="4"><asp:button id="btnSearch" runat="server" text="[Search]" cssclass="search_button"></asp:button></td>
									</tr>
									<tr>
										<td height="23"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="grid_table" id="tableUsageCounts" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblUsageCounts" runat="server">[PageHeader]</asp:label></th></tr>
						<tr>
							<td>
								<table class="fields" cellspacing="0" cellpadding="0">
									<tr>
										<td style="WIDTH: 50%" colspan="2"><asp:label id="lblSubscriberNumberOf" runat="server">[Number of subscribers]</asp:label></td>
										<td><asp:label id="lblSubscriberCount" runat="server">[N]</asp:label></td>
									</tr>
									<tr>
										<td colspan="2"><asp:label id="lblUserNumberOf" runat="server">[Number of users]</asp:label></td>
										<td><asp:label id="lblUserCount" runat="server">[N]</asp:label></td>
									</tr>
									<tr>
										<td colspan="2"><asp:label id="lblTypeNumberOf" runat="server">[Number of types]</asp:label></td>
										<td><asp:label id="lblTypeCount" runat="server">[N]</asp:label></td>
									</tr>
									<tr>
										<td colspan="2"><asp:label id="lblProductsNumberOf" runat="server">[Number of products]</asp:label></td>
										<td><asp:label id="lblProductsCount" runat="server">[N]</asp:label></td>
									</tr>
									<tr>
										<td width="5%"></td>
										<td><asp:label id="lblProductName" runat="server"></asp:label></td>
										<td><asp:label id="lblProductCount" runat="server"></asp:label></td>
									</tr>
									<tr>
										<td height="23"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table class="grid_table" id="tableSubscribers" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td align="right">
								<div><asp:label id="lblDatagridIcons" runat="server"></asp:label></div>
							</td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblSubscriberGridHeader" runat="server">[ ]</asp:label></th></tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td><asp:datagrid id="dgSubscriberList" runat="server" cssclass="grid" gridlines="None" allowsorting="True"
												autogeneratecolumns="False">
												<footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<columns>
													<asp:boundcolumn datafield="Number" headertext="[ID Number]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="NameEN" headertext="[NameEN]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="NameNative" headertext="[NameNative]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn visible="False" datafield="Username" headertext="[Username]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="typeEn" headertext="[TypeEN]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="typeNative" headertext="[TypeNative]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="type_count" headertext="[Type count]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
												</columns>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
											</asp:datagrid></td>
									</tr>
								</table>
							</td>													
						</tr>
					</table>
					
					<table class="grid_table" id="tableNonUsageSubscribers" cellspacing="0" cellpadding="0" runat="server">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td align="right">
								<div><asp:label id="lblDatagridNonUsageIcons" runat="server"></asp:label></div>
							</td>
						</tr>
						<tr>
							<th>
								<asp:label id="lblSubscriberNonUsageGridHeader" runat="server">[ ]</asp:label></th></tr>
						<tr>
							<td>
								<table class="datagrid" cellspacing="0" cellpadding="0">
									<tr>
										<td><asp:datagrid id="dgSubscriberNonUsageList" runat="server" cssclass="grid" gridlines="None" allowsorting="True"
												autogeneratecolumns="False">
												<footerstyle cssclass="grid_footer"></footerstyle>
												<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
												<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
												<itemstyle cssclass="grid_item"></itemstyle>
												<headerstyle cssclass="grid_header"></headerstyle>
												<columns>
													<asp:boundcolumn datafield="Number" headertext="[ID Number]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="NameEN" headertext="[NameEN]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="NameNative" headertext="[NameNative]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn visible="False" datafield="Username" headertext="[Username]">
														<itemstyle cssclass="padding"></itemstyle>
													</asp:boundcolumn>													
												</columns>
												<pagerstyle cssclass="grid_pager"></pagerstyle>
											</asp:datagrid></td>
									</tr>
								</table>
							</td>													
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td>
					<table class="empty_table" cellspacing="0">
						<tr valign="top">
							<td align="left" rowspan="2"><asp:validationsummary id="ValidationSummary" runat="server" width="100%" displaymode="List"></asp:validationsummary></td>
							<td align="right"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
</form>
</body>
