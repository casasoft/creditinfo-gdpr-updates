using System;
using System.Data;
using System.Xml;
using Statistic.DAL;

namespace Statistic.BLL {
    /// <summary>
    /// Summary description for StatisticFactory.
    /// </summary>
    public class StatisticFactory {
        private readonly StatisticDALC statsDAL;
        public StatisticFactory() { statsDAL = new StatisticDALC(); }

        #region Report

        public DataSet GetAllReports() { return statsDAL.GetAllReports(); }
        public ReportData GetReport(int nReportID) { return statsDAL.GetReport(nReportID); }
        public DataSet GetReports(int nCubeID, int nCreditInfoID) { return statsDAL.GetReports(nCubeID, nCreditInfoID); }
        public string GetReportXmlData(int nReportId) { return statsDAL.GetReportXmlData(nReportId); }

        public bool StoreReport(ReportData report) {
            if (report.Created == DateTime.MinValue) {
                report.Created = DateTime.Now;
            }

            return statsDAL.StoreReport(report);
        }

        #endregion

        #region Cube

        public DataSet GetAllCubes() { return statsDAL.GetAllCubes(); }
        public CubeData GetCube(int nCubeID) { return statsDAL.GetCube(nCubeID); }

        #endregion

        #region Usage

        public DataSet GetNonUsageCounts(DateTime dtFrom, DateTime dtTo, XBool IsBillable) { return statsDAL.GetNonUsageCounts(dtFrom, dtTo, IsBillable); }

        /// <summary>
        /// Gets usage statistic for a specific period
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(DateTime dtFrom, DateTime dtTo, XBool IsBillable) { return statsDAL.GetUsageCounts(dtFrom, dtTo, IsBillable); }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(
            DateTime dtFrom, DateTime dtTo, XBool IsBillable, XBool IsSubscriberOpen, XBool IsUserOpen) { return statsDAL.GetUsageCounts(dtFrom, dtTo, IsBillable, IsSubscriberOpen, IsUserOpen); }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by Subscriber info.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <param name="strSubscriberIDNumber">Number identifying the subscriber</param>
        /// <param name="strSubscriberName">Subscribers name</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(
            DateTime dtFrom,
            DateTime dtTo,
            XBool IsBillable,
            XBool IsSubscriberOpen,
            XBool IsUserOpen,
            string strSubscriberIDNumber,
            string strSubscriberName) {
            return statsDAL.GetUsageCounts(
                dtFrom, dtTo, IsBillable, IsSubscriberOpen, IsUserOpen, strSubscriberIDNumber, strSubscriberName);
        }

        /// <summary>
        /// Gets usage statistic for a specific period by subscriber ID.
        /// </summary>
        /// <param name="dtFrom">Begin date.</param>
        /// <param name="dtTo">End date.</param>
        /// <param name="IsBillable">Get billed events or not.</param>
        /// <param name="SubscriberID">Unique ID for the subscriber.</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(DateTime dtFrom, DateTime dtTo, XBool IsBillable, int SubscriberID) { return statsDAL.GetUsageCounts(dtFrom, dtTo, IsBillable, SubscriberID); }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by User info.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <param name="strUserIDNumber">Number identifying the user</param>
        /// <param name="strUserName">Name of the user</param>
        /// <param name="strUserUsername">Users username</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(
            DateTime dtFrom,
            DateTime dtTo,
            XBool IsBillable,
            XBool IsSubscriberOpen,
            XBool IsUserOpen,
            string strUserIDNumber,
            string strUserName,
            string strUserUsername) {
            return statsDAL.GetUsageCounts(
                dtFrom, dtTo, IsBillable, IsSubscriberOpen, IsUserOpen, strUserIDNumber, strUserName, strUserUsername);
        }

        /// <summary>
        /// Gets all subscribers that have preformed an event the system in a specific period.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable">Event billed or not</param>
        /// <returns>DataSet with the subscribers and there stats</returns>
        public DataSet GetUsageSubscribers(DateTime dtFrom, DateTime dtTo, XBool IsBillable) { return statsDAL.GetUsageSubscribers(dtFrom, dtTo, IsBillable); }

        public DataSet GetNonUsageSubscribers(DateTime dtFrom, DateTime dtTo) { return statsDAL.GetNonUsageSubscribers(dtFrom, dtTo); }

        /// <summary>
        /// Return DataSet of Subscribers without any user with usages for selected date.
        /// </summary>
        /// <param name="dtFrom"></param>
        /// <param name="dtTo"></param>
        /// <returns></returns>
        public DataSet GetNonUsageBySubscribers(DateTime dtFrom, DateTime dtTo, XBool IsBillable, XBool IsSubscriberOpen, XBool IsUserOpen)
        {
            return statsDAL.GetNonUsageBySubscribers(dtFrom, dtTo, IsBillable, IsSubscriberOpen, IsUserOpen);
        }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageSubscribers(
            DateTime dtFrom, DateTime dtTo, XBool IsBillable, XBool IsSubscriberOpen, XBool IsUserOpen) { return statsDAL.GetUsageSubscribers(dtFrom, dtTo, IsBillable, IsSubscriberOpen, IsUserOpen); }

        public DataSet GetNonUsageSubscribers(DateTime dtFrom, DateTime dtTo, XBool IsSubscriberOpen, XBool IsUserOpen) { return statsDAL.GetNonUsageSubscribers(dtFrom, dtTo, IsSubscriberOpen, IsUserOpen); }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by Subscriber info.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <param name="strSubscriberIDNumber">Number identifying the subscriber</param>
        /// <param name="strSubscriberName">Subscribers name</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageSubscribers(
            DateTime dtFrom,
            DateTime dtTo,
            XBool IsBillable,
            XBool IsSubscriberOpen,
            XBool IsUserOpen,
            string strSubscriberIDNumber,
            string strSubscriberName) {
            return statsDAL.GetUsageSubscribers(
                dtFrom, dtTo, IsBillable, IsSubscriberOpen, IsUserOpen, strSubscriberIDNumber, strSubscriberName);
        }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by Subscriber info.
        /// </summary>
        /// <param name="dtFrom">Begin date.</param>
        /// <param name="dtTo">End date.</param>
        /// <param name="IsBillable"></param>
        /// <param name="SubscriberID">Unique ID for the subscriber.</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count).</returns>
        public DataSet GetUsageSubscribers(DateTime dtFrom, DateTime dtTo, XBool IsBillable, int SubscriberID) { return statsDAL.GetUsageSubscribers(dtFrom, dtTo, IsBillable, SubscriberID); }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by User info.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <param name="strUserIDNumber">Number identifying the user</param>
        /// <param name="strUserName">Name of the user</param>
        /// <param name="strUserUsername">Users username</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageSubscribers(
            DateTime dtFrom,
            DateTime dtTo,
            XBool IsBillable,
            XBool IsSubscriberOpen,
            XBool IsUserOpen,
            string strUserIDNumber,
            string strUserName,
            string strUserUsername) {
            return statsDAL.GetUsageSubscribers(
                dtFrom, dtTo, IsBillable, IsSubscriberOpen, IsUserOpen, strUserIDNumber, strUserName, strUserUsername);
        }

        public DataSet GetNonUsageSubscribersUsers(DateTime dtFrom, DateTime dtTo,  XBool IsSubscriberOpen, XBool IsUserOpen,XBool IsBillable)
        {
            return statsDAL.GetNonUsageSubscribersUsers(dtFrom, dtTo, IsSubscriberOpen, IsUserOpen, IsBillable);
        }

        /// <summary>
        /// Gets a report indicating who and when request information about an id number.
        /// </summary>
        /// <param name="ciid">CreditinfoId of a user.</param>
        /// <param name="from">Date and time from.</param>
        /// <param name="to">Date and time to.</param>
        /// <param name="filterOutEmployees">Should employees be included or not.</param>
        /// <param name="isEN">Is english used.</param>
        /// <returns>A dataset containg request report.</returns>
        public XmlDocument GetRequestReport(string ciid, DateTime from, DateTime to, bool filterOutEmployees, bool isEN) {
//			string lastCiid = "", lastQueryType = "";
//			System.IO.StringWriter stream = new System.IO.StringWriter();
//			XmlTextWriter writer = new XmlTextWriter(stream);
//			writer.Formatting = Formatting.Indented;
//			writer.WriteStartDocument();
//			writer.WriteStartElement("CigData");
//			bool isFirst = true;
//			foreach(DataRow row in statsDAL.GetRequestReport(idNumber, from, to).Tables[0].Rows)
//			{
//				if(row["Creditinfoid"].ToString().Equals(lastCiid))
//				{
//					if(!row["query_type"].ToString().Equals(lastQueryType)) // New Type.
//					{
//						lastQueryType = row["query_type"].ToString();
//						writer.WriteEndElement();
//						writer.WriteStartElement("Type");						
//						writer.WriteElementString("Name", isEN ? row["TypeEN"].ToString() : row["TypeNative"].ToString());						
//					}
//					writer.WriteElementString("Date", DateTime.Parse(row["Created"].ToString()).ToShortDateString());
//				}
//				else // New CIID.
//				{
//					lastCiid = row["Creditinfoid"].ToString();
//					lastQueryType = row["query_type"].ToString();
//					if(!isFirst)
//					{
//						writer.WriteEndElement();
//						writer.WriteEndElement();						
//					}
//					else
//						isFirst = false;
//					writer.WriteStartElement("CigEntity");
//					writer.WriteElementString("CIID", row["Creditinfoid"].ToString());
//					writer.WriteElementString("Name", isEN ? row["NameEN"].ToString() : row["NameNative"].ToString());
//					writer.WriteStartElement("Type");
//					writer.WriteElementString("Name", isEN ? row["TypeEN"].ToString() : row["TypeNative"].ToString());
//					writer.WriteElementString("Date", DateTime.Parse(row["Created"].ToString()).ToShortDateString());
//				}
//			}
//			writer.WriteEndElement();
//			writer.WriteEndElement();
//			writer.WriteEndElement();
//			writer.Flush();
//
//			XmlDocument doc = new XmlDocument();
//			doc.LoadXml(stream.GetStringBuilder().ToString());
//			return doc;

            XmlDataDocument datadoc = new XmlDataDocument(statsDAL.GetRequestReport(ciid, from, to, filterOutEmployees));
            datadoc.DataSet.EnforceConstraints = false;
            return datadoc;
        }

        //public DataSet GetCigEntity

        #endregion
    }
}