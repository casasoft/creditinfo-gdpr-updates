namespace Statistic.BLL {
    /// <summary>
    /// Summary description for CubeData.
    /// </summary>
    public class CubeData {
        // private members
        // empty constructor
        public CubeData() { }
        // full constructor
        public CubeData(int CubeID, string NameNative, string NameEN, string NameDB, string Description) {
            this.CubeID = CubeID;
            this.NameNative = NameNative;
            this.NameEN = NameEN;
            this.NameDB = NameDB;
            this.Description = Description;
        }

        // public accessors
        public int CubeID { get; set; }
        public string NameNative { get; set; }
        public string NameEN { get; set; }
        public string NameDB { get; set; }
        public string Description { get; set; }
    }
}