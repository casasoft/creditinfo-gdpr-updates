namespace Statistic.BLL {
    /// <summary>
    /// Three value boolean variable. True/False/null.
    /// </summary>
    public class XBool {
        public XBool(bool newValue) { Value = newValue; }
        public bool Value { get; set; }
        public override string ToString() { return Value.ToString(); }
    }
}