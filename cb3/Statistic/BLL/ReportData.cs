using System;

namespace Statistic.BLL {
    /// <summary>
    /// Summary description for ReportData.
    /// </summary>
    public class ReportData {
        // private members
        // empty constructor
        public ReportData() { Created = DateTime.MinValue; }
        // full constructor
        public ReportData(
            int CubeID, int ID, string XmlData, string Name, bool IsPublic, int CreditinfoID, DateTime Created) {
            this.CubeID = CubeID;
            this.ID = ID;
            this.XmlData = XmlData;
            this.Name = Name;
            this.IsPublic = IsPublic;
            this.CreditinfoID = CreditinfoID;
            this.Created = Created;
        }

        // public accessors
        public int CubeID { get; set; }
        public int ID { get; set; }
        public string XmlData { get; set; }
        public string Name { get; set; }
        public bool IsPublic { get; set; }
        public int CreditinfoID { get; set; }
        public DateTime Created { get; set; }
    }
}