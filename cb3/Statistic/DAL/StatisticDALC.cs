using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using Logging.BLL;
using Statistic.BLL;
using UserAdmin.DAL;
using cb3;
using cb3.Audit;

namespace Statistic.DAL {
    /// <summary>
    /// Summary description for StatisticDALC.
    /// </summary>
    public class StatisticDALC {
        /// <summary>
        /// Database functions for statistic component
        /// </summary>
        private const string className = "StatisticDALC";

        #region Report

        public DataSet GetAllReports() {
            const string funcName = "GetAllReports()";

            const string query = "SELECT [report_id], [xmldata], [name], [ispublic], [creditinfoid], [created], [cube_id] "
                                 + "FROM [stats_report] "
                                 + "WHERE [ispublic]=1 "
                                 + "ORDER BY [created] ";
            try {
                return executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return null;
        }

        public DataSet GetReports(int nCubeID, int nCreditInfoID) {
            const string funcName = "GetAllReports()";

            string query = "SELECT [report_id], [xmldata], [name], [ispublic], [creditinfoid], [created], [cube_id] "
                           + "FROM [stats_report] "
                           + "WHERE [cube_id]=" + nCubeID + " "
                           + "AND ([ispublic]=1 "
                           + "OR [creditinfoid]=" + nCreditInfoID + ") "
                           + "ORDER BY [created] ";
            try {
                return executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return null;
        }

        public string GetReportXmlData(int nReportId) {
            const string funcName = "GetReportXmlData(int nReportId)";
            var ds = new DataSet();
            string query = "SELECT [xmldata] "
                           + "FROM [stats_report] "
                           + "WHERE [report_id]=" + nReportId + " ";
            try {
                ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                            return ds.Tables[0].Rows[i]["xmldata"].ToString();
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return null;
        }

        /// <summary>
        /// Saves a report to database (INSERT if report.ID is -1 else UPDATE)
        /// </summary>
        /// <param name="report">The report to store</param>
        /// <returns>True/False</returns>
        public bool StoreReport(ReportData report) {
            const string funcName = "StoreReport(ReportData report)";
            OleDbCommand myOleDbCommand;
            try {
                using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    myOleDbConn.Open();
                    // begin transaction...
                    var myTrans = myOleDbConn.BeginTransaction();
                    try {
                        string strSql;
                        if (report.ID == -1) {
                            // insert into stats_report
                            strSql = "INSERT INTO [stats_report]"
                                     + "([xmldata], [name], [ispublic], [creditinfoid], [created], [cube_id])"
                                     + "VALUES(?, ?, ?, ?, ?, ?)";
                        } else {
                            strSql = "UPDATE [stats_report] "
                                     +
                                     "SET [xmldata]=?, [name]=?, [ispublic]=?, [creditinfoid]=?, [created]=?, [cube_id]=? "
                                     + "WHERE [report_id]=" + report.ID;
                        }
                        myOleDbCommand = new OleDbCommand(strSql, myOleDbConn) {Transaction = myTrans};

                        var myParam = new OleDbParameter("xmldata", OleDbType.LongVarChar)
                                                 {
                                                     Value = report.XmlData,
                                                     Direction = ParameterDirection.Input
                                                 };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("name", OleDbType.VarChar, 50)
                                  {
                                      Value = report.Name,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ispublic", OleDbType.Boolean)
                                  {
                                      Value = report.IsPublic,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("creditinfoid", OleDbType.Integer)
                                  {
                                      Value = report.CreditinfoID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("created", OleDbType.Date)
                                  {
                                      Value = report.Created,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("cube_id", OleDbType.Integer)
                                  {
                                      Value = report.CubeID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                        myTrans.Commit();
                    } catch (Exception e) {
                        Logger.WriteToLog(className + " : " + funcName + " : " + e.Message, true);
                        myTrans.Rollback();
                        return false;
                    } finally {
                        // myOleDbCommand.Connection.Close(); // � ekki a� �urfa ef using er nota�
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(className + ": " + funcName + e.Message, true);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Fetches one specific report from database using primary key.
        /// </summary>
        /// <param name="nReportID">Reports unique identifier</param>
        /// <returns>ReportData object</returns>
        public ReportData GetReport(int nReportID) {
            const string funcName = "GetReport(int nReportID)";

            string query = "SELECT [report_id], [xmldata], [name], [ispublic], [creditinfoid], [created], [cube_id] "
                           + "FROM [stats_report] "
                           + "WHERE [report_id]=" + nReportID + " ";
            try {
                var ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                            var data = new ReportData
                                       {
                                           ID = int.Parse(ds.Tables[0].Rows[i]["report_id"].ToString()),
                                           XmlData = ds.Tables[0].Rows[i]["xmldata"].ToString(),
                                           Name = ds.Tables[0].Rows[i]["name"].ToString(),
                                           IsPublic = bool.Parse(ds.Tables[0].Rows[i]["ispublic"].ToString()),
                                           CreditinfoID = int.Parse(ds.Tables[0].Rows[i]["creditinfoid"].ToString()),
                                           Created = DateTime.Parse(ds.Tables[0].Rows[i]["created"].ToString()),
                                           CubeID = int.Parse(ds.Tables[0].Rows[i]["cube_id"].ToString())
                                       };

                            return data;
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return null;
        }

        #endregion

        #region Cube

        public DataSet GetAllCubes() {
            const string funcName = "GetAllCubes()";

            const string query = "SELECT [cube_id], [nameNative], [nameEN], [nameDB], [description] "
                                 + "FROM [stats_cube] "
                                 + "ORDER BY [cube_id] ";
            try {
                return executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return null;
        }

        public CubeData GetCube(int nCubeID) {
            const string funcName = "GetCube(int nCubeID)";

            string query = "SELECT [cube_id], [nameNative], [nameEN], [nameDB], [description] "
                           + "FROM [stats_cube] "
                           + "WHERE [cube_id]=" + nCubeID + " "
                           + "ORDER BY [cube_id] ";
            try {
                var ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                            var data = new CubeData
                                       {
                                           CubeID = int.Parse(ds.Tables[0].Rows[i]["cube_id"].ToString()),
                                           Description = ds.Tables[0].Rows[i]["description"].ToString(),
                                           NameDB = ds.Tables[0].Rows[i]["nameDB"].ToString(),
                                           NameEN = ds.Tables[0].Rows[i]["nameEN"].ToString(),
                                           NameNative = ds.Tables[0].Rows[i]["nameNative"].ToString()
                                       };

                            return data;
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return null;
        }

        #endregion

        #region Usage

        /// <summary>
        /// Gets usage statistic for a specific period
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(DateTime dtFrom, DateTime dtTo, XBool IsBillable) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = " AND np_Usage.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }

            string strSql = "SELECT 	COUNT(DISTINCT SubscriberID) as subscriber_count, COUNT(DISTINCT UserID) as user_count, COUNT(DISTINCT query_type) as type_count, COUNT(DISTINCT np_Usage.id) as product_count "
                            + "FROM np_Usage INNER JOIN au_users ON np_Usage.UserID = au_users.id WHERE created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "' AND created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "'"
                            + strWhere;

            DataSet mySet = executeSelectStatement(strSql);

            strSql = "SELECT COUNT(np_Usage.query_type) AS type_count, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     + "FROM np_Usage INNER JOIN np_UsageTypes ON np_Usage.query_type = np_UsageTypes.id "
                     + "WHERE (np_Usage.created >= '"
                     + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                     + "') AND (np_Usage.created < '"
                     + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                     + "') "
                     + strWhere
                     + "GROUP BY np_Usage.query_type, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     + "ORDER BY COUNT(np_Usage.query_type) DESC";

            mySet.Tables.Add(executeSelectStatementDataTable(strSql));
            return mySet;
        }

        public DataSet GetNonUsageCounts(DateTime dtFrom, DateTime dtTo, XBool IsBillable) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = " AND np_Usage.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }

            string strSql = "SELECT 	COUNT(DISTINCT SubscriberID) as subscriber_count, COUNT(DISTINCT UserID) as user_count, COUNT(DISTINCT query_type) as type_count, COUNT(DISTINCT np_Usage.id) as product_count "
                            + "FROM np_Usage INNER JOIN au_users ON np_Usage.UserID = au_users.id WHERE created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "' AND created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "'"
                            + strWhere;

            DataSet mySet = executeSelectStatement(strSql);

            strSql = "SELECT COUNT(np_Usage.query_type) AS type_count, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     + "FROM np_Usage INNER JOIN np_UsageTypes ON np_Usage.query_type = np_UsageTypes.id "
                     + "WHERE (np_Usage.created >= '"
                     + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                     + "') AND (np_Usage.created < '"
                     + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                     + "') "
                     + strWhere
                     + "GROUP BY np_Usage.query_type, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     + "ORDER BY COUNT(np_Usage.query_type) DESC";

            mySet.Tables.Add(executeSelectStatementDataTable(strSql));
            return mySet;
        }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(
            DateTime dtFrom, DateTime dtTo, XBool IsBillable, XBool IsSubscriberOpen, XBool IsUserOpen) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = " AND np_Usage.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }
            if (IsSubscriberOpen != null) {
                strWhere += "AND au_Subscribers.IsOpen = '" + IsSubscriberOpen + "' ";
            }
            if (IsUserOpen != null) {
                strWhere += "AND au_users.IsOpen = '" + IsUserOpen + "' ";
            }

            string strSql = "SELECT 	COUNT(DISTINCT SubscriberID) as subscriber_count, COUNT(DISTINCT UserID) as user_count, COUNT(DISTINCT query_type) as type_count, COUNT(DISTINCT np_Usage.id) as product_count "
                            +
                            "FROM au_Subscribers RIGHT OUTER JOIN au_users ON au_Subscribers.CreditInfoID = au_users.SubscriberID RIGHT OUTER JOIN np_Usage ON au_users.id = np_Usage.UserID "
                            + "WHERE (np_Usage.created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "') AND (np_Usage.created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "') "
                            + strWhere;

            DataSet mySet = executeSelectStatement(strSql);

            strSql = "SELECT COUNT(np_Usage.query_type) AS type_count, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     +
                     "FROM au_Subscribers RIGHT OUTER JOIN au_users ON au_Subscribers.CreditInfoID = au_users.SubscriberID RIGHT OUTER JOIN np_Usage ON au_users.id = np_Usage.UserID INNER JOIN np_UsageTypes ON np_Usage.query_type = np_UsageTypes.id "
                     + "WHERE (np_Usage.created >= '"
                     + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                     + "') AND (np_Usage.created < '"
                     + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                     + "') "
                     + strWhere
                     + "GROUP BY np_Usage.query_type, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     + "ORDER BY COUNT(np_Usage.query_type) DESC";

            mySet.Tables.Add(executeSelectStatementDataTable(strSql));
            return mySet;
        }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by Subscriber info.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <param name="strSubscriberIDNumber">Number identifying the subscriber</param>
        /// <param name="strSubscriberName">Subscribers name</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(
            DateTime dtFrom,
            DateTime dtTo,
            XBool IsBillable,
            XBool IsSubscriberOpen,
            XBool IsUserOpen,
            string strSubscriberIDNumber,
            string strSubscriberName) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = " AND stats_UsageBySubscriber.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }
            // Subscribers/Users active true/false/both
            if (IsSubscriberOpen != null) {
                strWhere += "AND subsisopen = '" + IsSubscriberOpen + "' ";
            }
            if (IsUserOpen != null) {
                strWhere += "AND userisopen = '" + IsUserOpen + "' ";
            }
            // User search
            if (!string.IsNullOrEmpty(strSubscriberIDNumber)) {
                strWhere += "AND number=N'" + strSubscriberIDNumber + "' ";
            }
            if (strSubscriberName != null & strSubscriberName.Length > 0) {
                strWhere += "AND (NameEn LIKE '%" + strSubscriberName + "%' OR NameNative LIKE N'%" + strSubscriberName +
                            "%')";
            }

            string strSql = "SELECT 	COUNT(DISTINCT SubscriberID) as subscriber_count, COUNT(DISTINCT UserID) as user_count, COUNT(DISTINCT query_type) as type_count, COUNT(DISTINCT id) as product_count "
                            + "FROM stats_UsageBySubscriber WHERE created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "' AND created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "' "
                            + strWhere;

            DataSet mySet = executeSelectStatement(strSql);

            strSql = "SELECT COUNT(stats_UsageBySubscriber.query_type) AS type_count, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     +
                     "FROM stats_UsageBySubscriber INNER JOIN np_UsageTypes ON stats_UsageBySubscriber.query_type = np_UsageTypes.id "
                     + "WHERE (stats_UsageBySubscriber.created >= '"
                     + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                     + "') AND (stats_UsageBySubscriber.created < '"
                     + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                     + "') "
                     + strWhere
                     + "GROUP BY stats_UsageBySubscriber.query_type, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     + "ORDER BY COUNT(stats_UsageBySubscriber.query_type) DESC";

            mySet.Tables.Add(executeSelectStatementDataTable(strSql));
            return mySet;
        }

        /// <summary>
        /// Gets usage statistic for a specific period by subscriber ID.
        /// </summary>
        /// <param name="dtFrom">Begin date.</param>
        /// <param name="dtTo">End date.</param>
        /// <param name="IsBillable">Get billed events or not.</param>
        /// <param name="SubscriberID">Unique ID for the subscriber.</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(DateTime dtFrom, DateTime dtTo, XBool IsBillable, int SubscriberID) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = " AND stats_UsageBySubscriber.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }

            // User search
            if (SubscriberID > -1) {
                strWhere += "AND subscriberID=" + SubscriberID;
            }

            string strSql = "SELECT 	COUNT(DISTINCT SubscriberID) as subscriber_count, COUNT(DISTINCT UserID) as user_count, COUNT(DISTINCT query_type) as type_count, COUNT(DISTINCT id) as product_count "
                            + "FROM stats_UsageBySubscriber WHERE created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "' AND created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "' "
                            + strWhere;

            DataSet mySet = executeSelectStatement(strSql);

            strSql = "SELECT COUNT(stats_UsageBySubscriber.query_type) AS type_count, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     +
                     "FROM stats_UsageBySubscriber INNER JOIN np_UsageTypes ON stats_UsageBySubscriber.query_type = np_UsageTypes.id "
                     + "WHERE (stats_UsageBySubscriber.created >= '"
                     + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                     + "') AND (stats_UsageBySubscriber.created < '"
                     + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                     + "') "
                     + strWhere
                     + "GROUP BY stats_UsageBySubscriber.query_type, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     + "ORDER BY COUNT(stats_UsageBySubscriber.query_type) DESC";

            mySet.Tables.Add(executeSelectStatementDataTable(strSql));
            return mySet;
        }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by User info.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <param name="strUserIDNumber">Number identifying the user</param>
        /// <param name="strUserName">Name of the user</param>
        /// <param name="strUserUsername">Users username</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageCounts(
            DateTime dtFrom,
            DateTime dtTo,
            XBool IsBillable,
            XBool IsSubscriberOpen,
            XBool IsUserOpen,
            string strUserIDNumber,
            string strUserName,
            string strUserUsername) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = " AND stats_UsageByUser.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }
            // Subscribers/Users active true/false/both
            if (IsSubscriberOpen != null) {
                strWhere += "AND subsisopen = '" + IsSubscriberOpen + "' ";
            }
            if (IsUserOpen != null) {
                strWhere += "AND userisopen = '" + IsUserOpen + "' ";
            }
            // User search
            if (!string.IsNullOrEmpty(strUserIDNumber)) {
                strWhere += "AND UserIdNumber=N'" + strUserIDNumber + "' ";
            }
            if (!string.IsNullOrEmpty(strUserUsername)) {
                strWhere += "AND username LIKE '%" + strUserUsername + "%' ";
            }
            if (strUserName != null & strUserName.Length > 0) {
                strWhere += "AND (User_NameEn LIKE '%" + strUserName + "%' OR User_NameNative LIKE N'%" + strUserName +
                            "%')";
            }

            string strSql = "SELECT 	COUNT(DISTINCT SubscriberID) as subscriber_count, COUNT(DISTINCT UserID) as user_count, COUNT(DISTINCT query_type) as type_count, COUNT(DISTINCT id) as product_count "
                            + "FROM stats_UsageByUser WHERE created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "' AND created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "' "
                            + strWhere;

            DataSet mySet = executeSelectStatement(strSql);

            strSql = "SELECT COUNT(stats_UsageByUser.query_type) AS type_count, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     +
                     "FROM stats_UsageByUser INNER JOIN np_UsageTypes ON stats_UsageByUser.query_type = np_UsageTypes.id "
                     + "WHERE (stats_UsageByUser.created >= '"
                     + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                     + "') AND (stats_UsageByUser.created < '"
                     + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                     + "') "
                     + strWhere
                     + "GROUP BY stats_UsageByUser.query_type, np_UsageTypes.typeEN, np_UsageTypes.typeNative "
                     + "ORDER BY COUNT(stats_UsageByUser.query_type) DESC";

            mySet.Tables.Add(executeSelectStatementDataTable(strSql));
            return mySet;
        }

        /// <summary>
        /// Gets all subscribers that have preformed an event the system in a specific period.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable">Event billed or not</param>
        /// <returns>DataSet with the subscribers and there stats</returns>
        public DataSet GetUsageSubscribers(DateTime dtFrom, DateTime dtTo, XBool IsBillable) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = "AND stats_UsageBySubscriber.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }

            string strSql = "SELECT stats_UsageBySubscriber.Creditinfoid, COUNT(stats_UsageBySubscriber.query_type) AS type_count, "
                            +
                            "stats_UsageBySubscriber.NameEN, stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username, "
                            +
                            "np_UsageTypes.typeEN, np_UsageTypes.typeNative, stats_UsageBySubscriber.query_type, stats_UsageBySubscriber.Number "
                            + "FROM stats_UsageBySubscriber INNER JOIN "
                            + "np_UsageTypes ON stats_UsageBySubscriber.query_type = np_UsageTypes.id "
                            + "WHERE (stats_UsageBySubscriber.created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "') AND (stats_UsageBySubscriber.created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "') "
                            + strWhere
                            +
                            "GROUP BY stats_UsageBySubscriber.query_type, stats_UsageBySubscriber.Creditinfoid, stats_UsageBySubscriber.NameEN, "
                            + "stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username, np_UsageTypes.typeEN, "
                            + "np_UsageTypes.typeNative, dbo.stats_UsageBySubscriber.Number "
                            + "ORDER BY stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username";

            DataSet mySet = executeSelectStatement(strSql);
            return mySet;
        }

        /// <summary>
        /// Gets all subscribers that have preformed an event the system in a specific period.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable">Event billed or not</param>
        /// <returns>DataSet with the subscribers and there stats</returns>
        public DataSet GetNonUsageSubscribersUsers(DateTime dtFrom, DateTime dtTo,  XBool IsSubscriberOpen, XBool IsUserOpen,XBool IsBillable )
        {
            //string strWhere = " AND stats_UsageBySubscriber.ID is null ";
            //StringBuilder sb = new StringBuilder();
            //sb.Append("SELECT     dbo.au_users.CreditInfoID AS Number,dbo.au_users.UserName as Username, i.FirstNameEN +  ' ' + i.SurNameEN AS NameEn , c.NameNative  + ' | ' + i.FirstNameNative + ' ' + i.SurNameNative as NameNative  ");
            //sb.Append("FROM  dbo.au_users INNER JOIN dbo.np_Individual as i on i.CreditInfoID = dbo.au_users.CreditInfoID LEFT OUTER JOIN dbo.np_Companys as c on c.CreditInfoID = dbo.au_users.SubscriberID ");
            //sb.Append("WHERE dbo.au_users.Id not in  ( ");
            //sb.Append("SELECT UserID FROM  dbo.np_Usage ");
            //sb.Append("WHERE ( created >= '" + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day + "') AND ( created < '" + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day + "') ");
            //sb.Append(") Order by c.NameNative, dbo.au_users.UserName ");
            //if (IsUserOpen != null)
            //{
            //    string sValue;
            //    if (IsUserOpen.Value)
            //    {
            //        sValue = "True";
            //    }
            //    else
            //    {
            //        sValue = "False";
            //    }
            //    sb.Append("AND dbo.au_users.IsOpen ='" + sValue + "' ");
            //}


            StringBuilder sqlString = new StringBuilder();
            sqlString.Append("SELECT * FROM ( ");
            sqlString.Append("SELECT u.CreditInfoId as Number, u.Id, u.SubscriberId, u.UserName as Username, ");
            sqlString.Append("ISNULL(c.NameNative, (ltrim(rtrim(i.FirstNameNative + ' ' + i.SurNameNative)))) as NameNative, ");
            sqlString.Append("ISNULL(c.NameEn, (ltrim(rtrim(i.FirstNameEn + ' ' + i.SurNameEn)))) as NameEn ");
            sqlString.Append("FROM dbo.au_users as u ");
            sqlString.Append("LEFT OUTER JOIN dbo.np_Companys as c ON c.CreditInfoId = u.SubscriberId ");
            sqlString.Append("LEFT OUTER JOIN dbo.np_Individual as i ON i.CreditInfoId = u.SubscriberId ");
            sqlString.Append("LEFT OUTER JOIN dbo.np_CreditInfoUser as ciu ON ciu.CreditInfoID = u.SubscriberId ");
            sqlString.Append("WHERE u.Id NOT IN ");
            sqlString.Append("(SELECT UserID FROM  dbo.np_Usage WHERE ( created >= '" + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day + "') AND ( created < '" + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day + "') ");
            if (IsBillable != null)
            {
                if (IsBillable.Value)
                {
                    sqlString.Append("AND IsBillable = 1 ");
                }
                else
                {
                    sqlString.Append("AND IsBillable = 0");
                }
            }
            sqlString.Append(") ");
            
            if (IsUserOpen != null)
            {
                if (IsUserOpen.Value)
                {
                    sqlString.Append("AND u.IsOpen = 'True'");
                }
                else 
                {
                    sqlString.Append("AND u.IsOpen = 'False'");
                }
            }

            if (IsSubscriberOpen != null)
            {
                if (IsSubscriberOpen.Value)
                {
                    sqlString.Append("AND ciu.IsSearchable = 'True' ");
                }
                else
                {
                    sqlString.Append("AND ciu.IsSearchable = 'False' ");
                }
            }

            sqlString.Append(") as a ");
            sqlString.Append("ORDER BY a.NameNative, a.UserName");


            DataSet mySet = executeSelectStatement(sqlString.ToString());

            return mySet;
        }


        public DataSet GetNonUsageSubscribers(DateTime dtFrom, DateTime dtTo) {
            string strSql = "SELECT u.Creditinfoid as Number,c.NameEN,c.NameNative,u.Username "
                            + "FROM au_users as u "
                            + "JOIN au_subscribers as s on s.creditinfoid = u.subscriberid "
                            + "JOIN np_Companys as c on c.creditinfoid = u.subscriberid "
                            + "LEFT OUTER JOIN ( "
                            + "SELECT dbo.np_Usage.UserId "
                            + "FROM dbo.np_Usage "
                            + "WHERE (np_Usage.created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "') AND (np_Usage.created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "') "
                            + "GROUP BY np_Usage.UserId "
                            + ") np_Usage1 ON u.id = np_Usage1.Userid WHERE (np_Usage1.Userid is null) "
                            + "ORDER BY c.NameNative, u.Username";

            DataSet mySet = executeSelectStatement(strSql);
            return mySet;
        }
        /// <summary>
        /// Return DataSet of Subscribers without any user with usages for selected date.
        /// </summary>
        /// <param name="dtFrom"></param>
        /// <param name="dtTo"></param>
        /// <returns></returns>
        public DataSet GetNonUsageBySubscribers(DateTime dtFrom, DateTime dtTo, XBool IsBillable, XBool IsSubscriberOpen, XBool IsUserOpen)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.Append("SELECT ciu.CreditInfoId  , n.Number, ");
            sqlString.Append("ISNULL(c.NameNative, (ltrim(rtrim(i.FirstNameNative + ' ' + i.SurNameNative)))) as NameNative, '' as UserName, ");
            sqlString.Append("ISNULL(c.NameEn, (ltrim(rtrim(i.FirstNameEn + ' ' + i.SurNameEn)))) as NameEn ");
            sqlString.Append("FROM dbo.au_Subscribers as ciu ");
            sqlString.Append("LEFT OUTER JOIN dbo.np_Companys as c ON c.CreditInfoId = ciu.CreditInfoId ");
            sqlString.Append("LEFT OUTER JOIN dbo.np_Individual as i ON i.CreditInfoId = ciu.CreditInfoId ");
            sqlString.Append("LEFT OUTER JOIN dbo.np_CreditInfoUser as ci ON ci.CreditInfoID = ciu.CreditInfoId ");
            sqlString.Append("LEFT JOIN dbo.np_IdNumbers as n ON n.CreditInfoID = ciu.CreditInfoId ");
            sqlString.Append("WHERE ciu.CreditInfoId  NOT  IN ");
            sqlString.Append("(SELECT SubscriberId FROM ( SELECT SubscriberId FROM dbo.au_users as u WHERE u.Id  IN ");

            sqlString.Append("(SELECT UserID FROM  dbo.np_Usage WHERE ( created >= '" + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day + "') AND ( created < '" + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day + "') ");
            if (IsBillable != null)
            {
                if (IsBillable.Value)
                {
                    sqlString.Append("AND IsBillable = 1 ");
                }
                else
                {
                    sqlString.Append("AND IsBillable = 0");
                }
            }
            sqlString.Append(") ");

            if (IsUserOpen != null)
            {
                if (IsUserOpen.Value)
                {
                    sqlString.Append("AND u.IsOpen = 'True'");
                }
                else
                {
                    sqlString.Append("AND u.IsOpen = 'False'");
                }
            }

            sqlString.Append("GROUP BY SubscriberId) as s ) ");

            if (IsSubscriberOpen != null)
            {
                if (IsSubscriberOpen.Value)
                {
                    sqlString.Append("AND ci.IsSearchable = 'True' ");
                }
                else
                {
                    sqlString.Append("AND ci.IsSearchable = 'False' ");
                }
            }

            DataSet mySet = executeSelectStatement(sqlString.ToString());
            return mySet;
        }


        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageSubscribers(
            DateTime dtFrom, DateTime dtTo, XBool IsBillable, XBool IsSubscriberOpen, XBool IsUserOpen) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = "AND stats_UsageBySubscriber.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }
            if (IsSubscriberOpen != null) {
                strWhere += "AND subsisopen = '" + IsSubscriberOpen + "' ";
            }
            if (IsUserOpen != null) {
                strWhere += "AND userisopen = '" + IsUserOpen + "' ";
            }

            string strSql = "SELECT stats_UsageBySubscriber.Creditinfoid, COUNT(stats_UsageBySubscriber.query_type) AS type_count, "
                            +
                            "stats_UsageBySubscriber.NameEN, stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username, "
                            +
                            "np_UsageTypes.typeEN, np_UsageTypes.typeNative, stats_UsageBySubscriber.query_type, stats_UsageBySubscriber.Number "
                            + "FROM stats_UsageBySubscriber INNER JOIN "
                            + "np_UsageTypes ON stats_UsageBySubscriber.query_type = np_UsageTypes.id "
                            + "WHERE (stats_UsageBySubscriber.created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "') AND (stats_UsageBySubscriber.created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "') "
                            + strWhere
                            +
                            "GROUP BY stats_UsageBySubscriber.query_type, stats_UsageBySubscriber.Creditinfoid, stats_UsageBySubscriber.NameEN, "
                            + "stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username, np_UsageTypes.typeEN, "
                            + "np_UsageTypes.typeNative, dbo.stats_UsageBySubscriber.Number "
                            + "ORDER BY stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username";

            DataSet mySet = executeSelectStatement(strSql);
            return mySet;
        }

        public DataSet GetNonUsageSubscribers(DateTime dtFrom, DateTime dtTo, XBool IsSubscriberOpen, XBool IsUserOpen) {
            string strWhere = "";

            if (IsSubscriberOpen != null) {
                strWhere += "AND s.isopen = '" + IsSubscriberOpen + "' ";
            }
            if (IsUserOpen != null) {
                strWhere += "AND u.isopen = '" + IsUserOpen + "' ";
            }

            string strSql = "SELECT s.[CreditInfoID] as Number, c.NameEN,c.NameNative, '' as Username "
                            + "FROM [dbo].[au_Subscribers] S "
                            + "INNER JOIN np_Companys as c on c.creditinfoid = s.CreditInfoID   "
                            + "where (select count (*) from [dbo].[np_Usage] Ug "
                            + "inner join [dbo].[au_users] Us on Us.id = Ug.UserId "
                            + "where created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "' AND created <= '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "' and us.subscriberID = S.creditinfoid and Ug.Userid is null) = 0 "
                            + strWhere;

            /*
			strSql="SELECT stats_Subscribers.Creditinfoid,"
				+ "stats_Subscribers.NameEN, stats_Subscribers.NameNative,stats_Subscribers.Username,stats_Subscribers.id,  A1.created "
				+ "FROM stats_Subscribers "
				+ "LEFT OUTER JOIN ( "
				+ "SELECT dbo.np_Usage.UserId,dbo.np_Usage.created  "
				+ "FROM dbo.np_Usage "
				+ "WHERE (np_Usage.created >= '"
				+ dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
				+ "') AND (np_Usage.created < '"
				+ dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day 
				+ "')) "
				+ "A1 ON stats_Subscribers.id = A1.Userid WHERE (created is null) "
				+ strWhere
				+ "GROUP BY id, Creditinfoid, Username, NameNative,NameEN, created "
				+ "ORDER BY NameNative, Username ";	
			*/
            DataSet mySet = executeSelectStatement(strSql);
            return mySet;
        }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by Subscriber info.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <param name="strSubscriberIDNumber">Number identifying the subscriber</param>
        /// <param name="strSubscriberName">Subscribers name</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageSubscribers(
            DateTime dtFrom,
            DateTime dtTo,
            XBool IsBillable,
            XBool IsSubscriberOpen,
            XBool IsUserOpen,
            string strSubscriberIDNumber,
            string strSubscriberName) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = "AND stats_UsageBySubscriber.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }
            // Subscribers/Users active true/false/both
            if (IsSubscriberOpen != null) {
                strWhere += "AND subsisopen = '" + IsSubscriberOpen + "' ";
            }
            if (IsUserOpen != null) {
                strWhere += "AND userisopen = '" + IsUserOpen + "' ";
            }
            // User search
            if (!string.IsNullOrEmpty(strSubscriberIDNumber)) {
                strWhere += "AND number=N'" + strSubscriberIDNumber + "' ";
            }
            if (strSubscriberName != null & strSubscriberName.Length > 0) {
                strWhere += "AND (NameEn LIKE '%" + strSubscriberName + "%' OR NameNative LIKE N'%" + strSubscriberName +
                            "%')";
            }

            string strSql = "SELECT stats_UsageBySubscriber.Creditinfoid, COUNT(stats_UsageBySubscriber.query_type) AS type_count, "
                            +
                            "stats_UsageBySubscriber.NameEN, stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username, "
                            +
                            "np_UsageTypes.typeEN, np_UsageTypes.typeNative, stats_UsageBySubscriber.query_type, stats_UsageBySubscriber.Number "
                            + "FROM stats_UsageBySubscriber INNER JOIN "
                            + "np_UsageTypes ON stats_UsageBySubscriber.query_type = np_UsageTypes.id "
                            + "WHERE (stats_UsageBySubscriber.created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "') AND (stats_UsageBySubscriber.created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "') "
                            + strWhere
                            +
                            "GROUP BY stats_UsageBySubscriber.query_type, stats_UsageBySubscriber.Creditinfoid, stats_UsageBySubscriber.NameEN, "
                            + "stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username, np_UsageTypes.typeEN, "
                            + "np_UsageTypes.typeNative, dbo.stats_UsageBySubscriber.Number "
                            + "ORDER BY stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username";

            DataSet mySet = executeSelectStatement(strSql);
            return mySet;
        }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by Subscriber info.
        /// </summary>
        /// <param name="dtFrom">Begin date.</param>
        /// <param name="dtTo">End date.</param>
        /// <param name="IsBillable"></param>
        /// <param name="SubscriberID">Unique ID for the subscriber.</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count).</returns>
        public DataSet GetUsageSubscribers(DateTime dtFrom, DateTime dtTo, XBool IsBillable, int SubscriberID) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = "AND stats_UsageBySubscriber.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }

            // User search
            if (SubscriberID > -1) {
                strWhere += "AND subscriberID=" + SubscriberID;
            }

            string strSql = "SELECT stats_UsageBySubscriber.Creditinfoid, COUNT(stats_UsageBySubscriber.query_type) AS type_count, "
                            +
                            "stats_UsageBySubscriber.NameEN, stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username, "
                            +
                            "np_UsageTypes.typeEN, np_UsageTypes.typeNative, stats_UsageBySubscriber.query_type, stats_UsageBySubscriber.Number "
                            + "FROM stats_UsageBySubscriber INNER JOIN "
                            + "np_UsageTypes ON stats_UsageBySubscriber.query_type = np_UsageTypes.id "
                            + "WHERE (stats_UsageBySubscriber.created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "') AND (stats_UsageBySubscriber.created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "') "
                            + strWhere
                            +
                            "GROUP BY stats_UsageBySubscriber.query_type, stats_UsageBySubscriber.Creditinfoid, stats_UsageBySubscriber.NameEN, "
                            + "stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username, np_UsageTypes.typeEN, "
                            + "np_UsageTypes.typeNative, dbo.stats_UsageBySubscriber.Number "
                            + "ORDER BY stats_UsageBySubscriber.NameNative, stats_UsageBySubscriber.Username";

            DataSet mySet = executeSelectStatement(strSql);
            return mySet;
        }

        /// <summary>
        /// Gets usage statistic for a specific period by Open/close subscribers/users by User info.
        /// </summary>
        /// <param name="dtFrom">Begin date</param>
        /// <param name="dtTo">End date</param>
        /// <param name="IsBillable"></param>
        /// <param name="IsSubscriberOpen">Subscribers open or closed</param>
        /// <param name="IsUserOpen">Users open or closed</param>
        /// <param name="strUserIDNumber">Number identifying the user</param>
        /// <param name="strUserName">Name of the user</param>
        /// <param name="strUserUsername">Users username</param>
        /// <returns>Data row with the results. 4 fields (subscriber_count,user_count,type_count,product_count)</returns>
        public DataSet GetUsageSubscribers(
            DateTime dtFrom,
            DateTime dtTo,
            XBool IsBillable,
            XBool IsSubscriberOpen,
            XBool IsUserOpen,
            string strUserIDNumber,
            string strUserName,
            string strUserUsername) {
            string strWhere = "";

            if (IsBillable != null) {
                strWhere = "AND stats_UsageByUser.IsBillable=" + Convert.ToByte(IsBillable.Value) + " ";
            }
            // Subscribers/Users active true/false/both
            if (IsSubscriberOpen != null) {
                strWhere += "AND subsisopen = '" + IsSubscriberOpen + "' ";
            }
            if (IsUserOpen != null) {
                strWhere += "AND userisopen = '" + IsUserOpen + "' ";
            }
            // User search
            if (!string.IsNullOrEmpty(strUserIDNumber)) {
                strWhere += "AND UserIdNumber=N'" + strUserIDNumber + "' ";
            }
            if (!string.IsNullOrEmpty(strUserUsername)) {
                strWhere += "AND username LIKE '%" + strUserUsername + "%' ";
            }
            if (strUserName != null & strUserName.Length > 0) {
                strWhere += "AND (User_NameEn LIKE '%" + strUserName + "%' OR User_NameNative LIKE N'%" + strUserName +
                            "%')";
            }

            string strSql = "SELECT stats_UsageByUser.Creditinfoid, COUNT(stats_UsageByUser.query_type) AS type_count, "
                            + "stats_UsageByUser.NameEN, stats_UsageByUser.NameNative, stats_UsageByUser.Username, "
                            +
                            "np_UsageTypes.typeEN, np_UsageTypes.typeNative, stats_UsageByUser.query_type, stats_UsageByUser.Number "
                            + "FROM stats_UsageByUser INNER JOIN "
                            + "np_UsageTypes ON stats_UsageByUser.query_type = np_UsageTypes.id "
                            + "WHERE (stats_UsageByUser.created >= '"
                            + dtFrom.Year + "-" + dtFrom.Month + "-" + dtFrom.Day
                            + "') AND (stats_UsageByUser.created < '"
                            + dtTo.Year + "-" + dtTo.Month + "-" + dtTo.Day
                            + "') "
                            + strWhere
                            +
                            "GROUP BY stats_UsageByUser.query_type, stats_UsageByUser.Creditinfoid, stats_UsageByUser.NameEN, "
                            + "stats_UsageByUser.NameNative, stats_UsageByUser.Username, np_UsageTypes.typeEN, "
                            + "np_UsageTypes.typeNative, dbo.stats_UsageByUser.Number "
                            + "ORDER BY stats_UsageByUser.NameNative, stats_UsageByUser.Username";

            DataSet mySet = executeSelectStatement(strSql);
            return mySet;
        }

        /// <summary>
        /// Gets a report indicating who and when request information about an Ciid.
        /// </summary>
        /// <param name="ciid">CreditinfoId of a user.</param>
        /// <param name="from">Date and time from.</param>
        /// <param name="to">Date and time to.</param>
        /// <param name="filterOutEmployees">Should employees be filtered out or included in the results</param>
        /// <returns>A dataset containg request report.</returns>
        public DataSet GetRequestReport(string ciid, DateTime from, DateTime to, bool filterOutEmployees) {
            OleDbCommand command = filterOutEmployees ? new OleDbCommand(
                                                            "SELECT	query_type, typeNative, typeEN, created, Creditinfoid, UserNameNative, UserNameEN, SubscriberNameNative, SubscriberNameEN FROM vm_stats_DataUsage WHERE (not UserType = 'Employee') AND query=? AND created>? AND created<=? ORDER BY Created DESC") : new OleDbCommand(
                                                                                                                                                                                                                                                                                                                               "SELECT	query_type, typeNative, typeEN, created, Creditinfoid, UserNameNative, UserNameEN, SubscriberNameNative, SubscriberNameEN FROM vm_stats_DataUsage WHERE query=? AND created>? AND created<=? ORDER BY Created DESC");

            var param = new OleDbParameter("@number", OleDbType.VarWChar, 100)
                        {
                            Direction = ParameterDirection.Input,
                            Value = ciid
                        };
            command.Parameters.Add(param);

            param = new OleDbParameter("@from", OleDbType.Date) {Direction = ParameterDirection.Input, Value = from};
            command.Parameters.Add(param);

            param = new OleDbParameter("@to", OleDbType.Date) {Direction = ParameterDirection.Input, Value = to};
            command.Parameters.Add(param);

            var adapter = new OleDbDataAdapter(command);
            return executeSelectStatement(adapter);
        }

        #endregion

        private static DataSet executeSelectStatement(OleDbDataAdapter adapter) {
            var FunctionName = "executeSelectStatement(OleDbDataAdapter adapter)";

            var ds = new DataSet("CigData");
            using (var connection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                connection.Open();
                adapter.SelectCommand.Connection = connection;
                try {
                    adapter.Fill(ds);
                } catch (Exception e) {
                    ds = null;
                    Logger.WriteToLog("StatisticDALC" + " : " + FunctionName + e.Message, true);
                }
                return ds;
            }
        }

        private static DataSet executeSelectStatement(string query) {
            const string FunctionName = "executeSelectStatement(string query)";
            

            var ds = new DataSet();
            var myAdapter = new OleDbDataAdapter();
            var myOleConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            try {
                myOleConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleConn);
                myAdapter.Fill(ds);
                return ds;
            } catch (Exception e) {
                Logger.WriteToLog("StatisticDALC" + " : " + FunctionName + e.Message, true);
            } finally {
                if (myOleConn != null) {
                    myOleConn.Close();
                }
            }
            return null;
        }

        private static DataTable executeSelectStatementDataTable(string query) {
            const string FunctionName = "executeSelectStatementDataTable(string query)";
            

            var dt = new DataTable();
            var myAdapter = new OleDbDataAdapter();
            var myOleConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            try {
                myOleConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleConn);
                myAdapter.Fill(dt);
                return dt;
            } catch (Exception e) {
                Logger.WriteToLog("StatisticDALC" + " : " + FunctionName + e.Message, true);
            } finally {
                if (myOleConn != null) {
                    myOleConn.Close();
                }
            }
            return null;
        }

    }
}