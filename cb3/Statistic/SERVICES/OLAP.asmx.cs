using System;
using System.ComponentModel;
using System.Configuration;
using System.Web.Services;
using Logging.BLL;
using OWC10;
using Statistic.BLL;

using Cig.Framework.Base.Configuration;

namespace Statistic.services {
    /// <summary>
    /// Summary description for OLAP.
    /// </summary>
    [WebService(Namespace = "https://ws.lt.is/CreditInfoGroup/", Description = "CIG OLAP Cube supporting services.")]
    public class OLAP : WebService {
        public OLAP() {
            //CODEGEN: This call is required by the ASP.NET Web Services Designer
            InitializeComponent();
        }

        #region Component Designer generated code

        //Required by the Web Services Designer 
        private IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if (disposing && components != null) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        [WebMethod]
        public string InitializePivotTableXML(string strDataMember) {
            string strXml;
            string strOLAPConn = CigConfig.Configure("lookupsettings.OLAPConnectionString");

            try {
                var factory = new StatisticFactory();
                var objPT = new PivotTableClass
                            {
                                ConnectionString = strOLAPConn,
                                DataMember = factory.GetCube(int.Parse(strDataMember)).NameDB
                            };
                strXml = objPT.XMLData;
            } catch (Exception err) {
                strXml = "<err>" + err.Source + " - " + err.Message + "</err>";
                Logger.WriteToLog("Statistic.services.InitializePivotTableXML(" + strDataMember + "):" + strXml, true);
                Logger.WriteToLog(
                    "Statistic.services.InitializePivotTableXML(" + strDataMember + "): \n" + err.StackTrace, false);
            }
            return strXml;
        }

        [WebMethod]
        public string LoadCustomPivotTableReport(string strCity1, string strCity2) {
            string strXml;
            string strOLAPConn = CigConfig.Configure("lookupsettings.OLAPConnectionString");
            var objPT = new PivotTableClass();
            PivotView objPTView;
            PivotField fldCity, fldProdFamily;
            PivotFieldSet fSetCustomers, fSetProduct;
            try {
                //set the connection string and data member
                objPT.ConnectionString = strOLAPConn;
                objPT.DataMember = "Sales";
                //objPT.AllowPropertyToolbox = False
                objPT.AllowFiltering = false;
                objPTView = objPT.ActiveView;
                objPTView.TitleBar.Caption = "City Comparison of Drink Sales";
                //define the column elements
                objPTView.ColumnAxis.InsertFieldSet(objPTView.FieldSets["Time"], null, false);
                objPTView.ColumnAxis.FieldSets["Time"].Fields["Year"].Expanded = true;
                //define the row elements
                fSetCustomers = objPTView.FieldSets["Customers"];
                objPTView.RowAxis.InsertFieldSet(fSetCustomers, null, false);
                fSetCustomers.Fields["Country"].IsIncluded = false;
                fSetCustomers.Fields["State Province"].IsIncluded = false;
                fSetCustomers.Fields["Name"].IsIncluded = false;
                //define the members of the row elements
                fldCity = fSetCustomers.Fields["City"];
                fldCity.IncludedMembers = new Object[] {strCity1, strCity2};
                //exclude all other field row members in the fieldset
                fSetProduct = objPTView.FieldSets["Product"];
                objPTView.RowAxis.InsertFieldSet(fSetProduct, null, false);
                fSetProduct.Fields["Product Department"].IsIncluded = false;
                fSetProduct.Fields["Product Category"].IsIncluded = false;
                fSetProduct.Fields["Product Subcategory"].IsIncluded = false;
                fSetProduct.Fields["Brand Name"].IsIncluded = false;
                fSetProduct.Fields["Product Name"].IsIncluded = false;
                fldProdFamily = fSetProduct.Fields["Product Family"];
                fldProdFamily.IncludedMembers = "Drink";
                // define the measures
                //objPTView.DataAxis.InsertTotal(objPTView.Totals("Unit Sales"))
                objPTView.DataAxis.InsertTotal(objPTView.Totals["Store Sales"], null);
                objPTView.DataAxis.Totals["Store Sales"].NumberFormat = "Currency";
                //dim ChartSpace1 As ChartSpaceClass = New ChartSpaceClass
                //ChartSpace1.Charts(0).Type() = ChartChartTypeEnum.chChartTypeBar3D

                strXml = objPT.XMLData;
            } catch (Exception err) {
                strXml = "<err>" + err.Source + " - " + err.Message + "</err>";
            }
            return strXml;
        }

        [WebMethod]
        public string ApplyCustomGrouping(string strReportXMLData) {
            string strXml;
            var objPT = new PivotTableClass();
            PivotView objPTView;
            PivotFieldSet fsTime;
            PivotField fsHalfYear;

            try {
                strReportXMLData = strReportXMLData.Replace("{", "<");
                strReportXMLData = strReportXMLData.Replace("}", ">");
                objPT.XMLData = strReportXMLData;
                objPTView = objPT.ActiveView;
                //Set a variable to the Time field set. 
                fsTime = objPTView.FieldSets["Time"];

                //Add a custom group field named "Group1" to the Time field set. 
                fsHalfYear = fsTime.AddCustomGroupField("CustomGroup1", "CustomGroup1", "Quarter");
                //Add a custom field set member. This member includes all "Q1" 
                //and "Q2" members under 1997. 
                fsHalfYear.AddCustomGroupMember(
                    fsTime.Member.ChildMembers["1997"].Name, new Object[] {"Q1", "Q2"}, "1stHalf");

                //Add another custom fieldset member to include all "Q3" and "Q4" members under 1997. 
                fsHalfYear.AddCustomGroupMember(
                    fsTime.Member.ChildMembers["1997"].Name, new Object[] {"Q3", "Q4"}, "2ndHalf");

                //Collapse the fieldset at the custom member level
                fsHalfYear.Expanded = false;

                strXml = objPT.XMLData;
            } catch (Exception err) {
                strXml = "<err>" + err.Source + " - " + err.Message + "</err>";
            }
            return strXml;
        }

        [WebMethod]
        public string SaveReport(
            string strReportXMLData,
            string strName,
            string strCreditInfoID,
            int nCubeID,
            bool isPublic,
            string sReportID) {
            string strXml = "";
            try {
                var factory = new StatisticFactory();
                strReportXMLData = strReportXMLData.Replace("{", "<");
                strReportXMLData = strReportXMLData.Replace("}", ">");
                var report = new ReportData();
                try {
                    report.ID = int.Parse(sReportID);
                    report = factory.GetReport(report.ID);

                    if (!report.Name.Equals(strName)) {
                        report.ID = -1;
                        report.Created = DateTime.MinValue;
                    }
                } catch (Exception) {
                    report.ID = -1;
                }
                report.CreditinfoID = int.Parse(strCreditInfoID);
                report.CubeID = nCubeID;
                report.IsPublic = isPublic;
                report.Name = strName;
                report.XmlData = strReportXMLData;

                factory.StoreReport(report);
            } catch (Exception err) {
                strXml = "<err>" + err.Source + " - " + err.Message + "</err>";
            }
            return strXml;
        }

        [WebMethod]
        public string LoadSavedReport(string strReportId) {
            string strXml;
            var objPT = new PivotTableClass();
            var factory = new StatisticFactory();

            try {
                int nReportId = int.Parse(strReportId);
                strXml = factory.GetReportXmlData(nReportId);
                //strFilePath = "C:\\Inetpub\\wwwroot\\OLAPReport\\" + strFileName;
                //strXml = ReadStringFromFile(strFilePath);
                objPT.XMLData = strXml;
                strXml = objPT.XMLData;
            } catch (Exception err) {
                strXml = "<err>" + err.Source + " - " + err.Message + "</err>";
                Logger.WriteToLog("Statistic.services.LoadSavedReport(" + strReportId + "):" + strXml, true);
                Logger.WriteToLog("Statistic.services.LoadSavedReport(" + strReportId + "): \n" + err.StackTrace, false);
            }
            return strXml;
        }
    }
}