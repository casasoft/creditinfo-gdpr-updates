using System;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Statistic.BLL;

namespace Statistic {
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public class Statistic : Page {
        protected static CultureInfo ci;
        protected static ResourceManager rm;
        protected Button btnConnect;
        protected HtmlButton BUTTON1;
        protected HtmlButton BUTTON2;
        protected HtmlButton Button3;
        protected DropDownList ddlCubes;
        protected bool EN;
        protected Label lblName;
        protected Label lblPivotHeader;
        protected Label lblStatistic;
        protected ListBox lbSavedReports;
        protected HtmlInputButton myButton;
        protected HtmlTable outerGridTable;
        protected HtmlInputHidden tbCreditInfoID;
        protected TextBox tbName;

        private void Page_Load(object sender, EventArgs e) {
            tbCreditInfoID.Value = Session["UserCreditInfoID"].ToString();
            //if (culture.Equals("en-US"))
            //	EN = true;

            //rm = CIResource.CurrentManager;
            //ci = Thread.CurrentThread.CurrentCulture;

            var factory = new StatisticFactory();

            //this.btn_NewJudgement_Add.Attributes["onclick"] = "javascript ocument.NewJudgement.judgementText.value = HTML_TEXT.innerHTML;";
            //btnConnect.Attributes["onclick"] = "javascript:initializePivotTable(document.Form1.ddlCubes.value);";

            if (!IsPostBack) {
                ddlCubes.DataSource = factory.GetAllCubes();
                ddlCubes.DataTextField = "nameEN";
                ddlCubes.DataValueField = "cube_id";
                ddlCubes.DataBind();

                var sbJScript = new StringBuilder();
                sbJScript.Append("<Script Language=\"JavaScript\">");
                sbJScript.Append("function initializePivotTable(strDataMember) {\n");
                sbJScript.Append("// Purpose:  Call Web Service method to initialize an empty PivotTable\n");
                sbJScript.Append("}\n");
                sbJScript.Append("</Script>");
                RegisterClientScriptBlock("initializePivotTable(strDataMember)", sbJScript.ToString());
            } else {
                var sbJScript = new StringBuilder();
                sbJScript.Append("<Script Language=\"JavaScript\">");
                sbJScript.Append("function initializePivotTable(strDataMember) {\n");
                sbJScript.Append("// Purpose:  Call Web Service method to initialize an empty PivotTable\n");
                sbJScript.Append(
                    "var iCallID = service.svcOLAP.callService(oninitializePivotTableResult, 'InitializePivotTableXML', strDataMember);\n");
                sbJScript.Append("}\n");
                sbJScript.Append("</Script>");
                RegisterClientScriptBlock("initializePivotTable(strDataMember)", sbJScript.ToString());
            }

//			lbSavedReports.DataSource = factory.GetAllReports();
//			lbSavedReports.DataTextField = "name";
//			lbSavedReports.DataValueField = "report_id";
//			lbSavedReports.DataBind();

            BUTTON1.Visible = false;
            BUTTON2.Visible = false;

            LocalizeText();
        }

        private static void LocalizeText() { }

        public void btnConnect_Click(object sender, EventArgs e) {
            var factory = new StatisticFactory();
            try {
                lbSavedReports.DataSource = factory.GetReports(
                    int.Parse(ddlCubes.SelectedValue), int.Parse(Session["UserCreditInfoID"].ToString()));
                lbSavedReports.DataTextField = "name";
                lbSavedReports.DataValueField = "report_id";
                lbSavedReports.DataBind();
                lbSavedReports.Visible = true;
            } catch (Exception) {}

            lblName.Visible = true;
            tbName.Visible = true;

            BUTTON1.Visible = true;
            BUTTON2.Visible = true;
        }

        private void BUTTON1_ServerClick(object sender, EventArgs e) {
            var factory = new StatisticFactory();
            try {
                lbSavedReports.DataSource = factory.GetReports(
                    int.Parse(ddlCubes.SelectedValue), int.Parse(Session["UserCreditInfoID"].ToString()));
                lbSavedReports.DataTextField = "name";
                lbSavedReports.DataValueField = "report_id";
                lbSavedReports.DataBind();
                lbSavedReports.Visible = true;
            } catch (Exception) {}
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            this.BUTTON1.ServerClick += new System.EventHandler(this.BUTTON1_ServerClick);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}