using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Statistic {
    /// <summary>
    /// Summary description for TableUpdates.
    /// </summary>
    public class StatsFrame : Page {
        protected HtmlTableCell blas;
        protected PlaceHolder myPlaceHolder;
        protected PlaceHolder plhMain;
        // public Control uc;
        private void Page_Load(object sender, EventArgs e) {
            Control uc;
            Page.ID = Request.QueryString["action"];
            if (Page.ID == "") {
                Page.ID = ViewState["CurrentPage"].ToString();
            }
            ViewState["CurrentPage"] = Page.ID;

            switch (Page.ID) {
                case "4001":
                    uc = LoadControl("user_controls/UsageReport.ascx");
                    uc.ID = "UsageReport";
                    break;

                case "4002":
                    uc = LoadControl("user_controls/RequestReport.ascx");
                    uc.ID = "RequestReport";
                    break;

                default:
                    uc = LoadControl("user_controls/UsageReport.ascx");
                    uc.ID = "UsageReport";
                    break;
            }

            plhMain.Controls.Clear();
            plhMain.Controls.Add(uc);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}