using System;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Logging.BLL;
using Statistic.BLL;
using Statistic.Localization;

namespace Statistic {
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public class StatsOWC10 : Page {
        protected static CultureInfo ci;
        protected static ResourceManager rm;
        protected Button btnConnect;
        protected HtmlButton BUTTON1;
        protected HtmlButton BUTTON2;
        protected HtmlButton Button3;
        protected CheckBox cbIsPublic;
        protected DropDownList ddlCubes;
        protected bool EN;
        protected Label lblChartHeader;
        protected Label lblName;
        protected Label lblPivotHeader;
        protected Label lblStatistic;
        protected ListBox lbSavedReports;
        protected HtmlInputButton myButton;
        protected HtmlTable outerGridTable;
        protected HtmlTable tableChartSpace;
        protected HtmlInputHidden tbCreditInfoID;
        protected TextBox tbName;
        protected HtmlInputHidden tbReportID;
        protected HtmlTableRow trReportRow;

        private void Page_Load(object sender, EventArgs e) {
            try {
                Page.ID = "4102";
                string culture = Thread.CurrentThread.CurrentCulture.Name;
                tbCreditInfoID.Value = Session["UserCreditInfoID"].ToString();
                if (culture.Equals("en-US")) {
                    EN = true;
                }
                rm = CIResource.CurrentManager;
                ci = Thread.CurrentThread.CurrentCulture;

                var factory = new StatisticFactory();

                var sbJScript = new StringBuilder();
                if (!IsPostBack) {
                    ddlCubes.DataSource = factory.GetAllCubes();
                    ddlCubes.DataTextField = "nameEN";
                    ddlCubes.DataValueField = "cube_id";
                    ddlCubes.DataBind();

                    sbJScript.Append("<Script Language=\"JavaScript\">");
                    sbJScript.Append("function initializePivotTable(strDataMember) {\n");
                    sbJScript.Append("// Purpose:  Call Web Service method to initialize an empty PivotTable\n");
                    sbJScript.Append("}\n");
                    sbJScript.Append("</Script>");

                    trReportRow.Visible = false;
                } else if (lbSavedReports.SelectedIndex > -1) {
                    sbJScript.Append("<Script Language=\"JavaScript\">");
                    sbJScript.Append("function initializePivotTable(strDataMember) {\n");
                    sbJScript.Append("// Purpose:  Call Web Service method to initialize an empty PivotTable\n");
                    sbJScript.Append("LoadSavedReport();\n");
                    sbJScript.Append("}\n");
                    sbJScript.Append("</Script>");
                } else {
                    sbJScript.Append("<Script Language=\"JavaScript\">");
                    sbJScript.Append("function initializePivotTable(strDataMember) {\n");
                    sbJScript.Append("// Purpose:  Call Web Service method to initialize an empty PivotTable\n");
                    sbJScript.Append(
                        "var iCallID = service.svcOLAP.callService(oninitializePivotTableResult, 'InitializePivotTableXML', strDataMember);\n");
                    sbJScript.Append("}\n");
                    sbJScript.Append("</Script>");
                }
                RegisterClientScriptBlock("initializePivotTable(strDataMember)", sbJScript.ToString());

                LocalizeText();
            } catch (Exception ex) {
                Logger.WriteToLog("StatsOWC11.Page_Load(..) " + ex.Message, true);
            }
        }

        private void LocalizeText() {
            btnConnect.Text = rm.GetString("btnConnect", ci);
            BUTTON1.InnerHtml = rm.GetString("btnSave", ci);
            BUTTON2.InnerHtml = rm.GetString("btnLoad", ci);
            Button3.InnerHtml = rm.GetString("btnShowChart", ci);

            lblName.Text = rm.GetString("lblName", ci);
            lblStatistic.Text = rm.GetString("lblStatistic", ci);
            lblPivotHeader.Text = rm.GetString("lblPivotHeader", ci);
            lblChartHeader.Text = rm.GetString("lblChartHeader", ci);

            cbIsPublic.Text = rm.GetString("cbIsPublic", ci);
        }

        public void btnConnect_Click(object sender, EventArgs e) {
            var factory = new StatisticFactory();

            try {
                lbSavedReports.DataSource = factory.GetReports(
                    int.Parse(ddlCubes.SelectedValue), int.Parse(Session["UserCreditInfoID"].ToString()));
                lbSavedReports.DataTextField = "name";
                lbSavedReports.DataValueField = "report_id";
                lbSavedReports.DataBind();
                lbSavedReports.Visible = true;
            } catch (Exception) {}

            /*lblName.Visible = true;
			tbName.Visible = true;
			cbIsPublic.Visible = true;
			BUTTON1.Visible = true;
			BUTTON2.Visible = true;*/

            trReportRow.Visible = true;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}