<%@ Page language="c#" Codebehind="StatsOWC11.aspx.cs" AutoEventWireup="false" Inherits="Statistic.StatsOWC11"%>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Statistic</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<!--#include file=include\olap.js -->
  </HEAD>
	<body onload="InitializePage();initializePivotTable(document.Form1.ddlCubes.value)" ms_positioning="GridLayout">
		<form id="Form1" method="post" runat="server">
			<div id="service" style="BEHAVIOR: url(webservice.htc)"></div>
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblStatistic" runat="server">Statistic</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:dropdownlist id="ddlCubes" runat="server"></asp:dropdownlist><input id="tbCreditInfoID" type="hidden" name="tbCreditInfoID" runat="server">
																		<input id="tbReportID" type="hidden" name="tbCreditInfoID" runat="server">
																		<asp:button id="btnConnect" runat="server" text="Connect" cssclass="confirm_button"></asp:button>
																	</td>
																</tr>
																<tr valign="top" id="trReportRow" runat="server">
																	<td width="50%"><asp:label id="lblName" runat="server">Name</asp:label><br>
																		<asp:textbox id="tbName" runat="server"></asp:textbox>
																		<br>
																		<asp:checkbox id="cbIsPublic" runat="server" cssclass="radio" text="Public"></asp:checkbox><br>
																		<button class="confirm_button" id="BUTTON1" onclick="SaveReport();" type="button" runat="server">
																			Save Report</button>
																	</td>
																	<td width="50%"><asp:listbox id="lbSavedReports" runat="server" rows="5" width="200px"></asp:listbox><br>
																		<button class="confirm_button" id="BUTTON2" onclick="LoadSavedReport();" type="button" runat="server">
																			Load Report</button>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<th style="HEIGHT: 24px">
															<asp:label id="lblPivotHeader" runat="server">PivotTable</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
                              <OBJECT id=PivotTable1 
                              style="WIDTH: 502px; HEIGHT: 217px" height=217 
                              width=502 
                              classid=clsid:0002E55A-0000-0000-C000-000000000046 
                              VIEWASTEXT>
	<PARAM NAME="XMLData" VALUE='<xml xmlns:x="urn:schemas-microsoft-com:office:excel">&#13;&#10; <x:PivotTable>&#13;&#10;  <x:OWCVersion>11.0.0.6555         </x:OWCVersion>&#13;&#10;  <x:DisplayScreenTips/>&#13;&#10;  <x:CubeProvider>msolap.2</x:CubeProvider>&#13;&#10;  <x:CacheDetails/>&#13;&#10;  <x:PivotView>&#13;&#10;   <x:IsNotFiltered/>&#13;&#10;  </x:PivotView>&#13;&#10; </x:PivotTable>&#13;&#10;</xml>'>
	</OBJECT>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<table class="empty_table" cellspacing="0">
													<tr valign="top">
														<td align="left">
														</td>
														<td align="right">
															<button id="Button3" onclick="LoadChart()" type="button" runat="server" class="gray_button">
																Show chart</button>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableChartSpace" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<th>
															<asp:label id="lblChartHeader" runat="server">Chart</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
                              <OBJECT id=ChartSpace1 
                              style="VISIBILITY: hidden; WIDTH: 100%; HEIGHT: 350px" 
                              classid=CLSID:0002E55D-0000-0000-C000-000000000046 
                              viewastext>
	<PARAM NAME="XMLData" VALUE='<xml xmlns:x="urn:schemas-microsoft-com:office:excel">&#13;&#10; <x:ChartSpace>&#13;&#10;  <x:OWCVersion>11.0.0.6555         </x:OWCVersion>&#13;&#10;  <x:Width>21775</x:Width>&#13;&#10;  <x:Height>9260</x:Height>&#13;&#10;  <x:Palette>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#8080FF</x:Entry>&#13;&#10;   <x:Entry>#802060</x:Entry>&#13;&#10;   <x:Entry>#FFFFA0</x:Entry>&#13;&#10;   <x:Entry>#A0E0E0</x:Entry>&#13;&#10;   <x:Entry>#600080</x:Entry>&#13;&#10;   <x:Entry>#FF8080</x:Entry>&#13;&#10;   <x:Entry>#008080</x:Entry>&#13;&#10;   <x:Entry>#C0C0FF</x:Entry>&#13;&#10;   <x:Entry>#000080</x:Entry>&#13;&#10;   <x:Entry>#FF00FF</x:Entry>&#13;&#10;   <x:Entry>#80FFFF</x:Entry>&#13;&#10;   <x:Entry>#0080FF</x:Entry>&#13;&#10;   <x:Entry>#FF8080</x:Entry>&#13;&#10;   <x:Entry>#C0FF80</x:Entry>&#13;&#10;   <x:Entry>#FFC0FF</x:Entry>&#13;&#10;   <x:Entry>#FF80FF</x:Entry>&#13;&#10;  </x:Palette>&#13;&#10;  <x:DefaultFont>Arial</x:DefaultFont>&#13;&#10; </x:ChartSpace>&#13;&#10;</xml>'>
	<PARAM NAME="ScreenUpdating" VALUE="-1">
	<PARAM NAME="EnableEvents" VALUE="-1">
	</OBJECT>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
