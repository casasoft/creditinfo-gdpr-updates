<script id="iq_olap" language="javascript">
	
	var frm1;
	var service;

	// Page initialization code, called from Body onLoad event handler
	function InitializePage() {
		service = document.getElementById("service");	
	
		// Set page-level variables
		frm1 = document.forms[0];
				
		// Create an instance of the web service and call it svcOlap
		service.useService("/services/OLAP.asmx?WSDL","svcOLAP");		
	}
	
	
	
	function oninitializePivotTableResult(result) {
		// Purpose:	This function handles the wsOLAP.InitializePivotTableXML() web service result		
		var text = result.value; // result string
		// Evaluate return result
		if (!result.error) {
			if (text.indexOf("<err>") > -1 ) {
				alert(text);
			}
			// Assign the XML to the PivotList XMLData value
			frm1.PivotTable1.XMLData = text;
			frm1.ChartSpace1.style.visibility = "hidden";
		}
		else {
			alert("Unhandled error - " + result.errorDetail.code + " " + result.errorDetail.string);
		}
	}
	
	function LoadCustomReport() {	
		// Purpose:  Call Web Service method to initialize an empty PivotTable
		var strCity1 = frm1.sel_city1.options[frm1.sel_city1.selectedIndex].value;
		var strCity2 = frm1.sel_city2.options[frm1.sel_city2.selectedIndex].value;
		var iCallID = service.svcOLAP.callService(onLoadCustomReportResult, 'LoadCustomPivotTableReport', strCity1, strCity2);
	}
	
	function onLoadCustomReportResult(result) {
		// Purpose:	This function handles the wsOLAP.LoadCustomPivotTableReport() web service result
		var text = result.value; // result string
		// Evaluate return result
		if (!result.error) {
			if (text.indexOf("<err>") > -1 ) {
				alert(text);
			}
			// Assign the XML to the PivotList XMLData value
			frm1.PivotTable1.XMLData = text;
			// bind the Chart control to the PivotTable control
			frm1.ChartSpace1.Clear();
			frm1.ChartSpace1.DataSource = frm1.PivotTable1;
			frm1.ChartSpace1.style.visibility = "visible";
			//frm1.ChartSpace1.Charts(0).Type = 50;
		}
		else {
			alert("Unhandled error - " + result.errorDetail.code + " " + result.errorDetail.string);
		}
	}
	
	function ApplyCustomGrouping() {	
		// Purpose:  Call Web Service method to initialize an empty PivotTable
		var strReportXMLData = frm1.PivotTable1.XMLData;

		// The "<" and ">" tags cause errors when sent in the web service request.
		// Replace with "{" and "}".
		var pattern = /</ig;
        strReportXMLData = strReportXMLData.replace(pattern,"{");
        pattern = />/ig;
        strReportXMLData = strReportXMLData.replace(pattern,"}");

		var iCallID = service.svcOLAP.callService(onApplyCustomGroupingResult, 'ApplyCustomGrouping', strReportXMLData);
	}
	
	function onApplyCustomGroupingResult(result) {
		// Purpose:	This function handles the wsOLAP.LoadCustomPivotTableReport() web service result
		var text = result.value; // result string
		// Evaluate return result
		if (!result.error) {
			if (text.indexOf("<err>") > -1 ) {
				alert(text);
			}
			// Assign the XML to the PivotList XMLData value
			frm1.PivotTable1.XMLData = text;
			frm1.ChartSpace1.Clear();
			frm1.ChartSpace1.DataSource = frm1.PivotTable1;
			frm1.ChartSpace1.style.visibility = "visible";
		}
		else {
			alert("Unhandled error - " + result.errorDetail.code + " " + result.errorDetail.string);
		}
	}
	
	function SaveReport() {	
		// Purpose:  Call Web Service method to save the custom report
		var strReportXMLData = frm1.PivotTable1.XMLData;

		// The "<" and ">" tags cause errors when sent in the web service request.
		// Replace with "{" and "}".
		var pattern = /</ig;
        strReportXMLData = strReportXMLData.replace(pattern,"{");
        pattern = />/ig;
        strReportXMLData = strReportXMLData.replace(pattern,"}");

		var iCallID = service.svcOLAP.callService(onSaveReportResult, 'SaveReport', strReportXMLData, document.Form1.tbName.value, document.Form1.tbCreditInfoID.value, document.Form1.ddlCubes.value, document.Form1.cbIsPublic.checked, document.Form1.tbReportID.value);
	}
	
	function onSaveReportResult(result) {
		// Purpose:	This function handles the wsOLAP.onSaveReportResult() web service result
		var text = result.value; // result string
		
		// Evaluate return result
		if (!result.error) {
			if (text.indexOf("<err>") > -1 ) {
				alert(text);
			}
			else
			{
				alert("Report was successfully saved.");
			}
		}
		else {
			alert("Unhandled error - " + result.errorDetail.code + " " + result.errorDetail.string);
		}
	}
	
	function LoadSavedReport() {	
		// Purpose:  Call Web Service method load the saved report
		document.Form1.tbReportID.value = document.Form1.lbSavedReports.value;
		document.Form1.tbName.value = document.Form1.lbSavedReports.options[document.Form1.lbSavedReports.selectedIndex].innerHTML
		var iCallID = service.svcOLAP.callService(onLoadSavedReportResult, 'LoadSavedReport', document.Form1.lbSavedReports.value);
	}
	
	function onLoadSavedReportResult(result) {
		// Purpose:	This function handles the wsOLAP.onLoadSavedReportResult() web service result
		var text = result.value; // result string
		// Evaluate return result
		if (!result.error) {
			if (text.indexOf("<err>") > -1 ) {
				alert(text);
			}
			// Assign the XML to the PivotList XMLData value
			frm1.PivotTable1.XMLData = text;
			frm1.ChartSpace1.Clear();
			frm1.ChartSpace1.DataSource = frm1.PivotTable1;
			frm1.ChartSpace1.style.visibility = "visible";			
		}
		else {
			alert("Unhandled error - " + result.errorDetail.code + " " + result.errorDetail.string);
		}
	}
	
	function LoadChart() {
		// Assign the XML to the PivotList XMLData value
		frm1.ChartSpace1.Clear();
		frm1.ChartSpace1.DataSource = frm1.PivotTable1;
		frm1.ChartSpace1.style.visibility = "visible";	
	}
</script>