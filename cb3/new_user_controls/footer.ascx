<%@ Control Language="c#" AutoEventWireup="false" Codebehind="footer.ascx.cs" Inherits="CreditInfoGroup.new_user_controls.footer" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<!-- F�TUR -->
<table width="100%" cellpadding="0" cellspacing="0">
<tr><td align="right">

<asp:Literal runat="server" ID="lblServer" /><br />
<asp:Literal runat="server" ID="lblVersionWeb" />


</td></tr>
	<tr>
		<td>
			<div class="head_foot">&nbsp;&nbsp;Creditinfo Malta Ltd. | 19A Sir Adrian Dingli Street | Sliema SLM 1904 | Tel: +356 2131 2344 | Fax: +356 2131 2388 | info@creditinfo.com.mt  </div>
		</td>
	</tr>
	<tr>
		<td height="15">

        </td>
	</tr>
	<tr>
		<td background="<%=Application.Get("AppPath")%>/img/bottom.gif" height="33">
			<!--img src="<%=Request.ApplicationPath%>/img/bottom.gif" alt=""-->
		</td>
	</tr>
</table>
<!-- F�TUR ENDAR -->
<!--------- SCRIPTS SECTION  --->
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>


<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>

<script>

    $(document).ready(function () {
        var isPrintBlocked = "<%= (HttpContext.Current.Session["isPrintBlocked"] != null)? (bool)Session["isPrintBlocked"] : false  %>";
        if (isPrintBlocked === "True") {
            $("<style type='text/css' media='print'> * { display: none; } </style>").appendTo("head");


            function onCopyToClipboard() {
                var aux = document.createElement("input");
                aux.setAttribute("value", " ");
                document.body.appendChild(aux);
                aux.select();
                document.execCommand("copy");
                document.body.removeChild(aux);
            }

            $(window).keyup(function(e) {
                if (e.keyCode == 44) {
                    onCopyToClipboard();
                }
            });

            $("body").on("contextmenu",
                function(e) {
                    return false;
                });

            $('body').bind('cut copy paste',
                function(e) {
                    e.preventDefault();
                });
        }

    });
</script>