#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using CreditInfoGroup.Localization;
using CYBERAKT.WebControls.Navigation;
using eStreamBG.WebControls;
using UserAdmin.BLL;

using Cig.Framework.Base.Configuration;


#endregion

namespace CreditInfoGroup.new_user_controls {
    /// <summary>
    ///		Summary description for panelSubBar.
    /// </summary>
    public class panelSubBar : UserControl {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        protected ASPnetMenu myMenu;
        private int myParentID;
        protected PanelBar thePanelSubBar;

        private void Page_Load(object sender, EventArgs e) {
            // PanelBar activation
            thePanelSubBar.LicenseType = PanelBar.eLicenseType.Developer;
            thePanelSubBar.LicenseName = "Ragnar Dyer";
            thePanelSubBar.LicenseCompany = "Creditinfo Group Ltd";
            thePanelSubBar.LicenseNumber = "Ugn1KKQS57yd0vBJrvGoqakpaWs=";

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            //LocalizeText();

            if (!IsPostBack) {
                BuildSimpleMenu();
            }
        }

        // Byggja upp menu me� �v� a� lesa upp �r Products t�flunni...
        private void BuildSimpleMenu() {
            if (Context.User.Identity.Name == "") {
                return;
            }

            myParentID = -1;
            int Counter = 0;
            if (Session[ID] != null) {
                myParentID = (int) Session[ID];
            }

            //myParentID = Convert.ToInt32(this.Parent.Page.ID);

            DataSet myProductSet = myFactory.GetAllTheOpenProducts();

            PanelBarGroupControl group = null;
            PanelBarItemControl item;

            // Athuga hvort notandi hafi a�gang a� v�rum. Birta menu samkv�mt a�gangi notenda...
            for (int i = 0; i < myProductSet.Tables[0].Rows.Count; i++) {
                // Fyrir Parent gr�ppu - n.b. Datasetti� kemur ra�a� m.v. parentID og svo ProductID. Parents eru me�
                // "0" � ParentID og koma �v� fremst...
                if ((int) myProductSet.Tables[0].Rows[i]["ProductID"] == myParentID) {
                    // Ef notandinn hefur a�gang a� �essari v�ru...
                    if (Context.User.IsInRole(myProductSet.Tables[0].Rows[i]["ProductID"].ToString()) &&
                        Convert.ToBoolean(myProductSet.Tables[0].Rows[i]["IsInner"])) {
                        group = new PanelBarGroupControl
                                {
                                    Label =
                                        (ci.Name == Session["Defculture"].ToString()
                                             ? myProductSet.Tables[0].Rows[i]["ProductNameNative"].ToString()
                                             : myProductSet.Tables[0].Rows[i]["ProductNameEN"].ToString()),
                                    ID = myProductSet.Tables[0].Rows[i]["ProductID"].ToString(),
                                    State = "Exploded"
                                };

                        // Fyrir native e�a ensku...

                        // Fyrri partur af URL (root url) tekinn �r web.config...
                        // group.Href = CigConfig.Configure("lookupsettings.rootURL"].ToString() + myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString();

                        thePanelSubBar.MenuStructure.Add(group);
                        Counter = 0;

                        /* ekkert item fyrir group li�inn.
						if(myProductSet.Tables[0].Rows[i]["Menu"].ToString().Trim().ToLower().Equals("true"))
						{
							item	= new PanelBarItemControl();
							if (i % 2 > 0)
							{
								item.Image = "../img/MenuLayer3Light.bmp";
							}
							else
							{
								item.Image = "../img/MenuLayer3Dark.bmp";
							}	

							item.ImagePosition = "Background";

							// Fyrir native e�a ensku...
							if (ci.Name.ToString() == CigConfig.Configure("lookupsettings.nativeCulture"))
							{
								if (myProductSet.Tables[0].Rows[i]["MenuNameNative"] != System.DBNull.Value)
									item.Label = myProductSet.Tables[0].Rows[i]["MenuNameNative"].ToString();
								else
									item.Label = myProductSet.Tables[0].Rows[i]["ProductNameNative"].ToString();
							}
							else
							{
								if (myProductSet.Tables[0].Rows[i]["MenuNameEN"] != System.DBNull.Value)
									item.Label = myProductSet.Tables[0].Rows[i]["MenuNameEN"].ToString();
								else
									item.Label  = myProductSet.Tables[0].Rows[i]["ProductNameEN"].ToString();
							}
					
							// Fyrri partur af URL (root url) tekinn �r web.config...
							item.Href = Request.ApplicationPath + myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString();
							item.ID = myProductSet.Tables[0].Rows[i]["ProductID"].ToString();

							group.GroupStructure.Add(item);
						}*/
                    }
                }
                    // Fyrir children. Ra�ast undir parents menu items � datasettinu en allir items eru � parentgroup.
                    // �a� �arf s.s. a� "hand" me�h�ndla �esssi parent-child samb�nd (f�sk) vegna �ess hvernig menu
                    // kerfi� er byggt upp (t.d. ekki h�gt a� finna index sem er �a� eina sem h�gt er a� nota � addat).
                else if ((int) myProductSet.Tables[0].Rows[i]["ParentID"] == myParentID) {
                    // Ef notandi hefur a�gang a� parent...
                    if (Context.User.IsInRole(myProductSet.Tables[0].Rows[i]["ProductID"].ToString())) {
                        //Item fer a�eins sem menu child ef �a� er merkt �annig � db
                        if (myProductSet.Tables[0].Rows[i]["Menu"].ToString().Trim().ToLower().Equals("submenu")) {
                            //						PanelBarGroupControl parent = new PanelBarGroupControl();
                            //						parent.ID = myProductSet.Tables[0].Rows[i]["ParentID"].ToString();
                            //						parent = (PanelBarGroupControl) thePanelSubBar.MenuStructure[(thePanelSubBar.MenuStructure.BinarySearch(parent))];
                            //(PanelBarGroupControl) thePanelSubBar.FindControl(myProductSet.Tables[0].Rows[i]["ParentID"].ToString());
                            item = new PanelBarItemControl();

                            if (Counter%2 > 0) {
                                item.Image = Application.Get("AppPath") + "/img/MenuLayer3Light.bmp";
                            } else {
                                item.Image = Application.Get("AppPath") + "/img/MenuLayer3Dark.bmp";
                            }
                            Counter++;

                            item.ImagePosition = "Background";

                            // Fyrir native e�a ensku...
                            if (ci.Name == CigConfig.Configure("lookupsettings.nativeCulture")) {
                                item.Label = "  - " + myProductSet.Tables[0].Rows[i]["ProductNameNative"];
                            } else {
                                item.Label = "  - " + myProductSet.Tables[0].Rows[i]["ProductNameEN"];
                            }

                            // Fyrri partur af URL (root url) tekinn �r web.config...
                            item.Href = Application.Get("AppPath") +
                                        myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString();
                            //	item.Href = CigConfig.Configure("lookupsettings.rootURL"].ToString() + myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString();
                            //	item.Href = myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString();
                            item.ID = myProductSet.Tables[0].Rows[i]["ProductID"].ToString();

                            if (group != null) {
                                group.GroupStructure.Add(item);
                            }
                        }
                    }
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new EventHandler(this.Page_Load); }

        #endregion
    }
}