#region

using System;
using System.Configuration;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

using Cig.Framework.Base.Configuration;


#endregion

namespace CreditInfoGroup.new_user_controls {
    /// <summary>
    ///		Summary description for UserInfo.
    /// </summary>
    public class UserInfo : UserControl {
        protected Label lblUserInfo;
        private bool nativeCult;

        private void Page_Load(object sender, EventArgs e) {
            if (IsPostBack) {
                return;
            }
            string nativeCulture = Thread.CurrentThread.CurrentCulture.Name;
            string culture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }
            string userName = "";
            string subscriberName = "";
            var builder = new StringBuilder();
            if (nativeCult) {
                if (Session["NativeUserName"] != null) {
                    userName = Session["NativeUserName"].ToString();
                }
                if (Session["NativeSubscriberName"] != null) {
                    subscriberName = Session["NativeSubscriberName"].ToString();
                }
                // if no native name the get the english one
                if (userName.Trim() == "") {
                    if (Session["ENUserName"] != null) {
                        userName = Session["ENUserName"].ToString();
                    }
                }
                if (subscriberName.Trim() == "") {
                    if (Session["ENSubscriberName"] != null) {
                        subscriberName = Session["ENSubscriberName"].ToString();
                    }
                }
            } else {
                if (Session["ENUserName"] != null) {
                    userName = Session["ENUserName"].ToString();
                }
                if (Session["ENSubscriberName"] != null) {
                    subscriberName = Session["ENSubscriberName"].ToString();
                }
                // if no english name then get the native one
                if (userName.Trim() == "") {
                    if (Session["NativeUserName"] != null) {
                        userName = Session["NativeUserName"].ToString();
                    }
                }
                if (subscriberName.Trim() == "") {
                    if (Session["NativeSubscriberName"] != null) {
                        subscriberName = Session["NativeSubscriberName"].ToString();
                    }
                }
            }
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(subscriberName)) {
                return;
            }
            builder.AppendFormat(@"{0} | {1}", userName, subscriberName);
            lblUserInfo.Text = builder.ToString();
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}