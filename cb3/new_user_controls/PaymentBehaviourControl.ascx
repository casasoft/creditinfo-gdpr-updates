<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PaymentBehaviourControl.ascx.cs" Inherits="CreditInfoGroup.new_user_controls.PaymentBehaviourControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="Web" Namespace="WebChart" Assembly="WebChart" %>



<TABLE id=Table1 cellSpacing=1 cellPadding=1 width="100%" border=0>
  <TR>
    <TD style="HEIGHT: 15px"></TD></TR>
  <TR>
    <TD><asp:label id=lblTitle Font-Bold="True" runat="server">Grei�sluheg�un eftir t�mabilum</asp:label></TD></TR>
  <TR>
    <TD style="HEIGHT: 21px"></TD></TR>
  <TR>
    <TD></TD></TR>
  <TR>
    <TD></TD></TR>
  <TR>
    <TD><asp:label id=lblDescription Font-Bold="True" runat="server">Sk�ringar:</asp:label></TD></TR>
  <TR>
    <TD><asp:label id=lblDaysTitle Font-Bold="True" runat="server">Days:</asp:label>&nbsp; 
<asp:label id=lblDays runat="server">Me�altal �eirra daga sem l��a fr� eindaga a� grei�sludegi. � merkir eitt sta�alfr�vik til e�a fr�.</asp:label></TD></TR>
  <TR>
    <TD><asp:label id=lblCountTitle Font-Bold="True" runat="server">Fj�ldi:</asp:label>&nbsp; 
<asp:label id=lblCount runat="server">Fj�lda f�rslna sem hafa borist. A�eins eru birt g�gn �ar sem fj�ldi f�rslna er a.m.k. 10 og fj�ldi fyrirt�kja a.m.k., fyrir hvern �rsfj�r�ung.</asp:label></TD></TR>
  <TR>
    <TD><asp:label id=lblIndustryTitle Font-Bold="True" runat="server">Atvinnugrein:</asp:label>&nbsp; 
<asp:label id=lblIndustry runat="server">Fyrirt�ki me� sama �SAT flokkun.</asp:label></TD></TR>
  <TR>
    <TD style="HEIGHT: 15px"></TD></TR>
  <TR>
    <TD style="HEIGHT: 15px"><asp:label id=lblPaymentIndexTitle Font-Bold="True" runat="server">Payment index:</asp:label>&nbsp; 
<asp:label id=lblPaymentIndex runat="server">B1 - Slowly 1-15</asp:label></TD></TR>
  <TR>
    <TD style="HEIGHT: 15px"></TD></TR>
  <TR>
    <TD id=tdSummaryTable align=center 
  runat="server"></TD></TR>
  <TR>
    <TD style="HEIGHT: 15px"><asp:label id=lblNoDataAvailable Font-Bold="True" runat="server">There is not enough data available to display payment informations</asp:label></TD></TR>
  <TR>
    <TD><WEB:CHARTCONTROL id=wcAvg runat="server" Padding="13" ImageID="13b0c740-51b9-4cca-a34d-f9304fc70463" YCustomStart="0" YValuesInterval="0" ShowTitlesOnBackground="False" TopPadding="20" ChartPadding="30" YCustomEnd="0" ChartFormat="Gif" Height="227px" Width="600px">
<charts>
<web:ColumnChart Legend="lkj" ShowLineMarkers="False">
<fill color="Transparent">
</Fill>

<datalabels>

<background color="Transparent">
</Background>

<border color="Transparent">
</Border>

</DataLabels>

<data>
<web:ChartPoint XValue="03.2003" YValue="10"></web:ChartPoint>
<web:ChartPoint XValue="04.2003" YValue="5"></web:ChartPoint>
<web:ChartPoint XValue="01.2004" YValue="2"></web:ChartPoint>
<web:ChartPoint XValue="02.2004" YValue="8"></web:ChartPoint>
<web:ChartPoint XValue="03.2004" YValue="5"></web:ChartPoint>
</Data>

<line color="Red">
</Line>
</web:ColumnChart>
<web:ColumnChart ShowLineMarkers="False">
<datalabels>

<background color="Transparent">
</Background>

<border color="Transparent">
</Border>

</DataLabels>

<data>
<web:ChartPoint XValue="03.2004" YValue="8"></web:ChartPoint>
<web:ChartPoint XValue="04.2003" YValue="15"></web:ChartPoint>
<web:ChartPoint XValue="01.2004" YValue="2"></web:ChartPoint>
<web:ChartPoint XValue="02.2004" YValue="5"></web:ChartPoint>
<web:ChartPoint XValue="03.2004" YValue="7"></web:ChartPoint>
</Data>
</web:ColumnChart>
<web:SmoothLineChart DataXValueField="10" DataYValueField="20">
<datalabels>

<background color="Transparent">
</Background>

<border color="Transparent">
</Border>

</DataLabels>

<data>
<web:ChartPoint XValue="03.2003" YValue="20"></web:ChartPoint>
<web:ChartPoint XValue="04.2003" YValue="10"></web:ChartPoint>
<web:ChartPoint XValue="01.2004" YValue="18"></web:ChartPoint>
<web:ChartPoint XValue="02.2004" YValue="25"></web:ChartPoint>
<web:ChartPoint XValue="03.2004" YValue="22"></web:ChartPoint>
</Data>

<line color="Red">
</Line>
</web:SmoothLineChart>
</Charts>

<xtitle forecolor="White" stringformat="Center,Near,Character,LineLimit" font="Tahoma, 8pt, style=Bold">
</XTitle>

<yaxisfont forecolor="115, 138, 156" stringformat="Far,Near,Character,LineLimit" font="Tahoma, 8pt, style=Bold">
</YAxisFont>

<charttitle stringformat="Near,Near,Character,LineLimit" text="Payment history - average days" font="Verdana, 10pt, style=Bold">
</ChartTitle>

<xaxisfont forecolor="115, 138, 156" stringformat="Center,Near,Character,LineLimit" font="Tahoma, 8pt, style=Bold">
</XAxisFont>

<legend width="200">
</legend>

<background angle="90" forecolor="White" endpoint="100, 400" color="Transparent" hatchstyle="DiagonalBrick">
</Background>

<ytitle forecolor="White" stringformat="Center,Near,Character,DirectionVertical" font="Tahoma, 8pt, style=Bold">
</YTitle>

<border color="222, 186, 132" width="2">
</Border>

<plotbackground angle="90" forecolor="White" endpoint="100, 400" color="Silver" hatchstyle="Horizontal">
</PlotBackground>
</WEB:CHARTCONTROL></TD></TR>
  <TR>
    <TD style="HEIGHT: 27px"></TD></TR>
  <TR>
    <TD><WEB:CHARTCONTROL id=wcStd runat="server" Padding="13" ImageID="13b0c740-51b9-4cca-a34d-f9304fc70463" YCustomStart="0" YValuesInterval="0" ShowTitlesOnBackground="False" TopPadding="20" ChartPadding="30" YCustomEnd="0" ChartFormat="Gif" Height="227px" Width="600px" GridLines="Both">
<charts>
<web:ColumnChart Legend="lkj" ShowLineMarkers="False">
<fill color="Transparent">
</Fill>

<datalabels>

<background color="Transparent">
</Background>

<border color="Transparent">
</Border>

</DataLabels>

<data>
<web:ChartPoint XValue="03.2003" YValue="10"></web:ChartPoint>
<web:ChartPoint XValue="04.2003" YValue="5"></web:ChartPoint>
<web:ChartPoint XValue="01.2004" YValue="2"></web:ChartPoint>
<web:ChartPoint XValue="02.2004" YValue="8"></web:ChartPoint>
<web:ChartPoint XValue="03.2004" YValue="5"></web:ChartPoint>
</Data>

<line color="Red">
</Line>
</web:ColumnChart>
<web:ColumnChart ShowLineMarkers="False">
<datalabels>

<background color="Transparent">
</Background>

<border color="Transparent">
</Border>

</DataLabels>

<data>
<web:ChartPoint XValue="03.2004" YValue="8"></web:ChartPoint>
<web:ChartPoint XValue="04.2003" YValue="15"></web:ChartPoint>
<web:ChartPoint XValue="01.2004" YValue="2"></web:ChartPoint>
<web:ChartPoint XValue="02.2004" YValue="5"></web:ChartPoint>
<web:ChartPoint XValue="03.2004" YValue="7"></web:ChartPoint>
</Data>
</web:ColumnChart>
<web:SmoothLineChart DataXValueField="10" DataYValueField="20">
<datalabels>

<background color="Transparent">
</Background>

<border color="Transparent">
</Border>

</DataLabels>

<data>
<web:ChartPoint XValue="03.2003" YValue="20"></web:ChartPoint>
<web:ChartPoint XValue="04.2003" YValue="10"></web:ChartPoint>
<web:ChartPoint XValue="01.2004" YValue="18"></web:ChartPoint>
<web:ChartPoint XValue="02.2004" YValue="25"></web:ChartPoint>
<web:ChartPoint XValue="03.2004" YValue="22"></web:ChartPoint>
</Data>

<line color="Red">
</Line>
</web:SmoothLineChart>
</Charts>

<xtitle forecolor="White" stringformat="Center,Near,Character,LineLimit" font="Tahoma, 8pt, style=Bold">
</XTitle>

<yaxisfont forecolor="115, 138, 156" stringformat="Far,Near,Character,LineLimit" font="Tahoma, 8pt, style=Bold">
</YAxisFont>

<charttitle stringformat="Near,Near,Character,LineLimit" text="Payment history - standard division" font="Verdana, 10pt, style=Bold">
</ChartTitle>

<xaxisfont forecolor="115, 138, 156" stringformat="Center,Near,Character,LineLimit" font="Tahoma, 8pt, style=Bold">
</XAxisFont>

<legend width="200">
</legend>

<background angle="90" centercolor="Transparent" forecolor="Transparent" endpoint="100, 400" color="Transparent" hatchstyle="DiagonalBrick">
</Background>

<ytitle forecolor="White" stringformat="Center,Near,Character,DirectionVertical" font="Tahoma, 8pt, style=Bold">
</YTitle>

<border color="222, 186, 132" width="2">
</Border>

<plotbackground angle="90" forecolor="White" endpoint="100, 400" color="Silver" hatchstyle="Horizontal">
</PlotBackground>
</WEB:CHARTCONTROL></TD></TR></TABLE>
