#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;
using Telerik.WebControls;
using UserAdmin.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CreditInfoGroup.new_user_controls {
    /// <summary>
    ///		Summary description for newtest_panelbar.
    /// </summary>
    public class panelBar : UserControl {
        //protected PanelbarExamples.CodeViewer Viewer1;
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        protected Label lblMenuHeader;
        private int nLevel;
        protected RadPanelbar radPanelBar;

        private void Page_Load(object sender, EventArgs e) {
            //Read from the database and construct the XML file

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (!IsPostBack) {
                string content = GenerateMenu();
                radPanelBar.LoadContentFromXmlString(content);
            }

            lblMenuHeader.Text = rm.GetString("txtMenuHeader", ci);

            //You can do it like this, too - using an XmlDocument object you can even add and modify items programmatically
            //XmlDocument doc = new XmlDocument ();
            //doc.LoadXml(content);
            //panelbarObj.SetXmlDocument(doc);

            //Set viewer's content
            //SetViewerContent(content);

            //Label1.Text = content;
        }

        /*private void SetViewerContent(string content)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(content);
			StringWriter stringWriter = new StringWriter();
			XmlTextWriter xmlWriter = new XmlTextWriter( stringWriter);                           
			xmlWriter.Formatting = Formatting.Indented;
			doc.WriteTo(xmlWriter);
			xmlWriter.Flush();
			Viewer1.setXmlContent(stringWriter.ToString());
			stringWriter.Close();
		}*/

        private string GenerateMenu() {
            if (Context.User.Identity.Name == "") {
                return "";
            }

            DataSet ds = myFactory.GetAllTheOpenProducts();

            //Set a parent-child relation
            ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["ProductID"], ds.Tables[0].Columns["ParentId"]);

            //Build recursively an XML string with the panelbar content structure
            string content = string.Empty;

            //Get the ROOT node (that does not have a parent), and call the RecursivelyPopulate method
            foreach (DataRow dbRow in ds.Tables[0].Rows) {
                if (dbRow.IsNull("ParentId")) {
                    nLevel = 1;
                    content += RecursivelyPopulate(dbRow);
                }
            }

            //Return a finished XML panelbar content string
            return "<PanelItems SubGroupCssClass=\"menu_level" + nLevel + "\" " + ">" +
                   content +
                   "</PanelItems>";
        }

        /* Recursive function that parses items in a datarow, and calls itself if items have children  */

        private string RecursivelyPopulate(DataRow dbRow) {
            string content = string.Empty;
            nLevel++;

            foreach (DataRow row in dbRow.GetChildRows("NodeRelation")) {
                // Ef notandinn hefur a�gang a� �essari v�ru...
                if ((!Context.User.IsInRole(row["ProductID"].ToString()) || !Convert.ToBoolean(row["IsInner"]))) {
                    continue;
                }
                // Fyrir native e�a ensku...
                var sLabel = ci.Name == Session["Defculture"].ToString() ? row["ProductNameNative"].ToString() : row["ProductNameEN"].ToString();

                var strImages = string.Empty;
                if (nLevel > 2) {
                    if (row.GetChildRows("NodeRelation").Length > 0) {
                        if (nLevel > 3) {
                            strImages = "ImageCollapsed=\"" + Application.Get("AppPath") +
                                        "/img/arrow_right_white.gif\" ImageExpanded=\"" + Application.Get("AppPath") +
                                        "/img/arrow_down_white.gif\" ImagePosition=\"Right\" ";
                        } else {
                            strImages = "ImageCollapsed=\"" + Application.Get("AppPath") +
                                        "/img/arrow_right.gif\" ImageExpanded=\"" + Application.Get("AppPath") +
                                        "/img/arrow_down.gif\" ImagePosition=\"Right\" ";
                        }
                    }
                }
//<PanelItem ID="Panel1" Text="Mail" Expanded="True" ImageCollapsed="mail.gif" ImageExpanded="mail.gif" ImageHover="mail.gif">
                content += "<PanelItem ID=\"" + row["ProductID"] + "\" Text=\"" + sLabel + "\" "
                           +
                           (!row.IsNull("NavigateUrl")
                                ? "Value=\"" + row["NavigateUrl"].ToString().Trim() + "\" "
                                : "")
                           //+ "Target=\"_self\" "
                           +
                           (!row.IsNull("NavigateUrl") && row["NavigateUrl"].ToString().StartsWith("http://")
                                ? "Target=\"_blank\" "
                                : "Target=\"_self\" ")
                           + (nLevel > 2 ? " SubGroupCssClass=\"menu_level" + nLevel + "\" " : "")
                           + strImages
                           + ">";

                if (dbRow.GetChildRows("NodeRelation").Length > 0) {
                    content += /*"<PanelItem>" +*/ RecursivelyPopulate(row) /*+ "</PanelItem>"*/;
                }
                content += "</PanelItem>";
            }

            nLevel--;
            return content;
        }

        private void radPanelBar_Load(object sender, EventArgs e)
        {
            try
            {
                string strParentID = Parent.Page.ID;
                radPanelBar.SelectedPanelItem = radPanelBar.FindPanelItemById(strParentID);

                while (strParentID != null && !strParentID.Equals("radPanelBar"))
                {
                    PanelItem item = radPanelBar.FindPanelItemById(strParentID);
                    item.Expanded = true;
                    strParentID = item.ParentItem.ID;
                }
            }
            catch (Exception)
            {
                try
                {
                    PanelItem item = null;
                    bool isOk;
                    string strParentID = Parent.Page.ID;

                    while (item == null)
                    {
                        isOk = true;
                        strParentID = myFactory.GetParentID(strParentID);
                        if (strParentID == null)
                        {
                            break;
                        }

                        while (strParentID != null && !strParentID.Equals("radPanelBar") && isOk)
                        {
                            item = radPanelBar.FindPanelItemById(strParentID);
                            if (item != null)
                            {
                                item.Expanded = true;
                                strParentID = item.ParentItem.ID;
                                isOk = true;
                            }
                            else if (strParentID.Length > 0)
                            {
                                isOk = false;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                }
                catch (Exception) { }
            }
        }

        private void radPanelBar_PanelItemClick(object sender, RadPanelbarClickEventArgs args) {
            if (args.ClickedPanelItem.Value.StartsWith("http://")) {
                Response.Redirect(args.ClickedPanelItem.Value, true);
            } else {
                Response.Redirect(CigConfig.Configure("lookupsettings.backRootName") + args.ClickedPanelItem.Value, true);
            }
        }

        private void radPanelBar_PanelItemExpand(object sender, RadPanelbarClickEventArgs args) {
            if (string.IsNullOrEmpty(args.ClickedPanelItem.Value)) {
                return;
            }
            if (args.ClickedPanelItem.Value.StartsWith("http://")) {
                Response.Redirect(args.ClickedPanelItem.Value, true);
            } else {
                Response.Redirect(
                    CigConfig.Configure("lookupsettings.backRootName") + args.ClickedPanelItem.Value, true);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.radPanelBar.Load += new EventHandler(this.radPanelBar_Load);
            this.radPanelBar.PanelItemExpand +=
                new RadPanelbar.RadPanelbarClickEventHandler(this.radPanelBar_PanelItemExpand);
            this.radPanelBar.PanelItemClick +=
                new RadPanelbar.RadPanelbarClickEventHandler(this.radPanelBar_PanelItemClick);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}