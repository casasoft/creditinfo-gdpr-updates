#region

using System;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CreditInfoGroup.BLL;
using CreditInfoGroup.Localization;
using WebChart;

using Cig.Framework.Base.Configuration;


#endregion

namespace CreditInfoGroup.new_user_controls {
    /// <summary>
    ///		Summary description for PaymentBehaviourControl.
    /// </summary>
    public class PaymentBehaviourControl : UserControl {
        private CultureInfo ci;
        protected Label lblCount;
        protected Label lblCountTitle;
        protected Label lblDays;
        protected Label lblDaysTitle;
        protected Label lblDescription;
        protected Label lblIndustry;
        protected Label lblIndustryTitle;
        protected Label lblNoDataAvailable;
        protected Label lblPaymentIndex;
        protected Label lblPaymentIndexTitle;
        protected Label lblTitle;
        private bool nativeCult;
        private ResourceManager rm;
        protected HtmlTableCell tdSummaryTable;
        protected ChartControl wcAvg;
        protected ChartControl wcStd;
        private void Page_Load(object sender, EventArgs e) { }

        protected void LocalizeText() {
            lblCountTitle.Text = rm.GetString("txtInvoices", ci) + ":";
            lblCount.Text = rm.GetString("txtInvoicesDescription", ci);
            lblDays.Text = rm.GetString("txtDaysDescription", ci);
            lblDaysTitle.Text = rm.GetString("txtDays", ci) + ":";
            lblDescription.Text = rm.GetString("txtDescription", ci);
            lblIndustry.Text = rm.GetString("txtIndustryDescription", ci);
            lblIndustryTitle.Text = rm.GetString("txtIndustry", ci) + ":";
            lblPaymentIndexTitle.Text = rm.GetString("txtPaymentIndex", ci);
            lblNoDataAvailable.Text = rm.GetString("txtNoDataAvailable", ci);

            lblTitle.Text = Session["PaymentBehaviourName"].ToString();
            //rm.GetString("txtPaymentBahaviour", ci);
        }

        public void generateChart(int creditInfoID, CompanyNaceCodeBLLC nace) {
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }
            LocalizeText();

            var factory = new CreditinfoGroupFactory();
            lblPaymentIndex.Text = factory.GetPaymentIndex(creditInfoID, !nativeCult);
            PBSData allCompanies = factory.GetPBSAllCompanies();
            PBSData thisIndustry = factory.GetPBSThisIndustry(nace.NaceCode.Substring(0, 2));
            PBSData thisCompany = factory.GetPBSCompany(creditInfoID);

            thisIndustry = RemoveQuartersNotAvailableForCompany(thisCompany, thisIndustry);
            allCompanies = RemoveQuartersNotAvailableForCompany(thisCompany, allCompanies);

            tdSummaryTable.Controls.Add(GenerateSummaryTable(thisCompany, thisIndustry, allCompanies));
            if (ci.Name == "en-US") {
                lblIndustry.Text = rm.GetString("txtIndustryDescription", ci) + " (" + nace.NaceCode + " - " +
                                   nace.DescriptionEN + ")";
            } else {
                lblIndustry.Text = rm.GetString("txtIndustryDescription", ci) + " (" + nace.NaceCode + " - " +
                                   nace.DescriptionNative + ")";
            }
            if (thisCompany.RowCount <= 0) {
                lblNoDataAvailable.Visible = true;
                wcAvg.Visible = false;
                wcStd.Visible = false;
            } else {
                lblNoDataAvailable.Visible = false;
                wcAvg.Visible = true;
                wcStd.Visible = true;
                wcAvg.Charts.Clear();
                wcAvg.ChartTitle.Text = rm.GetString("txtPaymentHistoryAverageDays", ci);

                wcAvg.Charts.Add(GetLineChartAvg(rm.GetString("txtCompany", ci), Color.Maroon, thisCompany));
                wcAvg.Charts.Add(GetLineChartAvg(rm.GetString("txtIndustry", ci), Color.Green, thisIndustry));
                wcAvg.Charts.Add(GetLineChartAvg(rm.GetString("txtAllCompanies", ci), Color.Navy, allCompanies));

                wcStd.Charts.Clear();
                wcStd.ChartTitle.Text = rm.GetString("txtPaymentHistroyStandardDivision", ci);

                wcStd.Charts.Add(GetLineChartStd(rm.GetString("txtCompany", ci), Color.Maroon, thisCompany));
                wcStd.Charts.Add(GetLineChartStd(rm.GetString("txtIndustry", ci), Color.Green, thisIndustry));
                wcStd.Charts.Add(GetLineChartStd(rm.GetString("txtAllCompanies", ci), Color.Navy, allCompanies));

                wcAvg.RedrawChart();
                wcStd.RedrawChart();
            }
        }

        private static PBSData RemoveQuartersNotAvailableForCompany(PBSData company, PBSData target) {
            var newData = new PBSData();
            for (int i = 0; i < company.RowCount; i++) {
                var cRow = company.GetRow(i);
                for (int j = 0; j < target.RowCount; j++) {
                    var tRow = target.GetRow(j);
                    if ((tRow.Quarter != cRow.Quarter) || (tRow.Year != cRow.Year)) {
                        continue;
                    }
                    newData.AddRow(tRow);
                    break;
                }
            }
            return newData;
        }

        private static SmoothLineChart GetLineChartAvg(string name, Color color, PBSData data) {
            var line = new SmoothLineChart {Legend = name};
            line.Line.Color = color;
            line.LineMarker.BorderColor = Color.Black;
            line.LineMarker.Color = Color.Black;
            for (int i = 0; i < data.RowCount; i++) {
                var row = data.GetRow(i);
                line.Data.Add(new ChartPoint(row.Quarter + "." + row.Year, float.Parse(row.Days)));
            }
            return line;
        }

        private static SmoothLineChart GetLineChartStd(string name, Color color, PBSData data) {
            var line = new SmoothLineChart {Legend = name};
            line.Line.Color = color;
            line.LineMarker.BorderColor = Color.Black;
            line.LineMarker.Color = Color.Black;
            for (int i = 0; i < data.RowCount; i++) {
                PBSRow row = data.GetRow(i);
                line.Data.Add(new ChartPoint(row.Quarter + "." + row.Year, float.Parse(row.Std)));
            }
            return line;
        }

        private HtmlTable GenerateSummaryTable(PBSData company, PBSData industry, PBSData all) {
            var table = new HtmlTable {Border = 1, CellPadding = 2, CellSpacing = 1};
            //table.Border=1;

            table.Rows.Add(GetTopRow());

            table.Rows.Add(GetTitleRow());

            for (int i = 0; i < company.RowCount; i++) {
                var row = new HtmlTableRow();

                var cell1 = new HtmlTableCell {InnerText = company.GetRow(i).Year};
                cell1.Style.Add("FONT-WEIGHT", "bold");
                row.Cells.Add(cell1);

                var cell2 = new HtmlTableCell {InnerText = company.GetRow(i).Quarter};
                cell2.Style.Add("FONT-WEIGHT", "bold");
                row.Cells.Add(cell2);

                var cell3 = new HtmlTableCell
                            {
                                Align = "right",
                                InnerText = (company.GetRow(i).Days + " � " + company.GetRow(i).Std)
                            };
                row.Cells.Add(cell3);

                var cell4 = new HtmlTableCell {Align = "right", InnerText = company.GetRow(i).Count};
                row.Cells.Add(cell4);

                var cell5 = new HtmlTableCell {Align = "right"};
                if (industry.RowCount > i) {
                    cell5.InnerText = industry.GetRow(i).Days + " � " + industry.GetRow(i).Std;
                } else {
                    cell5.InnerText = " ";
                }
                row.Cells.Add(cell5);

                var cell6 = new HtmlTableCell
                            {
                                Align = "right",
                                InnerText = (industry.RowCount > i ? industry.GetRow(i).Count : " ")
                            };
                row.Cells.Add(cell6);

                var cell7 = new HtmlTableCell {Align = "right"};
                if (all.RowCount > i) {
                    cell7.InnerText = all.GetRow(i).Days + " � " + all.GetRow(i).Std;
                } else {
                    cell7.InnerText = " ";
                }
                row.Cells.Add(cell7);

                var cell8 = new HtmlTableCell
                            {
                                Align = "right",
                                InnerText = (all.RowCount > i ? all.GetRow(i).Count : " ")
                            };
                row.Cells.Add(cell8);

                table.Rows.Add(row);
            }
            return table;
        }

        private HtmlTableRow GetTopRow() {
            var topRow = new HtmlTableRow();

            topRow.Style.Add("FONT-WEIGHT", "bold");

            var t1 = new HtmlTableCell {ColSpan = 2, InnerHtml = "&nbsp;"};
            topRow.Cells.Add(t1);

            /*HtmlTableCell t2 = new HtmlTableCell();
			t2.InnerText = "nbsp";
			topRow.Cells.Add(t2);*/

            var t3 = new HtmlTableCell {ColSpan = 2, InnerText = rm.GetString("txtCompany", ci)};
            topRow.Cells.Add(t3);

            var t4 = new HtmlTableCell {ColSpan = 2, InnerText = rm.GetString("txtIndustry", ci)};
            topRow.Cells.Add(t4);

            var t5 = new HtmlTableCell {ColSpan = 2, InnerText = rm.GetString("txtAllCompanies", ci)};
            topRow.Cells.Add(t5);

            return topRow;
        }

        private HtmlTableRow GetTitleRow() {
            var titleRow = new HtmlTableRow();

            titleRow.Style.Add("FONT-WEIGHT", "bold");

            var t1 = new HtmlTableCell {InnerText = rm.GetString("txtYear", ci)};
            titleRow.Cells.Add(t1);

            var t2 = new HtmlTableCell {InnerText = "�"};
            titleRow.Cells.Add(t2);

            var t3 = new HtmlTableCell {InnerText = rm.GetString("txtDays", ci)};
            titleRow.Cells.Add(t3);

            var t4 = new HtmlTableCell {InnerText = rm.GetString("txtInvoices", ci)};
            titleRow.Cells.Add(t4);

            var t5 = new HtmlTableCell {InnerText = rm.GetString("txtDays", ci)};
            titleRow.Cells.Add(t5);

            var t6 = new HtmlTableCell {InnerText = rm.GetString("txtInvoices", ci)};
            titleRow.Cells.Add(t6);

            var t7 = new HtmlTableCell {InnerText = rm.GetString("txtDays", ci)};
            titleRow.Cells.Add(t7);

            var t8 = new HtmlTableCell {InnerText = rm.GetString("txtInvoices", ci)};
            titleRow.Cells.Add(t8);

            return titleRow;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}