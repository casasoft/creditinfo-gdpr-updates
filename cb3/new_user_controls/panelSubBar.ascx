<%@ Register TagPrefix="cc1" Namespace="CYBERAKT.WebControls.Navigation" Assembly="ASPnetMenu" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="panelSubBar.ascx.cs" Inherits="CreditInfoGroup.new_user_controls.panelSubBar" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="estream" Namespace="eStreamBG.WebControls" Assembly="eStreamBG.WebControls.PanelBar" %>
<cc1:aspnetmenu id="myMenu" MenuStyle="FreeStyle" CausesValidation="False" runat="server" DefaultItemCssClass="LinkLevel2"
	DefaultItemSpacing="0" Width="100%" ExpandEffect="None" HideSelectElements="False" ContextControlID="mnuMain"
	ShadowEnabled="False" Visible="False"></cc1:aspnetmenu>
<eStream:PanelBar id="thePanelSubBar" runat="server" ItemCssClass="LinkLevel3" Width="100%" ItemWidth="100%"
	ItemTableAlign="Left" ItemTableWidth=" " Height="48px" SpaceBetweenMenus="1px" HeaderColor="White"
	GroupHeight=" " HeaderHeight=" " HeaderCssClass="LinkSubLevel" ExpandEffect="None" HeaderColorOver="Gray"
	ItemBorderColorOver="White" ItemColorOver="0, 192, 0" SingleExpandedMenu="False" NoJavascriptOutput="False"
	NoCookies="False"></eStream:PanelBar>
