#region

using System;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;

#endregion

using Cig.Framework.Base.Configuration;

namespace CreditInfoGroup.new_user_controls {
    /// <summary>
    ///		Summary description for language.
    /// </summary>
    public class language : UserControl {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected ImageButton imbtEN;
        protected ImageButton imgbtIS;

        private void Page_Load(object sender, EventArgs e) {
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();

            //imbtEN.ImageUrl = Request.ApplicationPath+"/img/lang_uk.gif";
            //imgbtIS.ImageUrl = Request.ApplicationPath+"/img/lang_isl.gif";

            // Request.ApplicationPath does not work on production server
            //imbtEN.ImageUrl = Request.ApplicationPath+"/img/lang_" + CigConfig.Configure("lookupsettings.LanguageOne"] + ".gif";
            //imgbtIS.ImageUrl = Request.ApplicationPath+"/img/lang_" + CigConfig.Configure("lookupsettings.LanguageTwo"] + ".gif";
            // Get the corrrect path from session
            imbtEN.ImageUrl = Application.Get("AppPath") + "/img/lang_" +
                              CigConfig.Configure("lookupsettings.LanguageOne") + ".gif";
            imgbtIS.ImageUrl = Application.Get("AppPath") + "/img/lang_" +
                               CigConfig.Configure("lookupsettings.LanguageTwo") + ".gif";

            /*if (Context.User.Identity.IsAuthenticated)
				hlbtnLogout.Visible = true;
			else
				hlbtnLogout.Visible = false;*/
            if (CigConfig.Configure("lookupsettings.HideENLanguageFlags") != null &&
                CigConfig.Configure("lookupsettings.HideENLanguageFlags").ToLower().Equals("true")) {
                imbtEN.Visible = false;
            }

            if (CigConfig.Configure("lookupsettings.HideNativeLanguageFlags") != null &&
                CigConfig.Configure("lookupsettings.HideNativeLanguageFlags").ToLower().Equals("true")) {
                imgbtIS.Visible = false;
            }
        }

        private void imbtEN_Click(object sender, ImageClickEventArgs e) {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(
                CigConfig.Configure("lookupsettings.LanguageOne"), false);
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            Session["culture"] = culture;
            // forcing reload to get the proper Culture in global.asax
            Response.AddHeader("Refresh", "0");
        }

        private void imgbtCYP_Click(object sender, ImageClickEventArgs e) {
            //	Thread.CurrentThread.CurrentCulture = new CultureInfo("el-GR",true);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(
                CigConfig.Configure("lookupsettings.LanguageTwo"), true);
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            Session["culture"] = culture;
            // forcing reload to get the proper Culture in global.asax
            Response.AddHeader("Refresh", "0");
        }

        private static void LocalizeText() {
            //this.hlbtnLogout.Text = rm.GetString("txtLogOut",ci);
            //this.lnkbtnPassword.Text = rm.GetString("lnkbtnPassword",ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.imbtEN.Click += new ImageClickEventHandler(this.imbtEN_Click);
            this.imgbtIS.Click += new ImageClickEventHandler(this.imgbtCYP_Click);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}