#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using CreditInfoGroup.BLL;
using Logging.BLL;
using UserAdmin.BLL;

#endregion

using Cig.Framework.Base.Configuration;


#pragma warning disable 618,612
namespace CreditInfoGroup.new_user_controls {
    /// <summary>
    ///		Summary description for sitePositionBar.
    /// </summary>
    public class sitePositionBar : UserControl {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        private string appPath = "";
        protected ArrayList arrBreadCrumb = new ArrayList();
        public int basepageID;
        private bool nativeCult;

        private void Page_Load(object sender, EventArgs e)
        {
            int basepageID1  = -1;
            if (!int.TryParse(Parent.Page.ID, out basepageID1))
            {
                basepageID1 = -1;
            }
            // Check user access rights to see whether the user has access to this page (url)
            CheckForAccess(basepageID1);

            // check the current culture
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture))
            {
                nativeCult = true;
            }

            // set the apppath (does this work over SSL?)
            appPath = Application.Get("AppPath").ToString();
            if (basepageID1 > -1)
            {
                AddBreadCrumb(basepageID1);
            }
        }

        /// <summary>
        /// Check user access rights for the url he is currently trying to access
        /// </summary>
        /// <param name="pageID">The ID of the current page (url) - Should be the same as an ID in the
        /// products table</param>
        public void CheckForAccess(int pageID) {
            // Pages that do not need validation
            string[] openPages = {"NoAuthorization.aspx", "error.aspx", "logon.aspx", "DatePicker.aspx"};
            bool isClosed = true;

            // Set current page and base path
            string page = Request.FilePath.Substring(Request.FilePath.LastIndexOf("/")).TrimStart('/');
            string path = CigConfig.Configure("lookupsettings.rootURL");
            if (!path.EndsWith("/")) {
                path += "/";
            }

            foreach (string openPage in openPages) {
                if (page.ToLower() == openPage.ToLower()) {
                    isClosed = false;
                }
            }

            // Check user access rights
            if (!Context.User.IsInRole(pageID.ToString()) && isClosed) {
                if (page.ToLower() == "default.aspx") {
                    Session["noAuthorizationPage"] = Request.FilePath;
                    Response.Redirect(path + "logon.aspx", true);
                } else {
                    Session["noAuthorizationPage"] = Request.FilePath;
                    Response.Redirect(path + "NoAuthorization.aspx", true);
                }
            }
        }

        public void AddBreadCrumb(int basepageID1) {
            try {
                // Get available products from catch
                var myProductSet = (DataSet) Cache["myProductSet"];
                var theHlValues = new HyperLinkValues();
                if (myProductSet == null) {
                    myProductSet = myFactory.GetAllTheOpenProducts(false);
                    Cache["myProductSet"] = myProductSet;
                }
                // Search for specific ID
                var myTable = myProductSet.Tables[0];
                var strExpr = "ProductID = " + basepageID1 + "";
                // Use the Select method to find all rows matching the filter.
                var foundRows = myTable.Select(strExpr);
                // Insert the "proper" values if current id was found
                if (foundRows.Length > 0) {
                    theHlValues.Name = nativeCult ? foundRows[0][1].ToString() : foundRows[0][2].ToString();

                    if (!foundRows[0][4].Equals(DBNull.Value)) {
                        theHlValues.Href = appPath + foundRows[0][4];
                    } else {
                        theHlValues.Href = null;
                    }
                    theHlValues.Class = "sitePosition2";

                    //Find the tree also
                    string parentID = foundRows[0][5].ToString();
                    //ArrayList arrBreadCrumbReversed = new ArrayList();
                    //arrBreadCrumbReversed.Add(theHlValues);
                    arrBreadCrumb.Clear();
                    arrBreadCrumb.Add(theHlValues);
                    while (!string.IsNullOrEmpty(parentID)) {
                        strExpr = "ProductID = " + parentID + "";
                        var pRows = myTable.Select(strExpr);
                        // Insert the "proper" values if current id was found
                        if (pRows.Length > 0) {
                            var theHL = new HyperLinkValues
                                        {
                                            Name = (nativeCult ? pRows[0][1].ToString() : pRows[0][2].ToString())
                                        };

                            if (!pRows[0][4].Equals(DBNull.Value)) {
                                theHL.Href = appPath + pRows[0][4];
                            } else {
                                theHL.Href = null;
                            }
                            theHL.Class = "sitePosition2";
                            //arrBreadCrumbReversed.Add(theHL);
                            arrBreadCrumb.Add(theHL);
                            parentID = pRows[0][5].ToString();
                        } else {
                            parentID = "0";
                        }
                    }

                    //arrBreadCrumb.Clear();

                    /*for(int i=arrBreadCrumbReversed.Count-1;i>=0;i--)
					{
						arrBreadCrumb.Add(arrBreadCrumbReversed[i]);
					}*/
                    Session["ArrBreadCrumb"] = arrBreadCrumb;
                }
            } catch (Exception err) {
                Logger.WriteToLog("Villa � Bread Crumb :" + err.Message + " :: " + err.StackTrace, true);
            }

            /*if(Session["ArrBreadCrumb"] != null) 
				arrBreadCrumb = (ArrayList) Session["ArrBreadCrumb"];

			// To hinder same name doubling at refresh...
			HyperLinkValues last = null;
			if (arrBreadCrumb.Count > 0)
			{
				last = (HyperLinkValues)arrBreadCrumb[arrBreadCrumb.Count-1];

				if (last.Name == theHlValues.Name)
					return;
			}

			if(arrBreadCrumb.Count > 4)  // the history list should contain max 5 last pages
				arrBreadCrumb.RemoveAt(0);
			arrBreadCrumb.Add(theHlValues); // insert the control
			Session["ArrBreadCrumb"] = arrBreadCrumb;*/
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new EventHandler(this.Page_Load); }

        #endregion
    }
}
#pragma warning restore 618,612
