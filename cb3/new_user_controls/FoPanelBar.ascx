<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FoPanelBar.ascx.cs" Inherits="CreditInfoGroup.new_user_controls.FoPanelBar" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="cc1" Namespace="CYBERAKT.WebControls.Navigation" Assembly="ASPnetMenu" %>
<%@ Register TagPrefix="estream" Namespace="eStreamBG.WebControls" Assembly="eStreamBG.WebControls.PanelBar" %>
<cc1:aspnetmenu id="myMenu" menustyle="ClassicHorizontal" causesvalidation="False" runat="server"
	defaultitemcssclass="links" defaultitemspacing="0" width="100%" expandeffect="None" hideselectelements="False"
	contextcontrolid="mnuMain" shadowenabled="False" visible="False" defaultitemcssclassover="linksactive"
	defaultitemselectedcssclass="linksselected"></cc1:aspnetmenu>
<estream:panelbar id="thePanelBar" runat="server" itemcssclass="LinkLevel3" width="100%" itemwidth="100%"
	itemtablealign="Left" itemtablewidth=" " height="48px" spacebetweenmenus="1px" headercolor="White"
	groupheight=" " headerheight=" " headercssclass="LinkLevel2" expandeffect="None" headercolorover="Gray"
	itembordercolorover="White" itemcolorover="192, 0, 0" singleexpandedmenu="False" nojavascriptoutput="False"
	nocookies="False" visible="False"></estream:panelbar>
<asp:label id="lblMenu" runat="server" width="100%"></asp:label>
