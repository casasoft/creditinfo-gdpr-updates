<%@ Control Language="c#" AutoEventWireup="false" Codebehind="panelbar.ascx.cs" Inherits="CreditInfoGroup.new_user_controls.panelBar" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="radp" Namespace="Telerik.WebControls" Assembly="RadPanelbar" %>
<table class="menu" cellspacing="0" cellpadding="0">
	<tr>
		<th>
			<asp:label id="lblMenuHeader" runat="server">Menu</asp:label></th></tr>
	<tr>
		<td>
			<table class="datagrid" id="tbCriteria" cellspacing="0" cellpadding="0">
				<tr>
					<td><radp:radpanelbar id="radPanelBar" runat="server" singleexpandedpanel="True" width="100%" theme="ClassicBlue"
							itemexpandedcssclass="panelbarItem" autopostback="True"></radp:radpanelbar></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
