#region

using System;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;

#endregion

namespace CreditInfoGroup.new_user_controls {
    /// <summary>
    ///		Summary description for footer.
    /// </summary>
    public class footer : UserControl {


        protected Literal lblServer;
        protected Literal lblVersionWeb;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here

            Assembly asm = Assembly.GetExecutingAssembly();
            DateTime buildDate = new DateTime(2000, 1, 1).AddDays(asm.GetName().Version.Build).AddSeconds(asm.GetName().Version.Revision * 2);
	        String dateWebApp = buildDate.ToString("dd-MM-yyyy HH:mm:ss");
            String versionWebApp = asm.GetName().Version.ToString();
            lblServer.Text = "Server: " + Server.MachineName;
            lblVersionWeb.Text = "Web: " + versionWebApp + " (" + dateWebApp + ")";
     
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {

       //     lblServer = new LiteralControl();
       //     lblVersionWeb = new LiteralControl();
            this.Load += new System.EventHandler(this.Page_Load); 
        
                
        }

        #endregion
    }
}