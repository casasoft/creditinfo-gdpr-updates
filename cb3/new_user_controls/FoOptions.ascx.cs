#region

using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;

#endregion

namespace CreditInfoGroup.new_user_controls {
    /// <summary>
    ///		Summary description for options.
    /// </summary>
    public class FoOptions : UserControl {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected LinkButton lbtnHelp;
        protected LinkButton lbtnLogout;
        protected LinkButton lbtnPrint;

        private void Page_Load(object sender, EventArgs e) {
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();

            lbtnLogout.Visible = Context.User.Identity.IsAuthenticated;
        }

        private void LocalizeText() {
            lbtnLogout.Text = rm.GetString("txtLogOut", ci) + "&nbsp;&nbsp;<img src=\"" + Application.Get("AppPath") +
                              "/img/logout.gif\" alt=\"\" border=\"0\" align=\"middle\">";
            lbtnHelp.Text = rm.GetString("txtHelp", ci) + "&nbsp;&nbsp;<img src=\"" + Application.Get("AppPath") +
                            "/img/help.gif\" alt=\"\" border=\"0\" align=\"middle\">";
            lbtnPrint.Text = rm.GetString("txtPrint", ci) + "&nbsp;&nbsp;<img src=\"" + Application.Get("AppPath") +
                             "/img/print.gif\" alt=\"\" border=\"0\" align=\"middle\">";
        }

        private void lbtnLogout_Click(object sender, EventArgs e) {
            FormsAuthentication.SignOut();
            Response.AddHeader("Refresh", "0");
            Session.Clear();
            Session.Abandon();
            lbtnLogout.Text = rm.GetString("txtLogOut", ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lbtnLogout.Click += new System.EventHandler(this.lbtnLogout_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}