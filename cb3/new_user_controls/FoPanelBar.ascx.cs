#region

using System;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;
using CYBERAKT.WebControls.Navigation;
using eStreamBG.WebControls;
using UserAdmin.BLL;

#endregion

namespace CreditInfoGroup.new_user_controls {
    /// <summary>
    ///		Summary description for FoPanelBar.
    /// </summary>
    public class FoPanelBar : UserControl {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        protected Label lblMenu;
        protected ASPnetMenu myMenu;
        protected PanelBar thePanelBar;

        private void Page_Load(object sender, EventArgs e) {
            // PanelBar activation
            thePanelBar.LicenseType = PanelBar.eLicenseType.Developer;
            thePanelBar.LicenseName = "Ragnar Dyer";
            thePanelBar.LicenseCompany = "Creditinfo Group Ltd";
            thePanelBar.LicenseNumber = "Ugn1KKQS57yd0vBJrvGoqakpaWs=";

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            //LocalizeText();

            if (!IsPostBack) {
                //BuildSimpleMenu();
                //BuildSimpleMenu2();
                BuildSimpleMenuAsString();
            }
        }

        /// <summary>
        /// Builds the front office menu as a string (via stringbuilder) which is then sent into a label.
        /// </summary>
        private void BuildSimpleMenuAsString() {
            if (Context.User.Identity.Name == "") {
                return;
            }

            var builder = new StringBuilder();
            var myProductSet = myFactory.GetAllTheOpenProducts();

            builder.Append(@"<table cellSpacing='0' cellPadding='0' border='0'><tr>");

            // Athuga hvort notandi hafi a�gang a� v�rum. Birta menu samkv�mt a�gangi notenda...
            for (int i = 0; i < myProductSet.Tables[0].Rows.Count; i++) {
                string navigationPath = GetNavigationPath(myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString());

                // Fyrir Parent gr�ppu - n.b. Datasetti� kemur ra�a� m.v. parentID og svo ProductID. Parents eru me�
                // "0" � ParentID og koma �v� fremst...
                if (myProductSet.Tables[0].Rows[i]["ParentID"] != DBNull.Value &&
                    (int) myProductSet.Tables[0].Rows[i]["ParentID"] == 0) {
                    // Ef notandinn hefur a�gang a� �essari v�ru...
                    if ((Convert.ToInt32(myProductSet.Tables[0].Rows[i]["ProductID"]) < 100) ||
                        (Context.User.IsInRole(myProductSet.Tables[0].Rows[i]["ProductID"].ToString()))
                        && Convert.ToBoolean(myProductSet.Tables[0].Rows[i]["IsInner"]) != true
                        && Convert.ToBoolean(myProductSet.Tables[0].Rows[i]["Menu"])) {
                        // Fyrir native e�a ensku...
                        if (ci.Name == Session["Defculture"].ToString()) {
                            var menuItemName = myProductSet.Tables[0].Rows[i]["MenuNameNative"] != DBNull.Value ? myProductSet.Tables[0].Rows[i]["MenuNameNative"].ToString() : myProductSet.Tables[0].Rows[i]["ProductNameNative"].ToString();

                            builder.AppendFormat(
                                @"<td class='linksMenu' ALIGN='CENTER'><a class='links' id='{0}' href='{1}'>{2}</a></td>",
                                myProductSet.Tables[0].Rows[i]["ProductID"],
                                navigationPath + myProductSet.Tables[0].Rows[i]["NavigateUrl"],
                                menuItemName);
                        } else {
                            var menuItemName = myProductSet.Tables[0].Rows[i]["MenuNameEN"] != DBNull.Value ? myProductSet.Tables[0].Rows[i]["MenuNameEN"].ToString() : myProductSet.Tables[0].Rows[i]["ProductNameEN"].ToString();

                            builder.AppendFormat(
                                @"<td class='linksMenu' ALIGN='CENTER'><a class='links' id='{0}' href='{1}'>{2}</a></td>",
                                myProductSet.Tables[0].Rows[i]["ProductID"],
                                navigationPath + myProductSet.Tables[0].Rows[i]["NavigateUrl"],
                                menuItemName);
                        }
                    }
                }
                    // Fyrir children. Ra�ast undir parents menu items � datasettinu en allir items eru � parentgroup.
                    // �a� �arf s.s. a� "hand" me�h�ndla �esssi parent-child samb�nd (f�sk) vegna �ess hvernig menu
                    // kerfi� er byggt upp (t.d. ekki h�gt a� finna index sem er �a� eina sem h�gt er a� nota � addat).
                else {
                    // Ef notandi hefur a�gang a� parent...
                    if (Context.User.IsInRole(myProductSet.Tables[0].Rows[i]["ParentID"].ToString()) &&
                        Convert.ToBoolean(myProductSet.Tables[0].Rows[i]["IsInner"]) != true &&
                        Convert.ToBoolean(myProductSet.Tables[0].Rows[i]["Menu"])) {
                        // Fyrir native e�a ensku...
                        if (ci.Name == Session["Defculture"].ToString()) {
                            builder.AppendFormat(
                                @"<td class='linksMenu' ALIGN='CENTER'><a class='links' id='{0}' href='{1}'>{2}</a></td>",
                                myProductSet.Tables[0].Rows[i]["ProductID"],
                                navigationPath + myProductSet.Tables[0].Rows[i]["NavigateUrl"],
                                myProductSet.Tables[0].Rows[i]["ProductNameNative"]);
                        } else {
                            builder.AppendFormat(
                                @"<td class='linksMenu' ALIGN='CENTER'><a class='links' id='{0}' href='{1}'>{2}</a></td>",
                                myProductSet.Tables[0].Rows[i]["ProductID"],
                                navigationPath + myProductSet.Tables[0].Rows[i]["NavigateUrl"],
                                myProductSet.Tables[0].Rows[i]["ProductNameEN"]);
                        }
                    }
                }
            }

            builder.Append(@"</tr></table>");
            lblMenu.Text = builder.ToString();
        }

        private string GetNavigationPath(string path) {
/*			if(Application.Get("AppPath").ToString().StartsWith("http://"))
				return "";
			else
				return Application.Get("AppPath").ToString();
*/
            return path.StartsWith("http://") ? "" : Application.Get("AppPath").ToString();
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}