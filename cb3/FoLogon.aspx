<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/FoHead.ascx" %>
<%@ Page language="c#" Codebehind="FoLogon.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.FoLogon" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>login</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						FoLogon.btnLogon.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.FoLogon.tbUserName.focus();
				}

		</SCRIPT>
	</HEAD>
	<body onload="SetFormFocus()" MS_POSITIONING="GridLayout" style="BACKGROUND-IMAGE: url(img/mainback.gif)"
		leftMargin="0" rightMargin="0">
		<form id="FoLogon" name="FoLogon" action="" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
								<td>
									<table cellSpacing="0" cellPadding="0" width="640" align="center" border="0" bgcolor="#951e16">
										<tr>
											<td bgColor="#951e16"><asp:label id="lbLogin" runat="server" CssClass="whiteHeader">Login</asp:label></td>
											<td bgColor="#951e16" align="right"><uc1:language id="Language1" runat="server"></uc1:language></td>
										</tr>
									</table>
								</td>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="640" align="center" border="0" bgcolor="white">
							<!--
							<tr>
								<td>&nbsp;
									<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0" bgcolor="white">
										<tr>
											<td Class="pageheader"></td>
										</tr>
									</table>
								</td>
							</tr>
							-->
							<tr>
								<td>
									<p>
										<table borderColor="#0000ff" cellSpacing="0" cellPadding="5" width="100%" align="center">
											<tr>
												<td style="HEIGHT: 46px" align="right" colSpan="2"><asp:label id="lbUserName" runat="server">User name</asp:label>:</td>
												<td style="HEIGHT: 46px" align="left" colSpan="2"><asp:textbox id="tbUserName" tabIndex="1" runat="server"></asp:textbox></td>
											</tr>
											<tr>
												<td align="right" colSpan="2"><asp:label id="lbPassword" runat="server">Password</asp:label>:</td>
												<td align="left" colSpan="2"><asp:textbox id="tbPassword" tabIndex="2" runat="server" TextMode="Password"></asp:textbox></td>
											</tr>
										</table>
										<table borderColor="#0000ff" cellSpacing="0" cellPadding="0" width="100%" align="center">
											<tr>
												<td style="WIDTH: 350px; HEIGHT: 60px" align="right">
													<div class="AroundButton" style="WIDTH: 80px; HEIGHT: 10px"><asp:button id="btnLogon" tabIndex="3" runat="server" CssClass="RegisterButton" Text="Logon"></asp:button></div>
												</td>
												<td style="HEIGHT: 60px" align="left">
													<div class="AroundButton" style="WIDTH: 80px; HEIGHT: 10px"><asp:button id="btCancel" runat="server" CssClass="RegisterButton" Text="Cancel"></asp:button></div>
												</td>
											</tr>
										</table>
										<table cellSpacing="0" cellPadding="0" width="100%" align="center">
											<tr>
												<td align="center" colSpan="2"><asp:label id="lblMessage" runat="server" ForeColor="Red" Font-Names="Verdana" Font-Size="X-Small"></asp:label></td>
											</tr>
										</table>
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="6" bgcolor="transparent"></td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
