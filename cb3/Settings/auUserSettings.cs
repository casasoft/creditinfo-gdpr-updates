﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Settings
{
    public struct AuUserSettings
    {
        public const int PASSWORD_CHANGE_DAYS = 90;

        public const int NUMBER_OF_WRONG_PASSWORD_ATTEMPT = 5;

    }
}