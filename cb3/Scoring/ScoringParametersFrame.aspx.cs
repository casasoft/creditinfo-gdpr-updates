﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace cb3.Scoring
{
    public partial class ScoringParametersFrame : System.Web.UI.Page
    {
        public ScoringParametersFrame()
        {
        }

        protected HtmlTableCell blas;
        protected PlaceHolder myPlaceHolder;



        protected void Page_Load(object sender, EventArgs e)
        {
            Control uc = null;
            Page.ID = Request.QueryString["action"];
            if (Page.ID == "")
            {
                Page.ID = ViewState["CurrentPage"].ToString();
            }
            ViewState["CurrentPage"] = Page.ID;

            switch (Page.ID)
            {
                case "5001":
                    uc = LoadControl("user_controls/FSIRanges.ascx");
                    uc.ID = "UsageReport";
                    break;

                case "5002":
                    uc = LoadControl("user_controls/CompanyAgeRanges.ascx");
                    uc.ID = "RequestReport";
                    break;


                case "5003":
                    uc = LoadControl("user_controls/AccountAgeRanges.ascx");
                    uc.ID = "RequestReport";
                    break;



                case "5004":
                    uc = LoadControl("user_controls/QuickRatioBound.ascx");
                    uc.ID = "RequestReport";
                    break;

                case "5005":
                    uc = LoadControl("user_controls/RiskBandsRanges.ascx");
                    uc.ID = "RequestReport";
                    break;
            }

            plhMain.Controls.Clear();
            plhMain.Controls.Add(uc);
        }


        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { //this.Load += new System.EventHandler(this.Page_Load);
        }
    }
}
