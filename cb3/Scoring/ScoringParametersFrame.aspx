﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScoringParametersFrame.aspx.cs"
    Inherits="cb3.Scoring.ScoringParametersFrame" %>

<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit"Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>CIG - Scoring Parameters</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema">
    <link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">

    <script language="JavaScript" src="popup.js"></script>

</head>
<body>
   
    <form id="ScoringParameters" method="post" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" ID="ScriptManager2" />
        
        
    <table width="997" height="600" align="center" border="0">
        <tr>
            <td colspan="4">
                <uc1:head ID="Head1" runat="server"></uc1:head>
            </td>
        </tr>
        	<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- IF setningar sem бkvarрa hvaрa user control er 
												loadaр inn н selluna... -->
									<asp:placeholder id="plhMain" runat="server"></asp:placeholder>
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>														
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="3">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>

    </form>
</body>
</html>
