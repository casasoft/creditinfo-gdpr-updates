﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using cb3.CR.DAL;
using System.Threading;
using cb3.Scoring.localization;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Resources;
using cb3.BLL.Scoring;
using System.Web.Caching;
using System.Drawing;

namespace cb3.Scoring
{
    public class ScoringEventArg<T> : List<T>
    {
    }

    public abstract class ScoringControlBase<T> : System.Web.UI.UserControl
        where T : IScoringItem
    {
        protected CultureInfo ci;
        protected ResourceManager rm;
        protected bool EN;
        protected readonly string SessionKey;
        protected Label lblError;

        protected ScoringDAL scoringDal = new ScoringDAL();
        protected event EventHandler OnSuccess;

        protected abstract void LocalizeText();
        protected abstract void UpdateLastRangeValue(ref T lastValue);
        protected abstract bool ValidateItem(T item, bool save);
        protected abstract void AddItem(List<T> ranges, T item);
        protected abstract void DataBind(List<T> ranges);
   
        protected int ItemCount { get; private set; }
        public ScoringControlBase(string sessionKey)
        {
            SessionKey = sessionKey;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US"))
                EN = true;
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();
            this.BindGrid();
        }

        protected void BindGrid()
        {
            List<T> ranges = this.LoadData();
            DataBind(ranges);
        }

        protected List<T> LoadData()
        {
            List<T> ranges = this.GetCahchedRanges();
            if (ranges == null)
            {
                ranges = scoringDal.LoadData<T>();
                this.SetCahcheRanges(ranges);
            }
            return ranges;
        }


        public void RemoveItemFromCahce()
        {
            if (Cache[SessionKey] != null)
                Cache.Remove(SessionKey);
        }

        public void RemovedCallback(String k, Object v, CacheItemRemovedReason r)
        {
        }

        private List<T> GetCahchedRanges()
        {
            if (Cache[SessionKey] != null)
            {
                return (List<T>)Cache[SessionKey];
            }
            return null;
        }

        private void SetCahcheRanges(List<T> ranges)
        {
            var onRemove = new CacheItemRemovedCallback(this.RemovedCallback);
            if (Cache[SessionKey] == null)
                Cache.Add(SessionKey, ranges, null, DateTime.Now.AddSeconds(60), Cache.NoSlidingExpiration, CacheItemPriority.High, onRemove);
            else
                Cache[SessionKey] = ranges;
        }
       
        protected bool CreateOrUpdate(List<T> ranges)
        {
            return scoringDal.CreateOrUpdate<T>(ranges);
        }

        protected virtual void btnNew_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid)
                return;

            var ranges = this.LoadData();
            var lastRange = this.GetLastItem(ranges);


            this.UpdateLastRangeValue(ref lastRange);

            if (!this.ValidateItem(lastRange, true))
                return;

            if (CreateOrUpdate(ranges))
            {
                this.RemoveItemFromCahce();
                if (OnSuccess != null)
                    OnSuccess(this, EventArgs.Empty);
            }
            this.BindGrid();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            RemoveItemFromCahce();
        }

        protected virtual void grid_Command(Object sender, DataGridCommandEventArgs e)
        {
            var button = e.CommandSource as LinkButton;
            if (button == null)
                return;

            var grid = sender as DataGrid;
            List<T> ranges = this.LoadData();
            this.EnableCellValidation(grid.Items[grid.Items.Count - 1].Cells[3], "valToRequiredValidator", true);

            if(grid.Items.Count != 1)
              this.EnableCellValidation(grid.Items[grid.Items.Count - 1].Cells[4], "valRequiredValidator", true);
            var dataGrid = sender as DataGrid;
            if (button.CommandName == "Add")
            {
                Page.Validate();
                if (!Page.IsValid)
                {
                    if (grid.Items.Count != 1)
                    {
                        this.EnableCellValidation(grid.Items[grid.Items.Count - 1].Cells[3], "valToRequiredValidator", false);
                        this.EnableCellValidation(grid.Items[grid.Items.Count - 1].Cells[4], "valRequiredValidator", false);
                    }
                    return;
                }
                var item = GetLastItem(ranges);
                UpdateLastRangeValue(ref item);
                if (!ValidateItem(item, false))
                    return;
                AddItem(ranges, item);
            }
            else
            {
                if (button.CommandName == "Delete")
                {
                    var itemIndex = int.Parse(grid.DataKeys[e.Item.ItemIndex].ToString());
                    MarkAsDeleted(ranges, itemIndex);
                }
            }

            this.SetCahcheRanges(ranges);
            //Session[SessionKey] = ranges;
            this.DataBind(ranges);
        }

        protected virtual void grid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (ItemCount == 1)
                {
                    var button = e.Item.Cells[e.Item.Cells.Count - 2];
                    button.Visible = false;
                    e.Item.Cells[1].Enabled = false;
                    var cell = e.Item.Cells[3];
                    
                    //if (e.Item.DataItem is AccountsAge)
                    //    this.EnableCellValidation(cell, "valRequiredValidator", true);
                    //else
                    //{
                    //    cell.Enabled = false;
                    //    this.EnableCellValidation(cell, "valRequiredValidator", false);
                    //}
                }
                else
                {
                    e.Item.Cells[1].Enabled = false;

                    if (e.Item.ItemIndex == 0)
                        this.EnableCellValidation(e.Item.Cells[3], "valRequiredValidator", false);
                    if (e.Item.ItemIndex != ItemCount - 1)
                    {
                        var button = e.Item.Cells[e.Item.Cells.Count - 1];
                        button.Visible = false;
                        button = e.Item.Cells[e.Item.Cells.Count - 2];
                        button.Visible = false;
                        foreach (TableCell cell in e.Item.Cells)
                            cell.Enabled = false;
                    }
                }
            }
        }

        protected void EnableCellValidation(TableCell cell, string validatorID, bool enable)
        {
            RequiredFieldValidator validator = cell.FindControl(validatorID) as RequiredFieldValidator;
            if (validator != null)
                validator.Enabled = enable;
        }

        int index = 1;
        protected int GetItemIndexCounter()
        {
            return index++;
        }

        protected void ResetItemIndexCounter(IEnumerable<T> ranges)
        {
            this.ItemCount = ranges.Count();
            index = 1;
        }

        protected T GetLastItem(List<T> ranges)
        {
            if(ranges.Count != 0)
                return (T)ranges.Where(x => x.Delete == false).Last();
            return default(T);
        }

        protected void MarkAsDeleted(List<T> ranges, int itemIndex)
        {
            var moveItem = ranges.Find(x => x.ItemIndex == itemIndex);
            if (ranges.Count > 1)
                moveItem.Delete = true;
        }
    }
}
