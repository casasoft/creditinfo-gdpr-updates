﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using cb3.Scoring.localization;
using System.Globalization;
using System.Resources;
using cb3.CR.DAL;
using System.Data;
using System.Drawing;

namespace cb3.Scoring.user_controls
{
    public partial class FSIRanges : ScoringControlBase<cb3.BLL.Scoring.FSI>
    {
        public FSIRanges() : base("FSIRanges") { base.OnSuccess += new EventHandler(FSIRanges_OnSuccess); }

        void FSIRanges_OnSuccess(object sender, EventArgs e)
        {
            lblError.Text = rm.GetString("RangeSavedSuccess");
            lblError.ForeColor = Color.Green;
        }


        protected System.Drawing.Color GetSlectedColor(object col)
        {
            return Color.FromName(col.ToString());
        }

        protected override void grid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (base.ItemCount == 1)
                {
                    var button = e.Item.Cells[e.Item.Cells.Count - 2];
                    button.Visible = false;
                    e.Item.Cells[1].Enabled = false;
                   // var cell = e.Item.Cells[3];
                   // this.EnableCellValidation(cell, "valRequiredValidator", false);
                }
                else
                {
                    if (e.Item.ItemIndex != ItemCount - 1)
                    {
                        var button = e.Item.Cells[e.Item.Cells.Count - 1];
                        button.Visible = false;
                        button = e.Item.Cells[e.Item.Cells.Count - 2];
                        button.Visible = false;

                        foreach (TableCell cell in e.Item.Cells)
                            cell.Enabled = false;

                    }

                    if (e.Item.ItemIndex == ItemCount - 1)
                        e.Item.Cells[1].Enabled = false;
                }
            }
        }
    
        protected override void LocalizeText()
        {
            lblPageHeader.Text = rm.GetString("lblFSIRanges", ci);
            lblDisclaimer.Text = rm.GetString("lblFSIRangesDisclaimer", ci);
        }


        protected override void UpdateLastRangeValue(ref cb3.BLL.Scoring.FSI lastValue)
        {
            DataGridItem item = grid.Items[grid.Items.Count - 1];
            var from = item.FindControl("tbFrom") as TextBox;
            var to = item.FindControl("tbTo") as TextBox;
            var value = item.FindControl("tbValue") as TextBox;
            var riskDescription = item.FindControl("tbRiskDescription") as TextBox;
            var labelColor = item.FindControl("tbColor") as TextBox;
            var nDDD = item.FindControl("tbDDDNumber") as TextBox;
            int result = 0;

            int minTest = 0;
            int? maxTest = new Nullable<int>();
            if (int.TryParse(from.Text, out result))
                minTest = result;
            if (int.TryParse(to.Text, out result))
                maxTest = result;

            lastValue.Color = labelColor.Text;
            lastValue.MinTest = minTest;
            lastValue.MaxTest = maxTest;
            lastValue.RiskDescription = riskDescription.Text;
            lastValue.Value = value.Text;
            lastValue.DDDNumber = int.Parse(nDDD.Text);

        }

        protected override bool ValidateItem(cb3.BLL.Scoring.FSI item, bool save)
        {
            if (item.MaxTest == null || (item.MaxTest.Value < item.MinTest))
            {
                bool valid = true;
                if (item.MaxTest != null)
                {
                    if (item.MaxTest.Value < item.MinTest)
                    {
                        valid = false;
                        lblError.Text = rm.GetString("ToMustBeGerater");
                        lblError.ForeColor = System.Drawing.Color.Red;
                    }
                }
                else
                {
                    if (save == false)
                    {
                        lblError.Text = rm.GetString("ToRequired");
                        lblError.ForeColor = System.Drawing.Color.Red;
                    }
                }
                if (save && valid)
                    return true;
                return false;
            }
            return true;
        }

        protected override void AddItem(List<cb3.BLL.Scoring.FSI> ranges, cb3.BLL.Scoring.FSI item)
        {
            var idx = ranges.Max(x => x.ItemIndex);
            cb3.BLL.Scoring.FSI value = new cb3.BLL.Scoring.FSI() { ItemIndex = idx + 1, MinTest = item.MaxTest.Value, Color="000000" };
            ranges.Add(value);
        }

        protected override void DataBind(List<cb3.BLL.Scoring.FSI> ranges)
        {
            var list = ranges.Where(x => x.Delete == false);
            base.ResetItemIndexCounter(list);
            grid.DataSource = list;
            grid.DataBind();
        }
    }
}