﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuickRatioBound.ascx.cs" Inherits="cb3.Scoring.user_controls.QuickRatioBound" %>
 <table width="100%">
        <tr>
            <td>
                <table class="grid_table" cellspacing="0" cellpadding="0">
                    <tr>
                        <th>
                            <asp:Label ID="lblPageHeader" runat="server">[PageHeader]</asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDisclaimer" runat="server">[PageDisclaimer]</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <asp:Label ID="lblQuickRatio" runat="server">[QuickRatioBound]</asp:Label>
                        </td>
                    </tr>
                            <tr>
                        <td>
                             <asp:TextBox ID="tbQuickRatioBound" runat="server" />
                             <asp:RequiredFieldValidator ID="requiredFielD" ControlToValidate="tbQuickRatioBound" runat="server" ErrorMessage="*" />
                             <asp:RegularExpressionValidator ID="regExpressionVal" runat="server" ControlToValidate="tbQuickRatioBound" ValidationExpression="^[-+]?[0-9]\d{0,2}(\.\d{1,2})?%?$"/>
                             
                             
                        </td>
                    </tr>
                     <tr>
                        <td>
                             <asp:Label ID="lblDDDBound" runat="server">[DDDBound]</asp:Label>
                        </td>
                    </tr>
                            <tr>
                        <td>
                             <asp:TextBox ID="tbDDDBound" runat="server" />
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="tbDDDBound" runat="server" ErrorMessage="*" />
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbDDDBound" ValidationExpression="^[-+]?[0-9]\d{0,2}(\.\d{1,2})?%?$"/>
                             
                             
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="empty_table" cellspacing="0">
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="btnCancel" runat="server" CssClass="cancel_button" Text="Cancel"
                                            CausesValidation="false" OnClick="btnCancel_Click"></asp:Button><asp:Button ID="btnNew"
                                                runat="server" CssClass="confirm_button" Text="Save" OnClick="btnNew_Click">
                                        </asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>