﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RiskBandsRanges.ascx.cs" Inherits="cb3.Scoring.user_controls.RiskBandsRanges" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<body>
    <form id="riskBandsRangesForm" method="post">

    <script type="text/javascript">
        function colorChanged(sender) {
            sender.get_element().style.backgroundColor = "#" + sender.get_selectedColor();
        }
    </script>

    <table width="100%">
        <tr>
            <td>
                <table class="grid_table" cellspacing="0" cellpadding="0">
                    <tr>
                        <th>
                            <asp:Label ID="lblPageHeader" runat="server">[PageHeader]</asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDisclaimer" runat="server">[PageDisclaimer]</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DataGrid ID="grid" DataKeyField="ItemIndex" runat="server" CssClass="tbScoringGrid"
                                OnItemCommand="grid_Command" AllowSorting="True" GridLines="None" AutoGenerateColumns="False"
                                OnItemDataBound="grid_ItemDataBound">
                                <ItemStyle CssClass="trScoringItem"></ItemStyle>
                                <HeaderStyle CssClass="tdScoringHeader"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="ID">
                                        <ItemTemplate>
                                            <%= this.GetItemIndexCounter() %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="From">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" MaxLength="10" ID="tbFrom" Text='<%#DataBinder.Eval(Container.DataItem,"MinTest") %>' />
                                           <asp:RegularExpressionValidator ID="valDecimalFromValidator" runat="server" ErrorMessage="*"
                                                      ControlToValidate="tbFrom" ValidationExpression="100|(([1-9][0-9])|[0-9])((\.|\,)(([0-9][0-9])|[0-9]))?">
                                                 </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="To">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbTo" runat="server" MaxLength="10" Text='<%#DataBinder.Eval(Container.DataItem,"MaxTest") %>' />
                                      <asp:RegularExpressionValidator ID="valDecimalToValidator" runat="server" ErrorMessage="*"
                                                      ControlToValidate="tbTo" ValidationExpression="100|(([1-9][0-9])|[0-9])((\.|\,)(([0-9][0-9])|[0-9]))?">
                                                 </asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Value">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbValue"  runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Value") %>' />
                                            <asp:RequiredFieldValidator ID="valRequiredValidator" ControlToValidate="tbValue"
                                                runat="server" Display="Dynamic" ErrorMessage="*" />
                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="tbValue"
                                                runat="server" Display="Dynamic" ErrorMessage="*" />
                                               
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Risk Description">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbRiskDescription" runat="server" MaxLength="255" Text='<%#DataBinder.Eval(Container.DataItem,"RiskDescription") %>' />
                                            <asp:RequiredFieldValidator ID="valRiskDescriptionValidator" ControlToValidate="tbRiskDescription"
                                                runat="server" Display="Dynamic" ErrorMessage="*" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Label Color">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="tbColor" MaxLength="6"  Width="50px" 
                                                Text='<%#DataBinder.Eval(Container.DataItem,"Color") %>'  BackColor=<%#this.GetSlectedColor(DataBinder.Eval(Container.DataItem,"Color")) %> />
                                            <ajaxToolkit:ColorPickerExtender runat="server" ID="ColorPickerExtender1" TargetControlID="tbColor"
                                                OnClientColorSelectionChanged="colorChanged" />
                                            <asp:ImageButton runat="Server" ID="Image1" Style="width: 15px; height: 15px" CausesValidation="false"
                                                ImageUrl="~/img/cp_button.png" AlternateText="Click to show color picker" />
                                            <ajaxToolkit:ColorPickerExtender ID="buttonCPE" runat="server" TargetControlID="tbColor"
                                                PopupButtonID="Image1" SampleControlID="Sample1" OnClientColorSelectionChanged="colorChanged" SelectedColor='<%#DataBinder.Eval(Container.DataItem,"Color") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:ButtonColumn Text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
                                        CommandName="Delete" CausesValidation="false">
                                        <ItemStyle CssClass="leftpadding1" VerticalAlign="Top" HorizontalAlign="Left"></ItemStyle>
                                    </asp:ButtonColumn>
                                    <asp:ButtonColumn Text="&lt;img src=&quot;../img/new.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
                                        CommandName="Add">
                                        <ItemStyle CssClass="leftpadding1" VerticalAlign="Top" HorizontalAlign="Left"></ItemStyle>
                                    </asp:ButtonColumn>
                                </Columns>
                                <PagerStyle CssClass="grid_pager"></PagerStyle>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="empty_table" cellspacing="0">
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="btnCancel" runat="server" CssClass="cancel_button" Text="Cancel"
                                            CausesValidation="false" OnClick="btnCancel_Click"></asp:Button><asp:Button ID="btnNew"
                                                runat="server" CssClass="confirm_button" Text="Save" OnClick="btnNew_Click">
                                        </asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
