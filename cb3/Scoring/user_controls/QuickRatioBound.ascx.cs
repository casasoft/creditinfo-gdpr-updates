﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cb3.Scoring.localization;
using System.Threading;
using System.Resources;
using System.Globalization;
using System.Drawing;

namespace cb3.Scoring.user_controls
{
    public partial class QuickRatioBound : ScoringControlBase<cb3.BLL.Scoring.QuickRatio>
    {
        public QuickRatioBound() : base("QuickRatioBound") {  }

     
        protected override void LocalizeText()
        {
            lblPageHeader.Text = rm.GetString("lblQuickRatio", ci);
            lblDisclaimer.Text = rm.GetString("lblQuickRatioDisclaimer", ci);
            lblQuickRatio.Text = rm.GetString("lblQuickRatio", ci);
            lblDDDBound.Text = rm.GetString("lblDDDBound", ci);
        }

        protected override void btnNew_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                if (quickRatio != null)
                {
                    quickRatio[0].QRBound = decimal.Parse(tbQuickRatioBound.Text);
                    quickRatio[0].DDDBound = decimal.Parse(tbDDDBound.Text);
                }
                if (CreateOrUpdate(quickRatio))
                {
                    Session[SessionKey] = null;
                    lblError.Text = rm.GetString("QuickRatioBoundSaved");
                    lblError.ForeColor = Color.Green;
                }
            }
        }

        protected override void UpdateLastRangeValue(ref cb3.BLL.Scoring.QuickRatio lastValue)
        {
            throw new NotImplementedException();
        }

        protected override bool ValidateItem(cb3.BLL.Scoring.QuickRatio item, bool save)
        {
            throw new NotImplementedException();
        }

        protected override void AddItem(List<cb3.BLL.Scoring.QuickRatio> ranges, cb3.BLL.Scoring.QuickRatio item)
        {
            throw new NotImplementedException();
        }

        List<cb3.BLL.Scoring.QuickRatio> quickRatio;
        protected override void DataBind(List<cb3.BLL.Scoring.QuickRatio> ranges)
        {
            if (ranges.Count == 1)
                quickRatio = ranges;
            else
            {
                quickRatio = new List<cb3.BLL.Scoring.QuickRatio>();
                quickRatio.Add(new cb3.BLL.Scoring.QuickRatio());
            }
            tbQuickRatioBound.Text = quickRatio[0].QRBound.ToString();
            tbDDDBound.Text = quickRatio[0].DDDBound.ToString();
        }
    }
}