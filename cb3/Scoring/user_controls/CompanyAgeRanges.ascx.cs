﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cb3.CR.DAL;
using System.Threading;
using cb3.Scoring.localization;
using System.Globalization;
using System.Resources;
using System.Drawing;

namespace cb3.Scoring.user_controls
{
    public partial class CompanyAgeRanges : ScoringControlBase<cb3.BLL.Scoring.CompanyAge>
    {
        public CompanyAgeRanges() : base("CompanyAgeRanges") { base.OnSuccess += new EventHandler(CompanyAgeRanges_OnSuccess); }

        void CompanyAgeRanges_OnSuccess(object sender, EventArgs e)
        {
            lblError.Text = rm.GetString("RangeSavedSuccess");
            lblError.ForeColor = Color.Green;
        }

        protected override void grid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            var cell = e.Item.Cells[3];
            
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (ItemCount == 1)
                {
                    this.EnableCellValidation(cell, "valRequiredValidator", false);
                    cell.Enabled = false;
                }
            }


            
            base.grid_ItemDataBound(sender, e);
        }
        protected override void LocalizeText()
        {
            lblError.Text = string.Empty;
            lblPageHeader.Text = rm.GetString("lblCompanyAgeRanges", ci);
            lblDisclaimer.Text = rm.GetString("lblCompanyAgeDisclaimer", ci);
        }

        protected override void DataBind(List<cb3.BLL.Scoring.CompanyAge> ranges)
        {
            var list = ranges.Where(x => x.Delete == false);
            base.ResetItemIndexCounter(list);
            grid.DataSource = list;
            grid.DataBind();
            this.EnableCellValidation(grid.Items[grid.Items.Count - 1].Cells[3], "valToRequiredValidator", false);
        }

        protected override void UpdateLastRangeValue(ref cb3.BLL.Scoring.CompanyAge lastValue)
        {
            DataGridItem item = grid.Items[grid.Items.Count - 1];
            var from = item.FindControl("tbFrom") as TextBox;
            var to = item.FindControl("tbTo") as TextBox;
            var value = item.FindControl("tbValue") as TextBox;
            int result = 0;
            int minTest = 0;
            int? maxTest = new Nullable<int>();
            if (int.TryParse(from.Text, out result))
                minTest = result;
            if (int.TryParse(to.Text, out result))
                maxTest = result;

            lastValue.MinTest = minTest;
            lastValue.MaxTest = maxTest;

            decimal outResult;
            if (decimal.TryParse(value.Text, out outResult))
            {
                lastValue.Value = outResult;
            }
        }


        protected override bool ValidateItem(cb3.BLL.Scoring.CompanyAge item, bool save)
        {
            if (save && item.MaxTest.HasValue)
            {
                lblError.Text = rm.GetString("CompanyAgeLastRangeMustHaveEmpty");
                lblError.ForeColor = System.Drawing.Color.Red;
                return false;
            }

            if (!item.MaxTest.HasValue || (item.MaxTest.HasValue && item.MaxTest.Value < item.MinTest))
            {
                bool valid = true;
                if (item.MaxTest.HasValue)
                {
                    if (item.MaxTest.Value < item.MinTest)
                    {
                       valid  = false;
                       lblError.Text = rm.GetString("ToMustBeGerater");
                       lblError.ForeColor = System.Drawing.Color.Red;
                    }
                }
                else
                {
                    if (!save)
                    {
                        lblError.Text = rm.GetString("ToRequired");
                        lblError.ForeColor = System.Drawing.Color.Red;
                    }
                }
                if (save && valid)
                    return true;
                return false;
            }

            return true;
        }

        protected override void AddItem(List<cb3.BLL.Scoring.CompanyAge> ranges, cb3.BLL.Scoring.CompanyAge item)
        {
             var idx = ranges.Max(x => x.ItemIndex);
             cb3.BLL.Scoring.CompanyAge fsi = new cb3.BLL.Scoring.CompanyAge() { ItemIndex = idx + 1, MinTest = item.MaxTest.Value };
             ranges.Add(fsi);
        }
    }
}
      