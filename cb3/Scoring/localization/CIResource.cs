﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Resources;
using System.Reflection;

namespace cb3.Scoring.localization
{
    public class CIResource
    {
        private const string ApplicationVariableName = "Scoring";

        /// <summary>
        /// <para>The CurrentManager property returns and caches the shared ResourceManager instance.
        /// </para>
        /// </summary>
        public static ResourceManager CurrentManager
        {
            get
            {
                var context = HttpContext.Current;
                if (null == context)
                {
                    throw new ArgumentException("Global_NoHttpContext");
                }
                var mgr = context.Cache[ApplicationVariableName] as ResourceManager;

                if (null == mgr)
                {
                    mgr = new ResourceManager(
                        "cb3.Scoring.resources.strings", Assembly.GetExecutingAssembly(), null);

                    // Add to the cache
                    context.Cache.Insert(ApplicationVariableName, mgr);
                }
                return mgr;
            }
        }
    }
}