using System;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using CR.Localization;
using CR.MTBLL;
using Logging.BLL;
using System.Web.Security;

using Cig.Framework.Base.Configuration;

namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for FoMTOtherInformation.
    /// </summary>
    public class FoMTOtherInformation : Page {
        protected Button btnClear;
        protected Button btnFind;
        private CultureInfo ci;
        private string culture;
        protected Label lblMessage;
        protected Label lblName;
        protected Label lblNationalID;
        protected Label lblOtherInformation;
        protected Label lblTitle;
        private ResourceManager rm;
        protected HtmlTableCell tdReportDisplay;
        protected HtmlTableRow trReportDisplay;
        protected TextBox txtName;
        protected TextBox txtNationalID;

        private void Page_Load(object sender, EventArgs e)
        {
            AddEnterEvent();

            if (Session["UserLoginID"] == null || Session["UserLoginID"].ToString() == string.Empty)
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session.Abandon();
                Response.Redirect("~/fodefault.aspx");
                Response.End();
            }

            culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) { }

            lblMessage.Visible = false;
            trReportDisplay.Visible = false;
            btnClear.Attributes.Add("onclick", "return clear_info()");
            if (!Page.IsPostBack)
            {
                LocalizeText();
            }
        }

        private void AddEnterEvent() {
            var frm = FindControl("FoOtherInformation");
            foreach (Control ctrl in frm.Controls) {
                if (!(ctrl is TextBox)) {
                    continue;
                }
                ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
            }
        }

        private void LocalizeText() {
            btnClear.Text = rm.GetString("txtClear", ci);
            btnFind.Text = rm.GetString("txtSearch", ci);
            lblOtherInformation.Text = rm.GetString("txtOtherInformation", ci);
            lblTitle.Text = rm.GetString("txtOtherInformationTitle", ci);
            lblNationalID.Text = rm.GetString("txtNationalID", ci);
            lblName.Text = rm.GetString("txtName", ci);
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblMessage.Text = errorMessage;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Visible = true;
        }

        private void DisplayMessage(String message) {
            lblMessage.Text = message;
            lblMessage.ForeColor = Color.Blue;
            lblMessage.Visible = true;
        }

        private void btnFind_Click(object sender, EventArgs e) {
            try {
                if (txtNationalID.Text.Trim() != "" || txtName.Text.Trim() != "") {
                    int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
                    int userID = int.Parse(Session["UserLoginID"].ToString());
                    string ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                    var generator = new MTReportGenerator(
                        rm, culture, userCreditInfoID, userID, ipAddress)
                                    {
                                        ReportType =
                                            Convert.ToInt32(
                                            ConfigurationSettings.AppSettings.Get("OtherInformationReportType"))
                                    };
                    var transformer = new XslTransform();

                    if (CigConfig.Configure("lookupsettings.foRootName") != null) {
                        transformer.Load(
                            Server.MapPath(
                                CigConfig.Configure("lookupsettings.foRootName") + "/CR/xsl/mt/MTReports.xslt"));
                    } else // at least it will work this way at production server
                    {
                        transformer.Load(Application.Get("AppPath") + "/CR/xsl/mt/MTReports.xslt");
                    }

                    //transformer.Load(Server.MapPath(Application.Get("AppPath")+"/CR/xsl/mt/MTReportStyles.xslt")); 
                    var sw = new StringWriter();
                    var doc = generator.GetOtherInformations(txtNationalID.Text, txtName.Text);
                    if (doc != null) {
                        transformer.Transform(doc, null, sw, null);
                        trReportDisplay.Visible = true;
                        tdReportDisplay.InnerHtml = sw.ToString();
                    } else {
                        Logger.WriteToLog(
                            "Error displaying court orders for national id  " + txtNationalID.Text + " or name " +
                            txtName.Text + " xmlDocument is null",
                            true);
                        DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
                    }
                } else {
                    DisplayMessage(rm.GetString("txtNoSearchCriteria", ci));
                }
            } catch (Exception exc) {
                Logger.WriteToLog(
                    "Error displaying court orders for national id  " + txtNationalID.Text + " or name " + txtName.Text,
                    exc,
                    true);
                DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}