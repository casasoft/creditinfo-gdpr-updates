#region

using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using FSI.BLL;
using FSI.Localization;

#endregion

namespace FSI {
    /// <summary>
    /// Summary description for SelectStatement.
    /// </summary>
    public class SelectStatement : Page {
        private static readonly FinancialStatementFactory theStatementFact = new FinancialStatementFactory();
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected Label blCompanyRegno;
        protected Button btnCancel;
        protected Button btnDelete;
        protected Button btnRegisterChange;
        protected DropDownList ddAccountPeriod;
        protected Label lblAccountPeriod;
        protected Label lblCompanyRegno;
        //	protected System.Web.UI.WebControls.Label lblFiscalYear;
        //	protected System.Web.UI.WebControls.TextBox tbFiscalYear;
        //	protected System.Web.UI.WebControls.LinkButton lbSearchNationalRegistry;
        //	protected System.Web.UI.WebControls.Button btnRegisterChange;
        //	protected System.Web.UI.WebControls.Button btnDelete;
        //	protected System.Web.UI.WebControls.Button btnCancel;
        protected Label lblControl;
        protected Label lblErrMsg;
        protected Label lblFiscalYear;
        protected Label lblPageTitle;
        protected Label lblSelect;
        protected RequiredFieldValidator rfvCIID;
        protected RequiredFieldValidator rfvFisicalYear;
        protected TextBox tbCompanyRegNo;
        protected TextBox tbCompRegNo;
        protected TextBox tbFiscalYear;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            // check the current culture
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            lblErrMsg.Visible = false;

            // Add <ENTER> event
            AddEnterEvent();

            if (!IsPostBack) {
                Session.Remove("FSI_tbCompanyRegNo");
                Session.Remove("FSI_year");
                Session.Remove("FSI_ddAccountPeriod");
                LocalizeText();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e) {
            // check if the statement exists...
            int companyCIID = theStatementFact.GetCIIDByNationalID(tbCompanyRegNo.Text);
            bool exists = false;
            if (companyCIID > 0) {
                exists = theStatementFact.IsStatementRegisted(
                    companyCIID, Convert.ToInt32(tbFiscalYear.Text), Convert.ToInt32(ddAccountPeriod.SelectedValue));
            }

            if (exists) {
                Server.Transfer("ConfirmDelete.aspx?ref=selstate");
            } else {
                lblErrMsg.Text = rm.GetString("txtStatementNotExists", ci);
                lblErrMsg.Visible = true;
            }
        }

        private void btnRegisterChange_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                int companyCIID = theStatementFact.GetCIIDByNationalID(tbCompanyRegNo.Text);
                // ef fyrirt�ki� er ekki til � kerfinu � �a� �.a.l. ekki �rsreikning
                if (companyCIID >= 0) {
                    Session["FSI_tbCompanyRegNo"] = tbCompanyRegNo.Text;
                    Session["FSI_year"] = tbFiscalYear.Text;
                    Session["FSI_ddAccountPeriod"] = ddAccountPeriod.SelectedValue;
                    Server.Transfer("StatementInput.aspx?ref=selstate");
                } else {
                    lblErrMsg.Text = rm.GetString("txtCompanyNotFound", ci);
                    lblErrMsg.Visible = true;
                }
            }
        }

        private void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtFinanciaStatement", ci);
            lblControl.Text = rm.GetString("txtControl", ci);
            blCompanyRegno.Text = rm.GetString("txtCompanyRegNo", ci);
            lblFiscalYear.Text = rm.GetString("txtFiscalYear", ci);
            btnRegisterChange.Text = rm.GetString("txtRegisterChange", ci);
            btnDelete.Text = rm.GetString("txtDelete", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);
            lblAccountPeriod.Text = rm.GetString("txtAccountPeriod", ci);
            rfvCIID.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvFisicalYear.ErrorMessage = rm.GetString("txtValueMissing", ci);
        }

        private void AddEnterEvent() {
            tbCompanyRegNo.Attributes.Add("onkeypress", "checkEnterKey();");
            tbFiscalYear.Attributes.Add("onkeypress", "checkEnterKey();");
            ddAccountPeriod.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.btnRegisterChange.Click += new System.EventHandler(this.btnRegisterChange_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}