#region

using System;
using System.Web.UI;

#endregion

//using FSI.user_control;

namespace FSI {
    /// <summary>
    /// Summary description for TemplateTest.
    /// </summary>
    public class TemplateTest : Page {
        //	public static int pageID = 805;
        //	protected sitePositionBar sitePositionBar;
        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
            //	sitePositionBar.basepageID = pageID;
            Page.ID = "805";
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}