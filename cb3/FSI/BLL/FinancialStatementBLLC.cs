#region

using System;
using System.Collections;

#endregion

namespace FSI.BLL {
    /// <remarks>
    /// This class provide all neccessary info for FinancialStatements
    /// </remarks>
    public class FinancialStatementBLLC {
        /// <summary>
        /// The accounts recievable
        /// </summary>
        private long accountsRecievable = int.MinValue;

        /// <summary>
        /// The administrative and other expenses
        /// </summary>
        private long administrativeAndOtherExpenses = int.MinValue;

        /// <summary>
        /// The cash at bank and in hand
        /// </summary>
        private long cashAtBankAndInHand = int.MinValue;

        /// <summary>
        /// The cash from operations
        /// </summary>
        private long cashFromOperations = int.MinValue;

        /// <summary>
        /// The cash provided by operating activities
        /// </summary>
        private long cashProvidedByOperatingActivities = int.MinValue;

        /// <summary>
        /// The claims towards associated companies
        /// </summary>
        private long claimsTowardsAssociatedCompanies = int.MinValue;

        /// <summary>
        /// The cost of sales
        /// </summary>
        private long costOfSales = int.MinValue;

        /// <summary>
        /// The credit institutions
        /// </summary>
        private long creditInstitutions = int.MinValue;

        /// <summary>
        /// The creditors (loans? debts?)
        /// </summary>
        private long creditors = int.MinValue;

        /// <summary>
        /// The current assets attribute
        /// </summary>
        private long currentAssets = int.MinValue;

        /// <summary>
        /// The Current assets less current liabilities
        /// </summary>
        private long currentAssetsLessCurrentLiabilities = int.MinValue;

        /// <summary>
        /// The Current portion of long term liabilities attribute
        /// </summary>
        private long currentPortionOfLongTermLiabilities = int.MinValue;

        /// <summary>
        /// The debtors
        /// </summary>
        private long debtors = int.MinValue;

        /// <summary>
        /// The debt to associated companies
        /// </summary>
        private long debtToAssociatedCompanies = int.MinValue;

        /// <summary>
        /// The Deferred tax asset
        /// </summary>
        private long deferredTaxAsset = int.MinValue;

        /// <summary>
        /// The depcrication of fixed assets
        /// </summary>
        private long depricationOfFixedAssets = int.MinValue;

        /// <summary>
        /// The distribution cost
        /// </summary>
        private long distributionCost = int.MinValue;

        /// <summary>
        /// The extra ordinary income cost
        /// </summary>
        private long extraOrdinaryIncomeCost = int.MinValue;

        /// <summary>
        /// The financial activities
        /// </summary>
        private long financialActivities = int.MinValue;

        /// <summary>
        /// The financial assets
        /// </summary>
        private long financialAssest = int.MinValue;

        /// <summary>
        /// The financial cost
        /// </summary>
        private long financialCost = int.MinValue;

        /// <summary>
        /// The financial income
        /// </summary>
        private long financialIncome = int.MinValue;

        /// <summary>
        /// The gross profit
        /// </summary>
        private long grossProfit = int.MinValue;

        /// <summary>
        /// The income from affiliates attribute
        /// </summary>
        private long incomeFromAffiliates = int.MinValue;

        /// <summary>
        /// The income from associated companies
        /// </summary>
        private long incomeFromAssociatedCompanies = int.MinValue;

        /// <summary>
        /// The income from operations after tax
        /// </summary>
        private long incomeFromOperationsAfterTax = int.MinValue;

        /// <summary>
        /// The operations before tax
        /// </summary>
        private long incomeFromOperationsBeforeTax = int.MinValue;

        /// <summary>
        /// The income from subsidiares
        /// </summary>
        private long incomeFromSubsidiaries = int.MinValue;

        /// <summary>
        /// The income tax
        /// </summary>
        private long incomeTax = int.MinValue;

        /// <summary>
        /// The increase/decrease in cash and in hand
        /// </summary>
        private long increaseDecreaseInCashAndCashInHand = int.MinValue;

        /// <summary>
        /// The intangable assets
        /// </summary>
        private long intangableAssets = int.MinValue;

        /// <summary>
        /// The inventory
        /// </summary>
        private long inventory = int.MinValue;

        /// <summary>
        /// The investing activities
        /// </summary>
        private long investingActivities = int.MinValue;

        // vi�b�tur � Balance Sheet - Equity & Debt
        /// <summary>
        /// The Issued share capital
        /// </summary>
        private long issuedShareCapital = int.MinValue;

        /// <summary>
        /// The loans overdraft
        /// </summary>
        private long loansOverdraft = int.MinValue;

        /// <summary>
        /// The long term debts
        /// </summary>
        private long longTermDebts = int.MinValue;

        /// <summary>
        /// The Minority holdings in Equity
        /// </summary>
        private long minorityHoldingsInEquity = int.MinValue;

        /// <summary>
        /// The net financial items
        /// </summary>
        private long netFinancialItems = int.MinValue;

        /// <summary>
        /// The net profit
        /// </summary>
        private long netProfit = int.MinValue;

        /// <summary>
        /// The operating profit before working capital changes
        /// </summary>
        private long operatingProfitBeforeWorkingCapitalChanges = int.MinValue;

        /// <summary>
        /// The operational profit
        /// </summary>
        private long operationalProfit = int.MinValue;

        /// <summary>
        /// The other current assets
        /// </summary>
        private long otherCurrentAssets = int.MinValue;

        /// <summary>
        /// The other equity
        /// </summary>
        private long otherEquity = int.MinValue;

        /// <summary>
        /// The other financial entries
        /// </summary>
        private long otherFinancialEntries = int.MinValue;

        /// <summary>
        /// The other liabilities
        /// </summary>
        private long otherLiabilities = int.MinValue;

        /// <summary>
        /// The Other operation revenue
        /// </summary>
        private long otherOperationRevenue = int.MinValue;

        /// <summary>
        /// The other short term debts
        /// </summary>
        private long otherShortTermDebt = int.MinValue;

        /// <summary>
        /// The pension funds liabilities attribute
        /// </summary>
        private long pensionFundsLiabilities = int.MinValue;

        /// <summary>
        /// The Profit and Loss account
        /// </summary>
        private long profitAndLossAccount = int.MinValue;

        /// <summary>
        /// The result before income from assoceated companies
        /// </summary>
        private long resultBeforeIncomeFromAssociatedCompanies = int.MinValue;

        /// <summary>
        /// The revenue from main operation
        /// </summary>
        private long revenueFromMainOperation = int.MinValue;

        /// <summary>
        /// The short term debts
        /// </summary>
        private long shortTermDebts = int.MinValue;

        /// <summary>
        /// The short term investments
        /// </summary>
        private long shortTermInvestments = int.MinValue;

        /// <summary>
        /// The staff cost
        /// </summary>
        private long staffCost = int.MinValue;

        /// <summary>
        /// The staff count
        /// </summary>
        private int staffCount = int.MinValue;

        /// <summary>
        /// The stock
        /// </summary>
        private long stock = int.MinValue;

        /// <summary>
        /// The tangable assets
        /// </summary>
        private long tangableAssets = int.MinValue;

        /// <summary>
        /// The Tax liabilities
        /// </summary>
        private long taxLiabilities = int.MinValue;

        /// <summary>
        /// The total assets
        /// </summary>
        private long totalAssets = int.MinValue;

        /// <summary>
        /// The total cost
        /// </summary>
        private long totalCost = int.MinValue;

        /// <summary>
        /// The total current assets
        /// </summary>
        private long totalCurrentAssets = int.MinValue;

        /// <summary>
        /// The total debts equity
        /// </summary>
        private long totalDebtsEquity = int.MinValue;

        /// <summary>
        /// The total equity
        /// </summary>
        private long totalEquity = int.MinValue;

        /// <summary>
        /// The total fixed assets
        /// </summary>
        private long totalFixedAssets = int.MinValue;

        /// <summary>
        /// The total liabilities
        /// </summary>
        private long totalLiabilities = int.MinValue;

        /// <summary>
        /// The Total revenue
        /// </summary>
        private long totalRevenue = int.MinValue;

        /// <summary>
        /// The total short term debts
        /// </summary>
        private long totalShortTermDebts = int.MinValue;

        public FinancialStatementBLLC() { Shareholders = new ArrayList(); }

        /// <summary>
        /// Gets/sets the companyCIID attribute
        /// </summary>
        public int CompanyCIID { get; set; }

        /// <summary>
        /// Gets/sets the fiscalYear attribute
        /// </summary>
        public string FiscalYear { get; set; }

        /// <summary>
        /// Gets/sets the staffCount attribute
        /// </summary>
        public int StaffCount { get { return staffCount; } set { staffCount = value; } }

        /// <summary>
        /// Gets/sets the originID attribute
        /// </summary>
        public int OriginID { get; set; }

        /// <summary>
        /// Gets/sets the currencyID attribute
        /// </summary>
        public string CurrencyID { get; set; }

        /// <summary>
        /// Gets/sets the mostRecentAccount attribute 
        /// </summary>
        public string MostRecentAccount { get; set; }

        /// <summary>
        /// Gets/sets the yearEnded attribute
        /// </summary>
        public DateTime YearEnded { get; set; }

        /// <summary>
        /// Gets/sets the accountMonths attribute
        /// </summary>
        public int AccountMonths { get; set; }

        /// <summary>
        /// Gets/sets the consolidated attribute
        /// </summary>
        public bool Consolidated { get; set; }

        /// <summary>
        /// Gets/sets the qualified attribute
        /// </summary>
        public bool Qualified { get; set; }

        /// <summary>
        /// Gets/sets the smallStatus attribute
        /// </summary>
        public bool SmallStatus { get; set; }

        /// <summary>
        /// Gets/sets the comments attribute
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets/sets the incomeFromMainOperation attribute
        /// </summary>
        public long RevenueFromMainOperation { get { return revenueFromMainOperation; } set { revenueFromMainOperation = value; } }

        /// <summary>
        /// Gets/sets the otherOperationRevenue attribute
        /// </summary>
        public long OtherOperationRevenue { get { return otherOperationRevenue; } set { otherOperationRevenue = value; } }

        /// <summary>
        /// Gets/sets the totalRevenue attribute
        /// </summary>
        public long TotalRevenue { get { return totalRevenue; } set { totalRevenue = value; } }

        /// <summary>
        /// Gets/sets the costOfSales attribute
        /// </summary>
        public long CostOfSales { get { return costOfSales; } set { costOfSales = value; } }

        /// <summary>
        /// Gets/sets the grossProfit attribute
        /// </summary>
        public long GrossProfit { get { return grossProfit; } set { grossProfit = value; } }

        /// <summary>
        /// Gets/sets the staffCost attribute
        /// </summary>
        public long StaffCost { get { return staffCost; } set { staffCost = value; } }

        /// <summary>
        /// Gets/sets the depricationOfFixedAssets attribute
        /// </summary>
        public long DepricationOfFixedAssets { get { return depricationOfFixedAssets; } set { depricationOfFixedAssets = value; } }

        /// <summary>
        /// Gets/sets the operationalProfit attribute
        /// </summary>
        public long OperationalProfit { get { return operationalProfit; } set { operationalProfit = value; } }

        /// <summary>
        /// Gets/sets the otherFinancialEntries attribute
        /// </summary>
        public long OtherFinancialEntries { get { return otherFinancialEntries; } set { otherFinancialEntries = value; } }

        /// <summary>
        /// Gets/sets the netFinancialItems attribute
        /// </summary>
        public long NetFinancialItems { get { return netFinancialItems; } set { netFinancialItems = value; } }

        /// <summary>
        /// Gets/sets the incomeFromOperationsBeforeTax attribute
        /// </summary>
        public long IncomeFromOperationsBeforeTax { get { return incomeFromOperationsBeforeTax; } set { incomeFromOperationsBeforeTax = value; } }

        /// <summary>
        /// Gets/sets the incomeTax attribute
        /// </summary>
        public long IncomeTax { get { return incomeTax; } set { incomeTax = value; } }

        /// <summary>
        /// Gets/sets the netProfit attribute
        /// </summary>
        public long NetProfit { get { return netProfit; } set { netProfit = value; } }

        /// <summary>
        /// Gets/sets the intangableAssets attribute
        /// </summary>
        public long IntangableAssets { get { return intangableAssets; } set { intangableAssets = value; } }

        /// <summary>
        /// Gets/sets the tangableAssets attribute
        /// </summary>
        public long TangableAssets { get { return tangableAssets; } set { tangableAssets = value; } }

        /// <summary>
        /// Gets/sets the financialAssets attribute
        /// </summary>
        public long FinancialAssets { get { return financialAssest; } set { financialAssest = value; } }

        /// <summary>
        /// Gets/sets the totalFixedAssets attribute
        /// </summary>
        public long TotalFixedAssets { get { return totalFixedAssets; } set { totalFixedAssets = value; } }

        /// <summary>
        /// Gets/sets the stock attribute
        /// </summary>
        public long Stock { get { return stock; } set { stock = value; } }

        /// <summary>
        /// Gets/sets the debtors attribute
        /// </summary>
        public long Debtors { get { return debtors; } set { debtors = value; } }

        /// <summary>
        /// Gets/sets the claimsTowardsAssociatedCompanies attribute
        /// </summary>
        public long ClaimsTowardsAssociatedCompanies { get { return claimsTowardsAssociatedCompanies; } set { claimsTowardsAssociatedCompanies = value; } }

        /// <summary>
        /// Gets/sets the shortTermInvestments attribute
        /// </summary>
        public long ShortTermInvestments { get { return shortTermInvestments; } set { shortTermInvestments = value; } }

        /// <summary>
        /// Gets/sets the otherCurrentAssets attribute
        /// </summary>
        public long OtherCurrentAssets { get { return otherCurrentAssets; } set { otherCurrentAssets = value; } }

        /// <summary>
        /// Gets/sets the cashAtBankAndInHand attribute
        /// </summary>
        public long CashAtBankAndInHand { get { return cashAtBankAndInHand; } set { cashAtBankAndInHand = value; } }

        /// <summary>
        /// Gets/sets the totalCurrentAssets attribute
        /// </summary>
        public long TotalCurrentAssets { get { return totalCurrentAssets; } set { totalCurrentAssets = value; } }

        /// <summary>
        /// Gets/sets the totalAssets attribute
        /// </summary>
        public long TotalAssets { get { return totalAssets; } set { totalAssets = value; } }

        /// <summary>
        /// Gets/sets the otherEquity attribute
        /// </summary>
        public long OtherEquity { get { return otherEquity; } set { otherEquity = value; } }

        /// <summary>
        /// Gets/sets the totalEquity attribute
        /// </summary>
        public long TotalEquity { get { return totalEquity; } set { totalEquity = value; } }

        /// <summary>
        /// Gets/sets the longTermDebts attribute
        /// </summary>
        public long LongTermDebts { get { return longTermDebts; } set { longTermDebts = value; } }

        /// <summary>
        /// Gets/sets the creditInstitutions attribute
        /// </summary>
        public long CreditInstitutions { get { return creditInstitutions; } set { creditInstitutions = value; } }

        /// <summary>
        /// Gets/sets the accountsRecievable attribute
        /// </summary>
        public long AccountsRecievable { get { return accountsRecievable; } set { accountsRecievable = value; } }

        /// <summary>
        /// Gets/sets the debtToAssociatedCompanies attribute
        /// </summary>
        public long DebtToAssociatedCompanies { get { return debtToAssociatedCompanies; } set { debtToAssociatedCompanies = value; } }

        /// <summary>
        /// Gets/sets the operatingProfitBeforeWorkingCapitalChanges attribute
        /// </summary>
        public long OperatingProfitBeforeWorkingCapitalChanges { get { return operatingProfitBeforeWorkingCapitalChanges; } set { operatingProfitBeforeWorkingCapitalChanges = value; } }

        /// <summary>
        /// Gets/sets the cashProvidedByOperatingActivities attribute
        /// </summary>
        public long CashProvidedByOperatingActivities { get { return cashProvidedByOperatingActivities; } set { cashProvidedByOperatingActivities = value; } }

        /// <summary>
        /// Gets/sets the investingActivities attribute
        /// </summary>
        public long InvestingActivities { get { return investingActivities; } set { investingActivities = value; } }

        /// <summary>
        /// Gets/sets the financialActivities attribute
        /// </summary>
        public long FinancialActivities { get { return financialActivities; } set { financialActivities = value; } }

        /// <summary>
        /// Gets/sets the increaseDecreaseInCashAndCashInHand attribute
        /// </summary>
        public long IncreaseDecreaseInCashAndCashInHand { get { return increaseDecreaseInCashAndCashInHand; } set { increaseDecreaseInCashAndCashInHand = value; } }

        /// <summary>
        /// Gets/sets the inventory attribute
        /// </summary>
        public long Inventory { get { return inventory; } set { inventory = value; } }

        /// <summary>
        /// Gets/sets the deferredTaxAsset attribute
        /// </summary>
        public long DeferredTaxAsset { get { return deferredTaxAsset; } set { deferredTaxAsset = value; } }

        // addet from Equity & Debt
        /// <summary>
        /// Gets/sets the issuedShareCapital attribute
        /// </summary>
        public long IssuedShareCapital { get { return issuedShareCapital; } set { issuedShareCapital = value; } }

        /// <summary>
        /// Gets/sets the profitAndLossAccount attribute
        /// </summary>
        public long ProfitAndLossAccount { get { return profitAndLossAccount; } set { profitAndLossAccount = value; } }

        /// <summary>
        /// Gets/sets the taxLiabilities attribute
        /// </summary>
        public long TaxLiabilities { get { return taxLiabilities; } set { taxLiabilities = value; } }

        /// <summary>
        /// Gets/sets the otherLiabilities attribute
        /// </summary>
        public long OtherLiabilities { get { return otherLiabilities; } set { otherLiabilities = value; } }

        /// <summary>
        /// Gets/sets the totalLiabilities attribute
        /// </summary>
        public long TotalLiabilities { get { return totalLiabilities; } set { totalLiabilities = value; } }

        /// <summary>
        /// Gets/sets the shortTermDebts attribute
        /// </summary>
        public long ShortTermDebts { get { return shortTermDebts; } set { shortTermDebts = value; } }

        /// <summary>
        /// Gets/sets the loansOverdraft attribute
        /// </summary>
        public long LoansOverdraft { get { return loansOverdraft; } set { loansOverdraft = value; } }

        /// <summary>
        /// Gets/sets the creditors attribute
        /// </summary>
        public long Creditors { get { return creditors; } set { creditors = value; } }

        /// <summary>
        /// Gets/sets the otherShortTermDebt attribute
        /// </summary>
        public long OtherShortTermDebt { get { return otherShortTermDebt; } set { otherShortTermDebt = value; } }

        /// <summary>
        /// Gets/sets the totalShortTermDebts attribute
        /// </summary>
        public long TotalShortTermDebts { get { return totalShortTermDebts; } set { totalShortTermDebts = value; } }

        /// <summary>
        /// Gets/sets the totalDebtsEquity attribute
        /// </summary>
        public long TotalDebtsEquity { get { return totalDebtsEquity; } set { totalDebtsEquity = value; } }

        /// <summary>
        /// Gets/sets the cashFromOperations attribute
        /// </summary>
        public long CashFromOperations { get { return cashFromOperations; } set { cashFromOperations = value; } }

        // B�tt vi� fr� Income Sheet
        /// <summary>
        /// Gets/sets the administrativeAndOtherExpenses attribute
        /// </summary>
        public long AdministrativeAndOtherExpenses { get { return administrativeAndOtherExpenses; } set { administrativeAndOtherExpenses = value; } }

        /// <summary>
        /// Gets/sets the distributionCost attribute
        /// </summary>
        public long DistributionCost { get { return distributionCost; } set { distributionCost = value; } }

        /// <summary>
        /// Gets/set the total cost attribute
        /// </summary>
        public long TotalCost { get { return totalCost; } set { totalCost = value; } }

        /// <summary>
        /// Gets/sets the financial income attribute
        /// </summary>
        public long FinancialIncome { get { return financialIncome; } set { financialIncome = value; } }

        /// <summary>
        /// Gets/sets the financial cost attribute
        /// </summary>
        public long FinancialCost { get { return financialCost; } set { financialCost = value; } }

        /// <summary>
        /// Gets/sets the incomeFromOperationsAfterTax attribute
        /// </summary>
        public long IncomeFromOperationsAfterTax { get { return incomeFromOperationsAfterTax; } set { incomeFromOperationsAfterTax = value; } }

        /// <summary>
        /// Gets/sets the extraOrdinaryIncomeCost attribute
        /// </summary>
        public long ExtraOrdinaryIncomeCost { get { return extraOrdinaryIncomeCost; } set { extraOrdinaryIncomeCost = value; } }

        /// <summary>
        /// Gets/sets the resultBeforeIncomeFromAssociatedComapnies attribute
        /// </summary>
        public long ResultBeforeIncomeFromAssociatedCompanies { get { return resultBeforeIncomeFromAssociatedCompanies; } set { resultBeforeIncomeFromAssociatedCompanies = value; } }

        /// <summary>
        /// Gets/sets the incomeFromAssociatedCompanies attribute
        /// </summary>
        public long IncomeFromAssociatedCompanies { get { return incomeFromAssociatedCompanies; } set { incomeFromAssociatedCompanies = value; } }

        public long MinorityHoldingsInEquity { get { return minorityHoldingsInEquity; } set { minorityHoldingsInEquity = value; } }
        // fields in datatable but not in input form
        /// <summary>
        /// Gets/sets the reviewed attribute
        /// </summary>
        public bool Reviewed { get; set; }

        /// <summary>
        /// Gets/sets the DenominationID attribute
        /// </summary>
        public int DenominationID { get; set; }

        public String DenominationEN { get; set; }
        public String DenominationNative { get; set; }

        /// <summary>
        /// Gets/sets the commentsID attribute
        /// </summary>
        public int CommentsID { get; set; }

        public String CommentsEN { get; set; }
        public String CommentsNative { get; set; }

        /// <summary>
        /// Gets/sets the dateOfDelivery attribute
        /// </summary>
        public DateTime DateOfDelivery { get; set; }

        /// <summary>
        /// Gets/sets the readyForWebPublishing attribute
        /// </summary>
        public bool ReadyForWebPublishing { get; set; }

        /// <summary>
        /// Gets/sets the registratorID attribute
        /// </summary>
        public int RegistratorID { get; set; }

        /// <summary>
        /// Gets/sets the modified attribute
        /// </summary>
        public DateTime Modified { get; set; }

        /// <summary>
        /// Gets/sets the _AFS_id attribute
        /// </summary>
        public int AFS_id { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersNumber attribute
        /// </summary>
        public int ShareholdersNumber { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersName attribute
        /// </summary>
        public string ShareholdersName { get; set; }

        /// <summary>
        /// Gets/sets the companyOrPrivatePerson attribute
        /// </summary>
        public int CompanyOrPrivatePerson { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersNationalIdentificationNumber attribute
        /// </summary>
        public string ShareholdersNationalIdentificationNumber { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersPercentageHold attribute
        /// </summary>
        public string ShareholdersPercentageHold { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersISO_countryCode attribute
        /// </summary>
        public string ShareholdersISO_countryCode { get; set; }

        /// <summary>
        /// Gets/sets the incomeFromAffiliates attribute
        /// </summary>
        public long IncomeFromAffiliates { get { return incomeFromAffiliates; } set { incomeFromAffiliates = value; } }

        /// <summary>
        /// Gets/sets the incomeFromSubsidiaries attribute
        /// </summary>
        public long IncomeFromSubsidiaries { get { return incomeFromSubsidiaries; } set { incomeFromSubsidiaries = value; } }

        // vi�b�tur tilheyrandi balance sheet
        /// <summary>
        /// Gets/sets the pensionFundsLiabilities attribute
        /// </summary>
        public long PensionFundsLiabilities { get { return pensionFundsLiabilities; } set { pensionFundsLiabilities = value; } }

        /// <summary>
        /// Gets/sets the currentPortionOfLongTermLiabilities attribute
        /// </summary>
        public long CurrentPortionOfLongTermLiabilities { get { return currentPortionOfLongTermLiabilities; } set { currentPortionOfLongTermLiabilities = value; } }

        /// <summary>
        /// Gets/sets the currentAssets attribute
        /// </summary>
        public long CurrentAssets { get { return currentAssets; } set { currentAssets = value; } }

        /// <summary>
        /// Gets/sets the currentAssetsLessCurrentLiabilities attribute
        /// </summary>
        public long CurrentAssetsLessCurrentLiabilities { get { return currentAssetsLessCurrentLiabilities; } set { currentAssetsLessCurrentLiabilities = value; } }

        /// <summary>
        /// Gets/sets the shareholders attribute
        /// </summary>
        public ArrayList Shareholders { get; set; }

        /// <summary>
        /// Gets/sets the deleted attribute
        /// </summary>
        public bool Deleted { get; set; }
    } // END CLASS DEFINITION FinancialStatementBLLC
}