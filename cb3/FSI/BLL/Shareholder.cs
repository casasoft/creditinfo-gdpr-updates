namespace FSI.BLL {
    /// <summary>
    /// Summary description for Shareholder.
    /// </summary>
    public class Shareholder {
        /// <summary>
        /// Gets/sets the _AFS_id attribute
        /// </summary>
        public int AFS_id { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersNumber attribute
        /// </summary>
        public int ShareholdersNumber { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersName attribute
        /// </summary>
        public string ShareholdersName { get; set; }

        /// <summary>
        /// Gets/sets the companyOrPrivatePerson attribute
        /// </summary>
        public int CompanyOrPrivatePerson { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersNationalIdentificationNumber attribute
        /// </summary>
        public string ShareholdersNationalIdentificationNumber { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersPercentageHold attribute
        /// </summary>
        public string ShareholdersPercentageHold { get; set; }

        /// <summary>
        /// Gets/sets the shareholdersISO_countryCode attribute
        /// </summary>
        public string ShareholdersISO_countryCode { get; set; }
    }
}