namespace FSI.BLL {
    /// <summary>
    /// This class is used for storing values for calculating Max credit values in the reports
    /// </summary>
    public class FISMaxCreditValues {
        private string _currency = "";
        private string _fsYear = "";
        public string FsYear { get { return _fsYear; } set { _fsYear = value; } }
        public string Currency { get { return _currency; } set { _currency = value; } }
        public string DenominationDescriptionNative { get; set; }
        public string DenominationDescriptionEN { get; set; }
        public long EquityTotal { get; set; }
        public int DenominationID { get; set; }
    }
}