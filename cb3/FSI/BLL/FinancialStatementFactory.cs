#region

using System;
using System.Collections;
using System.Data;
using CPI.DAL;
using FSI.DAL;
using FSI.Templates.DAL;
using UserAdmin.BLL.CIUsers;
using UserAdmin.DAL.CIUsers;

#endregion

namespace FSI.BLL {
    /// <summary>
    /// Summary description for FinancialStatementFactory.
    /// </summary>
    public class FinancialStatementFactory {
        private readonly CompanyProfileInputDALC theCProfileDALC = new CompanyProfileInputDALC();
        private readonly FinancialStatementDALC theStatementDALC = new FinancialStatementDALC();
        private readonly CreditInfoUserDALC theUserDALC = new CreditInfoUserDALC();

        /// <summary>
        /// Takes instance of FinancialStatementBLLC and transfer to corresponding function in the DAL lair
        /// </summary>
        /// <param name="theStatement">Instance of FinancialStatementBLLC</param>
        /// <returns>true if successful, false otherwise</returns>
        public bool AddStatement(FinancialStatementBLLC theStatement) { return theStatementDALC.AddFinancialStatement(theStatement); }

        /// <summary>
        /// Takes instance of FinancialStatementBLLC and transfer to corresponding function in the DAL lair
        /// </summary>
        /// <param name="theFinStatement">Instance of FinancialStatementBLLC</param>
        /// <returns>true if successful, false otherwise</returns>
        public bool UpdateFinancialStatement(FinancialStatementBLLC theFinStatement) { return theStatementDALC.UpdateFinancialStatement(theFinStatement); }

        /// <summary>
        /// Takes Companies CIID and transfer to corresponding function in the DAL lair
        /// </summary>
        /// <param name="companyCIID">The companies CIID</param>
        /// <param name="year">The statements fiscal year</param>
        /// <returns>true if successful, false otherwise</returns>
        public bool DeleteFinancialStatement(int companyCIID, int year) { return theStatementDALC.DeleteFinancialStatement(companyCIID, year); }

        /// <summary>
        /// Takes Companies CIID and transfer to corresponding function in the DAL lair
        /// </summary>
        /// <param name="companyCIID"></param>
        /// <param name="year">The fiscal year</param>
        /// <param name="accountPeriod"></param>
        /// <returns>true if successful, false otherwise</returns>
        public FinancialStatementBLLC GetFinancialStatement(int companyCIID, int year, int accountPeriod) { return theStatementDALC.GetFinancialStatement(companyCIID, year, accountPeriod); }

        /// <summary>
        /// Fetches all Financial statements for a specific company
        /// </summary>
        /// <param name="nCigID">Unique CreditInfoGroup ID</param>
        /// <returns>List of financial statement</returns>
        public DataSet GetFinancialStatements(int nCigID) { return theStatementDALC.GetFinancialStatements(nCigID); }

        /// <summary>
        /// Takes nationalID and uses CreditInfoUSerDALC class to get matching CIID
        /// </summary>
        /// <param name="nationalID">The requested CIID</param>
        /// <returns></returns>
        public int GetCIIDByNationalID(String nationalID) {
            // �ykist ekki finna falli� � keyrslut�ma, ���ir samt k��ann �n athugasemda ...!?	
            //	return this.theUserDALC.GetCIIDByNationalID(nationalID);
            // �etta virkar hinsvegar ...
            return theCProfileDALC.GetCIIDByNationalID(nationalID);
        }

        /// <summary>
        /// Returns the year for given afs id
        /// </summary>
        /// <param name="afsID">The afs id to find year for</param>
        /// <returns>The year if found, -1 if not found</returns>
        public string GetFinancialStatementYear(int afsID) { return theStatementDALC.GetFinancialStatementYear(afsID); }

        /// <summary>
        /// Returns instance of FISMaxCreditValues
        /// </summary>
        /// <param name="afsID">The statement id</param>
        /// <returns>Instance of FISMaxCreditValues</returns>
        public FISMaxCreditValues GetMaxCreditValues(int afsID) { return theStatementDALC.GetMaxCreditValues(afsID); }

        /// <summary>
        /// Returns the account period for given afs id
        /// </summary>
        /// <param name="afsID">The afs id to find account period for</param>
        /// <returns>The account period if found, -1 if not found</returns>
        public int GetFinancialStatementAccountPeriod(int afsID) { return theStatementDALC.GetFinancialStatementAccountPeriod(afsID); }

        /// <summary>
        /// Gets all available denomination as DataSet
        /// </summary>
        /// <returns></returns>
        public DataSet GetDenominationAsDataSet() { return theStatementDALC.GetDenominationAsDataSet(); }

        /// <summary>
        /// Gets all available sources
        /// </summary>
        /// <returns></returns>
        public DataSet GetSourceAsDataSet() { return theStatementDALC.GetSourceAsDataSet(); }

        /// <summary>
        /// Gets all available commentss
        /// </summary>
        /// <returns></returns>
        public DataSet GetCommentsAsDataSet() { return theStatementDALC.GetCommentsAsDataSet(); }

        public int GetTemplateID(int afs_id) {
            var sdacl = new StatementsDALC();
            return sdacl.GetTemplateID(afs_id);
        }

        public ArrayList GetTemplateValues(int ciid, int templateID, string afs_ids) {
            var sdacl = new StatementsDALC();
            return sdacl.GetTemplateValues(ciid, templateID, afs_ids);
        }

        /// <summary>
        /// Gets all the available currency
        /// </summary>
        /// <returns></returns>
        public DataSet GetCurrencyAsDataSet() { return theCProfileDALC.GetCurrencyAsDataSet(); }

        /// <summary>
        /// Checks if Statement does exist. Search by companyCIID and year
        /// </summary>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <param name="year">The statement year</param>
        /// <param name="accountPeriod"></param>
        /// <returns></returns>
        public bool IsStatementRegisted(int companyCIID, int year, int accountPeriod) { return theStatementDALC.IsStatementRegisted(companyCIID, year, accountPeriod); }

        public Company GetCompany(int CIID) { return theUserDALC.GetCompany(CIID); }
        public DataSet GetCompanyOperationAsDataSet(int CIID, bool current) { return theCProfileDALC.GetCompanyOperationAsDataSet(CIID, current); }
        public long GetEquityTotal(int afs_id) { return theStatementDALC.GetEquityTotal(afs_id); }
        public int GetDenominationID(int afs_id) { return theStatementDALC.GetDenominationID(afs_id); }
    }
}