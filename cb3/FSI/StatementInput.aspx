<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="StatementInput.aspx.cs" AutoEventWireup="false" Inherits="FSI.StatementInput" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>StatementInput</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<script language="JavaScript" src="Validation.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body ms_positioning="GridLayout">
		<form id="f_arsr" method="post" runat="server">
			<!--StatementInputForm-->
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td id="blas" colspan="2" runat="server">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblPageTitle" runat="server">Financial statement input page</asp:label>-
															<asp:label id="lblFinancialStatement" runat="server">Financial Statement</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 350px"><asp:label id="lblCompanyName" runat="server"></asp:label></td>
																	<td style="WIDTH: 250px"><asp:label id="lblCompanyNationalID" runat="server"></asp:label></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCompanyAddress" runat="server"></asp:label></td>
																	<td style="WIDTH: 234px"><asp:label id="lblCompanyFunction" runat="server"></asp:label></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCompanyCity" runat="server"></asp:label></td>
																	<td style="WIDTH: 234px"><asp:label id="lblMostRecentAccount" runat="server">Most recent account: 2002(12 months)</asp:label></td>
																</tr>
																<tr>
																	<td><asp:label id="lblFiscalYear" runat="server">Fiscal year:</asp:label></td>
																	<td style="WIDTH: 234px"><asp:textbox id="tbFiscalYear" runat="server"></asp:textbox></td>
																	<td><asp:textbox id="tbAFS_id" runat="server" width="48px" visible="False"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblYearEnded" runat="server">Year ended:</asp:label></td>
																	<td style="WIDTH: 234px"><asp:textbox id="tbYearEnded" runat="server"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbYearEnded', 250, 250);" type="button" value="...">
																		<asp:customvalidator id="CustomValidator1" runat="server" controltovalidate="tbYearEnded" errormessage="Date input not in correct format">*</asp:customvalidator></td>
																	<td></td>
																</tr>
																<tr>
																	<td style="HEIGHT: 37px"><asp:label id="lblAccountPeriodLength" runat="server">Account period length</asp:label></td>
																	<td style="WIDTH: 234px; HEIGHT: 37px"><asp:dropdownlist id="ddTotalMonths" runat="server">
																			<asp:listitem value="12" selected="True">12</asp:listitem>
																			<asp:listitem value="9">9</asp:listitem>
																			<asp:listitem value="6">6</asp:listitem>
																			<asp:listitem value="3">3</asp:listitem>
																		</asp:dropdownlist></td>
																	<td style="HEIGHT: 37px"></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCurrency" runat="server">Currency:</asp:label></td>
																	<td style="WIDTH: 234px"><asp:dropdownlist id="ddCurrency" runat="server">
																			<asp:listitem value="ISK">ISK</asp:listitem>
																			<asp:listitem value="USD">USD</asp:listitem>
																			<asp:listitem value="MTL" selected="True">MTL</asp:listitem>
																		</asp:dropdownlist></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblConsolidated" runat="server">Consolidated:</asp:label></td>
																	<td style="WIDTH: 234px"><asp:checkbox id="chbConsolidated" runat="server" cssclass="radio"></asp:checkbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblDenomination" runat="server">Denomination:</asp:label></td>
																	<td style="WIDTH: 234px"><asp:dropdownlist id="ddDenomination" runat="server"></asp:dropdownlist></td>
																	<td><input class="FSICheck" id="Text2" tabindex="-1" readonly type="text" size="10" name="Text2">
																	</td>
																</tr>
																<tr>
																	<td style="HEIGHT: 31px"><asp:label id="lblSource" runat="server">Source</asp:label>:</td>
																	<td style="WIDTH: 234px; HEIGHT: 31px"><asp:dropdownlist id="ddSource" runat="server"></asp:dropdownlist></td>
																	<td style="HEIGHT: 31px"></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblOtherFields" runat="server">Other fields</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 350px"><asp:label id="lblQualifiedAudit" runat="server">Qualified audit:</asp:label></td>
																	<td style="WIDTH: 250px"><asp:checkbox id="chbQualifiedAudit" runat="server" cssclass="radio"></asp:checkbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblSmallStatus" runat="server">Small status:</asp:label></td>
																	<td><asp:checkbox id="chbSmallStatus" runat="server" cssclass="radio"></asp:checkbox></td>
																	<td><input class="FSICheck" id="Text1" tabindex="-1" readonly type="text" size="10" name="Text1">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblComments" runat="server">Comments:</asp:label></td>
																	<td><asp:dropdownlist id="ddComments" runat="server"></asp:dropdownlist></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblStaffCount" runat="server">Staff count:</asp:label></td>
																	<td><asp:textbox id="tbStaffCount" runat="server"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblIncomeSheet" runat="server">Income sheet</asp:label></th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 350px"><asp:label id="lblRevenue" runat="server" font-bold="True">Revenue</asp:label></td>
																</tr>
																<tr>
																	<td><asp:label id="lblRevenueFromMainOperations" runat="server">Revenue from main operations</asp:label></td>
																	<td style="WIDTH: 250px"><asp:textbox onkeypress="return handle_int()" id="tbRevenueFromMainOperation" onkeydown="return handle_exit();"
																			onblur="input(this)" onfocus="input_on(this)" runat="server" maxlength="10"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblOtherOperationRevenue" runat="server" cssclass="label">Other operating revenue</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbOtherOperationRevenue" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusB(tbTotalRevenue, tbRevenueFromMainOperation, tbOtherOperationRevenue, tbRevenueError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbRevenueError" tabindex="-1" readonly type="text" maxlength="10"
																			size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblTotalRevenue" runat="server" font-bold="True">Total revenue</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbTotalRevenue" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusB(tbTotalRevenue, tbRevenueFromMainOperation, tbOtherOperationRevenue, tbRevenueError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCosts" runat="server" font-bold="True">Cost</asp:label></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCostOfSales" runat="server">Cost of sales</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbCostOfSales" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusD(tbTotalCost, tbCostOfSales, tbStaffCost, tbDepricationOfFixedAssets, tbAdministrativeAndOtherExpenses, tbAdministrativeAndOtherExpensesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblStaffCost" runat="server" cssclass="label">Staff cost</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbStaffCost" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusD(tbTotalCost, tbCostOfSales, tbStaffCost, tbDepricationOfFixedAssets, tbAdministrativeAndOtherExpenses, tbAdministrativeAndOtherExpensesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblAdministrativeAndOtherExpenses" runat="server">Administrative &amp; other expenses</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbAdministrativeAndOtherExpenses" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusD(tbTotalCost, tbCostOfSales, tbStaffCost, tbDepricationOfFixedAssets, tbAdministrativeAndOtherExpenses, tbAdministrativeAndOtherExpensesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbAdministrativeAndOtherExpensesError" tabindex="-1" readonly
																			type="text" size="10">
																	</td>
																</tr>
																<tr id="trDistributionCost" runat="server">
																	<td><asp:label id="lblDistributionCost" runat="server">Distribution cost</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbDistributionCost" onkeydown="return handle_exit();"
																			onblur="input(this)" onfocus="input_on(this)" runat="server">0</asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblDepricationOfFixedAssets" runat="server">Deprication of fixed assets</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbDepricationOfFixedAssets" onkeydown="return handle_exit();"
																			onblur="if (!er_samandr_rek()) afstemmaAminusBminusCminusD(tbTotalCost, tbCostOfSales, tbStaffCost, tbDepricationOfFixedAssets, tbAdministrativeAndOtherExpenses, tbAdministrativeAndOtherExpensesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblTotalCost" runat="server">Total cost</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbTotalCost" onkeydown="return handle_exit();"
																			onblur="if (!er_samandr_rek()) {afstemmaAminusBminusCminusD(tbTotalCost, tbCostOfSales, tbStaffCost, tbDepricationOfFixedAssets, tbAdministrativeAndOtherExpenses, tbAdministrativeAndOtherExpensesError);afstemmaAminusB(tbTotalRevenue, tbOperationalProfit, tbTotalCost, tbTotalCostError);};input(this);"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbTotalCostError" tabindex="-1" readonly type="text" size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblOperationalProfit" runat="server" font-bold="True">Operational profit</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbOperationalProfit" onkeydown="return handle_exit();"
																			onblur="if (!er_samandr_rek()) afstemmaAminusB(tbTotalRevenue, tbOperationalProfit, tbTotalCost, tbTotalCostError);afstemmaAplusB(tbOperationalProfit,tbNetFinancialItems, tbIncomeFromOperationsBeforeTax, tbIncomeFromOperationsBeforeTaxError);afstemmaAminusB(tbIncomeFromOperationsBeforeTax,tbIncomeFromOperationsAfterTax,tbIncomeTax, tbIncomeTaxError);input(this);"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblFinIncomeAndCosts" runat="server" font-bold="True">Financial income &amp; costs</asp:label></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblFinancialIncome" runat="server">Financial income</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbFinancialIncome" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBplusC(tbNetFinancialItems, tbFinancialIncome, tbFinancialCost, tbOtherFinancialEntries, tbOtherFinancialEntriesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblFinancialCost" runat="server">Financial cost</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbFinancialCost" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBplusC(tbNetFinancialItems, tbFinancialIncome, tbFinancialCost, tbOtherFinancialEntries, tbOtherFinancialEntriesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblOtherFinancialEntries" runat="server">Other financial items</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbOtherFinancialEntries" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBplusC(tbNetFinancialItems, tbFinancialIncome, tbFinancialCost, tbOtherFinancialEntries, tbOtherFinancialEntriesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbOtherFinancialEntriesError" tabindex="-1" readonly type="text"
																			size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblNetFinancialItems" runat="server">Net financial items</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbNetFinancialItems" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBplusC(tbNetFinancialItems, tbFinancialIncome, tbFinancialCost, tbOtherFinancialEntries, tbOtherFinancialEntriesError);afstemmaAplusB(tbOperationalProfit,tbNetFinancialItems, tbIncomeFromOperationsBeforeTax, tbIncomeFromOperationsBeforeTaxError);afstemmaAminusB(tbIncomeFromOperationsBeforeTax,tbIncomeFromOperationsAfterTax,tbIncomeTax, tbIncomeTaxError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblIncomeFromOperationsBeforeTax" runat="server">Income from operations before tax</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbIncomeFromOperationsBeforeTax" onkeydown="return handle_exit();"
																			onblur="afstemmaAplusB(tbOperationalProfit,tbNetFinancialItems, tbIncomeFromOperationsBeforeTax, tbIncomeFromOperationsBeforeTaxError);input(this);"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbIncomeFromOperationsBeforeTaxError" tabindex="-1" readonly
																			type="text" size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblIncomeTax" runat="server">Tax</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbIncomeTax" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusB(tbIncomeFromOperationsBeforeTax,tbIncomeFromOperationsAfterTax,tbIncomeTax, tbIncomeTaxError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbIncomeTaxError" tabindex="-1" readonly type="text" size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblIncomeFromOperationsAfterTax" runat="server">Income from operations after tax</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbIncomeFromOperationsAfterTax" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusB(tbResultBeforeIncomeFromAssociatedCompanies,tbIncomeFromOperationsAfterTax,tbExtraOrdinaryIncomeCost,tbExtraOrdinaryIncomeCostError);afstemmaAminusB(tbIncomeFromOperationsBeforeTax,tbIncomeFromOperationsAfterTax,tbIncomeTax, tbIncomeTaxError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblExtraOrdinaryIncomeCost" runat="server">Extraordinary income cost</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbExtraOrdinaryIncomeCost" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusB(tbResultBeforeIncomeFromAssociatedCompanies,tbIncomeFromOperationsAfterTax,tbExtraOrdinaryIncomeCost,tbExtraOrdinaryIncomeCostError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbExtraOrdinaryIncomeCostError" tabindex="-1" readonly type="text"
																			size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblResultBeforeIncomeFromAssociatedCompanies" runat="server">Result before income from associated companies</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbResultBeforeIncomeFromAssociatedCompanies"
																			onkeydown="return handle_exit();" onblur="afstemmaAminusB(tbResultBeforeIncomeFromAssociatedCompanies,tbIncomeFromOperationsAfterTax,tbExtraOrdinaryIncomeCost,tbExtraOrdinaryIncomeCostError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblIncomeFromAffiliates" runat="server">Income from affiliates</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbIncomeFromAffiliates" onkeydown="return handle_exit();"
																			onblur="afstemmaAplusB(tbIncomeFromAffiliates,tbIncomeFromSubsidiaries,tbIncomeFromAssociatedCompanies, tbIncomeFromAssociatedCompaniesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblIncomeFromSubsidiaries" runat="server">Income from subsidiaries</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbIncomeFromSubsidiaries" onkeydown="return handle_exit();"
																			onblur="afstemmaAplusB(tbIncomeFromAffiliates,tbIncomeFromSubsidiaries,tbIncomeFromAssociatedCompanies, tbIncomeFromAssociatedCompaniesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblIncomeFromAssociatedCompanies" runat="server">Income from associated companies</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbIncomeFromAssociatedCompanies" onkeydown="return handle_exit();"
																			onblur="afstemmaAplusB(tbIncomeFromAffiliates,tbIncomeFromSubsidiaries,tbIncomeFromAssociatedCompanies, tbIncomeFromAssociatedCompaniesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbIncomeFromAssociatedCompaniesError" tabindex="-1" readonly
																			type="text" size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblNetProfitHead" runat="server" font-bold="True">Net-profit</asp:label></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblNetProfit" runat="server">Net profit</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbNetProfit" onkeydown="return handle_exit();"
																			onblur="input(this)" onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblBalanceSheetAssets" runat="server">Balance sheet - ASSETS</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 350px"><asp:label id="lblFixedAssets" runat="server" font-bold="True">Fixed assets</asp:label></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblIntangibleAssets" runat="server">Intangible assets</asp:label></td>
																	<td style="WIDTH: 250px"><asp:textbox onkeypress="return handle_int()" id="tbIntangibleAssets" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusC(tbTotalFixedAssets,tbIntangibleAssets,tbTangibleAssets,tbFinancialAssets,tbFinancialAssetsError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblTangibleAssets" runat="server">Tangible assets</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbTangibleAssets" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusC(tbTotalFixedAssets,tbIntangibleAssets,tbTangibleAssets,tbFinancialAssets,tbFinancialAssetsError);this.className='input_text'"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblFinancialAssets" runat="server">Financial assets</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbFinancialAssets" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusC(tbTotalFixedAssets,tbIntangibleAssets,tbTangibleAssets,tbFinancialAssets,tbFinancialAssetsError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbFinancialAssetsError" tabindex="-1" readonly type="text"
																			size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblTotalFixedAssets" runat="server" font-bold="True">Total fixed assets</asp:label>
																	</td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbTotalFixedAssets" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusC(tbTotalFixedAssets,tbIntangibleAssets,tbTangibleAssets,tbFinancialAssets,tbFinancialAssetsError);afstemmaAplusB(tbTotalFixedAssets,tbTotalCurrentAssets,tbTotalAssets,tbTotalAssetsError);input(this);"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCurrentAssets" runat="server" font-bold="True">Current assets</asp:label></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblInventory" runat="server">Inventory</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbInventory" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusEminusF(tbTotalCurrentAssets,tbInventory,tbDebtors,tbClaimsTowardsAssociatedCompanies,tbShortTermInvestments,tbCashInBankHand,tbOtherCurrentAssets,tbOtherCurrentAssetsError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblDebtors" runat="server">Receivables &amp; Debtors</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbDebtors" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusEminusF(tbTotalCurrentAssets,tbInventory,tbDebtors,tbClaimsTowardsAssociatedCompanies,tbShortTermInvestments,tbCashInBankHand,tbOtherCurrentAssets,tbOtherCurrentAssetsError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblClaimsTowardsAssociatedCompanies" runat="server">Claims towards associated companies</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbClaimsTowardsAssociatedCompanies" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusEminusF(tbTotalCurrentAssets,tbInventory,tbDebtors,tbClaimsTowardsAssociatedCompanies,tbShortTermInvestments,tbCashInBankHand,tbOtherCurrentAssets,tbOtherCurrentAssetsError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblShortTermInvestments" runat="server">Short-term investments</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbShortTermInvestments" onkeydown="return handle_exit();"
																			onblur="input(this)" onfocus="afstemmaAminusBminusCminusDminusEminusF(tbTotalCurrentAssets,tbInventory,tbDebtors,tbClaimsTowardsAssociatedCompanies,tbShortTermInvestments,tbCashInBankHand,tbOtherCurrentAssets,tbOtherCurrentAssetsError);input_on(this)"
																			runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCashInBankHand" runat="server">Cash</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbCashInBankHand" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusEminusF(tbTotalCurrentAssets,tbInventory,tbDebtors,tbClaimsTowardsAssociatedCompanies,tbShortTermInvestments,tbCashInBankHand,tbOtherCurrentAssets,tbOtherCurrentAssetsError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<!--�G ER STADDUR H�R � VALIDATION D�MI !!! �A� �ARF A� ME�H�NDLA �ENNAN G�JA �V� A� HANN ER EXTRA M.V. �SLAND-->
																<tr id="trDeferredTaxAsset" runat="server">
																	<td><asp:label id="lblDeferredTaxAsset" runat="server">Deferred tax asset</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbDeferredTaxAsset" onkeydown="return handle_exit();"
																			onblur="input(this)" onfocus="input_on(this)" runat="server">0</asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<!-- �tti a� vera � lagi -->
																	<td><asp:label id="lblOtherCurrentAssets" runat="server">Other current assets</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbOtherCurrentAssets" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusEminusF(tbTotalCurrentAssets,tbInventory,tbDebtors,tbClaimsTowardsAssociatedCompanies,tbShortTermInvestments,tbCashInBankHand,tbOtherCurrentAssets,tbOtherCurrentAssetsError);input(this)"
																			onfocus="input_on(this)" runat="server" cssclass="input_text"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbOtherCurrentAssetsError" tabindex="-1" readonly type="text"
																			size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblTotalCurrentAssets" runat="server" font-bold="True">Total current assets</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbTotalCurrentAssets" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusEminusF(tbTotalCurrentAssets,tbInventory,tbDebtors,tbClaimsTowardsAssociatedCompanies,tbShortTermInvestments,tbCashInBankHand,tbOtherCurrentAssets,tbOtherCurrentAssetsError);afstemmaAplusB(tbTotalFixedAssets,tbTotalCurrentAssets,tbTotalAssets,tbTotalAssetsError);afstemmaAminusB(tbTotalCurrentAssets, tbTotalShortTermDebt, tbCurrentAssetsLessCurrentLiabilities, tbCurrentAssetsLessCurrentLiabilitiesError);input(this);"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																
																<tr>
																	<td><asp:label id="lblDereferedTaxations" runat="server" font-bold="True">Deferred taxation</asp:label></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<!-- B�i� -->
																	<td><asp:label id="lblTotalAssets" runat="server">Total assets1</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbTotalAssets" onkeydown="return handle_exit();"
																			onblur="afstemmaAplusB(tbTotalFixedAssets,tbTotalCurrentAssets,tbTotalAssets,tbTotalAssetsError);input(this);"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbTotalAssetsError" tabindex="-1" readonly type="text" size="10"></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblBalanceSheetEquityAndDebt" runat="server">Balance sheet - Equity and Debt</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 350px"><asp:label id="lblEquity" runat="server" font-bold="True">Equity</asp:label></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblIssuedShareCapital" runat="server">Issued share capital</asp:label></td>
																	<td style="WIDTH: 250px"><asp:textbox onkeypress="return handle_int()" id="tbIssuedShareCapital" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusB(tbTotalEquity, tbIssuedShareCapital, tbOtherEquity, tbOtherEquityError);input(this)" onfocus="input_on(this)"
																			runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr id="trProfitAndLossAccount" runat="server">
																	<td><asp:label id="lblProfitAndLossAccount" runat="server">Profit and loss account</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbProfitAndLossAccount" onkeydown="return handle_exit();"
																			runat="server">0</asp:textbox></td>
																	<td></td>
																</tr>																
																<tr>
																	<td><asp:label id="lblOtherEquity" runat="server">Other equity</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbOtherEquity" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusB(tbTotalEquity, tbIssuedShareCapital, tbOtherEquity, tbOtherEquityError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbOtherEquityError" tabindex="-1" readonly type="text" size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblTotalEquity" runat="server" font-bold="True">Total equity</asp:label></td>
																	<td>
																		<asp:textbox onkeypress="return handle_int()" id="tbTotalEquity" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusB(tbTotalEquity, tbIssuedShareCapital, tbOtherEquity, tbOtherEquityError);afstemmaAplusBplusCplusDplusE(tbTotalEquity, tbTotalLiabilities, tbLongTermDebts, tbTotalShortTermDebt, tbMinorityHoldingsInEquity, tbTotalDebtsAndEquity, tbTotalDebtsAndEquityError);input(this);"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblMinorityHoldingsInEquity" runat="server">Minority holdings in equity</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbMinorityHoldingsInEquity" onkeydown="return handle_exit();"
																			onblur="afstemmaAplusBplusCplusDplusE(tbTotalEquity, tbTotalLiabilities, tbLongTermDebts, tbTotalShortTermDebt, tbMinorityHoldingsInEquity, tbTotalDebtsAndEquity, tbTotalDebtsAndEquityError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblLiabilities" runat="server" font-bold="True">Liabilities</asp:label>
																	</td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblPensionFundsLiabilities" runat="server">Pension funds liabilities</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbPensionFundsLiabilities" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusC(tbTotalLiabilities, tbPensionFundsLiabilities, tbTaxLiabilities, tbOtherLiabilities, tbOtherLiabilitiesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblTaxLiabilities" runat="server">Tax liabilities</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbTaxLiabilities" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusC(tbTotalLiabilities, tbPensionFundsLiabilities, tbTaxLiabilities, tbOtherLiabilities, tbOtherLiabilitiesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblOtherLiabilities" runat="server">Other liabilities</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbOtherLiabilities" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusC(tbTotalLiabilities, tbPensionFundsLiabilities, tbTaxLiabilities, tbOtherLiabilities, tbOtherLiabilitiesError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbOtherLiabilitiesError" tabindex="-1" readonly type="text"
																			size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblTotalLiabilities" runat="server" font-bold="True">Total liabilities</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbTotalLiabilities" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusC(tbTotalLiabilities, tbPensionFundsLiabilities, tbTaxLiabilities, tbOtherLiabilities, tbOtherLiabilitiesError);afstemmaAplusBplusCplusDplusE(tbTotalEquity, tbTotalLiabilities, tbLongTermDebts, tbTotalShortTermDebt, tbMinorityHoldingsInEquity, tbTotalDebtsAndEquity, tbTotalDebtsAndEquityError);input(this);"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>																
																<tr>
																	<td><asp:label id="lblLongTermDebts" runat="server">Long term debts</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbLongTermDebts" onkeydown="return handle_exit();"
																			onblur="afstemmaAplusBplusCplusDplusE(tbTotalEquity, tbTotalLiabilities, tbLongTermDebts, tbTotalShortTermDebt, tbMinorityHoldingsInEquity, tbTotalDebtsAndEquity, tbTotalDebtsAndEquityError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblShortTermDebts" runat="server" font-bold="True">Short term debts</asp:label></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td><br></td>
																	<td></td>
																	<td></td>																	
																</tr>
																<tr>
																	<td><asp:label id="lblLoansOverdraft" runat="server">Loans/Overdraft</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbLoansOverdraft" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusE(tbTotalShortTermDebt,tbCreditors,tbDebtToAssociatedCompanies,tbNextYearPayment,tbLoansOverdraft,tbOtherShortTermDebt,tbOtherShortTermDebtError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCreditors" runat="server">Creditors</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbCreditors" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusE(tbTotalShortTermDebt,tbCreditors,tbDebtToAssociatedCompanies,tbNextYearPayment,tbLoansOverdraft,tbOtherShortTermDebt,tbOtherShortTermDebtError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblDebtToAssociatedCompanies" runat="server">Debt towards associated companies</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbDebtToAssociatedCompanies" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusE(tbTotalShortTermDebt,tbCreditors,tbDebtToAssociatedCompanies,tbNextYearPayment,tbLoansOverdraft,tbOtherShortTermDebt,tbOtherShortTermDebtError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblNextYearPayment" runat="server">Next year payment</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbNextYearPayment" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusE(tbTotalShortTermDebt,tbCreditors,tbDebtToAssociatedCompanies,tbNextYearPayment,tbLoansOverdraft,tbOtherShortTermDebt,tbOtherShortTermDebtError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblOtherShortTermDebt" runat="server">Other short term debt</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbOtherShortTermDebt" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusE(tbTotalShortTermDebt,tbCreditors,tbDebtToAssociatedCompanies,tbNextYearPayment,tbLoansOverdraft,tbOtherShortTermDebt,tbOtherShortTermDebtError);input(this)"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbOtherShortTermDebtError" tabindex="-1" readonly type="text"
																			size="10">
																	</td>
																</tr>
																<tr>
																	<td><asp:label id="lblTotalShortTermDebt" runat="server">Total short term debt</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbTotalShortTermDebt" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusBminusCminusDminusE(tbTotalShortTermDebt,tbCreditors,tbDebtToAssociatedCompanies,tbNextYearPayment,tbLoansOverdraft,tbOtherShortTermDebt,tbOtherShortTermDebtError);afstemmaAminusB(tbTotalCurrentAssets, tbTotalShortTermDebt, tbCurrentAssetsLessCurrentLiabilities, tbCurrentAssetsLessCurrentLiabilitiesError);afstemmaAplusBplusCplusDplusE(tbTotalEquity,tbTotalLiabilities,tbLongTermDebts,tbTotalShortTermDebt,tbMinorityHoldingsInEquity,tbTotalDebtsAndEquity,tbTotalDebtsAndEquityError);input(this);"
																			onfocus="input_on(this)" runat="server"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCurrentAssetsLessCurrentLiabilities" runat="server">Current assets less current liabilities</asp:label></td>
																	<td><asp:textbox onkeypress="return handle_int()" id="tbCurrentAssetsLessCurrentLiabilities" onkeydown="return handle_exit();"
																			onblur="afstemmaAminusB(tbTotalCurrentAssets, tbTotalShortTermDebt, tbCurrentAssetsLessCurrentLiabilities, tbCurrentAssetsLessCurrentLiabilitiesError);input(this)"
																			onfocus="input_on(this)" runat="server" cssclass="input_text"></asp:textbox></td>
																	<td><input class="FSICheck" id="tbCurrentAssetsLessCurrentLiabilitiesError" tabindex="-1" readonly
																			type="text" size="10">
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblTotalDebtsHead" runat="server" font-bold="True">Total debts</asp:label>
																		<br>
																	<td></td>
																	<td></td>
														</td>
													<tr>
														<td><asp:label id="lblTotalDebtsAndEquity" runat="server">Total debts &amp; equity</asp:label></td>
														<td><asp:textbox onkeypress="return handle_int()" id="tbTotalDebtsAndEquity" onkeydown="return handle_exit();"
																onblur="afstemmaAplusBplusCplusDplusE(tbTotalEquity, tbTotalLiabilities, tbLongTermDebts, tbTotalShortTermDebt, tbMinorityHoldingsInEquity, tbTotalDebtsAndEquity, tbTotalDebtsAndEquityError);input(this)"
																onfocus="input_on(this)" runat="server"></asp:textbox></td>
														<td><input class="FSICheck" id="tbTotalDebtsAndEquityError" tabindex="-1" readonly type="text"
																size="10">
														</td>
													</tr>
													<tr>
														<td height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td>
									<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
									<table class="grid_table" cellspacing="0" cellpadding="0">
										<tr>
											<th>
												<asp:label id="lblCashFlow" runat="server">Cash Flow</asp:label></th></tr>
										<tr>
											<td>
												<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
													<tr>
														<td style="WIDTH: 350px"><asp:label id="lblOperatingProfitBeforeWorkingCapitalChanges" runat="server">Operating profit before working capital changes</asp:label></td>
														<td style="WIDTH: 250px"><asp:textbox onkeypress="return handle_int()" id="tbOperatingProfitBeforeWorkingCapitalChanges"
																onkeydown="return handle_exit();" onblur="input(this)" onfocus="input_on(this)" runat="server"></asp:textbox></td>
														<td><input class="FSICheck" id="Text3" tabindex="-1" readonly type="text" size="10" name="Text3">
														</td>
													</tr>
													<tr>
														<td style="WIDTH: 306px"><asp:label id="lblCashFromOperations" runat="server">Cash from operations</asp:label></td>
														<td><asp:textbox onkeypress="return handle_int()" id="tbCashFromOperations" onkeydown="return handle_exit();"
																onblur="input(this)" onfocus="input_on(this)" runat="server"></asp:textbox></td>
														<td></td>
													</tr>
													<tr>
														<td style="WIDTH: 306px"><asp:label id="lblCashprovidedByOperatingActivities" runat="server">Net cash from operating activities</asp:label></td>
														<td><asp:textbox onkeypress="return handle_int()" id="tbCashprovidedByOperatingActivities" onkeydown="return handle_exit();"
																onblur="input(this)" onfocus="input_on(this)" runat="server"></asp:textbox></td>
														<td></td>
													</tr>
													<tr>
														<td style="WIDTH: 306px"><asp:label id="lblInvestingActivities" runat="server">Cash flows from investing activities</asp:label></td>
														<td><asp:textbox onkeypress="return handle_int()" id="tbInvestingActivities" onkeydown="return handle_exit();"
																onblur="input(this)" onfocus="input_on(this)" runat="server"></asp:textbox></td>
														<td></td>
													</tr>
													<tr>
														<td style="WIDTH: 306px"><asp:label id="lblFinancialActivities" runat="server">Cash flows from financing activities</asp:label></td>
														<td><asp:textbox onkeypress="return handle_int()" id="tbFinancialActivities" onkeydown="return handle_exit();"
																onblur="input(this)" onfocus="input_on(this)" runat="server"></asp:textbox></td>
														<td></td>
													</tr>
													<tr>
														<td style="WIDTH: 306px"><asp:label id="lblInDecreaseInCashAndInHand" runat="server">Increase(+)/decrease(-) in cash</asp:label></td>
														<td><asp:textbox onkeypress="return handle_int()" id="tbInDecreaseInCashAndInHand" onkeydown="return handle_exit();"
																onblur="input(this)" onfocus="input_on(this)" runat="server"></asp:textbox></td>
														<td></td>
													</tr>
													<tr>
														<td style="WIDTH: 306px" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td>
									<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
									<table class="empty_table" cellspacing="0">
										<tr>
											<td style="WIDTH: 375px"><asp:label id="lblReadyForWebPublish" runat="server">Ready for web publish</asp:label>:</td>
											<td><asp:checkbox id="chbReadyForWebPublish" runat="server" cssclass="radio"></asp:checkbox>&nbsp;<input class="FSICheck" id="Text4" tabindex="-1" readonly type="text" size="10" name="Text3">
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td align="left">
												<asp:label id="lblMsg" runat="server" visible="False" cssclass="confirm_text">The statement has been registered</asp:label><asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary></td>
											<td align="right"><asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel"></asp:button>&nbsp;<asp:button id="btnSubmit" runat="server" cssclass="confirm_button" text="Submit"></asp:button></td>
										</tr>
									</table>
									<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
							</tr>
						</table>
						<!-- Main Body Ends --></td>
				</tr>
				<tr>
					<td height="20"></td>
				</tr>
				<tr>
					<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
				</tr>
			</table>
			</td>
			<td width="2"></td>
			</tr>
			<tr>
				<td align="center" colspan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
			</tr>
			</table></form>
	</body>
</HTML>
