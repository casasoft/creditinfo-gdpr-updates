function byrja()
{
 d=new Date();
 
  if (document.f_arsr.a_a_du.value=="")
    document.f_arsr.a_a_du.value="31.12.2003"; // + (d.getFullYear()-1);
 
}

function handle_int()
{   ret=true;
    if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode != 45)
        ret=false;
    event.returnValue=ret;
}

function handle_real()
{   ret=true;
    if ((event.keyCode < 48 || event.keyCode > 57)&& event.keyCode != 44)
        ret=false;
    event.returnValue=ret;
}


function handle_exit()
{   if (event.keyCode==13 || event.keyCode==40)
      event.keyCode=9;
}

function hondla_tolur(next)
{   if(event.keyCode == 13 && document.f_arsr.elements[next] != "") 
      document.f_arsr.elements[next].select();      
    if (((event.keyCode < 48)||(event.keyCode > 57))&&(event.keyCode!=45)) 
      event.returnValue = false;
}

function hondla_dags(next)
{   if (event.keyCode == 13) 
      document.f_arsr.elements[next].select();
    if (((event.keyCode < 48)||(event.keyCode > 57))&&(event.keyCode!=46)) 
      event.returnValue = false;
}

function hondla_kt()
{   if (((event.keyCode < 48)||(event.keyCode > 57))&&(event.keyCode != 13)) 
      event.returnValue = false;
}


function set_fyrraar(thetta)
{
d=new Date();
thetta.value=d.getFullYear()-1;
}

function afstemmaAr(ar,err){
 var valu="";
 if (ar.value=="")
    valu="* Vantar gildi!"; 
 else if (ar.value <="1995" || ar.value >="2010")
    valu="* Utan marka"; 
 err.value=valu;
 maSkra();
 }

function afstemmaAplusB(a, b, samt, err){
 var valu="";
 var valu="";
 if (a.value==""||b.value==""||samt.value=="")
    valu="*";
 else 
   if (parseInt(samt.value) != parseInt(a.value) + parseInt(b.value))
     valu=parseInt(a.value) + parseInt(b.value);
 err.value=valu;
 maSkra();
 }

function afstemmaAplusBplusCplusDplusE(a, b, c, d, e, samt, err){
 var valu="";
 if (a.value==""||b.value==""||c.value==""||d.value==""||e.value==""||samt.value=="")
    { valu="*";}
 else 
   if (parseInt(samt.value) != parseInt(a.value) + parseInt(b.value) + parseInt(c.value) + parseInt(d.value) + parseInt(e.value))
     valu=parseInt(a.value) + parseInt(b.value) + parseInt(c.value) + parseInt(d.value) + parseInt(e.value);
 err.value=valu;
 maSkra();
 }


function afstemmaAminusB(a, b, c, err){
 var valu="";
 if (a.value==""||b.value==""||c.value=="")
    valu="*"; 
 else 
   if (parseInt(c.value) != parseInt(a.value) - parseInt(b.value))
     valu=parseInt(a.value) - parseInt(b.value);
  err.value=valu;
 maSkra();
 }

function afstemmaAminusBminusC(a, b, c, samt, err){
 var valu="";
 if (a.value==""||b.value==""||c.value==""||samt.value=="")
    { valu="*";}
 else 
   if (parseInt(samt.value) != parseInt(a.value) - parseInt(b.value) - parseInt(c.value))
     valu=parseInt(a.value) - parseInt(b.value) - parseInt(c.value);
 err.value=valu;
  maSkra();
 }

function afstemmaAminusBminusCminusD(a, b, c, d, samt, err){
 var valu="";
 if (a.value==""||b.value==""||c.value==""||d.value==""||samt.value=="")
    { valu="*";}
 else 
   if (parseInt(samt.value) != parseInt(a.value) - parseInt(b.value) - parseInt(c.value) - parseInt(d.value))
     valu=parseInt(a.value) - parseInt(b.value) - parseInt(c.value) - parseInt(d.value);
 err.value=valu;
  maSkra();
 }

function afstemmaAminusBminusCminusDminusE(a, b, c, d, e, samt, err){
 var valu="";
 if (a.value==""||b.value==""||c.value==""||d.value==""||e.value==""||samt.value=="")
    valu="*"; 
 else 
   if (parseInt(samt.value) != parseInt(a.value) - parseInt(b.value) - parseInt(c.value) - parseInt(d.value) - parseInt(e.value))
     valu=parseInt(a.value) - parseInt(b.value) - parseInt(c.value) - parseInt(d.value) - parseInt(e.value);
 err.value=valu;
 maSkra();
 }

function afstemmaAminusBminusCminusDminusEminusF(a, b, c, d, e, f, samt, err){
 var valu="";
 if (a.value==""||b.value==""||c.value==""||d.value==""||e.value==""||f.value==""||samt.value=="")
    valu="*"; 
 else 
   if (parseInt(samt.value) != parseInt(a.value) - parseInt(b.value) - parseInt(c.value) - parseInt(d.value) - parseInt(e.value) - parseInt(f.value))
     valu=parseInt(a.value) - parseInt(b.value) - parseInt(c.value) - parseInt(d.value) - parseInt(e.value) - parseInt(f.value);
 err.value=valu;
 maSkra();
 }


function afstemmaAminusBplusC(a, b, c, samt, err){
 var valu="";
 if (a.value==""||b.value==""||c.value==""||samt.value=="")
    valu="*"; 
 else 
   if (parseInt(samt.value) != parseInt(a.value) - parseInt(b.value) + parseInt(c.value))
     valu=parseInt(a.value) - parseInt(b.value) + parseInt(c.value);
 err.value=valu;
 maSkra();
 }


function maSkra()
{
ma=false;
if (
    (f_arsr.tbOtherOperationRevenue.value != "" && f_arsr.tbRevenueError.value =="" || er_samandr_rek()) &&
    (f_arsr.tbAdministrativeAndOtherExpenses.value != "" && f_arsr.tbAdministrativeAndOtherExpensesError.value ==""|| er_samandr_rek()) &&
    (f_arsr.tbTotalCost.value != "" && f_arsr.tbTotalCostError.value =="" || er_samandr_rek()) &&
    f_arsr.tbOtherFinancialEntries.value != "" && f_arsr.tbOtherFinancialEntriesError.value =="" &&
    f_arsr.tbIncomeFromOperationsBeforeTax.value != "" && f_arsr.tbIncomeFromOperationsBeforeTaxError.value =="" &&
    f_arsr.tbIncomeTax.value != "" && f_arsr.tbIncomeTaxError.value =="" &&
    f_arsr.tbExtraOrdinaryIncomeCost.value != "" && f_arsr.tbExtraOrdinaryIncomeCostError.value =="" &&
    f_arsr.tbIncomeFromAssociatedCompanies.value != "" && f_arsr.tbIncomeFromAssociatedCompaniesError.value =="" &&
    f_arsr.tbFinancialAssets.value != "" && f_arsr.tbFinancialAssetsError.value =="" &&
    f_arsr.tbOtherCurrentAssets.value != "" && f_arsr.tbOtherCurrentAssetsError.value =="" &&
    f_arsr.tbTotalAssets.value != "" && f_arsr.tbTotalAssetsError.value =="" &&
    f_arsr.tbOtherShortTermDebt.value != "" && f_arsr.tbOtherShortTermDebtError.value =="" &&
    f_arsr.tbCurrentAssetsLessCurrentLiabilities.value != "" && f_arsr.tbCurrentAssetsLessCurrentLiabilitiesError.value =="" &&
    f_arsr.tbOtherLiabilities.value != "" && f_arsr.tbOtherLiabilitiesError.value =="" &&
    f_arsr.tbOtherEquity.value != "" && f_arsr.tbOtherEquityError.value =="" &&
    f_arsr.tbTotalDebtsAndEquity.value != "" && f_arsr.tbTotalDebtsAndEquityError.value ==""
   )
    ma=true;
 if (ma)
   f_arsr.btnSubmit.className="SubmitAllowed";
 else
   f_arsr.btnSubmit.className="SubmitNotAllowed";

 return ma;
}

function senda()
 {
   if (!maSkra()){
     alert("Data on form is not valid!")
     return false;
    }
   else
	return true;
     //f_arsr.submit();
 }

function input_on(eg)
 { eg.className="input_on";
 }

function input(eg)
 { eg.className="input_text";
 }

function er_samandr_rek()
 {
   if (document.f_arsr.chbConsolidated.checked)
     return true;
   else
     return false;
 }

function set_samandr_rek(eg)
 {

   if (eg.checked)
    {  f_arsr.a_r_taa.className='input_dis';
       f_arsr.a_r_ar.className='input_dis';
       f_arsr.a_r_ra.className='input_dis';
       f_arsr.a_r_ksv.className='input_dis';
       f_arsr.a_r_lolg.className='input_dis';
       f_arsr.a_r_soar.className='input_dis';
       document.all.t_r_taa.style.color='#909090';
       document.all.t_r_ar.style.color='#909090';
       document.all.t_r_ra.style.color='#909090';
       document.all.t_r_ksv.style.color='#909090';
       document.all.t_r_lolg.style.color='#909090';
       document.all.t_r_soar.style.color='#909090';
       f_arsr.a_r_taa.value=0;
       f_arsr.a_r_ar.value=0;
       f_arsr.a_r_ra.value=0;
       f_arsr.a_r_ksv.value=0;
       f_arsr.a_r_lolg.value=0;
       f_arsr.a_r_soar.value=0;
       f_arsr.e_r_ar.value="";
       f_arsr.e_r_soar.value="";
       f_arsr.e_r_sr.value="";
    }
   else
    {  f_arsr.a_r_taa.className='input_en';
       f_arsr.a_r_ar.className='input_en';
       f_arsr.a_r_ra.className='input_en';
       f_arsr.a_r_ksv.className='input_en';
       f_arsr.a_r_lolg.className='input_en';
       f_arsr.a_r_soar.className='input_en';
       document.all.t_r_taa.style.color='black';
       document.all.t_r_ar.style.color='black';
       document.all.t_r_ra.style.color='black';
       document.all.t_r_ksv.style.color='black';
       document.all.t_r_lolg.style.color='black';
       document.all.t_r_soar.style.color='black';
    }
 }