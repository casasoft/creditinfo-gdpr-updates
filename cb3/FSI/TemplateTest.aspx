<%@ Register TagPrefix="uc4" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc2" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc3" TagName="menu" Src="../new_user_controls/menu.ascx" %>
<%@ Page language="c#" Codebehind="TemplateTest.aspx.cs" AutoEventWireup="false" Inherits="FSI.TemplateTest" %>
<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" >
<HTML>
  <HEAD>
    <title>TemplateTest</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" href="../css/style.css" type="text/css" >
  </HEAD>
  <body MS_POSITIONING="FlowLayout">
	
    <form id="Form1" method="post" runat="server">
	<uc1:head id=ctlHeader runat="server"></uc1:head>
<!-- MI�SV��I -->
	<div class="mainSection" style="POSITION: relative; TOP: 6px; TEXT-ALIGN: center">
		<table class="outerTable" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td class="mainShadeLeft"><img src="../img/spacer.gif" width="6" alt="" ></td>
				<td>
		<table class="innerTable" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="positionBar">	
				
				<uc4:sitePositionBar id=ctlSitePositionBar runat="server"></uc4:sitePositionBar>
				<!-- FYRIRS�GN � SV��I -->	
					<div class="pageNameBar">
						<div class="pageNameMark"><img src="../img/pagename_mark.gif" alt="" ></div>
						<span class="pageNameText">Information entry</span>
					</div>
				<!-- FYRIRS�GN � SV��I ENDAR -->
				<uc3:menu id="ctrlMenu" runat="server"></uc3:menu>
				
				<!-- S��A HEFST -->	
						<div class="content">
							<span class="h1">Basic Info</span>
							<div class="mainContent"> <!-- t�k hinga� -->
							
								<div class="formElement" style="LEFT: 0px; TOP: 0px">
									<span class="fieldHeader">Legal from</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<select class="input" style="WIDTH:420px">
											<option selected>Company type</option>
										</select>
									</div>
								</div>
								
								<div class="formElement" style="LEFT: 0px; TOP: 44px">
									<span class="fieldHeader">Name</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>								

								<div class="formElement" style="LEFT: 225px; TOP: 44px">
									<span class="fieldHeader">Unique id</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>	

								<div class="formElement" style="LEFT: 0px; TOP: 88px">
									<span class="fieldHeader">Trade name</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>								

								<div class="formElement" style="LEFT: 225px; TOP: 88px">
									<span class="fieldHeader">Former name</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>	
								
								<div class="formElement" style="LEFT: 450px; TOP: 88px">
									<span class="fieldHeader">E-mail</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>									
								
								<div class="formElement" style="LEFT: 0px; TOP: 132px">
									<span class="fieldHeader">Telephone</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>								

								<div class="formElement" style="LEFT: 225px; TOP: 132px">
									<span class="fieldHeader">Mobile</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>	
								
								<div class="formElement" style="LEFT: 450px; TOP: 132px">
									<span class="fieldHeader">Fax</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>									
								
								<div class="formElement" style="LEFT: 0px; TOP: 176px">
									<span class="fieldHeader">Address</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>								

								<div class="formElement" style="LEFT: 225px; TOP: 176px">
									<span class="fieldHeader">Area code</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>	
								
								<div class="formElement" style="LEFT: 450px; TOP: 176px">
									<span class="fieldHeader">City</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<input type="text" class="input" >
									</div>
								</div>	
								
								<div class="formElement" style="LEFT: 0px; TOP: 220px">
									<span class="fieldHeader">Homepage</span>
									<div class="formField" style="LEFT: 0px; TOP: 15px">
										<textarea class="input" style="WIDTH: 420px; HEIGHT: 69px" cols="5" rows="6"></textarea>
									</div>
								</div>	
																																								
								<div class="formElement" style="LEFT: 0px; TOP: 327px">
									<div class="formField" style="LEFT: 0px; TOP: 0px">
										<input type="button" class="button" value="Register" >
									</div>
								</div>									
							</div>
						</div>
					<!-- S��A ENDAR -->
            <DIV></DIV></td></tr></table></td>
			<td class="mainShadeRight"><img src="img/spacer.gif" width="6" alt="" ></td></tr></table>										
					</div>
	
	<!-- MI�SV��I ENDAR -->
	<uc2:footer id="ctlFooter" runat="server"></uc2:footer>
     </form>
	
  </body>
</HTML>
