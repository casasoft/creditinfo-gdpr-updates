#region

using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FSI.BLL;
using FSI.Localization;

#endregion

namespace FSI {
    /// <summary>
    /// Summary description for ConfirmDelete.
    /// </summary>
    public class ConfirmDelete : Page {
        private static readonly FinancialStatementFactory theStatementFact = new FinancialStatementFactory();
        // Localization
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected HtmlTableCell blas;
        protected Label blCompanyRegno;
        protected Button btnCancel;
        protected Button btnDelete;
        protected Label lblDeleteStatement;
        protected Label lblFiscalYear;
        protected Label lblMessage;
        protected Label lblPageTitle;
        protected TextBox tbCompanyRegNo;
        protected TextBox tbFiscalYear;
        public int Year { get; private set; }
        public string NationalID { get; private set; }

        private void Page_Load(object sender, EventArgs e) {
            // check the current culture
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (Request.QueryString["ref"] != null) {
                NationalID = Request.Params["tbCompanyRegNo"];
                tbCompanyRegNo.Text = Request.Params["tbCompanyRegNo"];
                Year = Convert.ToInt32(Request.Params["tbFiscalYear"]);
                tbFiscalYear.Text = Request.Params["tbFiscalYear"];
            }
            if (!IsPostBack) {
                LocalizeText();
            }
        }

        private void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtDeleteFinancialStatement", ci);
            lblDeleteStatement.Text = rm.GetString("txtDeleteThisFinancialStatement", ci);
            blCompanyRegno.Text = rm.GetString("txtCompanyRegNo", ci);
            lblFiscalYear.Text = rm.GetString("txtFiscalYear", ci);
            btnCancel.Text = rm.GetString("txtNo", ci);
            btnDelete.Text = rm.GetString("txtYes", ci);
        }

        private void btnDelete_Click(object sender, EventArgs e) {
            //		int ciid = theStatementFact.GetCIIDByNationalID(tbCompanyRegNo.Text);
            //		int year = Convert.ToInt32(tbFiscalYear.Text);

            lblMessage.Text = theStatementFact.DeleteFinancialStatement(
                                  theStatementFact.GetCIIDByNationalID(tbCompanyRegNo.Text), Convert.ToInt32(tbFiscalYear.Text)) ? rm.GetString("txtStatementDeleted") : rm.GetString("txtStatementDeletedFailed");
            lblMessage.Visible = true;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}