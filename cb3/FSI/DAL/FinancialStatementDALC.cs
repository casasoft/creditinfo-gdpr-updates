#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using DataProtection;
using FSI.BLL;
using Logging.BLL;
using cb3;
using cb3.Audit;

#endregion

//using NPayments.BLL;

namespace FSI.DAL {
    /// <summary>
    /// This class provides all neccessary functionality for financial statements database handling
    /// </summary>
    public class FinancialStatementDALC {
        /// <summary>
        /// For logging purpose
        /// </summary>
        private static String className = "FinancialStatementDALC";

        /// <summary>
        /// Takes instance of FinancialStatement and adds to the database
        /// </summary>
        /// <param name="theFinStatement">Instance of FinancialStatementsBLLC</param>
        /// <returns>true if ok, otherwise false</returns>
        public bool AddFinancialStatement(FinancialStatementBLLC theFinStatement) {
            // hmmm. einhvernveginn svona
            
            String funcName = "AddFinancialStatement(FinancialStatementBLLC theFinStatement)";
            if (theFinStatement.CompanyCIID < 0) {
                // ef ekki creditinfoID �� ver�ur a� b�ta vi� company 
            }
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();
                // the FSI_GeneralInfo table
                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "INSERT FSI_TemplateGeneralInfo(CreditInfoID,Financial_year,Financial_year_end,Account_period_length, " +
                        "Currency,Consolidated,Qualified_audit,Reviewed,Denomination_id,Employee_count,Comments_id,Date_of_delivery,Ready_for_web_publishing,Origin_id,Registrator_id,Modified) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        myOleDbConn);
                myOleDbCommand.Transaction = myTrans;
                //	myOleDbCommand.Connection = myOleDbConn;
                //	myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer);
                    myParam.IsNullable = false;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.CompanyCIID;
                        // h�r �arf a� finna e�a b�a til creditinfoID � fyrirt�ki�
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.FiscalYear;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year_end", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.YearEnded;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Account_period_length", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.AccountMonths;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Currency", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.CurrencyID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Consolidated", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Consolidated.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Qualified_audit", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Qualified.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Reviewed", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Reviewed;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Denomination_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.DenominationID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Employee_count", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    if (theFinStatement.StaffCount != int.MinValue) {
                        myParam.Value = theFinStatement.StaffCount;
                    }
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Comments_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.CommentsID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Date_of_delivery", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.DateOfDelivery;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Ready_for_web_publishing", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.ReadyForWebPublishing;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Origin_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.OriginID;
                    ;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Registrator_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.RegistratorID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Modified", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Modified;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    // n� � s��ast innsetta ID
                    myOleDbCommand.Parameters.Clear();
                    myOleDbCommand.CommandText = "SELECT @@IDENTITY FROM FSI_GeneralInfo";
                    theFinStatement.AFS_id = Convert.ToInt32(myOleDbCommand.ExecuteScalar());

                    // Setja einnig inn � Shareholders,Income sheet,Balance sheet og Cashflow
                    InsertShareholders(theFinStatement, myOleDbCommand);
                    InsertIncomeSheet(theFinStatement, myOleDbCommand);
                    InsertBalanceSheet(theFinStatement, myOleDbCommand);
                    InsertCashflow(theFinStatement, myOleDbCommand);
                    // framkv�ma ...
                    myTrans.Commit();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    myTrans.Rollback();
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes company CIID and fiscal year and deletes from the database
        /// </summary>
        /// <param name="companyCIID">The company internal system ID (CIID)</param>
        /// <returns>true of ok, otherwise false</returns>
        public bool DeleteFinancialStatement(int companyCIID, int year) {
            string funcName = "DeleteFinancialStatement(int companyCIID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "UPDATE FSI_GeneralInfo SET Deleted = 'True' WHERE CreditInfoID = " + companyCIID +
                            " AND Financial_year = " + year + "",
                            myOleDbConn);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns the year for given afs id
        /// </summary>
        /// <param name="afsID">The afs id to find year for</param>
        /// <returns>The year if found, -1 if not found</returns>
        public string GetFinancialStatementYear(int afsID) {
            string funcName = "GetFinancialStatementYear(int afsID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Financial_Year FROM FSI_TemplateGeneralInfo WHERE AFS_ID = " + afsID, myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            return reader.GetString(0);
                        }
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return "-1";
                }
            }
            return "-1";
        }

        public FISMaxCreditValues GetMaxCreditValues(int afsID) {
            string funcName = "FISMaxCreditValues GetMaxCreditValues(int afsID)";
            
            FISMaxCreditValues MaxCreditValues = new FISMaxCreditValues();
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Financial_Year,Currency, FSI_TemplateGeneralInfo.denomination_id, FSI_TemplateStatements.Total_equity, FSI_Denomination.Description_native,FSI_Denomination.Description_en  FROM FSI_TemplateGeneralInfo LEFT OUTER JOIN FSI_TemplateStatements ON FSI_TemplateStatements.AFSid = FSI_TemplateGeneralInfo.AFS_id LEFT OUTER JOIN FSI_Denomination ON FSI_TemplateGeneralInfo.denomination_id = FSI_Denomination.denomination_id WHERE FSI_TemplateGeneralInfo.AFS_ID = " +
                            afsID,
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            MaxCreditValues.FsYear = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            MaxCreditValues.Currency = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            MaxCreditValues.DenominationID = reader.GetInt32(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            MaxCreditValues.EquityTotal = reader.GetInt64(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            MaxCreditValues.DenominationDescriptionNative = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            MaxCreditValues.DenominationDescriptionEN = reader.GetString(5);
                        }
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return MaxCreditValues;
                }
            }
            return MaxCreditValues;
        }

        /// <summary>
        /// Returns the account period for given afs id
        /// </summary>
        /// <param name="afsID">The afs id to find account period for</param>
        /// <returns>The account period if found, -1 if not found</returns>
        public int GetFinancialStatementAccountPeriod(int afsID) {
            string funcName = "GetFinancialStatementAccountPeriod(int afsID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Account_period_length FROM FSI_TemplateGeneralInfo WHERE AFS_ID = " + afsID,
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            return reader.GetInt32(0);
                        }
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return -1;
                }
            }
            return -1;
        }

        /// <summary>
        /// Takes company CIID and fiscal year and returns an instance of financial statement
        /// </summary>
        /// <param name="nationallID">The companies nationalID</param>
        /// <param name="year">The requested fiscal year</param>
        /// <returns>Instance of financial statement</returns>
        public FinancialStatementBLLC GetFinancialStatement(int companyCIID, int year, int accountPeriod) {
            string funcName = "GetFinancialStatement(int companyCIID,int year,int accountPeriod)";
            FinancialStatementBLLC theFinancialStatement = new FinancialStatementBLLC();
            try {
                GetGeneralInfo(companyCIID, year, theFinancialStatement, accountPeriod);
                // h�r �arf einnig a� setja most recent account
                theFinancialStatement.MostRecentAccount = GetMostRecentAccount(companyCIID);
                if (!(theFinancialStatement.FiscalYear == "" || theFinancialStatement.FiscalYear == null)) {
                    GetShareholders(theFinancialStatement.AFS_id, theFinancialStatement);
                    GetIncomeSheet(theFinancialStatement.AFS_id, theFinancialStatement);
                    GetBalanceSheet(theFinancialStatement.AFS_id, theFinancialStatement);
                    GetCashflow(theFinancialStatement.AFS_id, theFinancialStatement);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                theFinancialStatement.FiscalYear = "-1"; // indicating that an db error has occurred
            }
            return theFinancialStatement;
        }

        /// <summary>
        /// Fetches all Financial statements for a specific company
        /// </summary>
        /// <param name="nCigID">Unique CreditInfoGroup ID</param>
        /// <returns>List of financial statement</returns>
        public DataSet GetFinancialStatements(int nCigID) {
            DataSet mySet = new DataSet();
            
            string strSql = "SELECT Financial_year, Account_period_length, created, TemplateID, NameNative, NameEN FROM FSI_TemplateGeneralInfo "
                            +
                            "LEFT JOIN FSI_Templates ON FSI_TemplateGeneralInfo.TemplateID = FSI_Templates.ID WHERE CreditInfoID=" +
                            nCigID;
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(strSql, myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        /// <summary>
        /// Takes instance of FinancialStatementBLLC and loop througt the Shareholders ArrayList and inserts
        /// </summary>
        /// <param name="theFinStatement">Instance of FinancialStatementBLLC</param>
        /// <param name="myOleDbCommand">The db command to use</param>
        /// <returns></returns>
        public bool InsertShareholders(FinancialStatementBLLC theFinStatement, OleDbCommand myOleDbCommand) {
            // �arf a� n�ta ArrayList til innsetningar
            string funcName = "InsertShareholders(FinancialStatementBLLC theFinStatement,OleDbCommand myOleDbCommand)";
            myOleDbCommand.CommandText =
                "INSERT INTO FSI_Shareholders(AFS_id,Name,Company_or_private_person,National_identification_number,Percentage_hold,ISO_country_code) VALUES(?,?,?,?,?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                for (int i = 0; i < theFinStatement.Shareholders.Count; i++) {
                    Shareholder theShareHolder = new Shareholder();
                    theShareHolder = (Shareholder) theFinStatement.Shareholders[i];
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theShareHolder.AFS_id;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Name", OleDbType.WChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theShareHolder.ShareholdersName;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Company_or_private_person", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theShareHolder.CompanyOrPrivatePerson;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("National_identification_number", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theShareHolder.ShareholdersNationalIdentificationNumber;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Percentage_hold", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theShareHolder.ShareholdersPercentageHold;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("ISO_country_code", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theShareHolder.ShareholdersISO_countryCode;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                throw (new ApplicationException(err.Message));
            }

            return true;
        }

        private bool InsertIncomeSheet(FinancialStatementBLLC theFinStatement, OleDbCommand myOleDbCommand) {
            string funcName = "InsertIncomeSheet(FinancialStatementBLLC theFinStatement,OleDbCommand myOleDbCommand)";
            myOleDbCommand.CommandText =
                "INSERT INTO FSI_IncomeSheet(AFS_id,Small_status,Revenue_from_main_operations,Other_operating_revenue," +
                "Total_revenue,Cost_of_sales,Administrative_and_other_expenses,Distribution_costs,Total_costs,Staff_costs,Deprication_of_fixed_assets," +
                "Operating_profit,Financial_income,Financial_costs,Other_financial_items,Net_financial_items,Income_from_operations_before_tax,Taxes," +
                "Income_from_operations_after_tax,Extraordinary_income_or_costs,Result_before_income_from_associated_companies,Income_from_affiliates," +
                "Income_from_subsidiaries,Income_from_associated_companies,Net_profit) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                myOleDbCommand.Parameters.Clear();
                myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = theFinStatement.AFS_id;
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Small_status", OleDbType.WChar);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = theFinStatement.SmallStatus.ToString();
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Revenue_from_main_operations", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.RevenueFromMainOperation != int.MinValue) {
                    myParam.Value = theFinStatement.RevenueFromMainOperation;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_operating_revenue", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherOperationRevenue != int.MinValue) {
                    myParam.Value = theFinStatement.OtherOperationRevenue;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_revenue", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalRevenue != int.MinValue) {
                    myParam.Value = theFinStatement.TotalRevenue;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cost_of_sales", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = theFinStatement.CostOfSales;
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Administrative_and_other_expenses", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.AdministrativeAndOtherExpenses != int.MinValue) {
                    myParam.Value = theFinStatement.AdministrativeAndOtherExpenses;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Distribution_costs", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.DistributionCost != int.MinValue) {
                    myParam.Value = theFinStatement.DistributionCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_costs", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalCost != int.MinValue) {
                    myParam.Value = theFinStatement.TotalCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Staff_costs", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.StaffCost != int.MinValue) {
                    myParam.Value = theFinStatement.StaffCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Deprication_of_fixed_assets", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.DepricationOfFixedAssets != int.MinValue) {
                    myParam.Value = theFinStatement.DepricationOfFixedAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Operating_profit", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OperationalProfit != int.MinValue) {
                    myParam.Value = theFinStatement.OperationalProfit;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Financial_income", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.FinancialIncome != int.MinValue) {
                    myParam.Value = theFinStatement.FinancialIncome;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Financial_costs", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.FinancialCost != int.MinValue) {
                    myParam.Value = theFinStatement.FinancialCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_financial_items", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherFinancialEntries != int.MinValue) {
                    myParam.Value = theFinStatement.OtherFinancialEntries;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Net_financial_items", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.NetFinancialItems != int.MinValue) {
                    myParam.Value = theFinStatement.NetFinancialItems;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_operations_before_tax", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromOperationsBeforeTax != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromOperationsBeforeTax;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Taxes", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeTax != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeTax;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_operations_after_tax", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromOperationsAfterTax != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromOperationsAfterTax;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Extraordinary_income_or_costs", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ExtraOrdinaryIncomeCost != int.MinValue) {
                    myParam.Value = theFinStatement.ExtraOrdinaryIncomeCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Result_before_income_from_associated_companies", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ResultBeforeIncomeFromAssociatedCompanies != int.MinValue) {
                    myParam.Value = theFinStatement.ResultBeforeIncomeFromAssociatedCompanies;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_affiliates", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromAffiliates != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromAffiliates;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_subsidiaries", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromSubsidiaries != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromSubsidiaries;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_associated_companies", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromAssociatedCompanies != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromAssociatedCompanies;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Net_profit", OleDbType.Integer);
                myParam.IsNullable = false;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.NetProfit != int.MinValue) {
                    myParam.Value = theFinStatement.NetProfit;
                }
                myOleDbCommand.Parameters.Add(myParam);

                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                throw (new ApplicationException(err.Message));
            }

            return true;
        }

        private bool InsertBalanceSheet(FinancialStatementBLLC theFinStatement, OleDbCommand myOleDbCommand) {
            string funcName = "InsertBalanceSheet(FinancialStatementBLLC theFinStatement,OleDbCommand myOleDbCommand)";
            myOleDbCommand.CommandText =
                "INSERT INTO FSI_BalanceSheet(AFS_id,Intangible_assets,Tangible_assets,Financial_assets," +
                "Total_fixed_assets,Inventory,Receivables_and_debtors,ms_towards_associated_companies,Short_term_investments," +
                "Cash,Deferred_tax_assets,Other_current_assets,Total_current_assets,Total_assets,Issued_share_capital,Profit_and_loss_account," +
                "Other_equity,Total_equity,Pension_funds_liabilities,Tax_liabilities,Other_liabilities,Total_liabilities,Long_term_debts," +
                "Loans_overdraft,Creditors,Debts_towards_associated_companies,Current_portion_of_long_term_liabilities,Other_short_term_debt," +
                "Current_assets,Total_short_term_debt,Current_assets_less_current_liabilities,Total_debts_and_equity,Minority_holdings_in_equity) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                myOleDbCommand.Parameters.Clear();
                myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = theFinStatement.AFS_id;
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Intangible_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IntangableAssets != int.MinValue) {
                    myParam.Value = theFinStatement.IntangableAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Tangible_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TangableAssets != int.MinValue) {
                    myParam.Value = theFinStatement.TangableAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Financial_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.FinancialAssets != int.MinValue) {
                    myParam.Value = theFinStatement.FinancialAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_fixed_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalFixedAssets != int.MinValue) {
                    myParam.Value = theFinStatement.TotalFixedAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Inventory", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.Inventory != int.MinValue) {
                    myParam.Value = theFinStatement.Inventory;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Receivables_and_debtors", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.Debtors != int.MinValue) {
                    myParam.Value = theFinStatement.Debtors;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("ms_towards_associated_companies", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ClaimsTowardsAssociatedCompanies != int.MinValue) {
                    myParam.Value = theFinStatement.ClaimsTowardsAssociatedCompanies;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Short_term_investments", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ShortTermInvestments != int.MinValue) {
                    myParam.Value = theFinStatement.ShortTermInvestments;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cash", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CashAtBankAndInHand != int.MinValue) {
                    myParam.Value = theFinStatement.CashAtBankAndInHand;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Deferred_tax_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.DeferredTaxAsset != int.MinValue) {
                    myParam.Value = theFinStatement.DeferredTaxAsset;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_current_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherCurrentAssets != int.MinValue) {
                    myParam.Value = theFinStatement.OtherCurrentAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_current_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalCurrentAssets != int.MinValue) {
                    myParam.Value = theFinStatement.TotalCurrentAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalAssets != int.MinValue) {
                    myParam.Value = theFinStatement.TotalAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Issued_share_capital", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalCurrentAssets != int.MinValue) {
                    myParam.Value = theFinStatement.IssuedShareCapital;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Profit_and_loss_account", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ProfitAndLossAccount != int.MinValue) {
                    myParam.Value = theFinStatement.ProfitAndLossAccount;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_equity", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherEquity != int.MinValue) {
                    myParam.Value = theFinStatement.OtherEquity;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_equity", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalEquity != int.MinValue) {
                    myParam.Value = theFinStatement.TotalEquity;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Pension_funds_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.PensionFundsLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.PensionFundsLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Tax_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TaxLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.TaxLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.OtherLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.TotalLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Long_term_debts", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.LongTermDebts != int.MinValue) {
                    myParam.Value = theFinStatement.LongTermDebts;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Loans_overdraft", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.LoansOverdraft != int.MinValue) {
                    myParam.Value = theFinStatement.LoansOverdraft;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Creditors", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.Creditors != int.MinValue) {
                    myParam.Value = theFinStatement.Creditors;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Debts_towards_associated_companies", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.DebtToAssociatedCompanies != int.MinValue) {
                    myParam.Value = theFinStatement.DebtToAssociatedCompanies;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Current_portion_of_long_term_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CurrentPortionOfLongTermLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.CurrentPortionOfLongTermLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_short_term_debt", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherShortTermDebt != int.MinValue) {
                    myParam.Value = theFinStatement.OtherShortTermDebt;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Current_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CurrentAssets != int.MinValue) {
                    myParam.Value = theFinStatement.CurrentAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_short_term_debt", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalShortTermDebts != int.MinValue) {
                    myParam.Value = theFinStatement.TotalShortTermDebts;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Current_assets_less_current_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CurrentAssetsLessCurrentLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.CurrentAssetsLessCurrentLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_debts_and_equity", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalDebtsEquity != int.MinValue) {
                    myParam.Value = theFinStatement.TotalDebtsEquity;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Minority_holdings_in_equity", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.MinorityHoldingsInEquity != int.MinValue) {
                    myParam.Value = theFinStatement.MinorityHoldingsInEquity;
                }
                myOleDbCommand.Parameters.Add(myParam);

                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                throw (new ApplicationException(err.Message));
            }

            return true;
        }

        public bool InsertCashflow(FinancialStatementBLLC theFinStatement, OleDbCommand myOleDbCommand) {
            string funcName = "InsertCashflow(FinancialStatementBLLC theFinStatement,OleDbCommand myOleDbCommand)";
            myOleDbCommand.CommandText =
                "INSERT INTO FSI_Cashflow(AFS_id,Operating_profit_before_working_capital_changes,Cash_from_operations,Net_cash_from_operating_activities,Cash_flow_from_investing_activities,Cash_flow_from_financing_activities,Increase_or_decrease_in_cash) VALUES(?,?,?,?,?,?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                myOleDbCommand.Parameters.Clear();
                myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = theFinStatement.AFS_id;
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Operating_profit_before_working_capital_changes", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OperatingProfitBeforeWorkingCapitalChanges != int.MinValue) {
                    myParam.Value = theFinStatement.OperatingProfitBeforeWorkingCapitalChanges;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cash_from_operations", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CashFromOperations != int.MinValue) {
                    myParam.Value = theFinStatement.CashFromOperations;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Net_cash_from_operating_activities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CashProvidedByOperatingActivities != int.MinValue) {
                    myParam.Value = theFinStatement.CashProvidedByOperatingActivities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cash_flow_from_investing_activities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.InvestingActivities != int.MinValue) {
                    myParam.Value = theFinStatement.InvestingActivities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cash_flow_from_financing_activities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.FinancialActivities != int.MinValue) {
                    myParam.Value = theFinStatement.FinancialActivities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Increase_or_decrease_in_cash", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncreaseDecreaseInCashAndCashInHand != int.MinValue) {
                    myParam.Value = theFinStatement.IncreaseDecreaseInCashAndCashInHand;
                }
                myOleDbCommand.Parameters.Add(myParam);

                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                throw (new ApplicationException(err.Message));
            }

            return true;
        }

        public DataSet GetDenominationAsDataSet() {
            DataSet mySet = new DataSet();
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM FSI_Denomination", myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        public DataSet GetSourceAsDataSet() {
            DataSet mySet = new DataSet();
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM FSI_Origin", myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        public DataSet GetCommentsAsDataSet() {
            DataSet mySet = new DataSet();
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM FSI_Comments", myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        public void GetGeneralInfo(
            int companyCIID, int year, FinancialStatementBLLC theFinancialStatement, int accountPeriod) {
            string funcName = "GetGeneralInfo(string nationallID,int year,FinancialStatementBLLC theFinancialStatement)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    String sql =
                        "SELECT FSI_TemplateGeneralInfo.AFS_id, FSI_TemplateGeneralInfo.CreditInfoID, FSI_TemplateGeneralInfo.Financial_year, " +
                        "FSI_TemplateGeneralInfo.Financial_year_end, FSI_TemplateGeneralInfo.Account_period_length, FSI_TemplateGeneralInfo.Currency, " +
                        "FSI_TemplateGeneralInfo.Consolidated, FSI_TemplateGeneralInfo.Qualified_audit, FSI_TemplateGeneralInfo.Reviewed, FSI_TemplateGeneralInfo.Denomination_id, " +
                        "FSI_TemplateGeneralInfo.Employee_count, FSI_TemplateGeneralInfo.Comments_id, FSI_TemplateGeneralInfo.Date_of_delivery, FSI_TemplateGeneralInfo.Ready_for_web_publishing, " +
                        "FSI_TemplateGeneralInfo.Origin_id, FSI_TemplateGeneralInfo.Registrator_id, FSI_TemplateGeneralInfo.Modified, FSI_Denomination.Description_native AS DenominationNative, " +
                        "FSI_Denomination.Description_en AS DenominationEN, FSI_Comments.Description_native AS CommentsNative, FSI_Comments.Description_en AS CommentsEN " +
                        "FROM FSI_TemplateGeneralInfo LEFT JOIN FSI_Denomination ON FSI_TemplateGeneralInfo.Denomination_id = FSI_Denomination.Denomination_id LEFT JOIN " +
                        "FSI_Comments ON FSI_TemplateGeneralInfo.Comments_id = FSI_Comments.Comments_id WHERE " +
                        "(FSI_TemplateGeneralInfo.CreditInfoID = " + companyCIID +
                        ") AND (FSI_TemplateGeneralInfo.Financial_year = " + year + ") " +
                        "AND (FSI_TemplateGeneralInfo.Account_period_length = " + accountPeriod + ") " +
                        "AND FSI_TemplateGeneralInfo.Ready_for_web_publishing = 'True'";

                    //	String sql = "SELECT AFS_id,CreditInfoID,Financial_year,Financial_year_end,Account_period_length, " +
                    //	"Currency,Consolidated,Qualified_audit,Reviewed,Denomination_id,Employee_count,Comments_id,Date_of_delivery,Ready_for_web_publishing,Origin_id,Registrator_id,Modified FROM FSI_GeneralInfo WHERE CreditInfoID = "+ companyCIID +" AND Financial_year = "+ year +" AND Account_period_length = "+ accountPeriod +"";

                    OleDbCommand myOleDbCommand = new OleDbCommand(sql, myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        theFinancialStatement.AFS_id = reader.GetInt32(0);
                        theFinancialStatement.CompanyCIID = reader.GetInt32(1);
                        theFinancialStatement.FiscalYear = reader.GetString(2);
                        if (!reader.IsDBNull(3)) {
                            theFinancialStatement.YearEnded = reader.GetDateTime(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            theFinancialStatement.AccountMonths = reader.GetInt32(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            theFinancialStatement.CurrencyID = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            theFinancialStatement.Consolidated = Convert.ToBoolean(reader.GetString(6));
                        }
                        if (!reader.IsDBNull(7)) {
                            theFinancialStatement.Qualified = Convert.ToBoolean(reader.GetString(7));
                        }
                        if (!reader.IsDBNull(8)) {
                            theFinancialStatement.Reviewed = Convert.ToBoolean(reader.GetString(8));
                        }
                        if (!reader.IsDBNull(9)) {
                            theFinancialStatement.DenominationID = reader.GetInt32(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            theFinancialStatement.StaffCount = reader.GetInt32(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            theFinancialStatement.CommentsID = reader.GetInt32(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            theFinancialStatement.DateOfDelivery = reader.GetDateTime(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            theFinancialStatement.ReadyForWebPublishing = Convert.ToBoolean(reader.GetString(13));
                        }
                        if (!reader.IsDBNull(14)) {
                            theFinancialStatement.OriginID = reader.GetInt32(14);
                        }
                        if (!reader.IsDBNull(15)) {
                            theFinancialStatement.RegistratorID = reader.GetInt32(15);
                        }
                        if (!reader.IsDBNull(16)) {
                            theFinancialStatement.Modified = reader.GetDateTime(16);
                        }

                        if (!reader.IsDBNull(17)) {
                            theFinancialStatement.DenominationNative = reader.GetString(17);
                        }
                        if (!reader.IsDBNull(18)) {
                            theFinancialStatement.DenominationEN = reader.GetString(18);
                        }
                        if (!reader.IsDBNull(19)) {
                            theFinancialStatement.CommentsNative = reader.GetString(19);
                        }
                        if (!reader.IsDBNull(20)) {
                            theFinancialStatement.CommentsEN = reader.GetString(20);
                        }

                        /*	if(!reader.IsDBNull(3))
							myNumber.NumberTypeNative = reader.GetString(3);
						if(!reader.IsDBNull(4))
							myNumber.NumberTypeID = reader.GetInt32(4);
					*/

                        //	myArr.Add(myNumber);
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    throw (new ApplicationException(err.Message));
                }
            }
        }

        public string GetMostRecentAccount(int companyCIID) {
            string funcName = "GetMostRecentAccount(companyCIID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Financial_year FROM FSI_TemplateGeneralInfo " +
                            "WHERE CreditInfoID = " + companyCIID +
                            " ORDER BY Financial_year DESC,financial_year_end DESC",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return reader.GetString(0);
                    } else {
                        return "N/A";
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    throw (new ApplicationException(err.Message));
                }
            }
        }

        public void GetShareholders(int afsID, FinancialStatementBLLC theFinancialStatement) {
            string funcName = "GetShareholders(int afsID,FinancialStatementBLLC theFinancialStatement)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT AFS_id,Name,Company_or_private_person,National_identification_number,Percentage_hold,ISO_country_code FROM FSI_Shareholders WHERE AFS_id = " +
                            afsID + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    // hmmm h�r �arf n�ttlega a� vera Arraylisti af Shareholders ...
                    // �etta krassar vi� null f�rslu �a� �arf a� koma � veg fyrir a� null f�rslur s�u skr��ar...
                    while (reader.Read()) {
                        Shareholder theHolder = new Shareholder();
                        theHolder.AFS_id = reader.GetInt32(0);
                        theHolder.ShareholdersName = reader.GetString(1);
                        theHolder.CompanyOrPrivatePerson = reader.GetInt32(2);
                        theHolder.ShareholdersNationalIdentificationNumber = reader.GetString(3);
                        theHolder.ShareholdersPercentageHold = reader.GetString(4);
                        theHolder.ShareholdersISO_countryCode = reader.GetString(5);

                        theFinancialStatement.Shareholders.Add(theHolder);
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    throw (new ApplicationException(err.Message));
                }
            }
        }

        public void GetIncomeSheet(int afsID, FinancialStatementBLLC theFinStatement) {
            string funcName = "GetIncomeSheet(int afsID,FinancialStatementBLLC theFinancialStatement)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT AFSid, 0,Revenue_from_main_operations,Other_operating_revenue," +
                            "Total_revenue,Cost_of_sales,Administrative_other_expenses,Distribution_cost,Total_costs,Staff_cost,Deprication_of_fixed_assets," +
                            "Operating_profit,Financial_income,Financial_cost,Other_financial_items,Net_financial_items,Income_from_operations_before_tax,Taxes," +
                            "Income_from_operations_after_tax,Extraordinary_income_cost,Result_before_income_from_associated_companies,Income_from_affiliates," +
                            "Income_from_subsidiaries,Income_from_associated_companies,Net_profit,Gross_profit FROM FSI_TemplateStatements WHERE AFSid = " +
                            afsID + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        theFinStatement.AFS_id = reader.GetInt32(0);
                        ;
                        //if(!reader.IsDBNull(1))
                        theFinStatement.SmallStatus = true; //Convert.ToBoolean(reader.GetString(1));
                        if (!reader.IsDBNull(2)) {
                            theFinStatement.RevenueFromMainOperation = reader.GetInt64(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            theFinStatement.OtherOperationRevenue = reader.GetInt64(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            theFinStatement.TotalRevenue = reader.GetInt64(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            theFinStatement.CostOfSales = reader.GetInt64(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            theFinStatement.AdministrativeAndOtherExpenses = reader.GetInt64(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            theFinStatement.DistributionCost = reader.GetInt64(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            theFinStatement.TotalCost = reader.GetInt64(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            theFinStatement.StaffCost = reader.GetInt64(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            theFinStatement.DepricationOfFixedAssets = reader.GetInt64(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            theFinStatement.OperationalProfit = reader.GetInt64(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            theFinStatement.FinancialIncome = reader.GetInt64(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            theFinStatement.FinancialCost = reader.GetInt64(13);
                        }
                        if (!reader.IsDBNull(14)) {
                            theFinStatement.OtherFinancialEntries = reader.GetInt64(14);
                        }
                        if (!reader.IsDBNull(15)) {
                            theFinStatement.NetFinancialItems = reader.GetInt64(15);
                        }
                        if (!reader.IsDBNull(16)) {
                            theFinStatement.IncomeFromOperationsBeforeTax = reader.GetInt64(16);
                        }
                        if (!reader.IsDBNull(17)) {
                            theFinStatement.IncomeTax = reader.GetInt64(17);
                        }
                        if (!reader.IsDBNull(18)) {
                            theFinStatement.IncomeFromOperationsAfterTax = reader.GetInt64(18);
                        }
                        if (!reader.IsDBNull(19)) {
                            theFinStatement.ExtraOrdinaryIncomeCost = reader.GetInt64(19);
                        }
                        if (!reader.IsDBNull(20)) {
                            theFinStatement.ResultBeforeIncomeFromAssociatedCompanies = reader.GetInt64(20);
                        }
                        if (!reader.IsDBNull(21)) {
                            theFinStatement.IncomeFromAffiliates = reader.GetInt64(21);
                        }
                        if (!reader.IsDBNull(22)) {
                            theFinStatement.IncomeFromSubsidiaries = reader.GetInt64(22);
                        }
                        if (!reader.IsDBNull(23)) {
                            theFinStatement.IncomeFromAssociatedCompanies = reader.GetInt64(23);
                        }
                        if (!reader.IsDBNull(24)) {
                            theFinStatement.NetProfit = reader.GetInt64(24);
                        }
                        if (!reader.IsDBNull(25)) {
                            theFinStatement.GrossProfit = reader.GetInt64(25);
                        }
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    throw (new ApplicationException(err.Message));
                }
            }
        }

        public void GetBalanceSheet(int afsID, FinancialStatementBLLC theFinStatement) {
            string funcName = "GetBalanceSheet(int afsID,FinancialStatementBLLC theFinancialStatement)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Intangible_assets,Tangible_assets,Financial_assets," +
                            "Total_fixed_assets,Inventory,Receivables_and_debtors,Claims_towards_associated_companies,Short_term_investments," +
                            "Cash,Deferred_tax_assets,Other_current_assets,Total_current_assets,Total_assets,Issued_share_capital,Profit_and_loss_account," +
                            "Other_equity,Total_equity,Pension_funds_liabilities,Tax_liabilities,Other_liabilities,Total_liabilities,Long_term_debts," +
                            "Loans_overdraft,Creditors,Debts_towards_associated_companies,0,Other_short_term_debt," +
                            "0, Total_short_term_debt,Current_assets_less_current_liabilities,Total_debts_and_equity,Minority_holdings_in_equity FROM FSI_TemplateStatements WHERE AFSid = " +
                            afsID + "",
                            myOleDbConn);

                    /*OleDbCommand myOleDbCommand = new OleDbCommand("SELECT Intangible_assets,Tangible_assets,Financial_assets,"+
						"Total_fixed_assets,Inventory,Receivables_and_debtors,Claims_towards_associated_companies,Short_term_investments,"+
						"Cash,Deferred_tax_assets,Other_current_assets,Total_current_assets,Total_assets,Issued_share_capital,Profit_and_loss_account,"+
						"Other_equity,Total_equity,Pension_funds_liabilities,Tax_liabilities,Other_liabilities,Total_liabilities,Long_term_debts,"+
						"Loans_overdraft,Creditors,Debts_towards_associated_companies,Current_portion_of_long_term_liabilities,Other_short_term_debt,"+
						"Current_assets,Total_short_term_debt,Current_assets_less_current_liabilities,Total_debts_and_equity,Minority_holdings_in_equity FROM FSI_TemplateStatements WHERE AFSid = "+ afsID +"",myOleDbConn);
					*/
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            theFinStatement.IntangableAssets = reader.GetInt64(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theFinStatement.TangableAssets = reader.GetInt64(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theFinStatement.FinancialAssets = reader.GetInt64(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            theFinStatement.TotalFixedAssets = reader.GetInt64(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            theFinStatement.Inventory = reader.GetInt64(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            theFinStatement.Debtors = reader.GetInt64(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            theFinStatement.ClaimsTowardsAssociatedCompanies = reader.GetInt64(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            theFinStatement.ShortTermInvestments = reader.GetInt64(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            theFinStatement.CashAtBankAndInHand = reader.GetInt64(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            theFinStatement.DeferredTaxAsset = reader.GetInt64(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            theFinStatement.OtherCurrentAssets = reader.GetInt64(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            theFinStatement.TotalCurrentAssets = reader.GetInt64(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            theFinStatement.TotalAssets = reader.GetInt64(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            theFinStatement.IssuedShareCapital = reader.GetInt64(13);
                        }
                        if (!reader.IsDBNull(14)) {
                            theFinStatement.ProfitAndLossAccount = reader.GetInt64(14);
                        }
                        if (!reader.IsDBNull(15)) {
                            theFinStatement.OtherEquity = reader.GetInt64(15);
                        }
                        if (!reader.IsDBNull(16)) {
                            theFinStatement.TotalEquity = reader.GetInt64(16);
                        }
                        if (!reader.IsDBNull(17)) {
                            theFinStatement.PensionFundsLiabilities = reader.GetInt64(17);
                        }
                        if (!reader.IsDBNull(18)) {
                            theFinStatement.TaxLiabilities = reader.GetInt64(18);
                        }
                        if (!reader.IsDBNull(19)) {
                            theFinStatement.OtherLiabilities = reader.GetInt64(19);
                        }
                        if (!reader.IsDBNull(20)) {
                            theFinStatement.TotalLiabilities = reader.GetInt64(20);
                        }
                        if (!reader.IsDBNull(21)) {
                            theFinStatement.LongTermDebts = reader.GetInt64(21);
                        }
                        if (!reader.IsDBNull(22)) {
                            theFinStatement.LoansOverdraft = reader.GetInt64(22);
                        }
                        if (!reader.IsDBNull(23)) {
                            theFinStatement.Creditors = reader.GetInt64(23);
                        }
                        if (!reader.IsDBNull(24)) {
                            theFinStatement.DebtToAssociatedCompanies = reader.GetInt64(24);
                        }
                        //if(!reader.IsDBNull(25))
                        //	theFinStatement.CurrentPortionOfLongTermLiabilities = reader.GetInt64(25);
                        if (!reader.IsDBNull(26)) {
                            theFinStatement.OtherShortTermDebt = reader.GetInt64(26);
                        }
                        //if(!reader.IsDBNull(27))
                        //	theFinStatement.CurrentAssets = reader.GetInt64(27);
                        if (!reader.IsDBNull(28)) {
                            theFinStatement.TotalShortTermDebts = reader.GetInt64(28);
                        }
                        if (!reader.IsDBNull(29)) {
                            theFinStatement.CurrentAssetsLessCurrentLiabilities = reader.GetInt64(29);
                        }
                        if (!reader.IsDBNull(30)) {
                            theFinStatement.TotalDebtsEquity = reader.GetInt64(30);
                        }
                        if (!reader.IsDBNull(31)) {
                            theFinStatement.MinorityHoldingsInEquity = reader.GetInt64(31);
                        }
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    throw (new ApplicationException(err.Message));
                }
            }
        }

        /// <summary>
        /// Returning the Equity total
        /// </summary>
        /// <param name="afs_id">The afs id</param>
        /// <returns>Equity total for given account</returns>
        public long GetEquityTotal(int afs_id) {
            string funcName = "GetEquityTotal(int afs_id)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Total_equity FROM FSI_TemplateStatements WHERE AFSid = " + afs_id + "", myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            return reader.GetInt64(0);
                        } else {
                            return -1;
                        }
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    throw (new ApplicationException(err.Message));
                }
            }
            return -1;
        }

        /// <summary>
        /// Returns the denomination_id for given statement id
        /// </summary>
        /// <param name="afs_id">The statement id</param>
        /// <returns>The denomination id if failes then -1</returns>
        public int GetDenominationID(int afs_id) {
            string funcName = "GetDenominationID(int afs_id)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Denomination_id FROM FSI_TemplateGeneralInfo WHERE AFS_id = " + afs_id + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            return reader.GetInt32(0);
                        } else {
                            return -1;
                        }
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    throw (new ApplicationException(err.Message));
                }
            }
            return -1;
        }

        public void GetCashflow(int afsID, FinancialStatementBLLC theFinStatement) {
            string funcName = "GetCashflow(int afsID,FinancialStatementBLLC theFinancialStatement)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT Operating_profit_before_working_capital_changes,Cash_from_operations," +
                            "Net_cash_from_operating_activities,Cash_flows_from_investing_activities,Cash_flow_from_financing_activities,Increase_or_decrease_in_cash " +
                            "FROM FSI_TemplateStatements WHERE AFSid = " + afsID + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            theFinStatement.OperatingProfitBeforeWorkingCapitalChanges = reader.GetInt64(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theFinStatement.CashFromOperations = reader.GetInt64(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theFinStatement.CashProvidedByOperatingActivities = reader.GetInt64(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            theFinStatement.InvestingActivities = reader.GetInt64(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            theFinStatement.FinancialActivities = reader.GetInt64(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            theFinStatement.IncreaseDecreaseInCashAndCashInHand = reader.GetInt64(5);
                        }
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    throw (new ApplicationException(err.Message));
                }
            }
        }

        /// <summary>
        /// Takes instance of FinancialStatementBLLC and updates in the database
        /// </summary>
        /// <param name="theFinStatement">Instance of FinancialStatementBLLC</param>
        /// <returns></returns>
        public bool UpdateFinancialStatement(FinancialStatementBLLC theFinStatement) {
            
            String funcName = "UpdateFinancialStatement(FinancialStatementBLLC theFinStatement)";

            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();
                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "UPDATE FSI_TemplateGeneralInfo SET CreditInfoID = ?,Financial_year = ?,Financial_year_end = ?,Account_period_length = ?,Currency = ?,Consolidated = ?, Qualified_audit = ?, Reviewed = ?, " +
                        "Denomination_id = ?,Employee_count = ?,Comments_id = ?,Date_of_delivery = ?,Ready_for_web_publishing = ?,Origin_id = ?,Registrator_id = ?,Modified = ? " +
                        "WHERE AFS_id = ?",
                        myOleDbConn);
                myOleDbCommand.Transaction = myTrans;

                try {
                    //	myOleDbCommand.CommandText = "UPDATE FSI_GeneralInfo SET CreditInfoID = ?,Financial_year = ?,Financial_year_end = ?,Account_period_length = ?,Currency = ?,Consolidated = ?, Qualified_audit = ?, Reviewed = ?, " +
                    //		"Denomination_id = ?,Employee_count = ?,Comments_id = ?,Date_of_delivery = ?,Ready_for_web_publishing = ?,Origin_id = ?,Registrator_id = ?,Modified = ? " + 
                    //		"WHERE AFS_id = ?";
                    //	myOleDbCommand.Transaction = myTrans;
                    OleDbParameter myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.CompanyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.FiscalYear;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year_end", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.YearEnded;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Account_period_length", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.AccountMonths;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Currency", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.CurrencyID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Consolidated", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Consolidated.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Qualified_audit", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Qualified.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Reviewed", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Reviewed.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Denomination_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.DenominationID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Employee_count", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.StaffCount;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Comments_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.CommentsID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Date_of_delivery", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.DateOfDelivery;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Ready_for_web_publishing", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.ReadyForWebPublishing.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Origin_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.OriginID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Registrator_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.RegistratorID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Modified", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Modified;
                    myOleDbCommand.Parameters.Add(myParam);

                    // where parameterinn ...
                    myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.AFS_id;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    //	UpdateGeneralInfo(theFinStatement,myOleDbCommand);
                    UpdateShareholders(theFinStatement, myOleDbCommand);
                    UpdateIncomeSheet(theFinStatement, myOleDbCommand);
                    UpdateBalanceSheet(theFinStatement, myOleDbCommand);
                    UpdateCashflow(theFinStatement, myOleDbCommand);
                    // framkv�ma ...
                    myTrans.Commit();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    myTrans.Rollback();
                    return false;
                }
            }
            return true;
        }

        private bool UpdateGeneralInfo(FinancialStatementBLLC theFinStatement, OleDbCommand myOleDbCommand) {
            String funcName = "UpdateGeneralInfo(FinancialStatementBLLC theFinStatement,OleDbCommand myOleDbCommand)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();
                try {
                    /*
					 CreditInfoID,Financial_year,Financial_year_end,Account_period_length, " +
					"Currency,Consolidated,Qualified_audit,Reviewed,Denomination_id,Employee_count,Comments_id,Date_of_delivery,Ready_for_web_publishing,Origin_id,Registrator_id,Modified
					*/
                    /*	myOleDbCommand = new OleDbCommand("UPDATE FSI_GeneralInfo SET CreditInfoID = ?,Financial_year = ?,Financial_year_end = ?,Account_period_length = ?,Currency = ?,Consolidated = ?, Qualified_audit = ?, Reviewed = ?, " +
						"Denomination_id = ?,Employee_count = ?,Comments_id = ?,Date_of_delivery = ?,Ready_for_web_publishing = ?,Origin_id = ?,Registrator_id = ?,Modified = ? " + 
						"WHERE AFS_id = ?" ,myOleDbConn);
				*/
                    myOleDbCommand.CommandText =
                        "UPDATE FSI_TemplateGeneralInfo SET CreditInfoID = ?,Financial_year = ?,Financial_year_end = ?,Account_period_length = ?,Currency = ?,Consolidated = ?, Qualified_audit = ?, Reviewed = ?, " +
                        "Denomination_id = ?,Employee_count = ?,Comments_id = ?,Date_of_delivery = ?,Ready_for_web_publishing = ?,Origin_id = ?,Registrator_id = ?,Modified = ? " +
                        "WHERE AFS_id = ?";
                    myOleDbCommand.Transaction = myTrans;
                    OleDbParameter myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.CompanyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.FiscalYear;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year_end", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    if (theFinStatement.YearEnded != DateTime.MinValue) {
                        myParam.Value = theFinStatement.YearEnded;
                    }
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Account_period_length", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.AccountMonths;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Currency", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.CurrencyID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Consolidated", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Consolidated.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Qualified_audit", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Qualified.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Reviewed", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Reviewed.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Denomination_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.DenominationID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Employee_count", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.StaffCount;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Comments_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.CommentsID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Date_of_delivery", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.DateOfDelivery;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Ready_for_web_publishing", OleDbType.VarChar);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.ReadyForWebPublishing.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Origin_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.OriginID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Registrator_id", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.RegistratorID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Modified", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.Modified;
                    myOleDbCommand.Parameters.Add(myParam);

                    // where parameterinn ...
                    myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theFinStatement.AFS_id;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    throw (new ApplicationException(err.Message));
                }
            }
            return true;
        }

        private bool UpdateShareholders(FinancialStatementBLLC theFinStatement, OleDbCommand myOleDbCommand) {
            // h�r �arf a� ey�a og svo uppf�ra(inserta) ... � �etta eftir �ar til shareholders ver�a settir inn
            return true;
        }

        private bool UpdateIncomeSheet(FinancialStatementBLLC theFinStatement, OleDbCommand myOleDbCommand) {
            String funcName = "UpdateIncomeSheet(FinancialStatementBLLC theFinStatement,OleDbCommand myOleDbCommand)";
            try {
                /*
				  INSERT INTO FSI_IncomeSheet(AFS_id,Small_status,Revenue_from_main_operations,Other_operating_revenue,"+
				"Total_revenue,Cost_of_sales,Administrative_and_other_expenses,Distribution_costs,Total_costs,Staff_costs,Deprication_of_fixed_assets,"+
				"Operating_profit,Financial_income,Financial_costs,Other_financial_items,Net_financial_items,Income_from_operations_before_tax,Taxes,"+
				"Income_from_operations_after_tax,Extraordinary_income_or_costs,Result_before_income_from_associated_companies,Income_from_affiliates,"+
				"Income_from_subsidiaries,Income_from_associated_companies,Net_profit
				*/
                myOleDbCommand.CommandText =
                    "UPDATE FSI_IncomeSheet SET Small_status = ?,Revenue_from_main_operations = ?,Other_operating_revenue = ?,Total_revenue = ?,Cost_of_sales = ?,Administrative_and_other_expenses = ?, Distribution_costs = ?, Total_costs = ?, " +
                    "Staff_costs = ?,Deprication_of_fixed_assets = ?,Operating_profit = ?,Financial_income = ?,Financial_costs = ?,Other_financial_items = ?,Net_financial_items = ?,Income_from_operations_before_tax = ?, " +
                    "Taxes = ?,Income_from_operations_after_tax = ?,Extraordinary_income_or_costs = ?,Result_before_income_from_associated_companies = ?,Income_from_affiliates = ?,Income_from_subsidiaries = ?, Income_from_associated_companies = ?,Net_profit = ? " +
                    "WHERE AFS_id = ?";
                //	myOleDbCommand.Transaction = myTrans;
                myOleDbCommand.Parameters.Clear();
                OleDbParameter myParam = new OleDbParameter("Small_status", OleDbType.WChar);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = theFinStatement.SmallStatus.ToString();
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Revenue_from_main_operations", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.RevenueFromMainOperation != int.MinValue) {
                    myParam.Value = theFinStatement.RevenueFromMainOperation;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_operating_revenue", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherOperationRevenue != int.MinValue) {
                    myParam.Value = theFinStatement.OtherOperationRevenue;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_revenue", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherOperationRevenue != int.MinValue) {
                    myParam.Value = theFinStatement.TotalRevenue;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cost_of_sales", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CostOfSales != int.MinValue) {
                    myParam.Value = theFinStatement.CostOfSales;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Administrative_and_other_expenses", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.AdministrativeAndOtherExpenses != int.MinValue) {
                    myParam.Value = theFinStatement.AdministrativeAndOtherExpenses;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Distribution_costs", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.DistributionCost != int.MinValue) {
                    myParam.Value = theFinStatement.DistributionCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_costs", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalCost != int.MinValue) {
                    myParam.Value = theFinStatement.TotalCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Staff_costs", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.StaffCost != int.MinValue) {
                    myParam.Value = theFinStatement.StaffCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Deprication_of_fixed_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.DepricationOfFixedAssets != int.MinValue) {
                    myParam.Value = theFinStatement.DepricationOfFixedAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Operating_profit", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OperationalProfit != int.MinValue) {
                    myParam.Value = theFinStatement.OperationalProfit;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Financial_income", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.FinancialIncome != int.MinValue) {
                    myParam.Value = theFinStatement.FinancialIncome;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Financial_costs", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.FinancialCost != int.MinValue) {
                    myParam.Value = theFinStatement.FinancialCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_financial_items", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherFinancialEntries != int.MinValue) {
                    myParam.Value = theFinStatement.OtherFinancialEntries;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Net_financial_items", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.NetFinancialItems != int.MinValue) {
                    myParam.Value = theFinStatement.NetFinancialItems;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_operations_before_tax", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromOperationsBeforeTax != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromOperationsBeforeTax;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Taxes", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeTax != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeTax;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_operations_after_tax", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromOperationsAfterTax != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromOperationsAfterTax;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Extraordinary_income_or_costs", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ExtraOrdinaryIncomeCost != int.MinValue) {
                    myParam.Value = theFinStatement.ExtraOrdinaryIncomeCost;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Result_before_income_from_associated_companies", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ResultBeforeIncomeFromAssociatedCompanies != int.MinValue) {
                    myParam.Value = theFinStatement.ResultBeforeIncomeFromAssociatedCompanies;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_affiliates", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromAffiliates != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromAffiliates;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_subsidiaries", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromSubsidiaries != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromSubsidiaries;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Income_from_associated_companies", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncomeFromAssociatedCompanies != int.MinValue) {
                    myParam.Value = theFinStatement.IncomeFromAssociatedCompanies;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Net_profit", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.NetProfit != int.MinValue) {
                    myParam.Value = theFinStatement.NetProfit;
                }
                myOleDbCommand.Parameters.Add(myParam);

                // where parameterinn ...
                myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = theFinStatement.AFS_id;
                myOleDbCommand.Parameters.Add(myParam);

                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                throw (new ApplicationException(err.Message));
            }
            return true;
        }

        private bool UpdateBalanceSheet(FinancialStatementBLLC theFinStatement, OleDbCommand myOleDbCommand) {
            String funcName = "UpdateBalanceSheet(FinancialStatementBLLC theFinStatement,OleDbCommand myOleDbCommand)";
            try {
                /*
				  AFS_id,Intangible_assets,Tangible_assets,Financial_assets,"+
				"Total_fixed_assets,Inventory,Receivables_and_debtors,Claims_towards_associtaed_companies,Short_term_investments,"+
				"Cash,Deferred_tax_assets,Other_current_assets,Total_current_assets,Total_assets,Issued_share_capital,Profit_and_loss_account,"+
				"Other_equity,Total_equity,Pension_funds_liabilities,Tax_liabilities,Other_liabilities,Total_liabilities,Long_term_debts,"+
				"Loans_overdraft,Creditors,Debts_towards_associated_companies,Current_portion_of_long_term_liabilities,Other_short_term_debt,"+
				"Current_assets,Total_short_term_debt,Current_assets_less_current_liabilities,Total_debts_and_equity
				*/
                myOleDbCommand.CommandText =
                    "UPDATE FSI_BalanceSheet SET Intangible_assets = ?,Tangible_assets = ?,Financial_assets = ?,Total_fixed_assets = ?,Inventory = ?,Receivables_and_debtors = ?, Claims_towards_associated_companies = ?, Short_term_investments = ?, " +
                    "Cash = ?,Deferred_tax_assets = ?,Other_current_assets = ?,Total_current_assets = ?,Total_assets = ?,Issued_share_capital = ?,Profit_and_loss_account = ?,Other_equity = ?, " +
                    "Total_equity = ?,Pension_funds_liabilities = ?,Tax_liabilities = ?,Other_liabilities = ?,Total_liabilities = ?,Long_term_debts = ?, Loans_overdraft = ?,Creditors = ?, " +
                    "Debts_towards_associated_companies = ?,Current_portion_of_long_term_liabilities = ?,Other_short_term_debt = ?,Current_assets = ?,Total_short_term_debt = ?,Current_assets_less_current_liabilities = ?, Total_debts_and_equity = ?, Minority_holdings_in_equity = ? " +
                    "WHERE AFS_id = ?";

                myOleDbCommand.Parameters.Clear();
                OleDbParameter myParam = new OleDbParameter("Intangible_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IntangableAssets != int.MinValue) {
                    myParam.Value = theFinStatement.IntangableAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Tangible_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TangableAssets != int.MinValue) {
                    myParam.Value = theFinStatement.TangableAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Financial_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.FinancialAssets != int.MinValue) {
                    myParam.Value = theFinStatement.FinancialAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_fixed_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalFixedAssets != int.MinValue) {
                    myParam.Value = theFinStatement.TotalFixedAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Inventory", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.Inventory != int.MinValue) {
                    myParam.Value = theFinStatement.Inventory;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Receivables_and_debtors", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.Debtors != int.MinValue) {
                    myParam.Value = theFinStatement.Debtors;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Claims_towards_associated_companies", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ClaimsTowardsAssociatedCompanies != int.MinValue) {
                    myParam.Value = theFinStatement.ClaimsTowardsAssociatedCompanies;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Short_term_investments", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ShortTermInvestments != int.MinValue) {
                    myParam.Value = theFinStatement.ShortTermInvestments;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cash", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CashAtBankAndInHand != int.MinValue) {
                    myParam.Value = theFinStatement.CashAtBankAndInHand;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Deferred_tax_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.DeferredTaxAsset != int.MinValue) {
                    myParam.Value = theFinStatement.DeferredTaxAsset;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_current_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherCurrentAssets != int.MinValue) {
                    myParam.Value = theFinStatement.OtherCurrentAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_current_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalCurrentAssets != int.MinValue) {
                    myParam.Value = theFinStatement.TotalCurrentAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalAssets != int.MinValue) {
                    myParam.Value = theFinStatement.TotalAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Issued_share_capital", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IssuedShareCapital != int.MinValue) {
                    myParam.Value = theFinStatement.IssuedShareCapital;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Profit_and_loss_account", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.ProfitAndLossAccount != int.MinValue) {
                    myParam.Value = theFinStatement.ProfitAndLossAccount;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_equity", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherEquity != int.MinValue) {
                    myParam.Value = theFinStatement.OtherEquity;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_equity", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalEquity != int.MinValue) {
                    myParam.Value = theFinStatement.TotalEquity;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Pension_funds_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.PensionFundsLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.PensionFundsLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Tax_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TaxLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.TaxLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.OtherLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.TotalLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Long_term_debts", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.LongTermDebts != int.MinValue) {
                    myParam.Value = theFinStatement.LongTermDebts;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Loans_overdraft", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.LoansOverdraft != int.MinValue) {
                    myParam.Value = theFinStatement.LoansOverdraft;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Creditors", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.Creditors != int.MinValue) {
                    myParam.Value = theFinStatement.Creditors;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Debts_towards_associated_companies", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.DebtToAssociatedCompanies != int.MinValue) {
                    myParam.Value = theFinStatement.DebtToAssociatedCompanies;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Current_portion_of_long_term_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CurrentPortionOfLongTermLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.CurrentPortionOfLongTermLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Other_short_term_debt", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OtherShortTermDebt != int.MinValue) {
                    myParam.Value = theFinStatement.OtherShortTermDebt;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Current_assets", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CurrentAssets != int.MinValue) {
                    myParam.Value = theFinStatement.CurrentAssets;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_short_term_debt", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalShortTermDebts != int.MinValue) {
                    myParam.Value = theFinStatement.TotalShortTermDebts;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Current_assets_less_current_liabilities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CurrentAssetsLessCurrentLiabilities != int.MinValue) {
                    myParam.Value = theFinStatement.CurrentAssetsLessCurrentLiabilities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Total_debts_and_equity", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.TotalDebtsEquity != int.MinValue) {
                    myParam.Value = theFinStatement.TotalDebtsEquity;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Minority_holdings_in_equity", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.MinorityHoldingsInEquity != int.MinValue) {
                    myParam.Value = theFinStatement.MinorityHoldingsInEquity;
                }
                myOleDbCommand.Parameters.Add(myParam);

                // where parameterinn ...
                myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = theFinStatement.AFS_id;
                myOleDbCommand.Parameters.Add(myParam);
                // Execute ...
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                throw (new ApplicationException(err.Message));
            }
            return true;
        }

        private bool UpdateCashflow(FinancialStatementBLLC theFinStatement, OleDbCommand myOleDbCommand) {
            String funcName = "UpdateCashflow(FinancialStatementBLLC theFinStatement,OleDbCommand myOleDbCommand)";
            // hmm � einhverjum tilfellum er ekki til neitt � Cash Flow �� �arf a� inserta � sta� update! (090904)
            if (!checkStatementsCashFlow(theFinStatement.AFS_id)) {
                return InsertCashflow(theFinStatement, myOleDbCommand);
            }
            try {
                /*
				  INSERT INTO FSI_Cashflow(AFS_id,Operating_profit_before_working_capital_changes,Cash_from_operations,Net_cash_from_operating_activities,Cash_flow_from_investing_activities,Cash_flow_from_financing_activities,Increase_or_decrease_in_cash
				*/
                myOleDbCommand.CommandText =
                    "UPDATE FSI_Cashflow SET Operating_profit_before_working_capital_changes = ?,Cash_from_operations = ?,Net_cash_from_operating_activities = ?,Cash_flow_from_investing_activities = ?,Cash_flow_from_financing_activities = ?,Increase_or_decrease_in_cash = ? " +
                    "WHERE AFS_id = ?";

                myOleDbCommand.Parameters.Clear();
                OleDbParameter myParam = new OleDbParameter(
                    "Operating_profit_before_working_capital_changes", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.OperatingProfitBeforeWorkingCapitalChanges != int.MinValue) {
                    myParam.Value = theFinStatement.OperatingProfitBeforeWorkingCapitalChanges;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cash_from_operations", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CashFromOperations != int.MinValue) {
                    myParam.Value = theFinStatement.CashFromOperations;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Net_cash_from_operating_activities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.CashProvidedByOperatingActivities != int.MinValue) {
                    myParam.Value = theFinStatement.CashProvidedByOperatingActivities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cash_flow_from_investing_activities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.InvestingActivities != int.MinValue) {
                    myParam.Value = theFinStatement.InvestingActivities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Cash_flow_from_financing_activities", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.FinancialActivities != int.MinValue) {
                    myParam.Value = theFinStatement.FinancialActivities;
                }
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Increase_or_decrease_in_cash", OleDbType.Integer);
                myParam.IsNullable = true;
                myParam.Direction = ParameterDirection.Input;
                if (theFinStatement.IncreaseDecreaseInCashAndCashInHand != int.MinValue) {
                    myParam.Value = theFinStatement.IncreaseDecreaseInCashAndCashInHand;
                }
                myOleDbCommand.Parameters.Add(myParam);

                // where parameterinn ...
                myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = theFinStatement.AFS_id;
                myOleDbCommand.Parameters.Add(myParam);
                // Execute ...
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                throw (new ApplicationException(err.Message));
            }
            return true;
        }

        private bool checkStatementsCashFlow(int AFS_id) {
            /*	String funcName = "checkStatementsCashFlow(int AFS_id,OleDbCommand myOleDbCommand)";
			// hmm � einhverjum tilfellum er ekki til neitt � Cash Flow �� �arf a� inserta � sta� update! (090904)
			
			try 
			{
				myOleDbCommand.CommandText = "SELECT AFS_id FROM FSI_Cashflow " +
					"WHERE AFS_id = ?";
				
				myOleDbCommand.Parameters.Clear();
				
				// where parameterinn ...
				OleDbParameter myParam = new OleDbParameter("AFS_id", OleDbType.Integer);
				myParam.Direction = ParameterDirection.Input;
				myParam.Value = AFS_id;
				myOleDbCommand.Parameters.Add(myParam);
				// Execute ...
				OleDbDataReader reader = myOleDbCommand.ExecuteReader();
				if(reader.Read()) 
				{
					return true;
				}
			}
			catch(Exception err)
			{
				Logger.WriteToLog(className +" : " + funcName + err.Message,true);
				throw(new ApplicationException(err.Message));
			}
	
			return false;
*/

            string funcName = "checkStatementsCashFlow(int AFS_id)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand("SELECT AFS_id FROM FSI_Cashflow WHERE AFS_id = " + AFS_id + "", myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return true;
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    return false;
                }
            }
            return false;
        }

        public bool IsStatementRegisted(int companyCIID, int year, int accountPeriod) {
            string funcName = "IsStatementRegisted(int companyCIID, int year)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM FSI_TemplateGeneralInfo WHERE CreditInfoID = " + companyCIID +
                            " AND Financial_year = " + year + " AND Deleted = 'False' AND Account_period_length = " +
                            accountPeriod + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return true;
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    return false;
                }
            }
            return false;
        }

    } // END CLASS DEFINITION FinancialStatementDALC
}