#region

using System;
using System.Reflection;
using System.Resources;
using System.Web;

#endregion

namespace FSI.Localization {
    /// <summary>
    /// Summary description for CIResources.
    /// </summary>
    public class CIResource {
        private const string _baseResourceFile = "cb3.FSI.resources.strings";

        /// <summary>
        /// Summary description for CIResource.
        /// </summary>
        private const string ApplicationVariableName = "FSI";

        //private string myTest;
        /// <summary>
        /// <para>The CurrentManager property returns and caches the shared ResourceManager instance.
        /// </para>
        /// </summary>
        public static ResourceManager CurrentManager {
            get {
                var context = HttpContext.Current;
                if (null == context) {
                    throw new ArgumentException("Global_NoHttpContext");
                }
                var mgr = context.Cache[ApplicationVariableName] as ResourceManager;

                if (null == mgr) {
                    mgr = new ResourceManager(_baseResourceFile, Assembly.GetExecutingAssembly(), null);

                    // Add to the cache
                    context.Cache.Insert(ApplicationVariableName, mgr);
                }
                return mgr;
            }
        }
    }
}