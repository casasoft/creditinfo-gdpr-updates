#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FSI.BLL;
using FSI.Localization;
using UserAdmin.BLL.CIUsers;

#endregion

using Cig.Framework.Base.Configuration;

namespace FSI {
    /// <summary>
    /// Summary description for StatementInput.
    /// </summary>
    public class StatementInput : Page {
        private static readonly FinancialStatementFactory theStatementFact = new FinancialStatementFactory();
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected HtmlTableCell blas;
        protected Button btnCancel;
        protected Button btnSubmit;
        protected CheckBox chbConsolidated;
        protected CheckBox chbQualifiedAudit;
        protected CheckBox chbReadyForWebPublish;
        protected CheckBox chbSmallStatus;
        protected CustomValidator CustomValidator1;
        protected DropDownList ddComments;
        protected DropDownList ddCurrency;
        protected DropDownList ddDenomination;
        protected DropDownList ddSource;
        protected DropDownList ddTotalMonths;
        protected Label lblAccountPeriodLength;
        protected Label lblAdministrativeAndOtherExpenses;
        protected Label lblBalanceSheetAssets;
        protected Label lblBalanceSheetEquityAndDebt;
        protected Label lblCashFlow;
        protected Label lblCashFromOperations;
        protected Label lblCashInBankHand;
        protected Label lblCashprovidedByOperatingActivities;
        protected Label lblClaimsTowardsAssociatedCompanies;
        protected Label lblComments;
        protected Label lblCompanyAddress;
        protected Label lblCompanyCity;
        protected Label lblCompanyFunction;
        protected Label lblCompanyName;
        protected Label lblCompanyNationalID;
        protected Label lblConsolidated;
        protected Label lblCostOfSales;
        protected Label lblCosts;
        protected Label lblCreditors;
        protected Label lblCurrency;
        protected Label lblCurrentAssets;
        protected Label lblCurrentAssetsLessCurrentLiabilities;
        protected Label lblDebtors;
        protected Label lblDebtToAssociatedCompanies;
        protected Label lblDeferredTaxAsset;
        protected Label lblDenomination;
        protected Label lblDepricationOfFixedAssets;
        protected Label lblDereferedTaxations;
        protected Label lblDistributionCost;
        protected Label lblEquity;
        protected Label lblExtraOrdinaryIncomeCost;
        protected Label lblFinancialActivities;
        protected Label lblFinancialAssets;
        protected Label lblFinancialCost;
        protected Label lblFinancialIncome;
        protected Label lblFinancialStatement;
        protected Label lblFinIncomeAndCosts;
        protected Label lblFiscalYear;
        protected Label lblFixedAssets;
        protected Label lblIncomeFromAffiliates;
        protected Label lblIncomeFromAssociatedCompanies;
        protected Label lblIncomeFromOperationsAfterTax;
        protected Label lblIncomeFromOperationsBeforeTax;
        protected Label lblIncomeFromSubsidiaries;
        protected Label lblIncomeSheet;
        protected Label lblIncomeTax;
        protected Label lblInDecreaseInCashAndInHand;
        protected Label lblIntangibleAssets;
        protected Label lblInventory;
        protected Label lblInvestingActivities;
        protected Label lblIssuedShareCapital;
        protected Label lblLiabilities;
        protected Label lblLoansOverdraft;
        protected Label lblLongTermDebt;
        protected Label lblLongTermDebts;
        protected Label lblMinorityHoldingsInEquity;
        protected Label lblMostRecentAccount;
        protected Label lblMsg;
        protected Label lblNetFinancialItems;
        protected Label lblNetProfit;
        protected Label lblNetProfitHead;
        protected Label lblNextYearPayment;
        protected Label lblOperatingProfitBeforeWorkingCapitalChanges;
        protected Label lblOperationalProfit;
        protected Label lblOtherCurrentAssets;
        protected Label lblOtherEquity;
        protected Label lblOtherFields;
        protected Label lblOtherFinancialEntries;
        protected Label lblOtherLiabilities;
        protected Label lblOtherOperationRevenue;
        protected Label lblOtherShortTermDebt;
        protected Label lblPageTitle;
        protected Label lblPensionFundsLiabilities;
        protected Label lblProfitAndLossAccount;
        protected Label lblQualifiedAudit;
        protected Label lblReadyForWebPublish;
        protected Label lblResultBeforeIncomeFromAssociatedCompanies;
        protected Label lblRevenue;
        protected Label lblRevenueFromMainOperations;
        protected Label lblShortTermDebts;
        protected Label lblShortTermInvestments;
        protected Label lblSmallStatus;
        protected Label lblSource;
        protected Label lblStaffCost;
        protected Label lblStaffCount;
        protected Label lblTangibleAssets;
        protected Label lblTaxLiabilities;
        protected Label lblTotalAssets;
        protected Label lblTotalAssetsHead;
        protected Label lblTotalCost;
        protected Label lblTotalCurrentAssets;
        protected Label lblTotalDebtsAndEquity;
        protected Label lblTotalDebtsAndEquityHead;
        protected Label lblTotalDebtsHead;
        protected Label lblTotalEquity;
        protected Label lblTotalFixedAssets;
        protected Label lblTotalLiabilities;
        protected Label lblTotalRevenue;
        protected Label lblTotalShortTermDebt;
        protected Label lblYearEnded;
        private bool nativeCult;
        protected TextBox tbAdministrativeAndOtherExpenses;
        protected TextBox tbAFS_id;
        protected TextBox tbCashFromOperations;
        protected TextBox tbCashInBankHand;
        protected TextBox tbCashprovidedByOperatingActivities;
        protected TextBox tbClaimsTowardsAssociatedCompanies;
        protected TextBox tbCostOfSales;
        protected TextBox tbCreditors;
        protected TextBox tbCurrentAssetsLessCurrentLiabilities;
        protected TextBox tbDebtors;
        protected TextBox tbDebtToAssociatedCompanies;
        protected TextBox tbDeferredTaxAsset;
        protected TextBox tbDepricationOfFixedAssets;
        protected TextBox tbDistributionCost;
        protected TextBox tbExtraOrdinaryIncomeCost;
        protected TextBox tbFinancialActivities;
        protected TextBox tbFinancialAssets;
        protected TextBox tbFinancialCost;
        protected TextBox tbFinancialIncome;
        protected TextBox tbFiscalYear;
        protected TextBox tbIncomeFromAffiliates;
        protected TextBox tbIncomeFromAssociatedCompanies;
        protected TextBox tbIncomeFromOperationsAfterTax;
        protected TextBox tbIncomeFromOperationsBeforeTax;
        protected TextBox tbIncomeFromSubsidiaries;
        protected TextBox tbIncomeTax;
        protected TextBox tbInDecreaseInCashAndInHand;
        protected TextBox tbIntangibleAssets;
        protected TextBox tbInventory;
        protected TextBox tbInvestingActivities;
        protected TextBox tbIssuedShareCapital;
        protected TextBox tbLoansOverdraft;
        protected TextBox tbLongTermDebts;
        protected TextBox tbMinorityHoldingsInEquity;
        protected TextBox tbNetFinancialItems;
        protected TextBox tbNetProfit;
        protected TextBox tbNextYearPayment;
        protected TextBox tbOperatingProfitBeforeWorkingCapitalChanges;
        protected TextBox tbOperationalProfit;
        protected TextBox tbOtherCurrentAssets;
        protected TextBox tbOtherEquity;
        protected TextBox tbOtherFinancialEntries;
        protected TextBox tbOtherLiabilities;
        protected TextBox tbOtherOperationRevenue;
        protected TextBox tbOtherShortTermDebt;
        protected TextBox tbPensionFundsLiabilities;
        protected TextBox tbProfitAndLossAccount;
        protected TextBox tbResultBeforeIncomeFromAssociatedCompanies;
        protected TextBox tbRevenueFromMainOperation;
        protected TextBox tbShortTermInvestments;
        protected TextBox tbStaffCost;
        protected TextBox tbStaffCount;
        protected TextBox tbTangibleAssets;
        protected TextBox tbTaxLiabilities;
        protected TextBox tbTotalAssets;
        protected TextBox tbTotalCost;
        protected TextBox tbTotalCurrentAssets;
        protected TextBox tbTotalDebtsAndEquity;
        protected TextBox tbTotalEquity;
        protected TextBox tbTotalFixedAssets;
        protected TextBox tbTotalLiabilities;
        protected TextBox tbTotalRevenue;
        protected TextBox tbTotalShortTermDebt;
        protected TextBox tbYearEnded;
        private FinancialStatementBLLC theStatement;
        protected HtmlTableRow trDeferredTaxAsset;
        protected HtmlTableRow trDistributionCost;
        protected HtmlTableRow trProfitAndLossAccount;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            // check the current culture
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            int year = 0;
            int accountPeriod = 0;
            if (!IsPostBack) {
                //	btnSubmit.Attributes ["onclick"] = "return senda();";
                //Hide rows that are not included in first version
                trDistributionCost.Visible = false;
                trDeferredTaxAsset.Visible = false;
                trProfitAndLossAccount.Visible = false;

                LocalizeText();
                InitBoxes();
                if (Session["FSI_tbCompanyRegNo"] != null) {
                    // n� � ID og YEAR params
                    string nationalID = Session["FSI_tbCompanyRegNo"].ToString();
                    Session["FISCNationalID"] = nationalID;
                    year = Convert.ToInt32(Session["FSI_year"].ToString());
                    accountPeriod = Convert.ToInt32(Session["FSI_ddAccountPeriod"].ToString());
                    // athuga hvort �rsreikningur s� til
                    // �arf a� skipta �t nationalID fyrir CIID ... e�a hva�?
                    int companyCIID = theStatementFact.GetCIIDByNationalID(nationalID);
                    // ef fyrirt�ki� er ekki til � kerfinu � �a� �.a.l. ekki �rsreikning
                    if (companyCIID >= 0) {
                        //S�kja uppl�singar um fyrirt�ki�
                        SetCompanyInfo(companyCIID);
                        tbFiscalYear.Text = year.ToString();
                        try {
                            ddTotalMonths.SelectedValue = accountPeriod.ToString();
                        } catch {}

                        // t�kka h�r lika ...
                        if (theStatementFact.IsStatementRegisted(companyCIID, year, accountPeriod)) {
                            theStatement = theStatementFact.GetFinancialStatement(companyCIID, year, accountPeriod);
                            if (!(string.IsNullOrEmpty(theStatement.FiscalYear))) {
                                // �rsreikningurinn er til ... fylla sv��i
                                tbAFS_id.Text = Convert.ToString(theStatement.AFS_id);
                                SetFieldValues(theStatement);
                                tbFiscalYear.Enabled = false;
                                btnSubmit.Text = rm.GetString("txtUpdate");
                                btnSubmit.CommandName = "Update";
                            }
                        }
                    } else {
                        Server.Transfer("SelectStatement.aspx");
                    }
                }
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            // h�r draga gildi �r innsl�ttarsv��um og henda � BLLC klasann og vista
            if (Page.IsValid) {
                InsertStatementValues();
            }
        }

        private void InsertStatementValues() {
            lblMsg.Visible = false;
            theStatement = new FinancialStatementBLLC();
            var nationalID = (string) Session["FISCNationalID"];
            theStatement.CompanyCIID = theStatementFact.GetCIIDByNationalID(nationalID);

            // Haus
            if (tbFiscalYear.Text != "") {
                theStatement.FiscalYear = tbFiscalYear.Text;
            }
            theStatement.AccountMonths = Convert.ToInt32(ddTotalMonths.SelectedValue);
            IFormatProvider format = CultureInfo.CurrentCulture;
            if (tbYearEnded.Text != "") {
                theStatement.YearEnded = DateTime.Parse(tbYearEnded.Text, format, DateTimeStyles.AllowWhiteSpaces);
            }
            theStatement.CurrencyID = ddCurrency.SelectedValue;
            theStatement.Consolidated = chbConsolidated.Checked;
            theStatement.DenominationID = Convert.ToInt32(ddDenomination.SelectedValue);
            theStatement.OriginID = Convert.ToInt32(ddSource.SelectedValue);
            // Other fields
            theStatement.Qualified = chbQualifiedAudit.Checked;
            theStatement.SmallStatus = chbSmallStatus.Checked;
            theStatement.ReadyForWebPublishing = chbReadyForWebPublish.Checked;
            theStatement.CommentsID = Convert.ToInt32(ddComments.SelectedValue);
            /*if(this.tbComments.Text != "")
				theStatement.Comments = this.tbComments.Text;*/
            if (tbStaffCount.Text != "") {
                theStatement.StaffCount = Convert.ToInt32(tbStaffCount.Text);
            }

            // Income sheet
            if (tbRevenueFromMainOperation.Text != "") {
                theStatement.RevenueFromMainOperation = Convert.ToInt32(tbRevenueFromMainOperation.Text);
            }
            if (tbOtherOperationRevenue.Text != "") {
                theStatement.OtherOperationRevenue = Convert.ToInt32(tbOtherOperationRevenue.Text);
            }
            if (tbTotalRevenue.Text != "") {
                theStatement.TotalRevenue = Convert.ToInt32(tbTotalRevenue.Text);
            }
            if (tbCostOfSales.Text != "") {
                theStatement.CostOfSales = Convert.ToInt32(tbCostOfSales.Text);
            }
            if (tbAdministrativeAndOtherExpenses.Text != "") {
                theStatement.AdministrativeAndOtherExpenses = Convert.ToInt32(tbAdministrativeAndOtherExpenses.Text);
            }
            if (tbDistributionCost.Text != "") {
                theStatement.DistributionCost = Convert.ToInt32(tbDistributionCost.Text);
            }
            if (tbTotalCost.Text != "") {
                theStatement.TotalCost = Convert.ToInt32(tbTotalCost.Text);
            }
            if (tbStaffCost.Text != "") {
                theStatement.StaffCost = Convert.ToInt32(tbStaffCost.Text);
            }
            if (tbDepricationOfFixedAssets.Text != "") {
                theStatement.DepricationOfFixedAssets = Convert.ToInt32(tbDepricationOfFixedAssets.Text);
            }
            if (tbOperationalProfit.Text != "") {
                theStatement.OperationalProfit = Convert.ToInt32(tbOperationalProfit.Text);
            }

            if (tbFinancialIncome.Text != "") {
                theStatement.FinancialIncome = Convert.ToInt32(tbFinancialIncome.Text);
            }
            if (tbFinancialCost.Text != "") {
                theStatement.FinancialCost = Convert.ToInt32(tbFinancialCost.Text);
            }
            if (tbOtherFinancialEntries.Text != "") {
                theStatement.OtherFinancialEntries = Convert.ToInt32(tbOtherFinancialEntries.Text);
            }
            if (tbNetFinancialItems.Text != "") {
                theStatement.NetFinancialItems = Convert.ToInt32(tbNetFinancialItems.Text);
            }
            if (tbIncomeFromOperationsBeforeTax.Text != "") {
                theStatement.IncomeFromOperationsBeforeTax = Convert.ToInt32(tbIncomeFromOperationsBeforeTax.Text);
            }
            if (tbIncomeTax.Text != "") {
                theStatement.IncomeTax = Convert.ToInt32(tbIncomeTax.Text);
            }
            if (tbIncomeFromOperationsAfterTax.Text != "") {
                theStatement.IncomeFromOperationsAfterTax = Convert.ToInt32(tbIncomeFromOperationsAfterTax.Text);
            }
            if (tbExtraOrdinaryIncomeCost.Text != "") {
                theStatement.ExtraOrdinaryIncomeCost = Convert.ToInt32(tbExtraOrdinaryIncomeCost.Text);
            }
            if (tbResultBeforeIncomeFromAssociatedCompanies.Text != "") {
                theStatement.ResultBeforeIncomeFromAssociatedCompanies =
                    Convert.ToInt32(tbResultBeforeIncomeFromAssociatedCompanies.Text);
            }
            if (tbIncomeFromAffiliates.Text != "") {
                theStatement.IncomeFromAffiliates = Convert.ToInt32(tbIncomeFromAffiliates.Text);
            }
            if (tbIncomeFromSubsidiaries.Text != "") {
                theStatement.IncomeFromSubsidiaries = Convert.ToInt32(tbIncomeFromSubsidiaries.Text);
            }
            if (tbIncomeFromAssociatedCompanies.Text != "") {
                theStatement.IncomeFromAssociatedCompanies = Convert.ToInt32(tbIncomeFromAssociatedCompanies.Text);
            }
            if (tbNetProfit.Text != "") {
                theStatement.NetProfit = Convert.ToInt32(tbNetProfit.Text);
            }

            // Balance sheet - ASSETS
            if (tbIntangibleAssets.Text != "") {
                theStatement.IntangableAssets = Convert.ToInt32(tbIntangibleAssets.Text);
            }
            if (tbTangibleAssets.Text != "") {
                theStatement.TangableAssets = Convert.ToInt32(tbTangibleAssets.Text);
            }
            if (tbFinancialAssets.Text != "") {
                theStatement.FinancialAssets = Convert.ToInt32(tbFinancialAssets.Text);
            }
            if (tbTotalFixedAssets.Text != "") {
                theStatement.TotalFixedAssets = Convert.ToInt32(tbTotalFixedAssets.Text);
            }
            if (tbInventory.Text != "") {
                theStatement.Inventory = Convert.ToInt32(tbInventory.Text);
            }
            if (tbDebtors.Text != "") {
                theStatement.Debtors = Convert.ToInt32(tbDebtors.Text);
            }
            if (tbClaimsTowardsAssociatedCompanies.Text != "") {
                theStatement.ClaimsTowardsAssociatedCompanies = Convert.ToInt32(tbClaimsTowardsAssociatedCompanies.Text);
            }
            if (tbShortTermInvestments.Text != "") {
                theStatement.ShortTermInvestments = Convert.ToInt32(tbShortTermInvestments.Text);
            }
            if (tbCashInBankHand.Text != "") {
                theStatement.CashAtBankAndInHand = Convert.ToInt32(tbCashInBankHand.Text);
            }
            if (tbDeferredTaxAsset.Text != "") {
                theStatement.DeferredTaxAsset = Convert.ToInt32(tbDeferredTaxAsset.Text);
            }
            if (tbOtherCurrentAssets.Text != "") {
                theStatement.OtherCurrentAssets = Convert.ToInt32(tbOtherCurrentAssets.Text);
            }
            if (tbTotalCurrentAssets.Text != "") {
                theStatement.TotalCurrentAssets = Convert.ToInt32(tbTotalCurrentAssets.Text);
            }
            if (tbTotalAssets.Text != "") {
                theStatement.TotalAssets = Convert.ToInt32(tbTotalAssets.Text);
            }

            // Balance sheet - Equity and Debt
            if (tbIssuedShareCapital.Text != "") {
                theStatement.IssuedShareCapital = Convert.ToInt32(tbIssuedShareCapital.Text);
            }
            if (tbProfitAndLossAccount.Text != "") {
                theStatement.ProfitAndLossAccount = Convert.ToInt32(tbProfitAndLossAccount.Text);
            }
            if (tbOtherEquity.Text != "") {
                theStatement.OtherEquity = Convert.ToInt32(tbOtherEquity.Text);
            }
            if (tbTotalEquity.Text != "") {
                theStatement.TotalEquity = Convert.ToInt32(tbTotalEquity.Text);
            }
            if (tbMinorityHoldingsInEquity.Text != "") {
                theStatement.MinorityHoldingsInEquity = Convert.ToInt32(tbMinorityHoldingsInEquity.Text);
            }
            if (tbPensionFundsLiabilities.Text != "") {
                theStatement.PensionFundsLiabilities = Convert.ToInt32(tbPensionFundsLiabilities.Text);
            }
            if (tbTaxLiabilities.Text != "") {
                theStatement.TaxLiabilities = Convert.ToInt32(tbTaxLiabilities.Text);
            }
            if (tbOtherLiabilities.Text != "") {
                theStatement.OtherLiabilities = Convert.ToInt32(tbOtherLiabilities.Text);
            }
            if (tbTotalLiabilities.Text != "") {
                theStatement.TotalLiabilities = Convert.ToInt32(tbTotalLiabilities.Text);
            }
            if (tbLongTermDebts.Text != "") {
                theStatement.LongTermDebts = Convert.ToInt32(tbLongTermDebts.Text);
            }
            if (tbLoansOverdraft.Text != "") {
                theStatement.LoansOverdraft = Convert.ToInt32(tbLoansOverdraft.Text);
            }
            if (tbCreditors.Text != "") {
                theStatement.Creditors = Convert.ToInt32(tbCreditors.Text);
            }
            if (tbDebtToAssociatedCompanies.Text != "") {
                theStatement.DebtToAssociatedCompanies = Convert.ToInt32(tbDebtToAssociatedCompanies.Text);
            }
            if (tbNextYearPayment.Text != "") {
                theStatement.CurrentPortionOfLongTermLiabilities = Convert.ToInt32(tbNextYearPayment.Text);
            }
            if (tbOtherShortTermDebt.Text != "") {
                theStatement.OtherShortTermDebt = Convert.ToInt32(tbOtherShortTermDebt.Text);
            }
            if (tbTotalShortTermDebt.Text != "") {
                theStatement.TotalShortTermDebts = Convert.ToInt32(tbTotalShortTermDebt.Text);
            }
            if (tbCurrentAssetsLessCurrentLiabilities.Text != "") {
                theStatement.CurrentAssetsLessCurrentLiabilities =
                    Convert.ToInt32(tbCurrentAssetsLessCurrentLiabilities.Text);
            }
            if (tbTotalDebtsAndEquity.Text != "") {
                theStatement.TotalDebtsEquity = Convert.ToInt32(tbTotalDebtsAndEquity.Text);
            }

            // Cash Flow
            if (tbOperatingProfitBeforeWorkingCapitalChanges.Text != "") {
                theStatement.OperatingProfitBeforeWorkingCapitalChanges =
                    Convert.ToInt32(tbOperatingProfitBeforeWorkingCapitalChanges.Text);
            }
            if (tbCashFromOperations.Text != "") {
                theStatement.CashFromOperations = Convert.ToInt32(tbCashFromOperations.Text);
            }
            if (tbCashprovidedByOperatingActivities.Text != "") {
                theStatement.CashProvidedByOperatingActivities =
                    Convert.ToInt32(tbCashprovidedByOperatingActivities.Text);
            }
            if (tbInvestingActivities.Text != "") {
                theStatement.InvestingActivities = Convert.ToInt32(tbInvestingActivities.Text);
            }
            if (tbFinancialActivities.Text != "") {
                theStatement.FinancialActivities = Convert.ToInt32(tbFinancialActivities.Text);
            }
            if (tbInDecreaseInCashAndInHand.Text != "") {
                theStatement.IncreaseDecreaseInCashAndCashInHand = Convert.ToInt32(tbInDecreaseInCashAndInHand.Text);
            }

            theStatement.Modified = DateTime.Now;

            if (btnSubmit.CommandName != "Update") {
                if (theStatementFact.AddStatement(theStatement)) {
                    lblMsg.Text = rm.GetString("txtStatementRegistered", ci);
                    lblMsg.ForeColor = Color.Black;
                    lblMsg.Visible = true;
                } else {
                    lblMsg.Text = rm.GetString("txtStatementRegisteredFailed", ci);
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Visible = true;
                }
            } else {
                if (tbAFS_id.Text != "") {
                    theStatement.AFS_id = Convert.ToInt32(tbAFS_id.Text);
                }

                if (theStatementFact.UpdateFinancialStatement(theStatement)) {
                    lblMsg.Text = rm.GetString("txtStatementUpdated", ci);
                    lblMsg.ForeColor = Color.Black;
                    lblMsg.Visible = true;
                } else {
                    lblMsg.Text = rm.GetString("txtStatementUpdatedFailed", ci);
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Visible = true;
                }
            }
        }

        private void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtFinancialStatementInputPage", ci);
            lblFinancialStatement.Text = rm.GetString("txtFinancialStatement", ci);
            lblMostRecentAccount.Text = rm.GetString("txtMostRecentAccount", ci);
            lblFiscalYear.Text = rm.GetString("txtFiscalYear", ci);
            lblYearEnded.Text = rm.GetString("txtYearEnded", ci);
            lblAccountPeriodLength.Text = rm.GetString("txtAccountPeriodLength", ci);
            lblCurrency.Text = rm.GetString("txtCurrency", ci);
            lblConsolidated.Text = rm.GetString("txtConsolidated", ci);
            lblDenomination.Text = rm.GetString("txtDenomination", ci);
            lblOtherFields.Text = rm.GetString("txtOtherFields", ci);
            lblQualifiedAudit.Text = rm.GetString("txtQualifiedAudit", ci);
            lblSmallStatus.Text = rm.GetString("txtSmallStatus", ci);
            lblComments.Text = rm.GetString("txtComments", ci);
            lblStaffCount.Text = rm.GetString("txtStaffCount", ci);
            lblIncomeSheet.Text = rm.GetString("txtIncomeSheet", ci);
            lblRevenue.Text = rm.GetString("txtRevenue", ci);
            lblRevenueFromMainOperations.Text = rm.GetString("txtRevenueFromMainOperations", ci);
            lblOtherOperationRevenue.Text = rm.GetString("txtOtherOperatingRevenue", ci);
            lblTotalRevenue.Text = rm.GetString("txtTotalRevenue", ci);
            lblCosts.Text = rm.GetString("txtCost", ci);
            lblCostOfSales.Text = rm.GetString("txtCostOfSales", ci);
            lblAdministrativeAndOtherExpenses.Text = rm.GetString("txtAdministrativeOtherExpenses", ci);
            lblDistributionCost.Text = rm.GetString("txtDistributionCost", ci);
            lblTotalCost.Text = rm.GetString("txtTotalCost", ci);
            lblStaffCost.Text = rm.GetString("txtStaffCost", ci);
            lblDepricationOfFixedAssets.Text = rm.GetString("txtDepricationOfFixedAssets", ci);
            lblOperationalProfit.Text = rm.GetString("txtOperationalProfit", ci);
            lblFinIncomeAndCosts.Text = rm.GetString("txtFinancialIncomeCosts", ci);
            lblFinancialIncome.Text = rm.GetString("txtFinancialIncome", ci);
            lblFinancialCost.Text = rm.GetString("txtFinancialCost", ci);
            lblOtherFinancialEntries.Text = rm.GetString("txtOtherFinancialItems", ci);
            lblNetFinancialItems.Text = rm.GetString("txtNetFinancialItems", ci);
            lblIncomeFromOperationsBeforeTax.Text = rm.GetString("txtIncomeFromOperationsBeforeTax", ci);
            lblIncomeTax.Text = rm.GetString("txtTaxes", ci);
            lblIncomeFromOperationsAfterTax.Text = rm.GetString("txtIncomeFromOperationsAfterTax", ci);
            lblExtraOrdinaryIncomeCost.Text = rm.GetString("txtExtraordinaryIncomeCost", ci);
            lblResultBeforeIncomeFromAssociatedCompanies.Text =
                rm.GetString("txtResultBeforeIncomeFromAssociatedCompanies", ci);
            lblIncomeFromAssociatedCompanies.Text = rm.GetString("txtIncomeFromAssociatedCompanies", ci);
            lblNetProfitHead.Text = rm.GetString("txtNetProfit", ci);
            lblNetProfit.Text = rm.GetString("txtProfitOrLoss", ci);
            lblBalanceSheetAssets.Text = rm.GetString("txtBalanceSheetASSETS", ci);
            lblFixedAssets.Text = rm.GetString("txtFixedAssets", ci);
            lblIntangibleAssets.Text = rm.GetString("txtIntangibleAssets", ci);
            lblTangibleAssets.Text = rm.GetString("txtTangibleAssets", ci);
            lblFinancialAssets.Text = rm.GetString("txtFinancialAssets", ci);
            lblTotalFixedAssets.Text = rm.GetString("txtTotalFixedAssets", ci);
            lblCurrentAssets.Text = rm.GetString("txtCurrentAssets", ci);
            lblInventory.Text = rm.GetString("txtInventory", ci);
            lblDebtors.Text = rm.GetString("txtReceivablesDebtors", ci);
            lblClaimsTowardsAssociatedCompanies.Text = rm.GetString("txtClaimsTowardsAssociatedCompanies", ci);
            lblShortTermInvestments.Text = rm.GetString("txtShortTermInvestments", ci);
            lblCashInBankHand.Text = rm.GetString("txtCash", ci);
            lblDeferredTaxAsset.Text = rm.GetString("txtDeferredTaxAsset", ci);
            lblOtherCurrentAssets.Text = rm.GetString("txtOtherCurrentAssets", ci);
            lblTotalCurrentAssets.Text = rm.GetString("txtTotalCurrentAssets", ci);
            lblTotalAssetsHead.Text = rm.GetString("txtTotalAssetsHead", ci);
            lblTotalAssets.Text = rm.GetString("txtTotalAssets", ci);
            lblBalanceSheetEquityAndDebt.Text = rm.GetString("txtBalanceSheetEquityAndDebt", ci);
            lblEquity.Text = rm.GetString("txtEquity", ci);
            lblIssuedShareCapital.Text = rm.GetString("txtIssuedShareCapital", ci);
            lblProfitAndLossAccount.Text = rm.GetString("txtProfitAndLossAccount", ci);
            lblDereferedTaxations.Text = rm.GetString("txtDereferedTaxations", ci);
            lblOtherEquity.Text = rm.GetString("txtOtherEquity", ci);
            lblTotalEquity.Text = rm.GetString("txtTotalEquity", ci);
            lblLiabilities.Text = rm.GetString("txtLiabilities", ci);
            lblTaxLiabilities.Text = rm.GetString("txtTaxLiabilities", ci);
            lblOtherLiabilities.Text = rm.GetString("txtOtherLiabilities", ci);
            lblTotalLiabilities.Text = rm.GetString("txtTotalLiabilities", ci);
            lblLongTermDebt.Text = rm.GetString("txtLongTermDebts", ci);
            lblLongTermDebts.Text = rm.GetString("txtLongTermDebts", ci);
            lblShortTermDebts.Text = rm.GetString("txtShortTermDebts", ci);
            lblLoansOverdraft.Text = rm.GetString("txtLoansOverdraft", ci);
            lblCreditors.Text = rm.GetString("txtCreditors", ci);
            lblDebtToAssociatedCompanies.Text = rm.GetString("txtDebtTowardsAssociatedCompanies", ci);
            lblOtherShortTermDebt.Text = rm.GetString("txtOtherShortTermDebt", ci);
            lblTotalShortTermDebt.Text = rm.GetString("txtTotalShortTermDebt", ci);
            lblTotalDebtsAndEquityHead.Text = rm.GetString("txtTotalDebtsEquity", ci);
            lblTotalDebtsHead.Text = rm.GetString("txtTotalDebts", ci);
            lblTotalDebtsAndEquity.Text = rm.GetString("txtTotalDebtsEquity", ci);
            lblCashFlow.Text = rm.GetString("txtCashFlow", ci);
            lblOperatingProfitBeforeWorkingCapitalChanges.Text =
                rm.GetString("txtOperatingProfitBeforeWorkingCapitalChanges", ci);
            lblCashFromOperations.Text = rm.GetString("txtCashFromOperations", ci);
            lblCashprovidedByOperatingActivities.Text = rm.GetString("txtNetCashFromOperatingActivities", ci);
            lblInvestingActivities.Text = rm.GetString("txtCashFlowsFromInvestingActivities", ci);
            lblFinancialActivities.Text = rm.GetString("txtCashFlowsFromFinancingActivities", ci);
            lblInDecreaseInCashAndInHand.Text = rm.GetString("txtIncreaseDecreaseInCash", ci);
            btnSubmit.Text = rm.GetString("txtSubmit", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);
            lblReadyForWebPublish.Text = rm.GetString("txtReadyForWebPublish", ci);

            lblIncomeFromAffiliates.Text = rm.GetString("txtIncomeFromAffiliates", ci);
            lblIncomeFromSubsidiaries.Text = rm.GetString("txtIncomeFromSubsidiaries", ci);
            lblPensionFundsLiabilities.Text = rm.GetString("txtPensionFundsLiabilities", ci);
            lblNextYearPayment.Text = rm.GetString("txtNextYearPayment", ci);
            lblCurrentAssetsLessCurrentLiabilities.Text = rm.GetString("txtCurrentAssetsLessCurrentLiabilities", ci);

            lblMinorityHoldingsInEquity.Text = rm.GetString("txtMinorityHoldingsInEquity", ci);
            lblSource.Text = rm.GetString("txtSource", ci);
        }

        private void InitBoxes() {
            InitDenominationBox();
            InitSourceBox();
            InitCurrencyBox();
            InitCommentBox();
        }

        private void InitDenominationBox() {
            var dsDenomination = theStatementFact.GetDenominationAsDataSet();
            ddDenomination.DataSource = dsDenomination;
            ddDenomination.DataTextField = nativeCult ? "Description_native" : "Description_en";

            ddDenomination.DataValueField = "Denomination_id";
            ddDenomination.DataBind();
        }

        private void InitSourceBox() {
            var dsSource = theStatementFact.GetSourceAsDataSet();
            ddSource.DataSource = dsSource;
            ddSource.DataTextField = nativeCult ? "Description_native" : "Description_en";
            ddSource.DataValueField = "Origin_id";
            ddSource.DataBind();
        }

        private void InitCommentBox() {
            var dsComments = theStatementFact.GetCommentsAsDataSet();
            ddComments.DataSource = dsComments;
            ddComments.DataTextField = nativeCult ? "Description_native" : "Description_en";
            ddComments.DataValueField = "Comments_id";
            ddComments.DataBind();
        }

        private void InitCurrencyBox() {
            var currencySet = theStatementFact.GetCurrencyAsDataSet();
            ddCurrency.DataSource = currencySet;
            ddCurrency.DataTextField = "CurrencyCode";
            ddCurrency.DataValueField = "CurrencyCode";
            ddCurrency.DataBind();
            // if currency order key in config then order the currency list ...
            if (CigConfig.Configure("lookupsettings.orderCurrencyList") != null) {
                OrderCurrencyList(CigConfig.Configure("lookupsettings.orderCurrencyList"));
            }
        }

        private void OrderCurrencyList(string orderList) {
            var strArray = orderList.Split(new[] {','});
            int index = 0;
            foreach (string id in strArray) {
                ListItem li = ddCurrency.Items.FindByValue(id);
                ddCurrency.Items.Remove(li);
                ddCurrency.Items.Insert(index, li);
                index++;
            }
        }

        private void SetFieldValues(FinancialStatementBLLC theStatement) {
            // Haus
            SetHeadFieldValues(theStatement);
            // Other fields
            SetOtherFieldsValues(theStatement);
            // Income sheet
            SetIncomeSheetFieldValues(theStatement);
            // Balance sheet - ASSETS
            SetBalanceSheetFieldValues(theStatement);
            // Balance sheet - Equity and Debt
            SetBalanceSheetEquityAndDebtFieldValues(theStatement);
            // Cash Flow
            SetCashFlowFieldValues(theStatement);
        }

        private void SetCompanyInfo(int ciid) {
            var theAdd = new Address();
            Company theComp = theStatementFact.GetCompany(ciid);
            if (Session["FISCNationalID"] != null) {
                theComp.NationalID = (string) Session["FISCNationalID"];
            }
            if (theComp.Address.Count > 0) {
                theAdd = (Address) theComp.Address[0]; // taka bara fyrsta item
            }

            // athuga me� comp operation if null athuga �� � Nace k��a t�flu
            /*	
				if(theComp.FuncDescriptionNative == null || theComp.FuncDescriptionNative == "" || theComp.FuncDescriptionEN == null || theComp.FuncDescriptionEN == "") 
				{
					dsCompanyOperation = theStatementFact.GetCompanyOperationAsDataSet(theStatement.CompanyCIID,true);
					if(dsCompanyOperation.Tables[0] != null)
						theComp.FuncDescriptionNative = dsCompanyOperation.Tables[0].I
				}
			*/
            // hmm hva� me� nafn etc... ? 
            if (nativeCult) {
                lblCompanyName.Text = theComp.NameNative != "" ? theComp.NameNative : theComp.NameEN;
                lblCompanyAddress.Text = theAdd.StreetNative != "" ? theAdd.StreetNative : theAdd.StreetEN;
                lblCompanyFunction.Text = theComp.FuncDescriptionNative != "" ? theComp.FuncDescriptionNative : theComp.FuncDescriptionEN;

                if (theComp.Address.Count > 0) {
                    lblCompanyCity.Text = theAdd.CityNameNative != "" ? theAdd.CityNameNative : theAdd.CityNameEN;
                }
            } else {
                lblCompanyName.Text = theComp.NameEN != "" ? theComp.NameEN : theComp.NameNative;
                lblCompanyAddress.Text = theAdd.StreetEN != "" ? theAdd.StreetEN : theAdd.StreetNative;
                lblCompanyFunction.Text = theComp.FuncDescriptionShortEN != "" ? theComp.FuncDescriptionShortEN : theComp.FuncDescriptionNative;

                if (theComp.Address.Count > 0) {
                    lblCompanyCity.Text = theAdd.CityNameEN != "" ? theAdd.CityNameEN : theAdd.CityNameNative;
                }
            }
            lblCompanyNationalID.Text = theComp.NationalID;
        }

        private void SetHeadFieldValues(FinancialStatementBLLC theStatement) {
            lblMostRecentAccount.Text += " (" + theStatement.MostRecentAccount + ")";
            tbFiscalYear.Text = theStatement.FiscalYear;
            ddTotalMonths.SelectedValue = Convert.ToString(theStatement.AccountMonths);
            // check for min data
            if (theStatement.YearEnded != DateTime.MinValue) {
                tbYearEnded.Text = Convert.ToString(theStatement.YearEnded.ToShortDateString());
            }
            try {
                ddCurrency.SelectedValue = theStatement.CurrencyID;
            } catch {}
            chbConsolidated.Checked = theStatement.Consolidated;
            chbReadyForWebPublish.Checked = theStatement.ReadyForWebPublishing;
            ddDenomination.SelectedValue = Convert.ToString(theStatement.DenominationID);
            try {
                ddSource.SelectedValue = Convert.ToString(theStatement.OriginID);
            } catch (Exception err) {
                // just catching the exception no need to do anything
            }
        }

        private void SetOtherFieldsValues(FinancialStatementBLLC theStatement) {
            chbQualifiedAudit.Checked = theStatement.Qualified;
            chbSmallStatus.Checked = theStatement.SmallStatus;
            try {
                ddComments.SelectedValue = Convert.ToString(theStatement.CommentsID);
            } catch {}

            if (theStatement.StaffCount != int.MinValue) {
                tbStaffCount.Text = Convert.ToString(theStatement.StaffCount);
            }
        }

        private void SetIncomeSheetFieldValues(FinancialStatementBLLC theStatement) {
            if (theStatement.RevenueFromMainOperation != int.MinValue) {
                tbRevenueFromMainOperation.Text = Convert.ToString(theStatement.RevenueFromMainOperation);
            }
            if (theStatement.OtherOperationRevenue != int.MinValue) {
                tbOtherOperationRevenue.Text = Convert.ToString(theStatement.OtherOperationRevenue);
            }
            if (theStatement.TotalRevenue != int.MinValue) {
                tbTotalRevenue.Text = Convert.ToString(theStatement.TotalRevenue);
            }
            if (theStatement.CostOfSales != int.MinValue) {
                tbCostOfSales.Text = Convert.ToString(theStatement.CostOfSales);
            }
            if (theStatement.AdministrativeAndOtherExpenses != int.MinValue) {
                tbAdministrativeAndOtherExpenses.Text = Convert.ToString(theStatement.AdministrativeAndOtherExpenses);
            }
            if (theStatement.DistributionCost != int.MinValue) {
                tbDistributionCost.Text = Convert.ToString(theStatement.DistributionCost);
            }
            if (theStatement.TotalCost != int.MinValue) {
                tbTotalCost.Text = Convert.ToString(theStatement.TotalCost);
            }
            if (theStatement.StaffCost != int.MinValue) {
                tbStaffCost.Text = Convert.ToString(theStatement.StaffCost);
            }
            if (theStatement.DepricationOfFixedAssets != int.MinValue) {
                tbDepricationOfFixedAssets.Text = Convert.ToString(theStatement.DepricationOfFixedAssets);
            }
            if (theStatement.OperationalProfit != int.MinValue) {
                tbOperationalProfit.Text = Convert.ToString(theStatement.OperationalProfit);
            }

            if (theStatement.FinancialIncome != int.MinValue) {
                tbFinancialIncome.Text = Convert.ToString(theStatement.FinancialIncome);
            }
            if (theStatement.FinancialCost != int.MinValue) {
                tbFinancialCost.Text = Convert.ToString(theStatement.FinancialCost);
            }
            if (theStatement.OtherFinancialEntries != int.MinValue) {
                tbOtherFinancialEntries.Text = Convert.ToString(theStatement.OtherFinancialEntries);
            }
            if (theStatement.NetFinancialItems != int.MinValue) {
                tbNetFinancialItems.Text = Convert.ToString(theStatement.NetFinancialItems);
            }
            if (theStatement.IncomeFromOperationsBeforeTax != int.MinValue) {
                tbIncomeFromOperationsBeforeTax.Text = Convert.ToString(theStatement.IncomeFromOperationsBeforeTax);
            }
            if (theStatement.IncomeTax != int.MinValue) {
                tbIncomeTax.Text = Convert.ToString(theStatement.IncomeTax);
            }
            if (theStatement.IncomeFromOperationsAfterTax != int.MinValue) {
                tbIncomeFromOperationsAfterTax.Text = Convert.ToString(theStatement.IncomeFromOperationsAfterTax);
            }
            if (theStatement.ExtraOrdinaryIncomeCost != int.MinValue) {
                tbExtraOrdinaryIncomeCost.Text = Convert.ToString(theStatement.ExtraOrdinaryIncomeCost);
            }
            if (theStatement.ResultBeforeIncomeFromAssociatedCompanies != int.MinValue) {
                tbResultBeforeIncomeFromAssociatedCompanies.Text =
                    Convert.ToString(theStatement.ResultBeforeIncomeFromAssociatedCompanies);
            }
            if (theStatement.IncomeFromAffiliates != int.MinValue) {
                tbIncomeFromAffiliates.Text = Convert.ToString(theStatement.IncomeFromAffiliates);
            }
            if (theStatement.IncomeFromSubsidiaries != int.MinValue) {
                tbIncomeFromSubsidiaries.Text = Convert.ToString(theStatement.IncomeFromSubsidiaries);
            }
            if (theStatement.IncomeFromAssociatedCompanies != int.MinValue) {
                tbIncomeFromAssociatedCompanies.Text = Convert.ToString(theStatement.IncomeFromAssociatedCompanies);
            }
            if (theStatement.NetProfit != int.MinValue) {
                tbNetProfit.Text = Convert.ToString(theStatement.NetProfit);
            }
        }

        private void SetBalanceSheetFieldValues(FinancialStatementBLLC theStatement) {
            if (theStatement.IntangableAssets != int.MinValue) {
                tbIntangibleAssets.Text = Convert.ToString(theStatement.IntangableAssets);
            }
            if (theStatement.TangableAssets != int.MinValue) {
                tbTangibleAssets.Text = Convert.ToString(theStatement.TangableAssets);
            }
            if (theStatement.FinancialAssets != int.MinValue) {
                tbFinancialAssets.Text = Convert.ToString(theStatement.FinancialAssets);
            }
            if (theStatement.TotalFixedAssets != int.MinValue) {
                tbTotalFixedAssets.Text = Convert.ToString(theStatement.TotalFixedAssets);
            }
            if (theStatement.Inventory != int.MinValue) {
                tbInventory.Text = Convert.ToString(theStatement.Inventory);
            }
            if (theStatement.Debtors != int.MinValue) {
                tbDebtors.Text = Convert.ToString(theStatement.Debtors);
            }
            if (theStatement.ClaimsTowardsAssociatedCompanies != int.MinValue) {
                tbClaimsTowardsAssociatedCompanies.Text = Convert.ToString(
                    theStatement.ClaimsTowardsAssociatedCompanies);
            }
            if (theStatement.ShortTermInvestments != int.MinValue) {
                tbShortTermInvestments.Text = Convert.ToString(theStatement.ShortTermInvestments);
            }
            if (theStatement.CashAtBankAndInHand != int.MinValue) {
                tbCashInBankHand.Text = Convert.ToString(theStatement.CashAtBankAndInHand);
            }
            if (theStatement.DeferredTaxAsset != int.MinValue) {
                tbDeferredTaxAsset.Text = Convert.ToString(theStatement.DeferredTaxAsset);
            }
            if (theStatement.OtherCurrentAssets != int.MinValue) {
                tbOtherCurrentAssets.Text = Convert.ToString(theStatement.OtherCurrentAssets);
            }
            if (theStatement.TotalCurrentAssets != int.MinValue) {
                tbTotalCurrentAssets.Text = Convert.ToString(theStatement.TotalCurrentAssets);
            }
            if (theStatement.TotalAssets != int.MinValue) {
                tbTotalAssets.Text = Convert.ToString(theStatement.TotalAssets);
            }
        }

        private void SetBalanceSheetEquityAndDebtFieldValues(FinancialStatementBLLC theStatement) {
            if (theStatement.IssuedShareCapital != int.MinValue) {
                tbIssuedShareCapital.Text = Convert.ToString(theStatement.IssuedShareCapital);
            }
            if (theStatement.ProfitAndLossAccount != int.MinValue) {
                tbProfitAndLossAccount.Text = Convert.ToString(theStatement.ProfitAndLossAccount);
            }
            if (theStatement.OtherEquity != int.MinValue) {
                tbOtherEquity.Text = Convert.ToString(theStatement.OtherEquity);
            }
            if (theStatement.TotalEquity != int.MinValue) {
                tbTotalEquity.Text = Convert.ToString(theStatement.TotalEquity);
            }
            if (theStatement.MinorityHoldingsInEquity != int.MinValue) {
                tbMinorityHoldingsInEquity.Text = Convert.ToString(theStatement.MinorityHoldingsInEquity);
            }
            if (theStatement.PensionFundsLiabilities != int.MinValue) {
                tbPensionFundsLiabilities.Text = Convert.ToString(theStatement.PensionFundsLiabilities);
            }
            if (theStatement.TaxLiabilities != int.MinValue) {
                tbTaxLiabilities.Text = Convert.ToString(theStatement.TaxLiabilities);
            }
            if (theStatement.OtherLiabilities != int.MinValue) {
                tbOtherLiabilities.Text = Convert.ToString(theStatement.OtherLiabilities);
            }
            if (theStatement.TotalLiabilities != int.MinValue) {
                tbTotalLiabilities.Text = Convert.ToString(theStatement.TotalLiabilities);
            }
            if (theStatement.LongTermDebts != int.MinValue) {
                tbLongTermDebts.Text = Convert.ToString(theStatement.LongTermDebts);
            }
            if (theStatement.LoansOverdraft != int.MinValue) {
                tbLoansOverdraft.Text = Convert.ToString(theStatement.LoansOverdraft);
            }
            if (theStatement.Creditors != int.MinValue) {
                tbCreditors.Text = Convert.ToString(theStatement.Creditors);
            }
            if (theStatement.DebtToAssociatedCompanies != int.MinValue) {
                tbDebtToAssociatedCompanies.Text = Convert.ToString(theStatement.DebtToAssociatedCompanies);
            }
            if (theStatement.CurrentPortionOfLongTermLiabilities != int.MinValue) {
                tbNextYearPayment.Text = Convert.ToString(theStatement.CurrentPortionOfLongTermLiabilities);
            }
            if (theStatement.OtherShortTermDebt != int.MinValue) {
                tbOtherShortTermDebt.Text = Convert.ToString(theStatement.OtherShortTermDebt);
            }
            if (theStatement.TotalShortTermDebts != int.MinValue) {
                tbTotalShortTermDebt.Text = Convert.ToString(theStatement.TotalShortTermDebts);
            }
            if (theStatement.CurrentAssetsLessCurrentLiabilities != int.MinValue) {
                tbCurrentAssetsLessCurrentLiabilities.Text =
                    Convert.ToString(theStatement.CurrentAssetsLessCurrentLiabilities);
            }
            if (theStatement.TotalDebtsEquity != int.MinValue) {
                tbTotalDebtsAndEquity.Text = Convert.ToString(theStatement.TotalDebtsEquity);
            }
            if (theStatement.MinorityHoldingsInEquity != int.MinValue) {
                tbMinorityHoldingsInEquity.Text = Convert.ToString(theStatement.MinorityHoldingsInEquity);
            }
        }

        private void SetCashFlowFieldValues(FinancialStatementBLLC theStatement) {
            if (theStatement.OperatingProfitBeforeWorkingCapitalChanges != int.MinValue) {
                tbOperatingProfitBeforeWorkingCapitalChanges.Text =
                    Convert.ToString(theStatement.OperatingProfitBeforeWorkingCapitalChanges);
            }
            if (theStatement.CashFromOperations != int.MinValue) {
                tbCashFromOperations.Text = Convert.ToString(theStatement.CashFromOperations);
            }
            if (theStatement.CashProvidedByOperatingActivities != int.MinValue) {
                tbCashprovidedByOperatingActivities.Text =
                    Convert.ToString(theStatement.CashProvidedByOperatingActivities);
            }
            if (theStatement.InvestingActivities != int.MinValue) {
                tbInvestingActivities.Text = Convert.ToString(theStatement.InvestingActivities);
            }

            if (theStatement.FinancialActivities != int.MinValue) {
                tbFinancialActivities.Text = Convert.ToString(theStatement.FinancialActivities);
            }
            if (theStatement.IncreaseDecreaseInCashAndCashInHand != int.MinValue) {
                tbInDecreaseInCashAndInHand.Text = Convert.ToString(theStatement.IncreaseDecreaseInCashAndCashInHand);
            }
        }

        public void DateValidate(object source, ServerValidateEventArgs value) {
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.CustomValidator1.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}