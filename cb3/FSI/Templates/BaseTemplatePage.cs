#region

using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using FSI.Localization;

using Cig.Framework.Base.Configuration;


#endregion

namespace FSI.Templates {
    /// <summary>
    /// Summary description for BaseTemplatePage.
    /// </summary>
    public class BaseTemplatePage : Page {
        // Localization
        protected static CultureInfo ci;
        protected static ResourceManager rm;
        /*protected string totalPageSteps;
		protected string currentPageStep;
		protected string nextPage;
		protected string previousPage;*/
        protected int fsiTemplate = -1;
        protected bool myIsEN;
        protected bool nativeCult;
        protected string pageName;

        /// <summary>
        /// Standard initialization for web page.
        /// </summary>
        protected bool PreparePageLoad() {
            myIsEN = false;
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                myIsEN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            // check the current culture
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }
            if (Session["FSITemplateID"] != null) {
                fsiTemplate = int.Parse(Session["FSITemplateID"].ToString());
                return true;
            }
            //	Logger.WriteToLog("No cpi template found in session",true);
            return false;
        }

        /// <summary>
        /// IsEN returns true if english or false if native culture.
        /// </summary>
        /// <returns>bool</returns>
        public bool IsEN() { return myIsEN; }
    }
}