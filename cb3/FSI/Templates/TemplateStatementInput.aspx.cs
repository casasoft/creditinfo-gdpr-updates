#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using FSI.BLL;
using FSI.Templates.BLL;
using FSI.Templates.BLL.generator;
using UserAdmin.BLL.CIUsers;

#endregion

using Cig.Framework.Base.Configuration;
using UserAdmin.BLL;

namespace FSI.Templates {
    /// <summary>
    /// Summary description for DynamicStatementInput.
    /// </summary>
    public class TemplateStatementInput : BaseTemplatePage {
        private readonly FinancialStatementFactory fact = new FinancialStatementFactory();
        protected HtmlTableCell blas;
        protected Button btnCancel;
        protected Button btnSubmit;
        protected CustomValidator CustomValidator1;
        protected CustomValidator cvYearEnded;
        protected DropDownList ddComments;
        protected DropDownList ddCurrency;
        protected DropDownList ddDenomination;
        protected DropDownList ddSource;
        protected DropDownList ddTotalMonths;
        protected Label lblAccountPeriodLength;
        protected Label lblComments;
        protected Label lblCompanyAddress;
        protected Label lblCompanyCity;
        protected Label lblCompanyFunction;
        protected Label lblCompanyName;
        protected Label lblCompanyNationalID;
        protected Label lblConsolidated;
        protected Label lblCurrency;
        protected Label lblDenomination;
        protected Label lblFinancialStatement;
        protected Label lblFiscalYear;
        protected Label lblMostRecentAccount;
        protected Label lblMsg;
        protected Label lblOtherFields;
        protected Label lblPageTitle;
        protected Label lblQualifiedAudit;
        protected Label lblReadyForWebPublish;
        protected Label lblSmallStatus;
        protected Label lblSource;
        protected Label lblStaffCount;
        protected Label lblYearEnded;
        protected Literal litMyJavascriptControl;
        protected Panel PnlForm;
        protected RadioButton rbtConsolidatedNo;
        protected RadioButton rbtConsolidatedYes;
        protected RadioButton rbtQualifiedAuditNo;
        protected RadioButton rbtQualifiedAuditYes;
        protected RadioButton rbtReadyForWebFalse;
        protected RadioButton rbtReadyForWebTrue;
        protected RadioButton rbtSmallStatusNo;
        protected RadioButton rbtSmallStatusYes;
        protected RequiredFieldValidator rfvYearEnded;
        protected TextBox tbAFS_id;
        protected TextBox tbFiscalYear;
        protected TextBox tbStaffCount;
        protected TextBox tbYearEnded;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "1006";
            PreparePageLoad();

            int year;
            int accountPeriod;

            if (!Page.IsPostBack) {
                LocalizeText();
                InitBoxes();
                FillAccountPeriodBox();

                if (Session["FSI_tbCompanyRegNo"] != null) {
                    // n� � ID og YEAR params
                    string nationalID = Session["FSI_tbCompanyRegNo"].ToString();
                    Session["FISCNationalID"] = nationalID;
                    year = Convert.ToInt32(Session["FSI_year"].ToString());
                    accountPeriod = Convert.ToInt32(Session["FSI_ddAccountPeriod"].ToString());
                    // athuga hvort �rsreikningur s� til
                    // �arf a� skipta �t nationalID fyrir CIID ... e�a hva�?

                    int companyCIID = fact.GetCIIDByNationalID(nationalID);
                    // ef fyrirt�ki� er ekki til � kerfinu � �a� �.a.l. ekki �rsreikning
                    if (companyCIID >= 0) {
                        //S�kja uppl�singar um fyrirt�ki�
                        SetCompanyInfo(companyCIID);
                        tbFiscalYear.Text = year.ToString();
                        try {
                            ddTotalMonths.SelectedValue = accountPeriod.ToString();
                        } catch {}

                        XmlDocument doc;
                        var validations = new StringBuilder();
                        var generator = new DynamicPageGenerator();
                        var genInfo = new GeneralInfoBLLC(companyCIID, year, accountPeriod);
                        ViewState["Last_Year_AFS_id"] = genInfo.Last_Year_AFS_id;
                        if (genInfo.IsFilledUp) {
                            // �rsreikningurinn er til ... fylla sv��i
                            tbAFS_id.Text = Convert.ToString(genInfo.AFS_id);
                            SetHeadFieldValues(genInfo);
                            tbFiscalYear.Enabled = false;
                            btnSubmit.Text = rm.GetString("txtUpdate", ci);
                            btnSubmit.CommandName = "Update";
                            doc = genInfo.Last_Year_AFS_id > 0 ? generator.GeneratePage(
                                                                     fsiTemplate,
                                                                     genInfo.AFS_id,
                                                                     genInfo.Last_Year_AFS_id,
                                                                     ref validations,
                                                                     (int.Parse(genInfo.FiscalYear) - 1) + "",
                                                                     genInfo.FiscalYear) : generator.GeneratePage(fsiTemplate, genInfo.AFS_id, ref validations);
                        } else {
                            doc = genInfo.Last_Year_AFS_id > -1 ? generator.GeneratePage(
                                                                      fsiTemplate,
                                                                      -1,
                                                                      genInfo.Last_Year_AFS_id,
                                                                      ref validations,
                                                                      (int.Parse(genInfo.FiscalYear) - 1) + "",
                                                                      genInfo.FiscalYear) : generator.GeneratePage(fsiTemplate, -1, ref validations);
                        }
                        AddJSValidation(validations.ToString());

                        var xsl =
                            new Transformer(
                                Server.MapPath(
                                    Request.ApplicationPath + "/" +
                                    CigConfig.Configure("lookupsettings.FSITemplateXslFile"))) {ControlHolder = PnlForm};
                        //doc.Save("c:\\cig\\templateform.xml");
                        xsl.TransformFields("FSI_INPUT", doc);
                    } else {
                        Server.Transfer("SelectStatement.aspx");
                    }
                }
            }
            btnSubmit.Attributes["onclick"] = "javascript: return SaveAllowed();";
        }

        private void FillAccountPeriodBox() {
            if (CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                ddTotalMonths.Items.Clear();
                ddTotalMonths.Items.Add(new ListItem("18", "18"));
                ddTotalMonths.Items.Add(new ListItem("17", "17"));
                ddTotalMonths.Items.Add(new ListItem("16", "16"));
                ddTotalMonths.Items.Add(new ListItem("15", "15"));
                ddTotalMonths.Items.Add(new ListItem("14", "14"));
                ddTotalMonths.Items.Add(new ListItem("13", "13"));
                ddTotalMonths.Items.Add(new ListItem("12", "12"));
                ddTotalMonths.Items.Add(new ListItem("11", "11"));
                ddTotalMonths.Items.Add(new ListItem("10", "10"));
                ddTotalMonths.Items.Add(new ListItem("9", "9"));
                ddTotalMonths.Items.Add(new ListItem("8", "8"));
                ddTotalMonths.Items.Add(new ListItem("7", "7"));
                ddTotalMonths.Items.Add(new ListItem("6", "6"));
                ddTotalMonths.Items.Add(new ListItem("5", "5"));
                ddTotalMonths.Items.Add(new ListItem("4", "4"));
                ddTotalMonths.Items.Add(new ListItem("3", "3"));
                ddTotalMonths.Items.Add(new ListItem("2", "2"));
                ddTotalMonths.SelectedValue = "12";
            }
        }

        protected void AddJSValidation(string validation) {
            string val = "<SCRIPT language=\"javascript\">\r\n";
            val += validation;
            val += "</script>";
            litMyJavascriptControl.Text = val;
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                Save();
            } else {
                var validations = new StringBuilder();
                var generator = new DynamicPageGenerator();

                int lastYearsID = -1;
                if (ViewState["Last_Year_AFS_id"] != null) {
                    lastYearsID = int.Parse(ViewState["Last_Year_AFS_id"].ToString());
                }

                var doc = lastYearsID > 0 ? generator.GeneratePage(
                                                        fsiTemplate,
                                                        lastYearsID,
                                                        LoadDynamicDataFromPage(),
                                                        ref validations,
                                                        (int.Parse(tbFiscalYear.Text) - 1) + "",
                                                        tbFiscalYear.Text) : generator.GeneratePage(fsiTemplate, LoadDynamicDataFromPage(), ref validations);

                AddJSValidation(validations.ToString());

                var xsl =
                    new Transformer(
                        Server.MapPath(
                            Request.ApplicationPath + "/" + CigConfig.Configure("lookupsettings.FSITemplateXslFile")))
                    {ControlHolder = PnlForm};
                //doc.Save("c:\\cig\\templateform.xml");
                xsl.TransformFields("FSI_INPUT", doc);
            }
        }

        private FieldCollectionBLLC LoadDynamicDataFromPage() {
            if (fsiTemplate > 0) {
                var coll = new TemplateFieldsCollectionBLLC(fsiTemplate);
                if (coll.Count > 0) {
                    var fColl = new FieldCollectionBLLC();
                    for (int i = 0; i < coll.Count; i++) {
                        var field = new FieldBLLC(coll.GetField(i).FieldID);
                        var valueField = new FieldBLLC
                                         {
                                             FieldName = field.FieldName,
                                             Value = Request[field.FieldName],
                                             Type = field.Type
                                         };
                        fColl.Add(valueField);
                    }
                    return fColl;
                }
            }
            return null;
        }

        private void Save() {
            lblMsg.Visible = false;
            var info = new GeneralInfoBLLC();
            var nationalID = (string) Session["FISCNationalID"];
            info.CompanyCIID = fact.GetCIIDByNationalID(nationalID);

            // Haus
            if (tbFiscalYear.Text != "") {
                info.FiscalYear = tbFiscalYear.Text;
            }
            info.AccountMonths = Convert.ToInt32(ddTotalMonths.SelectedValue);
            IFormatProvider format = CultureInfo.CurrentCulture;
            if (tbYearEnded.Text != "") {
                info.YearEnded = DateTime.Parse(tbYearEnded.Text, format, DateTimeStyles.AllowWhiteSpaces);
            }
            info.CurrencyID = ddCurrency.SelectedValue;
            info.Consolidated = rbtConsolidatedYes.Checked;
            info.DenominationID = Convert.ToInt32(ddDenomination.SelectedValue);
            if(ddSource.SelectedIndex != -1)
                 info.OriginID = Convert.ToInt32(ddSource.SelectedValue);
            // Other fields
            info.Qualified = rbtQualifiedAuditYes.Checked;
            info.SmallStatus = rbtSmallStatusYes.Checked;
            info.ReadyForWebPublishing = rbtReadyForWebTrue.Checked;
            info.CommentsID = Convert.ToInt32(ddComments.SelectedValue);
            info.Modified = DateTime.Now;
            /*if(this.tbComments.Text != "")
				theStatement.Comments = this.tbComments.Text;*/
            if (tbStaffCount.Text != "") {
                info.StaffCount = Convert.ToInt32(tbStaffCount.Text);
            }

            //Set last year - retrive from viewstate if available
            if (ViewState["Last_Year_AFS_id"] != null) {
                try {
                    info.Last_Year_AFS_id = int.Parse(ViewState["Last_Year_AFS_id"].ToString());
                } catch (Exception) {
                    info.Last_Year_AFS_id = -1;
                }
            }

            bool update = false;
            if (tbAFS_id.Text != "") {
                update = true;
                info.AFS_id = Convert.ToInt32(tbAFS_id.Text);
            }

            if (update) {
                info.Modified = DateTime.Now;
            }

            info.TemplateFields = LoadDynamicDataFromPage();
            info.TemplateID = fsiTemplate;

            uaFactory myUserAdminFactory = new uaFactory();
            info.UserID =   myUserAdminFactory.GetUserID(this.User.Identity.Name);
            if (info.Save()) {
                tbAFS_id.Text = info.AFS_id.ToString();
                if (update) {
                    lblMsg.Text = rm.GetString("txtStatementUpdated", ci);
                    lblMsg.CssClass = "confirm_text";
                    lblMsg.Visible = true;
                } else {
                    lblMsg.Text = rm.GetString("txtStatementRegistered", ci);
                    lblMsg.CssClass = "confirm_text";
                    lblMsg.Visible = true;
                }
            } else {
                info.AFS_id = -1;
                if (update) {
                    lblMsg.Text = rm.GetString("txtStatementUpdatedFailed", ci);
                    lblMsg.CssClass = "error_text";
                    lblMsg.Visible = true;
                } else {
                    lblMsg.Text = rm.GetString("txtStatementRegisteredFailed", ci);
                    lblMsg.CssClass = "error_text";
                    lblMsg.Visible = true;
                }
            }
            var generator = new DynamicPageGenerator();
            var xsl =
                new Transformer(
                    Server.MapPath(
                        Request.ApplicationPath + "/" + CigConfig.Configure("lookupsettings.FSITemplateXslFile")))
                {ControlHolder = PnlForm};
            var validations = new StringBuilder();
            if (info.Last_Year_AFS_id > 0) {
                xsl.TransformFields(
                    "FSI_INPUT",
                    generator.GeneratePage(
                        fsiTemplate,
                        info.AFS_id,
                        info.Last_Year_AFS_id,
                        ref validations,
                        (int.Parse(info.FiscalYear) - 1) + "",
                        info.FiscalYear));
            } else {
                xsl.TransformFields("FSI_INPUT", generator.GeneratePage(fsiTemplate, info.AFS_id, ref validations));
            }
            AddJSValidation(validations.ToString());
        }

        private void SetCompanyInfo(int ciid) {
            var theAdd = new Address();
            var theComp = fact.GetCompany(ciid);
            if (Session["FISCNationalID"] != null) {
                theComp.NationalID = (string) Session["FISCNationalID"];
            }
            if (theComp.Address.Count > 0) {
                theAdd = (Address) theComp.Address[0]; // taka bara fyrsta item
            }
            if (nativeCult) {
                lblCompanyName.Text = theComp.NameNative != "" ? theComp.NameNative : theComp.NameEN;
                lblCompanyAddress.Text = theAdd.StreetNative != "" ? theAdd.StreetNative : theAdd.StreetEN;
                lblCompanyFunction.Text = theComp.FuncDescriptionNative != "" ? theComp.FuncDescriptionNative : theComp.FuncDescriptionEN;

                if (theComp.Address.Count > 0) {
                    lblCompanyCity.Text = theAdd.CityNameNative != "" ? theAdd.CityNameNative : theAdd.CityNameEN;
                }
            } else {
                lblCompanyName.Text = theComp.NameEN != "" ? theComp.NameEN : theComp.NameNative;
                lblCompanyAddress.Text = theAdd.StreetEN != "" ? theAdd.StreetEN : theAdd.StreetNative;
                lblCompanyFunction.Text = theComp.FuncDescriptionShortEN != "" ? theComp.FuncDescriptionShortEN : theComp.FuncDescriptionNative;

                if (theComp.Address.Count > 0) {
                    lblCompanyCity.Text = theAdd.CityNameEN != "" ? theAdd.CityNameEN : theAdd.CityNameNative;
                }
            }
            lblCompanyNationalID.Text = theComp.NationalID;
        }

        private void SetHeadFieldValues(GeneralInfoBLLC info) {
            lblMostRecentAccount.Text += " (" + info.MostRecentAccount + ")";
            tbFiscalYear.Text = info.FiscalYear;
            ddTotalMonths.SelectedValue = Convert.ToString(info.AccountMonths);
            // check for min data
            if (info.YearEnded != DateTime.MinValue) {
                tbYearEnded.Text = Convert.ToString(info.YearEnded.ToShortDateString());
            }
            try {
                ddCurrency.SelectedValue = info.CurrencyID;
            } catch {}

            rbtConsolidatedYes.Checked = info.Consolidated;
            rbtConsolidatedNo.Checked = !info.Consolidated;

            rbtReadyForWebTrue.Checked = info.ReadyForWebPublishing;
            rbtReadyForWebFalse.Checked = !info.ReadyForWebPublishing;

            rbtQualifiedAuditYes.Checked = info.Qualified;
            rbtQualifiedAuditNo.Checked = !info.Qualified;

            rbtSmallStatusYes.Checked = info.SmallStatus;
            rbtSmallStatusNo.Checked = !info.SmallStatus;

            ddDenomination.SelectedValue = Convert.ToString(info.DenominationID);
            try {
                ddSource.SelectedValue = Convert.ToString(info.OriginID);
            } catch (Exception) {
                // just catching the exception no need to do anything
            }

            try {
                ddComments.SelectedValue = Convert.ToString(info.CommentsID);
            } catch {}

            if (info.StaffCount != int.MinValue) {
                tbStaffCount.Text = Convert.ToString(info.StaffCount);
            }
        }

        private void InitBoxes() {
            InitDenominationBox();
            InitSourceBox();
            InitCurrencyBox();
            InitCommentBox();
        }

        private void InitDenominationBox() {
            var dsDenomination = fact.GetDenominationAsDataSet();
            ddDenomination.DataSource = dsDenomination;
            ddDenomination.DataTextField = nativeCult ? "Description_native" : "Description_en";

            ddDenomination.DataValueField = "Denomination_id";
            ddDenomination.DataBind();
        }

        private void InitSourceBox() {
            var dsSource = fact.GetSourceAsDataSet();
            ddSource.DataSource = dsSource;
            ddSource.DataTextField = nativeCult ? "Description_native" : "Description_en";
            ddSource.DataValueField = "Origin_id";
            ddSource.DataBind();
        }

        private void InitCommentBox() {
            var dsComments = fact.GetCommentsAsDataSet();
            ddComments.DataSource = dsComments;
            ddComments.DataTextField = nativeCult ? "Description_native" : "Description_en";
            ddComments.DataValueField = "Comments_id";
            ddComments.DataBind();
        }

        private void InitCurrencyBox() {
            DataSet currencySet = fact.GetCurrencyAsDataSet();
            ddCurrency.DataSource = currencySet;
            ddCurrency.DataTextField = "CurrencyDescription";
            ddCurrency.DataValueField = "CurrencyCode";
            ddCurrency.DataBind();

            if (CigConfig.Configure("lookupsettings.orderCurrencyList") != null) {
                OrderCurrencyList(CigConfig.Configure("lookupsettings.orderCurrencyList"));
            }
        }

        private void OrderCurrencyList(string orderList) {
            var strArray = orderList.Split(new[] {','});
            int index = 0;
            foreach (string id in strArray) {
                var li = ddCurrency.Items.FindByValue(id);
                ddCurrency.Items.Remove(li);
                ddCurrency.Items.Insert(index, li);
                index++;
            }
        }

        protected void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtFinancialStatementInputPage", ci);
            lblFinancialStatement.Text = rm.GetString("txtFinancialStatement", ci);
            lblMostRecentAccount.Text = rm.GetString("txtMostRecentAccount", ci);
            lblFiscalYear.Text = rm.GetString("txtFiscalYear", ci);
            lblYearEnded.Text = rm.GetString("txtYearEnded", ci);
            lblAccountPeriodLength.Text = rm.GetString("txtAccountPeriodLength", ci);
            lblCurrency.Text = rm.GetString("txtCurrency", ci);
            lblConsolidated.Text = rm.GetString("txtConsolidated", ci);
            lblDenomination.Text = rm.GetString("txtDenomination", ci);
            lblOtherFields.Text = rm.GetString("txtOtherFields", ci);
            lblQualifiedAudit.Text = rm.GetString("txtQualifiedAudit", ci);
            lblSmallStatus.Text = rm.GetString("txtSmallStatus", ci);
            lblComments.Text = rm.GetString("txtComments", ci);
            lblStaffCount.Text = rm.GetString("txtStaffCount", ci);
            btnSubmit.Text = rm.GetString("txtSubmit", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);
            lblReadyForWebPublish.Text = rm.GetString("txtReadyForWebPublish", ci);
            lblSource.Text = rm.GetString("txtSource", ci);
            cvYearEnded.ErrorMessage = rm.GetString("txtDateInputIncorrect", ci);
            rfvYearEnded.ErrorMessage = rm.GetString("txtValueMissing", ci);
        }

        private void btnCancel_Click(object sender, EventArgs e) { Server.Transfer("TemplateSelectStatement.aspx"); }

        public void DateValidate(object source, ServerValidateEventArgs value) {
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cvYearEnded.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}