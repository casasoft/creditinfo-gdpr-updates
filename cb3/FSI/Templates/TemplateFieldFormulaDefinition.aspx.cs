#region

using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using FSI.Templates.BLL;

#endregion

namespace FSI.Templates {
    /// <summary>
    /// Summary description for TemplateFieldFormulaDefinition.
    /// </summary>
    public class TemplateFieldFormulaDefinition : BaseTemplatePage {
        protected Button btnAdd;
        protected Button btnCancel;
        protected ImageButton ibtnAddField;
        protected ImageButton ibtnAddOperator;
        protected ImageButton ibtnMoveDown;
        protected ImageButton ibtnMoveUp;
        protected ImageButton ibtnRemove;
        protected Label lblAvailableFields;
        protected Label lblDescriptionEN;
        protected Label lblDescriptionNative;
        protected Label lblErrMsg;
        protected Label lblFormula;
        protected Label lblFormulaDefinition;
        protected Label lblNameEN;
        protected Label lblNameNative;
        protected Label lblOperators;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        protected ListBox lbxFieldsInTemplate;
        protected ListBox lbxFormula;
        protected ListBox lbxOperators;
        protected TextBox txtDescriptionEN;
        protected TextBox txtDescriptionNative;
        protected TextBox txtFieldID;
        protected TextBox txtNameEN;
        protected TextBox txtNameNative;
        protected TextBox txtTemplateFieldID;
        protected TextBox txtValidationID;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "1005";
            PreparePageLoad();
            lblErrMsg.Visible = false;

            if (!Page.IsPostBack) {
                if (Context.Items.Contains("FieldID")) {
                    txtFieldID.Text = Context.Items["FieldID"].ToString();
                }
                if (Context.Items.Contains("TemplateFieldID")) {
                    txtTemplateFieldID.Text = Context.Items["TemplateFieldID"].ToString();
                }
                if (Context.Items.Contains("ValidationID")) {
                    txtValidationID.Text = Context.Items["ValidationID"].ToString();
                }

                LocalizeText();
                FillTemplateFields();
                FillOperators();
                if (txtValidationID.Text.Trim() != "") {
                    LoadValidation(int.Parse(txtValidationID.Text));
                }
            }
        }

        protected void LocalizeText() {
            lblNameNative.Text = rm.GetString("txtNameNative", ci);
            lblNameEN.Text = rm.GetString("txtNameEN", ci);
            lblDescriptionNative.Text = rm.GetString("txtDescriptionNative", ci);
            lblDescriptionEN.Text = rm.GetString("txtDescriptionEN", ci);
            lblFormula.Text = rm.GetString("txtFormula", ci);

            lblAvailableFields.Text = rm.GetString("txtAvailableFields", ci);

            if (txtFieldID.Text.Trim() != "") {
                lblFormulaDefinition.Text = rm.GetString("txtFormulaDefinitionForField", ci);
                var field = new FieldBLLC(int.Parse(txtFieldID.Text));
                if (nativeCult) {
                    lblFormulaDefinition.Text += " " + field.NameNative;
                } else {
                    lblFormulaDefinition.Text += " " + field.NameEN;
                }
            } else {
                lblFormulaDefinition.Text = rm.GetString("txtNewFormulaDefinition", ci);
            }

            lblOperators.Text = rm.GetString("txtOperators", ci);
            lblPageTitle.Text = rm.GetString("txtFSITemplate", ci);
            lblPageStep.Text = rm.GetString("", ci);
            btnAdd.Text = rm.GetString("txtSave", ci);
            btnCancel.Text = rm.GetString("txtBack", ci);
        }

        protected void LoadValidation(int id) {
            var validation = new FieldValidationBLLC(id);
            if (validation == null) {
                return;
            }
            txtNameNative.Text = validation.NameNative;
            txtNameEN.Text = validation.NameEN;
            txtDescriptionNative.Text = validation.DescriptionNative;
            txtDescriptionEN.Text = validation.DescriptionEN;
            for (int i = 0; i < validation.Formula.Count; i++) {
                var form = validation.Formula.GetFormula(i);
                if (form.IsField) {
                    var field = form.Field;
                    var li = new ListItem(field.NameNative, "FIELD_" + field.ID + "_" + form.ID);
                    if (!nativeCult) {
                        li.Text = field.NameEN;
                    }
                    lbxFormula.Items.Add(li);
                } else if (form.IsOperator) {
                    var oper = form.Operator;
                    var li = new ListItem(oper.Symbol, "OPERATOR_" + oper.ID + "_" + form.ID);
                    lbxFormula.Items.Add(li);
                }
            }
        }

        protected void FillTemplateFields() {
            FieldCollectionBLLC coll = null;
            coll = fsiTemplate > 0 ? new FieldCollectionBLLC(fsiTemplate) : new FieldCollectionBLLC(true);
            if (coll == null || coll.Count <= 0) {
                return;
            }
            for (int i = 0; i < coll.Count; i++) {
                var field = coll.GetField(i);
                if (field == null) {
                    continue;
                }
                var li = new ListItem(field.NameNative, "FIELD_" + field.ID + "_-1");
                if (!nativeCult) {
                    li.Text = field.NameEN;
                }
                lbxFieldsInTemplate.Items.Add(li);
            }
        }

        protected void FillOperators() {
            var coll = new OperatorCollectionBLLC();
            if (coll == null || coll.Count <= 0) {
                return;
            }
            for (int i = 0; i < coll.Count; i++) {
                OperatorBLLC oper = coll.GetOperator(i);
                if (oper == null) {
                    continue;
                }
                var li = new ListItem(oper.Symbol, "OPERATOR_" + oper.ID + "_-1");
                lbxOperators.Items.Add(li);
            }
        }

        private void ibtnAddField_Click(object sender, ImageClickEventArgs e) {
            foreach (ListItem item in lbxFieldsInTemplate.Items) {
                if (item.Selected) {
                    lbxFormula.Items.Add(item);
                }
            }
        }

        private void ibtnAddOperator_Click(object sender, ImageClickEventArgs e) {
            foreach (ListItem item in lbxOperators.Items) {
                if (item.Selected) {
                    lbxFormula.Items.Add(item);
                }
            }
        }

        private void ibtnRemove_Click(object sender, ImageClickEventArgs e) {
            var toDelete = new ArrayList();
            int index = 0;
            //First - get index of all items to delete
            foreach (ListItem item in lbxFormula.Items) {
                if (item.Selected) {
                    toDelete.Add(index);
                }
                index++;
            }

            //ok - now we have the index - the delete items in reversed order
            for (int i = toDelete.Count - 1; i >= 0; i--) {
                lbxFormula.Items.RemoveAt((int) toDelete[i]);
            }
        }

        private void ibtnMoveUp_Click(object sender, ImageClickEventArgs e) {
            var toMove = new ArrayList();
            int index = 0;
            //First - get index of all items to delete
            foreach (ListItem item in lbxFormula.Items) {
                if (item.Selected) {
                    toMove.Add(index);
                }
                index++;
            }

            //ok - now we have the index - the delete items in reversed order
            for (int i = 0; i < toMove.Count; i++) {
                if ((int) toMove[i] > 0) {
                    ListItem itm = lbxFormula.Items[(int) toMove[i]];
                    lbxFormula.Items.RemoveAt((int) toMove[i]);
                    lbxFormula.Items.Insert((int) toMove[i] - 1, itm);
                }
            }
        }

        private void ibtnMoveDown_Click(object sender, ImageClickEventArgs e) {
            var toMove = new ArrayList();
            int index = 0;
            //First - get index of all items to delete
            foreach (ListItem item in lbxFormula.Items) {
                if (item.Selected) {
                    toMove.Add(index);
                }
                index++;
            }

            //ok - now we have the index - the delete items in reversed order
            for (int i = toMove.Count - 1; i >= 0; i--) {
                if ((int) toMove[i] < lbxFormula.Items.Count - 1) {
                    ListItem itm = lbxFormula.Items[(int) toMove[i]];
                    lbxFormula.Items.RemoveAt((int) toMove[i]);
                    lbxFormula.Items.Insert((int) toMove[i] + 1, itm);
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            if (txtValidationID.Text.Trim() != "") {
                SaveValidation(false);
            } else {
                SaveValidation(true);
            }
        }

        protected void SaveValidation(bool isNew) {
            var validation = new FieldValidationBLLC {IsNew = isNew};
            if (!isNew) {
                validation.ID = int.Parse(txtValidationID.Text.Trim());
            }
            validation.NameNative = txtNameNative.Text;
            if (txtTemplateFieldID.Text.Trim().Length > 0) {
                validation.TemplateFieldID = int.Parse(txtTemplateFieldID.Text);
            }
            validation.NameEN = txtNameEN.Text;
            validation.DescriptionNative = txtDescriptionNative.Text;
            validation.DescriptionEN = txtDescriptionEN.Text;

            var coll = new FieldValidationFormulaCollectionBLLC();
            for (int i = 0; i < lbxFormula.Items.Count; i++) {
                var formula = new FieldValidationFormulaBLLC();
                var item = lbxFormula.Items[i];
                var arr = item.Value.Split('_');
                formula.ID = int.Parse(arr[2]);
                formula.ValidationItemID = int.Parse(arr[1]);
                formula.ValidationItemType = arr[0];
                formula.Order = i;
                coll.Add(formula);
            }
            validation.Formula = coll;
            if (validation.Save()) {
                txtValidationID.Text = validation.ID.ToString();
                TransferBack();
            } else {
                lblErrMsg.Text = "Error saving validation";
                lblErrMsg.Visible = true;
            }
        }

        private void TransferBack() {
            Context.Items.Add("TemplateFieldID", txtTemplateFieldID.Text);
            if (txtFieldID.Text.Trim() != "") {
                Context.Items.Add("FieldID", txtFieldID.Text);
                Server.Transfer("TemplateFieldFormulaOverview.aspx");
            } else {
                Context.Items.Add("FieldID", txtFieldID.Text);
                Server.Transfer("FieldValidationOverview.aspx");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e) { TransferBack(); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ibtnAddField.Click += new System.Web.UI.ImageClickEventHandler(this.ibtnAddField_Click);
            this.ibtnRemove.Click += new System.Web.UI.ImageClickEventHandler(this.ibtnRemove_Click);
            this.ibtnAddOperator.Click += new System.Web.UI.ImageClickEventHandler(this.ibtnAddOperator_Click);
            this.ibtnMoveUp.Click += new System.Web.UI.ImageClickEventHandler(this.ibtnMoveUp_Click);
            this.ibtnMoveDown.Click += new System.Web.UI.ImageClickEventHandler(this.ibtnMoveDown_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}