<%@ Register TagPrefix="ucl" TagName="options" Src="../../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="TemplateFieldFormulaOverview.aspx.cs" AutoEventWireup="false" Inherits="FSI.Templates.TemplateFieldFormulaOverview" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>TemplateFieldFormulaOverview</title>
		<% System.Web.HttpContext.Current.Response.AddHeader("Cache-Control","no-cache");%>
		<% System.Web.HttpContext.Current.Response.Expires = 0;%>
		<% System.Web.HttpContext.Current.Response.Cache.SetNoStore();%>
		<% System.Web.HttpContext.Current.Response.AddHeader("Pragma", "no-cache");%>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/CIGStyles.css" type="text/css" rel="stylesheet">
  </head>
	<body ms_positioning="GridLayout">
		<form id="TemplateFieldFormulaOverview" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->

									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblPageTitle" runat="server">FSI Template</asp:label>
																	</td>
																	<td align="right">
																		<asp:label id="lblPageStep" runat="server">Step</asp:label>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblFieldValidationsFormulas" runat="server" font-bold="True">Field validations for template 1 - Formulas</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblValidations" runat="server">Validations</asp:label>
																		<br>
																		<asp:listbox id="lbxValidations" runat="server" width="100%" height="300px" selectionmode="Multiple"></asp:listbox>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right">
															<asp:label id="lblDatagridIcons" runat="server"></asp:label>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblAllFormulasForField" runat="server">All formulas for field</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dtgrFields" runat="server" autogeneratecolumns="False" gridlines="None" cssclass="grid">
<footerstyle cssclass="grid_footer">
</FooterStyle>

<selecteditemstyle cssclass="grid_selecteditem">
</SelectedItemStyle>

<alternatingitemstyle cssclass="grid_alternatingitem">
</AlternatingItemStyle>

<itemstyle cssclass="grid_item">
</ItemStyle>

<headerstyle cssclass="grid_header">
</HeaderStyle>

<columns>
<asp:ButtonColumn Text="&lt;img src=&quot;../../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;" CommandName="Select">
<itemstyle cssclass="leftpadding">
</ItemStyle>
</asp:ButtonColumn>
<asp:ButtonColumn Text="&lt;img src=&quot;../../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;/&gt;" CommandName="Delete">
<itemstyle cssclass="nopadding">
</ItemStyle>
</asp:ButtonColumn>
<asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn HeaderText="Name">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="NameNative" HeaderText="NameNative">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="NameEN" HeaderText="NameEN">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn HeaderText="Description">
<headerstyle width="54%">
</HeaderStyle>

<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="DescriptionNative" HeaderText="DescriptionNative">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="DescriptionEN" HeaderText="DEscriptionEN">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
</Columns>

<pagerstyle cssclass="grid_pager">
</PagerStyle>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:textbox id="txtFieldID" runat="server" visible="False"></asp:textbox>
															<asp:textbox id="txtTemplateFieldID" runat="server" visible="False"></asp:textbox>
														</td>
														<td align="right">
															<asp:button id="btnAdd" runat="server" cssclass="confirm_button" text="Add"></asp:button>
															<br><br/>
															<asp:button id="btnBack" runat="server" cssclass="cancel_button" text="Back"></asp:button><asp:button id="btnNew" runat="server" cssclass="confirm_button" text="New"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>

									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
