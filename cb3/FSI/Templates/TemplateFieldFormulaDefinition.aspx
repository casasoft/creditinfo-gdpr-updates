<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="TemplateFieldFormulaDefinition.aspx.cs" AutoEventWireup="false" Inherits="FSI.Templates.TemplateFieldFormulaDefinition" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>TemplateFieldFormulaDefinition</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../../css/CIGStyles.css" type="text/css" rel="stylesheet">
	</head>
	<body ms_positioning="GridLayout">
		<form id="Template" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblPageTitle" runat="server">FSI Template</asp:label>
																	</td>
																	<td align="right">
																		<asp:label id="lblPageStep" runat="server">Step</asp:label>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td colspan="4">
																		<asp:label id="lblFormulaDefinition" runat="server" font-bold="True">Formula definition for field</asp:label>
																		<asp:textbox id="txtFieldID" runat="server" width="56px" visible="False"></asp:textbox>
																		<asp:textbox id="txtValidationID" runat="server" width="48px" visible="False"></asp:textbox>
																		<asp:textbox id="txtTemplateFieldID" runat="server" width="64px" visible="False"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td colspan="2">
																		<asp:label id="lblNameNative" runat="server">Name</asp:label><br>
																		<asp:textbox id="txtNameNative" runat="server" width="200px"></asp:textbox>
																	</td>
																	<td colspan="2">
																		<asp:label id="lblNameEN" runat="server">Name EN</asp:label><br>
																		<asp:textbox id="txtNameEN" runat="server" width="200px"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblDescriptionNative" runat="server">Descrition</asp:label>
																		<br>
																		<asp:textbox id="txtDescriptionNative" runat="server" width="100%" height="50px" textmode="MultiLine"></asp:textbox>
																	</td>
																	<td></td>
																	<td>
																		<asp:label id="lblDescriptionEN" runat="server">Description EN</asp:label>
																		<br>
																		<asp:textbox id="txtDescriptionEN" runat="server" width="100%" height="50px" textmode="MultiLine"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td valign="top" style="WIDTH: 350px">
																		<div>
																			<asp:label id="lblAvailableFields" runat="server">Available fields</asp:label>
																			<br>
																			<asp:listbox id="lbxFieldsInTemplate" runat="server" width="100%" height="242px" selectionmode="Multiple"></asp:listbox>
																		</div>
																		<br>
																		<div>
																			<asp:label id="lblOperators" runat="server">Operators</asp:label>
																			<br>
																			<asp:listbox id="lbxOperators" runat="server" width="100%" height="104px" selectionmode="Multiple"></asp:listbox>
																		</div>
																	</td>
																	<td>
																		<table height="150">
																			<tr>
																				<td>
																					<asp:imagebutton id="ibtnAddField" runat="server" imageurl="..\..\img\ARW08RT.ICO" width="16px" height="16px"></asp:imagebutton>
																				</td>
																			</tr>
																		</table>
																		<table height="100">
																			<tr>
																				<td>
																					<asp:imagebutton id="ibtnRemove" runat="server" imageurl="..\..\img\ARW08LT.ICO" width="16px" height="16px"></asp:imagebutton>
																				</td>
																			</tr>
																		</table>
																		<table height="100">
																			<tr>
																				<td>
																					<asp:imagebutton id="ibtnAddOperator" runat="server" imageurl="..\..\img\ARW08RT.ICO" width="16px"
																						height="16px"></asp:imagebutton>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td valign="top" style="WIDTH: 350px">
																		<asp:label id="lblFormula" runat="server">Formula</asp:label>
																		<br>
																		<asp:listbox id="lbxFormula" runat="server" width="100%" height="400px" selectionmode="Multiple"></asp:listbox>
																	</td>
																	<td>
																		<table>
																			<tr>
																				<td>
																					<asp:imagebutton id="ibtnMoveUp" runat="server" imageurl="..\..\img\ARW08UP.ICO" width="16px" height="16px"></asp:imagebutton>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<asp:imagebutton id="ibtnMoveDown" runat="server" imageurl="..\..\img\ARW08DN.ICO" width="16px" height="16px"></asp:imagebutton>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr valign="top">
														<td align="left">
															<asp:label id="lblErrMsg" runat="server" cssclass="error_text">Label</asp:label>
														</td>
														<td align="right">
															<asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel"></asp:button><asp:button id="btnAdd" runat="server" cssclass="confirm_button" text="Add"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
