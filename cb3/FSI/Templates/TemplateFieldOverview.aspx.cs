#region

using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FSI.Templates.BLL;
using Logging.BLL;

#endregion

namespace FSI.Templates {
    /// <summary>
    /// Summary description for TemplateFieldOverview.
    /// </summary>
    public class TemplateFieldOverview : BaseTemplatePage {
        protected Button btnBack;
        protected DataGrid dtgrFields;
        protected Label lblDatagridIcons;
        protected Label lblFieldValidations;
        protected Label lblPageStep;
        protected HtmlTable outerGridTable;
        protected TemplateFieldsCollectionBLLC tfColl;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "1007";
            PreparePageLoad();
            if (!Page.IsPostBack) {
                if (fsiTemplate > 0) {
                    LocalizeText();
                    FillDataGrid();
                } else {
                    Server.Transfer("Template.aspx");
                }
            }
        }

        protected void FillDataGrid() {
            tfColl = new TemplateFieldsCollectionBLLC(fsiTemplate);
            var coll = new FieldCollectionBLLC(fsiTemplate);
            dtgrFields.DataSource = coll;
            dtgrFields.DataBind();
        }

        protected void LocalizeText() {
            //lblPageTitle.Text = rm.GetString("txtFSITemplate", ci);
            lblPageStep.Text = rm.GetString("", ci);
            //lblAllFieldsInTemplate.Text = rm.GetString("txtAllFieldsInTemplate", ci);
            btnBack.Text = rm.GetString("txtBack", ci);
            if (fsiTemplate > 0) {
                var template = new TemplateBLLC(fsiTemplate);
                if (template != null) {
                    lblFieldValidations.Text += " " + rm.GetString("txtFor", ci) + " " + rm.GetString("txtTemplate", ci) +
                                                " ";
                    if (nativeCult) {
                        lblFieldValidations.Text += template.NameNative;
                    } else {
                        lblFieldValidations.Text += template.NameEN;
                    }
                }
            }
        }

        private void dtgrFields_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
                int fieldID = int.Parse(e.Item.Cells[2].Text);
                for (int i = 0; i < tfColl.Count; i++) {
                    TemplateFieldBLLC tf = tfColl.GetField(i);
                    if (tf.FieldID == fieldID) {
                        e.Item.Cells[1].Text = tf.ID.ToString();
                        break;
                    }
                }

                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[9].Text + "-" + e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[10].Text + "-" + e.Item.Cells[5].Text;
                    }
                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[10].Text + "-" + e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[9].Text + "-" + e.Item.Cells[4].Text;
                    }
                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }
                }

                WebDesign.CreateExplanationIcons(dtgrFields.Columns, lblDatagridIcons, rm, ci, 2);
            }
        }

        private void btnBack_Click(object sender, EventArgs e) { Server.Transfer("Template.aspx"); }

        private void dtgrFields_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                Context.Items.Add("FieldID", e.Item.Cells[2].Text);
                Context.Items.Add("TemplateFieldID", e.Item.Cells[1].Text);
                Server.Transfer("TemplateFieldFormulaOverview.aspx");
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dtgrFields.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrFields_ItemCommand);
            this.dtgrFields.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrFields_ItemDataBound);
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}