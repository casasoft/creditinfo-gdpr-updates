#region

using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FSI.Templates.BLL;
using Logging.BLL;

#endregion

namespace FSI.Templates {
    /// <summary>
    /// Summary description for TemplateFieldFormulaOverview.
    /// </summary>
    public class TemplateFieldFormulaOverview : BaseTemplatePage {
        protected Button btnAdd;
        protected Button btnBack;
        protected Button btnNew;
        protected DataGrid dtgrFields;
        protected Label lblAllFormulasForField;
        protected Label lblDatagridIcons;
        protected Label lblFieldValidationsFormulas;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        protected Label lblValidations;
        protected ListBox lbxValidations;
        protected HtmlTable outerGridTable;
        protected TextBox txtFieldID;
        protected TextBox txtTemplateFieldID;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "1008";
            PreparePageLoad();
            if (!Page.IsPostBack) {
                if ((!Context.Items.Contains("FieldID") && !Context.Items.Contains("TemplateFieldID")) ||
                    (Context.Items["FieldID"].ToString().Trim() == "" ||
                     Context.Items["TemplateFieldID"].ToString().Trim() == "")) {
                    Server.Transfer("TemplateFieldOverview.aspx");
                } else {
                    int fieldID = int.Parse(Context.Items["FieldID"].ToString());
                    int templateFieldID = int.Parse(Context.Items["TemplateFieldID"].ToString());
                    txtFieldID.Text = fieldID.ToString();
                    txtTemplateFieldID.Text = templateFieldID.ToString();
                    LocalizeText(fieldID);
                    FillDataGrid(templateFieldID);
                }
            }
        }

        protected void FillDataGrid(int templateFieldID) {
            FieldValidationCollectionBLLC coll = null;
            if (templateFieldID > 0) {
                coll = new FieldValidationCollectionBLLC(templateFieldID);
                dtgrFields.DataSource = coll;
                dtgrFields.DataBind();
            }
            FillAvailableValidations(coll);
        }

        protected void FillAvailableValidations(FieldValidationCollectionBLLC validations) {
            lbxValidations.Items.Clear();
            var coll = new FieldValidationCollectionBLLC(true);
            for (int i = 0; i < coll.Count; i++) {
                var validation = coll.GetField(i);
                if (validation == null) {
                    continue;
                }
                if (validations.ContainsValidationWithID(validation.ID)) {
                    continue;
                }
                lbxValidations.Items.Add(
                    new ListItem(
                        validation.NameNative + " - " + validation.DescriptionNative, validation.ID.ToString()));
            }
        }

        protected void LocalizeText(int fieldID) {
            lblPageTitle.Text = rm.GetString("txtFSITemplate", ci);
            lblPageStep.Text = rm.GetString("", ci);
            lblFieldValidationsFormulas.Text = rm.GetString("txtFieldValidations", ci);
            lblAllFormulasForField.Text = rm.GetString("txtValidationsForField", ci);
            btnNew.Text = rm.GetString("txtNew", ci);
            btnBack.Text = rm.GetString("txtBack", ci);
            if (fsiTemplate > 0) {
                var field = new FieldBLLC(fieldID);
                if (field != null) {
                    lblFieldValidationsFormulas.Text += " " + rm.GetString("txtFor", ci) + " ";
                    if (nativeCult) {
                        lblFieldValidationsFormulas.Text += field.NameNative;
                    } else {
                        lblFieldValidationsFormulas.Text += field.NameEN;
                    }
                }
            }
            lblFieldValidationsFormulas.Text += " - " + rm.GetString("txtFormulas", ci);
        }

        private void dtgrFields_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer) {
                return;
            }
            var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
            btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                            "')";

            //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
            //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
            WebDesign.CreateExplanationIcons(dtgrFields.Columns, lblDatagridIcons, rm, ci, 2);

            if (nativeCult) {
                //Set Name - Native if available, else EN
                if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                    e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                } else {
                    e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                }
                //Set Address - Native if available, else EN
                if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                    e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                } else {
                    e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                }
            } else {
                //Set Name - EN if available, else native
                if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                    e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                } else {
                    e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                }
                //Set Address - EN if available, else native
                if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                    e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                } else {
                    e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                }
            }
        }

        private void dtgrFields_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                Context.Items.Add("FieldID", txtFieldID.Text);
                Context.Items.Add("ValidationID", e.Item.Cells[2].Text);
                Context.Items.Add("TemplateFieldID", txtTemplateFieldID.Text);
                Server.Transfer("TemplateFieldFormulaDefinition.aspx");
            } else if (e.CommandName.Equals("Delete")) {
                var val = new FieldValidationBLLC
                          {
                              ID = int.Parse(e.Item.Cells[2].Text),
                              TemplateFieldID = int.Parse(txtTemplateFieldID.Text)
                          };
                if (val.RemoveValidationFromTemplateField()) {
                    FillDataGrid(int.Parse(txtTemplateFieldID.Text));
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e) {
            if (txtFieldID.Text.Trim() == "") {
                return;
            }
            Context.Items.Add("FieldID", txtFieldID.Text);
            Context.Items.Add("TemplateFieldID", txtTemplateFieldID.Text);
            Server.Transfer("TemplateFieldFormulaDefinition.aspx");
        }

        private void btnBack_Click(object sender, EventArgs e) { Server.Transfer("TemplateFieldOverview.aspx"); }

        private void btnAdd_Click(object sender, EventArgs e) {
            var coll = new FieldValidationCollectionBLLC();
            for (int i = 0; i < lbxValidations.Items.Count; i++) {
                if (!lbxValidations.Items[i].Selected) {
                    continue;
                }
                var field = new FieldValidationBLLC {ID = (int.Parse(lbxValidations.Items[i].Value))};
                coll.Add(field);
            }
            coll.TemplateFieldID = int.Parse(txtTemplateFieldID.Text);
            coll.AddValidationsToTemplateField();
            FillDataGrid(int.Parse(txtTemplateFieldID.Text));
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dtgrFields.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrFields_ItemCommand);
            this.dtgrFields.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrFields_ItemDataBound);
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}