﻿#region
using System.Collections;
#endregion

namespace FSI.Templates.BLL
{

    // This data matches database values in table [FSI_Templates]
    public enum TemplateType
    {
        MaltaOld = 1,
        Malta = 2
    }
}
