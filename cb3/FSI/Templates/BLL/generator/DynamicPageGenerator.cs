#region

using System.Data;
using System.Text;
using System.Xml;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL.generator {
    /// <summary>
    /// Summary description for DynamicPageGenerator.
    /// </summary>
    public class DynamicPageGenerator {
        public XmlDocument GeneratePage(int templateID, int afs_id, ref StringBuilder validations) {
            var coll = new TemplateFieldsCollectionBLLC(templateID);
            if (coll != null) {
                XmlDocument doc = XmlHelper.CreateDocument();
                XmlElement elForm = XmlHelper.CreateForm(doc);
                doc.AppendChild(elForm);
                XmlElement elPages = XmlHelper.CreatePages(doc);
                elForm.AppendChild(elPages);
                XmlElement elPage = XmlHelper.CreatePage(doc, "FSI Input", "FSI_INPUT");
                elPages.AppendChild(elPage);
                XmlElement elFields = XmlHelper.CreateFields(doc);
                elPage.AppendChild(elFields);
                int category = -1;
                var valGen = new ValidationGenerator();
                validations.Append(valGen.GenerateSaveAllowed(templateID));
                var dt = new DataTable();
                if (afs_id > 0) {
                    dt = GetValues(templateID, afs_id);
                }
                for (int i = 0; i < coll.Count; i++) {
                    TemplateFieldBLLC tField = coll.GetField(i);
                    var valColl = new FieldValidationCollectionBLLC(tField.ID);
                    var field = new FieldBLLC(tField.FieldID);
                    if (field.CategoryID != category) {
                        category = field.CategoryID;
                        elFields.AppendChild(XmlHelper.CreateHeader(doc, field.Category.NameNative, "ID" + i, "", ""));
                    }
                    if (afs_id > 0) {
                        if (valColl.Count > 0) {
                            string valNames = "";
                            string valName = "";
                            //Validation is for this field
                            for (int v = 0; v < valColl.Count; v++) {
                                validations.Append(
                                    valGen.GenerateValidationScript(field.ID, valColl.GetField(v).ID, ref valName));
                                if (valName != "") {
                                    valNames += valName;
                                }
                            }
                            elFields.AppendChild(
                                XmlHelper.CreateTextBox(
                                    doc, field.NameNative, field.FieldName, dt.Rows[0][field.FieldName], valNames));
                        } else {
                            elFields.AppendChild(
                                XmlHelper.CreateTextBox(
                                    doc, field.NameNative, field.FieldName, dt.Rows[0][field.FieldName], ""));
                        }
                    } else {
                        if (valColl.Count > 0) {
                            string valNames = "";
                            string valName = "";
                            //Validation is for this field
                            for (int v = 0; v < valColl.Count; v++) {
                                validations.Append(
                                    valGen.GenerateValidationScript(field.ID, valColl.GetField(v).ID, ref valName));
                                if (valName != "") {
                                    valNames += valName;
                                }
                            }
                            elFields.AppendChild(
                                XmlHelper.CreateTextBox(doc, field.NameNative, field.FieldName, null, valNames));
                        } else {
                            elFields.AppendChild(
                                XmlHelper.CreateTextBox(doc, field.NameNative, field.FieldName, null, ""));
                        }
                    }
                }
                return doc;
            }
            return null;
        }

        public XmlDocument GeneratePage(
            int templateID,
            int afs_id,
            int last_year_afs_id,
            ref StringBuilder validations,
            string lastYear,
            string currentYear) {
            var coll = new TemplateFieldsCollectionBLLC(templateID);
            if (coll != null) {
                XmlDocument doc = XmlHelper.CreateDocument();
                XmlElement elForm = XmlHelper.CreateForm(doc);
                doc.AppendChild(elForm);
                XmlElement elPages = XmlHelper.CreatePages(doc);
                elForm.AppendChild(elPages);
                XmlElement elPage = XmlHelper.CreatePage(doc, "FSI Input", "FSI_INPUT");
                elPages.AppendChild(elPage);
                XmlElement elFields = XmlHelper.CreateFields(doc);
                elPage.AppendChild(elFields);
                int category = -1;
                var valGen = new ValidationGenerator();
                validations.Append(valGen.GenerateSaveAllowed(templateID));
                var dt = new DataTable();
                if (afs_id > 0) {
                    dt = GetValues(templateID, afs_id);
                }
                DataTable lastYearDT = GetValues(templateID, last_year_afs_id);
                for (int i = 0; i < coll.Count; i++) {
                    TemplateFieldBLLC tField = coll.GetField(i);
                    var valColl = new FieldValidationCollectionBLLC(tField.ID);
                    var field = new FieldBLLC(tField.FieldID);
                    if (field.CategoryID != category) {
                        category = field.CategoryID;
                        elFields.AppendChild(
                            XmlHelper.CreateHeader(doc, field.Category.NameNative, "ID" + i, lastYear, currentYear));
                    }
                    if (afs_id > 0) {
                        if (valColl.Count > 0) {
                            string valNames = "";
                            string valName = "";
                            //Validation is for this field
                            for (int v = 0; v < valColl.Count; v++) {
                                validations.Append(
                                    valGen.GenerateValidationScript(field.ID, valColl.GetField(v).ID, ref valName));
                                if (valName != "") {
                                    valNames += valName;
                                }
                            }

                            elFields.AppendChild(
                                XmlHelper.CreateTextBox(
                                    doc,
                                    field.NameNative,
                                    field.FieldName,
                                    dt.Rows[0][field.FieldName],
                                    lastYearDT.Rows[0][field.FieldName],
                                    valNames));
                        } else {
                            elFields.AppendChild(
                                XmlHelper.CreateTextBox(
                                    doc,
                                    field.NameNative,
                                    field.FieldName,
                                    dt.Rows[0][field.FieldName],
                                    lastYearDT.Rows[0][field.FieldName],
                                    ""));
                        }
                    } else {
                        if (valColl.Count > 0) {
                            string valNames = "";
                            string valName = "";
                            //Validation is for this field
                            for (int v = 0; v < valColl.Count; v++) {
                                validations.Append(
                                    valGen.GenerateValidationScript(field.ID, valColl.GetField(v).ID, ref valName));
                                if (valName != "") {
                                    valNames += valName;
                                }
                            }
                            elFields.AppendChild(
                                XmlHelper.CreateTextBox(
                                    doc,
                                    field.NameNative,
                                    field.FieldName,
                                    null,
                                    lastYearDT.Rows[0][field.FieldName],
                                    valNames));
                        } else {
                            elFields.AppendChild(
                                XmlHelper.CreateTextBox(
                                    doc,
                                    field.NameNative,
                                    field.FieldName,
                                    null,
                                    lastYearDT.Rows[0][field.FieldName],
                                    ""));
                        }
                    }
                }
                return doc;
            }
            return null;
        }

        public XmlDocument GeneratePage(int templateID, FieldCollectionBLLC fieldValues, ref StringBuilder validations) {
            var coll = new TemplateFieldsCollectionBLLC(templateID);
            if (coll != null) {
                XmlDocument doc = XmlHelper.CreateDocument();
                XmlElement elForm = XmlHelper.CreateForm(doc);
                doc.AppendChild(elForm);
                XmlElement elPages = XmlHelper.CreatePages(doc);
                elForm.AppendChild(elPages);
                XmlElement elPage = XmlHelper.CreatePage(doc, "FSI Input", "FSI_INPUT");
                elPages.AppendChild(elPage);
                XmlElement elFields = XmlHelper.CreateFields(doc);
                elPage.AppendChild(elFields);
                int category = -1;
                var valGen = new ValidationGenerator();
                validations.Append(valGen.GenerateSaveAllowed(templateID));
                /*DataTable dt = new DataTable();
				if(afs_id>0)
					dt = GetValues(templateID, afs_id);*/
                for (int i = 0; i < coll.Count; i++) {
                    TemplateFieldBLLC tField = coll.GetField(i);
                    var valColl = new FieldValidationCollectionBLLC(tField.ID);
                    var field = new FieldBLLC(tField.FieldID);

                    int fieldValue = 0;
                    //Finna value fyrir �etta field.
                    for (int b = 0; b < fieldValues.Count; b++) {
                        if (fieldValues.GetField(b).FieldName == field.FieldName) {
                            if (fieldValues.GetField(b).Value != null && fieldValues.GetField(b).Value.ToString() != "") {
                                fieldValue = int.Parse(fieldValues.GetField(b).Value.ToString());
                            }
                        }
                    }
                    if (field.CategoryID != category) {
                        category = field.CategoryID;
                        elFields.AppendChild(XmlHelper.CreateHeader(doc, field.Category.NameNative, "ID" + i, "", ""));
                    }
                    if (valColl.Count > 0) {
                        string valNames = "";
                        string valName = "";
                        //Validation is for this field
                        for (int v = 0; v < valColl.Count; v++) {
                            validations.Append(
                                valGen.GenerateValidationScript(field.ID, valColl.GetField(v).ID, ref valName));
                            if (valName != "") {
                                valNames += valName;
                            }
                        }
                        elFields.AppendChild(
                            XmlHelper.CreateTextBox(doc, field.NameNative, field.FieldName, fieldValue, valNames));
                    } else {
                        elFields.AppendChild(
                            XmlHelper.CreateTextBox(doc, field.NameNative, field.FieldName, fieldValue, ""));
                    }
                }
                return doc;
            }
            return null;
        }

        public XmlDocument GeneratePage(
            int templateID,
            int last_year_afs_id,
            FieldCollectionBLLC fieldValues,
            ref StringBuilder validations,
            string lastYear,
            string currentYear) {
            var coll = new TemplateFieldsCollectionBLLC(templateID);
            if (coll != null) {
                XmlDocument doc = XmlHelper.CreateDocument();
                XmlElement elForm = XmlHelper.CreateForm(doc);
                doc.AppendChild(elForm);
                XmlElement elPages = XmlHelper.CreatePages(doc);
                elForm.AppendChild(elPages);
                XmlElement elPage = XmlHelper.CreatePage(doc, "FSI Input", "FSI_INPUT");
                elPages.AppendChild(elPage);
                XmlElement elFields = XmlHelper.CreateFields(doc);
                elPage.AppendChild(elFields);
                int category = -1;
                var valGen = new ValidationGenerator();
                validations.Append(valGen.GenerateSaveAllowed(templateID));
                /*DataTable dt = new DataTable();
				if(afs_id>0)
					dt = GetValues(templateID, afs_id);*/
                DataTable lastYearDT = GetValues(templateID, last_year_afs_id);
                for (int i = 0; i < coll.Count; i++) {
                    TemplateFieldBLLC tField = coll.GetField(i);
                    var valColl = new FieldValidationCollectionBLLC(tField.ID);
                    var field = new FieldBLLC(tField.FieldID);

                    int fieldValue = 0;
                    //Finna value fyrir �etta field.
                    for (int b = 0; b < fieldValues.Count; b++) {
                        if (fieldValues.GetField(b).FieldName == field.FieldName) {
                            if (fieldValues.GetField(b).Value != null && fieldValues.GetField(b).Value.ToString() != "") {
                                fieldValue = int.Parse(fieldValues.GetField(b).Value.ToString());
                            }
                        }
                    }

                    if (field.CategoryID != category) {
                        category = field.CategoryID;
                        elFields.AppendChild(
                            XmlHelper.CreateHeader(doc, field.Category.NameNative, "ID" + i, lastYear, currentYear));
                    }

                    if (valColl.Count > 0) {
                        string valNames = "";
                        string valName = "";
                        //Validation is for this field
                        for (int v = 0; v < valColl.Count; v++) {
                            validations.Append(
                                valGen.GenerateValidationScript(field.ID, valColl.GetField(v).ID, ref valName));
                            if (valName != "") {
                                valNames += valName;
                            }
                        }
                        elFields.AppendChild(
                            XmlHelper.CreateTextBox(
                                doc,
                                field.NameNative,
                                field.FieldName,
                                fieldValue,
                                lastYearDT.Rows[0][field.FieldName],
                                valNames));
                    } else {
                        elFields.AppendChild(
                            XmlHelper.CreateTextBox(
                                doc,
                                field.NameNative,
                                field.FieldName,
                                fieldValue,
                                lastYearDT.Rows[0][field.FieldName],
                                ""));
                    }
                }
                return doc;
            }
            return null;
        }

        protected static DataTable GetValues(int templateID, int afs_id) {
            var dalc = new StatementsDALC();
            return dalc.GetTemplateStatementsValues(templateID, afs_id);
        }
    }
}