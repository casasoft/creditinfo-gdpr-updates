#region

using System.Xml;

#endregion

namespace FSI.Templates.BLL.generator {
    /// <summary>
    /// Summary description for XmlHelper.
    /// </summary>
    public class XmlHelper {
        /// <summary>
        /// Creates a new empty xml document
        /// </summary>
        /// <returns>The new xml document</returns>
        public static XmlDocument CreateDocument() {
            var doc = new XmlDocument();
            XmlDeclaration decl = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(decl);
            return doc;
        }

        public static XmlElement CreateForm(XmlDocument doc) { return CreateElement(doc, "FORM", ""); }
        public static XmlElement CreatePages(XmlDocument doc) { return CreateElement(doc, "PAGES", ""); }
        public static XmlElement CreateFields(XmlDocument doc) { return CreateElement(doc, "FIELDS", ""); }

        public static XmlElement CreateTextBox(XmlDocument doc, string label, string id) {
            XmlElement el = CreateElement(doc, "FIELD", "");
            AddAttribute(doc, el, "type", "TextBox");
            AddAttribute(doc, el, "label", label);
            XmlElement elProperties = CreateElement(doc, "PROPERTIES", "");
            el.AppendChild(elProperties);
            XmlElement elProperty = CreateElement(doc, "PROPERTY", id);
            AddAttribute(doc, elProperty, "name", "ID");
            elProperties.AppendChild(elProperty);
            return el;
        }

        public static XmlElement CreateTextBox(XmlDocument doc, string label, string id, object val) {
            XmlElement el = CreateElement(doc, "FIELD", "");
            AddAttribute(doc, el, "type", "TextBox");
            AddAttribute(doc, el, "label", label);
            XmlElement elProperties = CreateElement(doc, "PROPERTIES", "");
            el.AppendChild(elProperties);
            XmlElement elProperty = CreateElement(doc, "PROPERTY", id);
            AddAttribute(doc, elProperty, "name", "ID");
            elProperties.AppendChild(elProperty);

            if (val != null) {
                XmlElement elProperty2 = CreateElement(doc, "PROPERTY", val.ToString());
                AddAttribute(doc, elProperty2, "name", "Value");
                elProperties.AppendChild(elProperty2);
            }
            return el;
        }

        public static XmlElement CreateTextBox(XmlDocument doc, string label, string id, object val, string valNames) {
            XmlElement el = CreateElement(doc, "FIELD", "");
            AddAttribute(doc, el, "type", "TextBox");
            AddAttribute(doc, el, "label", label);
            XmlElement elProperties = CreateElement(doc, "PROPERTIES", "");
            el.AppendChild(elProperties);
            XmlElement elProperty = CreateElement(doc, "PROPERTY", id);
            AddAttribute(doc, elProperty, "name", "ID");
            elProperties.AppendChild(elProperty);

            if (val != null) {
                XmlElement elProperty2 = CreateElement(doc, "PROPERTY", val.ToString());
                AddAttribute(doc, elProperty2, "name", "Value");
                elProperties.AppendChild(elProperty2);
            }

            XmlElement elProperty3 = CreateElement(doc, "PROPERTY", valNames + "input(this)");
            AddAttribute(doc, elProperty3, "name", "onblur");
            elProperties.AppendChild(elProperty3);

            return el;
        }

        public static XmlElement CreateTextBoxDescription(
            XmlDocument doc, string label, string id, string description, object val, string valNames) {
            XmlElement el = CreateElement(doc, "FIELD", "");
            AddAttribute(doc, el, "type", "TextBox");
            AddAttribute(doc, el, "label", label);
            AddAttribute(doc, el, "description", description);
            XmlElement elProperties = CreateElement(doc, "PROPERTIES", "");
            el.AppendChild(elProperties);
            XmlElement elProperty = CreateElement(doc, "PROPERTY", id);
            AddAttribute(doc, elProperty, "name", "ID");
            elProperties.AppendChild(elProperty);

            if (val != null) {
                XmlElement elProperty2 = CreateElement(doc, "PROPERTY", val.ToString());
                AddAttribute(doc, elProperty2, "name", "Value");
                elProperties.AppendChild(elProperty2);
            }

            XmlElement elProperty3 = CreateElement(doc, "PROPERTY", valNames + "input(this)");
            AddAttribute(doc, elProperty3, "name", "onblur");
            elProperties.AppendChild(elProperty3);

            return el;
        }

        public static XmlElement CreateTextBox(
            XmlDocument doc, string label, string id, object val, object oldVal, string valNames) {
            XmlElement el = CreateElement(doc, "FIELD", "");
            AddAttribute(doc, el, "type", "TextBox");
            AddAttribute(doc, el, "label", label);
            AddAttribute(doc, el, "oldval", "true");
            AddAttribute(doc, el, "oldvalValue", oldVal.ToString());
            XmlElement elProperties = CreateElement(doc, "PROPERTIES", "");
            el.AppendChild(elProperties);
            XmlElement elProperty = CreateElement(doc, "PROPERTY", id);
            AddAttribute(doc, elProperty, "name", "ID");
            elProperties.AppendChild(elProperty);

            if (val != null) {
                XmlElement elProperty2 = CreateElement(doc, "PROPERTY", val.ToString());
                AddAttribute(doc, elProperty2, "name", "Value");
                elProperties.AppendChild(elProperty2);
            }

            XmlElement elProperty3 = CreateElement(doc, "PROPERTY", valNames + "input(this)");
            AddAttribute(doc, elProperty3, "name", "onblur");
            elProperties.AppendChild(elProperty3);

            return el;
        }

        public static XmlElement CreateLabel(XmlDocument doc, string label, string id) {
            XmlElement el = CreateElement(doc, "FIELD", "");
            AddAttribute(doc, el, "type", "Label");
            AddAttribute(doc, el, "label", label);
            XmlElement elProperties = CreateElement(doc, "PROPERTIES", "");
            el.AppendChild(elProperties);
            XmlElement elProperty = CreateElement(doc, "PROPERTY", id);
            AddAttribute(doc, elProperty, "name", "ID");
            elProperties.AppendChild(elProperty);
            return el;
        }

        public static XmlElement CreateHeader(
            XmlDocument doc, string label, string id, string lastYear, string currentYear) {
            XmlElement el = CreateElement(doc, "FIELD", "");
            AddAttribute(doc, el, "type", "Header");
            AddAttribute(doc, el, "label", label);
            AddAttribute(doc, el, "lastYear", lastYear);
            AddAttribute(doc, el, "currentYear", currentYear);
            XmlElement elProperties = CreateElement(doc, "PROPERTIES", "");
            el.AppendChild(elProperties);
            XmlElement elProperty = CreateElement(doc, "PROPERTY", id);
            AddAttribute(doc, elProperty, "name", "ID");
            elProperties.AppendChild(elProperty);
            return el;
        }

        public static XmlElement CreatePage(XmlDocument doc, string title, string id) {
            XmlElement el = CreateElement(doc, "PAGE", "");
            AddAttribute(doc, el, "title", title);
            AddAttribute(doc, el, "id", id);
            return el;
        }

        /// <summary>
        /// Creates a new attribute and adds it to given xml element
        /// </summary>
        /// <param name="doc">The document to create attribute for</param>
        /// <param name="el">The element to add attribute to</param>
        /// <param name="name">The name of the attribute</param>
        /// <param name="val">The value of the attribute</param>
        /// <returns>the xml element with the new attribute added</returns>
        public static XmlElement AddAttribute(XmlDocument doc, XmlElement el, string name, string val) {
            XmlAttribute att = doc.CreateAttribute(name);
            att.InnerText = val;
            el.Attributes.Append(att);
            return el;
        }

        /// <summary>
        /// Creates a new xml element with given name and value
        /// </summary>
        /// <param name="doc">The xml document to create element for</param>
        /// <param name="name">The name of the element</param>
        /// <param name="val">The value for the element</param>
        /// <returns>The new xml element</returns>
        public static XmlElement CreateElement(XmlDocument doc, string name, string val) {
            XmlElement el = doc.CreateElement(name);
            if (val == "") {
                val = " ";
            }
            el.InnerText = val;
            return el;
        }
    }
}