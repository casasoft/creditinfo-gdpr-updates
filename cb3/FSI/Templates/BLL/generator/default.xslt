<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="remove">
	<xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"></xsl:output>
	<xsl:param name="pageid"/>
	
	<xsl:template match="/">		
		<!-- start form page -->
		<!-- set title of the current page -->
		<!--
			<tr><td colspan="3" align="center" style="font-size:25px"><xsl:value-of select="FORM/PAGES/PAGE[@id=$pageid]/@title" />
			</td></tr><tr><td colspan="3" style="height:20px"></td></tr>
		-->
		<!-- iterate through page fields -->
		<xsl:for-each select="FORM/PAGES/PAGE[@id=$pageid]/FIELDS/FIELD">
			<xsl:choose>
				<!-- html control -->
				<xsl:when test="@type='HTML'">
					<xsl:element name="td">
						<xsl:attribute name="colspan">3</xsl:attribute>
						<xsl:text><![CDATA[&lt;]]></xsl:text>
						<xsl:text>!-- #include file="</xsl:text>
						<xsl:value-of select="@src"></xsl:value-of>
						<xsl:text>" --</xsl:text>
						<xsl:text><![CDATA[&gt;]]></xsl:text>
					</xsl:element>
				</xsl:when>
				<!-- other controls -->
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="@type='Header'">
							<!-- field label column -->
							<xsl:if test="position() &gt; 1">
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:attribute name="height">23</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<xsl:text><![CDATA[</table></td></tr></table></td></tr>]]></xsl:text>
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:attribute name="height">10</xsl:attribute>
									</xsl:element>
								</xsl:element>
							</xsl:if><![CDATA[<tr><td><table class="grid_table" cellSpacing="0" cellPadding="0">]]>

							<xsl:element name="tr">
								<xsl:attribute name="id">TR_<xsl:value-of select="PROPERTIES/PROPERTY[@name='ID']"></xsl:value-of></xsl:attribute>
								<xsl:attribute name="width">100%</xsl:attribute>
								<!-- hide the row -->
								<xsl:if test="@display='none'">
									<xsl:attribute name="style">display:none;</xsl:attribute>
								</xsl:if>
								<xsl:element name="th">
									<xsl:element name="table">
										<xsl:attribute name="border">0</xsl:attribute>
										<xsl:attribute name="class">
											<xsl:text>grid_table_head</xsl:text>
										</xsl:attribute>
										<xsl:element name="tr">
											<xsl:element name="td">
												<xsl:attribute name="width">38%</xsl:attribute>
												<xsl:value-of select="@label"/>
											</xsl:element>
											<xsl:if test="@lastYear!='ID'">
												<xsl:element name="td">
													<xsl:attribute name="align">
														<xsl:text>right</xsl:text>
													</xsl:attribute>
													<xsl:attribute name="width">20%</xsl:attribute>
													<xsl:value-of select="@lastYear"></xsl:value-of>
													<xsl:text>&#xA0;&#xA0;</xsl:text>
													<!--
														<xsl:element name="asp:TextBox">
															<xsl:attribute name="runat">server</xsl:attribute>
															<xsl:attribute name="width">100px</xsl:attribute>										
															<xsl:attribute name="tabIndex">-1</xsl:attribute>
															<xsl:attribute name="readOnly"></xsl:attribute>
															<xsl:attribute name="value"><xsl:value-of select="@lastYear"></xsl:value-of></xsl:attribute>
														</xsl:element>
														-->
												</xsl:element>
											</xsl:if>
											<xsl:if test="@currentYear!=''">
												<xsl:element name="td">
													<xsl:attribute name="align">
														<xsl:text>right</xsl:text>
													</xsl:attribute>
													<xsl:attribute name="width">20%</xsl:attribute>
													<xsl:value-of select="@currentYear"/>
													<xsl:text>&#xA0;&#xA0;</xsl:text>
												</xsl:element>
												<xsl:element name="td">
												</xsl:element>
											</xsl:if>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							<xsl:text><![CDATA[<tr><td><table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0" border="0"><tr><td>]]></xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<!-- field label column -->
							<xsl:element name="tr">
								<xsl:attribute name="id">TR_<xsl:value-of select="PROPERTIES/PROPERTY[@name='ID']"></xsl:value-of></xsl:attribute>
								<xsl:attribute name="width">100%</xsl:attribute>
								<!-- hide the row -->
								<xsl:if test="@display='none'">
									<xsl:attribute name="style">display:none;</xsl:attribute>
								</xsl:if>

								<xsl:element name="td">
									<xsl:attribute name="valign">
										<xsl:text>top</xsl:text>
									</xsl:attribute>
									<xsl:attribute name="width">40%</xsl:attribute>
									<xsl:value-of select="@label"/>
								</xsl:element>

								<xsl:if test="@oldval='true'">
									<xsl:element name="td">
										<xsl:attribute name="valign">
											<xsl:text>top</xsl:text>
										</xsl:attribute>
										<xsl:attribute name="align">
											<xsl:text>right</xsl:text>
										</xsl:attribute>
										<xsl:attribute name="width">20%</xsl:attribute>

										<xsl:element name="asp:{@type}">
											<xsl:attribute name="runat">server</xsl:attribute>
											<xsl:attribute name="class">FSILastYear</xsl:attribute>
											<xsl:attribute name="width">100px</xsl:attribute>
											<xsl:attribute name="tabIndex">-1</xsl:attribute>
											<xsl:attribute name="readOnly"></xsl:attribute>
											<xsl:attribute name="value">
												<xsl:value-of select="@oldvalValue"></xsl:value-of>
											</xsl:attribute>
											<xsl:for-each select="./PROPERTIES/PROPERTY">
												<xsl:if test="@name='ID'">
													<xsl:attribute name="{@name}">
														<xsl:value-of select="current()"></xsl:value-of>_OLD_VALUE</xsl:attribute>
												</xsl:if>
											</xsl:for-each>
										</xsl:element>
									</xsl:element>
								</xsl:if>

								<!-- field column -->
								<xsl:element name="td">
									<xsl:attribute name="valign">
										<xsl:text>top</xsl:text>
									</xsl:attribute>
									<xsl:attribute name="align">
										<xsl:text>right</xsl:text>
									</xsl:attribute>
									<xsl:attribute name="width">20%</xsl:attribute>
									<!-- field element -->
									<xsl:element name="asp:{@type}">
										<xsl:attribute name="runat">server</xsl:attribute>
										<xsl:attribute name="CssClass">input_text</xsl:attribute>
										<xsl:attribute name="onfocus">input_on(this)</xsl:attribute>
										<xsl:attribute name="onkeypress">return handle_int()</xsl:attribute>
										<xsl:attribute name="onkeydown">return handle_exit();</xsl:attribute>
										<xsl:attribute name="width">100px</xsl:attribute>
										<xsl:for-each select="./PROPERTIES/PROPERTY">
											<xsl:attribute name="{@name}">
												<xsl:value-of select="current()"></xsl:value-of>
											</xsl:attribute>
										</xsl:for-each>
										<xsl:for-each select="./LISTITEMS/LISTITEM">
											<asp:ListItem value="{@value}">
												<xsl:value-of select="current()"></xsl:value-of>
											</asp:ListItem>
										</xsl:for-each>
									</xsl:element>
								</xsl:element>

								<xsl:element name="td">
									<xsl:attribute name="valign">
										<xsl:text>top</xsl:text>
									</xsl:attribute>
									<xsl:attribute name="width">20%</xsl:attribute>
									<xsl:element name="asp:{@type}">
										<xsl:attribute name="runat">server</xsl:attribute>
										<xsl:attribute name="class">FSICheck</xsl:attribute>
										<xsl:attribute name="tabIndex">-1</xsl:attribute>
										<xsl:attribute name="readOnly"></xsl:attribute>
										<xsl:for-each select="./PROPERTIES/PROPERTY">
											<xsl:if test="@name='ID'">
												<xsl:attribute name="{@name}">
													<xsl:value-of select="current()"></xsl:value-of>_ERR</xsl:attribute>
											</xsl:if>
										</xsl:for-each>
									</xsl:element>
								</xsl:element>

								<!-- validation message column -->
								<xsl:element name="td">
									<xsl:if test="@required='true'">
										<asp:RequiredFieldValidator ErrorMessage="Required" runat="server" ControlToValidate="{PROPERTIES/PROPERTY[@name='ID']}"/>
									</xsl:if>

									<xsl:if test="@validation='Date'">
										<asp:CompareValidator ErrorMessage="Dates Only" runat="server" Operator="DataTypeCheck" Type="Date" ControlToValidate="{PROPERTIES/PROPERTY[@name='ID']}"/>
									</xsl:if>

									<xsl:if test="@validation='Number'">
										<asp:CompareValidator ErrorMessage="Numbers Only" runat="server" Operator="DataTypeCheck" Type="Integer" ControlToValidate="{PROPERTIES/PROPERTY[@name='ID']}"/>
									</xsl:if>

									<xsl:if test="@validation='Currency'">
										<asp:CompareValidator ErrorMessage="Currency Only" runat="server" Operator="DataTypeCheck" Type="Currency" ControlToValidate="{PROPERTIES/PROPERTY[@name='ID']}"/>
									</xsl:if>
								</xsl:element>
							</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>

		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">23</xsl:attribute>
			</xsl:element>
		</xsl:element>
		<xsl:text><![CDATA[</table></td></tr></table></td></tr>]]></xsl:text>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Sample" userelativepaths="yes" externalpreview="no" url="example.xml" htmlbaseurl="" outputurl="output.htm" processortype="msxmldotnet" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->