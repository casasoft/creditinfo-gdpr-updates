#region

using System.Text;

#endregion

namespace FSI.Templates.BLL.generator {
    /// <summary>
    /// Summary description for ValidationGenerator.
    /// </summary>
    public class ValidationGenerator {
        public string GenerateValidationScript(int fieldID, int validationID, ref string validationName) {
            var field = new FieldBLLC(fieldID);
            var validation = new FieldValidationBLLC(validationID);

            var coll = new FieldValidationFormulaCollectionBLLC(validationID);
            if (coll != null && coll.Count > 0) {
                var builder = new StringBuilder();
                builder.Append("function ");
                builder.Append(field.FieldName);
                builder.Append("_");
                builder.Append(validation.NameNative.Replace(' ', '_'));
                validationName = field.FieldName + "_" + validation.NameNative.Replace(' ', '_') + "();";
                builder.Append("()\r\n");
                builder.Append("{\r\n");
                builder.Append("\tvar valu=\"\";\r\n");
                builder.Append("\tif (");
                bool firstFieldOver = false;
                for (int i = 0; i < coll.Count; i++) {
                    var formula = coll.GetFormula(i);
                    if (!formula.IsField) {
                        continue;
                    }
                    if (firstFieldOver) {
                        builder.Append("||");
                    }
                    var f = formula.Field;
                    builder.Append("f_arsr.");
                    builder.Append(f.FieldName);
                    builder.Append(".value==\"\"");
                    firstFieldOver = true;
                }
                builder.Append(")\r\n");
                builder.Append("\t\tvalu=\"*\";\r\n");
                builder.Append("\telse{\r\n");
                builder.Append("\t\tif (");
                for (int i = 0; i < coll.Count; i++) {
                    var formula = coll.GetFormula(i);
                    if (formula.IsField) {
                        var f = formula.Field;
                        builder.Append("parseInt(");
                        builder.Append("f_arsr.");
                        builder.Append(f.FieldName);
                        builder.Append(".value");
                        builder.Append(")");
                    } else if (formula.IsOperator) {
                        OperatorBLLC oper = formula.Operator;
                        builder.Append(" ");
                        builder.Append(oper.Symbol);
                        if (oper.Symbol == "=") {
                            builder.Append(oper.Symbol);
                        }
                        builder.Append(" ");
                    }
                }
                builder.Append(" )\r\n");
                builder.Append("\t\t\tvalu=\"\";\r\n");
                builder.Append("\t\telse\r\n");
                builder.Append("\t\t\tvalu=");

                for (int i = 0; i < coll.Count - 2; i++) {
                    FieldValidationFormulaBLLC formula = coll.GetFormula(i);
                    if (formula.IsField) {
                        FieldBLLC f = formula.Field;
                        builder.Append("parseInt(");
                        builder.Append("f_arsr.");
                        builder.Append(f.FieldName);
                        builder.Append(".value");
                        builder.Append(")");
                    } else if (formula.IsOperator) {
                        OperatorBLLC oper = formula.Operator;
                        builder.Append(" ");
                        builder.Append(oper.Symbol);
                        builder.Append(" ");
                    }
                }
                builder.Append(";\r\n");
                builder.Append("\t}\r\n");
                builder.Append("\t");
                builder.Append("f_arsr.");

                builder.Append(coll.GetFormula(coll.Count - 1).Field.FieldName);
                builder.Append("_");
                builder.Append("ERR.value=valu;\r\n");
                builder.Append("\tSaveAllowed();\r\n");
                builder.Append("}\r\n");
                return builder.ToString();
            }
            return null;
        }

        public string GenerateSaveAllowed(int templateID) {
            var coll = new FieldCollectionBLLC(templateID);
            if (coll.Count > 0) {
                var builder = new StringBuilder();
                builder.Append("function ");
                builder.Append("SaveAllowed()");
                builder.Append("{\r\n");
                builder.Append("\tif(");

                for (int i = 0; i < coll.Count; i++) {
                    if (i > 0) {
                        builder.Append("&&");
                    }

                    FieldBLLC f = coll.GetField(i);
                    builder.Append("f_arsr.");
                    builder.Append(f.FieldName);
                    builder.Append("_ERR.value==\"\"");
                }
                builder.Append("){\r\n");
                builder.Append("\t\tf_arsr.btnSubmit.className=\"confirm_button\";\r\n");
                builder.Append("\t\treturn true;");
                builder.Append("}");
                builder.Append("\telse{\r\n");
                builder.Append("\t\tf_arsr.btnSubmit.className=\"cancel_button\";\r\n");
                builder.Append("\t\treturn false;");
                builder.Append("}");
                builder.Append("}\r\n");
                return builder.ToString();
            }
            return "";
        }
    }
}