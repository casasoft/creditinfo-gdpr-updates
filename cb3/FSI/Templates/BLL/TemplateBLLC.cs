#region

using System.Collections;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL {
    /// <summary>
    /// Summary description for Template.
    /// </summary>
    public class TemplateBLLC {
        private TemplatesDALC dalc;
        private TemplateFieldsCollectionBLLC fields;
        private int id;
        public TemplateBLLC() { IsNew = true; }

        public TemplateBLLC(int id) {
            ID = id;
            IsNew = !DAL.FillTemplate(this);
        }

        protected TemplatesDALC DAL {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }

        public bool IsNew { get; set; }
        public int ID { get { return id; } set { id = value; } }
        public string NameNative { get; set; }
        public string NameEN { get; set; }
        public string DescriptionNative { get; set; }
        public string DescriptionEN { get; set; }

        public TemplateFieldsCollectionBLLC Fields {
            get {
                if (fields == null) {
                    fields = new TemplateFieldsCollectionBLLC(id);
                }
                return fields;
            }
            set { fields = value; }
        }

        public bool Save() {
            if (DAL.SaveTemplate(this)) {
                Fields.TemplateID = ID;
                for (int i = 0; i < Fields.Count; i++) {
                    TemplateFieldBLLC field = Fields.GetField(i);
                    field.TemplateID = ID;
                    field.IsNew = true;
                }
                Fields.Save();
                return true;
            }
            return false;
        }

        public bool Delete() { return DAL.DeleteTemplate(this); }
        public bool AreSomeFSUsingTemplate() { return DAL.AreSomeFSUsingTemplate(this); }
    }

    public class TemplateCollectionBLLC : ArrayList {
        protected TemplatesDALC dalc;
        //protected ArrayList templates;
        public TemplateCollectionBLLC() { DAL.FillTemplates(this); }

        protected TemplatesDALC DAL {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }

        public TemplateBLLC GetField(int i) {
            if (i < Count) {
                return (TemplateBLLC) this[i];
            }
            return null;
        }
    }
}