#region

using System;

#endregion

namespace FSI.Templates.BLL {
    /// <remarks>
    /// This class provides all the neccessary info for Report.  Inherits Company
    /// </remarks>
    [Serializable]
    public class CompanyBLLC {
        /// <summary>
        /// Gets/sets the name native attribute
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// Gets/sets the name en attribute
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Gets/sets the uniqueID attribute
        /// </summary>
        public string UniqueID { get; set; }

        /// <summary>
        /// Gets/sets the companyCIID attribute
        /// </summary>
        public int CompanyCIID { get; set; }
    } // END CLASS DEFINITION ReportCompanyBLLC
}