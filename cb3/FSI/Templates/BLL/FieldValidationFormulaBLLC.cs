#region

using System.Collections;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL {
    /// <summary>
    /// Summary description for FieldValidationFormulas.
    /// </summary>
    public class FieldValidationFormulaBLLC {
        public static string FIELD = "FIELD";
        public static string OPERATOR = "OPERATOR";
        private int validationItemID;
        private string validationItemType;
        public int ID { get; set; }
        public int ValidationID { get; set; }
        public string ValidationItemType { get { return validationItemType; } set { validationItemType = value; } }
        public int ValidationItemID { get { return validationItemID; } set { validationItemID = value; } }
        public bool IsModified { get; set; }
        public bool IsNew { get; set; }
        public bool IsDeleted { get; set; }

        public bool IsOperator {
            get {
                return validationItemType == OPERATOR;
            }
        }

        public bool IsField {
            get {
                return validationItemType == FIELD;
            }
        }

        public OperatorBLLC Operator {
            get {
                return IsOperator ? new OperatorBLLC(validationItemID) : null;
            }
        }

        public FieldBLLC Field {
            get {
                return IsField ? new FieldBLLC(validationItemID) : null;
            }
        }

        public int Order { get; set; }
    }

    public class FieldValidationFormulaCollectionBLLC : ArrayList {
        protected TemplatesDALC dalc;
        protected int validationID = -1;
        public FieldValidationFormulaCollectionBLLC() { }
        public FieldValidationFormulaCollectionBLLC(int validationID) { DALC.FillFieldValidationFormulas(this, validationID); }
        public int ValidationID { get { return validationID; } set { validationID = value; } }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }

        public FieldValidationFormulaBLLC GetFormula(int index) {
            if (index < Count) {
                return (FieldValidationFormulaBLLC) this[index];
            }
            return null;
        }

        public bool Save() {
            //Set all formulas as new
            for (int i = 0; i < Count; i++) {
                FieldValidationFormulaBLLC form = GetFormula(i);
                form.IsNew = true;
                form.ValidationID = validationID;
            }

            //First get formulas for the validation before changes
            var coll = new FieldValidationFormulaCollectionBLLC(validationID);
            if (coll != null) {
                //Go through all fields and mark if to insert, change or delete

                //Fyrir hvert field sem til er fyrir
                //Fara � gegnum field sem � a� vista
                //Ef til - �� merkja sem uppf�ra
                //Ef ekki til �ar - �� b�ta �eim vi� en merkja sem ey�a
                for (int i = 0; i < coll.Count; i++) {
                    FieldValidationFormulaBLLC tf = coll.GetFormula(i);
                    bool fieldFound = false;
                    for (int j = 0; j < Count; j++) {
                        FieldValidationFormulaBLLC nf = GetFormula(j);
                        if (nf.ID == tf.ID) {
                            if (nf.Order == tf.Order && nf.ValidationItemID == tf.ValidationItemID &&
                                nf.ValidationItemType == tf.ValidationItemType) {
                                nf.IsModified = false;
                            } else {
                                nf.IsModified = true;
                            }
                            nf.IsNew = false;
                            nf.IsDeleted = false;
                            fieldFound = true;
                            break;
                        }
                    }
                    if (!fieldFound) {
                        tf.IsDeleted = true;
                        tf.IsNew = false;
                        tf.IsModified = false;
                        Add(tf);
                    }
                }
            }
            return DALC.SaveFieldValidationFormulas(this);
        }
    }
}