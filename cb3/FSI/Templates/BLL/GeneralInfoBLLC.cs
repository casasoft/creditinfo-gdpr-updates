#region

using System;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL {
    /// <summary>
    /// Summary description for GeneralInfoBLLC.
    /// </summary>
    public class GeneralInfoBLLC {
        /// <summary>
        /// The AFS_id (this statement internal ID)
        /// </summary>
        private int _AFS_id = -1;

        private StatementsDALC dalc;

        /// <summary>
        /// The AFS_id (this statement internal ID)
        /// </summary>
        private int last_Year_AFS_id = -1;

        public GeneralInfoBLLC() { }

        public GeneralInfoBLLC(int companyCIID, int year, int accPer) {
            CompanyCIID = companyCIID;
            FiscalYear = year.ToString();
            AccountMonths = accPer;
            IsFilledUp = DALC.FillGeneralInfo(this);
        }

        public GeneralInfoBLLC(int afs_Id) {
            AFS_id = afs_Id;
            IsFilledUp = DALC.FillGeneralInfoFromAFS_ID(this);
        }

        public int TemplateID { get; set; }
        public bool IsFilledUp { get; set; }
        public FieldCollectionBLLC TemplateFields { get; set; }


        public int UserID { get; set; }

        /// <summary>
        /// Gets/sets the companyCIID attribute
        /// </summary>
        public int CompanyCIID { get; set; }

        /// <summary>
        /// Gets/sets the fiscalYear attribute
        /// </summary>
        public string FiscalYear { get; set; }

        /// <summary>
        /// Gets/sets the staffCount attribute
        /// </summary>
        public int StaffCount { get; set; }

        /// <summary>
        /// Gets/sets the originID attribute
        /// </summary>
        public int OriginID { get; set; }

        /// <summary>
        /// Gets/sets the currencyID attribute
        /// </summary>
        public string CurrencyID { get; set; }

        /// <summary>
        /// Gets/sets the mostRecentAccount attribute 
        /// </summary>
        public string MostRecentAccount { get; set; }

        /// <summary>
        /// Gets/sets the yearEnded attribute
        /// </summary>
        public DateTime YearEnded { get; set; }

        /// <summary>
        /// Gets/sets the accountMonths attribute
        /// </summary>
        public int AccountMonths { get; set; }

        /// <summary>
        /// Gets/sets the consolidated attribute
        /// </summary>
        public bool Consolidated { get; set; }

        /// <summary>
        /// Gets/sets the qualified attribute
        /// </summary>
        public bool Qualified { get; set; }

        /// <summary>
        /// Gets/sets the smallStatus attribute
        /// </summary>
        public bool SmallStatus { get; set; }

        /// <summary>
        /// Gets/sets the comments attribute
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets/sets the reviewed attribute
        /// </summary>
        public bool Reviewed { get; set; }

        /// <summary>
        /// Gets/sets the DenominationID attribute
        /// </summary>
        public int DenominationID { get; set; }

        public String DenominationEN { get; set; }
        public String DenominationNative { get; set; }

        /// <summary>
        /// Gets/sets the commentsID attribute
        /// </summary>
        public int CommentsID { get; set; }

        public String CommentsEN { get; set; }
        public String CommentsNative { get; set; }

        /// <summary>
        /// Gets/sets the dateOfDelivery attribute
        /// </summary>
        public DateTime DateOfDelivery { get; set; }

        /// <summary>
        /// Gets/sets the readyForWebPublishing attribute
        /// </summary>
        public bool ReadyForWebPublishing { get; set; }

        /// <summary>
        /// Gets/sets the registratorID attribute
        /// </summary>
        public int RegistratorID { get; set; }

        /// <summary>
        /// Gets/sets the modified attribute
        /// </summary>
        public DateTime Modified { get; set; }

        /// <summary>
        /// Gets/sets the _AFS_id attribute
        /// </summary>
        public int AFS_id { get { return _AFS_id; } set { _AFS_id = value; } }

        /// <summary>
        /// Gets/sets last year (both year and period)  _AFS_id attribute
        /// </summary>
        public int Last_Year_AFS_id { get { return last_Year_AFS_id; } set { last_Year_AFS_id = value; } }

        /// <summary>
        /// Gets/sets the deleted attribute
        /// </summary>
        public bool Deleted { get; set; }

        protected StatementsDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new StatementsDALC();
                }
                return dalc;
            }
        }

        public bool Save() { return DALC.AddFinancialStatement(this); }
    }
}