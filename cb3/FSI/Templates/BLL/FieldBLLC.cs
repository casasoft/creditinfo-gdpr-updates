#region

using System;
using System.Collections;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL {
    /// <summary>
    /// Summary description for FieldBLLC.
    /// </summary>
    [Serializable]
    public class FieldBLLC {
        private readonly bool isNew;
        private int categoryID;
        private TemplatesDALC dalc;
        public FieldBLLC() { }

        public FieldBLLC(int id) {
            ID = id;
            isNew = !DALC.FillField(this);
        }

        public bool IsNew { get { return isNew; } }
        public int ID { get; set; }
        public string NameNative { get; set; }
        public string NameEN { get; set; }
        public string Type { get; set; }
        public string FieldName { get; set; }
        public object Value { get; set; }
        public int CategoryID { get { return categoryID; } set { categoryID = value; } }

        public CategoryBLLC Category {
            get {
                if (categoryID > 0) {
                    var categ = new CategoryBLLC(categoryID);
                    if (!categ.IsNew) {
                        return categ;
                    }
                }
                return null;
            }
            set { categoryID = value.ID; }
        }

        public string CategoryNameNative { get { return Category.NameNative; } }
        public string CategoryNameEN { get { return Category.NameEN; } }
        public string DescriptionNative { get; set; }
        public string DescriptionEN { get; set; }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }
    }

    public class FieldCollectionBLLC : ArrayList {
        protected TemplatesDALC dalc;
        public FieldCollectionBLLC() { }

        public FieldCollectionBLLC(bool fill) {
            if (fill) {
                DALC.FillFields(this);
            }
        }

        public FieldCollectionBLLC(int templateID) { DALC.FillFields(this, templateID); }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }

        public FieldBLLC GetField(int i) {
            if (i < Count) {
                return (FieldBLLC) this[i];
            }
            return null;
        }
    }
}