#region

using System.Collections;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL {
    /// <summary>
    /// Summary description for FieldValidations.
    /// </summary>	
    public class FieldValidationBLLC {
        private TemplatesDALC dalc;
        private FieldValidationFormulaCollectionBLLC formula;
        private int id;
        private bool isNew = true;
        public FieldValidationBLLC() { }

        public FieldValidationBLLC(int validationID) {
            ID = validationID;
            if (DALC.FillFieldValidation(this)) {
                isNew = false;
            }
        }

        public int ID { get { return id; } set { id = value; } }
        public int TemplateFieldID { get; set; }
        public string NameNative { get; set; }
        public string NameEN { get; set; }
        public string DescriptionNative { get; set; }
        public string DescriptionEN { get; set; }
        public bool IsNew { get { return isNew; } set { isNew = value; } }

        public FieldValidationFormulaCollectionBLLC Formula {
            get {
                if (formula == null) {
                    formula = new FieldValidationFormulaCollectionBLLC(id);
                }
                return formula;
            }
            set { formula = value; }
        }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }

        public bool Save() {
            if (DALC.SaveFieldValidation(this)) {
                Formula.ValidationID = ID;
                return Formula.Save();
            }
            return false;
        }

        public bool Delete() { return DALC.DeleteFieldValidation(this); }
        public bool RemoveValidationFromTemplateField() { return DALC.RemoveValidationFromTemplateField(this); }
    }

    public class FieldValidationCollectionBLLC : ArrayList {
        protected TemplatesDALC dalc;
        protected int templateFieldID = -1;
        public FieldValidationCollectionBLLC() { }

        public FieldValidationCollectionBLLC(bool fill) {
            if (fill) {
                DALC.FillFieldValidations(this);
            }
        }

        public FieldValidationCollectionBLLC(int templateFieldID) {
            this.templateFieldID = templateFieldID;
            DALC.FillFieldValidations(this);
        }

        public int TemplateFieldID { get { return templateFieldID; } set { templateFieldID = value; } }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }

        public FieldValidationBLLC GetField(int i) {
            if (i < Count) {
                return (FieldValidationBLLC) this[i];
            }
            return null;
        }

        public bool ContainsValidationWithID(int id) {
            for (int i = 0; i < Count; i++) {
                if (GetField(i).ID == id) {
                    return true;
                }
            }
            return false;
        }

        public bool AddValidationsToTemplateField() { return DALC.AddValidationsToTemplateField(this); }
    }
}