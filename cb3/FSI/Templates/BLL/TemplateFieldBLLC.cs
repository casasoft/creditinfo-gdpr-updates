#region

using System.Collections;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL {
    /// <summary>
    /// Summary description for TemplateFieldBLLC.
    /// </summary>
    public class TemplateFieldBLLC {
        private TemplatesDALC dalc;
        private int id = -1;
        public TemplateFieldBLLC() { }

        public TemplateFieldBLLC(int id) {
            ID = id;
            DALC.FillTemplateField(this);
        }

        public int ID { get { return id; } set { id = value; } }
        public int TemplateID { get; set; }
        public int FieldID { get; set; }
        public int Order { get; set; }
        public bool IsModified { get; set; }
        public bool IsNew { get; set; }
        public bool IsDeleted { get; set; }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }
    }

    public class TemplateFieldsCollectionBLLC : ArrayList {
        protected TemplatesDALC dalc;
        protected int templateID = -1;
        public TemplateFieldsCollectionBLLC() { }

        public TemplateFieldsCollectionBLLC(int templateID) {
            this.templateID = templateID;
            DALC.FillTemplateFields(this, templateID);
        }

        public int TemplateID { get { return templateID; } set { templateID = value; } }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }

        public TemplateFieldBLLC GetField(int i) {
            if (i < Count) {
                return (TemplateFieldBLLC) this[i];
            }
            return null;
        }

        public bool Save() {
            //First get fields for the template before changes
            var coll = new TemplateFieldsCollectionBLLC(templateID);
            if (coll != null) {
                //Go through all fields and mark if to insert, change or delete

                //Fyrir hvert field sem til er fyrir
                //Fara � gegnum field sem � a� vista
                //Ef til - �� merkja sem uppf�ra
                //Ef ekki til �ar - �� b�ta �eim vi� en merkja sem ey�a
                for (int i = 0; i < coll.Count; i++) {
                    var tf = coll.GetField(i);
                    bool fieldFound = false;
                    for (int j = 0; j < Count; j++) {
                        TemplateFieldBLLC nf = GetField(j);
                        if (nf.FieldID != tf.FieldID) {
                            continue;
                        }
                        nf.IsModified = nf.Order != tf.Order;
                        nf.IsNew = false;
                        nf.IsDeleted = false;
                        fieldFound = true;
                        break;
                    }
                    if (fieldFound) {
                        continue;
                    }
                    tf.IsDeleted = true;
                    tf.IsNew = false;
                    tf.IsModified = false;
                    Add(tf);
                }
            }
            return DALC.SaveTemplateFields(this);
        }
    }
}