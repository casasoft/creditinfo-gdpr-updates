#region

using System.Collections;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL {
    /// <summary>
    /// Summary description for CategoryBLLC.
    /// </summary>
    public class CategoryBLLC {
        private readonly bool isNew;
        private TemplatesDALC dalc;
        public CategoryBLLC() { }

        public CategoryBLLC(int id) {
            ID = id;
            isNew = !DALC.FillCategory(this);
        }

        public bool IsNew { get { return isNew; } }
        public int ID { get; set; }
        public string NameNative { get; set; }
        public string NameEN { get; set; }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }
    }

    public class CategoryCollectionBLLC : ArrayList {
        protected TemplatesDALC dalc;
        public CategoryCollectionBLLC() { DALC.FillCategorys(this); }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }

        public CategoryBLLC GetField(int i) {
            if (i < Count) {
                return (CategoryBLLC) this[i];
            }
            return null;
        }
    }
}