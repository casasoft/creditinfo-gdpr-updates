#region

using System.Collections;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL {
    /// <summary>
    /// Summary description for TemplatesFactory.
    /// </summary>
    public class TemplatesFactory {
        private readonly TemplatesDALC dalc;
        public TemplatesFactory() { dalc = new TemplatesDALC(); }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nCigID"></param>
        /// <param name="strName"></param>
        /// <param name="strNationalID"></param>
        /// <returns></returns>
        public ArrayList FindCompany(int nCigID, string strName, string strNationalID) { return dalc.FindCompany(nCigID, strName, strNationalID); }
    }
}