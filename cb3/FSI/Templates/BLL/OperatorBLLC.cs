#region

using System.Collections;
using FSI.Templates.DAL;

#endregion

namespace FSI.Templates.BLL {
    /// <summary>
    /// Summary description for OperatorBLLC.
    /// </summary>
    public class OperatorBLLC {
        private TemplatesDALC dalc;
        public OperatorBLLC() { }

        public OperatorBLLC(int id) {
            ID = id;
            DALC.FillOperator(this);
        }

        public int ID { get; set; }
        public string NameNative { get; set; }
        public string NameEN { get; set; }
        public string Symbol { get; set; }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }
    }

    public class OperatorCollectionBLLC : ArrayList {
        protected TemplatesDALC dalc;
        public OperatorCollectionBLLC() { DALC.FillOperators(this); }

        protected TemplatesDALC DALC {
            get {
                if (dalc == null) {
                    dalc = new TemplatesDALC();
                }
                return dalc;
            }
        }

        public OperatorBLLC GetOperator(int index) {
            if (index < Count) {
                return (OperatorBLLC) this[index];
            }
            return null;
        }
    }
}