#region

using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FSI.Templates.BLL;
using Logging.BLL;

#endregion

namespace FSI.Templates {
    /// <summary>
    /// Summary description for FieldValidationOverview.
    /// </summary>
    public class FieldValidationOverview : BaseTemplatePage {
        protected Button btnNew;
        protected DataGrid dtgrValidations;
        protected Label lblAllFieldValidations;
        protected Label lblDatagridIcons;
        protected Label lblPageStep;
        protected HtmlTable outerGridTable;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "1003";
            PreparePageLoad();

            if (!Page.IsPostBack) {
                LocalizeText();
                FillDataGrid();
            }
        }

        protected void FillDataGrid() {
            var coll = new FieldValidationCollectionBLLC(true);
            dtgrValidations.DataSource = coll;
            dtgrValidations.DataBind();
        }

        protected void LocalizeText() {
            lblPageStep.Text = rm.GetString("", ci);
            lblAllFieldValidations.Text = rm.GetString("txtAllFieldsValidations", ci);
            btnNew.Text = rm.GetString("txtNew", ci);
        }

        private void dtgrValidations_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);

                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }
                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }
                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }
                }
            }

            WebDesign.CreateExplanationIcons(dtgrValidations.Columns, lblDatagridIcons, rm, ci, 2);
        }

        private void dtgrValidations_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                Context.Items.Add("ValidationID", e.Item.Cells[2].Text);
                Server.Transfer("TemplateFieldFormulaDefinition.aspx");
            } else if (e.CommandName.Equals("Delete")) {
                var val = new FieldValidationBLLC {ID = int.Parse(e.Item.Cells[2].Text)};
                if (val.Delete()) {
                    FillDataGrid();
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e) { Server.Transfer("TemplateFieldFormulaDefinition.aspx"); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dtgrValidations.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrValidations_ItemCommand);
            this.dtgrValidations.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrValidations_ItemDataBound);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}