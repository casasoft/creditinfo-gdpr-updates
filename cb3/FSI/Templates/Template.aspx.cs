#region

using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using FSI.Templates.BLL;

#endregion

namespace FSI.Templates {
    /// <summary>
    /// Summary description for Template.
    /// </summary>
    public class Template : BaseTemplatePage {
        protected Button btnAdd;
        protected Button btnContinue;
        protected ImageButton ibtnAddNace;
        protected ImageButton ibtnMoveDown;
        protected ImageButton ibtnMoveUp;
        protected ImageButton ibtnRemoveNace;
        protected Label lblAvailableFields;
        protected Label lblDescriptionEN;
        protected Label lblDescriptionNative;
        protected Label lblErrMsg;
        protected Label lblFieldsInTemplate;
        protected Label lblNameEN;
        protected Label lblNameNative;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        protected Label lblTemplate;
        protected ListBox lbxAvailableFields;
        protected ListBox lbxFieldsInTemplate;
        protected TextBox txtDescriptionEN;
        protected TextBox txtDescriptionNative;
        protected TextBox txtNameEN;
        protected TextBox txtNameNative;
        protected TextBox txtTemplateID;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "1004";
            PreparePageLoad();
            lblErrMsg.Visible = false;

            if (!Page.IsPostBack) {
                LocalizeText();
                FillAvailableFields();
                if (fsiTemplate > 0) {
                    LoadTemplate(fsiTemplate);
                }
            }
        }

        protected void LocalizeText() {
            lblNameNative.Text = rm.GetString("txtNameNative", ci);
            lblNameEN.Text = rm.GetString("txtNameEN", ci);
            lblDescriptionNative.Text = rm.GetString("txtDescriptionNative", ci);
            lblDescriptionEN.Text = rm.GetString("txtDescriptionEN", ci);
            lblAvailableFields.Text = rm.GetString("txtAvailableFields", ci);
            lblTemplate.Text = rm.GetString("txtTemplate", ci);
            lblPageTitle.Text = rm.GetString("txtFSITemplate", ci);
            lblFieldsInTemplate.Text = rm.GetString("txtFieldsInTemplate", ci);
            btnAdd.Text = rm.GetString("txtSave", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            lblPageStep.Text = rm.GetString("", ci);
        }

        protected void LoadTemplate(int id) {
            var template = new TemplateBLLC(id);
            if (template != null) {
                txtNameNative.Text = template.NameNative;
                txtNameEN.Text = template.NameEN;
                txtDescriptionNative.Text = template.DescriptionNative;
                txtDescriptionEN.Text = template.DescriptionEN;
                if (template.Fields != null) {
                    for (int i = 0; i < template.Fields.Count; i++) {
                        TemplateFieldBLLC tField = template.Fields.GetField(i);
                        var field = new FieldBLLC(tField.FieldID);
                        lbxFieldsInTemplate.Items.Add(
                            new ListItem(field.Category.NameNative + "-" + field.NameNative, field.ID.ToString()));
                    }
                }
            }
            ClearFieldsFromAllThatAreInTemplate();
        }

        protected void FillAvailableFields() {
            var coll = new FieldCollectionBLLC(true);
            for (int i = 0; i < coll.Count; i++) {
                var field = coll.GetField(i);
                if (field != null) {
                    lbxAvailableFields.Items.Add(
                        new ListItem(field.Category.NameNative + "-" + field.NameNative, field.ID.ToString()));
                }
            }
        }

        private void ibtnAddNace_Click(object sender, ImageClickEventArgs e) {
            var toDelete = new ArrayList();
            int index = 0;
            foreach (ListItem item in lbxAvailableFields.Items) {
                if (item.Selected) {
                    if (lbxFieldsInTemplate.Items.FindByValue(item.Value) == null) {
                        lbxFieldsInTemplate.Items.Add(item);
                        toDelete.Add(index);
                    }
                }
                index++;
            }
            //ok - remove all fields added in reversed order
            for (int i = toDelete.Count - 1; i >= 0; i--) {
                lbxAvailableFields.Items.RemoveAt((int) toDelete[i]);
            }
        }

        private void ibtnMoveUp_Click(object sender, ImageClickEventArgs e) {
            var toMove = new ArrayList();
            int index = 0;
            //First - get index of all items to delete
            foreach (ListItem item in lbxFieldsInTemplate.Items) {
                if (item.Selected) {
                    toMove.Add(index);
                }
                index++;
            }

            //ok - now we have the index - the delete items in reversed order
            for (int i = 0; i < toMove.Count; i++) {
                if ((int) toMove[i] > 0) {
                    ListItem itm = lbxFieldsInTemplate.Items[(int) toMove[i]];
                    lbxFieldsInTemplate.Items.RemoveAt((int) toMove[i]);
                    lbxFieldsInTemplate.Items.Insert((int) toMove[i] - 1, itm);
                }
            }
        }

        private void ibtnMoveDown_Click(object sender, ImageClickEventArgs e) {
            var toMove = new ArrayList();
            int index = 0;
            //First - get index of all items to delete
            foreach (ListItem item in lbxFieldsInTemplate.Items) {
                if (item.Selected) {
                    toMove.Add(index);
                }
                index++;
            }

            //ok - now we have the index - the delete items in reversed order
            for (int i = toMove.Count - 1; i >= 0; i--) {
                if ((int) toMove[i] < lbxFieldsInTemplate.Items.Count - 1) {
                    ListItem itm = lbxFieldsInTemplate.Items[(int) toMove[i]];
                    lbxFieldsInTemplate.Items.RemoveAt((int) toMove[i]);
                    lbxFieldsInTemplate.Items.Insert((int) toMove[i] + 1, itm);
                }
            }
        }

        private void ibtnRemoveNace_Click(object sender, ImageClickEventArgs e) {
            var toDelete = new ArrayList();
            int index = 0;
            //First - get index of all items to delete
            foreach (ListItem item in lbxFieldsInTemplate.Items) {
                if (item.Selected) {
                    toDelete.Add(index);
                    if (lbxAvailableFields.Items.FindByValue(item.Value) == null) {
                        lbxAvailableFields.Items.Add(item);
                    }
                }
                index++;
            }

            //ok - now we have the index - the delete items in reversed order
            for (int i = toDelete.Count - 1; i >= 0; i--) {
                lbxFieldsInTemplate.Items.RemoveAt((int) toDelete[i]);
            }
        }

        protected void ClearFieldsFromAllThatAreInTemplate() {
            foreach (ListItem item in lbxFieldsInTemplate.Items) {
                foreach (ListItem allItem in lbxAvailableFields.Items) {
                    if (item.Equals(allItem)) {
                        lbxAvailableFields.Items.Remove(allItem);
                        break;
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            if (fsiTemplate > 0) {
                SaveTemplate(false);
            } else {
                SaveTemplate(true);
            }
        }

        protected void SaveTemplate(bool isNew) {
            var template = new TemplateBLLC {IsNew = isNew};
            if (!isNew) {
                if (fsiTemplate > 0) {
                    template.ID = fsiTemplate;
                }
            }
            template.NameNative = txtNameNative.Text;
            template.NameEN = txtNameEN.Text;
            template.DescriptionNative = txtDescriptionNative.Text;
            template.DescriptionEN = txtDescriptionEN.Text;

            var coll = new TemplateFieldsCollectionBLLC();
            for (int i = 0; i < lbxFieldsInTemplate.Items.Count; i++) {
                var field = new TemplateFieldBLLC
                            {
                                FieldID = (int.Parse(lbxFieldsInTemplate.Items[i].Value)),
                                Order = i
                            };
                coll.Add(field);
            }
            template.Fields = coll;
            if (template.Save()) {
                Session["FSITemplateID"] = template.ID;
                Server.Transfer("TemplateFieldOverview.aspx");
            } else {
                lblErrMsg.Text = "Failed saving template";
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) {
            if (fsiTemplate > 0) {
                Server.Transfer("TemplateFieldOverview.aspx");
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ibtnAddNace.Click += new ImageClickEventHandler(this.ibtnAddNace_Click);
            this.ibtnRemoveNace.Click += new ImageClickEventHandler(this.ibtnRemoveNace_Click);
            this.ibtnMoveUp.Click += new ImageClickEventHandler(this.ibtnMoveUp_Click);
            this.ibtnMoveDown.Click += new ImageClickEventHandler(this.ibtnMoveDown_Click);
            this.btnAdd.Click += new EventHandler(this.btnAdd_Click);
            this.btnContinue.Click += new EventHandler(this.btnContinue_Click);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}