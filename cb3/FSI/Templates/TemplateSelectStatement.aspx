<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="TemplateSelectStatement.aspx.cs" AutoEventWireup="false" Inherits="FSI.Templates.TemplateSelectStatement" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TemplateSelectStatement</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>
					function checkEnterKey() 
					{    
							if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								SelectStatementForm.btnRegisterChange.click(); 
							}
					} 
					
					function checkSearchKey() 
					{    
							if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								SelectStatementForm.btnSearch.click(); 
							}
					} 
					function SetFormFocus()
						{
							document.SelectStatementForm.txtName.focus();
						}
		</script>
	</HEAD>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="SelectStatementForm" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td id="blas" colspan="2" runat="server">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblControl" runat="server">Control</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lblCIID" runat="server">[CIG ID]</asp:label><br>
																		<asp:textbox id="txtCreditInfoID" runat="server" maxlength="9"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblName" runat="server">[Name]</asp:label><br>
																		<asp:textbox id="txtName" runat="server" maxlength="100"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblUniqueIndentifier" runat="server">[Unique identifier]</asp:label><br>
																		<asp:textbox id="tbUniqueIndentifier" runat="server" maxlength="100"></asp:textbox></td>
																	<td align="right"><asp:button id="btnSearch" runat="server" cssclass="search_button" text="[Search]" commandname='"runit"'
																			causesvalidation="False"></asp:button></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr id="trSelectRow" runat="server" visible="false">
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblPageTitle" runat="server">Financial statement</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lblCompanyRegno" runat="server">Company Reg. no.</asp:label><br>
																		<asp:textbox id="tbCompanyRegNo" runat="server"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblFiscalYear" runat="server">Fiscal year</asp:label><br>
																		<asp:textbox id="tbFiscalYear" runat="server"></asp:textbox><asp:requiredfieldvalidator id="rfvFisicalYear" runat="server" errormessage="Value missing" controltovalidate="tbFiscalYear"
																			enabled="False">*</asp:requiredfieldvalidator></td>
																	<td style="WIDTH: 25%"><asp:label id="lblAccountPeriod" runat="server">Account period</asp:label><br>
																		<asp:dropdownlist id="ddAccountPeriod" runat="server">
																			<asp:listitem value="12" selected="True">12</asp:listitem>
																			<asp:listitem value="9">9</asp:listitem>
																			<asp:listitem value="6">6</asp:listitem>
																			<asp:listitem value="3">3</asp:listitem>
																		</asp:dropdownlist></td>
																	<td><asp:label id="lblTemplate" runat="server">Template</asp:label><br>
																		<asp:dropdownlist id="ddTemplate" runat="server"></asp:dropdownlist></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblDatagridHeader" runat="server"></asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divNameSearch" style="OVERFLOW-Y: scroll; OVERFLOW: auto" runat="server">
																			<asp:datagrid id="dgCompanies" runat="server" autogeneratecolumns="False" cssclass="grid" gridlines="None">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot; /&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="CompanyCIID" headertext="CreditInfoID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="UniqueID" headertext="NationalID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerFinancialStatementsGrid" cellspacing="0" cellpadding="0"
													runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblFinancialStatementsIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblFinancialStatementsHeader" runat="server"></asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dgFinancialStatements" runat="server" autogeneratecolumns="False" cssclass="grid"
																			gridlines="None">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;/&gt;"
																					commandname="Edit">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:boundcolumn datafield="Financial_year">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Account_period_length">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="created">
																					<headerstyle width="120px"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="NameNative"></asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="NameEN"></asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="TemplateID"></asp:boundcolumn>
																			</columns>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left"><asp:label id="lblErrMsg" runat="server" cssclass="error_text" visible="False">Label</asp:label>
															<asp:validationsummary id="ValidationSummary1" runat="server" width="184px"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel" visible="False"></asp:button><asp:button id="btnRegisterChange" runat="server" cssclass="confirm_button" text="Register/Change" visible="False"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
