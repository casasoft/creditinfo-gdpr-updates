#region

using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FSI.Templates.BLL;
using Logging.BLL;

#endregion

namespace FSI.Templates {
    /// <summary>
    /// Summary description for TemplateOverview.
    /// </summary>
    public class TemplateOverview : BaseTemplatePage {
        protected Button btnNew;
        protected DataGrid dtgrTemplates;
        protected Label lblAllTemplates;
        protected Label lblDatagridIcons;
        protected Label lblErrorMsg;
        protected Label lblPageStep;
        protected HtmlTable outerGridTable;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "1002";
            Session.Remove("FSITemplateID");
            PreparePageLoad();
            lblErrorMsg.Visible = false;
            if (!Page.IsPostBack) {
                LocalizeText();
                LoadTemplates();
            }
        }

        private void LocalizeText() {
            lblPageStep.Text = rm.GetString("", ci);
            lblAllTemplates.Text = rm.GetString("txtAllTemplates", ci);
            btnNew.Text = rm.GetString("txtNew", ci);
        }

        private void LoadTemplates() {
            var coll = new TemplateCollectionBLLC();
            dtgrTemplates.DataSource = coll;
            dtgrTemplates.DataBind();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            Server.Transfer("Template.aspx");
            /*FSI.Templates.BLL.generator.ValidationGenerator gen	= new FSI.Templates.BLL.generator.ValidationGenerator();

			string val = gen.GenerateValidationScript(3,1);

			if(val==null)
				Logging.BLL.Logger.WriteToLog("Value was null", false);
			else
				Logging.BLL.Logger.WriteToLog(val, false);*/
        }

        private void dtgrTemplates_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);

                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }
                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }
                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }
                }
            }
            WebDesign.CreateExplanationIcons(dtgrTemplates.Columns, lblDatagridIcons, rm, ci, 2);
        }

        private void dtgrTemplates_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                Session["FSITemplateID"] = e.Item.Cells[2].Text;
                Server.Transfer("Template.aspx");
            } else if (e.CommandName.Equals("Delete")) {
                var templ = new TemplateBLLC {ID = int.Parse(e.Item.Cells[3].Text)};
                if (templ.AreSomeFSUsingTemplate()) {
                    lblErrorMsg.Text = "Can't delete template - some FS are using the template";
                    lblErrorMsg.Visible = true;
                } else {
                    if (templ.Delete()) {
                        LoadTemplates();
                    } else {
                        lblErrorMsg.Text = "Failed deleting template";
                        lblErrorMsg.Visible = true;
                    }
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dtgrTemplates.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrTemplates_ItemCommand);
            this.dtgrTemplates.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrTemplates_ItemDataBound);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}