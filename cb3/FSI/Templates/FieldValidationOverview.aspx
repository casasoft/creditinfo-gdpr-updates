<%@ Register TagPrefix="uc1" TagName="head" Src="../../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="FieldValidationOverview.aspx.cs" AutoEventWireup="false" Inherits="FSI.Templates.FieldValidationOverview" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>TemplateFieldOverview</title>
		<% System.Web.HttpContext.Current.Response.AddHeader("Cache-Control","no-cache");%>
		<% System.Web.HttpContext.Current.Response.Expires = 0;%>
		<% System.Web.HttpContext.Current.Response.Cache.SetNoStore();%>
		<% System.Web.HttpContext.Current.Response.AddHeader("Pragma", "no-cache");%>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../../css/CIGStyles.css" type="text/css" rel="stylesheet">
	</head>
	<body ms_positioning="GridLayout">
		<form id="FieldValidationOverview" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right">
															<asp:label id="lblDatagridIcons" runat="server"></asp:label>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblAllFieldValidations" runat="server">All fields validations</asp:label>
																	</td>
																	<td align="right">
																		<asp:label id="lblPageStep" runat="server">Step</asp:label>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dtgrValidations" runat="server" gridlines="None" cssclass="grid" autogeneratecolumns="False">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;"
																					commandname="Select">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;/&gt;"
																					commandname="Delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:boundcolumn visible="False" datafield="ID" headertext="ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Name">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="NameNative" headertext="NameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="NameEN" headertext="NameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Description">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="DescriptionNative" headertext="DescriptionNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="DescriptionEN" headertext="DEscriptionEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
														</td>
														<td align="right">
															<asp:button id="btnNew" runat="server" cssclass="confirm_button" text="New"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
