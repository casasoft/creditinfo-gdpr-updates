<%@ Page language="c#" Codebehind="TemplateStatementInput.aspx.cs" AutoEventWireup="false" Inherits="FSI.Templates.TemplateStatementInput" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DynamicStatementInput</title>
		<script language="JavaScript" src="../../DatePicker.js"></script>
		<script language="JavaScript" src="../Validation.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<asp:literal id="litMyJavascriptControl" runat="server"></asp:literal>
	</HEAD>
	<body ms_positioning="GridLayout">
		<form id="f_arsr" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2" id="blas" runat="server">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblPageTitle" runat="server">Financial statement input page</asp:label>-
															<asp:label id="lblFinancialStatement" runat="server">Financial Statement</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 350px">
																		<asp:label id="lblCompanyName" runat="server"></asp:label>
																	</td>
																	<td style="WIDTH: 250px">
																		<asp:label id="lblCompanyNationalID" runat="server"></asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblCompanyAddress" runat="server"></asp:label>
																	</td>
																	<td>
																		<asp:label id="lblCompanyFunction" runat="server"></asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblCompanyCity" runat="server"></asp:label>
																	</td>
																	<td>
																		<asp:label id="lblMostRecentAccount" runat="server">Most recent account: 2002(12 months)</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblFiscalYear" runat="server">Fiscal year:</asp:label>
																	</td>
																	<td>
																		<asp:textbox id="tbFiscalYear" runat="server"></asp:textbox>
																	</td>
																	<td>
																		<asp:textbox id="tbAFS_id" runat="server" width="48px" visible="False"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblYearEnded" runat="server">Year ended:</asp:label>
																	</td>
																	<td>
																		<asp:textbox id="tbYearEnded" runat="server"></asp:textbox>
																		<input onclick="PopupPickerWithDept('tbYearEnded', 250, 250, 2);" type="button" value="..."
																			class="popup">
																		<asp:CustomValidator id="cvYearEnded" runat="server" ControlToValidate="tbYearEnded" EnableClientScript="False">*</asp:CustomValidator>
																		<asp:RequiredFieldValidator id="rfvYearEnded" runat="server" ControlToValidate="tbYearEnded">*</asp:RequiredFieldValidator>
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblAccountPeriodLength" runat="server">Account period length</asp:label>
																	</td>
																	<td>
																		<asp:dropdownlist id="ddTotalMonths" runat="server">
																			<asp:listitem value="12" selected="True">12</asp:listitem>
																			<asp:listitem value="9">9</asp:listitem>
																			<asp:listitem value="6">6</asp:listitem>
																			<asp:listitem value="3">3</asp:listitem>
																		</asp:dropdownlist>
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblCurrency" runat="server">Currency:</asp:label>
																	</td>
																	<td>
																		<asp:dropdownlist id="ddCurrency" runat="server">
																			<asp:listitem value="ISK">ISK</asp:listitem>
																			<asp:listitem value="USD">USD</asp:listitem>
																			<asp:listitem value="MTL" selected="True">MTL</asp:listitem>
																		</asp:dropdownlist>
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblConsolidated" runat="server">Consolidated:</asp:label>
																	</td>
																	<td>
																		<asp:RadioButton id="rbtConsolidatedYes" runat="server" Text="Yes" CssClass="radio" GroupName="Consolidated"></asp:RadioButton>&nbsp;
																		<asp:RadioButton id="rbtConsolidatedNo" runat="server" Text="No" CssClass="radio" GroupName="Consolidated"
																			Checked="True"></asp:RadioButton>
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblDenomination" runat="server">Denomination:</asp:label>
																	</td>
																	<td>
																		<asp:dropdownlist id="ddDenomination" runat="server"></asp:dropdownlist>
																	</td>
																	<td>
																		<input class="FSICheck" id="Text2" tabindex="-1" readonly type="text" size="10" name="Text2">
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblSource" runat="server">Source</asp:label>:</td>
																	<td>
																		<asp:dropdownlist id="ddSource" runat="server"></asp:dropdownlist>
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblOtherFields" runat="server">Other fields</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 350px">
																		<asp:label id="lblQualifiedAudit" runat="server">Qualified audit:</asp:label>
																	</td>
																	<td style="WIDTH: 250px">
																		<asp:RadioButton id="rbtQualifiedAuditYes" runat="server" Text="Yes" CssClass="radio" GroupName="QualifiedAudit"></asp:RadioButton>&nbsp;
																		<asp:RadioButton id="rbtQualifiedAuditNo" runat="server" Text="No" CssClass="radio" GroupName="QualifiedAudit"
																			Checked="True"></asp:RadioButton>
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblSmallStatus" runat="server">Small status:</asp:label>
																	</td>
																	<td>
																		<asp:RadioButton id="rbtSmallStatusYes" runat="server" Text="Yes" CssClass="radio" GroupName="SmallStatus"></asp:RadioButton>&nbsp;
																		<asp:RadioButton id="rbtSmallStatusNo" runat="server" Text="No" CssClass="radio" GroupName="SmallStatus"
																			Checked="True"></asp:RadioButton>
																	</td>
																	<td>
																		<input class="FSICheck" id="Text1" tabindex="-1" readonly type="text" size="10" name="Text1">
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblComments" runat="server">Comments:</asp:label>
																	</td>
																	<td>
																		<asp:dropdownlist id="ddComments" runat="server"></asp:dropdownlist>
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblStaffCount" runat="server">Staff count:</asp:label>
																	</td>
																	<td>
																		<asp:textbox id="tbStaffCount" runat="server"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td colspan="3">
												<asp:panel runat="server" id="PnlForm"></asp:panel>
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" width="100%" cellspacing="0">
													<tr>
														<td style="WIDTH: 375px">
															<asp:label id="lblReadyForWebPublish" runat="server">Ready for web publish</asp:label>:</td>
														<td>
															<asp:RadioButton id="rbtReadyForWebTrue" runat="server" Text="Yes" GroupName="WebPublishing" CssClass="radio"></asp:RadioButton>&nbsp;<asp:RadioButton id="rbtReadyForWebFalse" runat="server" Text="No" GroupName="WebPublishing" CssClass="radio"
																Checked="True"></asp:RadioButton>&nbsp;
														</td>
													</tr>
													<TR>
														<TD style="WIDTH: 375px; HEIGHT: 15px"></TD>
														<TD style="HEIGHT: 17px"></TD>
													</TR>
													<tr>
														<td align="left">
															<asp:label id="lblMsg" runat="server" visible="False">The statement has been registered</asp:label>
															<asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary>
														</td>
														<td align="right">
															<asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel"></asp:button><asp:button id="btnSubmit" runat="server" cssclass="confirm_button" text="Submit"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
