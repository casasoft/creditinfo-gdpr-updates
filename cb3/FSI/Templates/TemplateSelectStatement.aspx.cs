#region

using System;
using System.Configuration;
using System.Threading;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FSI.BLL;
using FSI.Templates.BLL;
using Logging.BLL;

using Cig.Framework.Base.Configuration;


#endregion

#pragma warning disable 618,612
namespace FSI.Templates {
    /// <summary>
    /// Summary description for TemplateSelectStatement.
    /// </summary>
    public class TemplateSelectStatement : BaseTemplatePage {
        protected HtmlTableCell blas;
        protected Button btnCancel;
        protected Button btnRegisterChange;
        protected Button btnSearch;
        protected DropDownList ddAccountPeriod;
        protected DropDownList ddTemplate;
        protected DataGrid dgCompanies;
        protected DataGrid dgFinancialStatements;
        protected HtmlGenericControl divNameSearch;
        protected Label lblAccountPeriod;
        protected Label lblCIID;
        protected Label lblCompanyRegno;
        protected Label lblControl;
        protected Label lblDatagridHeader;
        protected Label lblDatagridIcons;
        protected Label lblErrMsg;
        protected Label lblFinancialStatementsHeader;
        protected Label lblFinancialStatementsIcons;
        protected Label lblFiscalYear;
        protected Label lblName;
        protected Label lblPageTitle;
        protected Label lblTemplate;
        protected Label lblUniqueIndentifier;
        protected HtmlTable outerFinancialStatementsGrid;
        protected HtmlTable outerGridTable;
        protected RequiredFieldValidator rfvFisicalYear;
        protected TextBox tbCompanyRegNo;
        protected TextBox tbFiscalYear;
        protected TextBox tbUniqueIndentifier;
        protected HtmlTableRow trSelectRow;
        protected TextBox txtCreditInfoID;
        protected TextBox txtName;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "1001";
            PreparePageLoad();
            lblErrMsg.Visible = false;

            // Add <ENTER> event
            AddEnterEvent();

            if (IsPostBack) {
                return;
            }
            outerGridTable.Visible = false;
            outerFinancialStatementsGrid.Visible = false;
            Session.Remove("FSI_tbCompanyRegNo");
            Session.Remove("FSI_year");
            Session.Remove("FSI_ddAccountPeriod");
            Session.Remove("FSITemplateID");
            LocalizeText();
            LocalizeDatagrid();
            FillTemplatesBox();
            FillAccountPeriodBox();
        }

        private void FillAccountPeriodBox() {
            if (CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                ddAccountPeriod.Items.Clear();
                ddAccountPeriod.Items.Add(new ListItem("18", "18"));
                ddAccountPeriod.Items.Add(new ListItem("17", "17"));
                ddAccountPeriod.Items.Add(new ListItem("16", "16"));
                ddAccountPeriod.Items.Add(new ListItem("15", "15"));
                ddAccountPeriod.Items.Add(new ListItem("14", "14"));
                ddAccountPeriod.Items.Add(new ListItem("13", "13"));
                ddAccountPeriod.Items.Add(new ListItem("12", "12"));
                ddAccountPeriod.Items.Add(new ListItem("11", "11"));
                ddAccountPeriod.Items.Add(new ListItem("10", "10"));
                ddAccountPeriod.Items.Add(new ListItem("9", "9"));
                ddAccountPeriod.Items.Add(new ListItem("8", "8"));
                ddAccountPeriod.Items.Add(new ListItem("7", "7"));
                ddAccountPeriod.Items.Add(new ListItem("6", "6"));
                ddAccountPeriod.Items.Add(new ListItem("5", "5"));
                ddAccountPeriod.Items.Add(new ListItem("4", "4"));
                ddAccountPeriod.Items.Add(new ListItem("3", "3"));
                ddAccountPeriod.Items.Add(new ListItem("2", "2"));
                ddAccountPeriod.SelectedValue = "12";
            }
            if (CigConfig.Configure("lookupsettings.currentVersion").Equals("lt")) {
                ddAccountPeriod.Items.Clear();
                ddAccountPeriod.Items.Add(new ListItem("12", "12"));
                ddAccountPeriod.Items.Add(new ListItem("11", "11"));
                ddAccountPeriod.Items.Add(new ListItem("10", "10"));
                ddAccountPeriod.Items.Add(new ListItem("9", "9"));
                ddAccountPeriod.Items.Add(new ListItem("8", "8"));
                ddAccountPeriod.Items.Add(new ListItem("7", "7"));
                ddAccountPeriod.Items.Add(new ListItem("6", "6"));
                ddAccountPeriod.Items.Add(new ListItem("5", "5"));
                ddAccountPeriod.Items.Add(new ListItem("3", "3"));
                ddAccountPeriod.Items.Add(new ListItem("2", "2"));
                ddAccountPeriod.SelectedValue = "12";
            }
        }

        private void LocalizeText() {
            btnSearch.Text = rm.GetString("txtSearch", ci);
            btnRegisterChange.Text = rm.GetString("txtRegisterChange", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);

            // Headers/titles
            lblPageTitle.Text = rm.GetString("txtFinanciaStatement", ci);
            lblControl.Text = rm.GetString("txtControl", ci);

            // Fields
            lblCIID.Text = rm.GetString("txtCigID", ci);
            lblName.Text = rm.GetString("txtNameNative", ci);
            lblUniqueIndentifier.Text = rm.GetString("txtUniqueID", ci);
            lblCompanyRegno.Text = rm.GetString("txtCompanyRegNo", ci);
            lblFiscalYear.Text = rm.GetString("txtFiscalYear", ci);
            lblAccountPeriod.Text = rm.GetString("txtAccountPeriod", ci);
            lblTemplate.Text = rm.GetString("txtTemplate", ci);

            // Datagrids			
            lblDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblFinancialStatementsHeader.Text = rm.GetString("txtFinancialStatements", ci);

            rfvFisicalYear.ErrorMessage = rm.GetString("txtValueMissing", ci);
            //rfvCIID.ErrorMessage = rm.GetString("txtValueMissing",ci);			
        }

        private void LocalizeDatagrid() {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            var nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");

            dgCompanies.Columns[1].HeaderText = rm.GetString("txtCigID", ci);
            if (culture.Equals(nativeCulture)) {
                dgCompanies.Columns[2].HeaderText = rm.GetString("txtNameNative", ci);
                dgCompanies.Columns[2].Visible = true;
                dgCompanies.Columns[3].Visible = false;
            } else {
                dgCompanies.Columns[3].HeaderText = rm.GetString("txtNameEN", ci);
                dgCompanies.Columns[2].Visible = false;
                dgCompanies.Columns[3].Visible = true;
            }
            dgCompanies.Columns[4].HeaderText = rm.GetString("txtNationalID", ci);

            dgFinancialStatements.Columns[1].HeaderText = rm.GetString("txtFiscalYear", ci);
            dgFinancialStatements.Columns[2].HeaderText = rm.GetString("txtAccountPeriod", ci);
            dgFinancialStatements.Columns[3].HeaderText = rm.GetString("txtFiscalYear", ci);
            dgFinancialStatements.Columns[4].HeaderText = rm.GetString("txtTemplate", ci);
        }

        protected void FillTemplatesBox() {
            var coll = new TemplateCollectionBLLC();
            ddTemplate.DataSource = coll;
            ddTemplate.DataValueField = "ID";
            ddTemplate.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddTemplate.DataBind();
        }

        private void AddEnterEvent() {
            tbCompanyRegNo.Attributes.Add("onkeypress", "checkEnterKey();");
            tbFiscalYear.Attributes.Add("onkeypress", "checkEnterKey();");
            ddAccountPeriod.Attributes.Add("onkeypress", "checkEnterKey();");
            ddTemplate.Attributes.Add("onkeypress", "checkEnterKey();");

            txtCreditInfoID.Attributes.Add("onkeypress", "checkSearchKey();");
            txtName.Attributes.Add("onkeypress", "checkSearchKey();");
            tbUniqueIndentifier.Attributes.Add("onkeypress", "checkSearchKey();");
        }

        private void btnRegisterChange_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {
                return;
            }
            var fact = new FinancialStatementFactory();
            var companyCIID = fact.GetCIIDByNationalID(tbCompanyRegNo.Text);
            // ef fyrirt�ki� er ekki til � kerfinu � �a� �.a.l. ekki �rsreikning
            if (companyCIID >= 0) {
                Session["FSI_tbCompanyRegNo"] = tbCompanyRegNo.Text;
                Session["FSI_year"] = tbFiscalYear.Text;
                Session["FSI_ddAccountPeriod"] = ddAccountPeriod.SelectedValue;
                Session["FSITemplateID"] = ddTemplate.SelectedValue;
                Server.Transfer("TemplateStatementInput.aspx?ref=selstate");
            } else {
                lblErrMsg.Text = rm.GetString("txtCompanyNotFound", ci);
                lblErrMsg.Visible = true;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            var templateFactory = new TemplatesFactory();

            int ciid;

            try {
                ciid = int.Parse(txtCreditInfoID.Text.Trim());
            } catch (Exception) {
                ciid = -1;
            }

            var arrCompanies = templateFactory.FindCompany(ciid, txtName.Text.Trim(), tbUniqueIndentifier.Text.Trim());

            if (arrCompanies != null && arrCompanies.Count > 0) {
                dgCompanies.DataSource = arrCompanies;
                dgCompanies.DataBind();
                if (arrCompanies.Count < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll"))) {
                    divNameSearch.Style.Remove("HEIGHT");
                } else {
                    divNameSearch.Style.Add("HEIGHT", "150px");
                }
                outerGridTable.Visible = true;
                outerFinancialStatementsGrid.Visible = false;
                trSelectRow.Visible = false;
            } else {
                lblErrMsg.Text = rm.GetString("txtNoEntryFound", ci);
            }
        }

        private void dgCompanies_ItemDataBound(object sender, DataGridItemEventArgs e) { WebDesign.CreateExplanationIcons(dgCompanies.Columns, lblDatagridIcons, rm, ci, 2); }

        private void dgCompanies_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (!e.CommandName.Equals(DataGrid.SelectCommandName)) {
                return;
            }
            trSelectRow.Visible = true;
            rfvFisicalYear.Enabled = true;

            tbCompanyRegNo.Text = e.Item.Cells[4].Text.Trim();
            tbCompanyRegNo.Enabled = false;
            txtCreditInfoID.Text = e.Item.Cells[1].Text.Trim();
            txtName.Text = e.Item.Cells[2].Text.Trim();

            outerFinancialStatementsGrid.Visible = true;
            var factory = new FinancialStatementFactory();
            dgFinancialStatements.DataSource = factory.GetFinancialStatements(
                int.Parse(e.Item.Cells[1].Text.Trim()));
            dgFinancialStatements.DataBind();

            btnCancel.Visible = true;
            btnRegisterChange.Visible = true;

            outerGridTable.Visible = false;
            ((DataGrid) source).SelectedIndex = -1;
        }

        private void dgFinancialStatements_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[4].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[4].Text = e.Item.Cells[6].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[6].Text.Trim() != "" && e.Item.Cells[6].Text != "&nbsp;") {
                        e.Item.Cells[4].Text = e.Item.Cells[6].Text;
                    } else {
                        e.Item.Cells[4].Text = e.Item.Cells[5].Text;
                    }
                }
            }

            WebDesign.CreateExplanationIcons(dgFinancialStatements.Columns, lblFinancialStatementsIcons, rm, ci, 2);
        }

        private void dgFinancialStatements_EditCommand(object source, DataGridCommandEventArgs e) {
            Session["FSI_tbCompanyRegNo"] = tbCompanyRegNo.Text;
            Session["FSI_year"] = e.Item.Cells[1].Text.Trim();
            Session["FSI_ddAccountPeriod"] = e.Item.Cells[2].Text.Trim();
            Session["FSITemplateID"] = e.Item.Cells[7].Text.Trim();
            Server.Transfer("TemplateStatementInput.aspx?ref=selstate");
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearch.Click += new EventHandler(this.btnSearch_Click);
            this.dgCompanies.ItemCommand += new DataGridCommandEventHandler(this.dgCompanies_ItemCommand);
            this.dgCompanies.ItemDataBound += new DataGridItemEventHandler(this.dgCompanies_ItemDataBound);
            this.dgFinancialStatements.EditCommand +=
                new DataGridCommandEventHandler(this.dgFinancialStatements_EditCommand);
            this.dgFinancialStatements.ItemDataBound +=
                new DataGridItemEventHandler(this.dgFinancialStatements_ItemDataBound);
            this.btnRegisterChange.Click += new EventHandler(this.btnRegisterChange_Click);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}
#pragma warning restore 618,612
