#region

using System;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using FSI.DAL;
using FSI.Templates.BLL;
using Logging.BLL;
using cb3;
using cb3.Audit;

#endregion

namespace FSI.Templates.DAL {
    /// <summary>
    /// Summary description for FSITemplatesDALC.
    /// </summary>
    public class TemplatesDALC : BaseDALC {
        public TemplatesDALC() { ClassName = "TemplatesDALC "; }

        public bool FillTemplate(TemplateBLLC template) {
            FunctionName = "FillTemplate(TemplateBLLC template) ";
            string query = "SELECT * FROM FSI_Templates WHERE ID = " + template.ID;
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                template.NameNative = ds.Tables[0].Rows[0]["NameNative"].ToString();
                template.NameEN = ds.Tables[0].Rows[0]["NameEN"].ToString();
                template.DescriptionNative = ds.Tables[0].Rows[0]["DescriptionNative"].ToString();
                template.DescriptionEN = ds.Tables[0].Rows[0]["DescriptionEN"].ToString();
                return true;
            }
            return false;
        }

        public bool FillField(FieldBLLC field) {
            FunctionName = "FillField(FieldBLLC field) ";
            string query = "SELECT * FROM FSI_Fields WHERE ID = " + field.ID;
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                field.NameNative = ds.Tables[0].Rows[0]["NameNative"].ToString();
                field.NameEN = ds.Tables[0].Rows[0]["NameEN"].ToString();
                field.DescriptionNative = ds.Tables[0].Rows[0]["DescriptionNative"].ToString();
                field.DescriptionEN = ds.Tables[0].Rows[0]["DescriptionEN"].ToString();
                field.Type = ds.Tables[0].Rows[0]["Type"].ToString();
                field.FieldName = ds.Tables[0].Rows[0]["FieldName"].ToString();
                field.CategoryID = int.Parse(ds.Tables[0].Rows[0]["Category"].ToString());
                return true;
            }
            return false;
        }

        public bool FillTemplates(TemplateCollectionBLLC templates) {
            FunctionName = "FillTemplates(TemplatCollectionBLLC fields) ";
            const string query = "SELECT * from FSI_Templates";
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                templates.Clear();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    var template = new TemplateBLLC
                                   {
                                       ID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString()),
                                       NameNative = ds.Tables[0].Rows[i]["NameNative"].ToString(),
                                       NameEN = ds.Tables[0].Rows[i]["NameEN"].ToString(),
                                       DescriptionNative = ds.Tables[0].Rows[i]["DescriptionNative"].ToString(),
                                       DescriptionEN = ds.Tables[0].Rows[i]["DescriptionEN"].ToString(),
                                       IsNew = false
                                   };
                    templates.Add(template);
                }
                return true;
            }
            return false;
        }

        public bool FillFields(FieldCollectionBLLC fields) {
            FunctionName = "FillFields(FieldCollectionBLLC fields) ";
            const string query = "SELECT * from FSI_Fields order by Category, ID";
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                fields.Clear();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    var field = new FieldBLLC
                                {
                                    ID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString()),
                                    NameNative = ds.Tables[0].Rows[i]["NameNative"].ToString(),
                                    NameEN = ds.Tables[0].Rows[i]["NameEN"].ToString(),
                                    DescriptionNative = ds.Tables[0].Rows[i]["DescriptionNative"].ToString(),
                                    DescriptionEN = ds.Tables[0].Rows[i]["DescriptionEN"].ToString(),
                                    FieldName = ds.Tables[0].Rows[i]["FieldName"].ToString(),
                                    Type = ds.Tables[0].Rows[i]["Type"].ToString(),
                                    CategoryID = int.Parse(ds.Tables[0].Rows[i]["Category"].ToString())
                                };
                    fields.Add(field);
                }
                return true;
            }
            return false;
        }

        public bool FillFields(FieldCollectionBLLC fields, int templateID) {
            FunctionName = "FillFields(FieldCollectionBLLC fields) ";
            string query =
                "SELECT f.* from FSI_Fields f, FSI_TemplateFields tf where f.ID = tf.FieldID and tf.TemplateID = " +
                templateID + " ORDER BY f.Category, f.ID";
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                fields.Clear();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    var field = new FieldBLLC
                                {
                                    ID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString()),
                                    NameNative = ds.Tables[0].Rows[i]["NameNative"].ToString(),
                                    NameEN = ds.Tables[0].Rows[i]["NameEN"].ToString(),
                                    DescriptionNative = ds.Tables[0].Rows[i]["DescriptionNative"].ToString(),
                                    DescriptionEN = ds.Tables[0].Rows[i]["DescriptionEN"].ToString(),
                                    FieldName = ds.Tables[0].Rows[i]["FieldName"].ToString(),
                                    Type = ds.Tables[0].Rows[i]["Type"].ToString(),
                                    CategoryID = int.Parse(ds.Tables[0].Rows[i]["Category"].ToString())
                                };
                    fields.Add(field);
                }
                return true;
            }
            return false;
        }

        public bool FillTemplateField(TemplateFieldBLLC field) {
            FunctionName = "FillTemplateField(TemplateFieldBLLC field) ";
            string query = "SELECT * from FSI_TemplateFields where ID = " + field.ID;
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                field.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                field.TemplateID = int.Parse(ds.Tables[0].Rows[0]["TemplateID"].ToString());
                field.FieldID = int.Parse(ds.Tables[0].Rows[0]["FieldID"].ToString());
                field.Order = int.Parse(ds.Tables[0].Rows[0]["Order"].ToString());
                field.IsModified = false;
                return true;
            }
            return false;
        }

        public bool FillTemplateFields(TemplateFieldsCollectionBLLC fields, int templateID) {
            FunctionName = "FillTemplateFields(TemplateFieldsCollectionBLLC fields, int tempateID) ";
            string query = "SELECT * from FSI_TemplateFields where TemplateID = " + templateID + " ORDER BY [Order]";
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                fields.Clear();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    var field = new TemplateFieldBLLC
                                {
                                    ID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString()),
                                    TemplateID = int.Parse(ds.Tables[0].Rows[i]["TemplateID"].ToString()),
                                    FieldID = int.Parse(ds.Tables[0].Rows[i]["FieldID"].ToString()),
                                    Order = int.Parse(ds.Tables[0].Rows[i]["Order"].ToString()),
                                    IsModified = false
                                };
                    fields.Add(field);
                }
                return true;
            }
            return false;
        }

        public bool FillFieldValidation(FieldValidationBLLC validation) {
            FunctionName = "FillFieldValidation(FieldValidationBLLC validation) ";
            string query = "SELECT * FROM FSI_FieldValidations WHERE ID = " + validation.ID;
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                validation.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                validation.TemplateFieldID = -1;
                validation.NameNative = ds.Tables[0].Rows[0]["NameNative"].ToString();
                validation.NameEN = ds.Tables[0].Rows[0]["NameEN"].ToString();
                validation.DescriptionNative = ds.Tables[0].Rows[0]["DescriptionNative"].ToString();
                validation.DescriptionEN = ds.Tables[0].Rows[0]["DescriptionEN"].ToString();
                return true;
            }
            return false;
        }

        public bool FillFieldValidations(FieldValidationCollectionBLLC validations) {
            FunctionName = "FillFieldValidations(FieldValidationCollectionBLLC validations, int templateFieldID ";
            string query = "SELECT * FROM FSI_FieldValidations";
            if (validations.TemplateFieldID > 0) {
                query =
                    "SELECT fv.* FROM FSI_FieldValidations fv, FSI_TemplateFieldValidations tfv WHERE fv.ID=tfv.FieldValidationID AND tfv.TemplateFieldID = " +
                    validations.TemplateFieldID;
            }
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                validations.Clear();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    var fv = new FieldValidationBLLC
                             {
                                 ID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString()),
                                 NameNative = ds.Tables[0].Rows[i]["NameNative"].ToString(),
                                 NameEN = ds.Tables[0].Rows[i]["NameEN"].ToString(),
                                 DescriptionNative = ds.Tables[0].Rows[i]["DescriptionNative"].ToString(),
                                 DescriptionEN = ds.Tables[0].Rows[i]["DescriptionEN"].ToString(),
                                 TemplateFieldID = validations.TemplateFieldID
                             };
                    validations.Add(fv);
                }
                return true;
            }
            return false;
        }

        public bool FillOperator(OperatorBLLC oper) {
            FunctionName = "FillOperator(OperatorBLLC oper) ";
            string query = "SELECT * from FSI_Operators WHERE ID = " + oper.ID;
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                oper.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                oper.NameNative = ds.Tables[0].Rows[0]["OperatorNameNative"].ToString();
                oper.NameEN = ds.Tables[0].Rows[0]["OperatorNameEN"].ToString();
                oper.Symbol = ds.Tables[0].Rows[0]["Symbol"].ToString();
                return true;
            }
            return false;
        }

        public bool FillOperators(OperatorCollectionBLLC collection) {
            FunctionName = "FillOperators(OperatorCollectionBLLC collection) ";
            const string query = "SELECT * from FSI_Operators";
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                collection.Clear();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    var oper = new OperatorBLLC
                               {
                                   ID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString()),
                                   NameNative = ds.Tables[0].Rows[i]["OperatorNameNative"].ToString(),
                                   NameEN = ds.Tables[0].Rows[i]["OperatorNameEN"].ToString(),
                                   Symbol = ds.Tables[0].Rows[i]["Symbol"].ToString()
                               };
                    collection.Add(oper);
                }
                return true;
            }
            return false;
        }

        public bool FillFieldValidationFormulas(FieldValidationFormulaCollectionBLLC collection, int validationID) {
            FunctionName =
                "FillFieldValidationFormulas(FieldValidationFormulaCollectionBLLC collection, int validationID) ";
            string query = "SELECT * from FSI_FieldValidationFormulas WHERE ValidationID =" + validationID +
                           " ORDER BY [ORDER]";
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                collection.Clear();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    var formula = new FieldValidationFormulaBLLC
                                  {
                                      ID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString()),
                                      ValidationID = int.Parse(ds.Tables[0].Rows[i]["ValidationID"].ToString()),
                                      ValidationItemType = ds.Tables[0].Rows[i]["ValidationItemType"].ToString(),
                                      ValidationItemID = int.Parse(ds.Tables[0].Rows[i]["ValidationItemID"].ToString()),
                                      Order = int.Parse(ds.Tables[0].Rows[i]["Order"].ToString())
                                  };
                    collection.Add(formula);
                }
                return true;
            }
            return false;
        }

        public bool FillCategory(CategoryBLLC category) {
            FunctionName = "FillCategory(CategoryBLLC category) ";
            string query = "SELECT ID, NameNative, NameEN from FSI_FieldCategory WHERE ID = " + category.ID;
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                category.NameNative = ds.Tables[0].Rows[0]["NameNative"].ToString();
                category.NameEN = ds.Tables[0].Rows[0]["NameEN"].ToString();
                return true;
            }
            return false;
        }

        public bool FillCategorys(CategoryCollectionBLLC collection) {
            FunctionName = "FillCategorys(CategoryCollectionBLLC collection) ";
            const string query = "SELECT ID, NameNative, NameEN from FSI_FieldCategory";
            var ds = ExecuteSelectStatement(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                collection.Clear();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    var category = new CategoryBLLC
                                   {
                                       ID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString()),
                                       NameNative = ds.Tables[0].Rows[i]["NameNative"].ToString(),
                                       NameEN = ds.Tables[0].Rows[i]["NameEN"].ToString()
                                   };
                    collection.Add(category);
                }
                return true;
            }
            return false;
        }

        public bool SaveFieldValidationFormulas(FieldValidationFormulaCollectionBLLC collection) {
            for (int i = 0; i < collection.Count; i++) {
                FieldValidationFormulaBLLC formula = collection.GetFormula(i);
                if (formula.IsModified) {
                    UpdateFieldValidationFormula(formula);
                } else if (formula.IsDeleted) {
                    DeleteFieldValidationFormula(formula);
                } else if (formula.IsNew) {
                    InsertFieldValidationFormula(formula);
                }
            }
            FillFieldValidationFormulas(collection, collection.ValidationID);
            return true;
        }

        protected bool InsertFieldValidationFormula(FieldValidationFormulaBLLC formula) {
            FunctionName = "InsertFieldValidationFormula(FieldValidationFormulaBLLC formula) ";
            try {
                using (OleDbConnection myOleDbConn = OpenConnection()) {
                    try {
                        const string query = "INSERT INTO FSI_FieldValidationFormulas(ValidationID, ValidationItemType, ValidationItemID, [Order]) VALUES (?,?,?,?)";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("ValidationID", OleDbType.Integer)
                                      {
                                          Value = formula.ValidationID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ValidationItemType", OleDbType.VarChar)
                                  {
                                      Value = formula.ValidationItemType,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ValidationItemID", OleDbType.Integer)
                                  {
                                      Value = formula.ValidationItemID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("[Order]", OleDbType.Integer)
                                  {
                                      Value = formula.Order,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        myOleDbCommand.CommandText = "select @@identity";
                        myOleDbCommand.Parameters.Clear();
                        formula.ID = int.Parse(myOleDbCommand.ExecuteScalar().ToString());
                        formula.IsNew = false;
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        protected bool UpdateFieldValidationFormula(FieldValidationFormulaBLLC formula) {
            FunctionName = "UpdateFieldValidationFormula(FieldValidationFormulaBLLC formula) ";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        const string query = "UPDATE FSI_FieldValidationFormulas SET ValidationItemType = ?, ValidationItemID = ?, [Order] = ? WHERE ID = ? AND ValidationID = ?";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("ValidationItemType", OleDbType.VarChar)
                                      {
                                          Value = formula.ValidationItemType,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ValidationItemID", OleDbType.Integer)
                                  {
                                      Value = formula.ValidationItemID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("[Order]", OleDbType.Integer)
                                  {
                                      Value = formula.Order,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ID", OleDbType.Integer)
                                  {
                                      Value = formula.ID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ValidationID", OleDbType.Integer)
                                  {
                                      Value = formula.ValidationID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        formula.IsNew = false;
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        protected bool DeleteFieldValidationFormula(FieldValidationFormulaBLLC formula) {
            FunctionName = "DeleteFieldValidationFormula(FieldValidationFormulaBLLC formula) ";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        const string query = "DELETE FROM FSI_FieldValidationFormulas WHERE ValidationID = ? AND ID = ?";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("ValidationID", OleDbType.Integer)
                                      {
                                          Value = formula.ValidationID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("ID", OleDbType.Integer)
                                  {
                                      Value = formula.ID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public bool SaveTemplateFields(TemplateFieldsCollectionBLLC collection) {
            for (int i = 0; i < collection.Count; i++) {
                TemplateFieldBLLC field = collection.GetField(i);
                if (field.IsModified) {
                    UpdateTemplateField(field);
                } else if (field.IsDeleted) {
                    DeleteTemplateField(field);
                } else if (field.IsNew) {
                    InsertTemplateField(field);
                }
            }
            FillTemplateFields(collection, collection.TemplateID);
            return true;
        }

        protected bool InsertTemplateField(TemplateFieldBLLC field) {
            FunctionName = "InsertTemplateField(TemplateFieldBLLC field";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        const string query = "INSERT INTO FSI_TemplateFields(TemplateID, FieldID, [Order]) VALUES (?,?,?)";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("TemplateID", OleDbType.Integer)
                                      {
                                          Value = field.TemplateID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("FieldID", OleDbType.Integer)
                                  {
                                      Value = field.FieldID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("[Order]", OleDbType.Integer)
                                  {
                                      Value = field.Order,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        myOleDbCommand.CommandText = "select @@identity";
                        myOleDbCommand.Parameters.Clear();
                        field.ID = int.Parse(myOleDbCommand.ExecuteScalar().ToString());
                        field.IsNew = false;
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        protected bool UpdateTemplateField(TemplateFieldBLLC field) {
            FunctionName = "UpdateTemplateField(TemplateFieldBLLC field) ";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        const string query = "UPDATE FSI_TemplateFields SET [Order] = ? WHERE TemplateID = ? AND FieldID = ?";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("[Order]", OleDbType.Integer)
                                      {
                                          Value = field.Order,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("TemplateID", OleDbType.Integer)
                                  {
                                      Value = field.TemplateID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("FieldID", OleDbType.Integer)
                                  {
                                      Value = field.FieldID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        field.IsNew = false;
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        protected bool DeleteTemplateField(TemplateFieldBLLC field) {
            FunctionName = "DeleteTemplateField(TemplateFieldBLLC field) ";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    var myTrans = myOleDbConn.BeginTransaction();
                    try {
                        //First we must delete all validations from the field
                        string query = "DELETE FROM FSI_TemplateFieldValidations WHERE TemplateFieldID = ?";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn) {Transaction = myTrans};

                        var myParam = new OleDbParameter("TemplateFieldID", OleDbType.Integer)
                                      {
                                          Value = field.ID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        query = "DELETE FROM FSI_TemplateFields WHERE TemplateID = ? AND FieldID = ?";
                        myOleDbCommand.CommandText = query;
                        myOleDbCommand.Parameters.Clear();

                        myParam = new OleDbParameter("TemplateID", OleDbType.Integer)
                                  {
                                      Value = field.TemplateID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("FieldID", OleDbType.Integer)
                                  {
                                      Value = field.FieldID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        myTrans.Commit();
                    } catch (Exception e) {
                        myTrans.Rollback();
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public bool SaveTemplate(TemplateBLLC template) {
            if (template.IsNew) {
                return InsertTemplate(template);
            }
            return UpdateTemplate(template);
        }

        protected bool InsertTemplate(TemplateBLLC template) {
            FunctionName = "InsertTemplate(TemplateBLLC template)";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    const string query = "INSERT INTO FSI_Templates(NameNative, NameEN, DescriptionNative, DescriptionEN) VALUES " +
                                         "(?,?,?,?)";
                    try {
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("NameNative", OleDbType.VarChar)
                                      {
                                          Value = template.NameNative,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("NameEN", OleDbType.VarChar)
                                  {
                                      Value = template.NameEN,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DescriptionNative", OleDbType.VarChar)
                                  {
                                      Value = template.DescriptionNative,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DescriptionEN", OleDbType.VarChar)
                                  {
                                      Value = template.DescriptionEN,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        myOleDbCommand.CommandText = "select @@identity";
                        myOleDbCommand.Parameters.Clear();
                        template.ID = int.Parse(myOleDbCommand.ExecuteScalar().ToString());
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        protected bool UpdateTemplate(TemplateBLLC template) {
            FunctionName = "UpdateTemplate(TemplateBLLC template)";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        string query =
                            "UPDATE FSI_Templates SET NameNative=?, NameEN=?, DescriptionNative=?, DescriptionEN=? " +
                            " WHERE ID = " + template.ID;
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("NameNative", OleDbType.VarChar)
                                      {
                                          Value = template.NameNative,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("NameEN", OleDbType.VarChar)
                                  {
                                      Value = template.NameEN,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DescriptionNative", OleDbType.VarChar)
                                  {
                                      Value = template.DescriptionNative,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DescriptionEN", OleDbType.VarChar)
                                  {
                                      Value = template.DescriptionEN,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public bool AddValidationsToTemplateField(FieldValidationCollectionBLLC coll) {
            FunctionName = "AddValidationsToTemplateField(FieldValidationCollectionBLLC coll)";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        const string query = "INSERT INTO FSI_TemplateFieldValidations(TemplateFieldID, FieldValidationID) VALUES " +
                                             "(?,?)";
                        for (int i = 0; i < coll.Count; i++) {
                            var validation = coll.GetField(i);

                            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                            var myParam = new OleDbParameter("TemplateFieldID", OleDbType.Integer)
                                          {
                                              Value = coll.TemplateFieldID,
                                              Direction = ParameterDirection.Input
                                          };
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("FieldValidationID", OleDbType.Integer)
                                      {
                                          Value = validation.ID,
                                          Direction = ParameterDirection.Input
                                      };
                            myOleDbCommand.Parameters.Add(myParam);

                            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                        }
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public bool SaveFieldValidation(FieldValidationBLLC validation) { return validation.IsNew ? InsertFieldValidation(validation) : UpdateFieldValidation(validation); }

        protected bool InsertFieldValidation(FieldValidationBLLC validation) {
            FunctionName = "InsertFieldValidation(FieldValidationBLLC validation)";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        const string query = "INSERT INTO FSI_FieldValidations(NameNative, NameEN, DescriptionNative, DescriptionEN) VALUES " +
                                             "(?,?,?,?)";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("NameNative", OleDbType.VarChar)
                                      {
                                          Value = validation.NameNative,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("NameEN", OleDbType.VarChar)
                                  {
                                      Value = validation.NameEN,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DescriptionNative", OleDbType.VarChar)
                                  {
                                      Value = validation.DescriptionNative,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DescriptionEN", OleDbType.VarChar)
                                  {
                                      Value = validation.DescriptionEN,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        myOleDbCommand.CommandText = "select @@identity";
                        validation.ID = int.Parse(myOleDbCommand.ExecuteScalar().ToString());

                        if (validation.TemplateFieldID > 0) {
                            myOleDbCommand.CommandText =
                                "INSERT INTO FSI_TemplateFieldValidations(TemplateFieldID, FieldValidationID) VALUES (?,?)";
                            myOleDbCommand.Parameters.Clear();
                            myParam = new OleDbParameter("TemplateFieldID", OleDbType.Integer)
                                      {
                                          Value = validation.TemplateFieldID,
                                          Direction = ParameterDirection.Input
                                      };
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("FieldValidationID", OleDbType.Integer)
                                      {
                                          Value = validation.ID,
                                          Direction = ParameterDirection.Input
                                      };
                            myOleDbCommand.Parameters.Add(myParam);

                            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                        }
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public bool AreSomeFSUsingTemplate(TemplateBLLC template) {
            FunctionName = "IsStatementRegisted(int companyCIID, int year)";
            using (var myOleDbConn = OpenConnection()) {
                try {
                    var myOleDbCommand =
                        new OleDbCommand("SELECT * FROM FSI_TemplateGeneralInfo WHERE TemplateID = ?", myOleDbConn);
                    var myParam = new OleDbParameter("TemplateID", OleDbType.Integer)
                                  {
                                      Value = template.ID,
                                      Direction = ParameterDirection.Input
                                  };
                    myOleDbCommand.Parameters.Add(myParam);

                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        reader.Close();
                        return true;
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    return false;
                }
            }
            return false;
        }

        public bool DeleteTemplate(TemplateBLLC template) {
            FunctionName = "DeleteTemplate(TemplateBLLC template) ";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    var myTrans = myOleDbConn.BeginTransaction();
                    try {
                        //First remove validation from all fields in template
                        string query =
                            "DELETE FROM FSI_TemplateFieldValidations WHERE TemplateFieldID in(SELECT ID FROM FSI_TemplateFields WHERE TemplateID = ?)";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn) {Transaction = myTrans};

                        var myParam = new OleDbParameter("TemplateID", OleDbType.Integer)
                                      {
                                          Value = template.ID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        //Remove all template fields
                        query = "DELETE FROM FSI_TemplateFields WHERE TemplateID = ?";
                        myOleDbCommand.CommandText = query;
                        myOleDbCommand.Parameters.Clear();
                        myParam = new OleDbParameter("TemplateID", OleDbType.Integer)
                                  {
                                      Value = template.ID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        //Then remove the template itself
                        query = "DELETE FROM FSI_Templates WHERE ID = ?";
                        myOleDbCommand.CommandText = query;
                        myOleDbCommand.Parameters.Clear();
                        myParam = new OleDbParameter("ID", OleDbType.Integer)
                                  {
                                      Value = template.ID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        myTrans.Commit();
                    } catch (Exception e) {
                        myTrans.Rollback();
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public bool DeleteFieldValidation(FieldValidationBLLC validation) {
            FunctionName = "DeleteFieldValidation(FieldValidationBLLC validation) ";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    var myTrans = myOleDbConn.BeginTransaction();
                    try {
                        //First remove validation from all fields
                        string query = "DELETE FROM FSI_TemplateFieldValidations WHERE FieldValidationID = ?";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn) {Transaction = myTrans};

                        var myParam = new OleDbParameter("FieldValidationID", OleDbType.Integer)
                                      {
                                          Value = validation.ID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        //The remove the validation formulas
                        query = "DELETE FROM FSI_FieldValidationFormulas WHERE ValidationID = ?";
                        myOleDbCommand.CommandText = query;
                        myOleDbCommand.Parameters.Clear();
                        myParam = new OleDbParameter("ValidationID", OleDbType.Integer)
                                  {
                                      Value = validation.ID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        //Then remove the validation itself
                        query = "DELETE FROM FSI_FieldValidations WHERE ID = ?";
                        myOleDbCommand.CommandText = query;
                        myOleDbCommand.Parameters.Clear();
                        myParam = new OleDbParameter("ID", OleDbType.Integer)
                                  {
                                      Value = validation.ID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                        myTrans.Commit();
                    } catch (Exception e) {
                        myTrans.Rollback();
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public bool RemoveValidationFromTemplateField(FieldValidationBLLC validation) {
            FunctionName = "RemoveValidationFromTemplateField(FieldValidationBLLC validation) ";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        const string query = "DELETE FROM FSI_TemplateFieldValidations WHERE TemplateFieldID = ? AND FieldValidationID = ?";
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("TemplateFieldID", OleDbType.Integer)
                                      {
                                          Value = validation.TemplateFieldID,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("FieldValidationID", OleDbType.Integer)
                                  {
                                      Value = validation.ID,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        protected bool UpdateFieldValidation(FieldValidationBLLC validation) {
            FunctionName = "UpdateTemplate(TemplateBLLC template)";
            try {
                using (var myOleDbConn = OpenConnection()) {
                    try {
                        var query =
                            "UPDATE FSI_FieldValidations SET NameNative=?, NameEN=?, DescriptionNative=?, DescriptionEN=? " +
                            " WHERE ID = " + validation.ID;
                        var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                        var myParam = new OleDbParameter("NameNative", OleDbType.VarChar)
                                      {
                                          Value = validation.NameNative,
                                          Direction = ParameterDirection.Input
                                      };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("NameEN", OleDbType.VarChar)
                                  {
                                      Value = validation.NameEN,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DescriptionNative", OleDbType.VarChar)
                                  {
                                      Value = validation.DescriptionNative,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        myParam = new OleDbParameter("DescriptionEN", OleDbType.VarChar)
                                  {
                                      Value = validation.DescriptionEN,
                                      Direction = ParameterDirection.Input
                                  };
                        myOleDbCommand.Parameters.Add(myParam);

                        new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                    } catch (Exception e) {
                        Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", e, true);
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", e, true);
                return false;
            }
            return true;
        }

        public int ExecuteScalarSQL(string strSQL) {
            try {
                var command = new OleDbCommand {Connection = OpenConnection(), CommandText = strSQL};
                return Convert.ToInt32(command.ExecuteScalar());
            } catch (Exception ex) {
                Logger.WriteToLog(ClassName + ": " + FunctionName + " : ", ex, true);
                return -1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="creditInfoID"></param>
        /// <param name="name"></param>
        /// <param name="nationalID"></param>
        /// <returns></returns>
        public ArrayList FindCompany(int creditInfoID, string name, string nationalID) {
            const string funcName = "FindCompany(int creditInfoID, string name, string nationalID) ";
            string connectionString1 = DatabaseHelper.ConnectionString();
            var myList = new ArrayList();

            using (var myOleDbConn = new OleDbConnection(connectionString1)) {
                try {
                    myOleDbConn.Open();

                    string query =
                        "SELECT TOP 200 np_Companys.NameNative, np_Companys.NameEN, np_IDNumbers.Number, np_CreditInfoUser.CreditInfoID FROM " +
                        "np_CreditInfoUser LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers " +
                        "ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID ";

                    string where = "WHERE ";
                    bool whereStatement = false;

                    if (creditInfoID > -1) {
                        where += "np_CreditInfoUser.CreditInfoID = " + creditInfoID;
                        whereStatement = true;
                    }
                    if (nationalID.Length > 0) {
                        if (whereStatement) {
                            where += " AND ";
                        }
                        //hsj 2006.02.02 - don't limit search to numbertypeid for LT - np_IDNumbers.NumberTypeID = " + System.Configuration.CigConfig.Configure("lookupsettings.nationalID"].Trim() + " AND 
                        where += "np_IDNumbers.Number = '" + nationalID + "'";
                        whereStatement = true;
                    }
                    if (name.Length > 0) {
                        if (whereStatement) {
                            where += "AND ";
                        }

                        where +=
                            "exists (select * from np_word2ciid where np_Companys.creditinfoid=np_word2ciid.creditinfoid and np_word2ciid.type='n' ";
                        where += "and np_word2ciid.word like (N'" + name + "%')) ";

                        whereStatement = true;
                    }
                    if (whereStatement) {
                        query += where;
                    }

                    var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    CompanyBLLC theCompany;

                    while (reader.Read()) {
                        theCompany = new CompanyBLLC();

                        if (!reader.IsDBNull(0)) {
                            theCompany.NameNative = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theCompany.NameEN = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theCompany.UniqueID = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            theCompany.CompanyCIID = reader.GetInt32(3);
                        }

                        myList.Add(theCompany);
                    }
                    reader.Close();
                } catch (Exception e) {
                    Logger.WriteToLog("TemplatesDALC" + " : " + funcName, e, true);
                }
            }
            return myList;
        }
    }
}