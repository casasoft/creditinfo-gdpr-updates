#region

using System;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using System.Text;
using cb3.Audit;
using FSI.DAL;
using FSI.Templates.BLL;
using Logging.BLL;

#endregion

namespace FSI.Templates.DAL {
    /// <summary>
    /// Summary description for StatementsDALC.
    /// </summary>
    public class StatementsDALC : BaseDALC {
        public StatementsDALC() { ClassName = "StatementsDALC"; }

        public bool AddFinancialStatement(GeneralInfoBLLC info) {
            FunctionName = "AddFinancialStatement(GeneralInfoBLLC info) ";

            return info.AFS_id <= 0 ? InsertFinancialStatement(info) : UpdateFinancialStatement(info);
        }

        public DataTable GetTemplateStatementsValues(int templateID, int afs_id) {
            FunctionName = "GetTemplateStatementsValues(int templateID, int afs_id)";
            string query = "SELECT * FROM FSI_TEMPLATESTATEMENTS WHERE AFSID=" + afs_id;
            return LoadDataTable(query);
        }

        public bool FillGeneralInfo(GeneralInfoBLLC info) {
            FunctionName = "FillGeneralInfo(GeneralInfoBLLC info)";
            using (var myOleDbConn = OpenConnection()) {
                try {
                    string sql =
                        "SELECT FSI_TemplateGeneralInfo.AFS_id, FSI_TemplateGeneralInfo.TemplateID, FSI_TemplateGeneralInfo.CreditInfoID, FSI_TemplateGeneralInfo.Financial_year, " +
                        "FSI_TemplateGeneralInfo.Financial_year_end, FSI_TemplateGeneralInfo.Account_period_length, FSI_TemplateGeneralInfo.Currency, " +
                        "FSI_TemplateGeneralInfo.Consolidated, FSI_TemplateGeneralInfo.Qualified_audit, FSI_TemplateGeneralInfo.Reviewed, FSI_TemplateGeneralInfo.Denomination_id, " +
                        "FSI_TemplateGeneralInfo.Employee_count, FSI_TemplateGeneralInfo.Comments_id, FSI_TemplateGeneralInfo.Date_of_delivery, FSI_TemplateGeneralInfo.Ready_for_web_publishing, " +
                        "FSI_TemplateGeneralInfo.Origin_id, FSI_TemplateGeneralInfo.Registrator_id, FSI_TemplateGeneralInfo.Modified, FSI_Denomination.Description_native AS DenominationNative, " +
                        "FSI_Denomination.Description_en AS DenominationEN, FSI_Comments.Description_native AS CommentsNative, FSI_Comments.Description_en AS CommentsEN, FSI_TemplateGeneralInfo.Smallstatus " +
                        "FROM FSI_TemplateGeneralInfo LEFT JOIN FSI_Denomination ON FSI_TemplateGeneralInfo.Denomination_id = FSI_Denomination.Denomination_id LEFT JOIN " +
                        "FSI_Comments ON FSI_TemplateGeneralInfo.Comments_id = FSI_Comments.Comments_id WHERE " +
                        "(FSI_TemplateGeneralInfo.CreditInfoID = " + info.CompanyCIID +
                        ") AND (FSI_TemplateGeneralInfo.Financial_year = " + info.FiscalYear + ") " +
                        "AND (FSI_TemplateGeneralInfo.Account_period_length = " + info.AccountMonths + ")";

                    //	string sql = "SELECT AFS_id,CreditInfoID,Financial_year,Financial_year_end,Account_period_length, " +
                    //	"Currency,Consolidated,Qualified_audit,Reviewed,Denomination_id,Employee_count,Comments_id,Date_of_delivery,Ready_for_web_publishing,Origin_id,Registrator_id,Modified FROM FSI_GeneralInfo WHERE CreditInfoID = "+ companyCIID +" AND Financial_year = "+ year +" AND Account_period_length = "+ accountPeriod +"";

                    info.Last_Year_AFS_id = GetLastYearAFS_ID(
                        info.CompanyCIID, int.Parse(info.FiscalYear), info.AccountMonths);

                    var myOleDbCommand = new OleDbCommand(sql, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        info.AFS_id = reader.GetInt32(0);
                        info.TemplateID = reader.GetInt32(1);
                        info.CompanyCIID = reader.GetInt32(2);
                        info.FiscalYear = reader.GetString(3);
                        if (!reader.IsDBNull(4)) {
                            info.YearEnded = reader.GetDateTime(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            info.AccountMonths = reader.GetInt32(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            info.CurrencyID = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            info.Consolidated = Convert.ToBoolean(reader.GetString(7));
                        }
                        if (!reader.IsDBNull(8)) {
                            info.Qualified = Convert.ToBoolean(reader.GetString(8));
                        }
                        if (!reader.IsDBNull(9)) {
                            info.Reviewed = Convert.ToBoolean(reader.GetString(9));
                        }
                        if (!reader.IsDBNull(10)) {
                            info.DenominationID = reader.GetInt32(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            info.StaffCount = reader.GetInt32(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            info.CommentsID = reader.GetInt32(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            info.DateOfDelivery = reader.GetDateTime(13);
                        }
                        if (!reader.IsDBNull(14)) {
                            info.ReadyForWebPublishing = Convert.ToBoolean(reader.GetString(14));
                        }
                        if (!reader.IsDBNull(15)) {
                            info.OriginID = reader.GetInt32(15);
                        }
                        if (!reader.IsDBNull(16)) {
                            info.RegistratorID = reader.GetInt32(16);
                        }
                        if (!reader.IsDBNull(17)) {
                            info.Modified = reader.GetDateTime(17);
                        }

                        if (!reader.IsDBNull(18)) {
                            info.DenominationNative = reader.GetString(18);
                        }
                        if (!reader.IsDBNull(19)) {
                            info.DenominationEN = reader.GetString(19);
                        }
                        if (!reader.IsDBNull(20)) {
                            info.CommentsNative = reader.GetString(20);
                        }
                        if (!reader.IsDBNull(21)) {
                            info.CommentsEN = reader.GetString(21);
                        }

                        if (!reader.IsDBNull(22))
                        {
                            info.SmallStatus = Boolean.Parse(reader.GetString(22));
                        }

                        info.MostRecentAccount = GetMostRecentAccount(info.CompanyCIID);

                        return true;

                        /*	if(!reader.IsDBNull(3))
								myNumber.NumberTypeNative = reader.GetString(3);
							if(!reader.IsDBNull(4))
								myNumber.NumberTypeID = reader.GetInt32(4);
						*/

                        //	myArr.Add(myNumber);
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    return false;
                }
            }
            return false;
        }

        public bool FillGeneralInfoFromAFS_ID(GeneralInfoBLLC info) {
            FunctionName = "FillGeneralInfo(GeneralInfoBLLC info)";
            using (var myOleDbConn = OpenConnection()) {
                try {
                    string sql =
                        "SELECT FSI_TemplateGeneralInfo.AFS_id, FSI_TemplateGeneralInfo.TemplateID, FSI_TemplateGeneralInfo.CreditInfoID, FSI_TemplateGeneralInfo.Financial_year, " +
                        "FSI_TemplateGeneralInfo.Financial_year_end, FSI_TemplateGeneralInfo.Account_period_length, FSI_TemplateGeneralInfo.Currency, " +
                        "FSI_TemplateGeneralInfo.Consolidated, FSI_TemplateGeneralInfo.Qualified_audit, FSI_TemplateGeneralInfo.Reviewed, FSI_TemplateGeneralInfo.Denomination_id, " +
                        "FSI_TemplateGeneralInfo.Employee_count, FSI_TemplateGeneralInfo.Comments_id, FSI_TemplateGeneralInfo.Date_of_delivery, FSI_TemplateGeneralInfo.Ready_for_web_publishing, " +
                        "FSI_TemplateGeneralInfo.Origin_id, FSI_TemplateGeneralInfo.Registrator_id, FSI_TemplateGeneralInfo.Modified, FSI_Denomination.Description_native AS DenominationNative, " +
                        "FSI_Denomination.Description_en AS DenominationEN, FSI_Comments.Description_native AS CommentsNative, FSI_Comments.Description_en AS CommentsEN " +
                        "FROM FSI_TemplateGeneralInfo LEFT JOIN FSI_Denomination ON FSI_TemplateGeneralInfo.Denomination_id = FSI_Denomination.Denomination_id LEFT JOIN " +
                        "FSI_Comments ON FSI_TemplateGeneralInfo.Comments_id = FSI_Comments.Comments_id WHERE " +
                        "(FSI_TemplateGeneralInfo.AFS_id = " + info.AFS_id + ")";

                    var myOleDbCommand = new OleDbCommand(sql, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        info.AFS_id = reader.GetInt32(0);
                        info.TemplateID = reader.GetInt32(1);
                        info.CompanyCIID = reader.GetInt32(2);
                        info.FiscalYear = reader.GetString(3);
                        if (!reader.IsDBNull(4)) {
                            info.YearEnded = reader.GetDateTime(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            info.AccountMonths = reader.GetInt32(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            info.CurrencyID = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            info.Consolidated = Convert.ToBoolean(reader.GetString(7));
                        }
                        if (!reader.IsDBNull(8)) {
                            info.Qualified = Convert.ToBoolean(reader.GetString(8));
                        }
                        if (!reader.IsDBNull(9)) {
                            info.Reviewed = Convert.ToBoolean(reader.GetString(9));
                        }
                        if (!reader.IsDBNull(10)) {
                            info.DenominationID = reader.GetInt32(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            info.StaffCount = reader.GetInt32(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            info.CommentsID = reader.GetInt32(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            info.DateOfDelivery = reader.GetDateTime(13);
                        }
                        if (!reader.IsDBNull(14)) {
                            info.ReadyForWebPublishing = Convert.ToBoolean(reader.GetString(14));
                        }
                        if (!reader.IsDBNull(15)) {
                            info.OriginID = reader.GetInt32(15);
                        }
                        if (!reader.IsDBNull(16)) {
                            info.RegistratorID = reader.GetInt32(16);
                        }
                        if (!reader.IsDBNull(17)) {
                            info.Modified = reader.GetDateTime(17);
                        }

                        if (!reader.IsDBNull(18)) {
                            info.DenominationNative = reader.GetString(18);
                        }
                        if (!reader.IsDBNull(19)) {
                            info.DenominationEN = reader.GetString(19);
                        }
                        if (!reader.IsDBNull(20)) {
                            info.CommentsNative = reader.GetString(20);
                        }
                        if (!reader.IsDBNull(21)) {
                            info.CommentsEN = reader.GetString(21);
                        }

                        info.MostRecentAccount = GetMostRecentAccount(info.CompanyCIID);

                        return true;
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    return false;
                }
            }
            return false;
        }

        public int GetLastYearAFS_ID(int companyCIID, int year, int months) {
            FunctionName = "GetLastYearAFS_ID(int companyCIID, int year, int months) ";
            using (var myOleDbConn = OpenConnection()) {
                try {
                    string sql = "SELECT FSI_TemplateGeneralInfo.AFS_id " +
                                 "FROM FSI_TemplateGeneralInfo WHERE " +
                                 "(FSI_TemplateGeneralInfo.CreditInfoID = " + companyCIID +
                                 ") AND (FSI_TemplateGeneralInfo.Financial_year = " + (year - 1) + ") " +
                                 "AND (FSI_TemplateGeneralInfo.Account_period_length = " + months + ")";

                    //	string sql = "SELECT AFS_id,CreditInfoID,Financial_year,Financial_year_end,Account_period_length, " +
                    //	"Currency,Consolidated,Qualified_audit,Reviewed,Denomination_id,Employee_count,Comments_id,Date_of_delivery,Ready_for_web_publishing,Origin_id,Registrator_id,Modified FROM FSI_GeneralInfo WHERE CreditInfoID = "+ companyCIID +" AND Financial_year = "+ year +" AND Account_period_length = "+ accountPeriod +"";

                    var myOleDbCommand = new OleDbCommand(sql, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return reader.GetInt32(0);
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    return -1;
                }
            }
            return -1;
        }

        public int GetTemplateID(int afs_id) {
            FunctionName = "GetTemplateID(int afs_id) ";
            using (var myOleDbConn = OpenConnection()) {
                try {
                    string sql = "SELECT TemplateID FROM FSI_TemplateGeneralInfo WHERE AFS_id = " + afs_id;

                    var myOleDbCommand = new OleDbCommand(sql, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return reader.GetInt32(0);
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    return -1;
                }
            }
            return -1;
        }

        public ArrayList GetTemplateValues(int ciid, int templateID, string afs_ids) {
            var al = new ArrayList();
            var fields = new FieldCollectionBLLC(templateID);
            var ds = GetAccountAsDataset(ciid, afs_ids);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows != null) {
                Hashtable h;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    h = new Hashtable();
                    for (int j = 0; j < fields.Count; j++) {
                        FieldBLLC field = fields.GetField(j);
                        if (ds.Tables[0].Rows[i][field.FieldName] != null) {
                            h.Add(field.FieldName, ds.Tables[0].Rows[i][field.FieldName]);
                        }
                    }
                    al.Add(h);
                }
            }
            return al;
        }

        public DataSet GetAccountAsDataset(int companyCIID, string afs_ids) {
            var dsSheet = new DataSet();
            const string funcName = "GetProfitLossAccountAsDataset(int companyCIID) ";

            string query =
                "select * from FSI_TemplateStatements bs left join FSI_TemplateGeneralInfo gi on gi.AFS_ID = bs.AFSID " +
                "where gi.creditinfoid = " + companyCIID + " and gi.AFS_ID in (" + afs_ids +
                ") order by financial_year desc,financial_year_end desc";
            try {
                dsSheet = ExecuteSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(funcName, err, true);
            }
            return dsSheet;
        }

        public string GetMostRecentAccount(int companyCIID) {
            FunctionName = "GetMostRecentAccount(companyCIID)";
            using (var myOleDbConn = OpenConnection()) {
                try {
                    var myOleDbCommand = new OleDbCommand(
                        "SELECT Financial_year FROM FSI_GeneralInfo " +
                        "WHERE CreditInfoID = " + companyCIID + " ORDER BY Financial_year DESC",
                        myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return reader.GetString(0);
                    }
                    return "N/A";
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    throw (new ApplicationException(err.Message));
                }
            }
        }

        protected bool UpdateFinancialStatement(GeneralInfoBLLC info) {
            FunctionName = "UpdateFinancialStatement(GeneralInfoBLLC info)";

            using (var myOleDbConn = OpenConnection()) {
                var myTrans = myOleDbConn.BeginTransaction();
                var myOleDbCommand =
                    new OleDbCommand(
                        "UPDATE FSI_TemplateGeneralInfo SET CreditInfoID = ?,Financial_year = ?,Financial_year_end = ?,Account_period_length = ?,Currency = ?,Consolidated = ?, Qualified_audit = ?, Reviewed = ?, " +
                        "Denomination_id = ?,Employee_count = ?,Comments_id = ?,Date_of_delivery = ?,Ready_for_web_publishing = ?,Origin_id = ?,Registrator_id = ?,Modified = ?, Smallstatus = ? , ModifiedBy = ? " +
                        "WHERE AFS_id = ?",
                        myOleDbConn) {Transaction = myTrans};

                try {
                    //	myOleDbCommand.CommandText = "UPDATE FSI_GeneralInfo SET CreditInfoID = ?,Financial_year = ?,Financial_year_end = ?,Account_period_length = ?,Currency = ?,Consolidated = ?, Qualified_audit = ?, Reviewed = ?, " +
                    //		"Denomination_id = ?,Employee_count = ?,Comments_id = ?,Date_of_delivery = ?,Ready_for_web_publishing = ?,Origin_id = ?,Registrator_id = ?,Modified = ? " + 
                    //		"WHERE AFS_id = ?";
                    //	myOleDbCommand.Transaction = myTrans;
                    var myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                  {
                                      IsNullable = true,
                                      Direction = ParameterDirection.Input,
                                      Value = info.CompanyCIID
                                  };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.FiscalYear
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year_end", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.YearEnded
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Account_period_length", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.AccountMonths
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Currency", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.CurrencyID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Consolidated", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.Consolidated.ToString()
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Qualified_audit", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.Qualified.ToString()
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Reviewed", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.Reviewed.ToString()
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Denomination_id", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.DenominationID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Employee_count", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.StaffCount
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Comments_id", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.CommentsID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Date_of_delivery", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.DateOfDelivery
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Ready_for_web_publishing", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.ReadyForWebPublishing.ToString()
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Origin_id", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.OriginID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Registrator_id", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.RegistratorID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Modified", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.Modified
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Smallstatus", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.SmallStatus.ToString()
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("ModifiedBy", OleDbType.Integer)
                    {
                        IsNullable = true,
                        Direction = ParameterDirection.Input,
                        Value = info.UserID
                    };
                    myOleDbCommand.Parameters.Add(myParam);

                    //
                    // where parameterinn ...
                    myParam = new OleDbParameter("AFS_id", OleDbType.Integer)
                              {
                                  Direction = ParameterDirection.Input,
                                  Value = info.AFS_id
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    //	Update template fields
                    // Setja inn template fields
                    if (AddTemplateFields(info, myOleDbCommand)) {
                        // framkv�ma ...
                        myTrans.Commit();
                        return true;
                    }
                    myTrans.Rollback();
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : Failed addint template fields ", true);
                    return false;
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    myTrans.Rollback();
                    return false;
                }
            }
        }

        protected bool InsertFinancialStatement(GeneralInfoBLLC info) {
            FunctionName = "AddFinancialStatement(GeneralInfoBLLC info) ";

            using (var myOleDbConn = OpenConnection()) {
                var myTrans = myOleDbConn.BeginTransaction();
                // the FSI_GeneralInfo table
                var myOleDbCommand =
                    new OleDbCommand(
                        "INSERT FSI_TemplateGeneralInfo(TemplateID, CreditInfoID,Financial_year,Financial_year_end,Account_period_length, " +
                        "Currency,Consolidated,Qualified_audit,Reviewed,Denomination_id,Employee_count,Comments_id,Date_of_delivery,Ready_for_web_publishing,Origin_id,Registrator_id,Modified, CreatedBy) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        myOleDbConn) {Transaction = myTrans};
                //	myOleDbCommand.Connection = myOleDbConn;
                //	myOleDbConn.Open();
                try {
                    var myParam = new OleDbParameter("TemplateID", OleDbType.Integer)
                                  {
                                      IsNullable = false,
                                      Direction = ParameterDirection.Input,
                                      Value = info.TemplateID
                                  };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                              {
                                  IsNullable = false,
                                  Direction = ParameterDirection.Input,
                                  Value = info.CompanyCIID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.FiscalYear
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Financial_year_end", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.YearEnded
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Account_period_length", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.AccountMonths
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Currency", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.CurrencyID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Consolidated", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.Consolidated.ToString()
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Qualified_audit", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.Qualified.ToString()
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Reviewed", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.Reviewed
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Denomination_id", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.DenominationID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Employee_count", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input
                              };
                    if (info.StaffCount != int.MinValue) {
                        myParam.Value = info.StaffCount;
                    }
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Comments_id", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.CommentsID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Date_of_delivery", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.DateOfDelivery
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Ready_for_web_publishing", OleDbType.VarChar)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.ReadyForWebPublishing
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Origin_id", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.OriginID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Registrator_id", OleDbType.Integer)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.RegistratorID
                              };
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Modified", OleDbType.Date)
                              {
                                  IsNullable = true,
                                  Direction = ParameterDirection.Input,
                                  Value = info.Modified
                              };
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CreatedBy", OleDbType.Integer)
                    {
                        IsNullable = true,
                        Direction = ParameterDirection.Input,
                        Value = info.UserID
                    };
                    myOleDbCommand.Parameters.Add(myParam);


                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    // n� � s��ast innsetta ID
                    myOleDbCommand.Parameters.Clear();
                    myOleDbCommand.CommandText = "SELECT @@IDENTITY FROM FSI_TemplateGeneralInfo";
                    info.AFS_id = Convert.ToInt32(myOleDbCommand.ExecuteScalar());

                    // Setja inn template fields
                    if (AddTemplateFields(info, myOleDbCommand)) {
                        // framkv�ma ...
                        myTrans.Commit();
                        return true;
                    }
                    myTrans.Rollback();
                    return false;
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    myTrans.Rollback();
                    return false;
                }
            }
        }

        protected bool AddTemplateFields(GeneralInfoBLLC info, OleDbCommand command) {
            return IsTemplateFieldsRegistered(info.AFS_id)
                       ? UpdateTemplateFields(info, command)
                       : InsertTemplateFields(info, command);
        }

        protected bool InsertTemplateFields(GeneralInfoBLLC info, OleDbCommand command) {
            FunctionName = "InsertTemplateFields(GeneralInfoBLLC info, OleDbCommand command) ";

            command.Parameters.Clear();

            var builder = new StringBuilder();
            builder.Append("INSERT INTO FSI_TEMPLATESTATEMENTS(AFSID, ");

            var valueBuilder = new StringBuilder();
            valueBuilder.Append(" VALUES (");
            valueBuilder.Append(info.AFS_id);
            valueBuilder.Append(", ");

            FieldCollectionBLLC coll = info.TemplateFields;

            for (int i = 0; i < coll.Count; i++) {
                builder.Append(coll.GetField(i).FieldName);
                if (coll.GetField(i).Value != null && coll.GetField(i).Value.ToString().Trim() != "") {
                    valueBuilder.Append(coll.GetField(i).Value.ToString());
                } else {
                    valueBuilder.Append("0");
                }
                if (i < coll.Count - 1) {
                    builder.Append(", ");
                    valueBuilder.Append(", ");
                }
            }
            builder.Append(")");
            valueBuilder.Append(")");
            builder.Append(valueBuilder.ToString());

            command.CommandText = builder.ToString();
            new AuditFactory(command).PerformAuditProcess(); command.ExecuteNonQuery();
            return true;
        }

        protected bool UpdateTemplateFields(GeneralInfoBLLC info, OleDbCommand command) {
            FunctionName = "UpdateTemplateFields(GeneralInfoBLLC info, OleDbCommand command) ";

            command.Parameters.Clear();

            var builder = new StringBuilder();
            builder.Append("UPDATE FSI_TEMPLATESTATEMENTS SET ");

            FieldCollectionBLLC coll = info.TemplateFields;

            for (int i = 0; i < coll.Count; i++) {
                builder.Append(coll.GetField(i).FieldName);
                builder.Append(" = ");
                if (coll.GetField(i).Value != null && coll.GetField(i).Value.ToString().Trim() != "") {
                    builder.Append(coll.GetField(i).Value.ToString());
                } else {
                    builder.Append("0");
                }
                //	builder.Append(coll.GetField(i).Value);
                if (i < coll.Count - 1) {
                    builder.Append(", ");
                }
            }
            builder.Append("WHERE AFSID = ");
            builder.Append(info.AFS_id);

            command.CommandText = builder.ToString();
            new AuditFactory(command).PerformAuditProcess(); command.ExecuteNonQuery();
            return true;
        }

        public bool IsTemplateFieldsRegistered(int afs_id) {
            FunctionName = "IsTemplateFields(int afs_id)";
            using (var myOleDbConn = OpenConnection()) {
                try {
                    var myOleDbCommand =
                        new OleDbCommand("SELECT * FROM FSI_TemplateStatements WHERE AFSID=" + afs_id, myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return true;
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    return false;
                }
            }
            return false;
        }

        public bool IsStatementRegisted(int companyCIID, int year, int accountPeriod) {
            FunctionName = "IsStatementRegisted(int companyCIID, int year)";
            using (var myOleDbConn = OpenConnection()) {
                try {
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM FSI_TemplateGeneralInfo WHERE CreditInfoID = " + companyCIID +
                            " AND Financial_year = " + year + " AND Deleted = 'False' AND Account_period_length = " +
                            accountPeriod + "",
                            myOleDbConn);
                    var reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return true;
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(ClassName + " : " + FunctionName + " : ", err, true);
                    return false;
                }
            }
            return false;
        }
    }
}