<%@ Page language="c#" Codebehind="TemplateReport.aspx.cs" AutoEventWireup="false" Inherits="CR.TemplateReport" responseEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TemplateReport</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<style>.header { FONT-SIZE: 10px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.header2 { FONT-SIZE: 12px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.header3 { FONT-SIZE: 12px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.headerb { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.headerwhite { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.headerblack { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.conbig { FONT-WEIGHT: bolder; FONT-SIZE: 15px; COLOR: #891618; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.conbig2 { FONT-WEIGHT: bolder; FONT-SIZE: 14px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.con { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	P.break { PAGE-BREAK-AFTER: always }
	break { PAGE-BREAK-AFTER: always }
		</style>
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body leftMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<asp:Xml id="xmlxslTransform" runat="server"></asp:Xml>
		</form>
	</body>
</HTML>
