<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Page language="c#" Codebehind="Report.aspx.cs" AutoEventWireup="false" Inherits="CR.Report" responseEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Creditinfo Group Ltd.</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="expires" content="-1">
		<meta http-equiv="pragma" content="no-cache">
		<meta name="description" content="Lánstraust">
		<meta name="author" content="Lánstraust Brautarholti 10-14. S: 550-9600">
		<style>.header { FONT-SIZE: 10px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.header2 { FONT-SIZE: 12px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.header3 { FONT-SIZE: 12px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.headerb { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.headerwhite { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.headerblack { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.conbig { FONT-WEIGHT: bolder; FONT-SIZE: 15px; COLOR: #891618; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.conbig2 { FONT-WEIGHT: bolder; FONT-SIZE: 14px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.con { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	P.break { PAGE-BREAK-AFTER: always }
	break { PAGE-BREAK-AFTER: always }
		</style>
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>
		function PrintReport(){
			if(window.print){
				window.print();
			}
		}
		</script>
	</head>
	<body leftmargin="0" topmargin="0">
		<form id="Form1" method="post" runat="server">
			<asp:xml id="xmlxslTransform" runat="server"></asp:xml>
			<uc1:language id="Language1" runat="server" visible="False"></uc1:language>
			<table width="608">
				<tr>
					<td id="trLine" runat="server" style="HEIGHT: 15px" align="center" bgcolor="#951e16" colspan="5"></td>
				</tr>
				<tr class="dark-row">
					<td style="HEIGHT: 10px" align="center" colspan="8"></td>
				</tr>
				<tr>
					<td style="HEIGHT: 10px" align="center"></td>
					<td style="WIDTH: 144px"><div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnSendReport" runat="server" cssclass="RegisterButton" text="Send report"></asp:button></div>
					</td>
					<td><div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnPrint" runat="server" causesvalidation="False" cssclass="RegisterButton"
								text="Print"></asp:button></div>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="8"></td>
				</tr>
			</table>
		</form>
	</body>
</html>
