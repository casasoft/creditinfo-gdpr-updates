#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Xsl;
using CR.BLL;
using CR.Localization;

#endregion

namespace CR {
    /// <summary>
    /// Summary description for TemplateReport.
    /// </summary>
    public class TemplateReport : Page {
        protected Xml xmlxslTransform;

        private void Page_Load(object sender, EventArgs e) { displayReport(); // Put user code to initialize the page here
        }

        private void displayReport() {
            // check the current culture
            var rm = CIResource.CurrentManager;

            var cultureToUse = "Native";

            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }

            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            var generator = new TemplateReportGenerator(
                rm, cultureToUse, userCreditInfoID, userID, ipAddress);
            string xslFile = null;
            string xslSendFile = null;
            var doc = generator.GetReport("BasicReport", 4, "2,3", -1, ref xslFile, ref xslSendFile);
            var transformer = new XslTransform();
            doc.Save("c:\\cig\\TemplateReport.xml");
            transformer.Load(Server.MapPath(Request.ApplicationPath + xslFile)); //"/CR/xsl/general/CIGReport.xslt")); 
            xmlxslTransform.Document = doc;
            xmlxslTransform.Transform = transformer;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}