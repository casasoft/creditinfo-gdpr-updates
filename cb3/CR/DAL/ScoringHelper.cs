﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using cb3.BLL.Scoring;

namespace cb3.CR.DAL
{
    public sealed class ScoringHelper
    {

        static object lockThis = new object();
        ScoringDAL dal;
        List<CompanyAge> companyAges;
        List<AccountsAge> accountAges;
        List<cb3.BLL.Scoring.FSI> fsiList;
        List<cb3.BLL.Scoring.RiskBandsRange> riskBandList;
        List<cb3.BLL.Scoring.QuickRatio> quickRatio;

        public ScoringHelper()
        {
            dal = new ScoringDAL();
            LoadData();
        }


        private void LoadData()
        {

            companyAges = dal.GetScorCompanyAge();
            accountAges = dal.GetScorAccountsAge();
            fsiList = dal.GetScorFSI();
            quickRatio = dal.GetScorQuickRatio();
            riskBandList = dal.GetScorRiskBandsRanges();
        }

        public decimal? GetExclusionInterval(double CompanyAge)
        {
            if (companyAges.Count == 0)
                throw new ArgumentNullException("GetExclusionInterval");

            var result = companyAges.Where(x => CompanyAge >= x.MinTest && x.MaxTest != null && CompanyAge <= x.MaxTest ||
                CompanyAge >= x.MinTest && x.MaxTest == null);
            if (result.Count() != 0)
                return result.First().Value;
            return null;
        }

        public decimal? GetAccauntAgeExclusiveInterval(int AccountAge)
        {
            if (accountAges.Count == 0)
                throw new ArgumentNullException("GetAccauntAgeExclusiveInterval");

            var result = accountAges.Where(x => AccountAge >= x.MinTest && x.MaxTest != null && AccountAge <= x.MaxTest ||
                AccountAge >= x.MinTest && x.MaxTest == null);
            if (result.Count() != 0)
                return result.First().Value;
            return null;
        }

        public cb3.BLL.Scoring.RiskBandsRange GetRiskBandRange(decimal ratio)
        {
            if (riskBandList.Count == 0)
                throw new ArgumentNullException("GetRiskBandRange");

            var result = riskBandList.Where(x => ratio >= x.MinTest && x.MaxTest != null && ratio < x.MaxTest ||
                ratio >= x.MinTest && x.MaxTest == null);
            if (result.Count() != 0)
                return result.First();
            return null;
        }

        public cb3.BLL.Scoring.FSI GetFSIValue(decimal NetWorth)
        {
            if (NetWorth == decimal.MaxValue)
                return null;

            if (fsiList.Count == 0)
                throw new ArgumentNullException("GetFSIValue");

            var result = fsiList.Where(x => NetWorth >= x.MinTest && x.MaxTest != null && NetWorth < x.MaxTest ||
                NetWorth >= x.MinTest && x.MaxTest == null);
            if (result.Count() != 0)
                return result.First();
            return null;
        }

        internal QuickRatio GetScorQuickRatio()
        {
            if (quickRatio.Count == 0)
                return null;
            return quickRatio[0];
        }
    }
}
