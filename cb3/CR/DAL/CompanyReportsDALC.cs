#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CPI.BLL;
using DataProtection;
using Logging.BLL;
using cb3;

#endregion

using Cig.Framework.Base.Configuration;

#pragma warning disable 618,612
namespace CR.DAL {
    /// <summary>
    /// This class provides the database funtions for Company Reports
    /// </summary>
    public class CompanyReportsDALC {
        /// <summary>
        /// For logging purpose
        /// </summary>
        protected static string className = "CompanyReportsDALC";

        public DataSet GetShareholderInvolvements(int ownerCIID) {
            var dsInv = new DataSet();
            const string funcName = "GetShareholderInvolvements(string ownerCIID) ";

            string query =
                "select o.CompanyCIID, c.NameNative, c.NameEn from cpi_Owners o, np_Companys c where c.CreditInfoID = o.CompanyCIID and o.ownerCIID = " +
                ownerCIID + " ORDER BY NameNative, NameEN";
            try {
                dsInv = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsInv;
        }

        public DataSet GetPrincipalsInvolvements(int principalCIID) {
            var dsInv = new DataSet();
            const string funcName = "GetPrincipalsInvolvements(int principalCIID) ";

            string query =
                "SELECT p.CompanyCIID, c.NameNative, c.NameEN, j.NameNative AS JobTitleNative, j.NameEN AS JobTitleEN FROM cpi_CompanyPrincipals p LEFT JOIN np_Companys c ON p.CompanyCIID = c.CreditInfoID RIGHT JOIN cpi_JobTitle j on p.JobTitleID = j.JobTitleID WHERE p.PrincipalCIID = " +
                principalCIID + " ORDER BY c.NameNative, c.NameEN";
            try {
                dsInv = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsInv;
        }

        public DataSet GetBoardMemberInvolvements(int boardMemberCIID) {
            var dsInv = new DataSet();
            const string funcName = "GetBoardMemberInvolvements(int boardMemberCIID) ";

            string query =
                "SELECT b.CompanyCIID, c.NameNative, c.NameEN, m.TitleNative, m.TitleEN FROM cpi_BoardMembers b LEFT JOIN np_Companys c ON b.CompanyCIID = c.CreditInfoID RIGHT JOIN cpi_ManagementPositions m on b.ManagementPositionID = m.ManagementPositionID WHERE b.BoardMemberCIID = " +
                boardMemberCIID + " ORDER BY NameNative, NameEN";
            try {
                dsInv = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsInv;
        }

        public DataSet GetMTCompanyInvolvements(string nationalID, bool isCompany) {
            var dsMTCompanyInvolvements = new DataSet();
            const string funcName = "GetCompanyInvolvements(string nationalID) ";

            string query;
            if (isCompany) {
                query = "select * from fo_mt_CompanyInvolvements where IP_Co_Reg_No = '" + nationalID +
                        "' order by Ordering, Company_Name";
            } else {
                query = "select * from fo_mt_CompanyInvolvements where IP_ID_No = '" + nationalID +
                        "' order by Ordering, Company_Name";
            }
            try {
                dsMTCompanyInvolvements = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsMTCompanyInvolvements;
        }

        public DataSet GetMTDirectorsAndMore(string nationalID) {
            var dsMTDirectorsAndMore = new DataSet();
            const string funcName = "GetCompanyInvolvements(string nationalID) ";

            //string query = "select * from fo_mt_DirectorsAndMore where RegistrationID = '" + nationalID +
            //               "' order by Ordering, IP_Surname, IP_Co_Name";
            string query = string.Format(@"select * from fo_mt_DirectorsAndMore left outer join (
                            select (cast(sum(a.NumberOfShares)as decimal) / x.K) * 100 as TotalEquity, a.class, a.companyid, 
                            a.involvedpartyid from lmt_companycapital_issued a, (
                            select K = sum(NumberOfShares), companyid from dbo.lmt_companycapital_issued 
                            where companyid = '{0}' group by companyid) x where a.companyid = x.companyid
                            group by a.involvedpartyid, class,  a.companyid, x.K) xx on id = xx.involvedpartyid AND Involvement = 'Shareholder'
                            where RegistrationID = '{0}' ORDER BY Ordering, IP_Surname, IP_Co_Name", nationalID);
            try {
                dsMTDirectorsAndMore = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsMTDirectorsAndMore;
        }

        public string GetRegistrationFormAsString(int formID, bool native) {
            DataSet ds;
            const string funcName = "GetRegistrationFormAsString(int formID, bool native) ";

            string query = "select NameNative, NameEN from cpi_RegistrationForm where FormID=" + formID;
            try {
                ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        return native ? ds.Tables[0].Rows[0]["NameNative"].ToString() : ds.Tables[0].Rows[0]["NameEN"].ToString();
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return "";
        }

        public bool IsIDInDebitorsDatabase(int creditInfoID) {
            DataSet dsDD;
            const string funcName = "IsIDInDebitorsDatabase(int creditInfoID) ";
            int registrationStatus = 4;
            // hmm we need to check if the claims have registration status
            if (CigConfig.Configure("lookupsettings.RegisteredClaimID") != null) {
                registrationStatus = int.Parse(CigConfig.Configure("lookupsettings.RegisteredClaimID"));
            }

            string query = "select * from np_Claims where CreditInfoID = " + creditInfoID + " AND StatusID = " +
                           registrationStatus;
            try {
                dsDD = executeSelectStatement(query);
                if (dsDD.Tables.Count > 0) {
                    if (dsDD.Tables[0].Rows.Count > 0) {
                        return true;
                    }
                }
                return false;
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }
        }

        public DataSet GetCourtDecisionsAndDefaultingDebts(int creditInfoID) {
            var dsCDADD = new DataSet();
            const string funcName = "GetCourtDecisionsAndDefaultingDebts(int creditInfoID) ";
            int registrationStatus = 4;
            // hmm we need to check if the claims have registration status
            if (CigConfig.Configure("lookupsettings.RegisteredClaimID") != null) {
                registrationStatus = int.Parse(CigConfig.Configure("lookupsettings.RegisteredClaimID"));
            }

            string query = "select * from np_Claims where CreditInfoID = " + creditInfoID + " AND StatusID = " +
                           registrationStatus;
            try {
                dsCDADD = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return dsCDADD;
            }
            return dsCDADD;
        }

        public DataSet GetDirectorsAsDataset(int companyCIID) {
            var dsDirectors = new DataSet();
            const string funcName = "GetDirectorsAsDataset(int companyCIID) ";

            string query =
                "SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number,cpi_JobTitle.NameNative, " +
                "cpi_JobTitle.NameEN,cpi_JobTitle.JobTitleID,np_Address.StreetNative, np_Address.StreetEN, " +
                "np_Address.PostalCode, np_City.NameNative AS CityNameNative, np_City.NameEN AS CityNameEN, " +
                "cpi_Countries.NameNative As CountryNameNative, cpi_Countries.NameEN AS CountryNameEN, " +
                "np_Education.NameNative AS EdNameNative, " +
                "np_Education.NameEN AS EdNameEN,np_Education.EducationID " +
                "FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID " +
                "LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID " +
                "LEFT OUTER JOIN cpi_Countries ON np_Address.CountryID = cpi_Countries.CountryID " +
                "LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID " +
                "LEFT OUTER JOIN cpi_CompanyPrincipals ON np_Individual.CreditInfoID = cpi_CompanyPrincipals.PrincipalCIID " +
                "LEFT OUTER JOIN cpi_JobTitle ON cpi_CompanyPrincipals.JobTitleID = cpi_JobTitle.JobTitleID " +
                "LEFT OUTER JOIN np_Education ON cpi_CompanyPrincipals.EducationID = np_Education.EducationID " +
                "WHERE np_Individual.CreditInfoID IN (SELECT cpi_CompanyPrincipals.PrincipalCIID FROM cpi_CompanyPrincipals  " +
                "WHERE cpi_CompanyPrincipals.CompanyCIID = " + companyCIID + " ) " +
                "AND cpi_CompanyPrincipals.CompanyCIID = " + companyCIID;
            //"AND np_IDNumbers.NumberTypeID = 1 ";		
            try {
                dsDirectors = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsDirectors;
        }

        /*public string GetHistory(int companyCIID, bool native, string historyType)
		{
			DataSet ds = new DataSet();
			string funcName = "GetHistory(int companyCIID, bool native, string historyType)";
			
			string directorsHistoryType = "1";
			string query = "select HistoryNative, HistoryEN FROM cpi_CompanyHistory WHERE HistoryTypeID = '"+historyType+"' AND CompanyCIID = "+companyCIID;
			try
			{
				ds = executeSelectStatement(query);
				if(ds.Tables.Count>0)
				{
					if(ds.Tables[0] != null&&ds.Tables[0].Rows.Count>0)
					{
						if(native)
							return ds.Tables[0].Rows[0]["HistoryNative"].ToString();
						else
							return ds.Tables[0].Rows[0]["HistoryEN"].ToString();
					}
				}
			}
			catch (Exception err)
			{
				Logger.WriteToLog(className +" : " + funcName, err,true);
			}
			return "";
		}*/

        public int InsertInto_np_Usage(int creditInfoID, int queryType, string query, int resultCount, string ip, int userID) 
        {
            string sql = @"INSERT INTO np_Usage(Creditinfoid,query_type,query,result_count,ip,UserID) VALUES({0}, {1}, '{2}', {3}, '{4}', {5})
                        SELECT @@IDENTITY
                        ";

            sql = string.Format(sql, creditInfoID.ToString(), queryType.ToString(), query, resultCount.ToString(), ip, userID.ToString());
            int identity = -1;
            var myOleConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            try
            {
                myOleConn.Open();
                var command = new OleDbCommand(sql, myOleConn);
                int.TryParse(command.ExecuteScalar().ToString(), out identity);
            }
            catch (Exception ex)
            {
                Logger.WriteToLog(className + " : " + "InsertInto_np_Usage", ex, true);
            }
            finally {
                myOleConn.Close();
            }
            return identity;
        }

        public DataSet GetCyprusCompanyFormerNames(string companyNationalID) {
            var mySet = new DataSet();
            

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                string query =
                    "Select * FROM np_CompaniesHouse where Org_name_status_code = 'APR' AND RegistrationNumber = '" +
                    companyNationalID + "' ORDER BY Org_name";

                var myAdapter = new OleDbDataAdapter();
                myOleDbConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                myAdapter.Fill(mySet);
                return mySet;
            }
        }

        public DataSet GetExportToAsDataSet(int companyCIID) {
            var dsExportTo = new DataSet();
            const string funcName = "GetExportToAsDataSet(int companyCIID) ";

            string query =
                "select ec.CountryID, c.NameEN, c.NameNative from cpi_ExportCountries ec, cpi_Countries c where c.CountryID = ec.CountryID and ec.CompanyCIID = " +
                companyCIID;
            try {
                dsExportTo = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsExportTo;
        }

        public DataSet GetImportFromAsDataSet(int companyCIID) {
            var dsImportFrom = new DataSet();
            const string funcName = "GetImportFromAsDataSet(int companyCIID) ";

            string query =
                "select ic.CountryID, c.NameEN, c.NameNative from cpi_ImportCountries ic, cpi_Countries c where c.CountryID = ic.CountryID and ic.CompanyCIID = " +
                companyCIID;
            try {
                dsImportFrom = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsImportFrom;
        }

        public DataSet GetNACECodesAsDataSet(int companyCIID) {
            var dsNACE = new DataSet();
            const string funcName = "GetNACECodesAsDataSet(int companyCIID) ";

            string query =
                "select cnc.NaceCodeId, nc.DescriptionNative, nc.DescriptionEN, cnc.CurrentCode,cnc.OrderLevel from cpi_companiesNaceCodes cnc " +
                "left join cpi_NaceCodes nc on cnc.NaceCodeID = nc.NaceCodeID  where cnc.CompanyCIID = " + companyCIID +
                " order by cnc.OrderLevel";
            try {
                dsNACE = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsNACE;
        }

        public DataSet GetTradeTermsAsDataSet(int companyCIID) {
            var dsTT = new DataSet();
            const string funcName = "GetTradeTermsAsDataSet(int companyCIID) ";

            string query =
                "select ct.SalesTerm, ct.CommercialTrade, t.TermsDescriptionNative, t.TermsDescriptionEN, t.SalesTerm, t.PaymentTerm " +
                "from cpi_CompanyTradeTerms ct left join cpi_TradeTerms t on ct.TermsID = t.TermsID " +
                "where ct.CompanyCIID = " + companyCIID;
            try {
                dsTT = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsTT;
        }

        public DataSet GetBoardAsDataset(int companyCIID) {
            var dsBoard = new DataSet();
            const string funcName = "GetBoardAsDataset(int companyCIID) ";

            string query = "select " +
                           "bm.BoardMemberCIID, bm.companyciid, " +
                           "mp.titlenative TitleNative, mp.titleen TitleEN, " +
                           "COALESCE(c.NameNative, i.FirstNameNative) AS FirstNameNative," +
                           "COALESCE(c.NameEN, i.FirstNameEN) AS FirstNameEN, " +
                           "i.surnamenative, i.surnameen, " +
                           "id.number,idc.number as numberC, e.nameEn EducationEN, e.nameNative EducationNative, " +
                           "np_Address.StreetNative, np_Address.StreetEN, " +
                           "np_Address.PostalCode, np_City.NameNative AS CityNameNative, np_City.NameEN AS CityNameEN, " +
                           "cpi_Countries.NameNative As CountryNameNative, cpi_Countries.NameEN AS CountryNameEN " +
                           "from cpi_boardmembers bm left join cpi_managementpositions mp on bm.managementpositionid = mp.managementpositionid " +
                           "left join np_individual i on bm.boardmemberciid = i.creditinfoid " +
                           "left join np_Companys c on bm.boardmemberciid = c.creditinfoid " +
                           "LEFT OUTER JOIN np_Address ON i.CreditInfoID = np_Address.CreditInfoID " +
                           "LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID " +
                           "LEFT OUTER JOIN cpi_Countries ON np_Address.CountryID = cpi_Countries.CountryID " +
                           "left join np_idnumbers id on i.creditinfoid = id.creditinfoid " +
                           "left join np_idnumbers idc on c.creditinfoid = idc.creditinfoid " +
                           "left join np_education e on i.educationid = e.educationid where companyciid = " +
                           companyCIID;
            /*
			string query = "select "+
				"bm.BoardMemberCIID, bm.companyciid, "+
				"mp.titlenative TitleNative, mp.titleen TitleEN, "+
				"i.firstnamenative, i.firstnameen, "+
				"i.surnamenative, i.surnameen, "+
				"id.number, e.nameEn EducationEN, e.nameNative EducationNative, "+
				"np_Address.StreetNative, np_Address.StreetEN, "+
				"np_Address.PostalCode, np_City.NameNative AS CityNameNative, np_City.NameEN AS CityNameEN, "+
				"cpi_Countries.NameNative As CountryNameNative, cpi_Countries.NameEN AS CountryNameEN "+
                "from cpi_boardmembers bm left join cpi_managementpositions mp on bm.managementpositionid = mp.managementpositionid "+
				"left join np_individual i on bm.boardmemberciid = i.creditinfoid "+
				"left join np_Companys c on bm.boardmemberciid = c.creditinfoid "+
				"LEFT OUTER JOIN np_Address ON i.CreditInfoID = np_Address.CreditInfoID "+
				"LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID "+
				"LEFT OUTER JOIN cpi_Countries ON np_Address.CountryID = cpi_Countries.CountryID "+
				"left join np_idnumbers id on i.creditinfoid = id.creditinfoid "+
				"left join np_education e on i.educationid = e.educationid where companyciid = "+companyCIID;
				*/
            try {
                dsBoard = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsBoard;
        }

        public DataSet GetKeyEmployeesAsDataset(int companyCIID) {
            var dsKeyEmp = new DataSet();
            const string funcName = "GetKeyEmployeesAsDataset(int companyCIID) ";

            string query = "select p.principalciid, p.companyciid, j.NameNative JobTitleNative, j.NameEN JobTitleEN, " +
                           "i.firstnamenative, i.firstnameen, i.surnamenative, i.surnameen, id.number, e.nameEn EducationEn, " +
                           "e.nameNative EducationNative from cpi_companyprincipals p left join cpi_jobtitle j on " +
                           "p.JobTitleID = j.JobTitleID right join np_individual i on p.PrincipalCIID = i.creditinfoid " +
                           "left join np_idnumbers id on i.creditinfoid = id.creditinfoid left join np_education e on " +
                           "i.educationid = e.educationid where p.companyciid = " + companyCIID;

            try {
                dsKeyEmp = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsKeyEmp;
        }

        public DataSet GetBalanceSheetAsDataset(int companyCIID, string afs_ids) {
            var dsSheet = new DataSet();
            const string funcName = "GetBalanceSheetAsDataset(int companyCIID) ";

            string query =
                "select * from FSI_TemplateStatements bs left join FSI_TemplateGeneralInfo gi on gi.AFS_ID = bs.AFSID " +
                "where gi.creditinfoid = " + companyCIID + " and gi.AFS_ID in (" + afs_ids +
                ") order by financial_year desc,financial_year_end desc";
            try {
                dsSheet = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsSheet;
        }

        public DataSet GetProfitLossAccountAsDataset(int companyCIID, string afs_ids) {
            var dsSheet = new DataSet();
            const string funcName = "GetProfitLossAccountAsDataset(int companyCIID) ";

            string query =
                "select * from FSI_TemplateStatements bs left join FSI_TemplateGeneralInfo gi on gi.AFS_ID = bs.AFSID " +
                "where gi.creditinfoid = " + companyCIID + " and gi.AFS_ID in (" + afs_ids +
                ") order by financial_year desc, Financial_year_end desc";
            try {
                dsSheet = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsSheet;
        }

        public DataSet GetInvestmentsAsDataset(int companyCIID) {
            var dsInvestments = new DataSet();
            const string funcName = "GetInvestmentsAsDataset(int companyCIID) ";

            string query = "select c.CreditInfoID, c.NameNative, c.NameEN, id.number RegistrationID, s.Ownership, " +
                           "os.nameNative StatusNative, os.nativeEN StatusEN " +
                           "from cpi_subsidiaries s left join np_companys c on s.SubsidariesID = c.creditinfoid " +
                           "left join np_idnumbers id on c.creditinfoid = id.creditinfoid " +
                           "left join np_org_status os on c.org_status_code = os.id " +
                           "where s.CompanyCIID = " + companyCIID + " order by s.Ownership desc";
            try {
                dsInvestments = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsInvestments;
        }

        public DataSet GetBanksAsDataset(int companyCIID) {
            var dsBanks = new DataSet();
            const string funcName = "GetBanksAsDataset(int companyCIID) ";

            string query = "select b.NameNative, b.NameEN, b.Location, b.PostalCode, b.CommentNative, b.CommentEN " +
                           "from cpi_banks b left join cpi_companybanks cb on cb.BankID = b.BankId where cb.companyCIID = " +
                           companyCIID;
            try {
                dsBanks = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsBanks;
        }

        public DataSet GetRealEstatesAsDataset(int companyCIID) {
            var dsRealEstates = new DataSet();
            const string funcName = "GetRealEstatesAsDataset(int companyCIID) ";

            string query =
                "select re.AddressNative, re.AddressEN, re.InsuranceValue, re.MarkedValue, rel.DescriptionNative LocationNative, rel.DescriptionEN LocationEN, " +
                "reot.OwnerTypeNative TenureNative, reot.OwnerTypeEN TenureEN, PostCode, YearBuilt, Size, ret.TypeNative, ret.TypeEN, np_City.NameNative As CityNative, np_City.NameEN As CityEn " +
                "from cpi_realestates re left join cpi_realestatesownertype reot on re.OwnerTypeID = reot.OwnerTypeID " +
                "left join cpi_realestatestype ret on re.RealEstatesTypeID = ret.RealEstatesTypeID " +
                "left join np_City on re.CityID = np_City.CityID " +
                "left join cpi_realestateslocation rel on re.RealEstatesLocationID = rel.LocationID where companyCIID = " +
                companyCIID;
            try {
                dsRealEstates = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsRealEstates;
        }

        public DataSet GetCustomerTypeAsDataSet(int companyCIID) {
            var dsCustomerType = new DataSet();
            const string funcName = "GetCustomerTypeAsDataSet(int companyCIID) ";

            string query =
                "select cct.CustomerTypeID, ct.CustomerTypeEN, ct.CustomerTypeNative from cpi_CompaniesCustomerType cct, " +
                "cpi_CustomerType ct where ct.CustomerTypeID = cct.CustomerTypeID and cct.CompanyCIID = " + companyCIID;
            try {
                dsCustomerType = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCustomerType;
        }

        public DataSet GetCompanyState(int companyCIID) {
            var dsCompanyState = new DataSet();
            const string funcName = "GetCompanyState(int companyCIID) ";

            string query =
                "SELECT org.id, org.nativeEN, org.nameNative FROM np_org_status org LEFT OUTER JOIN np_Companys comp ON org.id = comp.org_status_code  WHERE comp.CreditInfoID = " +
                companyCIID;
            try {
                dsCompanyState = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCompanyState;
        }

        public string GetMTCompanyState(string nationalID) {
            const string funcName = "GetMTCompanyState(string nationalID) ";

            string query = "select lmt_mfsa_CompanyStates.State " +
                           "from lmt_mfsa_CompanyStates " +
                           "LEFT JOIN lmt_mfsa_Companies on lmt_mfsa_CompanyStates.State_ID = lmt_mfsa_Companies.State_ID " +
                           "WHERE lmt_mfsa_Companies.Registration_No = '" + nationalID + "'";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();
            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try {
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    if (!reader.IsDBNull(0)) {
                        return reader.GetString(0);
                    }
                    return null;
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            } finally {
                myOleDbConn.Close();
            }
            return null;
        }

        public string GetMTCompanyStateBO(int companyCIID) {
            const string funcName = "GetMTCompanyStateBO(int companyCIID) ";

            string query = "select lmt_mfsa_CompanyStates.State " +
                           "from lmt_mfsa_CompanyStates " +
                           "LEFT JOIN lmt_mfsa_Companies on lmt_mfsa_CompanyStates.State_ID = lmt_mfsa_Companies.State_ID " +
                           "LEFT JOIN np_IDNumbers ON np_IDNumbers.Number = lmt_mfsa_Companies.Registration_No " +
                           "WHERE np_IDNumbers.CreditinfoID = '" + companyCIID + "'";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();
            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try {
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    if (!reader.IsDBNull(0)) {
                        return reader.GetString(0);
                    }
                    return null;
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            } finally {
                myOleDbConn.Close();
            }
            return null;
        }

        public BoardSecretaryBLLC GetMTBoardSecretary(string nationalID) {
            const string funcName = "GetMTBoardSecretary(string nationalID) ";

            string query =
                "select IP_SurName As SurName, IP_Name As FirstName, IP_ID_No As NationalID from fo_mt_DirectorsAndMore where RegistrationID = '" +
                nationalID + "' and Involvement = 'SECRETARY'";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();
            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try {
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    var sec = new BoardSecretaryBLLC();

                    if (!reader.IsDBNull(0)) {
                        sec.SurNameNative = reader.GetString(0);
                    }
                    if (!reader.IsDBNull(1)) {
                        sec.FirstNameNative = reader.GetString(1);
                    }
                    if (!reader.IsDBNull(2)) {
                        sec.NationalID = reader.GetString(2);
                    }

                    return sec;
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            } finally {
                myOleDbConn.Close();
            }
            return null;
        }

        public string GetLegalFormCode(int legalFormID) {
            const string funcName = "GetLegalFormCode(int legalFormID) ";

            string query = "SELECT Code from cpi_RegistrationForm WHERE FormID = " + legalFormID;
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();
            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try {
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    if (!reader.IsDBNull(0)) {
                        return reader.GetString(0);
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return "-1";
            } finally {
                myOleDbConn.Close();
            }
            return "-1";
        }

        public DateTime GetMTCompanyRegDate(string nationalID) {
            const string funcName = "GetMTCompanyState(string nationalID) ";

            string query = "select Registration_Date from lmt_mfsa_Companies where Registration_No = '" + nationalID +
                           "'";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();
            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try {
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    if (!reader.IsDBNull(0)) {
                        return reader.GetDateTime(0);
                    }
                    return DateTime.MinValue;
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return DateTime.MinValue;
            } finally {
                myOleDbConn.Close();
            }
            return DateTime.MinValue;
        }

        protected static DataSet executeSelectStatement(string query) {
            

            var ds = new DataSet();
            var myAdapter = new OleDbDataAdapter();
            var myOleConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            try {
                myOleConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleConn);
                myAdapter.Fill(ds);
                return ds;
            } finally {
                myOleConn.Close();
            }
        }

        public decimal GetCreditScoring(string nationalID, int yearleap, DateTime now) {
            
            const string funcName = "GetCreditScoring(string nationalID,int yearleap,DateTime now) ";
            string dateFormatted = now.ToString("yyyy-MM-dd");
            string query = "select cast(.dbo.ci_score_odds('" + nationalID + "'," + yearleap + ",'" + dateFormatted +
                           "')*100 as decimal(5,2))";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();
            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try {
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    if (!reader.IsDBNull(0)) {
                        return reader.GetDecimal(0);
                    }
                    return -1;
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return -1;
            } finally {
                myOleDbConn.Close();
            }
            return -1;
        }

        public int GetScoreCategory(decimal creditScore, int yearLeap) {
            
            const string funcName = "GetScoreCategory(decimal creditScore,int yearLeap) ";
            string sCreditScore = Convert.ToString(creditScore).Replace(",", ".");
            string query = "select .dbo.ci_score_ratings(" + sCreditScore + "," + yearLeap + ")";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();
            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try {
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    if (!reader.IsDBNull(0)) {
                        return reader.GetInt32(0);
                    }
                    return -1;
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return -1;
            } finally {
                myOleDbConn.Close();
            }
            return -1;
        }

        public DataSet GetScoreRatingsAsDataSet() {
            var dsScoreRatings = new DataSet();
            const string funcName = "GetScoreRatingsAsDataSet() ";

            const string query = "select s_min, s_max, rating_na, rating_en from ci_score_ratingstbl where years=1 order by [order]";
            try {
                dsScoreRatings = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsScoreRatings;
        }

        public DataSet GetCompanySubsidaries(int companyCIID) {
            var dsCompanySubsidaries = new DataSet();
            const string funcName = "GetCompanySubsidaries(int companyCIID)";

            string query =
                "SELECT comp.NameNative, comp.NameEN,idnum.Number,SubsidariesID, Ownership FROM cpi_Subsidiaries cpSub LEFT OUTER JOIN np_Companys comp ON cpSub.SubsidariesID = comp.CreditInfoID LEFT OUTER JOIN np_IDNumbers idnum ON comp.CreditInfoID = idnum.CreditInfoID WHERE (CompanyCIID = " +
                companyCIID + ") AND idnum.NumberTypeID = 1";
            try {
                dsCompanySubsidaries = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCompanySubsidaries;
        }

        public DataSet GetCapital(int companyCIID) {
            var dsCapital = new DataSet();
            const string funcName = "GetCapital(int companyCIID)";

            string query = "SELECT * FROM cpi_Capital WHERE (CompanyCIID = " + companyCIID + ")";
            try {
                dsCapital = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCapital;
        }

        public decimal GetNumberOfShares(int companyCIID) {
            
            const string funcName = "GetNumberOfShares(int companyCIID)";
            string query = "SELECT IssuedNumberOfShares FROM cpi_Capital WHERE CompanyCIID = " + companyCIID;
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();
            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try {
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    if (!reader.IsDBNull(0)) {
                        return reader.GetDecimal(0);
                    }
                    return -1;
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return -1;
            } finally {
                myOleDbConn.Close();
            }
            return -1;
        }

        public DataSet GetCharges(int companyCIID) {
            var dsCharges = new DataSet();
            const string funcName = "GetCharges(int companyCIID)";

            string query =
                "select DateRegistered, DatePrepared, DescriptionNative, DescriptionEN, Amount, Sequence, NameNative, " +
                "NameEN, CurrencyCode, DescriptionFreeTextNative, DescriptionFreeTextEN " +
                "from cpi_Charges c, cpi_ChargesDescription cd, cpi_Banks b " +
                "where c.DescriptionID = cd.DescriptionID and c.BankID = b.BankID " +
                "and c.CompanyCIID = " + companyCIID + " " +
                "Order by DateRegistered desc";
            try {
                dsCharges = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCharges;
        }


        public string GetStatusAsString(int StatusID, bool nativeCult) {
            DataSet ds;
            const string funcName = "GetStatusAsString(int StatusID, bool nativeCult)";

            string query = "select NameNative, NameEN from cpi_StatusCodes where StatusID=" + StatusID;
            try {
                ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        return nativeCult ? ds.Tables[0].Rows[0]["NameNative"].ToString() : ds.Tables[0].Rows[0]["NameEN"].ToString();
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return "";
        }
    }
}
#pragma warning restore 618,612
