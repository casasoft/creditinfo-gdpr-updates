﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CPI.BLL;
using DataProtection;
using Logging.BLL;
using cb3;
using Cig.Framework.Base.Configuration;
using System.Collections.Generic;
using cb3.BLL.Scoring;
using System.Linq;
using cb3.Audit;

namespace cb3.CR.DAL
{
    public class ScoringDAL
    {
        public event EventHandler UpdateScoring;
        protected static string className = "ScoringDAL";
        private bool ExecuteNonQuery(string funcName, string command)
        {
            int result = -1;
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();

            var myOleDbCommand = new OleDbCommand(command, myOleDbConn);
            try
            {
                new AuditFactory(myOleDbCommand).PerformAuditProcess();
                result = myOleDbCommand.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                result = -1;
            }
            finally
            {
                myOleDbConn.Close();
            }
            return result != -1;
        }
        internal bool CreateOrUpdate(List<QuickRatio> quickRatio)
        {
            const string funcName = "CreateOrUpdate(List<CompanyAge> ranges)";

            if (quickRatio.Count == 1)
            {

                string command = string.Empty;

                if (quickRatio[0].Id == 0)
                    command = string.Format(@"INSERT INTO [dbo].[scorQuickRatio] ([QRBound], [DDDBound]) VALUES ({0}, {0})", quickRatio[0].QRBound, quickRatio[0].DDDBound);
                else
                    command = string.Format(@"UPDATE [dbo].[scorQuickRatio]  SET [QRBound] = {0}, [DDDBound] = {1}  WHERE ID = {2}", quickRatio[0].QRBound, quickRatio[0].DDDBound, quickRatio[0].Id);
                return this.ExecuteNonQuery(funcName, command);
            }
            else
                return false;

        }
        internal bool CreateOrUpdate(List<CompanyAge> ranges)
        {
            const string funcName = "CreateOrUpdate(List<CompanyAge> ranges)";
            StringBuilder commnds = new StringBuilder();

            #region create command
            foreach (var range in ranges)
            {
                if (range.Id != 0)
                {
                    if (range.Delete)
                    {
                        string deleteCommand = string.Format(@"delete from scorCompanyAge where id = {0}", range.Id);
                        commnds.AppendLine(deleteCommand);
                    }
                    else
                    {
                        string updateCommmand = @"UPDATE [scorCompanyAge]
                                               SET [MinTest] = {0}
                                                  ,[MaxTest] = {1}
                                                  ,[Value] = {2}
                                             WHERE ID = {3}";
                        commnds.AppendLine(string.Format(updateCommmand, range.MinTest, (range.MaxTest == null) ? "null" : range.MaxTest.Value.ToString(), (range.Value.HasValue) ? range.Value.Value.ToString() : "null", range.Id));

                    }
                }
                else
                {
                    if (!range.Delete)
                    {
                        string createCommmand = @"INSERT INTO [dbo].[scorCompanyAge]
                                           ([MinTest]
                                           ,[MaxTest]
                                           ,[Value])
                                     VALUES
                                           ({0}
                                           ,{1}
                                           ,{2})";
                        commnds.AppendLine(string.Format(createCommmand, range.MinTest, (range.MaxTest == null) ? "null" : range.MaxTest.Value.ToString(), (range.Value.HasValue) ? range.Value.Value.ToString() : "null"));
                    }
                }

            }
            #endregion
            return this.ExecuteNonQuery(funcName, commnds.ToString());

        }
        internal bool CreateOrUpdate(List<AccountsAge> ranges)
        {
            const string funcName = "CreateOrUpdate(List<scorAccountsAge> ranges)";
            StringBuilder commnds = new StringBuilder();

            #region create command
            foreach (var range in ranges)
            {
                if (range.Id != 0)
                {
                    if (range.Delete)
                    {
                        string deleteCommand = string.Format(@"delete from scorAccountsAge where id = {0}", range.Id);
                        commnds.AppendLine(deleteCommand);
                    }
                    else
                    {
                        string updateCommmand = @"UPDATE [scorAccountsAge]
                                               SET [MinTest] = {0}
                                                  ,[MaxTest] = {1}
                                                  ,[Value] = {2}
                                             WHERE ID = {3}";
                        commnds.AppendLine(string.Format(updateCommmand, range.MinTest, (range.MaxTest == null) ? "null" : range.MaxTest.Value.ToString(), (range.Value.HasValue) ? range.Value.Value.ToString() : "null", range.Id));

                    }
                }
                else
                {
                    if (!range.Delete)
                    {
                        string createCommmand = @"INSERT INTO [dbo].[scorAccountsAge]
                                           ([MinTest]
                                           ,[MaxTest]
                                           ,[Value])
                                     VALUES
                                           ({0}
                                           ,{1}
                                           ,{2})";
                        commnds.AppendLine(string.Format(createCommmand, range.MinTest, (range.MaxTest == null) ? "null" : range.MaxTest.Value.ToString(), (range.Value.HasValue) ? range.Value.Value.ToString() : "null"));
                    }
                }

            }
            #endregion
            return this.ExecuteNonQuery(funcName, commnds.ToString());
        }
        internal bool CreateOrUpdate(List<cb3.BLL.Scoring.FSI> ranges)
        {
            const string funcName = "CreateOrUpdate(List<cb3.BLL.Scoring.FSI> ranges)";
            StringBuilder commnds = new StringBuilder();

            #region create command
            foreach (var range in ranges)
            {
                if (range.Id != 0)
                {
                    if (range.Delete)
                    {
                        string deleteCommand = string.Format(@"delete from scorFSI where id = {0}", range.Id);
                        commnds.AppendLine(deleteCommand);
                    }
                    else
                    {
                        string updateCommmand = @"UPDATE [dbo].[scorFSI]
                                               SET [MinTest] = {0}
                                                  ,[MaxTest] = {1}
                                                  ,[Value] = '{2}'
                                                  ,[RiskDescription] = '{3}'
                                                  ,[Color] = '{4}'
                                                  ,[DDDNumber] = '{5}'  
                                             WHERE ID = {6}";
                        commnds.AppendLine(string.Format(updateCommmand, range.MinTest, (range.MaxTest == null) ? "null" : range.MaxTest.Value.ToString(), range.Value, range.RiskDescription, range.Color, range.DDDNumber, range.Id));

                    }
                }
                else
                {
                    if (!range.Delete)
                    {
                        string createCommmand = @"INSERT INTO [dbo].[scorFSI]
                                                   ([MinTest]
                                                   ,[MaxTest]
                                                   ,[Value]
                                                   ,[RiskDescription]
                                                   ,[Color], [DDDNumber])
                                             VALUES
                                                   ({0}
                                                   ,{1}
                                                   ,'{2}'
                                                   ,'{3}'
                                                   ,'{4}', {5})";
                        commnds.AppendLine(string.Format(createCommmand, range.MinTest, (range.MaxTest == null) ? "null" : range.MaxTest.Value.ToString(), range.Value, range.RiskDescription, range.Color, range.DDDNumber));
                    }
                }
               
            }
            #endregion
            return this.ExecuteNonQuery(funcName, commnds.ToString());
        }

        internal bool CreateOrUpdate(List<cb3.BLL.Scoring.RiskBandsRange> ranges)
        {
            const string funcName = "CreateOrUpdate(List<cb3.BLL.Scoring.RiskBandsRange> ranges)";
            StringBuilder commnds = new StringBuilder();

            #region create command
            foreach (var range in ranges)
            {
                if (range.Id != 0)
                {
                    if (range.Delete)
                    {
                        string deleteCommand = string.Format(@"delete from scorRiskBandsRanges where id = {0}", range.Id);
                        commnds.AppendLine(deleteCommand);
                    }
                    else
                    {
                        string updateCommmand = @"UPDATE [dbo].[scorRiskBandsRanges]
                                               SET [MinTest] = {0}
                                                  ,[MaxTest] = {1}
                                                  ,[Value] = '{2}'
                                                  ,[RiskDescription] = '{3}'
                                                  ,[Color] = '{4}'
                                             WHERE ID = {5}";
                        commnds.AppendLine(string.Format(updateCommmand, range.MinTest, (range.MaxTest == null) ? "null" : range.MaxTest.Value.ToString(), range.Value, range.RiskDescription, range.Color, range.Id));

                    }
                }
                else
                {
                    if (!range.Delete)
                    {
                        string createCommmand = @"INSERT INTO [dbo].[scorRiskBandsRanges]
                                                   ([MinTest]
                                                   ,[MaxTest]
                                                   ,[Value]
                                                   ,[RiskDescription]
                                                   ,[Color])
                                             VALUES
                                                   ({0}
                                                   ,{1}
                                                   ,'{2}'
                                                   ,'{3}'
                                                   ,'{4}')";
                        commnds.AppendLine(string.Format(createCommmand, range.MinTest, (range.MaxTest == null) ? "null" : range.MaxTest.Value.ToString(), range.Value, range.RiskDescription, range.Color));
                    }
                }

            }
            #endregion
            return this.ExecuteNonQuery(funcName, commnds.ToString());
        }

        public List<cb3.BLL.Scoring.RiskBandsRange> GetScorRiskBandsRanges()
        {
            const string funcName = "GetScorRiskBandsRanges() ";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();

            string query = @"SELECT [id]
                  ,[MinTest]
                  ,[MaxTest]
                  ,[Value]
                  ,[RiskDescription]
                  ,[Color]
              FROM [dbo].[scorRiskBandsRanges] ORDER BY ID ";

            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try
            {
                List<cb3.BLL.Scoring.RiskBandsRange> result = new List<cb3.BLL.Scoring.RiskBandsRange>();
                using (OleDbDataReader reader = myOleDbCommand.ExecuteReader())
                {
                    int itemIndex = 0;
                    while (reader.Read())
                    {
                        result.Add(new cb3.BLL.Scoring.RiskBandsRange()
                        {
                            ItemIndex = ++itemIndex,
                            Id = int.Parse(reader["ID"].ToString()),
                            MinTest = decimal.Parse(reader["MinTest"].ToString()),
                            MaxTest = (!string.IsNullOrEmpty(reader["MaxTest"].ToString())) ? decimal.Parse(reader["MaxTest"].ToString()) : new Nullable<decimal>(),
                            Value = reader["Value"].ToString(),
                            RiskDescription = reader["RiskDescription"].ToString(),
                            Color = reader["Color"].ToString(),
                        });
                    }
                }
                if (result.Count == 0)
                {
                    cb3.BLL.Scoring.RiskBandsRange item = new cb3.BLL.Scoring.RiskBandsRange() { ItemIndex = 1, MinTest = 0, Color = "000000" };
                    result.Add(item);
                }
                return result;
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
            finally
            {
                myOleDbConn.Close();
            }
        }

        public List<cb3.BLL.Scoring.FSI> GetScorFSI()
        {
            const string funcName = "GetScorFSI() ";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();

            string query = @"SELECT [id]
                  ,[MinTest]
                  ,[MaxTest]
                  ,[Value]
                  ,[RiskDescription]
                  ,[Color], [DDDNumber]
              FROM [dbo].[scorFSI] ORDER BY ID ";

            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try
            {
                List<cb3.BLL.Scoring.FSI> result = new List<cb3.BLL.Scoring.FSI>();
                using (OleDbDataReader reader = myOleDbCommand.ExecuteReader())
                {
                    int itemIndex = 0;
                    while (reader.Read())
                    {
                        result.Add(new cb3.BLL.Scoring.FSI() 
                        { 
                          ItemIndex = ++itemIndex,
                          Id = int.Parse(reader["ID"].ToString()),
                          MinTest = int.Parse(reader["MinTest"].ToString()),
                          MaxTest = (!string.IsNullOrEmpty(reader["MaxTest"].ToString())) ? int.Parse(reader["MaxTest"].ToString()) : new Nullable<int>(),
                          Value = reader["Value"].ToString(),
                          RiskDescription = reader["RiskDescription"].ToString(),
                          Color = reader["Color"].ToString(),
                          DDDNumber = int.Parse(reader["DDDNumber"].ToString())
                        });
                    }
                }
                if (result.Count == 0)
                {
                    cb3.BLL.Scoring.FSI item = new cb3.BLL.Scoring.FSI() { ItemIndex = 1, MinTest = 0, Color = "000000" };
                    result.Add(item);
                }
                return result;
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
            finally
            {
                myOleDbConn.Close();
            }
        }
        public List<CompanyAge> GetScorCompanyAge()
        {
            const string funcName = "GetScorCompanyAge() ";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();

            string query = @"SELECT [id]
                      ,[MinTest]
                      ,[MaxTest]
                      ,[Value]
                  FROM [dbo].[scorCompanyAge]";

            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try
            {
                List<CompanyAge> result = new List<CompanyAge>();
                using (OleDbDataReader reader = myOleDbCommand.ExecuteReader())
                {
                    int itemIndex = 0;
                    while (reader.Read())
                    {
                        result.Add(new CompanyAge()
                        {
                            ItemIndex = ++itemIndex,
                            Id = int.Parse(reader["ID"].ToString()),
                            MinTest = int.Parse(reader["MinTest"].ToString()),
                            
                            MaxTest = (!string.IsNullOrEmpty(reader["MaxTest"].ToString())) ? int.Parse(reader["MaxTest"].ToString()) : new Nullable<int>(),
                            Value = (!string.IsNullOrEmpty(reader["Value"].ToString())) ? decimal.Parse(reader["Value"].ToString()) : new Nullable<decimal>()
                        });
                    }
                }
                if (result.Count == 0)
                {
                    cb3.BLL.Scoring.CompanyAge item = new cb3.BLL.Scoring.CompanyAge() { ItemIndex = 1, MinTest = 0 };
                    result.Add(item);
                }
                return result;
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
            finally
            {
                myOleDbConn.Close();
            }
        }
        public List<AccountsAge> GetScorAccountsAge()
        {
            const string funcName = "GetScorAccountsAge() ";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();

            string query = @"SELECT [id]
                  ,[MinTest]
                  ,[MaxTest]
                  ,[Value]
              FROM [dbo].[scorAccountsAge]";

            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try
            {
                List<AccountsAge> result = new List<AccountsAge>();
                int itemIndex = 0;
                using (OleDbDataReader reader = myOleDbCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        result.Add(new AccountsAge()
                        {
                            ItemIndex = ++itemIndex,
                            Id = int.Parse(reader["ID"].ToString()),
                            MinTest = int.Parse(reader["MinTest"].ToString()),
                            MaxTest = (!string.IsNullOrEmpty(reader["MaxTest"].ToString())) ? int.Parse(reader["MaxTest"].ToString()) : new Nullable<int>(),
                            Value = (!string.IsNullOrEmpty(reader["Value"].ToString())) ? decimal.Parse(reader["Value"].ToString()) : new Nullable<decimal>()
                
                        });
                    }
                }
                if (result.Count == 0)
                {
                    cb3.BLL.Scoring.AccountsAge item = new cb3.BLL.Scoring.AccountsAge() { ItemIndex = 1, MinTest = 0 };
                    result.Add(item);
                }
                return result;
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
            finally
            {
                myOleDbConn.Close();
            }
        }
        public List<QuickRatio> GetScorQuickRatio()
        {
            const string funcName = "GetScorQuickRatio() ";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();

            string query = "select ID, QRBound, DDDBound from dbo.scorQuickRatio";

            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try
            {
                List<QuickRatio> result = new List<QuickRatio>();
                using (OleDbDataReader reader = myOleDbCommand.ExecuteReader(CommandBehavior.SingleRow))
                {
                    while (reader.Read())
                    {
                        QuickRatio ratio = new QuickRatio();
                        ratio.Id = int.Parse(reader["ID"].ToString());
                        ratio.QRBound = decimal.Parse(reader["QRBound"].ToString());
                        ratio.DDDBound = decimal.Parse(reader["DDDBound"].ToString());
                        result.Add(ratio);
                    }
                }
                return result;
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
            finally
            {
                myOleDbConn.Close();
            }
        }
      
        internal int? GetCompanyStatus(int CreditInfoId)
        {
    
            const string funcName = "GetFSIValue() ";
            var myOleDbConn = new  OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();

            string query = @"select org_status_code from dbo.np_Companys where CreditInfoId = ?";

            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);

            var param = new OleDbParameter("CreditInfoId", OleDbType.VarChar);
            param.Direction = ParameterDirection.Input;
            param.Value = CreditInfoId;

            myOleDbCommand.Parameters.Add(param);
            try
            {
                var result = myOleDbCommand.ExecuteScalar();
                if (result == null)
                    throw new ArgumentNullException("GetExclusionInterval");
                return int.Parse(result.ToString());
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                throw;
            }
            finally
            {
                myOleDbConn.Close();
            }
        }
        internal bool CreateOrUpdate<T>(List<T> ranges)
        {
            if (ranges.GetType() == typeof(List<cb3.BLL.Scoring.FSI>))
            {
                List<cb3.BLL.Scoring.FSI> result = (List<cb3.BLL.Scoring.FSI>)Convert.ChangeType(ranges, typeof(List<cb3.BLL.Scoring.FSI>));
                return this.CreateOrUpdate(result);
            }
            if (ranges.GetType() == typeof(List<cb3.BLL.Scoring.AccountsAge>))
            {
                List<cb3.BLL.Scoring.AccountsAge> result = (List<cb3.BLL.Scoring.AccountsAge>)Convert.ChangeType(ranges, typeof(List<cb3.BLL.Scoring.AccountsAge>));
                return this.CreateOrUpdate(result);
            }
            if (ranges.GetType() == typeof(List<cb3.BLL.Scoring.RiskBandsRange>))
            {
                List<cb3.BLL.Scoring.RiskBandsRange> result = (List<cb3.BLL.Scoring.RiskBandsRange>)Convert.ChangeType(ranges, typeof(List<cb3.BLL.Scoring.RiskBandsRange>));
                return this.CreateOrUpdate(result);
            }
            if (ranges.GetType() == typeof(List<cb3.BLL.Scoring.CompanyAge>))
            {
                List<cb3.BLL.Scoring.CompanyAge> result = (List<cb3.BLL.Scoring.CompanyAge>)Convert.ChangeType(ranges, typeof(List<cb3.BLL.Scoring.CompanyAge>));
                return this.CreateOrUpdate(result);
            }
            if (ranges.GetType() == typeof(List<cb3.BLL.Scoring.QuickRatio>))
            {
                List<cb3.BLL.Scoring.QuickRatio> result = (List<cb3.BLL.Scoring.QuickRatio>)Convert.ChangeType(ranges, typeof(List<cb3.BLL.Scoring.QuickRatio>));
                return this.CreateOrUpdate(result);
            }
            return false;

        }
        internal List<T> LoadData<T>()
        {
            if (typeof(T) == typeof(cb3.BLL.Scoring.FSI))
            {
                return (List<T>)Convert.ChangeType(this.GetScorFSI(), typeof(List<T>));
            }
            if (typeof(T) == typeof(cb3.BLL.Scoring.AccountsAge))
            {
                return (List<T>)Convert.ChangeType(this.GetScorAccountsAge(), typeof(List<T>));
            }
            if (typeof(T) == typeof(cb3.BLL.Scoring.CompanyAge))
            {
                return (List<T>)Convert.ChangeType(this.GetScorCompanyAge(), typeof(List<T>));
            }
            if (typeof(T) == typeof(cb3.BLL.Scoring.QuickRatio))
            {
                return (List<T>)Convert.ChangeType(this.GetScorQuickRatio(), typeof(List<T>));
            }
            if (typeof(T) == typeof(cb3.BLL.Scoring.RiskBandsRange))
            {
                return (List<T>)Convert.ChangeType(this.GetScorRiskBandsRanges(), typeof(List<T>));
            }
            return null;
        }

      
    }
}
