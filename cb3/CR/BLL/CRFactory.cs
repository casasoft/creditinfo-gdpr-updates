#region

using System;
using System.Data;
using Billing.BLL;
using Billing.DAL;
using CPI.BLL;
using CPI.DAL;
using CR.DAL;
using NPayments.DAL.Claims;
using UserAdmin.DAL.CIUsers;

#endregion

namespace CR.BLL {
    /// <summary>
    /// Summary description for CRFactory.
    /// </summary>
    public class CRFactory {
        private readonly BillingDALC billingDal;
        private readonly ClaimDALC claimDalc;
        private readonly CompanyProfileInputDALC cpiDacl;
        private readonly CompanyReportsDALC dacl;
        private readonly CreditInfoUserDALC userDacl;

        public CRFactory() {
            dacl = new CompanyReportsDALC();
            cpiDacl = new CompanyProfileInputDALC();
            userDacl = new CreditInfoUserDALC();
            billingDal = new BillingDALC();
            claimDalc = new ClaimDALC();
        }

        #region CR

        public DataSet GetShareholderInvolvements(int ownerCIID) { return dacl.GetShareholderInvolvements(ownerCIID); }
        public DataSet GetPrincipalsInvolvements(int ownerCIID) { return dacl.GetPrincipalsInvolvements(ownerCIID); }
        public DataSet GetBoardMemberInvolvements(int ownerCIID) { return dacl.GetBoardMemberInvolvements(ownerCIID); }
        public BoardSecretaryBLLC GetMTBoardSecretary(string nationalID) { return dacl.GetMTBoardSecretary(nationalID); }
        public DataSet GetMTCompanyInvolvements(string nationalID, bool isCompany) { return dacl.GetMTCompanyInvolvements(nationalID, isCompany); }
        public DataSet GetMTDirectorsAndMore(string nationalID) { return dacl.GetMTDirectorsAndMore(nationalID); }
        public decimal GetNumberOfShares(int companyCIID) { return dacl.GetNumberOfShares(companyCIID); }

        public int InsertInto_np_Usage(
            int creditInfoID, int queryType, string query, int resultCount, string ip, int userID) { return dacl.InsertInto_np_Usage(creditInfoID, queryType, query, resultCount, ip, userID); }

        public bool IsIDInDebitorsDatabase(int creditInfoID) { return dacl.IsIDInDebitorsDatabase(creditInfoID); }
        public string GetRegistrationFormAsString(int formID, bool native) { return dacl.GetRegistrationFormAsString(formID, native); }
        public HistoryBLLC GetHistory(int companyCIID, int historyTypeID) { return cpiDacl.GetHistory(companyCIID, historyTypeID); }
        public DataSet GetDirectorsAsDataset(int companyCIID) { return dacl.GetDirectorsAsDataset(companyCIID); }
        public DataSet GetCustomerTypeAsDataSet(int companyCIID) { return dacl.GetCustomerTypeAsDataSet(companyCIID); }
        public DataSet GetExportToAsDataSet(int companyCIID) { return dacl.GetExportToAsDataSet(companyCIID); }
        public DataSet GetImportFromTypeAsDataSet(int companyCIID) { return dacl.GetImportFromAsDataSet(companyCIID); }
        public DataSet GetBoardAsDataset(int companyCIID) { return dacl.GetBoardAsDataset(companyCIID); }
        public DataSet GetKeyEmployeesAsDataset(int companyCIID) { return dacl.GetKeyEmployeesAsDataset(companyCIID); }
        public DataSet GetInvestmentsAsDataset(int companyCIID) { return dacl.GetInvestmentsAsDataset(companyCIID); }
        public DataSet GetRealEstatesAsDataset(int companyCIID) { return dacl.GetRealEstatesAsDataset(companyCIID); }
        public DataSet GetBanksAsDataset(int companyCIID) { return dacl.GetBanksAsDataset(companyCIID); }
        public DataSet GetBalanceSheetAsDataset(int companyCIID, string afs_ids) { return dacl.GetBalanceSheetAsDataset(companyCIID, afs_ids); }
        public DataSet GetProfitLossAccountAsDataset(int companyCIID, string afs_ids) { return dacl.GetProfitLossAccountAsDataset(companyCIID, afs_ids); }
        public DataSet GetNACECodesAsDataSet(int companyCIID) { return dacl.GetNACECodesAsDataSet(companyCIID); }
        public DataSet GetTradeTermsAsDataSet(int companyCIID) { return dacl.GetTradeTermsAsDataSet(companyCIID); }

        public decimal GetCreditScoring(int companyCIID, int yearleap, DateTime now) {
            String nationalID = userDacl.getNationalIDByCIID(companyCIID);
            return dacl.GetCreditScoring(nationalID, yearleap, now);
            //	return -1;
        }

        public int GetScoreCategory(decimal creditScore, int yearLeap) { return dacl.GetScoreCategory(creditScore, yearLeap); }
        public DataSet GetScoreRatingsAsDataSet() { return dacl.GetScoreRatingsAsDataSet(); }
        public DataSet GetCompanySubsidaries(int companyCIID) { return dacl.GetCompanySubsidaries(companyCIID); }
        public DataSet GetCapital(int companyCIID) { return dacl.GetCapital(companyCIID); }
        public DataSet GetCharges(int companyCIID) { return dacl.GetCharges(companyCIID); }

        public DataSet GetCourtDecisionsAndDefaultingDebts(int creditInfoID) {
            // return dacl.GetCourtDecisionsAndDefaultingDebts(creditiInfoID);
            // return claimDalc.GetAllClaimsWithClaimOwnerNameNativeByCIIDAsDataSet(creditiInfoID);
            return claimDalc.GetAllActiveClaimsWithClaimOwnerNameNativeByCIIDAsDataSet(creditInfoID);
        }

        public DataSet GetCompanyState(int companyCIID) { return dacl.GetCompanyState(companyCIID); }
        public string GetMTCompanyState(string nationalID) { return dacl.GetMTCompanyState(nationalID); }
        public string GetMTCompanyStateBO(int companyCIID) { return dacl.GetMTCompanyStateBO(companyCIID); }
        public DateTime GetMTCompanyRegDate(string nationalID) { return dacl.GetMTCompanyRegDate(nationalID); }
        public DataSet GetCyprusCompanyFormerNames(string companyNationalID) { return dacl.GetCyprusCompanyFormerNames(companyNationalID); }
        public string GetStatusAsString(int statusID, bool native) { return dacl.GetStatusAsString(statusID, native); }
        public string GetLegalFormCode(int legalFormID) { return dacl.GetLegalFormCode(legalFormID); }

        #endregion

        #region CPI

        public ReportCompanyBLLC GetCompanyReport(String nationalID, int CIID, bool useNationalID) { return cpiDacl.GetCompanyReport(nationalID, CIID, useNationalID); }

        /// <summary>
        ///  Ltihuanian specifice
        /// </summary>
        /// <param name="nationalID"></param>
        /// <param name="CIID"></param>
        /// <param name="useNationalID"></param>
        /// <returns></returns>
        public ReportCompanyBLLC GetLTCompanyReport(String nationalID, int CIID, bool useNationalID) { return cpiDacl.GetLTCompanyReport(nationalID, CIID, useNationalID); }

        public HistoryOperationReviewBLLC GetHistoryOperationReview(int companyCIID) { return cpiDacl.GetHistoryOperationReview(companyCIID); }
        public DataSet GetShareHolderOwnerAsDataSet(int companyCIID) { return cpiDacl.GetShareHolderOwnerAsDataSet(companyCIID); }
        public DataSet GetShareHolderOwnerAsDataSet(int companyCIID, int ownerShipTypes) { return cpiDacl.GetShareHolderOwnerAsDataSet(companyCIID, ownerShipTypes); }
        public CapitalBLLC GetCPICapital(int companyCIID) { return cpiDacl.GetCapital(companyCIID); }
        public DataSet GetEmployeesCountAsDataSet(int companyCIID) { return cpiDacl.GetStaffCountAsDataSet(companyCIID); }
        public DataSet GetLaywersAsDataSet(int companyCIID) { return cpiDacl.GetLaywersAsDataSet(companyCIID); }
        public DataSet GetAuditorsAsDataSet(int companyCIID) { return cpiDacl.GetAuditorsAsDataSet(companyCIID); }
        public BoardSecretaryBLLC GetBoardSecretary(int companyCIID) { return cpiDacl.GetBoardSecretary(companyCIID); }
        public BoardSecretaryCompanyBLLC GetCompanyBoardSecretary(int companyCIID) { return cpiDacl.GetCompanyBoardSecretary(companyCIID); }

        #endregion

        #region CR

        public string GetCompanyStatusByCIID(int CIID) { return userDacl.GetCompanyStatusByCIID(CIID); }

        #endregion

        public DataSet GetFileFormats() { return cpiDacl.GetFileFormats(); }

        #region Billing

        public ReportCompanyBLLC GetBillingCompanyInfo(String nationalID, int CIID, bool useNationalID) { return cpiDacl.GetBillingCompanyInfo(nationalID, CIID, useNationalID); }
        public AgreementBLLC GetBillingAgreement(int nAgreementID) { return billingDal.GetAgreement(nAgreementID); }
        public string GetBillingEmployeeName(int nEmployeeCIID, bool IsNative) { return billingDal.GetEmployeeName(nEmployeeCIID, IsNative); }
        public BillingItemBLLC GetBillingItem(int nItemID) { return billingDal.GetRegistryItem(nItemID); }

        #endregion
    }
}