#region

using System.Xml;

#endregion

namespace CR.BLL {
    /// <summary>
    /// Helper class that contains methods for creating xml report 
    /// documents that are to be transformed with xsl into html or 
    /// some other format
    /// </summary>
    public class XmlHelper {
        /// <summary> Type of regular column </summary>
        public const string COLUMN = "td";

        /// <summary> Type of right centered column </summary>
        public const string RIGHT_COLUMN = "RightCol";

        /// <summary> Type of right centered header column </summary>
        public const string RIGHT_HEADER_COLUMN = "RightHeaderCol";

        /// <summary>
        /// Creates a new empty xml document
        /// </summary>
        /// <returns>The new xml document</returns>
        public static XmlDocument CreateDocument() {
            var doc = new XmlDocument();
            XmlDeclaration decl = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(decl);
            return doc;
        }

        /// <summary>
        /// Creates a new xml element with given name and value
        /// </summary>
        /// <param name="doc">The xml document to create element for</param>
        /// <param name="name">The name of the element</param>
        /// <param name="val">The value for the element</param>
        /// <returns>The new xml element</returns>
        public static XmlElement CreateElement(XmlDocument doc, string name, string val) {
            XmlElement el = doc.CreateElement(name);
            if (val == "") {
                val = " ";
            }
            el.InnerText = val;
            return el;
        }

        /// <summary>
        /// Creates a new xml element with given name and value
        /// </summary>
        /// <param name="doc">The xml document to create element for</param>
        /// <param name="name">The name of the element</param>
        /// <param name="attr1"></param>
        /// <param name="val1"></param>
        /// <param name="attr2"></param>
        /// <param name="val2"></param>
        /// <returns>The new xml element</returns>
        public static XmlElement CreateElement(
            XmlDocument doc, string name, string attr1, string val1, string attr2, string val2) {
            var el = doc.CreateElement(name);
            AddAttribute(doc, el, attr1, val1);
            AddAttribute(doc, el, attr2, val2);
            return el;
        }

        /// <summary>
        /// Creates a new xml element with given name and value
        /// </summary>
        /// <param name="doc">The xml document to create element for</param>
        /// <param name="name">The name of the element</param>
        /// <param name="attr1"></param>
        /// <param name="val1"></param>
        /// <returns>The new xml element</returns>
        public static XmlElement CreateElement(XmlDocument doc, string name, string attr1, string val1) {
            var el = doc.CreateElement(name);
            AddAttribute(doc, el, attr1, val1);
            return el;
        }

        /// <summary>
        /// Creates a new attribute and adds it to given xml element
        /// </summary>
        /// <param name="doc">The document to create attribute for</param>
        /// <param name="el">The element to add attribute to</param>
        /// <param name="name">The name of the attribute</param>
        /// <param name="val">The value of the attribute</param>
        /// <returns>the xml element with the new attribute added</returns>
        public static XmlElement AddAttribute(XmlDocument doc, XmlElement el, string name, string val) {
            var att = doc.CreateAttribute(name);
            att.InnerText = val;
            el.Attributes.Append(att);
            return el;
        }

        /// <summary>
        /// Creates a xml table element
        /// </summary>
        /// <param name="doc">The document to create table from</param>
        /// <param name="tableName">the name for the table</param>
        /// <returns>The table on xml form</returns>
        public static XmlElement GetXmlTable(XmlDocument doc, string tableName) { return GetXmlTable(doc, "table", tableName); }

        /// <summary>
        /// Creates a xml table element of specific type
        /// </summary>
        /// <param name="doc">The document to create table from</param>
        /// <param name="tableType">The type of the table</param>
        /// <param name="tableName">The name for the table</param>
        /// <returns>The table on xml format</returns>
        public static XmlElement GetXmlTable(XmlDocument doc, string tableType, string tableName) {
            var el = CreateElement(doc, tableType, "");
            el = AddAttribute(doc, el, "name", tableName);
            return el;
        }

        /// <summary>
        /// Creates a xml table with given header and a single row - containing given value
        /// </summary>
        /// <param name="doc">The document to create table from</param>
        /// <param name="tableName">Name of the table</param>
        /// <param name="header">Header text for the table</param>
        /// <param name="val">The value of the row</param>
        /// <returns>Xml table created</returns>
        public static XmlElement GetTableWithHeaderAndSingleRow(
            XmlDocument doc, string tableName, string header, string val) {
            var table = GetXmlTable(doc, tableName);
            table.AppendChild(GetXmlHeaderRowWithValue(doc, header));
            var row = GetXmlRow(doc);
            row.AppendChild(GetXmlColumn(doc, val));
            table.AppendChild(row);
            return table;
        }

        /// <summary>
        /// Creates a xml table with given header and a single row - containing values as list 
        /// </summary>
        /// <param name="doc">The document to create table from</param>
        /// <param name="tableName">Name of the table</param>
        /// <param name="header">Header text for the table</param>
        /// <param name="val">The value of the row</param>
        /// <param name="bullets"></param>
        /// <returns>Xml table created</returns>
        public static XmlElement GetTableWithHeaderAndListRow(
            XmlDocument doc, string tableName, string header, string val, bool bullets) {
            var table = GetXmlTable(doc, tableName);
            table.AppendChild(GetXmlHeaderRowWithValue(doc, header));
            var row = GetXmlRow(doc);
            row.AppendChild(GetTextAsList(doc, val, bullets));
            table.AppendChild(row);
            return table;
        }

        public static XmlElement GetXmlHeaderTableWithValues(
            XmlDocument doc, string tableName, string val1, string val2) {
            var table = GetXmlTable(doc, tableName);
            table.AppendChild(GetXmlHeaderRowWith2ValuesSecondRight(doc, val1, val2));
            return table;
        }

        /// <summary>
        /// Creates a xml header row.  Header row is used for table header - usually 
        /// with different color that other rows in the table.
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <returns>The header row on xml format</returns>
        public static XmlElement GetXmlHeaderRow(XmlDocument doc) {
            var el = CreateElement(doc, "HeaderRow", "");
            return el;
        }

        /// <summary>
        /// Creates a xml header row with given value.  Header row is used for table 
        /// header - usually with different color that other rows in the table.  Text
        /// is bold.
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="val">The value for the header row</param>
        /// <returns>The header row on xml format</returns>
        public static XmlElement GetXmlHeaderRowWithValue(XmlDocument doc, string val) {
            var row = GetXmlHeaderRow(doc);
            var col = GetXmlHeaderColumn(doc, "");
            col.AppendChild(GetXmlBoldText(doc, val));
            row.AppendChild(col);
            return row;
        }

        /// <summary>
        /// Creates a xml header row with given value.  Header row is used for table 
        /// header - usually with different color that other rows in the table.  Text
        /// is bold.
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="values">The values as array for all column in the header row</param>
        /// <returns>The header row on xml format</returns>
        public static XmlElement GetXmlHeaderRowWithValue(XmlDocument doc, string[] values) {
            var row = GetXmlHeaderRow(doc);
            for (int i = 0; i < values.Length; i++) {
                XmlElement col = GetXmlHeaderColumn(doc, "");
                col.AppendChild(GetXmlBoldText(doc, values[i]));
                row.AppendChild(col);
            }
            return row;
        }

        /// <summary>
        /// Creates a xml dark row with given value.  Dark row is used for table 
        /// financial informations - usually with different color that other rows in the table.  
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="values">The values as array for all column in the header row</param>
        /// <param name="bold">True if the text should be bold, false if not</param>
        /// <returns>The header row on xml format</returns>
        public static XmlElement GetXmlDarkRowWithValue(XmlDocument doc, string[] values, bool bold) {
            var row = GetXmlRow(doc);
            for (int i = 0; i < values.Length; i++) {
                XmlElement col;
                if (bold) {
                    col = GetXmlDarkColumn(doc, "");
                    col.AppendChild(GetXmlBoldText(doc, values[i]));
                } else {
                    col = GetXmlDarkColumn(doc, values[i]);
                }

                row.AppendChild(col);
            }
            return row;
        }

        /// <summary>
        /// Creates a xml row with given value.
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="values">The values as array for all column in the row</param>
        /// <param name="bold">True if the text should be bold, false if not</param>
        /// <returns>The row on xml format</returns>
        public static XmlElement GetXmlRowWithValue(XmlDocument doc, string[] values, bool bold) {
            var row = GetXmlRow(doc);
            for (int i = 0; i < values.Length; i++) {
                XmlElement col;
                if (bold) {
                    col = GetXmlColumn(doc, "");
                    col.AppendChild(GetXmlBoldText(doc, values[i]));
                } else {
                    col = GetXmlColumn(doc, values[i]);
                }
                row.AppendChild(col);
            }
            return row;
        }

        /// <summary>
        /// Creates a xml row with given value.
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="val1"></param>
        /// <param name="val2"></param>
        /// <returns>The row on xml format</returns>
        public static XmlElement GetXmlHeaderRowWith2ValuesSecondRight(XmlDocument doc, string val1, string val2) {
            var row = GetXmlHeaderRow(doc);
            var col1 = GetXmlColumn(doc, "");
            col1.AppendChild(GetXmlBoldText(doc, val1));
            var col2 = GetXmlColumn(doc, RIGHT_HEADER_COLUMN, "");
            col2.AppendChild(GetXmlBoldText(doc, val2));
            row.AppendChild(col1);
            row.AppendChild(col2);
            return row;
        }

        /// <summary>
        /// Creates a xml row with given value.
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="val2"></param>
        /// <param name="bold">True if the text should be bold, false if not</param>
        /// <param name="val1"></param>
        /// <returns>The row on xml format</returns>
        public static XmlElement GetXmlRowWith2ValuesSecondRight(XmlDocument doc, string val1, string val2, bool bold) {
            var row = GetXmlRow(doc);
            XmlElement col1;
            XmlElement col2;
            if (bold) {
                col1 = GetXmlColumn(doc, "");
                col1.AppendChild(GetXmlBoldText(doc, val1));
                col2 = GetXmlColumn(doc, RIGHT_COLUMN, "");
                col2.AppendChild(GetXmlBoldText(doc, val2));
            } else {
                col1 = GetXmlColumn(doc, val1);
                col2 = GetXmlColumn(doc, RIGHT_COLUMN, val2);
            }
            row.AppendChild(col1);
            row.AppendChild(col2);
            return row;
        }

        /// <summary>
        /// Creates a xml row with given value.
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="val2"></param>
        /// <param name="bold">True if the text should be bold, false if not</param>
        /// <param name="val1"></param>
        /// <returns>The row on xml format</returns>
        public static XmlElement GetFOXmlRowWith2ValuesSecondRight(XmlDocument doc, string val1, string val2, bool bold) {
            var row = GetXmlRow(doc);
            XmlElement col1;
            XmlElement col2;
            if (bold) {
                col1 = GetXmlColumn(doc, "");
                col1.AppendChild(GetXmlBoldText(doc, val1));
                col2 = GetXmlColumn(doc, RIGHT_COLUMN, "");
                col2.AppendChild(GetXmlBoldText(doc, val2));
            } else {
                col1 = GetXmlColumn(doc, val1);
                col2 = GetXmlColumn(doc, RIGHT_COLUMN, val2);
            }
            row.AppendChild(col1);
            row.AppendChild(col2);
            row.AppendChild(GetIndentColumn(doc));
            row.AppendChild(GetIndentColumn(doc));
            return row;
        }

        /// <summary>
        /// Creates a xml footer row containing given value and a colspan attribute
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="val">Value for the row</param>
        /// <param name="colspan">How many columns the single column added should span</param>
        /// <returns>The row created</returns>
        public static XmlElement GetXmlFooterRow(XmlDocument doc, string val, int colspan) {
            var row = GetXmlRow(doc);
            var col = GetXmlColumn(doc, "FooterCol", val);
            AddAttribute(doc, col, "colspan", colspan.ToString());
            row.AppendChild(col);
            return row;
        }

        /// <summary>
        /// Creates a xml node representing bold text
        /// </summary>
        /// <param name="doc">The document to create node from</param>
        /// <param name="text">The text to have bold</param>
        /// <returns>The bold node</returns>
        public static XmlElement GetXmlBoldText(XmlDocument doc, string text) {
            var el = CreateElement(doc, "bold", text);
            return el;
        }

        /// <summary>
        /// Creates a xml node representing bold text
        /// </summary>
        /// <param name="doc">The document to create node from</param>
        /// <param name="text">The text to have bold</param>
        /// <param name="bullets"></param>
        /// <returns>The bold node</returns>
        public static XmlElement GetXmlListText(XmlDocument doc, string text, bool bullets) {
            var el = bullets ? CreateElement(doc, "list", text) : CreateElement(doc, "NonBulletList", text);

            return el;
        }

        /// <summary>
        /// Creates a node representing normal text
        /// </summary>
        /// <param name="doc">The document to create node from</param>
        /// <param name="text">The text to include in the node</param>
        /// <returns>The normal text node</returns>
        public static XmlElement GetXmlNormalText(XmlDocument doc, string text) {
            var el = CreateElement(doc, "normal", text);
            return el;
        }

        /// <summary>
        /// Creates a node representing a row in a table
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <returns>The row node</returns>
        public static XmlElement GetXmlRow(XmlDocument doc) {
            var el = CreateElement(doc, "tr", "");
            return el;
        }

        /// <summary>
        /// Creates a node representing a table header
        /// </summary>
        /// <param name="doc">The document to create table header from</param>
        /// <returns>The table header node</returns>
        public static XmlElement GetXmlTableHeader(XmlDocument doc) {
            var el = CreateElement(doc, "TableHeader", "");
            return el;
        }

        /// <summary>
        /// Creates a node representing a table item
        /// </summary>
        /// <param name="doc">The document to create table item from</param>
        /// <returns>The table item node</returns>
        public static XmlElement GetXmlTableItem(XmlDocument doc) {
            var el = CreateElement(doc, "TableItem", "");
            return el;
        }

        /// <summary>
        /// Creates a node representing a table item
        /// </summary>
        /// <param name="doc">The document to create table item from</param>
        /// <param name="attrName"></param>
        /// <param name="attrValue"></param>
        /// <returns>The table item node</returns>
        public static XmlElement GetXmlTableItem(XmlDocument doc, string attrName, string attrValue) {
            var el = CreateElement(doc, "TableItem", "");
            AddAttribute(doc, el, attrName, attrValue);
            return el;
        }

        /// <summary>
        /// Creates a node representing a table footer
        /// </summary>
        /// <param name="doc">The document to create table footer from</param>
        /// <returns>The table footer node</returns>
        public static XmlElement GetXmlTableFooter(XmlDocument doc) {
            var el = CreateElement(doc, "TableFooter", "");
            return el;
        }

        /// <summary>
        /// Creates a node representing a column of given type in a row
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <param name="type">The type for the column</param>
        /// <param name="val">The text value for the column</param>
        /// <returns>The column node</returns>
        public static XmlElement GetXmlColumn(XmlDocument doc, string type, string val) {
            var el = CreateElement(doc, type, val);
            return el;
        }

        /// <summary>
        /// Creates a node representing a column in a row
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <param name="val">The text value for the column</param>
        /// <returns>The column node</returns>
        public static XmlElement GetXmlColumn(XmlDocument doc, string val) {
            var el = CreateElement(doc, "td", val);
            return el;
        }

        /// <summary>
        /// Creates a node representing a column in a row with a bold text
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <param name="val">The text value for the column</param>
        /// <returns>The bold column node</returns>
        public static XmlElement GetXmlBoldColumn(XmlDocument doc, string val) {
            var el = CreateElement(doc, "td", "");
            el.AppendChild(GetXmlBoldText(doc, val));
            return el;
        }

        /// <summary>
        /// Creates a node representing a header column for table
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <param name="val">The text value for the column</param>
        /// <returns>The header column node</returns>
        public static XmlElement GetXmlHeaderColumn(XmlDocument doc, string val) {
            var el = CreateElement(doc, "HeaderCol", val);
            AddAttribute(doc, el, "class", "header2");
            return el;
        }

        /// <summary>
        /// Creates a node representing a right-aligned header column for table
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <param name="val">The text value for the column</param>
        /// <returns>The header column node</returns>
        public static XmlElement GetXmlRightHeaderColumn(XmlDocument doc, string val) {
            var el = CreateElement(doc, RIGHT_HEADER_COLUMN, val);
            AddAttribute(doc, el, "class", "header2");
            return el;
        }

        /// <summary>
        /// Creates a node representing a header column for table
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <param name="val">The text value for the column</param>
        /// <returns>The header column node</returns>
        public static XmlElement GetXmlDarkColumn(XmlDocument doc, string val) {
            var el = CreateElement(doc, "DarkCol", val);
            AddAttribute(doc, el, "class", "headerb");
            return el;
        }

        /// <summary>
        /// Creates a xml indent column
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <returns>The indent column</returns>
        public static XmlElement GetIndentColumn(XmlDocument doc) { return CreateElement(doc, "IndentCol", ""); }

        /// <summary>
        /// Creates a xml dark indent column
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <returns>The indent column</returns>
        public static XmlElement GetDarkIndentColumn(XmlDocument doc) { return CreateElement(doc, "DarkIndentCol", ""); }

        /// <summary>
        /// Creates a xml header indent column
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <returns>The indent column</returns>
        public static XmlElement GetHeaderIndentColumn(XmlDocument doc) { return CreateElement(doc, "HeaderIndentCol", ""); }

        /// <summary>
        /// Creates a dark nbsp column
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <returns>The hdark nbsp column node</returns>
        public static XmlElement GetXmlDarkNBSPColumn(XmlDocument doc) { return CreateElement(doc, "Darknbsp", ""); }

        /// <summary>
        /// Creates a column representing a nbsp column
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <returns>The nbsp column</returns>
        public static XmlElement GetXmlNBSPRow(XmlDocument doc) {
            var row = GetXmlRow(doc);
            row.AppendChild(GetXmlNBSPColumn(doc));
            return row;
        }

        /// <summary>
        /// Creates a column representing a nbsp column
        /// </summary>
        /// <param name="doc">The document to create column from</param>
        /// <returns>The nbsp column</returns>
        public static XmlElement GetXmlNBSPColumn(XmlDocument doc) { return CreateElement(doc, "nbsp", ""); }

        public static XmlElement GetPopupLinkColumn(XmlDocument doc, string title, string link) {
            var col = GetXmlColumn(doc, "");
            col.AppendChild(CreateElement(doc, "PopupLink", "Title", title, "Link", link));
            return col;
        }

        public static XmlElement GetTextAsList(XmlDocument doc, string history, bool bullets) {
            var col = GetXmlColumn(doc, "");

            string[] arrHist;

            if (history.IndexOf('\r') > -1) {
                arrHist = history.Split('\r');
            } else if (history.IndexOf('\n') > -1) {
                arrHist = history.Split('\n');
            } else if (history.IndexOf("|") > -1) {
                arrHist = history.Split('|');
            } else {
                arrHist = new string[1];
                arrHist[0] = history;
            }
            for (int i = 0; i < arrHist.Length; i++) {
                string text = arrHist[i].Trim();
                if (text != "") // avoid czech "bad" data
                {
                    text = text.Trim(new[] {'\r', '\n'});
                    col.AppendChild(GetXmlListText(doc, text, bullets));
                }
            }
            return col;
        }
    }
}