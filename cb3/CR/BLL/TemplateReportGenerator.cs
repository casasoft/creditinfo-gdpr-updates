#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Xml;
using Billing.BLL;
using BusinessLogic.Generators.Report;
using CPI.BLL;
using CR.BLL.Financial;
using CreditWatch.BLL;
using FSI.BLL;
using Logging.BLL;
using UserAdmin.BLL.CIUsers;
using Logger=Logging.BLL.Logger;

#endregion

using Cig.Framework.Base.Configuration;

namespace CR.BLL {
    /// <summary>
    /// Summary description for TemplateReportGenerator.
    /// </summary>
    public class TemplateReportGenerator {
        protected AgreementBLLC agreement;
        protected CultureInfo ci;
        protected string cultureEnding;
        protected CRFactory factory = new CRFactory();
        protected FinancialInfo financialInfo;
        protected string ipAddress = "";
        protected bool nativeCult;
        protected int reportType = -1;
        protected ResourceManager rm;
        protected int userCreditInfoID = -1;
        protected int userID = -1;

        public TemplateReportGenerator(
            ResourceManager rm, string cultureToUse, int userCreditInfoID, int userID, string ipAddress) {
            this.userCreditInfoID = userCreditInfoID;
            this.userID = userID;
            this.ipAddress = ipAddress;
            string culture = "en-US";
            if (CigConfig.Configure("lookupsettings.nativeCulture") != null) {
                // strange sometimes cultureToUse is set to native culture and sometimes just to the string "Native" ?
                if (cultureToUse.Equals(CigConfig.Configure("lookupsettings.nativeCulture")) ||
                    cultureToUse.Equals("Native")) {
                    culture = CigConfig.Configure("lookupsettings.nativeCulture");
                    nativeCult = true;
                }
            }
            this.rm = rm;
            ci = new CultureInfo(culture);
            nativeCult = nativeCult;
            if (!nativeCult) {
                cultureEnding = "EN";
            } else {
                cultureEnding = "Native";
            }
        }

        /// <summary>
        /// Checks if the running version is for czceh
        /// </summary>
        /// <returns>True if the version is for czceh, false otherwise</returns>
        protected bool IsCzech {
            get {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                    if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech")) {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if the running version is for Slovakia
        /// </summary>
        /// <returns>True if the version is for Slovakia, false otherwise</returns>
        protected bool IsSlovak {
            get {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                    if (CigConfig.Configure("lookupsettings.currentVersion").Equals("slovakia")) {
                        return true;
                    }
                }
                return false;
            }
        }

        public XmlDocument GetReport(
            string template, int ciid, string ads_ids, int nAgreementID, ref string xslToUse, ref string xslToSend) {
            //template = "MTCreditReport";
            XmlElement reportTemplate = GetReportTemplate(template);
            if (reportTemplate != null) {
                reportType = int.Parse(reportTemplate.Attributes["Type"].InnerText);
                if (reportType > -1) {
                    string reportTitleKey = reportTemplate.Attributes["TitleKey"].InnerText;
                    xslToUse = reportTemplate.Attributes["XslFile"].InnerText;
                    xslToSend = reportTemplate.Attributes["XslSendFile"].InnerText;
                    XmlDocument doc = XmlHelper.CreateDocument();
                    XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
                    for (int i = 0; i < reportTemplate.ChildNodes.Count; i++) {
                        XmlElement el = GetReportSection(
                            doc,
                            template,
                            reportTitleKey,
                            reportTemplate.ChildNodes[i].Name,
                            ciid,
                            ads_ids,
                            nAgreementID);
                        if (el != null) {
                            rootElement.AppendChild(el);
                        }
                    }
                    doc.AppendChild(rootElement);
                    logUsage(ciid);
                    return doc;
                }
            }
            return null;
        }

        protected XmlElement GetReportSection(
            XmlDocument doc,
            string templateName,
            string reportTitleKey,
            string sectionName,
            int ciid,
            string afs_ids,
            int nAgreementID) {
            switch (sectionName) {
                case "Title":
                    return GetTitle(doc, reportTitleKey);
                case "Header":
                    return GetHeader(doc, ciid);
                case "CompanyHistory":
                    return GetCompanyHistory(doc, ciid);
                case "CompanyOperation":
                    return GetCompanyOperation(doc, ciid);
                case "CompanyReview":
                    return GetCompanyReview(doc, ciid);
                case "ExternalComments":
                    return GetExternalComments(doc, ciid);
                case "BoardMembers":
                    return GetBoardMembers(doc, ciid);
                case "LegalForm":
                    return GetLegalForm(doc, ciid);
                case "CustomerTypes":
                    return GetCustomerTypes(doc, ciid);
                case "ImportFrom":
                    return GetImportFrom(doc, ciid);
                case "ExportTo":
                    return GetExportTo(doc, ciid);
                case "BuyingTerms":
                    return GetBuyingTerms(doc, ciid);
                case "PaymentTerms":
                    return GetPaymentTerms(doc, ciid);
                case "Banks":
                    return GetBanks(doc, ciid);
                case "Employees":
                    return GetEmployees(doc, ciid);
                case "Lawyers":
                    return GetLawyers(doc, ciid);
                case "Auditors":
                    return GetAuditors(doc, ciid);
                case "Secretary":
                    return GetBoardSecretary(doc, ciid);
                case "NACE":
                    return GetNACECode(doc, ciid);
                case "CompanyState":
                    return GetCompanyState(doc, ciid);
                case "Shareholders":
                    return GetShareholders(doc, ciid);
                case "ShareholdersWithInvolvements":
                    return GetShareholdersWithInvolvements(doc, ciid);
                case "KeyEmployees":
                    return GetKeyEmployees(doc, ciid);
                case "KeyEmployeesWithInvolvements":
                    return GetKeyEmployeesWithInvolvements(doc, ciid);
                case "BoardMembersWithInvolvements":
                    return GetBoardMembersWithInvolvements(doc, ciid);
                case "RealEstates":
                    return GetRealEstates(doc, ciid);
                case "Subsidiaries":
                    return GetSubsidiaries(doc, ciid);
                case "Capital":
                    return GetCapital(doc, ciid);
                case "Charges":
                    return GetCharges(doc, ciid);
                case "CourtInfoAndDefaultingDebts":
                    return GetCourtInfoAndDefaultingDebts(doc, ciid);
                case "CreditScoring":
                    return GetCreditScoring(doc, ciid);
                case "MaximumCredit":
                    return GetMaximumCredit(doc, ciid, afs_ids);
                case "HeadFSI":
                    return GetHeadFSI(doc, ciid, afs_ids);
                case "BalanceSheet":
                    return GetBalanceSheet(doc, ciid, afs_ids);
                case "ProfitLossAccount":
                    return GetProfitLossAccount(doc, ciid, afs_ids);
                case "TemplateProfitLossAccount":
                    return GetTemplateProfitLossAccount(doc, ciid, afs_ids);
                case "TemplateRatios":
                    return GetTemplateRatios(doc, ciid, afs_ids);
                case "TemplateDefinitionOfRatios":
                    return GetTemplateDefinitionOfRatios(doc, afs_ids);
                case "Ratios":
                    return GetTemplateRatios(doc, ciid, afs_ids);
                case "CashFlow":
                    return GetCashFlow(doc, ciid, afs_ids);
                case "CashFlowShort":
                    return GetCashFlowShort(doc, ciid, afs_ids);
                case "DefinitionOfRatios":
                    return GetDefinitionOfRatios(doc, afs_ids);
                case "Footer":
                    return GetFooter(doc);
                case "RecentEnquiries":
                    return GetRecentEnquiries(doc, ciid);
                case "Audited":
                    return GetAudited(doc, ciid, afs_ids);

                    //CZ
                case "CZBusinessRegistry":
                    return CZGetBusinessRegistryText(doc, ciid);
                case "CZCompanyStatus":
                    return CZGetCompanyStatusDescription(doc, ciid);

                    //CY
                case "CYFormerNames":
                    return CYGetFormerNames(doc, ciid);

                    // Biling
                case "BillingHeader":
                    return GetBillingHeader(doc, ciid);
                case "AgreementNumber":
                    return GetBillingAgreementNumber(doc, ciid, nAgreementID);
                case "AgreementHeader":
                    return GetBillingAgreementHeader(doc, ciid, nAgreementID);
                case "AgreementItems":
                    return GetBillingAgreementItems(doc, ciid, nAgreementID);
                case "AgreementDescription":
                    return GetBillingAgreementDescription(doc, ciid, nAgreementID);
                case "AgreementComments":
                    return GetBillingAgreementComments(doc, ciid, nAgreementID);
                case "AgreementFooter":
                    return GetBillingAgreementFooter(doc, ciid, nAgreementID);
            }
            return null;
        }

        protected XmlElement GetReportTemplate(string templateName) {
            XmlDocument doc = new XmlDocument();
            doc.Load(new XmlTextReader(CigConfig.Configure("lookupsettings.rootURL") + "/CR/ReportConfig.xml"));
            XmlNodeList templateList = doc.GetElementsByTagName("Report");
            if (templateList.Count == 0) {
                Logger.WriteToLog("CR Report node not found", true);
                return null;
            } else {
                for (int i = 0; i < templateList.Count; i++) {
                    if (templateList[i].Attributes["Name"].InnerText == templateName) {
                        return (XmlElement) templateList[i];
                    }
                }
            }
            Logger.WriteToLog("CRTemplate - template \\" + templateName + "\\ not found in config", true);
            return null;
        }

        protected XmlElement GetTitle(XmlDocument doc, string sectionName) {
            XmlElement title = XmlHelper.GetXmlTable(doc, "Title", "tblTitle");
            XmlHelper.AddAttribute(doc, title, "title", rm.GetString("txt" + sectionName, ci));
            XmlHelper.AddAttribute(doc, title, "imageUrl", CigConfig.Configure("lookupsettings.reportImageFullUrl"));
            return title;
        }

        protected XmlElement GetFooter(XmlDocument doc) {
            XmlElement footer = XmlHelper.GetXmlTable(doc, "Footer", "tblFooter");
            XmlHelper.AddAttribute(doc, footer, "EndOfReport", rm.GetString("txtEndOfReport", ci));
            XmlHelper.AddAttribute(doc, footer, "Disclaimer", rm.GetString("txtReportDisclaimer", ci));
            return footer;
        }

        protected XmlElement GetHeader(XmlDocument doc, int companyCIID) {
            XmlElement header = XmlHelper.GetXmlTable(doc, "Header", "tblHeader");
            XmlHelper.AddAttribute(doc, header, "title", rm.GetString("txtBasicInfoHeader", ci));

            ReportCompanyBLLC theCompany = factory.GetCompanyReport("", companyCIID, false);
            if (theCompany != null) {
                string workPhone = "";
                string fax = "";
                if (theCompany.PNumbers != null) {
                    foreach (PhoneNumber myNumber in theCompany.PNumbers) {
                        if (CigConfig.Configure("lookupsettings.workCode") == Convert.ToString(myNumber.NumberTypeID)) {
                            workPhone = myNumber.Number;
                        } else if (CigConfig.Configure("lookupsettings.faxCode") == Convert.ToString(myNumber.NumberTypeID)) {
                            fax = myNumber.Number;
                        }
                    }
                }

                if (nativeCult) {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc, "Name", "title", rm.GetString("txtName", ci), "value", theCompany.NameNative));
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "RegistrationID",
                            "title",
                            rm.GetString("txtRegistrationID", ci),
                            "value",
                            theCompany.UniqueID));
                    if (theCompany.Address.Count > 0) {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc,
                                "Address",
                                "title",
                                rm.GetString("txtAddress", ci),
                                "value",
                                ((Address) theCompany.Address[0]).StreetNative));
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "Phone", "title", rm.GetString("txtPhone", ci), "value", workPhone));
                    }
                } else {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc, "Name", "title", rm.GetString("txtName", ci), "value", theCompany.NameEN));
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "RegistrationID",
                            "title",
                            rm.GetString("txtRegistrationID", ci),
                            "value",
                            theCompany.UniqueID));
                    if (theCompany.Address.Count > 0) {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc,
                                "Address",
                                "title",
                                rm.GetString("txtAddress", ci),
                                "value",
                                ((Address) theCompany.Address[0]).StreetEN));
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "Phone", "title", rm.GetString("txtPhone", ci), "value", workPhone));
                    }
                }

                string postalCode = "";
                if (theCompany.Address.Count > 0) {
                    // check for Czech version and them format the postal code from 00000 to 000 00
                    if (IsCzech || IsSlovak) {
                        postalCode = formatPostalCodeForCzech(((Address) theCompany.Address[0]).PostalCode);
                    } else {
                        postalCode = ((Address) theCompany.Address[0]).PostalCode;
                    }
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc, "PostalCode", "title", rm.GetString("txtPostalCode", ci), "value", postalCode));
                    header.AppendChild(
                        XmlHelper.CreateElement(doc, "Fax", "title", rm.GetString("txtFax", ci), "value", fax));
                }
                if (theCompany.Address.Count > 0) {
                    if (nativeCult) {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc,
                                "City",
                                "title",
                                rm.GetString("txtCity", ci),
                                "value",
                                ((Address) theCompany.Address[0]).CityNameNative));
                    } else {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc,
                                "City",
                                "title",
                                rm.GetString("txtCity", ci),
                                "value",
                                ((Address) theCompany.Address[0]).CityNameEN));
                    }
                }

                header.AppendChild(
                    XmlHelper.CreateElement(doc, "VAT", "title", rm.GetString("txtVAT", ci), "value", theCompany.VAT));

                if (theCompany.Address.Count > 0) {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "POBox",
                            "title",
                            rm.GetString("txtPOBox", ci),
                            "value",
                            ((Address) theCompany.Address[0]).PostBox));
                }

                if (theCompany.Established > DateTime.MinValue) {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "Founded",
                            "title",
                            rm.GetString("txtFounded", ci),
                            "value",
                            theCompany.Established.ToShortDateString()));
                } else {
                    header.AppendChild(
                        XmlHelper.CreateElement(doc, "Founded", "title", rm.GetString("txtFounded", ci), "value", " "));
                }

                if (theCompany.Registered > DateTime.MinValue) {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "Registered",
                            "title",
                            rm.GetString("txtRegDate", ci),
                            "value",
                            theCompany.Registered.ToShortDateString()));
                } else {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc, "Registered", "title", rm.GetString("txtRegDate", ci), "value", " "));
                }

                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "EnglishName", "title", rm.GetString("txtEnglishName", ci), "value", theCompany.NameEN));
                if (nativeCult) {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "FormerName",
                            "title",
                            rm.GetString("txtFormerlyKnownAs", ci),
                            "value",
                            theCompany.FormerNameNative));
                } else {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "FormerName",
                            "title",
                            rm.GetString("txtFormerlyKnownAs", ci),
                            "value",
                            theCompany.FormerNameEN));
                }

                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "NickName", "title", rm.GetString("txtNickName", ci), "value", theCompany.NameEN));
                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "HomePage", "title", rm.GetString("txtHomepage", ci), "value", theCompany.HomePage));

                if (theCompany.LastUpdated > DateTime.MinValue) {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "LastUpdate",
                            "title",
                            rm.GetString("txtLastUpdate", ci),
                            "value",
                            theCompany.LastUpdated.ToShortDateString()));
                } else {
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc, "LastUpdate", "title", rm.GetString("txtLastUpdate", ci), "value", " "));
                }

                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "Email", "title", rm.GetString("txtEmail", ci), "value", theCompany.Email));
            }
            return header;
        }

        protected XmlElement GetLegalForm(XmlDocument doc, int companyCIID) {
            XmlElement lf = XmlHelper.GetXmlTable(doc, "LegalForm", "tblLegalForm");
            XmlHelper.AddAttribute(doc, lf, "title", rm.GetString("txtCompanyLegalForm", ci));

            ReportCompanyBLLC theCompany = factory.GetCompanyReport("", companyCIID, false);
            if (theCompany != null) {
                lf.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "CompanyLegalForm",
                        "title",
                        rm.GetString("txtCompanyLegalForm", ci),
                        "value",
                        factory.GetRegistrationFormAsString(theCompany.RegistrationFormID, nativeCult)));
            }
            return lf;
        }

        private XmlElement CYGetFormerNames(XmlDocument doc, int companyCIID) {
            XmlElement cyprusFormerNames = XmlHelper.GetXmlTable(doc, "CyprusFormerNames", "tblLegalForm");
            XmlHelper.AddAttribute(doc, cyprusFormerNames, "title", rm.GetString("txtFormerlyKnownAs", ci));

            ReportCompanyBLLC comp = factory.GetCompanyReport("", companyCIID, false);
            if (comp != null && comp.UniqueID != null && comp.UniqueID.Trim() != "") {
                DataSet ds = factory.GetCyprusCompanyFormerNames(comp.UniqueID);
                if (ds.Tables[0].Rows.Count > 0) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        cyprusFormerNames.AppendChild(
                            XmlHelper.GetXmlTableItem(doc, "value", ds.Tables[0].Rows[i]["Org_name"].ToString()));
                    }
                }
            } else {
                cyprusFormerNames.AppendChild(
                    XmlHelper.GetXmlTableItem(doc, "value", rm.GetString("txtNoInformationAvailable", ci)));
            }
            return cyprusFormerNames;
        }

        protected XmlElement GetCompanyHistory(XmlDocument doc, int companyCIID) {
            XmlElement el = XmlHelper.GetXmlTable(doc, "CompanyHistory", "tblCompanyHistory");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtCompanyHistory", ci));
            HistoryOperationReviewBLLC theHist = factory.GetHistoryOperationReview(companyCIID);
            if (theHist != null) {
                string hist = "";
                if (nativeCult) {
                    if (theHist.HistoryNative != null && theHist.HistoryNative != "") {
                        hist = theHist.HistoryNative;
                    } else {
                        if (theHist.HistoryEN != null && theHist.HistoryEN != "") {
                            hist = theHist.HistoryEN;
                        } else {
                            return null;
                        }
                    }
                } else {
                    if (theHist.HistoryEN != null && theHist.HistoryEN != "") {
                        hist = theHist.HistoryEN;
                    } else {
                        if (theHist.HistoryNative != null && theHist.HistoryNative != "") {
                            hist = theHist.HistoryNative;
                        } else {
                            return null;
                        }
                    }
                }
                el.AppendChild(GetTextLines(doc, hist));
            }
            return el;
        }

        protected XmlElement GetExternalComments(XmlDocument doc, int companyCIID) {
            XmlElement el = XmlHelper.GetXmlTable(doc, "ExternalComments", "tblExternalComments");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtComments", ci));
            ReportCompanyBLLC comp = factory.GetCompanyReport("", companyCIID, false);
            if (comp != null) {
                string hist = "";
                if (nativeCult) {
                    if (comp.ExternalCommentNative != null && comp.ExternalCommentNative != "") {
                        hist = comp.ExternalCommentNative;
                    } else {
                        if (comp.ExternalCommentEN != null && comp.ExternalCommentEN != "") {
                            hist = comp.ExternalCommentEN;
                        } else {
                            return null;
                        }
                    }
                } else {
                    if (comp.ExternalCommentEN != null && comp.ExternalCommentEN != "") {
                        hist = comp.ExternalCommentEN;
                    } else {
                        if (comp.ExternalCommentNative != null && comp.ExternalCommentNative != "") {
                            hist = comp.ExternalCommentNative;
                        } else {
                            return null;
                        }
                    }
                }
                el.AppendChild(GetTextLines(doc, hist));
            }
            return el;
        }

        protected XmlElement GetCompanyOperation(XmlDocument doc, int companyCIID) {
            XmlElement el = XmlHelper.GetXmlTable(doc, "CompanyOperation", "tblCompanyOperation");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtCompanyOperation", ci));
            HistoryOperationReviewBLLC theHist = factory.GetHistoryOperationReview(companyCIID);
            if (theHist != null) {
                if ((theHist.OperationNative == null && theHist.OperationEN == null) ||
                    (theHist.OperationNative.Trim().Length == 0 && theHist.OperationEN.Trim().Length == 0)) {
                    return null;
                }
                string oper = "";
                if (nativeCult) {
                    if (theHist.OperationNative != null && theHist.OperationNative != "") {
                        oper = theHist.OperationNative;
                    } else {
                        oper = theHist.OperationEN;
                    }
                } else {
                    if (theHist.OperationEN != null && theHist.OperationEN != "") {
                        oper = theHist.OperationEN;
                    } else {
                        oper = theHist.OperationNative;
                    }
                }
                el.AppendChild(GetTextLines(doc, oper));
            }
            return el;
        }

        protected XmlElement GetCompanyReview(XmlDocument doc, int companyCIID) {
            XmlElement el = XmlHelper.GetXmlTable(doc, "CompanyReview", "tblNotes");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtNotes", ci));
            HistoryOperationReviewBLLC theHist = factory.GetHistoryOperationReview(companyCIID);
            if (theHist.CompanyReviewNative != null && theHist.CompanyReviewNative != "" &&
                theHist.CompanyReviewEN != null && theHist.CompanyReviewEN != "") {
                if (nativeCult) {
                    if (theHist.CompanyReviewNative != null && theHist.CompanyReviewNative != "") {
                        XmlHelper.AddAttribute(doc, el, "Value", theHist.CompanyReviewNative);
                    } else {
                        XmlHelper.AddAttribute(doc, el, "Value", theHist.CompanyReviewEN);
                    }
                } else {
                    if (theHist.CompanyReviewEN != null && theHist.CompanyReviewEN != "") {
                        XmlHelper.AddAttribute(doc, el, "Value", theHist.CompanyReviewEN);
                    } else {
                        XmlHelper.AddAttribute(doc, el, "Value", theHist.CompanyReviewNative);
                    }
                }
            } else {
                el = null;
            }

            return el;
        }

        protected XmlElement CZGetCompanyStatusDescription(XmlDocument doc, int companyCIID) {
            XmlElement el = XmlHelper.GetXmlTable(doc, "CZCompanyStatus", "tblCZCompanyStatus");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtCompanyStatusDescription", ci));
            HistoryOperationReviewBLLC theHist = factory.GetHistoryOperationReview(companyCIID);
            if (theHist.StatusDescriptionNative != null || theHist.StatusDescriptionEN != null) {
                string stText = "";
                if (nativeCult) {
                    if (theHist.StatusDescriptionNative != null && theHist.StatusDescriptionNative != "") {
                        stText = theHist.StatusDescriptionNative;
                    } else {
                        stText = theHist.StatusDescriptionEN;
                    }
                } else {
                    if (theHist.StatusDescriptionEN != null && theHist.StatusDescriptionEN != "") {
                        stText = theHist.StatusDescriptionEN;
                    } else {
                        stText = theHist.StatusDescriptionNative;
                    }
                }
                el.AppendChild(GetTextLines(doc, stText));
            } else {
                el = null;
            }
            return el;
        }

        protected XmlElement CZGetBusinessRegistryText(XmlDocument doc, int companyCIID) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "CZBusinessRegistry", "tblCompanyBusinessRegistryText");
            XmlHelper.AddAttribute(doc, table, "Title", rm.GetString("txtBusinessRegistryText", ci));

            ReportCompanyBLLC theCompany = factory.GetCompanyReport("", companyCIID, false);
            if (theCompany.UniqueID != null && theCompany.UniqueID.Trim() != "") {
                if (CZAddSecondLinkToBusinessRegistry(theCompany.RegistrationFormID)) {
                    if (CigConfig.Configure("lookupsettings.CZBusinessRegistryLink") != null) {
                        string sLink = CigConfig.Configure("lookupsettings.CZBusinessRegistryLink");
                        sLink = sLink.Replace("%NationalID%", theCompany.UniqueID);
                        XmlElement secondLink = XmlHelper.CreateElement(doc, "SecondLink", "");
                        table.AppendChild(secondLink);
                        XmlElement pLink = XmlHelper.CreateElement(doc, "PopupLink", "");
                        secondLink.AppendChild(pLink);
                        XmlHelper.AddAttribute(doc, pLink, "Title", rm.GetString("txtBusinessRegistryLink", ci));
                        XmlHelper.AddAttribute(doc, pLink, "Link", sLink);
                    }
                }

                if (CigConfig.Configure("lookupsettings.CZBusinessRegistryAresLink") != null) {
                    string sLink = CigConfig.Configure("lookupsettings.CZBusinessRegistryAresLink");
                    sLink = sLink.Replace("%NationalID%", theCompany.UniqueID);
                    XmlElement link = XmlHelper.CreateElement(doc, "FirstLink", "");
                    table.AppendChild(link);
                    XmlElement pLink = XmlHelper.CreateElement(doc, "PopupLink", "");
                    link.AppendChild(pLink);
                    XmlHelper.AddAttribute(doc, pLink, "Title", rm.GetString("txtBusinessRegistryAresLink", ci));
                    XmlHelper.AddAttribute(doc, pLink, "Link", sLink);
                }
            }

            HistoryOperationReviewBLLC theHist = factory.GetHistoryOperationReview(companyCIID);
            string brText = "";
            if (theHist != null) {
                if (nativeCult) {
                    if (theHist.BusinessRegistryTextNative != null && theHist.BusinessRegistryTextNative != "") {
                        brText = theHist.BusinessRegistryTextNative;
                    } else {
                        brText = theHist.BusinessRegistryTextEN;
                    }
                } else {
                    if (theHist.BusinessRegistryTextEN != null && theHist.BusinessRegistryTextEN != "") {
                        brText = theHist.BusinessRegistryTextEN;
                    } else {
                        brText = theHist.BusinessRegistryTextNative;
                    }
                }
            }

            table.AppendChild(GetTextLines(doc, brText));
            return table;
        }

        protected XmlElement GetTextLines(XmlDocument doc, string text) {
            XmlElement textEl = XmlHelper.CreateElement(doc, "Text", "");
            if (text == null) {
                return textEl;
            }
            string[] arrHist;

            if (text.IndexOf('\r') > -1) {
                arrHist = text.Split('\r');
            } else if (text.IndexOf('\n') > -1) {
                arrHist = text.Split('\n');
            } else if (text.IndexOf("|") > -1) {
                arrHist = text.Split('|');
            } else {
                arrHist = new string[1];
                arrHist[0] = text;
            }
            for (int i = 0; i < arrHist.Length; i++) {
                string splitText = arrHist[i].Trim();
                if (splitText != "") // avoid czech "bad" data
                {
                    splitText = splitText.Trim(new[] {'\r', '\n'});
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "Value", splitText);
                    textEl.AppendChild(item);
                }
            }
            return textEl;
        }

        protected bool CZAddSecondLinkToBusinessRegistry(int legalFormID) {
            int lfCode = int.Parse(factory.GetLegalFormCode(legalFormID));
            if (lfCode > -1) {
                ArrayList lfIDS = new ArrayList();
                lfIDS.AddRange(new[] {102, 104, 106, 111, 112, 113, 115, 117, 118, 121, 141, 145, 205, 301, 331, 421});
                if (lfIDS.Contains(lfCode)) {
                    return true;
                }
            }
            return false;
        }

        private XmlElement GetMaximumCredit(XmlDocument doc, int companyCIID, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            XmlElement maxCred = XmlHelper.GetXmlTable(doc, "MaximumCredit", "tblMaxCredit");
            XmlHelper.AddAttribute(doc, maxCred, "Title", rm.GetString("txtMaximumCredit", ci));

            FISMaxCreditValues MaxCreditValues = new FISMaxCreditValues();

            FinancialStatementFactory fsiFactory = new FinancialStatementFactory();

            string latest = "";
            string[] allAfs;
            double maxValue = -1;
            long equityTotal = -1;
            DateTime current = DateTime.Now;
            decimal creditScore = factory.GetCreditScoring(companyCIID, 1, DateTime.Now);
            int scoreCategory = factory.GetScoreCategory(creditScore, 1);
            int denomination_id = -1;
            int denominationMultiple = -1;

            string fsYear = "";
            if (afs_ids != "") {
                allAfs = afs_ids.Split(',');
                latest = allAfs[0]; //.ToString();
                MaxCreditValues = fsiFactory.GetMaxCreditValues(int.Parse(latest));
                fsYear = MaxCreditValues.FsYear; //fsiFactory.GetFinancialStatementYear(int.Parse(latest));
                // need to check if the company has DDD registrations
                DataSet ds = factory.GetCourtDecisionsAndDefaultingDebts(companyCIID);
                string strMaxValue = "";
                if (ds.Tables[0].Rows.Count > 0) {
                    XmlHelper.AddAttribute(doc, maxCred, "Value", rm.GetString("txtSecureCredit", ci) + " ");
                    return maxCred;
                }
                if (fsYear != null && fsYear != "" && latest != null && latest != "") {
                    if ((current.Year - Convert.ToInt32(fsYear)) > 2) {
                        XmlHelper.AddAttribute(doc, maxCred, "Value", rm.GetString("txtSecureCredit", ci) + " ");
                    } else {
                        // reikna h�r st�ffi�
                        equityTotal = MaxCreditValues.EquityTotal;
                        denomination_id = MaxCreditValues.DenominationID;
                        // �arf a� s�kja vi�eigandi multiple stu�ul �r config (nota� til a� forma amx credit result)
                        denominationMultiple = GetDenominationMultipleValue(denomination_id);
                        maxValue = GetMaximumAmount(scoreCategory, equityTotal, denominationMultiple);
                            // denominationMultiple is harcoded to 1, for now ... 
                        NumberFormatInfo format =
                            CultureInfo.CreateSpecificCulture(CigConfig.Configure("lookupsettings.nativeCulture")).
                                NumberFormat; // use default culture for formatting
                        format.CurrencyDecimalDigits = 0;
                        strMaxValue = maxValue.ToString("C", format);

                        if (maxValue <= 0) {
                            XmlHelper.AddAttribute(doc, maxCred, "Value", rm.GetString("txtSecureCredit", ci) + " ");
                        } else {
                            XmlHelper.AddAttribute(
                                doc, maxCred, "Value", rm.GetString("txtMaxCreditRecommended", ci) + strMaxValue);
                        }
                    }
                } else {
                    XmlHelper.AddAttribute(doc, maxCred, "Value", rm.GetString("txtSecureCredit", ci) + " ");
                }
            } else {
                XmlHelper.AddAttribute(doc, maxCred, "Value", rm.GetString("txtSecureCredit", ci) + " ");
            }

            return maxCred;
        }

        /// <summary>
        /// Reiknar �t fr� flokkunum Harald Moen. Forsendur eru �essar:
        /// Flokkar 1, 2, 3 & 4 == 20% af eigin f�
        /// Flokkar 5 og 6 == 15% af eigin f�
        /// Flokkar 7 og 8 == 10% af eigin f�
        /// Flokkar 9 og 10 v�ri s��an default �.e. "We recommend secure credit"
        /// </summary>
        /// <returns></returns>
        public double GetMaximumAmount(int scoreCategory, long equityTotal, int multiplyValue) {
            double maxAmount = 0;
            if ((scoreCategory == 1) || (scoreCategory == 2) || (scoreCategory == 3) || (scoreCategory == 4)) {
                maxAmount = (0.2*equityTotal)*multiplyValue;
            } else if ((scoreCategory == 5) || (scoreCategory == 6)) {
                maxAmount = (0.15*equityTotal)*multiplyValue;
            } else if ((scoreCategory == 7) || (scoreCategory == 8)) {
                maxAmount = (0.1*equityTotal)*multiplyValue;
            } else {
                maxAmount = 0;
            }

            return maxAmount;
        }

        /// <summary>
        /// Gets the "right" value to use for formatting the max credit calculation
        /// </summary>
        /// <param name="denomination_id"></param>
        /// <returns></returns>
        private int GetDenominationMultipleValue(int denomination_id) {
            int multipleWith = -1;
            if (CigConfig.Configure("lookupsettings.denominationValue1") != null ||
                CigConfig.Configure("lookupsettings.denominationValue2") != null ||
                CigConfig.Configure("lookupsettings.denominationValue3") != null ||
                CigConfig.Configure("lookupsettings.denominationValue4") != null) {
                switch (denomination_id) {
                    case 1:
                        multipleWith = Convert.ToInt32(CigConfig.Configure("lookupsettings.denominationValue1"));
                        return multipleWith;
                    case 2:
                        multipleWith = Convert.ToInt32(CigConfig.Configure("lookupsettings.denominationValue2"));
                        return multipleWith;
                    case 3:
                        multipleWith = Convert.ToInt32(CigConfig.Configure("lookupsettings.denominationValue3"));
                        return multipleWith;
                    case 4:
                        multipleWith = Convert.ToInt32(CigConfig.Configure("lookupsettings.denominationValue4"));
                        return multipleWith;
                    default:
                        return 1; // multiply with 1 if failes
                }
            }
            return 1;
        }

        private XmlElement GetCustomerTypes(XmlDocument doc, int companyCIID) {
            XmlElement customerTypes = XmlHelper.GetXmlTable(doc, "CustomerTypes", "tblCustomerTypes");
            XmlHelper.AddAttribute(doc, customerTypes, "title", rm.GetString("txtCustomerType", ci));

            DataSet ds = factory.GetCustomerTypeAsDataSet(companyCIID);
            if (ds.Tables.Count > 0) {
                if (ds.Tables[0].Rows.Count > 0) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        string data = ds.Tables[0].Rows[i]["CustomerType" + cultureEnding].ToString();
                        XmlElement item = XmlHelper.GetXmlTableItem(doc);
                        XmlHelper.AddAttribute(doc, item, "value", data);
                        customerTypes.AppendChild(item);
                    }
                } else {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                    customerTypes.AppendChild(item);
                }
            }
            return customerTypes;
        }

        private XmlElement GetImportFrom(XmlDocument doc, int companyCIID) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "ImportFrom", "tblImportFrom");
            XmlHelper.AddAttribute(doc, table, "title", rm.GetString("txtImportFrom", ci));

            DataSet ds = factory.GetImportFromTypeAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string data = ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString();
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "value", data);
                    table.AppendChild(item);
                }
            } else {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                table.AppendChild(item);
            }
            return table;
        }

        private XmlElement GetExportTo(XmlDocument doc, int companyCIID) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "ExportTo", "tblExportTo");
            XmlHelper.AddAttribute(doc, table, "title", rm.GetString("txtExportTo", ci));

            DataSet ds = factory.GetExportToAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string data = ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString();
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "value", data);
                    table.AppendChild(item);
                }
            } else {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                table.AppendChild(item);
            }
            return table;
        }

        private XmlElement GetPaymentTerms(XmlDocument doc, int companyCIID) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "PaymentTerms", "tblPaymentTerms");
            XmlHelper.AddAttribute(doc, table, "title", rm.GetString("txtPaymentTerms", ci));

            DataSet ds = factory.GetTradeTermsAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string show = ds.Tables[0].Rows[i]["SalesTerm"].ToString();
                    if (show.Equals("False")) {
                        string data = ds.Tables[0].Rows[i]["TermsDescription" + cultureEnding].ToString();
                        XmlElement item = XmlHelper.GetXmlTableItem(doc);
                        XmlHelper.AddAttribute(doc, item, "value", data);
                        table.AppendChild(item);
                    }
                }
            } else {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                table.AppendChild(item);
            }
            return table;
        }

        private XmlElement GetBuyingTerms(XmlDocument doc, int companyCIID) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "BuyingTerms", "tblBuyingTerms");
            XmlHelper.AddAttribute(doc, table, "title", rm.GetString("txtBuyingTerms", ci));

            DataSet ds = factory.GetTradeTermsAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string show = ds.Tables[0].Rows[i]["SalesTerm"].ToString();
                    if (show.Equals("True")) {
                        string data = ds.Tables[0].Rows[i]["TermsDescription" + cultureEnding].ToString();
                        XmlElement item = XmlHelper.GetXmlTableItem(doc);
                        XmlHelper.AddAttribute(doc, item, "value", data);
                        table.AppendChild(item);
                    }
                }
            } else {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                table.AppendChild(item);
            }
            return table;
        }

        private XmlElement GetBanks(XmlDocument doc, int companyCIID) {
            XmlElement banks = XmlHelper.GetXmlTable(doc, "Banks", "tblBanks");
            XmlHelper.AddAttribute(doc, banks, "title", rm.GetString("txtTradeBanks", ci));

            DataSet ds = factory.GetBanksAsDataset(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string data = ds.Tables[0].Rows[i]["Name" + cultureEnding] + ", " + ds.Tables[0].Rows[i]["Location"] +
                                  ", " + ds.Tables[0].Rows[i]["Postalcode"] + ", " +
                                  ds.Tables[0].Rows[i]["Comment" + cultureEnding];
                    string strippedData = data.Replace(", 0, 0, ", ""); // if no address etc. info the skip that part

                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "bank", rm.GetString("txtBank", ci));
                    XmlHelper.AddAttribute(doc, item, "value", strippedData);
                    banks.AppendChild(item);
                }
            } else {
                banks.AppendChild(
                    XmlHelper.GetXmlTableItem(doc, "value", rm.GetString("txtNoInformationAvailable", ci)));
            }
            return banks;
        }

        private XmlElement GetEmployees(XmlDocument doc, int companyCIID) {
            XmlElement employees = XmlHelper.GetXmlTable(doc, "Employees", "tblEmployees");
            XmlHelper.AddAttribute(doc, employees, "title", rm.GetString("txtEmployees", ci));

            DataSet ds = factory.GetEmployeesCountAsDataSet(companyCIID);
            if (ds.Tables.Count > 0) {
                if (ds.Tables[0].Rows.Count > 0) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        if (i >= 5) {
                            break;
                        }
                        string data = ds.Tables[0].Rows[i]["Year"] + " - " + ds.Tables[0].Rows[i]["Count"];
                        XmlElement item = XmlHelper.GetXmlTableItem(doc);
                        XmlHelper.AddAttribute(doc, item, "value", data);
                        employees.AppendChild(item);
                    }
                } else {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                    employees.AppendChild(item);
                }
            }
            return employees;
        }

        private XmlElement GetBoardMembers(XmlDocument doc, int companyCIID) {
            XmlElement board = XmlHelper.GetXmlTable(doc, "BoardMembers", "tblBoardMembers");
            XmlHelper.AddAttribute(doc, board, "title", rm.GetString("txtBoardMembers", ci));

            XmlElement th = XmlHelper.GetXmlTableHeader(doc);
            XmlHelper.AddAttribute(doc, th, "Board", rm.GetString("txtBoard", ci));
            XmlHelper.AddAttribute(doc, th, "RegistrationID", rm.GetString("txtRegistrationID", ci));
            XmlHelper.AddAttribute(doc, th, "Position", rm.GetString("txtPosition", ci));
            //XmlHelper.AddAttribute(doc, th, "Education", rm.GetString("txtEducation",ci));

            board.AppendChild(th);

            bool displayDDDMarksExplain = false;

            DataSet ds = factory.GetBoardAsDataset(companyCIID);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    if (factory.IsIDInDebitorsDatabase(int.Parse(ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString()))) {
                        XmlHelper.AddAttribute(doc, item, "DDDMark", "1");
                        displayDDDMarksExplain = true;
                    } else {
                        XmlHelper.AddAttribute(doc, item, "DDDMark", "");
                    }

                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "Board",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "Board",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "Board",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "Board",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        }
                    }
                    if (ds.Tables[0].Rows[i]["number"].ToString() == "") {
                        XmlHelper.AddAttribute(doc, item, "RegistrationID", ds.Tables[0].Rows[i]["numberC"].ToString());
                    } else {
                        XmlHelper.AddAttribute(doc, item, "RegistrationID", ds.Tables[0].Rows[i]["number"].ToString());
                    }
                    //XmlHelper.AddAttribute(doc, item, "Education", ds.Tables[0].Rows[i]["Education"+cultureEnding].ToString()); 
                    XmlHelper.AddAttribute(
                        doc, item, "Position", ds.Tables[0].Rows[i]["Title" + cultureEnding].ToString());
                    board.AppendChild(item);
                }
            }

            if (displayDDDMarksExplain) {
                XmlElement footer = XmlHelper.GetXmlTableFooter(doc);
                XmlHelper.AddAttribute(
                    doc, footer, "DDDMark1", "1: " + rm.GetString("txtIndividualRegisteredInDDD", ci));
                XmlHelper.AddAttribute(
                    doc, footer, "DDDMark2", "2: " + rm.GetString("txtIndividualHasRelationsToBankrupt", ci));
                XmlHelper.AddAttribute(doc, footer, "DDDMark3", "3: " + rm.GetString("txtBoth", ci));
                board.AppendChild(footer);
            }
            return board;
        }

        private XmlElement GetSubsidiaries(XmlDocument doc, int companyCIID) {
            XmlElement subs = XmlHelper.GetXmlTable(doc, "Subsidiaries", "tblSubsidiaries");
            XmlHelper.AddAttribute(doc, subs, "title", rm.GetString("txtInvestmentsAndCompanyRelations", ci));

            XmlElement th = XmlHelper.GetXmlTableHeader(doc);
            XmlHelper.AddAttribute(doc, th, "CompanyName", rm.GetString("txtCompanyName", ci));
            XmlHelper.AddAttribute(doc, th, "RegistrationID", rm.GetString("txtRegistrationID", ci));
            XmlHelper.AddAttribute(doc, th, "Ownership", rm.GetString("txtOwnership", ci));
            XmlHelper.AddAttribute(doc, th, "FormOfOperation", rm.GetString("txtFormOfOperation", ci));
            XmlHelper.AddAttribute(doc, th, "Status", rm.GetString("txtStatus", ci));

            subs.AppendChild(th);

            DataSet ds = factory.GetInvestmentsAsDataset(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                bool displayDDDMarksExplain = false;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);

                    //Check if in DDD - only display explain of DDD marks if someone is in the DDD
                    if (factory.IsIDInDebitorsDatabase(int.Parse(ds.Tables[0].Rows[i]["CreditInfoID"].ToString()))) {
                        XmlHelper.AddAttribute(doc, item, "DDDMark", "1");
                        displayDDDMarksExplain = true;
                    }

                    XmlHelper.AddAttribute(
                        doc, item, "CompanyName", ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString());
                    XmlHelper.AddAttribute(
                        doc, item, "RegistrationID", ds.Tables[0].Rows[i]["RegistrationID"].ToString());
                    string ownerShip = ds.Tables[0].Rows[i]["Ownership"].ToString();
                    if (ownerShip == "" || ownerShip == "0") {
                        ownerShip = "N/A";
                    }
                    XmlHelper.AddAttribute(doc, item, "Ownership", ownerShip);

                    //Get NACE Codes and display as comma separated
                    DataSet naceDS =
                        factory.GetNACECodesAsDataSet(int.Parse(ds.Tables[0].Rows[i]["CreditInfoID"].ToString()));
                    string nace = "";
                    if (naceDS.Tables.Count > 0) {
                        for (int j = 0; j < naceDS.Tables[0].Rows.Count; j++) {
                            nace += naceDS.Tables[0].Rows[j]["Description" + cultureEnding] + ", ";
                        }
                    }
                    nace = nace.Trim();
                    if (nace.EndsWith(",")) {
                        nace = nace.Remove(nace.Length - 1, 1);
                    }
                    XmlHelper.AddAttribute(doc, item, "FormOfOperation", nace);
                    XmlHelper.AddAttribute(
                        doc, item, "Status", ds.Tables[0].Rows[i]["Status" + cultureEnding].ToString());

                    subs.AppendChild(item);

                    if (displayDDDMarksExplain) {
                        XmlElement footer = XmlHelper.GetXmlTableFooter(doc);
                        XmlHelper.AddAttribute(
                            doc, footer, "DDDMark1", "1: " + rm.GetString("txtCompanyRegisteredInDDD", ci));
                        subs.AppendChild(footer);
                    }
                }
            } else {
                XmlElement item = XmlHelper.CreateElement(doc, "NoInfo", "");
                XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInvestmentsOrCompanyRelations", ci));
                subs.AppendChild(item);
            }
            return subs;
        }

        private XmlElement GetCharges(XmlDocument doc, int companyCIID) {
            XmlElement charges = XmlHelper.GetXmlTable(doc, "Charges", "tblCharges");
            XmlHelper.AddAttribute(doc, charges, "title", rm.GetString("txtCharges", ci));

            DataSet ds = factory.GetCharges(companyCIID);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                XmlElement th = XmlHelper.GetXmlTableHeader(doc);
                XmlHelper.AddAttribute(doc, th, "DateRegistered", rm.GetString("txtDateRegistered", ci));
                XmlHelper.AddAttribute(doc, th, "DatePrepared", rm.GetString("txtDatePrepared", ci));
                XmlHelper.AddAttribute(doc, th, "Type", rm.GetString("txtType", ci));
                XmlHelper.AddAttribute(doc, th, "Description", rm.GetString("txtDescription", ci));
                XmlHelper.AddAttribute(doc, th, "Amount", rm.GetString("txtAmount", ci));
                XmlHelper.AddAttribute(doc, th, "Sequence", rm.GetString("txtSequence", ci));
                XmlHelper.AddAttribute(doc, th, "Beneficiary", rm.GetString("txtBeneficiary", ci));

                charges.AppendChild(th);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(
                        doc,
                        item,
                        "DateRegistered",
                        ((DateTime) ds.Tables[0].Rows[i]["DateRegistered"]).ToShortDateString());
                    XmlHelper.AddAttribute(
                        doc, item, "DatePrepared", ((DateTime) ds.Tables[0].Rows[i]["DatePrepared"]).ToShortDateString());

                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["DescriptionNative"].ToString() == "") {
                            XmlHelper.AddAttribute(doc, item, "Type", ds.Tables[0].Rows[i]["DescriptionEN"].ToString());
                        } else {
                            XmlHelper.AddAttribute(
                                doc, item, "Type", ds.Tables[0].Rows[i]["DescriptionNative"].ToString());
                        }

                        if (ds.Tables[0].Rows[i]["DescriptionFreeTextNative"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc, item, "Description", ds.Tables[0].Rows[i]["DescriptionFreeTextNative"].ToString());
                        } else {
                            XmlHelper.AddAttribute(
                                doc, item, "Description", ds.Tables[0].Rows[i]["DescriptionFreeTextEN"].ToString());
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["DescriptionEN"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc, item, "Type", ds.Tables[0].Rows[i]["DescriptionNative"].ToString());
                        } else {
                            XmlHelper.AddAttribute(doc, item, "Type", ds.Tables[0].Rows[i]["DescriptionEN"].ToString());
                        }

                        if (ds.Tables[0].Rows[i]["DescriptionFreeTextEN"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc, item, "Description", ds.Tables[0].Rows[i]["DescriptionFreeTextNative"].ToString());
                        } else {
                            XmlHelper.AddAttribute(
                                doc, item, "Description", ds.Tables[0].Rows[i]["DescriptionFreeTextEN"].ToString());
                        }
                    }

                    XmlHelper.AddAttribute(doc, item, "Amount", ds.Tables[0].Rows[i]["Amount"].ToString());
                    XmlHelper.AddAttribute(doc, item, "CurrencyCode", ds.Tables[0].Rows[i]["CurrencyCode"].ToString());
                    XmlHelper.AddAttribute(doc, item, "Sequence", ds.Tables[0].Rows[i]["Sequence"].ToString());

                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["NameNative"].ToString() == "") {
                            XmlHelper.AddAttribute(doc, item, "Beneficiary", ds.Tables[0].Rows[i]["NameEN"].ToString());
                        } else {
                            XmlHelper.AddAttribute(
                                doc, item, "Beneficiary", ds.Tables[0].Rows[i]["NameNative"].ToString());
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["NameEN"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc, item, "Beneficiary", ds.Tables[0].Rows[i]["NameNative"].ToString());
                        } else {
                            XmlHelper.AddAttribute(doc, item, "Beneficiary", ds.Tables[0].Rows[i]["NameEN"].ToString());
                        }
                    }
                    charges.AppendChild(item);
                }
            } else {
                XmlElement item = XmlHelper.CreateElement(doc, "NoInfo", "");
                XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                charges.AppendChild(item);
            }
            return charges;
        }

        private XmlElement GetCapital(XmlDocument doc, int companyCIID) {
            XmlElement capital = XmlHelper.GetXmlTable(doc, "Capital", "tblCapital");
            XmlHelper.AddAttribute(doc, capital, "title", rm.GetString("txtCapital", ci));

            DataSet ds = factory.GetCapital(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                NumberFormatInfo capitalFormat = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
                capitalFormat.PercentDecimalDigits = 2;
                capitalFormat.NumberDecimalDigits = 2;

                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "CurrencyCode",
                        "title",
                        "Currency",
                        "value",
                        ds.Tables[0].Rows[0]["CurrencyCode"].ToString()));

                try {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "Nominal",
                            "title",
                            rm.GetString("txtNominal", ci),
                            "value",
                            double.Parse(ds.Tables[0].Rows[0]["NominalNumberOfShares"].ToString()).ToString(
                                "N", capitalFormat)));
                } catch (Exception) {
                    capital.AppendChild(
                        XmlHelper.CreateElement(doc, "Nominal", "title", rm.GetString("txtNominal", ci), "value", "N/A"));
                }
                try {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "Issued",
                            "title",
                            rm.GetString("txtIssued", ci),
                            "value",
                            double.Parse(ds.Tables[0].Rows[0]["IssuedNumberOfShares"].ToString()).ToString(
                                "N", capitalFormat)));
                } catch (Exception) {
                    capital.AppendChild(
                        XmlHelper.CreateElement(doc, "Issued", "title", rm.GetString("txtIssued", ci), "value", "N/A"));
                }
                try {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "Asked",
                            "title",
                            rm.GetString("txtAsked", ci),
                            "value",
                            double.Parse(ds.Tables[0].Rows[0]["Asked"].ToString()).ToString("N", capitalFormat)));
                } catch (Exception) {
                    capital.AppendChild(
                        XmlHelper.CreateElement(doc, "Asked", "title", rm.GetString("txtAsked", ci), "value", "N/A"));
                }
                try {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "Authorized",
                            "title",
                            rm.GetString("txtAuthorized", ci),
                            "value",
                            double.Parse(ds.Tables[0].Rows[0]["AuthorizedCapital"].ToString()).ToString(
                                "N", capitalFormat)));
                } catch (Exception) {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc, "Authorized", "title", rm.GetString("txtAuthorized", ci), "value", "N/A"));
                }
                try {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "PaidUp",
                            "title",
                            rm.GetString("txtPaidUp", ci),
                            "value",
                            double.Parse(ds.Tables[0].Rows[0]["PaidUp"].ToString()).ToString("N", capitalFormat)));
                } catch (Exception) {
                    capital.AppendChild(
                        XmlHelper.CreateElement(doc, "PaidUp", "title", rm.GetString("txtPaidUp", ci), "value", "N/A"));
                }

                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "ShareDescription",
                        "title",
                        rm.GetString("txtShareDescription", ci),
                        "value",
                        ds.Tables[0].Rows[0]["SharesDescription"].ToString()));

                try {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "ShareClass",
                            "title",
                            rm.GetString("txtShareClass", ci),
                            "value",
                            double.Parse(ds.Tables[0].Rows[0]["ShareClass"].ToString()).ToString("N", capitalFormat)));
                } catch (Exception) {
                    //Column not in the dataset - ignore
                }
            } else {
                return null;
                //capital.AppendChild(XmlHelper.CreateElement(doc, "NoInfo", "title", "", "value", rm.GetString("txtNoInformationAvailable",ci)));
            }
            return capital;
        }

        private XmlElement GetBoardMembersWithInvolvements(XmlDocument doc, int companyCIID) {
            XmlElement board = XmlHelper.GetXmlTable(doc, "BoardMembersWithInvolvements", "tblBoardMembers");
            XmlHelper.AddAttribute(doc, board, "title", rm.GetString("txtBoard", ci));

            /*		XmlElement th = XmlHelper.GetXmlTableHeader(doc);
					XmlHelper.AddAttribute(doc, th, "Board", rm.GetString("txtBoard",ci));
					XmlHelper.AddAttribute(doc, th, "RegistrationID", rm.GetString("txtRegistrationID",ci));
					XmlHelper.AddAttribute(doc, th, "Position", rm.GetString("txtPosition",ci));
					//XmlHelper.AddAttribute(doc, th, "Education", rm.GetString("txtEducation",ci));*/

            //board.AppendChild(th);

            bool displayDDDMarksExplain = false;

            DataSet ds = factory.GetBoardAsDataset(companyCIID);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    if (factory.IsIDInDebitorsDatabase(int.Parse(ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString()))) {
                        XmlHelper.AddAttribute(doc, item, "DDDMark", "1");
                        displayDDDMarksExplain = true;
                    }
                    /*else
					{
						XmlHelper.AddAttribute(doc, item, "DDDMark", "");
					}*/

                    XmlHelper.AddAttribute(doc, item, "NameTitle", rm.GetString("txtName", ci));
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        }
                    }

                    XmlHelper.AddAttribute(doc, item, "RegistrationIDTitle", rm.GetString("txtRegistrationID", ci));
                    XmlHelper.AddAttribute(doc, item, "RegistrationIDValue", ds.Tables[0].Rows[i]["number"].ToString());
                    //XmlHelper.AddAttribute(doc, item, "Education", ds.Tables[0].Rows[i]["Education"+cultureEnding].ToString()); 
                    XmlHelper.AddAttribute(doc, item, "PositionTitle", rm.GetString("txtPosition", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "PositionValue", ds.Tables[0].Rows[i]["Title" + cultureEnding].ToString());

                    //Involvements

                    int ciid = int.Parse(ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString());
                    DataSet inv = factory.GetBoardMemberInvolvements(ciid);
                    if (inv != null && inv.Tables.Count > 0 && inv.Tables[0].Rows.Count > 0) {
                        XmlHelper.AddAttribute(doc, item, "AlsoInvolvedInTitle", rm.GetString("txtAlsoInvolvedIn", ci));
                        for (int j = 0; j < inv.Tables[0].Rows.Count; j++) {
                            if (int.Parse(inv.Tables[0].Rows[j]["CompanyCIID"].ToString()) == companyCIID) {
                                continue;
                            }
                            XmlElement invItem = XmlHelper.CreateElement(doc, "Involvements", "");
                            if (nativeCult) {
                                if (inv.Tables[0].Rows[j]["NameNative"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                }

                                if (inv.Tables[0].Rows[j]["TitleNative"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Title", inv.Tables[0].Rows[j]["TitleNative"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Title", inv.Tables[0].Rows[j]["TitleEN"].ToString());
                                }
                            } else {
                                if (inv.Tables[0].Rows[j]["NameEN"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                }

                                if (inv.Tables[0].Rows[j]["TitleEN"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Title", inv.Tables[0].Rows[j]["TitleEN"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Title", inv.Tables[0].Rows[j]["TitleNative"].ToString());
                                }
                            }
                            item.AppendChild(invItem);
                        }
                    }

                    board.AppendChild(item);
                }
            }

            if (displayDDDMarksExplain) {
                XmlElement footer = XmlHelper.GetXmlTableFooter(doc);
                XmlHelper.AddAttribute(
                    doc, footer, "DDDMark1", "1: " + rm.GetString("txtIndividualRegisteredInDDD", ci));
                XmlHelper.AddAttribute(
                    doc, footer, "DDDMark2", "2: " + rm.GetString("txtIndividualHasRelationsToBankrupt", ci));
                XmlHelper.AddAttribute(doc, footer, "DDDMark3", "3: " + rm.GetString("txtBoth", ci));
                board.AppendChild(footer);
            }
            return board;
        }

        private XmlElement GetLawyers(XmlDocument doc, int companyCIID) {
            XmlElement laywers = XmlHelper.GetXmlTable(doc, "Lawyers", "tblLawyers");
            XmlHelper.AddAttribute(doc, laywers, "title", rm.GetString("txtLaywers", ci));

            XmlElement th = XmlHelper.GetXmlTableHeader(doc);
            XmlHelper.AddAttribute(doc, th, "Lawyer", rm.GetString("txtLaywers", ci));
            XmlHelper.AddAttribute(doc, th, "RegistrationID", rm.GetString("txtRegistrationID", ci));
            XmlHelper.AddAttribute(doc, th, "Address", rm.GetString("txtAddress", ci));
            laywers.AppendChild(th);

            DataSet ds = factory.GetLaywersAsDataSet(companyCIID);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);

                    if (ds.Tables[0].Rows[i]["Type"].ToString().Trim() ==
                        CigConfig.Configure("lookupsettings.individualID")) {
                        XmlHelper.AddAttribute(doc, item, "Individual", "true");
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Lawyer",
                                    ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                            } else {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Lawyer",
                                    ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                    ds.Tables[0].Rows[i]["SurNameNative"]);
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Lawyer",
                                    ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                    ds.Tables[0].Rows[i]["SurNameNative"]);
                            } else {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Lawyer",
                                    ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                            }
                        }
                    } else {
                        XmlHelper.AddAttribute(doc, item, "Company", "true");
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["NameNative"].ToString() == "") {
                                XmlHelper.AddAttribute(doc, item, "Lawyer", ds.Tables[0].Rows[i]["NameEN"].ToString());
                            } else {
                                XmlHelper.AddAttribute(
                                    doc, item, "Lawyer", ds.Tables[0].Rows[i]["NameNative"].ToString());
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["NameEN"].ToString() == "") {
                                XmlHelper.AddAttribute(
                                    doc, item, "Lawyer", ds.Tables[0].Rows[i]["NameNative"].ToString());
                            } else {
                                XmlHelper.AddAttribute(doc, item, "Lawyer", ds.Tables[0].Rows[i]["NameEN"].ToString());
                            }
                        }
                    }
                    XmlHelper.AddAttribute(doc, item, "RegistrationID", ds.Tables[0].Rows[i]["Number"].ToString());
                    XmlHelper.AddAttribute(
                        doc, item, "Address", ds.Tables[0].Rows[i]["Street" + cultureEnding].ToString());
                    laywers.AppendChild(item);
                }
            } else {
                return null;
            }
            return laywers;
        }

        private XmlElement GetAuditors(XmlDocument doc, int companyCIID) {
            XmlElement auditors = XmlHelper.GetXmlTable(doc, "Auditors", "tblAuditors");
            XmlHelper.AddAttribute(doc, auditors, "title", rm.GetString("txtAuditors", ci));

            XmlElement th = XmlHelper.GetXmlTableHeader(doc);
            XmlHelper.AddAttribute(doc, th, "Auditor", rm.GetString("txtAuditors", ci));
            XmlHelper.AddAttribute(doc, th, "RegistrationID", rm.GetString("txtRegistrationID", ci));
            XmlHelper.AddAttribute(doc, th, "Address", rm.GetString("txtAddress", ci));
            auditors.AppendChild(th);

            DataSet ds = factory.GetAuditorsAsDataSet(companyCIID);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);

                    if (ds.Tables[0].Rows[i]["Type"].ToString().Trim() ==
                        CigConfig.Configure("lookupsettings.individualID")) {
                        XmlHelper.AddAttribute(doc, item, "Individual", "true");
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Auditor",
                                    ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                            } else {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Auditor",
                                    ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                    ds.Tables[0].Rows[i]["SurNameNative"]);
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Auditor",
                                    ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                    ds.Tables[0].Rows[i]["SurNameNative"]);
                            } else {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Auditor",
                                    ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                            }
                        }
                    } else {
                        XmlHelper.AddAttribute(doc, item, "Company", "true");
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["NameNative"].ToString() == "") {
                                XmlHelper.AddAttribute(doc, item, "Auditor", ds.Tables[0].Rows[i]["NameEN"].ToString());
                            } else {
                                XmlHelper.AddAttribute(
                                    doc, item, "Auditor", ds.Tables[0].Rows[i]["NameNative"].ToString());
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["NameEN"].ToString() == "") {
                                XmlHelper.AddAttribute(
                                    doc, item, "Auditor", ds.Tables[0].Rows[i]["NameNative"].ToString());
                            } else {
                                XmlHelper.AddAttribute(doc, item, "Auditor", ds.Tables[0].Rows[i]["NameEN"].ToString());
                            }
                        }
                    }
                    XmlHelper.AddAttribute(doc, item, "RegistrationID", ds.Tables[0].Rows[i]["Number"].ToString());
                    XmlHelper.AddAttribute(
                        doc, item, "Address", ds.Tables[0].Rows[i]["Street" + cultureEnding].ToString());
                    auditors.AppendChild(item);
                }
            } else {
                return null;
            }
            return auditors;
        }

        private XmlElement GetBoardSecretary(XmlDocument doc, int companyCIID) {
            XmlElement bsTable = XmlHelper.GetXmlTable(doc, "BoardSecretary", "tblBoardSecretary");
            XmlHelper.AddAttribute(doc, bsTable, "title", rm.GetString("txtBoardSecretary", ci));

            XmlElement th = XmlHelper.GetXmlTableHeader(doc);
            XmlHelper.AddAttribute(doc, th, "BoardSecretary", rm.GetString("txtBoardSecretary", ci));
            XmlHelper.AddAttribute(doc, th, "RegistrationID", rm.GetString("txtRegistrationID", ci));
            XmlHelper.AddAttribute(doc, th, "History", rm.GetString("txtHistory", ci));

            bsTable.AppendChild(th);

            BoardSecretaryCompanyBLLC bsc = factory.GetCompanyBoardSecretary(companyCIID);

            if (bsc == null) {
                BoardSecretaryBLLC bs = factory.GetBoardSecretary(companyCIID);
                if (bs == null || bs.FirstNameNative == null) {
                    XmlElement noInfo = XmlHelper.CreateElement(doc, "NoInfo", "");
                    XmlHelper.AddAttribute(doc, noInfo, "value", rm.GetString("txtNoInformationAvailable", ci));
                    bsTable.AppendChild(noInfo);
                    return bsTable;
                } else {
                    XmlHelper.AddAttribute(doc, bsTable, "Individual", "true");

                    XmlElement item = XmlHelper.GetXmlTableItem(doc);

                    if (nativeCult) {
                        if (bs.FirstNameNative != null && bs.FirstNameNative.Trim() != "") {
                            XmlHelper.AddAttribute(doc, item, "Name", bs.FirstNameNative + " " + bs.SurNameNative);
                        } else {
                            XmlHelper.AddAttribute(doc, item, "Name", bs.FirstNameEN + " " + bs.SurNameEN);
                        }
                    } else {
                        if (bs.FirstNameEN != null && bs.FirstNameEN.Trim() != "") {
                            XmlHelper.AddAttribute(doc, item, "Name", bs.FirstNameEN + " " + bs.SurNameEN);
                        } else {
                            XmlHelper.AddAttribute(doc, item, "Name", bs.FirstNameNative + " " + bs.SurNameNative);
                        }
                    }

                    XmlHelper.AddAttribute(doc, item, "RegistrationID", bs.NationalID);

                    if (nativeCult) {
                        XmlHelper.AddAttribute(doc, item, "History", bs.HistoryNative);
                    } else {
                        XmlHelper.AddAttribute(doc, item, "History", bs.HistoryEN);
                    }
                    bsTable.AppendChild(item);
                    return bsTable;
                }
            } else {
                XmlHelper.AddAttribute(doc, bsTable, "Company", "true");

                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                if (nativeCult) {
                    if (bsc.NameNative != null && bsc.NameNative.Trim() != "") {
                        XmlHelper.AddAttribute(doc, item, "Name", bsc.NameNative);
                    } else {
                        XmlHelper.AddAttribute(doc, item, "Name", bsc.NameEN);
                    }
                } else {
                    if (bsc.NameEN != null && bsc.NameEN.Trim() != "") {
                        XmlHelper.AddAttribute(doc, item, "Name", bsc.NameEN);
                    } else {
                        XmlHelper.AddAttribute(doc, item, "Name", bsc.NameNative);
                    }
                }

                XmlHelper.AddAttribute(doc, item, "RegistrationID", bsc.NationalID);
                if (nativeCult) {
                    XmlHelper.AddAttribute(doc, item, "History", bsc.HistoryNative);
                } else {
                    XmlHelper.AddAttribute(doc, item, "History", bsc.HistoryEN);
                }
                bsTable.AppendChild(item);
                return bsTable;
            }
        }

        private XmlElement GetNACECode(XmlDocument doc, int companyCIID) {
            XmlElement naceCode = XmlHelper.GetXmlTable(doc, "NACECode", "tblNACECode");
            XmlHelper.AddAttribute(doc, naceCode, "title", rm.GetString("txtNACECode", ci));

            DataSet ds = factory.GetNACECodesAsDataSet(companyCIID);
            if (ds.Tables.Count > 0) {
                if (ds.Tables[0].Rows.Count > 0) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        string data = ds.Tables[0].Rows[i]["NaceCodeID"] + ": " +
                                      ds.Tables[0].Rows[i]["Description" + cultureEnding];
                        string currentCode = ds.Tables[0].Rows[i]["CurrentCode"].ToString();
                        if (currentCode.Equals("False")) {
                            XmlElement formerCode = XmlHelper.CreateElement(doc, "FormerCode", "");
                            XmlHelper.AddAttribute(doc, formerCode, "Title", rm.GetString("txtPastNACECode", ci));
                            XmlHelper.AddAttribute(doc, formerCode, "Code", data);
                            naceCode.AppendChild(formerCode);
                        } else {
                            XmlElement item = XmlHelper.GetXmlTableItem(doc);
                            XmlHelper.AddAttribute(doc, item, "Code", data);
                            naceCode.AppendChild(item);
                        }
                    }
                } else {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                    naceCode.AppendChild(item);
                }
            }
            return naceCode;
        }

        private XmlElement GetCompanyState(XmlDocument doc, int companyCIID) {
            XmlElement cs = XmlHelper.GetXmlTable(doc, "CompanyState", "tblCompanyState");
            XmlHelper.AddAttribute(doc, cs, "title", rm.GetString("txtCompanyState", ci));

            string state = factory.GetMTCompanyStateBO(companyCIID);
            string checkNA = "N/A";
            if (state != null) {
                if (nativeCult) {
                    if (!checkNA.Equals(state)) {
                        cs.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "State", "title", rm.GetString("txtCompanyState", ci), "value", state));
                    } else {
                        /*XmlElement s = XmlHelper.CreateElement(doc, "State", "title", rm.GetString("txtCompanyState",ci), "value", rm.GetString("txtNoCompanyStateInfoAvailable",ci));
						XmlHelper.AddAttribute(doc, s, "NoInfo", "true");
						cs.AppendChild(s);*/
                        return null;
                    }
                } else {
                    if (!checkNA.Equals(state)) {
                        cs.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "State", "title", rm.GetString("txtCompanyState", ci), "value", state));
                    } else {
                        /*
						XmlElement s = XmlHelper.CreateElement(doc, "State", "title", rm.GetString("txtCompanyState",ci), "value", rm.GetString("txtNoCompanyStateInfoAvailable",ci));
						XmlHelper.AddAttribute(doc, s, "NoInfo", "true");
						cs.AppendChild(s);*/
                        return null;
                    }
                }
            } else {
                return null;
            }
            return cs;
        }

        private XmlElement GetShareholders(XmlDocument doc, int companyCIID) {
            XmlElement shareholders = XmlHelper.GetXmlTable(doc, "Shareholders", "tblShareholders");
            XmlHelper.AddAttribute(doc, shareholders, "title", rm.GetString("txtOwnersShareholders", ci));

            DataSet ds = new DataSet();
            if (CigConfig.Configure("lookupsettings.OwnershipTypes") != null) {
                ds = factory.GetShareHolderOwnerAsDataSet(
                    companyCIID, int.Parse(CigConfig.Configure("lookupsettings.OwnershipTypes")));
            } else {
                ds = factory.GetShareHolderOwnerAsDataSet(companyCIID);
            }
            if (ds.Tables[0].Rows.Count > 0) {
                XmlElement th = XmlHelper.GetXmlTableHeader(doc);
                XmlHelper.AddAttribute(doc, th, "Name", rm.GetString("txtName", ci));
                XmlHelper.AddAttribute(doc, th, "Number", rm.GetString("txtRegistrationID", ci));
                if (CigConfig.Configure("lookupsettings.OwnershipInPercentage") != null &&
                    CigConfig.Configure("lookupsettings.OwnershipInPercentage").ToLower() == "true") {
                    XmlHelper.AddAttribute(doc, th, "Ownership", rm.GetString("txtOwnershipPercentage", ci));
                } else {
                    XmlHelper.AddAttribute(doc, th, "Ownership", rm.GetString("txtOwnership", ci));
                }
                XmlHelper.AddAttribute(doc, th, "ShareClass", rm.GetString("txtShareClass", ci));

                shareholders.AppendChild(th);
                /*
				DataSet ds = new DataSet();
			
				if(CigConfig.Configure("lookupsettings.OwnershipTypes"] != null)
					ds = factory.GetShareHolderOwnerAsDataSet(companyCIID,int.Parse(CigConfig.Configure("lookupsettings.OwnershipTypes"].ToString()));
				else
					ds = factory.GetShareHolderOwnerAsDataSet(companyCIID);
				if(ds.Tables[0].Rows.Count > 0)
				{
				*/
                ArrayList arrCiids = new ArrayList();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    bool idFound = false;
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (((string) arrCiids[j]).Equals(creditInfoId)) {
                            idFound = true;
                            break;
                        }
                    }
                    if (!idFound) {
                        XmlElement item = XmlHelper.GetXmlTableItem(doc);

                        string userType = ds.Tables[0].Rows[i]["Type"].ToString().Trim();
                        if (userType.Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                            // if(ds.Tables[0].Rows[i]["FirstNameNative"].ToString()=="")	
                            if (ds.Tables[0].Rows[i]["FirstName" + cultureEnding].ToString() != "") {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Name",
                                    ds.Tables[0].Rows[i]["FirstName" + cultureEnding] + " " +
                                    ds.Tables[0].Rows[i]["SurName" + cultureEnding]);
                            } else // we should at least be safe with native values
                            {
                                XmlHelper.AddAttribute(
                                    doc,
                                    item,
                                    "Name",
                                    ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                    ds.Tables[0].Rows[i]["SurNameNative"]);
                            }

                            XmlHelper.AddAttribute(doc, item, "Individual", "true");
                        } else {
                            if (ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString() != "") {
                                XmlHelper.AddAttribute(
                                    doc, item, "Name", ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString());
                            } else // we ashould at least be safe with native values
                            {
                                XmlHelper.AddAttribute(doc, item, "Name", ds.Tables[0].Rows[i]["NameNative"].ToString());
                            }
                            XmlHelper.AddAttribute(doc, item, "Company", "true");
                            /**TODO: Vantar a� finna l�singu � starfsemi**/
                        }
                        XmlHelper.AddAttribute(doc, item, "Number", ds.Tables[0].Rows[i]["number"].ToString());

                        if (CigConfig.Configure("lookupsettings.OwnershipInPercentage") != null &&
                            CigConfig.Configure("lookupsettings.OwnershipInPercentage").ToLower() == "true") {
                            string ownerShip = ds.Tables[0].Rows[i]["ownership_perc"].ToString();
                            if (ownerShip == null || ownerShip == "" || ownerShip == "0") {
                                XmlHelper.AddAttribute(doc, item, "Ownership", "N/A");
                            } else {
                                decimal num = Convert.ToDecimal(ownerShip);
                                NumberFormatInfo fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
                                fInfo.NumberDecimalDigits = 2;
                                XmlHelper.AddAttribute(doc, item, "Ownership", num.ToString("N", fInfo) + "%");
                            }
                        } else {
                            string ownerShip = ds.Tables[0].Rows[i]["ownership"].ToString();
                            if (ownerShip == null || ownerShip == "" || ownerShip == "0") {
                                XmlHelper.AddAttribute(doc, item, "Ownership", "N/A");
                            } else {
                                XmlHelper.AddAttribute(doc, item, "Ownership", ownerShip);
                            }
                        }

                        XmlHelper.AddAttribute(
                            doc, item, "ShareClass", ds.Tables[0].Rows[i]["OwnershipDescription" + cultureEnding] + "");
                        shareholders.AppendChild(item);
                    }
                }
            } else {
                return null;
            }

            return shareholders;
        }

        private XmlElement GetShareholdersWithInvolvements(XmlDocument doc, int companyCIID) {
            XmlElement shareholders = XmlHelper.GetXmlTable(
                doc, "ShareholdersWithInvolvements", "tblShareholdersWithInvolvements");
            XmlHelper.AddAttribute(doc, shareholders, "title", rm.GetString("txtOwnersShareholders", ci));

            DataSet ds = factory.GetShareHolderOwnerAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                ArrayList arrCiids = new ArrayList();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    bool idFound = false;
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (((string) arrCiids[j]).Equals(creditInfoId)) {
                            idFound = true;
                            break;
                        }
                    }
                    if (!idFound) {
                        XmlElement item = XmlHelper.GetXmlTableItem(doc);
                        XmlHelper.AddAttribute(doc, item, "NameTitle", rm.GetString("txtName", ci));
                        string userType = ds.Tables[0].Rows[i]["Type"].ToString().Trim();
                        if (userType.Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstName" + cultureEnding] + " " +
                                ds.Tables[0].Rows[i]["SurName" + cultureEnding]);
                            XmlHelper.AddAttribute(doc, item, "Individual", "true");
                        } else {
                            XmlHelper.AddAttribute(
                                doc, item, "NameValue", ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString());
                            XmlHelper.AddAttribute(doc, item, "Company", "true");
                            /**TODO: Vantar a� finna l�singu � starfsemi**/
                        }
                        XmlHelper.AddAttribute(doc, item, "NumberTitle", rm.GetString("txtRegistrationID", ci));
                        XmlHelper.AddAttribute(doc, item, "NumberValue", ds.Tables[0].Rows[i]["number"].ToString());

                        if (CigConfig.Configure("lookupsettings.OwnershipInPercentage") != null &&
                            CigConfig.Configure("lookupsettings.OwnershipInPercentage").ToLower() == "true") {
                            XmlHelper.AddAttribute(
                                doc, item, "OwnershipTitle", rm.GetString("txtOwnershipSubsidiaries", ci));
                        } else {
                            XmlHelper.AddAttribute(doc, item, "OwnershipTitle", rm.GetString("txtOwnership", ci));
                        }

                        if (CigConfig.Configure("lookupsettings.OwnershipInPercentage") != null &&
                            CigConfig.Configure("lookupsettings.OwnershipInPercentage").ToLower() == "true") {
                            string ownerShip = ds.Tables[0].Rows[i]["ownership_perc"].ToString();
                            if (ownerShip == null || ownerShip == "" || ownerShip == "0") {
                                XmlHelper.AddAttribute(doc, item, "OwnershipValue", "N/A");
                            } else {
                                decimal num = Convert.ToDecimal(ownerShip);
                                NumberFormatInfo fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
                                fInfo.NumberDecimalDigits = 2;
                                XmlHelper.AddAttribute(doc, item, "OwnershipValue", num.ToString("N", fInfo) + "%");
                            }
                        } else {
                            string ownerShip = ds.Tables[0].Rows[i]["ownership"].ToString();
                            if (ownerShip == null || ownerShip == "" || ownerShip == "0") {
                                XmlHelper.AddAttribute(doc, item, "OwnershipValue", "N/A");
                            } else {
                                XmlHelper.AddAttribute(doc, item, "OwnershipValue", ownerShip);
                            }
                        }

                        XmlHelper.AddAttribute(doc, item, "ShareClassTitle", rm.GetString("txtShareClass", ci));
                        XmlHelper.AddAttribute(
                            doc,
                            item,
                            "ShareClassValue",
                            ds.Tables[0].Rows[i]["OwnershipDescription" + cultureEnding].ToString());

                        //Now we need all companys this ciid is shareholder in
                        int ciid = int.Parse(creditInfoId);
                        DataSet inv = factory.GetShareholderInvolvements(ciid);
                        if (inv != null && inv.Tables.Count > 0 && inv.Tables[0].Rows.Count > 0) {
                            XmlHelper.AddAttribute(
                                doc, item, "AlsoOwnsSharesInTitle", rm.GetString("txtAlsoOwnsSharesIn", ci));
                            for (int j = 0; j < inv.Tables[0].Rows.Count; j++) {
                                if (int.Parse(inv.Tables[0].Rows[j]["CompanyCIID"].ToString()) == companyCIID) {
                                    continue;
                                }
                                XmlElement invItem = XmlHelper.CreateElement(doc, "Involvements", "");
                                if (nativeCult) {
                                    if (inv.Tables[0].Rows[j]["NameNative"].ToString() != "") {
                                        XmlHelper.AddAttribute(
                                            doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                    } else {
                                        XmlHelper.AddAttribute(
                                            doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                    }
                                } else {
                                    if (inv.Tables[0].Rows[j]["NameEN"].ToString() != "") {
                                        XmlHelper.AddAttribute(
                                            doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                    } else {
                                        XmlHelper.AddAttribute(
                                            doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                    }
                                }
                                item.AppendChild(invItem);
                            }
                        }
                        shareholders.AppendChild(item);
                    }
                }
            }
            return shareholders;
        }

        private XmlElement GetKeyEmployees(XmlDocument doc, int companyCIID) {
            XmlElement keyEmp = XmlHelper.GetXmlTable(doc, "KeyEmployees", "tblKeyEmployees");
            XmlHelper.AddAttribute(doc, keyEmp, "title", rm.GetString("txtkeyEmployees", ci));

            XmlElement th = XmlHelper.GetXmlTableHeader(doc);
            XmlHelper.AddAttribute(doc, th, "Name", rm.GetString("txtName", ci));
            XmlHelper.AddAttribute(doc, th, "Number", rm.GetString("txtRegistrationID", ci));
            XmlHelper.AddAttribute(doc, th, "Education", rm.GetString("txtEducation", ci));
            XmlHelper.AddAttribute(doc, th, "JobTitle", rm.GetString("txtPosition", ci));

            keyEmp.AppendChild(th);

            DataSet ds = factory.GetKeyEmployeesAsDataset(companyCIID);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "Name",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "Name",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "Name",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "Name",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        }
                    }

                    XmlHelper.AddAttribute(doc, item, "Number", ds.Tables[0].Rows[i]["number"].ToString());
                    XmlHelper.AddAttribute(
                        doc, item, "Education", ds.Tables[0].Rows[i]["Education" + cultureEnding].ToString());
                    XmlHelper.AddAttribute(
                        doc, item, "JobTitle", ds.Tables[0].Rows[i]["JobTitle" + cultureEnding].ToString());

                    keyEmp.AppendChild(item);
                }
            } else {
                XmlElement item = XmlHelper.CreateElement(doc, "NoInfo", "");
                XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                keyEmp.AppendChild(item);
            }
            return keyEmp;
        }

        private XmlElement GetKeyEmployeesWithInvolvements(XmlDocument doc, int companyCIID) {
            XmlElement keyEmp = XmlHelper.GetXmlTable(doc, "KeyEmployeesWithInvolvements", "tblKeyEmployees");
            XmlHelper.AddAttribute(doc, keyEmp, "title", rm.GetString("txtkeyEmployees", ci));

            DataSet ds = factory.GetKeyEmployeesAsDataset(companyCIID);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "NameTitle", rm.GetString("txtName", ci));
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        }
                    }

                    XmlHelper.AddAttribute(doc, item, "RegIDTitle", rm.GetString("txtRegistrationID", ci));
                    XmlHelper.AddAttribute(doc, item, "RegIDValue", ds.Tables[0].Rows[i]["number"].ToString());
                    //if(isCreditInfoReport)
                    //{
                    XmlHelper.AddAttribute(doc, item, "Education", "1");
                    XmlHelper.AddAttribute(doc, item, "EducationTitle", rm.GetString("txtEducation", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "EducationValue", ds.Tables[0].Rows[i]["Education" + cultureEnding].ToString());
                    //}
                    //else
                    //	XmlHelper.AddAttribute(doc, item, "Education", "0");
                    XmlHelper.AddAttribute(doc, item, "JobTitleTitle", rm.GetString("txtPosition", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "JobTitleValue", ds.Tables[0].Rows[i]["JobTitle" + cultureEnding].ToString());

                    //Now we need all companys this ciid is shareholder in
                    int ciid = int.Parse(ds.Tables[0].Rows[i]["principalciid"].ToString());
                    DataSet inv = factory.GetPrincipalsInvolvements(ciid);
                    if (inv != null && inv.Tables.Count > 0 && inv.Tables[0].Rows.Count > 0) {
                        XmlHelper.AddAttribute(doc, item, "AlsoInvolvedInTitle", rm.GetString("txtAlsoInvolvedIn", ci));
                        for (int j = 0; j < inv.Tables[0].Rows.Count; j++) {
                            if (int.Parse(inv.Tables[0].Rows[j]["CompanyCIID"].ToString()) == companyCIID) {
                                continue;
                            }
                            XmlElement invItem = XmlHelper.CreateElement(doc, "Involvements", "");
                            if (nativeCult) {
                                if (inv.Tables[0].Rows[j]["NameNative"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                }

                                if (inv.Tables[0].Rows[j]["JobTitleNative"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "JobTitle", inv.Tables[0].Rows[j]["JobTitleNative"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "JobTitle", inv.Tables[0].Rows[j]["JobTitleEN"].ToString());
                                }
                            } else {
                                if (inv.Tables[0].Rows[j]["NameEN"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                }

                                if (inv.Tables[0].Rows[j]["JobTitleEN"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "JobTitle", inv.Tables[0].Rows[j]["JobTitleEN"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "JobTitle", inv.Tables[0].Rows[j]["JobTitleNative"].ToString());
                                }
                            }
                            item.AppendChild(invItem);
                        }
                    }

                    keyEmp.AppendChild(item);
                }
            } else {
                XmlElement item = XmlHelper.CreateElement(doc, "NoInfo", "");
                XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable", ci));
                keyEmp.AppendChild(item);
            }
            return keyEmp;
        }

        private XmlElement GetRealEstates(XmlDocument doc, int companyCIID) {
            XmlElement re = XmlHelper.GetXmlTable(doc, "RealEstates", "tblRealEstates");
            XmlHelper.AddAttribute(doc, re, "title", rm.GetString("txtRealEstates", ci));

            DataSet ds = factory.GetRealEstatesAsDataset(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);

                    XmlHelper.AddAttribute(doc, item, "AddressTitle", rm.GetString("txtAddress", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "AddressValue", ds.Tables[0].Rows[i]["Address" + cultureEnding].ToString());

                    XmlHelper.AddAttribute(doc, item, "CityTitle", rm.GetString("txtCity", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "CityValue", ds.Tables[0].Rows[i]["City" + cultureEnding].ToString());

                    XmlHelper.AddAttribute(doc, item, "PostalCodeTitle", rm.GetString("txtPostalCode", ci));
                    XmlHelper.AddAttribute(doc, item, "PostalCodeValue", ds.Tables[0].Rows[i]["PostCode"].ToString());

                    XmlHelper.AddAttribute(doc, item, "InsuranceValueTitle", rm.GetString("txtInsuranceValue", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "InsuranceValueValue", ds.Tables[0].Rows[i]["InsuranceValue"].ToString());

                    XmlHelper.AddAttribute(doc, item, "YearBuiltTitle", rm.GetString("txtYearBuilt", ci));
                    XmlHelper.AddAttribute(doc, item, "YearBuiltValue", ds.Tables[0].Rows[i]["YearBuilt"].ToString());

                    XmlHelper.AddAttribute(doc, item, "LocationTitle", rm.GetString("txtLocation", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "LocationValue", ds.Tables[0].Rows[i]["Location" + cultureEnding].ToString());

                    XmlHelper.AddAttribute(doc, item, "SizeTitle", rm.GetString("txtSize", ci));
                    XmlHelper.AddAttribute(doc, item, "SizeValue", ds.Tables[0].Rows[i]["Size"].ToString());

                    XmlHelper.AddAttribute(doc, item, "TenureTitle", rm.GetString("txtTenure", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "TenureValue", ds.Tables[0].Rows[i]["Tenure" + cultureEnding].ToString());

                    XmlHelper.AddAttribute(doc, item, "RealEstateTypeTitle", rm.GetString("txtRealEstateType", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "RealEstateTypeValue", ds.Tables[0].Rows[i]["Type" + cultureEnding].ToString());

                    re.AppendChild(item);
                }
            } else {
                /*	XmlElement item = XmlHelper.CreateElement(doc, "NoInfo", "");
				XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoInformationAvailable",ci));
				re.AppendChild(item);
			*/
                re = null;
            }
            return re;
        }

        private XmlElement GetCourtInfoAndDefaultingDebts(XmlDocument doc, int companyCIID) {
            XmlElement courtInfoAndDefaultingDebts = XmlHelper.GetXmlTable(
                doc, "CourtInfoAndDefaultingDebts", "tblCourtInfoAndDefaultingDebts");
            XmlHelper.AddAttribute(
                doc, courtInfoAndDefaultingDebts, "title", rm.GetString("txtCourtInfoAndDefaultingDebts", ci));

            DataSet ds = factory.GetCourtDecisionsAndDefaultingDebts(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                //string check = "";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);

                    XmlHelper.AddAttribute(doc, item, "CIReferenceTitle", rm.GetString("txtCIReference", ci));
                    XmlHelper.AddAttribute(doc, item, "CIReferenceValue", ds.Tables[0].Rows[i]["ID"].ToString());

                    XmlHelper.AddAttribute(doc, item, "ClaimOwnerNameTitle", rm.GetString("txtClaimOwner", ci));
                    //check = ds.Tables[0].Rows[i]["ClaimOwnerName"].ToString();
                    if (ds.Tables[0].Rows[i]["ClaimOwnerName"].ToString() != null &&
                        ds.Tables[0].Rows[i]["ClaimOwnerName"].ToString() != "") {
                        XmlHelper.AddAttribute(
                            doc, item, "ClaimOwnerNameValue", ds.Tables[0].Rows[i]["ClaimOwnerName"].ToString());
                    } else {
                        XmlHelper.AddAttribute(doc, item, "ClaimOwnerNameValue", "N/A");
                    }

                    XmlHelper.AddAttribute(doc, item, "ClaimTypeTitle", rm.GetString("txtClaimType", ci));
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["TypeNative"].ToString() != null &&
                            ds.Tables[0].Rows[i]["TypeNative"].ToString() != "") {
                            XmlHelper.AddAttribute(
                                doc, item, "ClaimTypeValue", ds.Tables[0].Rows[i]["TypeNative"].ToString());
                        } else {
                            XmlHelper.AddAttribute(doc, item, "ClaimTypeValue", "N/A");
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["TypeEN"].ToString() != null &&
                            ds.Tables[0].Rows[i]["TypeEN"].ToString() != "") {
                            XmlHelper.AddAttribute(
                                doc, item, "ClaimTypeValue", ds.Tables[0].Rows[i]["TypeEN"].ToString());
                        } else {
                            XmlHelper.AddAttribute(doc, item, "ClaimTypeValue", "N/A");
                        }
                    }

                    XmlHelper.AddAttribute(doc, item, "AmountTitle", rm.GetString("txtAmount", ci));
                    if (ds.Tables[0].Rows[i]["Amount"].ToString() != "" &&
                        ds.Tables[0].Rows[i]["Amount"].ToString() != null) {
                        XmlHelper.AddAttribute(
                            doc, item, "AmountValue", ParseAmount(ds.Tables[0].Rows[i]["Amount"].ToString()));
                    } else {
                        XmlHelper.AddAttribute(doc, item, "AmountValue", "N/A");
                    }

                    // breyta � short date string fyrir birtingu!
                    XmlHelper.AddAttribute(doc, item, "RegDateTitle", rm.GetString("txtDate", ci));
                    DateTime regDate = new DateTime();
                    try {
                        regDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["RegDate"]);
                        XmlHelper.AddAttribute(doc, item, "RegDateValue", regDate.ToShortDateString());
                    } catch (Exception) // if error show long date string rather than nothing
                    {
                        XmlHelper.AddAttribute(doc, item, "RegDateValue", ds.Tables[0].Rows[i]["RegDate"].ToString());
                    }

                    XmlHelper.AddAttribute(doc, item, "ClaimDateTitle", rm.GetString("txtDate", ci));
                    DateTime claimDate = new DateTime();
                    try {
                        claimDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["ClaimDate"]);
                        XmlHelper.AddAttribute(doc, item, "ClaimDateValue", claimDate.ToShortDateString());
                    } catch (Exception) // if error show long date string rather than nothing
                    {
                        XmlHelper.AddAttribute(
                            doc, item, "ClaimDateValue", ds.Tables[0].Rows[i]["ClaimDate"].ToString());
                    }

                    XmlHelper.AddAttribute(doc, item, "CaseNrTitle", rm.GetString("txtRegNr", ci));
                    if (ds.Tables[0].Rows[i]["CaseNr"].ToString() != null &&
                        ds.Tables[0].Rows[i]["CaseNr"].ToString() != "") {
                        XmlHelper.AddAttribute(doc, item, "CaseNrValue", ds.Tables[0].Rows[i]["CaseNr"].ToString());
                    } else {
                        XmlHelper.AddAttribute(doc, item, "CaseNrValue", "N/A");
                    }

                    courtInfoAndDefaultingDebts.AppendChild(item);
                }
            } else {
                XmlElement item = XmlHelper.CreateElement(doc, "NoInfo", "");
                XmlHelper.AddAttribute(doc, item, "value", rm.GetString("txtNoCourtInfoAndDefaultingDebts", ci));
                courtInfoAndDefaultingDebts.AppendChild(item);
            }
            return courtInfoAndDefaultingDebts;
        }

        private string ParseAmount(string amount) {
            string cResult = "";
            int cCount = 0;
            amount = amount.Replace(",00", "");
            for (int i = amount.Length; i >= 1; i--) {
                cCount = cCount + 1;
                cResult = amount.Substring(i - 1, 1) + cResult;
                if (cCount == 3) {
                    cResult = " " + cResult;
                    cCount = 0;
                }
            }
            return cResult;
        }

        private XmlElement GetCreditScoring(XmlDocument doc, int companyCIID) {
            XmlElement creditScoring = XmlHelper.GetXmlTable(doc, "CreditScoring", "tblCreditScoring");
            XmlHelper.AddAttribute(doc, creditScoring, "title", rm.GetString("txtCreditScoring", ci));

            XmlElement score = XmlHelper.CreateElement(doc, "Score", "");
            creditScoring.AppendChild(score);

            decimal creditScore = factory.GetCreditScoring(companyCIID, 1, DateTime.Now);
            int scoreCategory = factory.GetScoreCategory(creditScore, 1);
            if (scoreCategory == -1) {
                XmlHelper.AddAttribute(doc, score, "ScoreValue", rm.GetString("txtCreditScoringNoDataAvailable", ci));
            } else {
                XmlHelper.AddAttribute(
                    doc,
                    score,
                    "ScoreValue",
                    rm.GetString("txtCreditScoringExplainText1", ci) + "" + creditScore + "% (" +
                    rm.GetString("txtCreditScoringExplainText2", ci) + " " + scoreCategory + ").");

                XmlElement scoreCategories = XmlHelper.CreateElement(doc, "ScoreCategories", "");
                XmlHelper.AddAttribute(doc, scoreCategories, "Value", rm.GetString("txtCreditCategories", ci));
                creditScoring.AppendChild(scoreCategories);
                DataSet dsScoreRatings = factory.GetScoreRatingsAsDataSet();
                if (dsScoreRatings.Tables.Count > 0) {
                    for (int i = 0; i < dsScoreRatings.Tables[0].Rows.Count; i++) {
                        if (i >= 11) {
                            break;
                        }
                        string data = i + 1 + ".        " + dsScoreRatings.Tables[0].Rows[i]["s_min"] + "% - " +
                                      dsScoreRatings.Tables[0].Rows[i]["s_max"] + "%";
                        if (nativeCult) {
                            data += "         " + dsScoreRatings.Tables[0].Rows[i]["rating_na"];
                        } else {
                            data += "          " + dsScoreRatings.Tables[0].Rows[i]["rating_en"];
                        }

                        XmlElement item = XmlHelper.GetXmlTableItem(doc);
                        XmlHelper.AddAttribute(doc, item, "ScoreCategoryValue", data);
                        creditScoring.AppendChild(item);
                    }
                }
                // add disclaimer
                XmlElement scoreDisclaimer = XmlHelper.CreateElement(doc, "ScoreDisclaimer", "");
                XmlHelper.AddAttribute(doc, scoreCategories, "Value", rm.GetString("txtCreditScoringDisclaimer", ci));
                creditScoring.AppendChild(scoreDisclaimer);
            }
            return creditScoring;
        }

        private String formatPostalCodeForCzech(string postalCode) {
            if (postalCode == null || postalCode == "") {
                return "N/A";
            }
            StringBuilder formatString = new StringBuilder(postalCode);
            formatString.Insert(3, " ");
            return formatString.ToString();
        }

        public XmlElement GetBalanceSheet(XmlDocument doc, int companyCIID, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = reportType;
            DataSet ds = factory.GetBalanceSheetAsDataset(companyCIID, afs_ids);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    financialInfo.SetValue(
                        i, FinancialInfo.CURRENCY, ds.Tables[0].Rows[i][FinancialInfo.CURRENCY].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.MONTHS, ds.Tables[0].Rows[i][FinancialInfo.MONTHS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.FINANCIAL_YEAR_END,
                        ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR_END].ToString());

                    financialInfo.SetValue(i, FinancialInfo.CASH, ds.Tables[0].Rows[i][FinancialInfo.CASH].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.RECEIVABLES, ds.Tables[0].Rows[i][FinancialInfo.RECEIVABLES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OTHER_CURRENT_ASSETS,
                        ds.Tables[0].Rows[i][FinancialInfo.OTHER_CURRENT_ASSETS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INVENTORY_STOCKS,
                        ds.Tables[0].Rows[i][FinancialInfo.INVENTORY_STOCKS].ToString());
                    try {
                        financialInfo.SetValue(
                            i,
                            FinancialInfo.TAX_LIABILITIES,
                            ds.Tables[0].Rows[i][FinancialInfo.TAX_LIABILITIES].ToString());
                        financialInfo.SetValue(
                            i,
                            FinancialInfo.OTHER_LIABILITIES,
                            ds.Tables[0].Rows[i][FinancialInfo.OTHER_LIABILITIES].ToString());
                        financialInfo.SetValue(
                            i,
                            FinancialInfo.CLAIMS_TOWARDS_ASSOCIATED_COMPANIES,
                            ds.Tables[0].Rows[i][FinancialInfo.CLAIMS_TOWARDS_ASSOCIATED_COMPANIES].ToString());
                    } catch (Exception) {}
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CURRENT_ASSETS_TOTAL,
                        ds.Tables[0].Rows[i][FinancialInfo.CURRENT_ASSETS_TOTAL].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.FINANCIALS_ASSETS,
                        ds.Tables[0].Rows[i][FinancialInfo.FINANCIALS_ASSETS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TANGIBLE_ASSETS, ds.Tables[0].Rows[i][FinancialInfo.TANGIBLE_ASSETS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INTANGIBLE_ASSETS,
                        ds.Tables[0].Rows[i][FinancialInfo.INTANGIBLE_ASSETS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.FIXED_ASSETS_TOTAL,
                        ds.Tables[0].Rows[i][FinancialInfo.FIXED_ASSETS_TOTAL].ToString());
                    financialInfo.SetValue(
                          i,
                          FinancialInfo.DEFERRED_TAX_ASSETS,
                          ds.Tables[0].Rows[i][FinancialInfo.DEFERRED_TAX_ASSETS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.ASSETS_TOTAL, ds.Tables[0].Rows[i][FinancialInfo.ASSETS_TOTAL].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OVERDRAFT_BANK_LOAN,
                        ds.Tables[0].Rows[i][FinancialInfo.OVERDRAFT_BANK_LOAN].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.CREDITORS, ds.Tables[0].Rows[i][FinancialInfo.CREDITORS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OTHER_SHORT_TERM_DEBTS,
                        ds.Tables[0].Rows[i][FinancialInfo.OTHER_SHORT_TERM_DEBTS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.NEXT_YEAR_MATURITIES,
                        ds.Tables[0].Rows[i][FinancialInfo.NEXT_YEAR_MATURITIES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.SHORT_TERM_DEBTS_TOTAL,
                        ds.Tables[0].Rows[i][FinancialInfo.SHORT_TERM_DEBTS_TOTAL].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.LONG_TERM_DEBTS, ds.Tables[0].Rows[i][FinancialInfo.LONG_TERM_DEBTS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.OBLIGATIONS, ds.Tables[0].Rows[i][FinancialInfo.OBLIGATIONS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TOTAL_DEBT, ds.Tables[0].Rows[i][FinancialInfo.TOTAL_DEBT].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.ISSUED_SHARE_CAPITAL,
                        ds.Tables[0].Rows[i][FinancialInfo.ISSUED_SHARE_CAPITAL].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.OTHER_EQUITY, ds.Tables[0].Rows[i][FinancialInfo.OTHER_EQUITY].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.EQUITY_TOTAL, ds.Tables[0].Rows[i][FinancialInfo.EQUITY_TOTAL].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.MINORITY_HOLDINGS_IN_EQUITY,
                        ds.Tables[0].Rows[i][FinancialInfo.MINORITY_HOLDINGS_IN_EQUITY].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.DEBT_EQUITY_TOTAL,
                        ds.Tables[0].Rows[i][FinancialInfo.DEBT_EQUITY_TOTAL].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.PROFIT_AND_LOSS_ACCOUNT,
                        ds.Tables[0].Rows[i][FinancialInfo.PROFIT_AND_LOSS_ACCOUNT].ToString());
                }
            }
            return financialInfo.GetBalanceSheetTemplate(doc);
        }

        public XmlElement GetCashFlow(XmlDocument doc, int companyCIID, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = reportType;
            DataSet ds = factory.GetProfitLossAccountAsDataset(companyCIID, afs_ids);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATING_PROFIT_BEFORE_WORKING_CAPITAL_CHANGES,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATING_PROFIT_BEFORE_WORKING_CAPITAL_CHANGES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CAPITAL_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CAPITAL_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FLOWS_FROM_FINANCING_ACTIVITIES,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FLOWS_FROM_FINANCING_ACTIVITIES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FLOWS_FROM_INVESTING_ACTIVITIES,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FLOWS_FROM_INVESTING_ACTIVITIES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCR_DECR_IN_CASH,
                        ds.Tables[0].Rows[i][FinancialInfo.INCR_DECR_IN_CASH].ToString());
                }
            }
            return financialInfo.GetCashFlowTemplate(doc);
        }

        public XmlElement GetCashFlowShort(XmlDocument doc, int companyCIID, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = reportType;
            DataSet ds = factory.GetProfitLossAccountAsDataset(companyCIID, afs_ids);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATING_PROFIT_BEFORE_WORKING_CAPITAL_CHANGES,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATING_PROFIT_BEFORE_WORKING_CAPITAL_CHANGES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CAPITAL_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CAPITAL_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FLOWS_FROM_FINANCING_ACTIVITIES,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FLOWS_FROM_FINANCING_ACTIVITIES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FLOWS_FROM_INVESTING_ACTIVITIES,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FLOWS_FROM_INVESTING_ACTIVITIES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCR_DECR_IN_CASH,
                        ds.Tables[0].Rows[i][FinancialInfo.INCR_DECR_IN_CASH].ToString());
                }
            }
            return financialInfo.GetCashFlowTemplateShort(doc);
        }

        public XmlElement GetHeadFSI(XmlDocument doc, int companyCIID, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = reportType;
            DataSet ds = factory.GetProfitLossAccountAsDataset(companyCIID, afs_ids);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.CURRENCY, ds.Tables[0].Rows[i][FinancialInfo.CURRENCY].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.ACCOUNT_PERIOD_LENGTH,
                        ds.Tables[0].Rows[i][FinancialInfo.ACCOUNT_PERIOD_LENGTH].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.FINANCIAL_YEAR_END,
                        ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR_END].ToString());
                }
            }
            return financialInfo.GetHeadFSITemplate(doc);
        }

        private XmlElement GetAudited(XmlDocument doc, int companyCIID, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = reportType;
            DataSet ds = factory.GetProfitLossAccountAsDataset(companyCIID, afs_ids);

            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string auditted = ds.Tables[0].Rows[i][FinancialInfo.QUALIFIED_AUDIT].ToString();
                    if (auditted.Equals("True")) {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "Yes");
                    } else {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "No");
                    }
                }
            }
            return financialInfo.GetAuditedTemplate(doc);
        }

        public XmlElement GetProfitLossAccount(XmlDocument doc, int companyCIID, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = reportType;
            DataSet ds = factory.GetProfitLossAccountAsDataset(companyCIID, afs_ids);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());

                    //financialInfo.SetValue(i, FinancialInfo., ds.Tables[0].Rows[i][FinancialInfo.].ToString());

                    financialInfo.SetValue(
                        i, FinancialInfo.CURRENCY, ds.Tables[0].Rows[i][FinancialInfo.CURRENCY].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.ACCOUNT_PERIOD_LENGTH,
                        ds.Tables[0].Rows[i][FinancialInfo.ACCOUNT_PERIOD_LENGTH].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.FINANCIAL_YEAR_END,
                        ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR_END].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.REVENUE_FROM_MAIN_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.REVENUE_FROM_MAIN_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.GROSS_PROFIT, ds.Tables[0].Rows[i][FinancialInfo.GROSS_PROFIT].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.COST_OF_SALES, ds.Tables[0].Rows[i][FinancialInfo.COST_OF_SALES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OTHER_OPERATING_REVENUE,
                        ds.Tables[0].Rows[i][FinancialInfo.OTHER_OPERATING_REVENUE].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.STAFF_COST, ds.Tables[0].Rows[i][FinancialInfo.STAFF_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.ADMINISTRATIVE_OTHER_EXPENSES,
                        ds.Tables[0].Rows[i][FinancialInfo.ADMINISTRATIVE_OTHER_EXPENSES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.DISTRIBUTION_COST,
                        ds.Tables[0].Rows[i][FinancialInfo.DISTRIBUTION_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.DEPRICATION_OF_FIXED_ASSETS,
                        ds.Tables[0].Rows[i][FinancialInfo.DEPRICATION_OF_FIXED_ASSETS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TOTAL_COSTS, ds.Tables[0].Rows[i][FinancialInfo.TOTAL_COSTS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_PROFIT,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_PROFIT].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.FINANCIAL_INCOME,
                        ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_INCOME].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_COST, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OTHER_FINANCIAL_ITEMS,
                        ds.Tables[0].Rows[i][FinancialInfo.OTHER_FINANCIAL_ITEMS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.NET_FINANCIAL_ITEMS,
                        ds.Tables[0].Rows[i][FinancialInfo.NET_FINANCIAL_ITEMS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCOME_FROM_OPERATIONS_BEFORE_TAX,
                        ds.Tables[0].Rows[i][FinancialInfo.INCOME_FROM_OPERATIONS_BEFORE_TAX].ToString());
                    financialInfo.SetValue(i, FinancialInfo.TAX, ds.Tables[0].Rows[i][FinancialInfo.TAX].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCOME_FROM_OPERATIONS_AFTER_TAX,
                        ds.Tables[0].Rows[i][FinancialInfo.INCOME_FROM_OPERATIONS_AFTER_TAX].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.EXTRAORDINARY_INCOME_COST,
                        ds.Tables[0].Rows[i][FinancialInfo.EXTRAORDINARY_INCOME_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCOME_FROM_ASSOCIATED_COMPANIES,
                        ds.Tables[0].Rows[i][FinancialInfo.INCOME_FROM_ASSOCIATED_COMPANIES].ToString());
                    //financialInfo.SetValue(i, FinancialInfo.NET_RESULT, ds.Tables[0].Rows[i][FinancialInfo.NET_RESULT].ToString());

                    try {
                        financialInfo.SetValue(
                            i,
                            FinancialInfo.REVENUE_FROM_MAIN_OPERATIONS,
                            ds.Tables[0].Rows[i][FinancialInfo.REVENUE_FROM_MAIN_OPERATIONS].ToString());
                    } catch (Exception) {}

                    financialInfo.SetValue(
                        i, FinancialInfo.INCOME, ds.Tables[0].Rows[i][FinancialInfo.INCOME].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TOTAL_REVENUE, ds.Tables[0].Rows[i][FinancialInfo.TOTAL_REVENUE].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_COST,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_COST].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.DEPRICATION, ds.Tables[0].Rows[i][FinancialInfo.DEPRICATION].ToString());

                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_COST, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_PROFIT,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_PROFIT].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.NET_FINANCIAL_ITEMS,
                        ds.Tables[0].Rows[i][FinancialInfo.NET_FINANCIAL_ITEMS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.PRE_TAX_PROFIT, ds.Tables[0].Rows[i][FinancialInfo.PRE_TAX_PROFIT].ToString());
                    financialInfo.SetValue(i, FinancialInfo.TAXES, ds.Tables[0].Rows[i][FinancialInfo.TAXES].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.NET_RESULT, ds.Tables[0].Rows[i][FinancialInfo.NET_RESULT].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CAPITAL_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CAPITAL_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCR_DECR_IN_CASH,
                        ds.Tables[0].Rows[i][FinancialInfo.INCR_DECR_IN_CASH].ToString());
                    string auditted = ds.Tables[0].Rows[i][FinancialInfo.QUALIFIED_AUDIT].ToString();
//					if(auditted.Equals("True"))
//						financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "Yes");
//					else
//						financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "No");
                    if (auditted.Equals("True")) {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, rm.GetString("txtYes", ci));
                    } else {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, rm.GetString("txtNo", ci));
                    }
                }
            }
            return financialInfo.GetProfitLossAccountTemplate(doc);
        }

        public XmlElement GetTemplateRatios(XmlDocument doc, int companyCIID, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = reportType;
            DataSet ds = factory.GetProfitLossAccountAsDataset(companyCIID, afs_ids);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.REVENUE_FROM_MAIN_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.REVENUE_FROM_MAIN_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.COST_OF_SALES, ds.Tables[0].Rows[i][FinancialInfo.COST_OF_SALES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OTHER_OPERATING_REVENUE,
                        ds.Tables[0].Rows[i][FinancialInfo.OTHER_OPERATING_REVENUE].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.STAFF_COST, ds.Tables[0].Rows[i][FinancialInfo.STAFF_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.ADMINISTRATIVE_OTHER_EXPENSES,
                        ds.Tables[0].Rows[i][FinancialInfo.ADMINISTRATIVE_OTHER_EXPENSES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.DISTRIBUTION_COST,
                        ds.Tables[0].Rows[i][FinancialInfo.DISTRIBUTION_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.DEPRICATION_OF_FIXED_ASSETS,
                        ds.Tables[0].Rows[i][FinancialInfo.DEPRICATION_OF_FIXED_ASSETS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TOTAL_COSTS, ds.Tables[0].Rows[i][FinancialInfo.TOTAL_COSTS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_PROFIT,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_PROFIT].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.FINANCIAL_INCOME,
                        ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_INCOME].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_COST, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OTHER_FINANCIAL_ITEMS,
                        ds.Tables[0].Rows[i][FinancialInfo.OTHER_FINANCIAL_ITEMS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.NET_FINANCIAL_ITEMS,
                        ds.Tables[0].Rows[i][FinancialInfo.NET_FINANCIAL_ITEMS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCOME_FROM_OPERATIONS_BEFORE_TAX,
                        ds.Tables[0].Rows[i][FinancialInfo.INCOME_FROM_OPERATIONS_BEFORE_TAX].ToString());
                    financialInfo.SetValue(i, FinancialInfo.TAX, ds.Tables[0].Rows[i][FinancialInfo.TAX].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCOME_FROM_OPERATIONS_AFTER_TAX,
                        ds.Tables[0].Rows[i][FinancialInfo.INCOME_FROM_OPERATIONS_AFTER_TAX].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.EXTRAORDINARY_INCOME_COST,
                        ds.Tables[0].Rows[i][FinancialInfo.EXTRAORDINARY_INCOME_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCOME_FROM_ASSOCIATED_COMPANIES,
                        ds.Tables[0].Rows[i][FinancialInfo.INCOME_FROM_ASSOCIATED_COMPANIES].ToString());
                    //financialInfo.SetValue(i, FinancialInfo.NET_RESULT, ds.Tables[0].Rows[i][FinancialInfo.NET_RESULT].ToString());

                    try {
                        financialInfo.SetValue(
                            i,
                            FinancialInfo.REVENUE_FROM_MAIN_OPERATIONS,
                            ds.Tables[0].Rows[i][FinancialInfo.REVENUE_FROM_MAIN_OPERATIONS].ToString());
                    } catch (Exception) {}

                    financialInfo.SetValue(
                        i, FinancialInfo.INCOME, ds.Tables[0].Rows[i][FinancialInfo.INCOME].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TOTAL_REVENUE, ds.Tables[0].Rows[i][FinancialInfo.TOTAL_REVENUE].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_COST,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_COST].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.DEPRICATION, ds.Tables[0].Rows[i][FinancialInfo.DEPRICATION].ToString());

                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_COST, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_PROFIT,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_PROFIT].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.NET_FINANCIAL_ITEMS,
                        ds.Tables[0].Rows[i][FinancialInfo.NET_FINANCIAL_ITEMS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.PRE_TAX_PROFIT, ds.Tables[0].Rows[i][FinancialInfo.PRE_TAX_PROFIT].ToString());
                    financialInfo.SetValue(i, FinancialInfo.TAXES, ds.Tables[0].Rows[i][FinancialInfo.TAXES].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.NET_RESULT, ds.Tables[0].Rows[i][FinancialInfo.NET_RESULT].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CAPITAL_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CAPITAL_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCR_DECR_IN_CASH,
                        ds.Tables[0].Rows[i][FinancialInfo.INCR_DECR_IN_CASH].ToString());
                    string auditted = ds.Tables[0].Rows[i][FinancialInfo.QUALIFIED_AUDIT].ToString();
                    //					if(auditted.Equals("True"))
                    //						financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "Yes");
                    //					else
                    //						financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "No");
                    if (auditted.Equals("True")) {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, rm.GetString("txtYes", ci));
                    } else {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, rm.GetString("txtNo", ci));
                    }
                }
            }

            return financialInfo.GetRatiosTemplate(doc);
        }

        public XmlElement GetTemplateProfitLossAccount(XmlDocument doc, int companyCIID, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = reportType;
            DataSet ds = factory.GetProfitLossAccountAsDataset(companyCIID, afs_ids);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());

                    financialInfo.SetValue(
                        i, FinancialInfo.INCOME, ds.Tables[0].Rows[i][FinancialInfo.INCOME].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TOTAL_REVENUE, ds.Tables[0].Rows[i][FinancialInfo.TOTAL_REVENUE].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_COST,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_COST].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.DEPRICATION, ds.Tables[0].Rows[i][FinancialInfo.DEPRICATION].ToString());

                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_COST, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_COST].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_PROFIT,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_PROFIT].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.NET_FINANCIAL_ITEMS,
                        ds.Tables[0].Rows[i][FinancialInfo.NET_FINANCIAL_ITEMS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.PRE_TAX_PROFIT, ds.Tables[0].Rows[i][FinancialInfo.PRE_TAX_PROFIT].ToString());
                    financialInfo.SetValue(i, FinancialInfo.TAXES, ds.Tables[0].Rows[i][FinancialInfo.TAXES].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.NET_RESULT, ds.Tables[0].Rows[i][FinancialInfo.NET_RESULT].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CAPITAL_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CAPITAL_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCR_DECR_IN_CASH,
                        ds.Tables[0].Rows[i][FinancialInfo.INCR_DECR_IN_CASH].ToString());
                    string auditted = ds.Tables[0].Rows[i][FinancialInfo.QUALIFIED_AUDIT].ToString();
//					if(auditted.Equals("True"))
//						financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "Yes");
//					else
//						financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "No");
                    if (auditted.Equals("True")) {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, rm.GetString("txtYes", ci));
                    } else {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, rm.GetString("txtNo", ci));
                    }
                }
            }
            return financialInfo.GetProfitLossAccountTemplateWithoutRatios(doc);
        }

        /*
		public XmlElement GetTemplateRatios(XmlDocument doc, int ciid, string afs_ids)
		{
			if(afs_ids==null||afs_ids.Trim().Length==0)
				return null;
			string[] afs = afs_ids.Split(',');
			if(afs.Length>0)
			{
				FinancialStatementFactory fsf = new FinancialStatementFactory();
				int templateID = fsf.GetTemplateID(int.Parse(afs[0]));
				ArrayList vals = fsf.GetTemplateValues(ciid, templateID, afs_ids);
				RatioGenerator rGen = new RatioGenerator(rm, ci);
				return rGen.GetRatios(doc, templateID, vals);
			}
			return null;
		}
		*/

        public XmlElement GetTemplateDefinitionOfRatios(XmlDocument doc, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            string[] afs = afs_ids.Split(',');
            if (afs.Length > 0) {
                FinancialStatementFactory fsf = new FinancialStatementFactory();
                int templateID = fsf.GetTemplateID(int.Parse(afs[0]));
                RatioGenerator rGen = new RatioGenerator(rm, ci);
                return rGen.GetRatiosDescription(doc, templateID);
            }
            return null;
        }

        public XmlElement GetDefinitionOfRatios(XmlDocument doc, string afs_ids) {
            if (afs_ids == null || afs_ids.Trim().Length == 0) {
                return null;
            }
            XmlElement defOfRatios = XmlHelper.GetXmlTable(doc, "DefinitionOfRatios", "tblDefinitionOfRatios");
            XmlHelper.AddAttribute(doc, defOfRatios, "title", rm.GetString("txtDefinitionOfRatios", ci));

            XmlElement item = XmlHelper.GetXmlTableItem(doc);
            XmlHelper.AddAttribute(doc, item, "Title", rm.GetString("txtProfitMargin", ci));
            XmlHelper.AddAttribute(doc, item, "Value", rm.GetString("txtProfitMarginDef", ci));
            defOfRatios.AppendChild(item);
            item = XmlHelper.GetXmlTableItem(doc);
            XmlHelper.AddAttribute(doc, item, "Title", rm.GetString("txtCurrentRatio", ci));
            XmlHelper.AddAttribute(doc, item, "Value", rm.GetString("txtCurrentRatioDef", ci));
            defOfRatios.AppendChild(item);
            item = XmlHelper.GetXmlTableItem(doc);
            XmlHelper.AddAttribute(doc, item, "Title", rm.GetString("txtQuickRatio", ci));
            XmlHelper.AddAttribute(doc, item, "Value", rm.GetString("txtQuickRatioDef", ci));
            defOfRatios.AppendChild(item);
            item = XmlHelper.GetXmlTableItem(doc);
            XmlHelper.AddAttribute(doc, item, "Title", rm.GetString("txtEquityRatio", ci));
            XmlHelper.AddAttribute(doc, item, "Value", rm.GetString("txtEquityRatioDef", ci));
            defOfRatios.AppendChild(item);
            item = XmlHelper.GetXmlTableItem(doc);
            XmlHelper.AddAttribute(doc, item, "Title", rm.GetString("txtDebtRatio", ci));
            XmlHelper.AddAttribute(doc, item, "Value", rm.GetString("txtDebtRatioDef", ci));
            defOfRatios.AppendChild(item);
            item = XmlHelper.GetXmlTableItem(doc);
            XmlHelper.AddAttribute(doc, item, "Title", rm.GetString("txtCollectionTime", ci));
            XmlHelper.AddAttribute(doc, item, "Value", rm.GetString("txtCollectionTimoDef", ci));
            defOfRatios.AppendChild(item);
            item = XmlHelper.GetXmlTableItem(doc);
            XmlHelper.AddAttribute(doc, item, "Title", rm.GetString("txtStockTurnover", ci));
            XmlHelper.AddAttribute(doc, item, "Value", rm.GetString("txtStockTurnoverDef", ci));
            defOfRatios.AppendChild(item);
            item = XmlHelper.GetXmlTableItem(doc);
            XmlHelper.AddAttribute(doc, item, "Title", rm.GetString("txtInterestCoverRatio", ci));
            XmlHelper.AddAttribute(doc, item, "Value", rm.GetString("txtInterestCoverRatioDef", ci));
            defOfRatios.AppendChild(item);
            item = XmlHelper.GetXmlTableItem(doc);
            XmlHelper.AddAttribute(doc, item, "Title", rm.GetString("txtROCERatio", ci));
            XmlHelper.AddAttribute(doc, item, "Value", rm.GetString("txtROCERatioDef", ci));
            defOfRatios.AppendChild(item);

            return defOfRatios;
        }

        protected XmlElement GetRecentEnquiries(XmlDocument doc, int companyCIID) {
            XmlElement recentEnq = XmlHelper.GetXmlTable(doc, "RecentEnquiries", "tblRecentEnquiries");
            XmlHelper.AddAttribute(doc, recentEnq, "title", rm.GetString("txtRecentEnquiries", ci));

            cwFactory creditFactory = new cwFactory();

            XmlElement row = XmlHelper.GetXmlRow(doc);
            XmlHelper.AddAttribute(doc, row, "LastMonthText", rm.GetString("txtLastMonth", ci));
            XmlHelper.AddAttribute(
                doc,
                row,
                "LastMonthValue",
                creditFactory.GetLogCountForLastXDaysPerCreditInfoUserExcludingEmployees(30, companyCIID).ToString());

            XmlHelper.AddAttribute(doc, row, "Last3MonthsText", rm.GetString("txtLast3Months", ci));
            XmlHelper.AddAttribute(
                doc,
                row,
                "Last3MonthsValue",
                creditFactory.GetLogCountForLastXDaysPerCreditInfoUserExcludingEmployees(90, companyCIID).ToString());

            XmlHelper.AddAttribute(doc, row, "Last6MonthsText", rm.GetString("txtLast6Months", ci));
            XmlHelper.AddAttribute(
                doc,
                row,
                "Last6MonthsValue",
                creditFactory.GetLogCountForLastXDaysPerCreditInfoUserExcludingEmployees(180, companyCIID).ToString());

            recentEnq.AppendChild(row);

            return recentEnq;
        }

        protected void logUsage(int ciid) { factory.InsertInto_np_Usage(userCreditInfoID, reportType, ciid + "", 1, ipAddress, userID); }

        #region BillingReport

        private bool IsAgreementOpen(int nAgreementID) {
            if (agreement == null) {
                return false;
            } else if (agreement.AgreementID != nAgreementID) {
                return false;
            }

            return true;
        }

        private XmlElement GetBillingHeader(XmlDocument doc, int companyCIID) {
            XmlElement header = XmlHelper.GetXmlTable(doc, "Header", "tblHeader");
            XmlHelper.AddAttribute(doc, header, "title", rm.GetString("txtBasicInfoHeader", ci));

            ReportCompanyBLLC theCompany = factory.GetBillingCompanyInfo("", companyCIID, false);
            if (theCompany != null) {
                string workPhone = "";
                string fax = "";
                if (theCompany.PNumbers != null) {
                    foreach (PhoneNumber myNumber in theCompany.PNumbers) {
                        if (CigConfig.Configure("lookupsettings.workCode") == Convert.ToString(myNumber.NumberTypeID)) {
                            workPhone = myNumber.Number;
                        } else if (CigConfig.Configure("lookupsettings.faxCode") == Convert.ToString(myNumber.NumberTypeID)) {
                            fax = myNumber.Number;
                        }
                    }
                }

                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "RegistrationID",
                        "title",
                        rm.GetString("txtRegistrationID", ci),
                        "value",
                        theCompany.UniqueID));
                if (nativeCult) {
                    if (theCompany.NameNative != null && theCompany.NameNative.Length > 0) {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "Name", "title", rm.GetString("txtName", ci), "value", theCompany.NameNative));
                    } else {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "Name", "title", rm.GetString("txtName", ci), "value", theCompany.NameEN));
                    }
                    if (theCompany.Address != null && theCompany.Address.Count > 0) {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc,
                                "Address",
                                "title",
                                rm.GetString("txtAddress", ci),
                                "value",
                                ((Address) theCompany.Address[0]).StreetNative));
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "Phone", "title", rm.GetString("txtPhone", ci), "value", workPhone));
                    }
                } else {
                    if (theCompany.NameEN != null && theCompany.NameEN.Length > 0) {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "Name", "title", rm.GetString("txtName", ci), "value", theCompany.NameEN));
                    } else {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "Name", "title", rm.GetString("txtName", ci), "value", theCompany.NameNative));
                    }
                    if (theCompany.Address != null && theCompany.Address.Count > 0) {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc,
                                "Address",
                                "title",
                                rm.GetString("txtAddress", ci),
                                "value",
                                ((Address) theCompany.Address[0]).StreetEN));
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc, "Phone", "title", rm.GetString("txtPhone", ci), "value", workPhone));
                    }
                }

                string postalCode = "";
                if (theCompany.Address != null && theCompany.Address.Count > 0) {
                    // check for Czech version and them format the postal code from 00000 to 000 00
                    if (IsCzech || IsSlovak) {
                        postalCode = formatPostalCodeForCzech(((Address) theCompany.Address[0]).PostalCode);
                    } else {
                        postalCode = ((Address) theCompany.Address[0]).PostalCode;
                    }
                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc, "PostalCode", "title", rm.GetString("txtPostalCode", ci), "value", postalCode));
                    header.AppendChild(
                        XmlHelper.CreateElement(doc, "Fax", "title", rm.GetString("txtFax", ci), "value", fax));

                    if (nativeCult) {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc,
                                "City",
                                "title",
                                rm.GetString("txtCity", ci),
                                "value",
                                ((Address) theCompany.Address[0]).CityNameNative));
                    } else {
                        header.AppendChild(
                            XmlHelper.CreateElement(
                                doc,
                                "City",
                                "title",
                                rm.GetString("txtCity", ci),
                                "value",
                                ((Address) theCompany.Address[0]).CityNameEN));
                    }

                    header.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "POBox",
                            "title",
                            rm.GetString("txtPOBox", ci),
                            "value",
                            ((Address) theCompany.Address[0]).PostBox));
                }

                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "Founded",
                        "title",
                        rm.GetString("txtFounded", ci),
                        "value",
                        theCompany.Established.ToShortDateString()));
                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "Registered",
                        "title",
                        rm.GetString("txtRegDate", ci),
                        "value",
                        theCompany.Registered.ToShortDateString()));

                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "EnglishName", "title", rm.GetString("txtEnglishName", ci), "value", theCompany.NameEN));
                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "NickName", "title", rm.GetString("txtNickName", ci), "value", theCompany.NameEN));
                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "HomePage", "title", rm.GetString("txtHomepage", ci), "value", theCompany.HomePage));
                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "LastUpdate",
                        "title",
                        rm.GetString("txtLastUpdate", ci),
                        "value",
                        theCompany.LastUpdated.ToShortDateString()));
                header.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "Email", "title", rm.GetString("txtEmail", ci), "value", theCompany.Email));
            }
            return header;
        }

        private XmlElement GetBillingAgreementHeader(XmlDocument doc, int companyCIID, int nAgreementID) {
            XmlElement agreementDescription = XmlHelper.GetXmlTable(doc, "AgreementHeader", "tblAgreementHeader");
            XmlHelper.AddAttribute(doc, agreementDescription, "Title", rm.GetString("txtBillingAgreement", ci));

            if (!IsAgreementOpen(nAgreementID)) {
                agreement = factory.GetBillingAgreement(nAgreementID);
            }

            if (agreement != null) {
                XmlHelper.AddAttribute(doc, agreementDescription, "CurrencyText", rm.GetString("txtCurrency", ci));
                XmlHelper.AddAttribute(doc, agreementDescription, "BeginDateText", rm.GetString("txtBeginDate", ci));
                XmlHelper.AddAttribute(doc, agreementDescription, "EndDateText", rm.GetString("txtEndDate", ci));

                XmlHelper.AddAttribute(doc, agreementDescription, "Currency", agreement.Currency);
                XmlHelper.AddAttribute(doc, agreementDescription, "BeginDate", agreement.BeginDate.ToShortDateString());
                XmlHelper.AddAttribute(doc, agreementDescription, "EndDate", agreement.EndDate.ToShortDateString());
            }
            return agreementDescription;
        }

        private XmlElement GetBillingAgreementItems(XmlDocument doc, int companyCIID, int nAgreementID) {
            if (!IsAgreementOpen(nAgreementID)) {
                agreement = factory.GetBillingAgreement(nAgreementID);
            }

            XmlElement xelItems = XmlHelper.GetXmlTable(doc, "AgreementItems", "tblAgreementItems");
            XmlHelper.AddAttribute(doc, xelItems, "Title", rm.GetString("txtAgreementItems", ci));
            XmlHelper.AddAttribute(doc, xelItems, "Name", rm.GetString("txtBillingName", ci));
            XmlHelper.AddAttribute(doc, xelItems, "Type", rm.GetString("txtType", ci));
            XmlHelper.AddAttribute(doc, xelItems, "Price", rm.GetString("txtPrice", ci));
            XmlHelper.AddAttribute(doc, xelItems, "Discount", rm.GetString("txtDiscount", ci));
            XmlHelper.AddAttribute(doc, xelItems, "Free", rm.GetString("txtFree", ci));
            XmlHelper.AddAttribute(doc, xelItems, "From", rm.GetString("txtBeginDate", ci));
            XmlHelper.AddAttribute(doc, xelItems, "To", rm.GetString("txtEndDate", ci));

            XmlElement xelItem = doc.CreateElement("AgreementItemsBLLC");
            XmlHelper.AddAttribute(doc, xelItem, "Name", rm.GetString("txtFixedAgreementPrice", ci));
            XmlHelper.AddAttribute(doc, xelItem, "BeginDate", agreement.BeginDate.ToShortDateString());
            XmlHelper.AddAttribute(doc, xelItem, "EndDate", agreement.EndDate.ToShortDateString());
            XmlHelper.AddAttribute(doc, xelItem, "Free", "");
            XmlHelper.AddAttribute(doc, xelItem, "Type", "");
            XmlHelper.AddAttribute(doc, xelItem, "Amount", agreement.AgreementAmount.ToString());
            XmlHelper.AddAttribute(doc, xelItem, "Discount", agreement.Discountpercent.ToString());
            xelItems.AppendChild(xelItem);

            foreach (AgreementItemBLLC item in agreement.ItemsOnAgreement) {
                //xelItems.InnerXml += item.ToXml();
                BillingItemBLLC billingItem = factory.GetBillingItem(item.ItemID);

                if (item.FixedAmount > 0) {
                    xelItem = doc.CreateElement("AgreementItemsBLLC");
                    if (nativeCult) {
                        XmlHelper.AddAttribute(doc, xelItem, "Name", billingItem.ItemNameNative);
                    } else {
                        XmlHelper.AddAttribute(doc, xelItem, "Name", billingItem.ItemNameEN);
                    }
                    XmlHelper.AddAttribute(doc, xelItem, "BeginDate", item.BeginDate.ToShortDateString());
                    XmlHelper.AddAttribute(doc, xelItem, "EndDate", item.EndDate.ToShortDateString());
                    XmlHelper.AddAttribute(doc, xelItem, "Free", "");
                    XmlHelper.AddAttribute(doc, xelItem, "Type", rm.GetString("txtFixed", ci));
                    XmlHelper.AddAttribute(doc, xelItem, "Amount", item.FixedAmount.ToString());
                    XmlHelper.AddAttribute(doc, xelItem, "Discount", item.FixedDiscountPercent.ToString());
                    xelItems.AppendChild(xelItem);
                }

                if (billingItem.MonthlyAmount > 0) {
                    xelItem = doc.CreateElement("AgreementItemsBLLC");
                    if (nativeCult) {
                        XmlHelper.AddAttribute(doc, xelItem, "Name", billingItem.ItemNameNative);
                    } else {
                        XmlHelper.AddAttribute(doc, xelItem, "Name", billingItem.ItemNameEN);
                    }
                    XmlHelper.AddAttribute(doc, xelItem, "BeginDate", item.BeginDate.ToShortDateString());
                    XmlHelper.AddAttribute(doc, xelItem, "EndDate", item.EndDate.ToShortDateString());
                    XmlHelper.AddAttribute(doc, xelItem, "Free", "");
                    XmlHelper.AddAttribute(doc, xelItem, "Type", rm.GetString("txtMonthly", ci));
                    XmlHelper.AddAttribute(doc, xelItem, "Amount", billingItem.MonthlyAmount.ToString());
                    XmlHelper.AddAttribute(doc, xelItem, "Discount", item.MonthlyDiscountPercent.ToString());
                    xelItems.AppendChild(xelItem);
                }

                if (billingItem.UsageAmount > 0) {
                    xelItem = doc.CreateElement("AgreementItemsBLLC");
                    if (nativeCult) {
                        XmlHelper.AddAttribute(doc, xelItem, "Name", billingItem.ItemNameNative);
                    } else {
                        XmlHelper.AddAttribute(doc, xelItem, "Name", billingItem.ItemNameEN);
                    }
                    XmlHelper.AddAttribute(doc, xelItem, "BeginDate", item.BeginDate.ToShortDateString());
                    XmlHelper.AddAttribute(doc, xelItem, "EndDate", item.EndDate.ToShortDateString());
                    XmlHelper.AddAttribute(doc, xelItem, "Free", item.Freezone.ToString());
                    XmlHelper.AddAttribute(doc, xelItem, "Type", rm.GetString("txtUsage", ci));
                    XmlHelper.AddAttribute(doc, xelItem, "Amount", billingItem.UsageAmount.ToString());
                    XmlHelper.AddAttribute(doc, xelItem, "Discount", item.UsageDiscountPercent.ToString());
                    xelItems.AppendChild(xelItem);
                }
            }
            return xelItems;
        }

        private XmlElement GetBillingAgreementComments(XmlDocument doc, int companyCIID, int nAgreementID) {
            if (!IsAgreementOpen(nAgreementID)) {
                agreement = factory.GetBillingAgreement(nAgreementID);
            }

            XmlElement xelComments = XmlHelper.GetXmlTable(doc, "AgreementComments", "tblAgreementComments");
            XmlHelper.AddAttribute(doc, xelComments, "Title", rm.GetString("txtAgreementComments", ci));
            XmlHelper.AddAttribute(doc, xelComments, "Comment", rm.GetString("txtComment", ci));
            XmlHelper.AddAttribute(doc, xelComments, "Date", rm.GetString("txtDate", ci));
            XmlHelper.AddAttribute(doc, xelComments, "Emails", rm.GetString("txtEmails", ci));

            foreach (AgreementCommentBLLC comment in agreement.CommentsOnAgreement) {
                xelComments.InnerXml += comment.ToXml();
            }

            return xelComments;
        }

        private XmlElement GetBillingAgreementNumber(XmlDocument doc, int companyCIID, int nAgreementID) {
            if (!IsAgreementOpen(nAgreementID)) {
                agreement = factory.GetBillingAgreement(nAgreementID);
            }

            XmlElement xmlElement = XmlHelper.GetXmlTable(doc, "AgreementNumber", "tblAgreementNumber");
            XmlHelper.AddAttribute(doc, xmlElement, "Title", rm.GetString("txtAgreementNumber", ci));
            XmlHelper.AddAttribute(doc, xmlElement, "AgreementNumber", agreement.AgreementNumber);

            return xmlElement;
        }

        private XmlElement GetBillingAgreementDescription(XmlDocument doc, int companyCIID, int nAgreementID) {
            if (!IsAgreementOpen(nAgreementID)) {
                agreement = factory.GetBillingAgreement(nAgreementID);
            }

            XmlElement xmlElement = XmlHelper.GetXmlTable(doc, "AgreementDescription", "tblAgreementDescription");
            XmlHelper.AddAttribute(doc, xmlElement, "Title", rm.GetString("txtAgreementDescription", ci));
            XmlHelper.AddAttribute(doc, xmlElement, "GeneralDescription", agreement.GeneralDescription);

            return xmlElement;
        }

        private XmlElement GetBillingAgreementFooter(XmlDocument doc, int companyCIID, int nAgreementID) {
            if (!IsAgreementOpen(nAgreementID)) {
                agreement = factory.GetBillingAgreement(nAgreementID);
            }

            XmlElement xmlElement = XmlHelper.GetXmlTable(doc, "AgreementFooter", "tblAgreementFooter");
            XmlHelper.AddAttribute(doc, xmlElement, "NotificationText_P1", rm.GetString("txtNotificationEmails_p1", ci));
            XmlHelper.AddAttribute(doc, xmlElement, "NotificationText_P2", rm.GetString("txtNotificationEmails_p2", ci));
            XmlHelper.AddAttribute(doc, xmlElement, "NotificationEmails", agreement.NotificationEmails);
            XmlHelper.AddAttribute(doc, xmlElement, "SignedByText", rm.GetString("txtSignedBy", ci));
            string strSignerName = factory.GetBillingEmployeeName(agreement.SignerCIID, nativeCult);
            XmlHelper.AddAttribute(doc, xmlElement, "SignedBy", strSignerName);
            XmlHelper.AddAttribute(doc, xmlElement, "ReminderOffset", agreement.ReminderOffset.ToString());

            return xmlElement;
        }

        #endregion
    }
}