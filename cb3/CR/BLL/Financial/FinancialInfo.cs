#region

using System;
using System.Collections;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Xml;
using CPI.BLL;
using FSI.BLL;
using FSI.Localization;
using Logging.BLL;
//using UserAdmin.BLL;

#endregion

namespace CR.BLL.Financial {
    /// <summary>
    /// FinancialInfo is a class for processing financial informations
    /// for Credit Info report.  It contains containers of data and methods to
    /// format the data on appropriate form.
    /// </summary>
    public class FinancialInfo {
        //head
        public const string ADMINISTRATIVE_OTHER_EXPENSES = "Administrative_other_expenses";
        public const string CAPITAL_FROM_OPERATIONS = "Net_cash_from_operating_activities";
        public const string CASH_FLOWS_FROM_FINANCING_ACTIVITIES = "Cash_Flow_From_Financing_Activities";
        public const string CASH_FLOWS_FROM_INVESTING_ACTIVITIES = "Cash_Flows_From_Investing_Activities";
        public const string CASH_FROM_OPERATIONS = "Cash_From_Operations";
        public const string COST_OF_SALES = "Cost_of_sales";
        public const string DEPRICATION = "Deprication_of_fixed_assets";
        public const string DEPRICATION_OF_FIXED_ASSETS = "Deprication_of_fixed_assets";
        public const string DISTRIBUTION_COST = "Distribution_cost";
        public const string EXTRAORDINARY_INCOME_COST = "Extraordinary_income_cost";
        public const string FINANCIAL_COST = "Financial_cost";
        public const string FINANCIAL_INCOME = "Financial_Income";
        public const string FINANCIAL_YEAR_END = "Financial_year_end";
        public const string GROSS_PROFIT = "Gross_profit";
        public const string INCOME = "Financial_income";
        public const string INCOME_FROM_ASSOCIATED_COMPANIES = "Income_From_Associated_Companies";
        public const string INCOME_FROM_OPERATIONS_AFTER_TAX = "Income_from_operations_after_tax";
        public const string INCOME_FROM_OPERATIONS_BEFORE_TAX = "Income_from_operations_before_tax";
        public const string INCR_DECR_IN_CASH = "Increase_or_decrease_in_cash";
        public const string NET_FINANCIAL_ITEMS = "Net_financial_items";
        public const string NET_RESULT = "Net_profit";

        public const string OPERATING_PROFIT_BEFORE_WORKING_CAPITAL_CHANGES =
            "Operating_profit_before_working_capital_changes";

        public const string OPERATIONAL_COST = "Total_costs";
        public const string OPERATIONAL_PROFIT = "Operating_profit";
        public const string OTHER_FINANCIAL_ITEMS = "Other_financial_items";
        public const string OTHER_OPERATING_REVENUE = "Other_operating_revenue";
        public const string PRE_TAX_PROFIT = "Income_from_operations_before_tax";
        public const string PROFIT_AND_LOSS_ACCOUNT = "Profit_And_Loss_Account";
        public const string QUALIFIED_AUDIT = "Qualified_audit";
        public const string REVENUE_FROM_MAIN_OPERATIONS = "Revenue_from_main_operations";
        public const string STAFF_COST = "Staff_cost";
        public const string TAX = "Taxes";
        public const string TAXES = "Taxes";
        public const string TOTAL_COSTS = "Total_costs";
        public const string TOTAL_REVENUE = "Total_revenue";
        public static string ACCOUNT_PERIOD_LENGTH = "Account_period_length";
        public static string ASSETS_TOTAL = "Total_assets";
        public static string CASH = "Cash";
        public static string CLAIMS_TOWARDS_ASSOCIATED_COMPANIES = "Claims_towards_associated_companies";
        public static string CREDITORS = "Creditors";
        public static string CURRENCY = "Currency";
        public static string CURRENT_ASSETS_TOTAL = "Total_current_assets";
        public static string DEBT_EQUITY_TOTAL = "Total_debts_and_equity";
        public static string DEFERRED_TAX_ASSETS = "Deferred_Tax_Assets";
        public static string EQUITY_TOTAL = "Total_equity";
        public static string FINANCIAL_YEAR = "Financial_year";
        public static string FINANCIALS_ASSETS = "Financial_assets";
        //Balance sheet
        // #1		
        public static string FIXED_ASSETS_TOTAL = "Total_fixed_assets";
        public static string INTANGIBLE_ASSETS = "Intangible_assets";
        public static string INVENTORY_STOCKS = "Inventory";
        public static string ISSUED_SHARE_CAPITAL = "Issued_share_capital";
        public static string LONG_TERM_DEBTS = "Long_term_debts";
        public static string MINORITY_HOLDINGS_IN_EQUITY = "Minority_holdings_in_equity";
        public static string MONTHS = "Account_period_length";
        public static string NEXT_YEAR_MATURITIES = "Short_term_investments";
        public static string OBLIGATIONS = "Debts_towards_associated_companies";
        public static string OTHER_CURRENT_ASSETS = "Other_current_assets";
        public static string OTHER_EQUITY = "Other_equity";
        public static string OTHER_LIABILITIES = "Other_Liabilities";
        public static string OTHER_SHORT_TERM_DEBTS = "Other_short_term_debt";
        public static string OVERDRAFT_BANK_LOAN = "Loans_overdraft";
        public static string RECEIVABLES = "Receivables_and_debtors";
        public static string SHORT_TERM_DEBTS = "Short_term_debts";
        public static string SHORT_TERM_DEBTS_TOTAL = "Total_short_term_debt";
        public static string TANGIBLE_ASSETS = "Tangible_assets";
        public static string TAX_LIABILITIES = "Tax_Liabilities";
        public static string TOTAL_DEBT = "Total_liabilities";
        //	public static string MINORITY_HOLDINGS_IN_EQUITY = "Total_equity";
        /// <summary> The culture info. </summary>
        protected CultureInfo ci;

        /// <summary> The class name. </summary>
        private string className = "BasicFinancialInfo";

        protected ResourceManager fsiRm;

        /// <summary> Type of the report to generate - defalt is basic report. </summary>
        private int reportType = ReportGenerator.companyProfileReportID;

        /// <summary> The resource manager </summary>
        protected ResourceManager rm;

        /// <summary> Contains collections of financial info for each year. </summary>
        public ArrayList years;

        /// <summary>
        /// Constructor - creates year container
        /// </summary>
        /// <param name="rm">The resource manager to use</param>
        /// <param name="ci">The culture info to use</param>
        public FinancialInfo(ResourceManager rm, CultureInfo ci) {
            years = new ArrayList();
            this.rm = rm;
            this.ci = ci;
            fsiRm = CIResource.CurrentManager;
        }

        /// <summary>
        /// Gets the number of years stored
        /// </summary>
        public int YearCount {
            get {
                if (years != null) {
                    return years.Count;
                } else {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of report to generate.
        /// </summary>
        public int ReportType { get { return reportType; } set { reportType = value; } }

        /// <summary>
        /// Returns true if the report to generate is basic report, false if not.
        /// </summary>
        protected bool isCompanyProfileReport { get { return reportType == ReportGenerator.companyProfileReportID; } }

        /// <summary>
        /// Returns true if the report to generate is corporate report, false if not.
        /// </summary>
        protected bool isCompanyReport { get { return reportType == ReportGenerator.companyReportID; } }

        /// <summary>
        /// Returns true if the report to generate is Credit Info report, false if not.
        /// </summary>
        protected bool isCreditInfoReport { get { return reportType == ReportGenerator.creditReportID; } }

        /// <summary>
        /// Creates a xml element that contains the balance sheet information
        /// formatted as a table
        /// </summary>
        /// <param name="doc">The document to create xml element from</param>
        /// <returns>The balance sheet formatted as a table</returns>
        public XmlElement getBalanceSheetTableAsXml(XmlDocument doc) {
            XmlElement balanceSheet = XmlHelper.GetXmlTable(doc, "tblBalanceSheet");

            //Header
            XmlElement headerRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtBalanceSheet", ci)));
            headerRow.AppendChild(hCol);
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_HEADER_COLUMN, "");
                col.AppendChild(XmlHelper.GetXmlBoldText(doc, GetValue(i, FINANCIAL_YEAR)));
                XmlHelper.AddAttribute(doc, col, "class", "header2");
                headerRow.AppendChild(col);
            }
            headerRow.AppendChild(XmlHelper.GetHeaderIndentColumn(doc));
            headerRow.AppendChild(XmlHelper.GetHeaderIndentColumn(doc));
            balanceSheet.AppendChild(headerRow);

            //Months
            XmlElement mRow = XmlHelper.GetXmlRow(doc);
            mRow.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
            for (int i = 0; i < YearCount; i++) {
                string sMonths = GetValue(i, MONTHS);
                if (sMonths.Length > 0 && int.Parse(sMonths) <= 12) {
                    XmlElement col = XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_HEADER_COLUMN, "");
                    col.AppendChild(XmlHelper.GetXmlBoldText(doc, sMonths + "." + rm.GetString("txtMonths", ci)));
                    mRow.AppendChild(col);
                }
            }
            mRow.AppendChild(XmlHelper.GetIndentColumn(doc));
            mRow.AppendChild(XmlHelper.GetIndentColumn(doc));
            balanceSheet.AppendChild(mRow);

            //Currency
            XmlElement cRow = XmlHelper.GetXmlRow(doc);
            cRow.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_HEADER_COLUMN, "");
                col.AppendChild(XmlHelper.GetXmlBoldText(doc, GetValue(i, CURRENCY)));
                cRow.AppendChild(col);
            }
            cRow.AppendChild(XmlHelper.GetIndentColumn(doc));
            cRow.AppendChild(XmlHelper.GetIndentColumn(doc));
            balanceSheet.AppendChild(cRow);

            //Current assets --------------------------------------------
            if (!isCompanyProfileReport) {
                if (isCreditInfoReport) {
                    balanceSheet.AppendChild(
                        XmlHelper.GetXmlRowWithValue(doc, new[] {rm.GetString("txtCurrentAssets", ci)}, true));

                    balanceSheet.AppendChild(getDarkRow(doc, "txtCash", CASH, false, true));
                    balanceSheet.AppendChild(getDarkRow(doc, "txtReceivables", RECEIVABLES, false, true));
                    balanceSheet.AppendChild(
                        getDarkRow(doc, "txtOtherCurrentAssets", OTHER_CURRENT_ASSETS, false, true));
                    balanceSheet.AppendChild(getDarkRow(doc, "txtInventoryStock", INVENTORY_STOCKS, false, true));
                }
                balanceSheet.AppendChild(getDarkRow(doc, "txtCurrentAssetsTotal", CURRENT_ASSETS_TOTAL, true, true));
                //Add a empty row
                if (isCreditInfoReport) // leave the empty row out for company report
                {
                    balanceSheet.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
                }
            }

            //Fixed assets ----------------------------------------------
            if (!isCompanyProfileReport) {
                if (isCreditInfoReport) {
                    // add headers only in creditReport
                    balanceSheet.AppendChild(
                        XmlHelper.GetXmlRowWithValue(doc, new[] {rm.GetString("txtFixedAssets", ci)}, true));

                    balanceSheet.AppendChild(getDarkRow(doc, "txtFinancialAssets", FINANCIALS_ASSETS, false, true));
                    balanceSheet.AppendChild(getDarkRow(doc, "txtTangibleAssets", TANGIBLE_ASSETS, false, true));
                    balanceSheet.AppendChild(getDarkRow(doc, "txtIntangibleAssets", INTANGIBLE_ASSETS, false, true));
                }
                balanceSheet.AppendChild(getDarkRow(doc, "txtFixedAssetsTotal", FIXED_ASSETS_TOTAL, true, true));
                //Add a empty row
                if (isCreditInfoReport) // leave the empty row out for company report
                {
                    balanceSheet.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
                }
            }

            //Assets total ----------------------------------------------
            balanceSheet.AppendChild(getRow(doc, "txtAssetsTotal", ASSETS_TOTAL, true, true));
            //Add a empty row
            balanceSheet.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            //Short term debts ------------------------------------------
            if (!isCompanyProfileReport) {
                if (isCreditInfoReport) {
                    balanceSheet.AppendChild(
                        XmlHelper.GetXmlRowWithValue(doc, new[] {rm.GetString("txtShortTermDebts", ci)}, true));

                    balanceSheet.AppendChild(getDarkRow(doc, "txtOverdraft_BankLoan", OVERDRAFT_BANK_LOAN, false, true));
                    balanceSheet.AppendChild(getDarkRow(doc, "txtCreditors", CREDITORS, false, true));
                    balanceSheet.AppendChild(
                        getDarkRow(doc, "txtOtherShortTermDebts", OTHER_SHORT_TERM_DEBTS, false, true));
                    balanceSheet.AppendChild(
                        getDarkRow(doc, "txtNextYearMaturities", NEXT_YEAR_MATURITIES, false, true));
                    balanceSheet.AppendChild(getDarkNBSPRow(doc));
                }
                balanceSheet.AppendChild(getDarkRow(doc, "txtShortTermDebtsTotal", SHORT_TERM_DEBTS_TOTAL, true, true));
                if (isCreditInfoReport) // leave the empty row out for company report
                {
                    balanceSheet.AppendChild(getDarkNBSPRow(doc));
                }
            }

            //Long term debts -------------------------------------------
            if (!isCompanyProfileReport) {
                balanceSheet.AppendChild(getDarkRow(doc, "txtLongTermDebts", LONG_TERM_DEBTS, true, true));
                if (isCreditInfoReport) {
                    balanceSheet.AppendChild(getDarkNBSPRow(doc));
                    balanceSheet.AppendChild(getDarkRow(doc, "txtObligations", OBLIGATIONS, true, true));
                    balanceSheet.AppendChild(getDarkNBSPRow(doc));
                    balanceSheet.AppendChild(getDarkNBSPRow(doc));
                }

                if (isCreditInfoReport) {
                    balanceSheet.AppendChild(getDarkRow(doc, "txtTotalDebt", TOTAL_DEBT, true, true));
                } else {
                    balanceSheet.AppendChild(getRow(doc, "txtTotalDebt", TOTAL_DEBT, true, true));
                }
                //Add a empty row
                balanceSheet.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
            }

            //Equity ----------------------------------------------------

            if (isCreditInfoReport) {
                balanceSheet.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new[] {rm.GetString("txtEquity", ci)}, true));
                balanceSheet.AppendChild(getDarkRow(doc, "txtIssuedShareCapital", ISSUED_SHARE_CAPITAL, false, true));
                balanceSheet.AppendChild(getDarkRow(doc, "txtOtherEquity", OTHER_EQUITY, false, true));
            }
            balanceSheet.AppendChild(getDarkRow(doc, "EquityTotal", EQUITY_TOTAL, true, true));
            if (isCreditInfoReport) {
                balanceSheet.AppendChild(
                    getDarkRow(doc, "txtMinorotyHoldingsInEquity", MINORITY_HOLDINGS_IN_EQUITY, false, true));
            }
            //Add a empty row
            if (isCreditInfoReport) // leave the empty row out for company report
            {
                balanceSheet.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
            }

            //Debt & equity total ---------------------------------------
            balanceSheet.AppendChild(getRow(doc, "txtDebtEquityTotal", DEBT_EQUITY_TOTAL, true, true));

            return balanceSheet;
        }

        public XmlElement getFinancialStatementAsXml(XmlDocument doc, int companyCIID, string afs_id) {
            XmlElement finStatement = XmlHelper.GetXmlTable(doc, "tblFinancialStatement");
            FinancialStatementFactory fsiFactory = new FinancialStatementFactory();
            string fsYear = fsiFactory.GetFinancialStatementYear(int.Parse(afs_id));
            int fsAccountPeriod = fsiFactory.GetFinancialStatementAccountPeriod(int.Parse(afs_id));
            if (fsYear != "-1" && fsAccountPeriod != -1) {
                FinancialStatementBLLC fsBll = fsiFactory.GetFinancialStatement(
                    companyCIID, int.Parse(fsYear), fsAccountPeriod);
                if (fsBll != null && fsBll.FiscalYear != "-1") {
                    CPIFactory factory = new CPIFactory();
                    ReportCompanyBLLC theCompany = factory.GetCompanyReport("", companyCIID, false);
                    finStatement.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new[] {theCompany.NameNative}, true));
                    finStatement.AppendChild(
                        XmlHelper.GetXmlRowWithValue(
                            doc, new[] {fsiRm.GetString("txtFinanciaStatement", ci) + " " + fsYear}, true));
                    finStatement.AppendChild(
                        XmlHelper.GetXmlRowWithValue(
                            doc,
                            new[] {fsiRm.GetString("txtYearEnded", ci) + " " + fsBll.YearEnded.ToShortDateString()},
                            true));
                    finStatement.AppendChild(
                        XmlHelper.GetXmlRowWithValue(
                            doc, new[] {rm.GetString("txtMonthsIncluded", ci) + " " + fsAccountPeriod}, false));

                    finStatement.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                    XmlElement row01 = XmlHelper.GetXmlRow(doc);
                    XmlElement col01 = XmlHelper.GetXmlColumn(doc, "");
                    row01.AppendChild(col01);
                    finStatement.AppendChild(row01);

                    if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US")) {
                        col01.AppendChild(
                            XmlHelper.GetXmlHeaderTableWithValues(
                                doc, "tblIncomeSheetHeader", fsiRm.GetString("txtIncomeSheet", ci), fsBll.CurrencyID));
                    } else {
                        col01.AppendChild(
                            XmlHelper.GetXmlHeaderTableWithValues(
                                doc, "tblIncomeSheetHeader", fsiRm.GetString("txtIncomeSheet", ci), fsBll.CurrencyID));
                    }

                    XmlElement row1 = XmlHelper.GetXmlRow(doc);
                    XmlElement col1 = XmlHelper.GetXmlColumn(doc, "");
                    row1.AppendChild(col1);
                    finStatement.AppendChild(row1);
                    col1.AppendChild(GetOperationalStatement(doc, fsBll));

                    finStatement.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                    XmlElement row02 = XmlHelper.GetXmlRow(doc);
                    XmlElement col02 = XmlHelper.GetXmlColumn(doc, "");
                    row02.AppendChild(col02);
                    finStatement.AppendChild(row02);

                    if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US")) {
                        col02.AppendChild(
                            XmlHelper.GetXmlHeaderTableWithValues(
                                doc,
                                "tblBalanceSheetASSETS",
                                fsiRm.GetString("txtBalanceSheetASSETS", ci),
                                fsBll.CurrencyID));
                    } else {
                        col02.AppendChild(
                            XmlHelper.GetXmlHeaderTableWithValues(
                                doc,
                                "tblBalanceSheetASSETS",
                                fsiRm.GetString("txtBalanceSheetASSETS", ci),
                                fsBll.CurrencyID));
                    }

                    XmlElement row2 = XmlHelper.GetXmlRow(doc);
                    XmlElement col2 = XmlHelper.GetXmlColumn(doc, "");
                    row2.AppendChild(col2);
                    finStatement.AppendChild(row2);

                    col2.AppendChild(GetBalanceSheetAssets(doc, fsBll));

                    finStatement.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                    XmlElement row03 = XmlHelper.GetXmlRow(doc);
                    XmlElement col03 = XmlHelper.GetXmlColumn(doc, "");
                    row03.AppendChild(col03);
                    finStatement.AppendChild(row03);

                    if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US")) {
                        col03.AppendChild(
                            XmlHelper.GetXmlHeaderTableWithValues(
                                doc,
                                "tblBalanceSheetEquityAndDebt",
                                fsiRm.GetString("txtBalanceSheetEquityAndDebt", ci),
                                fsBll.CurrencyID));
                    } else {
                        col03.AppendChild(
                            XmlHelper.GetXmlHeaderTableWithValues(
                                doc,
                                "tblBalanceSheetEquityAndDebt",
                                fsiRm.GetString("txtBalanceSheetEquityAndDebt", ci),
                                fsBll.CurrencyID));
                    }

                    XmlElement row3 = XmlHelper.GetXmlRow(doc);
                    XmlElement col3 = XmlHelper.GetXmlColumn(doc, "");
                    row3.AppendChild(col3);
                    finStatement.AppendChild(row3);

                    col3.AppendChild(GetBalanceSheetEquityAndDebt(doc, fsBll));

                    finStatement.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                    XmlElement row04 = XmlHelper.GetXmlRow(doc);
                    XmlElement col04 = XmlHelper.GetXmlColumn(doc, "");
                    row04.AppendChild(col04);
                    finStatement.AppendChild(row04);

                    if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US")) {
                        col04.AppendChild(
                            XmlHelper.GetXmlHeaderTableWithValues(
                                doc, "tblCashFlow", fsiRm.GetString("txtCashFlow", ci), fsBll.CurrencyID));
                    } else {
                        col04.AppendChild(
                            XmlHelper.GetXmlHeaderTableWithValues(
                                doc, "tblCashFlow", fsiRm.GetString("txtCashFlow", ci), fsBll.CurrencyID));
                    }

                    XmlElement row4 = XmlHelper.GetXmlRow(doc);
                    XmlElement col4 = XmlHelper.GetXmlColumn(doc, "");
                    row4.AppendChild(col4);
                    finStatement.AppendChild(row4);

                    col4.AppendChild(GetCashFlow(doc, fsBll));

                    finStatement.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

//					XmlElement row05 = XmlHelper.GetXmlRow(doc);
//					XmlElement col05 = XmlHelper.GetXmlColumn(doc, "");
//					row05.AppendChild(col05);
//					finStatement.AppendChild(row05);

//					if(Thread.CurrentThread.CurrentCulture.Name.Equals("en-US"))
//						col05.AppendChild(XmlHelper.GetXmlHeaderTableWithValues(doc, "tblOtherFields", fsiRm.GetString("txtOtherFields", ci)," "));
//					else
//						col05.AppendChild(XmlHelper.GetXmlHeaderTableWithValues(doc, "tblOtherFields", fsiRm.GetString("txtOtherFields", ci)," "));

//					XmlElement row5 = XmlHelper.GetXmlRow(doc);
//					XmlElement col5 = XmlHelper.GetXmlColumn(doc, "");
//					row5.AppendChild(col5);
//					finStatement.AppendChild(row5);

//					col5.AppendChild(GetOtherInformations(doc, fsBll));
                    return finStatement;
                }
            }
            return null;
        }

        private XmlElement GetOperationalStatement(XmlDocument doc, FinancialStatementBLLC bll) {
            XmlElement operTable = XmlHelper.GetXmlTable(doc, "tblOperationalStatement");
            operTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new[] {fsiRm.GetString("txtRevenue", ci)}, true));

            if (bll.RevenueFromMainOperation > int.MinValue && bll.RevenueFromMainOperation != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtRevenueFromMainOperations", ci),
                        formatNumber(bll.RevenueFromMainOperation.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtRevenueFromMainOperations", ci), "", false));			
            if (bll.CostOfSales > int.MinValue && bll.CostOfSales != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtCostOfSales", ci), formatNumber(bll.CostOfSales.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtCostOfSales", ci), "", false));
            if (bll.GrossProfit > int.MinValue && bll.GrossProfit != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtGrossProfit", ci), formatNumber(bll.GrossProfit.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtGrossProfit", ci), "0", false));

/*
			if(bll.TotalRevenue > int.MinValue)
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalRevenue", ci), formatNumber(bll.TotalRevenue.ToString()), true));
			else
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalRevenue", ci),"", true));
*/
            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            if (bll.OtherOperationRevenue > int.MinValue && bll.OtherOperationRevenue != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtOtherOperatingRevenue", ci),
                        formatNumber(bll.OtherOperationRevenue.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOtherOperatingRevenue", ci), "", false));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            operTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new[] {fsiRm.GetString("txtCost", ci)}, true));
            //if(bll.OtherOperationRevenue > int.MinValue && bll.OtherOperationRevenue !=0)
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOtherOperatingRevenue", ci), formatNumber(bll.OtherOperationRevenue.ToString()), false));
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOtherOperatingRevenue", ci), "", false));
            if (bll.StaffCost > int.MinValue && bll.StaffCost != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtStaffCost", ci), formatNumber(bll.StaffCost.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtStaffCost", ci), "", false));		
            if (bll.AdministrativeAndOtherExpenses > int.MinValue && bll.AdministrativeAndOtherExpenses != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtAdministrativeOtherExpenses", ci),
                        formatNumber(bll.AdministrativeAndOtherExpenses.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtAdministrativeOtherExpenses", ci), "", false));
            if (bll.DistributionCost > int.MinValue && bll.DistributionCost != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtDistributionCost", ci),
                        formatNumber(bll.DistributionCost.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtDistributionCost", ci), "", false));
            if (bll.DepricationOfFixedAssets > int.MinValue && bll.DepricationOfFixedAssets != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtDepricationOfFixedAssets", ci),
                        formatNumber(bll.DepricationOfFixedAssets.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtDepricationOfFixedAssets", ci), "", false));
            if (bll.TotalCost > int.MinValue && bll.TotalCost != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtTotalCost", ci), formatNumber(bll.TotalCost.ToString()), true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalCost", ci), "", true));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            if (bll.OperationalProfit > int.MinValue && bll.OperationalProfit != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtOperationalProfit", ci),
                        formatNumber(bll.OperationalProfit.ToString()),
                        true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOperationalProfit", ci), "", true));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
            //operTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new string[]{fsiRm.GetString("txtFinancialIncomeCosts", ci)}, true));

            if (bll.FinancialIncome > int.MinValue && bll.FinancialIncome != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtFinancialIncome", ci),
                        formatNumber(bll.FinancialIncome.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtFinancialIncome", ci), "", false));
            if (bll.FinancialCost > int.MinValue && bll.FinancialCost != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtFinancialCost", ci), formatNumber(bll.FinancialCost.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtFinancialCost", ci), "", false));
            if (bll.OtherFinancialEntries > int.MinValue && bll.OtherFinancialEntries != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtOtherFinancialItems", ci),
                        formatNumber(bll.OtherFinancialEntries.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOtherFinancialItems", ci), "", false));
            if (bll.NetFinancialItems > int.MinValue && bll.NetFinancialItems != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtNetFinancialItems", ci),
                        formatNumber(bll.NetFinancialItems.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtNetFinancialItems", ci), "", false));
            if (bll.IncomeFromOperationsBeforeTax > int.MinValue && bll.IncomeFromOperationsBeforeTax != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtIncomeFromOperationsBeforeTax", ci),
                        formatNumber(bll.IncomeFromOperationsBeforeTax.ToString()),
                        true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtIncomeFromOperationsBeforeTax", ci), "", true));
            if (bll.TaxLiabilities > int.MinValue && bll.TaxLiabilities.ToString().Trim().Equals("0")) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtTaxes", ci), formatNumber(bll.IncomeTax.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTaxes", ci), "", false));
            if (bll.IncomeFromOperationsAfterTax > int.MinValue && bll.IncomeFromOperationsAfterTax != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtIncomeFromOperationsAfterTax", ci),
                        formatNumber(bll.IncomeFromOperationsAfterTax.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtIncomeFromOperationsAfterTax", ci), "", false));
            if (bll.ExtraOrdinaryIncomeCost > int.MinValue && bll.ExtraOrdinaryIncomeCost != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtExtraordinaryIncomeCost", ci),
                        formatNumber(bll.ExtraOrdinaryIncomeCost.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtExtraordinaryIncomeCost", ci), "", false));
            if (bll.IncomeFromAssociatedCompanies > int.MinValue && bll.IncomeFromAssociatedCompanies != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtIncomeFromAssociatedCompanies", ci),
                        formatNumber(bll.IncomeFromAssociatedCompanies.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtIncomeFromAssociatedCompanies", ci), "", false));

            /*
			if(bll.ResultBeforeIncomeFromAssociatedCompanies > int.MinValue)
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtResultBeforeIncomeFromAssociatedCompanies", ci), formatNumber(bll.ResultBeforeIncomeFromAssociatedCompanies.ToString()), true));
			else
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtResultBeforeIncomeFromAssociatedCompanies", ci), "", true));

			if(bll.IncomeFromAssociatedCompanies > int.MinValue)
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtIncomeFromAssociatedCompanies", ci), formatNumber(bll.IncomeFromAssociatedCompanies.ToString()), false));
			else
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtIncomeFromAssociatedCompanies", ci), "", false));
			*/

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            if (bll.NetProfit > int.MinValue && bll.NetProfit != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtNetProfit", ci), formatNumber(bll.NetProfit.ToString()), true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtNetProfit", ci), "", true));
            /*
			if(bll.ProfitAndLossAccount > int.MinValue)
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtProfitOrLoss", ci), formatNumber(bll.ProfitAndLossAccount.ToString()), true));
			else
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtProfitOrLoss", ci), "", true));
			*/
            return operTable;
        }

        private XmlElement GetBalanceSheetAssets(XmlDocument doc, FinancialStatementBLLC bll) {
            XmlElement operTable = XmlHelper.GetXmlTable(doc, "tblBalanceSheetAssets");
            operTable.AppendChild(
                XmlHelper.GetXmlRowWithValue(doc, new[] {fsiRm.GetString("txtFixedAssets", ci)}, true));

            if (bll.IntangableAssets > int.MinValue && bll.IntangableAssets != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtIntangibleAssets", ci),
                        formatNumber(bll.IntangableAssets.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtIntangibleAssets", ci), "", false));
            if (bll.TangableAssets > int.MinValue && bll.TangableAssets != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtTangibleAssets", ci),
                        formatNumber(bll.TangableAssets.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTangibleAssets", ci), "", false));
            if (bll.FinancialAssets > int.MinValue && bll.FinancialAssets != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtFinancialAssets", ci),
                        formatNumber(bll.FinancialAssets.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtFinancialAssets", ci), "", false));
            if (bll.TotalFixedAssets > int.MinValue && bll.TotalFixedAssets != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtTotalFixedAssets", ci),
                        formatNumber(bll.TotalFixedAssets.ToString()),
                        true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalFixedAssets", ci), "", true));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
            //operTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new string[]{fsiRm.GetString("txtDereferedTaxations", ci)}, true));
            if (bll.DeferredTaxAsset > int.MinValue && bll.DeferredTaxAsset != 0)
            {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtDereferedTaxations", ci),
                        formatNumber(bll.DeferredTaxAsset.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtDereferedTaxations", ci), "", true));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
            operTable.AppendChild(
                XmlHelper.GetXmlRowWithValue(doc, new[] {fsiRm.GetString("txtCurrentAssets", ci)}, true));

            if (bll.Inventory > int.MinValue && bll.Inventory != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtInventory", ci), formatNumber(bll.Inventory.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtInventory", ci), "", false));
            if (bll.Debtors > int.MinValue && bll.Debtors != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtReceivablesDebtors", ci), formatNumber(bll.Debtors.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtReceivablesDebtors", ci), "", false));
            if (bll.ClaimsTowardsAssociatedCompanies > int.MinValue && bll.ClaimsTowardsAssociatedCompanies != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtClaimsTowardsAssociatedCompanies", ci),
                        formatNumber(bll.ClaimsTowardsAssociatedCompanies.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtClaimsTowardsAssociatedCompanies", ci), "", false));
            if (bll.ShortTermInvestments > int.MinValue && bll.ShortTermInvestments != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtShortTermInvestments", ci),
                        formatNumber(bll.ShortTermInvestments.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtShortTermInvestments", ci), "", false));
            if (bll.CashAtBankAndInHand > int.MinValue && bll.CashAtBankAndInHand != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtCash", ci), formatNumber(bll.CashAtBankAndInHand.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtCash", ci), "", false));
            if (bll.OtherCurrentAssets > int.MinValue && bll.OtherCurrentAssets != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtOtherCurrentAssets", ci),
                        formatNumber(bll.OtherCurrentAssets.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOtherCurrentAssets", ci), "", false));
            if (bll.TotalCurrentAssets > int.MinValue && bll.TotalCurrentAssets != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtTotalCurrentAssets", ci),
                        formatNumber(bll.TotalCurrentAssets.ToString()),
                        true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalCurrentAssets", ci), "", true));

            //operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
            ////operTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new string[]{fsiRm.GetString("txtDereferedTaxations", ci)}, true));
            //if (bll.DeferredTaxAsset > int.MinValue && bll.DeferredTaxAsset != 0) {
            //    operTable.AppendChild(
            //        XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
            //            doc,
            //            fsiRm.GetString("txtDereferedTaxations", ci),
            //            formatNumber(bll.DeferredTaxAsset.ToString()),
            //            true));
            //}
            ////else
            ////	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtDereferedTaxations", ci), "", true));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            if (bll.TotalAssets > int.MinValue && bll.TotalAssets != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtTotalAssets", ci), formatNumber(bll.TotalAssets.ToString()), true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalAssets", ci), "", true));

            return operTable;
        }

        private XmlElement GetBalanceSheetEquityAndDebt(XmlDocument doc, FinancialStatementBLLC bll) {
            XmlElement operTable = XmlHelper.GetXmlTable(doc, "tblBalanceSheetEquityAndDebt");
            operTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new[] {fsiRm.GetString("txtEquity", ci)}, true));

            if (bll.IssuedShareCapital > int.MinValue && bll.IssuedShareCapital != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtIssuedShareCapital", ci),
                        formatNumber(bll.IssuedShareCapital.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtIssuedShareCapital", ci), "", false));
            if (bll.ProfitAndLossAccount > int.MinValue && bll.ProfitAndLossAccount != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtNetProfit", ci),
                        formatNumber(bll.ProfitAndLossAccount.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtNetProfit", ci), "", false));
            if (bll.OtherEquity > int.MinValue && bll.OtherEquity != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtOtherEquity", ci), formatNumber(bll.OtherEquity.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOtherEquity", ci), "", false));
            if (bll.TotalEquity > int.MinValue && bll.TotalEquity != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtTotalEquity", ci), formatNumber(bll.TotalEquity.ToString()), true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalEquity", ci), "", true));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
            operTable.AppendChild(
                XmlHelper.GetXmlRowWithValue(doc, new[] {fsiRm.GetString("txtLiabilities", ci)}, true));

            if (bll.TaxLiabilities > int.MinValue && bll.TaxLiabilities != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtTaxLiabilities", ci),
                        formatNumber(bll.TaxLiabilities.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTaxLiabilities", ci), "", false));
            if (bll.OtherLiabilities > int.MinValue && bll.OtherLiabilities != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtOtherLiabilities", ci),
                        formatNumber(bll.OtherLiabilities.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOtherLiabilities", ci), "", false));
            /*
			if(bll.TotalLiabilities > int.MinValue)
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalLiabilities", ci), formatNumber(bll.TotalLiabilities.ToString()), true));
			else
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalLiabilities", ci), "", true));
			*/

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
//			operTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new string[]{fsiRm.GetString("txtLongTermDebts", ci)}, true));

            if (bll.LongTermDebts > int.MinValue && bll.LongTermDebts != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtLongTermDebts", ci), formatNumber(bll.LongTermDebts.ToString()), true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtLongTermDebts", ci), "", true));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            if (bll.ShortTermDebts > int.MinValue && bll.ShortTermDebts != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtShortTermDebts", ci), formatNumber(bll.ShortTermDebts.ToString()), true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtShortTermDebts", ci), "", true));

            //operTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new string[]{fsiRm.GetString("txtShortTermDebts", ci)}, true));			

            if (bll.LoansOverdraft > int.MinValue && bll.LoansOverdraft != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtLoansOverdraft", ci),
                        formatNumber(bll.LoansOverdraft.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtLoansOverdraft", ci), "", false));
            if (bll.Creditors > int.MinValue && bll.Creditors != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtCreditors", ci), formatNumber(bll.Creditors.ToString()), false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtCreditors", ci), "", false));
            if (bll.DebtToAssociatedCompanies > int.MinValue && bll.DebtToAssociatedCompanies != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtDebtTowardsAssociatedCompanies", ci),
                        formatNumber(bll.DebtToAssociatedCompanies.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtDebtTowardsAssociatedCompanies", ci), "", false));
            if (bll.OtherShortTermDebt > int.MinValue && bll.OtherShortTermDebt != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtOtherShortTermDebt", ci),
                        formatNumber(bll.OtherShortTermDebt.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOtherShortTermDebt", ci), "", false));

            if (bll.TotalShortTermDebts > int.MinValue && bll.TotalShortTermDebts != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtTotalShortTermDebt", ci),
                        formatNumber(bll.TotalShortTermDebts.ToString()),
                        true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalShortTermDebt", ci), "", true));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            //operTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new string[]{fsiRm.GetString("txtTotalDebts", ci)}, true));

            if (bll.TotalLiabilities > int.MinValue && bll.TotalLiabilities != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc, fsiRm.GetString("txtTotalDebts", ci), formatNumber(bll.TotalLiabilities.ToString()), true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalDebts", ci), "", true));

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            if (bll.TotalDebtsEquity > int.MinValue && bll.TotalDebtsEquity != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtTotalDebtsEquity", ci),
                        formatNumber(bll.TotalDebtsEquity.ToString()),
                        true));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtTotalDebtsEquity", ci), "", true));

            return operTable;
        }

        private XmlElement GetCashFlow(XmlDocument doc, FinancialStatementBLLC bll) {
            XmlElement operTable = XmlHelper.GetXmlTable(doc, "tblCashFlow");

            if (bll.OperatingProfitBeforeWorkingCapitalChanges > int.MinValue &&
                bll.OperatingProfitBeforeWorkingCapitalChanges != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtOperatingProfitBeforeWorkingCapitalChanges", ci),
                        formatNumber(bll.OperatingProfitBeforeWorkingCapitalChanges.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtOperatingProfitBeforeWorkingCapitalChanges", ci), "", false));	

            if (bll.CashFromOperations > int.MinValue && bll.CashFromOperations != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtCashFromOperations", ci),
                        formatNumber(bll.CashFromOperations.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtCashFromOperations", ci), "", false));		

            if (bll.CashFromOperations > int.MinValue && bll.CashFromOperations != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtNetCashFromOperatingActivities", ci),
                        formatNumber(bll.CashProvidedByOperatingActivities.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtNetCashFromOperatingActivities", ci), "", false));		

            if (bll.InvestingActivities > int.MinValue && bll.InvestingActivities != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtInvestingActivities", ci),
                        formatNumber(bll.InvestingActivities.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtInvestingActivities", ci), "", false));			

            if (bll.FinancialActivities > int.MinValue && bll.FinancialActivities != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtFinancialActivities", ci),
                        formatNumber(bll.FinancialActivities.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtFinancialActivities", ci), "", false));

            if (bll.IncreaseDecreaseInCashAndCashInHand > int.MinValue && bll.IncreaseDecreaseInCashAndCashInHand != 0) {
                operTable.AppendChild(
                    XmlHelper.GetFOXmlRowWith2ValuesSecondRight(
                        doc,
                        fsiRm.GetString("txtIncreaseDecreaseInCashAndCashInHand", ci),
                        formatNumber(bll.IncreaseDecreaseInCashAndCashInHand.ToString()),
                        false));
            }
            //else
            //	operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtIncreaseDecreaseInCashAndCashInHand", ci), "", false));

            return operTable;
        }

        private XmlElement GetOtherInformations(XmlDocument doc, FinancialStatementBLLC bll) {
            XmlElement operTable = XmlHelper.GetXmlTable(doc, "tblOtherInformations");

            /*
			if(bll.Qualified)
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtQualifiedAudit", ci), fsiRm.GetString("txtYes",ci), false));
			else
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtQualifiedAudit", ci), fsiRm.GetString("txtNo",ci), false));
			*/
            //operTable.AppendChild(XmlHelper.GetXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtQualifiedAudit", ci), bll.Qualified.ToString(), false));
            //operTable.AppendChild(XmlHelper.GetXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtQualifiedAudit", ci), bll.Qualified.ToString(), false));
            /*
			if(Thread.CurrentThread.CurrentCulture.Name.Equals("en-US"))
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtComments", ci), bll.CommentsEN, false)); //bll.CommentsID.ToString(), false));
			else
				operTable.AppendChild(XmlHelper.GetFOXmlRowWith2ValuesSecondRight(doc, fsiRm.GetString("txtComments", ci), bll.CommentsNative, false));
			*/

            operTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            return operTable;
        }

        /// <summary>
        /// Creates a xml element that contains the profit/loss account information
        /// formatted as a table
        /// </summary>
        /// <param name="doc">The document to create xml element from</param>
        /// <returns>The profit/loss account formatted as a table</returns>
        public XmlElement getProfitLossAccountAsXml(XmlDocument doc) {
            XmlElement plAccount = XmlHelper.GetXmlTable(doc, "tblProfitLossAccountt");

            //Header
            XmlElement headerRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtProfitLossAccount", ci)));
            headerRow.AppendChild(hCol);
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_HEADER_COLUMN, "");
                col.AppendChild(XmlHelper.GetXmlBoldText(doc, GetValue(i, FINANCIAL_YEAR)));
                XmlHelper.AddAttribute(doc, col, "class", "header2");
                headerRow.AppendChild(col);
            }
            headerRow.AppendChild(XmlHelper.GetHeaderIndentColumn(doc));
            headerRow.AppendChild(XmlHelper.GetHeaderIndentColumn(doc));
            plAccount.AppendChild(headerRow);

            //pofit/loss account
            // ! er h�r ! Breyta � total_revenue
            //	plAccount.AppendChild(getDarkRow(doc, "txtIncome", INCOME, false, true));
            plAccount.AppendChild(getDarkRow(doc, "txtIncome", TOTAL_REVENUE, false, true));
            if (!isCompanyProfileReport) {
                plAccount.AppendChild(getDarkRow(doc, "txtOperationalCost", OPERATIONAL_COST, false, true));
            }
            if (isCreditInfoReport) {
                plAccount.AppendChild(getDarkRow(doc, "txtDeprication", DEPRICATION, false, true));
            }
            if (!isCompanyProfileReport) {
                plAccount.AppendChild(getDarkRow(doc, "txtOperationalProfit", OPERATIONAL_PROFIT, false, true));
            }
            if (isCreditInfoReport) {
                plAccount.AppendChild(getDarkRow(doc, "txtNetFinancialItems", NET_FINANCIAL_ITEMS, false, true));
                plAccount.AppendChild(getDarkRow(doc, "txtPreTaxProfit", PRE_TAX_PROFIT, false, true));
                plAccount.AppendChild(getDarkRow(doc, "txtTaxes", TAXES, false, true));
            }

            plAccount.AppendChild(getDarkRow(doc, "txtNetResult", NET_RESULT, false, true));

            if (isCreditInfoReport) {
                //Add a empty dark row
                plAccount.AppendChild(getDarkNBSPRow(doc));

                //Cash flow ------------------------------------
                XmlElement dRow = XmlHelper.GetXmlDarkRowWithValue(doc, new[] {rm.GetString("txtCashFlow", ci)}, true);
                for (int i = 0; i < YearCount; i++) {
                    dRow.AppendChild(XmlHelper.GetXmlDarkNBSPColumn(doc));
                }

                dRow.AppendChild(XmlHelper.GetDarkIndentColumn(doc));
                plAccount.AppendChild(dRow);

                plAccount.AppendChild(getDarkRow(doc, "txtCapitalFromOperations", CAPITAL_FROM_OPERATIONS, false, true));
                plAccount.AppendChild(getDarkRow(doc, "txtCashFromOperations", CASH_FROM_OPERATIONS, false, true));
                plAccount.AppendChild(getDarkRow(doc, "txtIncrDescInCash", INCR_DECR_IN_CASH, false, true));
            }

            // Ratios -------------------------------------------
            plAccount.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            XmlElement ratiosRow = XmlHelper.GetXmlRow(doc);
            XmlElement rCol = XmlHelper.GetXmlColumn(doc, "");
            rCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtRatios", ci)));
            ratiosRow.AppendChild(rCol);
            for (int i = 0; i < YearCount; i++) {
                ratiosRow.AppendChild(XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, GetValue(i, FINANCIAL_YEAR)));
            }
            plAccount.AppendChild(ratiosRow);
            plAccount.AppendChild(getProfitMarginRow(doc));
            plAccount.AppendChild(getCurrentRatioRow(doc));
            plAccount.AppendChild(getQuickRatioRow(doc));
            plAccount.AppendChild(getEquityRatioRow(doc));
            plAccount.AppendChild(getDebtRatioRow(doc));
            plAccount.AppendChild(getCollectionTimeRow(doc));
            plAccount.AppendChild(getStockTurnoverRow(doc));

            // Auditted -----------------------------------------
            plAccount.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            XmlElement audittedRow = XmlHelper.GetXmlRow(doc);
            XmlElement aCol = XmlHelper.GetXmlColumn(doc, "");
            aCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtAuditted", ci)));
            audittedRow.AppendChild(aCol);

            for (int i = 0; i < YearCount; i++) {
                audittedRow.AppendChild(
                    XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, GetValue(i, QUALIFIED_AUDIT)));
            }

            plAccount.AppendChild(audittedRow);
            return plAccount;
        }

        protected XmlElement getProfitMarginRow(XmlDocument doc) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetXmlDarkColumn(doc, rm.GetString("txtProfitMargin", ci)));
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlDarkColumn(doc, getProfitMargin(i));
                XmlHelper.AddAttribute(doc, col, "align", "right");
                row.AppendChild(col);
            }
            row.AppendChild(XmlHelper.GetDarkIndentColumn(doc));
            row.AppendChild(XmlHelper.GetIndentColumn(doc));
            return row;
        }

        protected XmlElement getCurrentRatioRow(XmlDocument doc) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetXmlDarkColumn(doc, rm.GetString("txtCurrentRatio", ci)));
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlDarkColumn(doc, getCurrentRatio(i));
                XmlHelper.AddAttribute(doc, col, "align", "right");
                row.AppendChild(col);
            }
            row.AppendChild(XmlHelper.GetDarkIndentColumn(doc));
            row.AppendChild(XmlHelper.GetIndentColumn(doc));
            return row;
        }

        protected XmlElement getQuickRatioRow(XmlDocument doc) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetXmlDarkColumn(doc, rm.GetString("txtQuickRatio", ci)));
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlDarkColumn(doc, getQuickRatio(i));
                XmlHelper.AddAttribute(doc, col, "align", "right");
                row.AppendChild(col);
            }
            row.AppendChild(XmlHelper.GetDarkIndentColumn(doc));
            row.AppendChild(XmlHelper.GetIndentColumn(doc));
            return row;
        }

        protected XmlElement getEquityRatioRow(XmlDocument doc) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetXmlDarkColumn(doc, rm.GetString("txtEquityRatio", ci)));
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlDarkColumn(doc, getEquityRatio(i));
                XmlHelper.AddAttribute(doc, col, "align", "right");
                row.AppendChild(col);
            }
            row.AppendChild(XmlHelper.GetDarkIndentColumn(doc));
            row.AppendChild(XmlHelper.GetIndentColumn(doc));
            return row;
        }

        protected XmlElement getDebtRatioRow(XmlDocument doc) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetXmlDarkColumn(doc, rm.GetString("txtDebtRatio", ci)));
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlDarkColumn(doc, getDebtRatio(i));
                XmlHelper.AddAttribute(doc, col, "align", "right");
                row.AppendChild(col);
            }
            row.AppendChild(XmlHelper.GetDarkIndentColumn(doc));
            row.AppendChild(XmlHelper.GetIndentColumn(doc));
            return row;
        }

        protected XmlElement getCollectionTimeRow(XmlDocument doc) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetXmlDarkColumn(doc, rm.GetString("txtCollectionTime", ci)));
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlDarkColumn(doc, getCollectionTime(i));
                XmlHelper.AddAttribute(doc, col, "align", "right");
                row.AppendChild(col);
            }
            row.AppendChild(XmlHelper.GetDarkIndentColumn(doc));
            row.AppendChild(XmlHelper.GetIndentColumn(doc));
            return row;
        }

        protected XmlElement getStockTurnoverRow(XmlDocument doc) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetXmlDarkColumn(doc, rm.GetString("txtStockTurnover", ci)));
            for (int i = 0; i < YearCount; i++) {
                XmlElement col = XmlHelper.GetXmlDarkColumn(doc, getStockTurnover(i));
                XmlHelper.AddAttribute(doc, col, "align", "right");
                row.AppendChild(col);
            }
            row.AppendChild(XmlHelper.GetDarkIndentColumn(doc));
            row.AppendChild(XmlHelper.GetIndentColumn(doc));
            return row;
        }

        private NumberFormatInfo getPercentageFormat() {
            NumberFormatInfo fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            fInfo.PercentDecimalDigits = 2;
            fInfo.NaNSymbol = "N/A";
            fInfo.PercentPositivePattern = 0;
            fInfo.PercentNegativePattern = 0;
            return fInfo;
        }

        private string getProfitMargin(int yearIndex) {
            try {
                //Profit/Loss account
                //profit / turnover
                //		string income = GetValue(yearIndex, INCOME);
                string income = GetValue(yearIndex, TOTAL_REVENUE);
                string netResult = GetValue(yearIndex, NET_RESULT);
                double dIncome = double.Parse(income);
                double dNetResult = double.Parse(netResult);
                if (dIncome > 0) {
                    double result = (dNetResult/dIncome);

                    return result.ToString("P", getPercentageFormat());
                } else {
                    return "N/A";
                }
            } catch {
                return "N/A";
            }
        }

        private string getCurrentRatio(int yearIndex) {
            try {
                //From Balance sheet
                //Current assets / Current debt
                double dCurrAssets = double.Parse(GetValue(yearIndex, CURRENT_ASSETS_TOTAL));
                double dCurrDebt = double.Parse(GetValue(yearIndex, SHORT_TERM_DEBTS_TOTAL));
                if (dCurrDebt > 0) {
                    double result = (dCurrAssets/dCurrDebt);
                    //profit / turnover
                    return result.ToString("N");
                } else {
                    return "N/A";
                }
            } catch {
                return "N/A";
            }
        }

        private string getQuickRatio(int yearIndex) {
            //From Balance sheet
            //(Current assets-stocks) / Current debt
            try {
                double dCurrAssets = double.Parse(GetValue(yearIndex, CURRENT_ASSETS_TOTAL));
                double dStocks = double.Parse(GetValue(yearIndex, INVENTORY_STOCKS));
                double dCurrDebt = double.Parse(GetValue(yearIndex, SHORT_TERM_DEBTS_TOTAL));
                if (dCurrDebt > 0) {
                    double dResult = ((dCurrAssets - dStocks)/dCurrDebt);
                    return dResult.ToString("N");
                } else {
                    return "N/A";
                }
            } catch {
                return "N/A";
            }
        }

        private string getEquityRatio(int yearIndex) {
            //From product sheet
            //Equity / Total assets
            try {
                double dEquity = double.Parse(GetValue(yearIndex, EQUITY_TOTAL));
                //double dTAssets = double.Parse(GetValue(yearIndex, TOTAL_DEBT));
                double dTAssets = double.Parse(GetValue(yearIndex, ASSETS_TOTAL));
                if (dTAssets > 0) {
                    double dResult = dEquity/dTAssets;
                    return dResult.ToString("P", getPercentageFormat());
                } else {
                    return "N/A";
                }
            } catch {
                return "N/A";
            }
        }

        private string getDebtRatio(int yearIndex) {
            //From Balance sheet
            //Debt / Total(debts and equity)
            try {
                double dTotalDebtsAndEquity = double.Parse(GetValue(yearIndex, DEBT_EQUITY_TOTAL));
                double dTotalDebt = double.Parse(GetValue(yearIndex, TOTAL_DEBT));
                if (dTotalDebt > 0) {
                    // 21.09 Breytti �r �essu 
                    //	double dResult = ((dSDebt+dLDebts+dObligations)/dTotalDebt);
                    // � �etta
                    double dResult = (dTotalDebt/dTotalDebtsAndEquity);
                    return dResult.ToString("P", getPercentageFormat());
                } else {
                    return "N/A";
                }
            } catch {
                return "N/A";
            }
        }

        private string getInterestCoverRatio(int yearIndex) {
            //Profit before interest and tax / Interest payable
            //PRE_TAX_PROFIT / FINANCIAL_COST
            try {
                double dPTP = double.Parse(GetValue(yearIndex, PRE_TAX_PROFIT));
                double dFC = double.Parse(GetValue(yearIndex, FINANCIAL_COST));

                if (dFC > 0) {
                    double dResult = (dPTP/dFC);
                    return dResult.ToString("P", getPercentageFormat());
                } else {
                    return "N/A";
                }
            } catch {
                return "N/A";
            }
        }

        private string getROCERatio(int yearIndex) {
            //Net profit / (Long term debt + Total equity)
            //NET_RESULT / (LONG_TERM_DEBTS+EQUITY_TOTAL)
            try {
                double dNR = double.Parse(GetValue(yearIndex, NET_RESULT));
                double dLTD = double.Parse(GetValue(yearIndex, LONG_TERM_DEBTS));
                double dET = double.Parse(GetValue(yearIndex, EQUITY_TOTAL));
                if ((dLTD + dET) > 0) {
                    double dResult = (dNR/(dLTD + dET));
                    return dResult.ToString("N");
                } else {
                    return "N/A";
                }
            } catch {
                return "N/A";
            }
        }

        private string getCollectionTime(int yearIndex) {
            //((Account payables 01.01 + Account payables 31.12) / 2) / (Turnover / 365)
            try {
                double dAccountPayable0101 = double.Parse(GetValue(yearIndex + 1, RECEIVABLES));
                double dAccountPayable3112 = double.Parse(GetValue(yearIndex, RECEIVABLES));
                double dTurnover = double.Parse(GetValue(yearIndex, TOTAL_REVENUE));
                if (dTurnover > 0) {
                    double dResult = (((dAccountPayable0101 + dAccountPayable3112)/2)/(dTurnover/365));
                    return dResult.ToString("N");
                } else {
                    return "N/A";
                }
            } catch {
                return "N/A";
            }
        }

        private string getStockTurnover(int yearIndex) {
            //Product purchases / (Stock 01.01 + Stock 31.12) / 2
            return "N/A";

            /*try
			{
				if(yearIndex>0)
				{
					double dProductsPurchased = double.Parse(GetValue(yearIndex, SHORT_TERM_DEBTS_TOTAL));
					double dStock0101 = double.Parse(GetValue(yearIndex+1, INVENTORY_STOCKS));
					double dStock3112 = double.Parse(GetValue(yearIndex, INVENTORY_STOCKS));
					if((dStock0101+dStock3112)>0)
					{
						double dResult = (dProductsPurchased/((dStock0101+dStock3112)/2));
						return dResult.ToString("N");

					}
					else return "NA";
				}else return "NA";

			}
			catch{return "NA";}*/
        }

        /// <summary>
        /// Creates a dark row with given text key and data from given column
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="text">Key of the text to add to row</param>
        /// <param name="columnName">Column name of the financial value to add to row</param>
        /// <returns>The row</returns>
        protected XmlElement getDarkRow(XmlDocument doc, string text, string columnName) { return getDarkRow(doc, text, columnName, false, false); }

        /// <summary>
        /// Creates a dark row with given text key and data from given column
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="text">Key of the text to add to row</param>
        /// <param name="columnName">Column name of the financial value to add to row</param>
        /// <param name="firstBold">True if the text should be bold</param>
        /// <param name="formatNumbers">True if to format financial value with "," as a thousand separator</param>
        /// <returns>The row</returns>
        protected XmlElement getDarkRow(
            XmlDocument doc, string text, string columnName, bool firstBold, bool formatNumbers) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            if (firstBold) {
                XmlElement col = XmlHelper.GetXmlDarkColumn(doc, "");
                col.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString(text, ci)));
                row.AppendChild(col);
            } else {
                row.AppendChild(XmlHelper.GetXmlDarkColumn(doc, rm.GetString(text, ci)));
            }
            for (int i = 0; i < YearCount; i++) {
                string val = GetValue(i, columnName);
                if (val != "") {
                    if (formatNumbers) {
                        val = formatNumber(val);
                    }
                }

                XmlElement col = XmlHelper.GetXmlDarkColumn(doc, val);
                XmlHelper.AddAttribute(doc, col, "align", "right");
                row.AppendChild(col);
            }
            row.AppendChild(XmlHelper.GetDarkIndentColumn(doc));
            row.AppendChild(XmlHelper.GetIndentColumn(doc));
            return row;
        }

        /// <summary>
        /// Creates a empty dark row
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <returns>The row</returns>
        protected XmlElement getDarkNBSPRow(XmlDocument doc) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            for (int i = 0; i < YearCount + 2; i++) {
                row.AppendChild(XmlHelper.GetXmlDarkNBSPColumn(doc));
            }
            return row;
        }

        /// <summary>
        /// Creates a row with given text key and data from given column
        /// </summary>
        /// <param name="doc">The document to create row from</param>
        /// <param name="text">Key of the text to add to row</param>
        /// <param name="columnName">Column name of the financial value to add to row</param>
        /// <param name="firstBold">True if the text should be bold</param>
        /// <param name="formatNumbers">True if to format financial value with "," as a thousand separator</param>
        /// <returns>The row</returns>
        protected XmlElement getRow(XmlDocument doc, string text, string columnName, bool firstBold, bool formatNumbers) {
            XmlElement row = XmlHelper.GetXmlRow(doc);
            if (firstBold) {
                XmlElement col = XmlHelper.GetXmlColumn(doc, "");
                col.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString(text, ci)));
                row.AppendChild(col);
            } else {
                row.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString(text, ci)));
            }
            for (int i = 0; i < YearCount; i++) {
                if (formatNumbers) {
                    row.AppendChild(
                        XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, formatNumber(GetValue(i, columnName))));
                } else {
                    row.AppendChild(XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, GetValue(i, columnName)));
                }
            }
            return row;
        }

        /// <summary>
        /// Formats given number with a "," as a thousand separator
        /// </summary>
        /// <param name="number">The number to format</param>
        /// <returns>The given number formatted</returns>
        public string formatNumber(string number) {
            if (number == null || number.Trim().Length == 0) {
                return "";
            }
            double dNumber = double.Parse(number);
            NumberFormatInfo fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            fInfo.NumberDecimalDigits = 0;
            return dNumber.ToString("N", fInfo);

            /*string tmpNumber = number;
			string retNumber = "";
			while(tmpNumber.Length>3)
			{
				retNumber = tmpNumber.Substring(tmpNumber.Length-3, 3)+retNumber;
				tmpNumber = tmpNumber.Remove(tmpNumber.Length-3, 3);
				if(!tmpNumber.Equals("-"))
					retNumber = ","+retNumber;
			}
			return tmpNumber+retNumber;*/
        }

        /// <summary>
        /// Sets a financial value into container
        /// </summary>
        /// <param name="yearIndex">Index of the year the value belongs to</param>
        /// <param name="key">Key for the value</param>
        /// <param name="val">The value</param>
        /// <returns>True if succeeded storing the value, false of not</returns>
        public bool SetValue(int yearIndex, string key, string val) {
            string funcName = "SetValue(yearIndex, key, val)";
            try {
                Hashtable h;
                if (years.Count > yearIndex) {
                    Object obj = years[yearIndex];
                    if (obj != null) {
                        h = (Hashtable) obj;
                    } else {
                        h = new Hashtable();
                    }
                } else {
                    h = new Hashtable();
                    years.Add(h);
                }
                h[key] = val;
                years[yearIndex] = h;
                return true;
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }
        }

        /// <summary>
        /// Retuns a value from container
        /// </summary>
        /// <param name="yearIndex">Index of the year to return value from</param>
        /// <param name="key">Key of the value</param>
        /// <returns>The value if found, empty string if not found</returns>
        public string GetValue(int yearIndex, string key) {
            string funcName = "GetValue(yearIndex, key)";
            try {
                if (years.Count > yearIndex) {
                    Object obj = years[yearIndex];
                    if (obj != null) {
                        Hashtable h = (Hashtable) obj;
                        return (string) h[key];
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return "";
        }

        public XmlElement GetBalanceSheetTemplate(XmlDocument doc) {
            ArrayList elements = new ArrayList();
            elements.Add(INVENTORY_STOCKS);
            elements.Add(RECEIVABLES);
            elements.Add(CLAIMS_TOWARDS_ASSOCIATED_COMPANIES);
            elements.Add(CASH);
            elements.Add(OTHER_CURRENT_ASSETS);
            elements.Add(CURRENT_ASSETS_TOTAL);
            elements.Add(NEXT_YEAR_MATURITIES);
            elements.Add(FINANCIALS_ASSETS);
            elements.Add(TANGIBLE_ASSETS);
            elements.Add(INTANGIBLE_ASSETS);
            elements.Add(FIXED_ASSETS_TOTAL);
            elements.Add(DEFERRED_TAX_ASSETS);
            elements.Add(ASSETS_TOTAL);
            elements.Add(OVERDRAFT_BANK_LOAN);
            elements.Add(CREDITORS);
            elements.Add(OTHER_SHORT_TERM_DEBTS);
            elements.Add(NEXT_YEAR_MATURITIES);
            elements.Add(SHORT_TERM_DEBTS);
            elements.Add(SHORT_TERM_DEBTS_TOTAL);
            elements.Add(LONG_TERM_DEBTS);
            elements.Add(OBLIGATIONS);
            elements.Add(TOTAL_DEBT);
            elements.Add(ISSUED_SHARE_CAPITAL);
            elements.Add(OTHER_EQUITY);
            elements.Add(PROFIT_AND_LOSS_ACCOUNT);
            elements.Add(EQUITY_TOTAL);
            elements.Add(TAX_LIABILITIES);
            elements.Add(OTHER_LIABILITIES);
            elements.Add(MINORITY_HOLDINGS_IN_EQUITY);
            elements.Add(DEBT_EQUITY_TOTAL);

            if (IsTemplateNull(elements)) {
                //return null;
                XmlElement nothing = XmlHelper.GetXmlTable(doc, "tblNothing", "tblNothing");
                return nothing;
            }

            XmlElement balanceSheet = XmlHelper.GetXmlTable(doc, "BalanceSheet", "tblBalanceSheet");
            XmlHelper.AddAttribute(doc, balanceSheet, "title", rm.GetString("txtBalanceSheet", ci));

            XmlElement years = XmlHelper.CreateElement(doc, "Years", "");
            balanceSheet.AppendChild(years);

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Year", GetValue(i, FINANCIAL_YEAR));
                years.AppendChild(item);
            }

            //Year end
            XmlElement yearEnd = XmlHelper.CreateElement(doc, "YearEnd", "");
            XmlHelper.AddAttribute(doc, yearEnd, "Title", rm.GetString("txtYearEnded", ci));
            balanceSheet.AppendChild(yearEnd);
            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                string val = GetValue(i, FINANCIAL_YEAR_END);
                if (val.Length >= 10) {
                    val = val.Substring(0, 10);
                }
                XmlHelper.AddAttribute(doc, item, "YearEnd", val);
                yearEnd.AppendChild(item);
            }

            //Months
            XmlElement months = XmlHelper.CreateElement(doc, "Months", "");
            XmlHelper.AddAttribute(doc, months, "Title", rm.GetString("txtFSIMonths", ci));
            balanceSheet.AppendChild(months);
            for (int i = 0; i < YearCount; i++) {
                string sMonths = GetValue(i, MONTHS);
                if (sMonths.Length > 0) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "Month", sMonths + " " + rm.GetString("txtMonths", ci));
                    months.AppendChild(item);
                }
            }

            //Currency
            XmlElement currencys = XmlHelper.CreateElement(doc, "Currency", "");
            XmlHelper.AddAttribute(doc, currencys, "Title", rm.GetString("txtCurrency", ci));
            balanceSheet.AppendChild(currencys);
            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Currency", GetValue(i, CURRENCY));
                currencys.AppendChild(item);
            }

            //Current assets --------------------------------------------

            XmlElement currentAssets = XmlHelper.CreateElement(doc, "CurrentAssets", "");

            elements = new ArrayList();
            elements.Add(INVENTORY_STOCKS);
            elements.Add(RECEIVABLES);
            elements.Add(CLAIMS_TOWARDS_ASSOCIATED_COMPANIES);
            elements.Add(CASH);
            elements.Add(OTHER_CURRENT_ASSETS);
            elements.Add(CURRENT_ASSETS_TOTAL);
            elements.Add(NEXT_YEAR_MATURITIES);
            if (IsTemplateNull(elements)) {
                XmlHelper.AddAttribute(doc, currentAssets, "Title", "");
            } else {
                XmlHelper.AddAttribute(doc, currentAssets, "Title", rm.GetString("txtCurrentAssets", ci));
            }

            balanceSheet.AppendChild(currentAssets);

            if (GetFSValue(doc, "InventoryStock", "txtInventoryStock", INVENTORY_STOCKS, true) != null) {
                currentAssets.AppendChild(
                    GetFSValue(doc, "InventoryStock", "txtInventoryStock", INVENTORY_STOCKS, true));
            }
            if (GetFSValue(doc, "Receivables", "txtReceivables", RECEIVABLES, true) != null) {
                currentAssets.AppendChild(GetFSValue(doc, "Receivables", "txtReceivables", RECEIVABLES, true));
            }
            if (
                GetFSValue(
                    doc,
                    "ClaimsTowardsAssociatedCompanies",
                    "txtClaimsTowardsAssociatedCompanies",
                    CLAIMS_TOWARDS_ASSOCIATED_COMPANIES,
                    true) != null) {
                currentAssets.AppendChild(
                    GetFSValue(
                        doc,
                        "ClaimsTowardsAssociatedCompanies",
                        "txtClaimsTowardsAssociatedCompanies",
                        CLAIMS_TOWARDS_ASSOCIATED_COMPANIES,
                        true));
            }

            if (GetFSValue(doc, "Cash", "txtCash", CASH, true) != null) {
                currentAssets.AppendChild(GetFSValue(doc, "Cash", "txtCash", CASH, true));
            }
            if (GetFSValue(doc, "OtherCurrentAssets", "txtOtherCurrentAssets", OTHER_CURRENT_ASSETS, true) != null) {
                currentAssets.AppendChild(
                    GetFSValue(doc, "OtherCurrentAssets", "txtOtherCurrentAssets", OTHER_CURRENT_ASSETS, true));
            }
            if (GetFSValue(doc, "CurrentAssetsTotal", "txtCurrentAssetsTotal", CURRENT_ASSETS_TOTAL, true) != null) {
                currentAssets.AppendChild(
                    GetFSValue(doc, "CurrentAssetsTotal", "txtCurrentAssetsTotal", CURRENT_ASSETS_TOTAL, true));
            }
            if (GetFSValue(doc, "ShortTermInvestments", "txtShortTermInvestments", NEXT_YEAR_MATURITIES, true) != null) {
                currentAssets.AppendChild(
                    GetFSValue(doc, "ShortTermInvestments", "txtShortTermInvestments", NEXT_YEAR_MATURITIES, true));
            }

            //Fixed assets ----------------------------------------------
            XmlElement fixedAssets = XmlHelper.CreateElement(doc, "FixedAssets", "");

            elements = new ArrayList();
            elements.Add(FINANCIALS_ASSETS);
            elements.Add(TANGIBLE_ASSETS);
            elements.Add(INTANGIBLE_ASSETS);
            elements.Add(FIXED_ASSETS_TOTAL);
            if (IsTemplateNull(elements)) {
                XmlHelper.AddAttribute(doc, fixedAssets, "Title", "");
            } else {
                XmlHelper.AddAttribute(doc, fixedAssets, "Title", rm.GetString("txtFixedAssets", ci));
            }

            balanceSheet.AppendChild(fixedAssets);

            if (GetFSValue(doc, "FinancialAssets", "txtFinancialAssets", FINANCIALS_ASSETS, true) != null) {
                fixedAssets.AppendChild(
                    GetFSValue(doc, "FinancialAssets", "txtFinancialAssets", FINANCIALS_ASSETS, true));
            }
            if (GetFSValue(doc, "TangibleAssets", "txtTangibleAssets", TANGIBLE_ASSETS, true) != null) {
                fixedAssets.AppendChild(GetFSValue(doc, "TangibleAssets", "txtTangibleAssets", TANGIBLE_ASSETS, true));
            }
            if (GetFSValue(doc, "IntangibleAssets", "txtIntangibleAssets", INTANGIBLE_ASSETS, true) != null) {
                fixedAssets.AppendChild(
                    GetFSValue(doc, "IntangibleAssets", "txtIntangibleAssets", INTANGIBLE_ASSETS, true));
            }
            if (GetFSValue(doc, "FixedAssetsTotal", "txtFixedAssetsTotal", FIXED_ASSETS_TOTAL, true) != null) {
                fixedAssets.AppendChild(
                    GetFSValue(doc, "FixedAssetsTotal", "txtFixedAssetsTotal", FIXED_ASSETS_TOTAL, true));
            }

            //Deferred tax assets ---------------------------------------
            XmlElement deferredTaxAssets = XmlHelper.CreateElement(doc, "DeferredTaxAssets", "");
            XmlHelper.AddAttribute(doc, deferredTaxAssets, "Title", rm.GetString("txtDeferredTaxAssets", ci));
            balanceSheet.AppendChild(deferredTaxAssets);

            if (GetFSValue(doc, "DeferredTaxAssets", "txtDeferredTaxAssets", DEFERRED_TAX_ASSETS, true) != null) {
                deferredTaxAssets.AppendChild(
                    GetFSValue(doc, "DeferredTaxAssets", "txtDeferredTaxAssets", DEFERRED_TAX_ASSETS, true));
            }

            //Assets total ----------------------------------------------
            XmlElement assetsTotal = XmlHelper.CreateElement(doc, "AssetsTotal", "");
            XmlHelper.AddAttribute(doc, assetsTotal, "Title", rm.GetString("txtAssetsTotal", ci));
            balanceSheet.AppendChild(assetsTotal);

            if (GetFSValue(doc, "AssetsTotal", "txtAssetsTotal", ASSETS_TOTAL, true) != null) {
                assetsTotal.AppendChild(GetFSValue(doc, "AssetsTotal", "txtAssetsTotal", ASSETS_TOTAL, true));
            }

            //Short term debts ------------------------------------------
            XmlElement shortTermDebts = XmlHelper.CreateElement(doc, "ShortTermDebts", "");

            elements = new ArrayList();
            elements.Add(OVERDRAFT_BANK_LOAN);
            elements.Add(CREDITORS);
            elements.Add(OTHER_SHORT_TERM_DEBTS);
            elements.Add(NEXT_YEAR_MATURITIES);
            elements.Add(SHORT_TERM_DEBTS);

            if (IsTemplateNull(elements)) {
                XmlHelper.AddAttribute(doc, shortTermDebts, "Title", rm.GetString("", ci));
            } else {
                XmlHelper.AddAttribute(doc, shortTermDebts, "Title", rm.GetString("txtShortTermDebts", ci));
            }

            XmlHelper.AddAttribute(doc, shortTermDebts, "Title", rm.GetString("txtShortTermDebts", ci));
            balanceSheet.AppendChild(shortTermDebts);

            if (GetFSValue(doc, "Overdraft_BankLoan", "txtOverdraft_BankLoan", OVERDRAFT_BANK_LOAN, true) != null) {
                shortTermDebts.AppendChild(
                    GetFSValue(doc, "Overdraft_BankLoan", "txtOverdraft_BankLoan", OVERDRAFT_BANK_LOAN, true));
            }
            if (GetFSValue(doc, "Creditors", "txtCreditors", CREDITORS, true) != null) {
                shortTermDebts.AppendChild(GetFSValue(doc, "Creditors", "txtCreditors", CREDITORS, true));
            }
            if (GetFSValue(doc, "OtherShortTermDebts", "txtOtherShortTermDebts", OTHER_SHORT_TERM_DEBTS, true) != null) {
                shortTermDebts.AppendChild(
                    GetFSValue(doc, "OtherShortTermDebts", "txtOtherShortTermDebts", OTHER_SHORT_TERM_DEBTS, true));
            }
            if (GetFSValue(doc, "NextYearMaturities", "txtNextYearMaturities", NEXT_YEAR_MATURITIES, true) != null) {
                shortTermDebts.AppendChild(
                    GetFSValue(doc, "NextYearMaturities", "txtNextYearMaturities", NEXT_YEAR_MATURITIES, true));
            }
            //shortTermDebts.AppendChild(GetFSValue(doc, "ShortTermInvestments", "txtShortTermInvestments", NEXT_YEAR_MATURITIES, true));
            if (GetFSValue(doc, "ShortTermDebts", "txtShortTermDebts", SHORT_TERM_DEBTS, true) != null) {
                shortTermDebts.AppendChild(
                    GetFSValue(doc, "ShortTermDebts", "txtShortTermDebts", SHORT_TERM_DEBTS, true));
            }
            if (GetFSValue(doc, "ShortTermDebtsTotal", "txtShortTermDebtsTotal", SHORT_TERM_DEBTS_TOTAL, true) != null) {
                shortTermDebts.AppendChild(
                    GetFSValue(doc, "ShortTermDebtsTotal", "txtShortTermDebtsTotal", SHORT_TERM_DEBTS_TOTAL, true));
            }
            if (GetFSValue(doc, "Obligations", "txtDebtTowardsAssociatedCompanies", OBLIGATIONS, true) != null) {
                shortTermDebts.AppendChild(
                    GetFSValue(doc, "Obligations", "txtDebtTowardsAssociatedCompanies", OBLIGATIONS, true));
            }

            //Long term debts -------------------------------------------
            XmlElement longTermDebts = XmlHelper.CreateElement(doc, "LongTermDebts", "");
            XmlHelper.AddAttribute(doc, longTermDebts, "Title", rm.GetString("txtLongTermDebts", ci));
            balanceSheet.AppendChild(longTermDebts);

            if (GetFSValue(doc, "LongTermDebts", "txtLongTermDebts", LONG_TERM_DEBTS, true) != null) {
                longTermDebts.AppendChild(GetFSValue(doc, "LongTermDebts", "txtLongTermDebts", LONG_TERM_DEBTS, true));
            }
            if (GetFSValue(doc, "Obligations", "txtObligations", OBLIGATIONS, true) != null) {
                longTermDebts.AppendChild(GetFSValue(doc, "Obligations", "txtObligations", OBLIGATIONS, true));
            }
            if (
                GetFSValue(
                    doc, "DebtTowardsAssociatedCompanies", "txtDebtTowardsAssociatedCompanies", OBLIGATIONS, true) !=
                null) {
                longTermDebts.AppendChild(
                    GetFSValue(
                        doc, "DebtTowardsAssociatedCompanies", "txtDebtTowardsAssociatedCompanies", OBLIGATIONS, true));
            }

            if (GetFSValue(doc, "TotalDebt", "txtTotalDebt", TOTAL_DEBT, true) != null) {
                longTermDebts.AppendChild(GetFSValue(doc, "TotalDebt", "txtTotalDebt", TOTAL_DEBT, true));
            }
            if (GetFSValue(doc, "TotalLiabilities", "txtTotalLiabilities", TOTAL_DEBT, true) != null) {
                longTermDebts.AppendChild(GetFSValue(doc, "TotalLiabilities", "txtTotalLiabilities", TOTAL_DEBT, true));
            }
            if (
                GetFSAddValue(
                    doc,
                    "TotalDebtCalculated",
                    "txtTotalDebt",
                    SHORT_TERM_DEBTS_TOTAL,
                    LONG_TERM_DEBTS,
                    OBLIGATIONS,
                    true) != null) {
                longTermDebts.AppendChild(
                    GetFSAddValue(
                        doc,
                        "TotalDebtCalculated",
                        "txtTotalDebt",
                        SHORT_TERM_DEBTS_TOTAL,
                        LONG_TERM_DEBTS,
                        OBLIGATIONS,
                        true));
            }

            //Equity ----------------------------------------------------
            XmlElement equity = XmlHelper.CreateElement(doc, "Equity", "");
            elements = new ArrayList();
            elements.Add(ISSUED_SHARE_CAPITAL);
            elements.Add(OTHER_EQUITY);
            elements.Add(PROFIT_AND_LOSS_ACCOUNT);
            elements.Add(EQUITY_TOTAL);

            if (IsTemplateNull(elements)) {
                XmlHelper.AddAttribute(doc, equity, "Title", "");
            } else {
                XmlHelper.AddAttribute(doc, equity, "Title", rm.GetString("txtEquity", ci));
            }

            balanceSheet.AppendChild(equity);
            if (GetFSValue(doc, "IssuedShareCapital", "txtIssuedShareCapital", ISSUED_SHARE_CAPITAL, true) != null) {
                equity.AppendChild(
                    GetFSValue(doc, "IssuedShareCapital", "txtIssuedShareCapital", ISSUED_SHARE_CAPITAL, true));
            }
            if (GetFSValue(doc, "OtherEquity", "txtOtherEquity", OTHER_EQUITY, true) != null) {
                equity.AppendChild(GetFSValue(doc, "OtherEquity", "txtOtherEquity", OTHER_EQUITY, true));
            }
            if (GetFSValue(doc, "ProfitAndLossAccount", "txtProfitAndLossAccount", PROFIT_AND_LOSS_ACCOUNT, true) !=
                null) {
                equity.AppendChild(
                    GetFSValue(doc, "ProfitAndLossAccount", "txtProfitAndLossAccount", PROFIT_AND_LOSS_ACCOUNT, true));
            }
            if (
                GetFSAddValue(
                    doc, "OtherEquityCalculated", "txtOtherEquity", OTHER_EQUITY, PROFIT_AND_LOSS_ACCOUNT, true) != null) {
                equity.AppendChild(
                    GetFSAddValue(
                        doc, "OtherEquityCalculated", "txtOtherEquity", OTHER_EQUITY, PROFIT_AND_LOSS_ACCOUNT, true));
            }

            elements = new ArrayList();
            elements.Add(TAX_LIABILITIES);
            elements.Add(OTHER_LIABILITIES);

            if (IsTemplateNull(elements)) {
                XmlHelper.AddAttribute(doc, equity, "TitleLiabilities", "");
            } else {
                XmlHelper.AddAttribute(doc, equity, "TitleLiabilities", rm.GetString("txtLiabilities", ci));
            }

            if (GetFSValue(doc, "EquityTotal", "EquityTotal", EQUITY_TOTAL, true) != null) {
                equity.AppendChild(GetFSValue(doc, "EquityTotal", "EquityTotal", EQUITY_TOTAL, true));
            }
            if (GetFSValue(doc, "TaxLiabilities", "txtTaxLiabilities", TAX_LIABILITIES, true) != null) {
                equity.AppendChild(GetFSValue(doc, "TaxLiabilities", "txtTaxLiabilities", TAX_LIABILITIES, true));
            }
            if (GetFSValue(doc, "OtherLiabilities", "txtOtherLiabilities", OTHER_LIABILITIES, true) != null) {
                equity.AppendChild(GetFSValue(doc, "OtherLiabilities", "txtOtherLiabilities", OTHER_LIABILITIES, true));
            }
            if (
                GetFSValue(
                    doc, "MinorotyHoldingsInEquity", "txtMinorotyHoldingsInEquity", MINORITY_HOLDINGS_IN_EQUITY, true) !=
                null) {
                equity.AppendChild(
                    GetFSValue(
                        doc,
                        "MinorotyHoldingsInEquity",
                        "txtMinorotyHoldingsInEquity",
                        MINORITY_HOLDINGS_IN_EQUITY,
                        true));
            }

            //Debt & equity total ---------------------------------------
            XmlElement debtEquityTotal = XmlHelper.CreateElement(doc, "DebtEquityTotal", "");
            XmlHelper.AddAttribute(doc, debtEquityTotal, "Title", rm.GetString("txtDebtEquityTotal", ci));
            balanceSheet.AppendChild(debtEquityTotal);
            if (GetFSValue(doc, "TotalDebt", "txtTotalDebt", TOTAL_DEBT, true) != null) {
                debtEquityTotal.AppendChild(GetFSValue(doc, "TotalDebt", "txtTotalDebt", TOTAL_DEBT, true));
            }
            if (
                GetFSAddValue(
                    doc,
                    "TotalLiabilitiesCalculated",
                    "txtTotalLiabilities",
                    TAX_LIABILITIES,
                    OTHER_LIABILITIES,
                    LONG_TERM_DEBTS,
                    SHORT_TERM_DEBTS_TOTAL,
                    true) != null) {
                debtEquityTotal.AppendChild(
                    GetFSAddValue(
                        doc,
                        "TotalLiabilitiesCalculated",
                        "txtTotalLiabilities",
                        TAX_LIABILITIES,
                        OTHER_LIABILITIES,
                        LONG_TERM_DEBTS,
                        SHORT_TERM_DEBTS_TOTAL,
                        true));
            }
            if (GetFSValue(doc, "DebtEquityTotal", "txtDebtEquityTotal", DEBT_EQUITY_TOTAL, true) != null) {
                debtEquityTotal.AppendChild(
                    GetFSValue(doc, "DebtEquityTotal", "txtDebtEquityTotal", DEBT_EQUITY_TOTAL, true));
            }

            return balanceSheet;
        }

        public XmlElement GetRatiosTemplate(XmlDocument doc) {
            if (IsRatiosNull()) {
                return null;
            } else {
                XmlElement ratios1 = XmlHelper.GetXmlTable(doc, "Ratios", "tblRatios");
                XmlHelper.AddAttribute(doc, ratios1, "title", rm.GetString("txtRatios", ci));

                XmlElement years = XmlHelper.CreateElement(doc, "Years", "");
                ratios1.AppendChild(years);

                for (int i = 0; i < YearCount; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "Year", GetValue(i, FINANCIAL_YEAR));
                    years.AppendChild(item);
                }

                // Ratios -------------------------------------------
                XmlElement ratios = XmlHelper.CreateElement(doc, "Ratios", "");
                XmlHelper.AddAttribute(doc, ratios, "Title", rm.GetString("txtRatios", ci));
                ratios1.AppendChild(ratios);

                ratios.AppendChild(GetProfitMarginTemplate(doc));
                ratios.AppendChild(GetCurrentRatioTemplate(doc));
                ratios.AppendChild(GetQuickRatioTemplate(doc));
                ratios.AppendChild(GetEquityRatioTemplate(doc));
                ratios.AppendChild(GetDebtRatioTemplate(doc));
                ratios.AppendChild(GetCollectionTimeTemplate(doc));
                ratios.AppendChild(GetStockTurnoverTemplate(doc));
                ratios.AppendChild(GetInterestCoverRatioTemplate(doc));
                ratios.AppendChild(GetROCERatioTemplate(doc));

                return ratios1;
            }
        }

        public XmlElement GetHeadFSITemplate(XmlDocument doc) {
            XmlElement headFsi = XmlHelper.GetXmlTable(doc, "HeadFSI", "tblHeadFSI");
            XmlHelper.AddAttribute(doc, headFsi, "title", "HeadFSI");

            XmlElement years = XmlHelper.CreateElement(doc, "Years", "");
            headFsi.AppendChild(years);

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Year", GetValue(i, FINANCIAL_YEAR));
                years.AppendChild(item);
            }

            //Cash Flow -------------------------------------------
            XmlElement hfsi = XmlHelper.CreateElement(doc, "HeadFSI", "");
            XmlHelper.AddAttribute(doc, hfsi, "Title", rm.GetString("txtCashFlow", ci));
            headFsi.AppendChild(hfsi);

            hfsi.AppendChild(GetFSValueAsDateTime(doc, "FinancialYearEnd", "txtYearEnded", FINANCIAL_YEAR_END, true));
            hfsi.AppendChild(
                GetFSValue(doc, "AccountPeriodLength", "txtAccountPeriodLength", ACCOUNT_PERIOD_LENGTH, true));
            hfsi.AppendChild(GetFSValue(doc, "Currency", "txtCurrency", CURRENCY, false));

            return headFsi;
        }

        public XmlElement GetCashFlowTemplate(XmlDocument doc) {
            ArrayList elements = new ArrayList();

            elements.Add(OPERATING_PROFIT_BEFORE_WORKING_CAPITAL_CHANGES);
            elements.Add(CASH_FROM_OPERATIONS);
            elements.Add(CAPITAL_FROM_OPERATIONS);
            elements.Add(CASH_FLOWS_FROM_INVESTING_ACTIVITIES);
            elements.Add(CASH_FLOWS_FROM_FINANCING_ACTIVITIES);
            elements.Add(INCR_DECR_IN_CASH);

            if (IsTemplateNull(elements)) {
                //	return null;
                XmlElement nothing = XmlHelper.GetXmlTable(doc, "tblNothing", "tblNothing");
                return nothing;
            }

            XmlElement cashflow = XmlHelper.GetXmlTable(doc, "CashFlow", "tblCashFLow");
            XmlHelper.AddAttribute(doc, cashflow, "title", rm.GetString("txtCashFlow", ci));

            XmlElement years = XmlHelper.CreateElement(doc, "Years", "");
            cashflow.AppendChild(years);

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Year", GetValue(i, FINANCIAL_YEAR));
                years.AppendChild(item);
            }

            //Cash Flow -------------------------------------------
            XmlElement cf = XmlHelper.CreateElement(doc, "CashFlow", "");
            XmlHelper.AddAttribute(doc, cf, "Title", rm.GetString("txtCashFlow", ci));
            cashflow.AppendChild(cf);
            cf.AppendChild(
                GetFSValue(
                    doc,
                    "OperatingProfitBeforeWorkingCapitalChanges",
                    "txtOperatingProfitBeforeWorkingCapitalChanges",
                    OPERATING_PROFIT_BEFORE_WORKING_CAPITAL_CHANGES,
                    true));
            cf.AppendChild(GetFSValue(doc, "CashFromOperations", "txtCashFromOperations", CASH_FROM_OPERATIONS, true));
            cf.AppendChild(
                GetFSValue(
                    doc, "CapitalFromOperations", "txtNetCashFromOperatingActivities", CAPITAL_FROM_OPERATIONS, true));
            cf.AppendChild(
                GetFSValue(
                    doc,
                    "CashInvesting",
                    "txtCashFlowsFromInvestingActivities",
                    CASH_FLOWS_FROM_INVESTING_ACTIVITIES,
                    true));
            ;
            cf.AppendChild(
                GetFSValue(
                    doc,
                    "CashFinancing",
                    "txtCashFlowsFromFinancingActivities",
                    CASH_FLOWS_FROM_FINANCING_ACTIVITIES,
                    true));
            cf.AppendChild(GetFSValue(doc, "IncrDecrInCash", "txtIncreaseDecreaseInCash", INCR_DECR_IN_CASH, true));

            return cashflow;
        }

        public XmlElement GetCashFlowTemplateShort(XmlDocument doc) {
            ArrayList elements = new ArrayList();

            elements.Add(OPERATING_PROFIT_BEFORE_WORKING_CAPITAL_CHANGES);
            elements.Add(CASH_FROM_OPERATIONS);
            elements.Add(CAPITAL_FROM_OPERATIONS);
            elements.Add(CASH_FLOWS_FROM_INVESTING_ACTIVITIES);
            elements.Add(CASH_FLOWS_FROM_FINANCING_ACTIVITIES);
            elements.Add(INCR_DECR_IN_CASH);

            if (IsTemplateNull(elements)) {
                //	return null;
                XmlElement nothing = XmlHelper.GetXmlTable(doc, "tblNothing", "tblNothing");
                return nothing;
            }

            XmlElement cashflow = XmlHelper.GetXmlTable(doc, "CashFlow", "tblCashFLow");
            XmlHelper.AddAttribute(doc, cashflow, "title", rm.GetString("txtCashFlow", ci));

            XmlElement years = XmlHelper.CreateElement(doc, "Years", "");
            cashflow.AppendChild(years);

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Year", GetValue(i, FINANCIAL_YEAR));
                years.AppendChild(item);
            }

            //Cash Flow -------------------------------------------
            XmlElement cf = XmlHelper.CreateElement(doc, "CashFlow", "");
            XmlHelper.AddAttribute(doc, cf, "Title", rm.GetString("txtCashFlowShort", ci));
            cashflow.AppendChild(cf);
            cf.AppendChild(
                GetFSValue(
                    doc,
                    "OperatingProfitBeforeWorkingCapitalChanges",
                    "txtOperatingProfitBeforeWorkingCapitalChanges",
                    OPERATING_PROFIT_BEFORE_WORKING_CAPITAL_CHANGES,
                    true));
            cf.AppendChild(GetFSValue(doc, "CashFromOperations", "txtCashFromOperations", CASH_FROM_OPERATIONS, true));
            cf.AppendChild(
                GetFSValue(
                    doc,
                    "CapitalFromOperations",
                    "txtNetCashFromOperatingActivitiesShort",
                    CAPITAL_FROM_OPERATIONS,
                    true));
            cf.AppendChild(
                GetFSValue(
                    doc,
                    "CashInvesting",
                    "txtCashFlowsFromInvestingActivitiesShort",
                    CASH_FLOWS_FROM_INVESTING_ACTIVITIES,
                    true));
            ;
            cf.AppendChild(
                GetFSValue(
                    doc,
                    "CashFinancing",
                    "txtCashFlowsFromFinancingActivitiesShort",
                    CASH_FLOWS_FROM_FINANCING_ACTIVITIES,
                    true));
            cf.AppendChild(GetFSValue(doc, "IncrDecrInCash", "txtIncreaseDecreaseInCashShort", INCR_DECR_IN_CASH, true));

            return cashflow;
        }

        /// <summary>
        /// Creates a xml element that contains the profit/loss account information
        /// formatted as a table
        /// </summary>
        /// <param name="doc">The document to create xml element from</param>
        /// <returns>The profit/loss account formatted as a table</returns>
        public XmlElement GetProfitLossAccountTemplate(XmlDocument doc) {
            ArrayList elements = new ArrayList();
            elements.Add(REVENUE_FROM_MAIN_OPERATIONS);
            elements.Add(GROSS_PROFIT);
            elements.Add(COST_OF_SALES);
            elements.Add(OTHER_OPERATING_REVENUE);
            elements.Add(STAFF_COST);
            elements.Add(ADMINISTRATIVE_OTHER_EXPENSES);
            elements.Add(DISTRIBUTION_COST);
            elements.Add(DEPRICATION_OF_FIXED_ASSETS);
            elements.Add(TOTAL_COSTS);
            elements.Add(OPERATIONAL_PROFIT);
            elements.Add(FINANCIAL_INCOME);
            elements.Add(FINANCIAL_COST);
            elements.Add(OTHER_FINANCIAL_ITEMS);
            elements.Add(NET_FINANCIAL_ITEMS);
            elements.Add(INCOME_FROM_OPERATIONS_BEFORE_TAX);
            elements.Add(TAX);
            elements.Add(INCOME_FROM_OPERATIONS_AFTER_TAX);
            elements.Add(EXTRAORDINARY_INCOME_COST);
            elements.Add(INCOME_FROM_ASSOCIATED_COMPANIES);
            elements.Add(NET_RESULT);

            XmlElement plAccount = XmlHelper.GetXmlTable(doc, "ProfitLossAccount", "tblProfitLossAccountt");
            XmlHelper.AddAttribute(doc, plAccount, "title", rm.GetString("txtProfitLossAccount", ci));

            XmlElement years = XmlHelper.CreateElement(doc, "Years", "");
            plAccount.AppendChild(years);

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Year", GetValue(i, FINANCIAL_YEAR));
                years.AppendChild(item);
            }

            //header
            //Year end
            XmlElement yearEnd = XmlHelper.CreateElement(doc, "YearEnd", "");
            XmlHelper.AddAttribute(doc, yearEnd, "Title", rm.GetString("txtYearEnded", ci));
            plAccount.AppendChild(yearEnd);
            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                string val = GetValue(i, FINANCIAL_YEAR_END);
                if (val.Length >= 10) {
                    val = val.Substring(0, 10);
                }
                XmlHelper.AddAttribute(doc, item, "YearEnd", val);
                yearEnd.AppendChild(item);
            }

            //Months
            XmlElement months = XmlHelper.CreateElement(doc, "Months", "");
            XmlHelper.AddAttribute(doc, months, "Title", rm.GetString("txtFSIMonths", ci));
            plAccount.AppendChild(months);
            for (int i = 0; i < YearCount; i++) {
                string sMonths = GetValue(i, MONTHS);
                if (sMonths.Length > 0) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "Month", sMonths + " " + rm.GetString("txtMonths", ci));
                    months.AppendChild(item);
                }
            }

            //Currency
            XmlElement currencys = XmlHelper.CreateElement(doc, "Currency", "");
            XmlHelper.AddAttribute(doc, currencys, "Title", rm.GetString("txtCurrency", ci));
            plAccount.AppendChild(currencys);
            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Currency", GetValue(i, CURRENCY));
                currencys.AppendChild(item);
            }

            if (IsTemplateNull(elements)) {
                //return null;
                XmlElement nothing = XmlHelper.GetXmlTable(doc, "tblNothing", "tblNothing");
                return nothing;
            }

            //ProfitLossAccount -------------------------------------------
            XmlElement plAcc = XmlHelper.CreateElement(doc, "ProfitLossAccount", "");
            XmlHelper.AddAttribute(doc, plAcc, "Title", rm.GetString("txtProfitLossAccount", ci));
            plAccount.AppendChild(plAcc);

            elements = new ArrayList();
            elements.Add(REVENUE_FROM_MAIN_OPERATIONS);
            elements.Add(GROSS_PROFIT);
            elements.Add(COST_OF_SALES);
            if (IsTemplateNull(elements)) {
                XmlHelper.AddAttribute(doc, plAcc, "TitleRevenue", "");
            } else {
                XmlHelper.AddAttribute(doc, plAcc, "TitleRevenue", rm.GetString("txtRevenue", ci));
            }

            int count = 0;
            if (GetFSValue(doc, "Turnover", "txtTurnover", REVENUE_FROM_MAIN_OPERATIONS, true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "Turnover", "txtTurnover", REVENUE_FROM_MAIN_OPERATIONS, true));
            }
            if (GetFSValue(doc, "GrossProfit", "txtGrossProfit", GROSS_PROFIT, true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "GrossProfit", "txtGrossProfit", GROSS_PROFIT, true));
            }
            if (GetFSValue(doc, "CostSales", "txtCostOfSales", COST_OF_SALES, true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "CostSales", "txtCostOfSales", COST_OF_SALES, true));
            }
            if (GetFSValue(doc, "OtherOperatingRevenue", "txtOtherOperatingRevenue", OTHER_OPERATING_REVENUE, true) !=
                null) {
                plAcc.AppendChild(
                    GetFSValue(doc, "OtherOperatingRevenue", "txtOtherOperatingRevenue", OTHER_OPERATING_REVENUE, true));
            }

            elements = new ArrayList();
            elements.Add(REVENUE_FROM_MAIN_OPERATIONS);
            elements.Add(GROSS_PROFIT);
            elements.Add(COST_OF_SALES);
            if (IsTemplateNull(elements)) {
                XmlHelper.AddAttribute(doc, plAcc, "TitleCosts", "");
            } else {
                XmlHelper.AddAttribute(doc, plAcc, "TitleCosts", rm.GetString("txtCosts", ci));
            }

            if (GetFSValue(doc, "StaffCost", "txtStaffCost", STAFF_COST, true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "StaffCost", "txtStaffCost", STAFF_COST, true));
            }
            if (
                GetFSValue(
                    doc,
                    "AdministrativeOtherExpenses",
                    "txtAdministrativeOtherExpenses",
                    ADMINISTRATIVE_OTHER_EXPENSES,
                    true) != null) {
                plAcc.AppendChild(
                    GetFSValue(
                        doc,
                        "AdministrativeOtherExpenses",
                        "txtAdministrativeOtherExpenses",
                        ADMINISTRATIVE_OTHER_EXPENSES,
                        true));
            }
            if (
                GetFSValue(
                    doc,
                    "AdministrativeOtherExpenses",
                    "txtAdministrativeOtherExpenses",
                    ADMINISTRATIVE_OTHER_EXPENSES,
                    true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "DistributionCost", "txtDistributionCost", DISTRIBUTION_COST, true));
            }
            if (
                GetFSValue(
                    doc, "DepricationFixedAssets", "txtDepricationOfFixedAssets", DEPRICATION_OF_FIXED_ASSETS, true) !=
                null) {
                plAcc.AppendChild(
                    GetFSValue(
                        doc, "DepricationFixedAssets", "txtDepricationOfFixedAssets", DEPRICATION_OF_FIXED_ASSETS, true));
            }
            if (GetFSValue(doc, "TotalCosts", "txtTotalCost", TOTAL_COSTS, true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "TotalCosts", "txtTotalCost", TOTAL_COSTS, true));
            }
            if (GetFSValue(doc, "OperationalProfit", "txtOperationalProfit", OPERATIONAL_PROFIT, true) != null) {
                plAcc.AppendChild(
                    GetFSValue(doc, "OperationalProfit", "txtOperationalProfit", OPERATIONAL_PROFIT, true));
            }
            if (GetFSValue(doc, "FinancialIncome", "txtFinancialIncome", FINANCIAL_INCOME, true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "FinancialIncome", "txtFinancialIncome", FINANCIAL_INCOME, true));
            }
            if (GetFSValue(doc, "FinancialCost", "txtFinancialCost", FINANCIAL_COST, true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "FinancialCost", "txtFinancialCost", FINANCIAL_COST, true));
            }
            if (GetFSValue(doc, "OtherFinancialItems", "txtOtherFinancialItems", OTHER_FINANCIAL_ITEMS, true) != null) {
                plAcc.AppendChild(
                    GetFSValue(doc, "OtherFinancialItems", "txtOtherFinancialItems", OTHER_FINANCIAL_ITEMS, true));
            }
            if (GetFSValue(doc, "NetFinancialItems", "txtNetFinancialItems", NET_FINANCIAL_ITEMS, true) != null) {
                plAcc.AppendChild(
                    GetFSValue(doc, "NetFinancialItems", "txtNetFinancialItems", NET_FINANCIAL_ITEMS, true));
            }
            if (
                GetFSValue(
                    doc,
                    "IncomeFromOperationsBeforeTax",
                    "txtIncomeFromOperationsBeforeTax",
                    INCOME_FROM_OPERATIONS_BEFORE_TAX,
                    true) != null) {
                plAcc.AppendChild(
                    GetFSValue(
                        doc,
                        "IncomeFromOperationsBeforeTax",
                        "txtIncomeFromOperationsBeforeTax",
                        INCOME_FROM_OPERATIONS_BEFORE_TAX,
                        true));
            }
            if (GetFSValue(doc, "Tax", "txtTaxes", TAX, true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "Tax", "txtTaxes", TAX, true));
            }
            if (
                GetFSValue(
                    doc,
                    "IncomeFromOperationsAfterTax",
                    "txtIncomeFromOperationsAfterTax",
                    INCOME_FROM_OPERATIONS_AFTER_TAX,
                    true) != null) {
                plAcc.AppendChild(
                    GetFSValue(
                        doc,
                        "IncomeFromOperationsAfterTax",
                        "txtIncomeFromOperationsAfterTax",
                        INCOME_FROM_OPERATIONS_AFTER_TAX,
                        true));
            }
            if (
                GetFSValue(
                    doc, "ExtraOrdinaryIncomeCost", "txtExtraordinaryIncomeCost", EXTRAORDINARY_INCOME_COST, true) !=
                null) {
                plAcc.AppendChild(
                    GetFSValue(
                        doc, "ExtraOrdinaryIncomeCost", "txtExtraordinaryIncomeCost", EXTRAORDINARY_INCOME_COST, true));
            }
            if (
                GetFSValue(
                    doc,
                    "IncomeFromAssociatedCompanies",
                    "txtIncomeFromAssociatedCompanies",
                    INCOME_FROM_ASSOCIATED_COMPANIES,
                    true) != null) {
                plAcc.AppendChild(
                    GetFSValue(
                        doc,
                        "IncomeFromAssociatedCompanies",
                        "txtIncomeFromAssociatedCompanies",
                        INCOME_FROM_ASSOCIATED_COMPANIES,
                        true));
            }
            if (GetFSValue(doc, "ProfitOrLoss", "txtProfitOrLoss", NET_RESULT, true) != null) {
                plAcc.AppendChild(GetFSValue(doc, "ProfitOrLoss", "txtProfitOrLoss", NET_RESULT, true));
            }

            /*
			plAcc.AppendChild(GetFSValue(doc, "Income", "txtIncome", TOTAL_REVENUE, true));
			
			plAcc.AppendChild(GetFSValue(doc, "TotalCost", "txtTotalCost", OPERATIONAL_COST, true));
			plAcc.AppendChild(GetFSValue(doc, "OperationalCost", "txtOperationalCost", OPERATIONAL_COST, true));
			plAcc.AppendChild(GetFSSubtractValue(doc, "OperationalTurnover", "txtOperationalTurnover", TOTAL_REVENUE, OPERATIONAL_COST, true));

			plAcc.AppendChild(GetFSValue(doc, "Deprication", "txtDeprication", DEPRICATION, true));
			plAcc.AppendChild(GetFSValue(doc, "OperationalProfit", "txtOperationalProfit", OPERATIONAL_PROFIT, true));
			plAcc.AppendChild(GetFSValue(doc, "NetFinancialItems", "txtNetFinancialItems", NET_FINANCIAL_ITEMS, true));
			plAcc.AppendChild(GetFSValue(doc, "PreTaxProfit", "txtPreTaxProfit", PRE_TAX_PROFIT, true));
		
			plAcc.AppendChild(GetFSValue(doc, "IncomeBeforeTax", "txtIncomeBeforeTax", PRE_TAX_PROFIT, true));
			plAcc.AppendChild(GetFSValue(doc, "Taxes", "txtTaxes", TAXES, true));
			
			plAcc.AppendChild(GetFSValue(doc, "ProfitOrLoss", "txtProfitOrLoss", NET_RESULT, true));
			plAcc.AppendChild(GetFSValue(doc, "NetResult", "txtNetResult", NET_RESULT, true));
			*/

            //ProfitLossAccount -------------------------------------------
            XmlElement cashFlow = XmlHelper.CreateElement(doc, "CashFlow", "");
            XmlHelper.AddAttribute(doc, cashFlow, "Title", rm.GetString("txtCashFlow", ci));
            plAccount.AppendChild(cashFlow);

            if (GetFSValue(doc, "CapitalFromOperations", "txtCapitalFromOperations", CAPITAL_FROM_OPERATIONS, true) !=
                null) {
                cashFlow.AppendChild(
                    GetFSValue(doc, "CapitalFromOperations", "txtCapitalFromOperations", CAPITAL_FROM_OPERATIONS, true));
            }
            if (GetFSValue(doc, "CashFromOperations", "txtCashFromOperations", CASH_FROM_OPERATIONS, true) != null) {
                cashFlow.AppendChild(
                    GetFSValue(doc, "CashFromOperations", "txtCashFromOperations", CASH_FROM_OPERATIONS, true));
            }
            if (GetFSValue(doc, "IncrDescInCash", "txtIncrDescInCash", INCR_DECR_IN_CASH, true) != null) {
                cashFlow.AppendChild(GetFSValue(doc, "IncrDescInCash", "txtIncrDescInCash", INCR_DECR_IN_CASH, true));
            }

            // Ratios -------------------------------------------
            /*
			XmlElement ratios = XmlHelper.CreateElement(doc, "Ratios", "");
			XmlHelper.AddAttribute(doc, ratios, "Title",rm.GetString("txtRatios",ci));
			plAccount.AppendChild(ratios);

			ratios.AppendChild(GetProfitMarginTemplate(doc));
			ratios.AppendChild(GetCurrentRatioTemplate(doc));
			ratios.AppendChild(GetQuickRatioTemplate(doc));
			ratios.AppendChild(GetEquityRatioTemplate(doc));
			ratios.AppendChild(GetDebtRatioTemplate(doc));
			ratios.AppendChild(GetCollectionTimeTemplate(doc));
			ratios.AppendChild(GetStockTurnoverTemplate(doc));
			ratios.AppendChild(GetInterestCoverRatioTemplate(doc));
			ratios.AppendChild(GetROCERatioTemplate(doc));
			*/
            // Auditted -----------------------------------------
            XmlElement audit = XmlHelper.CreateElement(doc, "Auditted", "");
            XmlHelper.AddAttribute(doc, audit, "Title", rm.GetString("txtAuditted", ci));
            plAccount.AppendChild(audit);

            if (GetFSValue(doc, "Auditted", "txtAuditted", QUALIFIED_AUDIT, false) != null) {
                audit.AppendChild(GetFSValue(doc, "Auditted", "txtAuditted", QUALIFIED_AUDIT, false));
            }
            return plAccount;
        }

        public XmlElement GetAuditedTemplate(XmlDocument doc) {
            XmlElement auditbl = XmlHelper.GetXmlTable(doc, "Auditted", "tblAudited");
            XmlHelper.AddAttribute(doc, auditbl, "title", rm.GetString("Audited", ci));

            XmlElement years = XmlHelper.CreateElement(doc, "Years", "");
            auditbl.AppendChild(years);

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Year", GetValue(i, FINANCIAL_YEAR));
                years.AppendChild(item);
            }

            XmlElement audit = XmlHelper.CreateElement(doc, "Auditted", "");
            XmlHelper.AddAttribute(doc, audit, "Title", rm.GetString("txtAuditted", ci));
            auditbl.AppendChild(audit);

            if (GetFSValue(doc, "Auditted", "txtAuditted", QUALIFIED_AUDIT, false) != null) {
                audit.AppendChild(GetFSValue(doc, "Auditted", "txtAuditted", QUALIFIED_AUDIT, false));
            }
            return auditbl;
        }

        /// <summary>
        /// Creates a xml element that contains the profit/loss account information
        /// formatted as a table
        /// </summary>
        /// <param name="doc">The document to create xml element from</param>
        /// <returns>The profit/loss account formatted as a table</returns>
        public XmlElement GetProfitLossAccountTemplateWithoutRatios(XmlDocument doc) {
            XmlElement plAccount = XmlHelper.GetXmlTable(doc, "ProfitLossAccount", "tblProfitLossAccountt");
            XmlHelper.AddAttribute(doc, plAccount, "title", rm.GetString("txtProfitLossAccount", ci));

            XmlElement years = XmlHelper.CreateElement(doc, "Years", "");
            plAccount.AppendChild(years);

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Year", GetValue(i, FINANCIAL_YEAR));
                years.AppendChild(item);
            }

            //ProfitLossAccount -------------------------------------------
            XmlElement plAcc = XmlHelper.CreateElement(doc, "ProfitLossAccount", "");
            XmlHelper.AddAttribute(doc, plAcc, "Title", rm.GetString("txtProfitLossAccount", ci));
            plAccount.AppendChild(plAcc);

            plAcc.AppendChild(GetFSValue(doc, "Income", "txtIncome", TOTAL_REVENUE, true));
            plAcc.AppendChild(GetFSValue(doc, "OperationalCost", "txtOperationalCost", OPERATIONAL_COST, true));
            plAcc.AppendChild(
                GetFSSubtractValue(
                    doc, "OperationalTurnover", "txtOperationalTurnover", TOTAL_REVENUE, OPERATIONAL_COST, true));

            plAcc.AppendChild(GetFSValue(doc, "Deprication", "txtDeprication", DEPRICATION, true));
            plAcc.AppendChild(GetFSValue(doc, "OperationalProfit", "txtOperationalProfit", OPERATIONAL_PROFIT, true));
            plAcc.AppendChild(GetFSValue(doc, "NetFinancialItems", "txtNetFinancialItems", NET_FINANCIAL_ITEMS, true));
            plAcc.AppendChild(GetFSValue(doc, "PreTaxProfit", "txtPreTaxProfit", PRE_TAX_PROFIT, true));
            plAcc.AppendChild(GetFSValue(doc, "Taxes", "txtTaxes", TAXES, true));
            plAcc.AppendChild(GetFSValue(doc, "NetResult", "txtNetResult", NET_RESULT, true));

            //ProfitLossAccount -------------------------------------------
            XmlElement cashFlow = XmlHelper.CreateElement(doc, "CashFlow", "");
            XmlHelper.AddAttribute(doc, cashFlow, "Title", rm.GetString("txtCashFlow", ci));
            plAccount.AppendChild(cashFlow);

            cashFlow.AppendChild(
                GetFSValue(doc, "CapitalFromOperations", "txtCapitalFromOperations", CAPITAL_FROM_OPERATIONS, true));
            cashFlow.AppendChild(
                GetFSValue(doc, "CashFromOperations", "txtCashFromOperations", CASH_FROM_OPERATIONS, true));
            cashFlow.AppendChild(GetFSValue(doc, "IncrDescInCash", "txtIncrDescInCash", INCR_DECR_IN_CASH, true));

            // Ratios -------------------------------------------
            XmlElement ratios = XmlHelper.CreateElement(doc, "Ratios", "");
            XmlHelper.AddAttribute(doc, ratios, "Title", rm.GetString("txtRatios", ci));
            plAccount.AppendChild(ratios);

            // Auditted -----------------------------------------
            XmlElement audit = XmlHelper.CreateElement(doc, "Auditted", "");
            XmlHelper.AddAttribute(doc, audit, "Title", rm.GetString("txtAuditted", ci));
            plAccount.AppendChild(audit);

            audit.AppendChild(GetFSValue(doc, "Auditted", "txtAuditted", QUALIFIED_AUDIT, false));
            return plAccount;
        }

        protected XmlElement GetFSValue(
            XmlDocument doc, string elName, string text, string columnName, bool formatNumbers) {
            XmlElement el = XmlHelper.CreateElement(doc, elName, "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString(text, ci));
            int y = 0;
            for (int i = 0; i < YearCount; i++) {
                string val = GetValue(i, columnName);
                if (val != "" && val != "0" && val != "N/A") {
                    if (formatNumbers) {
                        val = formatNumber(val);
                    }
                } else {
                    val = "0";
                    y++;
                }
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", val);
                el.AppendChild(item);
            }
            //if(y==YearCount) return null;
            return el;
        }

        protected bool IsTemplateNull(ArrayList elements) {
            foreach (string elName in elements) {
                for (int i = 0; i < YearCount; i++) {
                    string val = GetValue(i, elName);
                    if (val != "" && val != "0" && val != "N/A" && val != null) {
                        return false;
                    }
                }
            }
            return true;
        }

        protected bool IsRatiosNull() {
            for (int i = 0; i < YearCount; i++) {
                if (getInterestCoverRatio(i) != "N/A") {
                    return false;
                }
                if (getROCERatio(i) != "N/A") {
                    return false;
                }
                if (getQuickRatio(i) != "N/A") {
                    return false;
                }
                if (getEquityRatio(i) != "N/A") {
                    return false;
                }
                if (getProfitMargin(i) != "N/A") {
                    return false;
                }
            }
            return true;
        }

        protected XmlElement GetFSValueAsDateTime(
            XmlDocument doc, string elName, string text, string columnName, bool formatNumbers) {
            XmlElement el = XmlHelper.CreateElement(doc, elName, "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString(text, ci));
            for (int i = 0; i < YearCount; i++) {
                string val = GetValue(i, columnName);
                if (val != "") {
                    val = Convert.ToDateTime(val).ToShortDateString();
                }
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", val);
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetFSSubtractValue(
            XmlDocument doc, string elName, string text, string columnName1, string columnName2, bool formatNumbers) {
            XmlElement el = XmlHelper.CreateElement(doc, elName, "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString(text, ci));
            for (int i = 0; i < YearCount; i++) {
                string val1 = GetValue(i, columnName1);
                string val2 = GetValue(i, columnName2);
                string valRes = "";
                if (val1 != "" && val2 != "") {
                    try {
                        valRes = (int.Parse(val1) - int.Parse(val2)).ToString();
                    } catch (Exception) {}
                }
                if (valRes != "") {
                    if (formatNumbers) {
                        valRes = formatNumber(valRes);
                    }
                }
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", valRes);
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetFSAddValue(
            XmlDocument doc, string elName, string text, string columnName1, string columnName2, bool formatNumbers) {
            XmlElement el = XmlHelper.CreateElement(doc, elName, "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString(text, ci));
            for (int i = 0; i < YearCount; i++) {
                string val1 = GetValue(i, columnName1);
                string val2 = GetValue(i, columnName2);
                string valRes = "";
                if (val1 != "" && val2 != "") {
                    try {
                        valRes = (int.Parse(val1) + int.Parse(val2)).ToString();
                    } catch (Exception) {}
                }
                if (valRes != "") {
                    if (formatNumbers) {
                        valRes = formatNumber(valRes);
                    }
                }
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", valRes);
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetFSAddValue(
            XmlDocument doc,
            string elName,
            string text,
            string columnName1,
            string columnName2,
            string columnName3,
            bool formatNumbers) {
            XmlElement el = XmlHelper.CreateElement(doc, elName, "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString(text, ci));
            for (int i = 0; i < YearCount; i++) {
                string val1 = GetValue(i, columnName1);
                string val2 = GetValue(i, columnName2);
                string val3 = GetValue(i, columnName3);
                string valRes = "";
                if (val1 == "") {
                    val1 = "0";
                }
                if (val2 == "") {
                    val2 = "0";
                }
                if (val3 == "") {
                    val3 = "0";
                }
                try {
                    valRes = (int.Parse(val1) + int.Parse(val2) + int.Parse(val3)).ToString();
                } catch (Exception) {}
                if (valRes != "") {
                    if (formatNumbers) {
                        valRes = formatNumber(valRes);
                    }
                }
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", valRes);
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetFSAddValue(
            XmlDocument doc,
            string elName,
            string text,
            string columnName1,
            string columnName2,
            string columnName3,
            string columnName4,
            bool formatNumbers) {
            XmlElement el = XmlHelper.CreateElement(doc, elName, "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString(text, ci));
            for (int i = 0; i < YearCount; i++) {
                string val1 = GetValue(i, columnName1);
                string val2 = GetValue(i, columnName2);
                string val3 = GetValue(i, columnName3);
                string val4 = GetValue(i, columnName4);
                string valRes = "";
                if (val1 == "") {
                    val1 = "0";
                }
                if (val2 == "") {
                    val2 = "0";
                }
                if (val3 == "") {
                    val3 = "0";
                }
                if (val4 == "") {
                    val4 = "0";
                }
                try {
                    valRes = (int.Parse(val1) + int.Parse(val2) + int.Parse(val3) + int.Parse(val4)).ToString();
                } catch (Exception) {}
                if (valRes != "") {
                    if (formatNumbers) {
                        valRes = formatNumber(valRes);
                    }
                }
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", valRes);
                el.AppendChild(item);
            }
            return el;
        }

        /*
		protected XmlElement GetFSAddValue(XmlDocument doc, string elName, string text, string columnName1, string columnName2, string columnName3, string columnName4, string columnName5, bool formatNumbers)
		{
			XmlElement el = XmlHelper.CreateElement(doc, elName, "");
			XmlHelper.AddAttribute(doc, el, "Title", rm.GetString(text,ci));
			for(int i=0;i<YearCount;i++)
			{
				string val1 = GetValue(i, columnName1);
				string val2 = GetValue(i, columnName2);
				string val3 = GetValue(i, columnName3);
				string val4 = GetValue(i, columnName4);
				string val5 = GetValue(i, columnName5);
				string valRes="";
				if(val1=="")
					val1="0";
				if(val2=="")
					val2="0";
				if(val3=="")
					val3="0";
				if(val4=="")
					val4="0";
				if(val5=="")
					val5="0";
				try
				{
					valRes = (int.Parse(val1)+int.Parse(val2)+int.Parse(val3)+int.Parse(val4)+int.Parse(val5)).ToString();
				}
				catch(Exception){}
				if(valRes != "")
					if(formatNumbers)
						valRes = formatNumber(valRes);
				XmlElement item = XmlHelper.GetXmlTableItem(doc);	
				XmlHelper.AddAttribute(doc, item, "Value", valRes);
				el.AppendChild(item);
			}
			return el;
		}
		protected XmlElement GetFSAddValue(XmlDocument doc, string elName, string text, string columnName1, string columnName2, string columnName3, string columnName4, string columnName5, string columnName6, bool formatNumbers)
		{
			XmlElement el = XmlHelper.CreateElement(doc, elName, "");
			XmlHelper.AddAttribute(doc, el, "Title", rm.GetString(text,ci));
			for(int i=0;i<YearCount;i++)
			{
				string val1 = GetValue(i, columnName1);
				string val2 = GetValue(i, columnName2);
				string val3 = GetValue(i, columnName3);
				string val4 = GetValue(i, columnName4);
				string val5 = GetValue(i, columnName5);
				string val6 = GetValue(i, columnName6);
				string valRes="";
				if(val1=="")
					val1="0";
				if(val2=="")
					val2="0";
				if(val3=="")
					val3="0";
				if(val4=="")
					val4="0";
				if(val5=="")
					val5="0";
				if(val6=="")
					val6="0";
				try
				{
					valRes = (int.Parse(val1)+int.Parse(val2)+int.Parse(val3)+int.Parse(val4)+int.Parse(val5)+int.Parse(val6)).ToString();
				}
				catch(Exception){}
				if(valRes != "")
					if(formatNumbers)
						valRes = formatNumber(valRes);
				XmlElement item = XmlHelper.GetXmlTableItem(doc);	
				XmlHelper.AddAttribute(doc, item, "Value", valRes);
				el.AppendChild(item);
			}
			return el;
		}*/

        protected XmlElement GetProfitMarginTemplate(XmlDocument doc) {
            XmlElement el = XmlHelper.CreateElement(doc, "ProfitMargin", "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtProfitMargin", ci));

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", getProfitMargin(i));
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetCurrentRatioTemplate(XmlDocument doc) {
            XmlElement el = XmlHelper.CreateElement(doc, "CurrentRatio", "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtCurrentRatio", ci));

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", getCurrentRatio(i));
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetQuickRatioTemplate(XmlDocument doc) {
            XmlElement el = XmlHelper.CreateElement(doc, "QuickRatio", "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtQuickRatio", ci));

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", getQuickRatio(i));
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetEquityRatioTemplate(XmlDocument doc) {
            XmlElement el = XmlHelper.CreateElement(doc, "EquityRatio", "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtEquityRatio", ci));

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", getEquityRatio(i));
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetDebtRatioTemplate(XmlDocument doc) {
            XmlElement el = XmlHelper.CreateElement(doc, "DebtRatio", "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtDebtRatio", ci));

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", getDebtRatio(i));
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetROCERatioTemplate(XmlDocument doc) {
            XmlElement el = XmlHelper.CreateElement(doc, "ROCERatio", "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtROCERatio", ci));

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", getROCERatio(i));
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetInterestCoverRatioTemplate(XmlDocument doc) {
            XmlElement el = XmlHelper.CreateElement(doc, "InterestCoverRatio", "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtInterestCoverRatio", ci));

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", getInterestCoverRatio(i));
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetCollectionTimeTemplate(XmlDocument doc) {
            XmlElement el = XmlHelper.CreateElement(doc, "CollectionTime", "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtCollectionTime", ci));

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", getCollectionTime(i));
                el.AppendChild(item);
            }
            return el;
        }

        protected XmlElement GetStockTurnoverTemplate(XmlDocument doc) {
            XmlElement el = XmlHelper.CreateElement(doc, "StockTurnover", "");
            XmlHelper.AddAttribute(doc, el, "Title", rm.GetString("txtStockTurnover", ci));

            for (int i = 0; i < YearCount; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(doc, item, "Value", getStockTurnover(i));
                el.AppendChild(item);
            }
            return el;
        }
    }
}