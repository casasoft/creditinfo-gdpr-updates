#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Xml;
using CPI.BLL;
using CR.BLL.Financial;
using CreditWatch.BLL;
using FSI.BLL;
using Logging.BLL;
using NPayments.BLL;
using NPayments.BLL.Claims;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using Logger=Logging.BLL.Logger;

using Cig.Framework.Base.Configuration;


#endregion

namespace CR.BLL {
    /// <summary>
    /// Summary description for ReportGenerator.
    /// </summary>
    public class ReportGenerator {
        public static int basicTafReportID = Convert.ToInt32(
            ConfigurationSettings.AppSettings.Get("BasicTAFReportType"));

        public static int companyProfileReportID =
            Convert.ToInt32(ConfigurationSettings.AppSettings.Get("BasicReportTypeID"));

        public static int companyReportID = Convert.ToInt32(
            ConfigurationSettings.AppSettings.Get("CompanyReportTypeID"));

        public static int creditReportID =
            Convert.ToInt32(ConfigurationSettings.AppSettings.Get("CreditInfoReportTypeID"));

        private readonly CRFactory factory;
        protected CultureInfo ci;
        protected string className = "ReportGenerator";
        protected string cultToUse = "";
        protected string cultureEnding = "Native";
        protected FinancialInfo financialInfo;
        protected string ipAddress = "";
        protected bool nativeCult;
        protected int reportType = companyProfileReportID;
        protected ResourceManager rm;
        protected int userCreditInfoID = -1;
        protected int userID = -1;

        public ReportGenerator(
            ResourceManager rm, string cultureToUse, int userCreditInfoID, int userID, string ipAddress) {
            /*companyProfileReportID = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("BasicReportType"));
			companyReportID  = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("CompanyReportType"));
			creditReportID  = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("CreditInfoReportType"));*/
            cultToUse = cultureToUse;
            this.userCreditInfoID = userCreditInfoID;
            this.userID = userID;
            this.ipAddress = ipAddress;
            string culture = "en-US";
            if (CigConfig.Configure("lookupsettings.nativeCulture") != null) {
                // strange sometimes cultureToUse is set to native culture and sometimes just to the string "Native" ?
                if (cultureToUse.Equals(CigConfig.Configure("lookupsettings.nativeCulture")) ||
                    cultureToUse.Equals("Native")) {
                    culture = CigConfig.Configure("lookupsettings.nativeCulture");
                    nativeCult = true;
                }
            }
            factory = new CRFactory();
            this.rm = rm;
            ci = new CultureInfo(culture);
            nativeCult = nativeCult;
            if (!nativeCult) {
                cultureEnding = "EN";
            }
        }

        public int ReportType { get { return reportType; } set { reportType = value; } }
        protected bool isCompanyProfileReport { get { return reportType == companyProfileReportID; } }
        protected bool isCompanyReport { get { return reportType == companyReportID; } }
        protected bool isCreditInfoReport { get { return reportType == creditReportID; } }
        protected bool isBasicTafReport { get { return reportType == basicTafReportID; } }

        /// <summary>
        /// Checks if the running version is for cyprus
        /// </summary>
        /// <returns>True if the version is for cyprus, false otherwise</returns>
        protected bool IsCyprus {
            get {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                    if (CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus")) {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if the running version is for czceh
        /// </summary>
        /// <returns>True if the version is for czceh, false otherwise</returns>
        protected bool IsCzech {
            get {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                    if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech")) {
                        return true;
                    }
                }
                return false;
            }
        }

        public XmlDocument getReport(int companyCIID, string afs_ids) {
            string funcName = "getReport(int companyCIID, string afs_ids)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            if (isCreditInfoReport) {
                XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtCreditInfoReport", ci));
            } else if (isCompanyReport) {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                    if (CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus")) {
                        XmlHelper.AddAttribute(
                            doc, rootElement, "Title", rm.GetString("txtCreditInfoStructureReport", ci));
                    } else {
                        XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtCompanyReport", ci));
                    }
                } else {
                    XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtCompanyReport", ci));
                }
            } else {
                XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtBasicReport", ci));
            }
            doc.AppendChild(rootElement);
            try {
                addReportComponents(doc, rootElement, companyCIID, afs_ids);
                logUsage(companyCIID.ToString());
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return doc;
        }

        public XmlDocument getFinancialReport(int companyCIID, string afs_id) {
            string funcName = "getFinancialReport(int companyCIID, string afs_ids)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            doc.AppendChild(rootElement);
            try {
                ReportCompanyBLLC theCompany = factory.GetCompanyReport("", companyCIID, false);

                if (theCompany != null && theCompany.CompanyCIID != 0) {
                    if (afs_id != null && afs_id.Trim() != "") {
                        if (financialInfo == null) {
                            financialInfo = new FinancialInfo(rm, ci);
                        }
                        financialInfo.ReportType = ReportType;

                        rootElement.AppendChild(financialInfo.getFinancialStatementAsXml(doc, companyCIID, afs_id));

                        //Add profit/loss account
                        //rootElement.AppendChild(getProfitLossAccount(doc, companyCIID, afs_ids));
                        //Add balance sheet
                        //rootElement.AppendChild(getBalanceSheet(doc, companyCIID, afs_ids));						
                        //Add definition of ratios
                        //rootElement.AppendChild(getDefinitionOfRatios(doc));
                        //mainTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
                        logUsage(companyCIID.ToString());
                        return doc;
                    }
                }
                return null;
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
        }

        protected void logUsage(string ciid) {
            string rt = null;
            if (isCompanyProfileReport) {
                rt = CigConfig.Configure("lookupsettings.BasicReportTypeID");
            } else if (isCompanyReport) {
                rt = CigConfig.Configure("lookupsettings.CompanyReportTypeID");
            } else if (isCreditInfoReport) {
                rt = CigConfig.Configure("lookupsettings.CreditInfoReportTypeID");
            } else if (isBasicTafReport) {
                rt = CigConfig.Configure("lookupsettings.BasicTAFReportType");
            } else {
                rt = ReportType.ToString();
            }

            if (rt != null) {
                factory.InsertInto_np_Usage(userCreditInfoID, int.Parse(rt), ciid, 1, ipAddress, userID);
            }
        }

        //B�i�
        protected XmlElement getBasicInfoRow(XmlDocument doc, string label1, string val1, string label2, string val2) {
            XmlElement row = XmlHelper.GetXmlHeaderRow(doc);
            row.AppendChild(XmlHelper.GetXmlHeaderColumn(doc, label1));
            if (val1 == "" || val1 == null || val1 == "0") {
                if (label1 != "" && label1 != "0" && label1 != null) // if no label the skip the N/A value
                {
                    val1 = "N/A";
                }
            }
            row.AppendChild(XmlHelper.GetXmlHeaderColumn(doc, val1));
            row.AppendChild(XmlHelper.GetXmlHeaderColumn(doc, label2));
            if (val2 == "" || val2 == "0" || val2 == null) {
                if (label2 != "" && label2 != "0" && label2 != null) // if no label the skip the N/A value
                {
                    val2 = "N/A";
                }
            }

            row.AppendChild(XmlHelper.GetXmlHeaderColumn(doc, val2));
            return row;
        }

        private XmlElement getCustomerCount(XmlDocument doc, string custCount) {
            XmlElement customerCount = XmlHelper.GetXmlTable(doc, "tblCustomerCount");
            customerCount.AppendChild(
                XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtEstimatedCustomerCount", ci)));
            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            hRow.AppendChild(XmlHelper.GetXmlColumn(doc, custCount));

            /*	XmlElement col1 = XmlHelper.GetXmlHeaderColumn(doc, "");
				col1.AppendChild(XmlHelper.GetXmlBoldText(doc,rm.GetString("txtEstimatedCustomerCount",ci)+" "));
				col1.AppendChild(XmlHelper.GetXmlNormalText(doc,custCount));
				hRow.AppendChild(col1);
				customerCount.AppendChild(hRow);
			*/
            customerCount.AppendChild(hRow);
            return customerCount;
        }

        //B�i�
        private XmlElement getCompanyForm(XmlDocument doc, string companyForm) {
            XmlElement customerCount = XmlHelper.GetXmlTable(doc, "tblCompanyForm");
            customerCount.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtCompanyLegalForm", ci)));
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetXmlColumn(doc, companyForm));
            /*	XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
				XmlElement col1 = XmlHelper.GetXmlHeaderColumn(doc, "");
				col1.AppendChild(XmlHelper.GetXmlBoldText(doc,rm.GetString("txtCompanyLegalForm",ci)+" "));
				col1.AppendChild(XmlHelper.GetXmlNormalText(doc,companyForm));
				hRow.AppendChild(col1);
				customerCount.AppendChild(hRow);
			*/
            customerCount.AppendChild(row);
            return customerCount;
        }

        //B�i�
        private XmlElement getCreditScoring(XmlDocument doc, int companyCIID) {
            XmlElement creditScoring = XmlHelper.GetXmlTable(doc, "tblCreditScoring");

            creditScoring.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtCreditScoring", ci)));

            XmlElement row = XmlHelper.GetXmlRow(doc);
            decimal creditScore = factory.GetCreditScoring(companyCIID, 1, DateTime.Now);
            int scoreCategory = factory.GetScoreCategory(creditScore, 1);
            if (scoreCategory == -1) {
                row.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtCreditScoringNoDataAvailable", ci)));
                creditScoring.AppendChild(row);
            } else {
                row.AppendChild(
                    XmlHelper.GetXmlColumn(
                        doc,
                        rm.GetString("txtCreditScoringExplainText1", ci) + "" + creditScore + "% (" +
                        rm.GetString("txtCreditScoringExplainText2", ci) + " " + scoreCategory + ")."));
                creditScoring.AppendChild(row);
                // one empty row 
                row = XmlHelper.GetXmlRow(doc);
                row.AppendChild(XmlHelper.GetXmlColumn(doc, ""));
                creditScoring.AppendChild(row);
                row = XmlHelper.GetXmlRow(doc);
                row.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtCreditCategories", ci)));
                creditScoring.AppendChild(row);
                DataSet dsScoreRatings = factory.GetScoreRatingsAsDataSet();
                if (dsScoreRatings.Tables.Count > 0) {
                    for (int i = 0; i < dsScoreRatings.Tables[0].Rows.Count; i++) {
                        if (i >= 11) {
                            break;
                        }
                        string data = i + 1 + ".        " + dsScoreRatings.Tables[0].Rows[i]["s_min"] + "% - " +
                                      dsScoreRatings.Tables[0].Rows[i]["s_max"] + "%";
                        if (nativeCult) {
                            data += "         " + dsScoreRatings.Tables[0].Rows[i]["rating_na"];
                        } else {
                            data += "          " + dsScoreRatings.Tables[0].Rows[i]["rating_en"];
                        }
                        XmlElement row1 = XmlHelper.GetXmlRow(doc);
                        row1.AppendChild(XmlHelper.GetXmlColumn(doc, data));
                        creditScoring.AppendChild(row1);
                    }
                }
                // add disclaimer
                row = XmlHelper.GetXmlRow(doc);
                row.AppendChild(XmlHelper.GetXmlColumn(doc, ""));
                creditScoring.AppendChild(row);
                row = XmlHelper.GetXmlFooterRow(doc, rm.GetString("txtCreditScoringDisclaimer", ci), 2);
                //	row.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtCreditScoringDisclaimer",ci)));
                creditScoring.AppendChild(row);
            }
            return creditScoring;
        }

        //B�i�
        private XmlElement getEmployees(XmlDocument doc, int companyCIID) {
            XmlElement employees = XmlHelper.GetXmlTable(doc, "tblEmployees");
            employees.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtEmployees", ci)));

            //DataSet ds = cpiFactory.GetStaffCountAsDataSet(companyCIID);
            DataSet ds = factory.GetEmployeesCountAsDataSet(companyCIID);
            if (ds.Tables.Count > 0) {
                if (ds.Tables[0].Rows.Count > 0) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        if (i >= 5) {
                            break;
                        }
                        string data = ds.Tables[0].Rows[i]["Year"] + " - " + ds.Tables[0].Rows[i]["Count"];
                        XmlElement row = XmlHelper.GetXmlRow(doc);
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, data));
                        employees.AppendChild(row);
                    }
                } else {
                    XmlElement noresult = XmlHelper.GetXmlRow(doc);
                    noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                    employees.AppendChild(noresult);
                }
            }
            return employees;
        }

        //B�i�
        private XmlElement getOwners(XmlDocument doc, int companyCIID) {
            XmlElement owners = XmlHelper.GetXmlTable(doc, "tblOwners");

            string[] headers = new string[5];
            headers[0] = rm.GetString("txtOwnersShareholders", ci);
            if (IsCyprus) {
                headers[1] = "";
                    //Dont add header text if cyprus unless some company is shareholer rm.GetString("txtRegistrationID",ci);
                // 18.01.2005 changed back by request from Costas
                // headers[2]=rm.GetString("txtOwnershipPercentage",ci);
                headers[2] = rm.GetString("txtOwnership", ci);
                headers[3] = rm.GetString("txtShareClass", ci);
            } else {
                headers[1] = rm.GetString("txtRegistrationID", ci);
                headers[2] = rm.GetString("txtOwnership", ci);
                headers[3] = "";
            }
            headers[4] = rm.GetString("txtFormOfOperation", ci);

            DataSet ds = factory.GetShareHolderOwnerAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                //owners.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));
                ArrayList arrCiids = new ArrayList();
                ArrayList arrOwnerShipTypes = new ArrayList();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    bool idFound = false;
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    string ownershipID = ds.Tables[0].Rows[i]["OwnerShipID"].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (((string) arrCiids[j]).Equals(creditInfoId) &&
                            ((string) arrOwnerShipTypes[j]).Equals(ownershipID)) {
                            idFound = true;
                            break;
                        }
                    }
                    if (!idFound) {
                        arrCiids.Add(creditInfoId);
                        arrOwnerShipTypes.Add(ownershipID);
                        XmlElement row = XmlHelper.GetXmlRow(doc);
                        string userType = ds.Tables[0].Rows[i]["Type"].ToString().Trim();
                        if (userType.Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                            row.AppendChild(
                                XmlHelper.GetXmlColumn(
                                    doc,
                                    ds.Tables[0].Rows[i]["FirstName" + cultureEnding] + " " +
                                    ds.Tables[0].Rows[i]["SurName" + cultureEnding]));
                            if (IsCyprus) //Not allowed to display id for individuals in cyprus
                            {
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, ""));
                            } else {
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["number"].ToString()));
                            }

                            row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["ownership"].ToString()));
                            if (IsCyprus) {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc, ds.Tables[0].Rows[i]["OwnershipDescription" + cultureEnding].ToString()));
                            } else {
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, " "));
                            }
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, " "));
                        } else {
                            row.AppendChild(
                                XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString()));
                            //ok - here are company - if cyprus we need to set the id header
                            if (IsCyprus) {
                                headers[1] = rm.GetString("txtRegistrationID", ci);
                            }
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["number"].ToString()));
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["ownership"].ToString()));
                            if (IsCyprus) {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc, ds.Tables[0].Rows[i]["OwnershipDescription" + cultureEnding].ToString()));
                            } else {
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, " "));
                            }
                            /**TODO: Vantar a� finna l�singu � starfsemi**/
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, ""));
                        }
                        owners.AppendChild(row);
                    }
                }

                if (owners.ChildNodes.Count > 0) {
                    owners.InsertBefore(XmlHelper.GetXmlHeaderRowWithValue(doc, headers), owners.FirstChild);
                } else {
                    owners.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));
                }
            } else {
                owners.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtOwnersShareholders", ci)));
                XmlElement row1 = XmlHelper.GetXmlRow(doc);
                row1.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                owners.AppendChild(row1);
            }

            return owners;
        }

        //B�i�
        private XmlElement GetOwnersNewType(XmlDocument doc, int companyCIID) {
            XmlElement shareholders = XmlHelper.GetXmlTable(doc, "Shareholders", "tblShareholders");
            XmlHelper.AddAttribute(doc, shareholders, "title", rm.GetString("txtOwnersShareholders", ci));

            DataSet ds = factory.GetShareHolderOwnerAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                ArrayList arrCiids = new ArrayList();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    bool idFound = false;
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (((string) arrCiids[j]).Equals(creditInfoId)) {
                            idFound = true;
                            break;
                        }
                    }
                    if (!idFound) {
                        XmlElement item = XmlHelper.GetXmlTableItem(doc);
                        XmlHelper.AddAttribute(doc, item, "NameTitle", rm.GetString("txtName", ci));
                        string userType = ds.Tables[0].Rows[i]["Type"].ToString().Trim();
                        if (userType.Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstName" + cultureEnding] + " " +
                                ds.Tables[0].Rows[i]["SurName" + cultureEnding]);
                            XmlHelper.AddAttribute(doc, item, "Company", "0");
                        } else {
                            XmlHelper.AddAttribute(
                                doc, item, "NameValue", ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString());
                            XmlHelper.AddAttribute(doc, item, "Company", "1");
                            /**TODO: Vantar a� finna l�singu � starfsemi**/
                        }
                        XmlHelper.AddAttribute(doc, item, "NumberTitle", rm.GetString("txtRegistrationID", ci));
                        XmlHelper.AddAttribute(doc, item, "NumberValue", ds.Tables[0].Rows[i]["number"].ToString());
                        XmlHelper.AddAttribute(doc, item, "OwnershipTitle", rm.GetString("txtOwnership", ci));
                        XmlHelper.AddAttribute(
                            doc, item, "OwnershipValue", ds.Tables[0].Rows[i]["ownership"].ToString());

                        //Now we need all companys this ciid is shareholder in
                        int ciid = int.Parse(creditInfoId);
                        DataSet inv = factory.GetShareholderInvolvements(ciid);
                        if (inv != null && inv.Tables.Count > 0 && inv.Tables[0].Rows.Count > 0) {
                            XmlHelper.AddAttribute(
                                doc, item, "AlsoOwnsSharesInTitle", rm.GetString("txtAlsoOwnsSharesIn", ci));
                            for (int j = 0; j < inv.Tables[0].Rows.Count; j++) {
                                if (int.Parse(inv.Tables[0].Rows[j]["CompanyCIID"].ToString()) == companyCIID) {
                                    continue;
                                }
                                XmlElement invItem = XmlHelper.CreateElement(doc, "Involvements", "");
                                if (nativeCult) {
                                    if (inv.Tables[0].Rows[j]["NameNative"].ToString() != "") {
                                        XmlHelper.AddAttribute(
                                            doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                    } else {
                                        XmlHelper.AddAttribute(
                                            doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                    }
                                } else {
                                    if (inv.Tables[0].Rows[j]["NameEN"].ToString() != "") {
                                        XmlHelper.AddAttribute(
                                            doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                    } else {
                                        XmlHelper.AddAttribute(
                                            doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                    }
                                }
                                item.AppendChild(invItem);
                            }
                        }
                        shareholders.AppendChild(item);
                    }
                }
            }
            return shareholders;
        }

        //B�i�
        private XmlElement GetBoardNewType(XmlDocument doc, int companyCIID) {
            XmlElement board = XmlHelper.GetXmlTable(doc, "BoardMembers", "tblBoardMembers");
            XmlHelper.AddAttribute(doc, board, "title", rm.GetString("txtBoard", ci));

            /*		XmlElement th = XmlHelper.GetXmlTableHeader(doc);
			XmlHelper.AddAttribute(doc, th, "Board", rm.GetString("txtBoard",ci));
			XmlHelper.AddAttribute(doc, th, "RegistrationID", rm.GetString("txtRegistrationID",ci));
			XmlHelper.AddAttribute(doc, th, "Position", rm.GetString("txtPosition",ci));
			//XmlHelper.AddAttribute(doc, th, "Education", rm.GetString("txtEducation",ci));*/

            //board.AppendChild(th);

            bool displayDDDMarksExplain = false;

            DataSet ds = factory.GetBoardAsDataset(companyCIID);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    if (factory.IsIDInDebitorsDatabase(int.Parse(ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString()))) {
                        XmlHelper.AddAttribute(doc, item, "DDDMark", "1");
                        displayDDDMarksExplain = true;
                    } else {
                        XmlHelper.AddAttribute(doc, item, "DDDMark", "");
                    }

                    XmlHelper.AddAttribute(doc, item, "NameTitle", rm.GetString("txtName", ci));
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        }
                    }

                    XmlHelper.AddAttribute(doc, item, "RegistrationIDTitle", rm.GetString("txtRegistrationID", ci));
                    XmlHelper.AddAttribute(doc, item, "RegistrationIDValue", ds.Tables[0].Rows[i]["number"].ToString());
                    //XmlHelper.AddAttribute(doc, item, "Education", ds.Tables[0].Rows[i]["Education"+cultureEnding].ToString()); 
                    XmlHelper.AddAttribute(doc, item, "PositionTitle", rm.GetString("txtPosition", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "PositionValue", ds.Tables[0].Rows[i]["Title" + cultureEnding].ToString());

                    //Involvements

                    int ciid = int.Parse(ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString());
                    DataSet inv = factory.GetBoardMemberInvolvements(ciid);
                    if (inv != null && inv.Tables.Count > 0 && inv.Tables[0].Rows.Count > 0) {
                        XmlHelper.AddAttribute(doc, item, "AlsoInvolvedInTitle", rm.GetString("txtAlsoInvolvedIn", ci));
                        for (int j = 0; j < inv.Tables[0].Rows.Count; j++) {
                            if (int.Parse(inv.Tables[0].Rows[j]["CompanyCIID"].ToString()) == companyCIID) {
                                continue;
                            }
                            XmlElement invItem = XmlHelper.CreateElement(doc, "Involvements", "");
                            if (nativeCult) {
                                if (inv.Tables[0].Rows[j]["NameNative"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                }

                                if (inv.Tables[0].Rows[j]["TitleNative"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Title", inv.Tables[0].Rows[j]["TitleNative"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Title", inv.Tables[0].Rows[j]["TitleEN"].ToString());
                                }
                            } else {
                                if (inv.Tables[0].Rows[j]["NameEN"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                }

                                if (inv.Tables[0].Rows[j]["TitleEN"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Title", inv.Tables[0].Rows[j]["TitleEN"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Title", inv.Tables[0].Rows[j]["TitleNative"].ToString());
                                }
                            }
                            item.AppendChild(invItem);
                        }
                    }

                    board.AppendChild(item);
                }
            }

            if (displayDDDMarksExplain) {
                XmlElement footer = XmlHelper.GetXmlTableFooter(doc);
                XmlHelper.AddAttribute(
                    doc, footer, "DDDMark1", "1: " + rm.GetString("txtIndividualRegisteredInDDD", ci));
                XmlHelper.AddAttribute(
                    doc, footer, "DDDMark2", "2: " + rm.GetString("txtIndividualHasRelationsToBankrupt", ci));
                XmlHelper.AddAttribute(doc, footer, "DDDMark3", "3: " + rm.GetString("txtBoth", ci));
                board.AppendChild(footer);
            }
            return board;
        }

        //B�i�
        private XmlElement GetKeyEmployeesNewType(XmlDocument doc, int companyCIID) {
            XmlElement keyEmp = XmlHelper.GetXmlTable(doc, "KeyEmployees", "tblKeyEmployees");
            XmlHelper.AddAttribute(doc, keyEmp, "title", rm.GetString("txtkeyEmployees", ci));

            DataSet ds = factory.GetKeyEmployeesAsDataset(companyCIID);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement item = XmlHelper.GetXmlTableItem(doc);
                    XmlHelper.AddAttribute(doc, item, "NameTitle", rm.GetString("txtName", ci));
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"]);
                        } else {
                            XmlHelper.AddAttribute(
                                doc,
                                item,
                                "NameValue",
                                ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                        }
                    }

                    XmlHelper.AddAttribute(doc, item, "RegIDTitle", rm.GetString("txtRegistrationID", ci));
                    XmlHelper.AddAttribute(doc, item, "RegIDValue", ds.Tables[0].Rows[i]["number"].ToString());
                    if (isCreditInfoReport) {
                        XmlHelper.AddAttribute(doc, item, "Education", "1");
                        XmlHelper.AddAttribute(doc, item, "EducationTitle", rm.GetString("txtEducation", ci));
                        XmlHelper.AddAttribute(
                            doc, item, "EducationValue", ds.Tables[0].Rows[i]["Education" + cultureEnding].ToString());
                    } else {
                        XmlHelper.AddAttribute(doc, item, "Education", "0");
                    }
                    XmlHelper.AddAttribute(doc, item, "JobTitleTitle", rm.GetString("txtPosition", ci));
                    XmlHelper.AddAttribute(
                        doc, item, "JobTitleValue", ds.Tables[0].Rows[i]["JobTitle" + cultureEnding].ToString());

                    //Now we need all companys this ciid is shareholder in
                    int ciid = int.Parse(ds.Tables[0].Rows[i]["principalciid"].ToString());
                    DataSet inv = factory.GetPrincipalsInvolvements(ciid);
                    if (inv != null && inv.Tables.Count > 0 && inv.Tables[0].Rows.Count > 0) {
                        XmlHelper.AddAttribute(doc, item, "AlsoInvolvedInTitle", rm.GetString("txtAlsoInvolvedIn", ci));
                        for (int j = 0; j < inv.Tables[0].Rows.Count; j++) {
                            if (int.Parse(inv.Tables[0].Rows[j]["CompanyCIID"].ToString()) == companyCIID) {
                                continue;
                            }
                            XmlElement invItem = XmlHelper.CreateElement(doc, "Involvements", "");
                            if (nativeCult) {
                                if (inv.Tables[0].Rows[j]["NameNative"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                }

                                if (inv.Tables[0].Rows[j]["JobTitleNative"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "JobTitle", inv.Tables[0].Rows[j]["JobTitleNative"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "JobTitle", inv.Tables[0].Rows[j]["JobTitleEN"].ToString());
                                }
                            } else {
                                if (inv.Tables[0].Rows[j]["NameEN"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameEN"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "Name", inv.Tables[0].Rows[j]["NameNative"].ToString());
                                }

                                if (inv.Tables[0].Rows[j]["JobTitleEN"].ToString() != "") {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "JobTitle", inv.Tables[0].Rows[j]["JobTitleEN"].ToString());
                                } else {
                                    XmlHelper.AddAttribute(
                                        doc, invItem, "JobTitle", inv.Tables[0].Rows[j]["JobTitleNative"].ToString());
                                }
                            }
                            item.AppendChild(invItem);
                        }
                    }

                    keyEmp.AppendChild(item);
                }
            }
            return keyEmp;
        }

        //B�i�
        private XmlElement getBalanceSheet(XmlDocument doc, int companyCIID, string years) {
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = ReportType;
            DataSet ds = factory.GetBalanceSheetAsDataset(companyCIID, years);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    financialInfo.SetValue(
                        i, FinancialInfo.CURRENCY, ds.Tables[0].Rows[i][FinancialInfo.CURRENCY].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.MONTHS, ds.Tables[0].Rows[i][FinancialInfo.MONTHS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());

                    financialInfo.SetValue(i, FinancialInfo.CASH, ds.Tables[0].Rows[i][FinancialInfo.CASH].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.RECEIVABLES, ds.Tables[0].Rows[i][FinancialInfo.RECEIVABLES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OTHER_CURRENT_ASSETS,
                        ds.Tables[0].Rows[i][FinancialInfo.OTHER_CURRENT_ASSETS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INVENTORY_STOCKS,
                        ds.Tables[0].Rows[i][FinancialInfo.INVENTORY_STOCKS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CURRENT_ASSETS_TOTAL,
                        ds.Tables[0].Rows[i][FinancialInfo.CURRENT_ASSETS_TOTAL].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.FINANCIALS_ASSETS,
                        ds.Tables[0].Rows[i][FinancialInfo.FINANCIALS_ASSETS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TANGIBLE_ASSETS, ds.Tables[0].Rows[i][FinancialInfo.TANGIBLE_ASSETS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INTANGIBLE_ASSETS,
                        ds.Tables[0].Rows[i][FinancialInfo.INTANGIBLE_ASSETS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.FIXED_ASSETS_TOTAL,
                        ds.Tables[0].Rows[i][FinancialInfo.FIXED_ASSETS_TOTAL].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.ASSETS_TOTAL, ds.Tables[0].Rows[i][FinancialInfo.ASSETS_TOTAL].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OVERDRAFT_BANK_LOAN,
                        ds.Tables[0].Rows[i][FinancialInfo.OVERDRAFT_BANK_LOAN].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.CREDITORS, ds.Tables[0].Rows[i][FinancialInfo.CREDITORS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OTHER_SHORT_TERM_DEBTS,
                        ds.Tables[0].Rows[i][FinancialInfo.OTHER_SHORT_TERM_DEBTS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.NEXT_YEAR_MATURITIES,
                        ds.Tables[0].Rows[i][FinancialInfo.NEXT_YEAR_MATURITIES].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.SHORT_TERM_DEBTS_TOTAL,
                        ds.Tables[0].Rows[i][FinancialInfo.SHORT_TERM_DEBTS_TOTAL].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.LONG_TERM_DEBTS, ds.Tables[0].Rows[i][FinancialInfo.LONG_TERM_DEBTS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.OBLIGATIONS, ds.Tables[0].Rows[i][FinancialInfo.OBLIGATIONS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TOTAL_DEBT, ds.Tables[0].Rows[i][FinancialInfo.TOTAL_DEBT].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.ISSUED_SHARE_CAPITAL,
                        ds.Tables[0].Rows[i][FinancialInfo.ISSUED_SHARE_CAPITAL].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.OTHER_EQUITY, ds.Tables[0].Rows[i][FinancialInfo.OTHER_EQUITY].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.EQUITY_TOTAL, ds.Tables[0].Rows[i][FinancialInfo.EQUITY_TOTAL].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.MINORITY_HOLDINGS_IN_EQUITY,
                        ds.Tables[0].Rows[i][FinancialInfo.MINORITY_HOLDINGS_IN_EQUITY].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.DEBT_EQUITY_TOTAL,
                        ds.Tables[0].Rows[i][FinancialInfo.DEBT_EQUITY_TOTAL].ToString());
                }
            }
            return financialInfo.getBalanceSheetTableAsXml(doc);
        }

        //B�i�
        private XmlElement getProfitLossAccount(XmlDocument doc, int companyCIID, string years) {
            if (financialInfo == null) {
                financialInfo = new FinancialInfo(rm, ci);
            }
            financialInfo.ReportType = ReportType;
            DataSet ds = factory.GetProfitLossAccountAsDataset(companyCIID, years);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    financialInfo.SetValue(
                        i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());

                    financialInfo.SetValue(
                        i, FinancialInfo.INCOME, ds.Tables[0].Rows[i][FinancialInfo.INCOME].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.TOTAL_REVENUE, ds.Tables[0].Rows[i][FinancialInfo.TOTAL_REVENUE].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_COST,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_COST].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.DEPRICATION, ds.Tables[0].Rows[i][FinancialInfo.DEPRICATION].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.OPERATIONAL_PROFIT,
                        ds.Tables[0].Rows[i][FinancialInfo.OPERATIONAL_PROFIT].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.NET_FINANCIAL_ITEMS,
                        ds.Tables[0].Rows[i][FinancialInfo.NET_FINANCIAL_ITEMS].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.PRE_TAX_PROFIT, ds.Tables[0].Rows[i][FinancialInfo.PRE_TAX_PROFIT].ToString());
                    financialInfo.SetValue(i, FinancialInfo.TAXES, ds.Tables[0].Rows[i][FinancialInfo.TAXES].ToString());
                    financialInfo.SetValue(
                        i, FinancialInfo.NET_RESULT, ds.Tables[0].Rows[i][FinancialInfo.NET_RESULT].ToString());

                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CAPITAL_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CAPITAL_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.CASH_FROM_OPERATIONS,
                        ds.Tables[0].Rows[i][FinancialInfo.CASH_FROM_OPERATIONS].ToString());
                    financialInfo.SetValue(
                        i,
                        FinancialInfo.INCR_DECR_IN_CASH,
                        ds.Tables[0].Rows[i][FinancialInfo.INCR_DECR_IN_CASH].ToString());
                    string auditted = ds.Tables[0].Rows[i][FinancialInfo.QUALIFIED_AUDIT].ToString();
                    if (auditted.Equals("True")) {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "Yes");
                    } else {
                        financialInfo.SetValue(i, FinancialInfo.QUALIFIED_AUDIT, "No");
                    }
                }
            }
            return financialInfo.getProfitLossAccountAsXml(doc);
        }

        //B�i�
        private XmlElement getBoard(XmlDocument doc, int companyCIID) {
            XmlElement board = XmlHelper.GetXmlTable(doc, "tblBoard");
            string[] headers;
            /*if(isCreditInfoReport)
			{
				headers = new string[4];
				headers[0]=rm.GetString("txtBoard",ci);
				headers[1]=rm.GetString("txtRegistrationID",ci);
				headers[2]=rm.GetString("txtEducation",ci);
				headers[3]=rm.GetString("txtPosition",ci);
			}
			else
			{*/
            if (IsCyprus) {
                headers = new string[2];
                headers[0] = rm.GetString("txtBoard", ci);
                headers[1] = rm.GetString("txtPosition", ci);
            } else {
                headers = new string[3];
                headers[0] = rm.GetString("txtBoard", ci);
                headers[1] = rm.GetString("txtRegistrationID", ci);
                headers[2] = rm.GetString("txtPosition", ci);
            }
            //}

            //board.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));

            bool displayDDDMarksExplain = false;

            DataSet ds = factory.GetBoardAsDataset(companyCIID);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    if (isCreditInfoReport) {
                        //Check if in DDD - only display explain of DDD marks if someone is in the DDD
                        if (factory.IsIDInDebitorsDatabase(
                            int.Parse(ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString()))) {
                            displayDDDMarksExplain = true;
                        }

                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                                row.AppendChild(
                                    getDebitorsDatabaseMark(
                                        doc,
                                        ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString(),
                                        ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                            } else {
                                row.AppendChild(
                                    getDebitorsDatabaseMark(
                                        doc,
                                        ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString(),
                                        ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                        ds.Tables[0].Rows[i]["SurNameNative"]));
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                                row.AppendChild(
                                    getDebitorsDatabaseMark(
                                        doc,
                                        ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString(),
                                        ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                        ds.Tables[0].Rows[i]["SurNameNative"]));
                            } else {
                                row.AppendChild(
                                    getDebitorsDatabaseMark(
                                        doc,
                                        ds.Tables[0].Rows[i]["BoardMemberCIID"].ToString(),
                                        ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                            }
                        }
                    } else {
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                            } else {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                        ds.Tables[0].Rows[i]["SurNameNative"]));
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                        ds.Tables[0].Rows[i]["SurNameNative"]));
                            } else {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                            }
                        }
                    }
                    if (!IsCyprus) //Not allowed to display id for individuals in cyprus
                    {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["number"].ToString()));
                    }
                    //if(isCreditInfoReport)
                    //	row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Education"+cultureEnding].ToString()));
                    row.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Title" + cultureEnding].ToString()));
                    board.AppendChild(row);
                }
            }

            if (board.ChildNodes.Count > 0) {
                board.InsertBefore(XmlHelper.GetXmlHeaderRowWithValue(doc, headers), board.FirstChild);
            } else {
                board.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));
            }

            //owners.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
            if (isCreditInfoReport && displayDDDMarksExplain) {
                board.AppendChild(
                    XmlHelper.GetXmlFooterRow(
                        doc, "1: " + rm.GetString("txtIndividualRegisteredInDDD", ci), headers.Length));
                board.AppendChild(
                    XmlHelper.GetXmlFooterRow(
                        doc, "2: " + rm.GetString("txtIndividualHasRelationsToBankrupt", ci), headers.Length));
                board.AppendChild(XmlHelper.GetXmlFooterRow(doc, "3: " + rm.GetString("txtBoth", ci), headers.Length));
            }
            return board;
        }

        private XmlElement getDebitorsDatabaseMark(XmlDocument doc, string ciID, string val) {
            XmlElement nameCol = XmlHelper.GetXmlColumn(doc, "");
            //Get CreditInfoID and check if in Debitors database
            if (ciID != null && ciID.Trim().Length > 0) {
                if (factory.IsIDInDebitorsDatabase(int.Parse(ciID))) {
                    nameCol.AppendChild(XmlHelper.GetXmlBoldText(doc, "1 "));
                }
            }
            nameCol.AppendChild(XmlHelper.GetXmlNormalText(doc, val));
            return nameCol;
        }

        //B�i�
        private XmlElement getKeyEmployees(XmlDocument doc, int companyCIID) {
            XmlElement keyEmp = XmlHelper.GetXmlTable(doc, "tblKeyEmployees");
            string[] headers;
            if (isCreditInfoReport) {
                headers = new string[4];
                headers[0] = rm.GetString("txtkeyEmployees", ci);
                if (IsCyprus) {
                    headers[1] = "";
                } else {
                    headers[1] = rm.GetString("txtRegistrationID", ci);
                }
                headers[2] = rm.GetString("txtEducation", ci);
                headers[3] = rm.GetString("txtPosition", ci);
            } else {
                headers = new string[3];
                headers[0] = rm.GetString("txtkeyEmployees", ci);
                if (IsCyprus) {
                    headers[1] = "";
                } else {
                    headers[1] = rm.GetString("txtRegistrationID", ci);
                }
                headers[2] = rm.GetString("txtPosition", ci);
            }

            //keyEmp.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));

            DataSet ds = factory.GetKeyEmployeesAsDataset(companyCIID);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                            row.AppendChild(
                                XmlHelper.GetXmlColumn(
                                    doc, ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                        } else {
                            row.AppendChild(
                                XmlHelper.GetXmlColumn(
                                    doc,
                                    ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                    ds.Tables[0].Rows[i]["SurNameNative"]));
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                            row.AppendChild(
                                XmlHelper.GetXmlColumn(
                                    doc,
                                    ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                    ds.Tables[0].Rows[i]["SurNameNative"]));
                        } else {
                            row.AppendChild(
                                XmlHelper.GetXmlColumn(
                                    doc, ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                        }
                    }
                    if (IsCyprus) {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, ""));
                    } else {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["number"].ToString()));
                    }
                    if (isCreditInfoReport) {
                        row.AppendChild(
                            XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Education" + cultureEnding].ToString()));
                    }
                    row.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["JobTitle" + cultureEnding].ToString()));
                    keyEmp.AppendChild(row);
                }
            }

            if (keyEmp.ChildNodes.Count > 0) {
                keyEmp.InsertBefore(XmlHelper.GetXmlHeaderRowWithValue(doc, headers), keyEmp.FirstChild);
            } else {
                keyEmp.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));
            }
            return keyEmp;
        }

        //B�i�
        private XmlElement getLawyers(XmlDocument doc, int companyCIID) {
            DataSet ds = factory.GetLaywersAsDataSet(companyCIID);
            if (ds == null || ds.Tables[0].Rows.Count <= 0) {
                return null;
            } else {
                XmlElement laywers = XmlHelper.GetXmlTable(doc, "tblLaywers");
                string[] headers;

                headers = new string[3];
                headers[0] = rm.GetString("txtLaywers", ci);
                if (IsCyprus) {
                    headers[1] = "";
                        //Not set reg id headers yes for cyprus - not allowed to display that for individuals
                } else {
                    headers[1] = rm.GetString("txtRegistrationID", ci);
                }
                headers[2] = rm.GetString("txtAddress", ci);

                //laywers.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    if (ds.Tables[0].Rows[i]["Type"].ToString().Trim() ==
                        CigConfig.Configure("lookupsettings.individualID")) {
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                            } else {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                        ds.Tables[0].Rows[i]["SurNameNative"]));
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                        ds.Tables[0].Rows[i]["SurNameNative"]));
                            } else {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                            }
                        }
                        if (IsCyprus) {
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, ""));
                        } else {
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Number"].ToString()));
                        }
                        row.AppendChild(
                            XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Street" + cultureEnding].ToString()));
                    } else {
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["NameNative"].ToString() == "") {
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameEN"].ToString()));
                            } else {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameNative"].ToString()));
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["NameEN"].ToString() == "") {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameNative"].ToString()));
                            } else {
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameEN"].ToString()));
                            }
                        }
                        if (IsCyprus) //We need the header for cyprus 
                        {
                            headers[1] = rm.GetString("txtRegistrationID", ci);
                        }
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Number"].ToString()));
                        row.AppendChild(
                            XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Street" + cultureEnding].ToString()));
                    }
                    laywers.AppendChild(row);
                }

                if (laywers.ChildNodes.Count > 0) {
                    laywers.InsertBefore(XmlHelper.GetXmlHeaderRowWithValue(doc, headers), laywers.FirstChild);
                } else {
                    laywers.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));
                }

                return laywers;
            }
        }

        //B�i�
        private XmlElement GetCompanySubsidaries(XmlDocument doc, int companyCIID) {
            DataSet ds = factory.GetCompanySubsidaries(companyCIID);
            if (ds == null || ds.Tables.Count <= 0) {
                return null;
            } else {
                XmlElement subsidaries = XmlHelper.GetXmlTable(doc, "tblCompanySubsidaries");

                XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
                XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
                hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtInvestmentsInOtherCompanies", ci)));
                XmlHelper.AddAttribute(doc, hCol, "colspan", "3");
                hRow.AppendChild(hCol);
                subsidaries.AppendChild(hRow);

                XmlElement row1 = XmlHelper.GetXmlRow(doc);
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtName", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtRegNr", ci)));
                subsidaries.AppendChild(row1);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    row.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString()));
                    row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Number"].ToString()));
                    subsidaries.AppendChild(row);
                }
                return subsidaries;
            }
        }

        //B�i�
        private XmlElement getAuditors(XmlDocument doc, int companyCIID) {
            DataSet ds = factory.GetAuditorsAsDataSet(companyCIID);
            if (ds == null || ds.Tables[0].Rows.Count <= 0) {
                return null;
            } else {
                XmlElement auditors = XmlHelper.GetXmlTable(doc, "tblAuditors");
                string[] headers;

                headers = new string[3];
                headers[0] = rm.GetString("txtAuditors", ci);
                if (IsCyprus) {
                    headers[1] = "";
                } else {
                    headers[1] = rm.GetString("txtRegistrationID", ci);
                }
                headers[2] = rm.GetString("txtAddress", ci);

                //auditors.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    if (ds.Tables[0].Rows[i]["Type"].ToString().Trim() ==
                        CigConfig.Configure("lookupsettings.individualID")) {
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                            } else {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                        ds.Tables[0].Rows[i]["SurNameNative"]));
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                        ds.Tables[0].Rows[i]["SurNameNative"]));
                            } else {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc,
                                        ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]));
                            }
                        }
                        if (IsCyprus) {
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, ""));
                        } else {
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Number"].ToString()));
                        }
                        row.AppendChild(
                            XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Street" + cultureEnding].ToString()));
                    } else {
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["NameNative"].ToString() == "") {
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameEN"].ToString()));
                            } else {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameNative"].ToString()));
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["NameEN"].ToString() == "") {
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameNative"].ToString()));
                            } else {
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameEN"].ToString()));
                            }
                        }
                        if (IsCyprus) {
                            headers[1] = rm.GetString("txtRegistrationID", ci);
                        }
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Number"].ToString()));
                        row.AppendChild(
                            XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Street" + cultureEnding].ToString()));
                    }
                    auditors.AppendChild(row);
                }

                if (auditors.ChildNodes.Count > 0) {
                    auditors.InsertBefore(XmlHelper.GetXmlHeaderRowWithValue(doc, headers), auditors.FirstChild);
                } else {
                    auditors.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));
                }
                return auditors;
            }
        }

        //B�i�
        private XmlElement getBoardSecretary(XmlDocument doc, int companyCIID) {
            XmlElement bsTable = XmlHelper.GetXmlTable(doc, "tblBoardSecretary");
            BoardSecretaryCompanyBLLC bsc = factory.GetCompanyBoardSecretary(companyCIID);

            if (bsc == null) {
                BoardSecretaryBLLC bs = factory.GetBoardSecretary(companyCIID);
                if (bs == null || bs.FirstNameNative == null) {
                    bsTable.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtBoardSecretary", ci)));
                    XmlElement row1 = XmlHelper.GetXmlRow(doc);
                    row1.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                    bsTable.AppendChild(row1);
                    return bsTable;
                } else {
                    string[] headers;
                    headers = new string[3];
                    headers[0] = rm.GetString("txtBoardSecretary", ci);
                    if (!IsCyprus) {
                        headers[1] = rm.GetString("txtRegistrationID", ci);
                    }
                    headers[2] = rm.GetString("txtHistory", ci);

                    //	bsTable.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));

                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    if (nativeCult) {
                        if (bs.FirstNameNative != null && bs.FirstNameNative.Trim() != "") {
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, bs.FirstNameNative + " " + bs.SurNameNative));
                        } else {
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, bs.FirstNameEN + " " + bs.SurNameEN));
                        }
                    } else {
                        if (bs.FirstNameEN != null && bs.FirstNameEN.Trim() != "") {
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, bs.FirstNameEN + " " + bs.SurNameEN));
                        } else {
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, bs.FirstNameNative + " " + bs.SurNameNative));
                        }
                    }

                    if (!IsCyprus) {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, bs.NationalID));
                    }
                    if (nativeCult) {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, bs.HistoryNative));
                    } else {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, bs.HistoryEN));
                    }
                    bsTable.AppendChild(row);

                    bsTable.InsertBefore(XmlHelper.GetXmlHeaderRowWithValue(doc, headers), bsTable.FirstChild);

                    return bsTable;
                }
            } else {
                string[] headers;
                headers = new string[3];
                headers[0] = rm.GetString("txtBoardSecretary", ci);
                headers[1] = rm.GetString("txtRegistrationID", ci);
                headers[2] = rm.GetString("txtHistory", ci);

                bsTable.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, headers));

                XmlElement row = XmlHelper.GetXmlRow(doc);
                if (nativeCult) {
                    if (bsc.NameNative != null && bsc.NameNative.Trim() != "") {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, bsc.NameNative));
                    } else {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, bsc.NameEN));
                    }
                } else {
                    if (bsc.NameEN != null && bsc.NameEN.Trim() != "") {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, bsc.NameEN));
                    } else {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, bsc.NameNative));
                    }
                }

                row.AppendChild(XmlHelper.GetXmlColumn(doc, bsc.NationalID));
                if (nativeCult) {
                    row.AppendChild(XmlHelper.GetXmlColumn(doc, bsc.HistoryNative));
                } else {
                    row.AppendChild(XmlHelper.GetXmlColumn(doc, bsc.HistoryEN));
                }
                bsTable.AppendChild(row);

                return bsTable;
            }
        }

        //Eftir
        private XmlElement getInvestments(XmlDocument doc, int companyCIID) {
            XmlElement investments = XmlHelper.GetXmlTable(doc, "tblInvestments");

            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtInvestmentsAndCompanyRelations", ci)));
            XmlHelper.AddAttribute(doc, hCol, "colspan", "5");
            hRow.AppendChild(hCol);
            investments.AppendChild(hRow);

            string[] headers;

            if (isCreditInfoReport) {
                headers = new string[5];
                headers[0] = rm.GetString("txtCompanyName", ci);
                headers[1] = rm.GetString("txtRegistrationID", ci);
                headers[2] = rm.GetString("txtOwnership", ci);
                headers[3] = rm.GetString("txtFormOfOperation", ci);
                headers[4] = rm.GetString("txtStatus", ci);
            } else {
                headers = new string[3];
                headers[0] = rm.GetString("txtCompanyName", ci);
                headers[1] = rm.GetString("txtRegistrationID", ci);
                headers[2] = rm.GetString("txtStatus", ci);
            }
            // just add header values if there are some values to display
            //	investments.AppendChild(XmlHelper.GetXmlRowWithValue(doc, headers, true));
            DataSet ds = factory.GetInvestmentsAsDataset(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                bool displayDDDMarksExplain = false;
                investments.AppendChild(XmlHelper.GetXmlRowWithValue(doc, headers, true));
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);

                    //Check if in DDD - only display explain of DDD marks if someone is in the DDD
                    if (factory.IsIDInDebitorsDatabase(int.Parse(ds.Tables[0].Rows[i]["CreditInfoID"].ToString()))) {
                        displayDDDMarksExplain = true;
                    }

                    row.AppendChild(
                        getDebitorsDatabaseMark(
                            doc,
                            ds.Tables[0].Rows[i]["CreditInfoID"].ToString(),
                            ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString()));
                    row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["RegistrationID"].ToString()));
                    if (isCreditInfoReport) {
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Ownership"].ToString()));
                        //Get NACE Codes and display as comma separated
                        DataSet naceDS =
                            factory.GetNACECodesAsDataSet(int.Parse(ds.Tables[0].Rows[i]["CreditInfoID"].ToString()));
                        string nace = "";
                        if (naceDS.Tables.Count > 0) {
                            for (int j = 0; j < naceDS.Tables[0].Rows.Count; j++) {
                                nace += naceDS.Tables[0].Rows[j]["Description" + cultureEnding] + ", ";
                            }
                        }
                        nace = nace.Trim();
                        if (nace.EndsWith(",")) {
                            nace = nace.Remove(nace.Length - 1, 1);
                        }
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, nace));
                    }
                    row.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Status" + cultureEnding].ToString()));

                    investments.AppendChild(row);
                    if (displayDDDMarksExplain) {
                        investments.AppendChild(
                            XmlHelper.GetXmlFooterRow(
                                doc, "1: " + rm.GetString("txtCompanyRegisteredInDDD", ci), headers.Length));
                    }
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                investments.AppendChild(noresult);
            }
            return investments;
        }

        //B�i�
        private XmlElement getRealEstates(XmlDocument doc, int companyCIID) {
            XmlElement realEstates = XmlHelper.GetXmlTable(doc, "tblRealEstates");

            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtRealEstates", ci)));
            XmlHelper.AddAttribute(doc, hCol, "colspan", "4");
            hRow.AppendChild(hCol);
            realEstates.AppendChild(hRow);

            DataSet ds = factory.GetRealEstatesAsDataset(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row1 = XmlHelper.GetXmlRow(doc);
                    row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtAddress", ci)));
                    row1.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Address" + cultureEnding].ToString()));
                    row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtPostalCode", ci)));
                    row1.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["PostCode"].ToString()));
                    realEstates.AppendChild(row1);

                    XmlElement row2 = XmlHelper.GetXmlRow(doc);
                    row2.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtInsuranceValue", ci)));
                    row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["InsuranceValue"].ToString()));
                    row2.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtYearBuilt", ci)));
                    row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["YearBuilt"].ToString()));
                    realEstates.AppendChild(row2);

                    XmlElement row3 = XmlHelper.GetXmlRow(doc);
                    row3.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtLocation", ci)));
                    row3.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Location" + cultureEnding].ToString()));
                    row3.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtSize", ci)));
                    row3.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Size"].ToString()));
                    realEstates.AppendChild(row3);

                    XmlElement row4 = XmlHelper.GetXmlRow(doc);
                    row4.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtTenure", ci)));
                    row4.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Tenure" + cultureEnding].ToString()));
                    row4.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtRealEstateType", ci)));
                    row4.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Type" + cultureEnding].ToString()));
                    realEstates.AppendChild(row4);

                    //Add a empty row
                    XmlElement eRow = XmlHelper.GetXmlRow(doc);
                    eRow.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                    realEstates.AppendChild(eRow);
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                realEstates.AppendChild(noresult);
            }
            return realEstates;
        }

        //B�i�
        private XmlElement getCyprusFormerNames(XmlDocument doc, string companyNationalID, bool nativeCult) {
            XmlElement cyprusFormerNames = XmlHelper.GetXmlTable(doc, "tblFormerNames");

            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtFormerlyKnownAs", ci)));
            XmlHelper.AddAttribute(doc, hCol, "colspan", "2");
            hRow.AppendChild(hCol);
            cyprusFormerNames.AppendChild(hRow);

            DataSet ds = factory.GetCyprusCompanyFormerNames(companyNationalID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row2 = XmlHelper.GetXmlRow(doc);
                    row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Org_name"].ToString()));
                    cyprusFormerNames.AppendChild(row2);
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                cyprusFormerNames.AppendChild(noresult);
            }
            return cyprusFormerNames;
        }

        //B�i�
        private XmlElement getCourtInfoAndDefaultingDebts(XmlDocument doc, int companyCIID) {
            XmlElement courtInfoAndDefaultingDebts = XmlHelper.GetXmlTable(doc, "tblCourtInfoAndDefaultingDebts");

            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtCourtInfoAndDefaultingDebts", ci)));
            XmlHelper.AddAttribute(doc, hCol, "colspan", "5");
            hRow.AppendChild(hCol);
            courtInfoAndDefaultingDebts.AppendChild(hRow);

            DataSet ds = factory.GetCourtDecisionsAndDefaultingDebts(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                // hva� me� milliheader?
                XmlElement row1 = XmlHelper.GetXmlRow(doc);
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtClaimOwner", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtClaimType", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtCapital", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtDate", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtRegNr", ci)));
                courtInfoAndDefaultingDebts.AppendChild(row1);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row2 = XmlHelper.GetXmlRow(doc);
                    if (ds.Tables[0].Rows[i]["ClaimOwnerName"].ToString() != null &&
                        ds.Tables[0].Rows[i]["ClaimOwnerName"].ToString() != "") {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["ClaimOwnerName"].ToString()));
                    } else {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                    }
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["TypeNative"].ToString() != null &&
                            ds.Tables[0].Rows[i]["TypeNative"].ToString() != "") {
                            row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["TypeNative"].ToString()));
                        } else {
                            row2.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["TypeEN"].ToString() != null &&
                            ds.Tables[0].Rows[i]["TypeEN"].ToString() != "") {
                            row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["TypeEN"].ToString()));
                        } else {
                            row2.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                        }
                    }
                    if (ds.Tables[0].Rows[i]["Amount"].ToString() != "" &&
                        ds.Tables[0].Rows[i]["Amount"].ToString() != null) {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Amount"].ToString()));
                    } else {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                    }
                    // breyta � short date string fyrir birtingu!
                    DateTime regDate = new DateTime();
                    try {
                        regDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["RegDate"]);
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, regDate.ToShortDateString()));
                    } catch (Exception) // if error show long date string rather than nothing
                    {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["RegDate"].ToString()));
                    }

                    if (ds.Tables[0].Rows[i]["CaseNr"].ToString() != null &&
                        ds.Tables[0].Rows[i]["CaseNr"].ToString() != "") {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["CaseNr"].ToString()));
                    } else {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                    }

                    courtInfoAndDefaultingDebts.AppendChild(row2);
                }
                //Add a empty row
                XmlElement eRow = XmlHelper.GetXmlRow(doc);
                eRow.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                courtInfoAndDefaultingDebts.AppendChild(eRow);
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoCourtInfoAndDefaultingDebts", ci)));
                courtInfoAndDefaultingDebts.AppendChild(noresult);
            }
            return courtInfoAndDefaultingDebts;
        }

        //B�i�
        private XmlElement getCompanyState(XmlDocument doc, int companyCIID) {
            XmlElement companyState = XmlHelper.GetXmlTable(doc, "tblCompanyState");

            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtCompanyState", ci)));
            XmlHelper.AddAttribute(doc, hCol, "colspan", "4");
            hRow.AppendChild(hCol);
            companyState.AppendChild(hRow);

            DataSet ds = factory.GetCompanyState(companyCIID);
            XmlElement row1 = XmlHelper.GetXmlRow(doc);
            string checkNA = "N/A";
            if (ds.Tables[0].Rows.Count > 0) {
                if (nativeCult) {
                    if (!checkNA.Equals(ds.Tables[0].Rows[0]["nameNative"].ToString())) {
                        row1.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[0]["nameNative"].ToString()));
                    } else {
                        return null;
                    }
                    //row1.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoCompanyStateInfoAvailable",ci)));
                } else {
                    if (!checkNA.Equals(ds.Tables[0].Rows[0]["nameNative"].ToString())) {
                        row1.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[0]["nativeEN"].ToString()));
                    } else {
                        return null;
                    }
                    //row1.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoCompanyStateInfoAvailable",ci)));
                }
                companyState.AppendChild(row1);
            } else {
                return null;
                //	row1.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoCompanyStateInfoAvailable",ci)));
                //	companyState.AppendChild(row1);
            }
            return companyState;
        }

        //B�i�
        private XmlElement getCapital(XmlDocument doc, int companyCIID) {
            XmlElement capital = XmlHelper.GetXmlTable(doc, "tblCapital");

            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtCapital", ci)));
            XmlHelper.AddAttribute(doc, hCol, "colspan", "6");
            hRow.AppendChild(hCol);
            capital.AppendChild(hRow);

            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                if (CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus")) {
                    capital.AppendChild(getStructureReportCapital(doc, companyCIID, false, false));
                    //Add a empty row
                    /*XmlElement eRow = XmlHelper.GetXmlRow(doc);
					eRow.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
					capital.AppendChild(eRow);*/
                    return capital;
                }
            }

            DataSet ds = factory.GetCapital(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                XmlElement row1 = XmlHelper.GetXmlRow(doc);
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtNominal", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtIssued", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtAsked", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtAuthorized", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtPaidUp", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtShareDescription", ci)));
                capital.AppendChild(row1);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    //AuthorizedCapital, IssuedNumberOfShares, Asked, PaidUp, NominalNumberOfShares, SharesDescription
                    XmlElement row2 = XmlHelper.GetXmlRow(doc);
                    row2.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NominalNumberOfShares"].ToString()));
                    row2.AppendChild(
                        XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["IssuedNumberOfShares"].ToString()));
                    row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Asked"].ToString()));
                    row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["AuthorizedCapital"].ToString()));
                    row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["PaidUp"].ToString()));
                    row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["SharesDescription"].ToString()));
                    capital.AppendChild(row2);

                    //Add a empty row
                    XmlElement eRow = XmlHelper.GetXmlRow(doc);
                    eRow.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                    capital.AppendChild(eRow);
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                capital.AppendChild(noresult);
            }

            return capital;
        }

        //B�i�
        private XmlElement getCharges(XmlDocument doc, int companyCIID, bool borderTable) {
            XmlElement charges;
            if (borderTable) {
                charges = XmlHelper.GetXmlTable(doc, "BorderTable", "tblCharges");
            } else {
                charges = XmlHelper.GetXmlTable(doc, "tblCharges");
            }

            XmlHelper.AddAttribute(doc, charges, "width", "100%");

            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtCharges", ci)));
            XmlHelper.AddAttribute(doc, hCol, "colspan", "7");
            hRow.AppendChild(hCol);
            charges.AppendChild(hRow);

            DataSet ds = factory.GetCharges(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                XmlElement row1 = XmlHelper.GetXmlRow(doc);
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtDateRegistered", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtDatePrepared", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtType", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtDescription", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtAmount", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtSequence", ci)));
                row1.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtBeneficiary", ci)));
                charges.AppendChild(row1);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    //AuthorizedCapital, IssuedNumberOfShares, Asked, PaidUp, NominalNumberOfShares, SharesDescription
                    XmlElement row2 = XmlHelper.GetXmlRow(doc);

                    if (!ds.Tables[0].Rows[i]["DateRegistered"].Equals(DBNull.Value)) {
                        row2.AppendChild(
                            XmlHelper.GetXmlColumn(
                                doc, ((DateTime) ds.Tables[0].Rows[i]["DateRegistered"]).ToShortDateString()));
                    } else {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                    }
                    if (!ds.Tables[0].Rows[i]["DatePrepared"].Equals(DBNull.Value)) {
                        row2.AppendChild(
                            XmlHelper.GetXmlColumn(
                                doc, ((DateTime) ds.Tables[0].Rows[i]["DatePrepared"]).ToShortDateString()));
                    } else {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                    }
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["DescriptionNative"].ToString() == "") {
                            row2.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                        } else {
                            row2.AppendChild(
                                XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["DescriptionNative"].ToString()));
                        }

                        if (ds.Tables[0].Rows[i]["DescriptionFreeTextNative"].ToString() == "") {
                            row2.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                        } else {
                            row2.AppendChild(
                                XmlHelper.GetXmlColumn(
                                    doc, ds.Tables[0].Rows[i]["DescriptionFreeTextNative"].ToString()));
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["DescriptionEN"].ToString() == "") {
                            row2.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                        } else {
                            row2.AppendChild(
                                XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["DescriptionEN"].ToString()));
                        }

                        if (ds.Tables[0].Rows[i]["DescriptionFreeTextEN"].ToString() == "") {
                            row2.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                        } else {
                            row2.AppendChild(
                                XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["DescriptionFreeTextEN"].ToString()));
                        }
                    }

                    row2.AppendChild(
                        XmlHelper.GetXmlColumn(
                            doc, ds.Tables[0].Rows[i]["Amount"] + ds.Tables[0].Rows[i]["CurrencyCode"].ToString()));
                    if (ds.Tables[0].Rows[i]["Sequence"].ToString() == "") {
                        row2.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                    } else {
                        row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Sequence"].ToString()));
                    }
                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["NameNative"].ToString() == "") {
                            row2.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                        } else {
                            row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameNative"].ToString()));
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["NameEN"].ToString() == "") {
                            row2.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
                        } else {
                            row2.AppendChild(XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["NameEN"].ToString()));
                        }
                    }
                    charges.AppendChild(row2);
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                charges.AppendChild(noresult);
            }
            return charges;
        }

        //B�i�
        private XmlElement getDefinitionOfRatios(XmlDocument doc) {
            XmlElement defOfRatios = XmlHelper.GetXmlTable(doc, "tblDefinitionOfRatios");

            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtDefinitionOfRatios", ci)));
            XmlHelper.AddAttribute(doc, hCol, "colspan", "5");
            hRow.AppendChild(hCol);
            defOfRatios.AppendChild(hRow);

            defOfRatios.AppendChild(
                XmlHelper.GetXmlRowWithValue(
                    doc, new[] {rm.GetString("txtProfitMargin", ci), rm.GetString("txtProfitMarginDef", ci)}, false));
            defOfRatios.AppendChild(
                XmlHelper.GetXmlRowWithValue(
                    doc, new[] {rm.GetString("txtCurrentRatio", ci), rm.GetString("txtCurrentRatioDef", ci)}, false));
            defOfRatios.AppendChild(
                XmlHelper.GetXmlRowWithValue(
                    doc, new[] {rm.GetString("txtQuickRatio", ci), rm.GetString("txtQuickRatioDef", ci)}, false));
            defOfRatios.AppendChild(
                XmlHelper.GetXmlRowWithValue(
                    doc, new[] {rm.GetString("txtEquityRatio", ci), rm.GetString("txtEquityRatioDef", ci)}, false));
            defOfRatios.AppendChild(
                XmlHelper.GetXmlRowWithValue(
                    doc, new[] {rm.GetString("txtDebtRatio", ci), rm.GetString("txtDebtRatioDef", ci)}, false));
            defOfRatios.AppendChild(
                XmlHelper.GetXmlRowWithValue(
                    doc, new[] {rm.GetString("txtCollectionTime", ci), rm.GetString("txtCollectionTimoDef", ci)}, false));
            defOfRatios.AppendChild(
                XmlHelper.GetXmlRowWithValue(
                    doc, new[] {rm.GetString("txtStockTurnover", ci), rm.GetString("txtStockTurnoverDef", ci)}, false));

            return defOfRatios;
        }

        //B�i�
        private XmlElement getBanks(XmlDocument doc, int companyCIID) {
            XmlElement banks = XmlHelper.GetXmlTable(doc, "tblBanks");

            banks.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtTradeBanks", ci)));

            DataSet ds = factory.GetBanksAsDataset(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string data = ds.Tables[0].Rows[i]["Name" + cultureEnding] + ", " + ds.Tables[0].Rows[i]["Location"] +
                                  ", " + ds.Tables[0].Rows[i]["Postalcode"] + ", " +
                                  ds.Tables[0].Rows[i]["Comment" + cultureEnding];
                    string strippedData = data.Replace(", 0, 0, ", ""); // if no address etc. info the skip that part
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    XmlElement col = XmlHelper.GetXmlColumn(doc, "");
                    col.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtBank", ci) + " "));
                    col.AppendChild(XmlHelper.GetXmlNormalText(doc, strippedData));
                    row.AppendChild(col);
                    banks.AppendChild(row);
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                banks.AppendChild(noresult);
            }
            return banks;
        }

        //B�i�
        private XmlElement getMaximumCredit(XmlDocument doc, int companyCIID, string afs_ids) {
            XmlElement maxCred = XmlHelper.GetXmlTable(doc, "tblMaxCredit");
            FISMaxCreditValues MaxCreditValues = new FISMaxCreditValues();

            maxCred.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtMaximumCredit", ci)));
            FinancialStatementFactory fsiFactory = new FinancialStatementFactory();

            string latest = "";
            string[] allAfs;
            double maxValue = -1;
            long equityTotal = -1;
            XmlElement col = XmlHelper.GetXmlColumn(doc, "");
            DateTime current = DateTime.Now;
            decimal creditScore = factory.GetCreditScoring(companyCIID, 1, DateTime.Now);
            int scoreCategory = factory.GetScoreCategory(creditScore, 1);
            int denomination_id = -1;
            int denominationMultiple = -1;

            string fsYear = "";
            if (afs_ids != "") {
                allAfs = afs_ids.Split(',');
                latest = allAfs[0]; //.ToString();
                MaxCreditValues = fsiFactory.GetMaxCreditValues(int.Parse(latest));
                fsYear = MaxCreditValues.FsYear; //fsiFactory.GetFinancialStatementYear(int.Parse(latest));
                // need to check if the company has DDD registrations
                DataSet ds = factory.GetCourtDecisionsAndDefaultingDebts(companyCIID);

                string strMaxValue = "";
                if (ds.Tables[0].Rows.Count > 0 || MaxCreditValues.EquityTotal < 0) {
                    col.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtSecureCredit", ci) + " "));
                    maxCred.AppendChild(col);
                    return maxCred;
                }
                if (fsYear != null && fsYear != "" && latest != null && latest != "") {
                    if ((current.Year - Convert.ToInt32(fsYear)) > 2) {
                        col.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtSecureCredit", ci) + " "));
                    } else {
                        // reikna h�r st�ffi�
                        equityTotal = MaxCreditValues.EquityTotal; //fsiFactory.GetEquityTotal(Convert.ToInt32(latest));
                        denomination_id = MaxCreditValues.DenominationID;
                            //fsiFactory.GetDenominationID(Convert.ToInt32(latest));
                        // �arf a� s�kja vi�eigandi multiple stu�ul �r config (nota� til a� forma amx credit result)
                        denominationMultiple = GetDenominationMultipleValue(denomination_id);
                        maxValue = getMaximumAmount(scoreCategory, equityTotal, denominationMultiple);
                            // denominationMultiple is harcoded to 1, for now ... 
                        //System.IFormatProvider format = CultureInfo.CreateSpecificCulture(CigConfig.Configure("lookupsettings.nativeCulture").ToString()); // use default culture for formatting
                        NumberFormatInfo format =
                            CultureInfo.CreateSpecificCulture(CigConfig.Configure("lookupsettings.nativeCulture")).
                                NumberFormat; // use default culture for formatting
                        format.CurrencyDecimalDigits = 0;
                        strMaxValue = maxValue.ToString("C", format);
                        //stbMaxValue.Append(strMaxValue.Substring(0,strMaxValue.Length-3));
                        /*if(denomination_id != 0 || denomination_id != 1)
							stbMaxValue.Append(" (" + rm.GetString("txtIn",ci) +" " + denominationDescription + " " + MaxCreditValues.Currency +")");
						else 
							stbMaxValue.Append("MaxCreditValues.Currency");*/

                        if (maxValue == 0) {
                            col.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtSecureCredit", ci) + " "));
                        } else {
                            col.AppendChild(
                                XmlHelper.GetXmlColumn(doc, rm.GetString("txtMaxCreditRecommended", ci) + strMaxValue));
                        }
                        //col.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtMaxCreditRecommended",ci)+ stbMaxValue));
                        //col.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtMaxCreditRecommended",ci)+ strMaxValue +" (" + denominationDescription + " " + MaxCreditValues.Currency +")"));
                    }
                } else {
                    col.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtSecureCredit", ci) + " "));
                }
            } else {
                col.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtSecureCredit", ci) + " "));
            }
            maxCred.AppendChild(col);

            return maxCred;
        }

        /// <summary>
        /// Reiknar �t fr� flokkunum Harald Moen. Forsendur eru �essar:
        /// Flokkar 1, 2, 3 & 4 == 20% af eigin f�
        /// Flokkar 5 og 6 == 15% af eigin f�
        /// Flokkar 7 og 8 == 10% af eigin f�
        /// Flokkar 9 og 10 v�ri s��an default �.e. "We recommend secure credit"
        /// </summary>
        /// <returns></returns>
        public double getMaximumAmount(int scoreCategory, long equityTotal, int multiplyValue) {
            double maxAmount = 0;
            if ((scoreCategory == 1) || (scoreCategory == 2) || (scoreCategory == 3) || (scoreCategory == 4)) {
                maxAmount = (0.2*equityTotal)*multiplyValue;
            } else if ((scoreCategory == 5) || (scoreCategory == 6)) {
                maxAmount = (0.15*equityTotal)*multiplyValue;
            } else if ((scoreCategory == 7) || (scoreCategory == 8)) {
                maxAmount = (0.1*equityTotal)*multiplyValue;
            } else {
                maxAmount = 0;
            }

            return maxAmount;
        }

        /// <summary>
        /// Gets the "right" value to use for formatting the max credit calculation
        /// </summary>
        /// <param name="denomination_id"></param>
        /// <returns></returns>
        private int GetDenominationMultipleValue(int denomination_id) {
            int multipleWith = -1;
            if (CigConfig.Configure("lookupsettings.denominationValue1") != null ||
                CigConfig.Configure("lookupsettings.denominationValue2") != null ||
                CigConfig.Configure("lookupsettings.denominationValue3") != null ||
                CigConfig.Configure("lookupsettings.denominationValue4") != null) {
                switch (denomination_id) {
                    case 1:
                        multipleWith = Convert.ToInt32(CigConfig.Configure("lookupsettings.denominationValue1"));
                        return multipleWith;
                    case 2:
                        multipleWith = Convert.ToInt32(CigConfig.Configure("lookupsettings.denominationValue2"));
                        return multipleWith;
                    case 3:
                        multipleWith = Convert.ToInt32(CigConfig.Configure("lookupsettings.denominationValue3"));
                        return multipleWith;
                    case 4:
                        multipleWith = Convert.ToInt32(CigConfig.Configure("lookupsettings.denominationValue4"));
                        return multipleWith;
                    default:
                        return 1; // multiply with 1 if failes
                }
            }
            return 1;
        }

        //B�i�
        private XmlElement getCustomerTypes(XmlDocument doc, int companyCIID) {
            XmlElement customerTypes = XmlHelper.GetXmlTable(doc, "tblCustomerType");

            customerTypes.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtCustomerType", ci)));

            DataSet ds = factory.GetCustomerTypeAsDataSet(companyCIID);
            if (ds.Tables.Count > 0) {
                if (ds.Tables[0].Rows.Count > 0) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        string data = ds.Tables[0].Rows[i]["CustomerType" + cultureEnding].ToString();
                        XmlElement row = XmlHelper.GetXmlRow(doc);
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, data));
                        customerTypes.AppendChild(row);
                    }
                } else {
                    XmlElement noresult = XmlHelper.GetXmlRow(doc);
                    noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                    customerTypes.AppendChild(noresult);
                }
            }
            return customerTypes;
        }

        //B�i�
        private XmlElement getImportFrom(XmlDocument doc, int companyCIID) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "tblImportFrom");

            table.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtImportFrom", ci)));

            DataSet ds = factory.GetImportFromTypeAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string data = ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString();
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    row.AppendChild(XmlHelper.GetXmlColumn(doc, data));
                    table.AppendChild(row);
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                table.AppendChild(noresult);
            }

            return table;
        }

        //B�i�
        private XmlElement getPaymentTerms(XmlDocument doc, int companyCIID) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "tblPaymentTerms");

            table.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtPaymentTerms", ci)));

            DataSet ds = factory.GetTradeTermsAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string show = ds.Tables[0].Rows[i]["SalesTerm"].ToString();
                    if (show.Equals("False")) {
                        string data = ds.Tables[0].Rows[i]["TermsDescription" + cultureEnding].ToString();
                        XmlElement row = XmlHelper.GetXmlRow(doc);
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, data));
                        table.AppendChild(row);
                    }
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                table.AppendChild(noresult);
            }
            return table;
        }

        //B�i�
        private XmlElement getBuyingTerms(XmlDocument doc, int companyCIID) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "tblBuyingTerms");

            table.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtBuyingTerms", ci)));

            DataSet ds = factory.GetTradeTermsAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string show = ds.Tables[0].Rows[i]["SalesTerm"].ToString();
                    if (show.Equals("True")) {
                        string data = ds.Tables[0].Rows[i]["TermsDescription" + cultureEnding].ToString();
                        XmlElement row = XmlHelper.GetXmlRow(doc);
                        row.AppendChild(XmlHelper.GetXmlColumn(doc, data));
                        table.AppendChild(row);
                    }
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtBuyingTerms", ci)));
                table.AppendChild(noresult);
            }
            return table;
        }

        //B�i�
        private XmlElement getExportTo(XmlDocument doc, int companyCIID) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "tblExportTo");

            table.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtExportTo", ci)));

            DataSet ds = factory.GetExportToAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string data = ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString();
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    row.AppendChild(XmlHelper.GetXmlColumn(doc, data));
                    table.AppendChild(row);
                }
            } else {
                XmlElement noresult = XmlHelper.GetXmlRow(doc);
                noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                table.AppendChild(noresult);
            }

            return table;
        }

        //B�i�
        private XmlElement getNACECode(XmlDocument doc, int companyCIID) {
            XmlElement naceCode = XmlHelper.GetXmlTable(doc, "tblNACECode");

            naceCode.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtNACECode", ci)));

            bool formerCodeRowNeeded = true;
            DataSet ds = factory.GetNACECodesAsDataSet(companyCIID);
            if (ds.Tables.Count > 0) {
                if (ds.Tables[0].Rows.Count > 0) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        string data = ds.Tables[0].Rows[i]["NaceCodeID"] + ": " +
                                      ds.Tables[0].Rows[i]["Description" + cultureEnding];
                        string currentCode = ds.Tables[0].Rows[i]["CurrentCode"].ToString();
                        if (currentCode.Equals("False")) {
                            if (!isCreditInfoReport) {
                                continue;
                            }
                            if (formerCodeRowNeeded) {
                                formerCodeRowNeeded = false;
                                naceCode.AppendChild(
                                    XmlHelper.GetXmlRowWithValue(doc, new[] {rm.GetString("txtPastNACECode", ci)}, true));
                            }
                        }
                        XmlElement row = XmlHelper.GetXmlRow(doc);
                        if (i == 0) // topmost item is primary code ... lets make that bold 
                        {
                            row.AppendChild(XmlHelper.GetXmlBoldColumn(doc, data));
                        } else {
                            row.AppendChild(XmlHelper.GetXmlColumn(doc, data));
                        }
                        naceCode.AppendChild(row);
                    }
                } else {
                    XmlElement noresult = XmlHelper.GetXmlRow(doc);
                    noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                    naceCode.AppendChild(noresult);
                }
            }
            return naceCode;
        }

        private XmlElement getEmptyRow(XmlDocument doc, int cols) {
            XmlElement eRow = XmlHelper.GetXmlHeaderRow(doc);
            for (int i = 0; i < cols; i++) {
                eRow.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
            }
            return eRow;
        }

        private String formatPostalCodeForCzech(string postalCode) {
            if (postalCode == null || postalCode == "") {
                return "N/A";
            }
            StringBuilder formatString = new StringBuilder(postalCode);
            formatString.Insert(3, " ");
            return formatString.ToString();
        }

        //B�i�
        protected XmlElement CZGetBusinessRegistryText(
            XmlDocument doc, string businessText, ReportCompanyBLLC theCompany) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "tblCompanyBusinessRegistryText");
            table.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtBusinessRegistryText", ci)));
            if (theCompany.UniqueID != null && theCompany.UniqueID.Trim() != "") {
                if (CZAddSecondLinkToBusinessRegistry(theCompany.RegistrationFormID)) {
                    if (CigConfig.Configure("lookupsettings.CZBusinessRegistryLink") != null) {
                        XmlElement secondLinkRow = XmlHelper.GetXmlRow(doc);
                        table.AppendChild(secondLinkRow);
                        string sLink = CigConfig.Configure("lookupsettings.CZBusinessRegistryLink");
                        sLink = sLink.Replace("%NationalID%", theCompany.UniqueID);
                        //linkRow.AppendChild(XmlHelper.GetPopupLinkColumn(doc, rm.GetString("txtBusinessRegistryLink",ci), "http://www.justice.cz/cgi-bin/sqw1250.cgi/or/l_subjf.sqw?INTRA=0&ICO="+nationalID));
                        secondLinkRow.AppendChild(
                            XmlHelper.GetPopupLinkColumn(doc, rm.GetString("txtBusinessRegistryLink", ci), sLink));
                    }
                }
                XmlElement linkRow = XmlHelper.GetXmlRow(doc);
                table.AppendChild(linkRow);
                if (CigConfig.Configure("lookupsettings.CZBusinessRegistryAresLink") != null) {
                    string sLink = CigConfig.Configure("lookupsettings.CZBusinessRegistryAresLink");
                    sLink = sLink.Replace("%NationalID%", theCompany.UniqueID);
                    //linkRow.AppendChild(XmlHelper.GetPopupLinkColumn(doc, rm.GetString("txtBusinessRegistryLink",ci), "http://www.justice.cz/cgi-bin/sqw1250.cgi/or/l_subjf.sqw?INTRA=0&ICO="+nationalID));
                    linkRow.AppendChild(
                        XmlHelper.GetPopupLinkColumn(doc, rm.GetString("txtBusinessRegistryAresLink", ci), sLink));
                }
            }
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetTextAsList(doc, businessText, false));
            table.AppendChild(row);
            return table;
        }

        protected bool CZAddSecondLinkToBusinessRegistry(int legalFormID) {
            int lfCode = int.Parse(factory.GetLegalFormCode(legalFormID));
            if (lfCode > -1) {
                ArrayList lfIDS = new ArrayList();
                lfIDS.AddRange(new[] {102, 104, 106, 111, 112, 113, 115, 117, 118, 121, 141, 145, 205, 301, 331, 421});
                if (lfIDS.Contains(lfCode)) {
                    return true;
                }
            }
            return false;
        }

        //B�i�
        protected XmlElement CZGetCompanyStatueDescriptionText(XmlDocument doc, string statusText) {
            XmlElement table = XmlHelper.GetXmlTable(doc, "tblCompanyStatueDescriptionText");
            table.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtCompanyStatusDescription", ci)));
            XmlElement row = XmlHelper.GetXmlRow(doc);
            row.AppendChild(XmlHelper.GetTextAsList(doc, statusText, false));
            table.AppendChild(row);
            return table;
        }

        public void addReportComponents(XmlDocument doc, XmlElement reportRoot, int companyCIID, string afs_ids) {
            //Here we need to check if this is Cyprus and Company Report - then redirect to another method because
            //the report is different from standard
            if (IsCyprus) {
                if (ReportType == companyReportID) {
                    AddStructureReportComponents(doc, reportRoot, companyCIID, afs_ids);
                    return;
                }
            }

            XmlElement basicInfoTable = XmlHelper.GetXmlTable(doc, "maintable", "tblBasicInfo");
            reportRoot.AppendChild(basicInfoTable);

            ReportCompanyBLLC theCompany = factory.GetCompanyReport("", companyCIID, false);

            HistoryOperationReviewBLLC theHistoryOperation = factory.GetHistoryOperationReview(companyCIID);
            if (theCompany != null) {
                basicInfoTable.AppendChild(getEmptyRow(doc, 4));

                //Add basic data for header
                XmlElement row1 = XmlHelper.GetXmlHeaderRow(doc);
                XmlElement col = XmlHelper.GetXmlHeaderColumn(doc, "");
                col.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtBasicInfoHeader", ci)));
                row1.AppendChild(XmlHelper.AddAttribute(doc, col, "colspan", "4"));
                basicInfoTable.AppendChild(row1);
                string workPhone = "";
                string fax = "";
                if (theCompany.PNumbers != null) {
                    foreach (PhoneNumber myNumber in theCompany.PNumbers) {
                        if (CigConfig.Configure("lookupsettings.workCode") == Convert.ToString(myNumber.NumberTypeID)) {
                            workPhone = myNumber.Number;
                        } else if (CigConfig.Configure("lookupsettings.faxCode") == Convert.ToString(myNumber.NumberTypeID)) {
                            fax = myNumber.Number;
                        }
                    }
                }

                if (nativeCult) {
                    basicInfoTable.AppendChild(
                        getBasicInfoRow(
                            doc,
                            rm.GetString("txtName", ci),
                            theCompany.NameNative,
                            rm.GetString("txtRegistrationID", ci),
                            theCompany.UniqueID));
                    if (theCompany.Address.Count > 0) {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(
                                doc,
                                rm.GetString("txtAddress", ci),
                                ((Address) theCompany.Address[0]).StreetNative,
                                rm.GetString("txtPhone", ci),
                                workPhone));
                    }
                } else {
                    basicInfoTable.AppendChild(
                        getBasicInfoRow(
                            doc,
                            rm.GetString("txtName", ci),
                            theCompany.NameEN,
                            rm.GetString("txtRegistrationID", ci),
                            theCompany.UniqueID));
                    if (theCompany.Address.Count > 0) {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(
                                doc,
                                rm.GetString("txtAddress", ci),
                                ((Address) theCompany.Address[0]).StreetEN,
                                rm.GetString("txtPhone", ci),
                                workPhone));
                    }
                }
                string postalCode = "";
                if (theCompany.Address.Count > 0) {
                    // check for Czech version and them format the postal code from 00000 to 000 00
                    if (IsCzech) {
                        postalCode = formatPostalCodeForCzech(((Address) theCompany.Address[0]).PostalCode);
                    } else {
                        postalCode = ((Address) theCompany.Address[0]).PostalCode;
                    }
                    basicInfoTable.AppendChild(
                        getBasicInfoRow(
                            doc, rm.GetString("txtPostalCode", ci), postalCode, rm.GetString("txtFax", ci), fax));
                }
                if (theCompany.Address.Count > 0) {
                    if (nativeCult) {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(
                                doc,
                                rm.GetString("txtCity", ci),
                                ((Address) theCompany.Address[0]).CityNameNative,
                                rm.GetString("txtVAT", ci),
                                theCompany.VAT));
                    } else {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(
                                doc,
                                rm.GetString("txtCity", ci),
                                ((Address) theCompany.Address[0]).CityNameEN,
                                rm.GetString("txtVAT", ci),
                                theCompany.VAT));
                    }
                }

                if (theCompany.Address.Count > 0) {
                    if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech")) {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(
                                doc,
                                "",
                                null,
                                rm.GetString("txtFounded", ci),
                                theCompany.Established.ToShortDateString()));
                    } else {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(
                                doc,
                                rm.GetString("txtPOBox", ci),
                                ((Address) theCompany.Address[0]).PostBox,
                                rm.GetString("txtFounded", ci),
                                theCompany.Established.ToShortDateString()));
                    }
                }
                if (IsCyprus) {
                    // leave former names part empty for Cyprus (that is in the basic info part)
                    if (nativeCult) {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(doc, rm.GetString("txtEnglishName", ci), theCompany.NameEN, "", null));
                    } else {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(doc, rm.GetString("txtEnglishName", ci), theCompany.NameEN, "", null));
                    }
                } else if (IsCzech) {
                    if (nativeCult) {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(doc, rm.GetString("txtEnglishName", ci), theCompany.NameEN, "", null));
                    } else {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(doc, rm.GetString("txtEnglishName", ci), theCompany.NameEN, "", null));
                    }
                } else {
                    if (nativeCult) {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(
                                doc,
                                rm.GetString("txtEnglishName", ci),
                                theCompany.NameEN,
                                rm.GetString("txtFormerlyKnownAs", ci),
                                theCompany.FormerNameNative));
                    } else {
                        basicInfoTable.AppendChild(
                            getBasicInfoRow(
                                doc,
                                rm.GetString("txtEnglishName", ci),
                                theCompany.NameEN,
                                rm.GetString("txtFormerlyKnownAs", ci),
                                theCompany.FormerNameEN));
                    }
                }

                if (IsCzech) {
                    basicInfoTable.AppendChild(
                        getBasicInfoRow(doc, null, null, rm.GetString("txtHomepage", ci), theCompany.HomePage));
                } else {
                    basicInfoTable.AppendChild(
                        getBasicInfoRow(
                            doc,
                            rm.GetString("txtNickName", ci),
                            theCompany.NameEN,
                            rm.GetString("txtHomepage", ci),
                            theCompany.HomePage));
                }
                basicInfoTable.AppendChild(
                    getBasicInfoRow(
                        doc,
                        rm.GetString("txtLastUpdate", ci),
                        theCompany.LastUpdated.ToShortDateString(),
                        rm.GetString("txtEmail", ci),
                        theCompany.Email));

                basicInfoTable.AppendChild(getEmptyRow(doc, 4));

                // get list of former names ... specially implemeted for Cyprus
                if (IsCyprus) {
                    reportRoot.AppendChild(getCyprusFormerNames(doc, theCompany.UniqueID, nativeCult));
                }

                // h�r �arf a� setja starfsemina � s�rl�nu
                //Add company form	(part of all reports) 	same as classification in Malta			
                reportRoot.AppendChild(
                    getCompanyForm(doc, factory.GetRegistrationFormAsString(theCompany.RegistrationFormID, nativeCult)));

                //Add credit scoring (just for creditReport and just if the CreditScoring key in config is set to True)
                if (IsCzech) {
                    if (reportType != companyProfileReportID) {
                        if (CigConfig.Configure("lookupsettings.CreditScoring") == "True") {
                            reportRoot.AppendChild(getCreditScoring(doc, companyCIID));
                        }
                    }
                }

                //Add default debtors database  (all reports)
                reportRoot.AppendChild(getCourtInfoAndDefaultingDebts(doc, companyCIID));
                //Add company history and operation (just for creditReport) !t�kka � null gildum
                if (reportType == creditReportID) {
                    if (nativeCult) {
                        if (theHistoryOperation.HistoryNative != "" && theHistoryOperation.HistoryNative != null) {
                            reportRoot.AppendChild(
                                XmlHelper.GetTableWithHeaderAndListRow(
                                    doc,
                                    "tblCompanyHistory",
                                    rm.GetString("txtCompanyHistory", ci),
                                    theHistoryOperation.HistoryNative,
                                    true));
                        }
                        // if no info then skip it
                        //	else
                        //		reportRoot.AppendChild(XmlHelper.GetTableWithHeaderAndSingleRow(doc, "tblCompanyHistory", rm.GetString("txtCompanyHistory",ci), rm.GetString("txtNoInformationAvailable",ci)));
                        if (!IsCyprus) {
                            if (theHistoryOperation.OperationNative != "" && theHistoryOperation.OperationNative != null) {
                                reportRoot.AppendChild(
                                    XmlHelper.GetTableWithHeaderAndListRow(
                                        doc,
                                        "tblCompanyOperation",
                                        rm.GetString("txtCompanyOperation", ci),
                                        theHistoryOperation.OperationNative,
                                        true));
                            }
                        }
                        // if no info then skip it
                        //	else
                        //		reportRoot.AppendChild(XmlHelper.GetTableWithHeaderAndSingleRow(doc, "tblCompanyOperation", rm.GetString("txtCompanyOperation",ci), rm.GetString("txtNoInformationAvailable",ci)));
                    } else {
                        if (theHistoryOperation.HistoryEN != "" && theHistoryOperation.HistoryEN != null) {
                            reportRoot.AppendChild(
                                XmlHelper.GetTableWithHeaderAndListRow(
                                    doc,
                                    "tblCompanyHistory",
                                    rm.GetString("txtCompanyHistory", ci),
                                    theHistoryOperation.HistoryEN,
                                    true));
                        }
                        // if no info then skip this part
                        //	else
                        //		reportRoot.AppendChild(XmlHelper.GetTableWithHeaderAndSingleRow(doc, "tblCompanyHistory", rm.GetString("txtCompanyHistory",ci), rm.GetString("txtNoInformationAvailable",ci)));
                        if (!IsCyprus) {
                            if (theHistoryOperation.OperationEN != "" && theHistoryOperation.OperationEN != null) {
                                reportRoot.AppendChild(
                                    XmlHelper.GetTableWithHeaderAndListRow(
                                        doc,
                                        "tblCompanyOperation",
                                        rm.GetString("txtCompanyOperation", ci),
                                        theHistoryOperation.OperationEN,
                                        true));
                            }
                        }
                        // if no info skip this part
                        //	else
                        //		reportRoot.AppendChild(XmlHelper.GetTableWithHeaderAndSingleRow(doc, "tblCompanyOperation", rm.GetString("txtCompanyOperation",ci), rm.GetString("txtNoInformationAvailable",ci)));
                    }
                }
                // the text from business registry (czech specific)
                if (IsCzech) {
                    if (nativeCult) {
                        if (theHistoryOperation.BusinessRegistryTextNative != null &&
                            theHistoryOperation.BusinessRegistryTextNative != "") {
                            reportRoot.AppendChild(
                                CZGetBusinessRegistryText(
                                    doc, theHistoryOperation.BusinessRegistryTextNative, theCompany));
                        }
                    } else {
                        if (theHistoryOperation.BusinessRegistryTextEN != null &&
                            theHistoryOperation.BusinessRegistryTextEN != "") {
                            reportRoot.AppendChild(
                                CZGetBusinessRegistryText(doc, theHistoryOperation.BusinessRegistryTextEN, theCompany));
                        }
                    }
                }

                //Add customer type (just for creditReport)
                if (reportType == creditReportID) {
                    if (!isCompanyProfileReport) {
                        reportRoot.AppendChild(getCustomerTypes(doc, companyCIID));
                    }
                }
                /* // t�k customer count �t. Er ekki � M�ltud�minu sem vi� erum h�r a� leggja til hli�sj�nar...
				//Add customer count (just for creditReport)
				if(reportType == creditReportID)
				{
					if(isCreditInfoReport)
						reportRoot.AppendChild(getCustomerCount(doc, theCompany.CustomerCount.ToString()));
				}
				*/
                //Add employees (just for creditReport)
                if (reportType == creditReportID) {
                    reportRoot.AppendChild(getEmployees(doc, companyCIID));
                }
                //Add NACE Codes (just for creditReport)
                //if(reportType == creditReportID)
                reportRoot.AppendChild(getNACECode(doc, companyCIID));

                //Add this here only for cyprus 
                if (IsCyprus) {
                    if (reportType == creditReportID) {
                        if (nativeCult) {
                            if (theHistoryOperation.OperationNative != "" && theHistoryOperation.OperationNative != null) {
                                reportRoot.AppendChild(
                                    XmlHelper.GetTableWithHeaderAndListRow(
                                        doc,
                                        "tblCompanyOperation",
                                        rm.GetString("txtCompanyOperation", ci),
                                        theHistoryOperation.OperationNative,
                                        true));
                            }
                        } else {
                            if (theHistoryOperation.OperationEN != "" && theHistoryOperation.OperationEN != null) {
                                reportRoot.AppendChild(
                                    XmlHelper.GetTableWithHeaderAndListRow(
                                        doc,
                                        "tblCompanyOperation",
                                        rm.GetString("txtCompanyOperation", ci),
                                        theHistoryOperation.OperationEN,
                                        true));
                            }
                        }
                    }
                }

                //Add Export to  (just for creditReport)
                if (reportType == creditReportID) {
                    reportRoot.AppendChild(getExportTo(doc, companyCIID));
                }
                //Add payment terms (just for creditReport)
                if (reportType == creditReportID) {
                    reportRoot.AppendChild(getPaymentTerms(doc, companyCIID));
                }
                //Add Import from (just for creditReport)
                if (reportType == creditReportID) {
                    reportRoot.AppendChild(getImportFrom(doc, companyCIID));
                }
                //Add buying terms (just for creditReport)
                if (reportType == creditReportID) {
                    reportRoot.AppendChild(getBuyingTerms(doc, companyCIID));
                }
                // Add company state
                XmlElement cStateEl = getCompanyState(doc, companyCIID);
                if (cStateEl != null) {
                    reportRoot.AppendChild(cStateEl);
                    if (nativeCult) {
                        if (theHistoryOperation.StatusDescriptionNative != null &&
                            theHistoryOperation.StatusDescriptionNative != "") {
                            reportRoot.AppendChild(
                                CZGetCompanyStatueDescriptionText(doc, theHistoryOperation.StatusDescriptionNative));
                        }
                    } else {
                        if (theHistoryOperation.StatusDescriptionEN != null &&
                            theHistoryOperation.StatusDescriptionEN != "") {
                            reportRoot.AppendChild(
                                CZGetCompanyStatueDescriptionText(doc, theHistoryOperation.StatusDescriptionEN));
                        }
                    }

                    /*if(nativeCult) 
					{
						if(theHistoryOperation.StatusDescriptionNative != null && theHistoryOperation.StatusDescriptionNative != "")
							reportRoot.AppendChild(XmlHelper.GetTableWithHeaderAndListRow(doc, "tblStatusDescription", rm.GetString("txtCompanyStatusDescription",ci), theHistoryOperation.StatusDescriptionNative, true));
					}
					else 
					{
						if(theHistoryOperation.StatusDescriptionEN != null && theHistoryOperation.StatusDescriptionEN != "")
							reportRoot.AppendChild(XmlHelper.GetTableWithHeaderAndListRow(doc, "tblStatusDescription", rm.GetString("txtCompanyStatusDescription",ci), theHistoryOperation.StatusDescriptionEN, true));
					}*/
                }
                // here we should have a table with Company involvements (includet in all reports) and that table includes 
                // Companies owned, Directors, Shareholders, Legal represetives(only for Malta?), Company secretary)
                // involvemets starts
                // add companies owned by report company (all reports)
                /*	XmlElement oc = GetCompanySubsidaries(doc, companyCIID);
						if(oc!=null)
							reportRoot.AppendChild(oc);
				*/
                //Add owners (all reports)
                if (IsCyprus && isCreditInfoReport) {
                    reportRoot.AppendChild(GetOwnersNewType(doc, companyCIID));
                } else {
                    reportRoot.AppendChild(getOwners(doc, companyCIID));
                }
                //Add board (all reports)
                if (IsCyprus && isCreditInfoReport) {
                    reportRoot.AppendChild(GetBoardNewType(doc, companyCIID));
                } else {
                    reportRoot.AppendChild(getBoard(doc, companyCIID));
                }
                //Add board secretary (all reports)
                if (!IsCzech) {
                    XmlElement bse = getBoardSecretary(doc, companyCIID);
                    if (bse != null) {
                        reportRoot.AppendChild(bse);
                    }
                }
                //Add laywers (just for credit report)
                if (reportType == creditReportID) {
                    XmlElement le = getLawyers(doc, companyCIID);
                    if (le != null) {
                        reportRoot.AppendChild(le);
                    }
                }
                //Add auditors (just for credit report)
                if (reportType == creditReportID) {
                    XmlElement ae = getAuditors(doc, companyCIID);
                    if (ae != null) {
                        reportRoot.AppendChild(ae);
                    }
                }
                // add legal representives (only excist for Malta? Check with Gu�ni?)

                // involements ends

                //Add key employees (just for creditreport)
                if (reportType == creditReportID) {
                    //if(IsCyprus)
                    //	reportRoot.AppendChild(GetKeyEmployeesNewType(doc, companyCIID));
                    //else
                    reportRoot.AppendChild(getKeyEmployees(doc, companyCIID));
                }

                //Add investments (for all reports)
                reportRoot.AppendChild(getInvestments(doc, companyCIID));
                //Add real estates (just for creditreport)
                if (reportType == creditReportID) {
                    reportRoot.AppendChild(getRealEstates(doc, companyCIID));
                }

                //Add banks (just for creditreport)
                if (reportType == creditReportID) {
                    reportRoot.AppendChild(getBanks(doc, companyCIID));
                }

                //Add Company Notes (just for creditreport)
                if (reportType == creditReportID) {
                    if (nativeCult) {
                        if (theHistoryOperation.CompanyReviewNative != "" &&
                            theHistoryOperation.CompanyReviewNative != null) {
                            reportRoot.AppendChild(
                                XmlHelper.GetTableWithHeaderAndSingleRow(
                                    doc,
                                    "tblCompanyNotes",
                                    rm.GetString("txtNotes", ci),
                                    theHistoryOperation.CompanyReviewNative));
                        }
                    } else {
                        if (theHistoryOperation.CompanyReviewEN != "" && theHistoryOperation.CompanyReviewEN != null) {
                            reportRoot.AppendChild(
                                XmlHelper.GetTableWithHeaderAndSingleRow(
                                    doc,
                                    "tblCompanyNotes",
                                    rm.GetString("txtNotes", ci),
                                    theHistoryOperation.CompanyReviewEN));
                        }
                    }
                }
                // Add capital section
                reportRoot.AppendChild(getCapital(doc, companyCIID));
                // financial statements (all report exept for company profile report)

                //Add charges
                if (reportType == creditReportID) {
                    if (IsCyprus) {
                        reportRoot.AppendChild(getCharges(doc, companyCIID, true));
                    } else {
                        reportRoot.AppendChild(getCharges(doc, companyCIID, false));
                    }
                }
                if (reportType != companyProfileReportID) {
                    if (afs_ids != null && afs_ids.Trim() != "") {
                        //Add balance sheet
                        reportRoot.AppendChild(getBalanceSheet(doc, companyCIID, afs_ids));
                        //Add profit/loss account
                        reportRoot.AppendChild(getProfitLossAccount(doc, companyCIID, afs_ids));
                        // Add maxCredit for Czech
                        if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                            if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech")) {
                                reportRoot.AppendChild(getMaximumCredit(doc, companyCIID, afs_ids));
                            }
                        }
                        //Add definition of ratios
                        reportRoot.AppendChild(getDefinitionOfRatios(doc));
                        //mainTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
                    }
                }

                AddEndOfReport(doc, reportRoot);
            }
        }

        //Eftir
        protected XmlElement GetRegistrationForm(XmlDocument doc, string regForm) {
            XmlElement registrationForm = XmlHelper.GetXmlTable(doc, "RegistrationForm", "tblRegistrationForm");
            XmlHelper.AddAttribute(doc, registrationForm, "title", rm.GetString("txtCompanyLegalForm", ci));
            registrationForm.AppendChild(XmlHelper.CreateElement(doc, "RegForm", "value", regForm));
            return registrationForm;
        }

        //B�i�
        protected void AddEndOfReport(XmlDocument doc, XmlElement reportRoot) {
            XmlElement endTable = XmlHelper.GetXmlTable(doc, "tblEnd");
            endTable.AppendChild(XmlHelper.GetXmlRowWithValue(doc, new[] {rm.GetString("txtEndOfReport", ci)}, false));
            reportRoot.AppendChild(endTable);

            //reportRoot.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

            XmlElement disclaimerTable = XmlHelper.GetXmlTable(doc, "tblDisclaimer");
            disclaimerTable.AppendChild(
                XmlHelper.GetXmlRowWithValue(doc, new[] {rm.GetString("txtReportDisclaimer", ci)}, false));
            reportRoot.AppendChild(disclaimerTable);
        }

        //Eftir
        protected void AddCyprusCompanyReportComponents(
            XmlDocument doc,
            XmlElement reportRoot,
            int companyCIID,
            string afs_ids,
            ReportCompanyBLLC theCompany,
            HistoryOperationReviewBLLC theHistoryOperation) {
            //Add company history
            if (nativeCult) {
                if (theHistoryOperation.HistoryNative != "" && theHistoryOperation.HistoryNative != null) {
                    reportRoot.AppendChild(
                        XmlHelper.GetTableWithHeaderAndSingleRow(
                            doc,
                            "tblCompanyHistory",
                            rm.GetString("txtCompanyHistory", ci),
                            theHistoryOperation.HistoryNative));
                } else {
                    reportRoot.AppendChild(
                        XmlHelper.GetTableWithHeaderAndSingleRow(
                            doc,
                            "tblCompanyHistory",
                            rm.GetString("txtCompanyHistory", ci),
                            rm.GetString("txtNoInformationAvailable", ci)));
                }
            } else {
                if (theHistoryOperation.HistoryEN != "" && theHistoryOperation.HistoryEN != null) {
                    reportRoot.AppendChild(
                        XmlHelper.GetTableWithHeaderAndSingleRow(
                            doc,
                            "tblCompanyHistory",
                            rm.GetString("txtCompanyHistory", ci),
                            theHistoryOperation.HistoryEN));
                } else {
                    reportRoot.AppendChild(
                        XmlHelper.GetTableWithHeaderAndSingleRow(
                            doc,
                            "tblCompanyHistory",
                            rm.GetString("txtCompanyHistory", ci),
                            rm.GetString("txtNoInformationAvailable", ci)));
                }
            }

            //Add shareholders
            reportRoot.AppendChild(getOwners(doc, companyCIID));

            //Add directors and directors history
            reportRoot.AppendChild(getKeyEmployees(doc, companyCIID));

            //Add secretary
            reportRoot.AppendChild(getBoardSecretary(doc, companyCIID));

            //Add capital
            reportRoot.AppendChild(getCapital(doc, companyCIID));

            //Add charges
            reportRoot.AppendChild(getCharges(doc, companyCIID, false));
        }

        //Eftir
        private XmlElement GetStructureReportBoardSecretary(XmlDocument doc, int companyCIID) {
            XmlElement bsTable = XmlHelper.GetXmlTable(doc, "BorderTable", "tblBoardSecretary");
            XmlHelper.AddAttribute(doc, bsTable, "width", "100%");
            XmlElement row = XmlHelper.GetXmlRow(doc);
            bsTable.AppendChild(row);
            XmlElement titleCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            XmlHelper.AddAttribute(doc, titleCol, "width", "25%");
            titleCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtBoardSecretary", ci)));
            row.AppendChild(titleCol);

            BoardSecretaryCompanyBLLC bsc = factory.GetCompanyBoardSecretary(companyCIID);
            if (bsc == null) {
                BoardSecretaryBLLC bs = factory.GetBoardSecretary(companyCIID);
                if (bs == null) {
                    row.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                    return bsTable;
                } else {
                    string secString = "";

                    if (nativeCult) {
                        if (bs.FirstNameNative != null && bs.FirstNameNative.Trim() != "") {
                            secString = bs.FirstNameNative + " " + bs.SurNameNative;
                        } else {
                            secString = bs.FirstNameEN + " " + bs.SurNameEN;
                        }
                        if (bs.Address != null && bs.Address.Count > 0) {
                            secString += " - ";
                            Address addr = (Address) bs.Address[0];

                            if (addr.StreetNative != null && addr.StreetNative.Trim() != "") {
                                secString += addr.StreetNative;
                            } else {
                                secString += addr.StreetEN;
                            }

                            if (addr.PostalCode != null && addr.PostalCode.Trim() != "") {
                                secString += ", " + addr.PostalCode;
                            }

                            if (addr.CityNameNative != null && addr.CityNameNative.Trim() != "") {
                                secString += ", " + addr.CityNameNative;
                            } else {
                                secString += ", " + addr.CityNameEN;
                            }
                        }
                    } else {
                        if (bs.FirstNameEN != null && bs.FirstNameEN.Trim() != "") {
                            secString = bs.FirstNameEN + " " + bs.SurNameEN;
                        } else {
                            secString = bs.FirstNameNative + " " + bs.SurNameNative;
                        }

                        if (bs.Address != null && bs.Address.Count > 0) {
                            secString += " - ";
                            Address addr = (Address) bs.Address[0];
                            if (addr.StreetEN != null && addr.StreetEN.Trim() != "") {
                                secString += addr.StreetEN;
                            } else {
                                secString += addr.StreetNative;
                            }

                            if (addr.PostalCode != null && addr.PostalCode.Trim() != "") {
                                secString += ", " + addr.PostalCode;
                            }

                            if (addr.CityNameEN != null && addr.CityNameEN.Trim() != "") {
                                secString += ", " + addr.CityNameEN;
                            } else {
                                secString += ", " + addr.CityNameNative;
                            }
                        }
                    }

                    row.AppendChild(XmlHelper.GetXmlColumn(doc, secString));

                    return bsTable;
                }
            } else {
                string secString = "";

                if (nativeCult) {
                    if (bsc.NameNative != null && bsc.NameNative.Trim() != "") {
                        secString = bsc.NameNative;
                    } else {
                        secString = bsc.NameEN;
                    }
                    if (bsc.Address != null && bsc.Address.Count > 0) {
                        secString += " - ";
                        Address addr = (Address) bsc.Address[0];

                        if (addr.StreetNative != null && addr.StreetNative.Trim() != "") {
                            secString += addr.StreetNative;
                        } else {
                            secString += addr.StreetEN;
                        }

                        if (addr.PostalCode != null && addr.PostalCode.Trim() != "") {
                            secString += ", " + addr.PostalCode;
                        }

                        if (addr.CityNameNative != null && addr.CityNameNative.Trim() != "") {
                            secString += ", " + addr.CityNameNative;
                        } else {
                            secString += ", " + addr.CityNameEN;
                        }
                    }
                } else {
                    if (bsc.NameEN != null && bsc.NameEN.Trim() != "") {
                        secString = bsc.NameEN;
                    } else {
                        secString = bsc.NameNative;
                    }

                    if (bsc.Address != null && bsc.Address.Count > 0) {
                        secString += " - ";
                        Address addr = (Address) bsc.Address[0];
                        if (addr.StreetEN != null && addr.StreetEN.Trim() != "") {
                            secString += addr.StreetEN;
                        } else {
                            secString += addr.StreetNative;
                        }

                        if (addr.PostalCode != null && addr.PostalCode.Trim() != "") {
                            secString += ", " + addr.PostalCode;
                        }

                        if (addr.CityNameEN != null && addr.CityNameEN.Trim() != "") {
                            secString += ", " + addr.CityNameEN;
                        } else {
                            secString += ", " + addr.CityNameNative;
                        }
                    }
                }

                row.AppendChild(XmlHelper.GetXmlColumn(doc, secString));

                return bsTable;
            }
        }

        //Eftir
        private XmlElement getStructureReportCapital(
            XmlDocument doc, int companyCIID, bool includeHeader, bool borderTable) {
            XmlElement capital;
            if (borderTable) {
                capital = XmlHelper.GetXmlTable(doc, "BorderTable", "tblCapital");
            } else {
                capital = XmlHelper.GetXmlTable(doc, "tblCapital");
            }
            XmlHelper.AddAttribute(doc, capital, "width", "100%");

            XmlElement upperRow = XmlHelper.GetXmlRow(doc);
            XmlElement lowerRow = XmlHelper.GetXmlRow(doc);

            capital.AppendChild(upperRow);
            capital.AppendChild(lowerRow);
            if (includeHeader) {
                XmlElement titleCol = XmlHelper.GetXmlColumn(doc, "RowSpanHeaderCol", "");
                XmlHelper.AddAttribute(doc, titleCol, "class", "header2");
                XmlHelper.AddAttribute(doc, titleCol, "rowspan", "2");
                titleCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtCapital", ci)));
                upperRow.AppendChild(titleCol);
            }

            XmlElement acCol;
            XmlElement inosCol;
            XmlElement askedCol;
            XmlElement puCol;

            NumberFormatInfo capitalFormat = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            capitalFormat.PercentDecimalDigits = 2;
            capitalFormat.NumberDecimalDigits = 2;

            DataSet ds = factory.GetCapital(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                try {
                    inosCol = XmlHelper.GetXmlColumn(
                        doc,
                        XmlHelper.RIGHT_COLUMN,
                        double.Parse(ds.Tables[0].Rows[0]["IssuedNumberOfShares"].ToString()).ToString(
                            "N", capitalFormat));
                } catch (Exception) {
                    inosCol = XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, "N/A");
                }

                try {
                    askedCol = XmlHelper.GetXmlColumn(
                        doc,
                        XmlHelper.RIGHT_COLUMN,
                        double.Parse(ds.Tables[0].Rows[0]["Asked"].ToString()).ToString("N", capitalFormat));
                } catch (Exception) {
                    askedCol = XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, "N/A");
                }

                try {
                    acCol = XmlHelper.GetXmlColumn(
                        doc,
                        XmlHelper.RIGHT_COLUMN,
                        double.Parse(ds.Tables[0].Rows[0]["AuthorizedCapital"].ToString()).ToString("N", capitalFormat));
                } catch (Exception) {
                    acCol = XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, "N/A");
                }

                try {
                    puCol = XmlHelper.GetXmlColumn(
                        doc,
                        XmlHelper.RIGHT_COLUMN,
                        double.Parse(ds.Tables[0].Rows[0]["PaidUp"].ToString()).ToString("N", capitalFormat) + " " +
                        ds.Tables[0].Rows[0]["CurrencyCode"]);
                } catch (Exception) {
                    puCol = XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, "N/A");
                }

                /*	//Add a empty row
					XmlElement eRow = XmlHelper.GetXmlRow(doc);
					eRow.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));
					capital.AppendChild(eRow);
				}*/
            } else {
                inosCol = XmlHelper.GetXmlColumn(
                    doc, XmlHelper.RIGHT_COLUMN, rm.GetString("txtNoInformationAvailable", ci));
                askedCol = XmlHelper.GetXmlColumn(
                    doc, XmlHelper.RIGHT_COLUMN, rm.GetString("txtNoInformationAvailable", ci));
                acCol = XmlHelper.GetXmlColumn(
                    doc, XmlHelper.RIGHT_COLUMN, rm.GetString("txtNoInformationAvailable", ci));
                puCol = XmlHelper.GetXmlColumn(
                    doc, XmlHelper.RIGHT_COLUMN, rm.GetString("txtNoInformationAvailable", ci));

                /*XmlElement noresult = XmlHelper.GetXmlRow(doc);
				noresult.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable",ci)));
				capital.AppendChild(noresult);*/
            }

            upperRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtAuthorized", ci)));
            upperRow.AppendChild(acCol);

            upperRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtIssued", ci)));
            upperRow.AppendChild(inosCol);

            lowerRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtAsked", ci)));
            lowerRow.AppendChild(askedCol);

            lowerRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtPaidUp", ci)));
            lowerRow.AppendChild(puCol);
            return capital;
        }

        //Eftir
        private XmlElement getStructureReportAnalyzeOfCapital(XmlDocument doc, int companyCIID) {
            XmlElement capital = XmlHelper.GetXmlTable(doc, "BorderTable", "tblAnalyzeOfCapital");
            XmlHelper.AddAttribute(doc, capital, "width", "100%");

            XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement hCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            hCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtAnalyzeOfAuthorizedCapital", ci)));
            XmlHelper.AddAttribute(doc, hCol, "colspan", "2");
            hRow.AppendChild(hCol);
            capital.AppendChild(hRow);

            XmlElement upperRow = XmlHelper.GetXmlRow(doc);
            XmlElement lowerRow = XmlHelper.GetXmlRow(doc);

            capital.AppendChild(upperRow);
            capital.AppendChild(lowerRow);

            XmlElement nominalCol;
            XmlElement descCol;

            DataSet ds = factory.GetCapital(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                try {
                    NumberFormatInfo capitalFormat = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
                    capitalFormat.PercentDecimalDigits = 2;
                    capitalFormat.NumberDecimalDigits = 2;
                    nominalCol = XmlHelper.GetXmlColumn(
                        doc,
                        XmlHelper.RIGHT_COLUMN,
                        double.Parse(ds.Tables[0].Rows[0]["NominalNumberOfShares"].ToString()).ToString(
                            "N", capitalFormat));
                } catch (Exception) {
                    nominalCol = XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, "N/A");
                }
                descCol = XmlHelper.GetXmlColumn(
                    doc, XmlHelper.RIGHT_COLUMN, ds.Tables[0].Rows[0]["SharesDescription"].ToString());
            } else {
                nominalCol = XmlHelper.GetXmlColumn(
                    doc, XmlHelper.RIGHT_COLUMN, rm.GetString("txtNoInformationAvailable", ci));
                descCol = XmlHelper.GetXmlColumn(
                    doc, XmlHelper.RIGHT_COLUMN, rm.GetString("txtNoInformationAvailable", ci));
            }

            upperRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtNominal", ci)));
            upperRow.AppendChild(nominalCol);

            lowerRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtShareDescription", ci)));
            lowerRow.AppendChild(descCol);

            return capital;
        }

        //Eftir
        private XmlElement getStructureReportShareholdersOwners(XmlDocument doc, int companyCIID) {
            XmlElement owners = XmlHelper.GetXmlTable(doc, "BorderTable", "tblShareholders");
            XmlHelper.AddAttribute(doc, owners, "width", "100%");
            int ownerShipTypes = 1; //Ordinary shares
            if (CigConfig.Configure("lookupsettings.OwnershipTypes") != null) {
                ownerShipTypes = int.Parse(CigConfig.Configure("lookupsettings.OwnershipTypes"));
            }
            //	DataSet ds = factory.GetShareHolderOwnerAsDataSet(companyCIID,ownerShipTypes);
            DataSet ds = factory.GetShareHolderOwnerAsDataSet(companyCIID);
            if (ds.Tables[0].Rows.Count > 0) {
                XmlElement hRow = XmlHelper.GetXmlHeaderRow(doc);
                hRow.AppendChild(XmlHelper.GetXmlHeaderColumn(doc, rm.GetString("txtOwnersShareholders", ci)));
                hRow.AppendChild(XmlHelper.GetXmlRightHeaderColumn(doc, rm.GetString("txtNumberOfShares", ci)));
                hRow.AppendChild(XmlHelper.GetXmlRightHeaderColumn(doc, rm.GetString("txtPercentage", ci)));
                hRow.AppendChild(XmlHelper.GetXmlRightHeaderColumn(doc, rm.GetString("txtOwnershipType", ci)));
                owners.AppendChild(hRow);
                ArrayList arrCiids = new ArrayList();
                ArrayList arrShareTypesIDs = new ArrayList();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    bool idFound = false;
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    string shareTypesID = ds.Tables[0].Rows[i]["OwnershipID"].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (((string) arrCiids[j]).Equals(creditInfoId) &&
                            ((string) arrShareTypesIDs[j]).Equals(shareTypesID)) {
                            idFound = true;
                            break;
                        }
                    }
                    if (!idFound) {
                        arrCiids.Add(creditInfoId);
                        arrShareTypesIDs.Add(shareTypesID);
                        XmlElement row = XmlHelper.GetXmlRow(doc);
                        string userType = ds.Tables[0].Rows[i]["Type"].ToString().Trim();
                        if (userType.Equals(CigConfig.Configure("lookupsettings.individualID"))) {
                            row.AppendChild(
                                XmlHelper.GetXmlColumn(
                                    doc,
                                    ds.Tables[0].Rows[i]["FirstName" + cultureEnding] + " " +
                                    ds.Tables[0].Rows[i]["SurName" + cultureEnding]));
                        } else {
                            row.AppendChild(
                                XmlHelper.GetXmlColumn(doc, ds.Tables[0].Rows[i]["Name" + cultureEnding].ToString()));
                        }
                        NumberFormatInfo ownerShipInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
                        ownerShipInfo.PercentDecimalDigits = 2;
                        ownerShipInfo.NumberDecimalDigits = 2;

                        string ownerShip = ds.Tables[0].Rows[i]["ownership"].ToString();
                        if (ownerShip != null && ownerShip.Trim() != null) {
                            try {
                                long lOwnerShip = long.Parse(ownerShip);
                                row.AppendChild(
                                    XmlHelper.GetXmlColumn(
                                        doc, XmlHelper.RIGHT_COLUMN, lOwnerShip.ToString("N", ownerShipInfo)));
                                decimal numOfShares = factory.GetNumberOfShares(companyCIID);
                                if (numOfShares > -1) {
                                    try {
                                        double d1 = double.Parse(ownerShip);
                                        double d2 = (double) numOfShares;
                                        double per = (d1/d2)*100;

                                        //NumberFormatInfo fInfo = (NumberFormatInfo)NumberFormatInfo.CurrentInfo.Clone();
                                        //fInfo.PercentDecimalDigits = 2;
                                        //return dNumber.ToString("N", fInfo);

                                        row.AppendChild(
                                            XmlHelper.GetXmlColumn(
                                                doc, XmlHelper.RIGHT_COLUMN, per.ToString("N", ownerShipInfo)));
                                    } catch (Exception) {
                                        row.AppendChild(XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, "N/A"));
                                    }
                                } else {
                                    row.AppendChild(XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, "N/A"));
                                }
                            } catch (Exception) {
                                //Error finding no of shares - then add N/A to both number of shares and percentage
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, "N/A"));
                                row.AppendChild(XmlHelper.GetXmlColumn(doc, XmlHelper.RIGHT_COLUMN, "N/A"));
                            }
                        }
                        row.AppendChild(
                            XmlHelper.GetXmlColumn(
                                doc, ds.Tables[0].Rows[i]["OwnershipDescription" + cultureEnding].ToString()));
                        owners.AppendChild(row);
                    }
                }
            } else {
                owners.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtOwnersShareholders", ci)));
                XmlElement row1 = XmlHelper.GetXmlRow(doc);
                row1.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                owners.AppendChild(row1);
            }

            return owners;
        }

        //Eftir
        private XmlElement getBoardMembersAndBoardHistory(XmlDocument doc, int companyCIID) {
            XmlElement directors = XmlHelper.GetXmlTable(doc, "BorderTable", "tblBoardMembersAndHistory");
            XmlHelper.AddAttribute(doc, directors, "width", "100%");

            XmlElement dRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement dCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            dCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtDirectors", ci)));
            //XmlHelper.AddAttribute(doc, hCol, "colspan", "2");
            dRow.AppendChild(dCol);
            directors.AppendChild(dRow);

            DataSet ds = factory.GetBoardAsDataset(companyCIID);
            if (ds.Tables.Count > 0) {
                ds = cleanDuplicatedBMembersCIIDS(ds);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    string dir = "";

                    if (nativeCult) {
                        if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString().Trim() != "") {
                            dir = ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"];
                        } else {
                            dir = ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"];
                        }
                        dir += " - ";
                        if (ds.Tables[0].Rows[i]["StreetNative"].ToString().Trim() != "") {
                            dir += ds.Tables[0].Rows[i]["StreetNative"];
                        }
                        if (ds.Tables[0].Rows[i]["PostalCode"].ToString().Trim() != "") {
                            dir += ", " + ds.Tables[0].Rows[i]["PostalCode"];
                        }
                        if (ds.Tables[0].Rows[i]["CityNameNative"].ToString().Trim() != "") {
                            dir += ", " + ds.Tables[0].Rows[i]["CityNameNative"].ToString().Trim();
                        }
                    } else {
                        if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString().Trim() != "") {
                            dir = ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"];
                        } else {
                            dir = ds.Tables[0].Rows[i]["FirstNameNative"] + " " + ds.Tables[0].Rows[i]["SurNameNative"];
                        }
                        dir += " - ";
                        if (ds.Tables[0].Rows[i]["StreetEN"].ToString().Trim() != "") {
                            dir += ds.Tables[0].Rows[i]["StreetEN"];
                        }
                        if (ds.Tables[0].Rows[i]["PostalCode"].ToString().Trim() != "") {
                            dir += ", " + ds.Tables[0].Rows[i]["PostalCode"];
                        }
                        if (ds.Tables[0].Rows[i]["CityNameEN"].ToString().Trim() != "") {
                            dir += ", " + ds.Tables[0].Rows[i]["CityNameEN"].ToString().Trim();
                        }
                    }
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    row.AppendChild(XmlHelper.GetXmlColumn(doc, dir));
                    directors.AppendChild(row);
                }
            }

            //Now add directors history
            XmlElement dhRow = XmlHelper.GetXmlHeaderRow(doc);
            XmlElement dhCol = XmlHelper.GetXmlHeaderColumn(doc, "");
            dhCol.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtDirectorsHistory", ci)));
            //XmlHelper.AddAttribute(doc, hCol, "colspan", "2");
            dhRow.AppendChild(dhCol);
            directors.AppendChild(dhRow);

            XmlElement historyRow = XmlHelper.GetXmlRow(doc);
            directors.AppendChild(historyRow);

            HistoryBLLC history = factory.GetHistory(
                companyCIID, Convert.ToInt32(CigConfig.Configure("lookupsettings.BoardMembersHistoryType")));

            if (history != null) {
                if (nativeCult) {
                    if (history.HistoryNative != null && history.HistoryNative.Trim() != "") {
                        historyRow.AppendChild(XmlHelper.GetTextAsList(doc, history.HistoryNative, true));
                    } else if (history.HistoryEN != null && history.HistoryEN.Trim() != "") {
                        historyRow.AppendChild(XmlHelper.GetTextAsList(doc, history.HistoryEN, true));
                    }
                } else {
                    if (history.HistoryEN != null && history.HistoryEN.Trim() != "") {
                        historyRow.AppendChild(XmlHelper.GetTextAsList(doc, history.HistoryEN, true));
                    } else if (history.HistoryNative != null && history.HistoryNative.Trim() != "") {
                        historyRow.AppendChild(XmlHelper.GetTextAsList(doc, history.HistoryNative, true));
                    }
                }
            } else {
                historyRow.AppendChild(XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
            }
            return directors;
        }

        //Eftir
        private XmlElement GetDirectors(XmlDocument doc, int companyCIID) {
            XmlElement directors = XmlHelper.GetXmlTable(doc, "Directors", "tblDirectors");
            XmlHelper.AddAttribute(doc, directors, "title", rm.GetString("txtDirectors", ci));

            DataSet ds = factory.GetDirectorsAsDataset(companyCIID);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    //	string dir = ds.Tables[0].Rows[i]["FirstName"+cultureEnding]+" "+ds.Tables[0].Rows[i]["SurName"+cultureEnding];
                    //	dir+=" - "+ds.Tables[0].Rows[i]["CityName"+cultureEnding]+", "+ds.Tables[0].Rows[i]["PostalCode"]+", "+ds.Tables[0].Rows[i]["Street"+cultureEnding];

                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    XmlHelper.AddAttribute(
                        doc,
                        row,
                        "Name",
                        ds.Tables[0].Rows[i]["firstname" + cultureEnding] + " " +
                        ds.Tables[0].Rows[i]["surname" + cultureEnding]);
                    XmlHelper.AddAttribute(doc, row, "ID", ds.Tables[0].Rows[i]["number"].ToString());
                    XmlHelper.AddAttribute(
                        doc,
                        row,
                        "Address",
                        ds.Tables[0].Rows[i]["Street" + cultureEnding] + " " + ds.Tables[0].Rows[i]["PostalCode"]);
                    XmlHelper.AddAttribute(
                        doc,
                        row,
                        "Country",
                        ds.Tables[0].Rows[i]["CityName" + cultureEnding] + " " +
                        ds.Tables[0].Rows[i]["CountryName" + cultureEnding]);
                    directors.AppendChild(row);
                }
            }
            return directors;
        }

        //Eftir
        private XmlElement GetShareholders(XmlDocument doc, int companyCIID) {
            XmlElement shareholders = XmlHelper.GetXmlTable(doc, "ShareHolders", "tblShareholders");
            XmlHelper.AddAttribute(doc, shareholders, "title", rm.GetString("txtOwnersShareholders", ci));

            DataSet ds = factory.GetShareHolderOwnerAsDataSet(companyCIID);
            if (ds.Tables.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    XmlHelper.AddAttribute(
                        doc,
                        row,
                        "Name",
                        ds.Tables[0].Rows[i]["firstname" + cultureEnding] + " " +
                        ds.Tables[0].Rows[i]["surname" + cultureEnding]);
                    XmlHelper.AddAttribute(doc, row, "ID", ds.Tables[0].Rows[i]["number"].ToString());
                    XmlHelper.AddAttribute(
                        doc, row, "Address", ds.Tables[0].Rows[i]["Street" + cultureEnding] + " "
                        /*+ds.Tables[0].Rows[i]["PostalCode"]*/);
                    XmlHelper.AddAttribute(
                        doc, row, "Country", /*ds.Tables[0].Rows[i]["CityName"+cultureEnding]+*/"N/A"
                        /*+ds.Tables[0].Rows[i]["CountryName"+cultureEnding]*/);
                    shareholders.AppendChild(row);
                }
            }
            return shareholders;
        }

        //Eftir
        protected XmlElement GetClaims(XmlDocument doc, int companyCIID) {
            XmlElement claims = XmlHelper.GetXmlTable(doc, "Claims", "tblClaims");
            XmlHelper.AddAttribute(doc, claims, "title", rm.GetString("txtCourtInfoAndDefaultingDebts" , ci));

            NPaymentsFactory fact = new NPaymentsFactory();
            ArrayList theClaims = fact.GetAllClaimsByCIID(companyCIID);

            if (theClaims != null) {
                NumberFormatInfo formatter = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
                formatter.PercentDecimalDigits = 2;
                formatter.NumberDecimalDigits = 2;
                if (theClaims.Count > 0) {
                    for (int i = 0; i < theClaims.Count; i++) {
                        Claim claim = (Claim) theClaims[i];
                        XmlElement row = XmlHelper.GetXmlRow(doc);
                        if (nativeCult) {
                            XmlHelper.AddAttribute(doc, row, "CaseNumberText", rm.GetString("txtCase", ci));
                            XmlHelper.AddAttribute(doc, row, "CaseNumber", claim.CaseNumber);

                            XmlHelper.AddAttribute(doc, row, "InfoSourceText", rm.GetString("txtInformationSource", ci));
                            XmlHelper.AddAttribute(doc, row, "InfoSource", claim.InformationNameNative);

                            XmlHelper.AddAttribute(doc, row, "ClaimOwnerText", rm.GetString("txtClaimOwner", ci));
                            XmlHelper.AddAttribute(doc, row, "ClaimOwner", GetName(claim.ClaimOwnerCIID));

                            XmlHelper.AddAttribute(doc, row, "DateText", rm.GetString("txtDate", ci));
                            XmlHelper.AddAttribute(doc, row, "Date", claim.RegDate.ToShortDateString());

                            XmlHelper.AddAttribute(doc, row, "InfoTypeText", rm.GetString("txtInformationType", ci));
                            XmlHelper.AddAttribute(doc, row, "InfoType", claim.ClaimTypeNative);

                            XmlHelper.AddAttribute(doc, row, "AmountText", rm.GetString("txtAmount", ci));
                            XmlHelper.AddAttribute(doc, row, "Amount", claim.Amount.ToString(formatter));

                            XmlHelper.AddAttribute(doc, row, "CIReferenceText", rm.GetString("txtCIReference", ci));
                            XmlHelper.AddAttribute(doc, row, "CIReference", claim.CreditInfoID.ToString());
                        } else {
                            XmlHelper.AddAttribute(doc, row, "InfoSourceText", rm.GetString("txtInformationSource", ci));
                            XmlHelper.AddAttribute(doc, row, "InfoSource", claim.InformationNameEN);

                            XmlHelper.AddAttribute(doc, row, "ClaimOwnerText", rm.GetString("txtClaimOwner", ci));
                            XmlHelper.AddAttribute(doc, row, "ClaimOwner", GetName(claim.ClaimOwnerCIID));

                            XmlHelper.AddAttribute(doc, row, "DateText", rm.GetString("txtDate", ci));
                            XmlHelper.AddAttribute(doc, row, "Date", claim.RegDate.ToShortDateString());

                            XmlHelper.AddAttribute(doc, row, "InfoTypeText", rm.GetString("txtInformationType", ci));
                            XmlHelper.AddAttribute(doc, row, "InfoType", claim.ClaimTypeEN);

                            XmlHelper.AddAttribute(doc, row, "AmountText", rm.GetString("txtAmount", ci));
                            XmlHelper.AddAttribute(doc, row, "Amount", claim.Amount.ToString(formatter));

                            XmlHelper.AddAttribute(doc, row, "CIReferenceText", rm.GetString("txtCIReference", ci));
                            XmlHelper.AddAttribute(doc, row, "CIReference", claim.CreditInfoID.ToString());
                        }
                        claims.AppendChild(row);
                    }
                } else {
                    claims.AppendChild(
                        XmlHelper.CreateElement(doc, "NoInfo", "value", rm.GetString("txtNoRegistrations", ci)));
                }
            } else {
                claims.AppendChild(
                    XmlHelper.CreateElement(doc, "NoInfo", "value", rm.GetString("txtNoRegistrations", ci)));
            }
            return claims;
        }

        protected string GetName(int creditInfoID) {
            uaFactory uaFact = new uaFactory();
            string name = "";
            if (nativeCult) {
                name = uaFact.GetCreditInfoUserNativeName(creditInfoID);
                if (name == null || name == "") {
                    name = uaFact.GetCreditInfoUserENName(creditInfoID);
                }
            } else {
                name = uaFact.GetCreditInfoUserENName(creditInfoID);
                if (name == null || name == "") {
                    name = uaFact.GetCreditInfoUserNativeName(creditInfoID);
                }
            }
            return name;
        }

        //B�i�
        private XmlElement GetCompantySecretarty(XmlDocument doc, int companyCIID) {
            XmlElement secretary = XmlHelper.GetXmlTable(doc, "BoardSecretary", "tblBoardSecretary");
            XmlHelper.AddAttribute(doc, secretary, "title", rm.GetString("txtBoardSecretary", ci));

            BoardSecretaryBLLC bs = factory.GetBoardSecretary(companyCIID);
            if (bs != null) {
                XmlElement row = XmlHelper.GetXmlRow(doc);
                if (nativeCult) {
                    if (bs.FirstNameNative != null && bs.FirstNameNative.Trim() != "") {
                        XmlHelper.AddAttribute(doc, row, "Name", bs.FirstNameNative + " " + bs.SurNameNative);
                    } else {
                        XmlHelper.AddAttribute(doc, row, "Name", bs.FirstNameEN + " " + bs.SurNameEN);
                    }
                    XmlHelper.AddAttribute(doc, row, "ID", bs.NationalID);
                    if (bs.Address != null && bs.Address.Count > 0) {
                        Address addr = (Address) bs.Address[0];
                        if (addr.StreetNative != null && addr.StreetNative.Trim() != "") {
                            XmlHelper.AddAttribute(doc, row, "Address", addr.StreetNative + ", " + addr.PostalCode);
                        } else {
                            XmlHelper.AddAttribute(doc, row, "Address", addr.StreetNative + ", " + addr.PostalCode);
                        }

                        if (addr.CityNameNative != null && addr.CityNameNative.Trim() != "") {
                            XmlHelper.AddAttribute(
                                doc, row, "Country", addr.CityNameNative + ", " + addr.CityLocationNative);
                        } else {
                            XmlHelper.AddAttribute(doc, row, "Country", addr.CityNameEN + ", " + addr.CityLocationEN);
                        }
                    }
                } else {
                    if (bs.FirstNameNative != null && bs.FirstNameEN.Trim() != "") {
                        XmlHelper.AddAttribute(doc, row, "Name", bs.FirstNameEN + " " + bs.SurNameEN);
                    } else {
                        XmlHelper.AddAttribute(doc, row, "Name", bs.FirstNameNative + " " + bs.SurNameNative);
                    }
                    XmlHelper.AddAttribute(doc, row, "ID", bs.NationalID);
                    if (bs.Address != null && bs.Address.Count > 0) {
                        Address addr = (Address) bs.Address[0];
                        if (addr.StreetEN != null && addr.StreetEN.Trim() != "") {
                            XmlHelper.AddAttribute(doc, row, "Address", addr.StreetEN + ", " + addr.PostalCode);
                        } else {
                            XmlHelper.AddAttribute(doc, row, "Address", addr.StreetNative + ", " + addr.PostalCode);
                        }

                        if (addr.CityNameEN != null && addr.CityNameEN.Trim() != "") {
                            XmlHelper.AddAttribute(doc, row, "Country", addr.CityNameEN + ", " + addr.CityLocationEN);
                        } else {
                            XmlHelper.AddAttribute(
                                doc, row, "Country", addr.CityNameNative + ", " + addr.CityLocationNative);
                        }
                    }
                }
                secretary.AppendChild(row);
            }
            return secretary;
        }

        //B�i�
        private XmlElement GetCompanyLaywers(XmlDocument doc, int companyCIID) {
            XmlElement laywers = XmlHelper.GetXmlTable(doc, "CompanyLaywers", "tblCompanyLaywers");
            XmlHelper.AddAttribute(doc, laywers, "title", rm.GetString("txtLaywers", ci));

            DataSet ds = factory.GetLaywersAsDataSet(companyCIID);
            if (ds != null || ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    if (ds.Tables[0].Rows[i]["Type"].ToString().Trim() ==
                        CigConfig.Configure("lookupsettings.individualID")) {
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["FirstNameNative"].ToString() == "") {
                                XmlHelper.AddAttribute(
                                    doc,
                                    row,
                                    "Name",
                                    ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                            } else {
                                XmlHelper.AddAttribute(
                                    doc,
                                    row,
                                    "Name",
                                    ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                    ds.Tables[0].Rows[i]["SurNameNative"]);
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["FirstNameEN"].ToString() == "") {
                                XmlHelper.AddAttribute(
                                    doc,
                                    row,
                                    "Name",
                                    ds.Tables[0].Rows[i]["FirstNameNative"] + " " +
                                    ds.Tables[0].Rows[i]["SurNameNative"]);
                            } else {
                                XmlHelper.AddAttribute(
                                    doc,
                                    row,
                                    "Name",
                                    ds.Tables[0].Rows[i]["FirstNameEN"] + " " + ds.Tables[0].Rows[i]["SurNameEN"]);
                            }
                        }
                    } else {
                        if (nativeCult) {
                            if (ds.Tables[0].Rows[i]["NameNative"].ToString() == "") {
                                XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameEN"].ToString());
                            } else {
                                XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameNative"].ToString());
                            }
                        } else {
                            if (ds.Tables[0].Rows[i]["NameEN"].ToString() == "") {
                                XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameNative"].ToString());
                            } else {
                                XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameEN"].ToString());
                            }
                        }
                    }

                    XmlHelper.AddAttribute(doc, row, "ID", ds.Tables[0].Rows[i]["Number"].ToString());
                    XmlHelper.AddAttribute(
                        doc, row, "Address", ds.Tables[0].Rows[i]["Street" + cultureEnding].ToString());
                    XmlHelper.AddAttribute(
                        doc, row, "Country", /*ds.Tables[0].Rows[i]["CityName"+cultureEnding]+*/"N/A"
                        /*+ds.Tables[0].Rows[i]["CountryName"+cultureEnding]*/);

                    laywers.AppendChild(row);
                }
            }
            return laywers;
        }


        protected XmlElement GetFinancialStrengthIndicator(XmlDocument doc, int companyCIID)
        {
            XmlElement finansicalStatementsIndocator = XmlHelper.GetXmlTable(doc, "FinancialStrengthIndicator", "tblRecentEnquiries");

            return finansicalStatementsIndocator;
        }

        protected XmlElement GetCreditLimit(XmlDocument doc, int companyCIID)
        {
            XmlElement creditLimit = XmlHelper.GetXmlTable(doc, "CreditLimit", "tblRecentEnquiries");

            return creditLimit;
        }

        //Eftir
        protected XmlElement GetRecentEnquiries(XmlDocument doc, int companyCIID) {
            XmlElement recentEnq = XmlHelper.GetXmlTable(doc, "RecentEnquiries", "tblRecentEnquiries");
            XmlHelper.AddAttribute(doc, recentEnq, "title", rm.GetString("txtRecentEnquiries", ci));

            cwFactory creditFactory = new cwFactory();

            XmlElement row = XmlHelper.GetXmlRow(doc);
            XmlHelper.AddAttribute(doc, row, "LastMonthText", rm.GetString("txtLastMonth", ci));
            XmlHelper.AddAttribute(
                doc,
                row,
                "LastMonthValue",
                creditFactory.GetLogCountForLastXDaysPerCreditInfoUserExcludingEmployees(30, companyCIID).ToString());

            XmlHelper.AddAttribute(doc, row, "Last3MonthsText", rm.GetString("txtLast3Months", ci));
            XmlHelper.AddAttribute(
                doc,
                row,
                "Last3MonthsValue",
                creditFactory.GetLogCountForLastXDaysPerCreditInfoUserExcludingEmployees(90, companyCIID).ToString());

            XmlHelper.AddAttribute(doc, row, "Last6MonthsText", rm.GetString("txtLast6Months", ci));
            XmlHelper.AddAttribute(
                doc,
                row,
                "Last6MonthsValue",
                creditFactory.GetLogCountForLastXDaysPerCreditInfoUserExcludingEmployees(180, companyCIID).ToString());

            recentEnq.AppendChild(row);

            return recentEnq;
        }

        protected void AddStructureReportComponents(
            XmlDocument doc, XmlElement reportRoot, int companyCIID, string afs_ids) {
            XmlElement basicInfoTable = XmlHelper.GetXmlTable(doc, "maintable", "tblBasicInfo");
            reportRoot.AppendChild(basicInfoTable);

            ReportCompanyBLLC theCompany = factory.GetCompanyReport("", companyCIID, false);

            HistoryOperationReviewBLLC theHistoryOperation = factory.GetHistoryOperationReview(companyCIID);
            if (theCompany != null) {
                //basicInfoTable.AppendChild(getEmptyRow(doc, 4));

                //Add title
                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));
                /*XmlElement row1 = XmlHelper.GetXmlRow(doc);
				XmlElement col = XmlHelper.GetXmlColumn(doc, "");
				XmlHelper.AddAttribute(doc, col, "colspan", "3");
				col.AppendChild(XmlHelper.GetXmlBoldText(doc, rm.GetString("txtCreditInfoStructureReport",ci)));					
				row1.AppendChild(XmlHelper.AddAttribute(doc, col, "colspan", "4"));
				basicInfoTable.AppendChild(row1);
				basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));*/

                XmlElement nameRow = XmlHelper.GetXmlRow(doc);
                basicInfoTable.AppendChild(nameRow);
                XmlElement nameCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, nameCol, "colspan", "3");
                nameRow.AppendChild(nameCol);
                nameCol.AppendChild(XmlHelper.GetXmlNormalText(doc, rm.GetString("txtCompanyName", ci) + " "));

                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                XmlElement addressRow = XmlHelper.GetXmlRow(doc);
                basicInfoTable.AppendChild(addressRow);
                XmlElement addressCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, addressCol, "colspan", "3");
                addressRow.AppendChild(addressCol);
                addressCol.AppendChild(XmlHelper.GetXmlNormalText(doc, rm.GetString("txtAddress", ci) + " "));
                //Add name and address
                if (nativeCult) {
                    nameCol.AppendChild(XmlHelper.GetXmlBoldText(doc, theCompany.NameNative));
                    if (theCompany.Address.Count > 0) {
                        Address addr = (Address) theCompany.Address[0];
                        string addStr = "";
                        if (addr.StreetNative != null && addr.StreetNative.Trim() != "") {
                            addStr = addr.StreetNative;
                        } else {
                            addStr = addr.StreetEN;
                        }
                        addStr += ", " + addr.PostalCode + ", ";
                        if (addr.CityNameNative != null && addr.CityNameNative.Trim() != "") {
                            addStr += addr.CityNameNative;
                        } else {
                            addStr += addr.CityNameEN;
                        }
                        addressCol.AppendChild(XmlHelper.GetXmlBoldText(doc, addStr));
                    }
                } else {
                    nameCol.AppendChild(XmlHelper.GetXmlBoldText(doc, theCompany.NameEN));
                    if (theCompany.Address.Count > 0) {
                        Address addr = (Address) theCompany.Address[0];
                        string addStr = "";
                        if (addr.StreetEN != null && addr.StreetEN.Trim() != "") {
                            addStr = addr.StreetEN;
                        } else {
                            addStr = addr.StreetNative;
                        }
                        addStr += ", " + addr.PostalCode + ", ";
                        if (addr.CityNameEN != null && addr.CityNameEN.Trim() != "") {
                            addStr += addr.CityNameEN;
                        } else {
                            addStr += addr.CityNameNative;
                        }
                        addressCol.AppendChild(XmlHelper.GetXmlBoldText(doc, addStr));
                    }
                }

                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                //Create a row that contains legal form table and date table
                XmlElement lfRow = XmlHelper.GetXmlRow(doc);

                basicInfoTable.AppendChild(lfRow);

                //Add legal form table
                XmlElement lfCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, lfCol, "width", "36%");
                XmlHelper.AddAttribute(doc, lfCol, "valign", "top");
                lfRow.AppendChild(lfCol);
                XmlElement lfTable = XmlHelper.GetXmlTable(doc, "BorderTable", "tblLegalForm");
                XmlHelper.AddAttribute(doc, lfTable, "width", "100%");
                lfTable.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtCompanyLegalForm", ci)));
                lfTable.AppendChild(
                    XmlHelper.GetXmlRowWithValue(
                        doc,
                        new[] {factory.GetRegistrationFormAsString(theCompany.RegistrationFormID, nativeCult)},
                        true));
                lfCol.AppendChild(lfTable);

                lfRow.AppendChild(XmlHelper.GetXmlNBSPColumn(doc));

                //Add date table
                XmlElement dateCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, dateCol, "width", "64%");
                lfRow.AppendChild(dateCol);
                XmlElement dateTable = XmlHelper.GetXmlTable(doc, "BorderTable", "tblDates");
                XmlHelper.AddAttribute(doc, dateTable, "width", "100%");
                XmlElement upperRow = XmlHelper.GetXmlRow(doc);
                dateTable.AppendChild(upperRow);
                upperRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtDateStarted", ci)));
                upperRow.AppendChild(XmlHelper.GetXmlColumn(doc, theCompany.Established.ToShortDateString()));

                upperRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtRegistrationID", ci)));
                upperRow.AppendChild(XmlHelper.GetXmlColumn(doc, theCompany.UniqueID));

                upperRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtLAR", ci)));
                if (theCompany.LAR != DateTime.MinValue) {
                    upperRow.AppendChild(XmlHelper.GetXmlColumn(doc, theCompany.LAR.ToShortDateString()));
                } else {
                    upperRow.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                }

                XmlElement lowerRow = XmlHelper.GetXmlRow(doc);
                dateTable.AppendChild(lowerRow);
                lowerRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtDateRegistered", ci)));
                if (theCompany.Registered != DateTime.MinValue) {
                    lowerRow.AppendChild(XmlHelper.GetXmlColumn(doc, theCompany.Registered.ToShortDateString()));
                } else {
                    lowerRow.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                }

                lowerRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtCreditInfoID", ci)));
                lowerRow.AppendChild(XmlHelper.GetXmlColumn(doc, theCompany.CompanyCIID.ToString()));

                lowerRow.AppendChild(XmlHelper.GetXmlBoldColumn(doc, rm.GetString("txtLR", ci)));
                if (theCompany.LR != DateTime.MinValue) {
                    lowerRow.AppendChild(XmlHelper.GetXmlColumn(doc, theCompany.LR.ToShortDateString()));
                } else {
                    lowerRow.AppendChild(XmlHelper.GetXmlColumn(doc, "N/A"));
                }

                dateCol.AppendChild(dateTable);

                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                //Add history
                //Add company history
                XmlElement historyRow = XmlHelper.GetXmlRow(doc);
                XmlElement historyCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, historyCol, "colspan", "3");
                historyRow.AppendChild(historyCol);
                XmlElement historyTable = XmlHelper.GetXmlTable(doc, "BorderTable", "tblHistory");
                historyCol.AppendChild(historyTable);
                historyTable.AppendChild(XmlHelper.GetXmlHeaderRowWithValue(doc, rm.GetString("txtCompanyHistory", ci)));
                XmlHelper.AddAttribute(doc, historyTable, "width", "100%");

                if (nativeCult) {
                    if (theHistoryOperation.HistoryNative != "" && theHistoryOperation.HistoryNative != null) {
                        historyTable.AppendChild(XmlHelper.GetTextAsList(doc, theHistoryOperation.HistoryNative, true));
                    } else {
                        historyTable.AppendChild(
                            XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                    }
                } else {
                    if (theHistoryOperation.HistoryNative != "" && theHistoryOperation.HistoryNative != null) {
                        historyTable.AppendChild(XmlHelper.GetTextAsList(doc, theHistoryOperation.HistoryEN, true));
                    } else {
                        historyTable.AppendChild(
                            XmlHelper.GetXmlColumn(doc, rm.GetString("txtNoInformationAvailable", ci)));
                    }
                }
                basicInfoTable.AppendChild(historyRow);
                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                //Add Shareholders
                XmlElement ownersRow = XmlHelper.GetXmlRow(doc);
                XmlElement ownersCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, ownersCol, "colspan", "3");
                ownersRow.AppendChild(ownersCol);
                ownersCol.AppendChild(getStructureReportShareholdersOwners(doc, companyCIID));
                basicInfoTable.AppendChild(ownersRow);
                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                //Add directors and directors history
                XmlElement keyEmpRow = XmlHelper.GetXmlRow(doc);
                XmlElement keyEmpCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, keyEmpCol, "colspan", "3");
                keyEmpRow.AppendChild(keyEmpCol);
                keyEmpCol.AppendChild(getBoardMembersAndBoardHistory(doc, companyCIID));
                basicInfoTable.AppendChild(keyEmpRow);
                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                //Add secretary
                XmlElement secretaryRow = XmlHelper.GetXmlRow(doc);
                XmlElement secretaryCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, secretaryCol, "colspan", "3");
                secretaryRow.AppendChild(secretaryCol);
                secretaryCol.AppendChild(GetStructureReportBoardSecretary(doc, companyCIID));
                basicInfoTable.AppendChild(secretaryRow);
                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                //Add capital
                XmlElement capitalRow = XmlHelper.GetXmlRow(doc);
                XmlElement capitalCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, capitalCol, "colspan", "3");
                capitalRow.AppendChild(capitalCol);
                capitalCol.AppendChild(getStructureReportCapital(doc, companyCIID, true, true));
                basicInfoTable.AppendChild(capitalRow);
                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                //Add analyze of authorized capital
                XmlElement analyzeOfCapitalRow = XmlHelper.GetXmlRow(doc);
                XmlElement analyzeOfCapitalCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, analyzeOfCapitalCol, "colspan", "3");
                analyzeOfCapitalRow.AppendChild(analyzeOfCapitalCol);
                analyzeOfCapitalCol.AppendChild(getStructureReportAnalyzeOfCapital(doc, companyCIID));
                basicInfoTable.AppendChild(analyzeOfCapitalRow);
                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                //Add charges
                XmlElement chargesRow = XmlHelper.GetXmlRow(doc);
                XmlElement chargesCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, chargesCol, "colspan", "3");
                chargesRow.AppendChild(chargesCol);
                chargesCol.AppendChild(getCharges(doc, companyCIID, true));
                basicInfoTable.AppendChild(chargesRow);

                basicInfoTable.AppendChild(XmlHelper.GetXmlNBSPRow(doc));

                XmlElement endRow = XmlHelper.GetXmlRow(doc);
                XmlElement endCol = XmlHelper.GetXmlColumn(doc, "");
                XmlHelper.AddAttribute(doc, endCol, "colspan", "3");

                endRow.AppendChild(endCol);

                AddEndOfReport(doc, endCol);
                basicInfoTable.AppendChild(endRow);
            }
        }

        private DataSet cleanDuplicatedBMembersCIIDS(DataSet ds) {
            if (ds.Tables.Count > 0) {
                ArrayList arrCiids = new ArrayList();
                ArrayList managementTitle = new ArrayList();
                for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    // athuga title � sta�inn ... �.e. fyrir boardmembers
                    string titleEN = ds.Tables[0].Rows[i]["TitleEN"].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (((string) arrCiids[j]).Equals(creditInfoId) && ((string) managementTitle[j]).Equals(titleEN)) {
                            ds.Tables[0].Rows.Remove(ds.Tables[0].Rows[i]);
                            break;
                        }
                    }
                    arrCiids.Add(creditInfoId);
                    managementTitle.Add(titleEN);
                }
            }
            return ds;
        }
    }
}