#region

using System;
using System.Configuration;
using System.IO;
using System.Web;
using WebSupergoo.ABCpdf4;
//using System.Resources;

#endregion

using Cig.Framework.Base.Configuration;

namespace CR.BLL {
    /// <summary>
    /// Summary description for ReportFormatConverter.
    /// </summary>
    public class ReportFormatConverter {
        public void CreatePDFFromHTML(string HTML, HttpResponse theResponse) {
            string filePath = HttpContext.Current.Server.MapPath(".");
            filePath += CigConfig.Configure("lookupsettings.temporaryPdfFilePath");

            //First save HTML to file
//			string filePath = CigConfig.Configure("lookupsettings.temporaryPdfFilePath"];

            string fileName = getFileName("FinancialStatement") + ".html";
            filePath += fileName;

            // Write the string to a file.
            var file = new StreamWriter(filePath);
            file.WriteLine(HTML);
            file.Flush();
            file.Close();

            //Create the PDF
            //		string file = this.CreateHTML(CompanyCIID);

            XSettings.License = CigConfig.Configure("lookupsettings.abcPDFLicenceKey");

            var theDoc = new Doc();

            //We first create a Doc object and inset the edges a little so that the 
            //HTML will appear in the middle of the page
            //		theDoc.Rect.Inset(72, 144);

            //We add the first page and indicate that we will be adding more pages 
            //by telling the Doc object that this is page one. We save the returned 
            //ID as this will be used to add subsequent pages.
            var theID = theDoc.AddImageUrl("file:///" + filePath, true, 608, false);

            //We now chain subsequent pages together. We stop when we reach a page 
            //which wasn't truncated.
            while (true) {
                theDoc.FrameRect();
                if (theDoc.GetInfo(theID, "Truncated") != "1") {
                    break;
                }
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }

            //After adding the pages we can flatten them. We can't do this until 
            //after the pages have been added because flattening will invalidate our 
            //previous ID and break the chain.
            for (int i = 1; i <= theDoc.PageCount; i++) {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }

            //		int theDot = filePath.LastIndexOf(".");
            //		string pdfFile = filePath.Substring(0, theDot) + ".pdf";

            theDoc.Encryption.Type = 2;
            theDoc.Encryption.OwnerPassword = CigConfig.Configure("lookupsettings.PDFPassword");
            theDoc.Encryption.CanChange = false;
            theDoc.Encryption.CanCopy = false;
            theDoc.Encryption.CanEdit = false;
            theDoc.Encryption.CanExtract = false;
            theDoc.Encryption.CanFillForms = false;
            theDoc.Encryption.CanPrint = true;
            theDoc.Encryption.CanPrintHi = true;

            theResponse.ContentType = "application/pdf";

            theDoc.Save(theResponse.OutputStream);
            theDoc.Clear();

            theResponse.Flush();

            //Delete the html file
            File.Delete(filePath);

            theResponse.End();

            //Finally we save. 
/*			theDoc.Save(pdfFile);
			theDoc.Clear(); 

			File.Delete(filePath);

			serverFilePath += fileName.Substring(0, fileName.LastIndexOf(".")) + ".pdf";

			return serverFilePath;*/
        }

        public void CreateExcelFromHTML(string HTML, HttpResponse Response) {
            Response.ClearContent();
            Response.ContentType = "applicat�ion/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment; filename=financialstatement.xls");
            Response.Write(HTML);
            Response.Flush();
            Response.Close();
        }

        /// <summary>
        /// Creates a name for temporary report file
        /// </summary>
        /// <returns>A name for report file based on the date/time the report was generated</returns>
        private static string getFileName(string name) {
            var dt = DateTime.Now;
            return name + dt.Day + "_" + dt.Month + "_" + dt.Hour + "_" + dt.Minute + "_" + dt.Second;
        }
    }
}