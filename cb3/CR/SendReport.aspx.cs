#region

using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Runtime.InteropServices;
using System.Threading;
using System.Web.Mail;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using CR.BLL;
using CR.Localization;
using HTML2RTF;
using Logging.BLL;
using WebSupergoo.ABCpdf4;

#endregion

using Cig.Framework.Base.Configuration;

namespace CR {
    /// <summary>
    /// Summary description for SendReport.
    /// </summary>
    public class SendReportForm : Page {
        protected HtmlTableCell blas;
        protected Button btnSend;
        private CultureInfo ci;
        private const string className = "SendReport";
        protected DropDownList ddlstFileFormat;
        private bool EN;
        protected Label lblBody;
        protected Label lblCompanyCIID;
        protected Label lblCompanyCIIDVal;
        protected Label lblError;
        protected Label lblSendAsFormat;
        protected Label lblSendReport;
        protected Label lblSendTo;
        protected Label lblSubject;
        protected RegularExpressionValidator revEmailValidator;
        protected RequiredFieldValidator rfvEmail;
        private ResourceManager rm;
        protected TextBox txtBody;
        protected TextBox txtEmail;
        protected TextBox txtSubject;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "850";
            AddEnterEvent();
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US")) {
                EN = true;
            }

            if (!IsPostBack) {
                InitDropDownBox();

                LocalizeText();
                if (Session["CompanyCIID"] != null) {
                    lblCompanyCIIDVal.Text = Session["CompanyCIID"].ToString();
                }
            }
        }

        private void InitDropDownBox() {
            var theFactory = new CRFactory();

            ddlstFileFormat.DataSource = theFactory.GetFileFormats();
            ddlstFileFormat.DataTextField = EN ? "NameEN" : "NameNative";
            ddlstFileFormat.DataValueField = "ID";
            ddlstFileFormat.DataBind();

            //Set the default file format
            try {
                ddlstFileFormat.SelectedValue = "3"; //CigConfig.Configure("lookupsettings.CRDefaultFileFormat"];
            } catch (Exception) {}
        }

        private void AddEnterEvent() {
            var frm = FindControl("SendReportForm");
            foreach (Control ctrl in frm.Controls) {
                if (!(ctrl is TextBox)) {
                    continue;
                }
                if (ctrl.ID != "txtBody") // Exclude the body textbox because that one is multiline and the Enter event is needed for linefeed
                {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        /// <summary>
        /// Sends report in email.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {
                return;
            }
            const string funcName = "btnSend_Click";

            string file = getReport(int.Parse(lblCompanyCIIDVal.Text), ddlstFileFormat.SelectedValue);

            if (file == null) {
                return;
            }
            try {
                string smtp = CigConfig.Configure("lookupsettings.SMTPServer");
                string bcc = CigConfig.Configure("lookupsettings.BCC");
                string from = CigConfig.Configure("lookupsettings.From");

                var myMail = new MailMessage
                             {
                                 From = from,
                                 To = txtEmail.Text,
                                 Bcc = bcc,
                                 Subject = txtSubject.Text,
                                 BodyFormat = MailFormat.Text,
                                 Body = txtBody.Text
                             };

                MailAttachment attachment = new MailAttachment(file);
                myMail.Attachments.Add(attachment);
                SmtpMail.SmtpServer = smtp;
                SmtpMail.Send(myMail);
                File.Delete(file);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                lblError.Visible = true;
            }
            Response.Redirect("../CPI/ReportSelection.aspx");
        }

        /// <summary>
        /// Localizes text for components on the page
        /// </summary>
        private void LocalizeText() {
            lblBody.Text = rm.GetString("txtBody", ci);
            lblCompanyCIID.Text = rm.GetString("txtCompanyCIID", ci);
            lblSendReport.Text = rm.GetString("txtSendReport", ci);
            lblSendTo.Text = rm.GetString("txtSendTo", ci);
            lblSubject.Text = rm.GetString("txtSubject", ci);
            lblError.Text = rm.GetString("txtFailedToSendReport", ci);
            revEmailValidator.ErrorMessage = rm.GetString("txtNotValidEmailAddress", ci);
            rfvEmail.ErrorMessage = rm.GetString("txtEmailAddressMissing", ci);
            btnSend.Text = rm.GetString("txtSend", ci);
            lblSendAsFormat.Text = rm.GetString("txtSendAsFormat", ci);
        }

        /// <summary>
        /// Creates a name for temporary report file
        /// </summary>
        /// <param name="companyID">The company id </param>
        /// <returns>A name for report file based on report id and date/time the report was generated</returns>
        private static string getFileName(string companyID) {
            DateTime dt = DateTime.Now;
            return "CompanyReport_" + companyID + "_" + dt.Day + "_" + dt.Month + "_" + dt.Hour + "_" + dt.Minute + "_" +
                   dt.Second; //+".html";
        }

        private string CreateExcel(int CompanyCIID) {
            string reportType = "3";
            string cultureToUse = "Native";
            string afs_ids = "";

            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }
            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }
            if (Session["AFSID"] != null) {
                afs_ids = Session["AFSID"].ToString();
            }
            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }

            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            var generator = new TemplateReportGenerator(
                rm, cultureToUse, userCreditInfoID, userID, ipAddress);
            string xslFile = null;
            string xslSendFile = null;
            XmlDocument doc = generator.GetReport(reportType, CompanyCIID, afs_ids, -1, ref xslFile, ref xslSendFile);
            var transformer = new XslTransform();
            //	doc.Save("c:\\cig\\TemplateReport.xml");
            transformer.Load(Server.MapPath(Request.ApplicationPath + xslFile));

            string filePath = CigConfig.Configure("lookupsettings.DirForTempFiles");
            string file = filePath + getFileName(CompanyCIID.ToString()) + ".xls";

            FileStream writer = File.Create(file);

            transformer.Transform(doc, null, writer, null);
                //generator.getReport(CompanyCIID, afs_ids), null, writer, null);
            writer.Flush();
            writer.Close();

            return file;
        }

        private string CreateHTML(int CompanyCIID) {
            string reportType = "43";
            string cultureToUse = "Native";
            string afs_ids = "";

            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }
            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }
            if (Session["AFSID"] != null) {
                afs_ids = Session["AFSID"].ToString();
            }
            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }

            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            var generator = new TemplateReportGenerator(
                rm, cultureToUse, userCreditInfoID, userID, ipAddress);
            string xslFile = "";
            string xslSendFile = "";
            XmlDocument doc = generator.GetReport(reportType, CompanyCIID, afs_ids, -1, ref xslFile, ref xslSendFile);
            XslTransform transformer = new XslTransform();
            //	doc.Save("c:\\cig\\TemplateReport.xml");
            //transformer.Load(Server.MapPath(Request.ApplicationPath+xslSendFile)); 

            transformer.Load(Server.MapPath(CigConfig.Configure("lookupsettings.backRootName") + xslSendFile));

            string filePath = CigConfig.Configure("lookupsettings.DirForTempFiles");
            string file = filePath + getFileName(CompanyCIID.ToString()) + ".html";

            FileStream writer = File.Create(file);

            transformer.Transform(doc, null, writer, null);
                //generator.getReport(CompanyCIID, afs_ids), null, writer, null);
            writer.Flush();
            writer.Close();

            return file;
        }

        private string CreatePDF(int CompanyCIID) {
/*			string reportType="3";
			string cultureToUse = "Native";
			string afs_ids = "";
				
			if(Session["ReportType"] != null)
				reportType = Session["ReportType"].ToString();
			if(Session["ReportCulture"] != null)
				cultureToUse = Session["ReportCulture"].ToString();
			if(Session["AFSID"] != null)
				afs_ids = Session["AFSID"].ToString();
			if(Session["ReportType"] != null)
				reportType = Session["ReportType"].ToString();

			int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
			int userID = int.Parse(Session["UserLoginID"].ToString());
			string ipAddress = Request.ServerVariables["REMOTE_ADDR"];



			TemplateReportGenerator generator = new TemplateReportGenerator(rm, cultureToUse, userCreditInfoID, userID, ipAddress);
			string xslFile = null;
			XmlDocument doc = generator.GetReport(reportType, CompanyCIID, afs_ids, ref xslFile);
			XslTransform transformer = new XslTransform();
			//	doc.Save("c:\\cig\\TemplateReport.xml");
			transformer.Load(Server.MapPath(Request.ApplicationPath+xslFile)); 



	//		string filePath = CigConfig.Configure("lookupsettings.DirForTempFiles"];
	//		string file = filePath+getFileName(CompanyCIID.ToString())+".pdf";

*/
            string file = CreateHTML(CompanyCIID);

            XSettings.License = CigConfig.Configure("lookupsettings.abcPDFLicenceKey");

            var theDoc = new Doc();

            //We first create a Doc object and inset the edges a little so that the 
            //HTML will appear in the middle of the page
            //		theDoc.Rect.Inset(72, 144);

            //We add the first page and indicate that we will be adding more pages 
            //by telling the Doc object that this is page one. We save the returned 
            //ID as this will be used to add subsequent pages.
            var theID = theDoc.AddImageUrl("file:///" + file, true, 608, false);

            //We now chain subsequent pages together. We stop when we reach a page 
            //which wasn't truncated.
            while (true) {
                theDoc.FrameRect();
                if (theDoc.GetInfo(theID, "Truncated") != "1") {
                    break;
                }
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }

            //After adding the pages we can flatten them. We can't do this until 
            //after the pages have been added because flattening will invalidate our 
            //previous ID and break the chain.
            for (int i = 1; i <= theDoc.PageCount; i++) {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }

            int theDot = file.LastIndexOf(".");
            string pdfFile = file.Substring(0, theDot) + ".pdf";

            //Finally we save. 
            theDoc.Save(pdfFile);
            theDoc.Clear();

            File.Delete(file);

            return pdfFile;
        }

        private string CreateXML(int CompanyCIID) {
            string reportType = "3";
            string cultureToUse = "Native";
            string afs_ids = "";

            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }
            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }
            if (Session["AFSID"] != null) {
                afs_ids = Session["AFSID"].ToString();
            }
            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }

            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            var generator = new TemplateReportGenerator(
                rm, cultureToUse, userCreditInfoID, userID, ipAddress);
            string xslFile = null;
            string xslSendFile = null;
            XmlDocument doc = generator.GetReport(reportType, CompanyCIID, afs_ids, -1, ref xslFile, ref xslSendFile);

            string filePath = CigConfig.Configure("lookupsettings.DirForTempFiles");
            string file = filePath + getFileName(CompanyCIID.ToString()) + ".xml";

            doc.Save(file);

            return file;
        }

        private string CreateRTF(int CompanyCIID) {
            string reportType = "3";
            string cultureToUse = "Native";
            string afs_ids = "";

            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }
            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }
            if (Session["AFSID"] != null) {
                afs_ids = Session["AFSID"].ToString();
            }
            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }

            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            var generator = new TemplateReportGenerator(
                rm, cultureToUse, userCreditInfoID, userID, ipAddress);
            string xslFile = null;
            string xslSendFile = null;
            XmlDocument doc = generator.GetReport(reportType, CompanyCIID, afs_ids, -1, ref xslFile, ref xslSendFile);
            var transformer = new XslTransform();
            //	doc.Save("c:\\cig\\TemplateReport.xml");
            transformer.Load(Server.MapPath(Request.ApplicationPath + xslFile));

            string filePath = CigConfig.Configure("lookupsettings.DirForTempFiles");
            string file = filePath + getFileName(CompanyCIID.ToString());

            FileStream writer = File.Create(file + ".html");

            transformer.Transform(doc, null, writer, null);
                //generator.getReport(CompanyCIID, afs_ids), null, writer, null);
            writer.Flush();
            writer.Close();

            var html2rtf = new Converter
                           {
                               PreserveAlignment = true,
                               PreserveBackgroundColor = true,
                               PreserveFontColor = true,
                               PreserveFontFace = true,
                               PreserveFontSize = true,
                               PreserveHyperlinks = true,
                               PreserveImages = true,
                               PreserveNestedTables = true,
                               PreservePageBreaks = true,
                               PreserveTables = true,
                               PreserveTableWidth = true
                           };

            html2rtf.ConvertFile(filePath + "svennaprufamedmynd.html", filePath + "svennaprufamedmynd.rtf");
            Marshal.ReleaseComObject(html2rtf); // Releases the object from memory

            return file + ".rtf";
        }

        /// <summary>
        /// Creates a report for given company and saves it to file
        /// </summary>
        /// <param name="companyCIID">The company id to generate report for</param>
        /// <param name="fileFormat"></param>
        /// <returns>The path and name for the report file </returns>
        private string getReport(int companyCIID, IEquatable<string> fileFormat) {
            const string funcName = "getReport(int companyCIID, String fileFormat)";
            try {
                const string file = "";

                if (fileFormat.Equals("3")) //HTML
                {
                    return CreateHTML(companyCIID);
                }
                if (fileFormat.Equals("1")) //XML
                {
                    return CreateXML(companyCIID);
                }
                if (fileFormat.Equals("2")) //PDF
                {
                    return CreatePDF(companyCIID);
                }
                if (fileFormat.Equals("4")) //EXCEL
                {
                    return CreateExcel(companyCIID);
                }
                return fileFormat.Equals("5") ? CreateRTF(companyCIID) : file;
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}