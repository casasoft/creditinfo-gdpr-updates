#region

using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Xsl;
using CR.BLL;
using CR.Localization;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CR {
    /// <summary>
    /// Summary description for FoStructureReport.
    /// </summary>
    public class FoStructureReport : Page {
        protected Xml xmlxslTransform;

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                displayReport();
            }
        }

        private void displayReport() {
            // check the current culture
            var rm = CIResource.CurrentManager;

            /*string companyId = "119713";
			string reportType="42";
			string cultureToUse = "Native";
			string afs_ids = "";
			string nationalID = "C129013";*/

            var companyId = "";
            var reportType = "";
            var cultureToUse = "";
            var afs_ids = "";

            if (Session["CompanyCIID"] != null) {
                companyId = Session["CompanyCIID"].ToString();
            }
            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }
            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }
            if (Session["AFSID"] != null) {
                afs_ids = Session["AFSID"].ToString();
            }

            var userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            var userID = int.Parse(Session["UserLoginID"].ToString());
            var ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            var generator = new ReportGenerator(rm, cultureToUse, userCreditInfoID, userID, ipAddress)
                            {ReportType = int.Parse(reportType)};
            var transformer = new XslTransform();
            //	transformer.Load(Server.MapPath(Application.Get("AppPath")+"/CR/xsl/structure/StructureReportDisplay.xslt")); 
            //	xmlxslTransform.Document = generator.getReport(int.Parse(companyId), afs_ids);
            //xmlxslTransform.Document.Save("c:\\cig\\rep.xml");
            //	xmlxslTransform.Transform = transformer;
            try {
                if (CigConfig.Configure("lookupsettings.foRootName") != null) {
                    transformer.Load(
                        Server.MapPath(
                            CigConfig.Configure("lookupsettings.foRootName") +
                            "/CR/xsl/structure/StructureReportDisplay.xslt"));
                } else // at least it will work this way at production server
                {
                    transformer.Load(Application.Get("AppPath") + "/CR/xsl/structure/StructureReportDisplay.xslt");
                }
                //	transformer.Load(Server.MapPath(Request.ApplicationPath+xslFile)); 
                xmlxslTransform.Document = generator.getReport(int.Parse(companyId), afs_ids);
                //	xmlxslTransform.Document = doc;
                xmlxslTransform.Transform = transformer;
            } catch (Exception err) {
                Logger.WriteToLog("displayReport(): The error is : " + err, true);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}