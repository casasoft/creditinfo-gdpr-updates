<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:import href='general/Title.xslt'/>
	<xsl:import href='general/BoardMembers.xslt'/>
	<xsl:import href='general/BoardMembersWithInvolvements.xslt'/>	
	<xsl:import href='general/LegalForm.xslt'/>
	<xsl:import href='general/CustomerTypes.xslt'/>	
	<xsl:import href='general/ImportFrom.xslt'/>	
	<xsl:import href='general/ExportTo.xslt'/>	
	<xsl:import href='general/CompanyHistory.xslt'/>	
	<xsl:import href='general/CompanyOperation.xslt'/>	
	<xsl:import href='general/CompanyReview.xslt'/>			
	<xsl:import href='general/BuyingTerms.xslt'/>	
	<xsl:import href='general/PaymentTerms.xslt'/>	
	<xsl:import href='general/Banks.xslt'/>	
	<xsl:import href='general/Employees.xslt'/>	
	<xsl:import href='general/Lawyers.xslt'/>	
	<xsl:import href='general/Auditors.xslt'/>	
	<xsl:import href='general/BoardSecretary.xslt'/>
	<xsl:import href='general/NACE.xslt'/>	
	<xsl:import href='general/CompanyState.xslt'/>	
	<xsl:import href='general/Shareholders.xslt'/>	
	<xsl:import href='general/KeyEmployees.xslt'/>	
	<xsl:import href='general/RealEstates.xslt'/>
	<xsl:import href='general/Subsidiaries.xslt'/>	
	<xsl:import href='general/Capital.xslt'/>	
	<xsl:import href='general/Charges.xslt'/>	
	<xsl:import href='general/CourtInfoAndDefaultingDebts.xslt'/>	
	<xsl:import href='general/CreditScoring.xslt'/>		
	<xsl:import href='general/BalanceSheet.xslt'/>		
	<xsl:import href='general/ProfitLossAccount.xslt'/>		
	<xsl:import href='general/DefinitionOfRatios.xslt'/>	
	<xsl:import href='general/MaximumCredit.xslt'/>		
	<xsl:import href='general/Footer.xslt'/>	
	
	<xsl:import href='cz/CZBusinessRegistry.xslt'/>
	<xsl:import href='cz/CZCompanyStatus.xslt'/>
	
	<xsl:import href='cy/CYFormerNames.xslt'/>	

	<xsl:import href='billing/Header.xslt'/>
	<xsl:import href='billing/AgreementHeader.xslt'/>
	<xsl:import href='billing/AgreementItems.xslt'/>
	<xsl:import href='billing/AgreementComments.xsl'/>
	<xsl:import href='billing/AgreementDescription.xsl'/>
	<xsl:import href='billing/AgreementFooter.xslt'/>
	<xsl:import href='billing/AgreementNumber.xslt'/>
	
	<xsl:output method='html' indent='yes' encoding="UTF-8" media-type='text/html' version='4.0' />

	<xsl:template match='/'>
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='Root' match='root'>
		<table id="tblMain" cellSpacing="0" cellPadding="0" width="608" class="report">
			<xsl:apply-templates />	
		</table>			
	</xsl:template>
		
	<xsl:template match='PopupLink'>
		<xsl:text disable-output-escaping="yes">&lt;A HREF='</xsl:text>
		<xsl:value-of select="@Link"/>				
		<xsl:text disable-output-escaping="yes">' target='_blank'&gt;</xsl:text>
		<xsl:value-of select="@Title"/>
		<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
	</xsl:template>
	
</xsl:stylesheet>

  <!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\temp\CR&#x2D;XML&#x2D;Test\test1.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->