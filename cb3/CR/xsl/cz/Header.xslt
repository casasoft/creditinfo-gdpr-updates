<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Header' match='Header'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
			</tr>
			<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td class="header2" colspan="4">
					<b><xsl:value-of select='@title'/></b></td>
			</tr>
			<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td class="header2"><xsl:value-of select='Name/@title'/></td>
				<td class="header2"><xsl:value-of select='Name/@value'/></td>
				<td class="header2"><xsl:value-of select='RegistrationID/@title'/></td>
				<td class="header2"><xsl:value-of select='RegistrationID/@value'/></td>
			</tr>
			<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td class="header2"><xsl:value-of select='Address/@title'/></td>
				<td class="header2"><xsl:value-of select='Address/@value'/></td>
				<td class="header2"><xsl:value-of select='Phone/@title'/></td>
				<td class="header2"><xsl:value-of select='Phone/@value'/></td>
			</tr>
			<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td class="header2"><xsl:value-of select='PostalCode/@title'/></td>
				<td class="header2"><xsl:value-of select='PostalCode/@value'/></td>
				<td class="header2"><xsl:value-of select='Fax/@title'/></td>
				<td class="header2"><xsl:value-of select='Fax/@value'/></td>
			</tr>
			<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td class="header2"><xsl:value-of select='City/@title'/></td>
				<td class="header2"><xsl:value-of select='City/@value'/></td>
				<td class="header2"><xsl:value-of select='VAT/@title'/></td>
				<td class="header2"><xsl:value-of select='VAT/@value'/></td>
			</tr>
			<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td class="header2"><xsl:value-of select='Founded/@title'/></td>
				<td class="header2"><xsl:value-of select='Founded/@value'/></td>
			</tr>
			<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td class="header2"><xsl:value-of select='EnglishName/@title'/></td>
				<td class="header2"><xsl:value-of select='EnglishName/@value'/></td>
				<td class="header2"><xsl:value-of select='HomePage/@title'/></td>
				<td class="header2"><xsl:value-of select='HomePage/@value'/></td>
			</tr>
			<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td class="header2"><xsl:value-of select='LastUpdate/@title'/></td>
				<td class="header2"><xsl:value-of select='LastUpdate/@value'/></td>
				<td class="header2"><xsl:value-of select='Email/@title'/></td>
				<td class="header2"><xsl:value-of select='Email/@value'/></td>
			</tr>
			<tr bgcolor="#666666">
				<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
			</tr>
			<tr>
				<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
			</tr>
					
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>

  