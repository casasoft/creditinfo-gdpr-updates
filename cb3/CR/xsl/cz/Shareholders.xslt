<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Shareholders' match='Shareholders'>
		<tr>
			<td>			
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2"><b><xsl:value-of select='@title'/></b></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@Number'/></b></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@Ownership'/></b></td>
					</tr>
					
					<xsl:if test='NoInfo'>	
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><xsl:value-of select='NoInfo/@value'/></td>
						</tr>
					</xsl:if>					
					
					<xsl:if test='TableItem'>	
				
						<xsl:for-each select="TableItem">
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td><xsl:value-of select='@Name'/> </td>
								<td><xsl:value-of select='@Number'/></td>
								<td align="center"><xsl:value-of select='@Ownership'/></td>
							</tr>
						</xsl:for-each>	
						</xsl:if>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>