<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--xsl:output method="html" indent="yes" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/REC-html40/loose.dtd"/-->
	<xsl:output method="html" indent="yes" encoding="ISO-8859-1"/>
	<xsl:template match="/">
		<!--xsl:element name="style">
			<xsl:attribute name="TYPE">
				<xsl:text>text/css</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="MEDIA">
				<xsl:text>screen</xsl:text>
			</xsl:attribute>
			<xsl:text>
			<![CDATA[<!
				body,td,div,input,select,textarea
				{
					font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
					font-size: 10px;
				}
			 >]]>
			</xsl:text>
		</xsl:element-->
		<table border="0" width="100%" id="table1" cellpadding="3" cellspacing="0">
			<xsl:element name="tr">
				<xsl:element name="td">
					<xsl:attribute name="height">30</xsl:attribute>
					<xsl:attribute name="bgcolor">#E7D7B6</xsl:attribute>
					<xsl:element name="b">
						<xsl:value-of select="/message/casesmainheader"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<tr>
				<td height="5"></td>
			</tr>
			<tr>
				<td>
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<xsl:apply-templates select="message/case"/>
						<xsl:if test="count(message/case) = 0">
							<tr>
								<td>
									<xsl:value-of select="/message/nocasesfoundtext"/>
								</td>
							</tr>
						</xsl:if>
					</table>
				</td>
			</tr>
			<tr>
				<td height="5"></td>
			</tr>
			<xsl:apply-templates select="message/BoardMembers"/>
			<xsl:apply-templates select="message/Principals"/>
			<xsl:apply-templates select="message/Shareholders"/>
			<xsl:apply-templates select="message/CompanyInvolvements"/>

			<xsl:if test="message/PaidUpCase">
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:attribute name="height">30</xsl:attribute>
						<xsl:attribute name="bgcolor">#E7D7B6</xsl:attribute>
						<xsl:attribute name="width">100%</xsl:attribute>
						<xsl:element name="b">
							<xsl:value-of select="/message/PaidUpCasesHeader"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<xsl:apply-templates select="message/PaidUpCase"/>
			<tr>
				<td height="30" bgcolor="#E7D7B6">
					<b>
						<xsl:value-of select="/message/recentenquiriesheader"/>
					</b>
				</td>
			</tr>
			<tr>
				<td>
					<xsl:value-of select="/message/lookuplastmonthheader"/>
					<xsl:value-of select="/message/lookuplastmonth"/>
				</td>
			</tr>
			<tr>
				<td>
					<xsl:value-of select="/message/lookuplast3monthsheader"/>
					<xsl:value-of select="/message/lookuplast3months"/>
				</td>
			</tr>
			<tr>
				<td>
					<xsl:value-of select="/message/lookuplast6monthsheader"/>
					<xsl:value-of select="/message/lookuplast6months"/>
				</td>
			</tr>
		</table>
	</xsl:template>



	<xsl:template match="case">
		<tr>
			<xsl:variable name="myid" select="@id"/>
			<td width="33%">
				<xsl:value-of select="/message/caseheader"/>
				<xsl:value-of select="@id"/>
			</td>
			<td width="33%"></td>
			<td>
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<xsl:choose>
					<xsl:when test="paidup = 'True'">
						<xsl:text disable-output-escaping="yes">&lt;table border="0" width="100%" bgcolor="#FFFFCC"&gt;</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text disable-output-escaping="yes">&lt;table border="0" width="100%" bgcolor="#CCFFFF"&gt;</xsl:text>
					</xsl:otherwise>
				</xsl:choose>


				<tr>
					<td>
						<b>
							<xsl:value-of select="/message/informationsourceheader"/>
						</b>
						<!--<br/>
						<xsl:value-of select="paidup"/>//-->
					</td>
					<td>
						<b>
							<xsl:value-of select="/message/claimownerheader"/>
						</b>
					</td>
					<td>
						<xsl:if test="count(date) &gt; 0">
							<b>
								<xsl:value-of select="/message/dateheader"/>
							</b>
						</xsl:if>
					</td>
				</tr>
				<tr>
					<td>
						<xsl:value-of select="informationsource"/>
					</td>
					<td>
						<xsl:value-of select="claimowner"/>
					</td>
					<td>
						<xsl:value-of select="date"/>
					</td>
				</tr>
				<xsl:if test="count(casefailingdate) &gt; 0">
					<tr>
						<td>
							<b>
								<xsl:value-of select="/message/casefailingdateheader"/>
							</b>
						</td>
						<td>
							<b>
								<xsl:value-of select="/message/courtrulingdateheader"/>
							</b>
						</td>
						<td>&#xA0;</td>
					</tr>
					<tr>
						<td>
							<xsl:value-of select="casefailingdate"/>
						</td>
						<td>
							<xsl:value-of select="courtrulingdate"/>
						</td>
						<td>&#xA0;</td>
					</tr>
				</xsl:if>
				<tr>
					<td>
						<b>
							<xsl:value-of select="/message/claimtypeheader"/>
						</b>
					</td>
					<td>
						<b>
							<xsl:value-of select="amountheader"/>
						</b>
					</td>
					<td>
						<b>
							<xsl:value-of select="/message/cireferenceheader"/>
						</b>
					</td>
				</tr>
				<tr>
					<td>
						<xsl:value-of select="claimtype"/>
					</td>
					<td>
						<xsl:value-of select="amount"/>&#xA0;<xsl:value-of select="currency"/></td>
					<td>
						<xsl:value-of select="cireference"/>
					</td>
				</tr>
				<xsl:for-each select="paidupdate">
					<tr>
						<td>
							<b>
								<xsl:value-of select="/message/paidupdateheader"/>
							</b>
						</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>
							<xsl:value-of select="/message/case/paidupdate"/>
						</td>
						<td></td>
						<td></td>
					</tr>
				</xsl:for-each>
				<tr>
					<td colspan="2">
						<b>
							<xsl:element name="font">
								<xsl:attribute name="color">
									<xsl:text>Maroon</xsl:text>
								</xsl:attribute>
								<xsl:value-of select="/message/claimrelationheader"/>
							</xsl:element>
						</b>
						<br/>
						<!--
							<xsl:value-of select="claimrelation"/>
						<br/-->
						<xsl:element name="table">
							<xsl:attribute name="width">80%</xsl:attribute>
							<xsl:for-each select="RelatedParty">
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:choose>
											<xsl:when test="IsSearched">
												<xsl:element name="u">
													<xsl:value-of select="Name"/>
													<xsl:text>&#xA0;</xsl:text>
													<xsl:if test="IdNumber">
														<xsl:text>(</xsl:text>
														<xsl:value-of select="IdNumber"/>
														<xsl:text>)</xsl:text>
													</xsl:if>
												</xsl:element>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="Name"/>
												<xsl:text>&#xA0;</xsl:text>
												<xsl:if test="IdNumber">
													<xsl:text>(</xsl:text>
													<xsl:value-of select="IdNumber"/>
													<xsl:text>)</xsl:text>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:element>
									<xsl:element name="td">
										<xsl:choose>
											<xsl:when test="IsSearched">
												<xsl:element name="u">
													<xsl:value-of select="Type"/>
												</xsl:element>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="Type"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</td>
					<xsl:if test="accruedpenalties_t9">
						<xsl:element name="td">
							<xsl:element name="b">
								<xsl:value-of select="/message/accruedpenaltiesheader"/>
							</xsl:element>
							<xsl:element name="br"/>
							<xsl:value-of select="accruedpenalties_t9"/>
							<xsl:text>&#xA0;</xsl:text>
							<xsl:value-of select="currency"/>
						</xsl:element>
					</xsl:if>
				</tr>
				<xsl:if test="collateral">
					<tr>
						<td colspan="3">
							<hr width="100%"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">&#xA0;<xsl:value-of select="/message/collateralheader"/>:</td>
					</tr>
					<tr>
						<td colspan="3">
							<table border="0" cellspacing="2" cellpadding="2">
								<tr>
									<td align="left">
										<b>
											<xsl:value-of select="/message/coltypeheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/coladdressheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colcityheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colzipheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colcountryheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colvalueheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colownerheader"/>
										</b>
									</td>
								</tr>
								<xsl:for-each select="collateral">
									<tr>
										<td>
											<xsl:value-of select="coltypeid"/>
										</td>
										<td>
											<xsl:value-of select="coladdress"/>
										</td>
										<td>
											<xsl:value-of select="colcity"/>
										</td>
										<td>
											<xsl:value-of select="colzip"/>
										</td>
										<td>
											<xsl:value-of select="colcountry"/>
										</td>
										<td>
											<xsl:value-of select="colvalue"/>
										</td>
										<td>
											<xsl:value-of select="colowner"/>
										</td>
									</tr>
								</xsl:for-each>
							</table>
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="accruedpenalties">
					<tr>
						<td colspan="3">
							<hr width="100%"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">&#xA0;<xsl:value-of select="/message/detailedclaiminformationheader"/>:</td>
					</tr>
					<tr>
						<td colspan="3">
							<xsl:element name="table">
								<xsl:attribute name="width">92%</xsl:attribute>
								<!--xsl:attribute name="border">1</xsl:attribute-->
								<xsl:if test="InitiallyDisbursedAmount">
									<xsl:element name="tr">
										<xsl:element name="td">
											<xsl:attribute name="align">
												<xsl:text>right</xsl:text>
											</xsl:attribute>
											<xsl:attribute name="width">35%</xsl:attribute>
											<xsl:element name="b">
												<xsl:value-of select="/message/InitiallyDisbursedAmountheader"/>
											</xsl:element>
											<xsl:element name="br"/>
											<xsl:value-of select="InitiallyDisbursedAmount"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency"/>
										</xsl:element>
										<xsl:if test="overdueprincipal">
											<xsl:element name="td">
												<xsl:attribute name="align">
													<xsl:text>right</xsl:text>
												</xsl:attribute>
												<xsl:attribute name="width">35%</xsl:attribute>
												<xsl:element name="b">
													<xsl:value-of select="/message/overdueprincipalheader"/>
												</xsl:element>
												<xsl:element name="br"/>
												<xsl:value-of select="overdueprincipal"/>
												<xsl:text>&#xA0;</xsl:text>
												<xsl:value-of select="currency"/>
											</xsl:element>
										</xsl:if>
										<xsl:if test="accruedinterest">
											<xsl:element name="td">
												<xsl:attribute name="align">
													<xsl:text>right</xsl:text>
												</xsl:attribute>
												<xsl:element name="b">
													<xsl:value-of select="/message/accruedinterestheader"/>
												</xsl:element>
												<xsl:element name="br"/>
												<xsl:value-of select="accruedinterest"/>
												<xsl:text>&#xA0;</xsl:text>
												<xsl:value-of select="currency"/>
											</xsl:element>
										</xsl:if>
									</xsl:element>
									<xsl:element name="tr">
										<xsl:element name="td">
											<xsl:attribute name="height">8</xsl:attribute>
										</xsl:element>
									</xsl:element>
								</xsl:if>
								<xsl:element name="tr">
									<xsl:if test="currentbalance">
										<xsl:element name="td">
											<xsl:attribute name="align">
												<xsl:text>right</xsl:text>
											</xsl:attribute>
											<xsl:element name="b">
												<xsl:value-of select="/message/currentbalanceheader"/>
											</xsl:element>
											<xsl:element name="br"/>
											<xsl:value-of select="currentbalance"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency"/>
										</xsl:element>
									</xsl:if>
									<xsl:if test="accruedpenalties">
										<xsl:element name="td"/>
										<xsl:element name="td">
											<xsl:attribute name="align">
												<xsl:text>right</xsl:text>
											</xsl:attribute>
											<xsl:element name="b">
												<xsl:value-of select="/message/accruedpenaltiesheader"/>
											</xsl:element>
											<xsl:element name="br"/>
											<xsl:value-of select="accruedpenalties"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency"/>
										</xsl:element>
									</xsl:if>
								</xsl:element>
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:attribute name="height">8</xsl:attribute>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</td>
					</tr>
				</xsl:if>
				<xsl:text disable-output-escaping="yes">&lt;/table&gt;</xsl:text>
			</td>
		</tr>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">10</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="BoardMembers">
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">30</xsl:attribute>
				<xsl:attribute name="bgcolor">#E7D7B6</xsl:attribute>
				<xsl:element name="b">
					<xsl:value-of select="/message/BoardMemberHeader"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:choose>
			<xsl:when test="BoardMember">
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:element name="table">
							<xsl:attribute name="border">0</xsl:attribute>
							<xsl:attribute name="cellspacing">2</xsl:attribute>
							<xsl:attribute name="cellpadding">2</xsl:attribute>
							<xsl:attribute name="width">100%</xsl:attribute>
							<xsl:element name="tr">
								<xsl:element name="td">
									<xsl:attribute name="width">50%</xsl:attribute>
									<xsl:attribute name="align">
										<xsl:text>left</xsl:text>
									</xsl:attribute>
									<xsl:element name="b">
										<xsl:value-of select="/message/BoardMemberNameHeader"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="td">
									<xsl:element name="b">
										<xsl:value-of select="/message/BoardMemberIdNumberHeader"/>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							<xsl:for-each select="BoardMember">
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:value-of select="Name"/>
									</xsl:element>
									<xsl:element name="td">
										<xsl:value-of select="IdNumber"/>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="/message/NoBoardMembersFound"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">5</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="Principals">
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">30</xsl:attribute>
				<xsl:attribute name="bgcolor">#E7D7B6</xsl:attribute>
				<xsl:element name="b">
					<xsl:value-of select="/message/PrincipalsHeader"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:choose>
			<xsl:when test="Principal">
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:element name="table">
							<xsl:attribute name="border">0</xsl:attribute>
							<xsl:attribute name="cellspacing">2</xsl:attribute>
							<xsl:attribute name="cellpadding">2</xsl:attribute>
							<xsl:attribute name="width">100%</xsl:attribute>
							<xsl:element name="tr">
								<xsl:element name="td">
									<xsl:attribute name="width">50%</xsl:attribute>
									<xsl:attribute name="align">
										<xsl:text>left</xsl:text>
									</xsl:attribute>
									<xsl:element name="b">
										<xsl:value-of select="/message/PrincipalsNameHeader"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="td">
									<xsl:element name="b">
										<xsl:value-of select="/message/PrincipalsIdNumberHeader"/>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							<xsl:for-each select="Principal">
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:value-of select="Name"/>
									</xsl:element>
									<xsl:element name="td">
										<xsl:value-of select="IdNumber"/>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="/message/NoPrincipalsFound"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">5</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="Shareholders">
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">30</xsl:attribute>
				<xsl:attribute name="bgcolor">#E7D7B6</xsl:attribute>
				<xsl:element name="b">
					<xsl:value-of select="/message/ShareholdersHeader"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:choose>
			<xsl:when test="Shareholder">
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:element name="table">
							<xsl:attribute name="border">0</xsl:attribute>
							<xsl:attribute name="cellspacing">2</xsl:attribute>
							<xsl:attribute name="cellpadding">2</xsl:attribute>
							<xsl:attribute name="width">100%</xsl:attribute>
							<xsl:element name="tr">
								<xsl:element name="td">
									<xsl:attribute name="width">50%</xsl:attribute>
									<xsl:attribute name="align">
										<xsl:text>left</xsl:text>
									</xsl:attribute>
									<xsl:element name="b">
										<xsl:value-of select="/message/ShareholdersNameHeader"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="td">
									<xsl:element name="b">
										<xsl:value-of select="/message/ShareholdersIdNumberHeader"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="td">
									<xsl:attribute name="align">
										<xsl:text>right</xsl:text>
									</xsl:attribute>
									<xsl:element name="b">
										<xsl:value-of select="/message/ShareholdersOwnershipPercentHeader"/>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							<xsl:for-each select="Shareholder">
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:value-of select="Name"/>
									</xsl:element>
									<xsl:element name="td">
										<xsl:value-of select="IdNumber"/>
									</xsl:element>
									<xsl:element name="td">
										<xsl:attribute name="align">
											<xsl:text>right</xsl:text>
										</xsl:attribute>
										<xsl:value-of select="OwnershipPercent"/>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="/message/NoShareholdersFound"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">5</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="CompanyInvolvements">
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">30</xsl:attribute>
				<xsl:attribute name="bgcolor">#E7D7B6</xsl:attribute>
				<xsl:element name="b">
					<xsl:value-of select="/message/CompanyInvolvementsHeader"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:choose>
			<xsl:when test="CompanyInvolvement">
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:element name="table">
							<xsl:attribute name="border">0</xsl:attribute>
							<xsl:attribute name="cellspacing">2</xsl:attribute>
							<xsl:attribute name="cellpadding">2</xsl:attribute>
							<xsl:attribute name="width">100%</xsl:attribute>
							<xsl:element name="tr">
								<xsl:element name="td">
									<xsl:attribute name="width">50%</xsl:attribute>
									<xsl:attribute name="align">
										<xsl:text>left</xsl:text>
									</xsl:attribute>
									<xsl:element name="b">
										<xsl:value-of select="/message/CompanyInvolvementsNameHeader"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="td">
									<xsl:element name="b">
										<xsl:value-of select="/message/CompanyInvolvementsIdNumberHeader"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="td">
									<xsl:attribute name="align">
										<xsl:text>right</xsl:text>
									</xsl:attribute>
									<xsl:element name="b">
										<xsl:value-of select="/message/CompanyInvolvementsOwnershipPercentHeader"/>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							<xsl:for-each select="CompanyInvolvement">
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:value-of select="Name"/>
									</xsl:element>
									<xsl:element name="td">
										<xsl:value-of select="IdNumber"/>
									</xsl:element>
									<xsl:element name="td">
										<xsl:attribute name="align">
											<xsl:text>right</xsl:text>
										</xsl:attribute>
										<xsl:value-of select="Ownership"/>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="/message/NoInvolvementsFound"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">5</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>





















































	<xsl:template match="PaidUpCase">
		<tr>
			<xsl:variable name="myid" select="@id"/>
			<td width="33%">
				<xsl:value-of select="/message/caseheader"/>
				<xsl:value-of select="@id"/>
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<xsl:element name="table">
					<xsl:attribute name="border">0</xsl:attribute>
					<xsl:attribute name="width">100%</xsl:attribute>
					<xsl:attribute name="bgcolor">
						<xsl:text>#CCFFFF</xsl:text>
					</xsl:attribute>


					<tr>
						<td>
							<b>
								<xsl:value-of select="/message/informationsourceheader"/>
							</b>
							<br/>
							<xsl:value-of select="paidup"/>
						</td>
						<td>
							<b>
								<xsl:value-of select="/message/claimownerheader"/>
							</b>
						</td>
						<td>
							<xsl:if test="count(date) &gt; 0">
								<b>
									<xsl:value-of select="/message/dateheader"/>
								</b>
							</xsl:if>
						</td>
					</tr>
					<tr>
						<td>
							<xsl:value-of select="informationsource"/>
						</td>
						<td>
							<xsl:value-of select="claimowner"/>
						</td>
						<td>
							<xsl:value-of select="date"/>
						</td>
					</tr>
					<xsl:if test="count(casefailingdate) &gt; 0">
						<tr>
							<td>
								<b>
									<xsl:value-of select="/message/casefailingdateheader"/>
								</b>
							</td>
							<td>

								<b>
									<xsl:value-of select="/message/courtrulingdateheader"/>
								</b>
							</td>
							<td>&#xA0;</td>
						</tr>
						<tr>
							<td>
								<xsl:value-of select="casefailingdate"/>
							</td>
							<td>
								<xsl:value-of select="courtrulingdate"/>
							</td>
							<td>&#xA0;</td>
						</tr>
					</xsl:if>
					<tr>
						<td>
							<b>
								<xsl:value-of select="/message/claimtypeheader"/>
							</b>
						</td>
						<td>
							<b>
								<xsl:value-of select="amountheader"/>
							</b>
						</td>
						<td>
							<b>
								<xsl:value-of select="/message/cireferenceheader"/>
							</b>
						</td>
					</tr>
					<tr>
						<td>
							<xsl:value-of select="claimtype"/>
						</td>
						<td>
							<xsl:value-of select="amount"/>&#xA0;<xsl:value-of select="currency"/></td>
						<td>
							<xsl:value-of select="cireference"/>
						</td>
					</tr>
					<tr>
						<xsl:element name="td">
							<xsl:attribute name="colspan">2</xsl:attribute>
							<xsl:element name="b">
								<xsl:element name="font">
									<xsl:attribute name="color">
										<xsl:text>Maroon</xsl:text>
									</xsl:attribute>
									<xsl:value-of select="/message/claimrelationheader"/>
								</xsl:element>
							</xsl:element>
							<xsl:element name="br"/>
							<xsl:element name="table">
								<xsl:attribute name="width">90%</xsl:attribute>
								<xsl:for-each select="RelatedParty">
									<xsl:element name="tr">
										<xsl:element name="td">
											<xsl:choose>
												<xsl:when test="IsSearched">
													<xsl:element name="u">
														<xsl:value-of select="Name"/>
														<xsl:text>&#xA0;</xsl:text>
														<xsl:if test="IdNumber">
															<xsl:text>(</xsl:text>
															<xsl:value-of select="IdNumber"/>
															<xsl:text>)</xsl:text>
														</xsl:if>
													</xsl:element>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="Name"/>
													<xsl:text>&#xA0;</xsl:text>
													<xsl:if test="IdNumber">
														<xsl:text>(</xsl:text>
														<xsl:value-of select="IdNumber"/>
														<xsl:text>)</xsl:text>
													</xsl:if>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:element>
										<xsl:element name="td">
											<xsl:choose>
												<xsl:when test="IsSearched">
													<xsl:element name="u">
														<xsl:value-of select="Type"/>
													</xsl:element>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="Type"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:for-each>
							</xsl:element>
						</xsl:element>
						<xsl:element name="td">
							<xsl:element name="b">
								<xsl:value-of select="/message/paidupdateheader"/>
							</xsl:element>
							<xsl:element name="br"/>
							<xsl:value-of select="paidupdate"/>
						</xsl:element>
					</tr>
					<tr>
						<td colspan="3">
							<hr width="100%"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">&#xA0;<xsl:value-of select="/message/collateralheader"/>:</td>
					</tr>
					<tr>
						<td colspan="3">
							<table border="0" cellspacing="2" cellpadding="2">
								<tr>
									<td align="left">
										<b>
											<xsl:value-of select="/message/coltypeheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/coladdressheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colcityheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colzipheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colcountryheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colvalueheader"/>
										</b>
									</td>
									<td>
										<b>
											<xsl:value-of select="/message/colownerheader"/>
										</b>
									</td>
								</tr>
								<xsl:for-each select="collateral">
									<tr>
										<td>
											<xsl:value-of select="coltypeid"/>
										</td>
										<td>
											<xsl:value-of select="coladdress"/>
										</td>
										<td>
											<xsl:value-of select="colcity"/>
										</td>
										<td>
											<xsl:value-of select="colzip"/>
										</td>
										<td>
											<xsl:value-of select="colcountry"/>
										</td>
										<td>
											<xsl:value-of select="colvalue"/>
										</td>
										<td>
											<xsl:value-of select="colowner"/>
										</td>
									</tr>
								</xsl:for-each>
							</table>
						</td>
					</tr>
					<xsl:if test="InitiallyDisbursedAmount">
						<tr>
							<td colspan="3">
								<hr width="100%"/>
							</td>
						</tr>
						<tr>
							<td colspan="3">&#xA0;<xsl:value-of select="/message/detailedclaiminformationheader"/>:</td>
						</tr>
						<tr>
							<td colspan="3">
								<xsl:element name="table">
									<xsl:attribute name="width">92%</xsl:attribute>
									<!--xsl:attribute name="border">1</xsl:attribute-->
									<xsl:element name="tr">
										<xsl:element name="td">
											<xsl:attribute name="align">
												<xsl:text>right</xsl:text>
											</xsl:attribute>
											<xsl:attribute name="width">35%</xsl:attribute>
											<xsl:element name="b">
												<xsl:value-of select="/message/InitiallyDisbursedAmountheader"/>
											</xsl:element>
											<xsl:element name="br"/>
											<xsl:value-of select="InitiallyDisbursedAmount"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency"/>
										</xsl:element>
										<xsl:element name="td">
											<xsl:attribute name="align">
												<xsl:text>right</xsl:text>
											</xsl:attribute>
											<xsl:attribute name="width">35%</xsl:attribute>
											<xsl:element name="b">
												<xsl:value-of select="/message/overdueprincipalheader"/>
											</xsl:element>
											<xsl:element name="br"/>
											<xsl:value-of select="overdueprincipal"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency"/>
										</xsl:element>
										<xsl:element name="td">
											<xsl:attribute name="align">
												<xsl:text>right</xsl:text>
											</xsl:attribute>
											<xsl:element name="b">
												<xsl:value-of select="/message/accruedinterestheader"/>
											</xsl:element>
											<xsl:element name="br"/>
											<xsl:value-of select="accruedinterest"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency"/>
										</xsl:element>
									</xsl:element>
									<xsl:element name="tr">
										<xsl:element name="td">
											<xsl:attribute name="height">8</xsl:attribute>
										</xsl:element>
									</xsl:element>
									<xsl:element name="tr">
										<xsl:element name="td">
											<xsl:attribute name="align">
												<xsl:text>right</xsl:text>
											</xsl:attribute>
											<xsl:element name="b">
												<xsl:value-of select="/message/currentbalanceheader"/>
											</xsl:element>
											<xsl:element name="br"/>
											<xsl:value-of select="currentbalance"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency"/>
										</xsl:element>
										<xsl:element name="td"/>
										<xsl:element name="td">
											<xsl:attribute name="align">
												<xsl:text>right</xsl:text>
											</xsl:attribute>
											<xsl:element name="b">
												<xsl:value-of select="/message/accruedpenaltiesheader"/>
											</xsl:element>
											<xsl:element name="br"/>
											<xsl:value-of select="accruedpenalties"/>
											<xsl:text>&#xA0;</xsl:text>
											<xsl:value-of select="currency"/>
										</xsl:element>
									</xsl:element>
									<xsl:element name="tr">
										<xsl:element name="td">
											<xsl:attribute name="height">8</xsl:attribute>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</td>
						</tr>
					</xsl:if>
				</xsl:element>
			</td>
		</tr>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">10</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>





	<xsl:template match="*"></xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="CourtInfoDefaultDebts" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\..\XML&#x2D;Samples\CourtInfoDefaultDebtsTest.xml" htmlbaseurl="" outputurl="..\..\..\..\..\..\xml&#x2D;Samples\CourtInfoDefaultDebtsTest.html" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->