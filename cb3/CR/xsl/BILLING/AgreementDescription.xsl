<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="AgreementDescription" match="AgreementDescription">
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">10</xsl:attribute>
			</xsl:element>
		</xsl:element>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="colspan">2</xsl:attribute>
				<xsl:element name="table">
					<xsl:attribute name="id">
						<xsl:value-of select="@name"/>
					</xsl:attribute>
					<xsl:attribute name="class">
						<xsl:text>grid_table</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="cellSpacing">0</xsl:attribute>
					<xsl:attribute name="cellPadding">0</xsl:attribute>
					<xsl:element name="tr">
						<xsl:element name="td">
							<xsl:value-of select="@Title"/>
						</xsl:element>
						<xsl:element name="td">
							<xsl:value-of select="@GeneralDescription"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">23</xsl:attribute>
			</xsl:element>
		</xsl:element>
		<xsl:text disable-output-escaping="yes">&lt;/td&gt;&lt;/table&gt;&lt;/tr&gt;</xsl:text>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\..\temp\CR&#x2D;XML&#x2D;Test\test1.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->