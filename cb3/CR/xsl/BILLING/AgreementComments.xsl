<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="AgreementComments" match="AgreementComments">
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="height">10</xsl:attribute>
			</xsl:element>
		</xsl:element>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:attribute name="colspan">2</xsl:attribute>
				<xsl:element name="table">
					<xsl:attribute name="id">
						<xsl:value-of select="@name"/>
					</xsl:attribute>
					<xsl:attribute name="class">
						<xsl:text>grid_table</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="cellSpacing">0</xsl:attribute>
					<xsl:attribute name="cellPadding">0</xsl:attribute>
					<xsl:element name="tr">
						<xsl:element name="th">
							<xsl:attribute name="colspan">3</xsl:attribute>
							<xsl:value-of select="@Title"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="tr">
						<xsl:element name="td">
							<xsl:element name="table">
								<xsl:attribute name="class">
									<xsl:text>fields</xsl:text>
								</xsl:attribute>
								<xsl:attribute name="cellSpacing">0</xsl:attribute>
								<xsl:attribute name="cellPadding">0</xsl:attribute>
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:element name="b">
											<xsl:value-of select="@Comment"/>
										</xsl:element>
									</xsl:element>
									<xsl:element name="td">
										<xsl:element name="b">
											<xsl:value-of select="@Date"/>
										</xsl:element>
									</xsl:element>
									<xsl:element name="td">
										<xsl:element name="b">
											<xsl:value-of select="@Emails"/>
										</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:for-each select="AgreementCommentBLLC">
									<xsl:element name="tr">
										<xsl:element name="td">
											<xsl:value-of select="Comment"/>
										</xsl:element>
										<xsl:element name="td">
											<xsl:value-of select="Inserted"/>
										</xsl:element>
										<xsl:element name="td">
											<xsl:value-of select="NotificationEmail"/>
										</xsl:element>
									</xsl:element>
								</xsl:for-each>
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:attribute name="height">23</xsl:attribute>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\..\temp\CR&#x2D;XML&#x2D;Test\test1.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->