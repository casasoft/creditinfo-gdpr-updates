<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="Header" match="Header">
		<!--xsl:element name="tr">
			<xsl:element name="td">
				<xsl:element name="table">
					<xsl:attribute name="class">
						<xsl:text>datagrid</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="cellSpacing">0</xsl:attribute>
					<xsl:attribute name="cellPadding">0</xsl:attribute>					
					<xsl:element name="tr"-->

		<xsl:text disable-output-escaping="yes">&lt;tr&gt;&lt;td&gt;&lt;table class="fields" cellspacing="0" cellpadding="0"&gt;&lt;tr valign="topt"&gt;</xsl:text>

		<xsl:element name="td">
			<xsl:element name="table">
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="Name/@value"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="RegistrationID/@value"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="Address/@value"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="City/@value"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="PostalCode/@value"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:value-of select="POBox/@value"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->