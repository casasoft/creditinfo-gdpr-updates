<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="AgreementHeader" match="AgreementHeader">
		<xsl:element name="td">
			<xsl:element name="table">
				<!--xsl:attribute name="border">1</xsl:attribute-->
				<xsl:attribute name="id">
					<xsl:value-of select="@name"/>
				</xsl:attribute>
				<xsl:attribute name="cellSpacing">0</xsl:attribute>
				<xsl:attribute name="cellPadding">0</xsl:attribute>
				<xsl:attribute name="width">100%</xsl:attribute>
				<xsl:element name="tr">
					<xsl:element name="td">
						<xsl:text>&#xA0;</xsl:text>
					</xsl:element>
					<xsl:element name="td">
						<xsl:element name="table">
							<xsl:attribute name="width">100%</xsl:attribute>
							<xsl:element name="tr">
								<xsl:element name="td">
									<xsl:attribute name="width">50%</xsl:attribute>
									<xsl:element name="b">
										<xsl:value-of select="@CurrencyText"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="td">
									<xsl:value-of select="@Currency"/>
								</xsl:element>
							</xsl:element>
							<xsl:element name="tr">
								<xsl:element name="td">
									<xsl:element name="b">
										<xsl:value-of select="@BeginDateText"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="td">
									<xsl:value-of select="@BeginDate"/>
								</xsl:element>
							</xsl:element>
							<xsl:element name="tr">
								<xsl:element name="td">
									<xsl:element name="b">
										<xsl:value-of select="@EndDateText"/>
									</xsl:element>
								</xsl:element>
								<xsl:element name="td">
									<xsl:value-of select="@EndDate"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:text disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\..\temp\CR&#x2D;XML&#x2D;Test\test1.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->