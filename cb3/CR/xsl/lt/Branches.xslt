<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Branches' match='Branches'>
		<tr><td>
		<table id='tblBranches' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead" style="WIDTH: 70px"><xsl:value-of select='Header/@CompanyCode'/></td>
				<td class="listhead"><xsl:value-of select='Header/@CompanyName'/></td>
				<td class="listhead">
					<xsl:value-of select='Header/@LERStatus'/>
					<!--<xsl:text disable-output-escaping="yes">/</xsl:text> -->
					<xsl:value-of select='Header/@SLERStatus'/>
				</td>
				<td class="listhead"><xsl:value-of select='Header/@District'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD>
							<xsl:choose>
								<xsl:when test='@imone'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@imone"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@imone"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@imone"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
						<TD><xsl:value-of select="@pavad"/></TD>
						<TD>
							<xsl:value-of select="@ler"/>
						<!--	<xsl:text disable-output-escaping="yes">/</xsl:text> -->
						</TD>
						<TD><xsl:value-of select="@District"/></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD></TD>
						<TD><xsl:value-of select="@sler"/></TD>
						<TD></TD>
					</TR>

					</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD>
							<xsl:choose>
								<xsl:when test='@imone'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@imone"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@imone"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@imone"/>
								</xsl:otherwise>				
							</xsl:choose>								
						</TD>
						<TD><xsl:value-of select="@pavad"/></TD>
						<TD>
							<xsl:value-of select="@ler"/>
						<!--	<xsl:text disable-output-escaping="yes">/</xsl:text> -->
						</TD>
						<TD><xsl:value-of select="@District"/></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD></TD>
						<TD><xsl:value-of select="@sler"/></TD>
						<TD></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>			
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>

  