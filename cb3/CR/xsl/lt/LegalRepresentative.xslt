<?xml version="1.0" encoding="UTF-8" ?>
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='LegalRepresentative' match='LegalRepresentative'>
		<tr><td colspan="7"><b><xsl:value-of select='@title'/></b></td></tr>
		<xsl:for-each select="tr">
			<xsl:call-template name="DirectorsAndMoreRow"/>
		</xsl:for-each>
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</stylesheet>

  