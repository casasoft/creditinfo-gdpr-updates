<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Claims' match='Claims'>
		<tr><td>
		<table id='tblClaims' class="list" width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<xsl:choose>
					<xsl:when test='NoInfo/@value'>
						<tr><td><xsl:value-of select="NoInfo/@value"/></td></tr>
					</xsl:when>
				</xsl:choose>
			
			<xsl:for-each select="tr">
				<tr>
					<td rowspan="4"></td>
					<td class="listhead" colspan="2"><xsl:value-of select="@CaseNumberText"/> : <xsl:value-of select="@CaseNumber"/></td>
					<td class="listhead" ><xsl:value-of select="@DirectorText"/> : <xsl:value-of select="@Director"/> </td>
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@ClaimTypeText"/></b>:<br></br><xsl:value-of select="@ClaimType"/></td>
					<td class="dark-row"><b><xsl:value-of select="@DateText"/></b>:<br></br><xsl:value-of select="@Date"/></td>
					<td class="dark-row"><b><xsl:value-of select="@AmountText"/></b>:<br></br><xsl:value-of select="@Amount"/></td>
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@CreditorCodeText"/></b>:<br></br><xsl:value-of select="@CreditorCode"/></td>
					<td class="dark-row"><b><xsl:value-of select="@CreditorNameText"/></b>:<br></br><xsl:value-of select="@CreditorName"/></td>
					<td class="dark-row" colspan="3"><b><xsl:value-of select="@InfoSourceText"/></b>:<br></br><xsl:value-of select="@InfoSource"/></td>
				</tr>
				<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>

  