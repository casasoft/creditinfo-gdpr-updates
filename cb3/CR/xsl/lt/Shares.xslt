<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Shares' match='Shares'>
		<tr><td>
		<table id='tblShares' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="3"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead"><xsl:value-of select='Header/@SharesType'/></td>
				<td class="listhead"><xsl:value-of select='Header/@ShareNum'/></td>
				<td class="listhead"><xsl:value-of select='Header/@ShareValue'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
		
			
			<xsl:for-each select="tr">
				<TR>
					<TD ><xsl:value-of select="@Pavad"/></TD>
					<TD><xsl:value-of select="@Skaic"/></TD>
					<TD><xsl:value-of select="@NomKaina"/></TD>
				</TR>
			</xsl:for-each>
			
			<tr height = '10'><td></td></tr>	
		
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>

  