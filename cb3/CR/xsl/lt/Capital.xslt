<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Capital' match='Capital'>
		<tr><td>
		<table id='tblCapital' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="3"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead" style="WIDTH: 100px"><xsl:value-of select='Header/@Nominal'/></td>
				<td class="listhead" style="WIDTH: 80px"><xsl:value-of select='Header/@Issued'/></td>
				<td class="listhead"><xsl:value-of select='Header/@PropertyForm'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<TR>
					<TD><xsl:value-of select="@IstatKap"/></TD>
					<TD><xsl:value-of select="@TvirData"/></TD>
					<TD><xsl:value-of select="@Pavad"/></TD>
				</TR>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>

  