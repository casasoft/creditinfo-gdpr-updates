<?xml version="1.0" encoding="UTF-8" ?>
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform">
<xsl:template name='OtherInformation' match='OtherInformation'>
		<tr><td>
		<table id='tblOtherInformation' width='100%' class="list" cellSpacing="0" cellPadding="1">
			<tr class="listhead" valign="top">
				<td><xsl:value-of select='Header/@Number'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@DecreeNumber'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@Source'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@Identity'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@Name'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@SonDaughter'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@PlaceOfBirth'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@Type'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR valign="top">
						<TD class="listhead" style="text-alignment:right;padding-top:2pt"><xsl:value-of select="@Number"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@DecreeNumber"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Source"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD>
							<xsl:choose>
								<xsl:when test='@Identity'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?TAFReport=true&amp;reqNationalID=</xsl:text>
									<xsl:value-of select="@Identity"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Identity"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Identity"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Name"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@SonDaughter"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@PlaceOfBirth"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Type"/></TD>
					</TR>
					</xsl:when>
					<xsl:otherwise>
					<TR class='dark-row' valign="top">
						<TD class="listhead" style="text-alignment:right;padding-top:2pt"><xsl:value-of select="@Number"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@DecreeNumber"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Source"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD>
							<xsl:choose>
								<xsl:when test='@Identity'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?TAFReport=true&amp;reqNationalID=</xsl:text>
									<xsl:value-of select="@Identity"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Identity"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Identity"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Name"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@SonDaughter"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@PlaceOfBirth"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Type"/></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</stylesheet>

  