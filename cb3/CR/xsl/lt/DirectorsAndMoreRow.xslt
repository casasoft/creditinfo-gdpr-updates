<?xml version="1.0" encoding="UTF-8" ?>
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='DirectorsAndMoreRow' match='DirectorsAndMoreRow'>
		<TR>
			<TD><xsl:value-of select="@Name"/></TD>
			<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
			<TD>ID#: 
				<xsl:choose>
					<xsl:when test='@PassNo'>
						<xsl:value-of select="@PassNo"/>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="@ID"/>
				<BR></BR>
				<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?TAFReport=true&amp;reqNationalID=</xsl:text>
				<xsl:value-of select="@ID"/>				
				<xsl:text disable-output-escaping="yes">'&gt;TAF-report&lt;/a&gt;</xsl:text>
			</TD>
			<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
			<TD><xsl:value-of select="@Address1"/><br></br><xsl:value-of select="@Address2"/></TD>
			<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
			<TD><xsl:value-of select="@City"/><br></br><xsl:value-of select="@Country"/></TD>
		</TR>
	</xsl:template>
</stylesheet>

  