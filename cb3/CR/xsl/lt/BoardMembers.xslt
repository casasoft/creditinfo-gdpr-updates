<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name='BoardMembers' match='BoardMembers'>
		<tr><td>
		<table id='tblBoardMembers' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="3"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead"><xsl:value-of select='Header/@BoardName'/></td>
				<td class="listhead"><xsl:value-of select='Header/@BoardAddress'/></td>
				<td class="listhead"><xsl:value-of select='Header/@BoardRole'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD><xsl:value-of select="@ID"/></TD>
						<TD><xsl:value-of select="@Name"/></TD>
						<TD><xsl:value-of select="@Position"/></TD>
					</TR>
					</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD><xsl:value-of select="@ID"/></TD>
						<TD><xsl:value-of select="@Name"/></TD>
						<TD><xsl:value-of select="@Position"/></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>

  