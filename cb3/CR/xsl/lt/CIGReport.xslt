<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	
		<xsl:import href='../general/Title.xslt'/>
		<xsl:import href='BasicInfo.xslt'/>
		<xsl:import href='MainInfo.xslt'/>
		<xsl:import href='BoardMembers.xslt'/>
		<xsl:import href='Capital.xslt'/>
		<xsl:import href='Shares.xslt'/>
		<xsl:import href='Employees.xslt'/>
		<xsl:import href='Activities.xslt'/>
		<xsl:import href='Branches.xslt'/>
		<xsl:import href='Revenue.xslt'/>
		<xsl:import href='Claims.xslt'/>
		<xsl:import href='PersonalInfo.xslt'/>
		<xsl:import href='Vehicles.xslt'/>
		<xsl:import href='Bankruptcy.xslt'/>
		<xsl:import href='Managers.xslt'/>
		
		<xsl:import href='../general/RecentEnquiries.xslt'/>
		

	
	<xsl:output method='html' indent='yes' encoding="UTF-8" media-type='text/html' version='4.0' />

	<xsl:template match='/'>
		<xsl:apply-templates />
	</xsl:template>
	
	

	<xsl:template name='Root' match='root'>
		<table id="tblMain" cellSpacing="0" cellPadding="0" width="608">
			<xsl:apply-templates />	
		</table>			
	</xsl:template>

		
	<xsl:template match='PopupLink'>
		<xsl:text disable-output-escaping="yes">&lt;A HREF='</xsl:text>
		<xsl:value-of select="@Link"/>				
		<xsl:text disable-output-escaping="yes">' target='_blank'&gt;</xsl:text>
		<xsl:value-of select="@Title"/>
		<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
	</xsl:template>
	
</xsl:stylesheet>

  