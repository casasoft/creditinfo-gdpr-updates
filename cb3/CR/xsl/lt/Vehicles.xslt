<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Vehicles' match='Vehicles'>
		<tr><td>
		<table id='tblVehicles' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="3"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead" Align="center" style="WIDTH: 70px"><xsl:value-of select='Header/@VehiclesDate'/></td>
				<td class="listhead" Align="center"><xsl:value-of select='Header/@OwnerShipType'/></td>
				<td class="listhead" Align="center"><xsl:value-of select='Header/@NumVehicles'/></td>
				
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD Align="right" style="WIDTH: 70px"><xsl:value-of select="@Data"/></TD>
						<TD Align="Center"><xsl:value-of select="@Tipas"/></TD>
						<TD Align="Center"><xsl:value-of select="@Kiekis"/></TD>
						
					</TR>
				</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD Align="right" style="WIDTH: 70px"><xsl:value-of select="@Data"/></TD>
						<TD Align="Center" ><xsl:value-of select="@Tipas"/></TD>
						<TD Align="Center"><xsl:value-of select="@Kiekis"/></TD>
						
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>

	</xsl:template>
</xsl:stylesheet>