<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name='MainInfo' match='MainInfo'>
		<tr><td>
		<table id='tblMainInfo' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="2"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='Phone/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='Phone/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='Fax/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='Fax/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='Mobile/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='Mobile/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='Address/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='Address/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='OfficeAddress/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='OfficeAddress/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='Email/@title'/> </b>
				</td>
				<td>
					<xsl:text disable-output-escaping="yes">&lt;A HREF='mailto:</xsl:text>
					<xsl:value-of select='Email/@value'/>
					<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
					<xsl:value-of select='Email/@value'/>
					<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='WWW/@title'/> </b>
				</td>
				<td>
					<xsl:text disable-output-escaping="yes">&lt;A HREF='</xsl:text>
					<xsl:value-of select='WWW/@value'/>
					<xsl:text disable-output-escaping="yes">'target='_blank'&gt;</xsl:text>
					<xsl:value-of select='WWW/@value'/>
					<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>
				</td>
			</tr>
			
	
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>

  