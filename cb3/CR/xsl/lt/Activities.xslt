<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Activities' match='Activities'>
		<tr><td>
		<table id='tblActivities' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="2"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead"><xsl:value-of select='Header/@ID'/></td>
				<td class="listhead"><xsl:value-of select='Header/@Name'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD width="80"><xsl:value-of select="@Kodas"/></TD>
						<TD><xsl:value-of select="@Pavad"/></TD>
					</TR>
					</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD><xsl:value-of select="@Kodas"/></TD>
						<TD><xsl:value-of select="@Pavad"/></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>
