<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name='BasicInfo' match='BasicInfo'>
		<tr><td>
		<table id='tblBasicInfo' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="2"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 100px">
					<b><xsl:value-of select='RegistrationID/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='RegistrationID/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 100px">
					<b><xsl:value-of select='Name/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='Name/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 100px">
					<b><xsl:value-of select='RegistrationForm/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='RegistrationForm/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 100px">
					<b><xsl:value-of select='LERStatus/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='LERStatus/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='Established/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='Established/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='Registered/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='Registered/@value'/>
				</td>
			</tr>
			
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='VATno/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='VATno/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='VATreg/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='VATreg/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='VATcancel/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='VATcancel/@value'/>
				</td>
			</tr>
			
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='Manager/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='Manager/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='MainAct/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='MainAct/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px">
					<b><xsl:value-of select='Region/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='Region/@value'/>
				</td>
			</tr>
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 150px ;color: red">
					<b ><xsl:value-of select='RegManager/@title'/> </b>
				</td>
				<td style="color: red">
					<xsl:value-of select='RegManager/@value'/>
				</td>
			</tr>
			
			<tr>
				<td Align="Left" CLASS="TRANSPARENT" VALIGN="Center" style="WIDTH: 100px">
					<b><xsl:value-of select='City/@title'/> </b>
				</td>
				<td>
					<xsl:value-of select='City/@value'/>
				</td>
			</tr>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>

  