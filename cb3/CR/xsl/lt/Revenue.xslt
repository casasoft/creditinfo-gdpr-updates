<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Revenue' match='Revenue'>
		<tr><td>
		<table id='tblRevenue' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="2"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead" Align="center" style="WIDTH: 70px"><xsl:value-of select='Header/@RevenueDate'/></td>
				<td class="listhead"  Align="center" ><xsl:value-of select='Header/@Revenue'/></td>
				
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<TR>	
					<TD Align="right" style="WIDTH: 70px"><xsl:value-of select="@date"/></TD>
					<TD Align="center"><xsl:value-of select="@revenue"/></TD>
					
				</TR>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>

  