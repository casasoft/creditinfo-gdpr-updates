<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Employees' match='Employees'>
		<tr><td>
		<table id='tblEmployees' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="2"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead"  Align="center" style="WIDTH: 70px"><xsl:value-of select='Header/@EmployeesDate'/></td>
				<td class="listhead" Align="center"><xsl:value-of select='Header/@NumEmployees'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD  Align="Right" style="WIDTH: 70px"><xsl:value-of select="@Data"/></TD>
						<TD Align="center"><xsl:value-of select="@Dsk"/></TD>
					</TR>
				</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD Align="Right" style="WIDTH: 70px"><xsl:value-of select="@Data"/></TD>
						<TD Align="center"><xsl:value-of select="@Dsk"/></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>

  