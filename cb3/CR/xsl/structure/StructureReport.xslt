<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:import href='TableColumns.xslt'/>
	
	<xsl:output method='html' indent='yes' encoding="ISO-8859-1" media-type='text/html' version='4.0' />

	<xsl:template match='/'>
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='Root' match='root'>
		<table id="tblLogo" cellSpacing="0" cellPadding="0" width="608">
			<tr>
				<td width="333" height="41" bgcolor="#891618" class="headerwhite">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
					<xsl:value-of select='@Title' />				
				</td>
				<td background="img/redline.gif">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
				<td>
					<img src="img/logo_cyprus1.gif" width="279" height="55" alt="" border="0"/>
				</td>
			</tr>
			<tr height = '2'><td></td></tr>			
		</table>	
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='MainTable' match='maintable'>
		<table id='{@name}' cellSpacing="0" cellPadding="0" width="608">
			<xsl:apply-templates />
			<tr>
				<td colSpan="2">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
			</tr>
		</table>
	</xsl:template>
	
	<xsl:template name='Table' match='table'>
		<table id='{@name}' cellSpacing="0" cellPadding="0">
			<xsl:apply-templates />			
		</table>
	</xsl:template>
	
	<xsl:template name='BorderTable' match='BorderTable'>
		<xsl:choose>
			<xsl:when test='@width'>
				<table id='{@name}' border="1" width='{@width}' cellSpacing="0" cellPadding="0">
					<xsl:apply-templates />			
				</table>
			</xsl:when>
			<xsl:otherwise>
				<table id='{@name}' border="1" cellSpacing="0" cellPadding="0">
					<xsl:apply-templates />			
				</table>
			</xsl:otherwise>
		</xsl:choose>			
	</xsl:template>
	
	<xsl:template name='Row' match='tr'>
		<tr>
			<xsl:apply-templates />
		</tr>
	</xsl:template>
	
	<xsl:template name='HeaderRow' match='HeaderRow'>
		<tr  bgColor="#9f9f9f">
			<xsl:apply-templates />
		</tr>
	</xsl:template>
	
	<xsl:template name='Value' match='value'>
		<xsl:call-template name="nbValue">
			<xsl:with-param name='value' select='.' />
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name='nbValue'>
		<xsl:param name='value' />
		<xsl:choose>
			<xsl:when test='string-length($value) = 0'></xsl:when>
			<xsl:otherwise>
				<small>
					<xsl:value-of select='$value' />
				</small>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name='HeaderValue'>
		<xsl:param name='value' />
		<xsl:choose>
			<xsl:when test='string-length($value) = 0'></xsl:when>
			<xsl:otherwise>
				<h4>
					<xsl:value-of select='$value' />
				</h4>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match='bold'>
		<b>
			<xsl:apply-templates />
		</b>
	</xsl:template>
	
	<xsl:template match='list'>
		<li>
			<xsl:apply-templates />
		</li>
	</xsl:template>
	
	<xsl:template match='normal'>
		<xsl:apply-templates />
	</xsl:template>
</xsl:stylesheet>
