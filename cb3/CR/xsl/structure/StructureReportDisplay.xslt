<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:import href='StructureReport.xslt'/>
	<xsl:import href='TableColumns.xslt'/>	
	
	<xsl:output method='html' indent='yes' encoding="ISO-8859-1" media-type='text/html' version='4.0' />

	<xsl:template match='/'>
		<xsl:apply-templates />
	</xsl:template>

</xsl:stylesheet>
