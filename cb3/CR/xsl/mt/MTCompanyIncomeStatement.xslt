<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name='IncomeStatement' match='IncomeStatement'>
    <tr>
      <td>
        <table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
          <tr>
            <td class="listhead" width="50%" style="border-bottom-width:1pt;">
              <b>
                <xsl:value-of select='@title'/>
              </b>
            </td>
            <xsl:for-each select="Years/TableItem">
              <td align="right" width="15%" class="listhead" style="border-bottom-width:1pt;">
                <b>
                  <xsl:value-of select='@Year'/>
                </b>
              </td>
            </xsl:for-each>
            <td width="15">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="15">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

          <!-- >> Blank Line -->
          <xsl:if test="TotalRevenue/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <!-- >> Turnover Section -->
          <xsl:if test="Turnover/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='Turnover/@Title'/>
              </td>
              <xsl:for-each select="Turnover/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CostOfSales/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='CostOfSales/@Title'/>
              </td>
              <xsl:for-each select="CostOfSales/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="GrossProfit/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='GrossProfit/@Title'/>
              </td>
              <xsl:for-each select="GrossProfit/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherOperatingRevenue/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OtherOperatingRevenue/@Title'/>
              </td>
              <xsl:for-each select="OtherOperatingRevenue/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalRevenue/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='TotalRevenue/@Title'/>
              </td>
              <xsl:for-each select="TotalRevenue/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Turnover Section -->

          <!-- >> Administration & Other Expenses Section -->
          <xsl:if test="OperationalProfit/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  ADMINISTRATION &amp; OTHER EXPENSES
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="SellingExpenses/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='SellingExpenses/@Title'/>
              </td>
              <xsl:for-each select="SellingExpenses/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="DistributionCost/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='DistributionCost/@Title'/>
              </td>
              <xsl:for-each select="DistributionCost/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AdministrativeOtherExpenses/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='AdministrativeOtherExpenses/@Title'/>
              </td>
              <xsl:for-each select="AdministrativeOtherExpenses/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="StaffCost/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='StaffCost/@Title'/>
              </td>
              <xsl:for-each select="StaffCost/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="DepreciationOfFixedAssets/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='DepreciationOfFixedAssets/@Title'/>
              </td>
              <xsl:for-each select="DepreciationOfFixedAssets/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherOperatingExpenses/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OtherOperatingExpenses/@Title'/>
              </td>
              <xsl:for-each select="OtherOperatingExpenses/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalCosts/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='TotalCosts/@Title'/>
              </td>
              <xsl:for-each select="TotalCosts/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OperationalProfit/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='OperationalProfit/@Title'/>
              </td>
              <xsl:for-each select="OperationalProfit/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Administration & Other Expenses Section -->

          <!-- >> Financial Items Section -->
          <xsl:if test="NetFinancialItems/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Financial Items
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="FinancialIncome/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='FinancialIncome/@Title'/>
              </td>
              <xsl:for-each select="FinancialIncome/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="InterestReceivable/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='InterestReceivable/@Title'/>
              </td>
              <xsl:for-each select="InterestReceivable/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="FinancialCost/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='FinancialCost/@Title'/>
              </td>
              <xsl:for-each select="FinancialCost/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="InterestPayable/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='InterestPayable/@Title'/>
              </td>
              <xsl:for-each select="InterestPayable/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherFinancialItems/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OtherFinancialItems/@Title'/>
              </td>
              <xsl:for-each select="OtherFinancialItems/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NetFinancialItems/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='NetFinancialItems/@Title'/>
              </td>
              <xsl:for-each select="NetFinancialItems/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Financial Income Section -->

          <!-- >> Non-Operating Income/Expenses Section -->
          <xsl:if test="NonOperatingIncomeExpenses/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select='NonOperatingIncomeExpenses/@Title'/>
              </td>
              <xsl:for-each select="NonOperatingIncomeExpenses/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Non-Operating Income/Expenses Section -->

          <!-- >> Operating and Non-Operating Results Section -->
          <xsl:if test="NetProfit/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  OPERATING &amp; NON-OPERATING RESULTS
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="ProfitBeforeExceptionalItem/@Title !=''  ">            
            <tr>
              <td>
                <xsl:value-of select='ProfitBeforeExceptionalItem/@Title'/>
              </td>
              <xsl:for-each select="ProfitBeforeExceptionalItem/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="ExceptionalItemIncomeCost/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='ExceptionalItemIncomeCost/@Title'/>
              </td>
              <xsl:for-each select="ExceptionalItemIncomeCost/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IncomeLossFromOperationsBeforeTax/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='IncomeLossFromOperationsBeforeTax/@Title'/>
              </td>
              <xsl:for-each select="IncomeLossFromOperationsBeforeTax/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Tax/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Tax/@Title'/>
              </td>
              <xsl:for-each select="Tax/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IncomeLossFromOperationsAfterTax/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='IncomeLossFromOperationsAfterTax/@Title'/>
              </td>
              <xsl:for-each select="IncomeLossFromOperationsAfterTax/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="ExtraordinaryIncomeCost/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='ExtraordinaryIncomeCost/@Title'/>
              </td>
              <xsl:for-each select="ExtraordinaryIncomeCost/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IncomeFromAssociatedCompanies/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='IncomeFromAssociatedCompanies/@Title'/>
              </td>
              <xsl:for-each select="IncomeFromAssociatedCompanies/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NetProfit/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='NetProfit/@Title'/>
              </td>
              <xsl:for-each select="NetProfit/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Operating and Non-Operating Results Section -->


          <tr>
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colSpan="2">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>