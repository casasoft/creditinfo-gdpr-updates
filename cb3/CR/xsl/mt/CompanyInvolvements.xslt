<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='CompanyInvolvements' match='CompanyInvolvements'>
		<tr><td>
		<table id='tblCompanyInvolvements' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead"><xsl:value-of select='Header/@Company'/></td>
				<td class="listhead"><xsl:value-of select='Header/@Position'/></td>
				<td class="listhead"><xsl:value-of select='Header/@Status'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
						<TD><xsl:value-of select="@Position"/></TD>
						<TD><xsl:value-of select="@Status"/></TD>
					</TR>
					</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>								
						</TD>
						<TD><xsl:value-of select="@Position"/></TD>
						<TD><xsl:value-of select="@Status"/></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>
