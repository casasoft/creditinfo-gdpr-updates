<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="PendingCases" match="PendingCases">
		<tr>
			<td>
				<table id="tblPendingCases" class="list" width="100%" cellSpacing="0" cellPadding="0">
					<tr class="sectionheaderblue">
						<td colspan="4">
							<!--<xsl:value-of select="@title"/>-->
              Pending Court Cases
						</td>
					</tr>
					<xsl:choose>
						<xsl:when test="NoInfo/@value">
							<tr>
								<td>
									<xsl:value-of select="NoInfo/@value"/>
								</td>
							</tr>
						</xsl:when>
					</xsl:choose>

					<xsl:for-each select="tr">
						<tr>
							<td rowspan="3"></td>
							<td class="listhead" colspan="3">
								<xsl:value-of select="@CaseNumberText"/>
								<xsl:value-of select="@CaseNumber"/>
							</td>
						</tr>
						<tr>
							<td class="dark-row">
								<b>
									<xsl:value-of select="@InfoSourceText"/>
								</b>:<br></br><xsl:value-of select="@InfoSource"/></td>
							<td class="dark-row">
								<b>
									<xsl:value-of select="@ClaimOwnerText"/>
								</b>:<br></br><xsl:value-of select="@ClaimOwner"/></td>
							<td class="dark-row">
								<b>
									<xsl:value-of select="@DateText"/>
								</b>:<br></br><xsl:value-of select="@Date"/></td>
						</tr>
						<tr>
							<td class="dark-row">
								<!--b>
									<xsl:value-of select="@AdjudicatorText"/>
								</b>:<br></br><xsl:value-of select="@Adjudicator"/-->
							</td>
							<xsl:element name="td">
								<xsl:attribute name="class">
									<xsl:text>dark-row</xsl:text>
								</xsl:attribute>
								<xsl:element name="b">
									<xsl:value-of select="@AmountText"/>
								</xsl:element>
								<xsl:text>:</xsl:text>
								<xsl:element name="br"/>
								<xsl:value-of select="@Amount"/>
								<xsl:text>&#xA0;</xsl:text>
								<xsl:value-of select="@CurrencyCode"/>
							</xsl:element>
							<td class="dark-row">
								<b>
									<xsl:value-of select="@CIReferenceText"/>
								</b>:<br></br><xsl:value-of select="@CIReference"/></td>
						</tr>
						<tr>
							<td>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</td>
						</tr>
					</xsl:for-each>
					<tr height="10">
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->