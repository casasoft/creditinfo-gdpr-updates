<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name='Ratios' match='Ratios'>
        <tr>
            <td>
                <table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">					
                    <tr bgcolor="#666666">
                        <td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                        <td width="240" class="header2"><b><xsl:value-of select='@title'/></b></td>
                        <xsl:for-each select="Years/TableItem">						
                            <td align="right"  class="header2"><b><xsl:value-of select='@Year'/></b></td>
                        </xsl:for-each>	
                        <td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                        <td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                    </tr>               
                                      
                    
                    <!-- Ratios-->           
                    <xsl:if test="Ratios/ProfitMargin/TableItem/@Value !='N/A'  ">
                    <tr bgColor="#E0E0E0">
                        <td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                        <td width="240"><xsl:value-of select='Ratios/ProfitMargin/@Title'/></td>
                        <xsl:for-each select="Ratios/ProfitMargin/TableItem">						
                            <td align="right" ><xsl:value-of select='@Value'/></td>
                        </xsl:for-each>
                        <td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                    </tr>
                    </xsl:if>
                    
                    <xsl:if test="Ratios/CurrentRatio/TableItem/@Value !='N/A'  ">
                        <tr bgColor="#E0E0E0">
                            <td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                            <td width="240"><xsl:value-of select='Ratios/CurrentRatio/@Title'/></td>
                            <xsl:for-each select="Ratios/CurrentRatio/TableItem">						
                                <td align="right" ><xsl:value-of select='@Value'/></td>
                            </xsl:for-each>
                            <td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                        </tr>
                    </xsl:if>

                    <xsl:if test="Ratios/QuickRatio/TableItem/@Value !='N/A'  ">
                    <tr bgColor="#E0E0E0">
                        <td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                        <td width="240"><xsl:value-of select='Ratios/QuickRatio/@Title'/></td>
                        <xsl:for-each select="Ratios/QuickRatio/TableItem">						
                            <td align="right" ><xsl:value-of select='@Value'/></td>
                        </xsl:for-each>
                        <td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                    </tr>
                    </xsl:if>
                   
                    
                    <xsl:if test="Ratios/EquityRatio/TableItem/@Value !='N/A'  ">
                    <tr bgColor="#E0E0E0">
                        <td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                        <td width="240"><xsl:value-of select='Ratios/EquityRatio/@Title'/></td>
                        <xsl:for-each select="Ratios/EquityRatio/TableItem">						
                            <td align="right" ><xsl:value-of select='@Value'/></td>
                        </xsl:for-each>
                        <td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                    </tr>
                    </xsl:if>

                  
                    <xsl:if test="Ratios/InterestCoverRatio/TableItem/@Value !='N/A'  ">
                    <tr bgColor="#E0E0E0">
                        <td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                        <td width="240"><xsl:value-of select='Ratios/InterestCoverRatio/@Title'/></td>
                        <xsl:for-each select="Ratios/InterestCoverRatio/TableItem">						
                            <td align="right" ><xsl:value-of select='@Value'/></td>
                        </xsl:for-each>
                        <td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                    </tr>
                    </xsl:if>
                    
                    
                    <xsl:if test="Ratios/ROCERatio/TableItem/@Value !='N/A'  ">
                    <tr bgColor="#E0E0E0">
                        <td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                        <td width="240"><xsl:value-of select='Ratios/ROCERatio/@Title'/></td>
                        <xsl:for-each select="Ratios/ROCERatio/TableItem">						
                            <td align="right" ><xsl:value-of select='@Value'/></td>
                        </xsl:for-each>
                        <td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                    </tr>  
                    </xsl:if>                                     
                    
                </table>			
            </td>		
        </tr>
    </xsl:template>
</xsl:stylesheet>