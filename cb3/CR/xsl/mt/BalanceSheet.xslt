<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='BalanceSheet' match='BalanceSheet'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240" class="header2"><b><xsl:value-of select='@title'/></b></td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right"  class="header2"><b><xsl:value-of select='@Year'/></b></td>
						</xsl:for-each>		
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<!--<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="Months/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Month'/></b></td>
						</xsl:for-each>
					</tr>
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="Currency/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Currency'/></b></td>
						</xsl:for-each>
					</tr>
					
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="YearEnd/TableItem">						
							<td align="right" ><b><xsl:value-of select='@YearEnd'/></b></td>
						</xsl:for-each>
					</tr>-->					
						
					
					<!-- header -->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='YearEnd/@Title'/></td>
						<xsl:for-each select="YearEnd/TableItem">						
							<td align="right" ><b><xsl:value-of select='@YearEnd'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='Months/@Title'/></td>
						<xsl:for-each select="Months/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Month'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='Currency/@Title'/></td>
						<xsl:for-each select="Currency/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Currency'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					
					<xsl:if test="FixedAssets/@Title !=''  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					
					<!-- Fixed assets -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='FixedAssets/@Title'/></b></td>
					</tr>
					</xsl:if>
					
                                        			<xsl:if test="FixedAssets/IntangibleAssets/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='FixedAssets/IntangibleAssets/@Title'/></td>
						<xsl:for-each select="FixedAssets/IntangibleAssets/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="FixedAssets/TangibleAssets/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='FixedAssets/TangibleAssets/@Title'/></td>
						<xsl:for-each select="FixedAssets/TangibleAssets/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="FixedAssets/FinancialAssets/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='FixedAssets/FinancialAssets/@Title'/></td>
						<xsl:for-each select="FixedAssets/FinancialAssets/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="FixedAssets/FixedAssetsTotal/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='FixedAssets/FixedAssetsTotal/@Title'/></b></td>
						<xsl:for-each select="FixedAssets/FixedAssetsTotal/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					
					<xsl:if test="CurrentAssets/@Title !=''  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					<!-- Current assets -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='CurrentAssets/@Title'/></b></td>
					</tr>
					</xsl:if>

					<xsl:if test="CurrentAssets/InventoryStock/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CurrentAssets/InventoryStock/@Title'/></td>
						<xsl:for-each select="CurrentAssets/InventoryStock/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="CurrentAssets/Receivables/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CurrentAssets/Receivables/@Title'/></td>
						<xsl:for-each select="CurrentAssets/Receivables/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="CurrentAssets/ClaimsTowardsAssociatedCompanies/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CurrentAssets/ClaimsTowardsAssociatedCompanies/@Title'/></td>
						<xsl:for-each select="CurrentAssets/ClaimsTowardsAssociatedCompanies/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>

					<xsl:if test="CurrentAssets/ShortTermInvestments/TableItem/@Value !='0'  ">				
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CurrentAssets//ShortTermInvestments/@Title'/></td>
						<xsl:for-each select="CurrentAssets//ShortTermInvestments/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>


					<xsl:if test="CurrentAssets/Cash/TableItem/@Value !='0'  ">				
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CurrentAssets/Cash/@Title'/></td>
						<xsl:for-each select="CurrentAssets/Cash/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>

					<xsl:if test="CurrentAssets/OtherCurrentAssets/TableItem/@Value !='0'  ">				
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CurrentAssets/OtherCurrentAssets/@Title'/></td>
						<xsl:for-each select="CurrentAssets/OtherCurrentAssets/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>					

					<xsl:if test="CurrentAssets/CurrentAssetsTotal/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='CurrentAssets/CurrentAssetsTotal/@Title'/></b></td>
						<xsl:for-each select="CurrentAssets/CurrentAssetsTotal/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>						
					</xsl:if>					
						
					
					<xsl:if test="DeferredTaxAssets/DeferredTaxAssets/TableItem/@Value !='0'  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					<!-- deferred tax assets -->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='DeferredTaxAssets/DeferredTaxAssets/@Title'/></b></td>
						<xsl:for-each select="DeferredTaxAssets/DeferredTaxAssets/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>					
					</xsl:if>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					
					<xsl:if test="AssetsTotal/AssetsTotal/TableItem/@Value !='0'  ">
					<!-- assets total -->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='AssetsTotal/AssetsTotal/@Title'/></b></td>
						<xsl:for-each select="AssetsTotal/AssetsTotal/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:if>
					
						
					<xsl:if test="Equity/@Title !=''  ">
					<!-- Equity -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Equity/@Title'/></b></td>
					</tr>
					</xsl:if>

					<xsl:if test="Equity/IssuedShareCapital/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='Equity/IssuedShareCapital/@Title'/></td>
						<xsl:for-each select="Equity/IssuedShareCapital/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="Equity/ProfitAndLossAccount/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='Equity/ProfitAndLossAccount/@Title'/></td>
						<xsl:for-each select="Equity/ProfitAndLossAccount/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="Equity/OtherEquity/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='Equity/OtherEquity/@Title'/></td>
						<xsl:for-each select="Equity/OtherEquity/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
				<!--	<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/OtherEquityCalculated/@Title'/></td>
						<xsl:for-each select="Equity/OtherEquityCalculated/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->

					<xsl:if test="Equity/EquityTotal/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='Equity/EquityTotal/@Title'/></b></td>
						<xsl:for-each select="Equity/EquityTotal/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>


					<!--<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/MinorotyHoldingsInEquity/@Title'/></td>
						<xsl:for-each select="Equity/MinorotyHoldingsInEquity/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
					<xsl:if test="Equity/@TitleLiabilities !=''  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Equity/@TitleLiabilities'/></b></td>
					</tr>
					</xsl:if>

					<xsl:if test="Equity/TaxLiabilities/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='Equity/TaxLiabilities/@Title'/></td>
						<xsl:for-each select="Equity/TaxLiabilities/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="Equity/OtherLiabilities/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='Equity/OtherLiabilities/@Title'/></td>
						<xsl:for-each select="Equity/OtherLiabilities/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<!--<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='LongTermDebts/TotalLiabilities/@Title'/></td>
						<xsl:for-each select="LongTermDebts/TotalLiabilities/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->				
					

					<xsl:if test="LongTermDebts/LongTermDebts/TableItem/@Value !='0'  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='LongTermDebts/LongTermDebts/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/LongTermDebts/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:if>			
					
						
					
					<!-- Short term debt -->
					<xsl:if test="ShortTermDebts/@Title !=''  ">
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='ShortTermDebts/@Title'/></b></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ShortTermDebts/Overdraft_BankLoan/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ShortTermDebts/Overdraft_BankLoan/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/Overdraft_BankLoan/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ShortTermDebts/Creditors/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ShortTermDebts/Creditors/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/Creditors/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
										
					<!--Debts toward associated companies -->
					<!--<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='LongTermDebts/Obligations/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/Obligations/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
					
					<xsl:if test="LongTermDebts/DebtTowardsAssociatedCompanies/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='LongTermDebts/DebtTowardsAssociatedCompanies/@Title'/></td>
						<xsl:for-each select="LongTermDebts/DebtTowardsAssociatedCompanies/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="ShortTermDebts/OtherShortTermDebts/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ShortTermDebts/OtherShortTermDebts/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/OtherShortTermDebts/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ShortTermDebts/ShortTermDebtsTotal/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='ShortTermDebts/ShortTermDebtsTotal/@Title'/></b></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>				
											

					<xsl:if test="DebtEquityTotal/TotalDebt/TableItem/@Value !='0'  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='DebtEquityTotal/TotalDebt/@Title'/></b></td>
						<xsl:for-each select="DebtEquityTotal/TotalDebt/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>				
						
				
					<xsl:if test="DebtEquityTotal/DebtEquityTotal/TableItem/@Value !='0'  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='DebtEquityTotal/DebtEquityTotal/@Title'/></b></td>
						<xsl:for-each select="DebtEquityTotal/DebtEquityTotal/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					
								
					<!-- short term debts -->
					<!--<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='ShortTermDebts/@Title'/></b></td>
					</tr>
					
					
					
					
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='ShortTermDebts/ShortTermDebtsTotal/@Title'/></b></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
										
					<!-- long term debts -->
				<!--	<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='LongTermDebts/LongTermDebts/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/LongTermDebts/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='LongTermDebts/Obligations/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/Obligations/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='LongTermDebts/TotalDebtCalculated/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/TotalDebtCalculated/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
						
						-->			
					<!-- Debt & equity total -->
					<!--<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='DebtEquityTotal/DebtEquityTotal/@Title'/></b></td>
						<xsl:for-each select="DebtEquityTotal/DebtEquityTotal/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
		

					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>