<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name='Auditted' match='Auditted'>
        <tr>
            <td>
                <table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">				
                                    
                    <xsl:if test="Auditted/Auditted/TableItem/@Value !='0'  ">                 
                         <tr>
                            <td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                            <td width="240"><b><xsl:value-of select='Auditted/@Title'/></b></td>
                            <xsl:for-each select="Auditted/Auditted/TableItem">						
                                <td align="right" ><xsl:value-of select='@Value'/></td>
                            </xsl:for-each>
                            <td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
                        </tr>
                    </xsl:if>                                 
                    
                    <tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>                                    
                    
                </table>			
            </td>		
        </tr>
    </xsl:template>
</xsl:stylesheet>