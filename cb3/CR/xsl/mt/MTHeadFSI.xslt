<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='HeadFSI' match='HeadFSI'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr class='sectionheader'>
						<td class="header2" width="50%"><b>Annual Financial Statements </b></td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right" width="15%"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>	<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					
					<tr>
						<td class="listhead"> </td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right" class="listhead" style="border-bottom-width:1pt;"><b><xsl:value-of select='@Year'/></b></td>
						</xsl:for-each>	
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					
					<!-- Profit/Loss account -->					
					<tr>
						<td><xsl:value-of select='HeadFSI/FinancialYearEnd/@Title'/></td>
						<xsl:for-each select="HeadFSI/FinancialYearEnd/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>		
										
					<tr>
						<td><xsl:value-of select='HeadFSI/AccountPeriodLength/@Title'/></td>
						<xsl:for-each select="HeadFSI/AccountPeriodLength/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					
					<tr>
						<td><xsl:value-of select='HeadFSI/Currency/@Title'/></td>
						<xsl:for-each select="HeadFSI/Currency/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>			
							
					
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>