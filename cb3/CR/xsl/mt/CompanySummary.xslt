﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:template name='CompanySummary' match='CompanySummary'>
    <xsl:if test='FinancialHighlights/@Info != "NoInfo" or count(VatNumber) != 0 or count(LastAnnualReturnFiled) != 0 or count(LastAccountsFiled) != 0'>


      <xsl:variable select="FinancialHighlights/Years/Year" name="Years"></xsl:variable>
      <tr>
        <td>
          <table id="tblRiskIndikator" width="100%" cellSpacing="0" cellPadding="0">
            <tr class="sectionheader">
              <td colspan="4">
                <xsl:value-of select="@title"/>
              </td>
            </tr>
            <tr>
              <td width="50%">
                <table width="100%" cellSpacing="0" cellPadding="0">
                  <tr>
                    <td valign="top">
                      <table width="100%" cellSpacing="0" cellPadding="0">
                        <xsl:if test="VatNumber">
                          <tr>
                            <!-- Vat data: -->
                            <td style="font-weight: bolder;">
                              <xsl:value-of select="VatNumber/@title"/>:
                            </td>

                            <!-- Status for vat: -->
                            <td>
                              <xsl:choose>
                                <xsl:when test="Status = 'Active'">
                                  <span style="color:green; font-weight:bold">
                                    Valid
                                  </span>
                                </xsl:when>
                                <xsl:otherwise>
                                  <span style="color:red; font-weight:bold">
                                    Invalid
                                  </span>
                                </xsl:otherwise>
                              </xsl:choose>
                              <span style="margin-left:5px">
                                <xsl:value-of select="VatNumber"/>
                              </span>
                            </td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="LastAnnualReturnFiled">
                          <tr>
                            <td style="font-weight: bolder;">
                              <xsl:value-of select="LastAnnualReturnFiled/@title"/>:
                            </td>
                            <td>
                              <xsl:value-of select="LastAnnualReturnFiled"/>
                            </td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="LastAccountsFiled">
                          <tr>
                            <td style="font-weight: bolder;">
                              <xsl:value-of select="LastAccountsFiled/@title"/>:
                            </td>
                            <td>
                              <xsl:value-of select="LastAccountsFiled"/>
                            </td>
                          </tr>
                        </xsl:if>
                      </table>
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                    <td width="50%">
                      <xsl:if test="count(FinancialHighlights/Years/Year) > 0">
                        <table width="100%" cellSpacing="0" cellPadding="0">
                          <tr>
                            <td colspan="3" style="font-weight: bolder;">
                              <xsl:value-of select="FinancialHighlights/@title"/>:
                            </td>
                          </tr>
                          <tr style="font-weight: bolder;">
                            <td>
                            </td>
                            <xsl:for-each select="FinancialHighlights/Years/Year">
                              <xsl:element name="td">
                                <xsl:value-of select="."/>
                              </xsl:element>
                            </xsl:for-each>
                          </tr>
                          <xsl:if test="count(FinancialHighlights/Currency/TableItem) > 0">
                            <tr>
                              <td>
                              </td>
                              <xsl:for-each select="$Years">
                                <xsl:variable name="Year" select="."></xsl:variable>
                                <xsl:element name="td">
                                  <xsl:value-of select="../../../FinancialHighlights/Currency/TableItem[@Year=$Year]/@Value"/>
                                </xsl:element>
                              </xsl:for-each>
                            </tr>
                            <tr>
                              <td colspan="3">
                              </td>
                            </tr>
                          </xsl:if>
                          <xsl:if test="count(FinancialHighlights/IssuedCapital/TableItem) > 0">
                            <tr>
                              <td  style="font-weight: bolder;">
                                <xsl:value-of select="FinancialHighlights/IssuedCapital/@title"/>:
                              </td>
                              <xsl:for-each select="$Years">
                                <xsl:variable name="Year" select="."/>
                                <xsl:element name="td">
                                  <xsl:value-of select="../../../FinancialHighlights/IssuedCapital/TableItem[@year=$Year]/@Value"/>
                                </xsl:element>
                              </xsl:for-each>
                            </tr>
                          </xsl:if>
                          <xsl:if test="count(FinancialHighlights/Turnover/TableItem) > 0">
                            <tr>
                              <td  style="font-weight: bolder;">
                                <xsl:value-of select="FinancialHighlights/Turnover/@title"/>:
                              </td>
                              <xsl:for-each select="$Years">
                                <xsl:variable name="Year" select="."/>
                                <xsl:element name="td">
                                  <xsl:value-of select="../../../FinancialHighlights/Turnover/TableItem[@year=$Year]/@Value"/>
                                </xsl:element>
                              </xsl:for-each>
                            </tr>
                          </xsl:if>
                          <xsl:if test="count(FinancialHighlights/AssetsTotal/TableItem) > 0">
                            <tr>
                              <td style="font-weight: bolder;">
                                <xsl:value-of select="FinancialHighlights/AssetsTotal/@title"/>:
                              </td>
                              <xsl:for-each select="$Years">
                                <xsl:variable name="Year" select="."/>
                                <xsl:element name="td">
                                  <xsl:value-of select="../../../FinancialHighlights/AssetsTotal/TableItem[@year=$Year]/@Value"/>
                                </xsl:element>
                              </xsl:for-each>
                            </tr>
                          </xsl:if>
                          <xsl:if test="count(FinancialHighlights/EquityTotal/TableItem) > 0">
                            <tr>
                              <td style="font-weight: bolder;">
                                <xsl:value-of select="FinancialHighlights/EquityTotal/@title"/>:
                              </td>
                              <xsl:for-each select="$Years">
                                <xsl:variable name="Year" select="."/>
                                <xsl:element name="td">
                                  <xsl:value-of select="../../../FinancialHighlights/EquityTotal/TableItem[@year=$Year]/@Value"/>
                                </xsl:element>
                              </xsl:for-each>
                            </tr>
                          </xsl:if>
                          <xsl:if test="count(FinancialHighlights/IncomeFromOperationsBeforeTax/TableItem) > 0">
                            <tr>
                              <td style="font-weight: bolder;">
                                <xsl:value-of select="FinancialHighlights/IncomeFromOperationsBeforeTax/@title"/>:
                              </td>
                              <xsl:for-each select="$Years">
                                <xsl:variable name="Year" select="."/>
                                <xsl:element name="td">
                                  <xsl:value-of select="../../../FinancialHighlights/IncomeFromOperationsBeforeTax/TableItem[@year=$Year]/@Value"/>
                                </xsl:element>
                              </xsl:for-each>
                            </tr>
                          </xsl:if>
                        </table>
                      </xsl:if>
                    </td>
                  </tr>

                </table>

              </td>
            </tr>
          </table>
        </td>
      </tr>
    </xsl:if>

  </xsl:template>
</xsl:stylesheet>
