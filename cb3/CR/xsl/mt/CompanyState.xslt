<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='CompanyState' match='CompanyState'>
		<tr><td>
		<table id='tblCompanyState' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td><xsl:value-of select='State/@value'/></td>
			</tr>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>
