<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name='IncomeStatement' match='IncomeStatement'>
    <tr>
      <td>
        <table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
          <tr bgcolor="#666666">
            <td width="15" bgColor="#666666">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240" class="header2">
              <b>
                <xsl:value-of select='@title'/>
              </b>
            </td>
            <xsl:for-each select="Years/TableItem">
              <td align="right"  class="header2">
                <b>
                  <xsl:value-of select='@Year'/>
                </b>
              </td>
            </xsl:for-each>
            <td width="15" bgColor="#666666">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="15" bgColor="#666666">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

          <!-- header -->
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='YearEnd/@Title'/>
            </td>
            <xsl:for-each select="YearEnd/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@YearEnd'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Months/@Title'/>
            </td>
            <xsl:for-each select="Months/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Month'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Currency/@Title'/>
            </td>
            <xsl:for-each select="Currency/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Value'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Consolidated/@Title'/>
            </td>
            <xsl:for-each select="Consolidated/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Value'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Abridged/@Title'/>
            </td>
            <xsl:for-each select="Abridged/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Value'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Denomination/@Title'/>
            </td>
            <xsl:for-each select="Denomination/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Value'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

          <!-- Blank Line -->
          <xsl:if test="TotalRevenue/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          
          <!-- >> Turnover Section -->
          <xsl:if test="Turnover/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Turnover/@Title'/>
              </td>
              <xsl:for-each select="Turnover/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CostOfSales/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='CostOfSales/@Title'/>
              </td>
              <xsl:for-each select="CostOfSales/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="GrossProfit/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='GrossProfit/@Title'/>
              </td>
              <xsl:for-each select="GrossProfit/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherOperatingRevenue/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='OtherOperatingRevenue/@Title'/>
              </td>
              <xsl:for-each select="OtherOperatingRevenue/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalRevenue/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='TotalRevenue/@Title'/>
              </td>
              <xsl:for-each select="TotalRevenue/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Turnover Section -->

          <!-- >> Administration & Other Expenses Section -->
          <xsl:if test="OperationalProfit/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>

            <!-- Administration & Other Expenses Header -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  ADMINISTRATION &amp; OTHER EXPENSES
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="SellingExpenses/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='SellingExpenses/@Title'/>
              </td>
              <xsl:for-each select="SellingExpenses/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="DistributionCost/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='DistributionCost/@Title'/>
              </td>
              <xsl:for-each select="DistributionCost/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AdministrativeOtherExpenses/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AdministrativeOtherExpenses/@Title'/>
              </td>
              <xsl:for-each select="AdministrativeOtherExpenses/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="StaffCost/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='StaffCost/@Title'/>
              </td>
              <xsl:for-each select="StaffCost/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="DepreciationOfFixedAssets/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='DepreciationOfFixedAssets/@Title'/>
              </td>
              <xsl:for-each select="DepreciationOfFixedAssets/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherOperatingExpenses/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='OtherOperatingExpenses/@Title'/>
              </td>
              <xsl:for-each select="OtherOperatingExpenses/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalCosts/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='TotalCosts/@Title'/>
              </td>
              <xsl:for-each select="TotalCosts/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OperationalProfit/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='OperationalProfit/@Title'/>
              </td>
              <xsl:for-each select="OperationalProfit/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Administration & Other Expenses Section -->

          <!-- >> Net Financial Items Section -->
          <xsl:if test="NetFinancialItems/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Financial Items
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="FinancialIncome/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='FinancialIncome/@Title'/>
              </td>
              <xsl:for-each select="FinancialIncome/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="InterestReceivable/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='InterestReceivable/@Title'/>
              </td>
              <xsl:for-each select="InterestReceivable/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="FinancialCost/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='FinancialCost/@Title'/>
              </td>
              <xsl:for-each select="FinancialCost/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="InterestPayable/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='InterestPayable/@Title'/>
              </td>
              <xsl:for-each select="InterestPayable/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherFinancialItems/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='OtherFinancialItems/@Title'/>
              </td>
              <xsl:for-each select="OtherFinancialItems/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NetFinancialItems/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='NetFinancialItems/@Title'/>
              </td>
              <xsl:for-each select="NetFinancialItems/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Net Financial Items Section -->

          <!-- >> Non-Operating Income/Expenses Section -->
          <xsl:if test="NonOperatingIncomeExpenses/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='NonOperatingIncomeExpenses/@Title'/>
              </td>
              <xsl:for-each select="NonOperatingIncomeExpenses/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Non-Operating Income/Expenses Section -->
          
          <!-- >> Operating & Non-Operating Results Section -->
          <xsl:if test="NetProfit/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  OPERATING &amp;  NON-OPERATING RESULTS
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="ProfitBeforeExceptionalItem/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='ProfitBeforeExceptionalItem/@Title'/>
              </td>
              <xsl:for-each select="ProfitBeforeExceptionalItem/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="ExceptionalItemIncomeCost/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='ExceptionalItemIncomeCost/@Title'/>
              </td>
              <xsl:for-each select="ExceptionalItemIncomeCost/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IncomeLossFromOperationsBeforeTax/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='IncomeLossFromOperationsBeforeTax/@Title'/>
              </td>
              <xsl:for-each select="IncomeLossFromOperationsBeforeTax/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Tax/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Tax/@Title'/>
              </td>
              <xsl:for-each select="Tax/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IncomeLossFromOperationsAfterTax/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='IncomeLossFromOperationsAfterTax/@Title'/>
              </td>
              <xsl:for-each select="IncomeLossFromOperationsAfterTax/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="ExtraordinaryIncomeCost/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='ExtraordinaryIncomeCost/@Title'/>
              </td>
              <xsl:for-each select="ExtraordinaryIncomeCost/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IncomeFromAssociatedCompanies/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='IncomeFromAssociatedCompanies/@Title'/>
              </td>
              <xsl:for-each select="IncomeFromAssociatedCompanies/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NetProfit/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='NetProfit/@Title'/>
              </td>
              <xsl:for-each select="NetProfit/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Operating & Non-Operating Results Section -->

          <tr>
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colSpan="2">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>