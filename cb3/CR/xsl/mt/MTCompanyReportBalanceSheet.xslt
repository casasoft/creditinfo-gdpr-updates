<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='BalanceSheet' match='BalanceSheet'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">					
					<tr>
						<td class="listhead" width="50%" style="border-bottom-width:1pt;"><b><xsl:value-of select='@title'/></b></td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right" width="15%" class="listhead" style="border-bottom-width:1pt;"><b><xsl:value-of select='@Year'/></b></td>
						</xsl:for-each>	
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<xsl:if test="FixedAssets/@Title !=''  ">

					
					<!-- Fixed assets -->
					<!--xsl:if test="FixedAssets/IntangibleAssets/TableItem/@Value !='0'  and FixedAssets/TangibleAssetst/TableItem/@Value !='0' and FixedAssets/FinancialAssets/TableItem/@Value !='0'  and FixedAssets/FixedAssetsTotal/TableItem/@Value !='0'  "-->	
					<tr>
						<td><b><xsl:value-of select='FixedAssets/@Title'/></b></td>						
					</tr>					
					</xsl:if>

					<xsl:if test="FixedAssets/IntangibleAssets/TableItem/@Value !='0'  ">					
					<tr>
						<td><xsl:value-of select='FixedAssets/IntangibleAssets/@Title'/></td>
						<xsl:for-each select="FixedAssets/IntangibleAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
						
						
					<xsl:if test="FixedAssets/TangibleAssets/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='FixedAssets/TangibleAssets/@Title'/></td>
						<xsl:for-each select="FixedAssets/TangibleAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					
					<xsl:if test="FixedAssets/FinancialAssets/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='FixedAssets/FinancialAssets/@Title'/></td>
						<xsl:for-each select="FixedAssets/FinancialAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="FixedAssets/FixedAssetsTotal/TableItem/@Value !='0'  ">
					<tr>
						<td><b><xsl:value-of select='FixedAssets/FixedAssetsTotal/@Title'/></b></td>
						<xsl:for-each select="FixedAssets/FixedAssetsTotal/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>										
					</xsl:if>
          
          
          <xsl:if test="DeferredTaxAssets/DeferredTaxAssets/TableItem/@Value !='0'  ">
            <tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>			
					<!-- assets total -->
					<tr>
						<td>Deferred Tax</td>
						<xsl:for-each select="DeferredTaxAssets/DeferredTaxAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:if>						
   
<xsl:if test="CurrentAssets/@Title !=''  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>			
										
					<!-- Current assets -->
					<!--xsl:if test="CurrentAssets/InventoryStock/TableItem/@Value !='0' and CurrentAssets/ClaimsTowardsAssociatedCompanies/TableItem/@Value !='0'   and CurrentAssets/Receivables/TableItem/@Value !='0'  and CurrentAssets/ShortTermInvestments/TableItem/@Value !='0'  and CurrentAssets/CurrentAssetsTotal/TableItem/@Value !='0' and CurrentAssets/OtherCurrentAssets/TableItem/@Value !='0'   and CurrentAssets/Cash/TableItem/@Value !='0'   and CurrentAssets/ShortTermInvestments/TableItem/@Value !='0'   "-->
					<tr>
						<td><b><xsl:value-of select='CurrentAssets/@Title'/></b></td>						
					</tr>					
</xsl:if>
					
					<xsl:if test="CurrentAssets/InventoryStock/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='CurrentAssets/InventoryStock/@Title'/></td>
						<xsl:for-each select="CurrentAssets/InventoryStock/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="CurrentAssets/Receivables/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='CurrentAssets/Receivables/@Title'/></td>
						<xsl:for-each select="CurrentAssets/Receivables/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="CurrentAssets/ClaimsTowardsAssociatedCompanies/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='CurrentAssets/ClaimsTowardsAssociatedCompanies/@Title'/></td>
						<xsl:for-each select="CurrentAssets/ClaimsTowardsAssociatedCompanies/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>
					
					<xsl:if test="CurrentAssets/ShortTermInvestments/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='CurrentAssets/ShortTermInvestments/@Title'/></td>
						<xsl:for-each select="CurrentAssets/ShortTermInvestments/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="CurrentAssets/Cash/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='CurrentAssets/Cash/@Title'/></td>
						<xsl:for-each select="CurrentAssets/Cash/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="CurrentAssets/OtherCurrentAssets/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='CurrentAssets/OtherCurrentAssets/@Title'/></td>
						<xsl:for-each select="CurrentAssets/OtherCurrentAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="CurrentAssets/CurrentAssetsTotal/TableItem/@Value !='0'  ">
					<tr>
						<td><b><xsl:value-of select='CurrentAssets/CurrentAssetsTotal/@Title'/></b></td>
						<xsl:for-each select="CurrentAssets/CurrentAssetsTotal/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					
					<!--<xsl:if test="DeferredTaxAssets/DeferredTaxAssets/TableItem/@Value !='0'  ">
					--><!-- assets total --><!--
					<tr>
						<td><b><xsl:value-of select='DeferredTaxAssets/DeferredTaxAssets/@Title'/></b></td>
						<xsl:for-each select="DeferredTaxAssets/DeferredTaxAssets/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
							
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:if>-->										
							
							
					<xsl:if test="AssetsTotal/AssetsTotal/TableItem/@Value !='0'  ">
					<!-- assets total -->
					<tr>
						<td><b><xsl:value-of select='AssetsTotal/AssetsTotal/@Title'/></b></td>
						<xsl:for-each select="AssetsTotal/AssetsTotal/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					</xsl:if>
					
					
					<!--xsl:if test="Equity/IssuedShareCapital/TableItem/@Value !='0' and Equity/ProfitAndLossAccount/TableItem/@Value !='0' and Equity/OtherEquity/TableItem/@Value !='0' and Equity/EquityTotal/TableItem/@Value !='0'   "-->
<xsl:if test="Equity/@Title !=''  ">
					<tr>
						<td><b><xsl:value-of select='Equity/@Title'/></b></td>			
					</tr>				
</xsl:if>
					
					<xsl:if test="Equity/IssuedShareCapital/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='Equity/IssuedShareCapital/@Title'/></td>
						<xsl:for-each select="Equity/IssuedShareCapital/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="Equity/ProfitAndLossAccount/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='Equity/ProfitAndLossAccount/@Title'/></td>
						<xsl:for-each select="Equity/ProfitAndLossAccount/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="Equity/OtherEquity/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='Equity/OtherEquity/@Title'/></td>
						<xsl:for-each select="Equity/OtherEquity/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>					
										
					<xsl:if test="Equity/EquityTotal/TableItem/@Value !='0'  ">
					<tr>
						<td><b><xsl:value-of select='Equity/EquityTotal/@Title'/></b></td>
						<xsl:for-each select="Equity/EquityTotal/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
				

					
					<!--xsl:if test="Equity/TaxLiabilities/TableItem/@Value !='0'  and Equity/OtherLiabilities/TableItem/@Value !='0'  "-->
					<tr>
						<td><b><xsl:value-of select='Equity/@TitleLiabilities'/></b></td>			
					</tr>
					
					<xsl:if test="Equity/TaxLiabilities/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='Equity/TaxLiabilities/@Title'/></td>
						<xsl:for-each select="Equity/TaxLiabilities/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="Equity/OtherLiabilities/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='Equity/OtherLiabilities/@Title'/></td>
						<xsl:for-each select="Equity/OtherLiabilities/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>		
					</xsl:if>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					
					
					<xsl:if test="LongTermDebts/LongTermDebts/TableItem/@Value !='0'  ">
					<tr>
						<td><b><xsl:value-of select='LongTermDebts/LongTermDebts/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/LongTermDebts/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>				
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					</xsl:if>
					
					<!--xsl:if test="ShortTermDebts/Overdraft_BankLoan/TableItem/@Value !='0'  and ShortTermDebts/Creditors/TableItem/@Value !='0'  and ShortTermDebts/Obligations/TableItem/@Value !='0'  and ShortTermDebts/OtherShortTermDebts/TableItem/@Value !='0'  and ShortTermDebts/ShortTermDebtsTotal/TableItem/@Value !='0'   "-->
<xsl:if test="ShortTermDebts/@Title !=''  ">
					<tr>
						<td><b><xsl:value-of select='ShortTermDebts/ShortTermDebts/@Title'/></b></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebts/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>					
</xsl:if>
					
					<xsl:if test="ShortTermDebts/Overdraft_BankLoan/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ShortTermDebts/Overdraft_BankLoan/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/Overdraft_BankLoan/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ShortTermDebts/Creditors/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ShortTermDebts/Creditors/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/Creditors/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					
					<xsl:if test="ShortTermDebts/Obligations/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ShortTermDebts/Obligations/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/Obligations/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					
					<xsl:if test="ShortTermDebts/OtherShortTermDebts/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ShortTermDebts/OtherShortTermDebts/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/OtherShortTermDebts/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ShortTermDebts/ShortTermDebtsTotal/TableItem/@Value !='0'  ">
					<tr>
						<td><b><xsl:value-of select='ShortTermDebts/ShortTermDebtsTotal/@Title'/></b></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					
						
					<xsl:if test="DebtEquityTotal/TotalDebt/TableItem/@Value !='0'  ">
					<tr>
						<td><b><xsl:value-of select='DebtEquityTotal/TotalDebt/@Title'/></b></td>
						<xsl:for-each select="DebtEquityTotal/TotalDebt/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:if>
					
					<xsl:if test="DebtEquityTotal/DebtEquityTotal/TableItem/@Value !='0'  ">
					<tr>
						<td><b><xsl:value-of select='DebtEquityTotal/DebtEquityTotal/@Title'/></b></td>
						<xsl:for-each select="DebtEquityTotal/DebtEquityTotal/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>
	

				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>