<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='RecentEnquiries' match='RecentEnquiries'>
		<tr><td>
		<table id='tblRecentEnquiries' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td><xsl:value-of select='tr/@Last6MonthsText'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='tr/@Last6MonthsValue'/></td>
			</tr>
			<tr>				
				<td><xsl:value-of select='tr/@Last3MonthsText'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='tr/@Last3MonthsValue'/></td>
			</tr>
			<tr>
				<td><xsl:value-of select='tr/@LastMonthText'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='tr/@LastMonthValue'/></td>
			</tr>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
</xsl:stylesheet>
