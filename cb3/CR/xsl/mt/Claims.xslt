<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name='Claims' match='Claims'>
    <tr>
      <td>
        <table id='tblClaims' class="list" width='100%' cellSpacing="0" cellPadding="0">
          <tr class='sectionheader'>
            <td colspan="4">
              <xsl:value-of select='@title'/>
            </td>
          </tr>
          <xsl:choose>
            <xsl:when test='NoInfo/@value'>
              <tr>
                <td>
                  <xsl:value-of select="NoInfo/@value"/>
                </td>
              </tr>
            </xsl:when>
          </xsl:choose>
          <xsl:if test="Info">
            <tr>
              <td>
                <b>
                  <xsl:value-of select="Info/@title"/>
                </b>
              </td>
              <td>
                <xsl:value-of select="Info/@value"/>
              </td>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <xsl:if test="Claimants">
            <tr>
              <td colspan="3">
                <b>
                  <xsl:value-of select="Claimants/@title"/>
                </b>
              </td>
            </tr>

       

            <xsl:for-each select="Claimants/Claimant">
              <tr>
                <td></td>
                <td>
                  <xsl:value-of select="."/>
                </td>
                <td>
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
            </xsl:for-each>
            <tr>
              <td colspan="3">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td align="right">
                <a href="javascript:show();">Click for Details</a>
              </td>
            </tr>
            <script language="javascript">
              function show()
              {
                var detail = document.getElementById('tblClaimsDetail');
                if( detail.style.display == '')
                detail.style.display = 'none';
                else
                detail.style.display = '';
              }
            </script>
            <tr>
              <td colspan="3">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
 
          </xsl:if>
          <xsl:if test="tr">
            <tr>
              <td colspan="4">
                <table id='tblClaimsDetail' class="list" width='100%' cellSpacing="0" cellPadding="0" style="display:none">

                  <xsl:for-each select="tr">
                    <tr>
                      <td rowspan="3"></td>
                      <td class="listhead" colspan="3">
                        <xsl:value-of select="@CaseNumberText"/>
                        <xsl:value-of select="@CaseNumber"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="dark-row">
                        <b>
                          <xsl:value-of select="@InfoSourceText"/>
                        </b>:<br></br><xsl:value-of select="@InfoSource"/>
                      </td>
                      <td class="dark-row">
                        <b>
                          <xsl:value-of select="@ClaimOwnerText"/>
                        </b>:<br></br><xsl:value-of select="@ClaimOwner"/>
                      </td>
                      <td class="dark-row">
                        <b>
                          <xsl:value-of select="@DateText"/>
                        </b>:<br></br><xsl:value-of select="@Date"/>
                      </td>
                    </tr>
                    <tr>
                      <xsl:element name="td">
                        <xsl:attribute name="class">dark-row</xsl:attribute>
                        <xsl:attribute name="title">
                          <xsl:value-of select="@ClaimTypeDesc"/>
                        </xsl:attribute>
                        <b>
                          <xsl:value-of select="@ClaimTypeText"/>
                        </b>:<br></br><xsl:value-of select="@ClaimType"/>
                      </xsl:element>

                      <td class="dark-row">
                        <xsl:choose>
                          <xsl:when test="@AmountText">
                            <b>
                              <xsl:value-of select="@AmountText"/>
                            </b>:<br></br><xsl:value-of select="@Amount"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:text>&#xA0;</xsl:text>
                          </xsl:otherwise>
                        </xsl:choose>
                      </td>

                      <td class="dark-row">
                        <b>
                          <xsl:value-of select="@CIReferenceText"/>
                        </b>:<br></br><xsl:value-of select="@CIReference"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
          </xsl:if>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </td>
    </tr>
    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
    <xsl:if test="OtherCourtInformation">
      <xsl:if test="count(../PendingCases) > 0">
        <tr>
          <td>
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        <xsl:if test="@title='Court information and defaulting debts (Amounts shown exclude legal interests and expenses)'">
          <tr class='sectionheader'>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              <b style="margin-left: -4px;">
                <!--<xsl:value-of select="OtherCourtInformation/@value"/>-->
                 Pending Court Cases
                <xsl:value-of select="count(../PendingCases/tr)"/>
              </b>
            </td>
          </tr>
          <tr>
            <td>
              <i>Pending Court Cases as from 2013 onwards – these do not bear any weight on any conclusions reached in Company Credit Reports. Zero value denotes that this information was not available </i>
            </td>
          </tr>
        </xsl:if>
      </xsl:if>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
