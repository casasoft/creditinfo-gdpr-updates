<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='DefinitionOfRatios' match='DefinitionOfRatios'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr>
						<td class="listhead" style="border-bottom-width:1pt;"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<xsl:for-each select="TableItem">
						<tr>
							<td width="100%"><b><xsl:value-of select='@Title'/>: </b><xsl:value-of select='@Value'/></td>
						</tr>
					</xsl:for-each>	
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>
