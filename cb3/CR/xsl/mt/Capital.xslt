<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="Capital" match="Capital">
		<tr>
			<td>
				<table id="tblCapital" width="100%" cellSpacing="0" cellPadding="0">
					<tr class="sectionheader">
						<td colspan="4">
							<xsl:value-of select="@title"/>
						</td>
					</tr>
					<xsl:element name="tr">
						<xsl:element name="td">
							<xsl:attribute name="height">10</xsl:attribute>
							<xsl:attribute name="colspan">4</xsl:attribute>
						</xsl:element>
					</xsl:element>
					<tr>
						<td>
						</td>
						<td>
							<b>
								<xsl:value-of select="NumberOfShares/@title"/>
							</b>
						</td>
						<td>
							<b>
								<xsl:value-of select="ShareCapital/@title"/>
							</b>
						</td>
						<td>
							<b>
								<xsl:value-of select="Nominal/@title"/>
							</b>
						</td>
					</tr>
					<tr>
						<td>
							<b>
								<xsl:value-of select="Authorized/@title"/>
							</b>
						</td>
						<td>
							<xsl:value-of select="Authorized/@value"/>
						</td>
						<td>
							<xsl:value-of select="AuthTimesNominal/@value" />
							
						<!--	<xsl:variable name="auth" select="Authorized/@value"/>
							<xsl:variable name="nom" select="Nominal/@value"/>
							
							<xsl:value-of select="$auth * $nom"/>
							<xsl:value-of select="translate($auth, ',', '.') * translate(substring-before($nom,' '), ',', '.')"/>-->
						</td>
						<td>
							<xsl:value-of select="Nominal/@value"/>
						</td>
					</tr>
					<tr>
						<td>
							<b>
								<xsl:value-of select="Issued/@title"/>
							</b>
						</td>
						<td>
							<xsl:value-of select="Issued/@value"/>
						</td>
						<td>
							<xsl:value-of select="IssuedTimesNominal/@value" />
							
							<!--<xsl:variable name="iss" select="Issued/@value"/>
							<xsl:variable name="nom" select="Nominal/@value"/>-->
							<!-- hsj 2005.10.25 -->
							<!-- skipta þarf "," út fyrir "." svo xslt geti notað töluna í útreikninga -->
							<!-- Nominal upphæðin samanstendur af tölu og gjaldmiðli og því skal klippa bilið og gjaldmiðilskóðan aftan af -->
							<!-- dæmi, '10,000 * 0,5 MTL' verður '10 * 0.5' -->
							<!--<xsl:value-of select="translate($iss,',', '.') * translate(substring-before($nom,' '), ',', '.')"/>-->
						</td>
						<td>
							<xsl:value-of select="Nominal/@value"/>
						</td>
					</tr>
					<xsl:element name="tr">
						<xsl:element name="td">
							<xsl:attribute name="height">10</xsl:attribute>
						</xsl:element>
					</xsl:element>
				</table>
			</td>
		</tr>
		<xsl:element name="tr">
			<xsl:element name="td">
				<xsl:text>&#xA0;</xsl:text>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->