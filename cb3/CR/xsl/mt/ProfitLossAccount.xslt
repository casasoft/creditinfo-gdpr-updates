<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='ProfitLossAccount' match='ProfitLossAccount'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240" class="header2"><b><xsl:value-of select='@title'/></b></td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right"  class="header2"><b><xsl:value-of select='@Year'/></b></td>
						</xsl:for-each>	
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>				
						
						
					<!-- Profit/Loss account -->
					<!-- header -->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='YearEnd/@Title'/></td>
						<xsl:for-each select="YearEnd/TableItem">						
							<td align="right" ><b><xsl:value-of select='@YearEnd'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='Months/@Title'/></td>
						<xsl:for-each select="Months/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Month'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='Currency/@Title'/></td>
						<xsl:for-each select="Currency/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Currency'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>				
					
					
					<xsl:if test="ProfitLossAccount/@TitleRevenue !=''  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>						
					<!-- Revenue-->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='ProfitLossAccount/@TitleRevenue'/></b></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/Turnover/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/Turnover/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/Turnover/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
	
					<xsl:if test="ProfitLossAccount/CostSales/TableItem/@Value !='0'  ">							
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/CostSales/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/CostSales/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="ProfitLossAccount/GrossProfit//TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/GrossProfit/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/GrossProfit/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>
										
					
					<xsl:if test="ProfitLossAccount/OtherOperatingRevenue/TableItem/@Value !='0'  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/OtherOperatingRevenue/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/OtherOperatingRevenue/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:if>
					
									
					<xsl:if test="ProfitLossAccount/@TitleCosts !=''  ">									
					<!--Costs -->
					
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='ProfitLossAccount/@TitleCosts'/></b></td>
					</tr>
					</xsl:if>

					<xsl:if test="ProfitLossAccount/StaffCost/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/StaffCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/StaffCost/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="ProfitLossAccount/AdministrativeOtherExpenses/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/AdministrativeOtherExpenses/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/AdministrativeOtherExpenses/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="ProfitLossAccount/DistributionCost//TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/DistributionCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/DistributionCost/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="ProfitLossAccount/DepricationFixedAssets/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/DepricationFixedAssets/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/DepricationFixedAssets/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="ProfitLossAccount/TotalCosts/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/TotalCosts/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/TotalCosts/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/OperationalProfit/TableItem/@Value !='0'  ">					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='ProfitLossAccount/OperationalProfit/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/OperationalProfit/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:if>
					
					
					
					
					<xsl:if test="ProfitLossAccount/FinancialIncome/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/FinancialIncome/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/FinancialIncome/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="ProfitLossAccount/FinancialCost/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/FinancialCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/FinancialCost/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/OtherFinancialItems/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/OtherFinancialItems/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/OtherFinancialItems/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="ProfitLossAccount/NetFinancialItems/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/NetFinancialItems/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/NetFinancialItems/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>

					<xsl:if test="ProfitLossAccount/IncomeFromOperationsBeforeTax/TableItem/@Value !='0'  ">										
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='ProfitLossAccount/IncomeFromOperationsBeforeTax/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/IncomeFromOperationsBeforeTax/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="ProfitLossAccount/Tax/TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/Tax/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/Tax/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/IncomeFromOperationsAfterTax//TableItem/@Value !='0'  ">
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='ProfitLossAccount/IncomeFromOperationsAfterTax/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/IncomeFromOperationsAfterTax/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>
				
					<xsl:if test="ProfitLossAccount/ExtraOrdinaryIncomeCost/TableItem/@Value !='0'  ">	
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/ExtraOrdinaryIncomeCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/ExtraOrdinaryIncomeCost/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>

					<xsl:if test="ProfitLossAccount/IncomeFromAssociatedCompanies/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='ProfitLossAccount/IncomeFromAssociatedCompanies/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/IncomeFromAssociatedCompanies/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>

					<xsl:if test="ProfitLossAccount/ProfitOrLoss/TableItem/@Value !='0'  ">					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='ProfitLossAccount/ProfitOrLoss/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/ProfitOrLoss/TableItem">						
							<td align="right" ><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					
					
					
					<!--<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/Income/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/Income/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/OperationalCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/OperationalCost/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/Deprication/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/Deprication/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/NetResult/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/NetResult/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="ProfitLossAccount/NetResult/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					-->
				
					<!-- Cashflow-->
					<!--<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='CashFlow/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/NetResult/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CashFlow/CapitalFromOperations/@Title'/></td>
						<xsl:for-each select="CashFlow/CapitalFromOperations/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CashFlow/CashFromOperations/@Title'/></td>
						<xsl:for-each select="CashFlow/CashFromOperations/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CashFlow/IncrDescInCash/@Title'/></td>
						<xsl:for-each select="CashFlow/IncrDescInCash/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->		
															
					<!-- Ratios
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Ratios/@Title'/></b></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/ProfitMargin/@Title'/></td>
						<xsl:for-each select="Ratios/ProfitMargin/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/CurrentRatio/@Title'/></td>
						<xsl:for-each select="Ratios/CurrentRatio/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/QuickRatio/@Title'/></td>
						<xsl:for-each select="Ratios/QuickRatio/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/EquityRatio/@Title'/></td>
						<xsl:for-each select="Ratios/EquityRatio/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/DebtRatio/@Title'/></td>
						<xsl:for-each select="Ratios/DebtRatio/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/CollectionTime/@Title'/></td>
						<xsl:for-each select="Ratios/CollectionTime/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/StockTurnover/@Title'/></td>
						<xsl:for-each select="Ratios/StockTurnover/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					
					
					<xsl:if test="Auditted/Auditted/TableItem/@Value !='0'  ">	
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
									
					
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><b><xsl:value-of select='Auditted/@Title'/></b></td>
						<xsl:for-each select="Auditted/Auditted/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	.
					</xsl:if>
					-->						
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>