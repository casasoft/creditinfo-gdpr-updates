<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name='BalanceSheetAssets' match='BalanceSheetAssets'>
    <tr>
      <td>
        <table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
          <tr>
            <td class="listhead" width="50%" style="border-bottom-width:1pt;">
              <b>
                <xsl:value-of select='@title'/>
              </b>
            </td>
            <xsl:for-each select="Years/TableItem">
              <td align="right" width="15%" class="listhead" style="border-bottom-width:1pt;">
                <b>
                  <xsl:value-of select='@Year'/>
                </b>
              </td>
            </xsl:for-each>
            <td width="15">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="15">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

          <!-- >> Intangible Assets Section -->
          <xsl:if test="IntangibleAssetsSubTotal/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Intangible Assets
                </b>
              </td>
            </tr>
          </xsl:if>

          <!-- >> Intangible Assets Section -->
          <xsl:if test="Goodwill/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Goodwill/@Title'/>
              </td>
              <xsl:for-each select="Goodwill/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherAssets/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OtherAssets/@Title'/>
              </td>
              <xsl:for-each select="OtherAssets/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IntangibleAssetsSubTotal/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='IntangibleAssetsSubTotal/@Title'/>
              </td>
              <xsl:for-each select="IntangibleAssetsSubTotal/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Intangible Assets Section -->

          <xsl:if test="FixedAssetsSubTotal/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Fixed Assets - Tangible
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="PropertyPlantEquipment/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='PropertyPlantEquipment/@Title'/>
              </td>
              <xsl:for-each select="PropertyPlantEquipment/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TransportEquipment/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='TransportEquipment/@Title'/>
              </td>
              <xsl:for-each select="TransportEquipment/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="FurnitureFixtures/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='FurnitureFixtures/@Title'/>
              </td>
              <xsl:for-each select="FurnitureFixtures/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="PropertyImprovements/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='PropertyImprovements/@Title'/>
              </td>
              <xsl:for-each select="PropertyImprovements/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="InvestmentProperty/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='InvestmentProperty/@Title'/>
              </td>
              <xsl:for-each select="InvestmentProperty/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherTangibleAssets/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OtherTangibleAssets/@Title'/>
              </td>
              <xsl:for-each select="OtherTangibleAssets/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="FixedAssetsSubTotal/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='FixedAssetsSubTotal/@Title'/>
              </td>
              <xsl:for-each select="FixedAssetsSubTotal/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Tangible Assets Section -->

          <!-- >> Financial Assets Section -->
          <xsl:if test="TotalFixedAssets/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Financial Assets
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI/@Title !=''  ">
            <tr>
              <td>
                <b>
                  Long-Term Investments
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="LTI_SOE/@Title !=''  ">
            <tr>
              <td>
                <b>
                  > Shares and Other Equity
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOE_CIS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='LTI_SOE_CIS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOE_CIS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOE_QS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='LTI_SOE_QS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOE_QS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOE_UQS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='LTI_SOE_UQS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOE_UQS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOE/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='LTI_SOE/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOE/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS/@Title !=''  ">
            <tr>
              <td>
                <b>
                  > Securities Other Than Shares
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_TB/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='LTI_SOTS_TB/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_TB/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_MGS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='LTI_SOTS_MGS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_MGS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_BCOD/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='LTI_SOTS_BCOD/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_BCOD/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_FD/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='LTI_SOTS_FD/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_FD/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_OS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='LTI_SOTS_OS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_OS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='LTI_SOTS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='LTI/@Title'/>
              </td>
              <xsl:for-each select="LTI/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Available for Sale Financial Assets
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOE/@Title !=''  ">
            <tr>
              <td>
                <b>
                  > Shares and Other Equity
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="AFSFA_SOE_CIS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='AFSFA_SOE_CIS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOE_CIS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOE_QS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='AFSFA_SOE_QS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOE_QS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOE_UQS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='AFSFA_SOE_UQS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOE_UQS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOE/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='AFSFA_SOE/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOE/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS/@Title !=''  ">
            <tr>
              <td>
                <b>
                  > Securities Other Than Shares
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="AFSFA_SOTS_TB/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='AFSFA_SOTS_TB/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_TB/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS_MGS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='AFSFA_SOTS_MGS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_MGS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS_BCOD/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='AFSFA_SOTS_BCOD/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_BCOD/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS_FD/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='AFSFA_SOTS_FD/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_FD/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS_OS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='AFSFA_SOTS_OS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_OS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='AFSFA_SOTS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="AFSFA/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='AFSFA/@Title'/>
              </td>
              <xsl:for-each select="AFSFA/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="FinancialAssetsSubTotal/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='FinancialAssetsSubTotal/@Title'/>
              </td>
              <xsl:for-each select="FinancialAssetsSubTotal/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalFixedAssets/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='TotalFixedAssets/@Title'/>
              </td>
              <xsl:for-each select="TotalFixedAssets/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Financial Assets Section -->

          <!-- >> Current Assets Section -->
          <xsl:if test="CA/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Current Assets
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Inventory/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CA_Inventory/@Title'/>
              </td>
              <xsl:for-each select="CA_Inventory/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Raw_Materials_WIP/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CA_Raw_Materials_WIP/@Title'/>
              </td>
              <xsl:for-each select="CA_Raw_Materials_WIP/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Goods_Held_Resale/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CA_Goods_Held_Resale/@Title'/>
              </td>
              <xsl:for-each select="CA_Goods_Held_Resale/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Trade_Receivables/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CA_Trade_Receivables/@Title'/>
              </td>
              <xsl:for-each select="CA_Trade_Receivables/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Other_Receivables/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CA_Other_Receivables/@Title'/>
              </td>
              <xsl:for-each select="CA_Other_Receivables/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Prepayments_Accrued_Income/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CA_Prepayments_Accrued_Income/@Title'/>
              </td>
              <xsl:for-each select="CA_Prepayments_Accrued_Income/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Property_Held_Resale/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CA_Property_Held_Resale/@Title'/>
              </td>
              <xsl:for-each select="CA_Property_Held_Resale/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='CA/@Title'/>
              </td>
              <xsl:for-each select="CA/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Current Assets Section -->

          <!-- >> Borrowings Current Section -->
          <xsl:if test="Assets_BC_DW12Ms/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
              <tr>
                <td>
                  <b>
                    Borrowings - Current - Due Within 12 Months
                  </b>
                </td>
              </tr>
          </xsl:if>

          <xsl:if test="Assets_BC_DW12Ms_LocalNonFinancial/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Assets_BC_DW12Ms_LocalNonFinancial/@Title'/>
              </td>
              <xsl:for-each select="Assets_BC_DW12Ms_LocalNonFinancial/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BC_DW12Ms_Residents/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Assets_BC_DW12Ms_Residents/@Title'/>
              </td>
              <xsl:for-each select="Assets_BC_DW12Ms_Residents/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BC_DW12Ms_NonResidents/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Assets_BC_DW12Ms_NonResidents/@Title'/>
              </td>
              <xsl:for-each select="Assets_BC_DW12Ms_NonResidents/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BC_DW12Ms/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='Assets_BC_DW12Ms/@Title'/>
              </td>
              <xsl:for-each select="Assets_BC_DW12Ms/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Borrowings Current Section -->

          <!-- >> Cash Section -->
          <xsl:if test="TotalCash/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Cash
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Currency_Cash_in_Hand/@Title !=''  ">
            <tr>
              <td>
                <b>
                  Currency - Cash in Hand
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Currency_Cash_in_Hand_Euro/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Cash_Currency_Cash_in_Hand_Euro/@Title'/>
              </td>
              <xsl:for-each select="Cash_Currency_Cash_in_Hand_Euro/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Currency_Cash_in_Hand_Foreign/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Cash_Currency_Cash_in_Hand_Foreign/@Title'/>
              </td>
              <xsl:for-each select="Cash_Currency_Cash_in_Hand_Foreign/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Currency_Cash_in_Hand/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='Cash_Currency_Cash_in_Hand/@Title'/>
              </td>
              <xsl:for-each select="Cash_Currency_Cash_in_Hand/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Bank_Deposits/@Title !=''  ">
            <tr>
              <td>
                <b>
                  Bank Deposits
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="Cash_Bank_Deposits_Current/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Cash_Bank_Deposits_Current/@Title'/>
              </td>
              <xsl:for-each select="Cash_Bank_Deposits_Current/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Bank_Deposits_Savings/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Cash_Bank_Deposits_Savings/@Title'/>
              </td>
              <xsl:for-each select="Cash_Bank_Deposits_Savings/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Bank_Deposits_Time/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Cash_Bank_Deposits_Time/@Title'/>
              </td>
              <xsl:for-each select="Cash_Bank_Deposits_Time/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Bank_Deposits/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='Cash_Bank_Deposits/@Title'/>
              </td>
              <xsl:for-each select="Cash_Bank_Deposits/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalCash/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='TotalCash/@Title'/>
              </td>
              <xsl:for-each select="TotalCash/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Cash Section -->

          <!-- >> Borrowings Non-Current Section -->
          <xsl:if test="Assets_BNC_O12Ms/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
              <tr>
                <td>
                  <b>
                    Borrowings - Non-Current - Over 12 Months
                  </b>
                </td>
              </tr>
          </xsl:if>

          <xsl:if test="Assets_BNC_O12Ms_LocalNonFinancial/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Assets_BNC_O12Ms_LocalNonFinancial/@Title'/>
              </td>
              <xsl:for-each select="Assets_BNC_O12Ms_LocalNonFinancial/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BNC_O12Ms_Residents/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Assets_BNC_O12Ms_Residents/@Title'/>
              </td>
              <xsl:for-each select="Assets_BNC_O12Ms_Residents/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BNC_O12Ms_NonResidents/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='Assets_BNC_O12Ms_NonResidents/@Title'/>
              </td>
              <xsl:for-each select="Assets_BNC_O12Ms_NonResidents/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BNC_O12Ms/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='Assets_BNC_O12Ms/@Title'/>
              </td>
              <xsl:for-each select="Assets_BNC_O12Ms/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Borrowings Non-Current Section -->

          <!-- >> Total Assets Section -->
          <xsl:if test="TotalAssets/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="ShortTermInvestments/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='ShortTermInvestments/@Title'/>
              </td>
              <xsl:for-each select="ShortTermInvestments/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Current_Tax_Asset/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='Current_Tax_Asset/@Title'/>
              </td>
              <xsl:for-each select="Current_Tax_Asset/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherCurrentAssets/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='OtherCurrentAssets/@Title'/>
              </td>
              <xsl:for-each select="OtherCurrentAssets/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalCurrentAssets/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='TotalCurrentAssets/@Title'/>
              </td>
              <xsl:for-each select="TotalCurrentAssets/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="DeferredTaxation/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='DeferredTaxation/@Title'/>
              </td>
              <xsl:for-each select="DeferredTaxation/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalAssets/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='TotalAssets/@Title'/>
              </td>
              <xsl:for-each select="TotalAssets/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Total Assets Section -->

          <tr>
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colSpan="2">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>