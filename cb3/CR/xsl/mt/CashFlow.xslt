<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='CashFlow' match='CashFlow'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">		
										
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240" class="header2"><b><xsl:value-of select='@title'/></b></td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right"  class="header2"><b><xsl:value-of select='@Year'/></b></td>
						</xsl:for-each>	
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>				
										
					<xsl:if test="CashFlow/OperatingProfitBeforeWorkingCapitalChanges/TableItem/@Value !='0'  ">													
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>						
						<td width="240"><xsl:value-of select='CashFlow/OperatingProfitBeforeWorkingCapitalChanges/@Title'/></td>
						<xsl:for-each select="CashFlow/OperatingProfitBeforeWorkingCapitalChanges/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>

					<xsl:if test="CashFlow/CashFromOperations/TableItem/@Value !='0'  ">											
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CashFlow/CashFromOperations/@Title'/></td>
						<xsl:for-each select="CashFlow/CashFromOperations/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="CashFlow/CapitalFromOperations/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CashFlow/CapitalFromOperations/@Title'/></td>
						<xsl:for-each select="CashFlow/CapitalFromOperations/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>				
					

					<xsl:if test="CashFlow/CashInvesting/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CashFlow/CashInvesting/@Title'/></td>
						<xsl:for-each select="CashFlow/CashInvesting/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	
					</xsl:if>

					<xsl:if test="CashFlow/CashFinancing/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CashFlow/CashFinancing/@Title'/></td>
						<xsl:for-each select="CashFlow/CashFinancing/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>

					<xsl:if test="CashFlow/IncrDecrInCash/TableItem/@Value !='0'  ">					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="240"><xsl:value-of select='CashFlow/IncrDecrInCash/@Title'/></td>
						<xsl:for-each select="CashFlow/IncrDecrInCash/TableItem">						
							<td align="right" ><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>								
										
																
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>