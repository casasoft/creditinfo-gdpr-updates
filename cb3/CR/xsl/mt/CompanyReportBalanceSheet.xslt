<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='BalanceSheet' match='BalanceSheet'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2"><b><xsl:value-of select='@title'/></b></td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right" class="header2"><b><xsl:value-of select='@Year'/></b></td>
						</xsl:for-each>	
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="Months/TableItem">						
							<td align="right"><b><xsl:value-of select='@Month'/></b></td>
						</xsl:for-each>
					</tr>
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="Currency/TableItem">						
							<td align="right"><b><xsl:value-of select='@Currency'/></b></td>
						</xsl:for-each>
					</tr>
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='FixedAssets/FixedAssetsTotal/@Title'/></b></td>
						<xsl:for-each select="FixedAssets/FixedAssetsTotal/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
										
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='CurrentAssets/CurrentAssetsTotal/@Title'/></b></td>
						<xsl:for-each select="CurrentAssets/CurrentAssetsTotal/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					<!-- deferred tax assets -->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='DeferredTaxAssets/DeferredTaxAssets/@Title'/></b></td>
						<xsl:for-each select="DeferredTaxAssets/DeferredTaxAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					<!-- assets total -->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='AssetsTotal/AssetsTotal/@Title'/></b></td>
						<xsl:for-each select="AssetsTotal/AssetsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
						<!-- Equity -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Equity/@Title'/></b></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/IssuedShareCapital/@Title'/></td>
						<xsl:for-each select="Equity/IssuedShareCapital/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/ProfitAndLossAccount/@Title'/></td>
						<xsl:for-each select="Equity/ProfitAndLossAccount/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/OtherEquity/@Title'/></td>
						<xsl:for-each select="Equity/OtherEquity/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>					
				<!--	<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/OtherEquityCalculated/@Title'/></td>
						<xsl:for-each select="Equity/OtherEquityCalculated/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Equity/EquityTotal/@Title'/></b></td>
						<xsl:for-each select="Equity/EquityTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<!--<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/MinorotyHoldingsInEquity/@Title'/></td>
						<xsl:for-each select="Equity/MinorotyHoldingsInEquity/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Equity/TaxLiabilities/@Title'/></b></td>
						<xsl:for-each select="Equity/TaxLiabilities/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Equity/OtherLiabilities/@Title'/></b></td>
						<xsl:for-each select="Equity/OtherLiabilities/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<!--<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='LongTermDebts/TotalLiabilities/@Title'/></td>
						<xsl:for-each select="LongTermDebts/TotalLiabilities/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='LongTermDebts/LongTermDebts/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/LongTermDebts/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='ShortTermDebts/ShortTermDebtsTotal/@Title'/></b></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='DebtEquityTotal/DebtEquityTotal/@Title'/></b></td>
						<xsl:for-each select="DebtEquityTotal/DebtEquityTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					
					
					
					
					
					
					
					
					
					
					<!-- Current assets -->
					<!--<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CurrentAssets/CurrentAssetsTotal/@Title'/></td>
						<xsl:for-each select="CurrentAssets/CurrentAssetsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
					
				
					<!-- Fixed assets -->
			
				<!--	<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='FixedAssets/FixedAssetsTotal/@Title'/></td>
						<xsl:for-each select="FixedAssets/FixedAssetsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
					
					<!-- assets total 
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='AssetsTotal/AssetsTotal/@Title'/></b></td>
						<xsl:for-each select="AssetsTotal/AssetsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
							-->		
					<!-- short term debts 
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ShortTermDebts/ShortTermDebtsTotal/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>-->
					

					<!-- long term debts 
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='LongTermDebts/LongTermDebts/@Title'/></td>
						<xsl:for-each select="LongTermDebts/LongTermDebts/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/IssuedShareCapital/@Title'/></td>
						<xsl:for-each select="Equity/IssuedShareCapital/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					-->
					<!-- Other equity calculated	
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/OtherEquity/@Title'/></td>
						<xsl:for-each select="Equity/OtherEquity/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					 -->
					<!-- Equity 
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Equity/EquityTotal/@Title'/></b></td>
						<xsl:for-each select="Equity/EquityTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>	-->			

					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>