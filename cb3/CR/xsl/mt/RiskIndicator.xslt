﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:template name='RiskIndicator' match='RiskIndicator'>
    <xsl:if test='count(//RiskIndicator/child::*) != 0'>
    <tr>
      <td>
        <table id="tblRiskIndikator" width="100%" cellSpacing="0" cellPadding="0">
          <tr class="sectionheader">
            <td colspan="4">
              <xsl:value-of select="@title"/>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <br/>
              </td>
          </tr>
          <tr>
            <td width="40%" valign="top">
              <table cellpadding="0" cellspacing="0"  width="90%" border="0" id="tbFinancialStrengthIndicator">
                <tr >
                  <td align="center" width="80%" class="tbFinancialStrengthIndicatorTitle">
                    <xsl:value-of select="FinancialStrengthIndicator/@title"/>
                  </td>
                  <!--td rowspan="3" width="20%" align="center" style="border: 0px; font-style:italic ">
                    Scale
                  </td--> 
                </tr>
                <tr>
                  <td align="center"  class="tbFinancialStrengthIndicatorValue">
                    <xsl:value-of select="FinancialStrengthIndicator"/>
                  </td>
                </tr>
                <tr>
                  <td class="trFinancialStrengthDescription">
                    <!--<xsl:value-of select="Risk/FinancialStrengthDescription"/>-->
                    <!--H is the lowest in the range<BR/>
                    5A is the highest in the range-->
                    H is the lowest in the range (0 - 10000)<BR/>
                    5A is the highest in the range (50000000 +)

                  </td>
                </tr>
              </table>
            </td >
            <xsl:if test="Risk/@RiskValue != ''">
            
            <td  width="20%" valign="top" align="left"  style="display:none">
              <table  cellpadding="0" cellspacing="0" border="0"  id="tbRisk">
                <tr>
                  <td align="center" class="tbRiskTitle">
                    <xsl:value-of select="Risk/@title"/>
                  </td>
                </tr>
                <xsl:if test="Risk/@RiskValue != ''">
                  <tr>
                    <xsl:variable name="Color" select="Risk/@color"></xsl:variable>
                    <xsl:element name="td">
                      <xsl:attribute name="align">
                        <xsl:text>center</xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name="class">
                        <xsl:text>tbRiskValue</xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name="style">
                        <xsl:text>background-color=</xsl:text>
                        <xsl:value-of select="$Color"/>
                      </xsl:attribute>
                      <xsl:value-of select="Risk/@RiskValue"/>
                    </xsl:element>
                  </tr>
                  <tr>
                    <td class="trRiskDesclaimer">
                      <!--1 - Minimal<br/>
                      2 - Above Average<br/>
                      3 - Below Average<br/>
                      4 - Caution<br/>-->


                      1 - Low<br/> 
                      2 - Above Average<br/> 
                      3 - Below Average<br/> 
                      4 - Caution<br/> 
                      NC - Not Classified

                    </td>
                  </tr>
                </xsl:if>
              </table>
              
            </td>
            </xsl:if>

            <td  width="40%"  valign="top">
              <table cellpadding="0" cellspacing="0" id="tbCreditLimit">
                <tr>
                  <td align="center"  class="tdCreditLimitTitle">
                    <xsl:value-of select="CreditLimit/@title"/>
                  </td>
                </tr>
                <tr>
                  <td align="center"  class="tdCreditLimitValue">
                    <xsl:value-of select="CreditLimit"/>
                  </td>
                </tr>
                <tr>
                  <td class="trCreditLimitDesclaimer">
                      <xsl:value-of select="CreditLimit/@Disclaimer"/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
