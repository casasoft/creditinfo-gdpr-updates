<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name='Ratios' match='Ratios'>
    <tr>
      <td>
        <table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
          <tr>
            <td class="listhead" width="50%" style="border-bottom-width:1pt;">
              <b>
                <xsl:value-of select='@title'/>
              </b>
            </td>
            <xsl:for-each select="Years/TableItem">
              <td align="right" width="15%" class="listhead" style="border-bottom-width:1pt;">
                <b>
                  <xsl:value-of select='@Year'/>
                </b>
              </td>
            </xsl:for-each>
            <td width="15">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="15">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

          <!-- >> I. Profitability Section -->
          <xsl:if test="ROE/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>

            <tr>
              <td>
                <b>
                  I. PROFITABILITY
                </b>
              </td>
            </tr>
            
            <tr>
              <td>
                <xsl:value-of select='ROE/@Title'/>
              </td>
              <xsl:for-each select="ROE/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select='ROA/@Title'/>
              </td>
              <xsl:for-each select="ROA/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select='ROS/@Title'/>
              </td>
              <xsl:for-each select="ROS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select='ROCE/@Title'/>
              </td>
              <xsl:for-each select="ROCE/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of I. Profitability Section -->

          <!-- >> II. Equity Section -->
          <xsl:if test="DebtToEquityRatio/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>

            <tr>
              <td>
                <b>
                  II. EQUITY
                </b>
              </td>
            </tr>
            
            <tr>
              <td>
                <xsl:value-of select='DebtToEquityRatio/@Title'/>
              </td>
              <xsl:for-each select="DebtToEquityRatio/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of II. Equity Section -->

          <!-- >> III. Solvency Section -->
          <xsl:if test="CurrentRatio/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>

            <tr>
              <td>
                <b>
                  III. SOLVENCY
                </b>
              </td>
            </tr>
            
            <tr>
              <td>
                <xsl:value-of select='CurrentRatio/@Title'/>
              </td>
              <xsl:for-each select="CurrentRatio/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select='QuickRatio/@Title'/>
              </td>
              <xsl:for-each select="QuickRatio/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select='CurrentLiabilitiesToNetWorth/@Title'/>
              </td>
              <xsl:for-each select="CurrentLiabilitiesToNetWorth/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select='TotalLiabilitiesToNetWorth/@Title'/>
              </td>
              <xsl:for-each select="TotalLiabilitiesToNetWorth/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select='FixedAssetsToNetWorth/@Title'/>
              </td>
              <xsl:for-each select="FixedAssetsToNetWorth/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select='InterestCoverRatio/@Title'/>
              </td>
              <xsl:for-each select="InterestCoverRatio/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of III. Solvency Section -->
          
          <!-- >> IV. Turnover Section -->
          <xsl:if test="AverageCollectionPeriod/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>

            <tr>
              <td>
                <b>
                  IV. TURNOVER
                </b>
              </td>
            </tr>
            
            <tr>
              <td>
                <xsl:value-of select='AverageCollectionPeriod/@Title'/>
              </td>
              <xsl:for-each select="AverageCollectionPeriod/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of IV. Turnover Section -->

          <tr>
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colSpan="2">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>


        </table>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>