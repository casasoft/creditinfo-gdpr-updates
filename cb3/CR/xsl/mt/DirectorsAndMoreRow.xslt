<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name='DirectorsAndMoreRow' match='DirectorsAndMoreRow'>
    <TR>
      <TD valign="top">
        <xsl:value-of select="@Name"/>
      </TD>
      <td valign="top">
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </td>
      <TD width='15%' valign="top">
        ID#:
        <xsl:choose>
          <xsl:when test='@PassNo'>
            <xsl:value-of select="@PassNo"/>
            <BR></BR>
            <xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?TAFReport=true&amp;reqNationalID=</xsl:text>
            <xsl:value-of select="@PassNo"/>
            <xsl:text disable-output-escaping="yes">'&gt;TAF-report&lt;/a&gt;</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test='@ID'>
                <xsl:value-of select="@ID"/>
                <BR></BR>
                <xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?TAFReport=true&amp;reqNationalID=</xsl:text>
                <xsl:value-of select="@ID"/>
                <xsl:text disable-output-escaping="yes">'&gt;TAF-report&lt;/a&gt;</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                N/A
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </TD>
      <td valign="top">
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </td>
      <TD valign="top">
        <xsl:value-of select="@Address1"/>
        <br></br>
        <xsl:value-of select="@Address2"/>
      </TD>
      <td valign="top">
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </td>
      <TD valign="top">
        <xsl:value-of select="@City"/>
        <br></br>
        <xsl:value-of select="@Country"/>
      </TD>
      <TD valign="top" align="right">
        <xsl:value-of select="@TotalEquity"/>
      </TD>
    </TR>
  </xsl:template>
</xsl:stylesheet>
