<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Capital' match='Capital'>
		<tr>
			<td>			
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
					
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2" colspan="7"><b><xsl:value-of select='@title'/></b></td>
					</tr>		
			<tr>
				<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td>
					<b><xsl:value-of select='Authorized/@title'/>: </b><xsl:value-of select='Authorized/@value'/>					
					<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text><b><xsl:value-of select='Issued/@title'/>: </b><xsl:value-of select='Issued/@value'/>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text><b><xsl:value-of select='PaidUp/@title'/>: </b><xsl:value-of select='PaidUp/@value'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='CurrencyCode/@value'/>
				</td>
			</tr>
			<tr>
				<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
				<td>
					<b><xsl:value-of select='Nominal/@title'/>: </b><xsl:value-of select='Nominal/@value'/>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
					<xsl:if test='ShareClass'>	
						<b><xsl:value-of select='ShareClass/@title'/>: </b><xsl:value-of select='ShareClass/@value'/>
					</xsl:if>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text>
					<b><xsl:value-of select='ShareDescription/@title'/>: </b><xsl:value-of select='ShareDescription/@value'/>
				</td>
			</tr>
				<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
			
		</table>	
	</td>		
	</tr>
	</xsl:template>
</xsl:stylesheet>