<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Secretary' match='Secretary'>
    <xsl:if test="count(tr) > 0">
      <tr>
        <td colspan="8">
          <b>
            <xsl:value-of select='@title'/>
          </b>
        </td>
      </tr>
      <xsl:for-each select="tr">
        <xsl:call-template name="DirectorsAndMoreRow"/>
      </xsl:for-each>
      <tr>
        <td>
          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
        </td>
      </tr>
    </xsl:if>
	</xsl:template>
</xsl:stylesheet>
