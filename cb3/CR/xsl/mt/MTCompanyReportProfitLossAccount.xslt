<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='ProfitLossAccount' match='ProfitLossAccount'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">					
					
					<tr>
						<td class="listhead" width="50%" style="border-bottom-width:1pt;"><b><xsl:value-of select='@title'/></b></td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right" width="15%" class="listhead" style="border-bottom-width:1pt;"><b><xsl:value-of select='@Year'/></b></td>
						</xsl:for-each>	
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<!-- Profit/Loss account -->
					
					<xsl:if test="ProfitLossAccount/@TitleRevenue !=''  ">
					<tr>
						<td><b><xsl:value-of select='ProfitLossAccount/@TitleRevenue'/></b></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/Turnover/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/Turnover/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/Turnover/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/CostSales/TableItem/@Value !='0'  ">			
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/CostSales/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/CostSales/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					
					<xsl:if test="ProfitLossAccount/GrossProfit//TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/GrossProfit/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/GrossProfit/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
															
					<xsl:if test="ProfitLossAccount/OtherOperatingRevenue/TableItem/@Value !='0'  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/OtherOperatingRevenue/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/OtherOperatingRevenue/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/@TitleCosts !=''  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>					
					<tr>
						<td><b><xsl:value-of select='ProfitLossAccount/@TitleCosts'/></b></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/StaffCost/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/StaffCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/StaffCost/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/AdministrativeOtherExpenses/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/AdministrativeOtherExpenses/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/AdministrativeOtherExpenses/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/DistributionCost//TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/DistributionCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/DistributionCost/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/DepricationFixedAssets/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/DepricationFixedAssets/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/DepricationFixedAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/TotalCosts/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/TotalCosts/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/TotalCosts/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/OperationalProfit/TableItem/@Value !='0'  ">					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					
					
					<tr>
						<td><b><xsl:value-of select='ProfitLossAccount/OperationalProfit/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/OperationalProfit/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>										
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>					
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/FinancialIncome/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/FinancialIncome/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/FinancialIncome/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/FinancialCost/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/FinancialCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/FinancialCost/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/OtherFinancialItems/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/OtherFinancialItems/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/OtherFinancialItems/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/NetFinancialItems/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/NetFinancialItems/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/NetFinancialItems/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/IncomeFromOperationsBeforeTax/TableItem/@Value !='0'  ">
					<tr>
						<td><b><xsl:value-of select='ProfitLossAccount/IncomeFromOperationsBeforeTax/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/IncomeFromOperationsBeforeTax/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/Tax/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/Tax/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/Tax/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/IncomeFromOperationsAfterTax//TableItem/@Value !='0'  ">
					<tr>
						<td><b><xsl:value-of select='ProfitLossAccount/IncomeFromOperationsAfterTax/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/IncomeFromOperationsAfterTax/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/ExtraOrdinaryIncomeCost/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/ExtraOrdinaryIncomeCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/ExtraOrdinaryIncomeCost/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/IncomeFromAssociatedCompanies/TableItem/@Value !='0'  ">
					<tr>
						<td><xsl:value-of select='ProfitLossAccount/IncomeFromAssociatedCompanies/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/IncomeFromAssociatedCompanies/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					</xsl:if>
					
					<xsl:if test="ProfitLossAccount/ProfitOrLoss/TableItem/@Value !='0'  ">
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>					
					
					<tr>
						<td><b><xsl:value-of select='ProfitLossAccount/ProfitOrLoss/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/ProfitOrLoss/TableItem">						
							<td align="right"><b><xsl:value-of select='@Value'/></b></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>					
					</xsl:if>									
					
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>