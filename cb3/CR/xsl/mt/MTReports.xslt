<?xml version="1.0" encoding="iso-8859-1" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

  <xsl:import href='CompanySummary.xslt'/>
  <xsl:import href='RiskIndicator.xslt'/>
  <xsl:import href='BasicClaims.xslt'/>	
	<xsl:import href='Capital.xslt'/>	
	<xsl:import href='Claims.xslt'/>	
	<xsl:import href='CompanyInvolvements.xslt'/>	
	<xsl:import href='CompanyState.xslt'/>	
	<xsl:import href='Directors.xslt'/>	
	<xsl:import href='DirectorsAndMore.xslt'/>	
	<xsl:import href='DirectorsAndMoreRow.xslt'/>	
	<xsl:import href='HasCourtOrders.xslt'/>	
	<xsl:import href='LegalRepresentative.xslt'/>
  <xsl:import href='JudicialRepresentative.xslt'/>
  <xsl:import href='OtherInformation.xslt'/>	
	<xsl:import href='PendingCases.xslt'/>	
	<xsl:import href='RecentEnquiries.xslt'/>	
	<xsl:import href='RegistrationForm.xslt'/>	
	<xsl:import href='Secretary.xslt'/>	
	<xsl:import href='Shareholders.xslt'/>	
	<xsl:import href='MTCompanyReportBalanceSheet.xslt'/>	
	<xsl:import href='MTCompanyReportProfitLossAccount.xslt'/>
	<xsl:import href='MTHeadFSI.xslt'/>
	<xsl:import href='MTCashFlow.xslt'/>
	<xsl:import href='MTRatios.xslt'/>
	<xsl:import href='MTDefinitionOfRatios.xslt'/>
	
	<xsl:output method='html' indent='yes' encoding="ISO-8859-1" media-type='text/html' version='4.0' />
	
	<xsl:template match='/'>
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='Root' match='root'>
		<table id="tblMain" cellSpacing="0" cellPadding="0" width="100%">
		<tr><td>
		<xsl:choose>
			<xsl:when test='@Title'>		
				<table id="tblLogo" cellSpacing="0" cellPadding="0" width="100%">
					<tr>
						<td class="pageheader">
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							<xsl:value-of select='@Title' />				
						</td>				
					</tr>
					<tr height = '10'><td></td></tr>			
				</table>	
			</xsl:when>
			<xsl:otherwise>	
			</xsl:otherwise>	
		</xsl:choose>			
		</td>
		</tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
		<xsl:apply-templates />
		</table>
	</xsl:template>
	
	<xsl:template name='Table' match='table'>
	<tr><td>
		<table id='{@name}' cellSpacing="0" cellPadding="0">
			<xsl:apply-templates />			
		</table>
		</td></tr>	
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Mt Report" userelativepaths="yes" externalpreview="no" url="..\..\..\..\..\..\XML&#x2D;Samples\MTReports.xml" htmlbaseurl="" outputurl="..\..\..\..\..\..\XML&#x2D;Samples\MTReports.html" processortype="internal" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->