<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name='BalanceSheetAssets' match='BalanceSheetAssets'>
    <tr>
      <td>
        <table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
          <tr bgcolor="#666666">
            <td width="15" bgColor="#666666">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240" class="header2">
              <b>
                <xsl:value-of select='@title'/>
              </b>
            </td>
            <xsl:for-each select="Years/TableItem">
              <td align="right"  class="header2">
                <b>
                  <xsl:value-of select='@Year'/>
                </b>
              </td>
            </xsl:for-each>
            <td width="15" bgColor="#666666">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="15" bgColor="#666666">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

          <!-- header -->
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='YearEnd/@Title'/>
            </td>
            <xsl:for-each select="YearEnd/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@YearEnd'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Months/@Title'/>
            </td>
            <xsl:for-each select="Months/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Month'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Currency/@Title'/>
            </td>
            <xsl:for-each select="Currency/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Value'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Consolidated/@Title'/>
            </td>
            <xsl:for-each select="Consolidated/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Value'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Abridged/@Title'/>
            </td>
            <xsl:for-each select="Abridged/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Value'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr bgColor="#E0E0E0">
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="240">
              <xsl:value-of select='Denomination/@Title'/>
            </td>
            <xsl:for-each select="Denomination/TableItem">
              <td align="right" >
                <b>
                  <xsl:value-of select='@Value'/>
                </b>
              </td>
            </xsl:for-each>
            <td>
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

          <!-- >> Intangible Assets Section -->
          <xsl:if test="IntangibleAssetsSubTotal/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Intangible Assets
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Goodwill/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Goodwill/@Title'/>
              </td>
              <xsl:for-each select="Goodwill/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherAssets/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='OtherAssets/@Title'/>
              </td>
              <xsl:for-each select="OtherAssets/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IntangibleAssetsSubTotal/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='IntangibleAssetsSubTotal/@Title'/>
              </td>
              <xsl:for-each select="IntangibleAssetsSubTotal/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Intangible Assets Section -->

          <!-- >> Tangible Assets Section -->
          <xsl:if test="FixedAssetsSubTotal/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Fixed Assets - Tangible
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="PropertyPlantEquipment/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='PropertyPlantEquipment/@Title'/>
              </td>
              <xsl:for-each select="PropertyPlantEquipment/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TransportEquipment/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='TransportEquipment/@Title'/>
              </td>
              <xsl:for-each select="TransportEquipment/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="FurnitureFixtures/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='FurnitureFixtures/@Title'/>
              </td>
              <xsl:for-each select="FurnitureFixtures/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="PropertyImprovements/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='PropertyImprovements/@Title'/>
              </td>
              <xsl:for-each select="PropertyImprovements/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="InvestmentProperty/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='InvestmentProperty/@Title'/>
              </td>
              <xsl:for-each select="InvestmentProperty/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherTangibleAssets/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='OtherTangibleAssets/@Title'/>
              </td>
              <xsl:for-each select="OtherTangibleAssets/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="FixedAssetsSubTotal/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='FixedAssetsSubTotal/@Title'/>
              </td>
              <xsl:for-each select="FixedAssetsSubTotal/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Tangible Assets Section -->

          <!-- >> Financial Assets Section -->
          <xsl:if test="TotalFixedAssets/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Financial Assets
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI/@Title !=''  ">
          <tr>
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td>
              <b>
                Long-Term Investments
              </b>
            </td>
          </tr>
          </xsl:if>

          <xsl:if test="LTI_SOE/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  > Shares and Other Equity
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="LTI_SOE_CIS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOE_CIS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOE_CIS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOE_QS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOE_QS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOE_QS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOE_UQS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOE_UQS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOE_UQS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOE/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOE/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOE/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="LTI_SOTS/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  > Securities Other Than Shares
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_TB/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOTS_TB/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_TB/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_MGS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOTS_MGS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_MGS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_BCOD/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOTS_BCOD/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_BCOD/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_FD/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOTS_FD/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_FD/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS_OS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOTS_OS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS_OS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI_SOTS/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI_SOTS/@Title'/>
              </td>
              <xsl:for-each select="LTI_SOTS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LTI/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='LTI/@Title'/>
              </td>
              <xsl:for-each select="LTI/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Available for Sale Financial Assets
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOE/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  > Shares and Other Equity
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="AFSFA_SOE_CIS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOE_CIS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOE_CIS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOE_QS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOE_QS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOE_QS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOE_UQS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOE_UQS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOE_UQS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOE/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOE/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOE/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  > Securities Other Than Shares
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="AFSFA_SOTS_TB/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOTS_TB/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_TB/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS_MGS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOTS_MGS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_MGS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS_BCOD/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOTS_BCOD/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_BCOD/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS_FD/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOTS_FD/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_FD/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS_OS/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOTS_OS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS_OS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA_SOTS/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA_SOTS/@Title'/>
              </td>
              <xsl:for-each select="AFSFA_SOTS/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="AFSFA/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='AFSFA/@Title'/>
              </td>
              <xsl:for-each select="AFSFA/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="FinancialAssetsSubTotal/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='FinancialAssetsSubTotal/@Title'/>
              </td>
              <xsl:for-each select="FinancialAssetsSubTotal/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="TotalFixedAssets/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='TotalFixedAssets/@Title'/>
              </td>
              <xsl:for-each select="TotalFixedAssets/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Financial Assets Section -->

          <!-- >> Current Assets Section -->
          <xsl:if test="CA/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Current Assets
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Inventory/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='CA_Inventory/@Title'/>
              </td>
              <xsl:for-each select="CA_Inventory/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Raw_Materials_WIP/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='CA_Raw_Materials_WIP/@Title'/>
              </td>
              <xsl:for-each select="CA_Raw_Materials_WIP/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Goods_Held_Resale/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='CA_Goods_Held_Resale/@Title'/>
              </td>
              <xsl:for-each select="CA_Goods_Held_Resale/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Trade_Receivables/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='CA_Trade_Receivables/@Title'/>
              </td>
              <xsl:for-each select="CA_Trade_Receivables/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Other_Receivables/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='CA_Other_Receivables/@Title'/>
              </td>
              <xsl:for-each select="CA_Other_Receivables/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Prepayments_Accrued_Income/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='CA_Prepayments_Accrued_Income/@Title'/>
              </td>
              <xsl:for-each select="CA_Prepayments_Accrued_Income/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA_Property_Held_Resale/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='CA_Property_Held_Resale/@Title'/>
              </td>
              <xsl:for-each select="CA_Property_Held_Resale/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CA/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <b>
                  <xsl:value-of select='CA/@Title'/>
                </b>
              </td>
              <xsl:for-each select="CA/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Current Assets Section -->

          <!-- >> Borrowings - Current - Due Within 12 Months (Balance Sheet - ASSETS) Section -->
          <xsl:if test="Assets_BC_DW12Ms/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Borrowings - Current - Due Within 12 Months
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BC_DW12Ms_LocalNonFinancial/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Assets_BC_DW12Ms_LocalNonFinancial/@Title'/>
              </td>
              <xsl:for-each select="Assets_BC_DW12Ms_LocalNonFinancial/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BC_DW12Ms_Residents/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Assets_BC_DW12Ms_Residents/@Title'/>
              </td>
              <xsl:for-each select="Assets_BC_DW12Ms_Residents/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BC_DW12Ms_NonResidents/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Assets_BC_DW12Ms_NonResidents/@Title'/>
              </td>
              <xsl:for-each select="Assets_BC_DW12Ms_NonResidents/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BC_DW12Ms/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Assets_BC_DW12Ms/@Title'/>
              </td>
              <xsl:for-each select="Assets_BC_DW12Ms/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Borrowings - Current - Due Within 12 Months (Balance Sheet - ASSETS) Section -->

          <!-- >> Cash Section -->
          <xsl:if test="TotalCash/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Cash
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Currency_Cash_in_Hand/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Currency - Cash in Hand
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="Cash_Currency_Cash_in_Hand_Euro/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Cash_Currency_Cash_in_Hand_Euro/@Title'/>
              </td>
              <xsl:for-each select="Cash_Currency_Cash_in_Hand_Euro/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Currency_Cash_in_Hand_Foreign/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Cash_Currency_Cash_in_Hand_Foreign/@Title'/>
              </td>
              <xsl:for-each select="Cash_Currency_Cash_in_Hand_Foreign/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Currency_Cash_in_Hand/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Cash_Currency_Cash_in_Hand/@Title'/>
              </td>
              <xsl:for-each select="Cash_Currency_Cash_in_Hand/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Bank_Deposits/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Bank Deposits
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Bank_Deposits_Current/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Cash_Bank_Deposits_Current/@Title'/>
              </td>
              <xsl:for-each select="Cash_Bank_Deposits_Current/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Bank_Deposits_Savings/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Cash_Bank_Deposits_Savings/@Title'/>
              </td>
              <xsl:for-each select="Cash_Bank_Deposits_Savings/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Bank_Deposits_Time/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Cash_Bank_Deposits_Time/@Title'/>
              </td>
              <xsl:for-each select="Cash_Bank_Deposits_Time/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Cash_Bank_Deposits/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Cash_Bank_Deposits/@Title'/>
              </td>
              <xsl:for-each select="Cash_Bank_Deposits/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="TotalCash/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='TotalCash/@Title'/>
              </td>
              <xsl:for-each select="TotalCash/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Cash Section -->

          <!-- >> Borrowings - Non-Current Assets - Over 12 Months (Balance Sheet - ASSETS) Section -->
          <xsl:if test="Assets_BNC_O12Ms/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td>
                <b>
                  Borrowings - Non-Current - Over 12 Months
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="Assets_BNC_O12Ms_LocalNonFinancial/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Assets_BNC_O12Ms_LocalNonFinancial/@Title'/>
              </td>
              <xsl:for-each select="Assets_BNC_O12Ms_LocalNonFinancial/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BNC_O12Ms_Residents/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Assets_BNC_O12Ms_Residents/@Title'/>
              </td>
              <xsl:for-each select="Assets_BNC_O12Ms_Residents/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BNC_O12Ms_NonResidents/@Title !=''  ">
            <tr bgColor="#E0E0E0">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Assets_BNC_O12Ms_NonResidents/@Title'/>
              </td>
              <xsl:for-each select="Assets_BNC_O12Ms_NonResidents/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Assets_BNC_O12Ms/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Assets_BNC_O12Ms/@Title'/>
              </td>
              <xsl:for-each select="Assets_BNC_O12Ms/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Borrowings - Non-Current Assets - Over 12 Months (Balance Sheet - ASSETS) Section -->

          <!-- >> Total Assets Section -->
          <xsl:if test="TotalAssets/@Title !=''  ">
            <!-- Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="ShortTermInvestments/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='ShortTermInvestments/@Title'/>
              </td>
              <xsl:for-each select="ShortTermInvestments/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="Current_Tax_Asset/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='Current_Tax_Asset/@Title'/>
              </td>
              <xsl:for-each select="Current_Tax_Asset/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherCurrentAssets/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='OtherCurrentAssets/@Title'/>
              </td>
              <xsl:for-each select="OtherCurrentAssets/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalCurrentAssets/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='TotalCurrentAssets/@Title'/>
              </td>
              <xsl:for-each select="TotalCurrentAssets/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="DeferredTaxation/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='DeferredTaxation/@Title'/>
              </td>
              <xsl:for-each select="DeferredTaxation/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalAssets/@Title !=''  ">
            <tr bgColor="#E0E0E0" style="font-weight:bold;">
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td width="240">
                <xsl:value-of select='TotalAssets/@Title'/>
              </td>
              <xsl:for-each select="TotalAssets/TableItem">
                <td align="right" >
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Borrowings - Non-Current Assets - Over 12 Months (Balance Sheet - ASSETS) Section -->

          <tr>
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colSpan="2">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>