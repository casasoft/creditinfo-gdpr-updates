<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name='BalanceSheetDebtAndEquity' match='BalanceSheetDebtAndEquity'>
    <tr>
      <td>
        <table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
          <tr>
            <td class="listhead" width="50%" style="border-bottom-width:1pt;">
              <b>
                <xsl:value-of select='@title'/>
              </b>
            </td>
            <xsl:for-each select="Years/TableItem">
              <td align="right" width="15%" class="listhead" style="border-bottom-width:1pt;">
                <b>
                  <xsl:value-of select='@Year'/>
                </b>
              </td>
            </xsl:for-each>
            <td width="15">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td width="15">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

          <!-- >> Company Equity Section -->
          <xsl:if test="TotalEquity/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Company Equity:
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IssuedShareCapital/@Title !=''  ">
            <tr>
              <td>
                <b>
                  Issued Share Capital - Paid
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="ISC_QS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='ISC_QS/@Title'/>
              </td>
              <xsl:for-each select="ISC_QS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="ISC_UQS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='ISC_UQS/@Title'/>
              </td>
              <xsl:for-each select="ISC_UQS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="IssuedShareCapital/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='IssuedShareCapital/@Title'/>
              </td>
              <xsl:for-each select="IssuedShareCapital/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="PreferenceCapital/@Title !=''  ">
            <tr>
              <td>
                <b>
                  Preference Capital
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="PreferenceCapital_QS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='PreferenceCapital_QS/@Title'/>
              </td>
              <xsl:for-each select="PreferenceCapital_QS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="PreferenceCapital_UQS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='PreferenceCapital_UQS/@Title'/>
              </td>
              <xsl:for-each select="PreferenceCapital_UQS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="PreferenceCapital/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='PreferenceCapital/@Title'/>
              </td>
              <xsl:for-each select="PreferenceCapital/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="MinorityInterest/@Title !=''  ">
            <tr>
              <td>
                <b>
                  Minority Interest
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="MinorityInterest_QS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='MinorityInterest_QS/@Title'/>
              </td>
              <xsl:for-each select="MinorityInterest_QS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="MinorityInterest_UQS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='MinorityInterest_UQS/@Title'/>
              </td>
              <xsl:for-each select="MinorityInterest_UQS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="MinorityInterest/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='MinorityInterest/@Title'/>
              </td>
              <xsl:for-each select="MinorityInterest/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="RetainingEarnings/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='RetainingEarnings/@Title'/>
              </td>
              <xsl:for-each select="RetainingEarnings/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="RevaluationReserves/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='RevaluationReserves/@Title'/>
              </td>
              <xsl:for-each select="RevaluationReserves/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="SharePremium/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='SharePremium/@Title'/>
              </td>
              <xsl:for-each select="SharePremium/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherEquity/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='OtherEquity/@Title'/>
              </td>
              <xsl:for-each select="OtherEquity/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalEquity/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='TotalEquity/@Title'/>
              </td>
              <xsl:for-each select="TotalEquity/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Company Equity Section -->

          <!-- >> Current Liabilities Due w 12 Months Section -->
          <xsl:if test="CL_DW12Ms/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Liabilities - Current - Due Within 12 Months
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="CL_DW12Ms_Trade_Other_Payables/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CL_DW12Ms_Trade_Other_Payables/@Title'/>
              </td>
              <xsl:for-each select="CL_DW12Ms_Trade_Other_Payables/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="DebtsTowardsAssociatedCompanies/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='DebtsTowardsAssociatedCompanies/@Title'/>
              </td>
              <xsl:for-each select="DebtsTowardsAssociatedCompanies/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CL_DW12Ms_Amounts_Due_to_Shareholders/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CL_DW12Ms_Amounts_Due_to_Shareholders/@Title'/>
              </td>
              <xsl:for-each select="CL_DW12Ms_Amounts_Due_to_Shareholders/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CL_DW12Ms_Amounts_Due_to_Residents/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CL_DW12Ms_Amounts_Due_to_Residents/@Title'/>
              </td>
              <xsl:for-each select="CL_DW12Ms_Amounts_Due_to_Residents/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CL_DW12Ms_Amounts_Due_to_NonResidents/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CL_DW12Ms_Amounts_Due_to_NonResidents/@Title'/>
              </td>
              <xsl:for-each select="CL_DW12Ms_Amounts_Due_to_NonResidents/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CL_DW12Ms_Advances_From_Customers_Prepayments/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CL_DW12Ms_Advances_From_Customers_Prepayments/@Title'/>
              </td>
              <xsl:for-each select="CL_DW12Ms_Advances_From_Customers_Prepayments/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherShortTermDebt/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OtherShortTermDebt/@Title'/>
              </td>
              <xsl:for-each select="OtherShortTermDebt/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>


          <xsl:if test="CL_DW12Ms/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='CL_DW12Ms/@Title'/>
              </td>
              <xsl:for-each select="CL_DW12Ms/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Current Liabilities Due w 12 Months Section -->

          <!-- >> Current Borrowings Due w 12 Months Section -->
          <xsl:if test="CB_DW12Ms/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Borrowings - Current - Due Within 12 Months
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="CB_DW12Ms_LO_From_Bank_OD_Local/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CB_DW12Ms_LO_From_Bank_OD_Local/@Title'/>
              </td>
              <xsl:for-each select="CB_DW12Ms_LO_From_Bank_OD_Local/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CB_DW12Ms_LO_From_Bank_Loans_Local/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CB_DW12Ms_LO_From_Bank_Loans_Local/@Title'/>
              </td>
              <xsl:for-each select="CB_DW12Ms_LO_From_Bank_Loans_Local/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CB_DW12Ms_LO_From_Local_NF_Companies/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CB_DW12Ms_LO_From_Local_NF_Companies/@Title'/>
              </td>
              <xsl:for-each select="CB_DW12Ms_LO_From_Local_NF_Companies/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CB_DW12Ms_LO_From_Residents_SH/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CB_DW12Ms_LO_From_Residents_SH/@Title'/>
              </td>
              <xsl:for-each select="CB_DW12Ms_LO_From_Residents_SH/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CB_DW12Ms_LO_From_Residents_Other/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CB_DW12Ms_LO_From_Residents_Other/@Title'/>
              </td>
              <xsl:for-each select="CB_DW12Ms_LO_From_Residents_Other/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CB_DW12Ms_LO_From_Bank_Loans_Foreign/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CB_DW12Ms_LO_From_Bank_Loans_Foreign/@Title'/>
              </td>
              <xsl:for-each select="CB_DW12Ms_LO_From_Bank_Loans_Foreign/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CB_DW12Ms_LO_From_NonResidents_Other/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CB_DW12Ms_LO_From_NonResidents_Other/@Title'/>
              </td>
              <xsl:for-each select="CB_DW12Ms_LO_From_NonResidents_Other/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CB_DW12Ms_Taxes_Duties/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='CB_DW12Ms_Taxes_Duties/@Title'/>
              </td>
              <xsl:for-each select="CB_DW12Ms_Taxes_Duties/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="CB_DW12Ms/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='CB_DW12Ms/@Title'/>
              </td>
              <xsl:for-each select="CB_DW12Ms/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="TotalCurrentLiabilities/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='TotalCurrentLiabilities/@Title'/>
              </td>
              <xsl:for-each select="TotalCurrentLiabilities/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Current Borrowings Due w 12 Months Section -->

          <!-- >> Non-Current Liabilities Section -->
          <xsl:if test="NCL_O12Ms/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Liabilities - Non-Current - Over 12 Months
                </b>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TaxLiabilitiesDeferredTax/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='TaxLiabilitiesDeferredTax/@Title'/>
              </td>
              <xsl:for-each select="TaxLiabilitiesDeferredTax/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OtherLiabilitiesProvisions/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OtherLiabilitiesProvisions/@Title'/>
              </td>
              <xsl:for-each select="OtherLiabilitiesProvisions/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCL_O12Ms_Deferred_Income/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCL_O12Ms_Deferred_Income/@Title'/>
              </td>
              <xsl:for-each select="NCL_O12Ms_Deferred_Income/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCL_O12Ms_Provisions/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCL_O12Ms_Provisions/@Title'/>
              </td>
              <xsl:for-each select="NCL_O12Ms_Provisions/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCL_O12Ms_Deferred_Taxation/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCL_O12Ms_Deferred_Taxation/@Title'/>
              </td>
              <xsl:for-each select="NCL_O12Ms_Deferred_Taxation/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCL_O12Ms_Other_NonCurrent_Liabilities/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCL_O12Ms_Other_NonCurrent_Liabilities/@Title'/>
              </td>
              <xsl:for-each select="NCL_O12Ms_Other_NonCurrent_Liabilities/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCL_O12Ms/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='NCL_O12Ms/@Title'/>
              </td>
              <xsl:for-each select="NCL_O12Ms/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Non-Current Liabilities Section -->

          <!-- >> Non-Current Borrowings Section -->
          <xsl:if test="LongTermDebtsSubtotal/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Borrowings - Non-Current - Over 12 Months
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="NCB_O12Ms_LTD_From_Bank_Loans_Local/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCB_O12Ms_LTD_From_Bank_Loans_Local/@Title'/>
              </td>
              <xsl:for-each select="NCB_O12Ms_LTD_From_Bank_Loans_Local/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCB_O12Ms_LTD_From_Local_NF_Companies/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCB_O12Ms_LTD_From_Local_NF_Companies/@Title'/>
              </td>
              <xsl:for-each select="NCB_O12Ms_LTD_From_Local_NF_Companies/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCB_O12Ms_LTD_From_Residents_SH/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCB_O12Ms_LTD_From_Residents_SH/@Title'/>
              </td>
              <xsl:for-each select="NCB_O12Ms_LTD_From_Residents_SH/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCB_O12Ms_LTD_From_Residents_Other/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCB_O12Ms_LTD_From_Residents_Other/@Title'/>
              </td>
              <xsl:for-each select="NCB_O12Ms_LTD_From_Residents_Other/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCB_O12Ms_LTD_From_Bank_Loans_Foreign/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCB_O12Ms_LTD_From_Bank_Loans_Foreign/@Title'/>
              </td>
              <xsl:for-each select="NCB_O12Ms_LTD_From_Bank_Loans_Foreign/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="NCB_O12Ms_LTD_From_NonResidents_Other/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='NCB_O12Ms_LTD_From_NonResidents_Other/@Title'/>
              </td>
              <xsl:for-each select="NCB_O12Ms_LTD_From_NonResidents_Other/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="LongTermDebtsSubtotal/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='LongTermDebtsSubtotal/@Title'/>
              </td>
              <xsl:for-each select="LongTermDebtsSubtotal/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Non-Current Borrowings Section -->

          <!-- >> SOTS Section -->
          <xsl:if test="SOTS/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Securities Other Than Shares
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="SOTS_STS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='SOTS_STS/@Title'/>
              </td>
              <xsl:for-each select="SOTS_STS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="SOTS_LTS/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='SOTS_LTS/@Title'/>
              </td>
              <xsl:for-each select="SOTS_LTS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="SOTS_FD/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='SOTS_FD/@Title'/>
              </td>
              <xsl:for-each select="SOTS_FD/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="SOTS/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='SOTS/@Title'/>
              </td>
              <xsl:for-each select="SOTS/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of SOTS Section -->

          <!-- >> Other Liabilities Section -->
          <xsl:if test="OL/@Title !=''  ">
            <!-- >> Blank Line -->
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td>
                <b>
                  Other Liabilities
                </b>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:if test="OL_Amounts_Due_to_Residents/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OL_Amounts_Due_to_Residents/@Title'/>
              </td>
              <xsl:for-each select="OL_Amounts_Due_to_Residents/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OL_Amounts_Due_to_NonResidents/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OL_Amounts_Due_to_NonResidents/@Title'/>
              </td>
              <xsl:for-each select="OL_Amounts_Due_to_NonResidents/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OL_Other_Not_Classified_Borrowings/@Title !=''  ">
            <tr>
              <td>
                <xsl:value-of select='OL_Other_Not_Classified_Borrowings/@Title'/>
              </td>
              <xsl:for-each select="OL_Other_Not_Classified_Borrowings/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="OL/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='OL/@Title'/>
              </td>
              <xsl:for-each select="OL/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Other Liabilities Section -->

          <!-- >> Blank Line -->
          <xsl:if test="TotalDebtsEquity/@Title !=''  ">
            <tr>
              <td width="15" bgColor="#ffffff">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colSpan="2">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          
          <!-- >> Total Debts Section -->
          <xsl:if test="TotalDebts/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='TotalDebts/@Title'/>
              </td>
              <xsl:for-each select="TotalDebts/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="TotalDebtsEquity/@Title !=''  ">
            <tr style="font-weight:bold;">
              <td>
                <xsl:value-of select='TotalDebtsEquity/@Title'/>
              </td>
              <xsl:for-each select="TotalDebtsEquity/TableItem">
                <td align="right">
                  <xsl:value-of select='@Value'/>
                </td>
              </xsl:for-each>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <!-- << End of Total Debts Section -->

          <tr>
            <td width="15" bgColor="#ffffff">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colSpan="2">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>