<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='BasicClaims' match='BasicClaims'>
		<tr>
			
		<td>
		<table id='tblBasicClaim' class="list" width='60%' cellSpacing="0" cellPadding="0">
			<tr>
				<td colspan="4"><b><xsl:value-of select='NPI/@title'/></b>
				</td>				
				<xsl:choose>									
				<xsl:when test="NPI/@value='No Records'">
					<td><xsl:value-of select="NPI/@value"/></td>
				</xsl:when>					
				<xsl:otherwise>	
					<td><font color="red"><xsl:value-of select="NPI/@value"/></font></td>
				</xsl:otherwise>
				</xsl:choose>				
			</tr>		
		</table>
		</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
