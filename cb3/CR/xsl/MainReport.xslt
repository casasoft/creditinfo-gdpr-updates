<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:import href='TableColumns.xslt'/>
	
	<xsl:output method='html' indent='yes' encoding="ISO-8859-1" media-type='text/html' version='4.0' />

	<xsl:template match='/'>
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='Root' match='root'>
		<table id="tblLogo" cellSpacing="0" cellPadding="0" width="608">
			<tr>
				<td width="333" height="41" bgcolor="#891618" class="headerwhite">
					<img src="img/iv.gif" width="15" height="1" alt="" border="0"/>Credit Report
				</td>
				<td background="img/redline.gif">
					<img src="img/iv.gif" width="1" height="1" alt="" border="0"/>
				</td>
				<td>
					<img src="http://www.lt.is/img/logo_small.gif" width="279" height="41" alt="" border="0"/>
				</td>
			</tr>
			<tr height = '2'><td></td></tr>			
		</table>	
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='MainTable' match='maintable'>
		<table id='{@name}' cellSpacing="0" cellPadding="0" width="608">
			<xsl:apply-templates />
			<tr>
				<td width="15" bgColor="#ffffff">
					<IMG height="1" alt="" src="img/iv.gif" width="15" border="0" />
				</td>
				<td colSpan="2">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name='Table' match='table'>
		<table id='{@name}' cellSpacing="0" cellPadding="0" width="608">
			<xsl:apply-templates />
			<tr>
				<td width="15" bgColor="#ffffff">
					<IMG height="1" alt="" src="img/iv.gif" width="15" border="0" />
				</td>
				<td colSpan="2">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template name='Row' match='tr'>
		<tr>
			<td width="15" bgColor="#ffffff">
				<IMG height="1" alt="" src="img/iv.gif" width="15" border="0" />
			</td>
			<xsl:apply-templates />
		</tr>
	</xsl:template>
	
	<xsl:template name='HeaderRow' match='HeaderRow'>
		<tr bgcolor="#666666">
			<td width="15" bgColor="#666666">
				<IMG height="1" alt="" src="img/iv.gif" width="15" border="0" />
			</td>
			<xsl:apply-templates />
		</tr>
	</xsl:template>
	
	<xsl:template name='Value' match='value'>
		<xsl:call-template name="nbValue">
			<xsl:with-param name='value' select='.' />
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name='nbValue'>
		<xsl:param name='value' />
		<xsl:choose>
			<xsl:when test='string-length($value) = 0'></xsl:when>
			<xsl:otherwise>
				<small>
					<xsl:value-of select='$value' />
				</small>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match='bold'>
		<b>
			<xsl:apply-templates />
		</b>
	</xsl:template>
	
	<xsl:template match='normal'>
		<xsl:apply-templates />
	</xsl:template>
</xsl:stylesheet>
