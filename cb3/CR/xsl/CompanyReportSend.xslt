<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:import href='CompanyReport.xslt' />
	<xsl:import href='TableColumns.xslt' />
	<xsl:output method='html' indent='yes' encoding="UTF-8" media-type='text/html' version='4.0' />
	
	<xsl:template match='/'>
		<HTML>
			<HEAD>
				<title>Creditinfo Group</title>
				<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1" />
				<meta name="CODE_LANGUAGE" Content="C#" />
				<meta name="vs_defaultClientScript" content="JavaScript" />
				<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
				<meta http-equiv="expires" content="-1" />
				<meta http-equiv="pragma" content="no-cache" />
				<meta name="description" content="Lánstraust" />
				<meta name="author" content="Lánstraust Brautarholti 10-14. S: 550-9600" />
				<style>.header { FONT-SIZE: 10px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
					.header2 { FONT-SIZE: 12px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
					.header3 { FONT-SIZE: 12px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
					.headerb { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
					.headerwhite { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
					.headerblack { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
					.conbig { FONT-WEIGHT: bolder; FONT-SIZE: 15px; COLOR: #891618; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
					.conbig2 { FONT-WEIGHT: bolder; FONT-SIZE: 14px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
					.con { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
					P.break { PAGE-BREAK-AFTER: always }
					break { PAGE-BREAK-AFTER: always }
				</style>
			</HEAD>
			<body leftMargin="0" topMargin="0">
				<xsl:apply-templates />
			</body>
		</HTML>
	</xsl:template>	
</xsl:stylesheet>
