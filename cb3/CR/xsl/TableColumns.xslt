<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:template name='NBSPCol' match='nbsp'>
		<td>
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</td>
	</xsl:template>
	
	<xsl:template name='HeaderCol' match='HeaderCol'>
		<xsl:choose>
			<xsl:when test='@colspan'>
				<td class='{@class}' colspan='{@colspan}'>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:when>
			<xsl:otherwise>
				<td class='{@class}'>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name='FooterCol' match='FooterCol'>
		<xsl:choose>
			<xsl:when test='@colspan'>
				<td class='headerb' colspan='{@colspan}'>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:when>
			<xsl:otherwise>
				<td class='{headerb}'>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name='RightHeaderCol' match='RightHeaderCol'>
		<xsl:choose>
			<xsl:when test='@colspan'>
				<td class='{@class}' bcolor = '#666666' align = 'right' colspan='{@colspan}'>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:when>
			<xsl:otherwise>
				<td class='{@class}' bcolor = '#666666' align = 'right' >
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name='RightCol' match='RightCol'>
		<xsl:choose>
			<xsl:when test='@colspan'>
				<td class='{@class}' align = 'right' colspan='{@colspan}'>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:when>
			<xsl:otherwise>
				<td class='{@class}' align = 'right' >
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name='NBSPDarkCol' match='Darknbsp'>	
		<xsl:choose>
			<xsl:when test='@colspan'>
				<td colspan='{@colspan}' bgcolor='#E0E0E0'>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
			</xsl:when>
			<xsl:otherwise>
				<td bgcolor='#E0E0E0'>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name='Col' match='td'>
		<xsl:choose>
			<xsl:when test='@colspan'>
				<td colspan='{@colspan}'>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:when>
			<xsl:otherwise>
				<td>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name='Indentcol' match='IndentCol'>
		<td width="15">
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</td>		
	</xsl:template>
	
	<xsl:template name='DarkIndentcol' match='DarkIndentCol'>
		<td width="15" bgColor="#E0E0E0">
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</td>		
	</xsl:template>
	
	<xsl:template name='HeaderIndentcol' match='HeaderIndentCol'>
		<td width="15" bgColor="#666666">
			<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
		</td>		
	</xsl:template>
	
	<xsl:template name='darkCol' match='DarkCol'>
		<xsl:choose>
			<xsl:when test='@align'>
				<td align='{@align}' bgcolor='#E0E0E0'>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:when>
			<xsl:otherwise>
				<td bgcolor='#E0E0E0'>
					<xsl:call-template name="nbValue">
						<xsl:with-param name='value' select='@value' />
					</xsl:call-template>
					<xsl:apply-templates />
				</td>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>

  