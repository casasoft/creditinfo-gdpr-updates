<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:import href='CompanyReport.xslt'/>
	<xsl:import href='TableColumns.xslt'/>	
	
	<xsl:output method='html' indent='yes' encoding="UTF-8" media-type='text/html' version='4.0' />

	<xsl:template match='/'>
		<xsl:apply-templates />
	</xsl:template>

</xsl:stylesheet>
