<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name='BoardMembersWithInvolvements' match='BoardMembersWithInvolvements'>
	<tr>
		<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td COLSPAN="3" class="header2"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<xsl:for-each select="TableItem">	
						<tr>
							<xsl:choose>
								<xsl:when test='@DDDMark'>
									<td width="15" bgColor="#ffffff"><B><xsl:value-of select='@DDDMark'/>:</B></td>								
								</xsl:when>
								<xsl:otherwise>
									<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								</xsl:otherwise>
							</xsl:choose>							
							<td><b><xsl:value-of select='@RegistrationIDTitle'/></b></td>
							<td><xsl:value-of select='@RegistrationIDValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>					
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@NameTitle'/></b></td>
							<td><xsl:value-of select='@NameValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
	
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@PositionTitle'/></b></td>
							<td><xsl:value-of select='@PositionValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td valign="top"><b><xsl:value-of select='@AlsoInvolvedInTitle'/></b></td>
							<td>
								<table cellSpacing="0" cellPadding="0" >
									<xsl:for-each select="Involvements">
										<tr>
											<td><xsl:value-of select='@Name'/>,<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>	
											<td><xsl:value-of select='@Title'/></td>											
										</tr>
									</xsl:for-each>		
								</table>							
							</td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>	
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:for-each>	
					
					<xsl:choose>
						<xsl:when test='string-length(TableFooter/@DDDMark1) = 0'></xsl:when>
							<xsl:otherwise>
								<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark1'/></td></tr>
								<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark2'/></td></tr>
								<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark3'/></td></tr>
							</xsl:otherwise>
						</xsl:choose>	
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</table>	
			</td>
		</tr>		
	</xsl:template>
</xsl:stylesheet>
