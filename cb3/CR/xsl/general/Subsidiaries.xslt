<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Subsidiaries' match='Subsidiaries'>
		<tr>
			<td>			
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2" colspan="5"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					
					<xsl:if test='NoInfo'>	
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><xsl:value-of select='NoInfo/@value'/></td>
						</tr>
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					</xsl:if>					
					
					<xsl:if test='TableItem'>	
					
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='TableHeader/@CompanyName'/></b></td>
						<td><b><xsl:value-of select='TableHeader/@RegistrationID'/></b></td>
						<td><b><xsl:value-of select='TableHeader/@Ownership'/></b></td>
						<td><b><xsl:value-of select='TableHeader/@FormOfOperation'/></b></td>
						<td><b><xsl:value-of select='TableHeader/@Status'/></b></td>
					</tr>
					
					<xsl:for-each select="TableItem">
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@DDDMark'/></b><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='@CompanyName'/> </td>
							<td><xsl:value-of select='@RegistrationID'/></td>
							<td align="center"><xsl:value-of select='@Ownership'/></td>
							<td><xsl:value-of select='@FormOfOperation'/></td>
							<td><xsl:value-of select='@Status'/></td>
						</tr>
					</xsl:for-each>	
					
					<xsl:if test='TableFooter'>					
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark1'/></td></tr>
					</xsl:if>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</xsl:if>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>