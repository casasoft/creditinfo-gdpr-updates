<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Title' match='Title'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="608">
					<tr>
						<td width="300" height="41" bgcolor="#891618" class="headerwhite">
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							<xsl:value-of select='@title' />				
						</td>
						<td background="img/redline.gif">
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
						</td>
						<td>
							<xsl:text disable-output-escaping="yes">&lt;img src='</xsl:text>
							<xsl:value-of select="@imageUrl"/>				
							<xsl:text disable-output-escaping="yes">' width="279" height="41" alt="" border="0"/&gt;</xsl:text>
						</td>
					</tr>
					<tr height = '2'><td></td></tr>			
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>





  