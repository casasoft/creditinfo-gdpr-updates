<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name='ShareholdersWithInvolvements' match='ShareholdersWithInvolvements'>
<tr>
		<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td COLSPAN="3" class="header2"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<xsl:for-each select="TableItem">						
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@NameTitle'/></b></td>
							<td><xsl:value-of select='@NameValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@NumberTitle'/></b></td>
							<td><xsl:value-of select='@NumberValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@OwnershipTitle'/></b></td>
							<td><xsl:value-of select='@OwnershipValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
						<xsl:if test='Involvements'>							
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td valign="top"><b><xsl:value-of select='@AlsoOwnsSharesInTitle'/></b></td>
								<td>
									<table cellSpacing="0" cellPadding="0" >
										<xsl:for-each select="Involvements">
											<tr>
												<td><xsl:value-of select='@Name'/></td>												
											</tr>
										</xsl:for-each>		
									</table>							
								</td>
								<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							</tr>
						</xsl:if>	
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:for-each>		
				</table>
			</td>
		</tr>				
	</xsl:template>
</xsl:stylesheet>