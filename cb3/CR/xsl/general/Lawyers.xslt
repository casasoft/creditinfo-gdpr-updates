<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Lawyers' match='Lawyers'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@Lawyer'/></b></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@RegistrationID'/></b></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@Address'/></b></td>
					</tr>
					
					<xsl:for-each select="TableItem">
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><xsl:value-of select='@Lawyer'/> </td>
							<td><xsl:value-of select='@RegistrationID'/></td>
							<td><xsl:value-of select='@Address'/></td>
						</tr>
					</xsl:for-each>	
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>

  