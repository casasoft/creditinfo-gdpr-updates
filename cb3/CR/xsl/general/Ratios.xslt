<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Ratios' match='Ratios'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2" colspan="5"><b><xsl:value-of select='@Title'/></b></td>
					</tr>
					<xsl:for-each select="Ratio">
						<tr class="headerb">
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><xsl:value-of select='@Title'/></td>
							<xsl:for-each select="TableItem">						
								<td align="right"><xsl:value-of select='@Value'/></td>
							</xsl:for-each>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
					</xsl:for-each>	
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>
