<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='RealEstates' match='RealEstates'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2" colspan="4"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<xsl:if test='TableItem'>	
						<xsl:for-each select="TableItem">
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td><b><xsl:value-of select='@AddressTitle'/></b></td><td><xsl:value-of select='@AddressValue'/></td>
								<td><b><xsl:value-of select='@PostalCodeTitle'/></b></td><td><xsl:value-of select='@PostalCodeValue'/></td>
							</tr>
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td><b><xsl:value-of select='@InsuranceValueTitle'/></b></td><td><xsl:value-of select='@InsuranceValueValue'/></td>
								<td><b><xsl:value-of select='@YearBuiltTitle'/></b></td><td><xsl:value-of select='@YearBuiltValue'/></td>
							</tr>
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td><b><xsl:value-of select='@LocationTitle'/></b></td><td><xsl:value-of select='@LocationValue'/></td>
								<td><b><xsl:value-of select='@SizeTitle'/></b></td><td><xsl:value-of select='@SizeValue'/></td>
							</tr>
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td><b><xsl:value-of select='@TenureTitle'/></b></td><td><xsl:value-of select='@TenureValue'/></td>
								<td><b><xsl:value-of select='@RealEstateTypeTitle'/></b></td><td><xsl:value-of select='@RealEstateTypeValue'/></td>
							</tr>
							<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
						</xsl:for-each>	
					</xsl:if>	
					<xsl:if test='NoInfo'>	
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><xsl:value-of select='NoInfo/@value'/></td>
						</tr>
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					</xsl:if>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>