<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='RecentEnquiries' match='RecentEnquiries'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2" colspan="4"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='tr/@Last6MonthsText'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='tr/@Last6MonthsValue'/></td>
					</tr>
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='tr/@Last3MonthsText'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='tr/@Last3MonthsValue'/></td>
					</tr>
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='tr/@LastMonthText'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='tr/@LastMonthValue'/></td>
					</tr>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>

