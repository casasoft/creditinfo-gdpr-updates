<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='ProfitLossAccount' match='ProfitLossAccount'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2"><b><xsl:value-of select='@title'/></b></td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right" class="header2"><b><xsl:value-of select='@Year'/></b></td>
						</xsl:for-each>	
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
						
					<!-- Profit/Loss account -->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/Income/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/Income/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/OperationalCost/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/OperationalCost/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/Deprication/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/Deprication/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/OperationalProfit/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/OperationalProfit/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/NetFinancialItems/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/NetFinancialItems/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/PreTaxProfit/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/PreTaxProfit/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/Taxes/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/Taxes/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ProfitLossAccount/NetResult/@Title'/></td>
						<xsl:for-each select="ProfitLossAccount/NetResult/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="ProfitLossAccount/NetResult/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
				
					<!-- Cashflow-->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='CashFlow/@Title'/></b></td>
						<xsl:for-each select="ProfitLossAccount/NetResult/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CashFlow/CapitalFromOperations/@Title'/></td>
						<xsl:for-each select="CashFlow/CapitalFromOperations/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CashFlow/CashFromOperations/@Title'/></td>
						<xsl:for-each select="CashFlow/CashFromOperations/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CashFlow/IncrDescInCash/@Title'/></td>
						<xsl:for-each select="CashFlow/IncrDescInCash/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
									
					<!-- Ratios -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Ratios/@Title'/></b></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/ProfitMargin/@Title'/></td>
						<xsl:for-each select="Ratios/ProfitMargin/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/CurrentRatio/@Title'/></td>
						<xsl:for-each select="Ratios/CurrentRatio/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/QuickRatio/@Title'/></td>
						<xsl:for-each select="Ratios/QuickRatio/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/EquityRatio/@Title'/></td>
						<xsl:for-each select="Ratios/EquityRatio/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/DebtRatio/@Title'/></td>
						<xsl:for-each select="Ratios/DebtRatio/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/CollectionTime/@Title'/></td>
						<xsl:for-each select="Ratios/CollectionTime/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Ratios/StockTurnover/@Title'/></td>
						<xsl:for-each select="Ratios/StockTurnover/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
									
					<!-- Debt & equity total -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Auditted/@Title'/></b></td>
						<xsl:for-each select="Auditted/Auditted/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>		

					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>