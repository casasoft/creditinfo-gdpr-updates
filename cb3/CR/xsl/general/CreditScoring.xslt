<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='CreditScoring' match='CreditScoring'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2" colspan="4"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Score/@ScoreValue'/></td>
					</tr>
					<xsl:if test='ScoreCategories'>		
						<xsl:if test='TableItem'>		
							<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					
							<xsl:for-each select="TableItem">
								<tr>
									<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
									<td><xsl:value-of select='@ScoreCategoryValue'/></td>
								</tr>						
							</xsl:for-each>
						</xsl:if>
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
						<tr>
							<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><xsl:value-of select='ScoreCategories/@Value'/></td>
						</tr>
					</xsl:if>
					<xsl:if test='ScoreDisclaimer/Value'>
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
						<tr>
							<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><xsl:value-of select='ScoreDisclaimer/@Value'/></td>
						</tr>
					</xsl:if>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>