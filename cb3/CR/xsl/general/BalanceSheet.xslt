<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='BalanceSheet' match='BalanceSheet'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2"><b><xsl:value-of select='@title'/></b></td>
						<xsl:for-each select="Years/TableItem">						
							<td align="right" class="header2"><b><xsl:value-of select='@Year'/></b></td>
						</xsl:for-each>	
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="Months/TableItem">						
							<td align="right"><b><xsl:value-of select='@Month'/></b></td>
						</xsl:for-each>
					</tr>
					<tr>
						<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="Currency/TableItem">						
							<td align="right"><b><xsl:value-of select='@Currency'/></b></td>
						</xsl:for-each>
					</tr>
					
					<!-- Current assets -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='CurrentAssets/@Title'/></b></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CurrentAssets/Cash/@Title'/></td>
						<xsl:for-each select="CurrentAssets/Cash/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CurrentAssets/Receivables/@Title'/></td>
						<xsl:for-each select="CurrentAssets/Receivables/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CurrentAssets/OtherCurrentAssets/@Title'/></td>
						<xsl:for-each select="CurrentAssets/OtherCurrentAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CurrentAssets/InventoryStock/@Title'/></td>
						<xsl:for-each select="CurrentAssets/InventoryStock/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='CurrentAssets/CurrentAssetsTotal/@Title'/></td>
						<xsl:for-each select="CurrentAssets/CurrentAssetsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
									
					<!-- Fixed assets -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='FixedAssets/@Title'/></b></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='FixedAssets/FinancialAssets/@Title'/></td>
						<xsl:for-each select="FixedAssets/FinancialAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='FixedAssets/TangibleAssets/@Title'/></td>
						<xsl:for-each select="FixedAssets/TangibleAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='FixedAssets/IntangibleAssets/@Title'/></td>
						<xsl:for-each select="FixedAssets/IntangibleAssets/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='FixedAssets/FixedAssetsTotal/@Title'/></td>
						<xsl:for-each select="FixedAssets/FixedAssetsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
									
					<!-- assets total -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='AssetsTotal/AssetsTotal/@Title'/></b></td>
						<xsl:for-each select="AssetsTotal/AssetsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
									
					<!-- short term debts -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='ShortTermDebts/@Title'/></b></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ShortTermDebts/Overdraft_BankLoan/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/Overdraft_BankLoan/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ShortTermDebts/Creditors/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/Creditors/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ShortTermDebts/OtherShortTermDebts/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/OtherShortTermDebts/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='ShortTermDebts/NextYearMaturities/@Title'/></td>
						<xsl:for-each select="ShortTermDebts/NextYearMaturities/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='ShortTermDebts/ShortTermDebtsTotal/@Title'/></b></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
										
					<!-- long term debts -->
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='LongTermDebts/LongTermDebts/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/LongTermDebts/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='LongTermDebts/Obligations/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/Obligations/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<xsl:for-each select="ShortTermDebts/ShortTermDebtsTotal/TableItem">						
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='LongTermDebts/TotalDebt/@Title'/></b></td>
						<xsl:for-each select="LongTermDebts/TotalDebt/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
						
					<!-- Equity -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Equity/@Title'/></b></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/IssuedShareCapital/@Title'/></td>
						<xsl:for-each select="Equity/IssuedShareCapital/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/OtherEquity/@Title'/></td>
						<xsl:for-each select="Equity/OtherEquity/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='Equity/EquityTotal/@Title'/></b></td>
						<xsl:for-each select="Equity/EquityTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					<tr bgColor="#E0E0E0">
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><xsl:value-of select='Equity/MinorotyHoldingsInEquity/@Title'/></td>
						<xsl:for-each select="Equity/MinorotyHoldingsInEquity/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
									
					<!-- Debt & equity total -->
					<tr>
						<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td><b><xsl:value-of select='DebtEquityTotal/DebtEquityTotal/@Title'/></b></td>
						<xsl:for-each select="DebtEquityTotal/DebtEquityTotal/TableItem">						
							<td align="right"><xsl:value-of select='@Value'/></td>
						</xsl:for-each>
						<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
		

					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>