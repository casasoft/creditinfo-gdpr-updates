<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='BoardMembers' match='BoardMembers'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@Board'/></b></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@RegistrationID'/></b></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@Position'/></b></td>
					</tr>
					
					<xsl:for-each select="TableItem">
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@DDDMark'/></b><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='@Board'/> </td>
							<td><xsl:value-of select='@RegistrationID'/></td>
							<td><xsl:value-of select='@Position'/></td>
						</tr>
					</xsl:for-each>	
								
					<xsl:if test='TableFooter'>			
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark1'/></td></tr>
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark2'/></td></tr>
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark3'/></td></tr>
					</xsl:if>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>

  