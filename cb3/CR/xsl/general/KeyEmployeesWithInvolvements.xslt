<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name='KeyEmployeesWithInvolvements' match='KeyEmployeesWithInvolvements'>
<tr>
		<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="608">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td COLSPAN="3" class="header2"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<xsl:if test='NoInfo'>	
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><xsl:value-of select='NoInfo/@value'/></td>
						</tr>
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>	
					</xsl:if>
					<xsl:if test='TableItem'>	
						<xsl:for-each select="TableItem">						
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td><b><xsl:value-of select='@NameTitle'/></b></td>
								<td><xsl:value-of select='@NameValue'/></td>
								<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							</tr>
							<xsl:choose>
								<xsl:when test='string-length(EducationTitle) = 0'></xsl:when>
								<xsl:otherwise>
									<tr>
										<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
										<td><b><xsl:value-of select='@EducationTitle'/></b></td>
										<td><xsl:value-of select='@EducationValue'/></td>
										<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
									</tr>	
								</xsl:otherwise>
							</xsl:choose>							
		
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td><b><xsl:value-of select='@JobTitleTitle'/></b></td>
								<td><xsl:value-of select='@JobTitleValue'/></td>
								<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							</tr>
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td valign="top"><b><xsl:value-of select='@AlsoInvolvedInTitle'/></b></td>
								<td>
									<table cellSpacing="0" cellPadding="0" >
										<xsl:for-each select="Involvements">
											<tr>
												<td><xsl:value-of select='@Name'/>,<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>	
												<td><xsl:value-of select='@JobTitle'/></td>											
												</tr>
										</xsl:for-each>		
									</table>							
								</td>
								<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							</tr>	
							<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
						</xsl:for-each>		
					</xsl:if>					
				</table>	
			</td>
		</tr>			
	</xsl:template>
</xsl:stylesheet>