<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='NACECode' match='NACECode'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<xsl:for-each select="TableItem">						
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>							
							<td>
								<xsl:choose>
									<xsl:when test="position() = 1">								
										<B><xsl:value-of select='@Code'/></B>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select='@Code'/>
									</xsl:otherwise>
								</xsl:choose>
							</td>						
						</tr>			
					</xsl:for-each>	
					
					<xsl:for-each select="FormerCode">
						<xsl:choose>
							<xsl:when test="position() = 1">
								<TR>
									<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
									<TD><B><xsl:value-of select='@Title'/></B></TD>
								</TR>
							</xsl:when>
						</xsl:choose>
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td>
								<xsl:value-of select='@Code'/>										
							</td>
						</tr>						
					</xsl:for-each>	
					
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>