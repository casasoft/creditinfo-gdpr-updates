<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:import href='TableColumns.xslt'/>
	
	<xsl:output method='html' indent='yes' encoding="UTF-8" media-type='text/html' version='4.0' />

	<xsl:template match='/'>
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='Root' match='root'>
		<table id="tblLogo" cellSpacing="0" cellPadding="0" width="608">
			<tr>
				<td width="333" height="41" bgcolor="#891618" class="headerwhite">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
					<xsl:value-of select='@Title' />				
				</td>
				<td background="img/redline.gif">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
				<td>
					<img src="../img/logo.gif" width="279" height="41" alt="" border="0"/>
				</td>
			</tr>
			<tr height = '2'><td></td></tr>			
		</table>	
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='MainTable' match='maintable'>
		<table id='{@name}' cellSpacing="0" cellPadding="0" width="600">
			<xsl:apply-templates />
			<tr>
				<td width="15" bgColor="#ffffff">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
				<td colSpan="2">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
			</tr>
		</table>
	</xsl:template>
	
	<xsl:template name='Table' match='table'>
		<table id='{@name}' cellSpacing="0" cellPadding="0" width="608">
			<xsl:apply-templates />
			<tr>
				<td width="15" bgColor="#ffffff">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
				<td colSpan="2">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
			</tr>
		</table>
	</xsl:template>
	
	<xsl:template name='BorderTable' match='BorderTable'>		
		<table id='{@name}' border="1" cellSpacing="0" cellPadding="0" width="608">
			<xsl:apply-templates />		
		</table>
		<table cellSpacing="0" cellPadding="0" width="608">
			<tr>
				<td width="15" bgColor="#ffffff">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
				<td colSpan="2">
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
				</td>
			</tr>
		</table>		
	</xsl:template>
	
	<xsl:template name='Row' match='tr'>
		<tr>
			<td width="15" bgColor="#ffffff">
				<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
			</td>
			<xsl:apply-templates />
		</tr>
	</xsl:template>
	
	<xsl:template name='HeaderRow' match='HeaderRow'>
		<tr bgcolor="#666666">
			<td width="15" bgColor="#666666">
				<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
			</td>
			<xsl:apply-templates />
		</tr>
	</xsl:template>
	
	<xsl:template name='Value' match='value'>
		<xsl:call-template name="nbValue">
			<xsl:with-param name='value' select='.' />
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name='nbValue'>
		<xsl:param name='value' />
		<xsl:choose>
			<xsl:when test='string-length($value) = 0'></xsl:when>
			<xsl:otherwise>
				<small>
					<xsl:value-of select='$value' />
				</small>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name='HeaderValue'>
		<xsl:param name='value' />
		<xsl:choose>
			<xsl:when test='string-length($value) = 0'></xsl:when>
			<xsl:otherwise>
				<h4>
					<xsl:value-of select='$value' />
				</h4>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match='bold'>
		<b>
			<xsl:apply-templates />
		</b>
	</xsl:template>
	
	<xsl:template match='list'>
		<li>
			<xsl:apply-templates />
		</li>
	</xsl:template>
	
	<xsl:template match='NonBulletList'>	
			<xsl:apply-templates />
			<br></br>
	</xsl:template>
	
	<xsl:template match='PopupLink'>
		<xsl:text disable-output-escaping="yes">&lt;A HREF='</xsl:text>
		<xsl:value-of select="@Link"/>				
		<xsl:text disable-output-escaping="yes">' target='_blank'&gt;</xsl:text>
		<xsl:value-of select="@Title"/>
		<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
	</xsl:template>
	
	<xsl:template match='normal'>
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='Shareholders' match='Shareholders'>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="608">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td COLSPAN="3" class="header2"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<xsl:for-each select="TableItem">						
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@NameTitle'/></b></td>
							<td><xsl:value-of select='@NameValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@OwnershipTitle'/></b></td>
							<td><xsl:value-of select='@OwnershipValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td valign="top"><b><xsl:value-of select='@AlsoOwnsSharesInTitle'/></b></td>
							<td>
								<table cellSpacing="0" cellPadding="0" >
									<xsl:for-each select="Involvements">
										<tr>
											<td><xsl:value-of select='@Name'/></td>												
										</tr>
									</xsl:for-each>		
								</table>							
							</td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>	
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:for-each>		
				</table>			
	</xsl:template>
	
	<xsl:template name='KeyEmployees' match='KeyEmployees'>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="608">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td COLSPAN="3" class="header2"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<xsl:for-each select="TableItem">						
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@NameTitle'/></b></td>
							<td><xsl:value-of select='@NameValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
						<xsl:choose>
							<xsl:when test='string-length(EducationTitle) = 0'></xsl:when>
							<xsl:otherwise>
								<tr>
									<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
									<td><b><xsl:value-of select='@EducationTitle'/></b></td>
									<td><xsl:value-of select='@EducationValue'/></td>
									<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								</tr>	
							</xsl:otherwise>
						</xsl:choose>							
		
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@JobTitleTitle'/></b></td>
							<td><xsl:value-of select='@JobTitleValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td valign="top"><b><xsl:value-of select='@AlsoInvolvedInTitle'/></b></td>
							<td>
								<table cellSpacing="0" cellPadding="0" >
									<xsl:for-each select="Involvements">
										<tr>
											<td><xsl:value-of select='@Name'/>,<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>	
											<td><xsl:value-of select='@JobTitle'/></td>											
										</tr>
									</xsl:for-each>		
								</table>							
							</td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>	
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:for-each>		
				</table>			
	</xsl:template>
	
	<xsl:template name='BoardMembers' match='BoardMembers'>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="608">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td COLSPAN="3" class="header2"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					<xsl:for-each select="TableItem">						
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@NameTitle'/></b></td>
							<td><xsl:value-of select='@NameValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
	
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='@PositionTitle'/></b></td>
							<td><xsl:value-of select='@PositionValue'/></td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td valign="top"><b><xsl:value-of select='@AlsoInvolvedInTitle'/></b></td>
							<td>
								<table cellSpacing="0" cellPadding="0" >
									<xsl:for-each select="Involvements">
										<tr>
											<td><xsl:value-of select='@Name'/>,<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>	
											<td><xsl:value-of select='@Title'/></td>											
										</tr>
									</xsl:for-each>		
								</table>							
							</td>
							<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						</tr>	
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
					</xsl:for-each>	
					
					<xsl:choose>
						<xsl:when test='string-length(TableFooter/@DDDMark1) = 0'></xsl:when>
							<xsl:otherwise>
								<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark1'/></td></tr>
								<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark2'/></td></tr>
								<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark3'/></td></tr>
		
								<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
							</xsl:otherwise>
						</xsl:choose>
						
				
				</table>			
	</xsl:template>
</xsl:stylesheet>
