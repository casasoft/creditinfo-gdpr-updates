<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:output method='html' indent='yes' encoding="ISO-8859-1" media-type='text/html' version='4.0' />

	<xsl:template match='/'>
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template name='Root' match='root'>
		<table id="tblMain" cellSpacing="0" cellPadding="0" width="100%">
		<tr><td>
		<xsl:choose>
			<xsl:when test='@Title'>		
				<table id="tblLogo" cellSpacing="0" cellPadding="0" width="100%">
					<tr>
						<td class="pageheader">
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							<xsl:value-of select='@Title' />				
						</td>				
					</tr>
					<tr height = '10'><td></td></tr>			
				</table>	
			</xsl:when>
			<xsl:otherwise>	
			</xsl:otherwise>	
		</xsl:choose>	
					
		
		</td>
		</tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
		<xsl:apply-templates />
		</table>
	</xsl:template>
	
	<xsl:template name='Capital' match='Capital'>
		<tr><td>
		<table id='tblCapital' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td><xsl:value-of select='@title'/></td></tr>
			
			<tr>
				<td>
					<b><xsl:value-of select='Issued/@title'/>: </b><xsl:value-of select='Issued/@value'/>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><b><xsl:value-of select='Authorized/@title'/>: </b><xsl:value-of select='Authorized/@value'/>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><b><xsl:value-of select='PaidUp/@title'/>: </b><xsl:value-of select='PaidUp/@value'/>
				</td>
			</tr>
			<tr>
				<td>
					<b><xsl:value-of select='Nominal/@title'/>: </b><xsl:value-of select='Nominal/@value'/>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
					<b><xsl:value-of select='ShareClass/@title'/>: </b><xsl:value-of select='ShareClass/@value'/>
					<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
					<b><xsl:value-of select='ShareDescription/@title'/>: </b><xsl:value-of select='ShareDescription/@value'/>
				</td>
			</tr>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='DirectorsAndMore' match='DirectorsAndMore'>
		<tr><td>
		<table id='tblDirectorsAndMore' width='100%' cellSpacing="0" cellPadding="0">
			<xsl:apply-templates />			
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='Directors' match='Directors'>
		<tr><td colspan="7"><b><xsl:value-of select='@title'/></b></td></tr>
		<xsl:for-each select="tr">
			<xsl:call-template name="DirectorsAndMoreRow"/>
		</xsl:for-each>
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='Shareholders' match='Shareholders'>
		<tr><td colspan="7"><b><xsl:value-of select='@title'/></b></td></tr>
		<xsl:for-each select="tr">
			<xsl:call-template name="DirectorsAndMoreRow"/>
		</xsl:for-each>
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='CompanyInvolvements' match='CompanyInvolvements'>
		<tr><td>
		<table id='tblCompanyInvolvements' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead"><xsl:value-of select='Header/@Company'/></td>
				<td class="listhead"><xsl:value-of select='Header/@Position'/></td>
				<td class="listhead"><xsl:value-of select='Header/@Status'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
						<TD><xsl:value-of select="@Position"/></TD>
						<TD><xsl:value-of select="@Status"/></TD>
					</TR>
					</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>								
						</TD>
						<TD><xsl:value-of select="@Position"/></TD>
						<TD><xsl:value-of select="@Status"/></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='ShareholderInvolvements' match='ShareholderInvolvements'>
		<tr><td>
		<table id='tblShareholderInvolvements' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead"><xsl:value-of select='Header/@Company'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
					</TR>
					</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>								
						</TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='BoardMemberInvolvements' match='BoardMemberInvolvements'>
		<tr><td>
		<table id='tblBoardMemberInvolvements' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead"><xsl:value-of select='Header/@Company'/></td>
				<td class="listhead"><xsl:value-of select='Header/@Position'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
						<TD><xsl:value-of select="@Position"/></TD>
					</TR>
					</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>								
						</TD>
						<TD><xsl:value-of select="@Position"/></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='PrincipalInvolvements' match='PrincipalInvolvements'>
		<tr><td>
		<table id='tblPrincipalInvolvements' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td class="listhead"><xsl:value-of select='Header/@Company'/></td>
				<td class="listhead"><xsl:value-of select='Header/@Title'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR class='dark-row'>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
						<TD><xsl:value-of select="@Title"/></TD>
					</TR>
					</xsl:when>
					<xsl:otherwise>
					<TR>
						<TD>
							<xsl:choose>
								<xsl:when test='@ID'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?reqNationalID=</xsl:text>
									<xsl:value-of select="@ID"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Name"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Name"/>
								</xsl:otherwise>				
							</xsl:choose>								
						</TD>
						<TD><xsl:value-of select="@Title"/></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='LegalRepresentative' match='LegalRepresentative'>
		<tr><td colspan="7"><b><xsl:value-of select='@title'/></b></td></tr>
		<xsl:for-each select="tr">
			<xsl:call-template name="DirectorsAndMoreRow"/>
		</xsl:for-each>
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='DirectorsAndMoreRow' match='DirectorsAndMoreRow'>
		<TR>
			<TD><xsl:value-of select="@Name"/></TD>
			<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
			<TD>ID#: 
				<xsl:choose>
					<xsl:when test='@PassNo'>
						<xsl:value-of select="@PassNo"/>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="@ID"/>
				<BR></BR>
				<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?LibraReport=true&amp;reqNationalID=</xsl:text>
				<xsl:value-of select="@ID"/>				
				<xsl:text disable-output-escaping="yes">'&gt;TAF-report&lt;/a&gt;</xsl:text>
			</TD>
			<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
			<TD><xsl:value-of select="@Address1"/><br></br><xsl:value-of select="@Address2"/></TD>
			<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
			<TD><xsl:value-of select="@City"/><br></br><xsl:value-of select="@Country"/></TD>
		</TR>
	</xsl:template>
	
	<xsl:template name='Secretary' match='Secretary'>
		<tr><td colspan="7"><b><xsl:value-of select='@title'/></b></td></tr>
		<xsl:for-each select="tr">
			<xsl:call-template name="DirectorsAndMoreRow"/>
		</xsl:for-each>
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='PendingCases' match='PendingCases'>
		<tr><td>
		<table id='tblPendingCases' class="list" width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<xsl:choose>
					<xsl:when test='NoInfo/@value'>
						<tr><td><xsl:value-of select="NoInfo/@value"/></td></tr>
					</xsl:when>
				</xsl:choose>
			
			<xsl:for-each select="tr">
				<tr>
					<td rowspan="3"></td>
					<td class="listhead" colspan="3"><xsl:value-of select="@CaseNumberText"/> <xsl:value-of select="@CaseNumber"/></td>
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@InfoSourceText"/></b>:<br></br><xsl:value-of select="@InfoSource"/></td>
					<td class="dark-row"><b><xsl:value-of select="@ClaimOwnerText"/></b>:<br></br><xsl:value-of select="@ClaimOwner"/></td>
					<td class="dark-row"><b><xsl:value-of select="@DateText"/></b>:<br></br><xsl:value-of select="@Date"/></td>
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@AdjudicatorText"/></b>:<br></br><xsl:value-of select="@Adjudicator"/></td>
					<td class="dark-row"><b><xsl:value-of select="@AmountText"/></b>:<br></br><xsl:value-of select="@Amount"/></td>
					<td class="dark-row"><b><xsl:value-of select="@CIReferenceText"/></b>:<br></br><xsl:value-of select="@CIReference"/></td>
				</tr>
				<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='Claims' match='Claims'>
		<tr><td>
		<table id='tblClaims' class="list" width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<xsl:choose>
					<xsl:when test='NoInfo/@value'>
						<tr><td><xsl:value-of select="NoInfo/@value"/></td></tr>
					</xsl:when>
				</xsl:choose>
			
			<xsl:for-each select="tr">
				<tr>
					<!-- <td rowspan="3"></td> -->
					<td class="listhead" colspan="2"><xsl:value-of select="@CaseNumberText"/> <xsl:value-of select="@CaseNumber"/></td>
					<td class="listhead" colspan="2"><xsl:value-of select="@CIReferenceText"/> : <xsl:value-of select="@CIReferenceID"/></td>
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@DateText"/></b>:<br></br><xsl:value-of select="@Date"/></td>
					<td class="dark-row" colspan="2"><b><xsl:value-of select="@ClaimOwnerText"/></b>:<br></br><xsl:value-of select="@ClaimOwner"/></td>
					<td class="dark-row"><b><xsl:value-of select="@ClaimTypeText"/></b>:<br></br><xsl:value-of select="@ClaimType"/></td>
					
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@GazetteYearText"/></b>:<br></br><xsl:value-of select="@GazetteYear"/></td>
					<td class="dark-row" colspan="2"><b><xsl:value-of select="@GazettePageText"/></b>:<br></br><xsl:value-of select="@GazettePage"/></td>
					<td class="dark-row"><b><xsl:value-of select="@InfoSourceText"/></b>:<br></br><xsl:value-of select="@InfoSource"/></td>
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@AmountText"/></b>:<br></br><xsl:value-of select="@Amount"/></td>
					<td class="dark-row" colspan="2"><b><xsl:value-of select="@AgentText"/></b>:<br></br><xsl:value-of select="@Agent"/></td>
					<td class="dark-row"><b><xsl:value-of select="@CommentText"/></b>:<br></br><xsl:value-of select="@Comment"/></td>
				</tr>
				<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='CaseACompanies' match='CaseACompanies'>
		<tr><td>
		<table id='tblMiscCases' class="list" width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<xsl:choose>
					<xsl:when test='NoInfo/@value'>
						<tr><td><xsl:value-of select="NoInfo/@value"/></td></tr>
					</xsl:when>
				</xsl:choose>
			
			<xsl:for-each select="tr">
				<tr>
					<!-- <td rowspan="3"></td> -->
					<td class="listhead" colspan="1"><xsl:value-of select="@CaseNumberText"/> <xsl:value-of select="@CaseNumber"/></td>
					<td class="listhead" colspan="2"><xsl:value-of select="@CIReferenceText"/> : <xsl:value-of select="@CIReferenceID"/></td>
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@DateText"/></b>:<br></br><xsl:value-of select="@Date"/></td>
					<td class="dark-row" colspan="2"><b><xsl:value-of select="@ClaimOwnerText"/></b>:<br></br><xsl:value-of select="@ClaimOwner"/></td>
					<td class="dark-row"><b><xsl:value-of select="@ClaimTypeText"/></b>:<br></br><xsl:value-of select="@ClaimType"/></td>
					
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@GazetteYearText"/></b>:<br></br><xsl:value-of select="@GazetteYear"/></td>
					<td class="dark-row" colspan="2"><b><xsl:value-of select="@GazettePageText"/></b>:<br></br><xsl:value-of select="@GazettePage"/></td>
					<td class="dark-row"><b><xsl:value-of select="@InfoSourceText"/></b>:<br></br><xsl:value-of select="@InfoSource"/></td>
				</tr>
				<tr>
					<td class="dark-row" colspan="4"><b><xsl:value-of select="@CommentText"/></b>:<br></br><xsl:value-of select="@Comment"/></td>
				</tr>
				<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='BouncedCheques' match='BouncedCheques'>
		<tr><td>
		<table id='tblBouncedCheques' class="list" width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td colspan="4"><xsl:value-of select='@title'/></td></tr>
			<xsl:choose>
					<xsl:when test='NoInfo/@value'>
						<tr><td><xsl:value-of select="NoInfo/@value"/></td></tr>
					</xsl:when>
				</xsl:choose>
			
			<xsl:for-each select="tr">
				<tr>
					<!-- <td rowspan="3"></td> -->
					<td class="listhead" colspan="1"><xsl:value-of select="@CaseNumberText"/> <xsl:value-of select="@CaseNumber"/></td>
					<td class="listhead" colspan="2"><xsl:value-of select="@CIReferenceText"/> : <xsl:value-of select="@CIReferenceID"/></td>
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@DateText"/></b>:<br></br><xsl:value-of select="@Date"/></td>
					<td class="dark-row" colspan="2"><b><xsl:value-of select="@ClaimOwnerText"/></b>:<br></br><xsl:value-of select="@ClaimOwner"/></td>
					<td class="dark-row"><b><xsl:value-of select="@ClaimTypeText"/></b>:<br></br><xsl:value-of select="@ClaimType"/></td>
					
				</tr>
				<tr>
					<td class="dark-row"><b><xsl:value-of select="@AmountText"/></b>:<br></br><xsl:value-of select="@Amount"/></td>
					<td class="dark-row" colspan="2"><b><xsl:value-of select="@AgentText"/></b>:<br></br><xsl:value-of select="@Agent"/></td>
					<td class="dark-row"><b><xsl:value-of select="@IssuedBankText"/></b>:<br></br><xsl:value-of select="@IssuedBank"/></td>
				</tr>
				<tr>
					<td class="dark-row" colspan="4"><b><xsl:value-of select="@CommentText"/></b>:<br></br><xsl:value-of select="@Comment"/></td>
				</tr>
				<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='RegistrationForm' match='RegistrationForm'>
		<tr><td>
		<table id='tblRegistrationForm' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td><xsl:value-of select='RegForm/@value'/></td>
			</tr>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='RecentEnquiries' match='RecentEnquiries'>
		<tr><td>
		<table id='tblRecentEnquiries' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td><xsl:value-of select='tr/@Last6MonthsText'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='tr/@Last6MonthsValue'/></td>
			</tr>
			<tr>				
				<td><xsl:value-of select='tr/@Last3MonthsText'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='tr/@Last3MonthsValue'/></td>
			</tr>
			<tr>
				<td><xsl:value-of select='tr/@LastMonthText'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='tr/@LastMonthValue'/></td>
			</tr>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='CompanyState' match='CompanyState'>
		<tr><td>
		<table id='tblCompanyState' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td><xsl:value-of select='State/@value'/></td>
			</tr>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='HasCourtOrders' match='HasCourtOrders'>
		<tr><td>
		<table id='tblHasCourtOrders' width='100%' cellSpacing="0" cellPadding="0">
			<tr class='sectionheader'><td><xsl:value-of select='@title'/></td></tr>
			<tr>
				<td><xsl:value-of select='State/@value'/></td>
			</tr>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>
	
	<xsl:template name='Table' match='table'>
	<tr><td>
		<table id='{@name}' cellSpacing="0" cellPadding="0">
			<xsl:apply-templates />			
		</table>
		</td></tr>	
	</xsl:template>
	
	<xsl:template name='OtherInformation' match='OtherInformation'>
		<tr><td>
		<table id='tblOtherInformation' width='100%' class="list" cellSpacing="0" cellPadding="1">
			<tr class="listhead" valign="top">
				<td><xsl:value-of select='Header/@Number'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@DecreeNumber'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@Source'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@Identity'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@Name'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@SonDaughter'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@PlaceOfBirth'/></td>
				<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
				<td><xsl:value-of select='Header/@Type'/></td>
			</tr>
			<xsl:variable name="dark" select="'1'" />
			<xsl:for-each select="tr">
				<xsl:choose>
					<xsl:when test="position() mod 2 = 0">
					<TR valign="top">
						<TD class="listhead" style="text-alignment:right;padding-top:2pt"><xsl:value-of select="@Number"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@DecreeNumber"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Source"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD>
							<xsl:choose>
								<xsl:when test='@Identity'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?TAFReport=true&amp;reqNationalID=</xsl:text>
									<xsl:value-of select="@Identity"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Identity"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Identity"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Name"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@SonDaughter"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@PlaceOfBirth"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Type"/></TD>
					</TR>
					</xsl:when>
					<xsl:otherwise>
					<TR class='dark-row' valign="top">
						<TD class="listhead" style="text-alignment:right;padding-top:2pt"><xsl:value-of select="@Number"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@DecreeNumber"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Source"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD>
							<xsl:choose>
								<xsl:when test='@Identity'>							
									<xsl:text disable-output-escaping="yes">&lt;A HREF='FoSearch.aspx?LibraReport=true&amp;reqNationalID=</xsl:text>
									<xsl:value-of select="@Identity"/>				
									<xsl:text disable-output-escaping="yes">'&gt;</xsl:text>
									<xsl:value-of select="@Identity"/>
									<xsl:text disable-output-escaping="yes">&lt;/a&gt;</xsl:text>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Identity"/>
								</xsl:otherwise>				
							</xsl:choose>						
						</TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Name"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@SonDaughter"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@PlaceOfBirth"/></TD>
						<TD><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text></TD>
						<TD><xsl:value-of select="@Type"/></TD>
					</TR>
					</xsl:otherwise>				
				</xsl:choose>
			</xsl:for-each>
			<tr height = '10'><td></td></tr>	
		</table>	
		</td></tr>	
		<tr><td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
	</xsl:template>

</xsl:stylesheet>
