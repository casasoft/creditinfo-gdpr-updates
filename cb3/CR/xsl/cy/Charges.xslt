<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='Charges' match='Charges'>
		<tr>
			<td>			
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%" border="1">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2" colspan="7"><b><xsl:value-of select='@title'/></b></td>
					</tr>
					
					<xsl:if test='NoInfo'>	
						<tr>
							<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><xsl:value-of select='NoInfo/@value'/></td>
						</tr>
					</xsl:if>
					
					<xsl:if test='TableItem'>					
						<tr>
							<td width="15"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
							<td><b><xsl:value-of select='TableHeader/@DateRegistered'/></b></td>
							<td><b><xsl:value-of select='TableHeader/@DatePrepared'/></b></td>
							<td><b><xsl:value-of select='TableHeader/@Type'/></b></td>
							<td><b><xsl:value-of select='TableHeader/@Description'/></b></td>
							<td><b><xsl:value-of select='TableHeader/@Amount'/></b></td>
							<td><b><xsl:value-of select='TableHeader/@Sequence'/></b></td>
							<td><b><xsl:value-of select='TableHeader/@Beneficiary'/></b></td>
						</tr>
					
						<xsl:for-each select="TableItem">
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td><xsl:value-of select='@DateRegistered'/></td>
								<td><xsl:value-of select='@DatePrepared'/></td>
								<td><xsl:value-of select='@Type'/></td>							
								<xsl:choose>
									<xsl:when test='string-length(Description) = 0'>
										<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
									</xsl:when>	
									<xsl:otherwise>
										<td><xsl:value-of select='@Description'/></td>
									</xsl:otherwise>
								</xsl:choose>	
							
								<td><xsl:value-of select='@Amount'/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select='@CurrencyCode'/></td>
								<xsl:choose>
									<xsl:when test='string-length(Description) = 0'>
										<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
									</xsl:when>	
									<xsl:otherwise>
										<td align="center"><xsl:value-of select='@Sequence'/></td>
									</xsl:otherwise>
								</xsl:choose>	
								<td><xsl:value-of select='@Beneficiary'/></td>
							</tr>
						</xsl:for-each>		
					</xsl:if>
					<xsl:if test='TableFooter'>								
						<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td> <td class="headerb" colspan="3"><xsl:value-of select='TableFooter/@DDDMark1'/></td></tr>
					</xsl:if>
					<!--
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				-->
				</table>
				<table id='ChargesFooter' cellSpacing="0" cellPadding="2" width="100%">
				<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				
				</table>
							
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>