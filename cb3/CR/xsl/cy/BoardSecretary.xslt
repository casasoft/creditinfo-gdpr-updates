<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name='BoardSecretary' match='BoardSecretary'>
		<tr>
			<td>
				<table id='{@name}' cellSpacing="0" cellPadding="0" width="100%">
					<tr bgcolor="#666666">
						<td width="15" bgColor="#666666"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@BoardSecretary'/></b></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@RegistrationID'/></b></td>
						<td class="header2"><b><xsl:value-of select='TableHeader/@History'/></b></td>
					</tr>
					<xsl:choose>
						<xsl:when test='NoInfo'>
							<tr>
								<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
								<td><xsl:value-of select='NoInfo/@value'/> </td>
							</tr>
						</xsl:when>
						<xsl:otherwise>
							<xsl:for-each select="TableItem">
								<tr>
									<td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
									<td><xsl:value-of select='@Name'/> </td>
									<xsl:choose>
										<xsl:when test='@Individual'>
											<td><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
										</xsl:when>
										<xsl:otherwise>
											<td><xsl:value-of select='@RegistrationID'/></td>								
										</xsl:otherwise>							
									</xsl:choose>
									<td><xsl:value-of select='@History'/></td>
								</tr>
							</xsl:for-each>	
						</xsl:otherwise>							
					</xsl:choose>
					<tr><td width="15" bgColor="#ffffff"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colSpan="2"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				</table>			
			</td>		
		</tr>
	</xsl:template>
</xsl:stylesheet>
