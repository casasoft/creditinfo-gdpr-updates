#region

using System;
using System.Configuration;
using System.IO;
using System.Resources;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using CR.BLL;
using CR.Localization;
using Logging.BLL;
using WebSupergoo.ABCpdf4;

#endregion

using Cig.Framework.Base.Configuration;

namespace CR {
    /// <summary>
    /// Summary description for FoReport.
    /// </summary>
    public class FoReport : Page {
        protected Literal litCssPath;
        protected Xml xmlxslTransform;

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                litCssPath.Text = CigConfig.Configure("lookupsettings.reportCssFullUrl");

                displayReport();
            }
        }

        private void displayReport() {
            ResourceManager rm;
            rm = CIResource.CurrentManager;

            string companyId = "";
            string reportType = "3";
            string cultureToUse = "Native";
            string afs_ids = "";
            string reportFormat = "HTML";

            if (Session["CompanyCIID"] != null) {
                companyId = Session["CompanyCIID"].ToString();
            }
            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }
            if (Session["ReportFormat"] != null) {
                reportFormat = Session["ReportFormat"].ToString();
            }
            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }
            if (Session["AFSID"] != null) {
                afs_ids = Session["AFSID"].ToString();
            }

            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            var generator = new TemplateReportGenerator(
                rm, cultureToUse, userCreditInfoID, userID, ipAddress);
            string xslFile = null;
            string xslSendFile = null;
            var doc = generator.GetReport(
                reportType, int.Parse(companyId), afs_ids, -1, ref xslFile, ref xslSendFile);
            var transformer = new XslTransform();
            //doc.Save("c:\\cig\\TemplateReport.xml");
            //	if(CigConfig.Configure("lookupsettings.foRootName"] != null)
            //		transformer.Load(Server.MapPath(CigConfig.Configure("lookupsettings.foRootName"].ToString() +"/CR/xsl/FOFinancialStatementDisplay.xslt"));
            //	else // at least it will work this way at production server
            //		transformer.Load(Application.Get("AppPath")+"/CR/xsl/FOFinancialStatementDisplay.xslt");

            try {
                switch (reportFormat) {
                    case "HTML":
                        if (CigConfig.Configure("lookupsettings.foRootName") != null) {
                            transformer.Load(Server.MapPath(CigConfig.Configure("lookupsettings.foRootName") + xslFile));
                        } else // at least it will work this way at production server
                        {
                            transformer.Load(Application.Get("AppPath") + xslFile);
                        }
                        //	transformer.Load(Server.MapPath(Request.ApplicationPath+xslFile)); 
                        xmlxslTransform.Document = doc;
                        xmlxslTransform.Transform = transformer;

                        break;
                    case "PDF":
                        var xslt = new XslTransform();
                        xslt.Load(Application.Get("AppPath") + xslFile);
                        var fileDir = CigConfig.Configure("lookupsettings.TMPFilesLocation");
                            //"C:/Inetpub/wwwroot/BulkSampleCSharp/files/"; // get 
                        string file = fileDir + Util.CreateUniqueString() + ".html";
                        var writer = new StreamWriter(file);
                        writer.Write(doc.OuterXml);
                        writer.Flush();
                        writer.Close();

                        xslt.Transform(file, file);
                        ///css style adding
                        ///
                        var reader = new StreamReader(file);
                        string newHtml = reader.ReadToEnd();
                        reader.Close();
                        newHtml = "<LINK href=\"" + CigConfig.Configure("lookupsettings.reportAdditionalCssFullUrl") +
                                  "\" type=\"text/css\" rel=\"stylesheet\">" + newHtml;
                        newHtml = "<LINK href=\"" + CigConfig.Configure("lookupsettings.reportFoCssFullUrl") +
                                  "\" type=\"text/css\" rel=\"stylesheet\">" + newHtml;
                        writer = new StreamWriter(file, false, Encoding.UTF8);
                        writer.Write(newHtml);
                        writer.Flush();
                        writer.Close();

                        ///********************************************
                        ///start

                        XSettings.License = CigConfig.Configure("lookupsettings.AbcPDFLicence");
                        var theDoc = new Doc();

                        //We first create a Doc object and inset the edges a little so that the 
                        //HTML will appear in the middle of the page
                        //		theDoc.Rect.Inset(72, 144);

                        //We add the first page and indicate that we will be adding more pages 
                        //by telling the Doc object that this is page one. We save the returned 
                        //ID as this will be used to add subsequent pages.
                        var theID = theDoc.AddImageUrl("file:///" + file, true, 620, false);

                        //We now chain subsequent pages together. We stop when we reach a page 
                        //which wasn't truncated.
                        while (true) {
                            theDoc.FrameRect();
                            if (theDoc.GetInfo(theID, "Truncated") != "1") {
                                break;
                            }
                            theDoc.Page = theDoc.AddPage();
                            theID = theDoc.AddImageToChain(theID);
                        }

                        //After adding the pages we can flatten them. We can't do this until 
                        //after the pages have been added because flattening will invalidate our 
                        //previous ID and break the chain.
                        for (int i = 1; i <= theDoc.PageCount; i++) {
                            theDoc.PageNumber = i;
                            theDoc.Flatten();
                        }

                        Response.ContentType = "application/pdf";
                        theDoc.Save(Response.OutputStream);
                        theDoc.Clear();
                        Response.Flush();
                        Response.End();

                        ///end
                        ///********************************************

                        break;
                }
            } catch (Exception err) {
                Logger.WriteToLog("displayReport(): The error is : " + err, true);
            }
        }

        /// <summary>
        /// Takes html string and writes to disk. The file location is specified in cig.cfg.xml
        /// </summary>
        /// <param name="htmlString">The html string that shall be wtitten to disk</param>
        /// <returns>The file name and location</returns>
        public string WriteHTMLtoFile(string htmlString) {
            string fileDir = CigConfig.Configure("lookupsettings.TMPFilesLocation");
                //"C:/Inetpub/wwwroot/BulkSampleCSharp/files/"; // get 
            int theSeed = DateTime.Now.Second;
            var randFigure = new Random(theSeed);
            string createdFileName = randFigure.Next().ToString();

            var writer = new StreamWriter(fileDir + createdFileName + ".html");
                // File.Create("C:/Inetpub/wwwroot/BulkSampleCSharp/files/" + randFigure.ToString() + ".html"); // need to create folder for files and set the location in conf file
            writer.Write(htmlString);
            writer.Flush();
            writer.Close();

            return fileDir + createdFileName + ".html";
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}