﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using cb3.CR.DAL;
using CPI.BLL;
using cb3.Scoring.localization;
using System.Globalization;
using cb3.BLL.Scoring;

namespace cb3.CR.MTBLL
{
    class ScoringCompanyBusinessObject
    {
        int creditInfoId;
        ScoringHelper helper = new ScoringHelper();

        ReportCompanyBLLC company;

        private bool IsCompnayCPrefix
        {
            get
            {
                if (company != null && company.UniqueID.StartsWith("C "))
                    return true;
                return false;
            }

        }

        public ScoringCompanyBusinessObject(ReportCompanyBLLC company, XmlDocument input)
        {
            this.SetCompanyAge(company.Established);

            this.SetCurrentAssetsTotal(input);
            this.SetCurrentLiabilitiesTotal(input);

            this.SetDDD(input);
            this.SetAccountAge(input);
            this.SetQuickRatio(input);
            this.SetNetWorth(input);
            this.creditInfoId = company.CompanyCIID;
            this.company = company;
        }

        public decimal? CurrentAssetsTotal { get; private set; }
        public decimal? ShortTermDebtsTotal { get; private set; }

        private void SetCurrentAssetsTotal(XmlDocument input)
        {
            var currentAssets = input.SelectSingleNode("//CurrentAssetsTotal/TableItem");
            if (currentAssets != null)
            {
                decimal outValue = 0.0M;
                if(decimal.TryParse(currentAssets.Attributes["Value"].InnerText, out outValue))
                {
                    CurrentAssetsTotal = outValue;
                }
            }
        }

          private void SetCurrentLiabilitiesTotal(XmlDocument input)
        {
            var currentLiabilities = input.SelectSingleNode("//ShortTermDebtsTotal/TableItem");
            if (currentLiabilities != null)
            {
                decimal outValue = 0.0M;
                if(decimal.TryParse(currentLiabilities.Attributes["Value"].InnerText, out outValue))
                {
                    ShortTermDebtsTotal = outValue;
                }
            }
        }

        private void SetDDD(XmlDocument input)
        {
            DDD = new List<decimal>();
            var ddd = input.SelectNodes("//Claims[@name='tblClaims']/tr[@CIReferenceText='CI reference']/@DDDAmount");
            if (ddd != null && ddd.Count > 0)
            {
                foreach(XmlNode node in ddd)
                {
                    decimal outValue = 0.0M;
                    if (decimal.TryParse(node.Value, out outValue))
                        DDD.Add(outValue);
                }
            }
        }

        private void SetNetWorth(XmlDocument input)
        {
            var netWorth = input.SelectNodes("//EquityTotal/TableItem");
            if (netWorth != null && netWorth.Count != 0)
            {
                var value = decimal.Parse(netWorth[0].Attributes["Value"].InnerText);

                if (value < 0)
                    value = 0;
                NetWorth = value;
            }
            else
                NetWorth = decimal.MaxValue;
        }

        private void SetQuickRatio(XmlDocument input)
        {
            var quickRatio = input.SelectNodes("//QuickRatio/TableItem");
            if (quickRatio != null && quickRatio.Count != 0)
            {
                var value = decimal.Parse(quickRatio[0].Attributes["Value"].InnerText);

                if (value < 0)
                    value = 0;
                QuickRatio = value;
            }
            else
                QuickRatio =decimal.MaxValue;
        }

        private void SetAccountAge(XmlDocument input)
        {
            var nodeList = input.SelectNodes("//HeadFSI/Years/TableItem");

            var currency = input.SelectSingleNode("//HeadFSI/Currency/TableItem");
            if (currency != null)
            {
                if(currency.Attributes["Value"] != null)
                    Currency = currency.Attributes["Value"].InnerText;
            }
            var accountsYear = 0;
            if (nodeList != null)
            {
                foreach (XmlNode year in nodeList)
                {
                    var currentYear = int.Parse(year.Attributes["Year"].InnerText);
                    if (currentYear > accountsYear)
                        accountsYear = currentYear;
                }
            }
            AccountsAge = DateTime.Now.Year - 1 - accountsYear;
            if (AccountsAge < 0)
                AccountsAge = 0;
        }

        private void SetCompanyAge(DateTime registeredDate)
        {
            if (DateTime.Today.Subtract(registeredDate).TotalDays < 0)
            {
                CompanyAge = 0;
                return;
            }
            DateTime today = DateTime.Today;
            double months = 12;
            CompanyAge = (((today.Year - registeredDate.Year) * months) + today.Month - registeredDate.Month) / months;
        }


        public int? CompanyStatus
        {
            get
            {
                return new ScoringDAL().GetCompanyStatus(this.creditInfoId);
            }
        }

        public decimal NetWorth { get; private set; }
        public double CompanyAge { get; private set; }
        public decimal InitialPrecentage { get; private set; }
        public decimal PrecentageReduce { get; private set; }
        public string Currency { get; private set; }
        public IList<decimal> DDD { get; private set; }

        public decimal CL
        {
            get
            {
                if (QuickRatio == decimal.MaxValue || NetWorth == decimal.MaxValue)
                    return decimal.MaxValue;

                var cl = (NetWorth * (InitialPrecentage - PrecentageReduce)) / 100;
                if (QRP.HasValue)
                {
                    if (QuickRatio < QRP.Value)
                    {
                        cl = (cl * QuickRatio);
                    }
                }

                var max = 400000;

                // According to Remy this rule should not be live yet
                //if (DDD.Count != 0 && cl != 0)
                //{
                //    if (DDDBound.HasValue && (DDD.Sum() > (DDDBound * NetWorth / 100)))
                //        cl -= DDD.Sum();
                //}
                return cl > max ? max : cl;
            }
        }


        public decimal QuickRatio { get; private set; }

        public decimal? DDDBound
        {
            get
            {
                var ratio = helper.GetScorQuickRatio();
                if (ratio != null)
                    return ratio.DDDBound;
                return null;
            }
        }

        public decimal? QRP
        {
            get
            {
                var ratio = helper.GetScorQuickRatio();
                if (ratio != null)
                    return ratio.QRBound;
                return null;
            }
        }

        public int AccountsAge { get; set; }
        private string creditLimit;
        public string GetCreditLimit()
        {
            return creditLimit;
        }

        public void SetCreditLimit(string val)
        {
            creditLimit = val;
        }

        public ScoringResult ScoringResult { get; set; }


        public bool CompanyAgeIsInExclusionInterval()
        {
            var value = helper.GetExclusionInterval(CompanyAge);

            if (value == null)
                return false;
            InitialPrecentage = value.Value;
            return true;
        }


        internal bool AccountAgeIsInExclusiveInterval()
        {
            var value = helper.GetAccauntAgeExclusiveInterval(AccountsAge);
            if (value == null)
                return true;
            PrecentageReduce = value.Value;
            return false;
        }

        internal cb3.BLL.Scoring.FSI GetFSIValue()
        {
            return helper.GetFSIValue(NetWorth);
        }

        internal cb3.BLL.Scoring.RiskBandsRange GetRiskBandRange(decimal ratio)
        {
            return helper.GetRiskBandRange(ratio);
        }


        enum Lookup_CompanyStatus
        {
            Active = 1,
            InDissolution = 2,
            StruckOff = 3,
            Defunct = 4,
            InProcessofMerging = 5,
            StruckOffFollowingMerger = 6,
            InProcessofConverting = 7,
            InProcessofDividing = 8,
            StruckOffFollowingDivision = 9,
            SubjectToUNEUSanctions = 10
        }


        ScoringResult GetSovereignRisk(int? companyStatus)
        {

            var value = (Lookup_CompanyStatus)Enum.Parse(typeof(Lookup_CompanyStatus), CompanyStatus.ToString());
            string localized = CIResource.CurrentManager.GetString(value.ToString());
            return new ScoringResult(localized, new cb3.BLL.Scoring.FSI() { Value = localized }, this.GetDefaultRiskBand());
        }

        private RiskBandsRange GetDefaultRiskBand()
        {
            return new RiskBandsRange() { RiskDescription = CIResource.CurrentManager.GetString("ScoringResult.NC"), Color = "#bc514c" };
        }

        internal ScoringResult GetResult()
        {
            bool isActive = CompanyStatus.HasValue && CompanyStatus.Value == 1;

            var riskBandResult = GetDefaultRiskBand();
            if (IsCompnayCPrefix)
            {
                if (!isActive)
                    return this.GetSovereignRisk(CompanyStatus);
            }
            else
            {
                if (isActive)
                    return new ScoringResult(CIResource.CurrentManager.GetString("NotApplicable"), 
                        new cb3.BLL.Scoring.FSI() { Value = CIResource.CurrentManager.GetString("NotApplicable") }, 
                        riskBandResult);
                else
                    return this.GetSovereignRisk(CompanyStatus);
            }

           
            if (NetWorth <= 0)
                return new ScoringResult(CIResource.CurrentManager.GetString("GuaranteesRequired"),
                    new cb3.BLL.Scoring.FSI() { Value = CIResource.CurrentManager.GetString("ScoringResult.N") }, riskBandResult);

            if (!this.CompanyAgeIsInExclusionInterval())
                return new ScoringResult("New Business",
                    new cb3.BLL.Scoring.FSI() { Value = CIResource.CurrentManager.GetString("ScoringResult.NB") }, riskBandResult);

            if (AccountsAge < 0)
                AccountsAge = 0;

            if (this.AccountAgeIsInExclusiveInterval())
                return new ScoringResult(CIResource.CurrentManager.GetString("AccountTooOld"), new cb3.BLL.Scoring.FSI()
                {
                    Value = CIResource.CurrentManager.GetString("AccountTooOld")
                }, riskBandResult);

            var fsi = this.GetFSIValue();
            if (fsi == null)
                throw new ArgumentException(CIResource.CurrentManager.GetString("NetWorthUnknown"));

            var cl = CL;
            if (cl == decimal.MaxValue)
                return new ScoringResult(CIResource.CurrentManager.GetString("NoQuickRatio"), fsi, riskBandResult);

            string clResult = cl.ToString("0,0", CultureInfo.InvariantCulture) + " " + this.Currency;
           
            if (cl < 0)
            {
                clResult = CIResource.CurrentManager.GetString("GuaranteesRequired");
            }
            CRRatio = 0;
            if (DDD.Count <= fsi.DDDNumber)
            {
                if (CurrentAssetsTotal.HasValue && ShortTermDebtsTotal.HasValue && ShortTermDebtsTotal.Value != 0)
                    CRRatio = CurrentAssetsTotal.Value / ShortTermDebtsTotal.Value;
            }
            var riskBandRangeResut = this.GetRiskBandRange(CRRatio);
            if (riskBandRangeResut != null)
                riskBandResult = riskBandRangeResut;

            return new ScoringResult(clResult, fsi, riskBandResult);
        }
        public decimal CRRatio { get; private set; }
    }
}
