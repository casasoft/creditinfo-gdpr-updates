#region

using CPI.BLL;

#endregion

namespace CR.MTBLL {
    /// <summary>
    /// Summary description for MTCapital.
    /// </summary>
    public class MTCapitalBLLC : CapitalBLLC {
        protected string shareClass;
        public new string ShareClass { get { return shareClass; } set { shareClass = value; } }
    }
}