﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using cb3.CR.DAL;
using CPI.BLL;
using System.Resources;
using cb3.Scoring.localization;
using cb3.CR.MTBLL;

namespace CR.MTBLL
{
    public class MTScoringBuilder 
    {
        private XmlDocument input;
        XmlNode root;
 
        ScoringCompanyBusinessObject builder;
        ReportCompanyBLLC company;
        public MTScoringBuilder(ReportCompanyBLLC company, int ciid, XmlDocument input)
        {
            this.input = input;

            root = input.CreateElement("RiskIndicator");
            this.company = company;
        }

        public XmlNode RiskIndicator
        {
            get { return root; }
        }

        internal XmlNode Build()
        {           
            builder = new ScoringCompanyBusinessObject(company, input);
            var result = builder.GetResult();

            var attr = input.CreateAttribute("name");
            attr.InnerText = "tblRiskIndicator";

            root.Attributes.Append(attr);

            attr = input.CreateAttribute("title");
            attr.InnerText = CIResource.CurrentManager.GetString("RiskIndicator");
            root.Attributes.Append(attr);

            XmlElement element = input.CreateElement("FinancialStrengthIndicator");
                    
            element.InnerText = result.FSI;

            attr = input.CreateAttribute("title");
            attr.InnerText = CIResource.CurrentManager.GetString("FinancialStrenghtIndicator");
            
            element.Attributes.Append(attr);

            attr = input.CreateAttribute("RiskDescription");
            attr.InnerText = string.IsNullOrEmpty(result.RiskDescription) ? string.Empty : result.RiskDescription;
            element.Attributes.Append(attr);

            attr = input.CreateAttribute("color");
            attr.InnerText = string.IsNullOrEmpty(result.Color) ? "000000" : result.Color;
            element.Attributes.Append(attr);


            root.AppendChild(element);


            element = input.CreateElement("Risk");
            attr = input.CreateAttribute("color");
            attr.InnerText = string.IsNullOrEmpty(result.RiskBandColor) ? "000000" : result.RiskBandColor;
            element.Attributes.Append(attr);

            attr = input.CreateAttribute("title");
            attr.InnerText = CIResource.CurrentManager.GetString("RiskLabel");
            element.Attributes.Append(attr);

            attr = input.CreateAttribute("RiskValue");
            attr.InnerText = result.RiskBandValue;
            element.Attributes.Append(attr);

            var riskDescription = input.CreateElement("RiskDescription");
            riskDescription.InnerText = CIResource.CurrentManager.GetString("RiskDescription"); 
            element.AppendChild(riskDescription);

            var financialStrengthDescription = input.CreateElement("FinancialStrengthDescription");
            financialStrengthDescription.InnerText = CIResource.CurrentManager.GetString("FinancialStrengthDescription");
            element.AppendChild(financialStrengthDescription);
       

            root.AppendChild(element);

            element = input.CreateElement("CreditLimit");
            attr = input.CreateAttribute("Disclaimer");
            attr.InnerText = CIResource.CurrentManager.GetString("Diclaimer");
            element.Attributes.Append(attr);

            attr = input.CreateAttribute("title");
            attr.InnerText = CIResource.CurrentManager.GetString("CreditLimit");
            element.Attributes.Append(attr);

            element.InnerText = result.CL;
            root.AppendChild(element);
            return root;
        }
    }
}
