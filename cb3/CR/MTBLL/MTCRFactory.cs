#region

using System.Data;
using CR.BLL;
using CR.MTDAL;

#endregion

namespace CR.MTBLL {
    /// <summary>
    /// Summary description for MTCRFactory.
    /// </summary>
    public class MTCRFactory : CRFactory {
        protected MTCompanyReportsDALC mtDacl;
        public MTCRFactory() { mtDacl = new MTCompanyReportsDALC(); }
        public DataSet GetOtherInformation(string nationalID, string name) { return mtDacl.GetOtherInformation(nationalID, name); }
        public DataSet GetMTPendingCases(string nationalID) { return mtDacl.GetMTPendingCases(nationalID); }
        public DataSet GetMTClaims(int ciid, bool getCourtOrders) { return mtDacl.GetMTClaims(ciid, getCourtOrders); }
        public DataSet GetMTClaimsBasic(int ciid) { return mtDacl.GetMTClaimsBasic(ciid); }
        public bool HasCourtOrders(string nationalID) { return mtDacl.HasCourtOrders(nationalID); }
        public MTCapitalBLLC GetMTCapital(string nationalID) { return mtDacl.GetMTCapital(nationalID); }
        public string GetMTRegistrationForm(string nationalID) { return mtDacl.GetMTRegistrationForm(nationalID); }
    }
}