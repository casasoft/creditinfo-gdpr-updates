﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace cb3.CR.MTBLL
{
    class ScoringResult
    {

        public string CL { get; private set; }
        public string FSI { get; private set; }
        public string RiskDescription { get; private set; }
        public string Color { get; private set; }

        public string RiskBandDescription { get; private set; }
        public string RiskBandValue { get; private set; }
        public string RiskBandColor { get; private set; }

        public ScoringResult(string cl, cb3.BLL.Scoring.FSI fsi, cb3.BLL.Scoring.RiskBandsRange risk)
        {
            this.CL = cl;
            FSI = fsi.Value;
            RiskDescription = fsi.RiskDescription;
            Color = fsi.Color;
            if (risk != null)
            {
                RiskBandDescription = risk.RiskDescription;
                RiskBandValue = risk.Value != null ? risk.Value : risk.RiskDescription;/*int.Parse(risk.Value.ToString()).ToString()*/ ;
                RiskBandColor = risk.Color;
            }
        }
    }
}
