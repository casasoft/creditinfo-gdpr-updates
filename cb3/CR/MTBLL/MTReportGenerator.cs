#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Xml;
using CPI.BLL;
using CR.BLL;
using Logging.BLL;
using UserAdmin.BLL;

#endregion

using Cig.Framework.Base.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace CR.MTBLL {
    public class MTReportGenerator : ReportGenerator {
        protected MTCRFactory mtFactory;

        public MTReportGenerator(
            ResourceManager rm, string cultureToUse, int userCreditInfoID, int userID, string ipAddress)
            : base(rm, cultureToUse, userCreditInfoID, userID, ipAddress) {
            className = "MTReportGenerator";
            mtFactory = new MTCRFactory();
        }

        # region Public Methods:

        public XmlDocument GetMTCompanyProfile(int ciid, string nationalID) {
            const string funcName = "getFOCompanyProfile(string nationalID)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtBasicReport", ci));
            doc.AppendChild(rootElement);
            try {
                AddMTCompanyProfile(doc, rootElement, ciid, nationalID);
                logUsage(ciid.ToString());
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return doc;
        }

        public XmlDocument GetMTCompanyReport(int ciid, string nationalID, string afs_ids) {
            const string funcName = "GetMTCompanyReport(int ciid, string nationalID)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            //XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtCompanyReport", ci));
            XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtCompanyCreditReport", ci));
            doc.AppendChild(rootElement);
            try {
                AddMTCompanyReport(doc, rootElement, ciid, nationalID, afs_ids);
                logUsage(ciid.ToString());
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return doc;
        }

        public XmlDocument GetMTCompanyBasicTAFReport(int ciid, string nationalID) {
            const string funcName = "getFOCompanyBasicTAFReport(string nationalID)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            XmlHelper.AddAttribute(doc, rootElement, "Title", "Basic TAF Report");
            doc.AppendChild(rootElement);
            try {
                AddMTCompanyBasicTAFReport(doc, rootElement, ciid, nationalID);
                logUsage(ciid.ToString());
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return doc;
        }

        public XmlDocument GetMTCompanyTAFReport(int ciid, string nationalID) {
            const string funcName = "getFOCompanyTAFReport(string nationalID)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtTAFReport", ci));
            doc.AppendChild(rootElement);
            try {
                AddMTCompanyTAFReport(doc, rootElement, ciid, nationalID);
                logUsage(ciid.ToString());
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return doc;
        }

        public XmlDocument GetMTIndividualBasicTAFReport(int ciid, string nationalID) {
            const string funcName = "getFOCompanyTAFReport(string nationalID)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            XmlHelper.AddAttribute(doc, rootElement, "Title", "Basic TAF Report");
            doc.AppendChild(rootElement);
            try {
                AddMTIndividualBasicTAFReport(doc, rootElement, ciid, nationalID);
                logUsage(ciid.ToString());
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return doc;
        }

        public XmlDocument GetMTIndividualTAFReport(int ciid, string nationalID) {
            const string funcName = "getFOCompanyTAFReport(string nationalID)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtTAFReport", ci));
            doc.AppendChild(rootElement);
            try {
                AddMTIndividualTAFReport(doc, rootElement, ciid, nationalID);
                logUsage(ciid.ToString());
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return doc;
        }

        public XmlDocument GetOtherInformations(string nationalID, string name) {
            const string funcName = "GetOtherInformations(string nationalID, string name)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            doc.AppendChild(rootElement);
            try {
                AddOtherInformation(doc, rootElement, nationalID, name);
                logUsage(nationalID);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return doc;
        }

        #endregion

        private void AddMTCompanyTAFReport(XmlDocument doc, XmlNode reportRoot, int ciid, string nationalID) {
            bool isMalta = false;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                isMalta = CigConfig.Configure("lookupsettings.currentVersion").ToLower() == "malta";
            }

            var userAdminFactory = new uaFactory();
            //Add "Has Court Orders"
            if (nationalID.Trim() != "") {
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOHasCourtOrders")))) {
                    if (!isMalta) {
                        reportRoot.AppendChild(GetMTHasCourtOrders(doc, nationalID));
                    } else {
                        //display court orders specifically for Maltese version
                        reportRoot.AppendChild(GetMTClaims(doc, ciid, true));
                    }
                }
            }

            //HSJ 2005.10.13, add court orders here for malta??
            //Add claims
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOClaims")))) {
                reportRoot.AppendChild(GetMTClaims(doc, ciid, false));
            }
            if (nationalID.Trim() != "") {
                //Add pending cases
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOPendingCases")))) {
                    reportRoot.AppendChild(GetMTPendingCases(doc, nationalID));
                }
                //Add company state
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOCompanyState")))) {
                    reportRoot.AppendChild(GetMTCompanyState(doc, nationalID));
                }
                //Add company involvements
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOCompanyInvolvements")))) {
                    reportRoot.AppendChild(GetMTCompanyInvolvements(doc, nationalID, true));
                }
            }
            //Add recent enquiries
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FORecentEnquiries")))) {
                reportRoot.AppendChild(GetRecentEnquiries(doc, ciid));
            }
            //AddEndOfReport(doc, reportRoot);
        }

        private void AddMTCompanyBasicTAFReport(XmlDocument doc, XmlNode reportRoot, int ciid, string nationalID) {
            bool isMalta = false;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                isMalta = CigConfig.Configure("lookupsettings.currentVersion").ToLower() == "malta";
            }

            var userAdminFactory = new uaFactory();
            //Add "Has Court Orders"
            if (nationalID.Trim() == "") {
                return;
            }
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOHasCourtOrders")))) {
                if (isMalta) {
                    reportRoot.AppendChild(GetMTClaimsBasic(doc, ciid));
                }
            }

            //AddEndOfReport(doc, reportRoot);
        }

        private void AddMTIndividualBasicTAFReport(XmlDocument doc, XmlNode reportRoot, int ciid, string nationalID) {
            bool isMalta = false;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                isMalta = CigConfig.Configure("lookupsettings.currentVersion").ToLower() == "malta";
            }

            var userAdminFactory = new uaFactory();
            //Add "Has Court Orders"
            if (nationalID.Trim() == "") {
                return;
            }
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOHasCourtOrders")))) {
                if (isMalta) {
                    reportRoot.AppendChild(GetMTClaimsBasic(doc, ciid));
                }
            }
        }

        private void AddMTIndividualTAFReport(XmlDocument doc, XmlNode reportRoot, int ciid, string nationalID) {
            bool isMalta = false;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                isMalta = CigConfig.Configure("lookupsettings.currentVersion").ToLower() == "malta";
            }

            var userAdminFactory = new uaFactory();
            //Add "Has Court Orders"
            if (nationalID.Trim() != "") {
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOHasCourtOrders")))) {
                    if (!isMalta) {
                        reportRoot.AppendChild(GetMTHasCourtOrders(doc, nationalID));
                    } else {
                        //display court orders specifically for Maltese version
                        reportRoot.AppendChild(GetMTClaims(doc, ciid, true));
                    }
                }
            }
            //Add claims
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOClaims")))) {
                reportRoot.AppendChild(GetMTClaims(doc, ciid, false));
            }
            //Add pending cases
            if (nationalID.Trim() != "") {
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOPendingCases")))) {
                    reportRoot.AppendChild(GetMTPendingCases(doc, nationalID));
                }

                //Add company involvements
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOCompanyInvolvements")))) {
                    reportRoot.AppendChild(GetMTCompanyInvolvements(doc, nationalID, false));
                }
            }
            //Add recent enquiries
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FORecentEnquiries")))) {
                reportRoot.AppendChild(GetRecentEnquiries(doc, ciid));
            }

            //	AddEndOfReport(doc, reportRoot);
        }

        private void AddMTCompanyProfile(XmlDocument doc, XmlNode reportRoot, int ciid, string nationalID) {
            ReportCompanyBLLC theCompany = mtFactory.GetCompanyReport("", ciid, false);

            //HistoryOperationReviewBLLC theHistoryOperation = factory.GetHistoryOperationReview(companyCIID);
            if (theCompany == null) {
                return;
            }
            bool isMalta = false;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                isMalta = CigConfig.Configure("lookupsettings.currentVersion").ToLower() == "malta";
            }

            var userAdminFactory = new uaFactory();
            //Add "Has Court Orders"
            if (nationalID.Trim() != "") {
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOHasCourtOrders")))) {
                    if (!isMalta) {
                        reportRoot.AppendChild(GetMTHasCourtOrders(doc, nationalID));
                    } else {
                        //display court orders specifically for Maltese version
                        reportRoot.AppendChild(GetMTClaims(doc, ciid, true));
                    }
                }
            }

            //Add claims
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOClaims")))) {
                reportRoot.AppendChild(GetMTClaims(doc, ciid, false));
            }
            //Add company state
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOCompanyState")))) {
                reportRoot.AppendChild(GetMTCompanyState(doc, nationalID));
            }
            //Add company involvements
            if (nationalID.Trim() != "") {
                //Add pending cases
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOPendingCases")))) {
                    reportRoot.AppendChild(GetMTPendingCases(doc, nationalID));
                }

                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOCompanyInvolvements")))) {
                    reportRoot.AppendChild(GetMTCompanyInvolvements(doc, nationalID, true));
                }

                //Add directors
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FODirectorsAndMore")))) {
                    reportRoot.AppendChild(GetMTDirectorsAndMore(doc, nationalID));
                }

                //Add capital

                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOCapital")))) {
                    reportRoot.AppendChild(GetMTCapital(doc, nationalID));
                }

                //Add registration form
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FORegistrationForm")))) {
                    reportRoot.AppendChild(GetRegistrationForm(doc, mtFactory.GetMTRegistrationForm(nationalID)));
                }
            }
            //Add recent enquiries
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FORecentEnquiries")))) {
                reportRoot.AppendChild(GetRecentEnquiries(doc, ciid));
            }

            //	AddEndOfReport(doc, reportRoot);			
        }

        private void AddMTCompanyReport(
            XmlDocument doc, XmlNode reportRoot, int ciid, string nationalID, string afs_ids) {
            ReportCompanyBLLC theCompany = mtFactory.GetCompanyReport("", ciid, false);

            //HistoryOperationReviewBLLC theHistoryOperation = factory.GetHistoryOperationReview(companyCIID);
            if (theCompany == null) {
                return;
            }
            bool isMalta = false;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                isMalta = CigConfig.Configure("lookupsettings.currentVersion").ToLower() == "malta";
            }

            XmlNode companySummary = this.GetCompanySummary(theCompany, doc);
            reportRoot.AppendChild(companySummary);

            MTScoringBuilder scorBuilder = new MTScoringBuilder(theCompany, ciid, doc);
            reportRoot.AppendChild(scorBuilder.RiskIndicator);

            var userAdminFactory = new uaFactory();
            //Add "Has Court Orders"
            if (nationalID.Trim() != "") {
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOHasCourtOrders")))) {
                    if (!isMalta) {
                        reportRoot.AppendChild(GetMTHasCourtOrders(doc, nationalID));
                    } else {
                        //display court orders specifically for Maltese version
                        reportRoot.AppendChild(GetMTClaims(doc, ciid, true));
                    }
                }
            }

            //Add claims
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOClaims")))) {
                reportRoot.AppendChild(GetMTClaims(doc, ciid, false));
            }
            //Add company state
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOCompanyState")))) {
                reportRoot.AppendChild(GetMTCompanyState(doc, nationalID));
            }
            if (nationalID.Trim() != "") {
                //Add pending cases
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOPendingCases")))) {
                    reportRoot.AppendChild(GetMTPendingCases(doc, nationalID));
                }
     
                //Add directors
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FODirectorsAndMore")))) {
                    reportRoot.AppendChild(GetMTDirectorsAndMore(doc, nationalID));
                }

                //Add capital
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOCapital")))) {
                    reportRoot.AppendChild(GetMTCapital(doc, nationalID));
                }


                //Add company involvements
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FOCompanyInvolvements"))))
                {
                    reportRoot.AppendChild(GetMTCompanyInvolvements(doc, nationalID, true));
                }


                //Add registration form
                if (userAdminFactory.HasUserAccessToProduct(
                    userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FORegistrationForm")))) {
                    reportRoot.AppendChild(GetRegistrationForm(doc, mtFactory.GetMTRegistrationForm(nationalID)));
                }
            }

            //Add Balance sheet

            if (afs_ids != null)
            {
                var tGen = new TemplateReportGenerator(
                    rm, cultToUse, userCreditInfoID, userID, ipAddress);
                reportRoot.AppendChild(tGen.GetHeadFSI(doc, ciid, afs_ids));
                reportRoot.AppendChild(tGen.GetProfitLossAccount(doc, ciid, afs_ids));
                reportRoot.AppendChild(tGen.GetBalanceSheet(doc, ciid, afs_ids));
                reportRoot.AppendChild(tGen.GetCashFlow(doc, ciid, afs_ids));
                reportRoot.AppendChild(tGen.GetTemplateRatios(doc, ciid, afs_ids));
                reportRoot.AppendChild(tGen.GetDefinitionOfRatios(doc, afs_ids));
            }
            //Add recent enquiries
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FORecentEnquiries")))) {
                reportRoot.AppendChild(GetRecentEnquiries(doc, ciid));
            }

            try
            {
                scorBuilder.Build();
            }
            catch { }
            this.BindSummaruInformation(companySummary, doc);
            //	AddEndOfReport(doc, reportRoot);			
        }

        private void BindSummaruInformation(XmlNode summary, XmlDocument doc)
        {
            var element = XmlHelper.CreateElement(doc, "FinancialHighlights", "title", rm.GetString("txtFinancialHighlights", ci));
            XmlNodeList list = doc.SelectNodes("//HeadFSI/Years/TableItem");

            summary.AppendChild(element);

            System.Collections.IList years = new System.Collections.Generic.List<string>();

            var yearsElement = XmlHelper.CreateElement(doc, "Years", string.Empty);
            element.AppendChild(yearsElement);
            for (int i = 0; i < list.Count; i++)
            {
                if (i == 2)
                    break;
                years.Add(list[i].Attributes["Year"].InnerText);
                var year = XmlHelper.CreateElement(doc, "Year", list[i].Attributes["Year"].InnerText);
                yearsElement.AppendChild(year);

            }

            list = doc.SelectNodes("//HeadFSI/Currency/TableItem");
            var currency = XmlHelper.CreateElement(doc, "Currency", string.Empty);

            for (int i = 0; i < list.Count; i++)
            {
                if (i == 2)
                    break;
                var item = XmlHelper.CreateElement(doc, "TableItem", "Value", list[i].Attributes["Value"].InnerText, "Year", years[i].ToString());
                currency.AppendChild(item);
            }

            element.AppendChild(currency);

            BindSummaryNode(doc, element, years, "//IssuedShareCapital/TableItem", "IssuedCapital", "txtIssuedCapital");
            BindSummaryNode(doc, element, years, "//ProfitLossAccount/Turnover/TableItem", "Turnover", "txtFHITurnover");
            BindSummaryNode(doc, element, years, "//AssetsTotal/AssetsTotal/TableItem", "AssetsTotal", "txtAssetsTotal");
            BindSummaryNode(doc, element, years, "//EquityTotal/TableItem", "EquityTotal", "txtEquityTotal");
            BindSummaryNode(doc, element, years, "//IncomeFromOperationsBeforeTax/TableItem", "IncomeFromOperationsBeforeTax", "txtFHIPreTaxProfit");


            var noInfo = doc.CreateAttribute("Info");
            element.Attributes.Append(noInfo);

            if (years.Count == 0)
            {
                noInfo.InnerText = "NoInfo";
            }

        }

        private void BindSummaryNode(XmlDocument doc, XmlNode summary, System.Collections.IList years, string xPath, string nodeName, string label)
        {
            XmlNodeList list = doc.SelectNodes(xPath);
            if (list != null)
            {
                XmlElement element = XmlHelper.CreateElement(doc, nodeName, "title", rm.GetString(label, ci));
                summary.AppendChild(element);
                for (int i = 0; i < list.Count; i++)
                {
                    if (i == 2)
                        break;
                    var el = XmlHelper.CreateElement(doc, "TableItem", "Value", list[i].Attributes["Value"].InnerText, "year", years[i].ToString());
                    element.AppendChild(el);
                }
            }
        }

        private XmlNode GetCompanySummary(ReportCompanyBLLC theCompany, XmlDocument doc)
        {
            XmlElement summary = XmlHelper.GetXmlTable(doc, "CompanySummary", "tblCompanySummary");
            XmlHelper.AddAttribute(doc, summary, "title", rm.GetString("txtCompanySummary", ci));

            XmlElement  element;
            if (!string.IsNullOrEmpty(theCompany.VAT))
            {
                element = XmlHelper.CreateElement(doc, "VatNumber", "title", rm.GetString("txtVatNumber", ci));
                element.InnerText = theCompany.VAT;
                summary.AppendChild(element);

                // Add VAT status:
                var statusTag = XmlHelper.CreateElement(doc, "Status", theCompany.Status);
                summary.AppendChild(statusTag);
            }

            CPIFactory factory = new CPIFactory();
            var ds = factory.GetAnnualReturAndAccountsDatesAsDataSet(theCompany.CompanyCIID);
            if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow rw in ds.Tables[0].Rows)
                {
                    var docType = rw["DocumentType"].ToString();
                    string key = string.Empty;
                    string label = string.Empty;

                    string value = null;

                    if (rw["DataArchivedPub"] != null)
                        value = DateTime.Parse(rw["DataArchivedPub"].ToString()).ToShortDateString();
                    switch (docType)
                    {
                        case "Annual Return":
                            key = "LastAnnualReturnFiled";
                            label = rm.GetString("txtLastAnnualReturnFiled", ci);
                            break;
                        case "Accounts":
                            key = "LastAccountsFiled";
                            label = rm.GetString("txtLastAccountsFiled", ci);
                            break;

                    }
                    if (!string.IsNullOrEmpty(value) && !string.IsNullOrEmpty(key))
                    {
                        element = XmlHelper.CreateElement(doc, key, "title", label);
                        element.InnerText = value;
                        summary.AppendChild(element);
                    }
                }
            }
            return summary;  
        }

        
        private void AddOtherInformation(XmlDocument doc, XmlNode reportRoot, string nationalID, string name) {
            XmlElement otherInfo = XmlHelper.GetXmlTable(doc, "OtherInformation", "tblOtherInformation");

            DataSet ds = mtFactory.GetOtherInformation(nationalID, name);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                //Add column header
                XmlElement header = doc.CreateElement("Header");
                XmlHelper.AddAttribute(doc, header, "Number", "#");
                XmlHelper.AddAttribute(doc, header, "DecreeNumber", rm.GetString("txtDecreeNumber", ci));
                XmlHelper.AddAttribute(doc, header, "Source", rm.GetString("txtSource", ci));
                XmlHelper.AddAttribute(doc, header, "Identity", rm.GetString("txtIdentity", ci));
                XmlHelper.AddAttribute(doc, header, "Name", rm.GetString("txtName", ci));
                XmlHelper.AddAttribute(doc, header, "SonDaughter", rm.GetString("txtSonDaughter", ci));
                XmlHelper.AddAttribute(doc, header, "PlaceOfBirth", rm.GetString("txtPlaceOfBirth", ci));
                XmlHelper.AddAttribute(doc, header, "Type", rm.GetString("txtType", ci));
                otherInfo.AppendChild(header);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    XmlHelper.AddAttribute(doc, row, "Number", (i + 1) + ". ");
                    XmlHelper.AddAttribute(doc, row, "DecreeNumber", ds.Tables[0].Rows[i]["decree_nr"].ToString());
                    XmlHelper.AddAttribute(doc, row, "Source", ds.Tables[0].Rows[i]["inf_source"].ToString());
                    XmlHelper.AddAttribute(doc, row, "Identity", ds.Tables[0].Rows[i]["ident"].ToString());
                    XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["surname"].ToString());
                    XmlHelper.AddAttribute(doc, row, "SonDaughter", ds.Tables[0].Rows[i]["son_daughterof"].ToString());
                    XmlHelper.AddAttribute(doc, row, "PlaceOfBirth", ds.Tables[0].Rows[i]["birthplace"].ToString());
                    XmlHelper.AddAttribute(doc, row, "Type", ds.Tables[0].Rows[i]["type"].ToString());
                    otherInfo.AppendChild(row);
                }
            } else {
                XmlElement row = XmlHelper.GetXmlRow(doc);
                XmlHelper.AddAttribute(doc, row, "Number", rm.GetString("txtNoRecordsFound", ci));
                otherInfo.AppendChild(row);
            }
            reportRoot.AppendChild(otherInfo);
        }

        private XmlElement GetMTCompanyState(XmlDocument doc, string nationalID) {
            XmlElement companyState = XmlHelper.GetXmlTable(doc, "CompanyState", "tblCompanyState");
            XmlHelper.AddAttribute(doc, companyState, "title", rm.GetString("txtCompanyState", ci));

            string state = mtFactory.GetMTCompanyState(nationalID);
            if (state == null) {
                companyState.AppendChild(
                    XmlHelper.CreateElement(doc, "State", "value", rm.GetString("txtNoCompanyStateInfoAvailable", ci)));
            } else {
                companyState.AppendChild(XmlHelper.CreateElement(doc, "State", "value", state));
            }
            return companyState;
        }

        private XmlElement GetMTHasCourtOrders(XmlDocument doc, string nationalID) {
            XmlElement hasCourtOrders = XmlHelper.GetXmlTable(doc, "HasCourtOrders", "tblHasCourtOrders");
            XmlHelper.AddAttribute(doc, hasCourtOrders, "title", rm.GetString("txtHasCourtOrders", ci));

            if (mtFactory.HasCourtOrders(nationalID)) {
                hasCourtOrders.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "State",
                        "value",
                        rm.GetString("txtIndividualHasCourtOrdersRegisteredNoCompanyStateInfoAvailable", ci)));
            } else {
                hasCourtOrders.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "State", "value", rm.GetString("txtNoCourtOrdersRegisteredForIndividual", ci)));
            }
            return hasCourtOrders;
        }

        private XmlElement GetMTCapital(XmlDocument doc, string nationalID) {
            XmlElement capital = XmlHelper.GetXmlTable(doc, "Capital", "tblCapital");
            XmlHelper.AddAttribute(doc, capital, "title", rm.GetString("txtCapital", ci));

            MTCapitalBLLC capitalBLLC = mtFactory.GetMTCapital(nationalID);

            if (capitalBLLC != null && capitalBLLC.CompanyCIID > -1) {
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "Nominal",
                        "title",
                        rm.GetString("txtNominal", ci),
                        "value",
                        capitalBLLC.NominalNumberOfShares + " " + capitalBLLC.CurrencyCode));
                if (capitalBLLC.NominalNumberOfShares > 0) {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "Issued",
                            "title",
                            rm.GetString("txtIssued", ci),
                            "value",
                            formatNumber(capitalBLLC.IssuedNumberOfShares/capitalBLLC.NominalNumberOfShares)));
                } else {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc, "Issued", "title", rm.GetString("txtIssued", ci), "value", formatNumber(0)));
                }
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "IssuedTimesNominal",
                        "title",
                        "IssuedXNominal",
                        "value",
                        formatNumber(capitalBLLC.IssuedNumberOfShares)));
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "NumberOfShares", "title", rm.GetString("txtNumberOfShares", ci), "value", "0"));
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "ShareCapital", "title", rm.GetString("txtShareCapital", ci), "value", "0"));
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "Asked", "title", rm.GetString("txtAsked", ci), "value", capitalBLLC.Asked.ToString()));
                if (capitalBLLC.NominalNumberOfShares > 0) {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc,
                            "Authorized",
                            "title",
                            rm.GetString("txtAuthorized", ci),
                            "value",
                            formatNumber(capitalBLLC.AuthorizedCapital/capitalBLLC.NominalNumberOfShares)));
                } else {
                    capital.AppendChild(
                        XmlHelper.CreateElement(
                            doc, "Authorized", "title", rm.GetString("txtAuthorized", ci), "value", formatNumber(0)));
                }
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "AuthTimesNominal",
                        "title",
                        "AuthXNominal",
                        "value",
                        formatNumber(capitalBLLC.AuthorizedCapital)));
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "PaidUp", "title", rm.GetString("txtPaidUp", ci), "value", capitalBLLC.PaidUp + " %"));
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc,
                        "ShareDescription",
                        "title",
                        rm.GetString("txtShareDescription", ci),
                        "value",
                        capitalBLLC.SharesDescription));
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "ShareClass", "title", rm.GetString("txtShareClass", ci), "value", capitalBLLC.ShareClass));
            } else {
                capital.AppendChild(
                    XmlHelper.CreateElement(
                        doc, "NoResult", "title", "", "value", rm.GetString("txtNoInformationAvailable", ci)));
            }

            return capital;
        }

        public string formatNumber(decimal number) {
            var fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            fInfo.NumberDecimalDigits = 0;
            return number.ToString("N", fInfo);
        }

        private XmlElement GetMTCompanyInvolvements(XmlDocument doc, string nationalID, bool isCompany) {
            XmlElement involvements = XmlHelper.GetXmlTable(doc, "CompanyInvolvements", "tblCompanyInvolvements");
            XmlHelper.AddAttribute(doc, involvements, "title", rm.GetString("txtCompanyInvolvements", ci));

            var ds = mtFactory.GetMTCompanyInvolvements(nationalID, isCompany);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                //Add column header
                XmlElement header = doc.CreateElement("Header");
                XmlHelper.AddAttribute(doc, header, "Company", rm.GetString("txtCompany", ci));
                XmlHelper.AddAttribute(doc, header, "Position", rm.GetString("txtPosition", ci).Replace(":", ""));
                XmlHelper.AddAttribute(doc, header, "Status", rm.GetString("txtStatus", ci));
                involvements.AppendChild(header);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    XmlHelper.AddAttribute(doc, row, "ID", ds.Tables[0].Rows[i]["Registration_No"].ToString());
                    XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["Company_Name"].ToString());
                    XmlHelper.AddAttribute(doc, row, "Position", ds.Tables[0].Rows[i]["Involvement"].ToString());
                    XmlHelper.AddAttribute(doc, row, "Status", ds.Tables[0].Rows[i]["State"].ToString());
                    involvements.AppendChild(row);
                }
            } else {
                XmlElement row = XmlHelper.GetXmlRow(doc);
                XmlHelper.AddAttribute(doc, row, "Name", rm.GetString("txtNoCompanyInvolvementsFoundFor", ci));
                involvements.AppendChild(row);
            }
            return involvements;
        }

        private XmlElement GetMTPendingCases(XmlDocument doc, string nationalID) {
            XmlElement pCases = XmlHelper.GetXmlTable(doc, "PendingCases", "tblPendingCases");
            XmlHelper.AddAttribute(doc, pCases, "title", rm.GetString("txtPendingCases", ci));

            DataSet ds = mtFactory.GetMTPendingCases(nationalID);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    XmlHelper.AddAttribute(doc, row, "CIReferenceText", rm.GetString("txtCIReference", ci));
                    XmlHelper.AddAttribute(doc, row, "CIReference", ds.Tables[0].Rows[i]["ID"].ToString());
                    XmlHelper.AddAttribute(doc, row, "CaseNumberText", rm.GetString("txtCase", ci));
                    XmlHelper.AddAttribute(doc, row, "CaseNumber", ds.Tables[0].Rows[i]["WritNumber"].ToString());
                    XmlHelper.AddAttribute(doc, row, "ClaimType", ds.Tables[0].Rows[i]["WritNumber"].ToString());

                    XmlHelper.AddAttribute(doc, row, "InfoSourceText", rm.GetString("txtInformationSource", ci));
                    XmlHelper.AddAttribute(doc, row, "AdjudicatorText", rm.GetString("txtAdjudicator", ci));
                    if (nativeCult) {
                        XmlHelper.AddAttribute(doc, row, "InfoSource", ds.Tables[0].Rows[i]["CourtNative"].ToString());
                        XmlHelper.AddAttribute(
                            doc, row, "Adjudicator", ds.Tables[0].Rows[i]["AdjudicatorNative"].ToString());
                    } else {
                        XmlHelper.AddAttribute(doc, row, "InfoSource", ds.Tables[0].Rows[i]["CourtEN"].ToString());
                        XmlHelper.AddAttribute(
                            doc, row, "Adjudicator", ds.Tables[0].Rows[i]["AdjudicatorEN"].ToString());
                    }
                    XmlHelper.AddAttribute(doc, row, "ClaimOwnerText", rm.GetString("txtClaimOwner", ci));
                    XmlHelper.AddAttribute(doc, row, "ClaimOwner", ds.Tables[0].Rows[i]["PlaintiffName"].ToString());
                    XmlHelper.AddAttribute(doc, row, "DateText", rm.GetString("txtDateRegistered", ci));
                    XmlHelper.AddAttribute(
                        doc,
                        row,
                        "Date",
                        DateTime.Parse(ds.Tables[0].Rows[i]["DateOfWrit"].ToString()).ToShortDateString());
                    XmlHelper.AddAttribute(doc, row, "AmountText", rm.GetString("txtValueOfSuit", ci));
                    XmlHelper.AddAttribute(doc, row, "Amount", string.Format("{0:0.00}", ds.Tables[0].Rows[i]["ValueOfSuit"]));
                    XmlHelper.AddAttribute(doc, row, "CurrencyCode", ds.Tables[0].Rows[i]["CurrencyCode"].ToString());
                    pCases.AppendChild(row);
                }
            } else {
                pCases.AppendChild(
                    XmlHelper.CreateElement(doc, "NoInfo", "value", rm.GetString("txtNoRegistrations", ci)));
            }
            return pCases;
        }

        protected XmlElement GetMTClaims(XmlDocument doc, int companyCIID,  bool CourtOrders) {
            XmlElement claims = XmlHelper.GetXmlTable(doc, "Claims", "tblClaims");

            if (CourtOrders) {
                XmlHelper.AddAttribute(doc, claims, "title", rm.GetString("txtHasCourtOrders", ci));
            } else {
                XmlHelper.AddAttribute(doc, claims, "title", rm.GetString("txtCourtInfoAndDefaultingDebts", ci) + " " + rm.GetString("txtCourtInfoAndDefaultingDebtsExtraText", ci));
                XmlHelper.AddAttribute(doc, claims, "titleExtended",  rm.GetString("txtCourtInfoAndDefaultingDebtsExtraText", ci));
                var curtDetailLabel = rm.GetString("txtOtherCourtInformationDetail", ci);
                if (!string.IsNullOrEmpty(curtDetailLabel))
                {
                    claims.AppendChild(
                                     XmlHelper.CreateElement(doc, "OtherCourtInformation", "title",
                                     rm.GetString("txtOtherCourtInformation", ci), "value", string.Format(curtDetailLabel,
                                     string.Empty)));
                }
                //XmlHelper.CreateElement(doc, "PendingCases", "title", rm.GetString("txtCourtInfoAndDefaultingDebtsExtraText", ci), "10");
            }
            
            DataSet ds = mtFactory.GetMTClaims(companyCIID, CourtOrders);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                 claims.AppendChild(
                    XmlHelper.CreateElement(doc, "Info", "title", rm.GetString("txtRegistrations", ci), "value", "Yes"));
            }

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                var formatter = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
                formatter.PercentDecimalDigits = 2;
                formatter.NumberDecimalDigits = 2;

                var claimOwners = claims.AppendChild(XmlHelper.CreateElement(doc, "Claimants", "title", rm.GetString("txtClaimants")));
                List<string> claimOwnerList = new List<string>();
                //var curtDetailLabel = rm.GetString("txtOtherCourtInformationDetail", ci);
                //if (!string.IsNullOrEmpty(curtDetailLabel))
                //{
                //    claims.AppendChild(
                //                     XmlHelper.CreateElement(doc, "OtherCourtInformation", "title",
                //                     rm.GetString("txtOtherCourtInformation", ci), "value", string.Format(curtDetailLabel, 
                //                     ds.Tables[0].Rows.Count.ToString())));
                //}
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    XmlHelper.AddAttribute(doc, row, "CIReferenceText", rm.GetString("txtCIReference", ci));
                    XmlHelper.AddAttribute(doc, row, "CIReference", ds.Tables[0].Rows[i]["ID"].ToString());
                    XmlHelper.AddAttribute(doc, row, "CaseNumberText", rm.GetString("txtCase", ci));
                    XmlHelper.AddAttribute(doc, row, "CaseNumber", ds.Tables[0].Rows[i]["CaseNr"].ToString());

                    XmlHelper.AddAttribute(doc, row, "InfoSourceText", rm.GetString("txtInformationSource", ci));
                    XmlHelper.AddAttribute(doc, row, "ClaimTypeText", rm.GetString("txtClaimType", ci));
                    if (nativeCult) {
                        XmlHelper.AddAttribute(
                            doc, row, "InfoSource", ds.Tables[0].Rows[i]["InfoSourceNative"].ToString());
                        XmlHelper.AddAttribute(doc, row, "ClaimType", ds.Tables[0].Rows[i]["TypeNative"].ToString());
                        XmlHelper.AddAttribute(
                            doc, row, "ClaimTypeDesc", ds.Tables[0].Rows[i]["DescriptionNative"].ToString());
                    } else {
                        XmlHelper.AddAttribute(doc, row, "InfoSource", ds.Tables[0].Rows[i]["InfoSourceEN"].ToString());
                        XmlHelper.AddAttribute(doc, row, "ClaimType", ds.Tables[0].Rows[i]["TypeEN"].ToString());
                        XmlHelper.AddAttribute(
                            doc, row, "ClaimTypeDesc", ds.Tables[0].Rows[i]["DescriptionEN"].ToString());
                    }
                    XmlHelper.AddAttribute(doc, row, "ClaimOwnerText", rm.GetString("txtClaimOwner", ci));
                    XmlHelper.AddAttribute(
                        doc, row, "ClaimOwner", GetName(int.Parse(ds.Tables[0].Rows[i]["ClaimOwnerCIID"].ToString())));

                  
                    var claimantName = GetName(int.Parse(ds.Tables[0].Rows[i]["ClaimOwnerCIID"].ToString()));
                    if (!claimOwnerList.Exists(x => x == claimantName))
                    {
                        claimOwnerList.Add(claimantName);
                    }
                    XmlHelper.AddAttribute(doc, row, "DateText", rm.GetString("txtDate", ci));
                    DateTime claimDate;
                    try {
                        claimDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["ClaimDate"]);
                        XmlHelper.AddAttribute(doc, row, "Date", claimDate.ToShortDateString());
                    } catch (Exception) // if error show long date string rather than nothing
                    {
                        XmlHelper.AddAttribute(doc, row, "Date", ds.Tables[0].Rows[i]["ClaimDate"] + "");
                    }

                    if (!CourtOrders) {
                        XmlHelper.AddAttribute(doc, row, "AmountText", rm.GetString("txtAmount", ci));
                        try {
                            XmlHelper.AddAttribute(
                                doc,
                                row,
                                "Amount",
                                float.Parse(ds.Tables[0].Rows[i]["Amount"].ToString()).ToString(formatter) + " " +
                                ds.Tables[0].Rows[i]["CurrencyID"]);
                            XmlHelper.AddAttribute(
                            doc,
                            row,
                            "DDDAmount",
                            float.Parse(ds.Tables[0].Rows[i]["Amount"].ToString()).ToString(formatter));
                        } catch (Exception) {
                            XmlHelper.AddAttribute(doc, row, "Amount", ds.Tables[0].Rows[i]["Amount"] + "");
                        }
                    }
                    claims.AppendChild(row);
                }
                foreach (var claimant in claimOwnerList)
                    claimOwners.AppendChild(XmlHelper.CreateElement(doc, "Claimant", claimant));
            } 
            else 
            {
                claims.AppendChild(
                    XmlHelper.CreateElement(doc, "NoInfo", "value", rm.GetString("txtNoRegistrations", ci)));
            }
            return claims;
        }

        protected XmlElement GetMTClaimsBasic(XmlDocument doc, int companyCIID) {
            XmlElement claims = XmlHelper.GetXmlTable(doc, "BasicClaims", "tblBasicClaims");
            XmlHelper.AddAttribute(doc, claims, "title", "");

            XmlElement npi = doc.CreateElement("NPI");
            XmlHelper.AddAttribute(doc, npi, "title", rm.GetString("txtNPIStatus", ci));

            DataSet ds = mtFactory.GetMTClaimsBasic(companyCIID);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                XmlHelper.AddAttribute(doc, npi, "value", "YES (Caution!)");
            } else {
                XmlHelper.AddAttribute(doc, npi, "value", "No Records");
//				claims.AppendChild(XmlHelper.CreateElement(doc, "value", "No Records", rm.GetString("txtNPIStatus",ci)));			
            }
            claims.AppendChild(npi);
            return claims;
        }

        private XmlElement GetMTDirectorsAndMore(XmlDocument doc, string nationalID)
        {
            XmlElement involvements = XmlHelper.GetXmlTable(doc, "DirectorsAndMore", "tblDirectorsAndMore");

            XmlHelper.AddAttribute(doc, involvements, "title", rm.GetString("txtCompanyOwnership", ci));

            XmlElement directors = doc.CreateElement("Directors");
            XmlHelper.AddAttribute(doc, directors, "title", rm.GetString("txtDirectors", ci));

            XmlElement shareholders = doc.CreateElement("Shareholders");
            XmlHelper.AddAttribute(doc, shareholders, "title", rm.GetString("txtOwnersShareholders", ci));
            XmlHelper.AddAttribute(doc, shareholders, "title1", rm.GetString("txtTotalEquity", ci));


            XmlElement legalrepresentative = doc.CreateElement("LegalRepresentative");
            XmlHelper.AddAttribute(doc, legalrepresentative, "title", rm.GetString("txtLegalRepresentative", ci));

            XmlElement judicialRepresentative = doc.CreateElement("JudicialRepresentative");
            XmlHelper.AddAttribute(doc, judicialRepresentative, "title", rm.GetString("txtJudicialRepresentative", ci));

            XmlElement secretary = doc.CreateElement("Secretary");
            XmlHelper.AddAttribute(doc, secretary, "title", rm.GetString("txtBoardSecretary", ci));

            involvements.AppendChild(directors);
            involvements.AppendChild(shareholders);
            involvements.AppendChild(legalrepresentative);
            involvements.AppendChild(judicialRepresentative);
            involvements.AppendChild(secretary);

            DataSet ds = mtFactory.GetMTDirectorsAndMore(nationalID);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    if ((ds.Tables[0].Rows[i]["ip_co_reg_no"] == null ||
                         ds.Tables[0].Rows[i]["ip_co_reg_no"].ToString().Trim() == "") &&
                        (ds.Tables[0].Rows[i]["ip_co_name"] == null ||
                         ds.Tables[0].Rows[i]["ip_co_name"].ToString().Trim() == ""))
                    {
                        //Individual
                        XmlHelper.AddAttribute(
                            doc, row, "Name", ds.Tables[0].Rows[i]["IP_Surname"] + " " + ds.Tables[0].Rows[i]["IP_Name"]);
                        string passNo = ds.Tables[0].Rows[i]["IP_Passport_No"].ToString();
                        if (passNo != null && passNo.Trim().Length > 0)
                        {
                            XmlHelper.AddAttribute(doc, row, "PassNo", passNo);
                        }
                        XmlHelper.AddAttribute(doc, row, "ID", ds.Tables[0].Rows[i]["IP_ID_No"].ToString());
                    }
                    else
                    {
                        //Company
                        XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["IP_Co_Name"].ToString());
                        if (ds.Tables[0].Rows[i]["IP_Co_Reg_No"] != null &&
                            ds.Tables[0].Rows[i]["IP_Co_Reg_No"].ToString() != "")
                        {
                            XmlHelper.AddAttribute(doc, row, "ID", ds.Tables[0].Rows[i]["IP_Co_Reg_No"].ToString());
                        }
                    }
                    XmlHelper.AddAttribute(doc, row, "Address1", ds.Tables[0].Rows[i]["IP_Address_1"].ToString());
                    XmlHelper.AddAttribute(doc, row, "Address2", ds.Tables[0].Rows[i]["IP_Address_2"].ToString());
                    XmlHelper.AddAttribute(doc, row, "Country", ds.Tables[0].Rows[i]["Country"].ToString());
                    if (ds.Tables[0].Rows[i]["Locality"] != null && ds.Tables[0].Rows[i]["Locality"].ToString() != "")
                    {
                        XmlHelper.AddAttribute(doc, row, "City", ds.Tables[0].Rows[i]["Locality"].ToString());
                    }
                    else
                    {
                        XmlHelper.AddAttribute(doc, row, "City", ds.Tables[0].Rows[i]["locality_id"].ToString());
                    }
                    switch (int.Parse(ds.Tables[0].Rows[i]["Ordering"].ToString()))
                    {
                        case 1:
                            directors.AppendChild(row);
                            break;
                        case 2:
                            string value = ds.Tables[0].Rows[i]["TotalEquity"].ToString();
                            if (!string.IsNullOrEmpty(value))
                            {
                                //if (value.StartsWith("0"))
                                //{
                                //    var result = decimal.Parse(value);
                                //    var str = result.ToString();
                                //    if (str.LastIndexOf('.') != -1)
                                //    {
                                //        var parsedPercentage = str.Substring(0, str.LastIndexOf('.') + 3);
                                //        if (parsedPercentage == "0.00")
                                //        {
                                //            var firstDigitPos = 0;
                                //            for (int j = 0; j < str.Length; j++)
                                //            {
                                //                if (str[j] != '0' && str[j] != '.')
                                //                {
                                //                    firstDigitPos = j;
                                //                    break;
                                //                }
                                //            }
                                //            XmlHelper.AddAttribute(doc, row, "TotalEquity", str.Substring(0, firstDigitPos + 2) + "%");
                                //        }
                                //        else
                                //            XmlHelper.AddAttribute(doc, row, "TotalEquity", parsedPercentage + "%");

                                //    }                                       
                                //    else
                                //        XmlHelper.AddAttribute(doc, row, "TotalEquity", str + "%");
                                //}
                                //else
                                //{
                                    var result = decimal.Parse(value);
                                    var str = result.ToString();
                                    if(str.LastIndexOf('.') != -1)  
                                         XmlHelper.AddAttribute(doc, row, "TotalEquity", str.Substring(0, str.LastIndexOf('.') + 5) + "%");
                                    else
                                        XmlHelper.AddAttribute(doc, row, "TotalEquity", str + "%");
                                //}
                            }
                            shareholders.AppendChild(row);
                            break;
                        case 4:
                            legalrepresentative.AppendChild(row);
                            break;
                        case 5:
                            judicialRepresentative.AppendChild(row);
                            break;
                        case 3:
                            secretary.AppendChild(row);
                            break;
                    }
                }
            }
            else
            {
                XmlElement row = XmlHelper.GetXmlRow(doc);
                XmlHelper.AddAttribute(doc, row, "Name", rm.GetString("txtNoInformationAvailable", ci));
                involvements.AppendChild(row);
            }
            return involvements;
        }
    }
}
