<%@ Page language="c#" Codebehind="FoFinancialReport.aspx.cs" AutoEventWireup="false" Inherits="CR.FoFinancialReport" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Creditinfo Group Ltd.</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta http-equiv="expires" content="-1">
		<meta http-equiv="pragma" content="no-cache">
		<meta content="Lánstraust" name="description">
		<meta content="Lánstraust Brautarholti 10-14. S: 550-9600" name="author">
	</HEAD>
	<BODY>
		<form id="Form1" method="post" runat="server">
			<uc1:language id="Language1" runat="server" Visible="False"></uc1:language>
			<table width="608">
				<TBODY>
					<TR class="dark-row">
						<TD style="HEIGHT: 10px" align="center" colSpan="8"><asp:label id="lblReport" runat="server"></asp:label></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="8"><asp:label id="lblMessage" runat="server" forecolor="Black" font-bold="True"></asp:label></TD>
					</TR>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
