<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="FoCompanyReport.aspx.cs" AutoEventWireup="false" Inherits="CR.FoCompanyReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReportSelection</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						FoCompanyReportForm.btnSearch.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.FoCompanyReportForm.txtEmail.focus();
				}

		</SCRIPT>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="FoCompanyReportForm" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td style="BACKGROUND-IMAGE: url(../img/mainback.gif)"><table cellSpacing="0" cellPadding="0" width="919" align="center" border="0">
							<tr>
								<td class="mainShadeLeft" style="WIDTH: 568px" vAlign="top" align="right" width="568"><IMG alt="" src="../img/spacer.gif" width="6"></td>
								<td>
									<table cellSpacing="0" cellPadding="0" width="919" align="center" border="0">
										<tr>
											<td class="PageTitle" bgColor="#951e16" colSpan="3">
												<table borderColor="#0" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<tr>
														<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
														<td align="right"><uc1:language id="Language1" runat="server"></uc1:language></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td style="WIDTH: 568px" vAlign="top" width="568" bgColor="#ced0c5"><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
											<td class="betweensides"></td>
											<td style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px"
												vAlign="top" width="766" bgColor="#ffffff">
												<table cellSpacing="0" cellPadding="2" width="700" border="0">
													<tr>
														<td>
															<!-- Contenti� byrjar -->
															<TABLE id="tbDefault" style="WIDTH: 678px; HEIGHT: 398px" borderColor="#0000ff" cellSpacing="0"
																cellPadding="0" width="678" align="center">
																<TR>
																	<td style="WIDTH: 239px; HEIGHT: 17px"></td>
																	<TD style="WIDTH: 224px; HEIGHT: 17px"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="left" colSpan="8"><asp:label id="lblReport" runat="server" CssClass="HeadMain" Width="100%"> Report </asp:label></TD>
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="center" colSpan="8"></TD>
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="center"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="left" colSpan="8"><asp:label id="lblFindReport" runat="server" CssClass="HeadLists">Find report</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
																</TR>
																<TR class="dark-row">
																	<TD style="WIDTH: 491px; HEIGHT: 10px" align="center" colSpan="8"></TD>
																	<TD style="WIDTH: 491px; HEIGHT: 10px" align="center"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 239px; HEIGHT: 13px" align="left"><asp:label id="lblName" runat="server">Name</asp:label></TD>
																	<TD style="WIDTH: 224px; HEIGHT: 13px" align="left"><asp:label id="lblNationalID" runat="server">ID</asp:label></TD>
																</TR>
																<tr>
																	<TD style="WIDTH: 239px; HEIGHT: 15px" align="left"><asp:textbox id="txtName" runat="server" Width="200px"></asp:textbox></TD>
																	<TD style="WIDTH: 382px; HEIGHT: 15px" align="left" colSpan="3"><asp:textbox id="txtNationalID" runat="server" Width="137px"></asp:textbox></TD>
																	<TD style="WIDTH: 416px; HEIGHT: 15px" align="left"></TD>
																</tr>
																<TR>
																	<TD style="WIDTH: 491px; HEIGHT: 10px" align="center" colSpan="8"></TD>
																	<TD style="WIDTH: 491px; HEIGHT: 10px" align="center"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 445px; HEIGHT: 25px" align="left" colSpan="2">
																		<table>
																			<tr>
																				<td>
																					<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnSearch" runat="server" CssClass="RegisterButton" Text="Search"></asp:button></DIV>
																				</td>
																				<td>
																					<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnClear" runat="server" CssClass="RegisterButton" Text="Clear"></asp:button></DIV>
																				</td>
																			</tr>
																		</table>
																	</TD>
																	<TD style="WIDTH: 602px; HEIGHT: 25px" align="left"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 491px; HEIGHT: 18px" align="left" colSpan="8">&nbsp;&nbsp;&nbsp;</TD>
																	<TD style="WIDTH: 491px; HEIGHT: 18px" align="left"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 491px; HEIGHT: 18px" align="left" colSpan="8"><asp:label id="lblSearchResult" runat="server" CssClass="HeadLists">Search Result</asp:label></TD>
																	<TD style="WIDTH: 491px; HEIGHT: 18px" align="left"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 491px; HEIGHT: 13px" align="left" colSpan="8"></TD>
																	<TD style="WIDTH: 491px; HEIGHT: 13px" align="left"></TD>
																</TR>
																<TR id="trCompanyInfoRow" runat="server">
																	<TD style="WIDTH: 445px; HEIGHT: 165px" vAlign="top" align="left" colSpan="2">
																		<table style="WIDTH: 408px; HEIGHT: 94px">
																			<TR>
																				<TD vAlign="top"><asp:label id="lblResultNationalID" runat="server">ResultNationalID</asp:label></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 15px" vAlign="top"><asp:label id="lblResultName" runat="server">ResultName</asp:label></TD>
																			</TR>
																			<TR>
																				<TD vAlign="top"><asp:label id="lblResultAddress" runat="server">ResultAddress</asp:label></TD>
																			</TR>
																			<TR>
																				<TD vAlign="top"><asp:label id="lblResultCity" runat="server">ResultCity</asp:label></TD>
																			</TR>
																			<TR>
																				<TD vAlign="top"></TD>
																			</TR>
																			<TR>
																				<TD vAlign="top"><asp:label id="lblOperation" runat="server" Font-Bold="True">Operation</asp:label>&nbsp;
																					<asp:label id="lblResultOperation" runat="server">ResultOperation</asp:label></TD>
																			</TR>
																			<TR>
																				<TD vAlign="top"><asp:label id="lblNACE" runat="server" Font-Bold="True">Nace</asp:label>&nbsp;
																					<asp:label id="lblResultNACE" runat="server">ResultNace</asp:label></TD>
																			</TR>
																		</table>
																	</TD>
																	<TD style="WIDTH: 644px; HEIGHT: 165px" vAlign="top" align="left">
																		<TABLE id="Table2" style="WIDTH: 219px; HEIGHT: 130px">
																			<TR>
																				<TD id="trBasicRow" runat="server">
																					<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnBasicReport" runat="server" CssClass="RegisterButton" Text="Basic Report"></asp:button></DIV>
																				</TD>
																			</TR>
																			<TR>
																				<TD id="trCompanyRow" runat="server">
																					<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnCompanyReport" runat="server" CssClass="RegisterButton" Text="Company Report"></asp:button></DIV>
																				</TD>
																			</TR>
																			<TR>
																				<TD id="trCreditInfoRow" runat="server">
																					<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnCreditInfoReport" runat="server" CssClass="RegisterButton" Text="CreditInfo Report"></asp:button></DIV>
																				</TD>
																			</TR>
																			<TR>
																				<TD id="trOrderRow" runat="server">
																					<table>
																						<tr>
																							<td><asp:label id="lblOrderType" runat="server">Label</asp:label></td>
																						</tr>
																						<tr>
																							<td><asp:dropdownlist id="ddlDeliverySpeed" runat="server" Width="152px"></asp:dropdownlist></td>
																						</tr>
																						<tr>
																							<td>
																								<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnOrderReport" runat="server" CssClass="RegisterButton" Text="Order Report"></asp:button></DIV>
																							</td>
																						</tr>
																					</table>
																				</TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="WIDTH: 644px; HEIGHT: 165px" align="left" colSpan="2"></TD>
																	<TD style="WIDTH: 602px; HEIGHT: 165px" align="left"></TD>
																</TR>
																<TR id="trOrderReceipe" runat="server">
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="left" colSpan="8">
																		<TABLE id="Table3" style="WIDTH: 648px; HEIGHT: 94px">
																			<TR>
																				<TD style="WIDTH: 120px" vAlign="top"><asp:label id="lblRecNationalIDLabel" runat="server">ID</asp:label></TD>
																				<TD vAlign="top"><asp:label id="lblRecNationalID" runat="server">Label</asp:label></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 120px; HEIGHT: 15px" vAlign="top"><asp:label id="lblRecCreditInfoIDLabel" runat="server">CreditInfo ID</asp:label></TD>
																				<TD style="HEIGHT: 15px" vAlign="top"><asp:label id="lblRecCreditInfoID" runat="server">Label</asp:label></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 120px; HEIGHT: 7px" vAlign="top"><asp:label id="lblRecNameLabel" runat="server">Name</asp:label></TD>
																				<TD style="HEIGHT: 7px" vAlign="top"><asp:label id="lblRecName" runat="server">Label</asp:label></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 120px" vAlign="top"></TD>
																				<TD vAlign="top"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 120px" vAlign="top" colSpan="3"><asp:label id="lblOrderReceived" runat="server" Font-Bold="True">Order received</asp:label></TD>
																				<TD vAlign="top"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="left" colSpan="8"><asp:label id="lblNoCompanyFound" runat="server" ForeColor="Red">No company found</asp:label></TD>
																	<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
																</TR>
																<TR id="trSearchGridRow" runat="server">
																	<TD style="WIDTH: 621px" colSpan="4">
																		<TABLE id="Table1" style="WIDTH: 408px; HEIGHT: 158px">
																			<TR>
																				<TD><asp:datagrid id="dtgrCompanys" runat="server" Width="600px" Font-Size="Smaller" GridLines="Vertical"
																						CellPadding="4" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE"
																						AutoGenerateColumns="False" ForeColor="Black">
																						<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
																						<AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
																						<ItemStyle ForeColor="Black" BackColor="White"></ItemStyle>
																						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#895A4F"></HeaderStyle>
																						<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
																						<Columns>
																							<asp:ButtonColumn Text="Select" HeaderText="Select" CommandName="Select"></asp:ButtonColumn>
																							<asp:BoundColumn DataField="NameNative" SortExpression="NameNative" HeaderText="Name"></asp:BoundColumn>
																							<asp:BoundColumn DataField="NameEN" SortExpression="NameEN" HeaderText="Name (EN)"></asp:BoundColumn>
																							<asp:BoundColumn DataField="Number" SortExpression="Number" HeaderText="ID"></asp:BoundColumn>
																							<asp:BoundColumn DataField="StreetNative" SortExpression="StreetNative" HeaderText="Address"></asp:BoundColumn>
																							<asp:BoundColumn DataField="StreetEN" SortExpression="StreetEN" HeaderText="Address (EN)"></asp:BoundColumn>
																							<asp:BoundColumn DataField="PostalCode" HeaderText="Post code"></asp:BoundColumn>
																							<asp:BoundColumn DataField="CityNative" SortExpression="CityNative" HeaderText="City"></asp:BoundColumn>
																							<asp:BoundColumn DataField="CityEN" SortExpression="CityEN" HeaderText="City (EN)"></asp:BoundColumn>
																						</Columns>
																						<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
																					</asp:datagrid></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="WIDTH: 602px"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 239px; HEIGHT: 25px" align="left"></TD>
																	<TD style="WIDTH: 224px; HEIGHT: 25px" align="left"></TD>
																	<TD style="HEIGHT: 25px" align="left"></TD>
																</TR>
																<tr>
																	<td style="WIDTH: 491px; HEIGHT: 15px" align="center" bgColor="#951e16" colSpan="8"></td>
																	<TD style="HEIGHT: 15px" align="center" bgColor="#951e16"></TD>
																	<TD style="HEIGHT: 15px" align="center" bgColor="#951e16"></TD>
																	<TD style="HEIGHT: 15px" align="center" bgColor="#951e16"></TD>
																</tr>
																<TR class="dark-row">
																	<TD style="WIDTH: 491px; HEIGHT: 10px" align="center" colSpan="8"></TD>
																	<TD style="HEIGHT: 10px" align="center"></TD>
																	<TD style="HEIGHT: 10px" align="center"></TD>
																	<TD style="HEIGHT: 10px" align="center"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 239px; HEIGHT: 10px" align="left">
																	</TD>
																	<TD style="WIDTH: 224px; HEIGHT: 15px" align="center"></TD>
																	<TD style="HEIGHT: 15px" align="center"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 491px" align="center" colSpan="8"></TD>
																	<TD align="center"></TD>
																	<TD align="center"></TD>
																	<TD align="center"></TD>
																</TR>
															</TABLE>
															<!-- Contenti� endar --></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td style="WIDTH: 568px" width="568" bgColor="#e2e2e2" height="1"><img height="1" src="" width="153"></td>
											<td><img height="1" src="" width="10"></td>
											<td width="766" bgColor="#e2e2e2" height="1"><img height="1" src="" width="758"></td>
										</tr>
									</table>
								</td>
								<td class="mainShadeRight"><IMG alt="" src="img/spacer.gif" width="6"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer2" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
