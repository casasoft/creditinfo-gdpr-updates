#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using CR.DAL;
using CR.MTBLL;
using Logging.BLL;
using cb3;

#endregion

using Cig.Framework.Base.Configuration;

using System.Collections.Generic;

namespace CR.MTDAL {
    public class MTCompanyReportsDALC : CompanyReportsDALC {
        public MTCompanyReportsDALC() { className = "MTCompanyReportsDALC"; }

        public DataSet GetOtherInformation(string nationalID, string name) {
            var dsOtherInformation = new DataSet();
            const string funcName = "GetOtherInformation(string nationalID, string name) ";

            string query;
            if (nationalID.Trim() != "") {
                query = "select * from mt_fo_OtherInformation where ident = '" + nationalID + "' order by surname";
            } else {
                query = "select * from mt_fo_OtherInformation where surname like '%" + name + "%' order by surname";
            }
            try {
                dsOtherInformation = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsOtherInformation;
        }

        public bool HasCourtOrders(string nationalID) {
            const string funcName = "HasCourtOrders(string nationalID) ";
            string query = "select * from mt_fo_OtherInformation where ident = '" + nationalID + "'";
            var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            myOleDbConn.Open();
            var myOleDbCommand = new OleDbCommand(query, myOleDbConn);
            try {
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            } finally {
                if (myOleDbConn != null) {
                    myOleDbConn.Close();
                }
            }
            return false;
        }

        public DataSet GetMTPendingCases(string nationalID) {
            var dsOtherInformation = new DataSet();
            const string funcName = "GetMTPendingCases(string nationalID) ";

            string statusRegistered = ConfigurationSettings.AppSettings.Get("PendingCaseStatusRegistered");
            string query = "select * from mt_v_PendingCases where defendantID = '" + nationalID + "' and StatusID = " +
                           statusRegistered + " order by DateOfWrit desc";
            try {
                dsOtherInformation = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsOtherInformation;
        }

        public DataSet GetMTClaimsBasic(int creditInfoID) {
            var dsClaims = new DataSet();
            const string funcName = "GetMTClaimsBasic(int creditInfoID) ";

            string statusRegistered = ConfigurationSettings.AppSettings.Get("ClaimStatusRegistered");
            string query = "select * from mt_v_Claims where CreditInfoID = " + creditInfoID + " and StatusID = " +
                           statusRegistered;
            try {
                dsClaims = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + " query=\"" + query + "\"", err, true);
            }
            return dsClaims;
        }

        public DataSet GetMTClaims(int creditInfoID, bool getCourtOrders) {
            var dsClaims = new DataSet();
            const string funcName = "GetMTClaims(int creditInfoID) ";

            var isMalta = false;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                isMalta = CigConfig.Configure("lookupsettings.currentVersion").ToLower() == "malta";
            }

            string CourtOrderFilter = "";

            if (isMalta) {
                string validCaseTypes = CigConfig.Configure("lookupsettings.ValidCaseTypes");
                if (getCourtOrders) {
                    CourtOrderFilter = " AND ClaimTypeId IN (" + validCaseTypes + ") ";
                } else {
                    CourtOrderFilter = " AND ClaimTypeId NOT IN (" + validCaseTypes + ") ";
                }
            }

            string statusRegistered = CigConfig.Configure("lookupsettings.ClaimStatusRegistered");
            string query = "select * from mt_v_Claims where CreditInfoID = " + creditInfoID + " and StatusID = " +
                           statusRegistered + CourtOrderFilter + " order by ClaimDate desc";
            try {
                dsClaims = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + " query=\"" + query + "\"", err, true);
            }
            return dsClaims;
        }

        public string GetMTRegistrationForm(string nationalID) {
            const string funcName = "GetMTCapital(int companyCIID)";
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM mt_v_CompanyRegistrationID WHERE Registration_No = '" + nationalID + "'",
                            myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    var res = "";
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            res = reader.GetString(0);
                        }
                        /*if(!reader.IsDBNull(1))
							myCapital.ShareClass = reader.GetString(1);*/
                    }
                    reader.Close();

                    return res;
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public MTCapitalBLLC GetMTCapital(string nationalID) {
            const string funcName = "GetMTCapital(int companyCIID)";
            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    //OleDbCommand myOleDbCommand = new OleDbCommand("SELECT * FROM cpi_Capital WHERE CompanyCIID = " + companyCIID, myOleDbConn);
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM mt_v_Capital WHERE Registration_No = '" + nationalID + "'", myOleDbConn);

                    var reader = myOleDbCommand.ExecuteReader();
                    var myCapital = new MTCapitalBLLC();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            myCapital.CompanyCIID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            myCapital.ShareClass = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            myCapital.AuthorizedCapital = Convert.ToDecimal(reader.GetValue(2));
                        }
                        if (!reader.IsDBNull(3)) {
                            myCapital.NominalNumberOfShares = Convert.ToDecimal(reader.GetDouble(3));
                        }
                        if (!reader.IsDBNull(4)) {
                            myCapital.IssuedNumberOfShares = Convert.ToDecimal(reader.GetValue(4));
                        }
                        if (!reader.IsDBNull(5)) {
                            myCapital.PaidUp = reader.GetString(5) == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(reader.GetString(5));
                        }

                        myCapital.CurrencyCode = !reader.IsDBNull(7) ? reader.GetString(7) : "MTL";

                        /*if(!reader.IsDBNull(3))
							myCapital.Asked = reader.GetDecimal(3);
						
						if(!reader.IsDBNull(6))
							myCapital.SharesDescription = reader.GetString(6);
						if(!reader.IsDBNull(7))
							myCapital.CurrencyCode = reader.GetString(7);
						if(!reader.IsDBNull(8))
							myCapital.CurrencyCode = reader.GetString(8);*/
                    }

                    reader.Close();

                    return myCapital;
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }
    }
}