<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Page language="c#" Codebehind="SendReport.aspx.cs" AutoEventWireup="false" Inherits="CR.SendReportForm" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReportSelection</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						SendReportForm.btnSend.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.SendReportForm.txtEmail.focus();
				}

		</script>
	</HEAD>
	<body ms_positioning="GridLayout">
		<form id="SendReportForm" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblSendReport" runat="server">Send report</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblCompanyCIID" runat="server">Company CIID</asp:label><br>
																		<asp:label id="lblCompanyCIIDVal" runat="server">22</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblSendTo" runat="server">Email</asp:label><br>
																		<asp:textbox id="txtEmail" runat="server"></asp:textbox>
																		<asp:regularexpressionvalidator id="revEmailValidator" runat="server" errormessage="RegularExpressionValidator"
																			controltovalidate="txtEmail" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(;\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*">*</asp:regularexpressionvalidator>
																		<asp:requiredfieldvalidator id="rfvEmail" runat="server" controltovalidate="txtEmail" errormessage="RequiredFieldValidator">*</asp:requiredfieldvalidator>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblSubject" runat="server">Subject</asp:label>
																		<br>
																		<asp:textbox id="txtSubject" runat="server"></asp:textbox>
																	</td>
																</tr>
																<TR>
																	<TD>
																		<asp:Label id="lblSendAsFormat" runat="server"></asp:Label><BR>
																		<asp:DropDownList id="ddlstFileFormat" runat="server"></asp:DropDownList></TD>
																</TR>
																<tr>
																	<td>
																		<asp:label id="lblBody" runat="server">Body</asp:label>
																		<br>
																		<asp:textbox id="txtBody" runat="server" width="100%" height="88px" textmode="MultiLine"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:label id="lblError" runat="server" cssclass="error_text" visible="False">Failed to send report</asp:label>
															<asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary>
														</td>
														<td align="right">
															<asp:button id="btnSend" runat="server" cssclass="confirm_button" text="Send"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
