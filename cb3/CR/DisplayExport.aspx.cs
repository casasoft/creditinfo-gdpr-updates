#region

using System;
using System.Configuration;
using System.IO;
using System.Resources;
using System.Web.UI;
using System.Xml;
using System.Xml.Xsl;
using CR.BLL;
using CR.Localization;
using WebSupergoo.ABCpdf4;

#endregion

using Cig.Framework.Base.Configuration;

namespace CR {
    /// <summary>
    /// Summary description for DisplayExport.
    /// </summary>
    public class DisplayExport : Page {
        private ResourceManager rm;
        private void Page_Load(object sender, EventArgs e) { DisplayReport(); }

        private void DisplayReport() {
            rm = CIResource.CurrentManager;

            string companyId = "";
            string reportFormat = "2";

            if (Session["CompanyCIID"] != null) {
                companyId = Session["CompanyCIID"].ToString();
            }
            if (Session["ReportFormat"] != null) {
                reportFormat = Session["ReportFormat"].ToString();
            }

            try {
                if (reportFormat.Equals("3")) //HTML
                {
                    CreateHTML(int.Parse(companyId));
                } else if (reportFormat.Equals("1")) //XML
                {
                    CreateXML(int.Parse(companyId));
                } else if (reportFormat.Equals("2")) //PDF
                {
                    CreatePDF(int.Parse(companyId));
                }
                /*else if(reportFormat.Equals("4")) //EXCEL
				{
					return this.CreateExcel(companyCIID);
				}
				else if(reportFormat.Equals("5")) //RTF
				{
					return this.CreateRTF(companyCIID);
				}*/

                //return file;
            } catch (Exception) {
                //Logger.WriteToLog(className +" : " + funcName, err,true);
                //return null;
            }
        }

        private string CreateHTML(int CompanyCIID) {
            string reportType = "43";
            string cultureToUse = "Native";
            string afs_ids = "";

            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }
            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }
            if (Session["AFSID"] != null) {
                afs_ids = Session["AFSID"].ToString();
            }
            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }

            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            var generator = new TemplateReportGenerator(
                rm, cultureToUse, userCreditInfoID, userID, ipAddress);
            string xslFile = "";
            string xslSendFile = "";
            XmlDocument doc = generator.GetReport(reportType, CompanyCIID, afs_ids, -1, ref xslFile, ref xslSendFile);
            var transformer = new XslTransform();

            transformer.Load(Server.MapPath(CigConfig.Configure("lookupsettings.backRootName") + xslSendFile));
            //StringWriter writer = new StringWriter();

            string filePath = CigConfig.Configure("lookupsettings.DirForTempFiles");
            string file = filePath + GetFileName(CompanyCIID.ToString()) + ".xls";

            FileStream writer = File.Create(file);

            transformer.Transform(doc, null, writer, null);
                //generator.getReport(CompanyCIID, afs_ids), null, writer, null);

            //transformer.Transform(doc, null, writer, null);//generator.getReport(CompanyCIID, afs_ids), null, writer, null);
            writer.Flush();
            writer.Close();

            //writer.Flush();
            //	string res = writer.ToString();

            writer.Close();

            return file;
        }

        /// <summary>
        /// Creates a name for temporary report file
        /// </summary>
        /// <param name="companyID">The company id </param>
        /// <returns>A name for report file based on report id and date/time the report was generated</returns>
        private static string GetFileName(string companyID) {
            DateTime dt = DateTime.Now;
            return "CompanyReport_" + companyID + "_" + dt.Day + "_" + dt.Month + "_" + dt.Hour + "_" + dt.Minute + "_" +
                   dt.Second; //+".html";
        }

        private void CreatePDF(int CompanyCIID) {
            string htmlFileName = CreateHTML(CompanyCIID);

            XSettings.License = CigConfig.Configure("lookupsettings.abcPDFLicenceKey");
            var theDoc = new Doc();

            // theDoc.EmbedFont("verdana", "Unicode");
            //theDoc.Font = theDoc.EmbedFont("arial", "Unicode");

            //We first create a Doc object and inset the edges a little so that the 
            //HTML will appear in the middle of the page
            //		theDoc.Rect.Inset(72, 144);

            //We add the first page and indicate that we will be adding more pages 
            //by telling the Doc object that this is page one. We save the returned 
            //ID as this will be used to add subsequent pages.
            int theID = theDoc.AddImageUrl("file:///" + htmlFileName, true, 608, false);
            //theID = theDoc.AddImageHtml(html, true, 608, false); 

            //We now chain subsequent pages together. We stop when we reach a page 
            //which wasn't truncated.
            while (true) {
                theDoc.FrameRect();
                if (theDoc.GetInfo(theID, "Truncated") != "1") {
                    break;
                }
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }

            //After adding the pages we can flatten them. We can't do this until 
            //after the pages have been added because flattening will invalidate our 
            //previous ID and break the chain.
            for (int i = 1; i <= theDoc.PageCount; i++) {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }

            //theDoc.EmbedFont("verdana", "Unicode");
            //theDoc.Font = theDoc.EmbedFont("arial", "Unicode");
            //Finally we save. 
            Response.ContentType = "application/pdf";

            theDoc.Save(Response.OutputStream);
            theDoc.Clear();
        }

        private void CreateXML(int CompanyCIID) {
            string reportType = "3";
            string cultureToUse = "Native";
            string afs_ids = "";

            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }
            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }
            if (Session["AFSID"] != null) {
                afs_ids = Session["AFSID"].ToString();
            }
            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }

            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            var generator = new TemplateReportGenerator(
                rm, cultureToUse, userCreditInfoID, userID, ipAddress);
            string xslFile = null;
            string xslSendFile = null;
            XmlDocument doc = generator.GetReport(reportType, CompanyCIID, afs_ids, -1, ref xslFile, ref xslSendFile);

            Response.ContentType = "text/xml";

            //doc.Save("c:\\CIG\\xmlreport.xml");
            //doc.Save(Response.OutputStream);
            Response.Output.Write(doc.OuterXml);
            Response.Flush();
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new EventHandler(this.Page_Load); }

        #endregion
    }
}