#region

using System;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using CR.BLL;
using CR.Localization;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CR {
    /// <summary>
    /// This web page displayes compant report
    /// </summary>
    public class Report : Page {
        protected Button btnPrint;
        protected Button btnSendReport;
        protected HtmlTableCell trLine;
        protected Xml xmlxslTransform;

        private void Page_Load(object sender, EventArgs e) {
            try {
                if (!IsPostBack) {
                    displayReport();
                }
            } catch (NullReferenceException) {
                FormsAuthentication.SignOut();
                Response.AddHeader("Refresh", "0");
                Session.Abandon();
            }
        }

        private void displayReport() {
            // check the current culture
            ResourceManager rm = CIResource.CurrentManager;
            CultureInfo ci = Thread.CurrentThread.CurrentCulture;

            btnSendReport.Text = rm.GetString("txtSendReport", ci);
            btnPrint.Text = rm.GetString("txtPrint", ci);

            string companyId = "";
            string reportType = "BasicReport";
            string cultureToUse = "Native";
            string afs_ids = "";
            int nAgreementID = -1;

            if (Session["CompanyCIID"] != null) {
                companyId = Session["CompanyCIID"].ToString();
            }
            if (Session["ReportType"] != null) {
                reportType = Session["ReportType"].ToString();
            }
            if (Session["ReportCulture"] != null) {
                cultureToUse = Session["ReportCulture"].ToString();
            }
            if (Session["AFSID"] != null) {
                afs_ids = Session["AFSID"].ToString();
            }
            if (Session["AgreementID"] != null) {
                try {
                    nAgreementID = int.Parse(Session["AgreementID"].ToString());
                } catch (Exception) {
                    nAgreementID = -1;
                }
            }

            int userCreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString());
            int userID = int.Parse(Session["UserLoginID"].ToString());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];

            if (reportType.Equals("BillingReport")) {
                trLine.Visible = false;
                btnSendReport.Visible = false;
                btnPrint.Visible = false;
            }

            var generator = new TemplateReportGenerator(
                rm, cultureToUse, userCreditInfoID, userID, ipAddress);
            string xslFile = null;
            string xslSendFile = null;
            XmlDocument doc = generator.GetReport(
                reportType, int.Parse(companyId), afs_ids, nAgreementID, ref xslFile, ref xslSendFile);
            //doc.Save(Server.MapPath(Request.ApplicationPath+"/test-bo-credit.xml"));
            //Logger.WriteToLog("Report: "+reportType,false);
            var transformer = new XslTransform();
            //doc.Save("c:\\cig\\TemplateReport.xml");
//			transformer.Load(Server.MapPath(Request.ApplicationPath+xslFile));
            try {
                if (CigConfig.Configure("lookupsettings.backRootName") != null) {
                    transformer.Load(Server.MapPath(CigConfig.Configure("lookupsettings.backRootName") + xslFile));
                } else // at least it will work this way at production server
                {
                    transformer.Load(Application.Get("AppPath") + xslFile);
                }
                //	transformer.Load(Server.MapPath(Request.ApplicationPath+xslFile)); 
                xmlxslTransform.Document = doc;
                xmlxslTransform.Transform = transformer;
            } catch (Exception err) {
                Logger.WriteToLog("displayReport(): The error is : " + err, true);
            }

            xmlxslTransform.Document = doc;
            xmlxslTransform.Transform = transformer;
        }

        private void btnSendReport_Click(object sender, EventArgs e) { Server.Transfer("SendReport.aspx"); }

        private void btnPrint_Click(object sender, EventArgs e) {
            displayReport();
            const string myScript = "<script language=Javascript>window.print();</script>";
            Page.RegisterClientScriptBlock("alert", myScript);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSendReport.Click += new System.EventHandler(this.btnSendReport_Click);
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}