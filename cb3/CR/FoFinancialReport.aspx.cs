#region

using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.Localization;
using Logging.BLL;
using WebSupergoo.ABCpdf4;

#endregion

using Cig.Framework.Base.Configuration;

namespace CR {
    /// <summary>
    /// Summary description for FoFinancialReport.
    /// </summary>
    public class FoFinancialReport : Page {
        private CultureInfo ci;
        private string culture;
        protected Label lblMessage;
        protected Label lblReport;
        protected Literal litCssPath;
        private bool nativeCult;
        public bool NativeCult { get { return nativeCult; } }

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                //litCssPath.Text = CigConfig.Configure("lookupsettings.reportCssFullUrl"];

                displayReport();
            }
        }

        private void displayReport() {
            culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            culture = Thread.CurrentThread.CurrentCulture.Name;
            var rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            string reportFormat = "HTML";
            string reportHtml = "";

            if (Session["ReportFormat"] != null) {
                reportFormat = Session["ReportFormat"].ToString();
            }
            if (Session["FoFinancialReport"] != null) {
                reportHtml = Session["FoFinancialReport"].ToString();
            }

            try {
                switch (reportFormat) {
                    case "HTML":

                        try {
                            reportHtml = "<LINK href=\"" +
                                         CigConfig.Configure("lookupsettings.reportAdditionalCssFullUrl") +
                                         "\" type=\"text/css\" rel=\"stylesheet\">" + reportHtml;
                            reportHtml = "<LINK href=\"" + CigConfig.Configure("lookupsettings.reportFoCssFullUrl") +
                                         "\" type=\"text/css\" rel=\"stylesheet\">" + reportHtml;
                            // 15.3.2006 Marek - add report title - don't like the solution at all
                            ResourceManager FSIrm = FSI.Localization.CIResource.CurrentManager;
                            string repTitle =
                                string.Format(
                                    "<td width=\"333\" height=\"41\" bgcolor=\"#891618\" class=\"headerwhite\">&nbsp;{0}</td>",
                                    FSIrm.GetString("txtFinanciaStatement", ci));
                            const string replaceKey = "<td width=\"333\" height=\"41\" bgcolor=\"#891618\" class=\"headerwhite\">&nbsp;</td>";
                            reportHtml = reportHtml.Replace(replaceKey, repTitle);
                            lblReport.Text = reportHtml;
                        } catch (Exception exc) {
                            Logger.WriteToLog("Error displaying financial data for CIID ", exc, true);
                            DisplayErrorMessage(rm.GetString("txtNoReportFound", ci));
                        }

                        break;
                    case "PDF":
                        string fileDir = CigConfig.Configure("lookupsettings.TMPFilesLocation");
                        string file = fileDir + Util.CreateUniqueString() + ".html";

                        reportHtml = "<LINK href=\"" + CigConfig.Configure("lookupsettings.reportAdditionalCssFullUrl") +
                                     "\" type=\"text/css\" rel=\"stylesheet\">" + reportHtml;
                        reportHtml = "<LINK href=\"" + CigConfig.Configure("lookupsettings.reportFoCssFullUrl") +
                                     "\" type=\"text/css\" rel=\"stylesheet\">" + reportHtml;

                        // 15.3.2006 Marek - add report title - don't like the solution at all
                        ResourceManager FSIrm2 = FSI.Localization.CIResource.CurrentManager;
                        string repTitle2 =
                            string.Format(
                                "<td width=\"333\" height=\"41\" bgcolor=\"#891618\" class=\"headerwhite\">&nbsp;{0}</td>",
                                FSIrm2.GetString("txtFinanciaStatement", ci));
                        const string replaceKey2 = "<td width=\"333\" height=\"41\" bgcolor=\"#891618\" class=\"headerwhite\">&nbsp;</td>";
                        reportHtml = reportHtml.Replace(replaceKey2, repTitle2);

                        var writer = new StreamWriter(file, false, Encoding.UTF8);
                        writer.Write(reportHtml);
                        writer.Flush();
                        writer.Close();

                        ///********************************************
                        ///start

                        XSettings.License = CigConfig.Configure("lookupsettings.AbcPDFLicence");
                        var theDoc = new Doc();

                        //We first create a Doc object and inset the edges a little so that the 
                        //HTML will appear in the middle of the page
                        //		theDoc.Rect.Inset(72, 144);

                        //We add the first page and indicate that we will be adding more pages 
                        //by telling the Doc object that this is page one. We save the returned 
                        //ID as this will be used to add subsequent pages.
                        var theID = theDoc.AddImageUrl("file:///" + file, true, 620, false);

                        //We now chain subsequent pages together. We stop when we reach a page 
                        //which wasn't truncated.
                        while (true) {
                            theDoc.FrameRect();
                            if (theDoc.GetInfo(theID, "Truncated") != "1") {
                                break;
                            }
                            theDoc.Page = theDoc.AddPage();
                            theID = theDoc.AddImageToChain(theID);
                        }

                        //After adding the pages we can flatten them. We can't do this until 
                        //after the pages have been added because flattening will invalidate our 
                        //previous ID and break the chain.
                        for (int i = 1; i <= theDoc.PageCount; i++) {
                            theDoc.PageNumber = i;
                            theDoc.Flatten();
                        }

                        Response.ContentType = "application/pdf";
                        theDoc.Save(Response.OutputStream);
                        theDoc.Clear();
                        Response.Flush();
                        Response.End();

                        ///end
                        ///********************************************

                        break;
                }
            } catch (Exception err) {
                Logger.WriteToLog("displayReport(): The error is : " + err, true);
            }
        }

        /// <summary>
        /// Takes html string and writes to disk. The file location is specified in cig.cfg.xml
        /// </summary>
        /// <param name="htmlString">The html string that shall be wtitten to disk</param>
        /// <returns>The file name and location</returns>
        public string WriteHTMLtoFile(string htmlString) {
            string fileDir = CigConfig.Configure("lookupsettings.TMPFilesLocation");
                //"C:/Inetpub/wwwroot/BulkSampleCSharp/files/"; // get 
            int theSeed = DateTime.Now.Second;
            var randFigure = new Random(theSeed);
            string createdFileName = randFigure.Next().ToString();

            var writer = new StreamWriter(fileDir + createdFileName + ".html");
                // File.Create("C:/Inetpub/wwwroot/BulkSampleCSharp/files/" + randFigure.ToString() + ".html"); // need to create folder for files and set the location in conf file
            writer.Write(htmlString);
            writer.Flush();
            writer.Close();

            return fileDir + createdFileName + ".html";
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblMessage.Text = errorMessage;
            //this.lblMessage.ForeColor = Color.Red;
            lblMessage.Visible = true;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}