#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Xml;
using CR.BLL;
using CreditWatch.BLL;
using UserAdmin.BLL;
using Logger=Logging.BLL.Logger;

#endregion

using Cig.Framework.Base.Configuration;

namespace CR.CYBLL {
    /// <summary>
    /// Summary description for CYReportGenerator.
    /// </summary>
    public class CYReportGenerator : ReportGenerator {
        private readonly cwFactory cwFactory;
        protected CRFactory crFactory;

        public CYReportGenerator(
            ResourceManager rm, string cultureToUse, int userCreditInfoID, int userID, string ipAddress)
            : base(rm, cultureToUse, userCreditInfoID, userID, ipAddress) {
            className = "CYReportGenerator";
            crFactory = new CRFactory();
            cwFactory = new cwFactory();
        }

        public XmlDocument GetCYCompanyLibraReport(int ciid, string nationalID) {
            const string funcName = "getCYCompanyLibraReport(string nationalID)";
            XmlDocument doc = XmlHelper.CreateDocument();
            XmlElement rootElement = XmlHelper.CreateElement(doc, "root", "");
            XmlHelper.AddAttribute(doc, rootElement, "Title", rm.GetString("txtLibraReport", ci));
            doc.AppendChild(rootElement);
            try {
                AddCYCompanyLibraReport(doc, rootElement, ciid);
                logUsage(ciid.ToString());
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return doc;
        }

        private void AddCYCompanyLibraReport(XmlDocument doc, XmlElement reportRoot, int ciid) {
            var userAdminFactory = new uaFactory();
            //Add claims 
            // a�gangst�kk h�r mi�ast a�eins vi� Libra report en ekki li�i innan Libra. �etta �yrfti �� jafnvel a� sko�a betur
            //	if(userAdminFactory.HasUserAccessToProduct(userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_LibraReport"]))) 
            //	reportRoot.AppendChild(GetCYClaims(doc, reportRoot,ciid));
            GetCYClaims(doc, reportRoot, ciid);
            //Add company involvements
            if (userAdminFactory.HasUserAccessToProduct(
                userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_LibraReport")))) {
                reportRoot.AppendChild(GetBoardMemberInvolvements(doc, ciid));
                reportRoot.AppendChild(GetShareholderInvolvements(doc, ciid));
                // 11012005 taken out by request from Costas
                //	reportRoot.AppendChild(GetPrincipalsInvolvements(doc, ciid,true));
            }
            //Add recent enquiries // hmm � �etta vi� K�pur? Tek �etta �t � bili
            /*	if(userAdminFactory.HasUserAccessToProduct(userID, int.Parse(CigConfig.Configure("lookupsettings.ProductKey_FORecentEnquiries"]))) 
				reportRoot.AppendChild(GetRecentEnquiries(doc, ciid));
		*/
            AddEndOfReport(doc, reportRoot);
        }

        protected void GetCYClaims(XmlDocument doc, XmlElement reportRoot, int companyCIID) {
            var claims = XmlHelper.GetXmlTable(doc, "Claims", "tblClaims");
            XmlHelper.AddAttribute(doc, claims, "title", rm.GetString("txtClaims", ci));
            var ds = nativeCult ? cwFactory.GetSplitUpNativeSearchDetailsList(companyCIID) : cwFactory.GetSplitUpSearchDetailsList(companyCIID, true);
            /*  �etta er svona n�na ...
				mySet.Tables.Add("Bankruptcy");
				mySet.Tables.Add("BouncedCheques");
				mySet.Tables.Add("CaseDecisionAgainstCompanies");
				mySet.Tables.Add("OtherCheques");
				// �yrfti �v� a� loopa � gegnum allar ds t�flur og byggja upp claim flokka � �ann h�tt
			*/
            bool noRecFound = true;
            foreach (DataTable theTable in ds.Tables) {
                //if(ds.Tables.Count>0&&ds.Tables[0].Rows.Count > 0)
                if (theTable.Rows.Count > 0) {
                    noRecFound = false;
                    switch (theTable.TableName) {
                        case "Bankruptcy":
                            reportRoot.AppendChild(
                                AddOrdinaryClaims(doc, theTable, rm.GetString("txtBankruptcy", ci), true));
                            //claimType = rm.GetString("txtBankruptcy",ci);
                            break;
                        case "BouncedCheques":
                            reportRoot.AppendChild(
                                AddBounchedChequesClaims(doc, theTable, rm.GetString("txtBounchedCheques", ci)));
                            break;
                        case "CaseDecisionAgainstCompanies":
                            //	AddMiscClaims(doc, theTable);
                            reportRoot.AppendChild(
                                AddOrdinaryClaims(doc, theTable, rm.GetString("txtCasesAgainstCompanies", ci), false));
                            //	claimType = rm.GetString("txtCasesAgainstCompanies",ci);
                            break;
                        case "OtherCheques":
                            reportRoot.AppendChild(
                                AddOrdinaryClaims(doc, theTable, rm.GetString("txtMiscCases", ci), true));
                            break;
                    }
                    /*
					NumberFormatInfo formatter = (NumberFormatInfo)NumberFormatInfo.CurrentInfo.Clone();
					formatter.PercentDecimalDigits = 2;
					formatter.NumberDecimalDigits=2;
					for(int i=0;i<ds.Tables[0].Rows.Count;i++)
					{
						XmlElement row = XmlHelper.getXmlRow(doc);
						XmlHelper.AddAttribute(doc, row, "CIReferenceText", rm.GetString("txtCIReference", ci));
						XmlHelper.AddAttribute(doc, row, "CIReference", ds.Tables[0].Rows[i]["CreditInfoID"].ToString());
						XmlHelper.AddAttribute(doc, row, "CaseNumberText", rm.GetString("txtCase", ci));
						XmlHelper.AddAttribute(doc, row, "CaseNumber", ds.Tables[0].Rows[i]["CaseNr"].ToString());
						
						XmlHelper.AddAttribute(doc, row, "InfoSourceText", rm.GetString("txtInformationSource", ci));
						XmlHelper.AddAttribute(doc, row, "ClaimTypeText", rm.GetString("txtClaimType", ci));					
						if(nativeCult)
						{						
							XmlHelper.AddAttribute(doc, row, "InfoSource", ds.Tables[0].Rows[i]["InfoSourceNative"].ToString());
							// jafnvel svona
						//	XmlHelper.AddAttribute(doc,row,"ClaimType",claimType);
							// � sta�inn fyrir �etta ...
							XmlHelper.AddAttribute(doc, row, "ClaimType", ds.Tables[0].Rows[i]["TypeNative"].ToString());
						}
						else
						{
							XmlHelper.AddAttribute(doc, row, "InfoSource", ds.Tables[0].Rows[i]["InfoSourceEN"].ToString());
							XmlHelper.AddAttribute(doc, row, "ClaimType", ds.Tables[0].Rows[i]["TypeEN"].ToString());
						}
						XmlHelper.AddAttribute(doc, row, "ClaimOwnerText", rm.GetString("txtClaimOwner", ci));
						XmlHelper.AddAttribute(doc, row, "ClaimOwner", GetName(int.Parse(ds.Tables[0].Rows[i]["ClaimOwnerCIID"].ToString())));
						XmlHelper.AddAttribute(doc, row, "DateText", rm.GetString("txtDate", ci));
						XmlHelper.AddAttribute(doc, row, "Date", DateTime.Parse(ds.Tables[0].Rows[0]["RegDate"].ToString()).ToShortDateString());
						XmlHelper.AddAttribute(doc, row, "AmountText", rm.GetString("txtAmount", ci));
						XmlHelper.AddAttribute(doc, row, "Amount", float.Parse(ds.Tables[0].Rows[i]["Amount"].ToString()).ToString(formatter));
						claims.AppendChild(row);
					}
				*/
                }
                //		else
                //		{
                //			claims.AppendChild(XmlHelper.CreateElement(doc, "NoInfo", "value", rm.GetString("txtNoRegistrations",ci)));
                //		}
            }
            if (noRecFound) {
                claims.AppendChild(
                    XmlHelper.CreateElement(doc, "NoInfo", "value", rm.GetString("txtNoRegistrations", ci)));
                reportRoot.AppendChild(claims);
            }
            //	return claims;			
        }

        private XmlElement AddOrdinaryClaims(XmlDocument doc, DataTable theTable, string header, bool fullVersion) {
            var claims = XmlHelper.GetXmlTable(doc, "Claims", "tblClaims");

            if (!fullVersion) {
                claims = XmlHelper.GetXmlTable(doc, "CaseACompanies", "tblCaseACompanies");
            }

            XmlHelper.AddAttribute(doc, claims, "title", header);

            var formatter = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            formatter.PercentDecimalDigits = 2;
            formatter.NumberDecimalDigits = 2;
            for (int i = 0; i < theTable.Rows.Count; i++) {
                var row = XmlHelper.GetXmlRow(doc);
                XmlHelper.AddAttribute(doc, row, "CaseNumberText", rm.GetString("txtCase", ci));
                XmlHelper.AddAttribute(doc, row, "CaseNumber", theTable.Rows[i]["case_nr"].ToString());
                XmlHelper.AddAttribute(doc, row, "CIReferenceText", rm.GetString("txtCIReference", ci));
                XmlHelper.AddAttribute(doc, row, "CIReferenceID", theTable.Rows[i]["claim_id"].ToString());

                XmlHelper.AddAttribute(doc, row, "DateText", rm.GetString("txtDate", ci));
                if (theTable.Rows[i]["claim_date"].ToString() != null) {
                    var theDate = DateTime.Parse(theTable.Rows[i]["claim_date"].ToString());
                    //	XmlHelper.AddAttribute(doc,row,"Date",theTable.Rows[i]["claim_date"].ToString()); // h�r �arf a� vera short date string
                    XmlHelper.AddAttribute(doc, row, "Date", theDate.ToShortDateString());
                        // h�r �arf a� vera short date string
                } else {
                    XmlHelper.AddAttribute(doc, row, "Date", theTable.Rows[i]["claim_date"].ToString());
                }
                XmlHelper.AddAttribute(doc, row, "ClaimOwnerText", rm.GetString("txtClaimOwner", ci));
                XmlHelper.AddAttribute(doc, row, "ClaimOwner", theTable.Rows[i]["claim_owner_name"].ToString());
                XmlHelper.AddAttribute(doc, row, "InfoSourceText", rm.GetString("txtInformationSource", ci));
                XmlHelper.AddAttribute(doc, row, "ClaimTypeText", rm.GetString("txtCaseType", ci));
                if (nativeCult) {
                    XmlHelper.AddAttribute(
                        doc, row, "InfoSource", theTable.Rows[i]["information_source_name"].ToString());
                    // jafnvel svona
                    //	XmlHelper.AddAttribute(doc,row,"ClaimType",claimType);
                    // � sta�inn fyrir �etta ...
                    XmlHelper.AddAttribute(doc, row, "ClaimType", theTable.Rows[i]["claim_type"].ToString());
                } else {
                    XmlHelper.AddAttribute(
                        doc, row, "InfoSource", theTable.Rows[i]["information_source_name"].ToString());
                    XmlHelper.AddAttribute(doc, row, "ClaimType", theTable.Rows[i]["claim_type"].ToString());
                }
                XmlHelper.AddAttribute(doc, row, "GazetteYearText", rm.GetString("txtGazetteYear", ci));
                XmlHelper.AddAttribute(doc, row, "GazetteYear", theTable.Rows[i]["gazette_year"].ToString());
                XmlHelper.AddAttribute(doc, row, "GazettePageText", rm.GetString("txtGazettePage", ci));
                XmlHelper.AddAttribute(doc, row, "GazettePage", theTable.Rows[0]["gazette_Page"].ToString());
                if (fullVersion) {
                    XmlHelper.AddAttribute(doc, row, "AmountText", rm.GetString("txtAmount", ci));
                    if (!string.IsNullOrEmpty(theTable.Rows[i]["amount"].ToString())) {
                        XmlHelper.AddAttribute(
                            doc, row, "Amount", float.Parse(theTable.Rows[i]["amount"].ToString()).ToString(formatter));
                    } else {
                        XmlHelper.AddAttribute(doc, row, "Amount", theTable.Rows[i]["amount"].ToString());
                    }
                    XmlHelper.AddAttribute(doc, row, "AgentText", rm.GetString("txtAgent", ci));
                    XmlHelper.AddAttribute(doc, row, "Agent", theTable.Rows[0]["agent"].ToString());
                }
                XmlHelper.AddAttribute(doc, row, "CommentText", rm.GetString("txtComment", ci));
                XmlHelper.AddAttribute(doc, row, "Comment", theTable.Rows[0]["comment"].ToString());
                claims.AppendChild(row);
            }
            return claims;
        }

        private XmlElement AddBounchedChequesClaims(XmlDocument doc, DataTable theTable, string header) {
            var claims = XmlHelper.GetXmlTable(doc, "BouncedCheques", "tblBouncedCheques");
            XmlHelper.AddAttribute(doc, claims, "title", header);

            var formatter = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            formatter.PercentDecimalDigits = 2;
            formatter.NumberDecimalDigits = 2;
            for (int i = 0; i < theTable.Rows.Count; i++) {
                XmlElement row = XmlHelper.GetXmlRow(doc);
                XmlHelper.AddAttribute(doc, row, "CaseNumberText", rm.GetString("txtCase", ci));
                XmlHelper.AddAttribute(doc, row, "CaseNumber", theTable.Rows[i]["case_nr"].ToString());
                XmlHelper.AddAttribute(doc, row, "CIReferenceText", rm.GetString("txtCIReference", ci));
                XmlHelper.AddAttribute(doc, row, "CIReferenceID", theTable.Rows[i]["claim_id"].ToString());

                XmlHelper.AddAttribute(doc, row, "DateText", rm.GetString("txtDate", ci));
                if (theTable.Rows[i]["claim_date"].ToString() != null) {
                    var theDate = DateTime.Parse(theTable.Rows[i]["claim_date"].ToString());
                    //	XmlHelper.AddAttribute(doc,row,"Date",theTable.Rows[i]["claim_date"].ToString()); // h�r �arf a� vera short date string
                    XmlHelper.AddAttribute(doc, row, "Date", theDate.ToShortDateString());
                        // h�r �arf a� vera short date string
                } else {
                    XmlHelper.AddAttribute(doc, row, "Date", theTable.Rows[i]["claim_date"].ToString());
                }
                XmlHelper.AddAttribute(doc, row, "ClaimOwnerText", rm.GetString("txtClaimOwner", ci));
                XmlHelper.AddAttribute(doc, row, "ClaimOwner", theTable.Rows[i]["claim_owner_name"].ToString());
                XmlHelper.AddAttribute(doc, row, "ClaimTypeText", rm.GetString("txtCaseType", ci));
                if (nativeCult) {
                    XmlHelper.AddAttribute(doc, row, "ClaimType", theTable.Rows[i]["claim_type"].ToString());
                } else {
                    XmlHelper.AddAttribute(doc, row, "ClaimType", theTable.Rows[i]["claim_type"].ToString());
                }
                XmlHelper.AddAttribute(doc, row, "AmountText", rm.GetString("txtAmount", ci));
                if (!string.IsNullOrEmpty(theTable.Rows[i]["amount"].ToString())) {
                    XmlHelper.AddAttribute(
                        doc, row, "Amount", float.Parse(theTable.Rows[i]["amount"].ToString()).ToString(formatter));
                } else {
                    XmlHelper.AddAttribute(doc, row, "Amount", theTable.Rows[i]["amount"].ToString());
                }
                XmlHelper.AddAttribute(doc, row, "IssuedBankText", rm.GetString("txtBank", ci));
                XmlHelper.AddAttribute(doc, row, "IssuedBank", theTable.Rows[i]["cheque_issue_bank"].ToString());
                XmlHelper.AddAttribute(doc, row, "AgentText", rm.GetString("txtAgent", ci));
                XmlHelper.AddAttribute(doc, row, "Agent", theTable.Rows[0]["agent"].ToString());

                XmlHelper.AddAttribute(doc, row, "CommentText", rm.GetString("txtComment", ci));
                XmlHelper.AddAttribute(doc, row, "Comment", theTable.Rows[0]["comment"].ToString());
                claims.AppendChild(row);
            }
            return claims;
        }

        private XmlElement GetBoardMemberInvolvements(XmlDocument doc, int ciid) {
            var involvements = XmlHelper.GetXmlTable(
                doc, "BoardMemberInvolvements", "tblBoardMemberInvolvements");
            //	XmlElement involvements = XmlHelper.getXmlTable(doc, "CompanyInvolvements", "tblCompanyInvolvements");
            XmlHelper.AddAttribute(doc, involvements, "title", rm.GetString("txtBoardMemberInvolvements", ci));

            // h�r �arf a� n� � �rennskonar involvements GetBoardMemberInvolvement,GetShareholderInvolvements og GetPrincipalsInvolvememts
            DataSet ds = crFactory.GetBoardMemberInvolvements(ciid);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                //Add column header
                // The header also taken out by request by Costas 20.01.2005
                //	XmlElement header = doc.CreateElement("Header");
                //	XmlHelper.AddAttribute(doc, header, "Company", rm.GetString("txtCompany",ci));
                // Taken out by request from Costas 11012005
                // XmlHelper.AddAttribute(doc, header, "Position", rm.GetString("txtPosition",ci).Replace(":", ""));
                //	involvements.AppendChild(header);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    XmlHelper.AddAttribute(doc, row, "ID", ds.Tables[0].Rows[i]["CompanyCIID"].ToString());
                    if (nativeCult) {
                        XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameNative"].ToString());
                        // Taken out by request from Costas 11012005
                        //		XmlHelper.AddAttribute(doc, row, "Position", ds.Tables[0].Rows[i]["TitleNative"].ToString());
                    } else {
                        if (ds.Tables[0].Rows[i]["NameEN"].ToString() != "") {
                            XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameEN"].ToString());
                        } else {
                            XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameNative"].ToString());
                        }
                        // Taken out by request from Costas 11012005
                        //		XmlHelper.AddAttribute(doc, row, "Position", ds.Tables[0].Rows[i]["TitleEN"].ToString());
                    }

                    involvements.AppendChild(row);
                }
            } else {
                XmlElement row = XmlHelper.GetXmlRow(doc);
                XmlHelper.AddAttribute(doc, row, "Name", rm.GetString("txtNoBoardMemberInvolvementFoundFor", ci));
                involvements.AppendChild(row);
                //	involvements.AppendChild(XmlHelper.CreateElement(doc, "NoInfo","value", rm.GetString("txtNoBoardMemberInvolvementFoundFor",ci)));
                // claims.AppendChild(XmlHelper.CreateElement(doc, "NoInfo", "value", rm.GetString("txtNoRegistrations",ci)));
            }
            return involvements;
        }

        private XmlElement GetShareholderInvolvements(XmlDocument doc, int ciid) {
            XmlElement involvements = XmlHelper.GetXmlTable(
                doc, "ShareholderInvolvements", "tblShareholderInvolvements");
            XmlHelper.AddAttribute(doc, involvements, "title", rm.GetString("txtShareholderInvolvements", ci));

            // h�r �arf a� n� � �rennskonar involvements GetBoardMemberInvolvement,GetShareholderInvolvements og GetPrincipalsInvolvememts
            DataSet ds = crFactory.GetShareholderInvolvements(ciid);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                //Add column header
                // The header also taken out by request by Costas 20.01.2005
                /*	XmlElement header = doc.CreateElement("Header");
				XmlHelper.AddAttribute(doc, header, "Company", rm.GetString("txtCompany",ci));
				XmlHelper.AddAttribute(doc, header, "Position", rm.GetString("txtPosition",ci).Replace(":", ""));
				XmlHelper.AddAttribute(doc, header, "Status", rm.GetString("txtStatus",ci));
				involvements.AppendChild(header);
			*/
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    XmlElement row = XmlHelper.GetXmlRow(doc);
                    XmlHelper.AddAttribute(doc, row, "ID", ds.Tables[0].Rows[i]["CompanyCIID"].ToString());
                    if (nativeCult) {
                        XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameNative"].ToString());
                    } else {
                        if (ds.Tables[0].Rows[i]["NameEN"].ToString() != "") {
                            XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameEN"].ToString());
                        } else {
                            XmlHelper.AddAttribute(doc, row, "Name", ds.Tables[0].Rows[i]["NameNative"].ToString());
                        }
                    }

                    //	XmlHelper.AddAttribute(doc, row, "Position", ds.Tables[0].Rows[i]["Involvement"].ToString());
                    //	XmlHelper.AddAttribute(doc, row, "Status", ds.Tables[0].Rows[i]["State"].ToString());
                    involvements.AppendChild(row);
                }
            } else {
                XmlElement row = XmlHelper.GetXmlRow(doc);
                XmlHelper.AddAttribute(doc, row, "Name", rm.GetString("txtNoShareholderInvolvementsFoundFor", ci));
                involvements.AppendChild(row);
            }
            return involvements;
        }
    }
}