using System;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CR.Localization;
using UserAdmin.BLL;
using UserAdmin.BLL.auDepartments;
using UserAdmin.BLL.auUsers;

using Cig.Framework.Base.Configuration;


namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for FoUserProfile.
    /// </summary>
    public class FoUserProfile : Page {
        private CultureInfo ci;
        protected Label CreditWatchLimit;
        protected Label CreditWatchLimitLabel;
        protected CompareValidator cvRepeatedPassword;
        protected Label DateCreated;
        protected Label DateCreatedLabel;
        protected Label DateUpdated;
        protected Label DateUpdatedLabel;
        protected Label Department;
        protected Label DepartmentLabel;
        protected Label EmailLabel;
        protected Label lblMessage;
        protected Label lblMyProfile;
        protected Label lblSearch;
        private uaFactory myUserAdminFactory;
        protected Label Name;
        protected Label NameLabel;
        protected Label lblPasswordLabel;
        protected Label RegisteredBy;
        protected Label RegisteredByLabel;
        protected RegularExpressionValidator regExpEmailValidator;
        protected Label lblRepeatedPasswordLabel;
        private ResourceManager rm;
        protected Button SaveButton;
        protected Label SubscriberName;
        protected Label SubscriberNameLabel;
        protected TextBox txtEmail;
        protected TextBox txtPassword;
        protected TextBox txtRepeatedPassword;
        private string userName = "";
        protected Label lblUsername;
        protected Label lblUserNameLabel;
        protected Label UserSettingsLabel;
        protected ValidationSummary valSummary;

        private void Page_Load(object sender, EventArgs e) {
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (Session["UserLoginID"] == null || Session["UserLoginID"].ToString() == string.Empty)
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session.Abandon();
                Response.Redirect("~/fodefault.aspx");
                Response.End();
            }

            if (Session["UserLoginName"] != null) {
                userName = Session["UserLoginName"].ToString();
            }

            myUserAdminFactory = new uaFactory();
            if (Page.IsPostBack) {return;}
            LocalizeText();
            LoadUser();
        }

        private void LoadUser() {
            auDepartmentBLLC dep = null;
            if (userName.Length > 0) {
                var user = myUserAdminFactory.GetSpecificUser(userName);
                if (user != null) {
                    dep = myUserAdminFactory.GetDepartment(user.DepartmentID);
                    DateCreated.Text = user.Created.ToString();
                    DateUpdated.Text = user.Updated.ToString();
                    txtEmail.Text = user.Email;
                    lblUsername.Text = user.UserName;
                    this.CreditWatchLimit.Text = user.CntCreditWatch.ToString();

                    var userReg = myUserAdminFactory.GetUser(user.RegisteredBy);
                    if (userReg != null) {
                        RegisteredBy.Text = userReg.UserName;
                    }
                }
            }

            var nativeCulture = Thread.CurrentThread.CurrentCulture.Name;
            var culture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture)) {
                if (Session["NativeUserName"] != null) {
                    Name.Text = Session["NativeUserName"].ToString();
                }
                if (Session["NativeSubscriberName"] != null) {
                    SubscriberName.Text = Session["NativeSubscriberName"].ToString();
                }
                // if no native name the get the english one
                if (userName.Trim() == "" || userName == null) {
                    if (Session["ENUserName"] != null) {
                        Name.Text = Session["ENUserName"].ToString();
                    }
                }
                if (SubscriberName.Text.Trim() == "" || SubscriberName.Text == null) {
                    if (Session["ENSubscriberName"] != null) {
                        SubscriberName.Text = Session["ENSubscriberName"].ToString();
                    }
                }
                Department.Text = dep == null ? "" : dep.NameNative;
            } else {
                if (Session["ENUserName"] != null) {
                    Name.Text = Session["ENUserName"].ToString();
                }
                if (Session["ENSubscriberName"] != null) {
                    SubscriberName.Text = Session["ENSubscriberName"].ToString();
                }
                // if no english name then get the native one
                if (userName.Trim() == "" || userName == null) {
                    if (Session["NativeUserName"] != null) {
                        Name.Text = Session["NativeUserName"].ToString();
                    }
                }
                if (SubscriberName.Text.Trim() == "" || SubscriberName.Text == null) {
                    if (Session["NativeSubscriberName"] != null) {
                        SubscriberName.Text = Session["NativeSubscriberName"].ToString();
                    }
                }
                Department.Text = dep == null ? "" : dep.NameEN;
            }
        }

        private void SaveButton_Click(object sender, EventArgs e) {
            if (!this.IsValid)
            {
                return;
            }
            if (userName.Length <= 0) {return;}
            auUsersBLLC user = myUserAdminFactory.GetSpecificUser(userName);
            if (user == null) {return;}
            user.Email = txtEmail.Text;
            myUserAdminFactory.UpdateUserAccountDetails(user);
            if (txtPassword.Text.Length <= 0) {return;}
            var passwordAndSalt = String.Concat(txtPassword.Text, user.Salt);

            // Now hash them
            var hashedPasswordAndSalt =
                FormsAuthentication.HashPasswordForStoringInConfigFile(passwordAndSalt, "SHA1");

            myUserAdminFactory.UpdateUserPassword(hashedPasswordAndSalt, user.Salt, userName);
        }

        private void LocalizeText() {
            NameLabel.Text = rm.GetString("txtName", ci);
            SubscriberNameLabel.Text = rm.GetString("txtSubscriber", ci);
            DepartmentLabel.Text = rm.GetString("txtDepartment", ci);
            DateCreatedLabel.Text = rm.GetString("txtDateCreated", ci);
            RegisteredByLabel.Text = rm.GetString("txtRegisteredBy", ci);
            DateUpdatedLabel.Text = rm.GetString("txtDateUpdated", ci);
            UserSettingsLabel.Text = rm.GetString("txtUserSettings", ci);
            lblUserNameLabel.Text = rm.GetString("txtUserName", ci);
            lblPasswordLabel.Text = rm.GetString("txtPassword", ci);
            lblRepeatedPasswordLabel.Text = rm.GetString("txtPasswordVerify", ci);
            EmailLabel.Text = rm.GetString("txtEmail", ci);
            CreditWatchLimitLabel.Text = rm.GetString("txtCreditWatchLimit", ci);
            lblMyProfile.Text = rm.GetString("txtMyProfile", ci);
            SaveButton.Text = rm.GetString("txtSave", ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}