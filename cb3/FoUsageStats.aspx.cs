using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;
using Statistic.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.auUsers;
using System.Web.Security;

using Cig.Framework.Base.Configuration;


namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for FoUsageStats.
    /// </summary>
    public class FoUsageStats : Page {
        private bool _isBillable;
        protected Button btnSearch;
        private CultureInfo ci;
        protected CompareValidator cpvDateFrom;
        protected CompareValidator cpvDateTo;
        private string culture;
        protected DropDownList ddlProducts;
        protected DropDownList ddlUsers;
        protected DataGrid dgrUsage;
        private uaFactory factory;
        protected HyperLink hlMail;
        protected Label lblBillable;
        protected Label lblDateFrom;
        protected Label lblDateTo;
        protected Label lblHeader;
        protected Label lblMessage;
        protected Label lblProductCount;
        protected Label lblProductName;
        protected Label lblProducts;
        protected Label lblProductsCount;
        protected Label lblSubscriber;
        protected Label lblSubscriberTitle;
        protected Label lblUserCount;
        protected Label lblUsers;
        private bool nativeCult;
        protected RadioButtonList rblBillable;
        protected RequiredFieldValidator rfvDateFrom;
        protected RequiredFieldValidator rfvDateTo;
        private ResourceManager rm;
        protected HtmlTable tblInformation;
        protected TextBox txtDateFrom;
        protected TextBox txtDateTo;
        private auUsersBLLC user;
        protected ValidationSummary valSummary;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = "7000";

            if (Session["UserLoginID"] == null || Session["UserLoginID"].ToString() == string.Empty)
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session.Abandon();
                Response.Redirect("~/fodefault.aspx");
                Response.End();
            }

            if (!User.IsInRole(Page.ID)) {
                Server.Transfer("FoNoAuthorization.aspx");
            }

            AddEnterEvent();
            culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            factory = new uaFactory();
            lblMessage.Visible = false;

            try {
                _isBillable = Convert.ToBoolean(CigConfig.Configure("lookupsettings.IsBillableActive"));
            } catch {
                _isBillable = false;
            }
            lblBillable.Visible = _isBillable;
            rblBillable.Visible = _isBillable;

            if (!Page.IsPostBack) {
                txtDateTo.Text = DateTime.Now.ToShortDateString();
                DateTime dateBack = DateTime.Now - new TimeSpan(30, 0, 0, 0);
                txtDateFrom.Text = dateBack.ToShortDateString();
                LocalizeText();
                LocalizeDataGrid();
                tblInformation.Visible = false;
            }
        }

        private void LocalizeText() {
            // Labels
            lblHeader.Text = rm.GetString("txtUsageStatistics", ci);
            lblDateFrom.Text = rm.GetString("txtDateFrom", ci);
            lblDateTo.Text = rm.GetString("txtDateTo", ci);
            lblSubscriberTitle.Text = rm.GetString("txtSubscriber", ci);
            lblUsers.Text = rm.GetString("txtUsers", ci);
            lblProducts.Text = rm.GetString("txtProducts", ci);
            lblBillable.Text = rm.GetString("lblBillable", ci);

            // Buttons
            btnSearch.Text = rm.GetString("btnSearch", ci);

            // RadioButtons
            rblBillable.Items[0].Text = rm.GetString("lblTrue", ci);
            rblBillable.Items[1].Text = rm.GetString("lblFalse", ci);
            rblBillable.Items[2].Text = rm.GetString("lblBoth", ci);

            // Validators
            rfvDateFrom.ErrorMessage = rm.GetString("lblErrorMissingDate", ci);
            rfvDateTo.ErrorMessage = rm.GetString("lblErrorMissingDate", ci);
            cpvDateFrom.ErrorMessage = rm.GetString("lblErrorNotDate", ci);
            cpvDateTo.ErrorMessage = rm.GetString("lblErrorNotDate", ci);
        }

        private void LocalizeDataGrid() {
            dgrUsage.Columns[0].HeaderText = rm.GetString("txtProduct", ci);
            dgrUsage.Columns[1].HeaderText = rm.GetString("txtProduct", ci);
            dgrUsage.Columns[2].HeaderText = rm.GetString("txtUserName", ci);
            dgrUsage.Columns[3].HeaderText = rm.GetString("txtCount", ci);
        }

        private void AddEnterEvent() {
            var frm = FindControl("frmUsageStatistics");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            tblInformation.Visible = true;
            var statsFactory = new StatisticFactory();
            user = factory.GetUser(int.Parse(Session["UserLoginID"].ToString()));
            DataSet statsData;
            DataSet subscriberData;
            if (txtDateFrom.Text != String.Empty && txtDateTo.Text != String.Empty) {
                if (rblBillable.Items[2].Selected) {
                    statsData = statsFactory.GetUsageCounts(
                        DateTime.Parse(txtDateFrom.Text),
                        DateTime.Parse(txtDateTo.Text).AddDays(1),
                        null,
                        user.SubscriberID);
                    subscriberData = statsFactory.GetUsageSubscribers(
                        DateTime.Parse(txtDateFrom.Text),
                        DateTime.Parse(txtDateTo.Text).AddDays(1),
                        null,
                        user.SubscriberID);
                } else {
                    statsData = statsFactory.GetUsageCounts(
                        DateTime.Parse(txtDateFrom.Text),
                        DateTime.Parse(txtDateTo.Text).AddDays(1),
                        new XBool(rblBillable.Items[0].Selected),
                        user.SubscriberID);
                    subscriberData = statsFactory.GetUsageSubscribers(
                        DateTime.Parse(txtDateFrom.Text),
                        DateTime.Parse(txtDateTo.Text).AddDays(1),
                        new XBool(rblBillable.Items[0].Selected),
                        user.SubscriberID);
                }

                if (statsData != null && User.IsInRole("7200")) {
                    // Make the stats visable and load with data.
                    //lblSubscriberCount.Text	= statsData.Tables[0].Rows[0]["subscriber_count"].ToString();
                    lblUserCount.Text = statsData.Tables[0].Rows[0]["user_count"].ToString();
                    //lblTypeCount.Text		= statsData.Tables[0].Rows[0]["type_count"].ToString();
                    lblProductsCount.Text = statsData.Tables[0].Rows[0]["product_count"].ToString();

                    lblProductName.Text = "";
                    lblProductCount.Text = "";

                    if (statsData.Tables[1] != null) {
                        foreach (DataRow row in statsData.Tables[1].Rows) {
                            lblProductCount.Text += row[0] + "<br/>";
                            if (ci.Name == "en-US") {
                                lblProductName.Text += row[1] + "<br/>";
                            } else {
                                lblProductName.Text += row[2] + "<br/>";
                            }
                        }
                        LoadDropDownLists(statsData.Tables[1]);
                    }
                    ddlProducts.Visible = true;
                    ddlUsers.Visible = true;
                    lblSubscriber.Visible = true;
                    lblUsers.Visible = true;
                    lblProductCount.Visible = true;
                    lblProductName.Visible = true;
                    lblProducts.Visible = true;
                    lblSubscriberTitle.Visible = true;
                }

                if (User.IsInRole("7100") && !User.IsInRole("7200")) {
                    lblSubscriber.Visible = false;
                    lblUsers.Visible = false;
                    lblProducts.Visible = false;
                    lblSubscriberTitle.Visible = false;
                    lblProductCount.Visible = false;
                    lblProductName.Visible = false;
                    ddlProducts.Visible = false;
                    ddlUsers.Visible = false;
                }

                if (subscriberData != null) {
                    if (User.IsInRole("7100") && !User.IsInRole("7200")) {
                        var view1 = new DataView(subscriberData.Tables[0]);

                        string strWhere = "username='" + User.Identity.Name + "'";

                        view1.RowFilter = strWhere;
                        dgrUsage.DataSource = view1;
                        dgrUsage.DataBind();
                    } else if (User.IsInRole("7200")) {
                        ViewState["subscriberData"] = subscriberData;
                        dgrUsage.DataSource = subscriberData;
                        dgrUsage.DataBind();
                    }

                    if (nativeCult) {
                        dgrUsage.Columns[0].Visible = false;
                        dgrUsage.Columns[1].Visible = true;
                    } else {
                        dgrUsage.Columns[0].Visible = true;
                        dgrUsage.Columns[1].Visible = false;
                    }
                }
            }
        }

        private void LoadDropDownLists(DataTable table) {
            var view = new DataView(table);
            if (nativeCult) {
                view.Sort = "typeNative";
                ddlProducts.DataValueField = "typeNative";
            } else {
                view.Sort = "typeEN";
                ddlProducts.DataValueField = "typeEN";
            }

            ddlProducts.DataSource = view;
            ddlProducts.DataBind();
            ddlProducts.Items.Insert(0, new ListItem("---", null));

            DataSet usersSet = factory.GetUsersBySubscriberAsDataSet(user.SubscriberID);
            ddlUsers.DataSource = usersSet;
            ddlUsers.DataValueField = "username";
            ddlUsers.DataBind();
            ddlUsers.Items.Insert(0, new ListItem("---", null));
        }

        private void Filters_SelectedIndexChanged(object sender, EventArgs e) {
            var subscriberData = (DataSet) ViewState["subscriberData"];
            var view = new DataView(subscriberData.Tables[0]);
            var strWhere = "";

            if (ddlUsers.SelectedIndex != 0) {
                strWhere = "username='" + ddlUsers.SelectedValue + "'";
            }
            if (ddlProducts.SelectedIndex != 0) {
                if (!string.IsNullOrEmpty(strWhere)) {
                    strWhere += " AND ";
                }
                strWhere += "TypeNative='" + ddlProducts.SelectedValue + "'";
            }
            view.RowFilter = strWhere;
            dgrUsage.DataSource = view;
            dgrUsage.DataBind();
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.ddlProducts.SelectedIndexChanged += new System.EventHandler(this.Filters_SelectedIndexChanged);
            this.ddlUsers.SelectedIndexChanged += new System.EventHandler(this.Filters_SelectedIndexChanged);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}