#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;

#endregion

using Cig.Framework.Base.Configuration;

#pragma warning disable 618,612

namespace CPI {
    /// <summary>
    /// Summary description for Capital.
    /// </summary>
    public class Capital : BaseCPIPage {
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnLoadFromCompanyRegistry;
        protected Button btnReg;
        private int CIID = -1;
        protected CustomValidator cvAsked;
        protected CustomValidator cvAuthorizedCapital;
        protected CustomValidator cvIssuedNumberOfShares;
        protected CustomValidator cvNominalNumberOfShares;
        protected CustomValidator cvPaidUp;
        protected DropDownList ddlstCurrency;
        protected NumberFormatInfo fInfo;
        protected Label lblAsked;
        protected Label lblAuthorizedCapital;
        protected Label lblCapital;
        protected Label lblCurrency;
        protected Label lblErrorMessage;
        protected Label lblIssuedNoOfShares;
        protected Label lblNominalNumberOfShares;
        protected Label lblPageStep;
        protected Label lblPaidUp;
        protected Label lblShareClass;
        protected Label lblSharesDescription;
        protected TextBox txtAsked;
        protected TextBox txtAuthorizedCapital;
        protected TextBox txtIssuedNoOfShares;
        protected TextBox txtNominalNumberOfShares;
        protected TextBox txtPaidUp;
        protected TextBox txtShareClass;
        protected TextBox txtSharesDescription;
        protected ValidationSummary ValidationSummary;

        private static bool IsMalta {
            get {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                    if (CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                        return true;
                    }
                }
                return false;
            }
        }

        private void Page_Load(object sender, EventArgs e) {
            pageName = "Capital";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                //Server.Transfer("SearchForReport.aspx?redirect=Capital&new=true");
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            string toLoadFromCompanyRegistry =
                CigConfig.Configure("lookupsettings.EnableToLoadCapitalFromCompanyRegistry");
            if (toLoadFromCompanyRegistry != null && toLoadFromCompanyRegistry.ToLower().Equals("true")) {
                btnLoadFromCompanyRegistry.Visible = true;
            } else {
                btnLoadFromCompanyRegistry.Visible = false;
            }

            lblErrorMessage.Visible = false;

            fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            fInfo.NumberDecimalDigits = 2;

            txtAuthorizedCapital.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" +
                                                        fInfo.NumberGroupSeparator + "', '" +
                                                        fInfo.NumberDecimalSeparator + "', 2,'NaN');";
            txtAsked.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" + fInfo.NumberGroupSeparator +
                                            "', '" + fInfo.NumberDecimalSeparator + "', 2,'NaN');";
            txtIssuedNoOfShares.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" +
                                                       fInfo.NumberGroupSeparator + "', '" +
                                                       fInfo.NumberDecimalSeparator + "', 2,'NaN');";
            txtNominalNumberOfShares.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" +
                                                            fInfo.NumberGroupSeparator + "', '" +
                                                            fInfo.NumberDecimalSeparator + "', 2,'NaN');";
            txtPaidUp.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" + fInfo.NumberGroupSeparator +
                                             "', '" + fInfo.NumberDecimalSeparator + "', 2,'NaN');";

            if (!Page.IsPostBack) {
                //Localization
                LocalizeText();
                //Localization
                BindCurrency();
                LoadInfo();
            }
        }

        private void BindCurrency() {
            var myFactory = new CPIFactory();
            var currencySet = myFactory.GetCurrencyAsDataSet();
            //currencySet
            /*
			for(int i=0;i<currencySet.Tables[0].Rows.Count;i++)
			{
				currencySet.Tables[0].Rows[i][CurrencyDescriptionNative].ToString();
				currencySet.Tables[0].Rows[i][CurrencyDescriptionNative].ToString();
				//financialInfo.SetValue(i, FinancialInfo.FINANCIAL_YEAR, ds.Tables[0].Rows[i][FinancialInfo.FINANCIAL_YEAR].ToString());
			}

			
			foreach(DataRow row in currencySet.Tables[)
			{
				arraylist.Add(row);
			}
			
			ddlstCurrency.Items.Insert(
			*/

            ddlstCurrency.DataSource = currencySet;
            ddlstCurrency.DataTextField = "CurrencyDescription";
            ddlstCurrency.DataValueField = "CurrencyCode";
            ddlstCurrency.DataBind();
            // if currency order key in config then order the currency list ...
            if (CigConfig.Configure("lookupsettings.orderCurrencyList") != null) {
                OrderCurrencyList(CigConfig.Configure("lookupsettings.orderCurrencyList"));
            }
            if (IsMalta) {
                try {
                    ddlstCurrency.SelectedValue = "EUR";
                } catch (Exception) {
//Ignore - just try if this is in currency
                }
            }
        }

        private void OrderCurrencyList(string orderList) {
            string[] strArray = orderList.Split(new[] {','});
            int index = 0;
            foreach (string id in strArray) {
                ListItem li = ddlstCurrency.Items.FindByValue(id);
                ddlstCurrency.Items.Remove(li);
                ddlstCurrency.Items.Insert(index, li);
                index++;
            }
        }

        private void LocalizeText() {
            lblAsked.Text = rm.GetString("txtAsked", ci);
            lblAuthorizedCapital.Text = rm.GetString("txtAuthorizedCapital", ci);
            lblCapital.Text = rm.GetString("txtCapital", ci);
            lblIssuedNoOfShares.Text = rm.GetString("txtIssuedNumberOfShares", ci);
            lblNominalNumberOfShares.Text = rm.GetString("txtNominalNumberOfShares", ci);
            lblPaidUp.Text = rm.GetString("txtPaidUp", ci);
            lblSharesDescription.Text = rm.GetString("txtSharesDescription", ci);
            lblCurrency.Text = rm.GetString("txtCurrency", ci);

            cvAsked.ErrorMessage = rm.GetString("txtCapitalError", ci);
            cvAuthorizedCapital.ErrorMessage = rm.GetString("txtCapitalError", ci);
            cvIssuedNumberOfShares.ErrorMessage = rm.GetString("txtCapitalError", ci);
            cvNominalNumberOfShares.ErrorMessage = rm.GetString("txtCapitalError", ci);
            cvPaidUp.ErrorMessage = rm.GetString("txtCapitalError", ci);

            btnReg.Text = rm.GetString("txtSave", ci);
            //	this.btnCancel.Text = rm.GetString("txtCancel",ci);
            btnCancel.Text = rm.GetString("txtBack", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);

            txtShareClass.Text = rm.GetString("txtShareClass", ci);
            btnLoadFromCompanyRegistry.Text = rm.GetString("txtLoadBoardFromCompanyRegistry", ci);

            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
        }

        private void btnReg_Click(object sender, EventArgs e) {
            var myCapital = GetInfoFromPage();

            if (myCapital == null) {
                return;
            }
            var myFactory = new CPIFactory();

            if (myFactory.AddCapital(myCapital, CIID)) {
                DisplayMessage(rm.GetString("txtInfoSaved", ci));
            } else {
                DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }
        private void btnCancel_Click(object sender, EventArgs e) { TransferToPreviousPage(); }

        private CapitalBLLC GetInfoFromPage() {
            var myCapital = new CapitalBLLC();

            try {
                if (txtAuthorizedCapital.Text.Length > 0) {
                    myCapital.AuthorizedCapital = decimal.Parse(txtAuthorizedCapital.Text);
                }
                if (txtAsked.Text.Length > 0) {
                    myCapital.Asked = decimal.Parse(txtAsked.Text);
                }
                if (txtIssuedNoOfShares.Text.Length > 0) {
                    myCapital.IssuedNumberOfShares = decimal.Parse(txtIssuedNoOfShares.Text);
                }
                if (txtNominalNumberOfShares.Text.Length > 0) {
                    myCapital.NominalNumberOfShares = decimal.Parse(txtNominalNumberOfShares.Text);
                }
                if (txtPaidUp.Text.Length > 0) {
                    myCapital.PaidUp = decimal.Parse(txtPaidUp.Text);
                }

                myCapital.ShareClass = txtShareClass.Text;

                myCapital.CurrencyCode = ddlstCurrency.SelectedValue;

                myCapital.SharesDescription = txtSharesDescription.Text;
            } catch (Exception) {
                DisplayErrorMessage("Error getting info from page");
                return null;
            }

            return myCapital;
        }

        private void LoadInfo() {
            var myFactory = new CPIFactory();
            var myCapital = myFactory.GetCapital(CIID);

            if (myCapital != null) {
                if (myCapital.CompanyCIID > -1) {
                    if (myCapital.Asked > -1) {
                        txtAsked.Text = formatNumber(myCapital.Asked);
                    }
                    if (myCapital.AuthorizedCapital > -1) {
                        txtAuthorizedCapital.Text = formatNumber(myCapital.AuthorizedCapital);
                    }
                    if (myCapital.IssuedNumberOfShares > -1) {
                        txtIssuedNoOfShares.Text = formatNumber(myCapital.IssuedNumberOfShares);
                    }
                    if (myCapital.NominalNumberOfShares > -1) {
                        txtNominalNumberOfShares.Text = formatNumber(myCapital.NominalNumberOfShares);
                    }
                    if (myCapital.PaidUp > -1) {
                        txtPaidUp.Text = formatNumber(myCapital.PaidUp);
                    }

                    ddlstCurrency.SelectedValue = myCapital.CurrencyCode;

                    txtSharesDescription.Text = myCapital.SharesDescription;

                    txtShareClass.Text = myCapital.ShareClass;

                    btnReg.Text = rm.GetString("txtUpdate", ci);
                } else {
                    btnReg.Text = rm.GetString("txtSave", ci);
                    //H�r v�ri h�gt a� setja in N/A � �ll field
                }
            } else {
                DisplayErrorMessage("Error getting data from database");
            }
        }

        public string formatNumber(decimal number) { return number.ToString("N", fInfo); }

        private void DisplayMessage(String message) {
            lblErrorMessage.ForeColor = Color.Blue;
            lblErrorMessage.Text = message;
            lblErrorMessage.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblErrorMessage.ForeColor = Color.Red;
            lblErrorMessage.Text = errorMessage;
            lblErrorMessage.Visible = true;
        }

        private void cvIssuedNumberOfShares_ServerValidate(object source, ServerValidateEventArgs args) {
            if (args.Value != "") {
                try {
                    args.IsValid = decimal.Parse(args.Value) > -1;
                } catch (Exception) {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        private void cvAsked_ServerValidate(object source, ServerValidateEventArgs args) {
            if (args.Value != "") {
                try {
                    args.IsValid = decimal.Parse(args.Value) > -1;
                } catch (Exception) {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        private void cvPaidUp_ServerValidate(object source, ServerValidateEventArgs args) {
            if (args.Value != "") {
                try {
                    args.IsValid = decimal.Parse(args.Value) > -1;
                } catch (Exception) {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        private void cvAuthorizedCapital_ServerValidate(object source, ServerValidateEventArgs args) {
            if (args.Value != "") {
                try {
                    args.IsValid = decimal.Parse(args.Value) > -1;
                } catch (Exception) {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        private void cvNominalNumberOfShares_ServerValidate(object source, ServerValidateEventArgs args) {
            if (args.Value != "") {
                try {
                    args.IsValid = decimal.Parse(args.Value) > -1;
                } catch (Exception) {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        private void btnLoadFromCompanyRegistry_Click(object sender, EventArgs e) {
            var factory = new CPIFactory();
            string nationalId = factory.GetNationalIDByCIID(CIID);
            if (nationalId == null) {
                return;
            }
            DataSet ds = factory.GetCapitalFromCompanyRegistryAsDataSet(nationalId);
            if (ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0) {
                return;
            }
            txtShareClass.Text = ds.Tables[0].Rows[0]["ShareClass"].ToString();
            txtAuthorizedCapital.Text = ds.Tables[0].Rows[0]["AuthCap"].ToString();
            txtNominalNumberOfShares.Text = ds.Tables[0].Rows[0]["Nominal"].ToString();
            txtIssuedNoOfShares.Text = ds.Tables[0].Rows[0]["Issued"].ToString();
            txtPaidUp.Text = ds.Tables[0].Rows[0]["PaidUp"].ToString();
            btnReg_Click(this, null);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cvAuthorizedCapital.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.cvAuthorizedCapital_ServerValidate);
            this.cvIssuedNumberOfShares.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.cvIssuedNumberOfShares_ServerValidate);
            this.cvNominalNumberOfShares.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.cvNominalNumberOfShares_ServerValidate);
            this.cvAsked.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.cvAsked_ServerValidate);
            this.cvPaidUp.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.cvPaidUp_ServerValidate);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnLoadFromCompanyRegistry.Click += new System.EventHandler(this.btnLoadFromCompanyRegistry_Click);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}
#pragma warning enable 618,612