<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="Principals.aspx.cs" AutoEventWireup="false" Inherits="CPI.Principals" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Principals</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						BoardMembersForm.btnReg.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.BoardMembersForm.tbFirstName.focus();
				}

		</script>
	</HEAD>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="BoardMembersForm" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblPrincipals" runat="server">Principals</asp:label></td>
																	<td align="right"><asp:label id="lblPageStep" runat="server">Step</asp:label></td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lblCIID" runat="server">CIID</asp:label><br>
																		<asp:textbox id="txtCIID" runat="server" maxlength="13"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblPersonalID" runat="server" cssclass="label">Personal ID</asp:label><br>
																		<asp:textbox id="tbNationalID" runat="server" maxlength="100"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" cssclass="label" controltovalidate="tbNationalID"
																			errormessage="Value missing!">*</asp:requiredfieldvalidator></td>
																	<td style="WIDTH: 25%"><asp:label id="lblFirstName" runat="server" cssclass="label">Firstname</asp:label><br>
																		<asp:textbox id="tbFirstName" runat="server" maxlength="50"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" cssclass="label" controltovalidate="tbFirstName"
																			errormessage="Value missing!">*</asp:requiredfieldvalidator></td>
																	<td><asp:label id="lblSurname" runat="server" cssclass="label">Surname</asp:label><br>
																		<asp:textbox id="tbSurname" runat="server" maxlength="50"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" cssclass="label" controltovalidate="tbSurname"
																			errormessage="Value missing!">*</asp:requiredfieldvalidator></td>
																</tr>
																<tr>
																	<td align="right" colspan="4"><asp:button id="btnSearch" runat="server" cssclass="search_button" text="Search" causesvalidation="False"></asp:button>&nbsp;
																		<input onclick="GoNordurNidur('txtCIID','tbNationalID','tbFirstName','tbSurname','2', 670, 900);"
																			type="button" value="..." class="popup"></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblSubHeader" runat="server">[SubHeader]</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0" border="0">
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lblEducation" runat="server" cssclass="label">Education</asp:label><br>
																		<asp:dropdownlist id="ddEducation" runat="server">
																			<asp:listitem value="N/A">N/A</asp:listitem>
																		</asp:dropdownlist></td>
																	<td style="WIDTH: 25%"><asp:label id="lblJobtitle" runat="server" cssclass="label">Job title</asp:label><br>
																		<asp:dropdownlist id="ddJobtitle" runat="server">
																			<asp:listitem value="No particular">Job title</asp:listitem>
																		</asp:dropdownlist></td>
																	<td style="WIDTH: 25%"><asp:label id="lblAddress" runat="server" cssclass="label">Address</asp:label><br>
																		<asp:textbox id="tbAddress" runat="server" maxlength="100"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblAreaCode" runat="server" cssclass="label">Area code</asp:label><br>
																		<asp:dropdownlist id="ddlAreaCode" runat="server">
																			<asp:listitem value="N/A">N/A</asp:listitem>
																		</asp:dropdownlist></td>
																</tr>
																<!-- gpg addet -->
																<tr>
																	<td colspan="2"><asp:label id="lblHistoryNative" runat="server">History Native</asp:label><br>
																		<asp:textbox id="tbHistoryNative" runat="server" width="100%" maxlength="500" textmode="MultiLine"
																			height="150px"></asp:textbox></td>
																	<td colspan="2"><asp:label id="lblHistoryEN" runat="server">History EN</asp:label><br>
																		<asp:textbox id="tbHistoryEN" runat="server" width="100%" maxlength="500" textmode="MultiLine"
																			height="150px"></asp:textbox></td>
																</tr>
																<!-- gpg add ends -->
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerNameSearchGridTable" cellspacing="0" cellpadding="0"
													runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dtgrNameSearch" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="CreditInfoID" headertext="Creditinfo ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="Number" headertext="National ID">
																						<headerstyle width="12%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Name">
																						<headerstyle width="30%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Address">
																						<headerstyle width="20%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetNative" headertext="AddressNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetEN" headertext="AddressEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="City">
																						<headerstyle width="20%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityNative" headertext="CityNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityEN" headertext="CityEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left"><asp:label id="lblMessage" runat="server" cssclass="confirm_text">Message</asp:label><asp:validationsummary id="ValidationSummary1" runat="server" cssclass="label" height="31px"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel" causesvalidation="False"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" text="Continue" causesvalidation="False"></asp:button><asp:button id="btnRegHistory" runat="server" cssclass="confirm_button" text="Reg. history"
																causesvalidation="False" commandname="Insert"></asp:button><asp:button id="btnReg" runat="server" cssclass="confirm_button" text="Reg." commandname="Insert"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerPrincipalsGridTable" cellspacing="0" cellpadding="0"
													runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblPrincipalsDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblPrincipalsDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dtgrPrincipals" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False">
																			<FooterStyle CssClass="grid_footer"></FooterStyle>
																			<SelectedItemStyle CssClass="grid_selecteditem"></SelectedItemStyle>
																			<AlternatingItemStyle CssClass="grid_alternatingitem"></AlternatingItemStyle>
																			<ItemStyle CssClass="grid_item"></ItemStyle>
																			<HeaderStyle CssClass="grid_header"></HeaderStyle>
																			<Columns>
																				<asp:ButtonColumn Text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;/&gt;"
																					CommandName="Update">
																					<ItemStyle CssClass="leftpadding"></ItemStyle>
																				</asp:ButtonColumn>
																				<asp:ButtonColumn Text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;/&gt;"
																					CommandName="Delete">
																					<ItemStyle CssClass="nopadding"></ItemStyle>
																				</asp:ButtonColumn>
																				<asp:BoundColumn DataField="CreditInfoID" HeaderText="CreditInfoID">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="Number" HeaderText="National ID">
																					<HeaderStyle Width="10%"></HeaderStyle>
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn HeaderText="Name">
																					<HeaderStyle Width="24%"></HeaderStyle>
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="FirstNameNative" HeaderText="FirstNameNative">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="SurNameNative" HeaderText="SurNameNative">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="FirstNameEN" HeaderText="FirstNameEN">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="SurNameEN" HeaderText="SurNameEN">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn HeaderText="Title">
																					<HeaderStyle Width="20%"></HeaderStyle>
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="NameNative" HeaderText="TitleNative">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="NameEN" HeaderText="TitleEN">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn HeaderText="Education">
																					<HeaderStyle Width="20%"></HeaderStyle>
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="EdNameNative" HeaderText="EducationNative">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="EdNameEN" HeaderText="EducationEN">
																					<ItemStyle CssClass="padding"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="JobTitleID" HeaderText="JobTitleID"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle CssClass="grid_pager"></PagerStyle>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
