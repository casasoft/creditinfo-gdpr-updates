#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for Charges.
    /// </summary>
    public class Charges : BaseCPIPage {
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnReg;
        private int CIID = -1;
        protected CustomValidator Customvalidator1;
        protected CustomValidator cvAmount;
        protected CustomValidator cvDatePrepared;
        protected CustomValidator cvDateRegistered;
        protected DropDownList ddlstBeneficiary;
        protected DropDownList ddlstCurrency;
        protected DropDownList ddlstDescription;
        protected DataGrid dgCharges;
        protected Label lblAmount;
        protected Label lblBenificiary;
        protected Label lblCharges;
        protected Label lblCurrency;
        protected Label lblDatagridHeader;
        protected Label lblDatagridIcons;
        protected Label lblDatePrepared;
        protected Label lblDateRegistered;
        protected Label lblDescription;
        protected Label lblErrorMessage;
        protected Label lblFreeDescriptionEN;
        protected Label lblFreeDescriptionNative;
        protected Label lblPageStep;
        protected Label lblSequence;
        protected HtmlTable outerGridTable;
        protected TextBox tbFreeDescriptionEN;
        protected TextBox tbFreeDescriptionNative;
        protected TextBox txtAmount;
        protected TextBox txtDatePrepared;
        protected TextBox txtDateRegistered;
        protected TextBox txtID;
        protected TextBox txtSequence;
        protected ValidationSummary ValidationSummary;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "Charges";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new"); 
                //Server.Transfer("SearchForReport.aspx?redirect=Charges");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            lblErrorMessage.Visible = false;

            if (!Page.IsPostBack) {
                //Localization
                LocalizeText();
                //Localization
                InitDropDownLists();
                InitDates();
                LoadPage();
            }
        }

        private void InitDates() {
            txtDatePrepared.Text = DateTime.Today.ToShortDateString();
            txtDateRegistered.Text = DateTime.Today.ToShortDateString();
        }

        private void LoadPage() {
            var myFactory = new CPIFactory();
//			ChargesBLLC myCharges = myFactory.GetCharges(CIID);
            DataSet mySet = myFactory.GetChargesAsDataSet(CIID);

            if (mySet != null) {
                dgCharges.DataSource = mySet;
                dgCharges.DataBind();
            } else {
                DisplayErrorMessage("Error getting data from database");
            }
/*			if(myCharges != null)
			{
				if(myCharges.CreditInfoID > -1)
				{
					if(myCharges.Amount > -1)
						this.txtAmount.Text = "" + myCharges.Amount;
					this.ddlstBeneficiary.SelectedValue = myCharges.BankID;
					this.ddlstCurrency.SelectedValue = myCharges.CurrencyCode;
					if(myCharges.DatePrepared.Year > 1899)
						this.txtDatePrepared.Text = myCharges.DatePrepared.ToShortDateString();
					else
						this.txtDatePrepared.Text = "";
					if(myCharges.DateRegistered.Year > 1899)
						this.txtDateRegistered.Text = myCharges.DateRegistered.ToShortDateString();
					else
						this.txtDateRegistered.Text = "";
					if(myCharges.DescriptionID > -1)
						this.ddlstDescription.SelectedValue = myCharges.DescriptionID.ToString();
					this.txtSequence.Text = myCharges.Sequence;

					this.btnReg.Text = rm.GetString("txtUpdate",ci);
				}
				else
				{
					this.btnReg.Text = rm.GetString("txtSave",ci);
					//H�r v�ri h�gt a� setja in N/A � �ll field
				}
			}
			else
			{
				DisplayErrorMessage("Error getting data from database");  
			}
*/
        }

        private void InitDropDownLists() {
            var myFactory = new CPIFactory();

            BindBeneficiary(myFactory);
            BindCurrency(myFactory);
            BindDescription(myFactory);
        }

        private void BindDescription(CPIFactory myFactory) {
            var descriptionSet = myFactory.GetChargesDescriptionAsDataSet();
            ddlstDescription.DataSource = descriptionSet;
            ddlstDescription.DataTextField = !nativeCult ? "DescriptionEN" : "DescriptionNative";
            ddlstDescription.DataValueField = "descriptionID";
            ddlstDescription.DataBind();
        }

        private void BindBeneficiary(CPIFactory myFactory) {
            /*
			 DataSet dsBanks = new DataSet();
			if(Cache["dsBanks"] == null) 
			{
				dsBanks = myFact.GetBanksAsDataSet();
				Cache["dsBanks"] = dsBanks;
			} else
				dsBanks = (DataSet) Cache["dsBanks"];
			 */

            DataSet bankSet;
            if (Cache["dsChargesBanks"] == null) {
                bankSet = myFactory.GetBanksAsDataSet(!nativeCult);
                Cache["dsChargesBanks"] = bankSet;
            } else {
                bankSet = (DataSet) Cache["dsChargesBanks"];
            }

            ddlstBeneficiary.DataSource = bankSet;
            ddlstBeneficiary.DataTextField = !nativeCult ? "NameEN" : "NameNative";
            ddlstBeneficiary.DataValueField = "BankID";
            ddlstBeneficiary.DataBind();
            if (CigConfig.Configure("lookupsettings.orderedBankList") != null) {
                OrderBenificiaryList(CigConfig.Configure("lookupsettings.orderedBankList"));
            }
        }

        private void OrderBenificiaryList(string orderList) {
            string[] strArray = orderList.Split(new[] {','});
            //	this.ddlstBeneficiary.FindControl()
            int index = 0;
            foreach (string id in strArray) {
                ListItem li = ddlstBeneficiary.Items.FindByValue(id);
                ddlstBeneficiary.Items.Remove(li);
                ddlstBeneficiary.Items.Insert(index, li);
                index++;
            }
        }

        private void BindCurrency(CPIFactory myFactory) {
            var currencySet = myFactory.GetCurrencyAsDataSet();
            ddlstCurrency.DataSource = currencySet;
            ddlstCurrency.DataTextField = !nativeCult ? "CurrencyCode" : "CurrencyCode";
            ddlstCurrency.DataValueField = "CurrencyCode";
            ddlstCurrency.DataBind();

            // if currency order key in config then order the currency list ...
            if (CigConfig.Configure("lookupsettings.orderCurrencyList") != null) {
                OrderCurrencyList(CigConfig.Configure("lookupsettings.orderCurrencyList"));
            }
        }

        private void OrderCurrencyList(string orderList) {
            string[] strArray = orderList.Split(new[] {','});
            int index = 0;
            foreach (string id in strArray) {
                ListItem li = ddlstCurrency.Items.FindByValue(id);
                ddlstCurrency.Items.Remove(li);
                ddlstCurrency.Items.Insert(index, li);
                index++;
            }
        }

        private void DisplayMessage(String message) {
            lblErrorMessage.ForeColor = Color.Blue;
            lblErrorMessage.Text = message;
            lblErrorMessage.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblErrorMessage.ForeColor = Color.Red;
            lblErrorMessage.Text = errorMessage;
            lblErrorMessage.Visible = true;
        }

        private void LocalizeText() {
            lblAmount.Text = rm.GetString("txtAmount", ci);
            lblBenificiary.Text = rm.GetString("txtBeneficiary", ci);
            lblCharges.Text = rm.GetString("txtCharges", ci);
            lblCurrency.Text = rm.GetString("txtCurrency", ci);
            lblDatePrepared.Text = rm.GetString("txtDatePrepared", ci);
            lblDateRegistered.Text = rm.GetString("txtDateRegistered", ci);
            lblDescription.Text = rm.GetString("txtType", ci);
            lblSequence.Text = rm.GetString("txtSequence", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            btnReg.Text = rm.GetString("txtSave", ci);
            btnCancel.Text = rm.GetString("txtBack", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            cvAmount.ErrorMessage = rm.GetString("txtChargesError", ci);
            cvDatePrepared.ErrorMessage = rm.GetString("txtChargesError", ci);
            cvDateRegistered.ErrorMessage = rm.GetString("txtChargesError", ci);
            lblFreeDescriptionNative.Text = rm.GetString("txtDescription", ci);
            lblFreeDescriptionEN.Text = rm.GetString("txtDescriptionEN", ci);

            lblDatagridHeader.Text = rm.GetString("txtCharges", ci);

            SetGridHeaders();
        }

        private void SetGridHeaders() {
            dgCharges.Columns[3].HeaderText = rm.GetString("txtDateRegistered", ci);
            dgCharges.Columns[4].HeaderText = rm.GetString("txtDatePrepared", ci);

            dgCharges.Columns[6].HeaderText = rm.GetString("txtType", ci);
            dgCharges.Columns[7].HeaderText = rm.GetString("txtType", ci);
            dgCharges.Columns[9].HeaderText = rm.GetString("txtBeneficiary", ci);
            dgCharges.Columns[10].HeaderText = rm.GetString("txtBeneficiary", ci);

            dgCharges.Columns[11].HeaderText = rm.GetString("txtAmount", ci);
            dgCharges.Columns[12].HeaderText = rm.GetString("txtCurrency", ci);
            dgCharges.Columns[13].HeaderText = rm.GetString("txtSequence", ci);

            if (!nativeCult) {
                dgCharges.Columns[6].Visible = false;
                dgCharges.Columns[7].Visible = true;
                dgCharges.Columns[9].Visible = false;
                dgCharges.Columns[10].Visible = true;
            } else {
                dgCharges.Columns[6].Visible = true;
                dgCharges.Columns[7].Visible = false;
                dgCharges.Columns[9].Visible = true;
                dgCharges.Columns[10].Visible = false;
            }
        }

        private ChargesBLLC GetInfoFromPage() {
            var myCharges = new ChargesBLLC();

            if (txtID.Text.Trim().Length > 0) {
                try {
                    myCharges.ID = int.Parse(txtID.Text.Trim());
                } catch (Exception) {
                    return null;
                }
            }

            if (txtAmount.Text != "") {
                myCharges.Amount = decimal.Parse(txtAmount.Text);
            }
            myCharges.BankID = ddlstBeneficiary.SelectedValue;
            if (txtDatePrepared.Text != "") {
                myCharges.DatePrepared = DateTime.Parse(txtDatePrepared.Text);
            }
            if (txtDateRegistered.Text != "") {
                myCharges.DateRegistered = DateTime.Parse(txtDateRegistered.Text);
            }
            myCharges.DescriptionID = int.Parse(ddlstDescription.SelectedValue);
            myCharges.Sequence = txtSequence.Text;
            myCharges.CurrencyCode = ddlstCurrency.SelectedValue;
            if (tbFreeDescriptionEN.Text != "") {
                myCharges.DescriptionFreeTextEN = tbFreeDescriptionEN.Text;
            }
            if (tbFreeDescriptionNative.Text != "") {
                myCharges.DescriptionFreeTextNative = tbFreeDescriptionNative.Text;
            }

            return myCharges;
        }

        private void btnReg_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {
                return;
            }
            var myCharges = GetInfoFromPage();

            if (myCharges != null) {
                var myFactory = new CPIFactory();

                if (myFactory.AddCharges(myCharges, CIID)) {
                    DisplayMessage(rm.GetString("txtInfoSaved", ci));
                    ClearBoxes();
                    LoadPage();
                } else {
                    DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
                }
            } else {
                DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
            }
        }

        private void ClearBoxes() {
            txtID.Text = "";
            txtAmount.Text = "";
            txtDatePrepared.Text = "";
            txtDateRegistered.Text = "";
            txtSequence.Text = "";

            ddlstBeneficiary.SelectedIndex = 0;
            ddlstDescription.SelectedIndex = 0;
            ddlstCurrency.SelectedIndex = 0;

            InitDates();
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }
        private void btnCancel_Click(object sender, EventArgs e) { TransferToPreviousPage(); }

        private void dgCharges_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);
            }

            WebDesign.CreateExplanationIcons(dgCharges.Columns, lblDatagridIcons, rm, ci);
        }

        private void dgCharges_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("update")) {
                if (e.Item.Cells[2].Text.Trim() != "&nbsp;") {
                    txtID.Text = e.Item.Cells[2].Text.Trim();
                }

                if (e.Item.Cells[11].Text.Trim() != "&nbsp;") {
                    txtAmount.Text = e.Item.Cells[11].Text.Trim();
                }
                if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                    txtDatePrepared.Text = e.Item.Cells[4].Text.Trim();
                }
                if (e.Item.Cells[3].Text.Trim() != "&nbsp;") {
                    txtDateRegistered.Text = e.Item.Cells[3].Text.Trim();
                }

                if (e.Item.Cells[13].Text.Trim() != "&nbsp;") {
                    txtSequence.Text = e.Item.Cells[13].Text.Trim();
                }

                if (e.Item.Cells[12].Text.Trim() != "&nbsp;") {
                    ddlstCurrency.SelectedValue = e.Item.Cells[12].Text.Trim();
                }
                if (e.Item.Cells[8].Text.Trim() != "&nbsp;") {
                    ddlstBeneficiary.SelectedValue = e.Item.Cells[8].Text.Trim();
                }
                if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                    ddlstDescription.SelectedValue = e.Item.Cells[5].Text.Trim();
                }
                if (e.Item.Cells[14].Text.Trim() != "&nbsp;") {
                    tbFreeDescriptionNative.Text = e.Item.Cells[14].Text.Trim();
                }
                if (e.Item.Cells[15].Text.Trim() != "&nbsp;") {
                    tbFreeDescriptionEN.Text = e.Item.Cells[15].Text.Trim();
                }
            } else if (e.CommandName.Equals("delete")) {
                int ID = -1;
                try {
                    ID = int.Parse(e.Item.Cells[2].Text.Trim());
                } catch (Exception) {
                    ID = -1;
                }
                if (ID > -1) {
                    var myFactory = new CPIFactory();
                    myFactory.DeleteCharges(ID);

                    LoadPage();
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            this.dgCharges.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCharges_ItemCommand);
            this.dgCharges.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCharges_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}