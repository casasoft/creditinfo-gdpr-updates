#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.Localization;

using Cig.Framework.Base.Configuration;


#endregion

namespace CPI {
    /// <summary>
    /// Summary description for AddBank.
    /// </summary>
    public class AddBank : Page {
        public static CultureInfo ci;
        private static string pageName = "AddBank.aspx";
        public static ResourceManager rm;
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnAdd;
        protected Button btnCancel;
        private int CIID = -1;
        public string currentPageStep;
        protected Label lblErrMsg;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        protected ListBox lbxBanks;
        private bool nativeCult;
        public string totalPageSteps;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = CigConfig.Configure("lookupsettings.PageIDAddBank.aspx");

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=AddBank");
            }
            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) {
                Server.Transfer("BasicInformation.aspx?action=update");
            }
            // check the current culture
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            String nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            lblErrMsg.Visible = false;
            if (!IsPostBack) {
                InitControls();
            }
        }

        private void InitControls() {
            BindBankBoxes();
            SetPageSteps();
            LocalizeText();
        }

        private void btnCancel_Click(object sender, EventArgs e) { Server.Transfer("Operation.aspx"); }

        public void BindBankBoxes() {
            DataSet dsBanks = new DataSet();
            if (Cache["dsBanks"] == null) {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null &&
                    CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                    dsBanks = myFact.GetMTBanksAsDataSet();
                } else {
                    dsBanks = myFact.GetBanksAsDataSet(nativeCult);
                }
                Cache["dsBanks"] = dsBanks;
            } else {
                dsBanks = (DataSet) Cache["dsBanks"];
            }

            if (dsBanks.Tables.Count > 0) {
                lbxBanks.DataSource = dsBanks;
                if (nativeCult) {
                    lbxBanks.DataTextField = "NameNative";
                } else {
                    lbxBanks.DataTextField = "NameEN";
                }
                lbxBanks.DataValueField = "BankID";
                lbxBanks.DataBind();
            } else {
                lblErrMsg.Text = "Could not get bank overview";
                lblErrMsg.Visible = true;
            }
            if (CigConfig.Configure("lookupsettings.orderedBankList") != null) {
                OrderBankList(CigConfig.Configure("lookupsettings.orderedBankList"));
            }
            //
        }

        private void OrderBankList(string orderList) {
            string[] strArray;
            strArray = orderList.Split(new[] {','});
            int index = 0;
            foreach (string id in strArray) {
                ListItem li = lbxBanks.Items.FindByValue(id);
                lbxBanks.Items.Remove(li);
                lbxBanks.Items.Insert(index, li);
                index++;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            ArrayList arrCB = new ArrayList();
            foreach (ListItem item in lbxBanks.Items) {
                if (item.Selected) {
                    CheckBoxValues theCBL = new CheckBoxValues();
                    theCBL.Value = item.Value;
                    ;
                    theCBL.Checked = item.Selected;
                    theCBL.Text = item.Text;
                    arrCB.Add(theCBL);
                }
            }
            Session["arrAddetBanks"] = arrCB;
            Server.Transfer("Operation.aspx");
        }

        private void SetPageSteps() {
            // get the totalPageSteps
            if (Cache["TotalPageSteps"] != null) {
                totalPageSteps = (string) Cache["TotalPageSteps"];
            } else {
                totalPageSteps = CigConfig.Configure("lookupsettings.TotalPageSteps");
                Cache["TotalPageSteps"] = totalPageSteps;
            }
            // get current page step
            currentPageStep = CigConfig.Configure(pageName);
        }

        private void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtAddBank", ci);
            btnAdd.Text = rm.GetString("txtAdd", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnAdd.Click += new EventHandler(this.btnAdd_Click);
            this.ID = "AddBankFo";
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}