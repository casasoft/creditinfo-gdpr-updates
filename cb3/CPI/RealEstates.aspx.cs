#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for RealEstates.
    /// </summary>
    public class RealEstates : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        protected HtmlTableCell blas;
        protected Button btnCancel;
        protected Button btnCitySearch;
        protected Button btnContinue;
        protected Button btnReg;
        private int CIID = 1;
        protected DropDownList ddAreaCode;
        protected DropDownList ddlCity;
        protected DropDownList ddlCountry;
        protected DropDownList ddLocation;
        protected DropDownList ddPermisesType;
        protected DropDownList ddTenure;
        private DataSet dsEstateAreaCodes = new DataSet();
        private DataSet dsEstates = new DataSet();
        private DataSet dsEstatesCountry = new DataSet();
        private DataSet dsEstatesLocation = new DataSet();
        private DataSet dsEstatesPermises = new DataSet();
        private DataSet dsEstatesTenure = new DataSet();
        protected DataGrid dtgRealEstates;
        protected NumberFormatInfo fInfo;
        protected Label lblAddress;
        protected Label lblAreaCode;
        protected Label lblCity;
        protected Label lblCitySearch;
        protected Label lblCountry;
        protected Label lblDatagridHeader;
        protected Label lblDatagridIcons;
        protected Label lblInsuranceValue;
        protected Label lblLocation;
        protected Label lblMessage;
        protected Label lblPageStep;
        protected Label lblPermisesType;
        protected Label lblRealEstates;
        protected Label lblSize;
        protected Label lblTenure;
        protected Label lblYearBuilt;
        protected HtmlTable outerGridTable;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected RequiredFieldValidator rfvCity;
        protected TextBox tbAddress;
        protected TextBox tbInsuranceValue;
        protected TextBox tbSize;
        protected TextBox tbYearBuilt;
        protected TextBox txtCitySearch;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "RealEstates";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new"); 
                //Server.Transfer("SearchForReport.aspx?redirect=RealEstates");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            AddEnterEvent();

            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech") ||
                    CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus")) {
                    RequiredFieldValidator2.Enabled = false;
                    RequiredFieldValidator3.Enabled = false;
                }
            }
            fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            fInfo.NumberDecimalDigits = 2;

            lblMessage.Visible = false;
            FillDataSets();
            SetGridHeaders();

            tbInsuranceValue.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" +
                                                    fInfo.NumberGroupSeparator + "', '" + fInfo.NumberDecimalSeparator +
                                                    "', 2,'NaN');";

            if (!IsPostBack) {
                BindBoxes();
                InitRealEstatesGrid();
                BindRealEstatesGrid();
                LocalizeText();
            }

            if (CigConfig.Configure("lookupsettings.currentVersion") != null) {
                if (CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                    RequiredFieldValidator2.Enabled = false;
                    RequiredFieldValidator3.Enabled = false;
                    try {
                        ddlCountry.SelectedValue = "126"; //Malta
                    } catch (Exception) {}
                }
            }
        }

        private void FillDataSets() {
            FillAreaCodesSet();
            FillTenureSet();
            FillLocationSet();
            FillPermisesSet();
            FillEstatesSet();
            FillCountrySet();
        }

        private void BindBoxes() {
            BindAreaCodeBox();
            BindTenureBox();
            BindLocationBox();
            BindPermisesBox();
            BindCountryBox();
        }

        private void FillAreaCodesSet() {
            // DataSet stuff
            if (Cache["dsEstateAreaCodes"] != null) {
                dsEstateAreaCodes = (DataSet) Cache["dsEstateAreaCodes"];
            } else {
                dsEstateAreaCodes = myFact.GetEstatesPostalCodeAsDataSet();
                Cache["dsEstateAreaCodes"] = dsEstateAreaCodes;
            }
        }

        private void FillTenureSet() {
            // DataSet stuff
            if (Cache["dsEstatesTenure"] != null) {
                dsEstatesTenure = (DataSet) Cache["dsEstatesTenure"];
            } else {
                dsEstatesTenure = myFact.GetTenureAsDataSet();
                Cache["dsEstatesTenure"] = dsEstatesTenure;
            }
        }

        private void FillLocationSet() {
            // DataSet stuff...
            if (Cache["dsEstatesLocation"] != null) {
                dsEstatesLocation = (DataSet) Cache["dsEstatesLocation"];
            } else {
                dsEstatesLocation = myFact.GetRealEstatesLocationAsDataSet();
                Cache["dsEstatesLocation"] = dsEstatesLocation;
            }
        }

        private void FillPermisesSet() {
            // DataSet stuff ...
            if (Cache["dsEstatesPermises"] != null) {
                dsEstatesPermises = (DataSet) Cache["dsEstatesPermises"];
            } else {
                dsEstatesPermises = myFact.GetEstatesPermisesTypeAsDataSet();
                Cache["dsEstatesPermises"] = dsEstatesPermises;
            }
        }

        private void FillEstatesSet() {
            if (Session["dsEstates"] != null) {
                dsEstates = (DataSet) Session["dsEstates"];
            } else {
                dsEstates = myFact.GetRealEstatesAsDataSet(CIID);
                Session["dsEstates"] = dsEstates;
            }
        }

        private void FillCountrySet() {
            if (Session["dsEstatesCountry"] != null) {
                dsEstatesCountry = (DataSet) Session["dsEstatesCountry"];
            } else {
                dsEstatesCountry = myFact.GetCountriesAsDataSet(nativeCult);
                // disable caching because the user can change culture and how the countries are ordered
                //		Session["dsEstatesCountry"] = dsEstatesCountry;
            }
        }

        public void BindAreaCodeBox() {
            // DataBind stuff
            ddAreaCode.DataSource = dsEstateAreaCodes;
            ddAreaCode.DataTextField = "PostalCode";
            ddAreaCode.DataBind();
            try {
                ddAreaCode.SelectedValue = "N/A";
            } catch (Exception) {}
        }

        public void BindTenureBox() {
            // DataBind stuff
            ddTenure.DataSource = dsEstatesTenure;
            ddTenure.DataTextField = nativeCult ? "OwnerTypeNative" : "OwnerTypeEN";
            ddTenure.DataValueField = "OwnerTypeID";
            ddTenure.DataBind();
        }

        public void BindLocationBox() {
            // DataBind stuff...			
            if (nativeCult) {
                dsEstatesLocation.Tables[0].DefaultView.Sort = "DescriptionNative";
                ddLocation.DataSource = dsEstatesLocation;
                ddLocation.DataTextField = "DescriptionNative";
            } else {
                dsEstatesLocation.Tables[0].DefaultView.Sort = "DescriptionEN";
                ddLocation.DataSource = dsEstatesLocation;
                ddLocation.DataTextField = "DescriptionEN";
            }
            ddLocation.DataValueField = "LocationID";
            ddLocation.DataBind();
        }

        public void BindPermisesBox() {
            // DataBind stuff..
            //this.ddPermisesType.DataSource = dsEstatesPermises;
            if (nativeCult) {
                dsEstatesPermises.Tables[0].DefaultView.Sort = "TypeNative";
                ddPermisesType.DataSource = dsEstatesPermises.Tables[0].DefaultView;
                ddPermisesType.DataTextField = "TypeNative";
            } else {
                dsEstatesPermises.Tables[0].DefaultView.Sort = "TypeEN";
                ddPermisesType.DataSource = dsEstatesPermises.Tables[0].DefaultView;
                ddPermisesType.DataTextField = "TypeEN";
            }
            ddPermisesType.DataValueField = "RealEstatesTypeID";
            ddPermisesType.DataBind();
        }

        private void BindCityBox(DataSet dsCity) {
            ddlCity.DataSource = dsCity;
            ddlCity.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddlCity.DataValueField = "CityID";
            ddlCity.DataBind();
        }

        private void BindCountryBox() {
            // DataBind stuff..
            ddlCountry.DataSource = dsEstatesCountry;
            ddlCountry.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddlCountry.DataValueField = "CountryID";
            ddlCountry.DataBind();

            // if country order key in config then order the countrie list ...
            if (CigConfig.Configure("lookupsettings.orderedCountryList") != null) {
                OrderCountryList(CigConfig.Configure("lookupsettings.orderedCountryList"));
            }
        }

        private void OrderCountryList(string orderList) {
            string[] strArray = orderList.Split(new[] {','});
            int index = 0;
            foreach (string id in strArray) {
                ListItem li = ddlCountry.Items.FindByValue(id); //.lbxBanks.Items.FindByValue(id);
                ddlCountry.Items.Remove(li);
                ddlCountry.Items.Insert(index, li);
                index++;
            }
        }

        public void BindRealEstatesGrid() {
            dtgRealEstates.DataSource = dsEstates;
            dtgRealEstates.DataBind();
        }

        public void InitRealEstatesGrid() {
            dtgRealEstates.Columns[5].Visible = false;
            dtgRealEstates.Columns[7].Visible = false;
            dtgRealEstates.Columns[9].Visible = false;

            dtgRealEstates.Columns[6].Visible = false;
            dtgRealEstates.Columns[8].Visible = false;
            dtgRealEstates.Columns[10].Visible = false;
        }

        private void dtgRealEstates_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    e.Item.Cells[2].Text = e.Item.Cells[5].Text;
                    e.Item.Cells[3].Text = e.Item.Cells[7].Text;
                    e.Item.Cells[4].Text = e.Item.Cells[9].Text;
                } else {
                    e.Item.Cells[2].Text = e.Item.Cells[6].Text;
                    e.Item.Cells[3].Text = e.Item.Cells[8].Text;
                    e.Item.Cells[4].Text = e.Item.Cells[10].Text;
                }
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
            }

            WebDesign.CreateExplanationIcons(dtgRealEstates.Columns, lblDatagridIcons, rm, ci);
        }

        private void dtgRealEstates_ItemCommand(object source, DataGridCommandEventArgs e) {
            try {
                int estateID = Convert.ToInt32(dsEstates.Tables[0].Rows[e.Item.ItemIndex]["RealEstatesID"].ToString());
                switch (e.CommandName) {
                    case "Delete": {
                        int rowToDelete = e.Item.ItemIndex;
                        if (rowToDelete >= 0) {
                            dsEstates.Tables[0].Rows[rowToDelete].Delete();
                            myFact.DeleteRealEstate(estateID, CIID);
                        }
                        Session["dsEstates"] = null;
                        FillEstatesSet();
                        BindRealEstatesGrid();
                    }
                        break;
                    case "Update": {
                        // do some update stuff
                        tbAddress.Text = nativeCult ? dsEstates.Tables[0].Rows[e.Item.ItemIndex]["AddressNative"].ToString() : dsEstates.Tables[0].Rows[e.Item.ItemIndex]["AddressEN"].ToString();
                        tbSize.Text = dsEstates.Tables[0].Rows[e.Item.ItemIndex]["Size"].ToString();
                        tbYearBuilt.Text = dsEstates.Tables[0].Rows[e.Item.ItemIndex]["YearBuilt"].ToString();
                        ddTenure.SelectedValue = dsEstates.Tables[0].Rows[e.Item.ItemIndex]["OwnerTypeID"].ToString();
                        ddPermisesType.SelectedValue =
                            dsEstates.Tables[0].Rows[e.Item.ItemIndex]["RealEstatesTypeID"].ToString();
                        ddLocation.SelectedValue = dsEstates.Tables[0].Rows[e.Item.ItemIndex]["LocationID"].ToString();
                        ddAreaCode.SelectedValue = dsEstates.Tables[0].Rows[e.Item.ItemIndex]["PostCode"].ToString();
                        tbInsuranceValue.Text =
                            double.Parse(dsEstates.Tables[0].Rows[e.Item.ItemIndex]["InsuranceValue"].ToString()).ToString(
                                "N", fInfo);
                        //N� � gildi � gagnagrunn
                        string sCityID = dsEstates.Tables[0].Rows[e.Item.ItemIndex]["CityID"].ToString();
                        if (sCityID != null && sCityID.Trim() != "") {
                            int cityID = int.Parse(sCityID);
                            if (cityID > -1) {
                                string cityName = myFact.GetCityName(cityID, !nativeCult);
                                if (!string.IsNullOrEmpty(cityName)) {
                                    ddlCity.Items.Add(new ListItem(cityName, cityID.ToString()));
                                    ddlCity.SelectedValue = cityID.ToString();
                                }
                            }
                        }
                        //this.ddlCity.SelectedValue = dsEstates.Tables[0].Rows[e.Item.ItemIndex]["CityID"].ToString();
                        ddlCountry.SelectedValue = dsEstates.Tables[0].Rows[e.Item.ItemIndex]["CountryID"].ToString();

                        Session["EstateID"] =
                            Convert.ToInt32(dsEstates.Tables[0].Rows[e.Item.ItemIndex]["RealEstatesID"].ToString());
                        btnReg.CommandName = "Update";
                        btnReg.Text = rm.GetString("txtUpdate", ci);
                    }
                        break;
                }
            } catch (Exception err) {
                Logger.WriteToLog("RealEstates.aspx : dtgRealEstates_ItemCommand " + err.Message, true);
            }
        }

        private void btnReg_Click(object sender, EventArgs e) {
            var forReal = new RealEstatesBLLC();
            if (nativeCult) {
                forReal.AddressNative = tbAddress.Text;
            } else {
                forReal.AddressEN = tbAddress.Text;
            }

            forReal.AreaCode = ddAreaCode.SelectedValue;
            forReal.Location = Convert.ToInt32(ddLocation.SelectedValue);
            forReal.PermisesType = ddPermisesType.SelectedValue;
            forReal.Size = tbSize.Text;
            forReal.Tenure = Convert.ToInt32(ddTenure.SelectedValue);
            forReal.YearBuilt = tbYearBuilt.Text;
            forReal.EstateTypeID = Convert.ToInt32(ddPermisesType.SelectedValue);
            if (tbInsuranceValue.Text != "") {
                forReal.InsuranceValue = float.Parse(tbInsuranceValue.Text);
            }
            if (ddlCity.SelectedValue.Trim() != "") {
                forReal.CityID = Convert.ToInt32(ddlCity.SelectedValue);
            }
            forReal.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
            if (btnReg.CommandName == "Update") {
                forReal.RealEstateID = (int) Session["EstateID"];
            }
            if (myFact.AddRealEstate(forReal, CIID)) {
                DisplayMessage(rm.GetString("txtTheInfoHasBeenRegisted", ci));

                Session["dsEstates"] = null;
                FillEstatesSet();
                BindRealEstatesGrid();
                EmptyBoxes();
                BindBoxes();
            } else {
                DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
            }
        }

        private void DisplayMessage(String message) {
            lblMessage.Text = message;
            lblMessage.ForeColor = Color.Blue;
            lblMessage.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblMessage.Text = errorMessage;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Visible = true;
        }

        private void EmptyBoxes() {
            tbAddress.Text = "";
            tbSize.Text = "";
            tbYearBuilt.Text = "";
            // init button etc
            btnReg.Text = rm.GetString("txtRegister", ci);
            ddTenure.SelectedIndex = 0;
            ddPermisesType.SelectedIndex = 0;
            ddLocation.SelectedIndex = 0;
            ddAreaCode.SelectedIndex = 0;
            tbInsuranceValue.Text = "";
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }

        private void LocalizeText() {
            lblRealEstates.Text = rm.GetString("txtRealEstates", ci);
            lblAddress.Text = rm.GetString("txtAddress", ci);
            lblAreaCode.Text = rm.GetString("txtAreaCode", ci);
            lblSize.Text = rm.GetString("txtSize", ci);
            lblYearBuilt.Text = rm.GetString("txtYearBuilt", ci);
            lblPermisesType.Text = rm.GetString("txtTypeOfPermises", ci);
            lblLocation.Text = rm.GetString("txtLocation", ci);
            RequiredFieldValidator1.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator3.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblTenure.Text = rm.GetString("txtTenure", ci);
            btnReg.Text = rm.GetString("txtSave", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            btnCancel.Text = rm.GetString("txtBack", ci);
            lblInsuranceValue.Text = rm.GetString("txtInsuranceValue", ci);
            lblCity.Text = rm.GetString("txtCity", ci);
            lblCountry.Text = rm.GetString("txtCountry", ci);
            rfvCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblCitySearch.Text = rm.GetString("txtSearch", ci);

            lblDatagridHeader.Text = rm.GetString("txtRealEstates", ci);
        }

        private void SetGridHeaders() {
            dtgRealEstates.Columns[2].HeaderText = rm.GetString("txtAddress", ci);
            dtgRealEstates.Columns[3].HeaderText = rm.GetString("txtTypeOfPermises", ci);
            dtgRealEstates.Columns[4].HeaderText = rm.GetString("txtLocation", ci);
        }

        private void btnCancel_Click(object sender, EventArgs e) { TransferToPreviousPage(); }

        private void AddEnterEvent() {
            tbAddress.Attributes.Add("onkeypress", "checkEnterKey();");
            ddAreaCode.Attributes.Add("onkeypress", "checkEnterKey();");
            tbSize.Attributes.Add("onkeypress", "checkEnterKey();");
            tbYearBuilt.Attributes.Add("onkeypress", "checkEnterKey();");
            ddPermisesType.Attributes.Add("onkeypress", "checkEnterKey();");
            ddLocation.Attributes.Add("onkeypress", "checkEnterKey();");
            ddTenure.Attributes.Add("onkeypress", "checkEnterKey();");
            ddlCity.Attributes.Add("onkeypress", "checkEnterKeyForCity();");
        }

        private void btnCitySearch_Click(object sender, EventArgs e) {
            if (txtCitySearch.Text.Trim() != "") {
                DataSet dsCity = myFact.GetCityListAsDataSet(txtCitySearch.Text, !nativeCult);
                BindCityBox(dsCity);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCitySearch.Click += new EventHandler(this.btnCitySearch_Click);
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnReg.Click += new EventHandler(this.btnReg_Click);
            this.btnContinue.Click += new EventHandler(this.btnContinue_Click);
            this.dtgRealEstates.ItemCommand += new DataGridCommandEventHandler(this.dtgRealEstates_ItemCommand);
            this.dtgRealEstates.ItemDataBound += new DataGridItemEventHandler(this.dtgRealEstates_ItemDataBound);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}