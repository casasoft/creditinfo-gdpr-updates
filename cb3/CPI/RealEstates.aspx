<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="RealEstates.aspx.cs" AutoEventWireup="false" Inherits="CPI.RealEstates" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>RealEstates</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						RealEstatesForm.btnReg.click(); 
					}
				} 
				
				function checkEnterKeyForCity() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						RealEstatesForm.btnCitySearch.click(); 
					}
				}
		
				function SetFormFocus()
				{
					document.RealEstatesForm.tbAddress.focus();
				}

		</script>
	</HEAD>
	<body ms_positioning="GridLayout" onload="SetFormFocus()">
		<form id="RealEstatesForm" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblRealEstates" runat="server">Real estates</asp:label>
																	</td>
																	<td align="right">
																		<asp:label id="lblPageStep" runat="server">Step</asp:label>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 25%">
																		<asp:label id="lblAddress" runat="server">Address</asp:label>
																		<br>
																		<asp:textbox id="tbAddress" runat="server" maxlength="100"></asp:textbox>
																		<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" errormessage="Value missing!" controltovalidate="tbAddress">*</asp:requiredfieldvalidator>
																	</td>
																	<td style="WIDTH: 25%">
																		<asp:label id="lblAreaCode" runat="server">Area code</asp:label>
																		<br>
																		<asp:dropdownlist id="ddAreaCode" runat="server"></asp:dropdownlist>
																	</td>
																	<td style="WIDTH: 25%">
																		<asp:label id="lblCity" runat="server">City</asp:label>
																		<asp:label id="lblCitySearch" runat="server">Search</asp:label>
																		<br>
																		<asp:dropdownlist id="ddlCity" runat="server"></asp:dropdownlist>&nbsp;
																		<asp:textbox id="txtCitySearch" runat="server" cssclass="short_input" maxlength="10"></asp:textbox>&nbsp;
																		<asp:button id="btnCitySearch" runat="server" text="..." causesvalidation="False" cssclass="popup"></asp:button>
																		<asp:requiredfieldvalidator id="rfvCity" runat="server" controltovalidate="ddlCity" errormessage="Value missing!"
																			display="None">*</asp:requiredfieldvalidator>
																	</td>
																	<td style="WIDTH: 25%">
																		<asp:label id="lblCountry" runat="server">Country</asp:label>
																		<br>
																		<asp:dropdownlist id="ddlCountry" runat="server"></asp:dropdownlist>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblSize" runat="server">Size</asp:label>
																		<br>
																		<asp:textbox id="tbSize" runat="server" maxlength="9"></asp:textbox>
																		<asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" errormessage="Value missing!" controltovalidate="tbSize">*</asp:requiredfieldvalidator>
																	</td>
																	<td>
																		<asp:label id="lblYearBuilt" runat="server">Year built</asp:label><br>
																		<asp:textbox id="tbYearBuilt" runat="server" cssclass="short_input" maxlength="4"></asp:textbox>
																		<asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" errormessage="Value missing" controltovalidate="tbYearBuilt">*</asp:requiredfieldvalidator>
																	</td>
																	<td>
																		<asp:label id="lblInsuranceValue" runat="server">Insurance value</asp:label>
																		<br>
																		<asp:textbox id="tbInsuranceValue" runat="server" maxlength="8"></asp:textbox>
																	</td>
																	<td>
																		<asp:label id="lblPermisesType" runat="server">Type of permises</asp:label>
																		<br>
																		<asp:dropdownlist id="ddPermisesType" runat="server">
																			<asp:listitem value="Cinema">Cinema</asp:listitem>
																		</asp:dropdownlist>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblLocation" runat="server">Location</asp:label>
																		<br>
																		<asp:dropdownlist id="ddLocation" runat="server">
																			<asp:listitem value="Leased">Airport area</asp:listitem>
																		</asp:dropdownlist>
																	</td>
																	<td>
																		<asp:label id="lblTenure" runat="server">Tenure</asp:label>
																		<br>
																		<asp:dropdownlist id="ddTenure" runat="server">
																			<asp:listitem value="Cinema">Leased (with an option)</asp:listitem>
																		</asp:dropdownlist>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:label id="lblMessage" runat="server" cssclass="confirm_text"></asp:label>
															<asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary>
														</td>
														<td align="right">
															<asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel" causesvalidation="False"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" text="Continue" causesvalidation="False"></asp:button><asp:button id="btnReg" runat="server" cssclass="confirm_button" text="Reg."></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right">
															<asp:label id="lblDatagridIcons" runat="server"></asp:label>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblDatagridHeader" runat="server"></asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dtgRealEstates" runat="server" gridlines="None" autogeneratecolumns="False"
																			cssclass="grid">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;/&gt;"
																					commandname="Update">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;/&gt;"
																					commandname="Delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:boundcolumn headertext="Address">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Type">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Location">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="AddressNative" headertext="AddressNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="AddressEN" headertext="AddressEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="TypeNative" headertext="TypeNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="TypeEN" headertext="TypeEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="DescriptionNative" headertext="LocationNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="DescriptionEN" headertext="LocationEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
