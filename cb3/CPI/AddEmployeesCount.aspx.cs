#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.Localization;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for EmployeesCount.
    /// </summary>
    public class AddEmployeesCount : Page {
        public static CultureInfo ci;
        private static string pageName = "AddEmployeesCount.aspx";
        public static ResourceManager rm;
        private readonly CPIFactory myFact = new CPIFactory();
        protected HtmlTableCell blas;
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnRegStaffCount;
        private int CIID = -1;
        public string currentPageStep;
        protected CustomValidator CustomValidator1;
        protected CustomValidator CustomValidator10;
        protected CustomValidator CustomValidator2;
        protected CustomValidator CustomValidator3;
        protected CustomValidator CustomValidator4;
        protected CustomValidator CustomValidator5;
        protected CustomValidator CustomValidator6;
        protected CustomValidator CustomValidator7;
        protected CustomValidator CustomValidator8;
        protected CustomValidator CustomValidator9;
        private DataSet dsStaffCount;
        protected DataGrid dtgrStaffCountOverview;
        protected Label lblDatagridIcons;
        protected Label lblEmployeesCount;
        protected Label lblEmployeesOverview;
        protected Label lblMessage;
        protected Label lblPageStep;
        protected Label lblStaffCount;
        protected Label lblYear;
        protected HtmlTable outerGridTable;
        protected RangeValidator RangeValidator1;
        protected RangeValidator RangeValidator10;
        protected RangeValidator RangeValidator2;
        protected RangeValidator RangeValidator3;
        protected RangeValidator RangeValidator4;
        protected RangeValidator RangeValidator5;
        protected RangeValidator RangeValidator6;
        protected RangeValidator RangeValidator7;
        protected RangeValidator RangeValidator8;
        protected RangeValidator RangeValidator9;
        protected TextBox tbEmpCountYear1;
        protected TextBox tbEmpCountYear10;
        protected TextBox tbEmpCountYear2;
        protected TextBox tbEmpCountYear3;
        protected TextBox tbEmpCountYear4;
        protected TextBox tbEmpCountYear5;
        protected TextBox tbEmpCountYear6;
        protected TextBox tbEmpCountYear7;
        protected TextBox tbEmpCountYear8;
        protected TextBox tbEmpCountYear9;
        protected TextBox tbEmpYear1;
        protected TextBox tbEmpYear10;
        protected TextBox tbEmpYear2;
        protected TextBox tbEmpYear3;
        protected TextBox tbEmpYear4;
        protected TextBox tbEmpYear5;
        protected TextBox tbEmpYear6;
        protected TextBox tbEmpYear7;
        protected TextBox tbEmpYear8;
        protected TextBox tbEmpYear9;
        public string totalPageSteps;
        protected ValidationSummary ValidationSummary1;
        //	private CPIFactory theFact = new CPIFactory();	
        private void Page_Load(object sender, EventArgs e) {
            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();

            Page.ID = CigConfig.Configure("lookupsettings.PageIDAddEmployeesCount.aspx");

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=AddEmployeesCount");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            // check the current culture
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            String nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            FillDataSets();
            SetColumnsHeaderByCulture();
            lblMessage.Visible = false;
            if (!IsPostBack) {
                InitControls();
            }
        }

        private void InitControls() {
            BindStaffCountGrid();
            SetPageSteps();
            LocalizeText();
        }

        private void TextBox5_TextChanged(object sender, EventArgs e) { }

        private void btnRegStaffCount_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                //	NameValuesPair theVPair = new NameValuesPair(); // use StaffCount instead
                StaffCountBLLC theStaffCount = new StaffCountBLLC();
                ArrayList arrYearEmployees = new ArrayList();
                int countBoxes = 0;
                int countBoxesWText = 0;
                bool addToArray = false;
                System.Web.UI.Control theForm = FindControl("EmployeesCountForm");
                TextBox theBox = new TextBox();
                foreach (System.Web.UI.Control ctrl in theForm.Controls) {
                    if (ctrl.GetType() == typeof (TextBox)) {
                        theBox = (TextBox) ctrl;
                        countBoxes++;
                        if (theBox.Text != "") {
                            countBoxesWText++;
                            if (countBoxesWText%2 > 0) {
                                // then odd number and we should create new instance of StaffCountBLLC
                                theStaffCount = new StaffCountBLLC();
                                theStaffCount.CIID = CIID;
                                addToArray = false;
                            } else {
                                addToArray = true;
                            }

                            if (theBox.ID.IndexOf("EmpYear") > 0) {
                                theStaffCount.Year = theBox.Text;
                            } else {
                                if (theBox.Text != null) {
                                    theStaffCount.EmployeesCount = Convert.ToInt32(theBox.Text);
                                }
                            }
                            if (addToArray) {
                                arrYearEmployees.Add(theStaffCount);
                            }
                        }
                    }
                }
                if (arrYearEmployees.Count > 0) {
                    // a� uppf�ra strax e�a ekki �a� er spurningin
                    if (myFact.InsertEmployeeCountYears(arrYearEmployees, CIID)) {
                        Session["dsStaffCount"] = null;
                        FillDataSets();
                        BindStaffCountGrid();
                        ClearTextBoxes();
                    } else {
                        DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
                    }
                    //	Session["arrYearEmployees"] = arrYearEmployees;
                }
            }
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblMessage.Text = errorMessage;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Visible = true;
        }

        private void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args) {
            if ((tbEmpYear1.Text == "") || (tbEmpYear1.Text == null)) {
                args.IsValid = false;
            }
        }

        private void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbEmpYear2.Text == "" || tbEmpYear2.Text == null) {
                args.IsValid = false;
            }
        }

        private void CustomValidator3_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbEmpYear3.Text == "" || tbEmpYear3.Text == null) {
                args.IsValid = false;
            }
        }

        private void CustomValidator4_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbEmpYear4.Text == "" || tbEmpYear4.Text == null) {
                args.IsValid = false;
            }
        }

        private void CustomValidator5_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbEmpYear5.Text == "" || tbEmpYear5.Text == null) {
                args.IsValid = false;
            }
        }

        private void CustomValidator6_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbEmpYear6.Text == "" || tbEmpYear6.Text == null) {
                args.IsValid = false;
            }
        }

        private void CustomValidator7_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbEmpYear7.Text == "" || tbEmpYear7.Text == null) {
                args.IsValid = false;
            }
        }

        private void CustomValidator8_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbEmpYear8.Text == "" || tbEmpYear8.Text == null) {
                args.IsValid = false;
            }
        }

        private void CustomValidator9_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbEmpYear9.Text == "" || tbEmpYear9.Text == null) {
                args.IsValid = false;
            }
        }

        private void CustomValidator10_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbEmpYear10.Text == "" || tbEmpYear10.Text == null) {
                args.IsValid = false;
            }
        }

        private void BindStaffCountGrid() {
            dsStaffCount.Tables[0].DefaultView.Sort = "Year DESC";
            dtgrStaffCountOverview.DataSource = dsStaffCount.Tables[0].DefaultView;
            dtgrStaffCountOverview.DataBind();
            SetColumnsHeaderByCulture();
        }

        public void FillDataSets() {
            if (Session["dsStaffCount"] == null) {
                dsStaffCount = myFact.GetStaffCountAsDataSet(CIID);
                Session["dsStaffCount"] = dsStaffCount;
            } else {
                dsStaffCount = (DataSet) Session["dsStaffCount"];
            }
        }

        private void dtgrStaffCountOverview_UpdateCommand(object source, DataGridCommandEventArgs e) {
            TextBox countText = (TextBox) e.Item.Cells[3].Controls[0];

            // Retrieve the updated values.
            String year = e.Item.Cells[2].Text;
            String count = countText.Text;
            // for updating the database
            StaffCountBLLC theStaffCount = new StaffCountBLLC();
            ArrayList arrYearEmployees = new ArrayList();
            theStaffCount.Year = year;
            if (count != "") {
                theStaffCount.EmployeesCount = Convert.ToInt32(count);
            }

            arrYearEmployees.Add(theStaffCount);
            if (myFact.InsertEmployeeCountYears(arrYearEmployees, CIID)) {
                DataRow dr;

                // Create a DataView and specify the field to sort by.
                DataView StaffCountView = new DataView(dsStaffCount.Tables[0]);
                StaffCountView.Sort = "Year";

                // Remove the old entry and clear the row filter.
                StaffCountView.RowFilter = "Year='" + year + "'";

                if (StaffCountView.Count > 0) {
                    StaffCountView.Delete(0);
                }
                StaffCountView.RowFilter = "";

                // Add the new entry.
                dr = dsStaffCount.Tables[0].NewRow();
                dr[1] = year;
                dr[2] = count;

                dsStaffCount.Tables[0].Rows.Add(dr);
                // h�r �arfa a� sortera upp � n�tt vegna �ess a� adda�a datarowin kemur aftast.
                dsStaffCount.Tables[0].DefaultView.Sort = "Year DESC";

                dtgrStaffCountOverview.EditItemIndex = -1;

                BindStaffCountGrid();
            }
        }

        private void dtgrStaffCountOverview_EditCommand(object source, DataGridCommandEventArgs e) {
            dtgrStaffCountOverview.EditItemIndex = e.Item.ItemIndex;
            BindStaffCountGrid();
        }

        private void dtgrStaffCountOverview_CancelCommand(object source, DataGridCommandEventArgs e) {
            dtgrStaffCountOverview.EditItemIndex = -1;
            BindStaffCountGrid();
        }

        private void btnContinue_Click(object sender, EventArgs e) { Server.Transfer("ShareholdersOwners.aspx"); }

        private void SetPageSteps() {
            // get the totalPageSteps
            if (Cache["TotalPageSteps"] != null) {
                totalPageSteps = (string) Cache["TotalPageSteps"];
            } else {
                totalPageSteps = CigConfig.Configure("lookupsettings.TotalPageSteps");
                Cache["TotalPageSteps"] = totalPageSteps;
            }
            // get current page step
            currentPageStep = CigConfig.Configure(pageName);
        }

        private void LocalizeText() {
            lblStaffCount.Text = rm.GetString("txtAddStaffCountYears", ci);
            lblYear.Text = rm.GetString("txtYear", ci);
            lblEmployeesCount.Text = rm.GetString("txtEmployees", ci);
            lblEmployeesOverview.Text = rm.GetString("txtEmployeesOverview", ci);
            btnRegStaffCount.Text = rm.GetString("txtSave", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            //		this.btnCancel.Text = rm.GetString("txtCancel",ci);
            btnCancel.Text = rm.GetString("txtBack", ci);
        }

        private void SetColumnsHeaderByCulture() {
            dtgrStaffCountOverview.Columns[1].HeaderText = rm.GetString("txtYear", ci);
            dtgrStaffCountOverview.Columns[2].HeaderText = rm.GetString("txtStaffCount", ci);
        }

        private void ClearTextBoxes() {
            System.Web.UI.Control theForm = FindControl("EmployeesCountForm");
            foreach (System.Web.UI.Control ctrl in theForm.Controls) {
                if (ctrl.GetType() == typeof (TextBox)) {
                    TextBox txt = (TextBox) ctrl;
                    txt.Text = "";
                }
            }
        }

        private void dtgrStaffCountOverview_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                LinkButton btnTemp = (LinkButton) e.Item.Cells[0].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);

                /*for(int i=0;i<e.Item.Cells[2].Controls.Count;i++)
				{
					if(e.Item.Cells[2].Controls[i] is System.Web.UI.WebControls.LinkButton)
					{
						switch(((LinkButton)e.Item.Cells[2].Controls[i]).CommandName)
						{
							case "Edit":
								((LinkButton)e.Item.Cells[2].Controls[i]).Text = rm.GetString("txtEdit",ci);
								break;
							case "Cancel":
								((LinkButton)e.Item.Cells[2].Controls[i]).Text = rm.GetString("txtCancel",ci);
								break;
							case "Update":
								((LinkButton)e.Item.Cells[2].Controls[i]).Text = rm.GetString("txtUpdate",ci);
								break;
						}

					}
				}*/
            }

            WebDesign.CreateExplanationIcons(dtgrStaffCountOverview.Columns, lblDatagridIcons, rm, ci);
        }

        private void dtgrStaffCountOverview_ItemCommand(object source, DataGridCommandEventArgs e) {
            int rowToDelete = -1;
            //int boardMemberCIID = -1;

            try {
                //year = (string) dsStaffCount.Tables[0].Rows[e.Item.ItemIndex]["Year"];
                if (e.CommandName == "Delete") {
                    rowToDelete = e.Item.ItemIndex;
                    if (rowToDelete >= 0) {
                        string year = e.Item.Cells[1].Text.Trim();
                        //this.dsStaffCount.Tables[0].Rows[rowToDelete].Delete();
                        if (CIID > 0) {
                            myFact.DeleteStaffCountYear(year, CIID);
                        }
                        dsStaffCount = myFact.GetStaffCountAsDataSet(CIID);
                        Session["dsStaffCount"] = dsStaffCount;
                        BindStaffCountGrid();
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog("AddEmployeesCount.aspx : dtgrStaffCountOverview_ItemCommand " + err.Message, true);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            Server.Transfer("Auditors.aspx");
/*
			ClearTextBoxes();
*/
        }

        private void AddEnterEvent() {
            System.Web.UI.Control frm = FindControl("EmployeesCountForm");
            foreach (System.Web.UI.Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                    ;
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.CustomValidator1.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator1_ServerValidate);
            this.CustomValidator2.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator2_ServerValidate);
            this.CustomValidator3.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator3_ServerValidate);
            this.CustomValidator4.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator4_ServerValidate);
            this.tbEmpYear5.TextChanged += new System.EventHandler(this.TextBox5_TextChanged);
            this.CustomValidator5.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator5_ServerValidate);
            this.CustomValidator6.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator6_ServerValidate);
            this.CustomValidator7.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator7_ServerValidate);
            this.CustomValidator8.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator8_ServerValidate);
            this.CustomValidator9.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator9_ServerValidate);
            this.CustomValidator10.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CustomValidator10_ServerValidate);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnRegStaffCount.Click += new System.EventHandler(this.btnRegStaffCount_Click);
            this.dtgrStaffCountOverview.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrStaffCountOverview_ItemCommand);
            this.dtgrStaffCountOverview.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrStaffCountOverview_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}