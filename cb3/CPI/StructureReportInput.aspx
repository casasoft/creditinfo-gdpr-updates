<%@ Page language="c#" Codebehind="StructureReportInput.aspx.cs" AutoEventWireup="false" Inherits="CPI.StructureReportInput"%>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>StructureReportInput</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<script>
					
					function checkBasicInfoEnterKey()
					{
						if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								document.StructureReportInfoForm.btnBasicReg.click(); 
							}
					}
					function checkBoardMemberEnterKey()
					{
						if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								document.StructureReportInfoForm.btnRegBoardMember.click(); 
							}
					}
					function checkShareholdersOwnerEnterKey()
					{
						if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								document.StructureReportInfoForm.btnRegShareOwner.click(); 
							}
					}
					function checkSectretaryEnterKey()
					{
						if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								document.StructureReportInfoForm.btnRegSecretary.click(); 
							}
					}
					function checkCapitalEnterKey()
					{
						if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								document.StructureReportInfoForm.btnRegCapital.click(); 
							}
					}
					function checkChargesEnterKey()
					{
							if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								document.StructureReportInfoForm.btnRegCharges.click(); 
							}
					}
					function SetFormFocus()
					{
						document.StructureReportInfoForm.tbName.focus();
						//	ScrollIt();
					}
				/*	
					function GoNordurNidur(fieldname,radControl,w,h)
					{
						var selvalue = getRadioSelValue(radControl);
						PopupExtHandler(fieldname,w,h,selvalue);
					}
					function getRadioSelValue(radControl) 
					{ 
				//	document.write("Type is: " + radControl.type + " Name is: " + radControl.name + " Length is : " +radControl.length + " Value[0] is: " + radControl[0].value + " Checked[0] is: " + radControl[0].checked); 
				//	document.write("Name is: " + radControl.name); 
						for(var i = 0; i < radControl.length; i++) 
						{ 
							if(radControl[i].checked) 
							{ 
								return radControl[i].value; 
							} 
						}
						return false; 
					}
				*/
		</script>
	</HEAD>
	<body onload="SetFormFocus()">
		<form id="StructureReportInfoForm" name="StructureReportInfoForm" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<!-- Basic info starts -->
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td><asp:label id="lblPageTitle" runat="server">Information Entry</asp:label>-
																		<asp:label id="lblBasicInfo" runat="server">Basic Info</asp:label></td>
																	<td align="right"><asp:label id="lblPageStep" runat="server">Step 1 of 1</asp:label></td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td colspan="2"><asp:label id="lblLegalForm" runat="server">Legal form</asp:label><br>
																		<asp:dropdownlist id="ddLegalForm" runat="server" Width="100%">
																			<asp:ListItem Value="0">Company Type</asp:ListItem>
																		</asp:dropdownlist></td>
																</tr>
																<tr>
																	<td><asp:label id="lblName" runat="server">Name</asp:label><br>
																		<asp:textbox id="tbName" runat="server" maxlength="100"></asp:textbox><asp:customvalidator id="custValBasicName" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td><asp:label id="lblNameEN" runat="server">Name (EN)</asp:label><br>
																		<asp:textbox id="txtNameEN" runat="server" maxlength="100"></asp:textbox></td>
																	<td><asp:label id="lblUniqueID" runat="server">Unique ID</asp:label><br>
																		<asp:textbox id="tbUniqueID" runat="server" maxlength="100"></asp:textbox><asp:customvalidator id="custValBasicUniqueID" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td><asp:label id="lblTradeName" runat="server">Trade Name</asp:label><br>
																		<asp:textbox id="tbTradeName" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblTradeNameEN" runat="server">Trade Name (EN)</asp:label><br>
																		<asp:textbox id="txtTradeNameEN" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblVAT" runat="server">VAT</asp:label><br>
																		<asp:textbox id="tbVAT" runat="server" maxlength="50"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblFormerName" runat="server">Former Name</asp:label><br>
																		<asp:textbox id="tbFormerName" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblFormerNameEN" runat="server">Former Name (EN)</asp:label><br>
																		<asp:textbox id="txtFormerNameEN" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblSymbol" runat="server">Symbol</asp:label><br>
																		<asp:textbox id="txtSymbol" runat="server" maxlength="10"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblTel" runat="server">Tel</asp:label><br>
																		<asp:textbox id="tbTel" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblMobile" runat="server">Mobile</asp:label><br>
																		<asp:textbox id="tbMobile" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblFax" runat="server">Fax</asp:label><br>
																		<asp:textbox id="tbFax" runat="server" maxlength="50"></asp:textbox></td>
																</tr>
																<tr id="CYPLARROW" runat="server">
																	<td><asp:label id="lblLAR" runat="server">L.A.R.</asp:label><br>
																		<asp:textbox id="tbLAR" runat="server"></asp:textbox><asp:customvalidator id="custValidLARBasic" runat="server" errormessage="Error in date input" controltovalidate="tbLAR">*</asp:customvalidator></td>
																	<td><asp:label id="lblLR" runat="server">L.R.</asp:label><br>
																		<asp:textbox id="tbLR" runat="server"></asp:textbox><asp:customvalidator id="custValidLRBasic" runat="server" errormessage="Error in date input" controltovalidate="tbLR">*</asp:customvalidator></td>
																	<td><asp:label id="lbStructureResearchDate" runat="server">Structure report research</asp:label><br>
																		<asp:textbox id="tbStructureReportResearchDate" runat="server"></asp:textbox><asp:customvalidator id="custValidSRRBasic" runat="server" errormessage="Eror in Date input" controltovalidate="tbStructureReportResearchDate">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblAddressHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:label id="lblAddress" runat="server">Address</asp:label><br>
																		<asp:textbox id="tbAddress" runat="server" maxlength="100"></asp:textbox><asp:customvalidator id="custValidAddressBasic" runat="server" errormessage="Value missing!" controltovalidate="tbAddress">*</asp:customvalidator></td>
																	<td><asp:label id="lblAddressEN" runat="server">Address (EN)</asp:label><br>
																		<asp:textbox id="txtAddressEN" runat="server" maxlength="100"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblPostBox" runat="server">Postbox</asp:label><br>
																		<asp:textbox id="tbPostbox" runat="server" maxlength="20"></asp:textbox></td>
																	<td><asp:label id="lblAreaCode" runat="server">Area code</asp:label><br>
																		<asp:textbox id="tbAreaCode" runat="server" maxlength="30"></asp:textbox></td>
																	<td><asp:label id="lblCity" runat="server">City</asp:label><asp:label id="lblCitySearch" runat="server">City</asp:label><br>
																		<asp:dropdownlist id="ddCity" runat="server" width="100px"></asp:dropdownlist><asp:customvalidator id="custValidCityBasic" runat="server" errormessage="Value missing!">*</asp:customvalidator><asp:textbox id="txtCitySearch" runat="server" maxlength="10"></asp:textbox>&nbsp;<asp:button id="btnCitySearch" runat="server" CssClass="popup" causesvalidation="False" text="..."></asp:button></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblAdditionalInfoHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:label id="lblHomePage" runat="server">Homepage</asp:label><br>
																		<asp:textbox id="tbHomePage" runat="server" maxlength="100"></asp:textbox></td>
																	<td><asp:label id="lblEmail" runat="server">E-mail</asp:label><br>
																		<asp:textbox id="tbEmail" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblRegisted" runat="server">Registed</asp:label><br>
																		<asp:textbox id="tbRegisted" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbRegisted', 250, 250);" type="button" value="...">
																		<asp:customvalidator id="custValidRegistedDate" runat="server" errormessage="Date input in wrong format"
																			controltovalidate="tbRegisted">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td><asp:label id="lblEstablished" runat="server">Established</asp:label><br>
																		<asp:textbox id="tbEstablished" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbEstablished', 250, 250);" type="button" value="...">
																		<asp:customvalidator id="custValidEstablishedDateBasic" runat="server" errormessage="Date input in wrong format"
																			controltovalidate="tbEstablished">*</asp:customvalidator></td>
																	<td><asp:label id="lblLastContacted" runat="server">Last contacted</asp:label><br>
																		<asp:textbox id="tbLastContacted" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbLastContacted', 250, 250);" type="button"
																			value="...">
																		<asp:customvalidator id="custValidLastContactedDateBasic" runat="server" errormessage="Date input in wrong format"
																			controltovalidate="tbLastContacted">*</asp:customvalidator></td>
																	<td><asp:label id="lblLastUpdated" runat="server">Last updated</asp:label><br>
																		<asp:textbox id="tbLastUpdated" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbLastUpdated', 250, 250);" type="button" value="...">
																		<asp:customvalidator id="custDateLastUpdateBasic" runat="server" errormessage="Date input in wrong format"
																			controltovalidate="tbLastUpdated">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblHisOpReview" runat="server">History, operation and review</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:label id="lblHistoryN" runat="server">History (native)</asp:label><br>
																		<asp:textbox id="tbHistoryN" runat="server" width="100%" height="102px" textmode="MultiLine"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblHistoryE" runat="server">History (english)</asp:label><br>
																		<asp:textbox id="tbHistoryE" runat="server" width="100%" height="102px" textmode="MultiLine"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblRegMsg" runat="server" visible="False" cssclass="confirm_text">The info has been registed</asp:label><asp:validationsummary id="BasicValidationSummary" runat="server" enabled="False"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnBasicCancel" runat="server" causesvalidation="False" text="Cancel" cssclass="cancel_button"></asp:button><asp:button id="btnBasicReg" runat="server" text="Reg" cssclass="confirm_button"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<!-- Basic info ends -->
										<tr>
											<td height="10"></td>
										</tr>
										<!-- Shareholders/Owners starts -->
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblShareholdersOwners" runat="server">Shareholders/Owner</asp:label><br>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:radiobuttonlist id="rblShareOwnerPersComp" runat="server" CssClass="radio" repeatdirection="Horizontal">
																			<asp:ListItem Value="2" Selected="True">Person</asp:ListItem>
																			<asp:ListItem Value="1">Company</asp:ListItem>
																		</asp:radiobuttonlist></td>
																</tr>
																<tr>
																	<td><asp:label id="lblShareOwnerCIID" runat="server">CIID</asp:label><br>
																		<asp:textbox id="txtShareOwnerCIID" runat="server" maxlength="13"></asp:textbox></td>
																	<td><asp:label id="lblShareOwnerNationalID" runat="server">National ID</asp:label><br>
																		<asp:textbox id="tbShareOwnerNationalID" runat="server" maxlength="100"></asp:textbox><asp:customvalidator id="validShareOwnerNatID" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td><asp:textbox id="Textbox6" runat="server" width="8px" visible="False"></asp:textbox><asp:textbox id="txtAddressNative" runat="server" width="8px" visible="False"></asp:textbox><asp:textbox id="txtPostalCode" runat="server" width="8px" visible="False"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblShareOwnerFirstName" runat="server">Firstname/Company name</asp:label><br>
																		<asp:textbox id="tbShareOwnerFirstname" runat="server" maxlength="100"></asp:textbox></td>
																	<td><asp:label id="lblShareOwnerSurname" runat="server">Surname</asp:label><br>
																		<asp:textbox id="tbShareOwnerSurname" runat="server" maxlength="50"></asp:textbox></td>
																	<td align="right"><asp:button id="btnShareOwnerSearch" runat="server" causesvalidation="False" text="Search" cssclass="search_button"></asp:button>&nbsp;
																		<INPUT onclick="GoNordurNidur('txtShareOwnerCIID','tbShareOwnerNationalID','tbShareOwnerFirstname','tbShareOwnerSurname',document.StructureReportInfoForm.rblShareOwnerPersComp, 670, 900);"
																			type="button" value="..." class="popup"></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="Label1" runat="server">Shareholders/Owner</asp:label><br>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:label id="lblShareOwnerOwnership" runat="server">Ownership (shares)</asp:label><br>
																		<asp:textbox id="tbShareOwnerOwnership" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="custValShareholdersOwnerOwnership" runat="server" errormessage="Value missing!"
																			cssclass="label" enableclientscript="False">*</asp:customvalidator></td>
																	<td><asp:label id="lblShareOwnerOwnershipType" runat="server">Ownership type</asp:label>
																		<br>
																		<asp:dropdownlist id="ddlShareOwnerOwnershipType" runat="server" Width="100%"></asp:dropdownlist></td>
																</tr>
																<!-- innsetn -->
																<tr id="tdShareOwnerHistory" runat="server">
																	<td><asp:label id="lblShareOwnerHistoryNative" runat="server">History Native</asp:label><br>
																		<asp:textbox id="tbShareOwnerHistoryNative" runat="server" maxlength="500" width="400px" height="150px"
																			textmode="MultiLine"></asp:textbox></td>
																	<td><asp:label id="lblShareOwnerHistoryEN" runat="server">History EN</asp:label><br>
																		<asp:textbox id="tbShareOwnerHistoryEN" runat="server" maxlength="500" width="400px" height="150px"
																			textmode="MultiLine"></asp:textbox></td>
																</tr>
																<!-- innsetn endar -->
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableShareOwnerNameSearchGrid" cellSpacing="0" cellPadding="0"
													runat="server">
													<tr>
														<td align="right"><asp:label id="lblShareOwnerNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblShareOwnerNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellSpacing="0" cellPadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divShareOwnerNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dtgrShareOwnerNameSearch" runat="server" cssclass="grid" autogeneratecolumns="False"
																				gridlines="None">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="CreditInfoID" headertext="Creditinfo ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="Number" headertext="National ID">
																						<headerstyle width="10%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Name">
																						<headerstyle width="30%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Address">
																						<headerstyle width="25%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetNative" headertext="AddressNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetEN" headertext="AddressEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="PostalCode" headertext="Post code">
																						<headerstyle width="17%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblShareOwnerErrMsg" runat="server" visible="False" cssclass="error_text">Insert/Update failed</asp:label><asp:validationsummary id="ShareholdersValidationsummary" runat="server" enabled="False"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancelShareOwner" runat="server" causesvalidation="False" text="Cancel" cssclass="cancel_button"></asp:button><asp:button id="btnRegShareOwnerHistory" runat="server" text="Reg. history" cssclass="gray_button"></asp:button><asp:button id="btnRegShareOwner" runat="server" text="Reg." cssclass="confirm_button"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableShareholdersGrid" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right"><asp:label id="lblShareholdersDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblShareholdersDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:datagrid id="dtgrShareholders" runat="server" cssclass="grid" autogeneratecolumns="False"
																			gridlines="None">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;"
																					commandname="Update">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
																					commandname="Delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:boundcolumn datafield="CreditInfoID" headertext="CIID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Number" headertext="ID">
																					<headerstyle width="19%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Name">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameNative" headertext="FirstNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameNative" headertext="SurNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameEN" headertext="FirstNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameEN" headertext="SurNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Ownership" headertext="Ownership">
																					<headerstyle width="15%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Type" headertext="Type">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameNative" headertext="NameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameEN" headertext="NameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="OwnershipID" headertext="OwnershipID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Ownership type">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="OwnershipDescriptionNative" headertext="OwnershipDescriptionNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="OwnershipDescriptionEN" headertext="OwnershipDescriptionEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																		</asp:datagrid></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<!-- Shareholders/Owners endar -->
										<tr>
											<td height="10"></td>
										</tr>
										<!-- Board members starts -->
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblBoardMembers" runat="server" font-bold="True">Board members</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:label id="lblBoardMemberCIID" runat="server">CIID</asp:label><br>
																		<asp:textbox id="tbBoardMemberCIID" runat="server" maxlength="13"></asp:textbox></td>
																	<td><asp:label id="lblBoardMemberPersonalID" runat="server">Personal ID</asp:label><br>
																		<asp:textbox id="tbBoardMemberPersonalID" runat="server" maxlength="100"></asp:textbox><asp:customvalidator id="custValidPersIDBoardMembers" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblBoardMemberFirstName" runat="server">First name</asp:label><br>
																		<asp:textbox id="tbBoardMemberFirstName" runat="server" maxlength="50"></asp:textbox><asp:customvalidator id="custValidFirstNameBoardMember" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td><asp:label id="lblBoardMemberSurName" runat="server">Surname</asp:label><br>
																		<asp:textbox id="tbBoardMemberSurName" runat="server" maxlength="50"></asp:textbox><asp:customvalidator id="custValidSurNameBoardMember" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td align="right"><asp:button id="btnBoardMemberSearch" runat="server" causesvalidation="False" text="Search"
																			cssclass="search_button"></asp:button>&nbsp; <INPUT onclick="GoNordurNidur('tbBoardMemberCIID','tbBoardMemberPersonalID','tbBoardMemberFirstName','tbBoardMemberSurName','2', 670, 900);"
																			type="button" value="..." class="popup"></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblBoardMembersSubHeader" runat="server">Board members</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:label id="lblBoardMemberManagementPosition" runat="server" width="145px">Management position</asp:label><br>
																		<asp:dropdownlist id="ddBoardMemberManagementPosition" runat="server" Width="100%">
																			<asp:ListItem Value="No particular">No particular</asp:ListItem>
																		</asp:dropdownlist></td>
																	<td><asp:label id="lblBoardMemberAreaCode" runat="server" visible="False">Area code</asp:label><br>
																		<asp:dropdownlist id="ddlBoardMemberAreaCode" runat="server" visible="False" Width="100%">
																			<asp:ListItem Value="N/A">N/A</asp:ListItem>
																		</asp:dropdownlist></td>
																	<td><asp:label id="lblBoardMemberAddress" runat="server" visible="False">Address</asp:label><br>
																		<asp:textbox id="tbBoardMemberAddress" runat="server" visible="False"></asp:textbox></td>
																</tr>
																<tr id="Td1" runat="server">
																	<td><asp:label id="lblBoardMemberHistoryNative" runat="server">History Native</asp:label><br>
																		<asp:textbox id="tbBoardMemberHistoryNative" runat="server" maxlength="500" width="400px" height="150px"
																			textmode="MultiLine"></asp:textbox></td>
																	<td><asp:label id="lblBoardMemberHistoryEN" runat="server">History EN</asp:label><br>
																		<asp:textbox id="tbBoardMemberHistoryEN" runat="server" maxlength="500" width="400px" height="150px"
																			textmode="MultiLine"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableBoardMemberNameSearch" cellSpacing="0" cellPadding="0"
													runat="server">
													<tr>
														<td align="right"><asp:label id="lblBoardMemberNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblBoardMemberNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellSpacing="0" cellPadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divBoardMemberNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dtgrBoardMemberNameSearch" runat="server" cssclass="grid" autogeneratecolumns="False"
																				gridlines="None">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="CreditInfoID" headertext="Creditinfo ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="Number" headertext="National ID">
																						<headerstyle width="12%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Name">
																						<headerstyle width="30%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Address">
																						<headerstyle width="20px"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetNative" headertext="AddressNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetEN" headertext="AddressEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="PostalCode" headertext="Post code">
																						<headerstyle width="13px"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblBoardMemberMessage" runat="server" visible="False" cssclass="confirm_text">Message</asp:label><asp:validationsummary id="BoardMembersValidationsummary" runat="server" width="100%" height="22px" enabled="False"></asp:validationsummary></td>
														<td align="right">
															<asp:button id="btnCancelBoardMember" runat="server" cssclass="cancel_button" text="Cancel"
																causesvalidation="False"></asp:button><asp:button id="btnRegBoardMemberHistory" runat="server" causesvalidation="False" text="Reg. history"
																cssclass="gray_button" commandname="Insert"></asp:button><asp:button id="btnRegBoardMember" runat="server" text="Reg." cssclass="confirm_button"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableBoardMOverview" cellSpacing="0" cellPadding="0" runat="server">
													<tr>
														<td align="right"><asp:label id="lblBoardMOverviewDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblBoardMOverviewDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:datagrid id="dtgrBoardMOverview" runat="server" cssclass="grid" autogeneratecolumns="False"
																			gridlines="None">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;"
																					commandname="Update">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
																					commandname="Delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:boundcolumn datafield="CreditInfoID" headertext="CreditInfoID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Number" headertext="ID">
																					<headerstyle width="12%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Name">
																					<headerstyle width="31%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Title">
																					<headerstyle width="25%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameNative" headertext="FirstNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameNative" headertext="SurNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameEN" headertext="FirstNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameEN" headertext="SurNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="TitleNative" headertext="TitleNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="TitleEN" headertext="TitleEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																		</asp:datagrid></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<!-- Board members ends -->
										<tr>
											<td height="10"></td>
										</tr>
										<!-- Secratery starts -->
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblSecretary" runat="server">Secretary</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:radiobuttonlist id="rblSecretaryPersonCompany" runat="server" CssClass="radio" repeatdirection="Horizontal"
																			autopostback="True">
																			<asp:ListItem Value="2" Selected="True">Person</asp:ListItem>
																			<asp:ListItem Value="1">Company</asp:ListItem>
																		</asp:radiobuttonlist></td>
																</tr>
																<tr>
																	<td><asp:label id="lblSecretaryCIID" runat="server">Creditinfo ID</asp:label><br>
																		<asp:textbox id="tbSecretaryCIID" runat="server" maxlength="13" enableviewstate="False"></asp:textbox></td>
																	<td><asp:label id="lblSecretaryNationalID" runat="server">National ID</asp:label><br>
																		<asp:textbox id="tbSecretaryNationalID" runat="server" maxlength="100" enableviewstate="False"></asp:textbox><asp:customvalidator id="custValidNationalIDSecretary" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblSecretaryFirstName" runat="server">Firstname</asp:label><br>
																		<asp:textbox id="tbSecretaryFirstName" runat="server" maxlength="50" enableviewstate="False"></asp:textbox><asp:customvalidator id="custValidFirstNameSecretary" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td><asp:label id="lblSecretarySurName" runat="server">Surname</asp:label><br>
																		<asp:textbox id="tbSecretarySurName" runat="server" maxlength="50" enableviewstate="False"></asp:textbox></td>
																	<td align="right"><asp:button id="btnSecretarySearch" runat="server" causesvalidation="False" text="Search" cssclass="search_button"></asp:button>&nbsp;
																		<INPUT onclick="GoNordurNidur('tbSecretaryCIID','tbSecretaryNationalID','tbSecretaryFirstName','tbSecretarySurName',document.StructureReportInfoForm.rblSecretaryPersonCompany, 670, 900);"
																			type="button" value="..." class="popup"></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblSecretarySubHeader" runat="server">Secretary</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:label id="lblSecretaryHistoryNative" runat="server">History Native</asp:label><br>
																		<asp:textbox id="tbSecretaryHistoryNative" runat="server" width="400px" height="100px" textmode="MultiLine"
																			enableviewstate="False"></asp:textbox></td>
																	<td><asp:label id="lblSecretaryHistoryEnglish" runat="server">History English</asp:label><br>
																		<asp:textbox id="tbSecretaryHistoryEnglish" runat="server" width="400px" height="100px" textmode="MultiLine"
																			enableviewstate="False"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableSecretaryNameSearch" cellSpacing="0" cellPadding="0"
													runat="server">
													<tr>
														<td align="right"><asp:label id="lblSecretaryNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblSecretaryNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellSpacing="0" cellPadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divSecretaryNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dtgrSecretaryNameSearch" runat="server" cssclass="grid" autogeneratecolumns="False"
																				gridlines="None">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:ButtonColumn Text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;&gt;"
																						CommandName="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:ButtonColumn>
																					<asp:BoundColumn DataField="CreditInfoID" HeaderText="Creditinfo ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="Number" HeaderText="National ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Name">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="NameNative" HeaderText="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="NameEN" HeaderText="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Address">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="StreetNative" HeaderText="AddressNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="StreetEN" HeaderText="AddressEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="PostalCode" HeaderText="Post code">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="IsCompany" HeaderText="IsCompany">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:BoundColumn>
																				</columns>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblSecretaryMsg" runat="server" visible="False" cssclass="error_text">Insert/Update failed</asp:label><asp:validationsummary id="SecretaryValidationsummary" runat="server" enabled="False"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancelSecretary" runat="server" causesvalidation="False" text="Cancel" cssclass="cancel_button"></asp:button><asp:button id="btnRegSecretary" runat="server" text="Reg." cssclass="confirm_button"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableRegisteredCompanySecretary" cellSpacing="0" cellPadding="0"
													runat="server">
													<tr>
														<td align="right"><asp:label id="lblRegisteredCompanySecretaryDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblRegisteredCompanySecretaryDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:datagrid id="dgRegisteredCompanySecretary" runat="server" cssclass="grid" autogeneratecolumns="False"
																			gridlines="None">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:ButtonColumn Text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;"
																					CommandName="update">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:ButtonColumn>
																				<asp:ButtonColumn Text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
																					CommandName="delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:ButtonColumn>
																				<asp:BoundColumn DataField="CreditInfoID" HeaderText="CreditInfoID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="Number" HeaderText="National ID">
																					<headerstyle width="15px"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn HeaderText="Name">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="FirstNameNative" HeaderText="Firstname">
																					<headerstyle width="29%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="SurNameNative" HeaderText="Surname">
																					<headerstyle width="30%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="FirstNameEN" HeaderText="Firstname (EN)">
																					<headerstyle width="29%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="SurNameEN" HeaderText="Lastname (EN)">
																					<headerstyle width="30%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="HistoryNative" HeaderText="History">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="HistoryEN" HeaderText="History (EN)">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="NameEN" HeaderText="NameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="NameNative" HeaderText="NameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="Type" HeaderText="Type">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																			</columns>
																		</asp:datagrid></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<!-- Secratery ends -->
										<tr>
											<td height="10"></td>
										</tr>
										<!-- Capital starts -->
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblCapital" runat="server">Capital</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:label id="lblAuthorizedCapital" runat="server">Authorized capital</asp:label><br>
																		<asp:textbox id="txtAuthorizedCapital" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="custValidAuCapital" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td><asp:label id="lblIssuedNoOfShares" runat="server">Issued number of shares</asp:label><br>
																		<asp:textbox id="txtIssuedNoOfShares" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="custValIssuedShares" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td><asp:label id="lblNominalNumberOfShares" runat="server">Nominal number of shares</asp:label><br>
																		<asp:textbox id="txtNominalNumberOfShares" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="custValidNomNumberShares" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td><asp:label id="lblAsked" runat="server">Asked</asp:label><br>
																		<asp:textbox id="txtAsked" runat="server" maxlength="13"></asp:textbox>
																		<asp:customvalidator id="custValidAsked" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td><asp:label id="lblPaidUp" runat="server">Paid up</asp:label><br>
																		<asp:textbox id="txtPaidUp" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="custValPaidUp" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td><asp:label id="lblSharesDescription" runat="server">Shares description</asp:label><br>
																		<asp:textbox id="txtSharesDescription" runat="server" maxlength="50"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblCurrency" runat="server">Currency</asp:label><br>
																		<asp:dropdownlist id="ddlstCurrency" runat="server" Width="100%"></asp:dropdownlist></td>
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblErrorMessage" runat="server" visible="False" cssclass="error_text">Error</asp:label><asp:validationsummary id="CapitalValidationSummary" runat="server" enabled="False"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancelCapital" runat="server" text="Cancel" cssclass="cancel_button"></asp:button><asp:button id="btnRegCapital" runat="server" text="Register" cssclass="confirm_button"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<!-- Capital endar -->
										<tr>
											<td height="10"></td>
										</tr>
										<!-- Charges byrjar -->
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblCharges" runat="server">Charges</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" cellSpacing="0" cellPadding="0">
																<tr>
																	<td style="HEIGHT: 62px"><asp:label id="lblChargesDateRegistered" runat="server">Date registered</asp:label><br>
																		<asp:textbox id="tbChargesDateRegistered" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbChargesDateRegistered', 250, 250);" type="button"
																			value="...">
																		<asp:customvalidator id="custValDatRegCharges" runat="server" errormessage="Value missing!" controltovalidate="tbChargesDateRegistered">*</asp:customvalidator></td>
																	<td style="HEIGHT: 62px"><asp:label id="lblChargesDatePrepared" runat="server">Date prepared</asp:label><br>
																		<asp:textbox id="tbChargesDatePrepared" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbChargesDatePrepared', 250, 250);" type="button"
																			value="...">
																		<asp:customvalidator id="custValDatePrepCharges" runat="server" errormessage="Value missing!" controltovalidate="tbChargesDatePrepared">*</asp:customvalidator><asp:textbox id="txtID" runat="server" width="8px" visible="False"></asp:textbox></td>
																	<TD style="HEIGHT: 62px"></TD>
																</tr>
																<tr>
																	<td><asp:label id="lblChargesDescription" runat="server" width="80px" height="18px">Description/No</asp:label><br>
																		<asp:dropdownlist id="ddlChargesDescription" runat="server" Width="100%"></asp:dropdownlist></td>
																	<td><asp:label id="lblChargesSequence" runat="server">Sequence</asp:label><br>
																		<asp:textbox id="tbChargesSequence" runat="server" maxlength="50"></asp:textbox></td>
																	<TD></TD>
																</tr>
																<tr>
																	<td style="HEIGHT: 60px"><asp:label id="lblChargesAmount" runat="server">Amount</asp:label><br>
																		<asp:textbox id="tbChargesAmount" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="custValAmountCharges" runat="server" errormessage="Value missing!">*</asp:customvalidator></td>
																	<td style="HEIGHT: 60px"><asp:label id="lblChargesCurrency" runat="server">Currency</asp:label><br>
																		<asp:dropdownlist id="ddlChargesCurrency" runat="server"></asp:dropdownlist></td>
																	<!--	<td></td> -->
																</tr>
																<tr>
																	<TD style="HEIGHT: 60px"><asp:label id="lblChargesBenificiary" runat="server">Benificiary</asp:label>
																		<asp:dropdownlist id="ddlChargesBeneficiary" runat="server" Width="100%"></asp:dropdownlist></TD>
																	<td>
																	</td>
																	<!--	<td>
																	</td> -->
																</tr>
																<tr>
																	<td><asp:label id="lblChargesDescriptionNative" runat="server">Description native</asp:label><br>
																		<asp:textbox id="tbChargesDescriptionNative" runat="server" maxlength="500" width="400px" height="100px"
																			textmode="MultiLine"></asp:textbox><asp:customvalidator id="Customvalidator8" runat="server" errormessage="Error" controltovalidate="tbChargesAmount">*</asp:customvalidator></td>
																	<td><asp:label id="lblChargesDescriptionEN" runat="server">Description en</asp:label><br>
																		<asp:textbox id="tbChargesDescriptionEN" runat="server" maxlength="500" width="400px" height="100px"
																			textmode="MultiLine"></asp:textbox></td>
																	<!--	<TD></TD> -->
																</tr>
																<tr>
																	<td height="23"></td>
																	<TD height="23"></TD>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblChargesMessage" runat="server" visible="False" cssclass="error_text">Error</asp:label><asp:validationsummary id="ChargesValidationsummary" runat="server" enabled="False"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancelCharges" runat="server" text="Cancel" cssclass="cancel_button"></asp:button><asp:button id="btnRegCharges" runat="server" text="Register" cssclass="confirm_button"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="tableCharges" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right">
															<asp:label id="lblChargesDatagridIcons" runat="server"></asp:label>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblChargesDatagridHeader" runat="server"></asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dgCharges" runat="server" gridlines="None" cssclass="grid" autogeneratecolumns="False">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:ButtonColumn Text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;&gt;"
																					CommandName="update">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:ButtonColumn>
																				<asp:ButtonColumn Text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;&gt;"
																					CommandName="delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:ButtonColumn>
																				<asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DateRegistered" HeaderText="DateRegistered" DataFormatString="{0:d}">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DatePrepared" HeaderText="DatePrepared" DataFormatString="{0:d}">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="DescriptionID" HeaderText="DescriptionID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DescriptionNative" HeaderText="DescriptionNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="DescriptionEN" HeaderText="DescriptionEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="BankID" HeaderText="BeneficiaryID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="NameNative" HeaderText="BeneficiaryNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="NameEN" HeaderText="BeneficiaryEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="Amount" HeaderText="Amount">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="CurrencyCode" HeaderText="Currency">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="Sequence" HeaderText="Sequence">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="DescriptionFreeTextNative" HeaderText="DescriptionFreeTextNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="DescriptionFreeTextEN" HeaderText="DescriptionFreeTextEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:BoundColumn>
																			</columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<!-- Charges endar --></table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
