#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CPI.BLL;
using DataProtection;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using UserAdmin.DAL.CIUsers;
using cb3;

#endregion

using Cig.Framework.Base.Configuration;
using cb3.Audit;

namespace CPI.DAL {
    /// <remarks>
    /// This class provides the database funtions for CompanyProfile
    /// </remarks>
    public class CompanyProfileInputDALC : BaseDALC {
        /// <summary>
        /// For logging purpose
        /// </summary>
        private static string className = "CompanyProfileInputDALC";

        private readonly CreditInfoUserDALC userDALC = new CreditInfoUserDALC();

        /// <summary>
        /// Returns dataset of file formats available to export report in
        /// </summary>
        /// <returns>Dataset of file formats available to export report in</returns>
        public DataSet GetFileFormats() {
            string funcName = "GetFileFormats()";
            string theConnectionString = DatabaseHelper.ConnectionString();
            OleDbCommand theDBCommand;

            string sqlQuery = "Select ID, NameNative, NameEN from cr_FileFormat where IsOpen = 'True'";

            using (OleDbConnection theConnection = new OleDbConnection(theConnectionString)) {
                try {
                    theConnection.Open();
                    theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    OleDbDataAdapter theAdapter = new OleDbDataAdapter();
                    theAdapter.SelectCommand = theDBCommand;

                    //Return the results in a DataSet
                    DataSet theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                } finally {
                    // Ef �a� �arf a� ganga fr� einhverju �� � a� gera �a� h�r. � ekki a� �urfa me� "using" statements.
                }
            }
        }

        /// <summary>
        /// Takes an instance of ReportCompany and updates in the database
        /// </summary>
        /// <param name="theReport"></param>
        /// <returns></returns>
        public bool UpdateReportCompany(ReportCompanyBLLC theReport) {
            string funcName = "UpdateReportCompany(ReportCompanyBLLC theReport)";

            int resultID = 0;
            OleDbCommand myOleDbCommand = new OleDbCommand();

            CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
            Company theComp = new Company();
            theComp.CreditInfoID = theReport.CompanyCIID;
            theComp.NameNative = theReport.NameNative;
            theComp.NameEN = theReport.NameEN;
            theComp.Org_status_code = 1; // meaning not accessible
            theComp.IDNumbers = new ArrayList();

            theComp.Address = theReport.Address;

            theComp.Number = theReport.PNumbers;
            theComp.Type = CigConfig.Configure("lookupsettings.companyID");
            theComp.Established = theReport.Established;
            theComp.Registered = theReport.Registered;
            theComp.LastContacted = theReport.LastContacted;
            theComp.LastUpdate = theReport.LastUpdated;
            theComp.URL = theReport.HomePage;
            theComp.Email = theReport.Email;

            resultID = userDALC.UpdateCompanyFromBasicInfo(theComp);
            if (resultID < 0) {
                Logger.WriteToLog(
                    className + " : " + funcName + " : " + "Error i UpdateCompanyFromBasicInfo(theComp)", true);
                return false;
            }

            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();
                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "UPDATE cpi_CompanyReport SET CompanyFormerNameNative = ?,CompanyFormerNameEN = ?, CompanyTradeNameNative = ?, CompanyTradeNameEN = ?,InternalComment = ?,InternalCommentEN = ?,RegistrationFormID = ?, ReportsAuthorCIID = ?, Vat = ?, Symbol =?, LAR = ?, LR = ?,StructureReportDate = ? " +
                            "WHERE CompanyCIID = ?",
                            myOleDbConn);
                    myOleDbCommand.Transaction = myTrans;

                    OleDbParameter myParam;

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "CompanyFormerNameNative",
                        "CompanyFormerNameEN",
                        ParameterDirection.Input,
                        theReport.FormerNameNative,
                        theReport.FormerNameEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "CompanyTradeNameNative",
                        "CompanyTradeNameEN",
                        ParameterDirection.Input,
                        theReport.TradeNameNative,
                        theReport.TradeNameEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "InternalComment",
                        "InternalCommentEN",
                        ParameterDirection.Input,
                        theReport.InternalComments,
                        theReport.InternalCommentsEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("RegistrationFormID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.RegistrationFormID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("ReportsAuthorCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.RegisterCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Vat", OleDbType.VarChar);
                    myParam.Value = theReport.VAT;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Symbol", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.Symbol;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("LAR", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (theReport.LAR != DateTime.MinValue) {
                        myParam.Value = theReport.LAR;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("LR", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (theReport.LR != DateTime.MinValue) {
                        myParam.Value = theReport.LR;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("StructureReportDate", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (theReport.StructureReportResearchDate != DateTime.MinValue) {
                        myParam.Value = theReport.StructureReportResearchDate;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.CompanyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    UpdateContactPerson(theReport.ContactPerson, myOleDbCommand, theReport.CompanyCIID);

                    myTrans.Commit();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    myTrans.Rollback();
                    return false;
                }
            }
            return true;

/*Svenni t�k �t
		
			
			using(OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
			{
				myOleDbCommand = new OleDbCommand("UPDATE cpi_CompanyReport SET CompanyFormerNameNative = ?,CompanyFormerNameEN = ?, CompanyTradeNameNative = ?, CompanyTradeNameEN = ?,InternalComment = ?,RegistrationFormID = ?, ReportsAuthorCIID = ?, Vat = ? " +
							"WHERE CompanyCIID = ?" ,myOleDbConn);
				
			
				myOleDbCommand.Connection = myOleDbConn;
				myOleDbConn.Open();
				try
				{
					OleDbParameter myParam = new OleDbParameter("CompanyFormerNameNative", OleDbType.WChar);
					myParam.IsNullable = true;
					myParam.Direction = ParameterDirection.Input;
					if(theReport.FormerNameNative != null) // if no native info
						myParam.Value = theReport.FormerNameNative;
					else // use english instead
						myParam.Value = theReport.FormerNameEN;
					myOleDbCommand.Parameters.Add(myParam);
					myParam = new OleDbParameter("CompanyFormerNameEN", OleDbType.VarChar);
					myParam.IsNullable = true;
					myParam.Direction = ParameterDirection.Input;
					if(theReport.FormerNameEN != null) // if no english info 
						myParam.Value = theReport.FormerNameEN;
					else // use native instead
						myParam.Value = theReport.FormerNameNative;
					myOleDbCommand.Parameters.Add(myParam);
					myParam = new OleDbParameter("CompanyTradeNameNative", OleDbType.WChar);
					myParam.IsNullable = true;
					myParam.Direction = ParameterDirection.Input;
					if(theReport.TradeNameNative != null) // if no native info
						myParam.Value = theReport.TradeNameNative;
					else // use english instead
						myParam.Value = theReport.TradeNameEN;
					myOleDbCommand.Parameters.Add(myParam);
					myParam = new OleDbParameter("CompanyTradeNameEN", OleDbType.VarChar);
					myParam.Direction = ParameterDirection.Input;
					if(theReport.TradeNameEN != null) // if no english info
						myParam.Value = theReport.TradeNameEN;
					else // use native instead
						myParam.Value = theReport.TradeNameNative;
					myOleDbCommand.Parameters.Add(myParam);
					myParam = new OleDbParameter("InternalComment", OleDbType.WChar);
					myParam.Direction = ParameterDirection.Input;
					myParam.Value = theReport.InternalComments;
					myOleDbCommand.Parameters.Add(myParam);
					myParam = new OleDbParameter("RegistrationFormID", OleDbType.Integer);
					myParam.Direction = ParameterDirection.Input;
					myParam.Value = theReport.RegistrationFormID;
					myOleDbCommand.Parameters.Add(myParam);
					myParam = new OleDbParameter("ReportsAuthorCIID", OleDbType.Integer);
					myParam.Direction = ParameterDirection.Input;
					myParam.Value = theReport.RegisterCIID;
					myOleDbCommand.Parameters.Add(myParam);
					myParam = new OleDbParameter("Vat",OleDbType.VarChar);
					myParam.Value = theReport.VAT;
					myOleDbCommand.Parameters.Add(myParam);
					myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
					myParam.Direction = ParameterDirection.Input;
					myParam.Value = theReport.CompanyCIID;
					myOleDbCommand.Parameters.Add(myParam);
					
					myOleDbCommand.ExecuteNonQuery();
				} 
				catch(Exception err)
				{
					Logger.WriteToLog(className +" : " + funcName + err.Message,true);
					return false;
				}
			}
		
			return true;
*/
        }

        public bool UpdateReportCompanyFromControlPage(ReportCompanyBLLC theReport) {
            string funcName = "UpdateReportCompanyFromControlPage(ReportCompanyBLLC theReport)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand =
                    new OleDbCommand(
                        "UPDATE cpi_CompanyReport SET ExpireDate = ?,ExternalCommentNative = ?, ExternalCommentEN = ?, StatusID = ?" +
                        " WHERE CompanyCIID = ?",
                        myOleDbConn);

                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter("ExpireDate", OleDbType.Date);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.ExpireDate;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "ExternalCommentNative",
                        "ExternalCommentEN",
                        ParameterDirection.Input,
                        theReport.ExternalCommentNative,
                        theReport.ExternalCommentEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("StatusID", OleDbType.Integer);
                    myParam.IsNullable = true;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.StatusID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.CompanyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Takes an instance of ReportCompany and adds to database
        /// </summary>
        /// <param name="theReport">Instance of ReportCompany</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddReportCompany(ReportCompanyBLLC theReport) {
            // athuga hvort company report s� �egar til...
            // ef til �� uppf�ra annars hreint insert
            if (CompanyReportAlreadyRegisted(theReport.CompanyCIID)) {
                // do some update stuff
                return UpdateReportCompany(theReport);
            } else {
                // do some insert stuff
                return InsertReportCompany(theReport);
            }
        }

        /// <summary>
        /// Takes reports id and deletes the matching report from database
        /// </summary>
        /// <param name="reportsID">The reports ID</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool DeleteReportCompany(int reportsID) { return false; }

        public ArrayList FindCompanyReport(int creditInfoID, string name, string nationalID) {
            string funcName = "FindCompanyReport(int creditInfoID, string name, string nationalID) ";

            // Fylla reportbaunina �.e.
            // s�kja reportcompany
            // s�kja HistoryOperationReview
            // s�kja companyPrincipals og �eirra jobtitles
            // s�kja BoardMembers og �eirra ManagementPostions
            // s�kja RealEstates og RealEstatesTypes og RealEstatesOwnerType
            // s�kja ReportStatus
            // s�kja StaffCount
            // s�kja CompaniesNaceCode
            // s�kja Export/Import Countries
            // s�kja Companies Owners/Ownership
            // s�kja CompaniesSubsidaries
            // s�kja CompaniesBanks
            // s�kja CompaniesTradeTerms (??)
            // s�kja MarketingPostitions
            // ....gera �etta � viewi e�a hva�??
            // ... e�a hreinlega hafa �etta einfalt og s�kja hvert fyrir sig �egar kemur a� �v�
            // a� birta sk�rsluna
            
            ArrayList myList = new ArrayList();
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    string query = "SELECT TOP " + CigConfig.Configure("lookupsettings.cpiMaxHits").Trim() +
                                   " np_Companys.NameNative, np_Companys.NameEN, np_IDNumbers.Number, np_CreditInfoUser.CreditInfoID FROM " +
                                   "np_CreditInfoUser LEFT OUTER JOIN cpi_CompanyReport ON np_CreditInfoUser.CreditInfoID = cpi_CompanyReport.CompanyCIID " +
                                   "LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers " +
                                   "ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID ";

                    string where = "WHERE ";
                    bool whereStatement = false;

                    if (creditInfoID > -1) {
                        where += "np_CreditInfoUser.CreditInfoID = " + creditInfoID;
                        whereStatement = true;
                    }

                    if (nationalID.Length > 0) {
                        if (whereStatement) {
                            where += " AND ";
                        }
                        //where += "np_IDNumbers.NumberTypeID = " + CigConfig.Configure("lookupsettings.nationalID"].Trim() + " AND ";
                        where += "np_IDNumbers.Number = '" + nationalID + "'";
                        whereStatement = true;
                    }

                    //string where2 = null;
                    //string query2=null;

                    if (name.Length > 0) {
                        /*if(whereStatement)
								where += " AND ";
							where += "(np_Companys.NameNative LIKE N'%" + name + "%' OR np_Companys.NameEN LIKE N'%" + name + "%')";
							whereStatement = true;*/

                        if (whereStatement) {
                            where += "AND ";
                        }

                        //where2 = where;
                        //query2 = query;

                        where +=
                            "exists (select * from np_word2ciid where np_Companys.creditinfoid=np_word2ciid.creditinfoid and np_word2ciid.type='n' ";
                        where += "and np_word2ciid.word like (N'" + name + "%')) ";

                        //query += "(np_Companys.NameNative LIKE(N'%"+myComp.NameNative+"%') OR np_Companys.NameEN LIKE('%"+myComp.NameNative+"%')) ";
                        //where += "contains(np_Companys.NameNative,'\""+name+"*\"') ";
                        //where2 += "contains(np_Companys.NameEN,'\""+name+"*\"') ";
                        whereStatement = true;
                    }

                    if (whereStatement) {
                        query += where;
                        //	if(query2!=null)
                        //		query2+=where2;
                    }

                    //	if(query2!=null&&query2.Trim()!="")
                    //		query+=" UNION "+query2;

                    OleDbCommand myOleDbCommand = new OleDbCommand(query, myOleDbConn);

                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();

                    ReportCompanyBLLC theReportCompany;

                    while (reader.Read()) {
                        theReportCompany = new ReportCompanyBLLC();

                        if (!reader.IsDBNull(0)) {
                            theReportCompany.NameNative = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theReportCompany.NameEN = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theReportCompany.UniqueID = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            theReportCompany.CompanyCIID = reader.GetInt32(3);
                        }

                        myList.Add(theReportCompany);
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
            }
            return myList;
        }

        /// <summary>
        /// Takes Companies national ID and returns instance of ReportCompany 
        /// </summary>
        /// <param name="companyCIID">The companies national ID</param>
        /// <param name="CIID">The companies internal system id</param>
        /// <param name="nationalID">indicates wether to use nationalID or CIID</param>
        /// <returns>Instance of ReportCompanyBLLC</returns>
        public ReportCompanyBLLC GetCompanyReport(string nationalID, int CIID, bool useNationalID) { return GetCompanyReport(nationalID, CIID, useNationalID, false); }

        /// <summary>
        /// Takes Companies national ID and returns instance of ReportCompany 
        /// </summary>
        /// <param name="companyCIID">The companies national ID</param>
        /// <param name="CIID">The companies internal system id</param>
        /// <param name="nationalID">indicates wether to use nationalID or CIID</param>
        /// <param name="isSearchable">True will remove all nonSearchable companies, False will discard IsSearchable field.</param>
        /// <returns>Instance of ReportCompanyBLLC</returns>
        public ReportCompanyBLLC GetCompanyReport(string nationalID, int CIID, bool useNationalID, bool isSearchable) {
            int creditInfoID = -1;
            ReportCompanyBLLC theReportCompany = new ReportCompanyBLLC();
            string funcName = "GetCompanyReport(string nationalID) ";
            if (useNationalID) {
                creditInfoID = GetCIIDByNationalID(nationalID);
            } else {
                creditInfoID = CIID;
            }

            if (creditInfoID > 0) {
                // Fylla reportbaunina �.e.
                // s�kja reportcompany
                // s�kja HistoryOperationReview
                // s�kja companyPrincipals og �eirra jobtitles
                // s�kja BoardMembers og �eirra ManagementPostions
                // s�kja RealEstates og RealEstatesTypes og RealEstatesOwnerType
                // s�kja ReportStatus
                // s�kja StaffCount
                // s�kja CompaniesNaceCode
                // s�kja Export/Import Countries
                // s�kja Companies Owners/Ownership
                // s�kja CompaniesSubsidaries
                // s�kja CompaniesBanks
                // s�kja CompaniesTradeTerms (??)
                // s�kja MarketingPostitions
                // ....gera �etta � viewi e�a hva�??
                // ... e�a hreinlega hafa �etta einfalt og s�kja hvert fyrir sig �egar kemur a� �v�
                // a� birta sk�rsluna
                
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    try {
                        myOleDbConn.Open();
                        //	OleDbCommand myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                        // SELECT np_CreditInfoUser.Email, np_Companys.NameNative,np_Companys.NameEN,np_Companys.Established,np_Companys.LastContacted,np_Companys.LastUpdate,np_IDNumbers.Number,CompanyFormerNameNative,CompanyFormerNameEN,CompanyTradeNameNative,CompanyTradeNameEN,RegistrationInfoNative,RegistrationInfoEN,CommentI,CommentII,Vat,CustomerCount,ReportsAuthorCIID,CustomerComments,ImportComments,ExportComments,PaymentComments,SalesComments,cpi_CompanyReport.StatusID FROM np_CreditInfoUser LEFT OUTER JOIN cpi_CompanyReport ON np_CreditInfoUser.CreditInfoID = cpi_CompanyReport.CompanyCIID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE CompanyCIID = 22 AND np_IDNumbers.NumberTypeID = 1
                        string strSql =
                            "SELECT np_CreditInfoUser.Email, np_Companys.NameNative,np_Companys.NameEN,np_Companys.Established,np_Companys.LastContacted,np_Companys.LastUpdate,np_Companys.URL,np_IDNumbers.Number,CompanyFormerNameNative,CompanyFormerNameEN,CompanyTradeNameNative,CompanyTradeNameEN,RegistrationInfoNative," +
                            "RegistrationInfoEN,InternalComment,ExternalCommentNative,np_VatNumbers.Number as Vat,CustomerCount,ReportsAuthorCIID,CustomerComments,ImportComments,ExportComments,PaymentComments,SalesComments,cpi_CompanyReport.StatusID, np_Address.StreetNative, np_Address.StreetEN,np_Address.PostalCode,np_Address.PostBox, np_City.NameEN,np_City.NameNative,np_City.CityID, cpi_CompanyReport.RegistrationFormID, np_CreditInfoUser.CreditInfoID,ExpireDate,ReportID,InternalCommentEN,Symbol,ExternalCommentEN, cpi_companyReport.LAR,cpi_companyReport.LR,cpi_companyReport.StructureReportDate, np_Companys.Registered, np_VatNumbers.Note   " +
                            "FROM np_CreditInfoUser LEFT OUTER JOIN cpi_CompanyReport ON np_CreditInfoUser.CreditInfoID = cpi_CompanyReport.CompanyCIID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID " +
                            "LEFT OUTER JOIN np_Address ON np_Companys.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID " +
                            "LEFT OUTER JOIN cpi_RegistrationForm ON cpi_CompanyReport.RegistrationFormID = cpi_RegistrationForm.FormID LEFT OUTER JOIN dbo.np_VatNumbers ON np_Companys.CreditInfoID = dbo.np_VatNumbers.CreditInfoID " +
                            "WHERE np_CreditInfoUser.CreditInfoID = " + creditInfoID;
                        //np_IDNumbers.NumberTypeID = 1
                        if (isSearchable) {
                            strSql += "AND np_CreditInfoUser.IsSearchable = 'True' ";
                        }
                        OleDbCommand myOleDbCommand = new OleDbCommand(strSql, myOleDbConn);
                        OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                        if (reader.Read()) {
                            if (!reader.IsDBNull(0)) {
                                theReportCompany.Email = reader.GetString(0);
                            }
                            if (!reader.IsDBNull(1)) {
                                theReportCompany.NameNative = reader.GetString(1);
                            }
                            if (!reader.IsDBNull(2)) {
                                theReportCompany.NameEN = reader.GetString(2);
                            }
                            if (!reader.IsDBNull(3)) {
                                theReportCompany.Established = reader.GetDateTime(3);
                            }
                            if (!reader.IsDBNull(4)) {
                                theReportCompany.LastContacted = reader.GetDateTime(4);
                            }
                            if (!reader.IsDBNull(5)) {
                                theReportCompany.LastUpdated = reader.GetDateTime(5);
                            }
                            if (!reader.IsDBNull(6)) {
                                theReportCompany.HomePage = reader.GetString(6);
                            }
                            if (!reader.IsDBNull(7)) {
                                theReportCompany.UniqueID = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8)) {
                                theReportCompany.FormerNameNative = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9)) {
                                theReportCompany.FormerNameEN = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10)) {
                                theReportCompany.TradeNameNative = reader.GetString(10);
                            }
                            if (!reader.IsDBNull(11)) {
                                theReportCompany.TradeNameEN = reader.GetString(11);
                            }
                            if (!reader.IsDBNull(12)) {
                                theReportCompany.RegistrationInfoNative = reader.GetString(12);
                            }
                            if (!reader.IsDBNull(13)) {
                                theReportCompany.RegistrationInfoEN = reader.GetString(13);
                            }
                            if (!reader.IsDBNull(14)) {
                                theReportCompany.InternalComments = reader.GetString(14);
                            }
                            if (!reader.IsDBNull(15)) {
                                theReportCompany.ExternalCommentNative = reader.GetString(15);
                            }
                            if (!reader.IsDBNull(16)) {
                                theReportCompany.VAT = reader.GetString(16);
                            }
                            if (!reader.IsDBNull(17)) {
                                theReportCompany.CustomerCount = reader.GetInt32(17);
                            }
                            if (!reader.IsDBNull(18)) {
                                theReportCompany.RegisterCIID = reader.GetInt32(18);
                            }
                            if (!reader.IsDBNull(19)) {
                                theReportCompany.CustomerComments = reader.GetString(19);
                            }
                            if (!reader.IsDBNull(20)) {
                                theReportCompany.ImportComments = reader.GetString(20);
                            }
                            if (!reader.IsDBNull(21)) {
                                theReportCompany.ExportComments = reader.GetString(21);
                            }
                            if (!reader.IsDBNull(22)) {
                                theReportCompany.PaymentsComments = reader.GetString(22);
                            }
                            if (!reader.IsDBNull(23)) {
                                theReportCompany.SalesComments = reader.GetString(23);
                            }
                            if (!reader.IsDBNull(24)) {
                                theReportCompany.StatusID = reader.GetInt32(24);
                            }
/*	Svenni t�k �t			if(!reader.IsDBNull(25))
								theReportCompany.AddressNative = reader.GetString(25);
							if(!reader.IsDBNull(26))
								theReportCompany.AddressEN = reader.GetString(26);
							if(!reader.IsDBNull(27))
								theReportCompany.AreaCode = reader.GetString(27);
							if(!reader.IsDBNull(28))
								theReportCompany.PostBox = reader.GetString(28);
							if(!reader.IsDBNull(29))
								theReportCompany.CityNative = reader.GetString(29);
							if(!reader.IsDBNull(30))
								theReportCompany.CityEN = reader.GetString(30);
							if(!reader.IsDBNull(31))
								theReportCompany.CityID = reader.GetInt32(31);
*/
                            if (!reader.IsDBNull(32)) {
                                theReportCompany.RegistrationFormID = reader.GetInt32(32);
                            }
                            if (!reader.IsDBNull(33)) {
                                theReportCompany.CompanyCIID = reader.GetInt32(33);
                            }
                            if (!reader.IsDBNull(34)) {
                                theReportCompany.ExpireDate = reader.GetDateTime(34);
                            }
                            if (!reader.IsDBNull(35)) {
                                theReportCompany.ReportID = reader.GetInt32(35);
                            }
                            if (!reader.IsDBNull(36)) {
                                theReportCompany.InternalCommentsEN = reader.GetString(36);
                            }
                            if (!reader.IsDBNull(37)) {
                                theReportCompany.Symbol = reader.GetString(37);
                            }
                            if (!reader.IsDBNull(38)) {
                                theReportCompany.ExternalCommentEN = reader.GetString(38);
                            }
                            if (!reader.IsDBNull(39)) {
                                theReportCompany.LAR = reader.GetDateTime(39);
                            }
                            if (!reader.IsDBNull(40)) {
                                theReportCompany.LR = reader.GetDateTime(40);
                            }
                            if (!reader.IsDBNull(41)) {
                                theReportCompany.StructureReportResearchDate = reader.GetDateTime(41);
                            }
                            if (!reader.IsDBNull(42)) {
                                theReportCompany.Registered = reader.GetDateTime(42);
                            }
                            if (!reader.IsDBNull(43))
                            {
                                theReportCompany.Status = reader.GetString(43);
                            }

                            // h�r vantar a� taka address? og s�ma/GSM/Fax + a� hafa �etta � transaction
                            theReportCompany.PNumbers = userDALC.GetPhoneNumberList(creditInfoID);
                            theReportCompany.Address = userDALC.GetAddressList(creditInfoID);

                            theReportCompany.ContactPerson = GetContactPerson(creditInfoID);
                        }
                    } catch (Exception e) {
                        Logger.WriteToLog(className + " : " + funcName, e, true);
                    }
                }
            }
            return theReportCompany;
        }

        /// <summary>
        /// Takes Companies national ID and returns instance of ReportCompany 
        /// </summary>
        /// <param name="companyCIID">The companies national ID</param>
        /// <param name="CIID">The companies internal system id</param>
        /// <param name="nationalID">indicates wether to use nationalID or CIID</param>
        /// <returns>Instance of ReportCompanyBLLC</returns>
        public ReportCompanyBLLC GetBillingCompanyInfo(string nationalID, int CIID, bool useNationalID) {
            int creditInfoID = -1;
            ReportCompanyBLLC theReportCompany = new ReportCompanyBLLC();
            string funcName = "GetCompanyReport(string nationalID) ";
            if (useNationalID) {
                creditInfoID = GetCIIDByNationalID(nationalID);
            } else {
                creditInfoID = CIID;
            }

            if (creditInfoID > 0) {
                // Fylla reportbaunina
                // ....gera �etta � viewi e�a hva�??
                // ... e�a hreinlega hafa �etta einfalt og s�kja hvert fyrir sig �egar kemur a� �v�
                // a� birta sk�rsluna
                
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    try {
                        myOleDbConn.Open();
                        //	OleDbCommand myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                        // SELECT np_CreditInfoUser.Email, np_Companys.NameNative,np_Companys.NameEN,np_Companys.Established,np_Companys.LastContacted,np_Companys.LastUpdate,np_IDNumbers.Number,CompanyFormerNameNative,CompanyFormerNameEN,CompanyTradeNameNative,CompanyTradeNameEN,RegistrationInfoNative,RegistrationInfoEN,CommentI,CommentII,Vat,CustomerCount,ReportsAuthorCIID,CustomerComments,ImportComments,ExportComments,PaymentComments,SalesComments,cpi_CompanyReport.StatusID FROM np_CreditInfoUser LEFT OUTER JOIN cpi_CompanyReport ON np_CreditInfoUser.CreditInfoID = cpi_CompanyReport.CompanyCIID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE CompanyCIID = 22 AND np_IDNumbers.NumberTypeID = 1
                        string strSql = "SELECT dbo.np_CreditInfoUser.Email, CASE WHEN np_CreditInfoUser.Type = 1 THEN dbo.np_Companys.NameNative ELSE dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative END AS NameNative, "
                                        +
                                        "CASE WHEN np_CreditInfoUser.Type = 1 THEN dbo.np_Companys.NameEN ELSE dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN END AS NameEN,"
                                        +
                                        "dbo.np_Companys.Established, dbo.np_Companys.LastContacted, dbo.np_Companys.LastUpdate, dbo.np_Companys.URL, "
                                        +
                                        "dbo.np_IDNumbers.Number, dbo.np_Address.StreetNative, dbo.np_Address.StreetEN, dbo.np_Address.PostalCode, dbo.np_Address.PostBox, "
                                        +
                                        "dbo.np_City.NameEN AS Expr1, dbo.np_City.NameNative AS Expr2, dbo.np_City.CityID, dbo.np_CreditInfoUser.CreditInfoID, "
                                        + "dbo.np_Companys.Registered "
                                        + "FROM dbo.np_Companys RIGHT OUTER JOIN "
                                        + "dbo.np_City RIGHT OUTER JOIN "
                                        + "dbo.np_Individual RIGHT OUTER JOIN "
                                        + "dbo.np_CreditInfoUser LEFT OUTER JOIN "
                                        +
                                        "dbo.np_Address ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Address.CreditInfoID ON "
                                        +
                                        "dbo.np_Individual.CreditInfoID = dbo.np_CreditInfoUser.CreditInfoID ON dbo.np_City.CityID = dbo.np_Address.CityID ON "
                                        +
                                        "dbo.np_Companys.CreditInfoID = dbo.np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN "
                                        +
                                        "dbo.np_IDNumbers ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_IDNumbers.CreditInfoID "
                                        + "WHERE np_CreditInfoUser.CreditInfoID = " + creditInfoID;
                        OleDbCommand myOleDbCommand = new OleDbCommand(strSql, myOleDbConn);
                        OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                        if (reader.Read()) {
                            if (!reader.IsDBNull(0)) {
                                theReportCompany.Email = reader.GetString(0);
                            }
                            if (!reader.IsDBNull(1)) {
                                theReportCompany.NameNative = reader.GetString(1);
                            }
                            if (!reader.IsDBNull(2)) {
                                theReportCompany.NameEN = reader.GetString(2);
                            }
                            if (!reader.IsDBNull(3)) {
                                theReportCompany.Established = reader.GetDateTime(3);
                            }
                            if (!reader.IsDBNull(4)) {
                                theReportCompany.LastContacted = reader.GetDateTime(4);
                            }
                            if (!reader.IsDBNull(5)) {
                                theReportCompany.LastUpdated = reader.GetDateTime(5);
                            }
                            if (!reader.IsDBNull(6)) {
                                theReportCompany.HomePage = reader.GetString(6);
                            }
                            if (!reader.IsDBNull(7)) {
                                theReportCompany.UniqueID = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(15)) {
                                theReportCompany.CompanyCIID = reader.GetInt32(15);
                            }
                            if (!reader.IsDBNull(16)) {
                                theReportCompany.Registered = reader.GetDateTime(16);
                            }

                            // h�r vantar a� taka address? og s�ma/GSM/Fax + a� hafa �etta � transaction
                            theReportCompany.PNumbers = userDALC.GetPhoneNumberList(creditInfoID);
                            theReportCompany.Address = userDALC.GetAddressList(creditInfoID);

                            theReportCompany.ContactPerson = GetContactPerson(creditInfoID);
                        }
                    } catch (Exception e) {
                        Logger.WriteToLog(className + " : " + funcName, e, true);
                    }
                }
            }
            return theReportCompany;
        }

        /// <summary>
        /// Takes Companies national ID and returns instance of ReportCompany 
        /// </summary>
        /// <param name="companyCIID">The companies national ID</param>
        /// <param name="CIID">The companies internal system id</param>
        /// <param name="nationalID">indicates wether to use nationalID or CIID</param>
        /// <returns>Instance of ReportCompanyBLLC</returns>
        public ReportCompanyBLLC GetCompanyReport(int creditInfoID) {
            ReportCompanyBLLC theReportCompany = new ReportCompanyBLLC();
            string funcName = "GetCompanyReport(string creditInfoID) ";

            // Fylla reportbaunina �.e.
            // s�kja reportcompany
            // s�kja HistoryOperationReview
            // s�kja companyPrincipals og �eirra jobtitles
            // s�kja BoardMembers og �eirra ManagementPostions
            // s�kja RealEstates og RealEstatesTypes og RealEstatesOwnerType
            // s�kja ReportStatus
            // s�kja StaffCount
            // s�kja CompaniesNaceCode
            // s�kja Export/Import Countries
            // s�kja Companies Owners/Ownership
            // s�kja CompaniesSubsidaries
            // s�kja CompaniesBanks
            // s�kja CompaniesTradeTerms (??)
            // s�kja MarketingPostitions
            // ....gera �etta � viewi e�a hva�??
            // ... e�a hreinlega hafa �etta einfalt og s�kja hvert fyrir sig �egar kemur a� �v�
            // a� birta sk�rsluna
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    //	OleDbCommand myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                    // SELECT np_CreditInfoUser.Email, np_Companys.NameNative,np_Companys.NameEN,np_Companys.Established,np_Companys.LastContacted,np_Companys.LastUpdate,np_IDNumbers.Number,CompanyFormerNameNative,CompanyFormerNameEN,CompanyTradeNameNative,CompanyTradeNameEN,RegistrationInfoNative,RegistrationInfoEN,CommentI,CommentII,Vat,CustomerCount,ReportsAuthorCIID,CustomerComments,ImportComments,ExportComments,PaymentComments,SalesComments,cpi_CompanyReport.StatusID FROM np_CreditInfoUser LEFT OUTER JOIN cpi_CompanyReport ON np_CreditInfoUser.CreditInfoID = cpi_CompanyReport.CompanyCIID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE CompanyCIID = 22 AND np_IDNumbers.NumberTypeID = 1
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT np_CreditInfoUser.Email, np_Companys.NameNative,np_Companys.NameEN,np_Companys.Established,np_Companys.LastContacted,np_Companys.LastUpdate,np_Companys.URL,np_IDNumbers.Number,CompanyFormerNameNative,CompanyFormerNameEN,CompanyTradeNameNative,CompanyTradeNameEN,RegistrationInfoNative," +
                            "RegistrationInfoEN,InternalComment,ExternalCommentNative,Vat,CustomerCount,ReportsAuthorCIID,CustomerComments,ImportComments,ExportComments,PaymentComments,SalesComments,cpi_CompanyReport.StatusID, np_Address.StreetNative, np_Address.StreetEN,np_Address.PostalCode,np_Address.PostBox, np_City.NameEN,np_City.NameNative,np_City.CityID, cpi_CompanyReport.RegistrationFormID, np_CreditInfoUser.CreditInfoID,ExpireDate,ReportID,InternalCommentEN,Symbol,ExternalCommentEN, cpi_companyReport.LAR,cpi_companyReport.LR,cpi_companyReport.StructureReportDate, np_Companys.Registered  " +
                            "FROM np_CreditInfoUser LEFT OUTER JOIN cpi_CompanyReport ON np_CreditInfoUser.CreditInfoID = cpi_CompanyReport.CompanyCIID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID " +
                            "LEFT OUTER JOIN np_Address ON np_Companys.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_City ON np_Address.CityID = np_City.CityID " +
                            "LEFT OUTER JOIN cpi_RegistrationForm ON cpi_CompanyReport.RegistrationFormID = cpi_RegistrationForm.FormID " +
                            "WHERE np_CreditInfoUser.CreditInfoID = " + creditInfoID,
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            theReportCompany.Email = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theReportCompany.NameNative = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theReportCompany.NameEN = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            theReportCompany.Established = reader.GetDateTime(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            theReportCompany.LastContacted = reader.GetDateTime(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            theReportCompany.LastUpdated = reader.GetDateTime(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            theReportCompany.HomePage = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            theReportCompany.UniqueID = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            theReportCompany.FormerNameNative = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            theReportCompany.FormerNameEN = reader.GetString(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            theReportCompany.TradeNameNative = reader.GetString(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            theReportCompany.TradeNameEN = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            theReportCompany.RegistrationInfoNative = reader.GetString(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            theReportCompany.RegistrationInfoEN = reader.GetString(13);
                        }
                        if (!reader.IsDBNull(14)) {
                            theReportCompany.InternalComments = reader.GetString(14);
                        }
                        if (!reader.IsDBNull(15)) {
                            theReportCompany.ExternalCommentNative = reader.GetString(15);
                        }
                        if (!reader.IsDBNull(16)) {
                            theReportCompany.VAT = reader.GetString(16);
                        }
                        if (!reader.IsDBNull(17)) {
                            theReportCompany.CustomerCount = reader.GetInt32(17);
                        }
                        if (!reader.IsDBNull(18)) {
                            theReportCompany.RegisterCIID = reader.GetInt32(18);
                        }
                        if (!reader.IsDBNull(19)) {
                            theReportCompany.CustomerComments = reader.GetString(19);
                        }
                        if (!reader.IsDBNull(20)) {
                            theReportCompany.ImportComments = reader.GetString(20);
                        }
                        if (!reader.IsDBNull(21)) {
                            theReportCompany.ExportComments = reader.GetString(21);
                        }
                        if (!reader.IsDBNull(22)) {
                            theReportCompany.PaymentsComments = reader.GetString(22);
                        }
                        if (!reader.IsDBNull(23)) {
                            theReportCompany.SalesComments = reader.GetString(23);
                        }
                        if (!reader.IsDBNull(24)) {
                            theReportCompany.StatusID = reader.GetInt32(24);
                        }
                        /*	Svenni t�k �t			if(!reader.IsDBNull(25))
														theReportCompany.AddressNative = reader.GetString(25);
													if(!reader.IsDBNull(26))
														theReportCompany.AddressEN = reader.GetString(26);
													if(!reader.IsDBNull(27))
														theReportCompany.AreaCode = reader.GetString(27);
													if(!reader.IsDBNull(28))
														theReportCompany.PostBox = reader.GetString(28);
													if(!reader.IsDBNull(29))
														theReportCompany.CityNative = reader.GetString(29);
													if(!reader.IsDBNull(30))
														theReportCompany.CityEN = reader.GetString(30);
													if(!reader.IsDBNull(31))
														theReportCompany.CityID = reader.GetInt32(31);
						*/
                        if (!reader.IsDBNull(32)) {
                            theReportCompany.RegistrationFormID = reader.GetInt32(32);
                        }
                        if (!reader.IsDBNull(33)) {
                            theReportCompany.CompanyCIID = reader.GetInt32(33);
                        }
                        if (!reader.IsDBNull(34)) {
                            theReportCompany.ExpireDate = reader.GetDateTime(34);
                        }
                        if (!reader.IsDBNull(35)) {
                            theReportCompany.ReportID = reader.GetInt32(35);
                        }
                        if (!reader.IsDBNull(36)) {
                            theReportCompany.InternalCommentsEN = reader.GetString(36);
                        }
                        if (!reader.IsDBNull(37)) {
                            theReportCompany.Symbol = reader.GetString(37);
                        }
                        if (!reader.IsDBNull(38)) {
                            theReportCompany.ExternalCommentEN = reader.GetString(38);
                        }
                        if (!reader.IsDBNull(39)) {
                            theReportCompany.LAR = reader.GetDateTime(39);
                        }
                        if (!reader.IsDBNull(40)) {
                            theReportCompany.LR = reader.GetDateTime(40);
                        }
                        if (!reader.IsDBNull(41)) {
                            theReportCompany.StructureReportResearchDate = reader.GetDateTime(41);
                        }
                        if (!reader.IsDBNull(42)) {
                            theReportCompany.Registered = reader.GetDateTime(42);
                        }

                        // h�r vantar a� taka address? og s�ma/GSM/Fax + a� hafa �etta � transaction
                        theReportCompany.PNumbers = userDALC.GetPhoneNumberList(creditInfoID);
                        theReportCompany.Address = userDALC.GetAddressList(creditInfoID);
                        theReportCompany.ContactPerson = GetContactPerson(creditInfoID);
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    return null;
                }
            }

            return theReportCompany;
        }

        /// <summary>
        /// Returns the report status for given companyCIID
        /// </summary>
        /// <param name="creditInfoID">The companies internal system id</param>
        /// <returns>The status id for that company or 2 if no report is found</returns>
        public int GetReportStatus(int creditInfoID) {
            ReportCompanyBLLC theReportCompany = new ReportCompanyBLLC();
            string funcName = "GetReportStatus(int creditInfoID ";

            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    //	OleDbCommand myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                    // SELECT np_CreditInfoUser.Email, np_Companys.NameNative,np_Companys.NameEN,np_Companys.Established,np_Companys.LastContacted,np_Companys.LastUpdate,np_IDNumbers.Number,CompanyFormerNameNative,CompanyFormerNameEN,CompanyTradeNameNative,CompanyTradeNameEN,RegistrationInfoNative,RegistrationInfoEN,CommentI,CommentII,Vat,CustomerCount,ReportsAuthorCIID,CustomerComments,ImportComments,ExportComments,PaymentComments,SalesComments,cpi_CompanyReport.StatusID FROM np_CreditInfoUser LEFT OUTER JOIN cpi_CompanyReport ON np_CreditInfoUser.CreditInfoID = cpi_CompanyReport.CompanyCIID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE CompanyCIID = 22 AND np_IDNumbers.NumberTypeID = 1
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT cpi_CompanyReport.StatusID  " +
                        "FROM cpi_CompanyReport " +
                        "WHERE cpi_CompanyReport.CompanyCIID = " + creditInfoID,
                        myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return reader.GetInt32(0);
                    } else {
                        return 2; // meaning not ready
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    return -1;
                }
            }
        }

        /// <summary>
        /// Takes nationalID and finds matching CIID and returns
        /// </summary>
        /// <param name="nationalID">The companies nationalID</param>
        /// <returns>The companies internal system ID (CIID)</returns>
        public int GetCIIDByNationalID(string nationalID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // �essi virkar r�tt
                //SELECT DISTINCT np_CreditInfoUser.CreditInfoID FROM np_CreditInfoUser LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE np_IDNumbers.Number = '1111111110' AND np_IDNumbers.NumberTypeID = 1
                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT DISTINCT np_CreditInfoUser.CreditInfoID FROM np_CreditInfoUser LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE np_IDNumbers.Number = '" +
                        nationalID + "' AND np_IDNumbers.NumberTypeID = 1",
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return reader.GetInt32(0);
                }
                return -1;
            }
        }

        /// <summary>
        /// Takes CIID and finds matching National ID and returns
        /// </summary>
        /// <param name="CIID">The companies CIID</param>
        /// <returns>The companies National ID</returns>
        public string GetNationalIDByCIID(int CIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // �essi virkar r�tt
                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT DISTINCT np_IDNumbers.Number FROM np_IDNumbers where np_IDNumbers.CreditInfoID = " +
                        CIID + " and np_IDNumbers.NumberTypeID = 1",
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return reader.GetString(0);
                }
                return null;
            }
        }

        public string GetUserTypeByNationalID(string nationalID) {
            int creditInfoID = GetCIIDByNationalID(nationalID);
            string type = "-1";
            if (creditInfoID > 0) {
                type = userDALC.GetCIUserType(creditInfoID);
            }

            return type;
        }

        /// <summary>
        /// Returns all available areacodes as DataSet
        /// </summary>
        /// <returns>All available AreaCodes as DataSet</returns>
        public DataSet GetAreaCodesAsDataSet() {
            DataSet dsAreaCodes = new DataSet();
            string funcName = "GetAreaCodesAsDataSet() ";

            
            string query =
                "SELECT Distinct Postcode As PostalCode, CityNative AS LocationNative, CityEN AS LocationEN FROM np_Postcode where postcode is not null order by Postcode";

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsAreaCodes);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsAreaCodes;
        }

        public DataSet GetReportStatusAsDataSet() {
            DataSet dsReportStatus = new DataSet();
            string funcName = "GetReportStatusAsDataSet() ";

            
            string query = "SELECT * FROM cpi_ReportStatus";

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsReportStatus);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsReportStatus;
        }

        public DataSet GetExportAreasAsDataSet(int companyCIID) {
            DataSet dsExportAreaCodes = new DataSet();
            string funcName = "GetExportAreasAsDataSet(int companyCIID) ";

            
            string query =
                "SELECT cpi_Countries.CountryID,cpi_Countries.NameNative,cpi_Countries.NameEN FROM cpi_ExportCountries LEFT OUTER JOIN cpi_Countries ON cpi_ExportCountries.CountryID = cpi_Countries.CountryID WHERE cpi_ExportCountries.CompanyCIID = " +
                companyCIID + "";

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsExportAreaCodes);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsExportAreaCodes;
        }

        public DataSet GetImportAreasAsDataSet(int companyCIID) {
            DataSet dsImportAreaCodes = new DataSet();
            string funcName = "GetImportAreasAsDataSet(int companyCIID) ";

            
            string query =
                "SELECT cpi_Countries.CountryID,cpi_Countries.NameNative,cpi_Countries.NameEN FROM cpi_ImportCountries LEFT OUTER JOIN cpi_Countries ON cpi_ImportCountries.CountryID = cpi_Countries.CountryID WHERE cpi_ImportCountries.CompanyCIID = " +
                companyCIID + "";

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsImportAreaCodes);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsImportAreaCodes;
        }

        /// <summary>
        /// Get all available company registration forms
        /// </summary>
        /// <returns>available registration forms as DataSet</returns>
        public DataSet GetRegistrationFormAsDataSet() {
            DataSet dsRegistrationForm = new DataSet();
            string funcName = "GetRegistrationFormAsDataSet() ";

            
            string query = "SELECT * FROM cpi_RegistrationForm";

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsRegistrationForm);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsRegistrationForm;
        }

        /// <summary>
        /// Returns all available Tenure types
        /// </summary>
        /// <returns>All available Tenure types as DataSet</returns>
        public DataSet GetTenureAsDataSet() {
            DataSet dsTenure = new DataSet();
            string funcName = "GetTenureAsDataSet() ";

            
            string query = "SELECT * FROM cpi_RealEstatesOwnerType";

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsTenure);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsTenure;
        }

        /// <summary>
        /// Returns all available Estates locations
        /// </summary>
        /// <returns>All available estates location as DataSet</returns>
        public DataSet GetEstatesPostalCodeAsDataSet() {
            DataSet dsEstatesLocations = new DataSet();
            string funcName = "GetEstatesPostalCodeAsDataSet() ";

            
            string query = "SELECT * FROM cpi_EstatesPostalCode";

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsEstatesLocations);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsEstatesLocations;
        }

        /// <summary>
        /// Returns all available Estates locations
        /// </summary>
        /// <returns>All available estates location as DataSet</returns>
        public DataSet GetRealEstatesLocationAsDataSet() {
            DataSet dsEstatesLocations = new DataSet();
            string funcName = "GetRealEstatesLocationAsDataSet() ";

            
            string query = "SELECT * FROM cpi_RealEstatesLocation";

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsEstatesLocations);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsEstatesLocations;
        }

        /// <summary>
        /// Returns all available Estates permises types
        /// </summary>
        /// <returns>All available Estates permises types as DataSet</returns>
        public DataSet GetEstatesPermisesTypeAsDataSet() {
            DataSet dsEstatesPermisesTypes = new DataSet();
            string funcName = "GetEstatesPermisesTypeAsDataSet() ";

            
            string query = "SELECT * FROM cpi_RealEstatesType";

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsEstatesPermisesTypes);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsEstatesPermisesTypes;
        }

        /// <summary>
        /// Takes companies internal system id and returns real estates as DataSet
        /// </summary>
        /// <param name="companyCIID">The companies internal system id (CIID)</param>
        /// <returns>The real estate(s) as DataSet</returns>
        public DataSet GetRealEstatesAsDataSet(int companyCIID) {
            DataSet dsRealEstates = new DataSet();
            string funcName = "GetRealEstatesAsDataSet(int companyCIID) ";

            
            /*	
			 * SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, np_Individual.SurNameNative, np_Individual.SurNameEN, np_Address.StreetNative, np_Address.StreetEN,np_IDNumbers.Number,cpi_ManagementPositions.TitleNative,cpi_ManagementPositions.TitleEN FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_BoardMembers ON np_Individual.CreditInfoID = cpi_BoardMembers.BoardMemberCIID LEFT OUTER JOIN cpi_ManagementPositions ON cpi_BoardMembers.ManagementPositionID = cpi_ManagementPositions.ManagementPositionID  WHERE np_Individual.CreditInfoID IN (SELECT cpi_BoardMembers.BoardMemberCIID FROM cpi_BoardMembers WHERE cpi_BoardMembers.CompanyCIID = 1) AND np_IDNumbers.NumberTypeID = 1
			*/
            string query =
                "SELECT DISTINCT cpi_RealEstates.RealEstatesID,cpi_RealEstates.CompanyCIID,cpi_RealEstates.Size,cpi_RealEstates.YearBuilt, cpi_RealEstates.AddressNative,cpi_RealEstates.AddressEN,cpi_RealEstates.OwnerTypeID,cpi_RealEstates.PostCode,cpi_RealEstates.CityID,cpi_RealEstates.CountryID,cpi_RealEstatesType.RealEstatesTypeID ,cpi_RealEstatesType.TypeNative,cpi_RealEstatesType.TypeEN,cpi_RealEstates.InsuranceValue,cpi_RealEstatesLocation.LocationID ,cpi_RealEstatesLocation.DescriptionNative,cpi_RealEstatesLocation.DescriptionEN " +
                "FROM cpi_RealEstates LEFT OUTER JOIN cpi_RealEstatesType ON cpi_RealEstates.RealEstatesTypeID = cpi_RealEstatesType.RealEstatesTypeID " +
                "LEFT OUTER JOIN cpi_RealEstatesLocation ON cpi_RealEstates.RealEstatesLocationID = cpi_RealEstatesLocation.LocationID " +
                "LEFT OUTER JOIN cpi_RealEstatesOwnerType ON cpi_RealEstates.OwnerTypeID = cpi_RealEstatesOwnerType.OwnerTypeID " +
                "WHERE cpi_RealEstates.CompanyCIID = " + companyCIID + "";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsRealEstates);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsRealEstates;
        }

        /// <summary>
        /// Takes instance of Principal and updates in the database
        /// </summary>
        /// <param name="thePrincipal">instance of PrincipalBLLC</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdatePrincipals(PrincipalsBLLC thePrincipal) { return false; }

        /// <summary>
        /// Takes Principal internal CIID and deletes from database
        /// </summary>
        /// <param name="principalsID">Principal CIID</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool DeletePrincipal(int principalsCIID, int companyCIID, int jobTitleID) {
            string funcName = "DeletePrincipals(int principalsCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_CompanyPrincipals WHERE PrincipalCIID = " + principalsCIID +
                                         " AND CompanyCIID = " + companyCIID + " AND JobTitleID = " + jobTitleID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Deletes shareholder owner from database
        /// </summary>
        /// <param name="shareholdersCIID">The shareholder's internal System ID</param>
        /// <param name="companyCIID">The companys internal system ID</param>
        /// <returns></returns>
        public bool DeleteShareholdersOwner(int shareholdersCIID, int companyCIID, int ownershipID) {
            string funcName = "DeleteShareholdersOwner(int shareholdersCIID,int companyCIID,int ownershipID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_Owners WHERE OwnerCIID = " + shareholdersCIID +
                                         " AND CompanyCIID = " + companyCIID + " AND OwnershipID = " + ownershipID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes Company system ID (CIID) and returns ArrayList of Principals
        /// </summary>
        /// <param name="companyCIID">Company system ID</param>
        /// <returns>ArrayList of Principals CIID's</returns>
        public ArrayList GetPrincipals(int companyCIID) {
            ArrayList arrPrincipals = new ArrayList();
            string funcName = "GetPrincipals(int companyCIID) ";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM cpi_CompanyPrincipals WHERE CompanyCIID = " + companyCIID + "", myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    while (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            arrPrincipals.Add(reader.GetInt32(0));
                        }
                    }
                    reader.Close();
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
            }
            return arrPrincipals;
        }

        /// <summary>
        /// Takes an instance of Principal and adds to database
        /// </summary>
        /// <param name="thePrincipal">instance of PrincipalBLLC/param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddPrincipal(PrincipalsBLLC thePrincipal, int companyCIID) {
            string funcName = "AddPrincipal(PrincipalsBLLC thePrincipal,int companyCIID)";
            bool result = false;
            int principalCIID = -1;
            // the princepals should always have national id however if somehow the system has failed and national ID is empty
            // then double check
            if (thePrincipal.CreditInfoID < 0) {
                principalCIID = GetCIIDByNationalID(thePrincipal.NationalID);
            } else {
                principalCIID = thePrincipal.CreditInfoID;
            }
            CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
            Indivitual theIndi = new Indivitual();
            theIndi.CreditInfoID = principalCIID;
            theIndi.FirstNameNative = thePrincipal.FirstNameNative;
            theIndi.SurNameNative = thePrincipal.SurNameNative;
            theIndi.FirstNameEN = thePrincipal.FirstNameEN;
            theIndi.SurNameEN = thePrincipal.SurNameEN;

            IDNumber theIDNumber = new IDNumber();
            try {
                theIDNumber.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID").Trim());
            } catch (Exception ex) {
                Logger.WriteToLog(className + " : " + funcName, ex, true);
                return false;
            }

            theIDNumber.Number = thePrincipal.NationalID;
            theIndi.IDNumbers = new ArrayList();
            theIndi.IDNumbers.Add(theIDNumber);

            theIndi.EducationID = 0; // meaning not accessible
            theIndi.ProfessionID = 0; // meaning not accessible
            theIndi.Number = new ArrayList();
            theIndi.Type = CigConfig.Configure("lookupsettings.individualID"); // meaning Indivitual
            if (principalCIID < 0) {
                principalCIID = theUserDAL.AddNewIndivitual(theIndi);
                if (principalCIID > -1) {
                    thePrincipal.CreditInfoID = principalCIID;
                    result = InsertPrincipal(thePrincipal, companyCIID);
                } else {
                    return false;
                }
            } else // CIID exists then update the user...
            {
                theIndi.IDNumbers = new ArrayList(); // prevent any ID numbers update
                // 	principalCIID = theUserDAL.UpdateIndivitual(theIndi); // not such a good idea to update personnal info here, use the UserAdmin for that one ...
                // h�r �arf einnig a� athuga hvort individual er �egar skr��ur principal	
                if (principalCIID > 0) {
                    thePrincipal.CreditInfoID = principalCIID;
                }
                result = UpdatePrincipal(thePrincipal, companyCIID);
            }
            return result;
        }

        /// <summary>
        /// Takes an instance of Board Member and add to database
        /// </summary>
        /// <param name="theBoardMember">instance of BoardMemberBLLC</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddBordMember(BoardMemberBLLC theBoardMember, int companyCIID) {
            string funcName = "AddBordMember(BoardMemberBLLC theBoardMember, int companyCIID)";

            bool result = false;
            int boardMemberCIID = -1;
            if (theBoardMember.CreditInfoID <= 0) {
                boardMemberCIID = GetCIIDByNationalID(theBoardMember.NationalID);
            } else {
                boardMemberCIID = theBoardMember.CreditInfoID;
            }

            CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
            Indivitual theIndi = new Indivitual();
            theIndi.CreditInfoID = boardMemberCIID;
            IDNumber theIDNumber = new IDNumber();
            if (theBoardMember.FirstNameNative != null && theBoardMember.SurNameNative != null) {
                theIndi.FirstNameNative = theBoardMember.FirstNameNative;
                theIndi.SurNameNative = theBoardMember.SurNameNative;
            } else {
                theIndi.FirstNameNative = theBoardMember.FirstNameEN;
                theIndi.SurNameNative = theBoardMember.SurNameEN;
            }

            try {
                theIDNumber.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID").Trim());
            } catch (Exception ex) {
                Logger.WriteToLog(className + " : " + funcName, ex, true);
                return false;
            }
            theIDNumber.Number = theBoardMember.NationalID;
            theIndi.IDNumbers = new ArrayList();
            theIndi.IDNumbers.Add(theIDNumber);
/*			Address theAddress = new Address();
			theAddress.StreetNative = theBoardMember.SingleAddressNative;
			// �a� �arf n�ttlega a� loada borgunum � Location boxi� ... e�a hva�?
			theAddress.CityID = 1; // meaning not accessible.  
			theIndi.Address = new ArrayList();
			theIndi.Address.Add(theAddress);
*/
            theIndi.EducationID = 0; // meaning not accessible
            theIndi.ProfessionID = 0; // meaning not accessible
            theIndi.Number = new ArrayList();

            theIndi.Type = CigConfig.Configure("lookupsettings.individualID");
            if (boardMemberCIID < 0) {
                boardMemberCIID = theUserDAL.AddNewIndivitual(theIndi);
                theBoardMember.CreditInfoID = boardMemberCIID;
                result = InsertBoardMember(theBoardMember, companyCIID);
            } else // CIID exists then update the user...
            {
                theIndi.IDNumbers = new ArrayList(); // prevent any ID numbers update
                //	boardMemberCIID = theUserDAL.UpdateIndivitual(theIndi);  // not such a good idea to update personal info here, use the UserAdmin for that one ...
                if (boardMemberCIID > 0) {
                    theBoardMember.CreditInfoID = boardMemberCIID;
                }
                result = UpdateBoardMember(theBoardMember, companyCIID);
            }
            return result;
        }

        /// <summary>
        /// This function serves as control function for inserting operation values 
        /// </summary>
        /// <param name="arrImportCountrieID">ArrayList with importCountrie values</param>
        /// <param name="arrExportCountrieID">ArrayList with exportCountrie values</param>
        /// <param name="arrCompanyOperation">ArrayList with companyOperation values</param>
        /// <param name="arrCompanyBanks">ArrayList with companyBanks values</param>
        /// <param name="arrCompanyCustomerTypes">ArrayList with companyCustomerType values</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if insert is successful, false otherwise</returns>
        public bool AddOperationInfo(
            ArrayList arrImportCountrieID,
            ArrayList arrExportCountrieID,
            ArrayList arrCompanyOperation,
            ArrayList arrFormerOperation,
            ArrayList arrCompanyBanks,
            ArrayList arrCompanyCustomerTypes,
            ArrayList arrSalesTerms,
            ArrayList arrPayingTerms,
            int companyCIID) {
            string funcName =
                "AddOperationInfo(ArrayList arrImportCountrieID,ArrayList arrExportCountrieID,ArrayList arrCompanyOperation,ArrayList arrCompanyBanks,ArrayList arrCompanyCustomerTypes, int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            // int creditInfoID = theComp.CreditInfoID;
            bool insertOK = false;
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbCommand.Transaction = myTrans;
                try {
                    insertOK = InsertImportCountrie(arrImportCountrieID, companyCIID, myOleDbCommand);
                    if (insertOK) {
                        insertOK = InsertExportCountrie(arrExportCountrieID, companyCIID, myOleDbCommand);
                    }
                    if (insertOK) {
                        insertOK = InsertCompanyOperation(arrCompanyOperation, companyCIID, myOleDbCommand, true);
                    }
                    if (insertOK) {
                        insertOK = InsertCompanyOperation(arrFormerOperation, companyCIID, myOleDbCommand, false);
                    }
                    if (insertOK) {
                        insertOK = InsertCompanyBanks(arrCompanyBanks, companyCIID, myOleDbCommand);
                    }
                    if (insertOK) {
                        insertOK = InsertCompanyCustomerType(arrCompanyCustomerTypes, companyCIID, myOleDbCommand);
                    }
                    if (insertOK) {
                        insertOK = InsertCompanyTradeTerms(arrSalesTerms, companyCIID, myOleDbCommand, true);
                    }
                    if (insertOK) {
                        insertOK = InsertCompanyTradeTerms(arrPayingTerms, companyCIID, myOleDbCommand, false);
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    myTrans.Rollback();
                    return false;
                }
                if (!insertOK) {
                    myTrans.Rollback();
                } else {
                    myTrans.Commit();
                }
            }
            return insertOK;
        }

        /// <summary>
        /// Inserts new instance of Principal
        /// </summary>
        /// <param name="thePrincipal">Instance of Principal</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if OK, false otherwise</returns>
        public bool InsertPrincipal(PrincipalsBLLC thePrincipal, int companyCIID) {
            string funcName = "InsertPrincipal(PrincipalBLLC thePrincipal, int companyCIID)";
            // if the board member is already registed for the company then he should be updated 
            //	if(PrincipalAlreadyRegisted(thePrincipal,companyCIID))
            //		return this.UpdatePrincipal(thePrincipal,companyCIID);
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_CompanyPrincipals(PrincipalCIID,CompanyCIID,JobTitleID,EducationID) VALUES(?,?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("PrincipalCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = thePrincipal.CreditInfoID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("JobTitleID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = thePrincipal.JobTitleID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("EducationID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = thePrincipal.EducationID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Inserts new instance of Company history
        /// </summary>
        /// <param name="theHistory">Instance of History</param>
        /// <param name="CIID">The companies internal system id</param>
        /// <returns></returns>
        public bool InsertHistory(HistoryBLLC theHistory, int CIID) {
            string funcName = "InsertHistory(HistoryBLLC theHistory,int CIID)";

            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_CompanyHistory(CompanyCIID,HistoryTypeID,HistoryNative,HistoryEN) VALUES(?,?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = CIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("HistoryTypeID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHistory.HistoryID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("HistoryNative", OleDbType.VarWChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHistory.HistoryNative;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("HistoryEN", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHistory.HistoryEN;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Inserts new instance of Board member
        /// </summary>
        /// <param name="theBoardMember">Instance of boardMember</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if OK, false otherwise</returns>
        public bool InsertBoardMember(BoardMemberBLLC theBoardMember, int companyCIID) {
            string funcName = "InsertBoardMember(BoardMemberBLLC theBoardMember, int companyCIID)";
            // if the board member is already registed for the company the he should be updated 
            if (BoardMemberAlreadyRegisted(theBoardMember, companyCIID)) {
                return UpdateBoardMember(theBoardMember, companyCIID);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_BoardMembers(BoardMemberCIID,ManagementPositionID,CompanyCIID) VALUES(?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("BoardMemberCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theBoardMember.CreditInfoID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("ManagementPositionID", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theBoardMember.ManagementPostionID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes ArrayList of ImportCountrieID and inserts to database
        /// </summary>
        /// <param name="arrImportCountrieID">ArrayList of ImportCountrieID's</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <param name="myOleDbConn">The connection instance to use</param>
        /// <returns>true if successfull, false otherwise</returns>
        public bool InsertImportCountrie(ArrayList arrImportCountrieID, int companyCIID, OleDbCommand myOleDbCommand) {
            bool isOK = DeleteImportCountrie(companyCIID, myOleDbCommand);
            // if delete failes then return false
            if (!isOK) {
                return isOK;
            }

            string funcName =
                "InsertImportCountrie(ArrayList arrImportCountrieID, int companyCIID,OleDbCommand myOleDbCommand)";
            myOleDbCommand.CommandText = "INSERT INTO cpi_ImportCountries(CompanyCIID,CountryID) VALUES(?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                for (int i = 0; i < arrImportCountrieID.Count; i++) {
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CountryID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = (int) arrImportCountrieID[i];
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Takes ArrayList of ExportCountrieID's and inserts to database
        /// </summary>
        /// <param name="arrExportCountrieID">ArrayList of ExportCountrieID's</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <param name="myOleDbConn">The connection instance to use</param>
        /// <returns>true if successfull, false otherwise</returns>
        public bool InsertExportCountrie(ArrayList arrExportCountrieID, int companyCIID, OleDbCommand myOleDbCommand) {
            bool isOK = DeleteExportCountrie(companyCIID, myOleDbCommand);
            // if delete failes then return false
            if (!isOK) {
                return isOK;
            }
            string funcName =
                "InsertExportCountrie(ArrayList arrExportCountrieID, int companyCIID,OleDbCommand myOleDbCommand)";
            myOleDbCommand.CommandText = "INSERT INTO cpi_ExportCountries(CompanyCIID,CountryID) VALUES(?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                for (int i = 0; i < arrExportCountrieID.Count; i++) {
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CountryID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = (int) arrExportCountrieID[i];
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// This function adds operation (nace codes) to company 
        /// </summary>
        /// <param name="arrCompanyOperation">ArrayList with companyOperation values</param>
        /// <param name="currentOperation">True if this is the current operation</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if insert is successful, false otherwise</returns>
        public bool AddCompanyOperation(ArrayList arrCompanyOperation, bool currentOperation, int companyCIID) {
            string funcName =
                "AddCompanyOperation(ArrayList arrImportCountrieID,ArrayList arrExportCountrieID,ArrayList arrCompanyOperation,ArrayList arrCompanyBanks,ArrayList arrCompanyCustomerTypes, int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            // int creditInfoID = theComp.CreditInfoID;
            bool insertOK = false;
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbCommand.Transaction = myTrans;
                try {
                    insertOK = InsertCompanyOperation(
                        arrCompanyOperation, companyCIID, myOleDbCommand, currentOperation);
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    myTrans.Rollback();
                    return false;
                }
                if (!insertOK) {
                    myTrans.Rollback();
                } else {
                    myTrans.Commit();
                }
            }
            return insertOK;
        }

        /// <summary>
        /// Takes ArrayList of CompanyOperationID's and inserts to database
        /// </summary>
        /// <param name="arrCompanyOperation">ArrayList of CompanyOperationID's</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <param name="myOleDbConn">The connection instance to use</param>
        /// <returns>true if successfull, false otherwise</returns>
        public bool InsertCompanyOperation(
            ArrayList arrCompanyOperation, int companyCIID, OleDbCommand myOleDbCommand, bool current) {
            bool isOK = DeleteCompanyOperaton(companyCIID, myOleDbCommand, current);
            // if delete failes then return false
            if (!isOK) {
                return isOK;
            }
            string funcName =
                "InsertCompanyOperation(ArrayList arrCompanyOperation, int companyCIID,OleDbCommand myOleDbCommand)";
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_CompaniesNaceCodes(CompanyCIID,NaceCodeID,CurrentCode, OrderLevel) VALUES(?,?,?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                int index = 0;
                for (int i = 0; i < arrCompanyOperation.Count; i++) {
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("NaceCodeID", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = arrCompanyOperation[i];
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CurrentCode", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = current.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("OrderLevel", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = index;
                    myOleDbCommand.Parameters.Add(myParam);
                    index++;
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Takes ArrayList of CompanyBankID's and inserts to database
        /// </summary>
        /// <param name="arrCompanyBanks">ArrayList of CompanyBankID's</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <param name="myOleDbConn">The connection instance to use</param>
        /// <returns>true if successfull, false otherwise</returns>
        public bool InsertCompanyBanks(ArrayList arrCompanyBanks, int companyCIID, OleDbCommand myOleDbCommand) {
            bool isOK = DeleteCompanyBanks(companyCIID, myOleDbCommand);
            // if delete failes then return false
            if (!isOK) {
                return isOK;
            }
            string funcName =
                "InsertCompanyBanks(ArrayList arrCompanyBanks, int companyCIID,OleDbCommand myOleDbCommand)";
            myOleDbCommand.CommandText = "INSERT INTO cpi_CompanyBanks(CompanyCIID,BankID) VALUES(?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                for (int i = 0; i < arrCompanyBanks.Count; i++) {
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("BankID", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = arrCompanyBanks[i];
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Takes ArrayList of CompanyCustomerTypes ID's and inserts to database
        /// </summary>
        /// <param name="arrCompanyCustomerTypes">ArrayList of CompanyCustomerTypes ID's</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <param name="myOleDbConn">The connection instance to use</param>
        /// <returns>true if successfull, false otherwise</returns>
        public bool InsertCompanyCustomerType(
            ArrayList arrCompanyCustomerTypes, int companyCIID, OleDbCommand myOleDbCommand) {
            bool isOK = DeleteCompanyCustomerType(companyCIID, myOleDbCommand);
            // if delete failes then return false
            if (!isOK) {
                return isOK;
            }
            string funcName =
                "InsertCompanyCustomerType(ArrayList arrCompanyCustomerTypes, int companyCIID,OleDbCommand myOleDbCommand)";
            myOleDbCommand.CommandText = "INSERT INTO cpi_CompaniesCustomerType(CustomerTypeID,CompanyCIID) VALUES(?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                for (int i = 0; i < arrCompanyCustomerTypes.Count; i++) {
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("CustomerTypeID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = (int) arrCompanyCustomerTypes[i];
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        public bool InsertCompanyTradeTerms(
            ArrayList arrSalesTerms, int companyCIID, OleDbCommand myOleDbCommand, bool sales) {
            bool isOK = DeleteCompanyTradeTerms(companyCIID, myOleDbCommand, sales);
            // if delete failes then return false
            if (!isOK) {
                return isOK;
            }
            string funcName =
                "InsertCompanyTradeTerms(ArrayList arrSalesTerms,int companyCIID,OleDbCommand myOleDbCommand,bool sales)";
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_CompanyTradeTerms(TermsID,SalesTerm,CompanyCIID) VALUES(?,?,?)";

            OleDbParameter myParam = new OleDbParameter();
            try {
                for (int i = 0; i < arrSalesTerms.Count; i++) {
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("TermsID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = (int) arrSalesTerms[i];
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("SalesTerms", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = sales.ToString();
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Inserts new instance of Shareholder
        /// </summary>
        /// <param name="theShareholder"></param>
        /// <param name="companyCIID"></param>
        /// <returns></returns>
        public bool InsertShareholderOwner(
            int ownerCIID, int ownershipID, int ownership, decimal ownershipPercentage, int companyCIID) {
            string funcName = "InsertShareholderOwner(ShareholdersOwnerPersonBLLC theShareholder)";
            // if the board member is already registed for the company the he should be updated 
            if (ShareholderAlreadyRegisted(ownerCIID, companyCIID, ownershipID)) {
                return UpdateShareholderOwner(ownerCIID, ownershipID, ownership, ownershipPercentage, companyCIID);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_Owners(CompanyCIID,OwnerCIID,OwnerShipID,Ownership, Ownership_Perc) VALUES(?,?,?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("OwnerCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = ownerCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("OwnerShipID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = ownershipID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Ownership", OleDbType.BigInt);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = ownership;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Ownership_Perc", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = ownershipPercentage;
                    myOleDbCommand.Parameters.Add(myParam);
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes instance of ReportCompanyBLLC and inserts in the database
        /// </summary>
        /// <param name="theReport">Instance of ReportCompanyBLLC</param>
        /// <returns>true if insert is successfull, false otherwise</returns>
        public bool InsertReportCompany(ReportCompanyBLLC theReport) {
            string funcName = "InsertReportCompany(ReportCompanyBLLC theReport)";

            OleDbCommand myOleDbCommand = new OleDbCommand();
            int resultID = 0;

            if (theReport.CompanyCIID < 1) {
                resultID = GetCIIDByNationalID(theReport.UniqueID);
            } else {
                resultID = theReport.CompanyCIID;
            }

            if (resultID > 0) {
                theReport.CompanyCIID = resultID;
            } else {
                // hmmm, h�r �arf a� uppf�ra np_CreditInfoUser, np_Companys og cpi_CompanyReport

                Company theComp = new Company();
                theComp.IDNumbers = new ArrayList();

                IDNumber theIDNumber = new IDNumber();
                theIDNumber.NumberTypeID = 1;
                theIDNumber.Number = theReport.UniqueID;
                theComp.IDNumbers.Add(theIDNumber);

                theComp.CreditInfoID = theReport.CompanyCIID;
                theComp.Email = theReport.Email;
                theComp.NameNative = theReport.NameNative;
                theComp.NameEN = theReport.NameEN;
                theComp.Org_status_code = 1; // meaning not accessible
                theComp.Address = theReport.Address;
                theComp.Number = theReport.PNumbers;
                theComp.Type = CigConfig.Configure("lookupsettings.companyID");
                theComp.Established = theReport.Established;
                theComp.Registered = theReport.Registered;
                theComp.LastContacted = theReport.LastContacted;
                theComp.LastUpdate = theReport.LastUpdated;
                theComp.URL = theReport.HomePage;

                CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
                resultID = userDALC.AddCompany(theComp);
                if (resultID < 0) {
                    Logger.WriteToLog(
                        className + " : " + funcName + " : " +
                        "Error i InsertReportCompany(ReportCompanyBLLC theReport)",
                        true);
                    return false;
                }
                theReport.CompanyCIID = resultID;
            }
//SVENNI
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();
                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "INSERT cpi_CompanyReport(CompanyCIID,CompanyFormerNameNative,CompanyFormerNameEN, CompanyTradeNameNative, " +
                            "CompanyTradeNameEN,InternalComment,InternalCommentEN,RegistrationFormID,ReportsAuthorCIID,Vat,Symbol,LAR,LR,StructureReportDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                            myOleDbConn);
                    myOleDbCommand.Transaction = myTrans;

                    OleDbParameter myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.IsNullable = false;
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.CompanyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "CompanyFormerNameNative",
                        "CompanyFormerNameEN",
                        ParameterDirection.Input,
                        theReport.FormerNameNative,
                        theReport.FormerNameEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "CompanyTradeNameNative",
                        "CompanyTradeNameEN",
                        ParameterDirection.Input,
                        theReport.TradeNameNative,
                        theReport.TradeNameEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "InternalComment",
                        "InternalCommentEN",
                        ParameterDirection.Input,
                        theReport.InternalComments,
                        theReport.InternalCommentsEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("RegistrationFormID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.RegistrationFormID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("ReportsAuthorCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.RegisterCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Vat", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.VAT;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Symbol", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theReport.Symbol;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("LAR", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (theReport.LAR != DateTime.MinValue) {
                        myParam.Value = theReport.LAR;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("LR", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (theReport.LR != DateTime.MinValue) {
                        myParam.Value = theReport.LR;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("StructureReportDate", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (theReport.StructureReportResearchDate != DateTime.MinValue) {
                        myParam.Value = theReport.StructureReportResearchDate;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    InsertContactPerson(theReport.ContactPerson, myOleDbCommand, theReport.CompanyCIID);

                    myTrans.Commit();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    myTrans.Rollback();
                    return false;
                }
                return true;
            }
            //SVENNI

            /*
                        // e-� svona
                        // shareholderCIID = theUserDAL.UpdateCompany(theComp);

                        // Eftirfarandi er nokku� n�rri lagi


                        using(OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                        {
                            myOleDbCommand = new OleDbCommand("INSERT cpi_CompanyReport(CompanyCIID,CompanyFormerNameNative,CompanyFormerNameEN, CompanyTradeNameNative, " +
                                "CompanyTradeNameEN,InternalComment,RegistrationFormID,ReportsAuthorCIID,Vat) VALUES (?,?,?,?,?,?,?,?,?)",myOleDbConn);

                            myOleDbCommand.Connection = myOleDbConn;
                            myOleDbConn.Open();
                            try
                            {
                                OleDbParameter myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                                myParam.IsNullable = false;
                                myParam.Direction = ParameterDirection.Input;
                                myParam.Value = theReport.CompanyCIID;
                                myOleDbCommand.Parameters.Add(myParam);
                                myParam = new OleDbParameter("CompanyFormerNameNative", OleDbType.WChar);
                                myParam.IsNullable = true;
                                myParam.Direction = ParameterDirection.Input;
                                if(theReport.FormerNameNative != null) // if no native info
                                    myParam.Value = theReport.FormerNameNative;
                                else // use english instead
                                    myParam.Value = theReport.FormerNameEN;
                                myOleDbCommand.Parameters.Add(myParam);
                                myParam = new OleDbParameter("CompanyFormerNameEN", OleDbType.VarChar);
                                myParam.IsNullable = true;
                                myParam.Direction = ParameterDirection.Input;
                                if(theReport.FormerNameEN != null) // if no english info
                                    myParam.Value = theReport.FormerNameEN;
                                else // use native instead
                                    myParam.Value = theReport.FormerNameNative;
                                myOleDbCommand.Parameters.Add(myParam);
                                myParam = new OleDbParameter("CompanyTradeNameNative", OleDbType.WChar);
                                myParam.IsNullable = true;
                                myParam.Direction = ParameterDirection.Input;
                                if(theReport.TradeNameNative != null) // if no native info
                                    myParam.Value = theReport.TradeNameNative;
                                else // use english instead
                                    myParam.Value = theReport.TradeNameEN;
                                myOleDbCommand.Parameters.Add(myParam);
                                myParam = new OleDbParameter("CompanyTradeNameEN", OleDbType.VarChar);
                                myParam.Direction = ParameterDirection.Input;
                                if(theReport.TradeNameEN != null) // if no english info
                                    myParam.Value = theReport.TradeNameEN;
                                else // use native instead
                                    myParam.Value = theReport.TradeNameNative;
                                myOleDbCommand.Parameters.Add(myParam);
                                myParam = new OleDbParameter("InternalComment", OleDbType.WChar);
                                myParam.Direction = ParameterDirection.Input;
                                myParam.Value = theReport.InternalComments;
                                myOleDbCommand.Parameters.Add(myParam);
                                myParam = new OleDbParameter("RegistrationFormID", OleDbType.Integer);
                                myParam.Direction = ParameterDirection.Input;
                                myParam.Value = theReport.RegistrationFormID;
                                myOleDbCommand.Parameters.Add(myParam);
                                myParam = new OleDbParameter("ReportsAuthorCIID", OleDbType.Integer);
                                myParam.Direction = ParameterDirection.Input;
                                myParam.Value = theReport.RegisterCIID;
                                myOleDbCommand.Parameters.Add(myParam);
                                myParam = new OleDbParameter("Vat", OleDbType.VarChar);
                                myParam.Direction = ParameterDirection.Input;
                                myParam.Value = theReport.VAT;
                                myOleDbCommand.Parameters.Add(myParam);

                                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                            } 
                            catch(Exception err)
                            {
                                Logger.WriteToLog(className +" : " + funcName + err.Message,true);
                                return false;
                            }
                        }
                        return true;

            */
        }

        public bool InsertSubsidiaries(int subsidiariesCIID, double ownership, int companyCIID) {
            string funcName = "InsertSubsidiaries(int subsidiariesCIID,double ownership, int companyCIID)";
            // if the board member is already registed for the company the he should be updated 
            //	if(ShareholderAlreadyRegisted(ownerCIID,companyCIID))
            //		return this.UpdateShareholderOwner(ownerCIID,ownershipID,ownership,companyCIID);
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_Subsidiaries(SubsidariesID,Ownership,CompanyCIID) VALUES(?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("SubsidariesID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = subsidiariesCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Ownership", OleDbType.Double);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = ownership;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        public bool InsertRealEstate(RealEstatesBLLC forReal, int companyCIID) {
            string funcName = "InsertRealEstate(RealEstatesBLLC forReal,int companyCIID)";
            // if the board member is already registed for the company the he should be updated 
            //	if(ShareholderAlreadyRegisted(ownerCIID,companyCIID))
            //		return this.UpdateShareholderOwner(ownerCIID,ownershipID,ownership,companyCIID);
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_RealEstates(CompanyCIID,RealEstatesLocationID,PostCode,Size,YearBuilt,RealEstatesTypeID,OwnerTypeID,AddressNative,AddressEN,InsuranceValue,CountryID,CityID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("RealEstatesLocationID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = forReal.Location;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("PostCode", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = forReal.AreaCode;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Size", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = forReal.Size;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("YearBuilt", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = forReal.YearBuilt;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("RealEstatesTypeID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = forReal.EstateTypeID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("OwnerTypeID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = forReal.Tenure;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "AddressNative",
                        "AddressEN",
                        ParameterDirection.Input,
                        forReal.AddressNative,
                        forReal.AddressEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("InsuranceValue", OleDbType.Double);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = forReal.InsuranceValue;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CountryID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = forReal.CountryID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CityID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = forReal.CityID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        public bool AddRealEstate(RealEstatesBLLC forReal, int companyCIID) {
            if (RealEstateAlreadyRegisted(forReal.RealEstateID, companyCIID)) {
                return UpdateRealEstate(forReal, companyCIID);
            } else {
                return InsertRealEstate(forReal, companyCIID);
            }
        }

        /// <summary>
        /// Takes instance of Principal and updates in the database
        /// </summary>
        /// <param name="thePrincipal">The principals instance</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if OK, false otherwise</returns>
        public bool UpdatePrincipal(PrincipalsBLLC thePrincipal, int companyCIID) {
            string funcName = "UpdatePrincipal(PrincipalsBLLC thePrincipal, int companyCIID)";
            // if the board member isn't already registed for the company the he should be inserted
            if (!PrincipalAlreadyRegisted(thePrincipal, companyCIID)) {
                return InsertPrincipal(thePrincipal, companyCIID);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "UPDATE cpi_CompanyPrincipals SET JobTitleID = " + thePrincipal.JobTitleID +
                                         ", EducationID = " + thePrincipal.EducationID + " WHERE PrincipalCIID = " +
                                         thePrincipal.CreditInfoID + " AND CompanyCIID = " + companyCIID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes instance of history and inserts/updates in the database
        /// </summary>
        /// <param name="CIID">The companies internal system id</param>
        /// <param name="theHistory">Instance of history</param>
        /// <returns>False if insert failes true otherwise </returns>
        public bool AddHistory(int CIID, HistoryBLLC theHistory) {
            string funcName = "AddHistory(int CIID,HistoryBLLC theHistory)";
            if (!HistoryAlreadyRegisted(theHistory, CIID)) {
                return InsertHistory(theHistory, CIID);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "UPDATE cpi_CompanyHistory SET HistoryNative = '" + theHistory.HistoryNative +
                                         "', HistoryEN = '" + theHistory.HistoryEN + "' WHERE CompanyCIID = " + CIID +
                                         " AND HistoryTypeID = " + theHistory.HistoryID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes instance of boardMember and updates in the database
        /// </summary>
        /// <param name="theBoardMember">The boardmember instance</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if OK, false otherwise</returns>
        public bool UpdateBoardMember(BoardMemberBLLC theBoardMember, int companyCIID) {
            string funcName = "UpdateBoardMember(BoardMemberBLLC theBoardMember, int companyCIID)";
            // if the board member isn't already registed for the company the he should be inserted
            if (!BoardMemberAlreadyRegisted(theBoardMember, companyCIID)) {
                return InsertBoardMember(theBoardMember, companyCIID);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "UPDATE cpi_BoardMembers SET ManagementPositionID = " +
                                         theBoardMember.ManagementPostionID + " WHERE BoardMemberCIID = " +
                                         theBoardMember.CreditInfoID + " AND CompanyCIID = " + companyCIID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Checks if a Principals for given company is already registed 
        /// </summary>
        /// <param name="thePrincipal">Instance of Principal</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if boardmember exists, false otherwise</returns>
        public bool PrincipalAlreadyRegisted(PrincipalsBLLC thePrincipal, int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_CompanyPrincipals WHERE cpi_CompanyPrincipals.CompanyCIID = " + companyCIID +
                        " AND cpi_CompanyPrincipals.PrincipalCIID = " + thePrincipal.CreditInfoID + "",
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if the company has already registed history in given history category
        /// </summary>
        /// <param name="theHistory">Instance of history</param>
        /// <param name="CIID">The companies internal system ID</param>
        /// <returns>True if registed, false otherwise</returns>
        public bool HistoryAlreadyRegisted(HistoryBLLC theHistory, int CIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_CompanyHistory WHERE  CompanyCIID = " + CIID + " AND HistoryTypeID = " +
                        theHistory.HistoryID + "",
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if a Board member in given company is already registed 
        /// </summary>
        /// <param name="theBoardMember">Instance of BoardMember</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if boardmember exists, false otherwise</returns>
        public bool BoardMemberAlreadyRegisted(BoardMemberBLLC theBoardMember, int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_BoardMembers WHERE cpi_BoardMembers.CompanyCIID = " + companyCIID +
                        " AND cpi_BoardMembers.BoardMemberCIID = " + theBoardMember.CreditInfoID + "",
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        public bool SubsidariesAlreadyRegisted(int subsidiaresID, int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_Subsidiaries WHERE SubsidariesID = " + subsidiaresID + " AND CompanyCIID = " +
                        companyCIID + "",
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if report for company has already been registed
        /// </summary>
        /// <param name="companyCIID">The companies internal system id</param>
        /// <returns>true if exists, false otherwise</returns>
        public bool CompanyReportAlreadyRegisted(int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_CompanyReport WHERE CompanyCIID = " + companyCIID + "", myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if RealEstate is already registed
        /// </summary>
        /// <param name="estateID">The estateID to search for</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if exists, false otherwise</returns>
        public bool RealEstateAlreadyRegisted(int estateID, int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_RealEstates WHERE CompanyCIID = " + companyCIID + " AND RealEstatesID = " +
                        estateID + "",
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if Status/Pro/eval has already been registed for given company
        /// </summary>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>True if exists, false otherwise</returns>
        public bool HistoryAlreadyExist(int companyCIID) {
            string funcName = "HistoryAlreadyExist(int companyCIID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM cpi_HistoryOperationReview WHERE CompanyCIID = " + companyCIID + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return true;
                    }
                    return false;
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
        }

        public bool ShareholderAlreadyRegisted(int shareholderCIID, int companyCIID, int ownershipID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                // athuga einnig hvort skr�� er � type einnig
                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_Owners WHERE cpi_Owners.CompanyCIID = " + companyCIID +
                        " AND cpi_Owners.OwnerCIID = " + shareholderCIID + " AND cpi_Owners.OwnershipID = " +
                        ownershipID + "",
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="shareholderCIID"></param>
        /// <param name="ownershipID"></param>
        /// <param name="ownership"></param>
        /// <param name="companyCIID"></param>
        /// <returns></returns>
        public bool UpdateShareholderOwner(
            int shareholderCIID, int ownershipID, int ownership, decimal ownershipPercentage, int companyCIID) {
            string funcName =
                "UpdateShareholderOwner(int shareholderCIID, int ownershipID, int ownership, int companyCIID)";
            // if the board member isn't already registed for the company the he should be inserted
            //string tmpQ = "UPDATE cpi_Owners SET Ownership = "+ ownership + ", OwnershipID = "+ownershipID+", Ownership_Perc = "+ownershipPercentage+" WHERE CompanyCIID = "+ companyCIID +" AND OwnerCIID = "+ shareholderCIID +"";
            // check if ownership has ,(comma) value. Switch out for .(period) before update
//			string strOwnership = Convert.ToString(ownership);
//			string manipOwnership = strOwnership.Replace(",",".");
            if (!ShareholderAlreadyRegisted(shareholderCIID, companyCIID, ownershipID)) {
                return InsertShareholderOwner(shareholderCIID, ownershipID, ownership, ownershipPercentage, companyCIID);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
//			myOleDbCommand.CommandText = "UPDATE cpi_Owners SET Ownership = "+ manipOwnership + " WHERE CompanyCIID = "+ companyCIID +" AND OwnerCIID = "+ shareholderCIID +"";
            myOleDbCommand.CommandText = "UPDATE cpi_Owners SET Ownership = " + ownership + ", OwnershipID = " +
                                         ownershipID + ", Ownership_Perc = " + ownershipPercentage +
                                         " WHERE CompanyCIID = " + companyCIID + " AND OwnerCIID = " + shareholderCIID +
                                         " AND OwnershipID = " + ownershipID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subsidiariesCIID"></param>
        /// <param name="ownership"></param>
        /// <param name="companyCIID"></param>
        /// <returns></returns>
        public bool UpdateSubsidiaries(int subsidiariesCIID, double ownership, int companyCIID) {
            string funcName = "UpdateSubsidiaries(int subsidiariesCIID,double ownership, int companyCIID)";
            // if the subsidiaries isn't already registed for the company the he should be inserted
            if (!SubsidariesAlreadyRegisted(subsidiariesCIID, companyCIID)) {
                return InsertSubsidiaries(subsidiariesCIID, ownership, companyCIID);
            }
            // check if ownership has ,(comma) value. Switch out for .(period) before update
            string strOwnership = Convert.ToString(ownership);
            string manipOwnership = strOwnership.Replace(",", ".");
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "UPDATE cpi_Subsidiaries SET Ownership = " + manipOwnership +
                                         " WHERE SubsidariesID = " + subsidiariesCIID + " AND CompanyCIID = " +
                                         companyCIID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theHOR"></param>
        /// <param name="companyCIID"></param>
        /// <returns></returns>
        public bool UpdateHistoryOperationReview(HistoryOperationReviewBLLC theHOR, int companyCIID) {
            string funcName = "UpdateHistoryOperationReview(HistoryOperationReviewBLLC theHOR,int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            // myOleDbCommand.CommandText = "UPDATE cpi_HistoryOperationReview SET HistoryNative = '"+ theHOR.HistoryNative + "',HistoryEN = '"+ theHOR.HistoryEN + "',OperationNative = '"+ theHOR.OperationNative + "',OperationEN = '"+ theHOR.OperationEN + "',CompanyReviewNative = '"+ theHOR.CompanyReviewNative + "',CompanyReviewEN = '"+ theHOR.CompanyReviewEN + "' WHERE CompanyCIID = "+ companyCIID +"";

            myOleDbCommand.CommandText =
                "UPDATE cpi_HistoryOperationReview SET HistoryNative = ?, HistoryEN = ?, OperationNative = ?, OperationEN = ?, CompanyReviewNative = ?, CompanyReviewEN = ?,BusinessRegistryTextNative = ?, BusinessRegistryTextEN = ?, StatusDescriptionNative = ?, StatusDescriptionEN = ? WHERE CompanyCIID = ?";
            // HistoryNative,HistoryEN,OperationNative,OperationEN,CompanyReviewNative,CompanyReviewEN,CompanyCIID
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam;

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        theHOR.HistoryNative,
                        theHOR.HistoryEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "OperationNative",
                        "OperationEN",
                        ParameterDirection.Input,
                        theHOR.OperationNative,
                        theHOR.OperationEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "CompanyReviewNative",
                        "CompanyReviewEN",
                        ParameterDirection.Input,
                        theHOR.CompanyReviewNative,
                        theHOR.CompanyReviewEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("BusinessRegistryTextNative", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHOR.BusinessRegistryTextNative;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("BusinessRegistryTextEN", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHOR.BusinessRegistryTextEN;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("StatusDescriptionNative", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHOR.StatusDescriptionNative;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("StatusDescriptionEN", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHOR.StatusDescriptionEN;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Value = companyCIID;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes instance of Board Member and updates matching item in database
        /// </summary>
        /// <param name="theBoardMember">Instance of BoardMemberBLLC</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdateBoardMembers(BoardMemberBLLC theBoardMember) { return false; }

        /// <summary>
        /// Takes comapanies system ID (CIID) and returns ArrayList of board members ID's
        /// </summary>
        /// <param name="companyCIID">The companies system ID (CIID)</param>
        /// <returns>ArrayList of board members ID's</returns>
        public ArrayList GetBoardMembers(int companyCIID) {
            /*	BoardMemberBLLC theBoardMember = new BoardMemberBLLC();
			ArrayList boardArr = new ArrayList();
			return boardArr;
		*/
            ArrayList arrBoard = new ArrayList();
            string funcName = "GetBordMembers(int companyCIID) ";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM cpi_BoardMembers WHERE CompanyCIID = " + companyCIID + "", myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    while (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            arrBoard.Add(reader.GetInt32(0));
                        }
                    }
                    reader.Close();
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
            }
            return arrBoard;
        }

        public HistoryBLLC GetHistory(int CIID, int historyTypeID) {
            HistoryBLLC theHistory = new HistoryBLLC();
            string funcName = "GetHistory(int CIID, int historyTypeID) ";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT HistoryTypeID,HistoryNative,HistoryEN FROM cpi_CompanyHistory WHERE CompanyCIID = " +
                            CIID + " AND HistoryTypeID = " + historyTypeID + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            theHistory.HistoryID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theHistory.HistoryNative = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theHistory.HistoryEN = reader.GetString(2);
                        }
                    }
                    reader.Close();
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
            }
            return theHistory;
        }

        /// <summary>
        /// Takes companies system ID (CIID) and returns board members as DataSet
        /// </summary>
        /// <param name="companyCIID">The companies system ID</param>
        /// <param name="board">true is board, false is principal</param>
        /// <returns>Board members as DataSet</returns>
        public DataSet GetBoardPrincipalsAsDataSet(int companyCIID, bool board) {
            DataSet dsBoardMembers = new DataSet();
            ArrayList arrBoardPrincipal = new ArrayList();
            if (board) {
                arrBoardPrincipal = GetBoardMembers(companyCIID);
            } else {
                arrBoardPrincipal = GetPrincipals(companyCIID);
            }
            string funcName = "GetBoardMembersAsDataSet(int companyCIID) ";

            if (arrBoardPrincipal.Count == 0) {
                return null;
            }

            
            string query =
                "SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number " +
                "FROM np_Individual LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID";
            //WHERE np_IDNumbers.NumberTypeId =  1 AND np_Individual.CreditInfoID = ";

            for (int i = 0; i < arrBoardPrincipal.Count; i++) {
                if (i == 0) {
                    query += " WHERE np_Individual.CreditInfoID = ";
                } else {
                    query += " OR np_Individual.CreditInfoID = ";
                }
                query += (int) arrBoardPrincipal[i];
            }

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsBoardMembers);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
            return dsBoardMembers;
        }

        /// <summary>
        /// Takes companies system ID (CIID) and returns principals as DataSet
        /// </summary>
        /// <param name="companyCIID">The companies system ID</param>
        /// <returns>Principals as DataSet</returns>
        public DataSet GetDetailedPrincipalsAsDataSet(int companyCIID) {
            DataSet dsDetailedPrincipals = new DataSet();
            string funcName = "GetDetailedPrincipalsAsDataSet(int companyCIID) ";

            
            /*	
			 * SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, np_Individual.SurNameNative, np_Individual.SurNameEN, np_Address.StreetNative, np_Address.StreetEN,np_IDNumbers.Number,cpi_ManagementPositions.TitleNative,cpi_ManagementPositions.TitleEN FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_BoardMembers ON np_Individual.CreditInfoID = cpi_BoardMembers.BoardMemberCIID LEFT OUTER JOIN cpi_ManagementPositions ON cpi_BoardMembers.ManagementPositionID = cpi_ManagementPositions.ManagementPositionID  WHERE np_Individual.CreditInfoID IN (SELECT cpi_BoardMembers.BoardMemberCIID FROM cpi_BoardMembers WHERE cpi_BoardMembers.CompanyCIID = 1) AND np_IDNumbers.NumberTypeID = 1
			*/
            string query =
                "SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number,cpi_JobTitle.NameNative,cpi_JobTitle.NameEN,cpi_JobTitle.JobTitleID,np_Address.StreetNative, np_Address.StreetEN,np_Education.NameNative AS EdNameNative,np_Education.NameEN AS EdNameEN,np_Education.EducationID  " +
                "FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID " +
                "LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_CompanyPrincipals ON np_Individual.CreditInfoID = cpi_CompanyPrincipals.PrincipalCIID " +
                "LEFT OUTER JOIN cpi_JobTitle ON cpi_CompanyPrincipals.JobTitleID = cpi_JobTitle.JobTitleID  " +
                "LEFT OUTER JOIN np_Education ON cpi_CompanyPrincipals.EducationID = np_Education.EducationID  " +
                "WHERE np_Individual.CreditInfoID IN (SELECT cpi_CompanyPrincipals.PrincipalCIID FROM cpi_CompanyPrincipals WHERE cpi_CompanyPrincipals.CompanyCIID = " +
                companyCIID + ") AND cpi_CompanyPrincipals.CompanyCIID = " + companyCIID + " ";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsDetailedPrincipals);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsDetailedPrincipals;
        }

        /// <summary>
        /// Searches for information about individual with given name, or part of name, in the National Registry
        /// </summary>
        /// <param name="name">The name, or part of name, to search for</param>
        /// <returns>Informations about all individual with given name</returns>
        public DataSet GetIndividualFromNationalInterfaceAsDataSet(string name) {
            DataSet ds = new DataSet();
            string funcName = "GetIndividualFromNationalInterfaceAsDataSet(string name) ";

            
            string query =
                "SELECT TOP 100 Name, IDNumber, Address, PostalCode from cpi_NationalInterfaceView where name like '" +
                name + "%' order by Name, Address";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(ds);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return ds;
        }

        /// <summary>
        /// Searches for informations about given company in the National Company Registry
        /// </summary>
        /// <param name="name">The name, or part of name, of the company to search for</param>
        /// <returns>Informations about all companies that match the name, or part of name, given</returns>
        public DataSet GetCompanyFromNationalInterfaceAsDataSet(string name) {
            DataSet ds = new DataSet();
            string funcName = "GetCompanyFromNationalInterfaceAsDataSet(string name) ";

            
            string query =
                "SELECT TOP 100 Name, IDNumber, Address, PostalCode from cpi_NationalCompanyInterfaceView where name like '" +
                name + "%' order by Name, Address";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(ds);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return ds;
        }

        /// <summary>
        /// Gets information about given company ssn (National id) from National Company Registry
        /// </summary>
        /// <param name="CompanySSN">The company SSN (National ID) to look for</param>
        /// <returns>Informations about the company as dataset</returns>
        public DataSet GetCompanyBySSNFromNationalInterfaceAsDataSet(string CompanySSN) {
            DataSet ds = new DataSet();
            string funcName = "GetCompanyBySSNFromNationalInterfaceAsDataSet(string CompanySSN) ";

            
            string query = "SELECT * from cpi_NationalCompanyInterfaceView where IDNumber = '" + CompanySSN + "'";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(ds);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return ds;
        }

        /// <summary>
        /// Searches for company board informations in the National Company Registry
        /// </summary>
        /// <param name="companySSN">The company SSN (National ID)</param>
        /// <returns>Information about the board of the company as DataSet</returns>
        public DataSet GetCompanyBoardFromCompanyRegistryAsDataSet(string companySSN) {
            DataSet ds = new DataSet();
            string funcName = "GetCompanyBoardFromCompanyRegistryAsDataSet(string companySSN) ";

            
            string query = "SELECT * from cpi_CompanyRegistryBoardView_new where CompanySSN = '" + companySSN +
                           "' order by Name";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(ds);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return ds;
        }

        public DataSet GetCompanyShareholdersFromCompanyRegistryAsDataSet(string companySSN) {
            DataSet ds = new DataSet();
            string funcName = "GetCompanyShareholdersFromCompanyRegistryAsDataSet(string companySSN) ";

            

            string query = "SELECT * from cpi_CompanyRegistryShareholdersIndividualView where CompanySSN = '" +
                           companySSN +
                           "' UNION SELECT * from cpi_CompanyRegistryShareholdersCompanyView where CompanySSN = '" +
                           companySSN + "'";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(ds);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return ds;
        }

        public DataSet GetCompanyBoardSecretaryFromCompanyRegistryAsDataSet(string companySSN) {
            DataSet ds = new DataSet();
            string funcName = "GetCompanyBoardSecretaryFromCompanyRegistryAsDataSet(string companySSN) ";

            

            string query = "SELECT * from cpi_CompanyRegistryBoardSecretaryIndividualView where CompanySSN = '" +
                           companySSN +
                           "' UNION SELECT * from cpi_CompanyRegistryBoardSecretaryCompanyView where CompanySSN = '" +
                           companySSN + "'";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(ds);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return ds;
        }

        public DataSet GetCapitalFromCompanyRegistryAsDataSet(string companySSN) {
            DataSet ds = new DataSet();
            string funcName = "GetCapitalFromCompanyRegistryAsDataSet(string companySSN) ";

            

            string query = "SELECT * from cpi_CompanyRegistryCapitalView where CompanySSN = '" + companySSN + "'";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(ds);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return ds;
        }

        public DataSet GetSubsidiariesFromCompanyRegistryAsDataSet(string companySSN) {
            DataSet ds = new DataSet();
            string funcName = "GetCapitalFromCompanyRegistryAsDataSet(string companySSN) ";

            

            string query = "SELECT * from cpi_CompanyRegistrySubsidiariesView where IP_Co_Reg_No = '" + companySSN + "'";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(ds);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return ds;
        }

        /// <summary>
        /// Takes companies system ID (CIID) and returns detailed info about board members as DataSet
        /// </summary>
        /// <param name="companyCIID">The companies internal system number</param>
        /// <returns>Detailed board members info as DataSet </returns>
        public DataSet GetDetailedBoardAsDataSet(int companyCIID) {
            DataSet dsDetailedBoardMembers = new DataSet();
            string funcName = "GetDetailedBoardPrincipalsAsDataSet(int companyCIID) ";

            
            /*	
		 * SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, np_Individual.SurNameNative, np_Individual.SurNameEN, np_Address.StreetNative, np_Address.StreetEN,np_IDNumbers.Number,cpi_ManagementPositions.TitleNative,cpi_ManagementPositions.TitleEN FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_BoardMembers ON np_Individual.CreditInfoID = cpi_BoardMembers.BoardMemberCIID LEFT OUTER JOIN cpi_ManagementPositions ON cpi_BoardMembers.ManagementPositionID = cpi_ManagementPositions.ManagementPositionID  WHERE np_Individual.CreditInfoID IN (SELECT cpi_BoardMembers.BoardMemberCIID FROM cpi_BoardMembers WHERE cpi_BoardMembers.CompanyCIID = 1) AND np_IDNumbers.NumberTypeID = 1
		*/

            /*
			string query = "SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
				"np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number,cpi_ManagementPositions.TitleNative,cpi_ManagementPositions.TitleEN,cpi_ManagementPositions.ManagementPositionID,np_Address.StreetNative, np_Address.StreetEN " +
				"FROM np_Individual " +
				"LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID " +
				"LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID " +
				"LEFT OUTER JOIN cpi_BoardMembers ON np_Individual.CreditInfoID = cpi_BoardMembers.BoardMemberCIID " +
				"LEFT OUTER JOIN cpi_ManagementPositions ON cpi_BoardMembers.ManagementPositionID = cpi_ManagementPositions.ManagementPositionID  " +
				"WHERE np_Individual.CreditInfoID IN (SELECT cpi_BoardMembers.BoardMemberCIID FROM cpi_BoardMembers WHERE cpi_BoardMembers.CompanyCIID = "+ companyCIID +") AND cpi_BoardMembers.CompanyCIID = "+ companyCIID +" ";
				
			*/
            string query = "SELECT " +
                           "COALESCE(np_Companys.CreditInfoID,np_Individual.CreditInfoID) AS CreditInfoID, " +
                           "np_IDNumbers.Number, " +
                           "COALESCE(np_Companys.NameNative, np_Individual.FirstNameNative) AS FirstNameNative, " +
                           "np_Individual.SurNameNative, " +
                           "COALESCE(np_Companys.NameEN, np_Individual.FirstNameEN) AS FirstNameEN, " +
                           "np_Individual.SurNameEN, " +
                           "cpi_ManagementPositions.TitleNative, " +
                           "cpi_ManagementPositions.TitleEN, " +
                           "cpi_ManagementPositions.ManagementPositionID, " +
                           "np_Address.StreetNative, " +
                           "CASE " +
                           "WHEN np_Companys.CreditInfoID IS NULL THEN 'false' " +
                           "ELSE 'true' " +
                           "END AS isCompany " +
                           "FROM cpi_BoardMembers " +
                           "LEFT OUTER JOIN np_Individual ON cpi_BoardMembers.BoardMemberCIID= np_Individual.CreditInfoID " +
                           "LEFT OUTER JOIN np_Companys ON cpi_BoardMembers.BoardMemberCIID = np_Companys.CreditInfoID " +
                           "LEFT OUTER JOIN np_Address ON cpi_BoardMembers.BoardMemberCIID = np_Address.CreditInfoID " +
                           "LEFT OUTER JOIN np_IDNumbers ON cpi_BoardMembers.BoardMemberCIID = np_IDNumbers.CreditInfoID " +
                           "LEFT OUTER JOIN cpi_ManagementPositions ON cpi_BoardMembers.ManagementPositionID = cpi_ManagementPositions.ManagementPositionID " +
                           "WHERE cpi_BoardMembers.CompanyCIID = " + companyCIID;
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsDetailedBoardMembers);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsDetailedBoardMembers;
        }

        /// <summary>
        /// Deletes companies board members
        /// </summary>
        /// <param name="boardMemberCIID">The board members internal system number</param>
        /// <param name="companyCIID">The companies internal system number</param>
        /// <returns>True if sucessfull, false otherwise</returns>
        public bool DeleteBoardMember(int boardMemberCIID, int companyCIID) {
            string funcName = "DeleteBoardMember(int boardMemberCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_BoardMembers WHERE BoardMemberCIID = " + boardMemberCIID +
                                         " AND CompanyCIID = " + companyCIID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Deletes company customer types
        /// </summary>
        /// <param name="companyCIID">The companies internal system number</param>
        /// <param name="myOleDbCommand">The command to use</param>
        /// <returns>True if successfull, false otherwise</returns>
        public bool DeleteCompanyCustomerType(int companyCIID, OleDbCommand myOleDbCommand) {
            string funcName = "DeleteCompanyCustomerType(int companyCIID,OleDbConnection myOleDbConn)";
            myOleDbCommand.CommandText = "DELETE FROM cpi_CompaniesCustomerType WHERE CompanyCIID = " + companyCIID + "";

            try {
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes company banks
        /// </summary>
        /// <param name="companyCIID">The companies internal system number</param>
        /// <param name="myOleDbCommand">The command to use</param>
        /// <returns>True if successfull, false otherwise</returns>
        public bool DeleteCompanyBanks(int companyCIID, OleDbCommand myOleDbCommand) {
            string funcName = "DeleteCompanyBanks(int companyCIID,OleDbConnection myOleDbConn)";
            myOleDbCommand.CommandText = "DELETE FROM cpi_CompanyBanks WHERE CompanyCIID = " + companyCIID + "";

            try {
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        public bool DeleteCompanyTradeTerms(int companyCIID, OleDbCommand myOleDbCommand, bool sales) {
            string funcName = "DeleteCompanyTradeTerms(int companyCIID,OleDbCommand myOleDbCommand,bool sales)";
            myOleDbCommand.CommandText = "DELETE FROM cpi_CompanyTradeTerms WHERE CompanyCIID = " + companyCIID +
                                         " AND SalesTerm = '" + sales + "'";

            try {
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes company nace code
        /// </summary>
        /// <param name="companyCIID">The company internal system number</param>
        /// <param name="myOleDbCommand">The command to use</param>
        /// <param name="current">wether to delete current or former nacecodes</param>
        /// <returns>True if successfull, false otherwise</returns>
        public bool DeleteCompanyOperaton(int companyCIID, OleDbCommand myOleDbCommand, bool current) {
            string funcName = "DeleteCompanyOperaton(int companyCIID,OleDbConnection myOleDbConn)";
            myOleDbCommand.CommandText = "DELETE FROM cpi_CompaniesNaceCodes WHERE CompanyCIID = " + companyCIID +
                                         " AND CurrentCode = '" + current + "'";

            try {
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes all registed companies export countries
        /// </summary>
        /// <param name="companyCIID">The companies internal system id</param>
        /// <param name="myOleDbCommand">The OledbCommand to use</param>
        /// <returns>true of successfull, false otherwise</returns>
        public bool DeleteExportCountrie(int companyCIID, OleDbCommand myOleDbCommand) {
            string funcName = "DeleteExportCountrie(int companyCIID,OleDbConnection myOleDbConn)";
            myOleDbCommand.CommandText = "DELETE FROM cpi_ExportCountries WHERE CompanyCIID = " + companyCIID + "";

            try {
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes all registed companies import countries
        /// </summary>
        /// <param name="companyCIID">The companies internal system id</param>
        /// <param name="myOleDbCommand">The OledbCommand to use</param>
        /// <returns>true of successfull, false otherwise</returns>
        public bool DeleteImportCountrie(int companyCIID, OleDbCommand myOleDbCommand) {
            string funcName = "DeleteImportCountrie(int companyCIID,OleDbConnection myOleDbConn)";
            myOleDbCommand.CommandText = "DELETE FROM cpi_ImportCountries WHERE CompanyCIID = " + companyCIID + "";

            try {
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Takes an instance of StaffCount and adds to the database
        /// </summary>
        /// <param name="theStaffCount">Instance of StaffCountBLLC</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddStaffCount(StaffCountBLLC theStaffCount) { return false; }

        /// <summary>
        /// Takes an instance of StaffCount and updates the database
        /// </summary>
        /// <param name="theStaffCount">Instance of StaffCountBLLC</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdateStaffCount(StaffCountBLLC theStaffCount) { return false; }

        /// <summary>
        /// Takes StaffCount ID and deletes matching items from database
        /// </summary>
        ///	<param name="companyCIID">The companies internal system id</param>
        ///	<param name="year">The year to delete</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool DeleteStaffCountYear(string year, int companyCIID) {
            string funcName = "DeleteStaffCountYear(int year,int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_StaffCount WHERE CompanyCIID = " + companyCIID +
                                         " AND Year = '" + year + "'";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes Companies system ID (CIID) and returns ArrayList of StaffCount instances
        /// </summary>
        /// <param name="companyCIID">The Companies system ID</param>
        /// <returns>ArrayList of StaffCount instances</returns>
        public ArrayList GetStaffCount(int companyCIID) {
            StaffCountBLLC theStaffCount = new StaffCountBLLC();
            ArrayList staffArr = new ArrayList();
            return staffArr;
        }

        /// <summary>
        /// Takes an instance of Subsidares and adds to the database 
        /// </summary>
        /// <param name="theSubsidaries">Instance of SubsidaresBLLC</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddSubsidiaries(SubsidiariesBLLC theSubsidiaries, int companyCIID)
        {
            string funcName = "AddSubsidaries(SubsidariesBLLC theSubsidaries)";
            bool result = false;
            int subsidiariesCIID = GetCIIDByNationalID(theSubsidiaries.NationalID);
            CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
            Company theComp = new Company();
            theComp.CreditInfoID = subsidiariesCIID;
            theComp.NameNative = theSubsidiaries.NameNative;
            theComp.NameEN = theSubsidiaries.NameEN;
            theComp.Org_status_code = 1; // meaning not accessible
            IDNumber theIDNumber = new IDNumber();
            theIDNumber.NumberTypeID = 1;
            theIDNumber.Number = theSubsidiaries.NationalID;
            theComp.IDNumbers = new ArrayList();
            theComp.IDNumbers.Add(theIDNumber);

            theComp.Address = theSubsidiaries.Address;

            theComp.Number = new ArrayList();
            theComp.Type = CigConfig.Configure("lookupsettings.companyID");
            try
            {
                if (subsidiariesCIID < 0)
                {
                    subsidiariesCIID = theUserDAL.AddCompany(theComp);
                    //	theShareholder.CreditInfoID = shareholderCIID;
                    //	result = InsertBoardMember(theBoardMember,companyCIID);
                    result = InsertSubsidiaries(subsidiariesCIID, theSubsidiaries.Ownership, companyCIID);
                }
                else // CIID exists then update the user...
                {
                    // not such a good idea to update the user ... lets focus on subsidiaries
                    /*	theComp.IDNumbers = new ArrayList(); // prevent any ID numbers update
					theComp.Address = new ArrayList(); // prevent any address updates
					subsidiariesCIID = theUserDAL.UpdateCompany(theComp);
					if(subsidiariesCIID > 0)
						theSubsidiaries.CreditInfoID = subsidiariesCIID;
				*/
                    result = UpdateSubsidiaries(subsidiariesCIID, theSubsidiaries.Ownership, companyCIID);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + ":" + funcName, err, true);
            }
            return result;
        }

        /// <summary>
        /// Takes subsidaries ID and deletes matching item from database 
        /// </summary>
        /// <param name="subsidariesID">The subsidaries ID</param>
        /// <param name="companyCIID">The companies internal system ID</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool DeleteSubsidaries(int subsidariesID, int companyCIID) {
            string funcName = "DeleteSubsidaries(int subsidariesID,int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_Subsidiaries WHERE SubsidariesID = " + subsidariesID +
                                         " AND CompanyCIID = " + companyCIID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes Companies system ID (CIID) and returns ArrayList of Subsidares instances
        /// </summary>
        /// <param name="companyCIID">The Companies system ID (CIID)</param>
        /// <returns>ArrayList of Subsidares</returns>
        public ArrayList GetSubsidaries(int companyCIID) {
            SubsidiariesBLLC theSubsidiaries = new SubsidiariesBLLC();
            ArrayList subsidariesArr = new ArrayList();
            return subsidariesArr;
        }

        /// <summary>
        /// Gets the companies subsidiares as DataSet
        /// </summary>
        /// <param name="companyCIID"></param>
        /// <returns>Subsidiares as DataSet</returns>
        public DataSet GetSubsidiariesAsDataSet(int companyCIID) {
            DataSet dsSubsidiaries = new DataSet();
            string funcName = "GetSubsidiariesAsDataSet(int companyCIID) ";
            // H�r �arf a� n� � National ID usersins en ekki bara creditinfoID
            
            string query =
                "SELECT DISTINCT np_Companys.CreditInfoID, np_Companys.NameNative, np_Companys.NameEN,cpi_Subsidiaries.Ownership,np_IDNumbers.Number " +
                "FROM np_Companys LEFT OUTER JOIN cpi_Subsidiaries ON cpi_Subsidiaries.SubsidariesID = np_Companys.CreditInfoID " +
                "LEFT OUTER JOIN np_IDNumbers ON cpi_Subsidiaries.SubsidariesID = np_IDNumbers.CreditInfoID " +
                "WHERE cpi_Subsidiaries.CompanyCIID = " + companyCIID + " ORDER BY cpi_Subsidiaries.Ownership DESC";
            //AND np_IDNumbers.NumberTypeID = 1
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsSubsidiaries);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsSubsidiaries;
        }

        /// <summary>
        /// Takes instance of HistoryOperationReview and adds to database
        /// </summary>
        /// <param name="theHOR">Instance of HistoryOperationReviewBLLC</param>
        /// <param name="companyCIID">Companies internal system ID</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddHistoryOperationReview(HistoryOperationReviewBLLC theHOR, int companyCIID) {
            string funcName = "AddHistoryOperationReview(HistoryOperationReviewBLLC theHOR)";
            // athuga hvort n� �egar s� til History � �etta
            if (HistoryAlreadyExist(companyCIID)) {
                return UpdateHistoryOperationReview(theHOR, companyCIID);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_HistoryOperationReview(HistoryNative,HistoryEN,OperationNative,OperationEN,CompanyReviewNative,CompanyReviewEN,BusinessRegistryTextNative, BusinessRegistryTextEN, StatusDescriptionNative, StatusDescriptionEN, CompanyCIID) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            // cpi_HistoryOperationReview.HistoryNative,cpi_HistoryOperationReview.HistoryEN, cpi_HistoryOperationReview.OperationNative,cpi_HistoryOperationReview.OperationEN, cpi_HistoryOperationReview.CompanyReviewNative,cpi_HistoryOperationReview.CompanyReviewEN FROM cpi_HistoryOperationReview WHERE cpi_HistoryOperationReview.CompanyCIID
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        theHOR.HistoryNative,
                        theHOR.HistoryEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "OperationNative",
                        "OperationEN",
                        ParameterDirection.Input,
                        theHOR.OperationNative,
                        theHOR.OperationEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "CompanyReviewNative",
                        "CompanyReviewEN",
                        ParameterDirection.Input,
                        theHOR.CompanyReviewNative,
                        theHOR.CompanyReviewEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("BusinessRegistryTextNative", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHOR.BusinessRegistryTextNative;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("BusinessRegistryTextEN", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHOR.BusinessRegistryTextEN;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("StatusDescriptionNative", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHOR.StatusDescriptionNative;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("StatusDescriptionEN", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = theHOR.StatusDescriptionEN;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes instance of HistoryOperationReview and updates in the database
        /// </summary>
        /// <param name="theHOR">Instance of HistoryOperationReviewBLLC</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdateHistoryOperationReview(HistoryOperationReviewBLLC theHOR) { return false; }

        /// <summary>
        /// Takes HistoryOperationReview ID and deletes from database
        /// </summary>
        /// <param name="horID">The HistoryOperationReview ID</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool DeleteHistoryOperationReview(int horID) { return false; }

        /// <summary>
        /// Takes Companies system ID (CIID) and returns instance of HistoryOperationReview
        /// </summary>
        /// <param name="companyCIID">The Companies system ID (CIID)</param>
        /// <returns>Instance of HistoryOperationReview instances</returns>
        public HistoryOperationReviewBLLC GetHistoryOperationReview(int companyCIID) {
            HistoryOperationReviewBLLC theReview = new HistoryOperationReviewBLLC();
            string funcName = "HistoryOperationReviewBLLC GetStatusProEval(int companyCIID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    //	OleDbCommand myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT cpi_HistoryOperationReview.HistoryNative,cpi_HistoryOperationReview.HistoryEN, cpi_HistoryOperationReview.OperationNative,cpi_HistoryOperationReview.OperationEN, cpi_HistoryOperationReview.CompanyReviewNative,cpi_HistoryOperationReview.CompanyReviewEN,BusinessRegistryTextNative,BusinessRegistryTextEN,StatusDescriptionNative, StatusDescriptionEN FROM cpi_HistoryOperationReview WHERE cpi_HistoryOperationReview.CompanyCIID = " +
                            companyCIID + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            theReview.HistoryNative = reader.GetString(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theReview.HistoryEN = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theReview.OperationNative = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            theReview.OperationEN = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            theReview.CompanyReviewNative = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            theReview.CompanyReviewEN = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            theReview.BusinessRegistryTextNative = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            theReview.BusinessRegistryTextEN = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            theReview.StatusDescriptionNative = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            theReview.StatusDescriptionEN = reader.GetString(9);
                        }

                        theReview.Exists = true;
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
            }
            return theReview;
        }

        /// <summary>
        /// Takes instance of RealEstate and adds to the database
        /// </summary>
        /// <param name="theRealEstate"></param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddRealEstate(RealEstatesBLLC theRealEstate) { return false; }

        /// <summary>
        /// Takes and instance of RealEstate and updates maching item in database
        /// </summary>
        /// <param name="theRealEstate">Instance of RealEstate</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool UpdateRealEstate(RealEstatesBLLC theRealEstate, int companyCIID) {
            string funcName = "UpdateRealEstate(RealEstatesBLLC theRealEstate, int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            /*
			myOleDbCommand.CommandText = "UPDATE cpi_RealEstates SET RealEstatesLocationID = "+ theRealEstate.Location + " ," +
				"PostCode = "+ theRealEstate.AreaCode +", Size = "+ theRealEstate.Size +",YearBuilt = "+ theRealEstate.YearBuilt +",RealEstatesTypeID = "+ theRealEstate.EstateTypeID +","+
				"OwnerTypeID = "+ theRealEstate.Tenure +",AddressNative = '"+ theRealEstate.AddressNative +"',AddressEN = '"+ theRealEstate.AddressEN +"', InsuranceValue = "+ theRealEstate.InsuranceValue + ", CountryID = " + theRealEstate.CountryID + ", CityID = " + theRealEstate.CityID +" " +
				"WHERE CompanyCIID = "+ companyCIID +" AND RealEstatesID = "+ theRealEstate.RealEstateID +"";
			*/

            myOleDbCommand.CommandText =
                "UPDATE cpi_RealEstates SET RealEstatesLocationID = ?, PostCode = ?, Size = ?, YearBuilt = ?, RealEstatesTypeID = ? , OwnerTypeID = ?, AddressNative = ?, AddressEN = ?, InsuranceValue = ?, CountryID = ?, CityID = ? WHERE CompanyCIID = ? AND RealEstatesID = ?";

            // CompanyCIID,RealEstatesLocationID,PostCode,Size,YearBuilt,RealEstatesTypeID,OwnerTypeID,AddressNative,AddressEN
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam;

                    myParam = new OleDbParameter("RealEstatesLocationID", OleDbType.Integer);
                    myParam.Value = theRealEstate.Location;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("PostCode", OleDbType.WChar);
                    myParam.Value = theRealEstate.AreaCode;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Size", OleDbType.VarChar);
                    myParam.Value = theRealEstate.Size;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("YearBuilt", OleDbType.WChar);
                    myParam.Value = theRealEstate.YearBuilt;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("RealEstatesTypeID", OleDbType.Integer);
                    myParam.Value = theRealEstate.EstateTypeID;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("OwnerTypeID", OleDbType.Integer);
                    myParam.Value = theRealEstate.Tenure;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myOleDbCommand,
                        "AddressNative",
                        "AddressEN",
                        ParameterDirection.Input,
                        theRealEstate.AddressNative,
                        theRealEstate.AddressEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("InsuranceValue", OleDbType.Double);
                    myParam.Value = theRealEstate.InsuranceValue;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CountryID", OleDbType.Integer);
                    myParam.Value = theRealEstate.CountryID;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CityID", OleDbType.Integer);
                    myParam.Value = theRealEstate.CityID;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Value = companyCIID;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("RealEstatesID", OleDbType.Integer);
                    myParam.Value = theRealEstate.RealEstateID;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes an RealEstate ID and deletes matching item from database
        /// </summary>
        /// <param name="realEstateID">The RealEstate's ID</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool DeleteRealEstate(int realEstateID, int companyCIID) {
            string funcName = "DeleteRealEstate(int realEstateID, int companyCIID) ";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_RealEstates WHERE RealEstatesID = " + realEstateID +
                                         " AND CompanyCIID = " + companyCIID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        public BoardSecretaryBLLC GetBoardSecretary(int companyCIID) {
            string funcName = "GetBoardSecretary(int companyCIID)";
            if (companyCIID > 0) {
                
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    try {
                        myOleDbConn.Open();
                        //	OleDbCommand myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                        // SELECT np_CreditInfoUser.Email, np_Companys.NameNative,np_Companys.NameEN,np_Companys.Established,np_Companys.LastContacted,np_Companys.LastUpdate,np_IDNumbers.Number,CompanyFormerNameNative,CompanyFormerNameEN,CompanyTradeNameNative,CompanyTradeNameEN,RegistrationInfoNative,RegistrationInfoEN,CommentI,CommentII,Vat,CustomerCount,ReportsAuthorCIID,CustomerComments,ImportComments,ExportComments,PaymentComments,SalesComments,cpi_CompanyReport.StatusID FROM np_CreditInfoUser LEFT OUTER JOIN cpi_CompanyReport ON np_CreditInfoUser.CreditInfoID = cpi_CompanyReport.CompanyCIID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE CompanyCIID = 22 AND np_IDNumbers.NumberTypeID = 1

                        string query =
                            "SELECT DISTINCT np_CreditInfoUser.CreditInfoID, np_CreditInfoUser.Type, np_Companys.NameNative,np_Companys.NameEN, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                            "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number, cpi_CompanyBoardSecretary.HistoryNative, cpi_CompanyBoardSecretary.HistoryEN, np_Address.StreetNative, np_Address.StreetEN " +
                            "FROM np_CreditInfoUser LEFT OUTER JOIN np_Individual ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Address ON np_CreditInfoUser.CreditInfoID = np_Address.CreditInfoID " +
                            "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_CompanyBoardSecretary ON np_CreditInfoUser.CreditInfoID = cpi_CompanyBoardSecretary.SecretaryCIID " +
                            "WHERE np_CreditInfoUser.CreditInfoID IN (SELECT cpi_CompanyBoardSecretary.SecretaryCIID FROM cpi_CompanyBoardSecretary WHERE cpi_CompanyBoardSecretary.CompanyCIID = " +
                            companyCIID + ") AND cpi_CompanyBoardSecretary.CompanyCIID = " + companyCIID + "";
                            // AND np_IDNumbers.NumberTypeID = 1";

                        //	OleDbCommand myOleDbCommand = new OleDbCommand("SELECT SecretaryCIID, CompanyCIID, HistoryNative, HistoryEN from cpi_CompanyBoardSecretary WHERE CompanyCIID = "+companyCIID,myOleDbConn);
                        OleDbCommand myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                        OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                        if (reader.Read()) {
                            BoardSecretaryBLLC theSecretary = new BoardSecretaryBLLC();
                            if (!reader.IsDBNull(0)) {
                                theSecretary.CreditInfoID = reader.GetInt32(0);
                            }
                            if (!reader.IsDBNull(9)) {
                                theSecretary.HistoryNative = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10)) {
                                theSecretary.HistoryEN = reader.GetString(10);
                            }
                            //Now we need customer informations - we can get them from useradmin
                            if (theSecretary.CreditInfoID > 0) {
                                uaFactory uaFact = new uaFactory();
                                Indivitual indi = uaFact.GetIndivitual(theSecretary.CreditInfoID);

                                if (indi != null) {
                                    try {
                                        int nationalID =
                                            int.Parse(CigConfig.Configure("lookupsettings.nationalID").Trim());

                                        foreach (IDNumber IDNumber in indi.IDNumbers) {
                                            if (IDNumber.NumberTypeID == nationalID) {
                                                theSecretary.NationalID = IDNumber.Number;
                                            }
                                        }
                                    } catch (Exception) {}

                                    theSecretary.FirstNameNative = indi.FirstNameNative;
                                    theSecretary.FirstNameEN = indi.FirstNameEN;
                                    theSecretary.SurNameNative = indi.SurNameNative;
                                    theSecretary.SurNameEN = indi.SurNameEN;
                                    theSecretary.Address = indi.Address;
                                }
                            }
                            return theSecretary;
                        }
                    } catch (Exception e) {
                        Logger.WriteToLog(className + " : " + funcName, e, true);
                    }
                }
            }
            return null;
        }

        public BoardSecretaryCompanyBLLC GetCompanyBoardSecretary(int companyCIID) {
            string funcName = "GetCompanyBoardSecretary(int companyCIID)";
            if (companyCIID > 0) {
                
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    try {
                        myOleDbConn.Open();
                        //	OleDbCommand myOleDbCommand = new OleDbCommand("SELECT cre.CreditInfoID, cre.Type, ind.FirstNameNative, ind.FirstNameEN, ind.SurNameNative, ind.SurNameEN, prof.NameNative, prof.NameEN, prof.DescriptionEN, prof.DescriptionNative, edu.NameEN, edu.NameNative, edu.DescriptionEN, edu.DescriptionNative  FROM np_Individual ind, np_Profession prof, np_Education edu , np_CreditInfoUser cre WHERE cre.CreditInfoID = ind.CreditInfoID AND ind.ProfessionID = prof.ProfessionID AND ind.EducationID = edu.EducationID AND cre.CreditInfoID ="+ creditInfoID +"",myOleDbConn);
                        // SELECT np_CreditInfoUser.Email, np_Companys.NameNative,np_Companys.NameEN,np_Companys.Established,np_Companys.LastContacted,np_Companys.LastUpdate,np_IDNumbers.Number,CompanyFormerNameNative,CompanyFormerNameEN,CompanyTradeNameNative,CompanyTradeNameEN,RegistrationInfoNative,RegistrationInfoEN,CommentI,CommentII,Vat,CustomerCount,ReportsAuthorCIID,CustomerComments,ImportComments,ExportComments,PaymentComments,SalesComments,cpi_CompanyReport.StatusID FROM np_CreditInfoUser LEFT OUTER JOIN cpi_CompanyReport ON np_CreditInfoUser.CreditInfoID = cpi_CompanyReport.CompanyCIID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID WHERE CompanyCIID = 22 AND np_IDNumbers.NumberTypeID = 1

                        string query =
                            "SELECT DISTINCT np_CreditInfoUser.CreditInfoID, np_CreditInfoUser.Type, np_Companys.NameNative,np_Companys.NameEN, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                            "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number, cpi_CompanyBoardSecretary.HistoryNative, cpi_CompanyBoardSecretary.HistoryEN, np_Address.StreetNative, np_Address.StreetEN " +
                            "FROM np_CreditInfoUser LEFT OUTER JOIN np_Individual ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Address ON np_CreditInfoUser.CreditInfoID = np_Address.CreditInfoID " +
                            "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_CompanyBoardSecretary ON np_CreditInfoUser.CreditInfoID = cpi_CompanyBoardSecretary.SecretaryCIID " +
                            "WHERE np_CreditInfoUser.CreditInfoID IN (SELECT cpi_CompanyBoardSecretary.SecretaryCIID FROM cpi_CompanyBoardSecretary WHERE cpi_CompanyBoardSecretary.CompanyCIID = " +
                            companyCIID + ") AND cpi_CompanyBoardSecretary.CompanyCIID = " + companyCIID +
                            "and np_CreditInfoUser.Type = " + CigConfig.Configure("lookupsettings.companyID");
                        //AND np_IDNumbers.NumberTypeID = 1

                        //	OleDbCommand myOleDbCommand = new OleDbCommand("SELECT SecretaryCIID, CompanyCIID, HistoryNative, HistoryEN from cpi_CompanyBoardSecretary WHERE CompanyCIID = "+companyCIID,myOleDbConn);
                        OleDbCommand myOleDbCommand = new OleDbCommand(query, myOleDbConn);
                        OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                        if (reader.Read()) {
                            BoardSecretaryCompanyBLLC theSecretary = new BoardSecretaryCompanyBLLC();
                            if (!reader.IsDBNull(0)) {
                                theSecretary.CreditInfoID = reader.GetInt32(0);
                            }
                            if (!reader.IsDBNull(9)) {
                                theSecretary.HistoryNative = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10)) {
                                theSecretary.HistoryEN = reader.GetString(10);
                            }
                            //Now we need customer informations - we can get them from useradmin
                            if (theSecretary.CreditInfoID > 0) {
                                uaFactory uaFact = new uaFactory();
                                Company comp = uaFact.GetCompany(theSecretary.CreditInfoID);

                                if (comp.CreditInfoID > 0) {
                                    try {
                                        int nationalID =
                                            int.Parse(CigConfig.Configure("lookupsettings.nationalID").Trim());

                                        foreach (IDNumber IDNumber in comp.IDNumbers) {
                                            if (IDNumber.NumberTypeID == nationalID) {
                                                theSecretary.NationalID = IDNumber.Number;
                                            }
                                        }
                                    } catch (Exception) {}

                                    theSecretary.NameNative = comp.NameNative;
                                    theSecretary.NameEN = comp.NameEN;
                                    theSecretary.Address = comp.Address;
                                } else {
                                    return null;
                                }
                            }
                            return theSecretary;
                        }
                    } catch (Exception e) {
                        Logger.WriteToLog(className + " : " + funcName, e, true);
                    }
                }
            }
            return null;
        }

        public DataSet GetBoardSecretaryAsDataSet(int companyCIID) {
            DataSet dsBoardSecretary = new DataSet();
            string funcName = "GetBoardSecretaryAsDataSet(int companyCIID) ";
            
            string query =
                "SELECT DISTINCT np_CreditInfoUser.CreditInfoID, np_CreditInfoUser.Type, np_Companys.NameNative,np_Companys.NameEN, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number, cpi_CompanyBoardSecretary.HistoryNative, cpi_CompanyBoardSecretary.HistoryEN, np_Address.StreetNative, np_Address.StreetEN " +
                "FROM np_CreditInfoUser LEFT OUTER JOIN np_Individual ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Address ON np_CreditInfoUser.CreditInfoID = np_Address.CreditInfoID " +
                "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_CompanyBoardSecretary ON np_CreditInfoUser.CreditInfoID = cpi_CompanyBoardSecretary.SecretaryCIID " +
                "WHERE np_CreditInfoUser.CreditInfoID IN (SELECT cpi_CompanyBoardSecretary.SecretaryCIID FROM cpi_CompanyBoardSecretary WHERE cpi_CompanyBoardSecretary.CompanyCIID = " +
                companyCIID + ") AND cpi_CompanyBoardSecretary.CompanyCIID = " + companyCIID + " ";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsBoardSecretary);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
            }
            return dsBoardSecretary;
        }

        /// <summary>
        /// Takes an instance of BoardSecretary and adds to database
        /// </summary>
        /// <param name="theShareholder">Instance of BoardSecretaryBLLC</param>
        /// <param name="companyCIID">The internal CIID number of the company</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddBoardSecretary(BoardSecretaryBLLC theSecretary, int companyCIID) {
            string funcName = "AddBoardSecretary(BoardSecretaryBLLC theSecretary, int companyCIID)";
            try {
                int secretaryCIID = -1;
                if (theSecretary.CreditInfoID > 0) {
                    secretaryCIID = theSecretary.CreditInfoID;
                } else {
                    secretaryCIID = GetCIIDByNationalID(theSecretary.NationalID);
                }

                //If no creditinfoid found, then we need to create the user
                if (secretaryCIID <= 0) {
                    CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
                    Indivitual theIndi = new Indivitual();

                    theIndi.FirstNameNative = theSecretary.FirstNameNative;
                    theIndi.SurNameNative = theSecretary.SurNameNative;
                    theIndi.FirstNameEN = theSecretary.FirstNameEN;
                    theIndi.SurNameEN = theSecretary.SurNameEN;

                    IDNumber theIDNumber = new IDNumber();
                    theIDNumber.NumberTypeID = 1;
                    theIDNumber.Number = theSecretary.NationalID;

                    theIndi.IDNumbers = new ArrayList();
                    theIndi.IDNumbers.Add(theIDNumber);

                    theIndi.EducationID = 1; // meaning not accessible
                    theIndi.ProfessionID = 1; // meaning not accessible
                    theIndi.Number = new ArrayList();
                    theIndi.Type = CigConfig.Configure("lookupsettings.individualID"); // "2"; // meaning Indivitual
                    try {
                        secretaryCIID = theUserDAL.AddNewIndivitual(theIndi);
                    } catch (Exception exc) {
                        Logger.WriteToLog(
                            className + ":" + funcName + " -- Error when inserting new individual", exc, true);
                    }
                }
                //ok - now we should have creditinfo id
                if (secretaryCIID > 0) {
                    theSecretary.CreditInfoID = secretaryCIID;
                    return InsertBoardSecretary(
                        theSecretary.CreditInfoID, companyCIID, theSecretary.HistoryNative, theSecretary.HistoryEN);
                } else {
                    return false;
                }
            } catch (Exception exc) {
                Logger.WriteToLog(className + ":" + funcName, exc, true);
                return false;
            }
        }

        public bool AddBoardSecretaryCompany(BoardSecretaryCompanyBLLC theSecretary, int CIID) {
            string funcName = "AddBoardSecretaryCompany(BoardSecretaryCompanyBLLC bSecretaryCompany, int CIID)";
            try {
                int secretaryCIID = -1;
                if (theSecretary.CreditInfoID > 0) {
                    secretaryCIID = theSecretary.CreditInfoID;
                } else {
                    secretaryCIID = GetCIIDByNationalID(theSecretary.NationalID);
                }

                //If no creditinfoid found, then we need to create the user
                if (secretaryCIID <= 0) {
                    CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
                    Company theCompany = new Company();

                    theCompany.NameEN = theSecretary.NameEN;
                    theCompany.NameNative = theSecretary.NameEN;

                    IDNumber theIDNumber = new IDNumber();
                    theIDNumber.NumberTypeID = 1;
                    theIDNumber.Number = theSecretary.NationalID;

                    theCompany.IDNumbers = new ArrayList();
                    theCompany.IDNumbers.Add(theIDNumber);

                    theCompany.Number = new ArrayList();
                    theCompany.Type = CigConfig.Configure("lookupsettings.companyID"); // "2"; // meaning Indivitual
                    try {
                        secretaryCIID = theUserDAL.AddCompany(theCompany);
                    } catch (Exception exc) {
                        Logger.WriteToLog(
                            className + ":" + funcName + " -- Error when inserting new company", exc, true);
                    }
                }
                //ok - now we should have creditinfo id
                if (secretaryCIID > 0) {
                    theSecretary.CreditInfoID = secretaryCIID;
                    return InsertBoardSecretary(
                        theSecretary.CreditInfoID, CIID, theSecretary.HistoryNative, theSecretary.HistoryEN);
                } else {
                    return false;
                }
            } catch (Exception exc) {
                Logger.WriteToLog(className + ":" + funcName, exc, true);
                return false;
            }
        }

        public bool DeleteBoardSecretary(int companyCIID) {
            string funcName = "DeleteBoardSecretary(int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_CompanyBoardSecretary WHERE CompanyCIID = " + companyCIID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Inserts new instance of board secretary
        /// </summary>
        /// <param name="theSecretary">The secretary to insert</param>
        /// <param name="companyCIID">The company ciid</param>
        /// <returns>true if inserted ok, false otherwise</returns>
        public bool InsertBoardSecretary(int secretaryCIID, int companyCIID, string historyNative, string historyEN) {
            string funcName = "InsertBoardSecretary(BoardSecretaryBLLC theSearetary, int companyCIID)";
            // if the board member is already registed for the company the he should be updated 
            if (IsBoardSecretaryAlreadyRegisted(companyCIID)) {
                return UpdateBoardSecretary(secretaryCIID, companyCIID, historyNative, historyEN);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_CompanyBoardSecretary(SecretaryCIID, CompanyCIID,HistoryNative, HistoryEN) VALUES(?,?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();

                    myParam = new OleDbParameter("SecretaryCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = secretaryCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        historyNative,
                        historyEN,
                        OleDbType.WChar);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Updates the BoardSecretary in the database
        /// </summary>
        /// <param name="theSecretary">The secretary info</param>
        /// <param name="companyCIID">The company ciid</param>
        /// <returns>true if update ok - false otherwise</returns>
        public bool UpdateBoardSecretary(int secretaryCIID, int companyCIID, string historyNative, string historyEN) {
            string funcName = "UpdateBoardSecretary(BoardSecretaryBLLC theSecretary, int companyCIID)";
            //Is the secretary already registered?
            if (!IsBoardSecretaryAlreadyRegisted(companyCIID)) {
                return InsertBoardSecretary(secretaryCIID, companyCIID, historyNative, historyEN);
            }

            //string tmpQ = "UPDATE cpi_CompanyBoardSecretary SET SecretaryCIID = "+ theSecretary.CreditInfoID+", HistoryNative = '"+theSecretary.HistoryNative+"' , HistoryEN = '"+theSecretary.HistoryEN+"' WHERE CompanyCIID = "+ companyCIID;
            string tmpQ =
                "UPDATE cpi_CompanyBoardSecretary SET SecretaryCIID = ?, HistoryNative = ?, HistoryEN = ? WHERE CompanyCIID = ?";

            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = tmpQ;
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam;

                    myParam = new OleDbParameter("SecretaryCIID", OleDbType.Integer);
                    myParam.Value = secretaryCIID;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        historyNative,
                        historyEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Value = companyCIID;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        protected bool IsBoardSecretaryAlreadyRegisted(int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_CompanyBoardSecretary WHERE CompanyCIID = " + companyCIID, myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Takes an instance of ShareholderOwnerPerson and adds to database
        /// </summary>
        /// <param name="theShareholder">Instance of ShareholderOwnerPersonBLLC</param>
        /// <param name="companyCIID">The internal CIID number of the company</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddShareholderOwner(ShareholdersOwnerPersonBLLC theShareholder, int companyCIID) {
            string funcName = "AddShareholderOwner(ShareholdersOwnerPersonBLLC theShareholder, int companyCIID)";
            bool result = false;
            int shareholderCIID = -1;
            if (theShareholder.CreditInfoID > 0) {
                shareholderCIID = theShareholder.CreditInfoID;
            } else {
                shareholderCIID = GetCIIDByNationalID(theShareholder.NationalID);
            }
            CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
            Indivitual theIndi = new Indivitual();
            theIndi.CreditInfoID = shareholderCIID;
            IDNumber theIDNumber = new IDNumber();

            theIndi.FirstNameNative = theShareholder.FirstNameNative;
            theIndi.SurNameNative = theShareholder.SurNameNative;
            theIndi.FirstNameEN = theShareholder.FirstNameEN;
            theIndi.SurNameEN = theShareholder.SurNameEN;

            theIDNumber.NumberTypeID = 1;
            theIDNumber.Number = theShareholder.NationalID;
            theIndi.IDNumbers = new ArrayList();
            theIndi.IDNumbers.Add(theIDNumber);

            theIndi.Address = theShareholder.Address;

            theIndi.EducationID = 0; // meaning not accessible
            theIndi.ProfessionID = 0; // meaning not accessible
            theIndi.Number = new ArrayList();
            theIndi.Type = CigConfig.Configure("lookupsettings.individualID"); // "2"; // meaning Indivitual
            try {
                if (shareholderCIID < 0) {
                    shareholderCIID = theUserDAL.AddNewIndivitual(theIndi);
                    //	theShareholder.CreditInfoID = shareholderCIID;
                    //	result = InsertBoardMember(theBoardMember,companyCIID);
                    if (shareholderCIID > -1) {
                        result = InsertShareholderOwner(
                            shareholderCIID,
                            theShareholder.OwnershipTypeID,
                            theShareholder.Ownership,
                            theShareholder.OwnershipPercentage,
                            companyCIID);
                    }
                } else // CIID exists then update the user...
                {
                    // not such a good idea to update the user ... lets focus on shareholders
                    /*
					theIndi.IDNumbers = new ArrayList(); // prevent any ID numbers update
					shareholderCIID = theUserDAL.UpdateIndivitual(theIndi);
					if(shareholderCIID > 0)
						theShareholder.CreditInfoID = shareholderCIID;
			*/
                    result = UpdateShareholderOwner(
                        shareholderCIID,
                        theShareholder.OwnershipTypeID,
                        theShareholder.Ownership,
                        theShareholder.OwnershipPercentage,
                        companyCIID);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + ":" + funcName, err, true);
            }
            return result;
        }

        /// <summary>
        /// Takes an instance of ShareholderOwnerCompany and adds to database
        /// </summary>
        /// <param name="theShareholder">Instance of ShareholderOwnerCompanyBLLC</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddShareholderOwner(ShareholdersOwnerCompanyBLLC theShareholder, int companyCIID) {
            string funcName = "AddShareholderOwner(ShareholdersOwnerCompanyBLLC theShareholder, int companyCIID)";
            bool result = false;
            int shareholderCIID = -1;
            if (theShareholder.CreditInfoID > 0) {
                shareholderCIID = theShareholder.CreditInfoID;
            } else {
                shareholderCIID = GetCIIDByNationalID(theShareholder.NationalID);
            }

            CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
            Company theComp = new Company();
            theComp.CreditInfoID = shareholderCIID;
            theComp.NameNative = theShareholder.NameNative;
            theComp.NameEN = theShareholder.NameEN;
            theComp.Org_status_code = 1; // meaning not accessible
            IDNumber theIDNumber = new IDNumber();
            theIDNumber.NumberTypeID = 1;
            theIDNumber.Number = theShareholder.NationalID;
            theComp.IDNumbers = new ArrayList();
            theComp.IDNumbers.Add(theIDNumber);

            theComp.Address = theShareholder.Address;

            theComp.Number = new ArrayList();
            theComp.Type = CigConfig.Configure("lookupsettings.companyID");
            try {
                if (shareholderCIID < 0) {
                    shareholderCIID = theUserDAL.AddCompany(theComp);
                    //	theShareholder.CreditInfoID = shareholderCIID;
                    //	result = InsertBoardMember(theBoardMember,companyCIID);

                    if (shareholderCIID > -1) {
                        result = InsertShareholderOwner(
                            shareholderCIID,
                            theShareholder.OwnershipTypeID,
                            theShareholder.Ownership,
                            theShareholder.OwnershipPercentage,
                            companyCIID);
                    }
                } else // CIID exists then update the user...
                {
                    // not such a good idea to update the user ... lets focus on shareholding
                    /*	theComp.IDNumbers = new ArrayList(); // prevent any ID numbers update
					shareholderCIID = theUserDAL.UpdateCompany(theComp);
					if(shareholderCIID > 0)
						theShareholder.CreditInfoID = shareholderCIID;
				*/
                    result = UpdateShareholderOwner(
                        shareholderCIID,
                        theShareholder.OwnershipTypeID,
                        theShareholder.Ownership,
                        theShareholder.OwnershipPercentage,
                        companyCIID);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + ":" + funcName, err, true);
            }
            return result;
        }

        /// <summary>
        /// This function returns all Companies owner as DataSet
        /// </summary>
        /// <param name="companyCIID">The companies internal system id</param>
        /// <returns>All owners as DataSet</returns>
        public DataSet GetShareHolderOwnerAsDataSet(int companyCIID) {
            DataSet dsShareHolders = new DataSet();
            string funcName = "GetShareHolderOwnerAsDataSet(int companyCIID) ";
            // athuga h�r a� einnig �arf a� vera h�gt a� s�kja fyrirt�ki ekki bara einstaklinga ..!
            // svona ?
            // SELECT DISTINCT np_CreditInfoUser.CreditInfoID, np_CreditInfoUser.Type, np_Companys.NameNative,np_Companys.NameEN, np_Individual.FirstNameNative, np_Individual.FirstNameEN, np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number,cpi_Owners.Ownership,np_Address.StreetNative, np_Address.StreetEN FROM np_CreditInfoUser LEFT OUTER JOIN np_Individual ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Address ON np_CreditInfoUser.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_Owners ON np_CreditInfoUser.CreditInfoID = cpi_Owners.OwnerCIID WHERE np_CreditInfoUser.CreditInfoID IN (SELECT cpi_Owners.OwnerCIID FROM cpi_Owners WHERE cpi_Owners.CompanyCIID = 1) AND np_IDNumbers.NumberTypeID = 1
            
            string query =
                "SELECT DISTINCT np_CreditInfoUser.CreditInfoID, np_CreditInfoUser.Type, np_Companys.NameNative,np_Companys.NameEN, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number,cpi_Owners.Ownership, cpi_Owners.OwnershipID ,np_Address.StreetNative, np_Address.StreetEN, cpi_Ownership.OwnershipDescriptionNative, cpi_Ownership.OwnershipDescriptionEN, cpi_Owners.Ownership_perc " +
                "FROM np_CreditInfoUser LEFT OUTER JOIN np_Individual ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Address ON np_CreditInfoUser.CreditInfoID = np_Address.CreditInfoID " +
                "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_Owners ON np_CreditInfoUser.CreditInfoID = cpi_Owners.OwnerCIID LEFT OUTER JOIN cpi_Ownership ON cpi_Owners.OwnershipID = cpi_Ownership.OwnershipID " +
                "WHERE np_CreditInfoUser.CreditInfoID IN (SELECT cpi_Owners.OwnerCIID FROM cpi_Owners WHERE cpi_Owners.CompanyCIID = " +
                companyCIID + ") AND cpi_Owners.CompanyCIID = " + companyCIID + " ORDER BY cpi_Owners.Ownership DESC";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsShareHolders);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsShareHolders;
        }

        /// <summary>
        /// This function returns all shares owned by a specific owner.
        /// </summary>
        /// <param name="ownerCiid">Ciid for the owner.</param>
        /// <returns>All shares as data set.</returns>
        public DataSet GetInvolvements(int ownerCiid) {
            DataSet dsShareholder = new DataSet("Shareholder");
            string funcName = "GetInvolvements(int debtor)";
            using (OleDbConnection connection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                OleDbDataAdapter adapter = new OleDbDataAdapter();
                OleDbCommand command =
                    new OleDbCommand(
                        "SELECT DISTINCT TOP 100 PERCENT np_CreditInfoUser.CreditInfoID, np_Companys.NameNative, np_Companys.NameEN, np_IDNumbers.Number, cpi_Owners.Ownership, cpi_Owners.OwnershipID, cpi_Ownership.OwnershipDescriptionNative, cpi_Ownership.OwnershipDescriptionEN, cpi_Owners.ownership_perc FROM np_IDNumbers RIGHT OUTER JOIN cpi_Ownership RIGHT OUTER JOIN cpi_Owners INNER JOIN np_CreditInfoUser ON cpi_Owners.CompanyCIID = np_CreditInfoUser.CreditInfoID ON  cpi_Ownership.OwnershipID = cpi_Owners.OwnershipID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID ON np_IDNumbers.CreditInfoID = np_CreditInfoUser.CreditInfoID WHERE cpi_Owners.OwnerCIID=? ORDER BY cpi_Owners.Ownership DESC",
                        connection);
                OleDbParameter param = new OleDbParameter("OwnerCIID", OleDbType.Integer);
                param.Value = ownerCiid;
                param.Direction = ParameterDirection.Input;
                command.Parameters.Add(param);
                adapter.SelectCommand = command;

                try {
                    connection.Open();
                    adapter.Fill(dsShareholder);
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                }
            }

            return dsShareholder;
        }

        /// <summary>
        /// This function returns all Companies owner as DataSet
        /// </summary>
        /// <param name="companyCIID">The companies internal system id</param>
        /// <returns>All owners as DataSet</returns>
        public DataSet GetShareHolderOwnerAsDataSet(int companyCIID, int ownerShipType) {
            DataSet dsShareHolders = new DataSet();
            string funcName = "GetShareHolderOwnerAsDataSet(int companyCIID) ";
            // athuga h�r a� einnig �arf a� vera h�gt a� s�kja fyrirt�ki ekki bara einstaklinga ..!
            // svona ?
            // SELECT DISTINCT np_CreditInfoUser.CreditInfoID, np_CreditInfoUser.Type, np_Companys.NameNative,np_Companys.NameEN, np_Individual.FirstNameNative, np_Individual.FirstNameEN, np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number,cpi_Owners.Ownership,np_Address.StreetNative, np_Address.StreetEN FROM np_CreditInfoUser LEFT OUTER JOIN np_Individual ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Address ON np_CreditInfoUser.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_Owners ON np_CreditInfoUser.CreditInfoID = cpi_Owners.OwnerCIID WHERE np_CreditInfoUser.CreditInfoID IN (SELECT cpi_Owners.OwnerCIID FROM cpi_Owners WHERE cpi_Owners.CompanyCIID = 1) AND np_IDNumbers.NumberTypeID = 1
            
            string query =
                "SELECT DISTINCT np_CreditInfoUser.CreditInfoID, np_CreditInfoUser.Type, np_Companys.NameNative,np_Companys.NameEN, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number,cpi_Owners.Ownership, cpi_Owners.OwnershipID ,np_Address.StreetNative, np_Address.StreetEN, cpi_Ownership.OwnershipDescriptionNative, cpi_Ownership.OwnershipDescriptionEN, cpi_Owners.Ownership_perc " +
                "FROM np_CreditInfoUser LEFT OUTER JOIN np_Individual ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Address ON np_CreditInfoUser.CreditInfoID = np_Address.CreditInfoID " +
                "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_Owners ON np_CreditInfoUser.CreditInfoID = cpi_Owners.OwnerCIID LEFT OUTER JOIN cpi_Ownership ON cpi_Owners.OwnershipID = cpi_Ownership.OwnershipID " +
                "WHERE np_CreditInfoUser.CreditInfoID IN (SELECT cpi_Owners.OwnerCIID FROM cpi_Owners WHERE cpi_Owners.CompanyCIID = " +
                companyCIID + ") AND cpi_Owners.CompanyCIID = " + companyCIID + " AND cpi_Owners.OwnershipID = " +
                ownerShipType + " ORDER BY cpi_Owners.Ownership DESC";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsShareHolders);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsShareHolders;
        }

        /// <summary>
        /// This functions return all available Banks as DataSet
        /// </summary>
        /// <returns>All available Banks</returns>
        public DataSet GetBanksAsDataSet() {
            DataSet dsBankSet = new DataSet();
            string funcName = "GetBanksAsDataSet() ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM cpi_Banks", myOleDbConn);
                    myAdapter.Fill(dsBankSet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsBankSet;
        }

        public DataSet GetOwnershipTypesAsDataSet() {
            DataSet dsOwnershipTypes = new DataSet();
            string funcName = "GetOwnershipTypesAsDataSet() ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand =
                        new OleDbCommand(
                            "SELECT OwnershipID, OwnershipDescriptionNative, OwnershipDescriptionEN FROM cpi_Ownership",
                            myOleDbConn);
                    myAdapter.Fill(dsOwnershipTypes);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsOwnershipTypes;
        }

        /// <summary>
        /// This function returns all available Banks as ordered dataset
        /// </summary>
        /// <param name="native">Tells wether the result shall be ordered by native or EN</param>
        /// <returns></returns>
        public DataSet GetBanksAsDataSet(bool native) {
            DataSet dsBankSet = new DataSet();
            string funcName = "GetBanksAsDataSet() ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    if (native) {
                        myAdapter.SelectCommand = new OleDbCommand(
                            "SELECT * FROM cpi_Banks ORDER BY NameNative", myOleDbConn);
                    } else {
                        myAdapter.SelectCommand = new OleDbCommand(
                            "SELECT * FROM cpi_Banks ORDER BY NameEN", myOleDbConn);
                    }
                    myAdapter.Fill(dsBankSet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsBankSet;
        }

        public DataSet GetMTBanksAsDataSet() {
            DataSet dsBankSet = new DataSet();
            string funcName = "GetMTBanksAsDataSet() ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand =
                        new OleDbCommand(
                            "SELECT BankID, NameNative+' '+Location AS NameNative, NameEN, Location FROM cpi_Banks ORDER BY NameNative",
                            myOleDbConn);
                    myAdapter.Fill(dsBankSet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsBankSet;
        }

        /// <summary>
        /// This function returns all available InExportAreas as DataSet
        /// </summary>
        /// <returns>All available InExportAreas</returns>
        public DataSet GetInExportAreasAsDataSet() {
            DataSet dsInExportAreasSet = new DataSet();

            return dsInExportAreasSet;
        }

        /// <summary>
        /// This function returns all Continents as DataSet
        /// </summary>
        /// <returns>All continents as DataSet</returns>
        public DataSet GetContinentsAsDataSet() {
            DataSet dsContinentsSet = new DataSet();
            string funcName = "GetContinentsAsDataSet() ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM cpi_Continents", myOleDbConn);
                    myAdapter.Fill(dsContinentsSet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return dsContinentsSet;
        }

        /// <summary>
        /// This function returns all Countries as DataSet
        /// </summary>
        /// <returns>All countries as DataSet</returns>
        public DataSet GetCountriesAsDataSet(bool native) {
            DataSet dsCountriesSet = new DataSet();
            string funcName = "GetCountriesAsDataSet() ";
            
            string query = "";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    if (native) {
                        query = "SELECT * FROM cpi_Countries ORDER BY NameNative";
                    } else {
                        query = "SELECT * FROM cpi_Countries ORDER BY NameEN";
                    }

                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsCountriesSet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCountriesSet;
        }

        /// <summary>
        /// This function returns all Countries by Continent as DataSet
        /// </summary>
        /// <param name="continentID"></param>
        /// <returns>All countries by ContinentID</returns>
        public DataSet GetCountriesPerContinentAsDataSet(int continentID) {
            DataSet dsCountriesPerContinentSet = new DataSet();

            return dsCountriesPerContinentSet;
        }

        /// <summary>
        /// This function returns all Operations by Company CIID
        /// </summary>
        /// <param name="companyCIID">The companies internal system number</param>
        /// <returns>Operations as DataSet</returns>
        public DataSet GetCompanyOperationAsDataSet(int companyCIID, bool current) {
            DataSet dsCompanyOperation = new DataSet();
            string funcName = "GetCompanyOperationAsDataSet(int companyCIID, bool current) ";
            
            //ArrayList arrCompNaceCode = GetCompanyNaceCodes(companyCIID,current);
            //string query = "SELECT * FROM cpi_NaceCodes WHERE LEN(NaceCodeID)=5 AND (NaceCodeID = ";
            /*if(arrCompNaceCode==null||arrCompNaceCode.Count<=0)
				return dsCompanyOperation;*/
            //string query = "SELECT NaceCodeID, (NaceCodeID + '  ' + DescriptionNative) as DescriptionNative,(NaceCodeID COLLATE SQL_Latin1_General_CP1_CI_AI + '  ' + DescriptionEN) as DescriptionEN, ShortDescriptionNative, ShortDescriptionEN, AbbreviationNative, AbbreviationEN, DefinitionNative, DefinitionEN, CodeType FROM cpi_NaceCodes where len(NaceCodeID)=5 AND (NaceCodeID = ";
           /* string query =
                "SELECT nc.NaceCodeID, (nc.NaceCodeID + '  ' + nc.DescriptionNative) as DescriptionNative, " +
                "(nc.NaceCodeID COLLATE SQL_Latin1_General_CP1_CI_AI + '  ' + nc.DescriptionEN) as DescriptionEN, " +
                "nc.ShortDescriptionNative, nc.ShortDescriptionEN, nc.AbbreviationNative, nc.AbbreviationEN, nc.DefinitionNative, nc.DefinitionEN, nc.CodeType " +
                "FROM cpi_CompaniesNaceCodes cnc left join cpi_NaceCodes nc on cnc.NaceCodeID=nc.NaceCodeID " +
                "where cnc.CompanyCIID = " + companyCIID + " and cnc.CurrentCode = '" + current +
                "' and len(nc.NaceCodeID)=5 order by OrderLevel";*/
             string query =
               "SELECT nc.NaceCodeID, (nc.NaceCodeID + '  ' + nc.DescriptionNative) as DescriptionNative, " +
               "(nc.NaceCodeID COLLATE SQL_Latin1_General_CP1_CI_AI + '  ' + nc.DescriptionEN) as DescriptionEN, " +
               "nc.ShortDescriptionNative, nc.ShortDescriptionEN, nc.AbbreviationNative, nc.AbbreviationEN, nc.DefinitionNative, nc.DefinitionEN, nc.CodeType " +
               "FROM cpi_CompaniesNaceCodes cnc left join cpi_NaceCodes nc on cnc.NaceCodeID=nc.NaceCodeID " +
               "where cnc.CompanyCIID = " + companyCIID + " and cnc.CurrentCode = '" + current +
               "'  order by OrderLevel";
            /*for(int i = 0; i < arrCompNaceCode.Count; i++)
			{
				if(i != 0)
					query += " OR NaceCodeID = ";
				query += (string) arrCompNaceCode[i];
			}
				query +=") ORDER BY OrderLevel";*/

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsCompanyOperation);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCompanyOperation;
        }

        /// <summary>
        /// This function return all Banks by Company CIID
        /// </summary>
        /// <param name="companyCIID">The companies internal system number</param>
        /// <returns>Companies Banks as DataSet</returns>
        public DataSet GetCompanyBanksAsDataSet(int companyCIID) {
            DataSet dsCompanyBanks = new DataSet();
            string funcName = "GetCompanyBanksAsDataSet(int companyCIID) ";
            
            ArrayList arrCompBanksID = GetCompanyBanksID(companyCIID);
            if (arrCompBanksID == null || arrCompBanksID.Count <= 0) {
                return dsCompanyBanks;
            }
            string query;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null &&
                CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                query = "SELECT *, namenative+' '+Location As NameNativeAndLocation FROM cpi_Banks WHERE BankID = '";
            } else {
                query = "SELECT * FROM cpi_Banks WHERE BankID = '";
            }
            for (int i = 0; i < arrCompBanksID.Count; i++) {
                if (i != 0) {
                    query += " OR BankID = '";
                }
                query += (string) arrCompBanksID[i] + "'";
            }

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsCompanyBanks);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCompanyBanks;
        }

        /// <summary>
        /// This function returns all Operations
        /// </summary>
        /// <returns>Operations as DataSet</returns>
        public DataSet GetOperationAsDataSet() {
            DataSet dsOperation = new DataSet();
            string funcName = "GetOperationAsDataSet() ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand =
                        new OleDbCommand(
                        //    "SELECT NaceCodeID, (NaceCodeID + '  ' + DescriptionNative) as CodeAndDescriptionNative,(NaceCodeID COLLATE SQL_Latin1_General_CP1_CI_AI + '  ' + DescriptionEN) as CodeAndDescriptionEN, ShortDescriptionNative, ShortDescriptionEN, AbbreviationNative, AbbreviationEN, DefinitionNative, DefinitionEN, CodeType FROM cpi_NaceCodes where len(NaceCodeID)=5",
                            "SELECT NaceCodeID, (NaceCodeID + '  ' + DescriptionNative) as CodeAndDescriptionNative,(NaceCodeID COLLATE SQL_Latin1_General_CP1_CI_AI + '  ' + DescriptionEN) as CodeAndDescriptionEN, ShortDescriptionNative, ShortDescriptionEN, AbbreviationNative, AbbreviationEN, DefinitionNative, DefinitionEN, CodeType FROM cpi_NaceCodes",
                            myOleDbConn);
                    myAdapter.Fill(dsOperation);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return dsOperation;
        }

        /// <summary>
        /// Gets available TradeTerms
        /// </summary>
        /// <param name="sales">Whether to pick sales or payment term</param>
        /// <returns>DataSet of TradeTerms</returns>
        public DataSet GetTradeTermsAsDataSet(bool sales) {
            DataSet dsTradeTerms = new DataSet();
            string funcName = "GetTradeTermsAsDataSet(bool sales) ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    if (sales) {
                        myAdapter.SelectCommand =
                            new OleDbCommand("SELECT * FROM cpi_TradeTerms WHERE SalesTerm = 'True'", myOleDbConn);
                    } else {
                        myAdapter.SelectCommand =
                            new OleDbCommand("SELECT * FROM cpi_TradeTerms WHERE PaymentTerm = 'True'", myOleDbConn);
                    }
                    myAdapter.Fill(dsTradeTerms);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return dsTradeTerms;
        }

        /// <summary>
        /// Get the company trade terms as DataSet
        /// </summary>
        /// <param name="companyCIID">The companies internar system number</param>
        /// <param name="sales">Wheter it's an sales or payment term</param>
        /// <returns>Trade terms as DataSet</returns>
        public DataSet GetCompanyTradeTermsAsDateSet(int companyCIID, bool sales) {
            DataSet dsCompanyTradeTerms = new DataSet();
            string funcName = "GetCompanyTradeTermsAsDateSet(int companyCIID, bool sales) ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand =
                        new OleDbCommand(
                            "SELECT TT.TermsID, TT.TermsDescriptionNative, TT.TermsDescriptionEN, CTT.SalesTerm, CTT.CommercialTrade FROM cpi_CompanyTradeTerms CTT LEFT OUTER JOIN cpi_TradeTerms TT ON TT.TermsID = CTT.TermsID WHERE CTT.CompanyCIID = " +
                            companyCIID + " AND CTT.SalesTerm = '" + sales + "'",
                            myOleDbConn);
                    myAdapter.Fill(dsCompanyTradeTerms);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return dsCompanyTradeTerms;
        }

        public ArrayList GetCompanyNaceCodes(int companyCIID, bool current) {
            ArrayList arrCNaceC = new ArrayList();
            string funcName = "GetCompanyNaceCodes(int companyCIID) ";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM cpi_CompaniesNaceCodes WHERE Len(NaceCodeID)=5 AND CompanyCIID = " +
                            companyCIID + " AND CurrentCode = '" + current + "'",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    while (reader.Read()) {
                        if (!reader.IsDBNull(1)) {
                            arrCNaceC.Add(reader.GetString(1));
                        }
                    }
                    reader.Close();
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
            }
            return arrCNaceC;
        }

        public ArrayList GetCompanyBanksID(int companyCIID) {
            ArrayList arrBankID = new ArrayList();
            string funcName = "GetCompanyBanksID(int companyCIID) ";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM cpi_CompanyBanks WHERE CompanyCIID = " + companyCIID + "", myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    while (reader.Read()) {
                        if (!reader.IsDBNull(1)) {
                            arrBankID.Add(reader.GetString(1));
                        }
                    }
                    reader.Close();
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
            }
            return arrBankID;
        }

        public DataSet GetCompaniesCustomersAsDataSet(int companyCIID) {
            /*	DataSet dsCompaniesCustomers = new DataSet();
			string funcName = "GetCompaniesCustomersAsDataSet() ";
			
			try
			{
				using(OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
				{
					OleDbDataAdapter myAdapter = new OleDbDataAdapter();
					myOleDbConn.Open();
					myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM cpi_CompaniesCustomerType WHERE CompanyCIID = "+ companyCIID +"",myOleDbConn);
					myAdapter.Fill(dsOperation);
				}
			}
			catch (Exception err)
			{
				Logger.WriteToLog(className +" : " + funcName + err.Message,true);
			}

			return CompaniesCustomers;
			*/
            DataSet dsCompaniesCustomersTypes = new DataSet();
            string funcName = "GetCompaniesCustomersAsDataSet() ";
            
            ArrayList arrCompCustID = GetCompaniesCustomersTypeID(companyCIID);
            if (arrCompCustID == null || arrCompCustID.Count <= 0) {
                return dsCompaniesCustomersTypes;
            }
            string query = "SELECT * FROM cpi_CustomerType WHERE CustomerTypeID = ";
            for (int i = 0; i < arrCompCustID.Count; i++) {
                if (i != 0) {
                    query += " OR CustomerTypeID = ";
                }
                query += (int) arrCompCustID[i];
            }

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsCompaniesCustomersTypes);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCompaniesCustomersTypes;
        }

        public ArrayList GetCompaniesCustomersTypeID(int companyCIID) {
            ArrayList arrCompCustID = new ArrayList();
            string funcName = "GetCompaniesCustomersID(int companyCIID) ";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM cpi_CompaniesCustomerType WHERE CompanyCIID = " + companyCIID + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    while (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            arrCompCustID.Add(reader.GetInt32(0));
                        }
                    }
                    reader.Close();
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                }
            }
            return arrCompCustID;
        }

        public DataSet GetCustomerTypesAsDataSet() {
            DataSet dsCustomersTypes = new DataSet();
            string funcName = "GetCompaniesCustomersAsDataSet() ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM cpi_CustomerType", myOleDbConn);
                    myAdapter.Fill(dsCustomersTypes);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return dsCustomersTypes;
        }

        public DataSet GetStaffCountAsDataSet(int companyCIID) {
            DataSet dsStaffCount = new DataSet();
            string funcName = "GetStaffCountAsDataSet(int companyCIID) ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand =
                        new OleDbCommand(
                            "SELECT TOP 10* FROM cpi_StaffCount WHERE CompanyCIID = " + companyCIID +
                            " ORDER BY Year desc",
                            myOleDbConn);
                    myAdapter.Fill(dsStaffCount);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return dsStaffCount;
        }

        public DataSet GetManagementPositionAsDataSet() {
            DataSet dsManagementPositions = new DataSet();
            string funcName = "GetManagementPositionAsDataSet() ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM cpi_ManagementPositions", myOleDbConn);
                    myAdapter.Fill(dsManagementPositions);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return dsManagementPositions;
        }

        /// <summary>
        /// Get all jobtitles as dataset
        /// </summary>
        /// <returns>All available Jobtitles as DataSet</returns>
        public DataSet GetJobTitlesAsDataSet() {
            DataSet dsJobTitles = new DataSet();
            string funcName = "GetJobTitlesAsDataSet() ";
            
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand("SELECT * FROM cpi_JobTitle", myOleDbConn);
                    myAdapter.Fill(dsJobTitles);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }

            return dsJobTitles;
        }

        public bool InsertEmployeeCountYears(ArrayList arrEmCount, int CIID) {
            string funcName = "InsertEmployeeCountYears(ArrayList arrEmCount, int CIID)";
            string year = "";
            int staffCount = 0;
            OleDbCommand myOleDbCommand = new OleDbCommand();
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();
                try {
                    myOleDbCommand.Transaction = myTrans;
                    myOleDbCommand.Connection = myOleDbConn;
                    foreach (StaffCountBLLC theStaffCount in arrEmCount) {
                        year = theStaffCount.Year;
                        staffCount = theStaffCount.EmployeesCount;
                        if (StaffCountYearAlreadyRegisted(year, CIID, myOleDbCommand)) {
                            UpdateStaffCountYear(year, staffCount, CIID, myOleDbCommand);
                        } else {
                            InsertStaffCountYear(year, staffCount, CIID, myOleDbCommand);
                        }
                    }

                    myTrans.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    myTrans.Rollback();
                    return false;
                } finally {
                    // myOleDbCommand.Connection.Close(); // � ekki a� �urfa ef using er nota�
                }

                return true;
            }
            // h�rna �arf a� t�kka � hvort �egar s� til �r. Ef �a� er til �� �arf a� update annars
            // er insert
            // hafa �etta allt � einni transaction!
        }

        public bool StaffCountYearAlreadyRegisted(string year, int CIID, OleDbCommand myOleDbCommand) {
            string funcName = "StaffCountYearAlreadyRegisted(string year,int CIID,OleDbCommand myOleDbCommand)";
            try {
                myOleDbCommand.CommandText = "SELECT Year FROM cpi_StaffCount WHERE CompanyCIID = " + CIID +
                                             " AND Year = " + year + "";
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    reader.Close();
                    return true;
                }
                reader.Close();
                return false;
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                throw (new ApplicationException(err.Message));
            } finally {
                // 	reader.Close();
            }
        }

        public bool UpdateStaffCountYear(string year, int staffCount, int CIID, OleDbCommand myOleDbCommand) {
            string funcName = "UpdateStaffCountYear(string year,int staffCount,int CIID,OleDbCommand myOleDbCommand)";
            myOleDbCommand.Parameters.Clear();
            //	myOleDbCommand.CommandText = "UPDATE cpi_StaffCount SET Count = ? WHERE CompanyCIID = ? AND Year = ? ";
            myOleDbCommand.CommandText = "UPDATE cpi_StaffCount SET Count = " + staffCount + " WHERE CompanyCIID = " +
                                         CIID + "  AND Year = '" + year + "'";
            try {
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                throw (new ApplicationException(err.Message));
            }
            return true;
        }

        public bool InsertStaffCountYear(string year, int staffCount, int CIID, OleDbCommand myOleDbCommand) {
            string funcName = "UpdateStaffCountYear(string year,int staffCount,int CIID,OleDbCommand myOleDbCommand)";
            myOleDbCommand.Parameters.Clear();
            myOleDbCommand.CommandText = "INSERT INTO cpi_StaffCount(CompanyCIID,Year,Count) VALUES(?,?,?)";

            try {
                OleDbParameter myParam = new OleDbParameter();
                myOleDbCommand.Parameters.Clear();
                myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = CIID;
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Year", OleDbType.VarChar);
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = year;
                myOleDbCommand.Parameters.Add(myParam);
                myParam = new OleDbParameter("Count", OleDbType.Integer);
                myParam.Direction = ParameterDirection.Input;
                myParam.Value = staffCount;
                myOleDbCommand.Parameters.Add(myParam);
                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                throw (new ApplicationException(err.Message));
            }
            return true;
        }


        public DataSet GetAnnualReturAndAccountsDatesAsDataSet(int companyID)
        {
            DataSet dsArchivedDates = new DataSet();
            string funcName = "GetCompanyDocuemtDatesAsDataSet(string companyID, string documetnType)";

            string query = string.Format(@"  
                      select max(b.DataArchivedPub) as DataArchivedPub, DocumentType from np_IDNumbers a, lmt_companiesdocuments b where  a.Creditinfoid = '{0}'
                      and a.Number = b.CompanyID and (b.DocumentType = 'Accounts' or b.DocumentType = 'Annual Return')
                      group by b.DocumentType", companyID);
            try
            {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsArchivedDates);
                }
            }
            catch (Exception err)
            {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsArchivedDates;
        }


        public DataSet GetArchivedDatesAsDataSet(int companyID) {
            DataSet dsArchivedDates = new DataSet();
            string funcName = "GetArchivedDatesAsDataSet(string companyID)";
            
            string query =
                "select npid.Creditinfoid,max(c.DataArchivedPub) as DataArchivedPub from lmt_companiesannualreturns as c join np_IDNumbers as npid on npid.Number = c.CompanyID WHERE Creditinfoid=" +
                companyID + " AND c.DocumentType='Accounts'  group by npid.Creditinfoid";
            //string query="select CompanyID,max(DataArchivedPub) from lmt_companiesannualreturns where CompanyID="+ companyID+" group by CompanyID";

            /*
			string query = "SELECT DISTINCT TOP " +maxAccounts+ "MAX(t.Created) AS Created ,MAX(t.Modified) As Modified " +
				"FROM FSI_TemplateGeneralInfo t LEFT OUTER JOIN FSI_Origin ON t.Origin_id = FSI_Origin.Origin_id " +
				"WHERE t.CreditInfoID = "+ companyCIID +" AND Ready_for_web_publishing = 'True' AND Deleted = 'False' " +
				"AND (Account_period_length = (SELECT MAX(Account_period_length) FROM FSI_TemplateGeneralInfo WHERE AFS_ID = t.AFS_ID))";
			
			*/

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsArchivedDates);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsArchivedDates;
        }

        public DataSet GetMaxDatesAsDataSet(int companyCIID) {
            DataSet dsCreatModifDates = new DataSet();
            string funcName = "GetCreateModifyDatesAsDataSet(int companyCIID)";
            string maxAccounts = CigConfig.Configure("lookupsettings.maxFiscalStatementsInReport");
            
            /*	
			 * SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, np_Individual.SurNameNative, np_Individual.SurNameEN, np_Address.StreetNative, np_Address.StreetEN,np_IDNumbers.Number,cpi_ManagementPositions.TitleNative,cpi_ManagementPositions.TitleEN FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_BoardMembers ON np_Individual.CreditInfoID = cpi_BoardMembers.BoardMemberCIID LEFT OUTER JOIN cpi_ManagementPositions ON cpi_BoardMembers.ManagementPositionID = cpi_ManagementPositions.ManagementPositionID  WHERE np_Individual.CreditInfoID IN (SELECT cpi_BoardMembers.BoardMemberCIID FROM cpi_BoardMembers WHERE cpi_BoardMembers.CompanyCIID = 1) AND np_IDNumbers.NumberTypeID = 1
			*/
            /*	string query = "SELECT DISTINCT FSI_GeneralInfo.Financial_year, FSI_Origin.Description_native,FSI_Origin.Description_en " +
					"FROM FSI_GeneralInfo LEFT OUTER JOIN FSI_Origin ON FSI_GeneralInfo.Origin_id = FSI_Origin.Origin_id " +
					"WHERE FSI_GeneralInfo.CreditInfoID = "+ companyCIID +" AND Ready_for_web_publishing = 'True' AND Deleted = 'False'";
			*/
            string query = "SELECT DISTINCT TOP " + maxAccounts +
                           "MAX(t.Created) AS Created ,MAX(t.Modified) As Modified " +
                           "FROM FSI_TemplateGeneralInfo t LEFT OUTER JOIN FSI_Origin ON t.Origin_id = FSI_Origin.Origin_id " +
                           "WHERE t.CreditInfoID = " + companyCIID;

            /*AND Ready_for_web_publishing = 'True' AND Deleted = 'False' " +
				"AND (Account_period_length = (SELECT MAX(Account_period_length) FROM FSI_TemplateGeneralInfo WHERE AFS_ID = t.AFS_ID))";*/
            /*				FROM         FSI_GeneralInfo
							WHERE     (CreditInfoID = 58) AND (Account_period_length =
									  (SELECT     MAX(Account_period_length)
										FROM          FSI_GeneralInfo
										WHERE      CreditInfoID = 58))
			*/

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsCreatModifDates);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsCreatModifDates;
        }

        public DataSet GetAvailableReportYearsAsDataSet(int companyCIID) {
            DataSet dsAccountYears = new DataSet();
            string funcName = "GetAvailableReportYearsAsDataSet(int companyCIID)";
            string maxAccounts = CigConfig.Configure("lookupsettings.maxFiscalStatementsInReport");
            
            /*	
			 * SELECT DISTINCT np_Individual.CreditInfoID, np_Individual.FirstNameNative, np_Individual.FirstNameEN, np_Individual.SurNameNative, np_Individual.SurNameEN, np_Address.StreetNative, np_Address.StreetEN,np_IDNumbers.Number,cpi_ManagementPositions.TitleNative,cpi_ManagementPositions.TitleEN FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_BoardMembers ON np_Individual.CreditInfoID = cpi_BoardMembers.BoardMemberCIID LEFT OUTER JOIN cpi_ManagementPositions ON cpi_BoardMembers.ManagementPositionID = cpi_ManagementPositions.ManagementPositionID  WHERE np_Individual.CreditInfoID IN (SELECT cpi_BoardMembers.BoardMemberCIID FROM cpi_BoardMembers WHERE cpi_BoardMembers.CompanyCIID = 1) AND np_IDNumbers.NumberTypeID = 1
			*/
            /*	string query = "SELECT DISTINCT FSI_GeneralInfo.Financial_year, FSI_Origin.Description_native,FSI_Origin.Description_en " +
				"FROM FSI_GeneralInfo LEFT OUTER JOIN FSI_Origin ON FSI_GeneralInfo.Origin_id = FSI_Origin.Origin_id " +
				"WHERE FSI_GeneralInfo.CreditInfoID = "+ companyCIID +" AND Ready_for_web_publishing = 'True' AND Deleted = 'False'";
		*/
            string query = "SELECT DISTINCT TOP " + maxAccounts +
                           " t.AFS_id,t.Financial_year, t.Account_period_length,t.Financial_year_end,FSI_Origin.Description_native,FSI_Origin.Description_en " +
                           "FROM FSI_TemplateGeneralInfo t LEFT OUTER JOIN FSI_Origin ON t.Origin_id = FSI_Origin.Origin_id " +
                           "WHERE t.CreditInfoID = " + companyCIID +
                           " AND Ready_for_web_publishing = 'True' AND Deleted = 'False' " +
                           "AND (Account_period_length = (SELECT MAX(Account_period_length) FROM FSI_TemplateGeneralInfo WHERE AFS_ID = t.AFS_ID)) order by t.Financial_year desc, t.Financial_year_end desc";
/*				FROM         FSI_GeneralInfo
				WHERE     (CreditInfoID = 58) AND (Account_period_length =
                          (SELECT     MAX(Account_period_length)
                            FROM          FSI_GeneralInfo
                            WHERE      CreditInfoID = 58))
*/

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsAccountYears);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return dsAccountYears;
        }

        public CompanyNaceCodeBLLC GetCompanyNaceCode(int companyCIID) {
            string funcName = "GetCompanyNaceCode(int companyCIID)";
            if (companyCIID > 0) {
                
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    try {
                        myOleDbConn.Open();
                        string sql = "SELECT cn.CompanyCIID, cn.NaceCodeID, c.DescriptionNative, c.DescriptionEN, " +
                                     "c.ShortDescriptionNative, c.ShortDescriptionEN, c.AbbreviationNative, " +
                                     "c.AbbreviationEN, c.DefinitionNative, c.DefinitionEN, c.CodeType " +
                                     "FROM cpi_CompaniesNaceCodes cn LEFT JOIN cpi_NaceCodes c ON cn.NaceCodeID = c.NaceCodeID " +
                                     "WHERE cn.OrderLevel=0 AND CompanyCIID = " + companyCIID +
                                     " AND CurrentCode = 'True'";
                        OleDbCommand myOleDbCommand = new OleDbCommand(sql, myOleDbConn);
                        OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                        if (reader.Read()) {
                            CompanyNaceCodeBLLC theNace = new CompanyNaceCodeBLLC();
                            if (!reader.IsDBNull(0)) {
                                theNace.CreditInfoID = reader.GetInt32(0);
                            }
                            if (!reader.IsDBNull(1)) {
                                theNace.NaceCode = reader.GetString(1);
                            }
                            if (!reader.IsDBNull(2)) {
                                theNace.DescriptionNative = reader.GetString(2);
                            }
                            if (!reader.IsDBNull(3)) {
                                theNace.DescriptionEN = reader.GetString(3);
                            }
                            if (!reader.IsDBNull(4)) {
                                theNace.ShortDescriptionNative = reader.GetString(4);
                            }
                            if (!reader.IsDBNull(5)) {
                                theNace.ShortDescriptionEN = reader.GetString(5);
                            }
                            if (!reader.IsDBNull(6)) {
                                theNace.AbbreviationNative = reader.GetString(6);
                            }
                            if (!reader.IsDBNull(7)) {
                                theNace.AbbreviationEN = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8)) {
                                theNace.DefinitionNative = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9)) {
                                theNace.DefinitionEN = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10)) {
                                theNace.CodeType = reader.GetString(10);
                            }

                            //Now we need information on how many companies have the current nace code
                            if (theNace.CreditInfoID > 0) {
                                theNace.NumberOfCompaniesInNaceCode = GetNumberOfCompaniesInNace(theNace.NaceCode);
                            }
                            return theNace;
                        }
                    } catch (Exception e) {
                        Logger.WriteToLog(className + " : " + funcName, e, true);
                    }
                }
            }
            return null;
        }

        private int GetNumberOfCompaniesInNace(string nace) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    // �essi virkar r�tt
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "select count(*) from cpi_CompaniesNaceCodes where NaceCodeID = '" + nace + "'", myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        return reader.GetInt32(0);
                    }
                } catch (Exception exc) {
                    Logger.WriteToLog(className + "- Error finding number of companies in nace code " + nace, exc, true);
                }
                return -1;
            }
        }

        public DateTime GetStructureReportDate(int creditInfoID) {
            string funcName = "GetStructureReportDateAsString(int creditInfoID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    // �essi virkar r�tt
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "select StructureReportDate from cpi_CompanyReport where CompanyCIID = " + creditInfoID + "",
                            myOleDbConn);
                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            return reader.GetDateTime(0);
                        } else {
                            return DateTime.MinValue;
                        }
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                }
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// This function was specially added for the Cyprus structure report input module
        /// </summary>
        /// <param name="theHOR">Instance of HistoryOperationReviewBLLC</param>
        /// <param name="companyCIID">The companies internal system id</param>
        /// <returns></returns>
        public bool AddCompanyHistory_Structure(HistoryOperationReviewBLLC theHOR, int companyCIID) {
            string funcName = "AddCompanyHistory_Structure(HistoryOperationReviewBLLC theHOR, int companyCIID)";
            // athuga hvort n� �egar s� til History � �etta
            if (HistoryAlreadyExist(companyCIID)) {
                return UpdateCompanyHistory_Structure(theHOR, companyCIID);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_HistoryOperationReview(HistoryNative,HistoryEN,CompanyCIID) VALUES(?,?,?)";
            // cpi_HistoryOperationReview.HistoryNative,cpi_HistoryOperationReview.HistoryEN, cpi_HistoryOperationReview.OperationNative,cpi_HistoryOperationReview.OperationEN, cpi_HistoryOperationReview.CompanyReviewNative,cpi_HistoryOperationReview.CompanyReviewEN FROM cpi_HistoryOperationReview WHERE cpi_HistoryOperationReview.CompanyCIID
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        theHOR.HistoryNative,
                        theHOR.HistoryEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// This function is specially added for Cyprus structure report input module
        /// </summary>
        /// <param name="theHOR">Instance of HistoryOperationReviewBLLC</param>
        /// <param name="companyCIID">The comapnies internal system ID</param>
        /// <returns></returns>
        public bool UpdateCompanyHistory_Structure(HistoryOperationReviewBLLC theHOR, int companyCIID) {
            string funcName = "UpdateCompanyHistory_Structure(HistoryOperationReviewBLLC theHOR,int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            // myOleDbCommand.CommandText = "UPDATE cpi_HistoryOperationReview SET HistoryNative = '"+ theHOR.HistoryNative + "',HistoryEN = '"+ theHOR.HistoryEN + "',OperationNative = '"+ theHOR.OperationNative + "',OperationEN = '"+ theHOR.OperationEN + "',CompanyReviewNative = '"+ theHOR.CompanyReviewNative + "',CompanyReviewEN = '"+ theHOR.CompanyReviewEN + "' WHERE CompanyCIID = "+ companyCIID +"";

            myOleDbCommand.CommandText =
                "UPDATE cpi_HistoryOperationReview SET HistoryNative = ?, HistoryEN = ? WHERE CompanyCIID = ?";
            // HistoryNative,HistoryEN,OperationNative,OperationEN,CompanyReviewNative,CompanyReviewEN,CompanyCIID
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam;

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        theHOR.HistoryNative,
                        theHOR.HistoryEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Value = companyCIID;
                    myParam.Direction = ParameterDirection.Input;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Takes Companies national ID and returns instance of ReportCompany. Specially implemented for Lithuania
        /// </summary>
        /// <param name="companyCIID">The companies national ID</param>
        /// <param name="CIID">The companies internal system id</param>
        /// <param name="nationalID">indicates wether to use nationalID or CIID</param>
        /// <returns>Instance of ReportCompanyBLLC</returns>
        public ReportCompanyBLLC GetLTCompanyReport(string nationalID, int CIID, bool useNationalID) {
            // Ltihuanian specific ... add those specific values into the one and only ReportBLLC
            //	CompanyReport theReportCompany  = CompanyReport();
            // like this
            ReportCompanyBLLC theReportCompany = new ReportCompanyBLLC();
            string funcName = "GetCompanyReport(string creditInfoID) ";

            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT ros_NationalCompanyInterfaceView.IDNumber, ros_NationalCompanyInterfaceView.Name, " +
                            " ros_NationalCompanyInterfaceView.Org_name_status_code, " +
                            " ler.NameNative AS lerStatusNative, ler.NameEN AS lerStatusEN, " +
                            " sler.NameNative AS slerStatusNative, sler.NameEN AS slerStatusEN, " +
                            " np_City.NameNative AS CityNative, np_City.NameEN AS CityEN, " +
                            " CASE WHEN shadow.dbo.findeksa.telefonas IS NOT NULL THEN shadow.dbo.findeksa.telefonas ELSE " +
                            " (CASE WHEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) IS NOT NULL THEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) ELSE cast(ros_NationalCompanyInterfaceView.PhoneNumber AS nvarchar) END)END AS TELEF, " +
                            " CASE WHEN shadow.dbo.findeksa.faksas IS NOT NULL THEN rtrim(shadow.dbo.findeksa.faksas) ELSE cast (ros_NationalCompanyInterfaceView.FaxNumber AS nvarchar) END AS FAX, " +
                            " ros_NationalCompanyInterfaceView.Address AS JURIDICAL_ADDRESS, " +
                            " ros_NationalCompanyInterfaceView.vadov, ros_NationalCompanyInterfaceView.vad_adres, ros_NationalCompanyInterfaceView.vad_telef, " +
                            " ros_NationalCompanyInterfaceView.ist_data, ros_NationalCompanyInterfaceView.reg_data, " +
                            " shadow.dbo.findeksa.mobilus AS MOBILE, " +
                            " CASE WHEN shadow.dbo.findeksa.adresas IS NOT NULL THEN shadow.dbo.findeksa.adresas ELSE " +
                            " (CASE WHEN ros_NationalCompanyInterfaceView.adres IS NOT NULL THEN ros_NationalCompanyInterfaceView.adres ELSE ros_NationalCompanyInterfaceView.Address END)END AS OFFICE_ADDRESS, " +
                            " CASE WHEN shadow.dbo.findeksa.email IS NOT NULL THEN rtrim(shadow.dbo.findeksa.email) ELSE ros_NationalCompanyInterfaceView.email END AS EMAIL, " +
                            " shadow.dbo.findeksa.www AS F_WWW, shadow.dbo.klas_nace.pavad, shadow.dbo.klas_nace.pavad_ENG, ros_NationalCompanyInterfaceView.Org_nameEN_status_code" +
                            " FROM ros_NationalCompanyInterfaceView " +
                            " LEFT OUTER JOIN np_City ON substring(cast(ros_NationalCompanyInterfaceView.City_ID AS char),1,2) = np_City.CityID " +
                            " LEFT OUTER JOIN cpi_StatusCodes ler ON ler.StatusID = ros_NationalCompanyInterfaceView.LERStatus_ID " +
                            " LEFT OUTER JOIN cpi_StatusCodes sler ON sler.StatusID = ros_NationalCompanyInterfaceView.SLERStatus_ID " +
                            " LEFT OUTER JOIN shadow.dbo.findeksa ON shadow.dbo.findeksa.imone = ros_NationalCompanyInterfaceView.IDNumber " +
                            " LEFT OUTER JOIN shadow.dbo.klas_nace ON shadow.dbo.klas_nace.kodas = ros_NationalCompanyInterfaceView.veikla " +
                            " WHERE IDNumber = " + CIID,
                            myOleDbConn);

                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            theReportCompany.NUniqueID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theReportCompany.NameNative = reader.GetString(1);
                            theReportCompany.NameEN = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theReportCompany.TypeNative = reader.GetString(2);
                            theReportCompany.TypeEN = reader.GetString(23);
                        }
                        if (!reader.IsDBNull(3)) {
                            theReportCompany.LERStatusNative = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            theReportCompany.LERStatusEN = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            theReportCompany.SLERStatusNative = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            theReportCompany.SLERStatusEN = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            theReportCompany.CityNative = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            theReportCompany.CityEN = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            theReportCompany.Tel = reader.GetString(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            theReportCompany.Fax = reader.GetString(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            theReportCompany.JurAddress = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            theReportCompany.Director = reader.GetString(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            theReportCompany.DirectorAddress = reader.GetString(13);
                        }
                        if (!reader.IsDBNull(14)) {
                            theReportCompany.DirectorPhone = reader.GetString(14);
                        }
                        if (!reader.IsDBNull(15)) {
                            theReportCompany.SEstablished = reader.GetString(15);
                        }
                        if (!reader.IsDBNull(16)) {
                            theReportCompany.SRegistered = reader.GetString(16);
                        }
                        if (!reader.IsDBNull(17)) {
                            theReportCompany.Mobile = reader.GetString(17);
                        }
                        if (!reader.IsDBNull(18)) {
                            theReportCompany.OfficeAddress = reader.GetString(18);
                        }
                        if (!reader.IsDBNull(19)) {
                            theReportCompany.Email = reader.GetString(19);
                        }
                        if (!reader.IsDBNull(20)) {
                            theReportCompany.WWW = reader.GetString(20);
                        }
                        if (!reader.IsDBNull(21)) {
                            theReportCompany.NaceCodeName = reader.GetString(21);
                            theReportCompany.NaceCodeNameEN = reader.GetString(22);
                        }
                        //if(!reader.IsDBNull(15))
                        //	theReportCompany.Established = reader.GetDateTime(15);
                        //if(!reader.IsDBNull(16))
                        //	theReportCompany.Registered = reader.GetDateTime(16);

                        //theReportCompany.PNumbers = userDALC.GetPhoneNumberList(creditInfoID);
                        //theReportCompany.Address = userDALC.GetAddressList(creditInfoID);
                        //theReportCompany.ContactPerson = this.GetContactPerson(creditInfoID);
                    }
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    return null;
                }
            }

            return theReportCompany;
        }


        # region Company_Laywers

        /// <summary>
        /// Takes an instance of LaywerIndividual and adds to database
        /// </summary>
        /// <param name="theShareholder">Instance of LaywerIndividualBLLC</param>
        /// <param name="companyCIID">The internal CIID number of the company</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddLaywer(LaywerIndividualBLLC theLaywer, int companyCIID) {
            string funcName = "AddLaywer(LaywerIndividualBLLC theLaywer, int companyCIID)";
            try {
                int laywerCIID = -1;
                if (theLaywer.CreditInfoID > 0) {
                    laywerCIID = theLaywer.CreditInfoID;
                } else {
                    laywerCIID = GetCIIDByNationalID(theLaywer.NationalID);
                }

                //If no creditinfoid found, then we need to create the user
                if (laywerCIID <= 0) {
                    CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
                    Indivitual theIndi = new Indivitual();

                    theIndi.FirstNameNative = theLaywer.FirstNameNative;
                    theIndi.SurNameNative = theLaywer.SurNameNative;
                    theIndi.FirstNameEN = theLaywer.FirstNameEN;
                    theIndi.SurNameEN = theLaywer.SurNameEN;

                    IDNumber theIDNumber = new IDNumber();
                    theIDNumber.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                    theIDNumber.Number = theLaywer.NationalID;

                    theIndi.IDNumbers = new ArrayList();
                    theIndi.IDNumbers.Add(theIDNumber);

                    theIndi.EducationID = 0; // meaning not accessible
                    theIndi.ProfessionID = 0; // meaning not accessible
                    theIndi.Number = new ArrayList();
                    theIndi.Type = CigConfig.Configure("lookupsettings.individualID"); // "2"; // meaning Indivitual

                    theIndi.Address = theLaywer.Address;

                    try {
                        laywerCIID = theUserDAL.AddNewIndivitual(theIndi);
                    } catch (Exception exc) {
                        Logger.WriteToLog(
                            className + ":" + funcName + " -- Error when inserting new individual", exc, true);
                    }
                }
                //ok - now we should have creditinfo id
                if (laywerCIID > 0) {
                    return InsertLaywer(
                        laywerCIID,
                        companyCIID,
                        theLaywer.OfficeNumber,
                        theLaywer.Building,
                        theLaywer.HistoryNative,
                        theLaywer.HistoryEN);
                } else {
                    return false;
                }
            } catch (Exception exc) {
                Logger.WriteToLog(className + ":" + funcName, exc, true);
                return false;
            }
        }

        /// <summary>
        /// Takes an instance of LaywerCompany and adds to database
        /// </summary>
        /// <param name="theShareholder">Instance of LaywerCompanyBLLC</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddLaywer(LaywerCompanyBLLC theLaywer, int companyCIID) {
            string funcName = "AddLaywer(LaywerIndividualBLLC theLaywer, int companyCIID)";
            try {
                int laywerCIID = -1;
                if (theLaywer.CreditInfoID > 0) {
                    laywerCIID = theLaywer.CreditInfoID;
                } else {
                    laywerCIID = GetCIIDByNationalID(theLaywer.NationalID);
                }

                //If no creditinfoid found, then we need to create the user
                if (laywerCIID <= 0) {
                    CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
                    Company theComp = new Company();

                    theComp.NameNative = theLaywer.NameNative;
                    theComp.NameEN = theLaywer.NameEN;
                    theComp.Org_status_code = 1; // meaning not accessible
                    IDNumber theIDNumber = new IDNumber();
                    theIDNumber.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                    theIDNumber.Number = theLaywer.NationalID;

                    theComp.IDNumbers = new ArrayList();
                    theComp.IDNumbers.Add(theIDNumber);

                    theComp.Number = new ArrayList();
                    theComp.Type = CigConfig.Configure("lookupsettings.companyID"); // "2"; // meaning Indivitual

                    theComp.Address = theLaywer.Address;

                    try {
                        laywerCIID = theUserDAL.AddCompany(theComp);
                    } catch (Exception exc) {
                        Logger.WriteToLog(
                            className + ":" + funcName + " -- Error when inserting new individual", exc, true);
                    }
                }
                //ok - now we should have creditinfo id
                if (laywerCIID > 0) {
                    return InsertLaywer(
                        laywerCIID,
                        companyCIID,
                        theLaywer.OfficeNumber,
                        theLaywer.Building,
                        theLaywer.HistoryNative,
                        theLaywer.HistoryEN);
                } else {
                    return false;
                }
            } catch (Exception exc) {
                Logger.WriteToLog(className + ":" + funcName, exc, true);
                return false;
            }
        }

        /// <summary>
        /// Inserts a laywer into the database
        /// </summary>
        /// <param name="laywerCIID">The id of the laywer</param>
        /// <param name="companyCIID">The company id of the laywer</param>
        /// <param name="officeNumber">The office number</param>
        /// <param name="building">The building</param>
        /// <param name="historyNative">The laywer history</param>
        /// <param name="historyEN">The laywer english history</param>
        /// <returns>True if inserted ok, false otherwise</returns>
        public bool InsertLaywer(
            int laywerCIID,
            int companyCIID,
            string officeNumber,
            string building,
            string historyNative,
            string historyEN) {
            string funcName =
                "InsertLaywer(int laywerCIID, int companyCIID, string officeNumber, string building, string historyNative, string historyEN)";
            // if the board member is already registed for the company the he should be updated 
            if (IsLaywerAlreadyRegisted(laywerCIID, companyCIID)) {
                return UpdateLaywer(laywerCIID, companyCIID, officeNumber, building, historyNative, historyEN);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_CompanyLaywers(LaywerCIID, CompanyCIID, OfficeNumber, Building, HistoryNative, HistoryEN) VALUES(?,?,?,?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();

                    myOleDbCommand.Parameters.Clear();

                    myParam = new OleDbParameter("LaywerCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = laywerCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("OfficeNumber", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = officeNumber;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Building", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = building;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        historyNative,
                        historyEN,
                        OleDbType.WChar);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// updates a laywer in the database
        /// </summary>
        /// <param name="laywerCIID">The id of the laywer</param>
        /// <param name="companyCIID">The company id of the laywer</param>
        /// <param name="officeNumber">The office number</param>
        /// <param name="building">The building</param>
        /// <param name="historyNative">The laywer history</param>
        /// <param name="historyEN">The laywer english history</param>
        /// <returns>True if updated ok, false otherwise</returns>
        public bool UpdateLaywer(
            int laywerCIID,
            int companyCIID,
            string officeNumber,
            string building,
            string historyNative,
            string historyEN) {
            string funcName =
                "UpdateLaywer(int laywerCIID, int companyCIID, string officeNumber, string building, string historyNative, string historyEN)";
            //Is the secretary already registered?
            if (!IsLaywerAlreadyRegisted(laywerCIID, companyCIID)) {
                return InsertLaywer(laywerCIID, companyCIID, officeNumber, building, historyNative, historyEN);
            }

            string tmpQ =
                "UPDATE cpi_CompanyLaywers SET OfficeNumber = ?, Building = ?, HistoryNative = ?, HistoryEN = ? WHERE CompanyCIID = ? and LaywerCIID = ?";

            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = tmpQ;
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    myOleDbCommand.Parameters.Clear();
                    OleDbParameter myParam = new OleDbParameter();

                    myParam = new OleDbParameter("OfficeNumber", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = officeNumber;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Building", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = building;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        historyNative,
                        historyEN,
                        OleDbType.WChar);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("LaywerCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = laywerCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Removes given laywer from given company
        /// </summary>
        /// <param name="laywerCIID">The id of the laywer</param>
        /// <param name="companyCIID">The company to remove laywer from</param>		
        /// <returns>True if laywer removed successfully, false otherwise</returns>
        public bool DeleteLaywer(int laywerCIID, int companyCIID) {
            string funcName = "DeleteLaywer(int laywerCIID,int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_CompanyLaywers WHERE LaywerCIID = " + laywerCIID +
                                         " AND CompanyCIID = " + companyCIID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Checks if laywer with given id exists for given company
        /// </summary>
        /// <param name="laywerCIID">The id of the laywer</param>
        /// <param name="companyCIID">The id of the company</param>
        /// <returns>True if the laywer exists, false otherwise</returns>
        protected bool IsLaywerAlreadyRegisted(int laywerCIID, int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_CompanyLaywers WHERE CompanyCIID = " + companyCIID + " AND LaywerCIID = " +
                        laywerCIID,
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// This function returns all Company layers as DataSet
        /// </summary>
        /// <param name="companyCIID">The company internal system id</param>
        /// <returns>All laywers as DataSet</returns>
        public DataSet GetLaywersAsDataSet(int companyCIID) {
            DataSet dsLaywers = new DataSet();
            string funcName = "GetLaywersAsDataSet(int companyCIID) ";
            
            string query =
                "SELECT DISTINCT np_CreditInfoUser.CreditInfoID, np_CreditInfoUser.Type, np_Companys.NameNative,np_Companys.NameEN, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number,cpi_CompanyLaywers.OfficeNumber, cpi_CompanyLaywers.Building, cpi_CompanyLaywers.HistoryNative, cpi_CompanyLaywers.HistoryEN, np_Address.StreetNative, np_Address.StreetEN " +
                "FROM np_CreditInfoUser LEFT OUTER JOIN np_Individual ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Address ON np_CreditInfoUser.CreditInfoID = np_Address.CreditInfoID " +
                "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_CompanyLaywers ON np_CreditInfoUser.CreditInfoID = cpi_CompanyLaywers.LaywerCIID " +
                "WHERE np_CreditInfoUser.CreditInfoID IN (SELECT cpi_CompanyLaywers.LaywerCIID FROM cpi_CompanyLaywers WHERE cpi_CompanyLaywers.CompanyCIID = " +
                companyCIID + ") AND cpi_CompanyLaywers.CompanyCIID = " + companyCIID +
                " ORDER BY cpi_CompanyLaywers.OfficeNumber DESC";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsLaywers);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
            }
            return dsLaywers;
        }

        #endregion

        # region Company_Auditors

        /// <summary>
        /// Takes an instance of AuditorIndividual and adds to database
        /// </summary>
        /// <param name="theShareholder">Instance of AuditorIndividualBLLC</param>
        /// <param name="companyCIID">The internal CIID number of the company</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddAuditor(AuditorIndividualBLLC theAuditor, int companyCIID) {
            string funcName = "AddAuditor(AuditorIndividualBLLC theAuditor, int companyCIID)";
            try {
                int auditorCIID = -1;
                if (theAuditor.CreditInfoID > 0) {
                    auditorCIID = theAuditor.CreditInfoID;
                } else {
                    auditorCIID = GetCIIDByNationalID(theAuditor.NationalID);
                }

                //If no creditinfoid found, then we need to create the user
                if (auditorCIID <= 0) {
                    CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
                    Indivitual theIndi = new Indivitual();

                    theIndi.FirstNameNative = theAuditor.FirstNameNative;
                    theIndi.SurNameNative = theAuditor.SurNameNative;
                    theIndi.FirstNameEN = theAuditor.FirstNameEN;
                    theIndi.SurNameEN = theAuditor.SurNameEN;

                    IDNumber theIDNumber = new IDNumber();
                    theIDNumber.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                    theIDNumber.Number = theAuditor.NationalID;

                    theIndi.IDNumbers = new ArrayList();
                    theIndi.IDNumbers.Add(theIDNumber);

                    theIndi.EducationID = 1; // meaning not accessible
                    theIndi.ProfessionID = 1; // meaning not accessible
                    theIndi.Number = new ArrayList();
                    theIndi.Type = CigConfig.Configure("lookupsettings.individualID"); // "2"; // meaning Indivitual

                    theIndi.Address = theAuditor.Address;

                    try {
                        auditorCIID = theUserDAL.AddNewIndivitual(theIndi);
                    } catch (Exception exc) {
                        Logger.WriteToLog(
                            className + ":" + funcName + " -- Error when inserting new individual", exc, true);
                    }
                }
                //ok - now we should have creditinfo id
                if (auditorCIID > 0) {
                    return InsertAuditor(
                        auditorCIID,
                        companyCIID,
                        theAuditor.OfficeNumber,
                        theAuditor.Building,
                        theAuditor.HistoryNative,
                        theAuditor.HistoryEN);
                } else {
                    return false;
                }
            } catch (Exception exc) {
                Logger.WriteToLog(className + ":" + funcName, exc, true);
                return false;
            }
        }

        /// <summary>
        /// Takes an instance of AuditorCompany and adds to database
        /// </summary>
        /// <param name="theShareholder">Instance of AutitorCompanyBLLC</param>
        /// <returns>true if ok, false otherwise</returns>
        public bool AddAuditor(AuditorCompanyBLLC theAuditor, int companyCIID) {
            string funcName = "AddAuditor(AutitorCompanyBLLC theAuditor, int companyCIID)";
            try {
                int auditorCIID = -1;
                if (theAuditor.CreditInfoID > 0) {
                    auditorCIID = theAuditor.CreditInfoID;
                } else {
                    auditorCIID = GetCIIDByNationalID(theAuditor.NationalID);
                }

                //If no creditinfoid found, then we need to create the user
                if (auditorCIID <= 0) {
                    CreditInfoUserDALC theUserDAL = new CreditInfoUserDALC();
                    Company theComp = new Company();

                    theComp.NameNative = theAuditor.NameNative;
                    theComp.NameEN = theAuditor.NameEN;
                    theComp.Org_status_code = 1; // meaning not accessible
                    IDNumber theIDNumber = new IDNumber();
                    theIDNumber.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                    theIDNumber.Number = theAuditor.NationalID;

                    theComp.IDNumbers = new ArrayList();
                    theComp.IDNumbers.Add(theIDNumber);

                    theComp.Number = new ArrayList();
                    theComp.Type = CigConfig.Configure("lookupsettings.companyID"); // "2"; // meaning Indivitual

                    theComp.Address = theAuditor.Address;

                    try {
                        auditorCIID = theUserDAL.AddCompany(theComp);
                    } catch (Exception exc) {
                        Logger.WriteToLog(
                            className + ":" + funcName + " -- Error when inserting new individual", exc, true);
                    }
                }
                //ok - now we should have creditinfo id
                if (auditorCIID > 0) {
                    return InsertAuditor(
                        auditorCIID,
                        companyCIID,
                        theAuditor.OfficeNumber,
                        theAuditor.Building,
                        theAuditor.HistoryNative,
                        theAuditor.HistoryEN);
                } else {
                    return false;
                }
            } catch (Exception exc) {
                Logger.WriteToLog(className + ":" + funcName, exc, true);
                return false;
            }
        }

        /// <summary>
        /// Inserts a auditor into the database
        /// </summary>
        /// <param name="laywerCIID">The id of the auditor</param>
        /// <param name="companyCIID">The company id of the auditor</param>
        /// <param name="officeNumber">The office number</param>
        /// <param name="building">The building</param>
        /// <param name="historyNative">The auditor history</param>
        /// <param name="historyEN">The auditor english history</param>
        /// <returns>True if inserted ok, false otherwise</returns>
        public bool InsertAuditor(
            int auditorCIID,
            int companyCIID,
            string officeNumber,
            string building,
            string historyNative,
            string historyEN) {
            string funcName =
                "InsertAuditor(int auditorCIID, int companyCIID, string officeNumber, string building, string historyNative, string historyEN)";
            // if the board member is already registed for the company the he should be updated 
            if (IsAuditorAlreadyRegisted(auditorCIID, companyCIID)) {
                return UpdateAuditor(auditorCIID, companyCIID, officeNumber, building, historyNative, historyEN);
            }
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_CompanyAuditors(AuditorCIID, CompanyCIID, OfficeNumber, Building, HistoryNative, HistoryEN) VALUES(?,?,?,?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();
                    myParam = new OleDbParameter("AuditorCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = auditorCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("OfficeNumber", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = officeNumber;
                    myOleDbCommand.Parameters.Add(myParam);
                    myParam = new OleDbParameter("Building", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = building;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        historyNative,
                        historyEN,
                        OleDbType.VarChar);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// updates a auditor in the database
        /// </summary>
        /// <param name="laywerCIID">The id of the auditor</param>
        /// <param name="companyCIID">The company id of the auditor</param>
        /// <param name="officeNumber">The office number</param>
        /// <param name="building">The building</param>
        /// <param name="historyNative">The auditor history</param>
        /// <param name="historyEN">The auditor english history</param>
        /// <returns>True if updated ok, false otherwise</returns>
        public bool UpdateAuditor(
            int auditorCIID,
            int companyCIID,
            string officeNumber,
            string building,
            string historyNative,
            string historyEN) {
            string funcName =
                "UpdateAuditor(int auditorCIID, int companyCIID, string officeNumber, string building, string historyNative, string historyEN)";
            //Is the secretary already registered?
            if (!IsAuditorAlreadyRegisted(auditorCIID, companyCIID)) {
                return InsertAuditor(auditorCIID, companyCIID, officeNumber, building, historyNative, historyEN);
            }

            string tmpQ =
                "UPDATE cpi_CompanyAuditors SET OfficeNumber = ?, Building = ?, HistoryNative = ?, HistoryEN = ? WHERE CompanyCIID = ? and AuditorCIID = ?";

            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = tmpQ;
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();

                    myParam = new OleDbParameter("OfficeNumber", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = officeNumber;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Building", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = building;
                    myOleDbCommand.Parameters.Add(myParam);

                    // Insert Native and EN strings
                    CreateParameterFromString(
                        myOleDbCommand,
                        "HistoryNative",
                        "HistoryEN",
                        ParameterDirection.Input,
                        historyNative,
                        historyEN,
                        OleDbType.VarChar);

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("AuditorCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = auditorCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Removes given auditor from given company
        /// </summary>
        /// <param name="laywerCIID">The id of the auditor</param>
        /// <param name="companyCIID">The company to remove auditor from</param>		
        /// <returns>True if auditor removed successfully, false otherwise</returns>
        public bool DeleteAuditor(int auditorCIID, int companyCIID) {
            string funcName = "DeleteAuditor(int auditorCIID,int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_CompanyAuditors WHERE AuditorCIID = " + auditorCIID +
                                         " AND CompanyCIID = " + companyCIID + "";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Checks if auditor with given id exists for given company
        /// </summary>
        /// <param name="laywerCIID">The id of the auditor</param>
        /// <param name="companyCIID">The id of the company</param>
        /// <returns>True if the auditor exists, false otherwise</returns>
        protected bool IsAuditorAlreadyRegisted(int auditorCIID, int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand(
                        "SELECT * FROM cpi_CompanyAuditors WHERE CompanyCIID = " + companyCIID + " AND AuditorCIID = " +
                        auditorCIID,
                        myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// This function returns all company auditors as DataSet
        /// </summary>
        /// <param name="companyCIID">The company internal system id</param>
        /// <returns>All auditors as DataSet</returns>
        public DataSet GetAuditorsAsDataSet(int companyCIID) {
            DataSet dsAuditors = new DataSet();
            string funcName = "GetAuditorsAsDataSet(int companyCIID) ";
            
            string query =
                "SELECT DISTINCT np_CreditInfoUser.CreditInfoID, np_CreditInfoUser.Type, np_Companys.NameNative,np_Companys.NameEN, np_Individual.FirstNameNative, np_Individual.FirstNameEN, " +
                "np_Individual.SurNameNative,np_Individual.SurNameEN, np_IDNumbers.Number,cpi_CompanyAuditors.OfficeNumber, cpi_CompanyAuditors.Building, cpi_CompanyAuditors.HistoryNative, cpi_CompanyAuditors.HistoryEN, np_Address.StreetNative, np_Address.StreetEN " +
                "FROM np_CreditInfoUser LEFT OUTER JOIN np_Individual ON np_Individual.CreditInfoID = np_CreditInfoUser.CreditInfoID LEFT OUTER JOIN np_Companys ON np_CreditInfoUser.CreditInfoID = np_Companys.CreditInfoID LEFT OUTER JOIN np_Address ON np_CreditInfoUser.CreditInfoID = np_Address.CreditInfoID " +
                "LEFT OUTER JOIN np_IDNumbers ON np_CreditInfoUser.CreditInfoID = np_IDNumbers.CreditInfoID LEFT OUTER JOIN cpi_CompanyAuditors ON np_CreditInfoUser.CreditInfoID = cpi_CompanyAuditors.AuditorCIID " +
                "WHERE np_CreditInfoUser.CreditInfoID IN (SELECT cpi_CompanyAuditors.AuditorCIID FROM cpi_CompanyAuditors WHERE cpi_CompanyAuditors.CompanyCIID = " +
                companyCIID + ") AND cpi_CompanyAuditors.CompanyCIID = " + companyCIID +
                " ORDER BY cpi_CompanyAuditors.OfficeNumber DESC";
            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(dsAuditors);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName + err.Message, true);
            }
            return dsAuditors;
        }

        #endregion

        #region Charges

        public bool AddCharges(ChargesBLLC myCharges, int companyCIID) {
            string funcName = "AddCharges(Charges myCharges, int companyCIID)";

            if (myCharges.ID > -1) {
                return UpdateCharges(myCharges);
            }

//			if(this.IsChargesAlreadyRegistered(companyCIID))
//				return this.UpdateCharges(myCharges, companyCIID);

            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_Charges(CompanyCIID, DateRegistered, DatePrepared, DescriptionID, Amount, Sequence, BankID, CurrencyCode, DescriptionFreeTextNative, DescriptionFreeTextEN) VALUES(?,?,?,?,?,?,?,?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DateRegistered", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCharges.DateRegistered.Year > 1899) {
                        myParam.Value = myCharges.DateRegistered;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DatePrepared", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCharges.DatePrepared.Year > 1899) {
                        myParam.Value = myCharges.DatePrepared;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DescriptionID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.DescriptionID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Amount", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCharges.Amount > -1) {
                        myParam.Value = myCharges.Amount;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Sequence", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.Sequence;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("BankID", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.BankID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CurrencyCode", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.CurrencyCode;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DescriptionFreeTextNative", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.DescriptionFreeTextNative;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DescriptionFreeTextEN", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.DescriptionFreeTextEN;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        public bool UpdateCharges(ChargesBLLC myCharges) {
            string funcName = "UpdateCharges(CPI.BLL.Charges myCharges)";

            OleDbCommand myOleDbCommand = new OleDbCommand();

            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();

                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "UPDATE cpi_Charges SET DateRegistered = ?,DatePrepared = ?,DescriptionID= ?, Amount = ?, Sequence = ?, BankID = ?, CurrencyCode = ?, DescriptionFreeTextNative = ?, DescriptionFreeTextEN = ?  WHERE ID =" +
                            myCharges.ID,
                            myOleDbConn);
                    myOleDbCommand.Transaction = myTrans;

                    OleDbParameter myParam = new OleDbParameter("DateRegistered", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCharges.DateRegistered.Year > 1899) {
                        myParam.Value = myCharges.DateRegistered;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DatePrepared", OleDbType.Date);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCharges.DatePrepared.Year > 1899) {
                        myParam.Value = myCharges.DatePrepared;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DescriptionID", OleDbType.Integer);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.DescriptionID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Amount", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCharges.Amount > -1) {
                        myParam.Value = myCharges.Amount;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Sequence", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.Sequence;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("BankID", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.BankID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CurrencyCode", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.CurrencyCode;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DescriptionFreeTextNative", OleDbType.WChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.DescriptionFreeTextNative;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("DescriptionFreeTextEN", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCharges.DescriptionFreeTextEN;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    myTrans.Commit();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        public bool DeleteCharges(int ID) {
            string funcName = "DeleteCharges(int ID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_Charges WHERE ID = " + ID;
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    return false;
                }
            }
            return true;
        }

        public ChargesBLLC GetCharges(int companyCIID) {
            string funcName = "GetCharges(int companyCIID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand("SELECT * FROM cpi_Charges WHERE CompanyCIID = " + companyCIID, myOleDbConn);

                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    ChargesBLLC myCharges = new ChargesBLLC();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            myCharges.CreditInfoID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            myCharges.DateRegistered = reader.GetDateTime(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            myCharges.DatePrepared = reader.GetDateTime(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            myCharges.DescriptionID = reader.GetInt32(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            myCharges.Amount = reader.GetDecimal(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            myCharges.Sequence = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            myCharges.BankID = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            myCharges.CurrencyCode = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            myCharges.DescriptionFreeTextNative = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            myCharges.DescriptionFreeTextEN = reader.GetString(9);
                        }
                    }

                    return myCharges;
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public DataSet GetChargesAsDataSet(int companyCIID) {
            DataSet mySet = new DataSet();
            string funcName = "GetChargesAsDataSet(int companyCIID) ";
            

            string query =
                "SELECT cpi_Charges.ID, cpi_Charges.DateRegistered, cpi_Charges.DatePrepared, cpi_Charges.DescriptionID, cpi_ChargesDescription.DescriptionNative, " +
                "cpi_ChargesDescription.DescriptionEN, cpi_Charges.BankID, cpi_Banks.NameNative, cpi_Banks.NameEN, cpi_Charges.Amount, " +
                "cpi_Charges.CurrencyCode, cpi_Charges.Sequence, cpi_Charges.DescriptionFreeTextNative,cpi_Charges.DescriptionFreeTextEN FROM cpi_Charges INNER JOIN cpi_Banks ON cpi_Charges.BankID = cpi_Banks.BankID " +
                "INNER JOIN cpi_ChargesDescription ON cpi_Charges.DescriptionID = cpi_ChargesDescription.DescriptionID " +
                "WHERE cpi_charges.CompanyCIID = " + companyCIID;

            try {
                using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    OleDbDataAdapter myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(query, myOleDbConn);
                    myAdapter.Fill(mySet);
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
                return null;
            }
            return mySet;
        }

        public bool IsChargesAlreadyRegistered(int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand("SELECT * FROM cpi_Charges WHERE CompanyCIID = " + companyCIID, myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        public DataSet GetCurrencyAsDataSet() {
            string funcName = "GetCurrencyAsDataSet()";
            string theConnectionString = DatabaseHelper.ConnectionString();
            OleDbCommand theDBCommand;

            string sqlQuery =
                "select CurrencyCode, CurrencyCode+' - '+CurrencyDescriptionEN As CurrencyDescription  from billing_Currency";

            using (OleDbConnection theConnection = new OleDbConnection(theConnectionString)) {
                try {
                    theConnection.Open();
                    theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    OleDbDataAdapter theAdapter = new OleDbDataAdapter();
                    theAdapter.SelectCommand = theDBCommand;

                    //Return the results in a DataSet
                    DataSet theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                } finally {
                    // Ef �a� �arf a� ganga fr� einhverju �� � a� gera �a� h�r. � ekki a� �urfa me� "using" statements.
                }
            }
        }

        public DataSet GetChargesDescriptionAsDataSet() {
            string funcName = "GetChargesDescriptionAsDataSet()";
            string theConnectionString = DatabaseHelper.ConnectionString();
            OleDbCommand theDBCommand;

            string sqlQuery = "Select * from cpi_ChargesDescription";

            using (OleDbConnection theConnection = new OleDbConnection(theConnectionString)) {
                try {
                    theConnection.Open();
                    theDBCommand = new OleDbCommand(sqlQuery, theConnection);

                    //Create new adapter that receives theDBCommand with params
                    OleDbDataAdapter theAdapter = new OleDbDataAdapter();
                    theAdapter.SelectCommand = theDBCommand;

                    //Return the results in a DataSet
                    DataSet theSet = new DataSet();
                    theAdapter.Fill(theSet);
                    return theSet;
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                } finally {
                    // Ef �a� �arf a� ganga fr� einhverju �� � a� gera �a� h�r. � ekki a� �urfa me� "using" statements.
                }
            }
        }

        #endregion

        #region Capital

        public bool AddCapital(CapitalBLLC myCapital, int companyCIID) {
            string funcName = "AddCapital(CapitalBLLC myCapital, int companyCIID)";

            if (IsCapitalAlreadyRegistered(companyCIID)) {
                return UpdateCapital(myCapital, companyCIID);
            }

            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_Capital(CompanyCIID, AuthorizedCapital, IssuedNumberOfShares, Asked, PaidUp, NominalNumberOfShares, SharesDescription, CurrencyCode, ShareClass) VALUES(?,?,?,?,?,?,?,?,?)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    OleDbParameter myParam = new OleDbParameter();
                    myOleDbCommand.Parameters.Clear();

                    myParam = new OleDbParameter("CompanyCIID", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = companyCIID;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("AuthorizedCapital", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.AuthorizedCapital > -1) {
                        myParam.Value = myCapital.AuthorizedCapital;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("IssuedNumberOfShares", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.IssuedNumberOfShares > -1) {
                        myParam.Value = myCapital.IssuedNumberOfShares;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Asked", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.Asked > -1) {
                        myParam.Value = myCapital.Asked;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("PaidUp", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.PaidUp > -1) {
                        myParam.Value = myCapital.PaidUp;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("NominalNumberOfShares", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.NominalNumberOfShares > -1) {
                        myParam.Value = myCapital.NominalNumberOfShares;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("SharesDescription", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCapital.SharesDescription;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CurrencyCode", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCapital.CurrencyCode;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("ShareClass", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCapital.ShareClass;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;
        }

        public bool UpdateCapital(CapitalBLLC myCapital, int companyCIID) {
            string funcName = "UpdateCapital(CapitalBLLC myCapital, int companyCIID)";

            OleDbCommand myOleDbCommand = new OleDbCommand();

            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();
                // begin transaction...
                OleDbTransaction myTrans = myOleDbConn.BeginTransaction();

                try {
                    myOleDbCommand =
                        new OleDbCommand(
                            "UPDATE cpi_Capital SET AuthorizedCapital = ?,IssuedNumberOfShares = ?,Asked= ?, PaidUp = ?, NominalNumberOfShares = ?, SharesDescription = ?, CurrencyCode = ?, ShareClass= ? WHERE CompanyCIID =" +
                            companyCIID,
                            myOleDbConn);
                    myOleDbCommand.Transaction = myTrans;

                    OleDbParameter myParam = new OleDbParameter("AuthorizedCapital", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.AuthorizedCapital > -1) {
                        myParam.Value = myCapital.AuthorizedCapital;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("IssuedNumberOfShares", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.IssuedNumberOfShares > -1) {
                        myParam.Value = myCapital.IssuedNumberOfShares;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("Asked", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.Asked > -1) {
                        myParam.Value = myCapital.Asked;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("PaidUp", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.PaidUp > -1) {
                        myParam.Value = myCapital.PaidUp;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("NominalNumberOfShares", OleDbType.Decimal);
                    myParam.Direction = ParameterDirection.Input;
                    if (myCapital.NominalNumberOfShares > -1) {
                        myParam.Value = myCapital.NominalNumberOfShares;
                    } else {
                        myParam.Value = null;
                    }
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("SharesDescription", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCapital.SharesDescription;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("CurrencyCode", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCapital.CurrencyCode;
                    myOleDbCommand.Parameters.Add(myParam);

                    myParam = new OleDbParameter("ShareClass", OleDbType.VarChar);
                    myParam.Direction = ParameterDirection.Input;
                    myParam.Value = myCapital.ShareClass;
                    myOleDbCommand.Parameters.Add(myParam);

                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();

                    myTrans.Commit();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName, err, true);
                    return false;
                }
            }
            return true;

            /*			string funcName = "UpdateCapital(CapitalBLLC myCapital, int companyCIID)";

                        string tmpQ = "UPDATE cpi_Capital SET AuthorizedCapital = "+ myCapital.AuthorizedCapital +", IssuedNumberOfShares = "+myCapital.IssuedNumberOfShares+", Asked = "+myCapital.Asked+", PaidUp = "+myCapital.PaidUp+", NominalNumberOfShares = "+ myCapital.NominalNumberOfShares + " , SharesDescription = '" + myCapital.SharesDescription + "' WHERE CompanyCIID = "+ companyCIID;

                        OleDbCommand myOleDbCommand = new OleDbCommand();
                        myOleDbCommand.CommandText = tmpQ;

                        using(OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                        {
                            myOleDbCommand.Connection = myOleDbConn;
                            myOleDbConn.Open();
                            try
                            {
                                new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                            } 
                            catch(Exception err)
                            {
                                Logger.WriteToLog(className +" : " + funcName, err,true);
                                return false;
                            }
                        }
                        return true;
            */
        }

        public bool DeleteCapital(int companyCIID) {
            string funcName = "DeleteCapital(int companyCIID)";
            OleDbCommand myOleDbCommand = new OleDbCommand();
            myOleDbCommand.CommandText = "DELETE FROM cpi_Capital WHERE CompanyCIID = " + companyCIID;
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbCommand.Connection = myOleDbConn;
                myOleDbConn.Open();
                try {
                    new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                } catch (Exception err) {
                    Logger.WriteToLog(className + " : " + funcName + err.Message, true);
                    return false;
                }
            }
            return true;
        }

        public CapitalBLLC GetCapital(int companyCIID) {
            string funcName = "GetCapital(int companyCIID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand("SELECT * FROM cpi_Capital WHERE CompanyCIID = " + companyCIID, myOleDbConn);

                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    CapitalBLLC myCapital = new CapitalBLLC();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            myCapital.CompanyCIID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            myCapital.AuthorizedCapital = reader.GetDecimal(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            myCapital.IssuedNumberOfShares = reader.GetDecimal(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            myCapital.Asked = reader.GetDecimal(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            myCapital.PaidUp = reader.GetDecimal(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            myCapital.NominalNumberOfShares = reader.GetDecimal(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            myCapital.SharesDescription = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            myCapital.CurrencyCode = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            myCapital.ShareClass = reader.GetString(8);
                        }
                    }

                    return myCapital;
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName + e.Message, true);
                    return null;
                }
            }
        }

        public bool IsCapitalAlreadyRegistered(int companyCIID) {
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                myOleDbConn.Open();

                OleDbCommand myOleDbCommand =
                    new OleDbCommand("SELECT * FROM cpi_Capital WHERE CompanyCIID = " + companyCIID, myOleDbConn);
                OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region ContactPerson

        public void InsertContactPerson(ContactPersonBLLC contactPerson, OleDbCommand myOleDbCommand, int creditInfoID) {
            myOleDbCommand.CommandText =
                "INSERT INTO cpi_ContactPerson(CreditInfoID, FirstNameNative, FirstNameEN, LastNameNative, LastNameEN, Email, Position, MessengerID, PhoneNumber, MobileNumber, FaxNumber, DateContacted) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
            OleDbParameter myParam = new OleDbParameter();

            myOleDbCommand.Parameters.Clear();

            myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer);
            myParam.Direction = ParameterDirection.Input;
            myParam.Value = creditInfoID;
            myOleDbCommand.Parameters.Add(myParam);

            // Insert Native and EN strings
            CreateTransliteratedParameterFromString(
                myOleDbCommand,
                "FirstNameNative",
                "FirstNameEN",
                ParameterDirection.Input,
                contactPerson.FirstNameNative,
                contactPerson.FirstNameEN,
                OleDbType.WChar);

            // Insert Native and EN strings
            CreateTransliteratedParameterFromString(
                myOleDbCommand,
                "LastNameNative",
                "LastNameEN",
                ParameterDirection.Input,
                contactPerson.LastNameNative,
                contactPerson.LastNameEN,
                OleDbType.WChar);

            myParam = new OleDbParameter("Email", OleDbType.VarChar);
            myParam.Direction = ParameterDirection.Input;
            myParam.Value = contactPerson.EmailAddress;
            myOleDbCommand.Parameters.Add(myParam);

            myParam = new OleDbParameter("Position", OleDbType.WChar);
            myParam.Direction = ParameterDirection.Input;
            myParam.Value = contactPerson.Position;
            myOleDbCommand.Parameters.Add(myParam);

            myParam = new OleDbParameter("MessengerID", OleDbType.VarChar);
            myParam.Direction = ParameterDirection.Input;
            myParam.Value = contactPerson.MessengerID;
            myOleDbCommand.Parameters.Add(myParam);

            myParam = new OleDbParameter("PhoneNumber", OleDbType.VarChar);
            myParam.Direction = ParameterDirection.Input;
            myParam.Value = contactPerson.PhoneNumber;
            myOleDbCommand.Parameters.Add(myParam);

            myParam = new OleDbParameter("MobileNumber", OleDbType.VarChar);
            myParam.Direction = ParameterDirection.Input;
            myParam.Value = contactPerson.MobileNumber;
            myOleDbCommand.Parameters.Add(myParam);

            myParam = new OleDbParameter("FaxNumber", OleDbType.VarChar);
            myParam.Direction = ParameterDirection.Input;
            myParam.Value = contactPerson.FaxNumber;
            myOleDbCommand.Parameters.Add(myParam);

            myParam = new OleDbParameter("DateContacted", OleDbType.Date);
            myParam.Direction = ParameterDirection.Input;
            myParam.Value = contactPerson.DateContacted;
            myOleDbCommand.Parameters.Add(myParam);

            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        private void UpdateContactPerson(ContactPersonBLLC contactPerson, OleDbCommand myOleDbCommand, int creditInfoID) {
            DeleteContactPerson(myOleDbCommand, creditInfoID);
            InsertContactPerson(contactPerson, myOleDbCommand, creditInfoID);
        }

        public void DeleteContactPerson(OleDbCommand myOleDbCommand, int creditInfoID) {
            myOleDbCommand.CommandText = "DELETE FROM cpi_ContactPerson WHERE CreditInfoID = ?";

            OleDbParameter myParam = new OleDbParameter();
            myOleDbCommand.Parameters.Clear();
            myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer);
            myParam.Direction = ParameterDirection.Input;
            myParam.Value = creditInfoID;
            myOleDbCommand.Parameters.Add(myParam);
            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
        }

        /*		public bool AddContactPerson(ContactPersonBLLC contactPerson, int creditInfoID)
                {
                    string funcName = "AddContactPerson(ContactPersonBLLC contactPerson, int creditInfoID)";

                    OleDbCommand myOleDbCommand = new OleDbCommand();
                    myOleDbCommand.CommandText = "INSERT INTO cpi_ContactPerson(CreditInfoID, FirstNameNative, LastNameNative, FirstNameEN, LastNameEN, Email, Position, MessengerID, PhoneNumber, MobileNumber, FaxNumber, DateContacted) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

                    using(OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                    {
                        myOleDbCommand.Connection = myOleDbConn;
                        myOleDbConn.Open();
                        try
                        {
                            OleDbParameter myParam = new OleDbParameter();
                            myOleDbCommand.Parameters.Clear();

                            myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = creditInfoID;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("FirstNameNative", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.FirstNameNative;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("LastNameNative", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.LastNameNative;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("FirstNameEN", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.FirstNameEN;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("LastNameEN", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.LastNameEN;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("Email", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.EmailAddress;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("Position", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.Position;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("MessengerID", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.MessengerID;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("PhoneNumber", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.PhoneNumber;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("MobileNumber", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.MobileNumber;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("FaxNumber", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.FaxNumber;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("DateContacted", OleDbType.Date);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.DateContacted;
                            myOleDbCommand.Parameters.Add(myParam);

                            new AuditFactory(myOleDbCommand).PerformAuditProcess(); myOleDbCommand.ExecuteNonQuery();
                        } 
                        catch(Exception err)
                        {
                            Logger.WriteToLog(className +" : " + funcName, err,true);
                            return false;
                        }
                    }
                    return true;
                }

                public bool UpdateContactPerson(ContactPersonBLLC contactPerson, int creditInfoID)
                {
                    string funcName = "UpdateContactPerson(ContactPersonBLLC contactPerson, int creditInfoID)";


                    OleDbCommand myOleDbCommand = new OleDbCommand();


                    using(OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                    {
                        myOleDbConn.Open();
                        // begin transaction...
                        OleDbTransaction myTrans = myOleDbConn.BeginTransaction();

                        try
                        {
                            myOleDbCommand = new OleDbCommand("UPDATE cpi_ContactPerson SET CreditInfoID = ?, FirstNameNative = ?, LastNameNative = ?, FirstNameEN = ?, LastNameEN = ?, Email = ?, Position = ?, MessengerID = ?, PhoneNumber = ?, MobileNumber = ?, FaxNumber = ?, DateContacted = ? WHERE CreditInfoID =" + creditInfoID ,myOleDbConn);
                            myOleDbCommand.Transaction = myTrans;

                            OleDbParameter myParam = new OleDbParameter("CreditInfoID", OleDbType.Integer);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = creditInfoID;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("FirstNameNative", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.FirstNameNative;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("LastNameNative", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.LastNameNative;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("FirstNameEN", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.FirstNameEN;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("LastNameEN", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.LastNameEN;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("Email", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.EmailAddress;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("Position", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.Position;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("MessengerID", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.MessengerID;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("PhoneNumber", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.PhoneNumber;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("MobileNumber", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.MobileNumber;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("FaxNumber", OleDbType.VarChar);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.FaxNumber;
                            myOleDbCommand.Parameters.Add(myParam);

                            myParam = new OleDbParameter("DateContacted", OleDbType.Date);
                            myParam.Direction = ParameterDirection.Input;
                            myParam.Value = contactPerson.DateContacted;
                            myOleDbCommand.Parameters.Add(myParam);

                            myOleDbCommand.ExecuteNonQuery();

                            myTrans.Commit();
                        } 
                        catch(Exception err)
                        {
                            Logger.WriteToLog(className +" : " + funcName, err,true);
                            return false;
                        }
                    }
                    return true;
                }

                public bool DeleteContactPerson(int creditInfoID)
                {
                    string funcName = "DeleteContactPerson(int creditInfoID)";
                    OleDbCommand myOleDbCommand = new OleDbCommand();
                    myOleDbCommand.CommandText = "DELETE FROM cpi_ContactPerson WHERE CreditInfoID = " + creditInfoID;

                    using(OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
                    {
                        myOleDbCommand.Connection = myOleDbConn;
                        myOleDbConn.Open();
                        try
                        {
                            myOleDbCommand.ExecuteNonQuery();
                        } 
                        catch(Exception err)
                        {
                            Logger.WriteToLog(className +" : " + funcName + err.Message,true);
                            return false;
                        }
                    }
                    return true;
                }
        */

        public ContactPersonBLLC GetContactPerson(int creditInfoID) {
            string funcName = "GetContactPerson(int creditInfoID)";
            
            using (OleDbConnection myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
                    OleDbCommand myOleDbCommand =
                        new OleDbCommand(
                            "SELECT * FROM cpi_ContactPerson WHERE CreditInfoID = " + creditInfoID, myOleDbConn);

                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    ContactPersonBLLC contactPerson = new ContactPersonBLLC();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            contactPerson.CreditInfoID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            contactPerson.FirstNameNative = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            contactPerson.LastNameNative = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3)) {
                            contactPerson.FirstNameEN = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            contactPerson.LastNameEN = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            contactPerson.EmailAddress = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            contactPerson.Position = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            contactPerson.MessengerID = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            contactPerson.PhoneNumber = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            contactPerson.MobileNumber = reader.GetString(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            contactPerson.FaxNumber = reader.GetString(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            contactPerson.DateContacted = reader.GetDateTime(11);
                        }
                    }

                    return contactPerson;
                } catch (Exception e) {
                    Logger.WriteToLog(className + " : " + funcName, e, true);
                    return null;
                }
            }
        }

        #endregion
    } // END CLASS DEFINITION CompanyProfileInputDALC
}
