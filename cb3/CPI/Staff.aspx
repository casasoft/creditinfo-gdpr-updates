<%@ Page language="c#" Codebehind="Staff.aspx.cs" AutoEventWireup="false" Inherits="CPI.Staff" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../user_controls/head.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>Staff</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
  <body MS_POSITIONING="GridLayout">
	
     <form id="_staffForm" method="post" runat="server">
			<TABLE id="tbDefault" borderColor="#0000ff" cellSpacing="0" cellPadding="0" width="600"
				align="center">
				<TR>
				</TR>
				<TR>
					<TD align="left" colSpan="8"><uc1:head id="ctlHeader" runat="server"></uc1:head></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 18px" align="center" colSpan="8"><asp:label id="lblPageTitle" runat="server" Width="100%" CssClass="pageheader"> Information entry </asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" align=left colSpan=8><asp:label id=lblKeyStaff runat="server" CssClass="label" Font-Bold="True">Key staff</asp:label>
					</TD>
				</TR>
				<TR class="dark-row">
					<TD style="HEIGHT: 10px" align="left" colSpan="8"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" align=left colSpan=8>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 24px" align="center"><asp:HyperLink id=hlRegBoardMembers runat="server" CssClass="seemore" NavigateUrl="RegBoardMembers.aspx">Reg. Boardmember</asp:HyperLink></TD>
					<TD style="HEIGHT: 24px" align="center"><asp:HyperLink id=hlRegStaff runat="server" CssClass="seemore">Key staff</asp:HyperLink></TD>
					<TD style="HEIGHT: 24px" align="center"><asp:HyperLink id=hlRegStaffCount runat="server" CssClass="seemore">Reg. Staff count</asp:HyperLink></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" align=left colSpan=8>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" align=left colSpan=8><asp:Label id=lblBoardMembersOverview runat="server" CssClass="label">Board members overview</asp:Label>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" align=left colSpan=8><asp:DataGrid id=dtgrBoardMOverview runat="server" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="4" GridLines="Vertical" ForeColor="Black">
<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A">
</SelectedItemStyle>

<AlternatingItemStyle BackColor="White">
</AlternatingItemStyle>

<ItemStyle BackColor="#F7F7DE">
</ItemStyle>

<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B">
</HeaderStyle>

<FooterStyle BackColor="#CCCC99">
</FooterStyle>

<Columns>
<asp:BoundColumn HeaderText="Name"></asp:BoundColumn>
<asp:BoundColumn HeaderText="ID"></asp:BoundColumn>
<asp:BoundColumn HeaderText="Action"></asp:BoundColumn>
</Columns>

<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages">
</PagerStyle>
</asp:DataGrid>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" align=left colSpan=8><asp:Label id=lblPrincipalsOverview runat="server" CssClass="label">Principals overview</asp:Label>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" align=left colSpan=8><asp:DataGrid id=dtgrPrincipalsOverview runat="server" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="4" GridLines="Vertical" ForeColor="Black">
<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A">
</SelectedItemStyle>

<AlternatingItemStyle BackColor="White">
</AlternatingItemStyle>

<ItemStyle BackColor="#F7F7DE">
</ItemStyle>

<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B">
</HeaderStyle>

<FooterStyle BackColor="#CCCC99">
</FooterStyle>

<Columns>
<asp:BoundColumn HeaderText="Name"></asp:BoundColumn>
<asp:BoundColumn HeaderText="ID"></asp:BoundColumn>
<asp:BoundColumn HeaderText="Action"></asp:BoundColumn>
</Columns>

<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages">
</PagerStyle>
</asp:DataGrid>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" align=left colSpan=8><asp:Label id=lblStaffCountOverview runat="server" CssClass="label">Staff count overview</asp:Label>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" align=left colSpan=8><asp:DataGrid id=dtgrStaffCountOverview runat="server" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="4" GridLines="Vertical" ForeColor="Black">
<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A">
</SelectedItemStyle>

<AlternatingItemStyle BackColor="White">
</AlternatingItemStyle>

<ItemStyle BackColor="#F7F7DE">
</ItemStyle>

<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B">
</HeaderStyle>

<FooterStyle BackColor="#CCCC99">
</FooterStyle>

<Columns>
<asp:BoundColumn HeaderText="Year"></asp:BoundColumn>
<asp:BoundColumn HeaderText="Staff count"></asp:BoundColumn>
</Columns>

<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages">
</PagerStyle>
</asp:DataGrid>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 24px" align=left colSpan=8>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 40px" align="left" colSpan="8"></TD>
				</TR>
				<TR>
					<TD align="left" width="100%" colSpan="8"><uc1:footer id=Footer1 runat="server"></uc1:footer></TD>
				</TR>
				<TR>
					<TD align="center" colSpan="8"></TD>
				</TR>
			</TABLE>
		</form>
	
  </body>
</HTML>
