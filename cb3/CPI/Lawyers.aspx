<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Page language="c#" Codebehind="Lawyers.aspx.cs" AutoEventWireup="false" Inherits="CPI.Lawyers" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>Lawyers</title>
		<% System.Web.HttpContext.Current.Response.AddHeader("Cache-Control","no-cache");%>
		<% System.Web.HttpContext.Current.Response.Expires = 0;%>
		<% System.Web.HttpContext.Current.Response.Cache.SetNoStore();%>
		<% System.Web.HttpContext.Current.Response.AddHeader("Pragma", "no-cache");%>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						LawyersForm.btnReg.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.LawyersForm.txtFirstname.focus();
				}

		</script>
</head>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="LawyersForm" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblLawyers" runat="server">Lawyers</asp:label></td>
																	<td align="right"><asp:label id="lblPageStep" runat="server">Step</asp:label></td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td colspan="3">
																		<asp:radiobutton id="rbtnIndividual" runat="server" autopostback="True" cssclass="radio" checked="True"
																			groupname="CustomerType"></asp:radiobutton>
																		<asp:image id="imgIndividual" runat="server" imageurl="../img/individual.gif"></asp:image>
																		<asp:radiobutton id="rbtnCompany" runat="server" autopostback="True" cssclass="radio" groupname="CustomerType"></asp:radiobutton>
																		<asp:image id="imgCompany" runat="server" imageurl="../img/company.gif"></asp:image>
																	</td>
																	<td><asp:textbox id="txtAddressEN" runat="server" visible="False" width="8px"></asp:textbox><asp:textbox id="txtAddressNative" runat="server" visible="False" width="8px"></asp:textbox><asp:textbox id="txtPostalCode" runat="server" visible="False" width="8px"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lblCIID" runat="server">CIID</asp:label><br>
																		<asp:textbox id="txtCIID" runat="server" maxlength="13"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblNationalID" runat="server">National ID</asp:label><br>
																		<asp:textbox id="txtNationalID" runat="server" maxlength="100"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblFirstName" runat="server">Firstname/Company name</asp:label><br>
																		<asp:textbox id="txtFirstname" runat="server" maxlength="100"></asp:textbox><asp:requiredfieldvalidator id="rfvFirstName" runat="server" controltovalidate="txtFirstname" errormessage="Value missing!"
																			cssclass="label">*</asp:requiredfieldvalidator></td>
																	<td><asp:label id="lblSurname" runat="server">Surname</asp:label><br>
																		<asp:textbox id="txtSurname" runat="server" maxlength="100"></asp:textbox></td>
																</tr>
																<tr>
																	<td align="right" colspan="4"><asp:button id="btnSearch" runat="server" cssclass="search_button" causesvalidation="False"
																			text="Search"></asp:button>&nbsp; <INPUT onclick="GoNordurNidur('txtCIID','txtNationalID','txtFirstname','txtSurname',document.LawyersForm.rbtnIndividual, 670, 900);"
																			type="button" value="..." class="popup"></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblSubHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 50%"><asp:label id="lblBuilding" runat="server">Building</asp:label><br>
																		<asp:textbox id="txtBuilding" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblOfficeNumber" runat="server">Office number</asp:label><br>
																		<asp:textbox id="txtOfficeNumber" runat="server" maxlength="50"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblHistoryNative" runat="server">History Native</asp:label><br>
																		<asp:textbox id="txtHistoryNative" runat="server" maxlength="500" width="100%" textmode="MultiLine"
																			height="150px"></asp:textbox></td>
																	<td><asp:label id="lblHistoryEN" runat="server">History EN</asp:label><br>
																		<asp:textbox id="txtHistoryEN" runat="server" maxlength="500" width="100%" textmode="MultiLine"
																			height="150px"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerNameSearchGridTable" cellspacing="0" cellpadding="0"
													runat="server">
													<tr>
														<td height="10"></td>
													</tr>													
													<tr>
														<td align="right"><asp:label id="lblNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td id="tdNameSearchGrid" style="HEIGHT: 25px" align="left" colspan="8" runat="server">
																		<div class="TA" id="divNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dtgrNameSearch" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;" commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="CreditInfoID" headertext="Creditinfo ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="Number" headertext="National ID">
																						<headerstyle width="10%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Name">
																						<headerstyle width="30%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Address">
																						<headerstyle width="25%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetNative" headertext="AddressNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetEN" headertext="AddressEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="City">
																						<headerstyle width="20%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityNative" headertext="CityNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityEN" headertext="CityEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left"><asp:label id="lblMsg" runat="server" visible="False" cssclass="error_text">Insert/Update failed</asp:label><asp:validationsummary id="ValidationSummary1" runat="server" width="208px"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancel" runat="server" cssclass="cancel_button" causesvalidation="False" text="Cancel"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" causesvalidation="False" text="Continue"></asp:button><asp:button id="btnReg" runat="server" cssclass="confirm_button" text="Reg."></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerLaywersGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td height="10"></td>
													</tr>												
													<tr>
														<td align="right"><asp:label id="lblLaywersDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblLaywersDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td><asp:datagrid id="dtgrLaywers" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;/&gt;" commandname="Update">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;/&gt;" commandname="Delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:boundcolumn datafield="CreditInfoID" headertext="CIID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Number" headertext="National ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Name">
																					<headerstyle width="20%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="NameNative" headertext="NameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="NameEN" headertext="NameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="FirstNameNative" headertext="FirstNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="FirstNameEN" headertext="FirstNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="SurNameNative" headertext="SurNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="SurNameEN" headertext="SurNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Street">
																					<headerstyle width="20%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="StreetNative" headertext="StreetNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="StreetEN" headertext="StreetEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="OfficeNumber" headertext="OfficeNumber">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Building" headertext="Building">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="HistoryNative" headertext="HistoryNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="HistoryEN" headertext="HistoryEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="Type" headertext="Type">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																		</asp:datagrid></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // END --></td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</html>
