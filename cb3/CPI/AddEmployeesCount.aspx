<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Page language="c#" Codebehind="AddEmployeesCount.aspx.cs" AutoEventWireup="false" Inherits="CPI.AddEmployeesCount" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>EmployeesCount</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>
					function checkEnterKey() 
					{    
							if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								EmployeesCountForm.btnRegStaffCount.click(); 
							}
					} 
					function SetFormFocus()
						{
							document.EmployeesCountForm.tbEmpYear1.focus();
						}
		</script>
</head>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="EmployeesCountForm" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblStaffCount" runat="server">Add staff count years</asp:label>
																	</td>
																	<td align="right">
																		<asp:label id="lblPageStep" runat="server">Step</asp:label>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblYear" runat="server" cssclass="label">Year</asp:label>
																	</td>
																	<td>
																		<asp:label id="lblEmployeesCount" runat="server" cssclass="label">Employees</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear1" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator1" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear1">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator1" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear1"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear1" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear2" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator2" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear2">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator2" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear2"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear2" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear3" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator3" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear3">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator3" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear3"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear3" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear4" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator4" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear4">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator4" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear4"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear4" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear5" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator5" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear5">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator5" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear5"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear5" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear6" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator6" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear6">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator6" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear6"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear6" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear7" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator7" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear7">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator7" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear7"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear7" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear8" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator8" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear8">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator8" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear8"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear8" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear9" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator9" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear9">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator9" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear9"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear9" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:textbox id="tbEmpYear10" runat="server" maxlength="4" cssclass="short_input"></asp:textbox>
																		<asp:customvalidator id="CustomValidator10" runat="server" errormessage="Value missing" controltovalidate="tbEmpCountYear10">*</asp:customvalidator>
																		<asp:rangevalidator id="RangeValidator10" runat="server" errormessage="Error in year input" controltovalidate="tbEmpYear10"
																			maximumvalue="2100" minimumvalue="1990" type="Integer">*</asp:rangevalidator>
																	</td>
																	<td>
																		<asp:textbox id="tbEmpCountYear10" runat="server" maxlength="9" cssclass="short_input"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right">
															<asp:label id="lblDatagridIcons" runat="server"></asp:label>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblEmployeesOverview" runat="server" cssclass="label">Employees overview</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dtgrStaffCountOverview" runat="server" gridlines="None" autogeneratecolumns="False"
																			cssclass="grid">
<footerstyle cssclass="grid_footer">
</FooterStyle>

<selecteditemstyle cssclass="grid_selecteditem">
</SelectedItemStyle>

<alternatingitemstyle cssclass="grid_alternatingitem">
</AlternatingItemStyle>

<itemstyle cssclass="grid_item">
</ItemStyle>

<headerstyle cssclass="grid_header">
</HeaderStyle>

<columns>
<asp:ButtonColumn Text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;/&gt;" CommandName="Delete">
<itemstyle cssclass="leftpadding">
</ItemStyle>
</asp:ButtonColumn>
<asp:BoundColumn DataField="Year" ReadOnly="True" HeaderText="Year">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="Count" HeaderText="Staff count">
<itemstyle cssclass="padding">
</ItemStyle>
</asp:BoundColumn>
</Columns>

<pagerstyle cssclass="grid_pager">
</PagerStyle>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:label id="lblMessage" runat="server" cssclass="confirm_text">Message</asp:label>
															<asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary>
														</td>
														<td align="right">
															<asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Back" causesvalidation="False"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" text="Continue"></asp:button><asp:button id="btnRegStaffCount" runat="server" cssclass="confirm_button" text="Reg."></asp:button>															
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
