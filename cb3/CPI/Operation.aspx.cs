#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

#pragma warning disable 618,612

namespace CPI {
    /// <summary>
    /// Summary description for Operation.
    /// </summary>
    public class Operation : BaseCPIPage {
        private const string pageClass = "Operation.apsx.cs";
        private readonly ArrayList arrCB = new ArrayList();
        private readonly ArrayList arrCompanyBanks = new ArrayList();
        private readonly ArrayList arrCompanyCustomerTypes = new ArrayList();
        private readonly ArrayList arrCompanyOperation = new ArrayList();
        private readonly ArrayList arrExportCountrieID = new ArrayList();
        //private ArrayList arrCBFormerOperation = new ArrayList();
        private readonly ArrayList arrFormerOperation = new ArrayList();
        private readonly ArrayList arrImportCountrieID = new ArrayList();
        private readonly ArrayList arrPayingTerms = new ArrayList();
        private readonly ArrayList arrSalesTerms = new ArrayList();
        private readonly CPIFactory myFact = new CPIFactory();
        private ArrayList arrAddetBanks = new ArrayList();
        private ArrayList arrAddetCustomerTypes = new ArrayList();
        private ArrayList arrAddetExportCountrieID = new ArrayList();
        private ArrayList arrAddetImportCountrieID = new ArrayList();
        private ArrayList arrAddetPayingTerms = new ArrayList();
        private ArrayList arrAddetSalesTerms = new ArrayList();
        protected Button btnBack;
        protected Button btnContinue;
        protected Button btnReg;
        protected CheckBoxList cblExportAreas;
        protected CheckBoxList cblImportAreas;
        protected CheckBoxList cblPaymentTerms;
        protected CheckBoxList cblSalesTerms;
        protected CheckBoxList chblOperation;
        private int CIID = 1; // hardcoded stuff // should be taken from session thought
        protected CheckBoxList ckblBanks;
        protected CheckBoxList ckblCustomerType;
        protected CheckBoxList ckblFormerOperation;
        protected HyperLink hlAdd;
        protected HyperLink hlAddBank;
        protected HyperLink hlAddCustomerType;
        protected HyperLink hlAddExportAreas;
        protected HyperLink hlAddFormerOperation;
        protected HyperLink hlAddImportAreas;
        protected HyperLink hlAddSalesTerms;
        protected HyperLink hlPaymentTerms;
        protected Label lblCurrentOperation;
        protected Label lblCustomerType;
        protected Label lblError;
        protected Label lblFormerOperation;
        protected Label lblMainExportAreas;
        protected Label lblMainImportAreas;
        protected Label lblNoBanks;
        protected Label lblNoFormerOperation;
        protected Label lblNoOperation;
        protected Label lblNoPayingTerms;
        protected Label lblNoSalesTerms;
        protected Label lblOperation;
        protected Label lblPageStep;
        protected Label lblPaymentTerms;
        protected Label lblSalesTerms;
        protected Label lblTradeBanks;
        protected HtmlInputHidden PageX;
        protected HtmlInputHidden PageY;
        //public string currentPageStep;
        private void Page_Load(object sender, EventArgs e) {
            pageName = "Operation";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new");
            }

            //if (Session["cpiTemplate"] == null) {
            //    Session["cpiTemplate"] = "General";
            //}

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            // Add <ENTER> event
            AddEnterEvent();

            if (Session["ReportCompany"] != null) {}

            /*
			if(Session["arrCB"] != null)
				arrCB = (ArrayList) Session["arrCB"];
			
			// check for former operation addet
			if(Session["arrCBFormerOperation"] != null)
				arrCBFormerOperation = (ArrayList) Session["arrCBFormerOperation"];*/
            // check for banks addet

            lblError.Visible = false;
            if (!IsPostBack) {
                if (Session["arrAddetBanks"] != null) {
                    arrAddetBanks = (ArrayList) Session["arrAddetBanks"];
                }

                // check for company types addet
                if (Session["arrAddetCustomerTypes"] != null) {
                    arrAddetCustomerTypes = (ArrayList) Session["arrAddetCustomerTypes"];
                }

                if (Session["arrImportCountrieID"] != null) {
                    arrAddetImportCountrieID = (ArrayList) Session["arrImportCountrieID"];
                }

                if (Session["arrExportCountrieID"] != null) {
                    arrAddetExportCountrieID = (ArrayList) Session["arrExportCountrieID"];
                }

                if (Session["arrCBSalesTerms"] != null) {
                    arrAddetSalesTerms = (ArrayList) Session["arrCBSalesTerms"];
                }

                if (Session["arrCBPaymentTerms"] != null) {
                    arrAddetPayingTerms = (ArrayList) Session["arrCBPaymentTerms"];
                }

                SetCurrentCompanyOperation();
                SetFormerCompanyOperation();
                SetCompanyBanks();
                SetCompanyCustomer();
                SetImportCountries();
                SetExportCountries();
                // 11062004 Er h�r � eftir a� �tf�ra f�llin
                SetSalesTerms();
                SetPayingTerms();
                LocalizeText();
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { Server.Transfer(nextPage); }

        public void SetCurrentCompanyOperation() {
            DataSet dsCompanyOperation = myFact.GetCompanyOperationAsDataSet(CIID, true);
            if (dsCompanyOperation.Tables.Count > 0) {
                chblOperation.DataSource = dsCompanyOperation;
                chblOperation.DataTextField = nativeCult ? "DescriptionNative" : "DescriptionEN";
                chblOperation.DataValueField = "NaceCodeID";
                chblOperation.DataBind();
                // select the whole bunch
                for (int i = 0; i < chblOperation.Items.Count; i++) {
                    chblOperation.Items[i].Selected = true;
                }
            }
            /*if(this.arrCB.Count > 0) 
			{
				for(int i=0; i< this.arrCB.Count; i++) 
				{
					CheckBoxValues theCBL = (CheckBoxValues) arrCB[i];
					if(chblOperation.Items.FindByValue(theCBL.Value)==null)
					{
						ListItem theListItem = new ListItem();
						theListItem.Selected = theCBL.Checked;
						theListItem.Value = theCBL.Value;
						theListItem.Text = theCBL.Text;
						this.chblOperation.Items.Add(theListItem);
					}
				}
			}*/
            lblNoOperation.Visible = dsCompanyOperation.Tables.Count <= 0;
        }

        public void SetFormerCompanyOperation() {
            DataSet dsFormerCompanyOperation = myFact.GetCompanyOperationAsDataSet(CIID, false);
            if (dsFormerCompanyOperation.Tables.Count > 0) {
                ckblFormerOperation.DataSource = dsFormerCompanyOperation;
                ckblFormerOperation.DataTextField = nativeCult ? "DescriptionNative" : "DescriptionEN";
                ckblFormerOperation.DataValueField = "NaceCodeID";
                ckblFormerOperation.DataBind();
                // select the whole bunch
                for (int i = 0; i < ckblFormerOperation.Items.Count; i++) {
                    ckblFormerOperation.Items[i].Selected = true;
                }
            }
            /*if(this.arrCBFormerOperation.Count > 0) 
			{
				for(int i=0; i< this.arrCBFormerOperation.Count; i++) 
				{
					CheckBoxValues theCBL = (CheckBoxValues) arrCBFormerOperation[i];
					if(ckblFormerOperation.Items.FindByValue(theCBL.Value)==null)
					{
						ListItem theListItem = new ListItem();
						theListItem.Selected = theCBL.Checked;
						theListItem.Value = theCBL.Value;
						theListItem.Text = theCBL.Text;
						this.ckblFormerOperation.Items.Add(theListItem);
					}
				}
				Session["FormerOperation"] = null;
			}*/
            lblNoFormerOperation.Visible = dsFormerCompanyOperation.Tables.Count <= 0;
        }

        public void SetCompanyBanks() {
            DataSet dsCompanyBanks = myFact.GetCompanyBanksAsDataSet(CIID);

            if (dsCompanyBanks.Tables.Count > 0) {
                ckblBanks.DataSource = dsCompanyBanks;
                if (nativeCult) {
                    if (CigConfig.Configure("lookupsettings.currentVersion") != null &&
                        CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                        ckblBanks.DataTextField = "NameNativeAndLocation";
                    } else {
                        ckblBanks.DataTextField = "NameNative";
                    }
                } else if (CigConfig.Configure("lookupsettings.currentVersion") != null &&
                           CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                    ckblBanks.DataTextField = "NameNativeAndLocation";
                } else {
                    ckblBanks.DataTextField = "NameEN";
                }
                ckblBanks.DataValueField = "BankID";
                ckblBanks.DataBind();
                // select the whole bunch
                for (int i = 0; i < ckblBanks.Items.Count; i++) {
                    ckblBanks.Items[i].Selected = true;
                }
            }
            if (arrAddetBanks.Count > 0) {
                for (int i = 0; i < arrAddetBanks.Count; i++) {
                    var theCBL = (CheckBoxValues) arrAddetBanks[i];
                    var theListItem = new ListItem
                                      {
                                          Selected = theCBL.Checked,
                                          Value = theCBL.Value,
                                          Text = theCBL.Text
                                      };
                    ckblBanks.Items.Add(theListItem);
                }
            }
            if (dsCompanyBanks.Tables.Count <= 0 && arrAddetBanks.Count <= 0) {
                lblNoBanks.Visible = true;
            } else {
                lblNoBanks.Visible = false;
            }
        }

        public void SetCompanyCustomer() {
            DataSet dsCompanyCustomer = myFact.GetCompaniesCustomersAsDataSet(CIID);
            if (dsCompanyCustomer.Tables.Count > 0) {
                ckblCustomerType.DataSource = dsCompanyCustomer;
                ckblCustomerType.DataTextField = nativeCult ? "CustomerTypeNative" : "CustomerTypeEN";
                ckblCustomerType.DataValueField = "CustomerTypeID";
                ckblCustomerType.DataBind();

                // select the whole bunch
                for (int i = 0; i < ckblCustomerType.Items.Count; i++) {
                    ckblCustomerType.Items[i].Selected = true;
                }
            }
            if (arrAddetCustomerTypes.Count > 0) {
                for (int i = 0; i < arrAddetCustomerTypes.Count; i++) {
                    var theCBL = (CheckBoxValues) arrAddetCustomerTypes[i];
                    var theListItem = new ListItem
                                      {
                                          Selected = theCBL.Checked,
                                          Value = theCBL.Value,
                                          Text = theCBL.Text
                                      };
                    ckblCustomerType.Items.Add(theListItem);
                }
            }
        }

        public void SetImportCountries() {
            DataSet dsImportCountries = myFact.GetImportAreasAsDataSet(CIID);
            if (dsImportCountries.Tables.Count > 0) {
                cblImportAreas.DataSource = dsImportCountries;
                cblImportAreas.DataTextField = nativeCult ? "NameNative" : "NameEN";
                cblImportAreas.DataValueField = "CountryID";
                cblImportAreas.DataBind();

                // select the whole bunch
                for (int i = 0; i < cblImportAreas.Items.Count; i++) {
                    cblImportAreas.Items[i].Selected = true;
                }
            }
            if (arrAddetImportCountrieID.Count > 0) {
                for (int i = 0; i < arrAddetImportCountrieID.Count; i++) {
                    var theCBL = (CheckBoxValues) arrAddetImportCountrieID[i];
                    var theListItem = new ListItem
                                      {
                                          Selected = theCBL.Checked,
                                          Value = theCBL.Value,
                                          Text = theCBL.Text
                                      };
                    cblImportAreas.Items.Add(theListItem);
                }
            }
        }

        public void SetExportCountries() {
            DataSet dsExportCountries = myFact.GetExportAreasAsDataSet(CIID);
            if (dsExportCountries.Tables.Count > 0) {
                cblExportAreas.DataSource = dsExportCountries;
                cblExportAreas.DataTextField = nativeCult ? "NameNative" : "NameEN";
                cblExportAreas.DataValueField = "CountryID";
                cblExportAreas.DataBind();

                // select the whole bunch
                for (int i = 0; i < cblExportAreas.Items.Count; i++) {
                    cblExportAreas.Items[i].Selected = true;
                }
            }
            if (arrAddetExportCountrieID.Count > 0) {
                for (int i = 0; i < arrAddetExportCountrieID.Count; i++) {
                    var theCBL = (CheckBoxValues) arrAddetExportCountrieID[i];
                    var theListItem = new ListItem
                                      {
                                          Selected = theCBL.Checked,
                                          Value = theCBL.Value,
                                          Text = theCBL.Text
                                      };
                    cblExportAreas.Items.Add(theListItem);
                }
            }
        }

        private void SetSalesTerms() {
            //	 Gera �etta einhverveginn svona 
            const bool sales = true;
            DataSet dsSalesTerm = myFact.GetCompanyTradeTermsAsDateSet(CIID, sales);
            //.GetCompanyOperationAsDataSet(CIID,false);
            if (dsSalesTerm.Tables.Count > 0) {
                cblSalesTerms.DataSource = dsSalesTerm;
                cblSalesTerms.DataTextField = nativeCult ? "TermsDescriptionNative" : "TermsDescriptionEN";
                cblSalesTerms.DataValueField = "TermsID";
                cblSalesTerms.DataBind();
                // select the whole bunch
                for (int i = 0; i < cblSalesTerms.Items.Count; i++) {
                    cblSalesTerms.Items[i].Selected = true;
                }
            }

            if (arrAddetSalesTerms.Count > 0) {
                for (int i = 0; i < arrAddetSalesTerms.Count; i++) {
                    var theCBL = (CheckBoxValues) arrAddetSalesTerms[i];
                    var theListItem = new ListItem
                                      {
                                          Selected = theCBL.Checked,
                                          Value = theCBL.Value,
                                          Text = theCBL.Text
                                      };
                    cblSalesTerms.Items.Add(theListItem);
                }
                //	Session["arrCBSalesTerms"] = null; // keep it in session??
            }
            if (dsSalesTerm.Tables.Count <= 0 && arrAddetSalesTerms.Count <= 0) {
                lblNoSalesTerms.Visible = true;
            } else {
                lblNoSalesTerms.Visible = false;
            }
        }

        private void SetPayingTerms() {
            const bool sales = false;
            DataSet dsPayingTerm = myFact.GetCompanyTradeTermsAsDateSet(CIID, sales);
            //.GetCompanyOperationAsDataSet(CIID,false);
            if (dsPayingTerm.Tables.Count > 0) {
                cblPaymentTerms.DataSource = dsPayingTerm;
                cblPaymentTerms.DataTextField = nativeCult ? "TermsDescriptionNative" : "TermsDescriptionEN";
                cblPaymentTerms.DataValueField = "TermsID";
                cblPaymentTerms.DataBind();
                // select the whole bunch
                for (int i = 0; i < cblPaymentTerms.Items.Count; i++) {
                    cblPaymentTerms.Items[i].Selected = true;
                }
            }

            if (arrAddetPayingTerms.Count > 0) {
                for (int i = 0; i < arrAddetPayingTerms.Count; i++) {
                    var theCBL = (CheckBoxValues) arrAddetPayingTerms[i];
                    var theListItem = new ListItem
                                      {
                                          Selected = theCBL.Checked,
                                          Value = theCBL.Value,
                                          Text = theCBL.Text
                                      };
                    cblPaymentTerms.Items.Add(theListItem);
                }
                //	Session["arrCBPaymentTerms"] = null; // keep it in session ??
            }
            if (dsPayingTerm.Tables.Count <= 0 && arrAddetPayingTerms.Count <= 0) {
                lblNoPayingTerms.Visible = true;
            } else {
                lblNoPayingTerms.Visible = false;
            }
        }

        public void GetCheckedOperationItems() {
            //int index = -1;
            arrCB.Clear();
            for (int i = 0; i < chblOperation.Items.Count; i++) {
                if (chblOperation.Items[i].Selected) {
                    /*index = this.arrCompanyOperation.IndexOf(chblOperation.Items[i].Value);
					if(index < 0)*/
                    arrCompanyOperation.Add(chblOperation.Items[i].Value);
                }
            }
        }

        public void GetCheckedFormerOperationItems() {
            //int index = -1;
            //int temp = 0;
            arrFormerOperation.Clear();
            for (int i = 0; i < ckblFormerOperation.Items.Count; i++) {
                if (ckblFormerOperation.Items[i].Selected) {
                    //index = this.arrFormerOperation.IndexOf(ckblFormerOperation.Items[i].Value);
                    //if(index < 0)
                    arrFormerOperation.Add(ckblFormerOperation.Items[i].Value);
                }
            }
        }

        public void GetCheckedBankItems() {
            for (int i = 0; i < ckblBanks.Items.Count; i++) {
                if (!ckblBanks.Items[i].Selected) {
                    continue;
                }
                var index = arrCompanyBanks.IndexOf(ckblBanks.Items[i].Value);
                if (index < 0) {
                    arrCompanyBanks.Add(ckblBanks.Items[i].Value);
                }
            }
        }

        public void GetCheckedCustomerTypeItems() {
            for (int i = 0; i < ckblCustomerType.Items.Count; i++) {
                if (ckblCustomerType.Items[i].Selected) {
                    arrCompanyCustomerTypes.Add(Convert.ToInt32(ckblCustomerType.Items[i].Value));
                }
            }
        }

        public void GetImportAreas() {
            for (int i = 0; i < cblImportAreas.Items.Count; i++) {
                if (!cblImportAreas.Items[i].Selected) {
                    continue;
                }
                int index = arrImportCountrieID.IndexOf(Convert.ToInt32(cblImportAreas.Items[i].Value));
                if (index < 0) // avoid multiple instances
                {
                    arrImportCountrieID.Add(Convert.ToInt32(cblImportAreas.Items[i].Value));
                }
            }
        }

        public void GetExportAreas() {
            for (int i = 0; i < cblExportAreas.Items.Count; i++) {
                if (cblExportAreas.Items[i].Selected) {
                    int index = arrExportCountrieID.IndexOf(Convert.ToInt32(cblExportAreas.Items[i].Value));
                    if (index < 0) // avoid multiple instances
                    {
                        arrExportCountrieID.Add(Convert.ToInt32(cblExportAreas.Items[i].Value));
                    }
                }
            }
        }

        public void GetCheckedSalesTerms() {
            for (int i = 0; i < cblSalesTerms.Items.Count; i++) {
                if (cblSalesTerms.Items[i].Selected) {
                    int index = arrSalesTerms.IndexOf(Convert.ToInt32(cblSalesTerms.Items[i].Value));
                    if (index < 0) // avoid multiple instances
                    {
                        arrSalesTerms.Add(Convert.ToInt32(cblSalesTerms.Items[i].Value));
                    }
                }
            }
        }

        public void GetCheckedPayingTerms() {
            for (int i = 0; i < cblPaymentTerms.Items.Count; i++) {
                if (cblPaymentTerms.Items[i].Selected) {
                    int index = arrPayingTerms.IndexOf(Convert.ToInt32(cblPaymentTerms.Items[i].Value));
                    if (index < 0) // avoid multiple instances
                    {
                        arrPayingTerms.Add(Convert.ToInt32(cblPaymentTerms.Items[i].Value));
                    }
                }
            }
        }

        private void AddEnterEvent() {
            chblOperation.Attributes.Add("onkeypress", "checkEnterKey();");
            ckblBanks.Attributes.Add("onkeypress", "checkEnterKey();");
            ckblCustomerType.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        private void btnReg_Click(object sender, EventArgs e) {
            const string funcName = "btnContinue_Click";
            try {
                GetCheckedOperationItems();
                Session["arrCompanyOperation"] = arrCompanyOperation;
                GetCheckedFormerOperationItems();
                Session["arrFormerOperation"] = arrFormerOperation;
                GetCheckedBankItems();
                Session["arrCompanyBanks"] = arrCompanyBanks;
                GetCheckedCustomerTypeItems();
                Session["arrCompanyCustomerTypes"] = arrCompanyCustomerTypes;
                GetImportAreas();
                GetExportAreas();
                GetCheckedSalesTerms();
                GetCheckedPayingTerms();
                if (myFact.AddOperationInfo(
                    arrImportCountrieID,
                    arrExportCountrieID,
                    arrCompanyOperation,
                    arrFormerOperation,
                    arrCompanyBanks,
                    arrCompanyCustomerTypes,
                    arrSalesTerms,
                    arrPayingTerms,
                    CIID)) {
                    DisplayMessage(rm.GetString("txtInfoSaved", ci));
                } else {
                    DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
                }

                // set Session ArrayList as null
                Session["arrImportCountrieID"] = null;
                Session["arrExportCountrieID"] = null;
                Session["arrCBSalesTerms"] = null;
                Session["arrCBPaymentTerms"] = null;
                Session["arrAddetBanks"] = null;
                Session["arrAddetCustomerTypes"] = null;

                // Clear unchecked boxes
                ResetCompanyBanks();
                ResetCompanyOperation();
                ResetCompanyFormerOperation();
                ResetCompanyCustomer();
                ResetImportCountries();
                ResetExportCountries();
                ResetSalesTerms();
                ResetPayingTerms();
            } catch (Exception err) {
                Logger.WriteToLog(pageClass + " : " + funcName + " : " + err, true);
                DisplayErrorMessage(rm.GetString("txtErrorOnPage", ci));
            }
        }

        private void DisplayMessage(String message) {
            lblError.Text = message;
            lblError.ForeColor = Color.Blue;
            lblError.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblError.Text = errorMessage;
            lblError.ForeColor = Color.Red;
            lblError.Visible = true;
        }

        private void ResetCompanyBanks() {
            DataSet dsCompanyBanks = myFact.GetCompanyBanksAsDataSet(CIID);
            if (dsCompanyBanks.Tables.Count <= 0) {
                return;
            }
            ckblBanks.DataSource = dsCompanyBanks;
            if (nativeCult) {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null &&
                    CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                    ckblBanks.DataTextField = "NameNativeAndLocation";
                } else {
                    ckblBanks.DataTextField = "NameNative";
                }
            } else if (CigConfig.Configure("lookupsettings.currentVersion") != null &&
                       CigConfig.Configure("lookupsettings.currentVersion").Equals("malta")) {
                ckblBanks.DataTextField = "NameNativeAndLocation";
            } else {
                ckblBanks.DataTextField = "NameEN";
            }
            ckblBanks.DataValueField = "BankID";
            ckblBanks.DataBind();
            // select the whole bunch
            for (int i = 0; i < ckblBanks.Items.Count; i++) {
                ckblBanks.Items[i].Selected = true;
            }
        }

        // the current operation
        private void ResetCompanyOperation() {
            DataSet dsCompanyOperation = myFact.GetCompanyOperationAsDataSet(CIID, true);
            if (dsCompanyOperation.Tables.Count > 0) {
                chblOperation.DataSource = dsCompanyOperation;
                chblOperation.DataTextField = nativeCult ? "DescriptionNative" : "DescriptionEN";
                chblOperation.DataValueField = "NaceCodeID";
                chblOperation.DataBind();
                // select the whole bunch
                for (int i = 0; i < chblOperation.Items.Count; i++) {
                    chblOperation.Items[i].Selected = true;
                }
            }
        }

        // the former operation
        private void ResetCompanyFormerOperation() {
            var dsCompanyFormerOperation = myFact.GetCompanyOperationAsDataSet(CIID, false);
            if (dsCompanyFormerOperation.Tables.Count <= 0) {
                return;
            }
            ckblFormerOperation.DataSource = dsCompanyFormerOperation;
            ckblFormerOperation.DataTextField = nativeCult ? "DescriptionNative" : "DescriptionEN";
            ckblFormerOperation.DataValueField = "NaceCodeID";
            ckblFormerOperation.DataBind();
            // select the whole bunch
            for (int i = 0; i < ckblFormerOperation.Items.Count; i++) {
                ckblFormerOperation.Items[i].Selected = true;
            }
        }

        private void ResetCompanyCustomer() {
            var dsCompanyCustomer = myFact.GetCompaniesCustomersAsDataSet(CIID);
            if (dsCompanyCustomer.Tables.Count <= 0) {
                return;
            }
            ckblCustomerType.DataSource = dsCompanyCustomer;
            ckblCustomerType.DataTextField = nativeCult ? "CustomerTypeNative" : "CustomerTypeEN";
            ckblCustomerType.DataValueField = "CustomerTypeID";
            ckblCustomerType.DataBind();

            // select the whole bunch
            for (int i = 0; i < ckblCustomerType.Items.Count; i++) {
                ckblCustomerType.Items[i].Selected = true;
            }
        }

        private void ResetImportCountries() {
            var dsImportCountries = myFact.GetImportAreasAsDataSet(CIID);
            if (dsImportCountries.Tables.Count <= 0) {
                return;
            }
            cblImportAreas.DataSource = dsImportCountries;
            cblImportAreas.DataTextField = nativeCult ? "NameNative" : "NameEN";
            cblImportAreas.DataValueField = "CountryID";
            cblImportAreas.DataBind();

            // select the whole bunch
            for (int i = 0; i < cblImportAreas.Items.Count; i++) {
                cblImportAreas.Items[i].Selected = true;
            }
        }

        private void ResetExportCountries() {
            var dsExportCountries = myFact.GetExportAreasAsDataSet(CIID);
            if (dsExportCountries.Tables.Count <= 0) {
                return;
            }
            cblExportAreas.DataSource = dsExportCountries;
            cblExportAreas.DataTextField = nativeCult ? "NameNative" : "NameEN";
            cblExportAreas.DataValueField = "CountryID";
            cblExportAreas.DataBind();

            // select the whole bunch
            for (int i = 0; i < cblExportAreas.Items.Count; i++) {
                cblExportAreas.Items[i].Selected = true;
            }
        }

        private void ResetSalesTerms() {
            SetSalesTerms();
            /*
			DataSet dsSalesTerms = new DataSet();
			dsSalesTerms = myFact.GetCompanyTradeTermsAsDateSet(CIID,true);
			if(dsSalesTerms.Tables.Count > 0)
			{
				this.cblExportAreas.DataSource = dsSalesTerms;
				if(nativeCult)
					this.cblExportAreas.DataTextField = "NameNative";
				else 
					this.cblExportAreas.DataTextField = "NameEN";
				this.cblExportAreas.DataValueField = "CountryID";
				this.cblExportAreas.DataBind();

				// select the whole bunch
				for(int i=0; i < cblExportAreas.Items.Count; i++)
					cblExportAreas.Items[i].Selected = true;
				
			}
			*/
        }

        private void ResetPayingTerms() { SetPayingTerms(); }

        private void LocalizeText() {
            lblOperation.Text = rm.GetString("txtOperation", ci);
            lblCurrentOperation.Text = rm.GetString("txtCurrentOperation", ci);
            lblNoOperation.Text = rm.GetString("txtNoOperationHasBeenRegisted", ci);
            hlAdd.Text = rm.GetString("txtAdd", ci);
            lblTradeBanks.Text = rm.GetString("txtTradeBanks", ci);
            lblNoBanks.Text = rm.GetString("txtNoBanks", ci);
            hlAddBank.Text = rm.GetString("txtAdd", ci);
            lblCustomerType.Text = rm.GetString("txtCustomerType", ci);
            hlAddCustomerType.Text = rm.GetString("txtAdd", ci);
            lblMainImportAreas.Text = rm.GetString("txtMainImportAreas", ci);
            hlAddImportAreas.Text = rm.GetString("txtAdd", ci);
            lblMainExportAreas.Text = rm.GetString("txtMainExportAreas", ci);
            hlAddExportAreas.Text = rm.GetString("txtAdd", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            btnReg.Text = rm.GetString("txtSave", ci);
            btnBack.Text = rm.GetString("txtBack", ci);
            lblError.Text = rm.GetString("txtErrorOnPage", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            lblFormerOperation.Text = rm.GetString("txtFormerOperation", ci);
            hlAddFormerOperation.Text = rm.GetString("txtAdd", ci);
            lblNoFormerOperation.Text = rm.GetString("txtNoFormerOperationHasBeenRegisted", ci);
            lblPaymentTerms.Text = rm.GetString("txtPaymentsTerms", ci);
            lblSalesTerms.Text = rm.GetString("txtSalesTerms", ci);
            hlAddSalesTerms.Text = rm.GetString("txtAdd", ci);
            hlPaymentTerms.Text = rm.GetString("txtAdd", ci);
        }

        private void btnBack_Click(object sender, EventArgs e) { Server.Transfer(previousPage); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}

#pragma warning restore 618,612
