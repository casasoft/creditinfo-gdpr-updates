<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="DBWC" Namespace="DBauer.Web.UI.WebControls" Assembly="DBauer.Web.UI.WebControls.HierarGrid" %>
<%@ Page language="c#" Codebehind="Operation.aspx.cs" AutoEventWireup="false" Inherits="CPI.Operation" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Operation</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						OperationForm.btnContinue.click(); 
					}
				} 
	function ScrollIt()
	{
		window.scrollTo(document.OperationForm.PageX.value, document.OperationForm.PageY.value);
    }
	function setcoords()
	{
		var myPageX;
		var myPageY;
		if (document.all)
		{
			myPageX = document.body.scrollLeft;
			myPageY = document.body.scrollTop;
        }
		else
		{
			myPageX = window.pageXOffset;
			myPageY = window.pageYOffset;
        }
		document.OperationForm.PageX.value = myPageX;
		document.OperationForm.PageY.value = myPageY;
    }
		</script>
	</head>
	<body onscroll="javascript:setcoords()" onload="javascript:ScrollIt()" ms_positioning="GridLayout">
		<form id="OperationForm" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblOperation" runat="server">Operation</asp:label>
																	</td>
																	<td align="right">
																		<asp:label id="lblPageStep" runat="server">Step 1 of ?</asp:label>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbDefault" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblCurrentOperation" runat="server" font-bold="True">Current operation</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblNoOperation" runat="server">No operation has been registed. To add operation click on add below</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:hyperlink id="hlAdd" runat="server" cssclass="seemore" navigateurl="AddOperation.aspx">Add</asp:hyperlink>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:checkboxlist id="chblOperation" runat="server" cssclass="radio"></asp:checkboxlist>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblFormerOperation" runat="server">Former operation</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblNoFormerOperation" runat="server">No former operation has been registed. To add operation click on add below</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:hyperlink id="hlAddFormerOperation" runat="server" cssclass="seemore" navigateurl="AddOperation.aspx?former=true">Add</asp:hyperlink>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:checkboxlist id="ckblFormerOperation" runat="server" cssclass="radio"></asp:checkboxlist>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblTradeBanks" runat="server" font-bold="True">Trade banks</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblNoBanks" runat="server">No banks has been registed. To add bank click on Add below</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:hyperlink id="hlAddBank" runat="server" cssclass="seemore" navigateurl="AddBank.aspx">Add</asp:hyperlink>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:checkboxlist id="ckblBanks" runat="server" cssclass="radio"></asp:checkboxlist>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblCustomerType" runat="server">Customer type</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:hyperlink id="hlAddCustomerType" runat="server" cssclass="seemore" navigateurl="AddCustomerType.aspx">Add</asp:hyperlink>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:checkboxlist id="ckblCustomerType" runat="server" cssclass="radio"></asp:checkboxlist>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblMainImportAreas" runat="server">Main import areas</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:hyperlink id="hlAddImportAreas" runat="server" cssclass="seemore" navigateurl="AddImExportAreas.aspx?import=true">Add</asp:hyperlink>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:checkboxlist id="cblImportAreas" runat="server" cssclass="radio"></asp:checkboxlist>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblMainExportAreas" runat="server">Main Export areas</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:hyperlink id="hlAddExportAreas" runat="server" cssclass="seemore" navigateurl="AddImExportAreas.aspx?import=false">Add</asp:hyperlink>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:checkboxlist id="cblExportAreas" runat="server" cssclass="radio"></asp:checkboxlist>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblSalesTerms" runat="server">Sales terms</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblNoSalesTerms" runat="server">No sales terms has been registed. To add sales terms click on add below</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:hyperlink id="hlAddSalesTerms" runat="server" cssclass="seemore" navigateurl="SalesPaymentTerms.aspx?sales=true">Add</asp:hyperlink>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:checkboxlist id="cblSalesTerms" runat="server" cssclass="radio"></asp:checkboxlist>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblPaymentTerms" runat="server">Payment terms</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblNoPayingTerms" runat="server">No paying terms has been registed. To add paying terms click on add below</asp:label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:hyperlink id="hlPaymentTerms" runat="server" cssclass="seemore" navigateurl="SalesPaymentTerms.aspx?payment=true">Add</asp:hyperlink>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:checkboxlist id="cblPaymentTerms" runat="server" cssclass="radio"></asp:checkboxlist>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:label id="lblError" runat="server" cssclass="error_text" visible="False">Error on page!</asp:label>
														</td>
														<td align="right">
															<asp:button id="btnBack" runat="server" cssclass="cancel_button" text="Back"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" text="Continue"></asp:button><asp:button id="btnReg" runat="server" cssclass="confirm_button" text="Reg"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<input type="hidden" id="PageX" name="PageX" value="0" runat="server"> <input id="PageY" type="hidden" value="0" name="PageY" runat="server">
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
