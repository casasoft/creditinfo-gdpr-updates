#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.DAL;
using CPI.Localization;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
//using NPayments.BLL.Localization;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for StructureReportInput.
    /// </summary>
    public class StructureReportInput : Page {
        public static CultureInfo ci;
        private const string pageName = "StructureReportInput.aspx";
        public static ResourceManager rm;
        private readonly CPIFactory myFact = new CPIFactory();
        protected ValidationSummary BasicValidationSummary;
        protected ValidationSummary BoardMembersValidationsummary;
        protected Button btnBasicCancel;
        protected Button btnBasicReg;
        private bool btnBasicRegClicked;
        protected Button btnBoardMemberSearch;
        protected Button btnCancelBoardMember;
        protected Button btnCancelCapital;
        protected Button btnCancelCharges;
        protected Button btnCancelSecretary;
        protected Button btnCancelShareOwner;
        protected Button btnCitySearch;
        protected Button btnRegBoardMember;
        private bool btnRegBoardMemberClicked;
        protected Button btnRegBoardMemberHistory;
        protected Button btnRegCapital;
        private bool btnRegCapitalClicked;
        protected Button btnRegCharges;
        private bool btnRegChargesClicked;
        protected Button btnRegSecretary;
        private bool btnRegSecretaryClicked;
        protected Button btnRegShareOwner;
        private bool btnRegShareOwnerClicked;
        protected Button btnRegShareOwnerHistory;
        protected Button btnSecretarySearch;
        protected Button btnShareOwnerSearch;
        protected ValidationSummary CapitalValidationSummary;
        protected ValidationSummary ChargesValidationsummary;
        private int CIID = -1;
        protected CustomValidator custDateLastUpdateBasic;
        protected CustomValidator Customvalidator8;
        protected CustomValidator custValAmountCharges;
        protected CustomValidator custValBasicName;
        protected CustomValidator custValBasicUniqueID;
        protected CustomValidator custValDatePrepCharges;
        protected CustomValidator custValDatRegCharges;
        protected CustomValidator custValidAddressBasic;
        protected CustomValidator custValidAsked;
        protected CustomValidator custValidAuCapital;
        protected CustomValidator custValidCityBasic;
        protected CustomValidator custValidEstablishedDateBasic;
        protected CustomValidator custValidFirstNameBoardMember;
        protected CustomValidator custValidFirstNameSecretary;
        protected CustomValidator custValidLARBasic;
        protected CustomValidator custValidLastContactedDateBasic;
        protected CustomValidator custValidLRBasic;
        protected CustomValidator custValidNationalIDSecretary;
        protected CustomValidator custValidNomNumberShares;
        protected CustomValidator custValidPersIDBoardMembers;
        protected CustomValidator custValidRegistedDate;
        protected CustomValidator custValidSRRBasic;
        protected CustomValidator custValidSurNameBoardMember;
        protected CustomValidator custValIssuedShares;
        protected CustomValidator custValPaidUp;
        protected CustomValidator custValShareholdersOwnerOwnership;
        protected HtmlTableRow CYPLARROW;
        protected DropDownList ddBoardMemberManagementPosition;
        protected DropDownList ddCity;
        protected DropDownList ddlBoardMemberAreaCode;
        protected DropDownList ddlChargesBeneficiary;
        protected DropDownList ddlChargesCurrency;
        protected DropDownList ddlChargesDescription;
        protected DropDownList ddLegalForm;
        protected DropDownList ddlShareOwnerOwnershipType;
        protected DropDownList ddlstCurrency;
        protected DataGrid dgCharges;
        protected DataGrid dgRegisteredCompanySecretary;
        protected HtmlGenericControl divBoardMemberNameSearch;
        protected HtmlGenericControl divNameSearch;
        protected HtmlGenericControl divSecretaryNameSearch;
        protected HtmlGenericControl divShareOwnerNameSearch;
        private DataSet dsDetailedBoardMembers = new DataSet();
        private DataSet dsManagementPositions = new DataSet();
        private DataSet dsRegistrationForms = new DataSet();
        private DataSet dsShareHolders = new DataSet();
        protected DataGrid dtgrBoardMemberNameSearch;
        protected DataGrid dtgrBoardMOverview;
        protected DataGrid dtgrSecretaryNameSearch;
        protected DataGrid dtgrShareholders;
        protected DataGrid dtgrShareOwnerNameSearch;
        protected NumberFormatInfo fInfo;
        protected Label Label1;
        protected Label lblAdditionalInfoHeader;
        protected Label lblAddress;
        protected Label lblAddressEN;
        protected Label lblAddressHeader;
        protected Label lblAreaCode;
        protected Label lblAsked;
        protected Label lblAuthorizedCapital;
        protected Label lblBasicInfo;
        protected Label lblBoardMemberAddress;
        protected Label lblBoardMemberAreaCode;
        protected Label lblBoardMemberCIID;
        protected Label lblBoardMemberFirstName;
        protected Label lblBoardMemberHistoryEN;
        protected Label lblBoardMemberHistoryNative;
        protected Label lblBoardMemberManagementPosition;
        protected Label lblBoardMemberMessage;
        protected Label lblBoardMemberNameSearchDatagridHeader;
        protected Label lblBoardMemberNameSearchDatagridIcons;
        protected Label lblBoardMemberPersonalID;
        protected Label lblBoardMembers;
        protected Label lblBoardMembersSubHeader;
        protected Label lblBoardMemberSurName;
        protected Label lblBoardMOverviewDatagridHeader;
        protected Label lblBoardMOverviewDatagridIcons;
        protected Label lblCapital;
        protected Label lblCharges;
        protected Label lblChargesAmount;
        protected Label lblChargesBenificiary;
        protected Label lblChargesCurrency;
        protected Label lblChargesDatagridHeader;
        protected Label lblChargesDatagridIcons;
        protected Label lblChargesDatePrepared;
        protected Label lblChargesDateRegistered;
        protected Label lblChargesDescription;
        protected Label lblChargesDescriptionEN;
        protected Label lblChargesDescriptionNative;
        protected Label lblChargesMessage;
        protected Label lblChargesSequence;
        protected Label lblCity;
        protected Label lblCitySearch;
        protected Label lblCurrency;
        protected Label lblEmail;
        protected Label lblErrorMessage;
        protected Label lblEstablished;
        protected Label lblFax;
        protected Label lblFormerName;
        protected Label lblFormerNameEN;
        protected Label lblHisOpReview;
        protected Label lblHistoryE;
        protected Label lblHistoryN;
        protected Label lblHomePage;
        protected Label lblIssuedNoOfShares;
        protected Label lblLAR;
        protected Label lblLastContacted;
        protected Label lblLastUpdated;
        protected Label lblLegalForm;
        protected Label lblLR;
        protected Label lblMobile;
        protected Label lblName;
        protected Label lblNameEN;
        protected Label lblNominalNumberOfShares;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        protected Label lblPaidUp;
        protected Label lblPostBox;
        protected Label lblRegisted;
        protected Label lblRegisteredCompanySecretaryDatagridHeader;
        protected Label lblRegisteredCompanySecretaryDatagridIcons;
        protected Label lblRegMsg;
        protected Label lblSecretary;
        protected Label lblSecretaryCIID;
        protected Label lblSecretaryFirstName;
        protected Label lblSecretaryHistoryEnglish;
        protected Label lblSecretaryHistoryNative;
        protected Label lblSecretaryMsg;
        protected Label lblSecretaryNameSearchDatagridHeader;
        protected Label lblSecretaryNameSearchDatagridIcons;
        protected Label lblSecretaryNationalID;
        protected Label lblSecretarySubHeader;
        protected Label lblSecretarySurName;
        protected Label lblShareholdersDatagridHeader;
        protected Label lblShareholdersDatagridIcons;
        protected Label lblShareholdersOwners;
        protected Label lblShareOwnerCIID;
        protected Label lblShareOwnerErrMsg;
        protected Label lblShareOwnerFirstName;
        protected Label lblShareOwnerHistoryEN;
        protected Label lblShareOwnerHistoryNative;
        protected Label lblShareOwnerNameSearchDatagridHeader;
        protected Label lblShareOwnerNameSearchDatagridIcons;
        protected Label lblShareOwnerNationalID;
        protected Label lblShareOwnerOwnership;
        protected Label lblShareOwnerOwnershipType;
        protected Label lblShareOwnerSurname;
        protected Label lblSharesDescription;
        protected Label lblSymbol;
        protected Label lblTel;
        protected Label lblTradeName;
        protected Label lblTradeNameEN;
        protected Label lblUniqueID;
        protected Label lblVAT;
        protected Label lbStructureResearchDate;
        private bool nativeCult;
        private bool newRegistration;
        protected RadioButtonList rblSecretaryPersonCompany;
        protected RadioButtonList rblShareOwnerPersComp;
        protected ValidationSummary SecretaryValidationsummary;
        protected ValidationSummary ShareholdersValidationsummary;
        protected HtmlTable tableBoardMemberNameSearch;
        protected HtmlTable tableBoardMOverview;
        protected HtmlTable tableCharges;
        protected HtmlTable tableRegisteredCompanySecretary;
        protected HtmlTable tableSecretaryNameSearch;
        protected HtmlTable tableShareholdersGrid;
        protected HtmlTable tableShareOwnerNameSearchGrid;
        protected TextBox tbAddress;
        protected TextBox tbAreaCode;
        protected TextBox tbBoardMemberAddress;
        protected TextBox tbBoardMemberCIID;
        protected TextBox tbBoardMemberFirstName;
        protected TextBox tbBoardMemberHistoryEN;
        protected TextBox tbBoardMemberHistoryNative;
        protected TextBox tbBoardMemberPersonalID;
        protected TextBox tbBoardMemberSurName;
        protected TextBox tbChargesAmount;
        protected TextBox tbChargesDatePrepared;
        protected TextBox tbChargesDateRegistered;
        protected TextBox tbChargesDescriptionEN;
        protected TextBox tbChargesDescriptionNative;
        protected TextBox tbChargesSequence;
        protected TextBox tbEmail;
        protected TextBox tbEstablished;
        protected TextBox tbFax;
        protected TextBox tbFormerName;
        protected TextBox tbHistoryE;
        protected TextBox tbHistoryN;
        protected TextBox tbHomePage;
        protected TextBox tbLAR;
        protected TextBox tbLastContacted;
        protected TextBox tbLastUpdated;
        protected TextBox tbLR;
        protected TextBox tbMobile;
        protected TextBox tbName;
        protected TextBox tbPostbox;
        protected TextBox tbRegisted;
        protected TextBox tbSecretaryCIID;
        protected TextBox tbSecretaryFirstName;
        protected TextBox tbSecretaryHistoryEnglish;
        protected TextBox tbSecretaryHistoryNative;
        protected TextBox tbSecretaryNationalID;
        protected TextBox tbSecretarySurName;
        protected TextBox tbShareOwnerFirstname;
        protected TextBox tbShareOwnerHistoryEN;
        protected TextBox tbShareOwnerHistoryNative;
        protected TextBox tbShareOwnerNationalID;
        protected TextBox tbShareOwnerOwnership;
        protected TextBox tbShareOwnerSurname;
        protected TextBox tbStructureReportResearchDate;
        protected TextBox tbTel;
        protected TextBox tbTradeName;
        protected TextBox tbUniqueID;
        protected TextBox tbVAT;
        protected HtmlTableRow Td1;
        protected HtmlTableRow tdShareOwnerHistory;
        protected TextBox Textbox6;
        private ReportCompanyBLLC theRCompany = new ReportCompanyBLLC();
        protected TextBox txtAddressEN;
        protected TextBox txtAddressNative;
        protected TextBox txtAsked;
        protected TextBox txtAuthorizedCapital;
        protected TextBox txtCitySearch;
        protected TextBox txtFormerNameEN;
        protected TextBox txtID;
        protected TextBox txtIssuedNoOfShares;
        protected TextBox txtNameEN;
        protected TextBox txtNominalNumberOfShares;
        protected TextBox txtPaidUp;
        protected TextBox txtPostalCode;
        protected TextBox txtShareOwnerCIID;
        protected TextBox txtSharesDescription;
        protected TextBox txtSymbol;
        protected TextBox txtTradeNameEN;
        protected CustomValidator validShareOwnerNatID;

        private void Page_Load(object sender, EventArgs e) {
            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else if (Request.QueryString["action"] == null) {
                Server.Transfer("SearchForReport.aspx?redirect=StructureReportInput");
            }

            if (CigConfig.Configure("lookupsettings.PageIDStructureReport.aspx") != null) {
                Page.ID = CigConfig.Configure("lookupsettings.PageIDStructureReport.aspx");
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (Request.QueryString["action"] != null) {
                if (Request.QueryString["action"].Equals("new")) {
                    newRegistration = true;
                } else {
                    newRegistration = false;
                }
                if (Request.QueryString["uniqueid"] != null) {
                    tbUniqueID.Text = Request.QueryString["uniqueid"];
                }
            } else {
                newRegistration = true;
            }

            if (CIID > 0) // athuga hvort � session s� report � �ennan a�ila
            {
                if (Session["ReportCompany"] != null) {
                    newRegistration = false;
                }
            }

            if (newRegistration) {
                SetRegSaveCommand();
            } else {
                SetRegUpdateCommand();
            }

            // check the current culture
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            String nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            // Add <ENTER> event // hmm? adding <ENTER> event disables all custom validators !?  (How stupid can that be ...?)
//			AddEnterEvent();
            InitGrids();
            // error labels
            lblShareOwnerErrMsg.Visible = false;
            // hmm kannski ekki gott a� gera �etta h�r...
            FillDataSets();
            if (!IsPostBack) {
                if (!newRegistration) {
                    FillDataSets();
                    BindBoxes();
                    BindGrids();
                    InitOtherBoxes();
                } else {
                    DateTime.Today.ToShortDateString();
                    tbEstablished.Text = DateTime.Today.ToShortDateString();
                    tbLastContacted.Text = DateTime.Today.ToShortDateString();
                    tbLastUpdated.Text = DateTime.Today.ToShortDateString();

                    // hva� me� �j��skr�rtengingu h�r og s�kja grunnuppl�singar ...?
                    // nota frekar CIG uppl�singar ef til eru ...?
                    string reg = CigConfig.Configure("lookupsettings.CompanyNationalRegistry");
                    // athugar hvort CIG uppl�singar eru til sta�ar
                    if (tbUniqueID.Text.Trim().Length > 0) {
                        if (!InitBoxesFromCIG(tbUniqueID.Text)) {
                            // ef ekki CIG uppl�singar athuga hvort �j��askr�rtenging er til sta�ar
                            if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                                // use national registry
                                if (tbUniqueID.Text.Trim().Length > 0) {
                                    InitBoxesFromNationalRegistry(tbUniqueID.Text);
                                }
                            }
                        }
                    }
                    FillDataSets();
                    BindBoxes();
                    InitOtherBoxes();
                }
                LocalizeText();
                tableShareOwnerNameSearchGrid.Visible = false;
                tableBoardMemberNameSearch.Visible = false;
                tableSecretaryNameSearch.Visible = false;
            } else // IsPostBack
            {
                //	string[] allkeys = Request.Form.AllKeys["btnRegShareOwners"];
                // athuga h�r hvern einasta hnapp
                string[] formButtonsName = {
                                               "btnRegCharges", "btnRegCapital", "btnRegSecretary", "btnRegBoardMember",
                                               "btnRegShareOwner", "btnBasicReg"
                                           };
                var requestButtonName = new string[2];
                foreach (string btnName in formButtonsName) {
                    requestButtonName = Request.Form.GetValues(btnName); //.AllKeys;
                    if (requestButtonName != null) {
                        SetButtonClicked(btnName);
                        break;
                    }
                }
            }
        }

        private void LocalizeText() {
            // basic reg
            LocalizeBasicText();
            // shareholders/owners
            LocalizeShareholdersOwnerText();
            // board members
            LocalizeBoardMemberText();
            // secretary
            LocalizeSecretaryText();
            // capital
            LocalizeCapitalText();
            // charges
            LocalizeCargesText();
        }

        public void SetRegSaveCommand() {
            // basic reg
            btnBasicReg.Text = rm.GetString("txtSave", ci);
            btnBasicReg.CommandName = "Registration";
            // shareholders/owners
            btnRegShareOwner.Text = rm.GetString("txtSave", ci);
            btnRegShareOwner.CommandName = "Registration";
            // board members
            btnRegBoardMember.Text = rm.GetString("txtSave", ci);
            btnRegBoardMember.CommandName = "Registration";
            // secretary
            btnRegSecretary.Text = rm.GetString("txtSave", ci);
            btnRegSecretary.CommandName = "Registration";
            // capital
            btnRegCapital.Text = rm.GetString("txtSave", ci);
            btnRegCapital.CommandName = "Registration";
            // charges
            btnRegCharges.Text = rm.GetString("txtSave", ci);
            btnRegCharges.CommandName = "Registration";
        }

        public void SetRegUpdateCommand() {
            // basic reg
            btnBasicReg.Text = rm.GetString("txtUpdate", ci);
            btnBasicReg.CommandName = "Update";
            // shareholders/owners
            btnRegShareOwner.Text = rm.GetString("txtUpdate", ci);
            btnRegShareOwner.CommandName = "Update";
            // board members
            btnRegBoardMember.Text = rm.GetString("txtUpdate", ci);
            btnRegBoardMember.CommandName = "Update";
            // secretary
            btnRegSecretary.Text = rm.GetString("txtUpdate", ci);
            btnRegSecretary.CommandName = "Update";
            // capital
            btnRegCapital.Text = rm.GetString("txtUpdate", ci);
            btnRegCapital.CommandName = "Update";
            // charges
            btnRegCharges.Text = rm.GetString("txtUpdate", ci);
            btnRegCharges.CommandName = "Update";
        }

        public void FillDataSets() {
            FillBasicInfoDataSet();
            FillShareHoldersOwnerDataSet();
            FillBoardMembersDataSet();
            FillSecretaryDataSet();
            FillChargesDataSet();
        }

        public void BindBoxes() {
            BindBasicInfoBoxes();
            BindOwnershipTypeBox();
            BindBoardMembersBoxes();
            BindCapitalBoxes();
            BindChargesBoxes();
        }

        public void BindOwnershipTypeBox() {
            var dsOwnershipTypes = new DataSet();
            if (Cache["dsOwnershipTypes"] == null) {
                dsOwnershipTypes = myFact.GetOwnershipTypesAsDataSet();
                Cache["dsOwnershipTypes"] = dsOwnershipTypes;
            } else {
                dsOwnershipTypes = (DataSet) Cache["dsOwnershipTypes"];
            }

            if (dsOwnershipTypes.Tables.Count > 0) {
                if (nativeCult) {
                    // 03112004 disable ordering
                    // dsOwnershipTypes.Tables[0].DefaultView.Sort = "OwnershipDescriptionNative";
                    ddlShareOwnerOwnershipType.DataSource = dsOwnershipTypes.Tables[0].DefaultView;
                    ddlShareOwnerOwnershipType.DataTextField = "OwnershipDescriptionNative";
                } else {
                    // 03112004 disable ordering
                    //dsOwnershipTypes.Tables[0].DefaultView.Sort = "OwnershipDescriptionEN";
                    ddlShareOwnerOwnershipType.DataSource = dsOwnershipTypes.Tables[0].DefaultView;
                    ddlShareOwnerOwnershipType.DataTextField = "OwnershipDescriptionEN";
                }
                ddlShareOwnerOwnershipType.DataValueField = "OwnershipID";
                ddlShareOwnerOwnershipType.DataBind();
            } else {
                Logger.WriteToLog("Could not get ownership types to display in ShareholdersOwners.aspx", true);
                lblShareOwnerErrMsg.Text = "Could not get ownership types overview";
                lblShareOwnerErrMsg.Visible = true;
            }
        }

        public void BindBasicInfoBoxes() {
            ddLegalForm.DataSource = dsRegistrationForms;
            ddLegalForm.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddLegalForm.DataValueField = "FormID";
            ddLegalForm.DataBind();
        }

        // basic reg
        public void LocalizeBasicText() {
            lblPageTitle.Text = rm.GetString("txtInformationEntry", ci);
            lblBasicInfo.Text = rm.GetString("txtBasicInfo", ci);
            lblLegalForm.Text = rm.GetString("txtLegalForm", ci);
            lblName.Text = rm.GetString("txtName", ci);
            lblNameEN.Text = rm.GetString("txtNameEN", ci);
            lblUniqueID.Text = rm.GetString("txtUniqueID", ci);
            lblSymbol.Text = rm.GetString("txtSymbol", ci);
            lblTradeName.Text = rm.GetString("txtTradeName", ci);
            lblTradeNameEN.Text = rm.GetString("txtTradeNameEN", ci);
            lblFormerName.Text = rm.GetString("txtFormerName", ci);
            lblFormerNameEN.Text = rm.GetString("txtFormerNameEN", ci);
            lblEmail.Text = rm.GetString("txtEmail", ci);
            lblTel.Text = rm.GetString("txtTelephone", ci);
            lblMobile.Text = rm.GetString("txtMobile", ci);
            lblFax.Text = rm.GetString("txtFax", ci);

            lblAddressHeader.Text = rm.GetString("txtAddress", ci);
            lblAddress.Text = rm.GetString("txtAddress", ci);
            lblAddressEN.Text = rm.GetString("txtAddressEN", ci);
            lblAreaCode.Text = rm.GetString("txtAreaCode", ci);
            lblPostBox.Text = rm.GetString("txtPostbox", ci);
            lblCity.Text = rm.GetString("txtCity", ci);

            lblAdditionalInfoHeader.Text = rm.GetString("txtAdditionalInfo", ci);
            lblHomePage.Text = rm.GetString("txtHomepage", ci);
            lblPostBox.Text = rm.GetString("txtPostbox", ci);
            lblEstablished.Text = rm.GetString("txtEstablished", ci);
            lblLastContacted.Text = rm.GetString("txtLastContacted", ci);
            lblLastUpdated.Text = rm.GetString("txtLastUpdated", ci);
            //	this.btnReg.Text = rm.GetString("txtRegister",ci);
            custDateLastUpdateBasic.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            custValDatePrepCharges.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            custValDatRegCharges.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            custValidEstablishedDateBasic.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            custValidLARBasic.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            custValidLastContactedDateBasic.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            custValAmountCharges.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValBasicName.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValBasicUniqueID.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidAddressBasic.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidAsked.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidCityBasic.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidFirstNameBoardMember.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidFirstNameSecretary.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidNationalIDSecretary.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidNomNumberShares.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidPersIDBoardMembers.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidSurNameBoardMember.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValIssuedShares.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValPaidUp.ErrorMessage = rm.GetString("txtValueMissing", ci);

            btnBasicCancel.Text = rm.GetString("txtCancel", ci);

            lblVAT.Text = rm.GetString("txtVAT", ci);

            lblCitySearch.Text = rm.GetString("txtSearch", ci);
        }

        // shareholders/owners
        public void LocalizeShareholdersOwnerText() {
            lblShareOwnerCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblShareholdersOwners.Text = rm.GetString("txtShareholders/Owner", ci);
            lblShareOwnerFirstName.Text = rm.GetString("txtFirstName", ci);
            lblShareOwnerSurname.Text = rm.GetString("txtSurName", ci);
            lblShareOwnerNationalID.Text = rm.GetString("txtNationalID", ci);
            btnShareOwnerSearch.Text = rm.GetString("txtSearch", ci);
            lblShareOwnerOwnership.Text = rm.GetString("txtOwnership", ci);
            lblShareOwnerOwnershipType.Text = rm.GetString("txtOwnershipType", ci);
            rblShareOwnerPersComp.Items[0].Text = rm.GetString("txtPerson", ci);
            rblShareOwnerPersComp.Items[1].Text = rm.GetString("txtCompany", ci);
            btnRegShareOwner.Text = rm.GetString("txtSave", ci);
            //	this.btnCancel.Text = rm.GetString("txtCancel",ci);
            btnCancelShareOwner.Text = rm.GetString("txtBack", ci);

            lblShareOwnerNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblShareholdersDatagridHeader.Text = rm.GetString("txtShareholders/Owner", ci);

            // localize the grid headers
            dtgrShareOwnerNameSearch.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrShareOwnerNameSearch.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrShareOwnerNameSearch.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrShareOwnerNameSearch.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dtgrShareOwnerNameSearch.Columns[9].HeaderText = rm.GetString("txtAreaCode", ci);

            // localize registed grid headers
            dtgrShareholders.Columns[2].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrShareholders.Columns[3].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrShareholders.Columns[4].HeaderText = rm.GetString("txtName", ci);
            dtgrShareholders.Columns[9].HeaderText = rm.GetString("txtOwnership", ci);
        }

        // board members
        public void LocalizeBoardMemberText() {
            lblBoardMemberCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblBoardMembers.Text = rm.GetString("txtBoardMembers", ci);
            lblBoardMemberFirstName.Text = rm.GetString("txtFirstName", ci);
            lblBoardMemberSurName.Text = rm.GetString("txtSurName", ci);
            lblBoardMemberPersonalID.Text = rm.GetString("txtPersonalID", ci);
            lblBoardMemberManagementPosition.Text = rm.GetString("txtManagementPosition", ci);
            btnRegBoardMember.Text = rm.GetString("txtSave", ci);
            btnBoardMemberSearch.Text = rm.GetString("txtSearch", ci);
            custValidFirstNameBoardMember.ErrorMessage = rm.GetString("txtFirstNameMissing", ci);
            custValidSurNameBoardMember.ErrorMessage = rm.GetString("txtSurNameMissing", ci);
            custValidPersIDBoardMembers.ErrorMessage = rm.GetString("txtPersonalIDMissing", ci);
            btnRegBoardMemberHistory.Text = rm.GetString("txtSaveHistory", ci);
            lblBoardMemberHistoryEN.Text = rm.GetString("txtHistoryEnglish", ci);
            lblBoardMemberHistoryNative.Text = rm.GetString("txtHistoryNative", ci);
            lblBoardMemberNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblBoardMOverviewDatagridHeader.Text = rm.GetString("txtBoardMembers", ci);

            // h�r �ar einnig a� localize headers � search griddi
            LocalizeBoardMemberNameSearchGridHeader();
        }

        private void LocalizeBoardMemberNameSearchGridHeader() {
            dtgrBoardMemberNameSearch.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrBoardMemberNameSearch.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrBoardMemberNameSearch.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrBoardMemberNameSearch.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dtgrBoardMemberNameSearch.Columns[9].HeaderText = rm.GetString("txtAreaCode", ci);
        }

        // secretary
        public void LocalizeSecretaryText() {
            lblSecretaryCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblSecretaryFirstName.Text = rm.GetString("txtFirstName", ci);
            lblSecretaryHistoryEnglish.Text = rm.GetString("txtHistoryEnglish", ci);
            lblSecretaryHistoryNative.Text = rm.GetString("txtHistoryNative", ci);
            lblSecretaryNationalID.Text = rm.GetString("txtNationalID", ci);
            lblSecretary.Text = rm.GetString("txtBoardSecretary", ci);
            lblSecretarySurName.Text = rm.GetString("txtSurName", ci);

            //			this.btnCancel.Text = rm.GetString("txtCancel", ci);
            btnCancelSecretary.Text = rm.GetString("txtBack", ci);
            btnRegSecretary.Text = rm.GetString("txtSave", ci);
            btnSecretarySearch.Text = rm.GetString("txtSearch", ci);

            custValidFirstNameSecretary.ErrorMessage = rm.GetString("txtValueMissing", ci);
            custValidNationalIDSecretary.ErrorMessage = rm.GetString("txtValueMissing", ci);

            lblSecretaryNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblRegisteredCompanySecretaryDatagridHeader.Text = rm.GetString("txtBoardSecretary", ci);

            LocalizeSecretarySearchGridHeader();
            LocalizeBoardSecretaryGridHeader();
        }

        /// <summary>
        /// Localizes the search grid header
        /// </summary>
        private void LocalizeSecretarySearchGridHeader() {
            dtgrSecretaryNameSearch.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrSecretaryNameSearch.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrSecretaryNameSearch.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrSecretaryNameSearch.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dtgrSecretaryNameSearch.Columns[9].HeaderText = rm.GetString("txtAreaCode", ci);
        }

        private void LocalizeBoardSecretaryGridHeader() {
            dgRegisteredCompanySecretary.Columns[2].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dgRegisteredCompanySecretary.Columns[3].HeaderText = rm.GetString("txtNationalID", ci);
            dgRegisteredCompanySecretary.Columns[4].HeaderText = rm.GetString("txtName", ci);
        }

        // capital
        public void LocalizeCapitalText() {
            lblAsked.Text = rm.GetString("txtAsked", ci);
            lblAuthorizedCapital.Text = rm.GetString("txtAuthorizedCapital", ci);
            lblCapital.Text = rm.GetString("txtCapital", ci);
            lblIssuedNoOfShares.Text = rm.GetString("txtIssuedNumberOfShares", ci);
            lblNominalNumberOfShares.Text = rm.GetString("txtNominalNumberOfShares", ci);
            lblPaidUp.Text = rm.GetString("txtPaidUp", ci);
            lblSharesDescription.Text = rm.GetString("txtSharesDescription", ci);
            lblCurrency.Text = rm.GetString("txtCurrency", ci);

            custValidAsked.ErrorMessage = rm.GetString("txtCapitalError", ci);
            custValidAuCapital.ErrorMessage = rm.GetString("txtCapitalError", ci);
            custValIssuedShares.ErrorMessage = rm.GetString("txtCapitalError", ci);
            custValidNomNumberShares.ErrorMessage = rm.GetString("txtCapitalError", ci);
            custValPaidUp.ErrorMessage = rm.GetString("txtCapitalError", ci);

            btnRegCapital.Text = rm.GetString("txtSave", ci);
            //	this.btnCancel.Text = rm.GetString("txtCancel",ci);
            btnCancelCapital.Text = rm.GetString("txtBack", ci);
        }

        // charges
        public void LocalizeCargesText() {
            lblChargesAmount.Text = rm.GetString("txtAmount", ci);
            lblChargesBenificiary.Text = rm.GetString("txtBeneficiary", ci);
            lblCharges.Text = rm.GetString("txtCharges", ci);
            lblChargesCurrency.Text = rm.GetString("txtCurrency", ci);
            lblChargesDatePrepared.Text = rm.GetString("txtDatePrepared", ci);
            lblChargesDateRegistered.Text = rm.GetString("txtDateRegistered", ci);
            lblChargesDescription.Text = rm.GetString("txtType", ci);
            lblChargesSequence.Text = rm.GetString("txtSequence", ci);
            btnRegCharges.Text = rm.GetString("txtSave", ci);
            //	this.btnCancel.Text = rm.GetString("txtCancel",ci);
            btnCancelCharges.Text = rm.GetString("txtBack", ci);

            lblChargesDatagridHeader.Text = rm.GetString("txtCharges", ci);

            custValAmountCharges.ErrorMessage = rm.GetString("txtChargesError", ci);
            custValDatePrepCharges.ErrorMessage = rm.GetString("txtChargesError", ci);
            custValDatRegCharges.ErrorMessage = rm.GetString("txtChargesError", ci);

            lblChargesDescriptionNative.Text = rm.GetString("txtDescription", ci);
            lblChargesDescriptionEN.Text = rm.GetString("txtDescriptionEN", ci);

            SetGridChargesHeaders();
        }

        public void AddEnterEvent() {
            AddBasicInfoEnterEvent();
            AddShareholderOwnerEnterEvent();
            AddBoardMemberEnterEvent();
            AddSecretaryEnterEvent();
            AddCapitalEnterEvent();
            AddChargesEnterEvent();
        }

        private void AddShareholderOwnerEnterEvent() {
            txtShareOwnerCIID.Attributes.Add("onkeypress", "checkShareholdersOwnerEnterKey();");
            tbShareOwnerNationalID.Attributes.Add("onkeypress", "checkShareholdersOwnerEnterKey();");
            tbShareOwnerFirstname.Attributes.Add("onkeypress", "checkShareholdersOwnerEnterKey();");
            tbShareOwnerSurname.Attributes.Add("onkeypress", "checkShareholdersOwnerEnterKey();");
            tbShareOwnerOwnership.Attributes.Add("onkeypress", "checkShareholdersOwnerEnterKey;");
            rblShareOwnerPersComp.Attributes.Add("onkeypress", "checkShareholdersOwnerEnterKey;");
            ddlShareOwnerOwnershipType.Attributes.Add("onkeypress", "checkShareholdersOwnerEnterKey;");
            tbShareOwnerHistoryNative.Attributes.Add("onkeypress", "checkShareholdersOwnerEnterKey;");
            tbShareOwnerHistoryEN.Attributes.Add("onkeypress", "checkShareholdersOwnerEnterKey;");
        }

        public void InitOtherBoxes() {
            InitBasicBoxes();
            InitShareholdersOwnerBoxes();
            InitBoardMemberBoxes();
            InitSecretaryBoxes();
            InitCapitalBoxes();
            InitChargesBoxes();
        }

        public void InitBasicBoxes() {
            if (Session["ReportCompany"] != null) {
                theRCompany = (ReportCompanyBLLC) Session["ReportCompany"];
            } else {
                theRCompany = myFact.GetCompanyReport(null, CIID, false);
            }

            tbName.Text = theRCompany.NameNative;
            tbFormerName.Text = theRCompany.FormerNameNative;
            tbTradeName.Text = theRCompany.TradeNameNative;

            txtNameEN.Text = theRCompany.NameEN;
            txtFormerNameEN.Text = theRCompany.FormerNameEN;
            txtTradeNameEN.Text = theRCompany.TradeNameEN;

            txtSymbol.Text = theRCompany.Symbol;

            if (CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus")) {
                if (theRCompany.LAR != DateTime.MinValue) {
                    tbLAR.Text = theRCompany.LAR.ToShortDateString();
                }
                if (theRCompany.LR != DateTime.MinValue) {
                    tbLR.Text = theRCompany.LR.ToShortDateString();
                }
                if (theRCompany.StructureReportResearchDate != DateTime.MinValue) {
                    tbStructureReportResearchDate.Text = theRCompany.StructureReportResearchDate.ToShortDateString();
                }
            }

            if (theRCompany.Address.Count > 0) {
                tbAddress.Text = ((Address) theRCompany.Address[0]).StreetNative;
                txtAddressEN.Text = ((Address) theRCompany.Address[0]).StreetEN;
                //TODO :: H�r �arf a� s�kja nafn � borg ef value er st�rra en 0 og inserta �v� inn � listbox
                int cityID = ((Address) theRCompany.Address[0]).CityID;
                if (cityID > -1) {
                    string cityName = myFact.GetCityName(cityID, !nativeCult);
                    if (!string.IsNullOrEmpty(cityName)) {
                        ddCity.Items.Add(new ListItem(cityName, cityID.ToString()));
                        ddCity.SelectedValue = cityID.ToString();
                    }
                }
                //this.ddCity.SelectedValue = ((Address)theRCompany.Address[0]).CityID.ToString();

                tbAreaCode.Text = ((Address) theRCompany.Address[0]).PostalCode;
                tbPostbox.Text = ((Address) theRCompany.Address[0]).PostBox;
            }

            tbEmail.Text = theRCompany.Email;

            if (theRCompany.Established.Year > 1899) {
                tbEstablished.Text = theRCompany.Established.ToShortDateString();
            }
            if (theRCompany.Registered.Year > 1899) {
                tbRegisted.Text = theRCompany.Registered.ToShortDateString();
            }
            if (theRCompany.LastContacted.Year > 1899) {
                tbLastContacted.Text = theRCompany.LastContacted.ToShortDateString();
            }
            if (theRCompany.LastUpdated.Year > 1899) {
                tbLastUpdated.Text = theRCompany.LastUpdated.ToShortDateString();
            }

            tbHomePage.Text = theRCompany.HomePage;

            tbUniqueID.Text = theRCompany.UniqueID;
            try {
                ddLegalForm.SelectedValue = theRCompany.RegistrationFormID.ToString();
            } catch (Exception) {}
            tbVAT.Text = theRCompany.VAT;

            try {
                foreach (PhoneNumber myNumber in theRCompany.PNumbers) {
                    if (CigConfig.Configure("lookupsettings.workCode") == Convert.ToString(myNumber.NumberTypeID)) {
                        tbTel.Text = myNumber.Number;
                    } else if (CigConfig.Configure("lookupsettings.mobileCode") == Convert.ToString(myNumber.NumberTypeID)) {
                        tbMobile.Text = myNumber.Number;
                    } else if (CigConfig.Configure("lookupsettings.faxCode") == Convert.ToString(myNumber.NumberTypeID)) {
                        tbFax.Text = myNumber.Number;
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(pageName + "::InitOtherBoxes() -" + err.Message + "--" + err.StackTrace, true);
            }
            // taka h�r historyEN og native
            var theReview = new HistoryOperationReviewBLLC();
            // fylla �etta tilvik o.sv.frv...
            theReview = myFact.GetHistoryOperationReview(CIID);
            tbHistoryE.Text = theReview.HistoryEN;
            tbHistoryN.Text = theReview.HistoryNative;
        }

        public void InitShareholdersOwnerBoxes() {
            // set historoy boxes
            HistoryBLLC theHistory = new HistoryBLLC();
            theHistory = myFact.GetHistory(
                CIID, Convert.ToInt32(CigConfig.Configure("lookupsettings.ShareholdersOwnerHistoryType")));
            tbShareOwnerHistoryNative.Text = theHistory.HistoryNative;
            tbShareOwnerHistoryEN.Text = theHistory.HistoryEN;
        }

        public void InitBoardMemberBoxes() { SetBoardMembersHistoryBoxes(); }
        public void InitSecretaryBoxes() { }

        public void InitCapitalBoxes() {
            lblErrorMessage.Visible = false;

            fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            fInfo.NumberDecimalDigits = 2;

            txtAuthorizedCapital.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" +
                                                        fInfo.NumberGroupSeparator + "', '" +
                                                        fInfo.NumberDecimalSeparator + "', 2,'NaN');";
            txtAsked.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" + fInfo.NumberGroupSeparator +
                                            "', '" + fInfo.NumberDecimalSeparator + "', 2,'NaN');";
            txtIssuedNoOfShares.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" +
                                                       fInfo.NumberGroupSeparator + "', '" +
                                                       fInfo.NumberDecimalSeparator + "', 2,'NaN');";
            txtNominalNumberOfShares.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" +
                                                            fInfo.NumberGroupSeparator + "', '" +
                                                            fInfo.NumberDecimalSeparator + "', 2,'NaN');";
            txtPaidUp.Attributes["onblur"] = "javascript: return CurrencyBlur(this, '" + fInfo.NumberGroupSeparator +
                                             "', '" + fInfo.NumberDecimalSeparator + "', 2,'NaN');";

            LoadCapitalInfo();
        }

        public void InitChargesBoxes() {
            tbChargesDatePrepared.Text = DateTime.Today.ToShortDateString();
            tbChargesDateRegistered.Text = DateTime.Today.ToShortDateString();
        }

        /// <summary>
        /// Searches in the CIG for information about given unique id (national id)
        /// </summary>
        /// <param name="uniqueID">The unique (national) id to search for</param>
        /// <returns>true if information found, false if not</returns>
        private bool InitBoxesFromCIG(string uniqueID) {
            try {
                int iCIID = myFact.GetCIIDByNationalID(uniqueID);
                if (iCIID > 0) {
                    Company companyInfo = myFact.GetCompany(iCIID);
                    if (companyInfo != null) {
                        if (nativeCult) {
                            tbName.Text = companyInfo.NameNative;
                            tbAddress.Text = companyInfo.SingleAddressNative;
                        } else {
                            tbName.Text = companyInfo.NameEN;
                            tbAddress.Text = companyInfo.SingleAddressEN;
                        }

                        CIID = iCIID;
                        tbEmail.Text = companyInfo.Email;
                        tbEstablished.Text = companyInfo.Established.ToShortDateString();
                        tbHomePage.Text = companyInfo.URL;
                        tbLastContacted.Text = companyInfo.LastContacted.ToShortDateString();
                        tbLastUpdated.Text = companyInfo.LastUpdate.ToShortDateString();

                        if (companyInfo.Number != null) {
                            foreach (PhoneNumber myNumber in companyInfo.Number) {
                                if (CigConfig.Configure("lookupsettings.workCode") ==
                                    Convert.ToString(myNumber.NumberTypeID)) {
                                    tbTel.Text = myNumber.Number;
                                } else if (CigConfig.Configure("lookupsettings.mobileCode") ==
                                           Convert.ToString(myNumber.NumberTypeID)) {
                                    tbMobile.Text = myNumber.Number;
                                } else if (CigConfig.Configure("lookupsettings.faxCode") ==
                                           Convert.ToString(myNumber.NumberTypeID)) {
                                    tbFax.Text = myNumber.Number;
                                }
                            }
                        }
                        tbAreaCode.Text = "";
                        tbFormerName.Text = "";
                        tbPostbox.Text = "";
                        tbTradeName.Text = "";
                        tbVAT.Text = "";
                        return true;
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(
                    pageName + "::InitBoxesFromCIG(" + uniqueID + ") -" + err.Message + "--" + err.StackTrace, true);
            }
            return false;
        }

        /// <summary>
        /// Searches in national company registry for information about given unique id (SSN)
        /// </summary>
        /// <param name="uniqueID">The unique id to search for</param>
        private void InitBoxesFromNationalRegistry(string uniqueID) {
            try {
                DataSet ds = myFact.GetCompanyBySSNFromNationalInterfaceAsDataSet(uniqueID);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null) {
                    if (ds.Tables[0].Rows.Count > 0) {
                        tbName.Text = ds.Tables[0].Rows[0]["Name"].ToString().Trim();
                        tbAddress.Text = ds.Tables[0].Rows[0]["Address"].ToString().Trim();
                        tbTel.Text = ds.Tables[0].Rows[0]["PhoneNumber"].ToString().Trim();
                        tbFax.Text = ds.Tables[0].Rows[0]["FaxNumber"].ToString().Trim();
                        tbAreaCode.Text = ds.Tables[0].Rows[0]["PostalCode"].ToString().Trim();
                        tbPostbox.Text = ds.Tables[0].Rows[0]["POBox"].ToString().Trim();
                        tbVAT.Text = ds.Tables[0].Rows[0]["VAT"].ToString().Trim();
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(
                    pageName + "::InitBoxesFromNationalRegistry(" + uniqueID + ") -" + err.Message + "--" +
                    err.StackTrace,
                    true);
            }
        }

        public void FillShareHoldersOwnerDataSet() {
            if (Session["dsShareHolders"] == null) {
                dsShareHolders = myFact.GetShareHolderOwnerAsDataSet(CIID);
                // af hverju er �etta?
                dsShareHolders = cleanDuplicatedCIIDS(dsShareHolders);
                Session["dsShareHolders"] = dsShareHolders;
            } else {
                dsShareHolders = (DataSet) Session["dsShareHolders"];
            }
        }

        public void FillBasicInfoDataSet() {
            if (Cache["dsRegistrationForms"] != null) {
                dsRegistrationForms = (DataSet) Cache["dsRegistrationForms"];
            } else {
                dsRegistrationForms = myFact.GetRegistrationFormAsDataSet();
            }
        }

        private static DataSet cleanDuplicatedCIIDS(DataSet ds) {
            if (ds.Tables.Count > 0) {
                var arrCiids = new ArrayList();
                var arrShareTypesIDs = new ArrayList();
                for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                    var creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    var shareTypesID = ds.Tables[0].Rows[i]["OwnershipID"].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (((string) arrCiids[j]).Equals(creditInfoId) &&
                            ((string) arrShareTypesIDs[j]).Equals(shareTypesID)) {
                            ds.Tables[0].Rows.Remove(ds.Tables[0].Rows[i]);
                            break;
                        }
                    }
                    arrCiids.Add(creditInfoId);
                    arrShareTypesIDs.Add(shareTypesID);
                }
            }
            return ds;
        }

        public void InitGrids() {
            InitShareHoldersGrid();
            InitBoardMembersGrid();
        }

        public void InitShareHoldersGrid() {
            dtgrShareholders.Columns[5].Visible = false;
            dtgrShareholders.Columns[6].Visible = false;
            dtgrShareholders.Columns[7].Visible = false;
            dtgrShareholders.Columns[8].Visible = false;
            dtgrShareholders.Columns[10].Visible = false;
            dtgrShareholders.Columns[11].Visible = false;
            dtgrShareholders.Columns[12].Visible = false;
        }

        public void BindGrids() {
            BindShareHoldersGrid();
            BindBoardMembersGrid();
        }

        public void BindShareHoldersGrid() {
            dtgrShareholders.DataSource = dsShareHolders;
            dtgrShareholders.DataBind();
        }

        public void BindBoardMembersGrid() {
            dtgrBoardMOverview.DataSource = dsDetailedBoardMembers;
            dtgrBoardMOverview.DataBind();
        }

        private void btnShareOwnerSearch_Click(object sender, EventArgs e) {
            var uaf = new uaFactory();

            DataSet dsNameSearch;
            if (rblShareOwnerPersComp.Items[0].Selected) {
                //Person
                var debtor = new Debtor();

                if (tbShareOwnerFirstname.Text.Trim() != "") {
                    debtor.FirstName = tbShareOwnerFirstname.Text;
                }
                if (tbShareOwnerSurname.Text.Trim() != "") {
                    debtor.SurName = tbShareOwnerSurname.Text;
                }
                if (txtShareOwnerCIID.Text.Trim() != "") {
                    debtor.CreditInfo = int.Parse(txtShareOwnerCIID.Text);
                }
                if (tbShareOwnerNationalID.Text.Trim() != "") {
                    debtor.IDNumber1 = tbShareOwnerNationalID.Text;
                    debtor.IDNumber2Type = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                }
                if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                    dsNameSearch = uaf.FindCustomerInNationalAndCreditInfo(debtor);
                    // if not found in national registry (f.ex. the Cyprus Whitelist is incomplete data) then search in our db
                    if (dsNameSearch.Tables.Count > 0) {
                        if (dsNameSearch.Tables[0] == null || dsNameSearch.Tables[0].Rows == null ||
                            dsNameSearch.Tables[0].Rows.Count <= 0) {
                            dsNameSearch = uaf.FindCustomer(debtor);
                        }
                    } else {
                        dsNameSearch = uaf.FindCustomer(debtor);
                    }
                } else {
                    dsNameSearch = uaf.FindCustomer(debtor);
                }
            } else {
                //Company				
                var company = new Company();

                if (tbShareOwnerFirstname.Text.Trim() != null) {
                    company.NameNative = tbShareOwnerFirstname.Text;
                }
                if (txtShareOwnerCIID.Text.Trim() != "") {
                    company.CreditInfoID = int.Parse(txtShareOwnerCIID.Text);
                }
                if (tbShareOwnerNationalID.Text.Trim() != "") {
                    var idn = new IDNumber
                              {
                                  Number = tbShareOwnerNationalID.Text,
                                  NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"))
                              };
                    company.IDNumbers = new ArrayList {idn};
                }
                if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                    dsNameSearch = uaf.FindCompanyInNationalAndCreditInfo(company);
                } else {
                    dsNameSearch = uaf.FindCompany(company);
                }
            }

            if (dsNameSearch.Tables.Count > 0 && dsNameSearch.Tables[0] != null) {
                if (dsNameSearch.Tables[0].Rows.Count > 0) {
                    dtgrShareOwnerNameSearch.DataSource = dsNameSearch;
                    dtgrShareOwnerNameSearch.DataBind();
                    tableShareOwnerNameSearchGrid.Visible = true;

                    var gridRows = dtgrShareOwnerNameSearch.Items.Count;
                    divShareOwnerNameSearch.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                    divShareOwnerNameSearch.Style["OVERFLOW"] = "auto";
                    divShareOwnerNameSearch.Style["OVERFLOW-X"] = "auto";
                } else {
                    DisplayMessage(rm.GetString("txtNoEntryFound", ci), lblShareOwnerErrMsg);
                    tableShareOwnerNameSearchGrid.Visible = false;
                }
            } else {
                tableShareOwnerNameSearchGrid.Visible = false;
            }
        }

        private static void DisplayMessage(String message, Label theLabel) {
            theLabel.Text = message;
            theLabel.ForeColor = Color.Blue;
            theLabel.Visible = true;
        }

        private void dtgrShareOwnerNameSearch_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }

                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }

                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);	
            }

            WebDesign.CreateExplanationIcons(
                dtgrShareOwnerNameSearch.Columns, lblShareOwnerNameSearchDatagridIcons, rm, ci);
        }

        private void dtgrShareOwnerNameSearch_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                /*	if(e.Item.Cells[7].Text != "&nbsp;")
						this.txtAddressNative.Text = e.Item.Cells[7].Text;
					if(e.Item.Cells[8].Text != "&nbsp;")
						this.txtAddressEN.Text = e.Item.Cells[8].Text;
					if(e.Item.Cells[9].Text != "&nbsp;")
						this.txtPostalCode.Text = e.Item.Cells[9].Text;
				*/

                switch (e.Item.Cells[1].Text) {
                    case "&nbsp;":
                        txtShareOwnerCIID.Text = "";
                        break;
                    default:
                        txtShareOwnerCIID.Text = e.Item.Cells[1].Text;
                        break;
                }
                tbShareOwnerNationalID.Text = e.Item.Cells[2].Text;
                if (tbShareOwnerNationalID.Text == "&nbsp;") {
                    tbShareOwnerNationalID.Text = "";
                }

                /*if(txtCIID.Text=="&nbsp;")
					txtCIID.Text="";*/

                string name = e.Item.Cells[3].Text.Trim();
                if (name == "&nbsp;") {
                    name = "";
                }
                if (rblShareOwnerPersComp.Items[0].Selected) {
                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            tbShareOwnerFirstname.Text = arrName[0];
                        } else {
                            tbShareOwnerSurname.Text = arrName[arrName.Length - 1];
                            tbShareOwnerFirstname.Text = name.Substring(
                                0, name.Length - (tbShareOwnerSurname.Text.Length + 1));
                        }
                    }
                } else {
                    tbShareOwnerFirstname.Text = name;
                }
                validShareOwnerNatID.Enabled = false;
                tableShareOwnerNameSearchGrid.Visible = false;
            }
        }

        private void btnRegShareOwner_Click(object sender, EventArgs e) {
            tableShareOwnerNameSearchGrid.Visible = false;
            bool person = rblShareOwnerPersComp.Items[0].Selected;
            bool insertOK = true;
            tableShareOwnerNameSearchGrid.Visible = false;
            if (Page.IsValid) {
                try {
                    Address myAddress = null;

                    if (person) // indivitual
                    {
                        ShareholdersOwnerPersonBLLC theSOPerson = new ShareholdersOwnerPersonBLLC();

                        if (nativeCult) {
                            theSOPerson.FirstNameNative = tbShareOwnerFirstname.Text;
                            theSOPerson.SurNameNative = tbShareOwnerSurname.Text;
                        } else {
                            theSOPerson.FirstNameEN = tbShareOwnerFirstname.Text;
                            theSOPerson.SurNameEN = tbShareOwnerSurname.Text;
                        }
                        if (txtShareOwnerCIID.Text.Trim() != "") {
                            theSOPerson.CreditInfoID = int.Parse(txtShareOwnerCIID.Text.Trim());
                        }
                        theSOPerson.Ownership = Convert.ToInt32(tbShareOwnerOwnership.Text);
                        theSOPerson.OwnershipTypeID = Convert.ToInt32(ddlShareOwnerOwnershipType.SelectedValue);
                        //	theSOPerson.Ownership = this.tbOwnership.Text;
                        theSOPerson.NationalID = tbShareOwnerNationalID.Text;

                        if (myAddress != null) {
                            theSOPerson.Address.Add(myAddress);
                        }

                        insertOK = myFact.AddShareholderOwner(theSOPerson, CIID);
                    } else // is company
                    {
                        ShareholdersOwnerCompanyBLLC theSOCompany = new ShareholdersOwnerCompanyBLLC();

                        if (nativeCult) {
                            theSOCompany.NameNative = tbShareOwnerFirstname.Text;
                        } else {
                            theSOCompany.NameEN = tbShareOwnerFirstname.Text;
                        }
                        theSOCompany.Ownership = Convert.ToInt32(tbShareOwnerOwnership.Text);
                        theSOCompany.OwnershipTypeID = Convert.ToInt32(ddlShareOwnerOwnershipType.SelectedValue);
                        //	theSOCompany.Ownership = this.tbOwnership.Text;
                        theSOCompany.NationalID = tbShareOwnerNationalID.Text;

                        if (myAddress != null) {
                            theSOCompany.Address.Add(myAddress);
                        }

                        insertOK = myFact.AddShareholderOwner(theSOCompany, CIID);
                    }
                } catch (Exception err) {
                    Logger.WriteToLog(err.ToString(), true);
                    insertOK = false;
                }
                insertOK = RegHistory(
                    Convert.ToInt32(CigConfig.Configure("lookupsettings.ShareholdersOwnerHistoryType")));
                if (!insertOK) {
                    DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci), lblShareOwnerErrMsg);
                } else {
                    DisplayMessage(rm.GetString("txtInfoSaved", ci), lblShareOwnerErrMsg);
                }
                // h�r �arf a� n�lla saerch griddi� og fela celluna

                Session["dsShareHolders"] = null;
                FillShareHoldersOwnerDataSet();
                BindShareHoldersGrid();
                ClearShareOwnersBoxes();
            }
        }

        private void DisplayErrorMessage(String errorMessage, Label theLabel) {
            theLabel.Text = errorMessage;
            theLabel.ForeColor = Color.Red;
            theLabel.Visible = true;
        }

        public void ClearShareOwnersBoxes() {
            txtShareOwnerCIID.Text = "";
            tbShareOwnerFirstname.Text = "";
            tbShareOwnerSurname.Text = "";
            tbShareOwnerNationalID.Text = "";
            tbShareOwnerOwnership.Text = "";
            // init button and NationalID field
            btnRegShareOwner.Text = rm.GetString("txtSave", ci);
            btnRegShareOwner.CommandName = "Update";
            tbShareOwnerNationalID.ReadOnly = false;
            ddlShareOwnerOwnershipType.SelectedIndex = -1;
            txtShareOwnerCIID.ReadOnly = false;
        }

        public void DateValidate(object source, ServerValidateEventArgs value) {
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime date = DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        public void ValidateShareholdersOwnersRequiredValues(object source, ServerValidateEventArgs value) {
            // custom event handler er ekki vakinn � t�m sv��i (arrg!!) ver� a� t�kka � sorce id og manually athuga vi�eigandi tb box (fjandi s�rt)
            if (!btnRegShareOwnerClicked) {
                value.IsValid = true;
                return;
            }
            var myValidShareOwnerNatID = (CustomValidator) source;
            string valName = myValidShareOwnerNatID.ID;
            switch (valName) {
                case "validShareOwnerNatID":
                    if (tbShareOwnerNationalID.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValShareholdersOwnerOwnership":
                    if (tbShareOwnerOwnership.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
            }
        }

        public void ValidateBasicInfoRequiredValues(object source, ServerValidateEventArgs value) {
            if (!btnBasicRegClicked) {
                value.IsValid = true;
                return;
            }
            string valName = "";
            var myValidator = (CustomValidator) source;
            valName = myValidator.ID;

            switch (valName) {
                case "custValBasicUniqueID":
                    if (tbUniqueID.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValBasicName":
                    if (tbName.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValidCityBasic":
                    if (ddCity.Items.Count <= 0) {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
            }
        }

        public void ValidateCapitalRequiredValues(object source, ServerValidateEventArgs value) {
            if (!btnRegCapitalClicked) {
                value.IsValid = true;
                return;
            }
            string valName = "";
            var myValidator = (CustomValidator) source;
            valName = myValidator.ID;

            switch (valName) {
                case "custValidAuCapital":
                    if (txtAuthorizedCapital.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValIssuedShares":
                    if (txtIssuedNoOfShares.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValidNomNumberShares":
                    if (txtNominalNumberOfShares.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValidAsked":
                    if (txtAsked.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValPaidUp":
                    if (txtPaidUp.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
            }
        }

        public void ValidateChargesRequiredValues(object source, ServerValidateEventArgs value) {
            if (!btnRegChargesClicked) {
                value.IsValid = true;
                return;
            }
            string valName = "";
            var myValidator = (CustomValidator) source;
            valName = myValidator.ID;

            switch (valName) {
                case "custValAmountCharges":
                    if (tbChargesAmount.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
            }
        }

        public void ValidateSecretaryRequiredValues(object source, ServerValidateEventArgs value) {
            if (!btnRegSecretaryClicked) {
                value.IsValid = true;
                return;
            }
            string valName = "";
            var myValidator = (CustomValidator) source;
            valName = myValidator.ID;

            switch (valName) {
                case "custValidNationalIDSecretary":
                    if (tbSecretaryNationalID.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValidFirstNameSecretary":
                    if (tbSecretaryFirstName.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
            }
        }

        public void ValidateBoardMemberRequiredValues(object source, ServerValidateEventArgs value) {
            if (!btnRegBoardMemberClicked) {
                value.IsValid = true;
                return;
            }
            string valName = "";
            var myValidator = (CustomValidator) source;
            valName = myValidator.ID;

            switch (valName) {
                case "custValidPersIDBoardMembers":
                    if (tbBoardMemberPersonalID.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValidFirstNameBoardMember":
                    if (tbBoardMemberFirstName.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
                case "custValidSurNameBoardMember":
                    if (tbBoardMemberSurName.Text == "") {
                        value.IsValid = false;
                        return;
                    }
                    value.IsValid = true;
                    break;
            }
        }

        public void BasicDateValidate(object source, ServerValidateEventArgs value) {
            if (!btnBasicRegClicked) {
                value.IsValid = true;
                return;
            }
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime date = DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        public void ChargesDateValidate(object source, ServerValidateEventArgs value) {
            if (!btnRegChargesClicked) {
                value.IsValid = true;
                return;
            }
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime date = DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        private void SetButtonClicked(string requestButtonName) {
            //"btnRegCharges","btnRegCapital","btnRegSecretary","btnRegBoardMember","btnRegShareOwner","btnBasicReg"};
            switch (requestButtonName) {
                case "btnRegCharges":
                    btnRegChargesClicked = true;
                    ChargesValidationsummary.Enabled = true;
                    // everthing else false
                    btnRegCapitalClicked = false;
                    btnRegSecretaryClicked = false;
                    btnRegBoardMemberClicked = false;
                    btnRegShareOwnerClicked = false;
                    btnBasicRegClicked = false;
                    // disable ValidationSummary
                    BoardMembersValidationsummary.Enabled = false;
                    BasicValidationSummary.Enabled = false;
                    ShareholdersValidationsummary.Enabled = false;
                    SecretaryValidationsummary.Enabled = false;
                    CapitalValidationSummary.Enabled = false;
                    break;
                case "btnRegCapital":
                    btnRegCapitalClicked = true;
                    CapitalValidationSummary.Enabled = true;
                    // everthing else false
                    btnRegChargesClicked = false;
                    btnRegSecretaryClicked = false;
                    btnRegBoardMemberClicked = false;
                    btnRegShareOwnerClicked = false;
                    btnBasicRegClicked = false;
                    // disable ValidationSummary
                    BoardMembersValidationsummary.Enabled = false;
                    BasicValidationSummary.Enabled = false;
                    ShareholdersValidationsummary.Enabled = false;
                    ChargesValidationsummary.Enabled = false;
                    SecretaryValidationsummary.Enabled = false;
                    break;
                case "btnRegSecretary":
                    btnRegSecretaryClicked = true;
                    SecretaryValidationsummary.Enabled = true;
                    // everthing else false
                    btnRegCapitalClicked = false;
                    btnRegChargesClicked = false;
                    btnRegBoardMemberClicked = false;
                    btnRegShareOwnerClicked = false;
                    btnBasicRegClicked = false;
                    // disable ValidationSummary
                    BoardMembersValidationsummary.Enabled = false;
                    BasicValidationSummary.Enabled = false;
                    ShareholdersValidationsummary.Enabled = false;
                    ChargesValidationsummary.Enabled = false;
                    CapitalValidationSummary.Enabled = false;
                    break;
                case "btnRegBoardMember":
                    btnRegBoardMemberClicked = true;
                    BoardMembersValidationsummary.Enabled = true;
                    // everthing else false 
                    btnRegSecretaryClicked = false;
                    btnRegCapitalClicked = false;
                    btnRegChargesClicked = false;
                    btnRegShareOwnerClicked = false;
                    btnBasicRegClicked = false;
                    // disable ValidationSummary
                    BasicValidationSummary.Enabled = false;
                    ShareholdersValidationsummary.Enabled = false;
                    ChargesValidationsummary.Enabled = false;
                    SecretaryValidationsummary.Enabled = false;
                    CapitalValidationSummary.Enabled = false;
                    break;
                case "btnRegShareOwner":
                    btnRegShareOwnerClicked = true;
                    ShareholdersValidationsummary.Enabled = true;
                    // everthing else false
                    btnRegBoardMemberClicked = false;
                    btnRegSecretaryClicked = false;
                    btnRegCapitalClicked = false;
                    btnRegChargesClicked = false;
                    btnBasicRegClicked = false;
                    // disable ValidationSummary
                    BoardMembersValidationsummary.Enabled = false;
                    BasicValidationSummary.Enabled = false;
                    ChargesValidationsummary.Enabled = false;
                    SecretaryValidationsummary.Enabled = false;
                    CapitalValidationSummary.Enabled = false;
                    break;
                case "btnBasicReg":
                    btnBasicRegClicked = true;
                    BasicValidationSummary.Enabled = true;
                    // everthing else false
                    btnRegShareOwnerClicked = false;
                    btnRegBoardMemberClicked = false;
                    btnRegSecretaryClicked = false;
                    btnRegCapitalClicked = false;
                    btnRegChargesClicked = false;
                    // disable ValidationSummary
                    BoardMembersValidationsummary.Enabled = false;
                    ShareholdersValidationsummary.Enabled = false;
                    ChargesValidationsummary.Enabled = false;
                    SecretaryValidationsummary.Enabled = false;
                    CapitalValidationSummary.Enabled = false;
                    break;
            }
        }

        public void FillBoardMembersDataSet() {
            if (Session["dsDetailedBoardMembers"] == null) {
                dsDetailedBoardMembers = myFact.GetDetailedBoardAsDataSet(CIID);
                dsDetailedBoardMembers = cleanDuplicatedBMembersCIIDS(dsDetailedBoardMembers);
                Session["dsDetailedBoardMembers"] = dsDetailedBoardMembers;
            } else {
                dsDetailedBoardMembers = (DataSet) Session["dsDetailedBoardMembers"];
            }
        }

        public void FillSecretaryDataSet() {
            DataSet dsBoardSecretary = myFact.GetBoardSecretaryAsDataSet(CIID);
            if (dsBoardSecretary.Tables[0].Rows.Count != 0) {
                dgRegisteredCompanySecretary.DataSource = dsBoardSecretary;
                dgRegisteredCompanySecretary.DataBind();
            }
        }

        public void FillChargesDataSet() {
            DataSet mySet = myFact.GetChargesAsDataSet(CIID);

            if (mySet != null) {
                if (mySet.Tables[0].Rows.Count > 0) {
                    dgCharges.DataSource = mySet;
                    dgCharges.DataBind();
                }
            } else {
                DisplayErrorMessage("Error getting data from database", lblChargesMessage);
            }
        }

        private void dgCharges_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);
            }

            WebDesign.CreateExplanationIcons(dgCharges.Columns, lblCharges, rm, ci);
        }

        private void dgCharges_ItemCommand(object source, DataGridCommandEventArgs e) {
            lblChargesMessage.Visible = false;
            if (e.CommandName.Equals("update")) {
                if (e.Item.Cells[2].Text.Trim() != "&nbsp;") {
                    txtID.Text = e.Item.Cells[2].Text.Trim();
                }

                if (e.Item.Cells[11].Text.Trim() != "&nbsp;") {
                    tbChargesAmount.Text = e.Item.Cells[11].Text.Trim();
                }
                if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                    tbChargesDatePrepared.Text = e.Item.Cells[4].Text.Trim();
                }
                if (e.Item.Cells[3].Text.Trim() != "&nbsp;") {
                    tbChargesDateRegistered.Text = e.Item.Cells[3].Text.Trim();
                }

                if (e.Item.Cells[13].Text.Trim() != "&nbsp;") {
                    tbChargesSequence.Text = e.Item.Cells[13].Text.Trim();
                }

                if (e.Item.Cells[12].Text.Trim() != "&nbsp;") {
                    ddlstCurrency.SelectedValue = e.Item.Cells[12].Text.Trim();
                }
                if (e.Item.Cells[8].Text.Trim() != "&nbsp;") {
                    ddlChargesBeneficiary.SelectedValue = e.Item.Cells[8].Text.Trim();
                }
                if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                    ddlChargesDescription.SelectedValue = e.Item.Cells[5].Text.Trim();
                }
                if (e.Item.Cells[14].Text.Trim() != "&nbsp;") {
                    tbChargesDescriptionNative.Text = e.Item.Cells[14].Text.Trim();
                }
                if (e.Item.Cells[15].Text.Trim() != "&nbsp;") {
                    tbChargesDescriptionEN.Text = e.Item.Cells[15].Text.Trim();
                }
            } else if (e.CommandName.Equals("delete")) {
                int ID1 = -1;
                try {
                    ID1 = int.Parse(e.Item.Cells[2].Text.Trim());
                } catch (Exception) {
                    ID1 = -1;
                }
                if (ID1 > -1) {
                    var myFactory = new CPIFactory();
                    myFactory.DeleteCharges(ID1);
                    ClearChargesBoxes();
                    LoadChargesSection();
                }
            }
        }

        private void LoadChargesSection() {
            DataSet mySet = myFact.GetChargesAsDataSet(CIID);

            if (mySet != null) {
                if (mySet.Tables[0].Rows.Count > 0) {
                    dgCharges.DataSource = mySet;
                    dgCharges.DataBind();
                }
            } else {
                DisplayErrorMessage("Error getting data from database", lblChargesMessage);
            }
        }

        private void SetGridChargesHeaders() {
            dgCharges.Columns[3].HeaderText = rm.GetString("txtDateRegistered", ci);
            dgCharges.Columns[4].HeaderText = rm.GetString("txtDatePrepared", ci);

            dgCharges.Columns[6].HeaderText = rm.GetString("txtType", ci);
            dgCharges.Columns[7].HeaderText = rm.GetString("txtType", ci);
            dgCharges.Columns[9].HeaderText = rm.GetString("txtBeneficiary", ci);
            dgCharges.Columns[10].HeaderText = rm.GetString("txtBeneficiary", ci);

            dgCharges.Columns[11].HeaderText = rm.GetString("txtAmount", ci);
            dgCharges.Columns[12].HeaderText = rm.GetString("txtCurrency", ci);
            dgCharges.Columns[13].HeaderText = rm.GetString("txtSequence", ci);

            if (!nativeCult) {
                dgCharges.Columns[6].Visible = false;
                dgCharges.Columns[7].Visible = true;
                dgCharges.Columns[9].Visible = false;
                dgCharges.Columns[10].Visible = true;
            } else {
                dgCharges.Columns[6].Visible = true;
                dgCharges.Columns[7].Visible = false;
                dgCharges.Columns[9].Visible = true;
                dgCharges.Columns[10].Visible = false;
            }
        }

        private void dtgrSecretaryNameSearch_ItemCommand(object source, DataGridCommandEventArgs e) {
            // 1 == CreditInfoID
            // 2 == National ID
            // 3 == Name
            // 4 == NameNative
            // 5 == NameEN
            // 6 == Address
            // 7 == AddressNative
            // 8 == AddressEN
            // 9 == Post code
            // 10 == IsCompany
            string tmp10 = e.Item.Cells[10].Text.Trim();

            if (e.CommandName.Equals("Select")) {
                tbSecretaryCIID.Text = e.Item.Cells[1].Text;
                tbSecretaryNationalID.Text = e.Item.Cells[2].Text;
                if (tbSecretaryNationalID.Text == "&nbsp;") {
                    tbSecretaryNationalID.Text = "";
                }
                if (tbSecretaryCIID.Text == "&nbsp;") {
                    tbSecretaryCIID.Text = "";
                    custValidNationalIDSecretary.Enabled = true;
                } else {
                    custValidNationalIDSecretary.Enabled = false;
                }

                if (e.Item.Cells[10].Text.Trim().Equals("1")) // that isCompany
                {
                    tbSecretaryFirstName.Text = e.Item.Cells[3].Text.Trim();
                    lblSecretaryFirstName.Text = rm.GetString("txtName", ci);
                    lblSecretarySurName.Visible = false;
                    tbSecretarySurName.Visible = false;
                } else // person ...
                {
                    lblSecretarySurName.Visible = true;
                    tbSecretarySurName.Visible = true;
                    lblSecretaryFirstName.Text = rm.GetString("txtFirstName", ci);

                    string name = e.Item.Cells[3].Text.Trim();
                    if (name == "&nbsp;") {
                        name = "";
                    }
                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            tbSecretaryFirstName.Text = arrName[0];
                        } else {
                            tbSecretarySurName.Text = arrName[arrName.Length - 1];
                            tbSecretaryFirstName.Text = name.Substring(
                                0, name.Length - (tbSecretarySurName.Text.Length + 1));
                        }
                    }
                }
            }
        }

        private void dtgrSecretaryNameSearch_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }

                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }

                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }
                }
            }

            WebDesign.CreateExplanationIcons(
                dtgrSecretaryNameSearch.Columns, lblSecretaryNameSearchDatagridIcons, rm, ci);
        }

        public void ClearSecretaryBoxes() {
            tbSecretaryCIID.Text = "";
            tbSecretaryFirstName.Text = "";
            tbSecretaryNationalID.Text = "";
            tbSecretarySurName.Text = "";
        }

        public void LoadSecretaryData(int CIID) {
            if (CIID > 0) {
                DataSet dsBoardSecretary = myFact.GetBoardSecretaryAsDataSet(CIID);
                if (dsBoardSecretary.Tables[0].Rows.Count != 0) {
                    dgRegisteredCompanySecretary.DataSource = dsBoardSecretary;
                    dgRegisteredCompanySecretary.DataBind();
                }
            }
        }

        private void dtgrShareholders_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    if (e.Item.Cells[10].Text.Trim() == CigConfig.Configure("lookupsettings.individualID")) // indivitual
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[5].Text + " " + e.Item.Cells[6].Text;
                    } else // is company
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[11].Text;
                    }
                    // Ownership type
                    e.Item.Cells[14].Text = e.Item.Cells[15].Text;
                } else {
                    if (e.Item.Cells[10].Text.Trim() == CigConfig.Configure("lookupsettings.individualID")) // indivitual
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[7].Text + " " + e.Item.Cells[8].Text;
                    } else // is company
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[12].Text;
                    }
                    // Ownership type
                    e.Item.Cells[14].Text = e.Item.Cells[16].Text;
                }
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
            }

            WebDesign.CreateExplanationIcons(dtgrShareholders.Columns, lblShareholdersDatagridIcons, rm, ci);
        }

        private void dtgrShareholders_ItemCommand(object source, DataGridCommandEventArgs e) {
            int rowToDelete = -1;
            try {
                int shareholdersCIID = Convert.ToInt32(dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString());

                if (e.CommandName == "Delete") {
                    rowToDelete = e.Item.ItemIndex;
                }
                if (rowToDelete >= 0) {
                    if (shareholdersCIID > 0) {
                        int ownershipID = int.Parse(dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["OwnershipID"].ToString().Trim());
                        myFact.DeleteShareholdersOwner(shareholdersCIID, CIID, ownershipID);
                    }
                    dsShareHolders.Tables[0].Rows[rowToDelete].Delete();
                    Session["dsShareHolders"] = null;
                    FillDataSets();
                    BindShareHoldersGrid();
                    ClearShareOwnerBoxes();
                    return;
                }

                if (e.CommandName == "Update") {
                    // do some fancy stuff
                    if (dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["Type"].ToString().Trim() ==
                        CigConfig.Configure("lookupsettings.individualID")) {
                        if (nativeCult) {
                            tbShareOwnerFirstname.Text =
                                dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["FirstNameNative"].ToString();
                            tbShareOwnerSurname.Text =
                                dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["SurNameNative"].ToString();
                        } else {
                            tbShareOwnerFirstname.Text =
                                dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["FirstNameEN"].ToString();
                            tbShareOwnerSurname.Text =
                                dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["SurNameEN"].ToString();
                        }
                        rblShareOwnerPersComp.Items[0].Selected = true;
                        rblShareOwnerPersComp.Items[1].Selected = false;
                    } else {
                        tbShareOwnerFirstname.Text = nativeCult ? dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["NameNative"].ToString() : dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["NameEN"].ToString();
                        tbShareOwnerSurname.Text = "";
                        rblShareOwnerPersComp.Items[1].Selected = true;
                    }
                    tbShareOwnerNationalID.Text = dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["Number"].ToString();
                    txtShareOwnerCIID.Text = dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString();
                    tbShareOwnerOwnership.Text = dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["Ownership"].ToString();
                    ddlShareOwnerOwnershipType.SelectedValue =
                        dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["OwnershipID"].ToString();
                    btnRegShareOwner.Text = rm.GetString("txtUpdate", ci);
                    btnRegShareOwner.CommandName = "Update";
                    tbShareOwnerNationalID.ReadOnly = true;
                    txtShareOwnerCIID.ReadOnly = true;
                }
            } catch (Exception err) {
                Logger.WriteToLog("RegBoardMember.aspx : dtgrBoardMOverview_ItemCommand " + err.Message, true);
            }
        }

        public void ClearShareOwnerBoxes() {
            txtShareOwnerCIID.Text = "";
            tbShareOwnerFirstname.Text = "";
            tbShareOwnerSurname.Text = "";
            tbShareOwnerNationalID.Text = "";
            tbShareOwnerOwnership.Text = "";
            // init button and NationalID field
            btnRegShareOwner.Text = rm.GetString("txtSave", ci);
            btnRegShareOwner.CommandName = "Update";
            tbShareOwnerNationalID.ReadOnly = false;
            ddlShareOwnerOwnershipType.SelectedIndex = -1;
            txtShareOwnerCIID.ReadOnly = false;
        }

        private void btnBasicReg_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {} else {
                try {
                    theRCompany = new ReportCompanyBLLC();
                    var theCoHistory = new HistoryOperationReviewBLLC();
                    theRCompany.RegisterCIID = (int) Session["UserCreditInfoID"];
                    theRCompany.Address = GetBasicAddress();
                    theRCompany.Email = tbEmail.Text;
                    if (tbEstablished.Text != "") {
                        theRCompany.Established = Convert.ToDateTime(tbEstablished.Text);
                    }
                    if (tbRegisted.Text != "") {
                        theRCompany.Registered = Convert.ToDateTime(tbRegisted.Text);
                    }

                    theRCompany.FormerNameNative = tbFormerName.Text;
                    theRCompany.TradeNameNative = tbTradeName.Text;
                    theRCompany.NameNative = tbName.Text;
                    theRCompany.FormerNameEN = txtFormerNameEN.Text;
                    theRCompany.NameEN = txtNameEN.Text;
                    theRCompany.TradeNameEN = txtTradeNameEN.Text;
                    theRCompany.Symbol = txtSymbol.Text;

                    // Cyprus specific
                    IFormatProvider format = CultureInfo.CurrentCulture;
                    if (tbLAR.Text != "") {
                        //	if (this.tbRegDate.Text != "")
                        //		myClaim.RegDate = DateTime.Parse(this.tbRegDate.Text,format, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                        theRCompany.LAR = DateTime.Parse(tbLAR.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbLR.Text != "") {
                        theRCompany.LR = DateTime.Parse(tbLR.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbStructureReportResearchDate.Text != "") {
                        theRCompany.StructureReportResearchDate = DateTime.Parse(
                            tbStructureReportResearchDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }

                    theRCompany.HomePage = tbHomePage.Text;
                    if (tbLastContacted.Text != "") {
                        theRCompany.LastContacted = Convert.ToDateTime(tbLastContacted.Text);
                    }
                    if (tbLastUpdated.Text != "") {
                        theRCompany.LastUpdated = Convert.ToDateTime(tbLastUpdated.Text);
                    }
                    theRCompany.LegalForm = Convert.ToInt32(ddLegalForm.SelectedValue);

                    theRCompany.UniqueID = tbUniqueID.Text;
                    theRCompany.RegistrationFormID = Convert.ToInt32(ddLegalForm.SelectedValue);
                    theRCompany.CompanyCIID = CIID;

                    theRCompany.PNumbers = new ArrayList();

                    PhoneNumber thePhoneNum;

                    if (tbTel.Text.Length > 0) {
                        thePhoneNum = new PhoneNumber
                                      {
                                          Number = tbTel.Text,
                                          NumberTypeID = Convert.ToInt32(CigConfig.Configure("lookupsettings.workCode"))
                                      };
                        theRCompany.PNumbers.Add(thePhoneNum);
                    }
                    if (tbMobile.Text.Length > 0) {
                        thePhoneNum = new PhoneNumber
                                      {
                                          Number = tbMobile.Text,
                                          NumberTypeID =
                                              Convert.ToInt32(CigConfig.Configure("lookupsettings.mobileCode"))
                                      };
                        theRCompany.PNumbers.Add(thePhoneNum);
                    }
                    if (tbFax.Text.Length > 0) {
                        thePhoneNum = new PhoneNumber
                                      {
                                          Number = tbFax.Text,
                                          NumberTypeID = Convert.ToInt32(CigConfig.Configure("lookupsettings.faxCode"))
                                      };
                        theRCompany.PNumbers.Add(thePhoneNum);
                    }

                    theRCompany.VAT = tbVAT.Text;
                    // The history part
                    if (tbHistoryN.Text != "") {
                        theCoHistory.HistoryNative = tbHistoryN.Text;
                    }
                    if (tbHistoryE.Text != "") {
                        theCoHistory.HistoryEN = tbHistoryE.Text;
                    }

                    if (myFact.AddReportCompany(theRCompany) &&
                        myFact.AddCompanyHistory_Structure(theCoHistory, theRCompany.CompanyCIID)) {
                        if (Session["CIID"] == null) // if new registration and CIID hasn't been stored in session yet
                        {
                            Session["CIID"] = theRCompany.CompanyCIID;
                        }

                        lblRegMsg.Text = btnBasicReg.CommandName == "Update" ? "The information has been updated" : "The information has been registed";

                        lblRegMsg.Visible = true;

                        Session["ReportCompany"] = theRCompany;
                        Session["ReportExists"] = true;
                    } else {
                        lblRegMsg.Text = btnBasicReg.CommandName == "Update" ? "Update failed" : "Insert failed";

                        lblRegMsg.Visible = true;
                    }
                } catch (Exception err) {
                    Logger.WriteToLog("BasicInformation.aspx" + " btnContinue_Click, MSG: " + err.Message, true);
                    //					isOK = false;
                }
            }
        }

        private ArrayList GetBasicAddress() {
            var myList = new ArrayList();

            //Address 1 -------------------------------
            var myAddress = new Address
                            {
                                PostBox = tbPostbox.Text,
                                PostalCode = tbAreaCode.Text,
                                CityID = Convert.ToInt32(ddCity.SelectedValue),
                                StreetNative = tbAddress.Text,
                                StreetEN = txtAddressEN.Text,
                                IsTradingAddress = false
                            };

            myList.Add(myAddress);

            return myList;
        }

        private void btnRegBoardMember_Click(object sender, EventArgs e) {
            tableBoardMemberNameSearch.Visible = false;
            if (Page.IsValid) {
                RegisterBoardMember(
                    tbBoardMemberPersonalID.Text,
                    tbBoardMemberFirstName.Text,
                    tbBoardMemberSurName.Text,
                    tbBoardMemberAddress.Text,
                    Convert.ToInt32(ddBoardMemberManagementPosition.SelectedValue),
                    tbBoardMemberCIID.Text);
            }
        }

        private void RegisterBoardMember(string nationalID, string firstName, string surName, string address, int positionID, string creditInfoID) {
            var theBoardMember = new BoardMemberBLLC();
            if (nativeCult) {
                theBoardMember.FirstNameNative = firstName;
                theBoardMember.SurNameNative = surName;
            } else {
                theBoardMember.FirstNameEN = firstName;
                theBoardMember.SurNameEN = surName;
            }

            if (tbBoardMemberCIID.Text != "") {
                theBoardMember.CreditInfoID = Convert.ToInt32(creditInfoID);
            }
            theBoardMember.NationalID = nationalID;
            theBoardMember.SingleAddressNative = address;
            theBoardMember.ManagementPostionID = positionID;

            // take the history
            bool historyOK = RegHistory(Convert.ToInt32(CigConfig.Configure("lookupsettings.BoardMembersHistoryType")));

            if (myFact.AddBordMember(theBoardMember, CIID)) {
                Session["dsDetailedBoardMembers"] = null;
                FillBoardMembersDataSet();
                BindBoardMembersGrid();
                ClearBoardMemberBoxes();
                if (historyOK) {
                    DisplayMessage(rm.GetString("txtInfoSaved", ci), lblBoardMemberMessage);
                }
            } else {
                DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci), lblBoardMemberMessage);
            }
            if (!historyOK) {
                DisplayErrorMessage(rm.GetString("txtSavingHistoryFailed", ci), lblBoardMemberMessage);
            }
        }

        private void dtgrBoardMemberNameSearch_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }

                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }

                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);	
            }

            WebDesign.CreateExplanationIcons(
                dtgrBoardMemberNameSearch.Columns, lblBoardMemberNameSearchDatagridIcons, rm, ci);
        }

        private void dtgrBoardMemberNameSearch_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                if (e.Item.Cells[1].Text == "&nbsp;") {
                    tbBoardMemberCIID.Text = "";
                    custValidPersIDBoardMembers.Enabled = true;
                } else {
                    custValidPersIDBoardMembers.Enabled = false;
                    tbBoardMemberCIID.Text = e.Item.Cells[1].Text;
                }

                tbBoardMemberPersonalID.Text = e.Item.Cells[2].Text;
                if (tbBoardMemberPersonalID.Text == "&nbsp;") {
                    tbBoardMemberPersonalID.Text = "";
                }

                tbBoardMemberAddress.Text = e.Item.Cells[6].Text;
                if (tbBoardMemberAddress.Text == "&nbsp;") {
                    tbBoardMemberAddress.Text = "";
                }
                /*if(txtCIID.Text=="&nbsp;")
					txtCIID.Text="";*/

                string name = e.Item.Cells[3].Text.Trim();
                if (name == "&nbsp;") {
                    name = "";
                }
                string[] arrName = name.Split(' ');
                if (arrName.Length > 0) {
                    if (arrName.Length == 1) {
                        tbBoardMemberFirstName.Text = arrName[0];
                    } else {
                        tbBoardMemberSurName.Text = arrName[arrName.Length - 1];
                        tbBoardMemberFirstName.Text = name.Substring(
                            0, name.Length - (tbBoardMemberSurName.Text.Length + 1));
                    }
                }
            }
        }

        public void InitBoardMembersGrid() {
            dtgrBoardMOverview.Columns[6].Visible = false;
            dtgrBoardMOverview.Columns[7].Visible = false;
            dtgrBoardMOverview.Columns[8].Visible = false;
            dtgrBoardMOverview.Columns[9].Visible = false;
            dtgrBoardMOverview.Columns[10].Visible = false;
            dtgrBoardMOverview.Columns[11].Visible = false;
        }

        private void dtgrBoardMOverview_ItemDataBound(object sender, DataGridItemEventArgs e) {
            string tmpString = "";
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    if (!(e.Item.Cells[6].Text == "") || (e.Item.Cells[6].Text == "&nbsp;")) // non native info
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[6].Text + " " + e.Item.Cells[7].Text;
                    } else // then display english rather then showing empty fields
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[8].Text + " " + e.Item.Cells[9].Text;
                    }

                    if (!(e.Item.Cells[10].Text == "") || (e.Item.Cells[10].Text == "&nbsp;")) // non native info
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[10].Text;
                    } else // then display english rather then showing empty fields
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[11].Text;
                    }
                } else {
                    tmpString = e.Item.Cells[8].Text.Trim();
                    if (!(e.Item.Cells[8].Text == "&nbsp;") || (e.Item.Cells[8].Text == "")) // non english info
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[8].Text + " " + e.Item.Cells[9].Text;
                    } else // then display native rather then showing empty fields
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[6].Text + " " + e.Item.Cells[7].Text;
                    }
                    if (!(e.Item.Cells[11].Text == "") || (e.Item.Cells[11].Text == "&nbsp;")) // non english info
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[11].Text;
                    } else // then display native rather then showing empty fields
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[10].Text;
                    }
                }
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
            }

            WebDesign.CreateExplanationIcons(dtgrBoardMOverview.Columns, lblBoardMOverviewDatagridIcons, rm, ci);
        }

        private void dtgrBoardMOverview_ItemCommand(object source, DataGridCommandEventArgs e) {
            int rowToDelete = -1;
            try {
                int boardMemberCIID = Convert.ToInt32(dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString());

                if (e.CommandName == "Delete") {
                    rowToDelete = e.Item.ItemIndex;
                }
                if (rowToDelete >= 0) {
                    dsDetailedBoardMembers.Tables[0].Rows[rowToDelete].Delete();
                    if (boardMemberCIID > 0) {
                        myFact.DeleteBoardMember(boardMemberCIID, CIID);
                    }
                    BindBoardMembersGrid();
                }
                if (e.CommandName == "Update") {
                    // do some fancy stuff
                    tbBoardMemberCIID.Text =
                        dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString();
                    tbBoardMemberFirstName.Text =
                        dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["FirstNameNative"].ToString();
                    tbBoardMemberSurName.Text =
                        dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["SurNameNative"].ToString();
                    tbBoardMemberPersonalID.Text =
                        dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["Number"].ToString();
                    try {
                        ddBoardMemberManagementPosition.SelectedValue =
                            dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["ManagementPositionID"].ToString();
                    } catch (Exception ex) {
                        Logger.WriteToLog("RegBoarMember.aspx : dtgrBoardMOverview_ItemCommand " + ex.Message, true);
                    }
                    tbBoardMemberAddress.Text =
                        dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["StreetNative"].ToString();
                    btnRegBoardMember.Text = rm.GetString("txtUpdate", ci);
                    btnRegBoardMember.CommandName = "Update";
                    tbBoardMemberPersonalID.ReadOnly = true;
                    tbBoardMemberCIID.ReadOnly = true;
                }
            } catch (Exception err) {
                Logger.WriteToLog("RegBoarMember.aspx : dtgrBoardMOverview_ItemCommand " + err.Message, true);
            }
        }

        public void BindBoardMembersBoxes() {
            if (Session["dsManagementPositions"] == null) {
                dsManagementPositions = myFact.GetManagementPositionAsDataSet();
                Session["dsManagementPositions"] = dsManagementPositions;
            } else {
                dsManagementPositions = (DataSet) Session["dsManagementPositions"];
            }

            //	if(dsManagementPositions.Tables[0] != null) 
            //	{
            ddBoardMemberManagementPosition.DataSource = dsManagementPositions;
            ddBoardMemberManagementPosition.DataTextField = nativeCult ? "TitleNative" : "TitleEN";
            ddBoardMemberManagementPosition.DataValueField = "ManagementPositionID";
            ddBoardMemberManagementPosition.DataBind();
            //	}
        }

        public void SetBoardMembersHistoryBoxes() {
            HistoryBLLC theHistory = myFact.GetHistory(
                CIID, Convert.ToInt32(CigConfig.Configure("lookupsettings.BoardMembersHistoryType")));
            tbBoardMemberHistoryNative.Text = theHistory.HistoryNative;
            tbBoardMemberHistoryEN.Text = theHistory.HistoryEN;
        }

        public void ClearBoardMemberBoxes() {
            tbBoardMemberCIID.Text = "";
            tbBoardMemberPersonalID.Text = "";
            tbBoardMemberAddress.Text = "";
            tbBoardMemberFirstName.Text = "";
            tbBoardMemberSurName.Text = "";
            ddBoardMemberManagementPosition.SelectedIndex = 0;
            ddBoardMemberManagementPosition.SelectedIndex = 0;
            // init Reg. button etc.
            btnRegBoardMember.Text = rm.GetString("txtSave", ci);
            btnRegBoardMember.CommandName = "Register";
            tbBoardMemberPersonalID.ReadOnly = false;
            tbBoardMemberCIID.ReadOnly = false;
            custValidPersIDBoardMembers.Enabled = true;
        }

        private void dgRegisteredCompanySecretary_ItemDataBound(object sender, DataGridItemEventArgs e) {
            // 2 == CreditInfoID
            // 3 == NationalID
            // 4 == Name
            // 5 == FirstNameNative
            // 6 == SurNameNative
            // 7 == FirstNameEN
            // 8 == SurNameEN
            // 9 == HistoryNative
            // 10 == HistoryEN
            // 11 == NameEN
            // 12 == NameNative
            // 13 == Type
//			string tmp1,tmp2,tmp3,tmp4,tmp5,tmp6,tmp7,tmp8,tmp9,tmp10,tmp11,tmp12,tmp13;
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
/*				tmp1 = e.Item.Cells[1].Text.Trim();
				tmp2 = e.Item.Cells[2].Text.Trim();
				tmp3 = e.Item.Cells[3].Text.Trim();
				tmp4 = e.Item.Cells[4].Text.Trim();
				tmp5 = e.Item.Cells[5].Text.Trim();
				tmp6 = e.Item.Cells[6].Text.Trim();
				tmp7 = e.Item.Cells[7].Text.Trim();
				tmp8 = e.Item.Cells[8].Text.Trim();
				tmp9 = e.Item.Cells[9].Text.Trim();
				tmp10 = e.Item.Cells[10].Text.Trim();
				tmp11 = e.Item.Cells[11].Text.Trim();
				tmp12 = e.Item.Cells[12].Text.Trim();
				tmp13 = e.Item.Cells[13].Text.Trim();
*/
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('Are you sure you want to delete?')";
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);

                if (nativeCult) {
                    //Set Name - Native if available, else 
                    if (e.Item.Cells[13].Text.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                        if (e.Item.Cells[12].Text.Trim() != "" && e.Item.Cells[12].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[12].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[11].Text;
                        }
                    } else {
                        if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[5].Text + e.Item.Cells[6].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[7].Text + e.Item.Cells[8].Text;
                        }
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[13].Text.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                        if (e.Item.Cells[11].Text.Trim() != "" && e.Item.Cells[11].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[11].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[12].Text;
                        }
                    } else {
                        if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[7].Text + e.Item.Cells[8].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[5].Text + e.Item.Cells[6].Text;
                        }
                    }
                }
            }

            WebDesign.CreateExplanationIcons(
                dgRegisteredCompanySecretary.Columns, lblRegisteredCompanySecretaryDatagridIcons, rm, ci);
        }

        private void dgRegisteredCompanySecretary_ItemCommand(object source, DataGridCommandEventArgs e) {
            // 2 == CreditInfoID
            // 3 == NationalID
            // 4 == Name
            // 5 == FirstNameNative
            // 6 == SurNameNative
            // 7 == FirstNameEN
            // 8 == SurNameEN
            // 9 == HistoryNative
            // 10 == HistoryEN
            // 11 == NameEN
            // 12 == NameNative
            // 13 == Type

            if (e.CommandName.Equals("update")) {
                if (e.Item.Cells[13].Text.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                    rblSecretaryPersonCompany.Items[0].Selected = false;
                    rblSecretaryPersonCompany.Items[1].Selected = true;
                    lblSecretaryFirstName.Text = rm.GetString("txtName", ci);
                    lblSecretarySurName.Visible = false;
                    tbSecretarySurName.Visible = false;

                    if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                        tbSecretaryFirstName.Text = e.Item.Cells[4].Text.Trim();
                    }
                } else {
                    rblSecretaryPersonCompany.Items[1].Selected = false;
                    rblSecretaryPersonCompany.Items[0].Selected = true;
                    lblSecretaryFirstName.Text = rm.GetString("txtFirstName", ci);
                    lblSecretarySurName.Visible = true;
                    tbSecretarySurName.Visible = true;

                    if (nativeCult) {
                        if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                            tbSecretaryFirstName.Text = e.Item.Cells[5].Text.Trim();
                        }
                        if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                            tbSecretarySurName.Text = e.Item.Cells[6].Text.Trim();
                        }
                    } else {
                        if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                            tbSecretaryFirstName.Text = e.Item.Cells[7].Text.Trim();
                        }
                        if (e.Item.Cells[8].Text.Trim() != "&nbsp;") {
                            tbSecretarySurName.Text = e.Item.Cells[8].Text.Trim();
                        }
                    }
                }

                if (e.Item.Cells[2].Text.Trim() != "&nbsp;") {
                    tbSecretaryCIID.Text = e.Item.Cells[2].Text.Trim();
                }
                if (e.Item.Cells[3].Text.Trim() != "&nbsp;") {
                    tbSecretaryNationalID.Text = e.Item.Cells[3].Text.Trim();
                }
                /*
								if(e.Item.Cells[6].Text.Trim() != "&nbsp;")
									this.txtFirstname.Text = e.Item.Cells[6].Text.Trim();
								if(e.Item.Cells[7].Text.Trim() != "&nbsp;")
									this.txtSurname.Text = e.Item.Cells[7].Text.Trim();
				*/
                if (e.Item.Cells[9].Text.Trim() != "&nbsp;") {
                    tbSecretaryHistoryNative.Text = e.Item.Cells[9].Text.Trim();
                }
                if (e.Item.Cells[10].Text.Trim() != "&nbsp;") {
                    tbSecretaryHistoryEnglish.Text = e.Item.Cells[10].Text.Trim();
                }
            } else if (e.CommandName.Equals("delete")) {
                int laywerCIID;
                try {
                    laywerCIID = int.Parse(e.Item.Cells[2].Text.Trim());
                } catch (Exception) {
                    laywerCIID = -1;
                }
                if (laywerCIID > -1) {
                    if (myFact.DeleteBoardSecretary(CIID)) {
                        DisplayMessage("Secretary deleted", lblSecretaryMsg);
                        ClearSecretaryBoxes();
                        LoadSecretaryData(CIID);
                    } else {
                        DisplayErrorMessage("Error deleting secretary", lblSecretaryMsg);
                    }
                }
            }
        }

        private void BindCapitalBoxes() {
            var myFactory = new CPIFactory();
            var currencySet = myFactory.GetCurrencyAsDataSet();
            ddlstCurrency.DataSource = currencySet;
            ddlstCurrency.DataTextField = !nativeCult ? "CurrencyCode" : "CurrencyCode";
            ddlstCurrency.DataValueField = "CurrencyCode";
            ddlstCurrency.DataBind();
            // if currency order key in config then order the currency list ...
            if (CigConfig.Configure("lookupsettings.orderCurrencyList") != null) {
                OrderCurrencyList(CigConfig.Configure("lookupsettings.orderCurrencyList"), "ddlstCurrency");
            }
        }

        private void OrderCurrencyList(string orderList, string ddListName) {
            string[] strArray = orderList.Split(new[] {','});
            int index = 0;
            foreach (string id in strArray) {
                var li = ddlstCurrency.Items.FindByValue(id);
                switch (ddListName) {
                    case "ddlstCurrency":
                        ddlstCurrency.Items.Remove(li);
                        ddlstCurrency.Items.Insert(index, li);
                        break;
                    case "ddlChargesCurrency":
                        ddlChargesCurrency.Items.Remove(li);
                        ddlChargesCurrency.Items.Insert(index, li);
                        break;
                }
                index++;
            }
        }

        private void LoadCapitalInfo() {
            var myFactory = new CPIFactory();
            var myCapital = myFactory.GetCapital(CIID);

            if (myCapital != null) {
                if (myCapital.CompanyCIID > -1) {
                    if (myCapital.Asked > -1) {
                        txtAsked.Text = formatNumber(myCapital.Asked);
                    }
                    if (myCapital.AuthorizedCapital > -1) {
                        txtAuthorizedCapital.Text = formatNumber(myCapital.AuthorizedCapital);
                    }
                    if (myCapital.IssuedNumberOfShares > -1) {
                        txtIssuedNoOfShares.Text = formatNumber(myCapital.IssuedNumberOfShares);
                    }
                    if (myCapital.NominalNumberOfShares > -1) {
                        txtNominalNumberOfShares.Text = formatNumber(myCapital.NominalNumberOfShares);
                    }
                    if (myCapital.PaidUp > -1) {
                        txtPaidUp.Text = formatNumber(myCapital.PaidUp);
                    }

                    ddlstCurrency.SelectedValue = myCapital.CurrencyCode;

                    txtSharesDescription.Text = myCapital.SharesDescription;

                    btnRegCapital.Text = rm.GetString("txtUpdate", ci);
                } else {
                    btnRegCapital.Text = rm.GetString("txtSave", ci);
                    //H�r v�ri h�gt a� setja in N/A � �ll field
                }
            } else {
                DisplayErrorMessage("Error getting data from database", lblErrorMessage);
            }
        }

        public string formatNumber(decimal number) { return number.ToString("N", fInfo); }

        private void btnRegCapital_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {
                return;
            }
            var myCapital = GetCapitalInfoFromPage();

            if (myCapital != null) {
                var myFactory = new CPIFactory();

                if (myFactory.AddCapital(myCapital, CIID)) {
                    DisplayMessage(rm.GetString("txtInfoSaved", ci), lblErrorMessage);
                } else {
                    DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci), lblErrorMessage);
                }
            }
        }

        private CapitalBLLC GetCapitalInfoFromPage() {
            var myCapital = new CapitalBLLC();

            try {
                if (txtAuthorizedCapital.Text.Length > 0) {
                    myCapital.AuthorizedCapital = decimal.Parse(txtAuthorizedCapital.Text);
                }
                if (txtAsked.Text.Length > 0) {
                    myCapital.Asked = decimal.Parse(txtAsked.Text);
                }
                if (txtIssuedNoOfShares.Text.Length > 0) {
                    myCapital.IssuedNumberOfShares = decimal.Parse(txtIssuedNoOfShares.Text);
                }
                if (txtNominalNumberOfShares.Text.Length > 0) {
                    myCapital.NominalNumberOfShares = decimal.Parse(txtNominalNumberOfShares.Text);
                }
                if (txtPaidUp.Text.Length > 0) {
                    myCapital.PaidUp = decimal.Parse(txtPaidUp.Text);
                }

                myCapital.CurrencyCode = ddlstCurrency.SelectedValue;

                myCapital.SharesDescription = txtSharesDescription.Text;
            } catch (Exception) {
                DisplayErrorMessage("Error getting info from page", lblErrorMessage);
                return null;
            }

            return myCapital;
        }

        private void BindChargesBoxes() {
            var myFactory = new CPIFactory();

            BindBeneficiary(myFactory);
            BindChargesCurrency(myFactory);
            BindChargesDescription(myFactory);
        }

        private void BindBeneficiary(CPIFactory myFactory) {
            /*
			 DataSet dsBanks = new DataSet();
			if(Cache["dsBanks"] == null) 
			{
				dsBanks = myFact.GetBanksAsDataSet();
				Cache["dsBanks"] = dsBanks;
			} else
				dsBanks = (DataSet) Cache["dsBanks"];
			 */

            DataSet bankSet;
            if (Cache["dsChargesBanks"] == null) {
                bankSet = myFactory.GetBanksAsDataSet(nativeCult);
                Cache["dsChargesBanks"] = bankSet;
            } else {
                bankSet = (DataSet) Cache["dsChargesBanks"];
            }

            ddlChargesBeneficiary.DataSource = bankSet;
            ddlChargesBeneficiary.DataTextField = !nativeCult ? "NameEN" : "NameNative";
            ddlChargesBeneficiary.DataValueField = "BankID";
            ddlChargesBeneficiary.DataBind();
            if (CigConfig.Configure("lookupsettings.orderedBankList") != null) {
                OrderBenificiaryList(CigConfig.Configure("lookupsettings.orderedBankList"));
            }
        }

        private void BindChargesCurrency(CPIFactory myFactory) {
            var currencySet = myFact.GetCurrencyAsDataSet();
            ddlChargesCurrency.DataSource = currencySet;
            ddlChargesCurrency.DataTextField = !nativeCult ? "CurrencyCode" : "CurrencyCode";
            ddlChargesCurrency.DataValueField = "CurrencyCode";
            ddlChargesCurrency.DataBind();

            // if currency order key in config then order the currency list ...
            if (CigConfig.Configure("lookupsettings.orderCurrencyList") != null) {
                OrderCurrencyList(CigConfig.Configure("lookupsettings.orderCurrencyList"), "ddlChargesCurrency");
            }
        }

        private void BindChargesDescription(CPIFactory myFactory) {
            var descriptionSet = myFact.GetChargesDescriptionAsDataSet();
            ddlChargesDescription.DataSource = descriptionSet;
            ddlChargesDescription.DataTextField = !nativeCult ? "DescriptionEN" : "DescriptionNative";
            ddlChargesDescription.DataValueField = "descriptionID";
            ddlChargesDescription.DataBind();
        }

        private void OrderBenificiaryList(string orderList) {
            string[] strArray = orderList.Split(new[] {','});
            //	this.ddlstBeneficiary.FindControl()
            int index = 0;
            foreach (string id in strArray) {
                var li = ddlChargesBeneficiary.Items.FindByValue(id);
                ddlChargesBeneficiary.Items.Remove(li);
                ddlChargesBeneficiary.Items.Insert(index, li);
                index++;
            }
        }

        private void btnRegCharges_Click(object sender, EventArgs e) {
            if (!Page.IsValid) {
                return;
            }
            var myCharges = GetChargesInfoFromPage();

            if (myCharges != null) {
                var myFactory = new CPIFactory();

                if (myFactory.AddCharges(myCharges, CIID)) {
                    DisplayMessage(rm.GetString("txtInfoSaved", ci), lblChargesMessage);
                    ClearChargesBoxes();
                    LoadChargesPage();
                } else {
                    DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci), lblChargesMessage);
                }
            } else {
                DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci), lblChargesMessage);
            }
        }

        private void ClearChargesBoxes() {
            txtID.Text = "";
            tbChargesAmount.Text = "";
            tbChargesDatePrepared.Text = "";
            tbChargesDateRegistered.Text = "";
            tbChargesSequence.Text = "";

            ddlChargesBeneficiary.SelectedIndex = 0;
            ddlChargesDescription.SelectedIndex = 0;
            ddlChargesCurrency.SelectedIndex = 0;

            tbChargesDescriptionNative.Text = "";
            tbChargesDescriptionEN.Text = "";

            InitChargesDates();
        }

        private void LoadChargesPage() {
            var myFactory = new CPIFactory();
            DataSet mySet = myFactory.GetChargesAsDataSet(CIID);

            if (mySet != null) {
                if (mySet.Tables[0].Rows.Count > 0) {
                    dgCharges.DataSource = mySet;
                    dgCharges.DataBind();
                }
            } else {
                DisplayErrorMessage("Error getting data from database", lblChargesMessage);
            }
        }

        private void InitChargesDates() {
            tbChargesDatePrepared.Text = DateTime.Today.ToShortDateString();
            tbChargesDateRegistered.Text = DateTime.Today.ToShortDateString();
        }

        private ChargesBLLC GetChargesInfoFromPage() {
            var myCharges = new ChargesBLLC();

            if (txtID.Text.Trim().Length > 0) {
                try
                {
                    myCharges.ID = int.Parse(txtID.Text.Trim());
                }
                catch (Exception ex)
                {

                    return null;
                }
            }

            if (tbChargesAmount.Text != "") {
                myCharges.Amount = decimal.Parse(tbChargesAmount.Text);
            }
            myCharges.BankID = ddlChargesBeneficiary.SelectedValue;
            if (tbChargesDatePrepared.Text != "") {
                myCharges.DatePrepared = DateTime.Parse(tbChargesDatePrepared.Text);
            }
            if (tbChargesDateRegistered.Text != "") {
                myCharges.DateRegistered = DateTime.Parse(tbChargesDateRegistered.Text);
            }
            myCharges.DescriptionID = int.Parse(ddlChargesDescription.SelectedValue);
            myCharges.Sequence = tbChargesSequence.Text;
            myCharges.CurrencyCode = ddlstCurrency.SelectedValue;
            if (tbChargesDescriptionEN.Text != "") {
                myCharges.DescriptionFreeTextEN = tbChargesDescriptionEN.Text;
            }
            if (tbChargesDescriptionNative.Text != "") {
                myCharges.DescriptionFreeTextNative = tbChargesDescriptionNative.Text;
            }

            return myCharges;
        }

        private void btnBoardMemberSearch_Click(object sender, EventArgs e) {
            SearchForBoardMemberNames(
                tbBoardMemberCIID.Text,
                tbBoardMemberPersonalID.Text,
                tbBoardMemberFirstName.Text,
                tbBoardMemberSurName.Text);
        }

        /// <summary>
        /// Searches for given name in the national name database
        /// </summary>
        /// <param name="name">The name or part of name to search for</param>
        /// <param name="ciid"></param>
        public void SearchForBoardMemberNames(string ciid, string nationalID, string name, string surName) {
            var uaf = new uaFactory();

            DataSet dsNameSearch;
            //Person
            var debtor = new Debtor();

            if (name.Trim() != "") {
                debtor.FirstName = name;
            }
            if (surName.Trim() != "") {
                debtor.SurName = surName;
            }
            if (ciid.Trim() != "") {
                debtor.CreditInfo = int.Parse(ciid);
            }
            if (nationalID.Trim() != "") {
                debtor.IDNumber1 = nationalID;
                debtor.IDNumber2Type = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
            }
            if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                dsNameSearch = uaf.FindCustomerInNationalAndCreditInfo(debtor);
                // if not found in national registry (f.ex. the Cyprus Whitelist is incomplete data) then search in our db
                if (dsNameSearch.Tables.Count > 0) {
                    if (dsNameSearch.Tables[0] == null || dsNameSearch.Tables[0].Rows == null ||
                        dsNameSearch.Tables[0].Rows.Count <= 0) {
                        dsNameSearch = uaf.FindCustomer(debtor);
                    }
                } else {
                    dsNameSearch = uaf.FindCustomer(debtor);
                }
            } else {
                dsNameSearch = uaf.FindCustomer(debtor);
            }
            if (dsNameSearch.Tables.Count > 0) {
                //if(dsNameSearch.Tables[0] !=null&& dsNameSearch.Tables.Count>0 && dsNameSearch.Tables[0].Rows!=null&&dsNameSearch.Tables[0].Rows.Count>0) 
                if (dsNameSearch.Tables[0] != null && dsNameSearch.Tables[0].Rows != null &&
                    dsNameSearch.Tables[0].Rows.Count > 0) {
                    dtgrBoardMemberNameSearch.DataSource = dsNameSearch;
                    dtgrBoardMemberNameSearch.DataBind();
                    tableBoardMemberNameSearch.Visible = true;

                    int gridRows = dtgrBoardMemberNameSearch.Items.Count;
                    divBoardMemberNameSearch.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                    divBoardMemberNameSearch.Style["OVERFLOW"] = "auto";
                    divBoardMemberNameSearch.Style["OVERFLOW-X"] = "auto";
                } else {
                    dtgrBoardMemberNameSearch.Visible = false;
                    DisplayMessage(rm.GetString("txtNoEntryFound", ci), lblBoardMemberMessage);
                }
            } else {
                DisplayMessage(rm.GetString("txtNoEntryFound", ci), lblBoardMemberMessage);
            }
        }

        private void btnSecretarySearch_Click(object sender, EventArgs e) {
            SearchForBoardSecretaryNames(
                tbSecretaryCIID.Text, tbSecretaryNationalID.Text, tbSecretaryFirstName.Text, tbSecretarySurName.Text);
        }

        public void SearchForBoardSecretaryNames(string ciid, string nationalID, string name, string surName) {
            var uaf = new uaFactory();
            DataSet dsNameSearch;
            if (rblSecretaryPersonCompany.Items[0].Selected) {
                // boxes/labels visibility
                lblSecretarySurName.Visible = true;
                tbSecretarySurName.Visible = true;

                var debtor = new Debtor();

                if (name.Trim() != null) {
                    debtor.FirstName = name;
                }
                if (surName.Trim() != null) {
                    debtor.SurName = surName;
                }
                if (ciid.Trim() != "") {
                    debtor.CreditInfo = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    debtor.IDNumber1 = nationalID;
                    debtor.IDNumber2Type = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                }
                if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                    dsNameSearch = uaf.FindCustomerInNationalAndCreditInfo(debtor);
                    // if not found in national registry (f.ex. the Cyprus Whitelist is incomplete data) then search in our db
                    if (dsNameSearch.Tables.Count > 0) {
                        if (dsNameSearch.Tables[0] == null || dsNameSearch.Tables[0].Rows == null ||
                            dsNameSearch.Tables[0].Rows.Count <= 0) {
                            dsNameSearch = uaf.FindCustomer(debtor);
                        }
                    } else {
                        dsNameSearch = uaf.FindCustomer(debtor);
                    }
                } else {
                    dsNameSearch = uaf.FindCustomer(debtor);
                }
            } else {
                //Company				
                // boxes 
                lblSecretarySurName.Visible = false;
                tbSecretarySurName.Visible = false;
                var company = new Company();

                if (name.Trim() != null) {
                    company.NameNative = name;
                }
                if (ciid.Trim() != "") {
                    company.CreditInfoID = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    IDNumber idn;
                    idn = new IDNumber();
                    idn.Number = nationalID;
                    idn.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                    company.IDNumbers = new ArrayList();
                    company.IDNumbers.Add(idn);
                }
                switch (CigConfig.Configure("lookupsettings.CompanyNationalRegistry")) {
                    case "True":
                        dsNameSearch = uaf.FindCompanyInNationalAndCreditInfo(company);
                        break;
                    default:
                        dsNameSearch = uaf.FindCompany(company);
                        break;
                }
            }

            if (dsNameSearch.Tables.Count > 0 && dsNameSearch.Tables[0] != null) {
                dtgrSecretaryNameSearch.DataSource = dsNameSearch;
                dtgrSecretaryNameSearch.DataBind();
                if (dtgrSecretaryNameSearch.Items.Count == 1) // only one found set the values directly to text boxes
                {
                    OneSecretaryItemFound(dsNameSearch);
                    return;
                }
                tableSecretaryNameSearch.Visible = true;

                int gridRows = dtgrSecretaryNameSearch.Items.Count;
                divSecretaryNameSearch.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                divSecretaryNameSearch.Style["OVERFLOW"] = "auto";
                divSecretaryNameSearch.Style["OVERFLOW-X"] = "auto";
            } else {
                tableSecretaryNameSearch.Visible = false;
            }
        }

        public void OneSecretaryItemFound(DataSet dsNameSearch) {
            string name = "";
            tbSecretaryCIID.Text = dsNameSearch.Tables[0].Rows[0]["CreditInfoID"].ToString();
            tbSecretaryNationalID.Text = dsNameSearch.Tables[0].Rows[0]["Number"].ToString();

            if (nativeCult) {
                // oDS.Tables[0].rows[0].Items["CLIENT_CITY"].toString()
                name = dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                if (rblSecretaryPersonCompany.Items[0].Selected) // person
                {
                    // �arf a� splitta upp name og surname kemur � einum streng

                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            tbSecretaryFirstName.Text = arrName[0];
                        } else {
                            tbSecretarySurName.Text = arrName[arrName.Length - 1];
                            tbSecretaryFirstName.Text = name.Substring(
                                0, name.Length - (tbSecretarySurName.Text.Length + 1));
                        }
                    }
                    //	this.txtFirstname.Text = dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString();
                    //	this.txtSurname.Text = dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString();
                } else {
                    tbSecretaryFirstName.Text = name.Trim(); // dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString();
                }
            } else {
                name = dsNameSearch.Tables[0].Rows[0]["NameEN"].ToString().Trim();
                if (name == "") // display native rather than nothing
                {
                    name = dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString();
                }

                if (rblSecretaryPersonCompany.Items[0].Selected) // person
                {
                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            tbSecretaryFirstName.Text = arrName[0];
                        } else {
                            tbSecretarySurName.Text = arrName[arrName.Length - 1];
                            tbSecretaryFirstName.Text = name.Substring(
                                0, name.Length - (tbSecretarySurName.Text.Length + 1));
                        }
                    }
                } else {
                    tbSecretaryFirstName.Text = name.Trim();
                }
            }
        }

        private void btnRegSecretary_Click(object sender, EventArgs e) {
            tableSecretaryNameSearch.Visible = false;
            bool person = rblSecretaryPersonCompany.Items[0].Selected;

            if (!Page.IsValid) {
                return;
            }
            bool insertOK = true;
            try {
                if (person) // indivitual
                {
                    var bSecretary = new BoardSecretaryBLLC();

                    if (nativeCult) {
                        bSecretary.FirstNameNative = tbSecretaryFirstName.Text;
                        bSecretary.SurNameNative = tbSecretarySurName.Text;
                    } else {
                        bSecretary.FirstNameEN = tbSecretaryFirstName.Text;
                        bSecretary.SurNameEN = tbSecretarySurName.Text;
                    }
                    if (tbSecretaryCIID.Text.Trim() != null) {
                        try {
                            bSecretary.CreditInfoID = int.Parse(tbSecretaryCIID.Text);
                        } catch (Exception) {
                            bSecretary.CreditInfoID = -1;
                        }
                    }
                    bSecretary.NationalID = tbSecretaryNationalID.Text;
                    bSecretary.HistoryNative = tbSecretaryHistoryNative.Text;
                    bSecretary.HistoryEN = tbSecretaryHistoryEnglish.Text;

                    insertOK = myFact.AddBoardSecretary(bSecretary, CIID);
                } else {
                    var bSecretaryCompany = new BoardSecretaryCompanyBLLC();

                    if (nativeCult) {
                        bSecretaryCompany.NameNative = tbSecretaryFirstName.Text;
                    } else {
                        bSecretaryCompany.NameEN = tbSecretaryFirstName.Text;
                    }
                    if (tbSecretaryCIID.Text.Trim() != null) {
                        try {
                            bSecretaryCompany.CreditInfoID = int.Parse(tbSecretaryCIID.Text);
                        } catch (Exception) {
                            bSecretaryCompany.CreditInfoID = -1;
                        }
                    }
                    bSecretaryCompany.NationalID = tbSecretaryNationalID.Text;
                    bSecretaryCompany.HistoryNative = tbSecretaryHistoryNative.Text;
                    bSecretaryCompany.HistoryEN = tbSecretaryHistoryEnglish.Text;

                    insertOK = myFact.AddBoardSecretaryCompany(bSecretaryCompany, CIID);
                }
            } catch (Exception err) {
                Logger.WriteToLog("Error occurred while inserting/updating board secretary", err, true);
                insertOK = false;
            }
            if (insertOK) {
                DisplayMessage(rm.GetString("txtTheInfoHasBeenRegisted", ci), lblSecretaryMsg);
                ClearSecretaryBoxes();
                LoadSecretaryData(CIID);
                tableSecretaryNameSearch.Visible = false;
            } else {
                DisplayErrorMessage(rm.GetString("txtErrorHasOccurred", ci), lblSecretaryMsg);
            }
        }

        private void btnRegBoardMemberHistory_Click(object sender, EventArgs e) {
            bool historyError = false;
            if (!RegHistory(Convert.ToInt32(CigConfig.Configure("lookupsettings.BoardMembersHistoryType")))) {
                historyError = true;
            }
            //if(!myFact.AddHistory(CIID,theHistory))
            //	historyError = true; 

            // take the history
            /*	theHistory.HistoryNative = tbHistoryNative.Text;
			theHistory.HistoryEN = tbHistoryEN.Text;
			theHistory.HistoryID = Convert.ToInt32(CigConfig.Configure("lookupsettings.BoardMembersHistoryType"]);
			if(!myFact.AddHistory(CIID,theHistory))
				historyError = true; 
		*/
            if (historyError) {
                DisplayErrorMessage(rm.GetString("txtSavingHistoryFailed", ci), lblBoardMemberMessage);
            }
        }

        private void btnRegShareOwnerHistory_Click(object sender, EventArgs e) {
            bool historyRegisted =
                RegHistory(Convert.ToInt32(CigConfig.Configure("lookupsettings.ShareholdersOwnerHistoryType")));
            // take the history
            /*	theHistory.HistoryNative = tbShareOwnerHistoryNative.Text;
			theHistory.HistoryEN = tbShareOwnerHistoryEN.Text;
			theHistory.HistoryID = Convert.ToInt32(CigConfig.Configure("lookupsettings.ShareholdersOwnerHistoryType"]);
			if(!myFact.AddHistory(CIID,theHistory))
				historyError = true; 
		*/
            if (!historyRegisted) {
                DisplayErrorMessage(rm.GetString("txtSavingHistoryFailed", ci), lblShareOwnerErrMsg);
            }
        }

        public bool RegHistory(int historyTypeID) {
            var theHistory = new HistoryBLLC();
            // take the history
            // h�r �arf a� sj�lfs�g�u a� lesa inn r�tt box

            if (int.Parse(CigConfig.Configure("lookupsettings.ShareholdersOwnerHistoryType")) == historyTypeID) {
                theHistory.HistoryNative = tbShareOwnerHistoryNative.Text;
                theHistory.HistoryEN = tbShareOwnerHistoryEN.Text;
            } else if (int.Parse(CigConfig.Configure("lookupsettings.BoardMembersHistoryType")) == historyTypeID) {
                theHistory.HistoryNative = tbBoardMemberHistoryNative.Text;
                theHistory.HistoryEN = tbBoardMemberHistoryEN.Text;
            } else if ((int.Parse(CigConfig.Configure("lookupsettings.SecretaryHistoryType")) == historyTypeID)) {
                theHistory.HistoryNative = tbSecretaryHistoryNative.Text;
                theHistory.HistoryEN = tbSecretaryHistoryEnglish.Text;
            }

            //		theHistory.HistoryNative = tbShareOwnerHistoryNative.Text;
            //		theHistory.HistoryEN = tbShareOwnerHistoryEN.Text;
            theHistory.HistoryID = historyTypeID;
            if (!myFact.AddHistory(CIID, theHistory)) {
                return false;
            }

            return true;
        }

        public void AddBasicInfoEnterEvent() {
            ddLegalForm.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbName.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            txtNameEN.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbUniqueID.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbTradeName.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            txtTradeNameEN.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbVAT.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbFormerName.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            txtFormerNameEN.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            txtSymbol.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbTel.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbMobile.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbFax.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbLAR.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbLR.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbStructureReportResearchDate.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbAddress.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            txtAddressEN.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbPostbox.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbAreaCode.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            ddCity.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbHomePage.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbEmail.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbEstablished.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbLastContacted.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbLastUpdated.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbHistoryN.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            tbHistoryE.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
            btnRegShareOwner.Attributes.Add("onkeypress", "checkBasicInfoEnterKey();");
        }

        public void AddBoardMemberEnterEvent() {
            tbBoardMemberCIID.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
            tbBoardMemberPersonalID.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
            tbBoardMemberFirstName.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
            tbBoardMemberSurName.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
            ddBoardMemberManagementPosition.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
            ddlBoardMemberAreaCode.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
            tbBoardMemberAddress.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
            tbBoardMemberHistoryNative.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
            tbBoardMemberHistoryEN.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
            btnRegBoardMember.Attributes.Add("onkeypress", "checkBoardMemberEnterKey();");
        }

        public void AddSecretaryEnterEvent() {
            tbSecretaryCIID.Attributes.Add("onkeypress", "checkSectretaryEnterKey();");
            tbSecretaryNationalID.Attributes.Add("onkeypress", "checkSectretaryEnterKey();");
            tbSecretaryFirstName.Attributes.Add("onkeypress", "checkSectretaryEnterKey();");
            tbSecretarySurName.Attributes.Add("onkeypress", "checkSectretaryEnterKey();");
            tbSecretaryHistoryNative.Attributes.Add("onkeypress", "checkSectretaryEnterKey();");
            tbSecretaryHistoryEnglish.Attributes.Add("onkeypress", "checkSectretaryEnterKey();");
            btnRegSecretary.Attributes.Add("onkeypress", "checkSectretaryEnterKey();");
        }

        public void AddCapitalEnterEvent() {
            txtAuthorizedCapital.Attributes.Add("onkeypress", "checkCapitalEnterKey();");
            txtIssuedNoOfShares.Attributes.Add("onkeypress", "checkCapitalEnterKey();");
            txtNominalNumberOfShares.Attributes.Add("onkeypress", "checkCapitalEnterKey();");
            txtAsked.Attributes.Add("onkeypress", "checkCapitalEnterKey();");
            txtPaidUp.Attributes.Add("onkeypress", "checkCapitalEnterKey();");
            txtSharesDescription.Attributes.Add("onkeypress", "checkCapitalEnterKey();");
            ddlstCurrency.Attributes.Add("onkeypress", "checkCapitalEnterKey();");
            btnRegCapital.Attributes.Add("onkeypress", "checkCapitalEnterKey();");
        }

        public void AddChargesEnterEvent() {
            tbChargesDateRegistered.Attributes.Add("onkeypress", "checkChargesEnterKey();");
            tbChargesDatePrepared.Attributes.Add("onkeypress", "checkChargesEnterKey();");
            ddlChargesDescription.Attributes.Add("onkeypress", "checkChargesEnterKey();");
            tbChargesSequence.Attributes.Add("onkeypress", "checkChargesEnterKey();");
            tbChargesAmount.Attributes.Add("onkeypress", "checkChargesEnterKey();");
            ddlChargesCurrency.Attributes.Add("onkeypress", "checkChargesEnterKey();");
            ddlChargesBeneficiary.Attributes.Add("onkeypress", "checkChargesEnterKey();");
            tbChargesDescriptionNative.Attributes.Add("onkeypress", "checkChargesEnterKey();");
            tbChargesDescriptionEN.Attributes.Add("onkeypress", "checkChargesEnterKey();");
            btnRegCharges.Attributes.Add("onkeypress", "checkChargesEnterKey();");
        }

        private void btnCitySearch_Click(object sender, EventArgs e) {
            if (txtCitySearch.Text.Trim() != "") {
                DataSet dsCity = myFact.GetCityListAsDataSet(txtCitySearch.Text, !nativeCult);
                BindCityList(dsCity);
            }
        }

        private void BindCityList(DataSet dsCity) {
            ddCity.DataSource = dsCity;
            ddCity.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddCity.DataValueField = "CityID";
            ddCity.DataBind();
        }

        private void ddlChargesCurrency_SelectedIndexChanged(object sender, EventArgs e) { }

        private static DataSet cleanDuplicatedBMembersCIIDS(DataSet ds) {
            if (ds.Tables.Count > 0) {
                var arrCiids = new ArrayList();
                var managementPositionIDs = new ArrayList();
                for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    // athuga title � sta�inn ... �.e. fyrir boardmembers
                    string managementPostionID = ds.Tables[0].Rows[i]["ManagementPositionID"].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (!((string) arrCiids[j]).Equals(creditInfoId) ||
                            !((string) managementPositionIDs[j]).Equals(managementPostionID)) {
                            continue;
                        }
                        ds.Tables[0].Rows.Remove(ds.Tables[0].Rows[i]);
                        break;
                    }
                    arrCiids.Add(creditInfoId);
                    managementPositionIDs.Add(managementPostionID);
                }
            }
            return ds;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.custValBasicName.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateBasicInfoRequiredValues);
            this.custValBasicUniqueID.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateBasicInfoRequiredValues);
            this.custValidLARBasic.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.BasicDateValidate);
            this.custValidLRBasic.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.BasicDateValidate);
            this.custValidSRRBasic.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.BasicDateValidate);
            this.custValidAddressBasic.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateBasicInfoRequiredValues);
            this.custValidCityBasic.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateBasicInfoRequiredValues);
            this.btnCitySearch.Click += new System.EventHandler(this.btnCitySearch_Click);
            this.custValidEstablishedDateBasic.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.BasicDateValidate);
            this.custValidLastContactedDateBasic.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.BasicDateValidate);
            this.custDateLastUpdateBasic.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.BasicDateValidate);
            this.btnBasicReg.Click += new System.EventHandler(this.btnBasicReg_Click);
            this.validShareOwnerNatID.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateShareholdersOwnersRequiredValues);
            this.btnShareOwnerSearch.Click += new System.EventHandler(this.btnShareOwnerSearch_Click);
            this.custValShareholdersOwnerOwnership.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateShareholdersOwnersRequiredValues);
            this.dtgrShareOwnerNameSearch.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrShareOwnerNameSearch_ItemCommand);
            this.dtgrShareOwnerNameSearch.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrShareOwnerNameSearch_ItemDataBound);
            this.btnRegShareOwnerHistory.Click += new System.EventHandler(this.btnRegShareOwnerHistory_Click);
            this.btnRegShareOwner.Click += new System.EventHandler(this.btnRegShareOwner_Click);
            this.dtgrShareholders.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrShareholders_ItemCommand);
            this.dtgrShareholders.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrShareholders_ItemDataBound);
            this.custValidPersIDBoardMembers.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateBoardMemberRequiredValues);
            this.custValidFirstNameBoardMember.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateBoardMemberRequiredValues);
            this.custValidSurNameBoardMember.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateBoardMemberRequiredValues);
            this.btnBoardMemberSearch.Click += new System.EventHandler(this.btnBoardMemberSearch_Click);
            this.dtgrBoardMemberNameSearch.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrBoardMemberNameSearch_ItemCommand);
            this.dtgrBoardMemberNameSearch.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrBoardMemberNameSearch_ItemDataBound);
            this.btnRegBoardMemberHistory.Click += new System.EventHandler(this.btnRegBoardMemberHistory_Click);
            this.btnRegBoardMember.Click += new System.EventHandler(this.btnRegBoardMember_Click);
            this.dtgrBoardMOverview.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrBoardMOverview_ItemCommand);
            this.dtgrBoardMOverview.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrBoardMOverview_ItemDataBound);
            this.custValidNationalIDSecretary.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateSecretaryRequiredValues);
            this.custValidFirstNameSecretary.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateSecretaryRequiredValues);
            this.btnSecretarySearch.Click += new System.EventHandler(this.btnSecretarySearch_Click);
            this.dtgrSecretaryNameSearch.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrSecretaryNameSearch_ItemCommand);
            this.dtgrSecretaryNameSearch.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrSecretaryNameSearch_ItemDataBound);
            this.btnRegSecretary.Click += new System.EventHandler(this.btnRegSecretary_Click);
            this.dgRegisteredCompanySecretary.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgRegisteredCompanySecretary_ItemCommand);
            this.dgRegisteredCompanySecretary.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgRegisteredCompanySecretary_ItemDataBound);
            this.custValidAuCapital.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateCapitalRequiredValues);
            this.custValIssuedShares.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateCapitalRequiredValues);
            this.custValidNomNumberShares.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateCapitalRequiredValues);
            this.custValidAsked.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateCapitalRequiredValues);
            this.custValPaidUp.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateCapitalRequiredValues);
            this.btnRegCapital.Click += new System.EventHandler(this.btnRegCapital_Click);
            this.custValDatRegCharges.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ChargesDateValidate);
            this.custValDatePrepCharges.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ChargesDateValidate);
            this.custValAmountCharges.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.ValidateChargesRequiredValues);
            this.ddlChargesCurrency.SelectedIndexChanged +=
                new System.EventHandler(this.ddlChargesCurrency_SelectedIndexChanged);
            this.btnRegCharges.Click += new System.EventHandler(this.btnRegCharges_Click);
            this.dgCharges.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCharges_ItemCommand);
            this.dgCharges.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCharges_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}