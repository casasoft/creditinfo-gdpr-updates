#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.Localization;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for AddCustomerType.
    /// </summary>
    public class AddCustomerType : Page {
        public static CultureInfo ci;
        private static string pageName = "AddCustomerType.aspx";
        public static ResourceManager rm;
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnAdd;
        protected Button btnCancel;
        protected CheckBoxList chklCustomerTypes;
        // addet
        private int CIID = -1;
        public string currentPageStep;
        protected Label lblErrMsg;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        private bool nativeCult;
        public string totalPageSteps;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = CigConfig.Configure("lookupsettings.PageIDAddCustomerType.aspx");

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=AddCustomerType");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            // check the current culture
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            String nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            lblErrMsg.Visible = false;
            if (!IsPostBack) {
                InitControls();
            }
        }

        private void InitControls() {
            BindCustomerBoxes();
            SetPageSteps();
            LocalizeText();
        }

        public void BindCustomerBoxes() {
            DataSet dsCustomerTypes = new DataSet();
            if (Cache["dsCustomerTypes"] == null) {
                dsCustomerTypes = myFact.GetCustomerTypesAsDataSet();
                Cache["dsCustomerTypes"] = dsCustomerTypes;
            } else {
                dsCustomerTypes = (DataSet) Cache["dsCustomerTypes"];
            }

            if (dsCustomerTypes.Tables.Count > 0) {
                if (nativeCult) {
                    dsCustomerTypes.Tables[0].DefaultView.Sort = "CustomerTypeNative";
                    chklCustomerTypes.DataSource = dsCustomerTypes.Tables[0].DefaultView;
                    chklCustomerTypes.DataTextField = "CustomerTypeNative";
                } else {
                    dsCustomerTypes.Tables[0].DefaultView.Sort = "CustomerTypeEN";
                    chklCustomerTypes.DataSource = dsCustomerTypes.Tables[0].DefaultView;
                    chklCustomerTypes.DataTextField = "CustomerTypeEN";
                }
                chklCustomerTypes.DataValueField = "CustomerTypeID";
                chklCustomerTypes.DataBind();
            } else {
                lblErrMsg.Text = "Could not get Customers types overview";
                lblErrMsg.Visible = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            ArrayList arrCT = new ArrayList();

            int countChecked = 0;
            CheckBoxValues theCBL;
            for (int i = 0; i < chklCustomerTypes.Items.Count; i++) {
                if (chklCustomerTypes.Items[i].Selected) {
                    countChecked++;
                    theCBL = new CheckBoxValues();
                    theCBL.Value = chklCustomerTypes.Items[i].Value;
                    theCBL.Checked = chklCustomerTypes.Items[i].Selected;
                    theCBL.Text = chklCustomerTypes.Items[i].Text;
                    arrCT.Add(theCBL);
                }
            }
            Session["arrAddetCustomerTypes"] = arrCT;
            Server.Transfer("Operation.aspx");
        }

        private void btnCancel_Click(object sender, EventArgs e) { Server.Transfer("Operation.aspx"); }

        private void SetPageSteps() {
            // get the totalPageSteps
            if (Cache["TotalPageSteps"] != null) {
                totalPageSteps = (string) Cache["TotalPageSteps"];
            } else {
                totalPageSteps = CigConfig.Configure("lookupsettings.TotalPageSteps");
                Cache["TotalPageSteps"] = totalPageSteps;
            }
            // get current page step
            currentPageStep = CigConfig.Configure(pageName);
        }

        private void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtAddCustomerType", ci);
            btnAdd.Text = rm.GetString("txtAdd", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnAdd.Click += new EventHandler(this.btnAdd_Click);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}