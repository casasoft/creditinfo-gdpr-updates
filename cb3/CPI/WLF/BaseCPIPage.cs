#region

using System;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Xml;
using CPI.Localization;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;


namespace CPI.WLF {
    /// <summary>
    /// Summary description for BaseCPIPage.
    /// </summary>
    public class BaseCPIPage : Page {
        // Localization
        protected static CultureInfo ci;
        protected static ResourceManager rm;
        protected string cpiTemplate;
        protected string currentPageStep;
        protected bool myIsEN;
        protected bool nativeCult;
        protected string nextPage;
        protected string pageName;
        protected string previousPage;
        protected string totalPageSteps;

        /// <summary>
        /// Standard initialization for web page.
        /// </summary>
        protected bool PreparePageLoad() {
            myIsEN = false;
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US")) {
                myIsEN = true;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            // check the current culture
            String nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }
            if (Session["cpiTemplate"] != null) {
                cpiTemplate = (string) Session["cpiTemplate"];
                return ReadTemplateConfig();
            } else {
                Logger.WriteToLog("No cpi template found in session", true);
                return false;
            }
        }

        protected bool ReadTemplateConfig() {
            XmlDocument doc = new XmlDocument();
            doc.Load(new XmlTextReader(CigConfig.Configure("lookupsettings.rootURL") + "/CPI/CPIConfig.xml"));
            XmlNodeList templateList = doc.GetElementsByTagName(cpiTemplate);
            if (templateList.Count == 0) {
                Logger.WriteToLog("CPI template " + cpiTemplate + " not found", true);
                return false;
            } else {
                XmlElement templateNode = (XmlElement) templateList[0];
                XmlNodeList list = templateNode.GetElementsByTagName("Page");
                totalPageSteps = list.Count.ToString();
                if (list.Count == 0) {
                    Logger.WriteToLog("No pages found in config - reading path: CPI/" + cpiTemplate + "/Page", true);
                    return false;
                } else {
                    for (int i = 0; i < list.Count; i++) {
                        XmlNode node = list[i];
                        if (node.Attributes["Name"].InnerText == pageName) {
                            currentPageStep = (i + 1).ToString();
                            Page.ID = node.Attributes["ID"].InnerText;
                            if (i > 0) {
                                XmlNode pre = list[i - 1];
                                previousPage = pre.Attributes["WebPage"].InnerText;
                            }
                            if (i < list.Count - 1) {
                                XmlNode nex = list[i + 1];
                                nextPage = nex.Attributes["WebPage"].InnerText;
                            }
                            return true;
                        }
                    }
                }
            }
            Logger.WriteToLog("CPI Template - step \\" + pageName + "\\ not found in config", true);
            return false;
        }

        protected void TransferToNextPage() {
            if (nextPage != null && nextPage != "") {
                Server.Transfer(nextPage);
            }
        }

        protected void TransferToPreviousPage() {
            if (previousPage != null && previousPage != "") {
                Server.Transfer(previousPage);
            }
        }

        /// <summary>
        /// IsEN returns true if english or false if native culture.
        /// </summary>
        /// <returns>bool</returns>
        public bool IsEN() { return myIsEN; }
    }
}