#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for Subsidiaries.
    /// </summary>
    public class Subsidiaries : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnLoadFromCompanyRegistry;
        protected Button btnReg;
        protected Button btnSearch;
        private int CIID = -1;
        protected CustomValidator CustomValidator1;
        protected HtmlGenericControl divNameSearch;
        private DataSet dsSubsidiaries = new DataSet();
        protected DataGrid dtgrNameSearch;
        protected DataGrid dtgrSubsidiaries;
        protected Label lblCIID;
        protected Label lblCompanieName;
        protected Label lblErrorMsg;
        protected Label lblInvestmentsCompanies;
        protected Label lblNameSearchDatagridHeader;
        protected Label lblNameSearchDatagridIcons;
        protected Label lblNationalID;
        protected Label lblOwnership;
        protected Label lblPageStep;
        protected Label lblSubHeader;
        protected Label lblSubsidiariesDatagridHeader;
        protected Label lblSubsidiariesDatagridIcons;
        protected HtmlTable outerNameSearchGridTable;
        protected HtmlTable outerSubsidiariesGridTable;
        protected RequiredFieldValidator rfvName;
        protected RequiredFieldValidator rfvNationalID;
        protected RequiredFieldValidator rfvOwnership;
        protected TextBox tbCompanyName;
        protected TextBox tbNationalID;
        protected TextBox tbOwnership;
        protected HtmlTableCell tdNameSearchGrid;
        protected TextBox txtAddressEN;
        protected TextBox txtAddressNative;
        protected TextBox txtCIID;
        protected TextBox txtPostalCode;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "Subsidiaries";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new"); 
                //Server.Transfer("SearchForReport.aspx?redirect=Subsidiaries");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            string toLoadFromCompanyRegistry =
                CigConfig.Configure("lookupsettings.EnableToLoadSubsidiariesFromCompanyRegistry");
            if (toLoadFromCompanyRegistry != null && toLoadFromCompanyRegistry.ToLower().Equals("true")) {
                btnLoadFromCompanyRegistry.Visible = true;
            } else {
                btnLoadFromCompanyRegistry.Visible = false;
            }

            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();

            FillDataSets();
            SetGridHeaders();
            LocalizeNameSearchGridHeader();
            InitSubsidiariesGrid();
            BindSubsidiariesGrid();
            lblErrorMsg.Visible = false;
            if (!IsPostBack) {
                outerNameSearchGridTable.Visible = false;
                tdNameSearchGrid.Visible = false;
                LocalizeText();
            }
        }

        private void FillDataSets()
        {
            dsSubsidiaries = myFact.GetSubsidiariesAsDataSet(CIID); ;
            return;
            if (Session["dsSubsidiaries"] == null)
            {
                dsSubsidiaries = myFact.GetSubsidiariesAsDataSet(CIID);
                Session["dsSubsidiaries"] = dsSubsidiaries;
            }
            else
            {
                dsSubsidiaries = (DataSet)Session["dsSubsidiaries"];
            }
        }

        private void BindSubsidiariesGrid() {
            dtgrSubsidiaries.DataSource = dsSubsidiaries;
            dtgrSubsidiaries.DataBind();
        }

        private void InitSubsidiariesGrid() {
            dtgrSubsidiaries.Columns[5].Visible = false;
            dtgrSubsidiaries.Columns[6].Visible = false;
        }

        private void dtgrSubsidiaries_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    e.Item.Cells[4].Text = e.Item.Cells[5].Text;
                } else {
                    e.Item.Cells[4].Text = e.Item.Cells[6].Text;
                }
                // For delete confirmation
                LinkButton btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
            }
            WebDesign.CreateExplanationIcons(dtgrSubsidiaries.Columns, lblSubsidiariesDatagridIcons, rm, ci);
        }

        private void dtgrSubsidiaries_ItemCommand(object source, DataGridCommandEventArgs e) {
            int rowToDelete = -1;
            int subsidiariesCIID = -1;
            bool result = false;
            try {
                subsidiariesCIID =
                    Convert.ToInt32(dsSubsidiaries.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString());

                if (e.CommandName == "Delete") {
                    rowToDelete = e.Item.ItemIndex;
                }
                if (rowToDelete >= 0) {
                    dsSubsidiaries.Tables[0].Rows[rowToDelete].Delete();
                    if (subsidiariesCIID > 0) {
                        result = myFact.DeleteSubsidaries(subsidiariesCIID, CIID);
                    }
                    if (!result) {
                        DisplayErrorMessage("Delete failed");
                        return;
                    }

                    BindSubsidiariesGrid();
                    ClearBoxes();
                    return;
                }

                if (e.CommandName == "Update") {
                    txtCIID.Text = dsSubsidiaries.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString();
                    tbCompanyName.Text = dsSubsidiaries.Tables[0].Rows[e.Item.ItemIndex]["NameNative"].ToString();
                    tbNationalID.Text = dsSubsidiaries.Tables[0].Rows[e.Item.ItemIndex]["Number"].ToString();
                    tbOwnership.Text = dsSubsidiaries.Tables[0].Rows[e.Item.ItemIndex]["Ownership"].ToString();

                    txtCIID.ReadOnly = true;
                    tbCompanyName.ReadOnly = true;
                    tbNationalID.ReadOnly = true;
                    btnReg.Text = rm.GetString("txtUpdate", ci);
                    btnReg.CommandName = "Update";
                }
            } catch (Exception err) {
                Logger.WriteToLog("RegBoarMember.aspx : dtgrBoardMOverview_ItemCommand " + err.Message, true);
            }
        }

        private void ClearBoxes() {
            txtCIID.Text = "";
            tbCompanyName.Text = "";
            tbNationalID.Text = "";
            tbOwnership.Text = "";
            // init button etc.
            tbCompanyName.ReadOnly = false;
            tbNationalID.ReadOnly = false;
            txtCIID.ReadOnly = false;
            btnReg.Text = rm.GetString("txtSave", ci);
            btnReg.CommandName = "Register";
            txtAddressNative.Text = "";
            txtAddressEN.Text = "";
            txtPostalCode.Text = "";
        }

        private void btnReg_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                RegSubsidiaries(
                    tbNationalID.Text,
                    tbCompanyName.Text,
                    tbOwnership.Text,
                    txtAddressNative.Text,
                    txtAddressEN.Text,
                    txtPostalCode.Text);
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }

        private void LocalizeText() {
            lblCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblInvestmentsCompanies.Text = rm.GetString("txtInvestmentsInOtherCompanies", ci);
            lblCompanieName.Text = rm.GetString("txtName", ci);
            lblNationalID.Text = rm.GetString("txtNationalID", ci);
            lblOwnership.Text = rm.GetString("txtOwnershipSubsidiaries", ci);
            rfvName.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvNationalID.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvOwnership.ErrorMessage = rm.GetString("txtValueMissing", ci);
            CustomValidator1.ErrorMessage = rm.GetString("txtErrorInDataInput", ci);
            btnReg.Text = rm.GetString("txtSave", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            //	this.btnCancel.Text = rm.GetString("txtCancel",ci);
            btnCancel.Text = rm.GetString("txtBack", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);
            btnLoadFromCompanyRegistry.Text = rm.GetString("txtLoadBoardFromCompanyRegistry", ci);

            lblSubHeader.Text = rm.GetString("txtAdditionalInfo", ci);
            lblNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblSubsidiariesDatagridHeader.Text = rm.GetString("txtInvestmentsInOtherCompanies", ci);
        }

        private void SetGridHeaders() {
            dtgrSubsidiaries.Columns[2].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrSubsidiaries.Columns[4].HeaderText = rm.GetString("txtName", ci);
            dtgrSubsidiaries.Columns[3].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrSubsidiaries.Columns[7].HeaderText = rm.GetString("txtOwnershipSubsidiaries", ci);
        }

        private void btnCancel_Click(object sender, EventArgs e) { TransferToPreviousPage(); }

        private void AddEnterEvent() {
            tbCompanyName.Attributes.Add("onkeypress", "checkEnterKey();");
            tbOwnership.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        /// <summary>
        /// Localizes the search grid header
        /// </summary>
        private void LocalizeNameSearchGridHeader() {
            dtgrNameSearch.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrNameSearch.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrNameSearch.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrNameSearch.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dtgrNameSearch.Columns[9].HeaderText = rm.GetString("txtAreaCode", ci);
        }

        /// <summary>
        /// Searches for given name in the national name database
        /// </summary>
        /// <param name="name">The name or part of name to search for</param>
        public void SearchForNames(string ciid, string nationalID, string name, string surName) {
            uaFactory uaf = new uaFactory();

            DataSet dsNameSearch;
            //Company				
            Company company = new Company();

            if (name.Trim() != null) {
                company.NameNative = name;
            }
            if (ciid.Trim() != "") {
                company.CreditInfoID = int.Parse(ciid);
            }
            if (nationalID.Trim() != "") {
                company.NationalID = nationalID;
            }
            /*{
				IDNumber idn = new IDNumber();
				idn.Number = nationalID;
				idn.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"]); 
				company.IDNumbers = new ArrayList();
				company.IDNumbers.Add(idn);
			}*/
            if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                dsNameSearch = uaf.FindCompanyInNationalAndCreditInfo(company);
            } else {
                dsNameSearch = uaf.FindCompany(company);
            }

            if (dsNameSearch.Tables.Count > 0 && dsNameSearch.Tables[0] != null) {
                if (dsNameSearch.Tables[0].Rows.Count > 0) {
                    dtgrNameSearch.DataSource = dsNameSearch;
                    dtgrNameSearch.DataBind();
                    tdNameSearchGrid.Visible = true;
                    outerNameSearchGridTable.Visible = true;

                    int gridRows = dtgrNameSearch.Items.Count;
                    if (gridRows <= 6) {
                        divNameSearch.Style["HEIGHT"] = "auto";
                    } else {
                        divNameSearch.Style["HEIGHT"] = "164px";
                    }
                    divNameSearch.Style["OVERFLOW"] = "auto";
                    divNameSearch.Style["OVERFLOW-X"] = "auto";
                } else {
                    DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                    tdNameSearchGrid.Visible = false;
                }
            } else {
                tdNameSearchGrid.Visible = false;
            }
        }

        private void DisplayMessage(String message) {
            lblErrorMsg.Text = message;
            lblErrorMsg.ForeColor = Color.Blue;
            lblErrorMsg.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblErrorMsg.Text = errorMessage;
            lblErrorMsg.ForeColor = Color.Red;
            lblErrorMsg.Visible = true;
        }

        private void dtgrNameSearch_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }

                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }

                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);	
            }
            WebDesign.CreateExplanationIcons(dtgrNameSearch.Columns, lblNameSearchDatagridIcons, rm, ci);
        }

        private void dtgrNameSearch_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                if (e.Item.Cells[7].Text != "&nbsp;") {
                    txtAddressNative.Text = e.Item.Cells[7].Text;
                }
                if (e.Item.Cells[8].Text != "&nbsp;") {
                    txtAddressEN.Text = e.Item.Cells[8].Text;
                }
                if (e.Item.Cells[9].Text != "&nbsp;") {
                    txtPostalCode.Text = e.Item.Cells[9].Text;
                }

                txtCIID.Text = e.Item.Cells[1].Text;
                if (txtCIID.Text == "&nbsp;") {
                    txtCIID.Text = "";
                }
                tbNationalID.Text = e.Item.Cells[2].Text;
                if (tbNationalID.Text == "&nbsp;") {
                    tbNationalID.Text = "";
                }

                string name = e.Item.Cells[3].Text.Trim();
                if (name == "&nbsp;") {
                    name = "";
                }
                tbCompanyName.Text = name;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e) { SearchForNames(txtCIID.Text, tbNationalID.Text, tbCompanyName.Text, ""); }

        private void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args) {
            if (tbOwnership.Text.Length > 0) {
                try {
                    double share = Convert.ToDouble(tbOwnership.Text);

                    if (share <= 100 && share > 0) {
                        args.IsValid = true;
                    } else {
                        args.IsValid = false;
                    }
                } catch (Exception) {
                    args.IsValid = false;
                }
            }
        }

        private void RegSubsidiaries(
            string natID, string name, string ownership, string addNative, string addEN, string postCode) {
            Address myAddress = null;

            if (addNative.Length > 0 || addEN.Length > 0 || postCode.Length > 0) {
                myAddress = new Address();
                myAddress.StreetNative = addNative;
                myAddress.StreetEN = addEN;
                myAddress.PostalCode = postCode;
                myAddress.CityID = -1;
            }

            bool result = false;
            SubsidiariesBLLC theSub = new SubsidiariesBLLC();
            if (nativeCult) {
                theSub.NameNative = name;
            } else {
                theSub.NameEN = name;
            }
            theSub.Ownership = Convert.ToDouble(ownership);
            theSub.NationalID = natID;
            //	theSOPerson.Ownership = this.tbOwnership.Text;

            if (myAddress != null) {
                theSub.Address.Add(myAddress);
            }

            result = myFact.AddSubsidaries(theSub, CIID);
            if (!result) {
                DisplayErrorMessage("Subsidiaries insert failed");
                return;
            }
            Session["dsSubsidiaries"] = null;
            FillDataSets();
            BindSubsidiariesGrid();
            ClearBoxes();
        }

        private void btnLoadFromCompanyRegistry_Click(object sender, EventArgs e) {
            string nationalId = myFact.GetNationalIDByCIID(CIID);
            if (nationalId != null) {
                DataSet ds = myFact.GetSubsidiariesFromCompanyRegistryAsDataSet(nationalId);
                if (ds.Tables.Count > 0 && ds.Tables[0] != null) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        //Get information about board member from CompanyRegistry
                        //string firstName="";
                        //string surName = "";
                        string ssn = ds.Tables[0].Rows[i]["Registration_No"].ToString();
                        string name = ds.Tables[0].Rows[i]["Company_Name"].ToString();
                        //string state = ds.Tables[0].Rows[i]["State"].ToString();
                        RegSubsidiaries(ssn, name, "0", "", "", "");
                    }
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearch.Click += new EventHandler(this.btnSearch_Click);
            this.CustomValidator1.ServerValidate += new ServerValidateEventHandler(this.CustomValidator1_ServerValidate);
            this.dtgrNameSearch.ItemCommand += new DataGridCommandEventHandler(this.dtgrNameSearch_ItemCommand);
            this.dtgrNameSearch.ItemDataBound += new DataGridItemEventHandler(this.dtgrNameSearch_ItemDataBound);
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnLoadFromCompanyRegistry.Click += new EventHandler(this.btnLoadFromCompanyRegistry_Click);
            this.btnContinue.Click += new EventHandler(this.btnContinue_Click);
            this.btnReg.Click += new EventHandler(this.btnReg_Click);
            this.dtgrSubsidiaries.ItemCommand += new DataGridCommandEventHandler(this.dtgrSubsidiaries_ItemCommand);
            this.dtgrSubsidiaries.ItemDataBound += new DataGridItemEventHandler(this.dtgrSubsidiaries_ItemDataBound);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}