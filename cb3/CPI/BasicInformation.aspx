<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="BasicInformation.aspx.cs" AutoEventWireup="false" Inherits="CPI.BasicInformation" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BasicInformation</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
<?xml version="1.0" encoding="ISO-8859-1" ?>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>
					function checkEnterKey() 
					{    
							if (event.keyCode == 13) 
							{        
								event.cancelBubble = true;
								event.returnValue = false;
								BasicInfoForm.btnReg.click(); 
							}
					} 
					function SetFormFocus()
						{
							document.BasicInfoForm.tbName.focus();
							ScrollIt();
						}

	function ScrollIt()
	{
		window.scrollTo(document.BasicInfoForm.PageX.value, document.BasicInfoForm.PageY.value);
    }
	function setcoords()
	{
		var myPageX;
		var myPageY;
		if (document.all)
		{
			myPageX = document.body.scrollLeft;
			myPageY = document.body.scrollTop;
        }
		else
		{
			myPageX = window.pageXOffset;
			myPageY = window.pageYOffset;
        }
		document.BasicInfoForm.PageX.value = myPageX;
		document.BasicInfoForm.PageY.value = myPageY;
    }
		</script>
	</HEAD>
	<body onscroll="javascript:setcoords()" onload="SetFormFocus()">
		<form id="BasicInfoForm" name="BasicInfoForm" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head" id="tbDefault">
																<tr>
																	<td>
																		<asp:label id="lblBasicInfo" runat="server">Basic Info</asp:label></td>
																	<td align="right"><asp:label id="lblPageStep" runat="server">Step 1 of ?</asp:label></td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td colSpan="2"><asp:label id="lblLegalForm" runat="server">Legal form</asp:label><br>
																		<asp:dropdownlist id="ddLegalForm" CssClass="auto_input" runat="server">
																			<asp:listitem value="0">Company Type</asp:listitem>
																		</asp:dropdownlist></td>
																</tr>
																<tr>
																	<td align="left" style="WIDTH: 25%"><asp:label id="lblName" runat="server">Name</asp:label><br>
																		<asp:textbox id="tbName" runat="server" maxlength="100"></asp:textbox>
																		<asp:label id="Label1" runat="server" cssclass="error_text">*</asp:label>
																		<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" controltovalidate="tbName" errormessage="Value missing!"
																			display="None"></asp:requiredfieldvalidator></td>
																	<td style="WIDTH: 25%"><asp:label id="lblNameEN" runat="server">Name (EN)</asp:label><br>
																		<asp:textbox id="txtNameEN" runat="server" maxlength="100"></asp:textbox></td>
																	<td align="left" style="WIDTH: 25%"><asp:label id="lblTradeName" runat="server">Trade Name</asp:label><br>
																		<asp:textbox id="tbTradeName" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblTradeNameEN" runat="server">Trade Name (EN)</asp:label><br>
																		<asp:textbox id="txtTradeNameEN" runat="server" maxlength="255"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblUniqueID" runat="server">Unique ID</asp:label><br>
																		<asp:textbox id="tbUniqueID" runat="server" maxlength="100"></asp:textbox>
																		<asp:label id="Label2" runat="server" cssclass="error_text">*</asp:label><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" errormessage="Valure missing!" controltovalidate="tbUniqueID"
																			display="None"></asp:requiredfieldvalidator></td>
																	<td><asp:label id="lblFormerName" runat="server">Former Name</asp:label><br>
																		<asp:textbox id="tbFormerName" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblFormerNameEN" runat="server">Former Name (EN)</asp:label><br>
																		<asp:textbox id="txtFormerNameEN" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblSymbol" runat="server">Symbol</asp:label><br>
																		<asp:textbox id="txtSymbol" runat="server" maxlength="10"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblVAT" runat="server">VAT</asp:label><br>
																		<asp:textbox id="tbVAT" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblTel" runat="server">Tel</asp:label><br>
																		<asp:textbox id="tbTel" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblMobile" runat="server">Mobile</asp:label><br>
																		<asp:textbox id="tbMobile" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblFax" runat="server">Fax</asp:label><br>
																		<asp:textbox id="tbFax" runat="server" maxlength="50"></asp:textbox></td>
																</tr>
																<tr id="CYPLARLABELSROW" runat="server">
																	<td><asp:label id="lblLAR" runat="server">L.A.R.</asp:label><br>
																		<asp:textbox id="tbLAR" runat="server"></asp:textbox><asp:customvalidator id="CustomValidator4" runat="server" errormessage="Error in date input" controltovalidate="tbLAR">*</asp:customvalidator></td>
																	<td><asp:label id="lblLR" runat="server">L.R.</asp:label><br>
																		<asp:textbox id="tbLR" runat="server"></asp:textbox><asp:customvalidator id="CustomValidator5" runat="server" errormessage="Error in date input" controltovalidate="tbLR">*</asp:customvalidator></td>
																	<td><asp:label id="lbStructureResearchDate" runat="server">Structure report research</asp:label><br>
																		<asp:textbox id="tbStructureReportResearchDate" runat="server"></asp:textbox><asp:customvalidator id="CustomValidator6" runat="server" errormessage="Eror in Date input" controltovalidate="tbStructureReportResearchDate">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblAddressHeader" runat="server">Address</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 33%"><asp:label id="lblAddress" runat="server">Address</asp:label><br>
																		<asp:textbox id="tbAddress" runat="server" maxlength="100"></asp:textbox>
																		<asp:label id="Label3" runat="server" cssclass="error_text">*</asp:label><asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" errormessage="Value missing!" controltovalidate="tbAddress"
																			display="None"></asp:requiredfieldvalidator></td>
																	<td style="WIDTH: 33%"><asp:label id="lblAddressEN" runat="server">Address (EN)</asp:label><br>
																		<asp:textbox id="txtAddressEN" runat="server" maxlength="100"></asp:textbox></td>
																	<td><asp:label id="lblCountry" runat="server">Country</asp:label><br>
																	<asp:DropDownList runat="server" ID="ddCountryList" />
																	</td>
																</tr>
																<tr>
																	<td style="HEIGHT: 63px"><asp:label id="lblPostBox" runat="server">Postbox</asp:label><br>
																		<asp:textbox id="tbPostbox" runat="server" maxlength="20"></asp:textbox></td>
																	<td style="HEIGHT: 63px"><asp:label id="lblAreaCode" runat="server">Area code</asp:label><br>
																		<asp:textbox id="tbAreaCode" runat="server" maxlength="30"></asp:textbox></td>
																	<td style="HEIGHT: 63px"><asp:label id="lblCity" runat="server">City</asp:label><asp:label id="lblCitySearch" runat="server">City</asp:label><br>
																		<asp:dropdownlist id="ddCity" runat="server"></asp:dropdownlist>
																		<asp:textbox id="txtCitySearch" runat="server" maxlength="10" cssclass="short_input"></asp:textbox>&nbsp;<asp:button id="btnCitySearch" runat="server" cssclass="popup" text="..." causesvalidation="False"></asp:button>
																		<asp:label id="Label4" runat="server" cssclass="error_text">*</asp:label>
																		<asp:requiredfieldvalidator id="rfvCity" runat="server" controltovalidate="ddCity" errormessage="Value missing!"
																			display="None"></asp:requiredfieldvalidator></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblTradingAddressHeader" runat="server">Trading address</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 33%"><asp:label id="lblTradingAddress" runat="server" width="100%">Trading address</asp:label><br>
																		<asp:textbox id="txtTradingAddress" runat="server" maxlength="100"></asp:textbox></td>
																	<td style="WIDTH: 33%"><asp:label id="lblTradingAddressEN" runat="server" width="100%">Trading address (EN)</asp:label><br>
																		<asp:textbox id="txtTradingAddressEN" runat="server" maxlength="100"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblTradingPostBox" runat="server">Postbox</asp:label><br>
																		<asp:textbox id="txtTradingPostBox" runat="server" maxlength="20"></asp:textbox></td>
																	<td><asp:label id="lblTradingPostalCode" runat="server">Area code</asp:label><br>
																		<asp:textbox id="txtTradingPostalCode" runat="server" maxlength="30"></asp:textbox></td>
																	<td><asp:label id="lblTradingCity" runat="server">City</asp:label><asp:label id="lblTradingCitySearch" runat="server">City</asp:label><br>
																		<asp:dropdownlist id="ddlTradingCity" runat="server"></asp:dropdownlist>&nbsp;<asp:textbox id="txtTradingCitySearch" runat="server" cssclass="short_input" maxlength="10"></asp:textbox>&nbsp;<asp:button id="btnTradingCitySearch" runat="server" cssclass="popup" text="..." causesvalidation="False"></asp:button>
																		<asp:label id="Label5" runat="server" cssclass="error_text">*</asp:label><asp:requiredfieldvalidator id="rfvTradingCity" runat="server" errormessage="*" controltovalidate="ddlTradingCity"
																			enabled="False" display="None"></asp:requiredfieldvalidator></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblAdditionalInfoHeader" runat="server">[AdditionalInfo]</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 33%"><asp:label id="lblHomePage" runat="server">Homepage</asp:label><br>
																		<asp:textbox id="tbHomePage" runat="server" maxlength="100"></asp:textbox></td>
																	<td style="WIDTH: 33%"><asp:label id="lblEmail" runat="server">E-mail</asp:label><br>
																		<asp:textbox id="tbEmail" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblRegistered" runat="server">Established</asp:label><br>
																		<asp:textbox id="tbRegistered" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbRegistered', 250, 250);" type="button" value="..."></td>
																</tr>
																<tr>
																	<td><asp:label id="lblEstablished" runat="server">Established</asp:label><br>
																		<asp:textbox id="tbEstablished" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbEstablished', 250, 250);" type="button" value="...">
																		<asp:customvalidator id="CustomValidator1" runat="server" errormessage="Date input in wrong format" controltovalidate="tbEstablished">*</asp:customvalidator></td>
																	<td><asp:label id="lblLastContacted" runat="server">Last contacted</asp:label><br>
																		<asp:textbox id="tbLastContacted" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbLastContacted', 250, 250);" type="button"
																			value="...">
																		<asp:customvalidator id="CustomValidator2" runat="server" errormessage="Date input in wrong format" controltovalidate="tbLastContacted">*</asp:customvalidator></td>
																	<td><asp:label id="lblLastUpdated" runat="server">Last updated</asp:label><br>
																		<asp:textbox id="tbLastUpdated" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbLastUpdated', 250, 250);" type="button" value="...">
																		<asp:customvalidator id="CustomValidator3" runat="server" errormessage="Date input in wrong format" controltovalidate="tbLastUpdated">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblContactInformation" runat="server">Contact information</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 33%"><asp:label id="lblContactFirstName" runat="server">First name</asp:label><br>
																		<asp:textbox id="txtContactFirstName" runat="server" maxlength="50"></asp:textbox></td>
																	<td style="WIDTH: 33%"><asp:label id="lblContactLastName" runat="server">Last name</asp:label><br>
																		<asp:textbox id="txtContactLastName" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblContactMessengerID" runat="server">Messenger ID</asp:label><br>
																		<asp:textbox id="txtContactMessengerID" runat="server" maxlength="255"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="lblContactFirstNameEN" runat="server">First name (EN)</asp:label><br>
																		<asp:textbox id="txtContactFirstNameEN" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblContactLastNameEN" runat="server">Last name (EN)</asp:label><br>
																		<asp:textbox id="txtContactLastNameEN" runat="server" maxlength="50"></asp:textbox></td>
																	<td></td>
																</tr>
																<tr>
																	<td><asp:label id="lblContactEmail" runat="server">Email</asp:label><br>
																		<asp:textbox id="txtContactEmail" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblContactPosition" runat="server">Position</asp:label><br>
																		<asp:textbox id="txtContactPosition" runat="server" maxlength="255"></asp:textbox></td>
																	<td><asp:label id="lblContactDateContacted" runat="server">Date contacted</asp:label><br>
																		<asp:textbox id="txtContactDateContacted" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('txtContactDateContacted', 250, 250);" type="button"
																			value="...">
																		<asp:customvalidator id="cvContactDateContacted" runat="server" errormessage="Date input in wrong format"
																			controltovalidate="txtContactDateContacted">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td><asp:label id="lblContactPhoneNumber" runat="server">Phone number</asp:label><br>
																		<asp:textbox id="txtContactPhoneNumber" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblContactMobileNumber" runat="server">Mobile number</asp:label><br>
																		<asp:textbox id="txtContactMobileNumber" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblContactFaxNumber" runat="server">Fax number</asp:label><br>
																		<asp:textbox id="txtContactFaxNumber" runat="server" maxlength="50"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblCommentsHeader" runat="server">[Comments]</asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 50%"><asp:label id="lblComments" runat="server" width="300px">Comments (will not be included in the report)</asp:label><br>
																		<asp:textbox id="tbComments" runat="server" maxlength="2048" width="360px" height="100px" textmode="MultiLine"></asp:textbox></td>
																	<td><asp:label id="lblInternalCommentEN" runat="server">CommentEN</asp:label><br>
																		<asp:textbox id="txtCommentEN" runat="server" maxlength="2048" width="360px" height="100px" textmode="MultiLine"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left"><asp:label id="lblRegMsg" runat="server" cssclass="confirm_text" visible="False">The info has been registed</asp:label><asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancel" runat="server" text="Cancel" causesvalidation="False" cssclass="cancel_button"></asp:button><asp:button id="btnContinue" runat="server" text="Continue" causesvalidation="False" cssclass="gray_button"></asp:button><asp:button id="btnReg" runat="server" text="Reg" cssclass="confirm_button"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
									</table>
									<input id="PageX" type="hidden" value="0" name="PageX" runat="server"> <input id="PageY" type="hidden" value="0" name="PageY" runat="server">
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
