<%@ Register TagPrefix="uc1" TagName="head" Src="../user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../user_controls/footer.ascx" %>
<%@ Page language="c#" Codebehind="TemplatePage.aspx.cs" AutoEventWireup="false" Inherits="CPI.TemplatePage" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>TemplatePage</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
			<TABLE id="tbDefault" borderColor="#0000ff" cellSpacing="0" cellPadding="0" width="600"
				align="center">
				<TR>
				</TR>
				<TR>
					<TD align="left" colSpan="8"><uc1:head id="ctlHeader" runat="server"></uc1:head></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 3px" align="center" colSpan="8"><asp:label id="lblPageTitle" runat="server" Width="100%" CssClass="pageheader"> PageTitle </asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 35px" align="center" colSpan="8"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 40px" align="left" colSpan="8"></TD>
				</TR>
				<TR>
					<TD align="left" width="100%" colSpan="8"><uc1:footer id=Footer1 runat="server"></uc1:footer></TD>
				</TR>
				<TR>
					<TD align="center" colSpan="8"></TD>
				</TR>
			</TABLE>
		</form>
	
  </body>
</HTML>
