<%@ Page language="c#" Codebehind="KeyEmployees.aspx.cs" AutoEventWireup="false" Inherits="CPI.KeyEmployees" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>KeyEmployees</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
	</head>
	<body ms_positioning="GridLayout">
		<form id="KeyStaffForm" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblPageTitle" runat="server">Information Entry</asp:label>-
																		<asp:label id="lblKeyEmployees" runat="server">Key employees</asp:label>
																	</td>
																	<td align="right">
																		<asp:label id="lblPageStep" runat="server">Step</asp:label>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
												</table>
												<!-- END // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right">
															<asp:hyperlink id="hlRegBoardMembers" runat="server" cssclass="seemore" navigateurl="RegBoardMembers.aspx">Edit  boardmembers</asp:hyperlink>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblBoardMembersOverview" runat="server">Board members</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dtgrBoardMOverview" runat="server" autogeneratecolumns="False" gridlines="None"
																			cssclass="grid">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																			<columns>
																				<asp:boundcolumn headertext="Name">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Number" headertext="National ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameNative" headertext="FirstNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameNative" headertext="SurNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameEN" headertext="FirstNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameEN" headertext="SurNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Title">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="TitleNative" headertext="TitleNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="TitleEN" headertext="TitleEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="Table1" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right">
															<asp:hyperlink id="hlRegStaff" runat="server" cssclass="seemore" navigateurl="Principals.aspx">Edit key staff</asp:hyperlink>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblPrincipalsOverview" runat="server">Principals</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dtgrPrincipals" runat="server" autogeneratecolumns="False" gridlines="None"
																			cssclass="grid">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																			<columns>
																				<asp:boundcolumn headertext="Name">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Number" headertext="National ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameNative" headertext="FirstNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameNative" headertext="SurNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameEN" headertext="FirstNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameEN" headertext="SurNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Title">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameNative" headertext="TitleNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameEN" headertext="TitleEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Education">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="EdNameNative" headertext="EducationNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="EdNameEN" headertext="EducationEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="Table2" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td align="right">
															<asp:hyperlink id="hlRegStaffCount" runat="server" cssclass="seemore" navigateurl="AddEmployeesCount.aspx">Edit staff count</asp:hyperlink>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblRegStaffCount" runat="server">Registed Staff count</asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dtgrStaffCountOverview" runat="server" autogeneratecolumns="False" gridlines="None"
																			cssclass="grid">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																			<columns>
																				<asp:boundcolumn datafield="Year" readonly="True" headertext="Year">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Count" headertext="Staff count">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:label id="lblError" runat="server" cssclass="error_text">Error has occurred</asp:label>
														</td>
														<td align="right">
															<asp:button id="btnContinue" runat="server" cssclass="confirm_button" text="Continue"></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
