#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;
using UserAdmin.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for Principals.
    /// </summary>
    public class Principals : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        private readonly PrincipalsBLLC thePrincipal = new PrincipalsBLLC();
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnReg;
        protected Button btnRegHistory;
        protected Button btnSearch;
        private int CIID = 1;
        protected DropDownList ddEducation;
        protected DropDownList ddJobtitle;
        protected DropDownList ddlAreaCode;
        protected HtmlGenericControl divNameSearch;
        private DataSet dsDetailedPrincipals = new DataSet();
        private DataSet dsEducationList = new DataSet();
        private DataSet dsJobtitle = new DataSet();
        protected DataGrid dtgrNameSearch;
        protected DataGrid dtgrPrincipals;
        protected Label lblAddress;
        protected Label lblAreaCode;
        protected Label lblCIID;
        protected Label lblEducation;
        protected Label lblFirstName;
        protected Label lblHistoryEN;
        protected Label lblHistoryNative;
        protected Label lblJobtitle;
        protected Label lblMessage;
        protected Label lblNameSearchDatagridHeader;
        protected Label lblNameSearchDatagridIcons;
        protected Label lblPageStep;
        protected Label lblPersonalID;
        protected Label lblPrincipals;
        protected Label lblPrincipalsDatagridHeader;
        protected Label lblPrincipalsDatagridIcons;
        protected Label lblSubHeader;
        protected Label lblSurname;
        protected HtmlTable outerNameSearchGridTable;
        protected HtmlTable outerPrincipalsGridTable;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected RequiredFieldValidator RequiredFieldValidator4;
        protected TextBox tbAddress;
        protected TextBox tbFirstName;
        protected TextBox tbHistoryEN;
        protected TextBox tbHistoryNative;
        protected TextBox tbNationalID;
        protected TextBox tbSurname;
        protected HtmlTableCell Td1;
        protected TextBox txtCIID;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "Principals";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new"); 
                //Server.Transfer("SearchForReport.aspx?redirect=Principals");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            // Add <ENTER> event
            AddEnterEvent();
            FillPrincipalsSet();
            SetGridHeaders();
            LocalizeNameSearchGridHeader();
            InitPrincipalsGrid();
            BindPrincipalsGrid();
            lblMessage.Visible = false;
            if (!IsPostBack) {
                outerNameSearchGridTable.Visible = false;

                lblAddress.Visible = false;
                tbAddress.Visible = false;
                lblAreaCode.Visible = false;
                ddlAreaCode.Visible = false;
                outerNameSearchGridTable.Visible = false;
                InitDDBoxes();
                LocalizeText();
                SetHistoryBoxes();
                RequiredFieldValidator4.Enabled = true;
            }
        }

        private void btnReg_Click(object sender, EventArgs e) {
            HistoryBLLC theHistory = new HistoryBLLC();
            bool historyError = false;
            if (nativeCult) {
                thePrincipal.FirstNameNative = tbFirstName.Text;
                thePrincipal.SurNameNative = tbSurname.Text;
                thePrincipal.SingleAddressNative = tbAddress.Text;
            } else {
                thePrincipal.FirstNameEN = tbFirstName.Text;
                thePrincipal.SurNameEN = tbSurname.Text;
                thePrincipal.SingleAddressEN = tbAddress.Text;
            }
            if (txtCIID.Text != "") {
                thePrincipal.CreditInfoID = Convert.ToInt32(txtCIID.Text);
            }
            thePrincipal.NationalID = tbNationalID.Text;
            thePrincipal.EducationID = Convert.ToInt32(ddEducation.SelectedValue);
            thePrincipal.JobTitleID = Convert.ToInt32(ddJobtitle.SelectedValue);

            // take the history
            theHistory.HistoryNative = tbHistoryNative.Text;
            theHistory.HistoryEN = tbHistoryEN.Text;
            theHistory.HistoryID = Convert.ToInt32(CigConfig.Configure("lookupsettings.DirectorHistoryType"));
            if (!myFact.AddHistory(CIID, theHistory)) {
                historyError = true;
            }

            if (myFact.AddPrincipal(thePrincipal, CIID)) {
                Session["dsDetailedPrincipals"] = null;
                FillPrincipalsSet();
                BindPrincipalsGrid();
                ClearBoxes();
                outerNameSearchGridTable.Visible = false;
            } else {
                DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
            }
            if (historyError) {
                DisplayErrorMessage(rm.GetString("txtSavingHistoryFailed", ci));
            }
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblMessage.Text = errorMessage;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Visible = true;
        }

        private void DisplayMessage(String message) {
            lblMessage.Text = message;
            lblMessage.ForeColor = Color.Blue;
            lblMessage.Visible = true;
        }

        private void ClearBoxes() {
            txtCIID.Text = "";
            tbNationalID.Text = "";
            tbAddress.Text = "";
            tbFirstName.Text = "";
            tbSurname.Text = "";
            ddJobtitle.SelectedIndex = 0;
            ddEducation.SelectedIndex = 0;
            // init buttons etc...
            btnReg.Text = rm.GetString("txtSave", ci);
            btnReg.CommandName = "Register";
            tbNationalID.ReadOnly = false;
            txtCIID.ReadOnly = false;
            tbFirstName.ReadOnly = false;
            tbSurname.ReadOnly = false;
            RequiredFieldValidator4.Enabled = true;
        }

        private void SetHistoryBoxes() {
            HistoryBLLC theHistory = new HistoryBLLC();
            theHistory = myFact.GetHistory(
                CIID, Convert.ToInt32(CigConfig.Configure("lookupsettings.DirectorHistoryType")));
            tbHistoryNative.Text = theHistory.HistoryNative;
            tbHistoryEN.Text = theHistory.HistoryEN;
        }

        public void InitPrincipalsGrid() {
            dtgrPrincipals.Columns[5].Visible = false;
            dtgrPrincipals.Columns[6].Visible = false;
            dtgrPrincipals.Columns[7].Visible = false;
            dtgrPrincipals.Columns[8].Visible = false;
            dtgrPrincipals.Columns[10].Visible = false;
            dtgrPrincipals.Columns[11].Visible = false;
            dtgrPrincipals.Columns[13].Visible = false;
            dtgrPrincipals.Columns[14].Visible = false;
        }

        public void BindPrincipalsGrid() {
            if (dsDetailedPrincipals.Tables[0] != null) {
                dtgrPrincipals.DataSource = dsDetailedPrincipals;
                dtgrPrincipals.DataBind();
            }
        }

        public void InitDDBoxes() {
            InitJobTitle();
            InitEducation();
        }

        public void InitJobTitle() {
            if (Session["dsJobtitle"] == null) {
                dsJobtitle = myFact.GetJobTitlesAsDataSet();
                Session["dsJobtitle"] = dsJobtitle;
            } else {
                dsJobtitle = (DataSet) Session["dsJobtitle"];
            }

            if (nativeCult) {
                dsJobtitle.Tables[0].DefaultView.Sort = "NameNative";
                ddJobtitle.DataSource = dsJobtitle.Tables[0].DefaultView;
                ddJobtitle.DataTextField = "NameNative";
            } else {
                dsJobtitle.Tables[0].DefaultView.Sort = "NameEN";
                ddJobtitle.DataSource = dsJobtitle.Tables[0].DefaultView;
                ddJobtitle.DataTextField = "NameEN";
            }
            ddJobtitle.DataValueField = "JobTitleID";
            ddJobtitle.DataBind();
        }

        public void InitEducation() {
            if (Session["dsEducationList"] == null) {
                dsEducationList = myFact.GetEducationListAsDataSet();
                Session["dsEducationList"] = dsEducationList;
            } else {
                dsEducationList = (DataSet) Session["dsEducationList"];
            }

            //this.ddEducation.DataSource = dsEducationList;	
            if (nativeCult) {
                dsEducationList.Tables[0].DefaultView.Sort = "NameNative";
                ddEducation.DataSource = dsEducationList.Tables[0].DefaultView;
                ddEducation.DataTextField = "NameNative";
            } else {
                dsEducationList.Tables[0].DefaultView.Sort = "NameEN";
                ddEducation.DataSource = dsEducationList.Tables[0].DefaultView;
                ddEducation.DataTextField = "NameEN";
            }
            ddEducation.DataValueField = "EducationID";
            ddEducation.DataBind();
        }

        public void FillPrincipalsSet() {
            if (Session["dsDetailedPrincipals"] == null) {
                dsDetailedPrincipals = myFact.GetDetailedPrincipalsAsDataSet(CIID);
                Session["dsDetailedPrincipals"] = dsDetailedPrincipals;
            } else {
                dsDetailedPrincipals = (DataSet) Session["dsDetailedPrincipals"];
            }
        }

        private void dtgrPrincipals_ItemDataBound(object sender, DataGridItemEventArgs e) {
            string tmpString = "";
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    if (e.Item.Cells[4].Text != "&nbsp;") // non native info
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[5].Text + " " + e.Item.Cells[6].Text;
                    } else // display english info rather then nothing
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[7].Text + " " + e.Item.Cells[8].Text;
                    }
                    if (e.Item.Cells[10].Text != "&nbsp;") // non native info
                    {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    } else // display english info rather then nothing
                    {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    }
                    if (e.Item.Cells[13].Text != "&nbsp;") // non native info
                    {
                        e.Item.Cells[12].Text = e.Item.Cells[13].Text;
                    } else // display english info rather then nothing
                    {
                        e.Item.Cells[12].Text = e.Item.Cells[14].Text;
                    }
                } else {
                    tmpString = e.Item.Cells[7].Text; ///???????????????????????????
                    if (e.Item.Cells[6].Text != "&nbsp;") // non english info
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[7].Text + " " + e.Item.Cells[8].Text;
                    } else // display native info rather then nothing
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[5].Text + " " + e.Item.Cells[6].Text;
                    }
                    if (e.Item.Cells[11].Text != "&nbsp;") // non english info
                    {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    } else // display native info rather then nothing
                    {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    }
                    if (e.Item.Cells[14].Text != "&nbsp;") // non english info
                    {
                        e.Item.Cells[12].Text = e.Item.Cells[14].Text;
                    } else // display native info rather then nothing
                    {
                        e.Item.Cells[12].Text = e.Item.Cells[13].Text;
                    }
                }

                // For delete confirmation
                LinkButton btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);

                WebDesign.CreateExplanationIcons(dtgrPrincipals.Columns, lblPrincipalsDatagridIcons, rm, ci);
            }
        }

        private void dtgrPrincipals_ItemCommand(object source, DataGridCommandEventArgs e) {
            int rowToDelete = -1;
            int boardMemberCIID = -1;
            int jobTitleID = -1;
            try {
                boardMemberCIID =
                    Convert.ToInt32(dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString());
                jobTitleID =
                    Convert.ToInt32(dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["JobTitleID"].ToString());
                if (e.CommandName == "Update") {
                    if (nativeCult) {
                        tbAddress.Text =
                            dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["StreetNative"].ToString();
                        tbFirstName.Text =
                            dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["FirstNameNative"].ToString();
                        tbSurname.Text =
                            dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["SurNameNative"].ToString();
                    } else {
                        tbAddress.Text = dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["StreetEN"].ToString();
                        tbFirstName.Text =
                            dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["FirstNameEN"].ToString();
                        tbSurname.Text = dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["SurNameEN"].ToString();
                    }

                    txtCIID.Text = dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString();
                    tbNationalID.Text = dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["Number"].ToString();
                    ddEducation.SelectedValue =
                        dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["EducationID"].ToString();
                    ddJobtitle.SelectedValue =
                        dsDetailedPrincipals.Tables[0].Rows[e.Item.ItemIndex]["JobTitleID"].ToString();

                    btnReg.Text = rm.GetString("txtUpdate", ci);
                    btnReg.CommandName = "Update";
                    tbNationalID.ReadOnly = true;
                    txtCIID.ReadOnly = true;
                    tbFirstName.ReadOnly = true;
                    tbSurname.ReadOnly = true;
                } else if (e.CommandName == "Delete") {
                    rowToDelete = e.Item.ItemIndex;
                    if (rowToDelete >= 0) {
                        dsDetailedPrincipals.Tables[0].Rows[rowToDelete].Delete();
                        if (boardMemberCIID > 0) {
                            if (!myFact.DeletePrincipal(boardMemberCIID, CIID, jobTitleID)) {
                                DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
                            }
                        }
                        dsDetailedPrincipals = myFact.GetDetailedPrincipalsAsDataSet(CIID);
                        Session["dsDetailedPrincipals"] = dsDetailedPrincipals;
                        BindPrincipalsGrid();
                    }
                }
                /*
				if(e.CommandName == "Update") 
				{
					// do some fancy stuff
					this.tbFirstName.Text = dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["FirstNameNative"].ToString();
					this.tbSurname.Text = dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["SurNameNative"].ToString();
					this.tbPersonalID.Text = dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["Number"].ToString();
					this.ddManagementPosition.SelectedIndex = Convert.ToInt32(dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["ManagementPositionID"].ToString());
					this.tbAddress.Text = dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["StreetNative"].ToString();
					this.btnReg.Text = "Update";
					this.btnReg.CommandName = "Update";
					this.tbPersonalID.ReadOnly = true;
				}
				*/
            } catch (Exception err) {
                Logger.WriteToLog("RegBoarMember.aspx : dtgrBoardMOverview_ItemCommand " + err.Message, true);
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }

        private void LocalizeText() {
            lblCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblPrincipals.Text = rm.GetString("txtPrincipals", ci);
            lblFirstName.Text = rm.GetString("txtFirstName", ci);
            lblSurname.Text = rm.GetString("txtSurName", ci);
            lblPersonalID.Text = rm.GetString("txtPersonalID", ci);
            lblAddress.Text = rm.GetString("txtAddress", ci);
            lblAreaCode.Text = rm.GetString("txtAreaCode", ci);
            lblJobtitle.Text = rm.GetString("txtJobTitle", ci);
            lblEducation.Text = rm.GetString("txtEducation", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            RequiredFieldValidator1.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator4.ErrorMessage = rm.GetString("txtValueMissing", ci);
            btnReg.Text = rm.GetString("txtSave", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            btnCancel.Text = rm.GetString("txtBack", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);
            btnRegHistory.Text = rm.GetString("txtSaveHistory", ci);
            lblHistoryEN.Text = rm.GetString("txtHistoryEnglish", ci);
            lblHistoryNative.Text = rm.GetString("txtHistoryNative", ci);

            lblNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblSubHeader.Text = rm.GetString("txtAdditionalInfo", ci);
            lblPrincipalsDatagridHeader.Text = rm.GetString("txtPrincipals", ci);
        }

        private void SetGridHeaders() {
            dtgrPrincipals.Columns[4].HeaderText = rm.GetString("txtName", ci);
            dtgrPrincipals.Columns[3].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrPrincipals.Columns[9].HeaderText = rm.GetString("txtTitle", ci);
            dtgrPrincipals.Columns[12].HeaderText = rm.GetString("txtEducation", ci);
            dtgrPrincipals.Columns[2].HeaderText = rm.GetString("txtCreditInfoID", ci);
        }

        /// <summary>
        /// Localizes the search grid header
        /// </summary>
        private void LocalizeNameSearchGridHeader() {
            dtgrNameSearch.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrNameSearch.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrNameSearch.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrNameSearch.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dtgrNameSearch.Columns[9].HeaderText = rm.GetString("txtCity", ci);
        }

        private void btnCancel_Click(object sender, EventArgs e) { TransferToPreviousPage(); }

        private void AddEnterEvent() {
            tbFirstName.Attributes.Add("onkeypress", "checkEnterKey();");
            tbSurname.Attributes.Add("onkeypress", "checkEnterKey();");
            tbNationalID.Attributes.Add("onkeypress", "checkEnterKey();");
            tbAddress.Attributes.Add("onkeypress", "checkEnterKey();");
            ddlAreaCode.Attributes.Add("onkeypress", "checkEnterKey();");
            ddJobtitle.Attributes.Add("onkeypress", "checkEnterKey();");
            ddEducation.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        /// <summary>
        /// Searches for given name in the national name database
        /// </summary>
        /// <param name="name">The name or part of name to search for</param>
        public void SearchForNames(string ciid, string nationalID, string name, string surName) {
            uaFactory uaf = new uaFactory();

            DataSet dsNameSearch;
            //Person
            Debtor debtor = new Debtor();

            if (name.Trim() != "") {
                debtor.FirstName = name;
            }
            if (surName.Trim() != "") {
                debtor.SurName = surName;
            }
            if (ciid.Trim() != "") {
                debtor.CreditInfo = int.Parse(ciid);
            }
            if (nationalID.Trim() != "") {
                debtor.IDNumber1 = nationalID;
                debtor.IDNumber2Type = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
            }
            if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                dsNameSearch = uaf.FindCustomerInNationalAndCreditInfo(debtor);
                // if not found in national registry (f.ex. the Cyprus Whitelist has incomplete data) then search in our db
                if (dsNameSearch.Tables.Count > 0) {
                    if (dsNameSearch.Tables[0] == null || dsNameSearch.Tables[0].Rows == null ||
                        dsNameSearch.Tables[0].Rows.Count <= 0) {
                        dsNameSearch = uaf.FindCustomer(debtor);
                    }
                } else {
                    dsNameSearch = uaf.FindCustomer(debtor);
                }
            } else {
                dsNameSearch = uaf.FindCustomer(debtor);
            }

            if (dsNameSearch.Tables[0] != null && dsNameSearch.Tables.Count > 0 && dsNameSearch.Tables[0].Rows != null &&
                dsNameSearch.Tables[0].Rows.Count > 0) {
                dtgrNameSearch.DataSource = dsNameSearch;
                dtgrNameSearch.DataBind();

                int gridRows = dtgrNameSearch.Items.Count;
                if (gridRows <= 6) {
                    divNameSearch.Style["HEIGHT"] = "auto";
                } else {
                    divNameSearch.Style["HEIGHT"] = "164px";
                }
                divNameSearch.Style["OVERFLOW"] = "auto";
                divNameSearch.Style["OVERFLOW-X"] = "auto";
                divNameSearch.Visible = true;

                outerNameSearchGridTable.Visible = true;
            } else {
                DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                outerNameSearchGridTable.Visible = false;
            }
        }

        private void dtgrNameSearch_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }

                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }

                    //Set city - Native if available, else EN
                    if (e.Item.Cells[10].Text.Trim() != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }

                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }

                    //Set city - Native if available, else EN
                    if (e.Item.Cells[11].Text.Trim() != "" && e.Item.Cells[11].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);	
            }

            WebDesign.CreateExplanationIcons(dtgrNameSearch.Columns, lblNameSearchDatagridIcons, rm, ci);
        }

        private void dtgrNameSearch_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                if (e.Item.Cells[1].Text == "&nbsp;") {
                    txtCIID.Text = "";
                    RequiredFieldValidator4.Enabled = true;
                } else {
                    txtCIID.Text = e.Item.Cells[1].Text;
                    RequiredFieldValidator4.Enabled = false;
                }

                tbNationalID.Text = e.Item.Cells[2].Text;
                if (tbNationalID.Text == "&nbsp;") {
                    tbNationalID.Text = "";
                }

                tbAddress.Text = e.Item.Cells[6].Text;
                if (tbAddress.Text == "&nbsp;") {
                    tbAddress.Text = "";
                }
                /*if(txtCIID.Text=="&nbsp;")
					txtCIID.Text="";*/

                string name = e.Item.Cells[3].Text.Trim();
                if (name == "&nbsp;") {
                    name = "";
                }
                string[] arrName = name.Split(' ');
                if (arrName.Length > 0) {
                    if (arrName.Length == 1) {
                        tbFirstName.Text = arrName[0];
                    } else {
                        tbSurname.Text = arrName[arrName.Length - 1];
                        tbFirstName.Text = name.Substring(0, name.Length - (tbSurname.Text.Length + 1));
                    }
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e) { SearchForNames(txtCIID.Text, tbNationalID.Text, tbFirstName.Text, tbSurname.Text); }

        private void btnRegHistory_Click(object sender, EventArgs e) {
            HistoryBLLC theHistory = new HistoryBLLC();
            bool historyError = false;
            // take the history
            theHistory.HistoryNative = tbHistoryNative.Text;
            theHistory.HistoryEN = tbHistoryEN.Text;
            theHistory.HistoryID = Convert.ToInt32(CigConfig.Configure("lookupsettings.DirectorHistoryType"));
            if (!myFact.AddHistory(CIID, theHistory)) {
                historyError = true;
            }

            if (historyError) {
                DisplayErrorMessage(rm.GetString("txtSavingHistoryFailed", ci));
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.dtgrNameSearch.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrNameSearch_ItemCommand);
            this.dtgrNameSearch.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrNameSearch_ItemDataBound);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnRegHistory.Click += new System.EventHandler(this.btnRegHistory_Click);
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            this.dtgrPrincipals.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrPrincipals_ItemCommand);
            this.dtgrPrincipals.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrPrincipals_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}