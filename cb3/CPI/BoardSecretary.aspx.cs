#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using Image=System.Web.UI.WebControls.Image;

#endregion

using Cig.Framework.Base.Configuration;

#pragma warning disable 618,612

namespace CPI {
    /// <summary>
    /// Summary description for Secretary.
    /// </summary>
    public class BoardSecretary : BaseCPIPage {
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnLoadFromCompanyRegistry;
        protected Button btnReg;
        protected Button btnSearch;
        private int CIID = -1;
        protected DataGrid dgRegisteredCompanySecretary;
        protected HtmlGenericControl divNameSearch;
        protected DataGrid dtgrNameSearch;
        protected CPIFactory factory = new CPIFactory();
        protected Image imgCompany;
        protected Image imgIndividual;
        protected Label lblCIID;
        protected Label lblFirstName;
        protected Label lblHistoryEnglish;
        protected Label lblHistoryNative;
        protected Label lblMsg;
        protected Label lblNameSearchDatagridHeader;
        protected Label lblNameSearchDatagridIcons;
        protected Label lblNationalID;
        protected Label lblPageStep;
        protected Label lblRegisteredCompanySecretaryDatagridHeader;
        protected Label lblRegisteredCompanySecretaryDatagridIcons;
        protected Label lblSecretary;
        protected Label lblSubHeader;
        protected Label lblSurname;
        protected HtmlTable outerGridTable;
        protected HtmlTable outerNameSearchGridTable;
        protected RadioButton rbtnCompany;
        protected RadioButton rbtnIndividual;
        protected RequiredFieldValidator rfvFirstName;
        protected RequiredFieldValidator rfvNationalID;
        protected HtmlTableCell tdNameSearchGrid;
        protected TextBox txtCIID;
        protected TextBox txtFirstname;
        protected TextBox txtHistoryEnglish;
        protected TextBox txtHistoryNative;
        protected TextBox txtNationalID;
        protected TextBox txtSurname;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "BoardSecretary";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new"); 
                //Server.Transfer("SearchForReport.aspx?redirect=BoardSecretary&new=true");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            /*if(Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
				Server.Transfer("BasicInformation.aspx?action=update");*/

            PreparePageLoad();

            string toLoadFromCompanyRegistry =
                CigConfig.Configure("lookupsettings.EnableToLoadBoardSecretaryFromCompanyRegistry");
            if (toLoadFromCompanyRegistry != null && toLoadFromCompanyRegistry.ToLower().Equals("true")) {
                btnLoadFromCompanyRegistry.Visible = true;
            } else {
                btnLoadFromCompanyRegistry.Visible = false;
            }

            if (!Page.IsPostBack) {
                outerNameSearchGridTable.Visible = false;

                LocalizeText();
                LoadSecretaryData(CIID);
                tdNameSearchGrid.Visible = false;
                rfvNationalID.Enabled = true;
            }
        }

        /// <summary>
        /// Loads secretary data from the database
        /// </summary>
        /// <param name="ciid">The ciid of the company to work with</param>
        protected void LoadSecretaryData(int ciid) {
            if (ciid > 0) {
                DataSet dsBoardSecretary = factory.GetBoardSecretaryAsDataSet(ciid);
                dgRegisteredCompanySecretary.DataSource = dsBoardSecretary;
                dgRegisteredCompanySecretary.DataBind();
            }
        }

        /// <summary>
        /// Localizes text of the webform
        /// </summary>
        private void LocalizeText() {
            lblCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblFirstName.Text = rm.GetString("txtFirstName", ci);
            lblHistoryEnglish.Text = rm.GetString("txtHistoryEnglish", ci);
            lblHistoryNative.Text = rm.GetString("txtHistoryNative", ci);
            lblNationalID.Text = rm.GetString("txtNationalID", ci);
            lblSecretary.Text = rm.GetString("txtBoardSecretary", ci);
            lblSurname.Text = rm.GetString("txtSurName", ci);
            btnCancel.Text = rm.GetString("txtBack", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            btnReg.Text = rm.GetString("txtSave", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);
            rfvFirstName.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvNationalID.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            LocalizeGridHeader();
            LocalizeBoardSecretaryGridHeader();
            btnLoadFromCompanyRegistry.Text = rm.GetString("txtLoadBoardFromCompanyRegistry", ci);

            lblNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblSubHeader.Text = rm.GetString("txtAdditionalInfo", ci);
            //	lblSubHeader.Text = rm.GetString("txtBoardSecretary", ci);
            lblRegisteredCompanySecretaryDatagridHeader.Text = rm.GetString("txtBoardSecretary", ci);

            imgIndividual.AlternateText = rm.GetString("txtPerson", ci);
            imgCompany.AlternateText = rm.GetString("txtCompany", ci);
        }

        /// <summary>
        /// Localizes the search grid header
        /// </summary>
        private void LocalizeGridHeader() {
            dtgrNameSearch.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrNameSearch.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrNameSearch.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrNameSearch.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dtgrNameSearch.Columns[9].HeaderText = rm.GetString("txtCity", ci);
        }

        private static void HidedgRegisteredCompanySecretaryColumns() {
            /*
			if(nativeCult)
			{
				this.dgRegisteredCompanySecretary.Columns[5].Visible = true;
				this.dgRegisteredCompanySecretary.Columns[4].Visible = true;
				this.dgRegisteredCompanySecretary.Columns[6].Visible = false;
				this.dgRegisteredCompanySecretary.Columns[7].Visible = false;
			}
			else
			{
				this.dgRegisteredCompanySecretary.Columns[5].Visible = false;
				this.dgRegisteredCompanySecretary.Columns[4].Visible = false;
				this.dgRegisteredCompanySecretary.Columns[6].Visible = true;
				this.dgRegisteredCompanySecretary.Columns[7].Visible = true;
			}
		*/
        }

        /// <summary>
        /// Searches for given name in the national name database
        /// </summary>
        /// <param name="nationalID"></param>
        /// <param name="name">The name or part of name to search for</param>
        /// <param name="ciid"></param>
        /// <param name="surName"></param>
        public void SearchForNames(string ciid, string nationalID, string name, string surName) {
            var uaf = new uaFactory();
            DataSet dsNameSearch;
            if (rbtnIndividual.Checked) {
                // boxes/labels visibility
                lblSurname.Visible = true;
                txtSurname.Visible = true;

                var debtor = new Debtor();

                if (name.Trim() != null) {
                    debtor.FirstName = name;
                }
                if (surName.Trim() != null) {
                    debtor.SurName = surName;
                }
                if (ciid.Trim() != "") {
                    debtor.CreditInfo = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    debtor.IDNumber1 = nationalID;
                    debtor.IDNumber2Type = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                }
                // hmm � �etta vi� um einstaklinga
                //		if(CigConfig.Configure("lookupsettings.CompanyNationalRegistry"] == "True")
                //			dsNameSearch= uaf.FindCustomerInNationalAndCreditInfo(debtor);
                //		else
//					dsNameSearch= uaf.FindCustomer(debtor); // �essi l�na var ein sj�anleg

                if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                    dsNameSearch = uaf.FindCustomerInNationalAndCreditInfo(debtor);
                    // if not found in national registry (f.ex. the Cyprus Whitelist is incomplete data) then search in our db
                    if (dsNameSearch.Tables.Count > 0) {
                        if (dsNameSearch.Tables[0] == null || dsNameSearch.Tables[0].Rows == null ||
                            dsNameSearch.Tables[0].Rows.Count <= 0) {
                            dsNameSearch = uaf.FindCustomer(debtor);
                        }
                    } else {
                        dsNameSearch = uaf.FindCustomer(debtor);
                    }
                } else {
                    dsNameSearch = uaf.FindCustomer(debtor);
                }
            } else {
                //Company				
                // boxes 
                lblSurname.Visible = false;
                txtSurname.Visible = false;
                var company = new Company();

                if (name.Trim() != null) {
                    company.NameNative = name;
                }
                if (ciid.Trim() != "") {
                    company.CreditInfoID = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    company.NationalID = nationalID;
                }
                /*{
					IDNumber idn = new IDNumber();
					idn.Number = nationalID;
					idn.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"]); 
					company.IDNumbers = new ArrayList();
					company.IDNumbers.Add(idn);
				}	*/
                dsNameSearch = CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True" ? uaf.FindCompanyInNationalAndCreditInfo(company) : uaf.FindCompany(company);
            }

            if (dsNameSearch.Tables.Count > 0 && dsNameSearch.Tables[0] != null) {
                dtgrNameSearch.DataSource = dsNameSearch;
                dtgrNameSearch.DataBind();
                if (dtgrNameSearch.Items.Count == 1) // only one found set the values directly to text boxes
                {
                    OneItemFound(dsNameSearch);
                    return;
                }
                tdNameSearchGrid.Visible = true;
                outerNameSearchGridTable.Visible = true;

                int gridRows = dtgrNameSearch.Items.Count;
                divNameSearch.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                divNameSearch.Style["OVERFLOW"] = "auto";
                divNameSearch.Style["OVERFLOW-X"] = "auto";
            } else {
                tdNameSearchGrid.Visible = false;
            }
        }

        public void OneItemFound(DataSet dsNameSearch) {
            string name;
            txtCIID.Text = dsNameSearch.Tables[0].Rows[0]["CreditInfoID"].ToString();
            txtNationalID.Text = dsNameSearch.Tables[0].Rows[0]["Number"].ToString();
            rfvNationalID.Enabled = false;

            if (nativeCult) {
                // oDS.Tables[0].rows[0].Items["CLIENT_CITY"].toString()
                name = dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString().Trim();
                if (rbtnIndividual.Checked) // person
                {
                    // �arf a� splitta upp name og surname kemur � einum streng

                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            txtFirstname.Text = arrName[0];
                        } else {
                            txtSurname.Text = arrName[arrName.Length - 1];
                            txtFirstname.Text = name.Substring(0, name.Length - (txtSurname.Text.Length + 1));
                        }
                    }
                    //	this.txtFirstname.Text = dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString();
                    //	this.txtSurname.Text = dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString();
                } else {
                    txtFirstname.Text = name.Trim(); // dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString();
                }
            } else {
                name = dsNameSearch.Tables[0].Rows[0]["NameEN"].ToString().Trim();
                if (name == "") // display native rather than nothing
                {
                    name = dsNameSearch.Tables[0].Rows[0]["NameNative"].ToString();
                }

                if (rbtnIndividual.Checked) // person
                {
                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            txtFirstname.Text = arrName[0];
                        } else {
                            txtSurname.Text = arrName[arrName.Length - 1];
                            txtFirstname.Text = name.Substring(0, name.Length - (txtSurname.Text.Length + 1));
                        }
                    }
                } else {
                    txtFirstname.Text = name.Trim();
                }
            }
        }

        /// <summary>
        /// Retrives debtor informations from page
        /// </summary>
        /// <returns>Debtor informations</returns>
        /*private Debtor GetDebtorInfoFromPage()
		{
			Debtor myDebtor = new Debtor();
			try
			{
				if (this.txtCIID.Text != "")
					myDebtor.CreditInfo = int.Parse(this.txtCIID.Text);
			}
			catch{}
			myDebtor.IDNumber1 = this.txtNationalID.Text;
			myDebtor.IDNumber2Type = int.Parse(CigConfig.Configure("lookupsettings.nationalID"]); 
			myDebtor.SurName = this.txtSurname.Text;
			myDebtor.FirstName = this.txtFirstname.Text;
			return myDebtor;
		}*/
/*
		private void lbtnNameSearch_Click(object sender, System.EventArgs e)
		{
			if(txtFirstname.Text.Trim().Length>0||txtSurname.Text.Trim().Length>0)
				SearchForNames("", "", txtFirstname.Text, txtSurname.Text);		
		}
		
		private void lbtnSearch_Click(object sender, System.EventArgs e)
		{
			if(txtNationalID.Text.Trim().Length>0)
				SearchForNames("", txtNationalID.Text, "", "");				
		}

		private void lbtnCIIDSearch_Click(object sender, System.EventArgs e)
		{
			if(txtCIID.Text.Trim().Length>0)
				SearchForNames(txtCIID.Text, "", "", "");			
		}
*/

        private void dtgrNameSearch_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }

                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }

                    //Set city - Native if available, else EN
                    if (e.Item.Cells[10].Text.Trim() != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }

                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }

                    //Set city - Native if available, else EN
                    if (e.Item.Cells[11].Text.Trim() != "" && e.Item.Cells[11].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    }
                }
            }

            WebDesign.CreateExplanationIcons(dtgrNameSearch.Columns, lblNameSearchDatagridIcons, rm, ci);
        }

        private void btnReg_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                RegBoardSecretary(
                    rbtnCompany.Checked,
                    txtCIID.Text,
                    txtNationalID.Text,
                    txtFirstname.Text,
                    txtSurname.Text,
                    txtHistoryNative.Text,
                    txtHistoryEnglish.Text);
            }
        }

        private void DisplayMessage(String message) {
            lblMsg.Text = message;
            lblMsg.ForeColor = Color.Blue;
            lblMsg.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblMsg.Text = errorMessage;
            lblMsg.ForeColor = Color.Red;
            lblMsg.Visible = true;
        }

        private void ClearBoxes() {
            txtCIID.Text = "";
            txtFirstname.Text = "";
            txtNationalID.Text = "";
            txtSurname.Text = "";
            rfvNationalID.Enabled = true;
        }

        private void dtgrNameSearch_ItemCommand(object source, DataGridCommandEventArgs e) {
            // 1 == CreditInfoID
            // 2 == National ID
            // 3 == Name
            // 4 == NameNative
            // 5 == NameEN
            // 6 == Address
            // 7 == AddressNative
            // 8 == AddressEN
            // 9 == city
            // 10 == CityNative
            // 11 == CityEN
            // 12 == IsCompany

            if (!e.CommandName.Equals("Select")) {
                return;
            }
            txtCIID.Text = e.Item.Cells[1].Text;
            txtNationalID.Text = e.Item.Cells[2].Text;
            if (txtNationalID.Text == "&nbsp;") {
                txtNationalID.Text = "";
            }
            if (txtCIID.Text == "&nbsp;") {
                txtCIID.Text = "";
                rfvNationalID.Enabled = true;
            } else {
                rfvNationalID.Enabled = false;
            }

            if (e.Item.Cells[12].Text.Trim().Equals("1")) // that isCompany
            {
                txtFirstname.Text = e.Item.Cells[3].Text.Trim();
                lblFirstName.Text = rm.GetString("txtName", ci);
                lblSurname.Visible = false;
                txtSurname.Visible = false;
            } else // person ...
            {
                lblSurname.Visible = true;
                txtSurname.Visible = true;
                lblFirstName.Text = rm.GetString("txtFirstName", ci);

                string name = e.Item.Cells[3].Text.Trim();
                if (name == "&nbsp;") {
                    name = "";
                }
                string[] arrName = name.Split(' ');
                if (arrName.Length > 0) {
                    if (arrName.Length == 1) {
                        txtFirstname.Text = arrName[0];
                    } else {
                        txtSurname.Text = arrName[arrName.Length - 1];
                        txtFirstname.Text = name.Substring(0, name.Length - (txtSurname.Text.Length + 1));
                    }
                }
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }
        private void btnCancel_Click(object sender, EventArgs e) { TransferToPreviousPage(); }

        private void dgRegisteredCompanySecretary_ItemCommand(object source, DataGridCommandEventArgs e) {
            // 2 == CreditInfoID
            // 3 == NationalID
            // 4 == Name
            // 5 == FirstNameNative
            // 6 == SurNameNative
            // 7 == FirstNameEN
            // 8 == SurNameEN
            // 9 == HistoryNative
            // 10 == HistoryEN
            // 11 == NameEN
            // 12 == NameNative
            // 13 == Type

            if (e.CommandName.Equals("update")) {
                if (e.Item.Cells[13].Text.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                    rbtnIndividual.Checked = false;
                    rbtnCompany.Checked = true;
                    lblFirstName.Text = rm.GetString("txtName", ci);
                    lblSurname.Visible = false;
                    txtSurname.Visible = false;

                    if (e.Item.Cells[4].Text.Trim() != "&nbsp;") {
                        txtFirstname.Text = e.Item.Cells[4].Text.Trim();
                    }
                } else {
                    rbtnCompany.Checked = false;
                    rbtnIndividual.Checked = true;
                    lblFirstName.Text = rm.GetString("txtFirstName", ci);
                    lblSurname.Visible = true;
                    txtSurname.Visible = true;

                    if (nativeCult) {
                        if (e.Item.Cells[5].Text.Trim() != "&nbsp;") {
                            txtFirstname.Text = e.Item.Cells[5].Text.Trim();
                        }
                        if (e.Item.Cells[6].Text.Trim() != "&nbsp;") {
                            txtSurname.Text = e.Item.Cells[6].Text.Trim();
                        }
                    } else {
                        if (e.Item.Cells[7].Text.Trim() != "&nbsp;") {
                            txtFirstname.Text = e.Item.Cells[7].Text.Trim();
                        }
                        if (e.Item.Cells[8].Text.Trim() != "&nbsp;") {
                            txtSurname.Text = e.Item.Cells[8].Text.Trim();
                        }
                    }
                }

                if (e.Item.Cells[2].Text.Trim() != "&nbsp;") {
                    txtCIID.Text = e.Item.Cells[2].Text.Trim();
                }
                if (e.Item.Cells[3].Text.Trim() != "&nbsp;") {
                    txtNationalID.Text = e.Item.Cells[3].Text.Trim();
                }

                if (e.Item.Cells[9].Text.Trim() != "&nbsp;") {
                    txtHistoryNative.Text = e.Item.Cells[9].Text.Trim();
                }
                if (e.Item.Cells[10].Text.Trim() != "&nbsp;") {
                    txtHistoryEnglish.Text = e.Item.Cells[10].Text.Trim();
                }
            } else if (e.CommandName.Equals("delete")) {
                int laywerCIID;
                try {
                    laywerCIID = int.Parse(e.Item.Cells[2].Text.Trim());
                } catch (Exception) {
                    laywerCIID = -1;
                }
                if (laywerCIID > -1) {
                    if (factory.DeleteBoardSecretary(CIID)) {
                        DisplayMessage("Secretary deleted");
                        ClearBoxes();
                        LoadSecretaryData(CIID);
                    } else {
                        DisplayErrorMessage("Error deleting secretary");
                    }
                }
            }
        }

        private void dgRegisteredCompanySecretary_ItemDataBound(object sender, DataGridItemEventArgs e) {
            // 2 == CreditInfoID
            // 3 == NationalID
            // 4 == Name
            // 5 == FirstNameNative
            // 6 == SurNameNative
            // 7 == FirstNameEN
            // 8 == SurNameEN
            // 9 == HistoryNative
            // 10 == HistoryEN
            // 11 == NameEN
            // 12 == NameNative
            // 13 == Type
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";

                if (nativeCult) {
                    //Set Name - Native if available, else 
                    if (e.Item.Cells[13].Text.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                        if (e.Item.Cells[12].Text.Trim() != "" && e.Item.Cells[12].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[12].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[11].Text;
                        }
                    } else {
                        if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[5].Text + " " + e.Item.Cells[6].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[7].Text + " " + e.Item.Cells[8].Text;
                        }
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[13].Text.Trim().Equals(CigConfig.Configure("lookupsettings.companyID"))) {
                        if (e.Item.Cells[11].Text.Trim() != "" && e.Item.Cells[11].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[11].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[12].Text;
                        }
                    } else {
                        if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[7].Text + " " + e.Item.Cells[8].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[5].Text + " " + e.Item.Cells[6].Text;
                        }
                    }
                }
            }
            WebDesign.CreateExplanationIcons(
                dgRegisteredCompanySecretary.Columns, lblRegisteredCompanySecretaryDatagridIcons, rm, ci);
        }

        /// <summary>
        /// Localizes the laywers grid header
        /// </summary>
        private void LocalizeBoardSecretaryGridHeader() {
            dgRegisteredCompanySecretary.Columns[2].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dgRegisteredCompanySecretary.Columns[3].HeaderText = rm.GetString("txtNationalID", ci);
            dgRegisteredCompanySecretary.Columns[4].HeaderText = rm.GetString("txtName", ci);
            HidedgRegisteredCompanySecretaryColumns();
        }

        private void btnSearch_Click(object sender, EventArgs e) { SearchForNames(txtCIID.Text, txtNationalID.Text, txtFirstname.Text, txtSurname.Text); }

        private void rbtnIndividual_CheckedChanged(object sender, EventArgs e) {
            ClearBoxes();
            if (rbtnIndividual.Checked) {
                //Person
                rbtnCompany.Checked = false; // some VS bug! Events do not work unless this is done!
                lblFirstName.Text = rm.GetString("txtFirstName", ci);
                lblSurname.Visible = true;
                txtSurname.Visible = true;
            } else {
                //Company
                rbtnIndividual.Checked = false; // some VS bug! Events do not work unless this is done!
                lblFirstName.Text = rm.GetString("txtCompany", ci);
                lblSurname.Visible = false;
                txtSurname.Visible = false;
            }
        }

        private void RegBoardSecretary(
            bool isCompany,
            string ciid,
            string nationalID,
            string firstName,
            string surName,
            string histNat,
            string histEN) {
            bool insertOK;

            try {
                if (!isCompany) // indivitual
                {
                    var bSecretary = new BoardSecretaryBLLC();

                    if (nativeCult) {
                        bSecretary.FirstNameNative = firstName;
                        bSecretary.SurNameNative = surName;
                    } else {
                        bSecretary.FirstNameEN = firstName;
                        bSecretary.SurNameEN = surName;
                    }
                    if (ciid != "") {
                        bSecretary.CreditInfoID = int.Parse(ciid);
                    }
                    bSecretary.NationalID = nationalID;
                    bSecretary.HistoryNative = histNat;
                    bSecretary.HistoryEN = histEN;

                    insertOK = factory.AddBoardSecretary(bSecretary, CIID);
                } else {
                    var bSecretaryCompany = new BoardSecretaryCompanyBLLC();

                    if (nativeCult) {
                        bSecretaryCompany.NameNative = firstName;
                    } else {
                        bSecretaryCompany.NameEN = firstName;
                    }
                    if (ciid != "") {
                        bSecretaryCompany.CreditInfoID = int.Parse(ciid);
                    }
                    bSecretaryCompany.NationalID = nationalID;
                    bSecretaryCompany.HistoryNative = histNat;
                    bSecretaryCompany.HistoryEN = histEN;

                    insertOK = factory.AddBoardSecretaryCompany(bSecretaryCompany, CIID);
                }
            } catch (Exception err) {
                Logger.WriteToLog("Error occurred while inserting/updating board secretary", err, true);
                insertOK = false;
            }
            if (insertOK) {
                DisplayMessage(rm.GetString("txtTheInfoHasBeenRegisted", ci));
                ClearBoxes();
                LoadSecretaryData(CIID);
                tdNameSearchGrid.Visible = false;
            }
        }

        private void btnLoadFromCompanyRegistry_Click(object sender, EventArgs e) {
            string nationalId = factory.GetNationalIDByCIID(CIID);
            if (nationalId != null) {
                DataSet ds = factory.GetCompanyBoardSecretaryFromCompanyRegistryAsDataSet(nationalId);
                if (ds.Tables.Count > 0 && ds.Tables[0] != null) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        //Get information about board member from CompanyRegistry
                        string firstName = "";
                        string surName = "";
                        string ssn = ds.Tables[0].Rows[i]["PersonSSN"].ToString();
                        string uName = ds.Tables[0].Rows[i]["Name"].ToString();
                        string[] arrName = uName.Split(' ');
                        if (arrName.Length > 0) {
                            if (arrName.Length == 1) {
                                firstName = arrName[0];
                            } else {
                                surName = arrName[arrName.Length - 1];
                                firstName = uName.Substring(0, uName.Length - (surName.Length + 1));
                            }
                        }
                        //string address = ds.Tables[0].Rows[i]["Address"].ToString();
                        //string pcode = ds.Tables[0].Rows[i]["PostalCode"].ToString();
                        string isCompany = ds.Tables[0].Rows[i]["IsCompany"].ToString();
                        if (isCompany.Trim() == "1") {
                            RegBoardSecretary(true, "-1", ssn, firstName, surName, "", "");
                        } else {
                            RegBoardSecretary(false, "-1", ssn, firstName, surName, "", "");
                        }
                    }
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rbtnIndividual.CheckedChanged += new System.EventHandler(this.rbtnIndividual_CheckedChanged);
            this.rbtnCompany.CheckedChanged += new System.EventHandler(this.rbtnIndividual_CheckedChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.dtgrNameSearch.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrNameSearch_ItemCommand);
            this.dtgrNameSearch.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrNameSearch_ItemDataBound);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnLoadFromCompanyRegistry.Click += new System.EventHandler(this.btnLoadFromCompanyRegistry_Click);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            this.dgRegisteredCompanySecretary.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgRegisteredCompanySecretary_ItemCommand);
            this.dgRegisteredCompanySecretary.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgRegisteredCompanySecretary_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}

#pragma warning restore 618,612