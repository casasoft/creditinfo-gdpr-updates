<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Page language="c#" Codebehind="Charges.aspx.cs" AutoEventWireup="false" Inherits="CPI.Charges" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>Charges</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
  </head>
	<body>
		<form id="Capital" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblCharges" runat="server">Charges</asp:label>
																	</td>
																	<td align="right">
																		<asp:label id="lblPageStep" runat="server">Step</asp:label>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td colspan=2 style="WIDTH: 33%">
																		<asp:label id="lblDateRegistered" runat="server">Date registered</asp:label>
																		<br>
																		<asp:textbox id="txtDateRegistered" runat="server" maxlength="10"></asp:textbox>&nbsp;
																		<input onclick="PopupPicker('txtDateRegistered', 250, 250);" type="button" value="..." class=popup>
																		<asp:customvalidator id="cvDateRegistered" runat="server" errormessage="Error" controltovalidate="txtDateRegistered">*</asp:customvalidator>
																	</td>
																	<td colspan=2 style="WIDTH: 33%">
																		<asp:label id="lblDatePrepared" runat="server">Date prepared</asp:label>
																		<br>
																		<asp:textbox id="txtDatePrepared" runat="server" maxlength="10"></asp:textbox>&nbsp;
																		<input onclick="PopupPicker('txtDatePrepared', 250, 250);" type="button" value="..." class=popup>
																		<asp:customvalidator id="cvDatePrepared" runat="server" errormessage="Error" controltovalidate="txtDatePrepared">*</asp:customvalidator>
																		<asp:textbox id="txtID" runat="server" width="8px" visible="False"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td colspan=2>
																		<asp:label id="lblSequence" runat="server">Sequence</asp:label>
																		<br>
																		<asp:textbox id="txtSequence" runat="server" maxlength="50"></asp:textbox>
																	</td>
																	<td colspan=2>
																		<asp:label id="lblAmount" runat="server">Amount</asp:label>
																		<br>
																		<asp:textbox id="txtAmount" runat="server" maxlength="13"></asp:textbox>
																		<asp:customvalidator id="cvAmount" runat="server" errormessage="Error" controltovalidate="txtAmount">*</asp:customvalidator>
																	</td>
																	<td colspan=2>
																		<asp:label id="lblCurrency" runat="server">Currency</asp:label>
																		<br>
																		<asp:dropdownlist id="ddlstCurrency" runat="server"></asp:dropdownlist>
																	</td>																																		
																</tr>
																<tr>
																	<td colspan=3>
																		<asp:label id="lblDescription" runat="server">Description/No</asp:label>
																		<br>
																		<asp:dropdownlist id="ddlstDescription" runat="server" cssclass="long_input"></asp:dropdownlist>
																	</td>																
																	<td colspan=3>
																		<asp:label id="lblBenificiary" runat="server">Benificiary</asp:label>
																		<br>
																		<asp:dropdownlist id="ddlstBeneficiary" runat="server" cssclass="long_input"></asp:dropdownlist>
																	</td>
																</tr>
																<tr>
																	<td colspan=3>
																		<asp:label id="lblFreeDescriptionNative" runat="server">Description native</asp:label>
																		<br>
																		<asp:textbox id="tbFreeDescriptionNative" runat="server" width="360px" maxlength="500" textmode="MultiLine"
																			height="100px"></asp:textbox>
																		<asp:customvalidator id="Customvalidator1" runat="server" errormessage="Error" controltovalidate="txtAmount">*</asp:customvalidator>
																	</td>
																	<td colspan=3>
																		<asp:label id="lblFreeDescriptionEN" runat="server">Description en</asp:label>
																		<br>
																		<asp:textbox id="tbFreeDescriptionEN" runat="server" width="360px" maxlength="500" textmode="MultiLine"
																			height="100px"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
															<asp:label id="lblErrorMessage" runat="server" cssclass="error_text">Error</asp:label>
															<asp:validationsummary id="ValidationSummary" runat="server"></asp:validationsummary>
														</td>
														<td align="right">
															<asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" text="Continue"></asp:button><asp:button id="btnReg" runat="server" cssclass="confirm_button" text="Register"></asp:button>															
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
													<tr>
														<td height="10"></td>
													</tr>												
													<tr>
														<td align="right">
															<asp:label id="lblDatagridIcons" runat="server"></asp:label>
														</td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblDatagridHeader" runat="server"></asp:label>
														</th>
													</tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:datagrid id="dgCharges" runat="server" autogeneratecolumns="False" gridlines="None" cssclass="grid">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;/&gt;" commandname="update">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;/&gt;" commandname="delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:boundcolumn visible="False" datafield="ID" headertext="ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="DateRegistered" headertext="DateRegistered" dataformatstring="{0:d}">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="DatePrepared" headertext="DatePrepared" dataformatstring="{0:d}">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="DescriptionID" headertext="DescriptionID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="DescriptionNative" headertext="DescriptionNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="DescriptionEN" headertext="DescriptionEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="BankID" headertext="BeneficiaryID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameNative" headertext="BeneficiaryNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameEN" headertext="BeneficiaryEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Amount" headertext="Amount">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="CurrencyCode" headertext="Currency">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Sequence" headertext="Sequence">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="DescriptionFreeTextNative" headertext="DescriptionFreeTextNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="DescriptionFreeTextEN" headertext="DescriptionFreeTextEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																		</asp:datagrid>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
