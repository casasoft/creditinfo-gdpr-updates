#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using Image=System.Web.UI.WebControls.Image;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for ShareholdersOwners.
    /// </summary>
    public class ShareholdersOwners : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnLoadFromCompanyRegistry;
        protected Button btnReg;
        protected Button btnSearch;
        private int CIID = 1;
        protected CustomValidator CustomValidator1;
        protected DropDownList ddlOwnershipType;
        protected HtmlGenericControl divNameSearch;
        private DataSet dsShareHolders = new DataSet();
        protected DataGrid dtgrNameSearch;
        protected DataGrid dtgrShareholders;
        protected Image imgCompany;
        protected Image imgIndividual;
        protected Label lblCIID;
        protected Label lblErrMsg;
        protected Label lblFirstName;
        protected Label lblNameSearchDatagridHeader;
        protected Label lblNameSearchDatagridIcons;
        protected Label lblNationalID;
        protected Label lblOwnership;
        protected Label lblOwnershipPercentage;
        protected Label lblOwnershipType;
        protected Label lblPageStep;
        protected Label lblShareholdersDatagridHeader;
        protected Label lblShareholdersDatagridIcons;
        protected Label lblShareholdersOwners;
        protected Label lblShareType;
        protected Label lblSubHeader;
        protected Label lblSurname;
        protected HtmlTable outerNameSearchGridTable;
        protected HtmlTable outerShareholdersGridTable;
        protected RadioButton rbtnCompany;
        protected RadioButton rbtnIndividual;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected RequiredFieldValidator RequiredFieldValidator3;
        protected TextBox tbFirstname;
        protected TextBox tbNationalID;
        protected TextBox tbOwnership;
        protected TextBox tbSurname;
        protected HtmlTableCell tdNameSearchGrid;
        protected TextBox txtAddressEN;
        protected TextBox txtAddressNative;
        protected TextBox txtCIID;
        protected TextBox txtOwnershipPercentage;
        protected TextBox txtPostalCode;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "ShareholdersOwners";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                //Server.Transfer("SearchForReport.aspx?redirect=ShareholdersOwners");
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            string toLoadFromCompanyRegistry =
                CigConfig.Configure("lookupsettings.EnableToLoadShareholdersFromCompanyRegistry");
            if (toLoadFromCompanyRegistry != null && toLoadFromCompanyRegistry.ToLower().Equals("true")) {
                btnLoadFromCompanyRegistry.Visible = true;
            } else {
                btnLoadFromCompanyRegistry.Visible = false;
            }

            AddEnterEvent();

            FillDataSets();
            SetGridHeaders();
            LocalizeNameSearchGridHeader();
            InitShareHoldersGrid();
            BindShareHoldersGrid();
            lblErrMsg.Visible = false;
            if (!IsPostBack) {
                dtgrShareholders.Columns[10].Visible = false; // ownership percentage
                if (CigConfig.Configure("lookupsettings.OwnershipInPercentage") != null &&
                    CigConfig.Configure("lookupsettings.OwnershipInPercentage").ToLower() == "true") {
                    txtOwnershipPercentage.Visible = true;
                    lblOwnershipPercentage.Visible = true;
                    tbOwnership.Visible = false;
                    lblOwnership.Visible = false;
                    CustomValidator1.Enabled = false;
                    RequiredFieldValidator2.ControlToValidate = "txtOwnershipPercentage";
                    dtgrShareholders.Columns[9].Visible = false; // ownership
                    dtgrShareholders.Columns[10].Visible = true; // ownership percentage
                }

                outerNameSearchGridTable.Visible = false;
                tdNameSearchGrid.Visible = false;
                LocalizeText();
                BindOwnershipTypeBox();
                RequiredFieldValidator1.Enabled = true;
            }
        }

        private void FillDataSets() {
            if (Session["dsShareHolders"] == null) {
                dsShareHolders = myFact.GetShareHolderOwnerAsDataSet(CIID);
                // hmm afhverju er �etta fall?
                dsShareHolders = cleanDuplicatedCIIDS(dsShareHolders);
                Session["dsShareHolders"] = dsShareHolders;
            } else {
                dsShareHolders = (DataSet) Session["dsShareHolders"];
            }
        }

        public void BindOwnershipTypeBox() {
            var dsOwnershipTypes = new DataSet();
            if (Cache["dsOwnershipTypes"] == null) {
                dsOwnershipTypes = myFact.GetOwnershipTypesAsDataSet();
                Cache["dsOwnershipTypes"] = dsOwnershipTypes;
            } else {
                dsOwnershipTypes = (DataSet) Cache["dsOwnershipTypes"];
            }

            if (dsOwnershipTypes.Tables.Count > 0) {
                if (nativeCult) {
                    // 03112004 disable ordering
                    // dsOwnershipTypes.Tables[0].DefaultView.Sort = "OwnershipDescriptionNative";
                    ddlOwnershipType.DataSource = dsOwnershipTypes.Tables[0].DefaultView;
                    ddlOwnershipType.DataTextField = "OwnershipDescriptionNative";
                } else {
                    // 03112004 disable ordering
                    //dsOwnershipTypes.Tables[0].DefaultView.Sort = "OwnershipDescriptionEN";
                    ddlOwnershipType.DataSource = dsOwnershipTypes.Tables[0].DefaultView;
                    ddlOwnershipType.DataTextField = "OwnershipDescriptionEN";
                }
                ddlOwnershipType.DataValueField = "OwnershipID";
                ddlOwnershipType.DataBind();
            } else {
                Logger.WriteToLog("Could not get ownership types to display in ShareholdersOwners.aspx", true);
                lblErrMsg.Text = "Could not get ownership types overview";
                lblErrMsg.Visible = true;
            }
        }

        private static DataSet cleanDuplicatedCIIDS(DataSet ds) {
            if (ds.Tables.Count > 0) {
                var arrCiids = new ArrayList();
                var arrShareTypesIDs = new ArrayList();
                for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                    string creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    string shareTypesID = ds.Tables[0].Rows[i]["OwnershipID"].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (!((string) arrCiids[j]).Equals(creditInfoId) ||
                            !((string) arrShareTypesIDs[j]).Equals(shareTypesID)) {
                            continue;
                        }
                        ds.Tables[0].Rows.Remove(ds.Tables[0].Rows[i]);
                        break;
                    }
                    arrCiids.Add(creditInfoId);
                    arrShareTypesIDs.Add(shareTypesID);
                }
            }
            return ds;
        }

        public void InitShareHoldersGrid() {
            dtgrShareholders.Columns[5].Visible = false;
            dtgrShareholders.Columns[6].Visible = false;
            dtgrShareholders.Columns[7].Visible = false;
            dtgrShareholders.Columns[8].Visible = false;
            dtgrShareholders.Columns[11].Visible = false;
            dtgrShareholders.Columns[12].Visible = false;
            dtgrShareholders.Columns[13].Visible = false;
        }

        public void BindShareHoldersGrid() {
            dtgrShareholders.DataSource = dsShareHolders;
            dtgrShareholders.DataBind();
        }

        private void dtgrShareholders_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    if (e.Item.Cells[11].Text.Trim() == CigConfig.Configure("lookupsettings.individualID")) // indivitual
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[5].Text + " " + e.Item.Cells[6].Text;
                    } else // is company
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[12].Text;
                    }
                    // Ownership type
                    e.Item.Cells[14].Text = e.Item.Cells[15].Text;
                } else {
                    if (e.Item.Cells[11].Text.Trim() == CigConfig.Configure("lookupsettings.individualID")) // indivitual
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[7].Text + " " + e.Item.Cells[8].Text;
                    } else // is company
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[13].Text;
                    }

                    // Ownership type
                    e.Item.Cells[14].Text = e.Item.Cells[16].Text;
                }

                if (e.Item.Cells[10].Text != "" && e.Item.Cells[10].Text != "&nbsp;") {
                    decimal num = Convert.ToDecimal(e.Item.Cells[10].Text);
                    var fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
                    fInfo.NumberDecimalDigits = 2;
                    e.Item.Cells[10].Text = num.ToString("N", fInfo);
                }

                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
            }
            WebDesign.CreateExplanationIcons(dtgrShareholders.Columns, lblShareholdersDatagridIcons, rm, ci);
        }

        private void dtgrShareholders_ItemCommand(object source, DataGridCommandEventArgs e) {
            int rowToDelete = -1;
            try {
                int shareholdersCIID = Convert.ToInt32(dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString());

                if (e.CommandName == "Delete") {
                    rowToDelete = e.Item.ItemIndex;
                }
                if (rowToDelete >= 0) {
                    if (shareholdersCIID > 0) {
                        int ownershipID = int.Parse(dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["OwnershipID"].ToString().Trim());
                        myFact.DeleteShareholdersOwner(shareholdersCIID, CIID, ownershipID);
                    }
                    dsShareHolders.Tables[0].Rows[rowToDelete].Delete();
                    Session["dsShareHolders"] = null;
                    FillDataSets();
                    BindShareHoldersGrid();
                    ClearBoxes();
                    return;
                }

                if (e.CommandName == "Update") {
                    // do some fancy stuff
                    if (dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["Type"].ToString().Trim() ==
                        CigConfig.Configure("lookupsettings.individualID")) {
                        if (nativeCult) {
                            tbFirstname.Text =
                                dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["FirstNameNative"].ToString();
                            tbSurname.Text = dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["SurNameNative"].ToString();
                        } else {
                            tbFirstname.Text = dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["FirstNameEN"].ToString();
                            tbSurname.Text = dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["SurNameEN"].ToString();
                        }
                        rbtnIndividual.Checked = true;
                        rbtnCompany.Checked = false;
                    } else {
                        tbFirstname.Text = nativeCult ? dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["NameNative"].ToString() : dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["NameEN"].ToString();
                        tbSurname.Text = "";
                        rbtnCompany.Checked = true;
                    }
                    tbNationalID.Text = dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["Number"].ToString();
                    txtCIID.Text = dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString();
                    tbOwnership.Text = dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["Ownership"].ToString();
                    txtOwnershipPercentage.Text =
                        dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["Ownership_Perc"].ToString();
                    ddlOwnershipType.SelectedValue =
                        dsShareHolders.Tables[0].Rows[e.Item.ItemIndex]["OwnershipID"].ToString();
                    btnReg.Text = rm.GetString("txtUpdate", ci);
                    btnReg.CommandName = "Update";
                    tbNationalID.ReadOnly = true;
                    RequiredFieldValidator1.Enabled = false;
                    txtCIID.ReadOnly = true;
                }
            } catch (Exception err) {
                Logger.WriteToLog("RegBoardMember.aspx : dtgrBoardMOverview_ItemCommand " + err.Message, true);
            }
        }

        private void btnReg_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                RegShareholder(
                    rbtnCompany.Checked,
                    txtCIID.Text,
                    tbNationalID.Text,
                    tbFirstname.Text,
                    tbSurname.Text,
                    txtAddressNative.Text,
                    txtAddressEN.Text,
                    txtPostalCode.Text,
                    tbOwnership.Text,
                    txtOwnershipPercentage.Text,
                    Convert.ToInt32(ddlOwnershipType.SelectedValue));
            }
        }

        private void RegShareholder(
            bool isCompany,
            string theCIID,
            string nationalID,
            string firstName,
            string surName,
            string addressNative,
            string addressEN,
            string postalCode,
            string ownership,
            string ownershipPerc,
            int ownershipTypeID) {
            if (ownershipPerc == "") {
                ownershipPerc = "0";
            }
            if (ownership == "") {
                ownership = "0";
            }
            bool insertOK;
            try {
                Address myAddress = null;

                if (addressEN.Length > 0 || addressNative.Length > 0 || postalCode.Length > 0) {
                    myAddress = new Address
                                {
                                    StreetNative = addressNative,
                                    StreetEN = addressEN,
                                    PostalCode = postalCode,
                                    CityID = (-1)
                                };
                }

                if (!isCompany) // indivitual
                {
                    var theSOPerson = new ShareholdersOwnerPersonBLLC();

                    if (nativeCult) {
                        theSOPerson.FirstNameNative = firstName;
                        theSOPerson.SurNameNative = surName;
                    } else {
                        theSOPerson.FirstNameEN = firstName;
                        theSOPerson.SurNameEN = surName;
                    }
                    if (theCIID.Trim() != "") {
                        theSOPerson.CreditInfoID = int.Parse(theCIID);
                    }
                    theSOPerson.Ownership = Convert.ToInt32(ownership);
                    theSOPerson.OwnershipTypeID = ownershipTypeID;
                    theSOPerson.OwnershipPercentage = Convert.ToDecimal(ownershipPerc);
                    theSOPerson.NationalID = nationalID;

                    if (myAddress != null) {
                        theSOPerson.Address.Add(myAddress);
                    }

                    insertOK = myFact.AddShareholderOwner(theSOPerson, CIID);
                } else // is company
                {
                    var theSOCompany = new ShareholdersOwnerCompanyBLLC();

                    if (nativeCult) {
                        theSOCompany.NameNative = firstName;
                    } else {
                        theSOCompany.NameEN = firstName;
                    }
                    if (theCIID.Trim() != "") {
                        theSOCompany.CreditInfoID = int.Parse(theCIID.Trim());
                    }
                    theSOCompany.Ownership = Convert.ToInt32(ownership);
                    theSOCompany.OwnershipTypeID = ownershipTypeID;
                    theSOCompany.OwnershipPercentage = Convert.ToDecimal(ownershipPerc);
                    theSOCompany.NationalID = nationalID;

                    if (myAddress != null) {
                        theSOCompany.Address.Add(myAddress);
                    }

                    insertOK = myFact.AddShareholderOwner(theSOCompany, CIID);
                }
            } catch (Exception err) {
                Logger.WriteToLog(err.ToString(), true);
                insertOK = false;
            }
            if (!insertOK) {
                DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
            } else {
                DisplayMessage(rm.GetString("txtInfoSaved", ci));
            }

            Session["dsShareHolders"] = null;
            FillDataSets();
            BindShareHoldersGrid();
            ClearBoxes();
        }

        private void ClearBoxes() {
            txtCIID.Text = "";
            tbFirstname.Text = "";
            tbSurname.Text = "";
            tbNationalID.Text = "";
            tbOwnership.Text = "";
            // init button and NationalID field
            btnReg.Text = rm.GetString("txtSave", ci);
            btnReg.CommandName = "Update";
            tbNationalID.ReadOnly = false;
            ddlOwnershipType.SelectedIndex = -1;
            txtCIID.ReadOnly = false;
            RequiredFieldValidator1.Enabled = true;
        }

        private void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args) {
            try {
                args.IsValid = Convert.ToInt32(tbOwnership.Text) > -1;
            } catch (Exception) {
                args.IsValid = false;
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }

        private void LocalizeText() {
            lblCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblShareholdersOwners.Text = rm.GetString("txtShareholders/Owner", ci);
            lblFirstName.Text = rm.GetString("txtFirstName", ci);
            lblSurname.Text = rm.GetString("txtSurName", ci);
            lblNationalID.Text = rm.GetString("txtNationalID", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);
            lblOwnership.Text = rm.GetString("txtOwnership", ci);
            lblOwnershipType.Text = rm.GetString("txtOwnershipType", ci);
            imgIndividual.AlternateText = rm.GetString("txtPerson", ci);
            imgCompany.AlternateText = rm.GetString("txtCompany", ci);
            RequiredFieldValidator3.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator1.ErrorMessage = rm.GetString("txtValueMissing", ci);
            CustomValidator1.ErrorMessage = rm.GetString("txtErrorInDataInput", ci);
            RequiredFieldValidator2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            btnReg.Text = rm.GetString("txtSave", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            btnCancel.Text = rm.GetString("txtBack", ci);
            btnLoadFromCompanyRegistry.Text = rm.GetString("txtLoadBoardFromCompanyRegistry", ci);
            lblNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblShareholdersDatagridHeader.Text = rm.GetString("txtShareholders/Owner", ci);
            lblSubHeader.Text = rm.GetString("txtAdditionalInfo", ci);
            lblOwnershipPercentage.Text = rm.GetString("txtOwnershipSubsidiaries", ci);
        }

        private void SetGridHeaders() {
            dtgrShareholders.Columns[2].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrShareholders.Columns[3].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrShareholders.Columns[4].HeaderText = rm.GetString("txtName", ci);
            dtgrShareholders.Columns[9].HeaderText = rm.GetString("txtOwnership", ci);
            dtgrShareholders.Columns[10].HeaderText = rm.GetString("txtOwnershipSubsidiaries", ci);
        }

        private void btnCancel_Click(object sender, EventArgs e) { TransferToPreviousPage(); }

        private void AddEnterEvent() {
            tbFirstname.Attributes.Add("onkeypress", "checkEnterKey();");
            tbSurname.Attributes.Add("onkeypress", "checkEnterKey();");
            tbOwnership.Attributes.Add("onkeypress", "checkEnterKey();");
            rbtnIndividual.Attributes.Add("onkeypress", "checkEnterKey();");
            rbtnCompany.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        /// <summary>
        /// Localizes the search grid header
        /// </summary>
        private void LocalizeNameSearchGridHeader() {
            dtgrNameSearch.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrNameSearch.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrNameSearch.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrNameSearch.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dtgrNameSearch.Columns[9].HeaderText = rm.GetString("txtCity", ci);
        }

        /// <summary>
        /// Searches for given name in the national name database
        /// </summary>
        /// <param name="nationalID"></param>
        /// <param name="name">The name or part of name to search for</param>
        /// <param name="ciid"></param>
        /// <param name="surName"></param>
        public void SearchForNames(string ciid, string nationalID, string name, string surName) {
            var uaf = new uaFactory();

            DataSet dsNameSearch;
            if (rbtnIndividual.Checked) {
                //Person
                var debtor = new Debtor();

                if (name.Trim() != "") {
                    debtor.FirstName = name;
                }
                if (surName.Trim() != "") {
                    debtor.SurName = surName;
                }
                if (ciid.Trim() != "") {
                    debtor.CreditInfo = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    debtor.IDNumber1 = nationalID;
                    debtor.IDNumber2Type = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                }
                if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                    dsNameSearch = uaf.FindCustomerInNationalAndCreditInfo(debtor);
                    // if not found in national registry (f.ex. the Cyprus Whitelist is incomplete data) then search in our db
                    if (dsNameSearch.Tables.Count > 0) {
                        if (dsNameSearch.Tables[0] == null || dsNameSearch.Tables[0].Rows == null ||
                            dsNameSearch.Tables[0].Rows.Count <= 0) {
                            dsNameSearch = uaf.FindCustomer(debtor);
                        }
                    } else {
                        dsNameSearch = uaf.FindCustomer(debtor);
                    }
                } else {
                    dsNameSearch = uaf.FindCustomer(debtor);
                }
            } else {
                //Company				
                var company = new Company();

                if (name.Trim() != null) {
                    company.NameNative = name;
                }
                if (ciid.Trim() != "") {
                    company.CreditInfoID = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    company.NationalID = nationalID;
                }
                /*{
					IDNumber idn = new IDNumber();
					idn.Number = nationalID;
					idn.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"]); 
					company.IDNumbers = new ArrayList();
					company.IDNumbers.Add(idn);
				}*/
                dsNameSearch = CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True" ? uaf.FindCompanyInNationalAndCreditInfo(company) : uaf.FindCompany(company);
            }

            if (dsNameSearch.Tables.Count > 0 && dsNameSearch.Tables[0] != null) {
                if (dsNameSearch.Tables[0].Rows.Count > 0) {
                    dtgrNameSearch.DataSource = dsNameSearch;
                    dtgrNameSearch.DataBind();
                    tdNameSearchGrid.Visible = true;

                    int gridRows = dtgrNameSearch.Items.Count;
                    divNameSearch.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                    divNameSearch.Style["OVERFLOW"] = "auto";
                    divNameSearch.Style["OVERFLOW-X"] = "auto";

                    outerNameSearchGridTable.Visible = true;
                } else {
                    DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                    tdNameSearchGrid.Visible = false;
                }
            } else {
                tdNameSearchGrid.Visible = false;
            }
        }

        private void DisplayMessage(String message) {
            lblErrMsg.Text = message;
            lblErrMsg.ForeColor = Color.Blue;
            lblErrMsg.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblErrMsg.Text = errorMessage;
            lblErrMsg.ForeColor = Color.Red;
            lblErrMsg.Visible = true;
        }

        private void dtgrNameSearch_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }

                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }

                    //Set city - Native if available, else EN
                    if (e.Item.Cells[10].Text.Trim() != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }

                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }

                    //Set city - Native if available, else EN
                    if (e.Item.Cells[11].Text.Trim() != "" && e.Item.Cells[11].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);	
            }

            WebDesign.CreateExplanationIcons(dtgrNameSearch.Columns, lblNameSearchDatagridIcons, rm, ci);
        }

        private void dtgrNameSearch_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                if (e.Item.Cells[7].Text != "&nbsp;") {
                    txtAddressNative.Text = e.Item.Cells[7].Text;
                }
                if (e.Item.Cells[8].Text != "&nbsp;") {
                    txtAddressEN.Text = e.Item.Cells[8].Text;
                }
                if (e.Item.Cells[9].Text != "&nbsp;") {
                    txtPostalCode.Text = e.Item.Cells[9].Text;
                }

                if (e.Item.Cells[1].Text == "&nbsp;") {
                    txtCIID.Text = "";
                    RequiredFieldValidator1.Enabled = true;
                } else {
                    txtCIID.Text = e.Item.Cells[1].Text;
                    RequiredFieldValidator1.Enabled = false;
                }
                tbNationalID.Text = e.Item.Cells[2].Text;
                if (tbNationalID.Text == "&nbsp;") {
                    tbNationalID.Text = "";
                }

                /*if(txtCIID.Text=="&nbsp;")
					txtCIID.Text="";*/

                string name = e.Item.Cells[3].Text.Trim();
                if (name == "&nbsp;") {
                    name = "";
                }
                if (rbtnIndividual.Checked) {
                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            tbFirstname.Text = arrName[0];
                        } else {
                            tbSurname.Text = arrName[arrName.Length - 1];
                            tbFirstname.Text = name.Substring(0, name.Length - (tbSurname.Text.Length + 1));
                        }
                    }
                } else {
                    tbFirstname.Text = name;
                }

                tdNameSearchGrid.Visible = false;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e) { SearchForNames(txtCIID.Text, tbNationalID.Text, tbFirstname.Text, tbSurname.Text); }

        private void btnLoadFromCompanyRegistry_Click(object sender, EventArgs e) {
            string nationalId = myFact.GetNationalIDByCIID(CIID);
            if (nationalId == null) {
                return;
            }
            var ds = myFact.GetCompanyShareholdersFromCompanyRegistryAsDataSet(nationalId);
            if (ds.Tables.Count <= 0 || ds.Tables[0] == null) {
                return;
            }
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                //Get information about board member from CompanyRegistry
                string firstName;
                string surName = "";
                string ssn = ds.Tables[0].Rows[i]["PersonSSN"].ToString();
                string uName = ds.Tables[0].Rows[i]["Name"].ToString();
                string[] arrName = uName.Split(' ');
                string isCompany = ds.Tables[0].Rows[i]["IsCompany"].ToString();
                if (arrName.Length > 0 && isCompany.Trim() != "1") {
                    if (arrName.Length == 1) {
                        firstName = arrName[0];
                    } else {
                        surName = arrName[arrName.Length - 1];
                        firstName = uName.Substring(0, uName.Length - (surName.Length + 1));
                    }
                } else {
                    surName = "";
                    firstName = uName;
                }
                string address = ds.Tables[0].Rows[i]["Address"].ToString();
                string pcode = ds.Tables[0].Rows[i]["PostalCode"].ToString();
                string mOwnId = ds.Tables[0].Rows[i]["OwnershipID"].ToString();
                string mOwn = ds.Tables[0].Rows[i]["Ownership"].ToString();
                string mOwnPerc = ds.Tables[0].Rows[i]["OwnershipPerc"].ToString();
                if (isCompany.Trim() == "1") {
                    RegShareholder(
                        true,
                        "-1",
                        ssn,
                        firstName,
                        surName,
                        address,
                        address,
                        pcode,
                        mOwn,
                        mOwnPerc,
                        int.Parse(mOwnId));
                } else {
                    RegShareholder(
                        false,
                        "-1",
                        ssn,
                        firstName,
                        surName,
                        address,
                        address,
                        pcode,
                        mOwn,
                        mOwnPerc,
                        int.Parse(mOwnId));
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSearch.Click += new EventHandler(this.btnSearch_Click);
            this.CustomValidator1.ServerValidate += new ServerValidateEventHandler(this.CustomValidator1_ServerValidate);
            this.dtgrNameSearch.ItemCommand += new DataGridCommandEventHandler(this.dtgrNameSearch_ItemCommand);
            this.dtgrNameSearch.ItemDataBound += new DataGridItemEventHandler(this.dtgrNameSearch_ItemDataBound);
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnLoadFromCompanyRegistry.Click += new EventHandler(this.btnLoadFromCompanyRegistry_Click);
            this.btnContinue.Click += new EventHandler(this.btnContinue_Click);
            this.btnReg.Click += new EventHandler(this.btnReg_Click);
            this.dtgrShareholders.ItemCommand += new DataGridCommandEventHandler(this.dtgrShareholders_ItemCommand);
            this.dtgrShareholders.ItemDataBound += new DataGridItemEventHandler(this.dtgrShareholders_ItemDataBound);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}