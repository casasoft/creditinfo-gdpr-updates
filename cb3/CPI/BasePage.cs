#region

using System;
using System.Web.UI;

#endregion

namespace CPI {
    /// <summary>
    /// Summary description for BasePage.
    /// </summary>
    public class BasePage : Page {
        public string PageTitle { get; set; }

        protected override void OnInit(EventArgs e) {
            Controls.AddAt(0, LoadControl("TestHeader.ascx"));
            //	this.Controls.AddAt(0, LoadControl("../user_controls/head.ascx"));
            base.OnInit(e);
            Controls.Add(LoadControl("TestFooter.ascx"));
        }
    }

    public class BaseControl : UserControl {
        public new BasePage Page { get { return (BasePage) base.Page; } }
    }
}