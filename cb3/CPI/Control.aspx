<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="Control.aspx.cs" AutoEventWireup="false" Inherits="CPI.Control" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Control</title>
		<script language="JavaScript" src="../DatePicker.js"></script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						ControlForm.btnFinish.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.ControlForm.tbReportsExpiryDate.focus();
				}

		</SCRIPT>
	</HEAD>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="ControlForm" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td><asp:label id="lblControl" runat="server">Control</asp:label></td>
																	<td align="right"><asp:label id="lblPageStep" runat="server">Step</asp:label></td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td style="WIDTH: 50%"><asp:label id="lblReportStatus" runat="server">Report status</asp:label><br>
																		<asp:dropdownlist id="ddReportStatus" runat="server"></asp:dropdownlist></td>
																	<td><asp:label id="lblReportsExpiry" runat="server">Reports expiry date</asp:label><br>
																		<asp:textbox id="tbReportsExpiryDate" runat="server" maxlength="10"></asp:textbox>&nbsp;<input class="popup" onclick="PopupPicker('tbReportsExpiryDate', 250, 250);" type="button"
																			size="20" value="...">
																		<asp:customvalidator id="CustomValidator1" runat="server" display="None" cssclass="label" errormessage="Error in date input"
																			controltovalidate="tbReportsExpiryDate">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td><asp:label id="lblComment" runat="server">Internal comment</asp:label><br>
																		<asp:textbox id="tbInternalComment" runat="server" maxlength="2048" width="100%" textmode="MultiLine"
																			height="150px"></asp:textbox></td>
																	<td><asp:label id="lblCommentEN" runat="server">Internal comment EN</asp:label><br>
																		<asp:textbox id="txtInternalCommentEN" runat="server" maxlength="2048" width="100%" textmode="MultiLine"
																			height="150px"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblMessage" runat="server" cssclass="error_text">Error</asp:label><br>
															<asp:validationsummary id="ValidationSummary1" runat="server" cssclass="error_text"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnBack" runat="server" cssclass="cancel_button" text="Back"></asp:button><asp:button id="btnFinish" runat="server" cssclass="confirm_button" text="Finish entry"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
