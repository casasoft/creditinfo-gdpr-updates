#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;
using UserAdmin.BLL.CIUsers;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for BasicInformation.
    /// </summary>
    public class BasicInformation : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnCancel;
        protected Button btnCitySearch;
        protected Button btnContinue;
        protected Button btnReg;
        protected Button btnTradingCitySearch;
        private int CIID = -1;
        protected CustomValidator CustomValidator1;
        protected CustomValidator CustomValidator2;
        protected CustomValidator CustomValidator3;
        protected CustomValidator CustomValidator4;
        protected CustomValidator CustomValidator5;
        protected CustomValidator CustomValidator6;
        protected CustomValidator cvContactDateContacted;
        protected HtmlTableRow CYPLARLABELSROW;
        protected DropDownList ddCity;
        protected DropDownList ddLegalForm;
        protected DropDownList ddCountryList;

        protected DropDownList ddlTradingCity;
        private DataSet dsRegistrationForms = new DataSet();
        private DataSet dsCountryList = new DataSet();
        protected Label Label1;
        protected Label Label2;
        protected Label Label3;
        protected Label Label4;
        protected Label Label5;
        protected Label lblAdditionalInfoHeader;
        protected Label lblAddress;
        protected Label lblAddressEN;
        protected Label lblAddressHeader;
        protected Label lblAreaCode;
        protected Label lblBasicInfo;
        protected Label lblCity;
        protected Label lblCitySearch;
        protected Label lblComments;
        protected Label lblCommentsHeader;
        protected Label lblContactDateContacted;
        protected Label lblContactEmail;
        protected Label lblContactFaxNumber;
        protected Label lblContactFirstName;
        protected Label lblContactFirstNameEN;
        protected Label lblContactInformation;
        protected Label lblContactLastName;
        protected Label lblContactLastNameEN;
        protected Label lblContactMessengerID;
        protected Label lblContactMobileNumber;
        protected Label lblContactPhoneNumber;
        protected Label lblContactPosition;
        protected Label lblEmail;
        protected Label lblEstablished;
        protected Label lblFax;
        protected Label lblFormerName;
        protected Label lblFormerNameEN;
        protected Label lblHomePage;
        protected Label lblInternalCommentEN;
        protected Label lblLAR;
        protected Label lblLastContacted;
        protected Label lblLastUpdated;
        protected Label lblLegalForm;
        protected Label lblLR;
        protected Label lblMobile;
        protected Label lblName;
        protected Label lblNameEN;
        protected Label lblPageStep;
        protected Label lblPostBox;
        protected Label lblRegistered;
        protected Label lblRegMsg;
        protected Label lblSymbol;
        protected Label lblTel;
        protected Label lblTradeName;
        protected Label lblTradeNameEN;
        protected Label lblTradingAddress;
        protected Label lblTradingAddressEN;
        protected Label lblTradingAddressHeader;
        protected Label lblTradingCity;
        protected Label lblTradingCitySearch;
        protected Label lblTradingPostalCode;
        protected Label lblTradingPostBox;
        protected Label lblUniqueID;
        protected Label lblVAT;
        protected Label lbStructureResearchDate;
        protected Label lblCountry;
        private bool newRegistration;
        protected HtmlInputHidden PageX;
        protected HtmlInputHidden PageY;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected RequiredFieldValidator RequiredFieldValidator4;
        protected RequiredFieldValidator rfvCity;
        protected RequiredFieldValidator rfvTradingCity;
        protected TextBox tbAddress;
        protected TextBox tbAreaCode;
        protected TextBox tbComments;
        protected TextBox tbEmail;
        protected TextBox tbEstablished;
        protected TextBox tbFax;
        protected TextBox tbFormerName;
        protected TextBox tbHomePage;
        protected TextBox tbLAR;
        protected TextBox tbLastContacted;
        protected TextBox tbLastUpdated;
        protected TextBox tbLR;
        protected TextBox tbMobile;
        protected TextBox tbName;
        protected TextBox tbPostbox;
        protected TextBox tbRegistered;
        protected TextBox tbStructureReportResearchDate;
        protected TextBox tbTel;
        protected TextBox tbTradeName;
        protected TextBox tbUniqueID;
        protected TextBox tbVAT;
        private ReportCompanyBLLC theRCompany = new ReportCompanyBLLC();
        protected TextBox txtAddressEN;
        protected TextBox txtCitySearch;
        protected TextBox txtCommentEN;
        protected TextBox txtContactDateContacted;
        protected TextBox txtContactEmail;
        protected TextBox txtContactFaxNumber;
        protected TextBox txtContactFirstName;
        protected TextBox txtContactFirstNameEN;
        protected TextBox txtContactLastName;
        protected TextBox txtContactLastNameEN;
        protected TextBox txtContactMessengerID;
        protected TextBox txtContactMobileNumber;
        protected TextBox txtContactPhoneNumber;
        protected TextBox txtContactPosition;
        protected TextBox txtFormerNameEN;
        protected TextBox txtNameEN;
        protected TextBox txtSymbol;
        protected TextBox txtTradeNameEN;
        protected TextBox txtTradingAddress;
        protected TextBox txtTradingAddressEN;
        protected TextBox txtTradingCitySearch;
        protected TextBox txtTradingPostalCode;
        protected TextBox txtTradingPostBox;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "BasicInformation";

            btnContinue.Enabled = false;

            CYPLARLABELSROW.Visible = CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus");
            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else if (Request.QueryString["action"] == null) {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&new=true");
            }

            PreparePageLoad();

            if (Request.QueryString["action"] != null) {
                newRegistration = Request.QueryString["action"].Equals("new");
                if (Request.QueryString["uniqueid"] != null) {
                    tbUniqueID.Text = Request.QueryString["uniqueid"];
                }
            } else {
                newRegistration = true;
            }

            if (CIID > 0) // athuga hvort � session s� report � �ennan a�ila
            {
                if (Session["ReportCompany"] != null) {
                    newRegistration = false;
                }
                if (Session["ReportExists"] == null) {
                    btnContinue.Enabled = false;
                }
            }

            if (newRegistration) {
                btnReg.Text = rm.GetString("txtSave", ci);
                btnReg.CommandName = "Registration";
                //	this.tbUniqueID.ReadOnly = false;
            } else {
                btnReg.Text = rm.GetString("txtUpdate", ci);
                btnReg.CommandName = "Update";

                btnContinue.Enabled = true;

                //	this.tbUniqueID.ReadOnly = true;
            }

            // Add <ENTER> event
            AddEnterEvent();

            if (!IsPostBack) {
                if (!newRegistration) {
                    FillDataSets();
                    BindBoxes();
                    InitOtherBoxes();
                } else {
                    DateTime.Today.ToShortDateString();
                    tbEstablished.Text = DateTime.Today.ToShortDateString();
                    tbLastContacted.Text = DateTime.Today.ToShortDateString();
                    tbLastUpdated.Text = DateTime.Today.ToShortDateString();
                    txtContactDateContacted.Text = DateTime.Today.ToShortDateString();

                    // hva� me� �j��skr�rtengingu h�r og s�kja grunnuppl�singar ...?
                    // nota frekar CIG uppl�singar ef til eru ...?
                    // athugar hvort CIG uppl�singar eru til sta�ar
                    if (tbUniqueID.Text.Trim().Length > 0) {
                        if (!InitBoxesFromCIG(tbUniqueID.Text)) {
                            // ef ekki CIG uppl�singar athuga hvort �j��askr�rtenging er til sta�ar
                            if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                                // use national registry
                                if (tbUniqueID.Text.Trim().Length > 0) {
                                    InitBoxesFromNationalRegistry(tbUniqueID.Text);
                                }
                            }
                        }
                    }
                    FillDataSets();
                    BindBoxes();
                }
                LocalizeText();
            }
            /*else
			{
				FillDataSets();
				BindBoxes();
				ddCity.SelectedValue = Request.Params["ddCity"];
				ddlTradingCity.SelectedValue = Request.Params["ddlTradingCity"];
			}*/
        }

        private void FillDataSets() {
            FillRegistrationSet();
            FillCountryListDataSet();
            //	FillCitySet();
        }

        private void FillRegistrationSet() {
            if (Cache["dsRegistrationForms"] != null) {
                dsRegistrationForms = (DataSet) Cache["dsRegistrationForms"];
            } else {
                dsRegistrationForms = myFact.GetRegistrationFormAsDataSet();
            }
        }

        private void FillCountryListDataSet()
        {
            if (Cache["dsCountryList"] != null)
            {
                dsCountryList = (DataSet)Cache["dsCountryList"];
            }
            else
            {
                dsCountryList = myFact.GetCountriesAsDataSet(false);
            }
        }

        /*private void FillCitySet()
		{
			bool en = false;
			if(nativeCult)
				en=false;
			else
				en=true;

			if(Cache["dsCity"] != null)
				dsCity = (DataSet) Cache["dsCity"];
			else
				dsCity = myFact.GetCityListAsDataSet(en);
		}*/

        private void BindBoxes() {
            BindLegalFormBox();
            BindCountryList();
            //BindCityList();
            //BindTradingCityList();
        }


        private void BindCountryList()
        {
            ddCountryList.DataSource = dsCountryList;
            ddCountryList.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddCountryList.DataValueField = "CountryID";

            ddCountryList.DataBind();
            var defaultItem = ddCountryList.Items.FindByText("Malta");
            if (defaultItem != null)
                defaultItem.Selected = true;
            ddCountryList.Items.Insert(0, new ListItem("N/A", "-1"));
        }

        private void BindLegalFormBox() {
            ddLegalForm.DataSource = dsRegistrationForms;
            ddLegalForm.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddLegalForm.DataValueField = "FormID";
            ddLegalForm.DataBind();
        }

        private void BindCityList(DataSet dsCity) {
            ddCity.DataSource = dsCity;
            ddCity.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddCity.DataValueField = "CityID";
            ddCity.DataBind();
        }

        private void BindTradingCityList(DataSet dsCity) {
            ddlTradingCity.DataSource = dsCity;
            ddlTradingCity.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddlTradingCity.DataValueField = "CityID";
            ddlTradingCity.DataBind();
        }

        /// <summary>
        /// Searches in the CIG for information about given unique id (national id)
        /// </summary>
        /// <param name="uniqueID">The unique (national) id to search for</param>
        /// <returns>true if information found, false if not</returns>
        private bool InitBoxesFromCIG(string uniqueID) {
            try {
                int iCIID = myFact.GetCIIDByNationalID(uniqueID);
                if (iCIID > 0) {
                    Company companyInfo = myFact.GetCompany(iCIID);
                    if (companyInfo != null) {
                        if (nativeCult) {
                            tbName.Text = companyInfo.NameNative;
                            tbAddress.Text = companyInfo.SingleAddressNative;
                        } else {
                            tbName.Text = companyInfo.NameEN;
                            tbAddress.Text = companyInfo.SingleAddressEN;
                        }

                        CIID = iCIID;
                        tbEmail.Text = companyInfo.Email;
                        tbEstablished.Text = companyInfo.Established.ToShortDateString();
                        tbHomePage.Text = companyInfo.URL;
                        tbLastContacted.Text = companyInfo.LastContacted.ToShortDateString();
                        tbLastUpdated.Text = companyInfo.LastUpdate.ToShortDateString();

                        if ((companyInfo.Registered > DateTime.MinValue) && (companyInfo.Registered < DateTime.MaxValue)) {
                            tbRegistered.Text = companyInfo.Registered.ToShortDateString();
                        }

                        if (companyInfo.Number != null) {
                            foreach (PhoneNumber myNumber in companyInfo.Number) {
                                if (CigConfig.Configure("lookupsettings.workCode") ==
                                    Convert.ToString(myNumber.NumberTypeID)) {
                                    tbTel.Text = myNumber.Number;
                                } else if (CigConfig.Configure("lookupsettings.mobileCode") ==
                                           Convert.ToString(myNumber.NumberTypeID)) {
                                    tbMobile.Text = myNumber.Number;
                                } else if (CigConfig.Configure("lookupsettings.faxCode") ==
                                           Convert.ToString(myNumber.NumberTypeID)) {
                                    tbFax.Text = myNumber.Number;
                                }
                            }
                        }
                        tbAreaCode.Text = "";
                        tbComments.Text = "";
                        tbFormerName.Text = "";
                        tbPostbox.Text = "";
                        tbTradeName.Text = "";
                        tbVAT.Text = "";
                        return true;
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(
                    pageName + "::InitBoxesFromCIG(" + uniqueID + ") -" + err.Message + "--" + err.StackTrace, true);
            }
            return false;
        }

        /// <summary>
        /// Searches in national company registry for information about given unique id (SSN)
        /// </summary>
        /// <param name="uniqueID">The unique id to search for</param>
        private void InitBoxesFromNationalRegistry(string uniqueID) {
            try {
                DataSet ds = myFact.GetCompanyBySSNFromNationalInterfaceAsDataSet(uniqueID);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null) {
                    if (ds.Tables[0].Rows.Count > 0) {
                        tbName.Text = ds.Tables[0].Rows[0]["Name"].ToString().Trim();
                        tbAddress.Text = ds.Tables[0].Rows[0]["Address"].ToString().Trim();
                        tbTel.Text = ds.Tables[0].Rows[0]["PhoneNumber"].ToString().Trim();
                        tbFax.Text = ds.Tables[0].Rows[0]["FaxNumber"].ToString().Trim();
                        tbAreaCode.Text = ds.Tables[0].Rows[0]["PostalCode"].ToString().Trim();
                        tbPostbox.Text = ds.Tables[0].Rows[0]["POBox"].ToString().Trim();
                        tbVAT.Text = ds.Tables[0].Rows[0]["VAT"].ToString().Trim();
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(
                    pageName + "::InitBoxesFromNationalRegistry(" + uniqueID + ") -" + err.Message + "--" +
                    err.StackTrace,
                    true);
            }
        }

        private void InitOtherBoxes() {
            if (Session["ReportCompany"] != null) {
                theRCompany = (ReportCompanyBLLC) Session["ReportCompany"];
            } else {
                theRCompany = myFact.GetCompanyReport(null, CIID, false);
            }

            tbName.Text = theRCompany.NameNative;
            tbFormerName.Text = theRCompany.FormerNameNative;
            tbTradeName.Text = theRCompany.TradeNameNative;

            txtNameEN.Text = theRCompany.NameEN;
            txtFormerNameEN.Text = theRCompany.FormerNameEN;
            txtTradeNameEN.Text = theRCompany.TradeNameEN;

            txtSymbol.Text = theRCompany.Symbol;

            if (CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus")) {
                if (theRCompany.LAR != DateTime.MinValue) {
                    tbLAR.Text = theRCompany.LAR.ToShortDateString();
                }
                if (theRCompany.LR != DateTime.MinValue) {
                    tbLR.Text = theRCompany.LR.ToShortDateString();
                }
                if (theRCompany.StructureReportResearchDate != DateTime.MinValue) {
                    tbStructureReportResearchDate.Text = theRCompany.StructureReportResearchDate.ToShortDateString();
                }
            }

            if (theRCompany.Address.Count > 0) {

                var address = ((Address)theRCompany.Address[0]);
                tbAddress.Text = address.StreetNative;
                txtAddressEN.Text = address.StreetEN;
                //TODO :: H�r �arf a� s�kja nafn � borg ef value er st�rra en 0 og inserta �v� inn � listbox
                int cityID = address.CityID;
                if (cityID > -1) {
                    string cityName = myFact.GetCityName(cityID, !nativeCult);
                    if (!string.IsNullOrEmpty(cityName)) {
                        ddCity.Items.Add(new ListItem(cityName, cityID.ToString()));
                        ddCity.SelectedValue = cityID.ToString();
                    }
                }
                //this.ddCity.SelectedValue = ((Address)theRCompany.Address[0]).CityID.ToString();

                tbAreaCode.Text = address.PostalCode;
                tbPostbox.Text = address.PostBox;

                if (address.CountryId.HasValue)
                {
                    var item = ddCountryList.Items.FindByValue(address.CountryId.Value.ToString());
                    if (item != null)
                    {
                        ddCountryList.SelectedItem.Selected = false;
                        item.Selected = true;
                    }
                }

            }

            // need to check for isTradingAddress value as well
            if (theRCompany.Address.Count > 1) {
                if (((Address) theRCompany.Address[1]).IsTradingAddress) {
                    txtTradingAddress.Text = ((Address) theRCompany.Address[1]).StreetNative;
                    txtTradingAddressEN.Text = ((Address) theRCompany.Address[1]).StreetEN;
                    //TODO :: H�r �arf a� s�kja nafn � borg ef value er st�rra en 0 og inserta �v� inn � listbox
                    int cityID = ((Address) theRCompany.Address[1]).CityID;
                    if (cityID > -1) {
                        string cityName = myFact.GetCityName(cityID, !nativeCult);
                        if (!string.IsNullOrEmpty(cityName)) {
                            ddlTradingCity.Items.Add(new ListItem(cityName, cityID.ToString()));
                            ddlTradingCity.SelectedValue = cityID.ToString();
                        }
                    }
                    //this.ddlTradingCity.SelectedValue = ((Address)theRCompany.Address[1]).CityID.ToString();
                    txtTradingPostalCode.Text = ((Address) theRCompany.Address[1]).PostalCode;
                    txtTradingPostBox.Text = ((Address) theRCompany.Address[1]).PostBox;
                }
            }

            if (theRCompany.ContactPerson.DateContacted.Year > 1899) {
                txtContactDateContacted.Text = theRCompany.ContactPerson.DateContacted.ToShortDateString();
            }
            txtContactEmail.Text = theRCompany.ContactPerson.EmailAddress;
            txtContactFaxNumber.Text = theRCompany.ContactPerson.FaxNumber;
            txtContactFirstName.Text = theRCompany.ContactPerson.FirstNameNative;
            txtContactLastName.Text = theRCompany.ContactPerson.LastNameNative;
            txtContactFirstNameEN.Text = theRCompany.ContactPerson.FirstNameEN;
            txtContactLastNameEN.Text = theRCompany.ContactPerson.LastNameEN;
            txtContactMessengerID.Text = theRCompany.ContactPerson.MessengerID;
            txtContactMobileNumber.Text = theRCompany.ContactPerson.MobileNumber;
            txtContactPhoneNumber.Text = theRCompany.ContactPerson.PhoneNumber;
            txtContactPosition.Text = theRCompany.ContactPerson.Position;

            tbComments.Text = theRCompany.InternalComments;
            txtCommentEN.Text = theRCompany.InternalCommentsEN;
            tbEmail.Text = theRCompany.Email;

            if (theRCompany.Established.Year > 1899) {
                tbEstablished.Text = theRCompany.Established.ToShortDateString();
            }
            if (theRCompany.LastContacted.Year > 1899) {
                tbLastContacted.Text = theRCompany.LastContacted.ToShortDateString();
            }
            if (theRCompany.LastUpdated.Year > 1899) {
                tbLastUpdated.Text = theRCompany.LastUpdated.ToShortDateString();
            }

            if ((theRCompany.Registered > DateTime.MinValue) && (theRCompany.Registered < DateTime.MaxValue)) {
                tbRegistered.Text = theRCompany.Registered.ToShortDateString();
            }

            tbHomePage.Text = theRCompany.HomePage;

            tbUniqueID.Text = theRCompany.UniqueID;
            try {
                ddLegalForm.SelectedValue = theRCompany.RegistrationFormID.ToString();
            } catch (Exception) {}
            tbVAT.Text = theRCompany.VAT;

            try {
                foreach (PhoneNumber myNumber in theRCompany.PNumbers) {
                    if (CigConfig.Configure("lookupsettings.workCode") == Convert.ToString(myNumber.NumberTypeID)) {
                        tbTel.Text = myNumber.Number;
                    } else if (CigConfig.Configure("lookupsettings.mobileCode") == Convert.ToString(myNumber.NumberTypeID)) {
                        tbMobile.Text = myNumber.Number;
                    } else if (CigConfig.Configure("lookupsettings.faxCode") == Convert.ToString(myNumber.NumberTypeID)) {
                        tbFax.Text = myNumber.Number;
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(pageName + "::InitOtherBoxes() -" + err.Message + "--" + err.StackTrace, true);
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }
        /* hmm �etta er ekki alveg a� gera sig	
		private void AddEnterEvent() 
		{
			System.Web.UI.Control frm = FindControl("BasicInfoForm");
			foreach(System.Web.UI.Control ctrl in frm.Controls)
			{
				if(ctrl is TextBox)
				{
					((TextBox)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");;
				}
				if(ctrl is DropDownList)
				{
					((DropDownList)ctrl).Attributes.Add("onkeypress", "checkEnterKey();");;
				}
			}
			
		}
	*/

        public void DateValidate(object source, ServerValidateEventArgs value) {
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        private void btnReg_Click(object sender, EventArgs e) {
//			bool isOK = true;
            if (Page.IsValid) {
                try {
                    theRCompany = new ReportCompanyBLLC
                                  {
                                      RegisterCIID = ((int) Session["UserCreditInfoID"]),
                                      Address = GetAddress(),
                                      Email = tbEmail.Text
                                  };
                    if (tbEstablished.Text != "") {
                        theRCompany.Established = Convert.ToDateTime(tbEstablished.Text);
                    }

                    theRCompany.FormerNameNative = tbFormerName.Text;
                    theRCompany.TradeNameNative = tbTradeName.Text;
                    theRCompany.NameNative = tbName.Text;
                    theRCompany.FormerNameEN = txtFormerNameEN.Text;
                    theRCompany.NameEN = txtNameEN.Text;
                    theRCompany.TradeNameEN = txtTradeNameEN.Text;
                    theRCompany.Symbol = txtSymbol.Text;

                    // Cyprus specific
                    IFormatProvider format = CultureInfo.CurrentCulture;
                    if (tbLAR.Text != "") {
                        //	if (this.tbRegDate.Text != "")
                        //		myClaim.RegDate = DateTime.Parse(this.tbRegDate.Text,format, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                        theRCompany.LAR = DateTime.Parse(tbLAR.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbLR.Text != "") {
                        theRCompany.LR = DateTime.Parse(tbLR.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }
                    if (tbStructureReportResearchDate.Text != "") {
                        theRCompany.StructureReportResearchDate = DateTime.Parse(
                            tbStructureReportResearchDate.Text, format, DateTimeStyles.AllowWhiteSpaces);
                    }

                    theRCompany.HomePage = tbHomePage.Text;
                    theRCompany.InternalComments = tbComments.Text;
                    theRCompany.InternalCommentsEN = txtCommentEN.Text;
                    if (tbLastContacted.Text != "") {
                        theRCompany.LastContacted = Convert.ToDateTime(tbLastContacted.Text);
                    }
                    if (tbLastUpdated.Text != "") {
                        theRCompany.LastUpdated = Convert.ToDateTime(tbLastUpdated.Text);
                    }
                    theRCompany.LegalForm = Convert.ToInt32(ddLegalForm.SelectedValue);

                    if (tbRegistered.Text.Trim() != "") {
                        theRCompany.Registered = Convert.ToDateTime(tbRegistered.Text);
                    }

                    theRCompany.UniqueID = tbUniqueID.Text;
                    theRCompany.RegistrationFormID = Convert.ToInt32(ddLegalForm.SelectedValue);
                    theRCompany.CompanyCIID = CIID;

                    theRCompany.PNumbers = new ArrayList();

                    PhoneNumber thePhoneNum;

                    if (tbTel.Text.Length > 0) {
                        thePhoneNum = new PhoneNumber
                                      {
                                          Number = tbTel.Text,
                                          NumberTypeID = Convert.ToInt32(CigConfig.Configure("lookupsettings.workCode"))
                                      };
                        theRCompany.PNumbers.Add(thePhoneNum);
                    }
                    if (tbMobile.Text.Length > 0) {
                        thePhoneNum = new PhoneNumber
                                      {
                                          Number = tbMobile.Text,
                                          NumberTypeID =
                                              Convert.ToInt32(CigConfig.Configure("lookupsettings.mobileCode"))
                                      };
                        theRCompany.PNumbers.Add(thePhoneNum);
                    }
                    if (tbFax.Text.Length > 0) {
                        thePhoneNum = new PhoneNumber
                                      {
                                          Number = tbFax.Text,
                                          NumberTypeID = Convert.ToInt32(CigConfig.Configure("lookupsettings.faxCode"))
                                      };
                        theRCompany.PNumbers.Add(thePhoneNum);
                    }

                    theRCompany.VAT = tbVAT.Text;

                    theRCompany.ContactPerson.FirstNameNative = txtContactFirstName.Text;
                    theRCompany.ContactPerson.LastNameNative = txtContactLastName.Text;
                    theRCompany.ContactPerson.FirstNameEN = txtContactFirstNameEN.Text;
                    theRCompany.ContactPerson.LastNameEN = txtContactLastNameEN.Text;
                    theRCompany.ContactPerson.EmailAddress = txtContactEmail.Text;
                    theRCompany.ContactPerson.Position = txtContactPosition.Text;
                    theRCompany.ContactPerson.MessengerID = txtContactMessengerID.Text;
                    theRCompany.ContactPerson.PhoneNumber = txtContactPhoneNumber.Text;
                    theRCompany.ContactPerson.MobileNumber = txtContactMobileNumber.Text;
                    theRCompany.ContactPerson.FaxNumber = txtContactFaxNumber.Text;
                    if (txtContactDateContacted.Text != "") {
                        theRCompany.ContactPerson.DateContacted = Convert.ToDateTime(txtContactDateContacted.Text);
                    }

                    // �arf a� taka tele/gsm og fax n�mer
                    //	myFact.UpdateCompanyFromBasicInfo();
                    if (myFact.AddReportCompany(theRCompany)) {
                        if (Session["CIID"] == null) // if new registration and CIID hasn't been stored in session yet
                        {
                            Session["CIID"] = theRCompany.CompanyCIID;
                        }

                        lblRegMsg.Text = btnReg.CommandName == "Update" ? "The information has been updated" : "The information has been registed";
                        lblRegMsg.CssClass = "confirm_text";
                        lblRegMsg.Visible = true;

//						if(isOK)
//						{
                        Session["ReportCompany"] = theRCompany;
                        Session["ReportExists"] = true;
                        btnContinue.Enabled = true;
                        //	Server.Transfer("Operation.aspx");
//						}
                    } else {
                        lblRegMsg.Text = btnReg.CommandName == "Update" ? "Update failed" : "Insert failed";
                        lblRegMsg.CssClass = "error_text";
                        lblRegMsg.Visible = true;
                    }
                } catch (Exception err) {
                    Logger.WriteToLog("BasicInformation.aspx" + " btnContinue_Click, MSG: " + err.Message, true);
//					isOK = false;
                }
            }
        }

        private ArrayList GetAddress() {
            var myList = new ArrayList();

            //Address 1 -------------------------------
            var myAddress = new Address
                            {
                                PostBox = tbPostbox.Text,
                                PostalCode = tbAreaCode.Text,
                                CityID = Convert.ToInt32(ddCity.SelectedValue),
                                StreetNative = tbAddress.Text,
                                StreetEN = txtAddressEN.Text,
                                IsTradingAddress = false,
                                CountryId = (ddCountryList.SelectedItem.Value == "-1") ? null : new Nullable<int>(int.Parse(ddCountryList.SelectedItem.Value))
                            };

            myList.Add(myAddress);

            //Trading address ------------------------
            myAddress = new Address {PostBox = txtTradingPostBox.Text, PostalCode = txtTradingPostalCode.Text};

            if (ddlTradingCity.SelectedValue.Trim() != "") {
                myAddress.CityID = Convert.ToInt32(ddlTradingCity.SelectedValue);
            }
            myAddress.StreetNative = txtTradingAddress.Text;
            myAddress.StreetEN = txtTradingAddressEN.Text;
            myAddress.IsTradingAddress = true;

            // avoid adding non value Addresses
            if (myAddress.StreetNative != "") {
                myList.Add(myAddress);
            }

            return myList;
        }

        private void LocalizeText() {
            lblBasicInfo.Text = rm.GetString("txtBasicInfo", ci);
            lblLegalForm.Text = rm.GetString("txtLegalForm", ci);
            lblName.Text = rm.GetString("txtName", ci);
            lblNameEN.Text = rm.GetString("txtNameEN", ci);
            lblUniqueID.Text = rm.GetString("txtUniqueID", ci);
            lblSymbol.Text = rm.GetString("txtSymbol", ci);
            lblTradeName.Text = rm.GetString("txtTradeName", ci);
            lblTradeNameEN.Text = rm.GetString("txtTradeNameEN", ci);
            lblFormerName.Text = rm.GetString("txtFormerName", ci);
            lblFormerNameEN.Text = rm.GetString("txtFormerNameEN", ci);
            lblEmail.Text = rm.GetString("txtEmail", ci);
            lblTel.Text = rm.GetString("txtTelephone", ci);
            lblMobile.Text = rm.GetString("txtMobile", ci);
            lblFax.Text = rm.GetString("txtFax", ci);

            lblAddress.Text = rm.GetString("txtAddress", ci);
            lblAddressEN.Text = rm.GetString("txtAddressEN", ci);
            lblAreaCode.Text = rm.GetString("txtAreaCode", ci);
            lblPostBox.Text = rm.GetString("txtPostbox", ci);
            lblCity.Text = rm.GetString("txtCity", ci);

            lblTradingAddressHeader.Text = rm.GetString("txtTradingAddressHeader", ci);
            lblTradingAddress.Text = rm.GetString("txtTradingAddress", ci);
            lblTradingAddressEN.Text = rm.GetString("txtAddressEN", ci);
            lblTradingCity.Text = rm.GetString("txtCity", ci);
            lblTradingPostalCode.Text = rm.GetString("txtAreaCode", ci);
            lblTradingPostBox.Text = rm.GetString("txtPostbox", ci);

            lblHomePage.Text = rm.GetString("txtHomepage", ci);
            lblPostBox.Text = rm.GetString("txtPostbox", ci);
            lblEstablished.Text = rm.GetString("txtEstablished", ci);
            lblLastContacted.Text = rm.GetString("txtLastContacted", ci);
            lblLastUpdated.Text = rm.GetString("txtLastUpdated", ci);
            lblComments.Text = rm.GetString("txtCommentsWillNotBeIncludedInTheReport", ci);
            lblInternalCommentEN.Text = rm.GetString("txtCommentsWillNotBeIncludedInTheReportEN", ci);
            //	this.btnReg.Text = rm.GetString("txtRegister",ci);
            CustomValidator1.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            CustomValidator2.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            CustomValidator3.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            RequiredFieldValidator1.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator2.ErrorMessage = rm.GetString("txtValueMissing", ci);
            RequiredFieldValidator4.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            rfvTradingCity.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            btnCancel.Text = rm.GetString("txtCancel", ci);
//			this.btnCancel.Text = rm.GetString("txtBack",ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            lblVAT.Text = rm.GetString("txtVAT", ci);

            lblContactDateContacted.Text = rm.GetString("txtLastContacted", ci);
            lblContactEmail.Text = rm.GetString("txtEmail", ci);
            lblContactFaxNumber.Text = rm.GetString("txtFax", ci);
            lblContactMobileNumber.Text = rm.GetString("txtMobile", ci);
            lblContactPhoneNumber.Text = rm.GetString("txtTelephone", ci);
            lblContactInformation.Text = rm.GetString("txtContactInformation", ci);
            lblContactFirstName.Text = rm.GetString("txtFirstName", ci);
            lblContactLastName.Text = rm.GetString("txtSurName", ci);
            lblContactFirstNameEN.Text = rm.GetString("txtFirstNameEN", ci);
            lblContactLastNameEN.Text = rm.GetString("txtSurNameEN", ci);

            lblContactMessengerID.Text = rm.GetString("txtMessengerID", ci);
            lblContactPosition.Text = rm.GetString("txtPosition", ci);

            lblCitySearch.Text = rm.GetString("txtSearch", ci);
            lblTradingCitySearch.Text = rm.GetString("txtSearch", ci);
            lblRegistered.Text = rm.GetString("txtRegistered", ci);

            lblAddressHeader.Text = rm.GetString("txtAddress", ci);
            lblAdditionalInfoHeader.Text = rm.GetString("txtAdditionalInfo", ci);
            lblCommentsHeader.Text = rm.GetString("txtCommentsHeader", ci);

            lblCountry.Text = rm.GetString("txtCountry", ci);

        }

        private void btnCancel_Click(object sender, EventArgs e) {
            //	Server.Transfer("Default.aspx");

            if (btnReg.CommandName == "Update") {
                return;
            } else {
                ClearBoxes();
            }
        }

        private void ClearBoxes() {
            txtFormerNameEN.Text = "";
            txtNameEN.Text = "";
            txtTradeNameEN.Text = "";
            txtSymbol.Text = "";
            tbAddress.Text = "";
            txtAddressEN.Text = "";
            tbAreaCode.Text = "";
            tbPostbox.Text = "";
            txtTradingAddress.Text = "";
            txtTradingAddressEN.Text = "";
            txtTradingPostalCode.Text = "";
            txtTradingPostBox.Text = "";
            tbComments.Text = "";
            lblInternalCommentEN.Text = "";
            tbEmail.Text = "";
            tbEstablished.Text = "";
            tbRegistered.Text = "";
            tbFax.Text = "";
            tbFormerName.Text = "";
            tbHomePage.Text = "";
            tbLastContacted.Text = "";
            tbHomePage.Text = "";
            tbLastContacted.Text = "";
            tbLastUpdated.Text = "";
            tbMobile.Text = "";
            tbName.Text = "";
            tbTel.Text = "";
            tbTradeName.Text = "";
            tbUniqueID.Text = "";
            // drop down boxes
            ddCity.SelectedIndex = 0;
            ddlTradingCity.SelectedIndex = 0;
            ddLegalForm.SelectedIndex = 0;

            txtContactDateContacted.Text = "";
            txtContactEmail.Text = "";
            txtContactFaxNumber.Text = "";
            txtContactFirstName.Text = "";
            txtContactFirstNameEN.Text = "";
            txtContactLastName.Text = "";
            txtContactLastNameEN.Text = "";
            txtContactMessengerID.Text = "";
            txtContactMobileNumber.Text = "";
            txtContactPhoneNumber.Text = "";
            txtContactPosition.Text = "";
        }

        private void AddEnterEvent() {
            tbAddress.Attributes.Add("onkeypress", "checkEnterKey();");
            tbAreaCode.Attributes.Add("onkeypress", "checkEnterKey();");
            tbComments.Attributes.Add("onkeypress", "checkEnterKey();");
            tbEmail.Attributes.Add("onkeypress", "checkEnterKey();");
            tbEstablished.Attributes.Add("onkeypress", "checkEnterKey();");
            tbRegistered.Attributes.Add("onkeypress", "checkEnterKey();");
            tbFax.Attributes.Add("onkeypress", "checkEnterKey();");
            tbFormerName.Attributes.Add("onkeypress", "checkEnterKey();");
            tbHomePage.Attributes.Add("onkeypress", "checkEnterKey();");
            tbLastContacted.Attributes.Add("onkeypress", "checkEnterKey();");
            tbHomePage.Attributes.Add("onkeypress", "checkEnterKey();");
            tbLastContacted.Attributes.Add("onkeypress", "checkEnterKey();");
            tbLastUpdated.Attributes.Add("onkeypress", "checkEnterKey();");
            tbMobile.Attributes.Add("onkeypress", "checkEnterKey();");
            tbName.Attributes.Add("onkeypress", "checkEnterKey();");
            tbPostbox.Attributes.Add("onkeypress", "checkEnterKey();");
            tbTel.Attributes.Add("onkeypress", "checkEnterKey();");
            tbTradeName.Attributes.Add("onkeypress", "checkEnterKey();");
            tbUniqueID.Attributes.Add("onkeypress", "checkEnterKey();");
            ddCity.Attributes.Add("onkeypress", "checkEnterKey();");
            ddLegalForm.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        private void btnCitySearch_Click(object sender, EventArgs e) {
            if (txtCitySearch.Text.Trim() != "") {
                DataSet dsCity = myFact.GetCityListAsDataSet(txtCitySearch.Text, !nativeCult);
                BindCityList(dsCity);
            }
        }

        private void btnTradingCitySearch_Click(object sender, EventArgs e) {
            if (txtTradingCitySearch.Text.Trim() != "") {
                DataSet dsCity = myFact.GetCityListAsDataSet(txtTradingCitySearch.Text, !nativeCult);
                BindTradingCityList(dsCity);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.CustomValidator4.ServerValidate += new ServerValidateEventHandler(this.DateValidate);
            this.CustomValidator5.ServerValidate += new ServerValidateEventHandler(this.DateValidate);
            this.CustomValidator6.ServerValidate += new ServerValidateEventHandler(this.DateValidate);
            this.btnCitySearch.Click += new EventHandler(this.btnCitySearch_Click);
            this.btnTradingCitySearch.Click += new EventHandler(this.btnTradingCitySearch_Click);
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnContinue.Click += new EventHandler(this.btnContinue_Click);
            this.btnReg.Click += new EventHandler(this.btnReg_Click);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}