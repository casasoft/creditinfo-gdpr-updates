#region

using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;

#endregion

namespace CPI {
    /// <summary>
    /// Summary description for KeyEmployees.
    /// </summary>
    public class KeyEmployees : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnContinue;
        private int CIID = 1;
        private DataSet dsBoardMembers;
        private DataSet dsPrincipals;
        private DataSet dsStaffCount;
        protected DataGrid dtgrBoardMOverview;
        protected DataGrid dtgrPrincipals;
        protected DataGrid dtgrStaffCountOverview;
        protected HyperLink hlRegBoardMembers;
        protected HyperLink hlRegStaff;
        protected HyperLink hlRegStaffCount;
        protected Label lblBoardMembersOverview;
        protected Label lblError;
        protected Label lblKeyEmployees;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        protected Label lblPrincipalsOverview;
        protected Label lblRegStaffCount;
        protected HtmlTable outerGridTable;
        protected HtmlTable Table1;
        protected HtmlTable Table2;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "KeyEmployees";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new"); 
                //Server.Transfer("SearchForReport.aspx?redirect=KeyEmployees");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            FillDataSets();
            lblError.Visible = false;
            SetGridHeaders();
            if (!IsPostBack) {
                InitBoardMembersGrid();
                BindBoardMembersGrid();
                InitPrincipalsGrid();
                BindPrincipalsGrid();
                BindStaffCountGrid();
                LocalizeText();
            }
        }

        private void FillDataSets() {
            if (Session["dsDetailedBoardMembers"] == null) {
                dsBoardMembers = myFact.GetDetailedBoardAsDataSet(CIID);
                Session["dsDetailedBoardMembers"] = dsBoardMembers;
            } else {
                dsBoardMembers = (DataSet) Session["dsDetailedBoardMembers"];
            }

            if (Session["dsDetailedPrincipals"] == null) {
                dsPrincipals = myFact.GetDetailedPrincipalsAsDataSet(CIID);
                Session["dsDetailedPrincipals"] = dsPrincipals;
            } else {
                dsPrincipals = (DataSet) Session["dsDetailedPrincipals"];
            }

            if (Session["dsStaffCount"] == null) {
                dsStaffCount = myFact.GetStaffCountAsDataSet(CIID);
                Session["dsStaffCount"] = dsStaffCount;
            } else {
                dsStaffCount = (DataSet) Session["dsStaffCount"];
            }
        }

        private void BindBoardMembersGrid() {
            //	dsBoardMembers = myFact.GetBoardPrincipalsAsDataSet(CIID,true);
            dtgrBoardMOverview.DataSource = dsBoardMembers;
            dtgrBoardMOverview.DataBind();
        }

        private void BindPrincipalsGrid() {
            //	dsPrincipals = myFact.GetBoardPrincipalsAsDataSet(CIID,false);
            dtgrPrincipals.DataSource = dsPrincipals;
            dtgrPrincipals.DataBind();
        }

        private void BindStaffCountGrid() {
            dsStaffCount.Tables[0].DefaultView.Sort = "Year DESC";
            dtgrStaffCountOverview.DataSource = dsStaffCount.Tables[0].DefaultView;
            dtgrStaffCountOverview.DataBind();
        }

        private void InitBoardMembersGrid() {
            dtgrBoardMOverview.Columns[2].Visible = false;
            dtgrBoardMOverview.Columns[3].Visible = false;
            dtgrBoardMOverview.Columns[4].Visible = false;
            dtgrBoardMOverview.Columns[5].Visible = false;
            dtgrBoardMOverview.Columns[7].Visible = false;
            dtgrBoardMOverview.Columns[8].Visible = false;
        }

        private void InitPrincipalsGrid() {
            dtgrPrincipals.Columns[2].Visible = false;
            dtgrPrincipals.Columns[3].Visible = false;
            dtgrPrincipals.Columns[4].Visible = false;
            dtgrPrincipals.Columns[5].Visible = false;
            dtgrPrincipals.Columns[7].Visible = false;
            dtgrPrincipals.Columns[8].Visible = false;
            dtgrPrincipals.Columns[10].Visible = false;
            dtgrPrincipals.Columns[11].Visible = false;
        }

        private void btnRegBoardmembers_Click(object sender, EventArgs e) { Server.Transfer("RegBoardMembers.aspx"); }
        private void btnRegStaff_Click(object sender, EventArgs e) { Server.Transfer("Principals.aspx"); }
        private void btnRegStaffCount_Click(object sender, EventArgs e) { Server.Transfer("EmployeesCount.aspx"); }

        private void dtgrBoardMOverview_ItemDataBound(object sender, DataGridItemEventArgs e) {
            // this.dtgrBoardMOverview.Columns[7].Visible = false;
            // this.dtgrBoardMOverview.Columns[8].Visible = false;
            if (e.Item.ItemIndex >= 0) {
                if (nativeCult) {
                    e.Item.Cells[0].Text = e.Item.Cells[2].Text + " " + e.Item.Cells[3].Text;
                    e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                } else {
                    e.Item.Cells[0].Text = e.Item.Cells[4].Text + " " + e.Item.Cells[5].Text;
                    e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                }
            }
        }

        private void dtgrBoardMOverview_ItemCommand(object source, DataGridCommandEventArgs e) {
            int rowToDelete = -1;
            if (e.CommandName == "Delete") {
                rowToDelete = e.Item.ItemIndex;
            }
            if (rowToDelete >= 0) {
                // b�a til einhvern k��a sem hendir �r griddinu
                dsBoardMembers.Tables[0].Rows[rowToDelete].Delete();
                BindBoardMembersGrid();
            }
        }

        private void dtgrPrincipalsOverview_ItemCommand(object source, DataGridCommandEventArgs e) {
            int rowToDelete = -1;
            if (e.CommandName == "Delete") {
                rowToDelete = e.Item.ItemIndex;
            }
            if (rowToDelete >= 0) {
                dsPrincipals.Tables[0].Rows[rowToDelete].Delete();
                BindPrincipalsGrid();
            }
        }

        private void dtgrStaffCountOverview_EditCommand(object source, DataGridCommandEventArgs e) {
            dtgrStaffCountOverview.EditItemIndex = e.Item.ItemIndex;
            BindStaffCountGrid();
        }

        private void dtgrStaffCountOverview_CancelCommand(object source, DataGridCommandEventArgs e) {
            dtgrStaffCountOverview.EditItemIndex = -1;
            BindStaffCountGrid();
        }

        private void dtgrStaffCountOverview_UpdateCommand(object source, DataGridCommandEventArgs e) {
            var countText = (TextBox) e.Item.Cells[1].Controls[0];

            // Retrieve the updated values.
            string year = e.Item.Cells[0].Text;
            string count = countText.Text;

            // Create a DataView and specify the field to sort by.
            var StaffCountView = new DataView(dsStaffCount.Tables[0]);
            StaffCountView.Sort = "Year";

            // Remove the old entry and clear the row filter.
            StaffCountView.RowFilter = "Year='" + year + "'";

            if (StaffCountView.Count > 0) {
                StaffCountView.Delete(0);
            }
            StaffCountView.RowFilter = "";

            // Add the new entry.
            DataRow dr = dsStaffCount.Tables[0].NewRow();
            dr[1] = year;
            dr[2] = count;

            dsStaffCount.Tables[0].Rows.Add(dr);
            // h�r �arfa a� sortera upp � n�tt vegna �ess a� adda�a datarowin kemur aftast.
            dsStaffCount.Tables[0].DefaultView.Sort = "Year DESC";

            dtgrStaffCountOverview.EditItemIndex = -1;
            BindStaffCountGrid();
        }

        private void dtgrPrincipals_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemIndex >= 0) {
                if (nativeCult) {
                    e.Item.Cells[0].Text = e.Item.Cells[2].Text + " " + e.Item.Cells[3].Text;
                    e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                } else {
                    e.Item.Cells[0].Text = e.Item.Cells[4].Text + " " + e.Item.Cells[5].Text;
                    e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                }
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }

        private void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtInformationEntry", ci);
            lblKeyEmployees.Text = rm.GetString("txtKeyEmployees", ci);
            //this.lblOverview.Text = rm.GetString("txtOverviews",ci);
            lblBoardMembersOverview.Text = rm.GetString("txtBoardMembers", ci);
            hlRegBoardMembers.Text = rm.GetString("txtEditBoardMembers", ci);
            lblPrincipalsOverview.Text = rm.GetString("txtPrincipals", ci);
            hlRegStaff.Text = rm.GetString("txtEditKeyStaff", ci);
            lblRegStaffCount.Text = rm.GetString("txtRegistedStaffCount", ci);
            hlRegStaffCount.Text = rm.GetString("txtEditStaffCount", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
        }

        private void SetGridHeaders() {
            // the Boardmembers grid
            dtgrBoardMOverview.Columns[0].HeaderText = rm.GetString("txtName", ci);
            dtgrBoardMOverview.Columns[1].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrBoardMOverview.Columns[6].HeaderText = rm.GetString("txtTitle", ci);
            // the Principalsgrid
            dtgrPrincipals.Columns[0].HeaderText = rm.GetString("txtName", ci);
            dtgrPrincipals.Columns[1].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrPrincipals.Columns[6].HeaderText = rm.GetString("txtTitle", ci);
            dtgrPrincipals.Columns[9].HeaderText = rm.GetString("txtEducation", ci);
            // the Regsited staff count year
            dtgrStaffCountOverview.Columns[0].HeaderText = rm.GetString("txtYear", ci);
            dtgrStaffCountOverview.Columns[1].HeaderText = rm.GetString("txtStaffCount", ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dtgrBoardMOverview.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrBoardMOverview_ItemDataBound);
            this.dtgrPrincipals.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrPrincipals_ItemDataBound);
            this.dtgrStaffCountOverview.CancelCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrStaffCountOverview_CancelCommand);
            this.dtgrStaffCountOverview.EditCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrStaffCountOverview_EditCommand);
            this.dtgrStaffCountOverview.UpdateCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrStaffCountOverview_UpdateCommand);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}