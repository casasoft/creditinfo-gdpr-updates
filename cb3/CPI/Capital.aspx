<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Page language="c#" Codebehind="Capital.aspx.cs" AutoEventWireup="false" Inherits="CPI.Capital" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Capital</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>
			function checkShareDescriptionMaxLength(){
				if(Capital.txtSharesDescription.value.length > 250){
					return false;
				}
			}
		</script>
	</HEAD>
	<body>
		<form id="Capital" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td><asp:label id="lblCapital" runat="server">Capital</asp:label></td>
																	<td align="right"><asp:label id="lblPageStep" runat="server">Step</asp:label></td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td style="WIDTH: 33%"><asp:label id="lblAuthorizedCapital" runat="server">Authorized capital</asp:label><br>
																		<asp:textbox id="txtAuthorizedCapital" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="cvAuthorizedCapital" runat="server" controltovalidate="txtAuthorizedCapital"
																			errormessage="Error">*</asp:customvalidator></td>
																	<td style="WIDTH: 33%"><asp:label id="lblIssuedNoOfShares" runat="server">Issued number of shares</asp:label><br>
																		<asp:textbox id="txtIssuedNoOfShares" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="cvIssuedNumberOfShares" runat="server" controltovalidate="txtIssuedNoOfShares"
																			errormessage="Error">*</asp:customvalidator></td>
																	<td><asp:label id="lblNominalNumberOfShares" runat="server">Nominal number of shares</asp:label><br>
																		<asp:textbox id="txtNominalNumberOfShares" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="cvNominalNumberOfShares" runat="server" controltovalidate="txtNominalNumberOfShares"
																			errormessage="Error">*</asp:customvalidator></td>
																</tr>
																<tr>
																	<td><asp:label id="lblAsked" runat="server">Asked</asp:label><br>
																		<asp:textbox id="txtAsked" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="cvAsked" runat="server" controltovalidate="txtAsked" errormessage="Error" Enabled="False">*</asp:customvalidator></td>
																	<td><asp:label id="lblPaidUp" runat="server">Paid up</asp:label><br>
																		<asp:textbox id="txtPaidUp" runat="server" maxlength="13"></asp:textbox><asp:customvalidator id="cvPaidUp" runat="server" controltovalidate="txtPaidUp" errormessage="Error">*</asp:customvalidator></td>
																	<td><asp:label id="lblShareClass" runat="server">Share class</asp:label><br>
																		<asp:textbox id="txtShareClass" runat="server" maxlength="255"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="HEIGHT: 61px" vAlign="top"><asp:label id="lblCurrency" runat="server">Currency</asp:label><br>
																		<asp:dropdownlist id="ddlstCurrency" runat="server"></asp:dropdownlist></td>
																	<td colSpan="2" style="HEIGHT: 61px"><asp:label id="lblSharesDescription" runat="server">Shares description</asp:label><br>
																		<asp:textbox id="txtSharesDescription" runat="server" maxlength="255" cssclass="long_input" TextMode="MultiLine"
																			Height="56px" onKeyPress="return checkShareDescriptionMaxLength()"></asp:textbox></td>
																	<td style="HEIGHT: 61px"></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblErrorMessage" runat="server" cssclass="error_text">Error</asp:label><asp:validationsummary id="ValidationSummary" runat="server"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel"></asp:button><asp:button id="btnLoadFromCompanyRegistry" runat="server" cssclass="gray_button" text="Load"
																causesvalidation="False"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" text="Continue"></asp:button><asp:button id="btnReg" runat="server" cssclass="confirm_button" text="Register"></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
