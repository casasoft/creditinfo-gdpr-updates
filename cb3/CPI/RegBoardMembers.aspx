<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Page language="c#" Codebehind="RegBoardMembers.aspx.cs" AutoEventWireup="false" Inherits="CPI.RegBoardMembers" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>RegBoardMembers</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						BoardMembersForm.btnReg.click(); 
					}
				} 
				
				function checkSearchEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						BoardMembersForm.btnSearch.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.BoardMembersForm.tbFirstName.focus();
				}

		</script>
	</HEAD>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="BoardMembersForm" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colSpan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr vAlign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr vAlign="top">
								<td vAlign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colSpan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td><asp:label id="lblBoardMembers" runat="server">Board members</asp:label></td>
																	<td align="right"><asp:label id="lblPageStep" runat="server">Step</asp:label></td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td style="WIDTH: 25%"><asp:radiobutton id="rbtnIndividual" runat="server" checked="True" groupname="CustomerType" autopostback="True"
																			cssclass="radio"></asp:radiobutton><asp:image id="imgIndividual" runat="server" imageurl="../img/individual.gif"></asp:image>&nbsp;
																	</td>
																	<td style="WIDTH: 25%"><asp:radiobutton id="rbtnCompany" runat="server" groupname="CustomerType" autopostback="True" cssclass="radio"></asp:radiobutton><asp:image id="imgCompany" runat="server" imageurl="../img/company.gif"></asp:image></td>
																	<td style="WIDTH: 25%"></td>
																	<td style="WIDTH: 25%"></td>
																</tr>
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lblCIID" runat="server">CIID</asp:label><br>
																		<asp:textbox id="txtCIID" runat="server" maxlength="13"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblPersonalID" runat="server">Personal ID</asp:label><br>
																		<asp:textbox id="tbPersonalID" runat="server" maxlength="100"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" cssclass="label" font-size="Smaller"
																			controltovalidate="tbPersonalID" errormessage="Personal ID missing">*</asp:requiredfieldvalidator></td>
																	<td style="WIDTH: 25%"><asp:label id="lblFirstName" runat="server">First name</asp:label><br>
																		<asp:textbox id="tbFirstName" runat="server" maxlength="50"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" controltovalidate="tbFirstName" errormessage="First name missing!">*</asp:requiredfieldvalidator></td>
																	<td><asp:label id="lbSurName" runat="server">Surname</asp:label><br>
																		<asp:textbox id="tbSurname" runat="server" maxlength="50"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lblAddress2" runat="server">Address</asp:label><br>
																		<asp:textbox id="txtAddress" runat="server" maxlength="50"></asp:textbox></td>
																	<td><asp:label id="lblCity" runat="server">City</asp:label><br>
																		<asp:textbox id="txtCity" runat="server" maxlength="50"></asp:textbox></td>
																	<td style="HEIGHT: 42px" align="right" colSpan="2"><asp:button id="btnSearch" runat="server" cssclass="search_button" causesvalidation="False"
																			text="Search"></asp:button>&nbsp; <input class="popup" onclick="GoNordurNidur('txtCIID','tbPersonalID','tbFirstName','tbSurname',document.BoardMembersForm.rbtnIndividual, 670, 900);"
																			type="button" value="...">
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<th>
															<asp:label id="lblSubHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellSpacing="0" cellPadding="0">
																<tr>
																	<td style="WIDTH: 33%" colSpan="2"><asp:label id="lblManagementPosition" runat="server" width="145px">Management position</asp:label><br>
																		<asp:dropdownlist id="ddManagementPosition" runat="server">
																			<asp:listitem value="No particular">No particular</asp:listitem>
																		</asp:dropdownlist></td>
																	<td style="WIDTH: 33%" colSpan="2"></td>
																	<td colSpan="2"></td>
																</tr>
																<tr id="Td1" runat="server">
																	<td colSpan="3"><asp:label id="lblHistoryNative" runat="server">History Native</asp:label><br>
																		<asp:textbox id="tbHistoryNative" runat="server" maxlength="500" width="100%" height="150px"
																			textmode="MultiLine"></asp:textbox></td>
																	<td colSpan="3"><asp:label id="lblHistoryEN" runat="server">History EN</asp:label><br>
																		<asp:textbox id="tbHistoryEN" runat="server" maxlength="500" width="100%" height="150px" textmode="MultiLine"></asp:textbox></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr id="trNameSearchGrid" runat="server">
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" cellSpacing="0" cellPadding="0">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellSpacing="0" cellPadding="0">
																<tr>
																	<td>
																		<div class="TA" id="divNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dtgrNameSearch" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;"
																						commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="CreditInfoID" headertext="Creditinfo ID">
																						<headerstyle width="9%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="Number" headertext="National ID">
																						<headerstyle width="11%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Name">
																						<headerstyle width="30%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Address">
																						<headerstyle width="30%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetNative" headertext="AddressNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetEN" headertext="AddressEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="City">
																						<headerstyle width="20%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityNative" headertext="CityNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityEN" headertext="CityEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="isCompany" headertext="isCompany">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellSpacing="0">
													<tr>
														<td align="left"><asp:label id="lblMessage" runat="server" cssclass="confirm_text">Message</asp:label><asp:validationsummary id="ValidationSummary1" runat="server" font-size="Smaller" width="100%" height="22px"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancel" runat="server" cssclass="cancel_button" causesvalidation="False"
																text="Cancel"></asp:button><asp:button id="btnLoadFromCompanyRegistry" runat="server" cssclass="gray_button" causesvalidation="False"
																text="Load"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" causesvalidation="False"
																text="Continue"></asp:button><asp:button id="btnRegHistory" runat="server" cssclass="confirm_button" causesvalidation="False"
																text="Reg. history" commandname="Insert"></asp:button><asp:button id="btnReg" runat="server" cssclass="confirm_button" text="Reg."></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="Table1" cellSpacing="0" cellPadding="0" runat="server">
													<tr>
														<td align="right"><asp:label id="lblBoardMOverviewDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblBoardMOverviewDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellSpacing="0" cellPadding="0">
																<tr>
																	<td><asp:datagrid id="dtgrBoardMOverview" runat="server" cssclass="grid" gridlines="None" autogeneratecolumns="False">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;/&gt;"
																					commandname="Update">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;/&gt;"
																					commandname="Delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:boundcolumn datafield="CreditInfoID" headertext="CreditInfoID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Number" headertext="ID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Name">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Title">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameNative" headertext="FirstNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameNative" headertext="SurNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameEN" headertext="FirstNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameEN" headertext="SurNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="TitleNative" headertext="TitleNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="TitleEN" headertext="TitleEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																		</asp:datagrid></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgColor="#000000" colSpan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
