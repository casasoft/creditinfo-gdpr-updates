#region

using System;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CPI.Localization;

#endregion

using Cig.Framework.Base.Configuration;


namespace CPI {
    /// <summary>
    /// Summary description for Staff.
    /// </summary>
    public class Staff : Page {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected DataGrid dtgrBoardMOverview;
        protected DataGrid dtgrPrincipalsOverview;
        protected DataGrid dtgrStaffCountOverview;
        protected HyperLink hlRegBoardMembers;
        protected HyperLink hlRegStaff;
        protected HyperLink hlRegStaffCount;
        protected Label lblBoardMembersOverview;
        protected Label lblErrMsg;
        protected Label lblKeyStaff;
        protected Label lblPageTitle;
        protected Label lblPrincipalsOverview;
        protected Label lblStaffCountOverview;
        private bool nativeCult;
        public bool NativeCult { get { return nativeCult; } }

        private void Page_Load(object sender, EventArgs e) {
            // check the current culture
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            String nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            SetGridHeaders();
            if (!IsPostBack) {
                BindBoardMembersGrid();
                BindPrincipalsGrid();
                BindStaffCountGrid();
                LocalizeText();
            }
        }

        private void BindBoardMembersGrid() {
            if (Session["dsBoardMembers"] == null) {
                // gera fyrirspurn...
            }
        }

        private void BindPrincipalsGrid() {
            if (Session["dsPrincipals"] == null) {
                // gera fyrirspurn...
            }
        }

        private void BindStaffCountGrid() {
            if (Session["dsStaffCount"] == null) {
                // gera fyrirspurn...
            }
        }

        private void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtInformationEntry", ci);
            lblKeyStaff.Text = rm.GetString("txtKeyStaff", ci);
            hlRegBoardMembers.Text = rm.GetString("txtRegBoardmember", ci);
            hlRegStaff.Text = rm.GetString("txtKeyStaff", ci);
            hlRegStaffCount.Text = rm.GetString("txtRegStaffCount", ci);
            lblBoardMembersOverview.Text = rm.GetString("txtBoardMembersOverview", ci);
            lblPrincipalsOverview.Text = rm.GetString("txtPrincipalsOverview", ci);
            lblStaffCountOverview.Text = rm.GetString("txtStaffCountOverview", ci);
        }

        private void SetGridHeaders() {
            // Board members grid
            dtgrBoardMOverview.Columns[0].HeaderText = rm.GetString("txtName", ci);
            dtgrBoardMOverview.Columns[1].HeaderText = rm.GetString("txtID", ci);
            dtgrBoardMOverview.Columns[2].HeaderText = rm.GetString("txtAction", ci);
            // principals Overview grid
            dtgrPrincipalsOverview.Columns[0].HeaderText = rm.GetString("txtName", ci);
            dtgrPrincipalsOverview.Columns[1].HeaderText = rm.GetString("txtID", ci);
            dtgrPrincipalsOverview.Columns[2].HeaderText = rm.GetString("txtAction", ci);
            // staff count overview
            dtgrStaffCountOverview.Columns[0].HeaderText = rm.GetString("txtYear", ci);
            dtgrStaffCountOverview.Columns[1].HeaderText = rm.GetString("txtStaffCountOverview", ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ID = "StaffForm";
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}