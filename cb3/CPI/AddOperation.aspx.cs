#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.Localization;

using Cig.Framework.Base.Configuration;


#endregion

namespace CPI {
    /// <summary>
    /// Summary description for AddOperation.
    /// </summary>
    public class AddOperation : Page {
        public static CultureInfo ci;
        // the page steps
        private const string pageName = "AddOperation.aspx";
        public static ResourceManager rm;
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnAdd;
        protected Button btnCancel;
        private int CIID = -1;
        public string currentPageStep;
        protected ImageButton ibtnAddNace;
        protected ImageButton ibtnMoveDown;
        protected ImageButton ibtnMoveUp;
        protected ImageButton ibtnRemoveNace;
        protected Label lblErrMsg;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        protected ListBox lbxCompanyNaceCodes;
        protected ListBox lbxNACECodes;
        private bool nativeCult;
        public string totalPageSteps;

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = CigConfig.Configure("lookupsettings.PageIDAddOperation.aspx");

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new");
                //Server.Transfer("SearchForReport.aspx?redirect=AddOperation");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            // check the current culture
            String culture = Thread.CurrentThread.CurrentCulture.Name;
            String nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }
            /*
			 // check the current culture
			String culture = Thread.CurrentThread.CurrentCulture.Name;
			String nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
			rm = CIResource.CurrentManager;
			ci = Thread.CurrentThread.CurrentCulture;
			if (culture.Equals(nativeCulture))
				nativeCult = true;	 
			*/

            lblErrMsg.Visible = false;
            if (!IsPostBack) {
                if (Request.QueryString["former"] != null) {
                    Session["FormerOperation"] = true;
                } else {
                    Session["FormerOperation"] = null;
                }
                InitControls();
            }
        }

        private void InitControls() {
            BindOperationBoxes();
            SetPageSteps();
            LocalizeText();
        }

        private void btnCancel_Click(object sender, EventArgs e) { Server.Transfer("Operation.aspx"); }

        public void BindOperationBoxes() {
            /*	DataSet dsCountries = Cache["dsCountries"];
			if(dsCountries.Tables.Count == 0) 
			{
				dsCountries = theCPIDAL.GetCountriesAsDataSet();
				Cache["dsCountries"] = dsCountries;
			}
		*/
            DataSet dsOperation;
            if (Cache["dsOperation"] == null) {
                dsOperation = myFact.GetOperationAsDataSet();
                Cache["dsOperation"] = dsOperation;
            } else {
                dsOperation = (DataSet) Cache["dsOperation"];
            }
            if (dsOperation.Tables.Count > 0) {
                lbxNACECodes.DataSource = dsOperation;
                lbxNACECodes.DataTextField = nativeCult ? "CodeAndDescriptionNative" : "CodeAndDescriptionEN";
                lbxNACECodes.DataValueField = "NaceCodeID";
                lbxNACECodes.DataBind();

                //Now load nace codes for the company
                DataSet currNaceCodes = Session["FormerOperation"] != null ? myFact.GetCompanyOperationAsDataSet(CIID, false) : myFact.GetCompanyOperationAsDataSet(CIID, true);
                if (currNaceCodes.Tables.Count > 0) {
                    lbxCompanyNaceCodes.DataSource = currNaceCodes;
                    lbxCompanyNaceCodes.DataTextField = nativeCult ? "DescriptionNative" : "DescriptionEN";
                    lbxCompanyNaceCodes.DataValueField = "NaceCodeID";
                    lbxCompanyNaceCodes.DataBind();
                }
            } else {
                lblErrMsg.Text = "Could not get Operation overview";
                lblErrMsg.Visible = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            if (Session["FormerOperation"] != null) {
                myFact.AddCompanyOperation(GetSelectedOperationItems(), false, CIID);
            } else {
                myFact.AddCompanyOperation(GetSelectedOperationItems(), true, CIID);
            }
            /*ArrayList arrCBFormerOperation = new ArrayList();
			ArrayList arrCB = new ArrayList();
				
			foreach (ListItem item in lbxCompanyNaceCodes.Items)
			{
				CheckBoxValues theCBL = new CheckBoxValues();
				theCBL.Value = item.Value;;
				theCBL.Checked = true;
				theCBL.Text = item.Text;
				if(Session["FormerOperation"] != null)
					arrCBFormerOperation.Add(theCBL);
				else
					arrCB.Add(theCBL);
			}

			if(Session["FormerOperation"] != null)
				Session["arrCBFormerOperation"] = arrCBFormerOperation;
			else
				Session["arrCB"] = arrCB;*/
            Server.Transfer("Operation.aspx");
        }

        public ArrayList GetSelectedOperationItems() {
            var co = new ArrayList();
            int index = -1;
            for (int i = 0; i < lbxCompanyNaceCodes.Items.Count; i++) {
                co.Add(lbxCompanyNaceCodes.Items[i].Value);
            }
            return co;
        }

        private void SetPageSteps() {
            // get the totalPageSteps
            if (Cache["TotalPageSteps"] != null) {
                totalPageSteps = (string) Cache["TotalPageSteps"];
            } else {
                totalPageSteps = CigConfig.Configure("lookupsettings.TotalPageSteps");
                Cache["TotalPageSteps"] = totalPageSteps;
            }
            // get current page step
            currentPageStep = CigConfig.Configure(pageName);
        }

        private void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtAddOperation", ci);
            /*if(Session["FormerOperation"] != null)
				this.lblOperation.Text = rm.GetString("txtFormerOperation",ci);
			else
				this.lblOperation.Text = rm.GetString("txtCurrentOperation",ci);*/

            lblErrMsg.Text = rm.GetString("txtErrorOnPage", ci);
            btnAdd.Text = rm.GetString("txtAdd", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
        }

        private void ibtnAddNace_Click(object sender, ImageClickEventArgs e) {
            foreach (ListItem item in lbxNACECodes.Items) {
                if (item.Selected) {
                    if (lbxCompanyNaceCodes.Items.Count < 5) {
                        if (lbxCompanyNaceCodes.Items.FindByValue(item.Value) == null) {
                            lbxCompanyNaceCodes.Items.Add(item);
                        }
                    }
                }
            }
        }

        private void ibtnRemoveNace_Click(object sender, ImageClickEventArgs e) {
            var toDelete = new ArrayList();
            int index = 0;
            //First - get index of all items to delete
            foreach (ListItem item in lbxCompanyNaceCodes.Items) {
                if (item.Selected) {
                    toDelete.Add(index);
                }
                index++;
            }

            //ok - now we have the index - the delete items in reversed order
            for (int i = toDelete.Count - 1; i >= 0; i--) {
                lbxCompanyNaceCodes.Items.RemoveAt((int) toDelete[i]);
            }
        }

        private void ibtnMoveUp_Click(object sender, ImageClickEventArgs e) {
            var toMove = new ArrayList();
            int index = 0;
            //First - get index of all items to delete
            foreach (ListItem item in lbxCompanyNaceCodes.Items) {
                if (item.Selected) {
                    toMove.Add(index);
                }
                index++;
            }

            //ok - now we have the index - the delete items in reversed order
            for (int i = 0; i < toMove.Count; i++) {
                if ((int) toMove[i] > 0) {
                    ListItem itm = lbxCompanyNaceCodes.Items[(int) toMove[i]];
                    lbxCompanyNaceCodes.Items.RemoveAt((int) toMove[i]);
                    lbxCompanyNaceCodes.Items.Insert((int) toMove[i] - 1, itm);
                }
            }
        }

        private void ibtnMoveDown_Click(object sender, ImageClickEventArgs e) {
            var toMove = new ArrayList();
            int index = 0;
            //First - get index of all items to delete
            foreach (ListItem item in lbxCompanyNaceCodes.Items) {
                if (item.Selected) {
                    toMove.Add(index);
                }
                index++;
            }

            //ok - now we have the index - the delete items in reversed order
            for (int i = toMove.Count - 1; i >= 0; i--) {
                if ((int) toMove[i] < lbxCompanyNaceCodes.Items.Count - 1) {
                    ListItem itm = lbxCompanyNaceCodes.Items[(int) toMove[i]];
                    lbxCompanyNaceCodes.Items.RemoveAt((int) toMove[i]);
                    lbxCompanyNaceCodes.Items.Insert((int) toMove[i] + 1, itm);
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ibtnAddNace.Click += new System.Web.UI.ImageClickEventHandler(this.ibtnAddNace_Click);
            this.ibtnRemoveNace.Click += new System.Web.UI.ImageClickEventHandler(this.ibtnRemoveNace_Click);
            this.ibtnMoveUp.Click += new System.Web.UI.ImageClickEventHandler(this.ibtnMoveUp_Click);
            this.ibtnMoveDown.Click += new System.Web.UI.ImageClickEventHandler(this.ibtnMoveDown_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}