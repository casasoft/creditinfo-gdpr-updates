#region

using System;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;

using Cig.Framework.Base.Configuration;

#endregion

namespace CPI {
    /// <summary>
    /// Summary description for ReportSelection.
    /// </summary>
    public class ReportSelection : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        protected HtmlTableCell blas;
        protected Button btnNextPage;
        private int CIID;
        protected DropDownList ddlstFileFormat;
        private DataSet dsReportYear = new DataSet();
        protected DataGrid dtgrYearSource;
        protected Label lblErrorMsg;
        protected Label lblLanguage;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        protected Label lblReportExpired;
        protected Label lblSendAsFormat;
        protected Label lblYear;
        protected RadioButtonList rblLanguage;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "ReportSelection";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                //Server.Transfer("SearchForReport.aspx?redirect=ReportSelection");
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            lblReportExpired.Visible = false;

            SetGridHeaders();
            FillDataSets();
            if (!IsPostBack) {
                BindBoxes();
                InitDropDownBox();
                LocalizeText();
            }
            //Check if report has expired
            CheckIfReportIsExpired();
        }

        private void InitDropDownBox() {
            ddlstFileFormat.DataSource = myFact.GetFileFormats();
            ddlstFileFormat.DataTextField = nativeCult ? "NameNative" : "NameEN";
            ddlstFileFormat.DataValueField = "ID";
            ddlstFileFormat.DataBind();

            //Set the default file format
            try {
                ddlstFileFormat.SelectedValue = "3"; //CigConfig.Configure("lookupsettings.CRDefaultFileFormat"];
            } catch (Exception) {}
        }

        private void CheckIfReportIsExpired() {
            lblReportExpired.Visible = false;
            ReportCompanyBLLC report = myFact.GetCompanyReport("", CIID, false);

            if (report.ExpireDate < DateTime.Now) {
                lblReportExpired.Visible = true;
                //btnNextPage.Enabled=false;
            } else if (report.LastContacted <
                       DateTime.Now.AddMonths(
                           -int.Parse(CigConfig.Configure("lookupsettings.monthsReportExpiresAfterLastContacted").Trim()))) {
                lblReportExpired.Visible = true;
                //btnNextPage.Enabled=false;
            } else {
                lblReportExpired.Visible = false;
                btnNextPage.Enabled = true;
            }
        }

        private void FillDataSets() {
            // bara a� fylla year setti� ... e�a hva�
            FillReportYearSet();
            //dsReportYear 
        }

        private void FillReportYearSet() { dsReportYear = myFact.GetAvailableReportYearsAsDataSet(CIID); }
        private void BindBoxes() { BindYearSourceGrid(); }

        private void BindYearSourceGrid() {
            dtgrYearSource.DataSource = dsReportYear;
            dtgrYearSource.DataBind();
        }

        private void LocalizeText() {
            lblPageTitle.Text = rm.GetString("txtReport", ci);
            lblLanguage.Text = rm.GetString("txtLanguage", ci);
            rblLanguage.Items[0].Text = rm.GetString("txtNative", ci);
            rblLanguage.Items[1].Text = rm.GetString("txtEnglish", ci);
            lblYear.Text = rm.GetString("txtYear", ci);
            btnNextPage.Text = rm.GetString("txtViewReport", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            lblErrorMsg.Text = rm.GetString("txtNoAccountSelected", ci);
            lblReportExpired.Text = rm.GetString("txtReportExpired", ci);
            lblSendAsFormat.Text = rm.GetString("txtFormat", ci);
        }

        private void SetGridHeaders() {
            dtgrYearSource.Columns[1].HeaderText = rm.GetString("txtYear", ci);
            dtgrYearSource.Columns[2].HeaderText = rm.GetString("txtSource", ci);
            dtgrYearSource.Columns[5].HeaderText = rm.GetString("txtAccountPeriod", ci);
        }

        private void dtgrYearSource_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header) {
                e.Item.Cells[2].Text = nativeCult ? e.Item.Cells[3].Text : e.Item.Cells[4].Text;
            }
        }

        private void btnNextPage_Click(object sender, EventArgs e) {
            lblErrorMsg.Visible = false;
            string lang = null;
            string reportType = CigConfig.Configure("lookupsettings.CreditInfoReportType");
            string years = null;
            string afs_id = "";
            if (rblLanguage.Items[0].Selected) {
                lang = rblLanguage.Items[0].Value;
            } else {
                lang = rblLanguage.Items[1].Value;
            }

            years = GetCheckedYears();
            afs_id = GetAFSIDs();
            Session["ReportType"] = reportType;
            Session["ReportCulture"] = lang;
            Session["Years"] = years;
            Session["AFSID"] = afs_id;
            Session["CompanyCIID"] = CIID;

            if (ddlstFileFormat.SelectedValue == "3") {
                Server.Transfer(Application.Get("AppPath") + "/CR/Report.aspx");
            } else {
                Session["ReportFormat"] = ddlstFileFormat.SelectedValue;
                Server.Transfer(Application.Get("AppPath") + "/CR/DisplayExport.aspx");
            }
        }

        private string GetCheckedYears()
        {
            var years = new StringBuilder();
            CheckBox cbYear;
            bool first = true;
            foreach (DataGridItem dgi in dtgrYearSource.Items)
            {
                cbYear = (CheckBox)dgi.Cells[0].Controls[1];
                if (cbYear.Checked)
                {
                    years.Append(dgi.Cells[1].Text);
                    if (!first)
                    {
                        years.Append(","); // Add the comma
                    }
                    first = false;
                }
            }
            return years.ToString();
        }

        private string GetAFSIDs() {
            var afs_id = new StringBuilder();
            CheckBox cbAccount;
            bool first = true;
            foreach (DataGridItem dgi in dtgrYearSource.Items) {
                cbAccount = (CheckBox) dgi.Cells[0].Controls[1];
                if (cbAccount.Checked) {
                    if (!first) {
                        afs_id.Append(","); // Add the comma
                    }
                    afs_id.Append(dgi.Cells[6].Text);
                    first = false;
                }
            }
            return afs_id.ToString();
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dtgrYearSource.ItemDataBound += new DataGridItemEventHandler(this.dtgrYearSource_ItemDataBound);
            this.btnNextPage.Click += new EventHandler(this.btnNextPage_Click);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}