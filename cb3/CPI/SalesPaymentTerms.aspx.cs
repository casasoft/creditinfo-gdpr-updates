#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.Localization;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for SalesPaymentTerms.
    /// </summary>
    public class SalesPaymentTerms : Page {
        public static CultureInfo ci;
        private const string pageName = "SalesPaymentTerms.aspx";
        public static ResourceManager rm;
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnAdd;
        protected Button btnCancel;
        protected CheckBoxList chklSalesPaymentsTerms;
        // addet
        //bool sales = false;
        private int CIID = -1;
        public string currentPageStep;
        protected Label lblErrMsg;
        protected Label lblPageStep;
        protected Label lblPageTitle;
        private bool nativeCult;
        public string totalPageSteps;
        public int CIID_ { get { return CIID; } }

        private void Page_Load(object sender, EventArgs e) {
            Page.ID = CigConfig.Configure("lookupsettings.PageIDSalesPaymentTerms.aspx");

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new"); 
                //Server.Transfer("SearchForReport.aspx?redirect=SalesPaymentTerms");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            // check the current culture
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            lblErrMsg.Visible = false;
            if (!IsPostBack) {
                if (Request.QueryString["sales"] != null) {
                    //sales = true;
                    btnAdd.CommandName = "sales";
                }
                if (Request.QueryString["payment"] != null) {
                    //sales = false;
                    btnAdd.CommandName = "payment";
                }
                InitControls();
            }
        }

        private void InitControls() {
            BindTradeBoxes();
            SetPageSteps();
            LocalizeText();
        }

        public void BindTradeBoxes() {
            // h�r �arf a� t�kka hvort um s� a� r��a Trade e�a Sales
            bool sales = false;
            if (btnAdd.CommandName == "sales") {
                sales = true;
            }

            DataSet dsTradeTypes = GetTradeSet(sales);

            if (dsTradeTypes.Tables.Count > 0) {
                chklSalesPaymentsTerms.DataSource = dsTradeTypes;
                chklSalesPaymentsTerms.DataTextField = nativeCult ? "TermsDescriptionNative" : "TermsDescriptionEN";
                chklSalesPaymentsTerms.DataValueField = "TermsID";
                chklSalesPaymentsTerms.DataBind();
            } else {
                lblErrMsg.Text = rm.GetString("txtCouldNotGetTradeTermsOverview", ci);
                    //"Could not get Trade terms overview";
                lblErrMsg.Visible = true;
            }
        }

        private DataSet GetTradeSet(bool sales) {
            if (sales) {
                DataSet dsSalesTerms;
                if (Cache["dsSalesTerms"] == null) {
                    dsSalesTerms = myFact.GetTradeTermsAsDataSet(sales);
                    Cache["dsSalesTerms"] = dsSalesTerms;
                } else {
                    dsSalesTerms = (DataSet) Cache["dsSalesTerms"];
                }
                return dsSalesTerms;
            }
            DataSet dsPaymentTerms;
            if (Cache["dsPaymentTerms"] == null) {
                dsPaymentTerms = myFact.GetTradeTermsAsDataSet(sales);
                Cache["dsPaymentTerms"] = dsPaymentTerms;
            } else {
                dsPaymentTerms = (DataSet) Cache["dsPaymentTerms"];
            }
            return dsPaymentTerms;
        }

        private void SetPageSteps() {
            // get the totalPageSteps
            if (Cache["TotalPageSteps"] != null) {
                totalPageSteps = (string) Cache["TotalPageSteps"];
            } else {
                totalPageSteps = CigConfig.Configure("lookupsettings.TotalPageSteps");
                Cache["TotalPageSteps"] = totalPageSteps;
            }
            // get current page step
            currentPageStep = CigConfig.Configure(pageName);
        }

        private void LocalizeText() {
            lblPageTitle.Text = btnAdd.CommandName == "sales" ? rm.GetString("txtAddSalesTerms", ci) : rm.GetString("txtAddPaymentsTerms", ci);
            btnAdd.Text = rm.GetString("txtAdd", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            // Einhvernveginn svona

            var arrCBTradeTerms = new ArrayList();

            int countChecked = 0;
            for (int i = 0; i < chklSalesPaymentsTerms.Items.Count; i++) {
                if (!chklSalesPaymentsTerms.Items[i].Selected) {
                    continue;
                }
                countChecked++;
                var theCBL = new CheckBoxValues
                                        {
                                            Value = chklSalesPaymentsTerms.Items[i].Value,
                                            Checked = chklSalesPaymentsTerms.Items[i].Selected,
                                            Text = chklSalesPaymentsTerms.Items[i].Text
                                        };
                arrCBTradeTerms.Add(theCBL);
            }
            if (btnAdd.CommandName == "sales") {
                Session["arrCBSalesTerms"] = arrCBTradeTerms;
            } else {
                Session["arrCBPaymentTerms"] = arrCBTradeTerms;
            }
            Server.Transfer("Operation.aspx");
        }

        private void btnCancel_Click(object sender, EventArgs e) { Server.Transfer("Operation.aspx"); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}