#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using Image=System.Web.UI.WebControls.Image;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for Auditors.
    /// </summary>
    public class Auditors : BaseCPIPage {
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnReg;
        protected Button btnSearch;
        private int CIID = -1;
        protected HtmlGenericControl divNameSearch;
        protected DataGrid dtgrAuditors;
        protected DataGrid dtgrNameSearch;
        protected CPIFactory factory = new CPIFactory();
        protected Image imgCompany;
        protected Image imgIndividual;
        protected Label lblBuilding;
        protected Label lblCIID;
        protected Label lblDatagridHeader;
        protected Label lblDatagridIcons;
        protected Label lblFirstName;
        protected Label lblHistoryEN;
        protected Label lblHistoryNative;
        protected Label lblLaywers;
        protected Label lblMsg;
        protected Label lblNameSearchDatagridHeader;
        protected Label lblNameSearchDatagridIcons;
        protected Label lblNationalID;
        protected Label lblOfficeNumber;
        protected Label lblPageStep;
        protected Label lblSubHeader;
        protected Label lblSurname;
        protected HtmlTable outerGridTable;
        protected HtmlTable outerNameSearchGridTable;
        protected RadioButton rbtnCompany;
        protected RadioButton rbtnIndividual;
        protected RequiredFieldValidator rfvFirstName;
        protected HtmlTableCell tdNameSearchGrid;
        protected TextBox txtAddressEN;
        protected TextBox txtAddressNative;
        protected TextBox txtBuilding;
        protected TextBox txtCIID;
        protected TextBox txtFirstname;
        protected TextBox txtHistoryEN;
        protected TextBox txtHistoryNative;
        protected TextBox txtNationalID;
        protected TextBox txtOfficeNumber;
        protected TextBox txtPostalCode;
        protected TextBox txtSurname;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "Auditors";

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new"); 
                //Server.Transfer("SearchForReport.aspx?redirect=Auditors&new=true");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            lblMsg.Visible = false;
            if (!Page.IsPostBack) {
                outerNameSearchGridTable.Visible = false;

                LocalizeText();
                LocalizeAuditorsGridHeader();
                LocalizeNameSearchGridHeader();
                LoadAuditors(CIID);
                tdNameSearchGrid.Visible = false;
            }
        }

        private void LoadAuditors(int ciid) {
            DataSet dsA = factory.GetAuditorsAsDataSet(ciid);
            if (dsA != null && dsA.Tables[0] != null) {
                dtgrAuditors.DataSource = dsA;
                dtgrAuditors.DataBind();
            }
        }

        public void ClearBoxes() {
            txtBuilding.Text = "";
            txtCIID.Text = "";
            txtFirstname.Text = "";
            txtHistoryEN.Text = "";
            txtHistoryNative.Text = "";
            txtNationalID.Text = "";
            txtOfficeNumber.Text = "";
            txtSurname.Text = "";
            txtPostalCode.Text = "";
            txtAddressEN.Text = "";
            txtAddressNative.Text = "";
        }

        /// <summary>
        /// Localizes text of the webform
        /// </summary>
        private void LocalizeText() {
            lblCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblFirstName.Text = rbtnIndividual.Checked ? rm.GetString("txtFirstName", ci) : rm.GetString("txtCompany", ci);
            lblHistoryEN.Text = rm.GetString("txtHistoryEnglish", ci);
            lblHistoryNative.Text = rm.GetString("txtHistoryNative", ci);
            lblNationalID.Text = rm.GetString("txtNationalID", ci);
            lblLaywers.Text = rm.GetString("txtAuditors", ci);
            lblSurname.Text = rm.GetString("txtSurName", ci);
            lblBuilding.Text = rm.GetString("txtBuilding", ci);
            lblOfficeNumber.Text = rm.GetString("txtOfficeNumber", ci);
            //		this.btnCancel.Text = rm.GetString("txtCancel", ci);
            btnCancel.Text = rm.GetString("txtBack", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            btnReg.Text = rm.GetString("txtSave", ci);
            rfvFirstName.ErrorMessage = rm.GetString("txtValueMissing", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            imgIndividual.AlternateText = rm.GetString("txtPerson", ci);
            imgCompany.AlternateText = rm.GetString("txtCompany", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);

            lblSubHeader.Text = rm.GetString("txtAdditionalInfo", ci);
            lblNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblDatagridHeader.Text = rm.GetString("txtAuditors", ci);
        }

        /// <summary>
        /// Localizes the search grid header
        /// </summary>
        private void LocalizeNameSearchGridHeader() {
            dtgrNameSearch.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrNameSearch.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrNameSearch.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrNameSearch.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dtgrNameSearch.Columns[9].HeaderText = rm.GetString("txtCity", ci);
        }

        /// <summary>
        /// Localizes the laywers grid header
        /// </summary>
        private void LocalizeAuditorsGridHeader() {
            dtgrAuditors.Columns[2].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrAuditors.Columns[3].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrAuditors.Columns[4].HeaderText = rm.GetString("txtName", ci);
            dtgrAuditors.Columns[11].HeaderText = rm.GetString("txtAddress", ci);
            dtgrAuditors.Columns[14].HeaderText = rm.GetString("txtOfficeNumber", ci);
            dtgrAuditors.Columns[15].HeaderText = rm.GetString("txtBuilding", ci);
        }

        /// <summary>
        /// Searches for given name in the national name database
        /// </summary>
        /// <param name="name">The name or part of name to search for</param>
        public void SearchForNames(string ciid, string nationalID, string name, string surName) {
            var uaf = new uaFactory();

            DataSet dsNameSearch;
            if (rbtnIndividual.Checked) {
                //Person
                var debtor = new Debtor();

                if (name.Trim() != null) {
                    debtor.FirstName = name;
                }
                if (surName.Trim() != null) {
                    debtor.SurName = surName;
                }
                if (ciid.Trim() != "") {
                    debtor.CreditInfo = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    debtor.IDNumber1 = nationalID;
                    debtor.IDNumber2Type = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                }
/*				if(CigConfig.Configure("lookupsettings.CompanyNationalRegistry"] == "True")
					dsNameSearch= uaf.FindCustomerInNationalAndCreditInfo(debtor);
				else
					dsNameSearch= uaf.FindCustomer(debtor);
*/
                if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                    dsNameSearch = uaf.FindCustomerInNationalAndCreditInfo(debtor);
                    // if not found in national registry (f.ex. the Cyprus Whitelist is incomplete data) then search in our db
                    if (dsNameSearch.Tables.Count > 0) {
                        if (dsNameSearch.Tables[0] == null || dsNameSearch.Tables[0].Rows == null ||
                            dsNameSearch.Tables[0].Rows.Count <= 0) {
                            dsNameSearch = uaf.FindCustomer(debtor);
                        }
                    } else {
                        dsNameSearch = uaf.FindCustomer(debtor);
                    }
                } else {
                    dsNameSearch = uaf.FindCustomer(debtor);
                }
            } else {
                //Company				
                var company = new Company();

                if (name.Trim() != null) {
                    company.NameNative = name;
                }
                if (ciid.Trim() != "") {
                    company.CreditInfoID = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    company.NationalID = nationalID;
                }
                /*{
					IDNumber idn = new IDNumber();
					idn.Number = nationalID;
					idn.NumberTypeID = int.Parse(CigConfig.Configure("lookupsettings.nationalID"]); 
					company.IDNumbers = new ArrayList();
					company.IDNumbers.Add(idn);
				}	*/
                dsNameSearch = CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True" ? uaf.FindCompanyInNationalAndCreditInfo(company) : uaf.FindCompany(company);
            }

            if (dsNameSearch.Tables.Count > 0 && dsNameSearch.Tables[0] != null) {
                if (dsNameSearch.Tables[0].Rows.Count > 0) {
                    dtgrNameSearch.DataSource = dsNameSearch;
                    dtgrNameSearch.DataBind();
                    tdNameSearchGrid.Visible = true;

                    int gridRows = dtgrNameSearch.Items.Count;
                    divNameSearch.Style["HEIGHT"] = gridRows <= 6 ? "auto" : "164px";
                    divNameSearch.Style["OVERFLOW"] = "auto";
                    divNameSearch.Style["OVERFLOW-X"] = "auto";

                    outerNameSearchGridTable.Visible = true;
                } else {
                    DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                    tdNameSearchGrid.Visible = false;
                }
            } else {
                DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                tdNameSearchGrid.Visible = false;
            }
        }

        private void dtgrNameSearch_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }

                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }
                    //Set city - Native if available, else EN
                    if (e.Item.Cells[10].Text.Trim() != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }

                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }
                    //Set city - Native if available, else EN
                    if (e.Item.Cells[11].Text.Trim() != "" && e.Item.Cells[11].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    }
                }
            }

            WebDesign.CreateExplanationIcons(dtgrNameSearch.Columns, lblNameSearchDatagridIcons, rm, ci);
        }

        private void dtgrAuditors_ItemDataBound(object sender, DataGridItemEventArgs e) {
            /*
			0-Select
			1-Delete
			2-CreditInfoID
			3-Number
			4-Name
			5-NameNative
			6-NameEN
			7-FirstNameNative
			8-FirstNameEN
			9-SurNameNative
			10-SurNameEN
			11-Street
			12-StreetNative
			13-StreetEN
			14-OfficeNumber
			15-Building
			16-HistoryNative
			17-HistoryEN
			18-Type
			*/
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //((LinkButton)e.Item.Cells[1].Controls[0]).Text = rm.GetString("txtDelete",ci);
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtUpdate",ci);

                if (e.Item.Cells[18].Text.Trim() == CigConfig.Configure("lookupsettings.individualID")) {
                    if (nativeCult) {
                        //Set Name - Native if available, else EN
                        if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[7].Text + " " + e.Item.Cells[9].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[8].Text + " " + e.Item.Cells[10].Text;
                        }

                        //Set Address - Native if available, else EN
                        if (e.Item.Cells[12].Text.Trim() != "" && e.Item.Cells[12].Text != "&nbsp;") {
                            e.Item.Cells[11].Text = e.Item.Cells[12].Text;
                        } else {
                            e.Item.Cells[11].Text = e.Item.Cells[13].Text;
                        }
                    } else {
                        //Set Name - EN if available, else native
                        if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[8].Text + " " + e.Item.Cells[10].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[7].Text + " " + e.Item.Cells[9].Text;
                        }

                        //Set Address - EN if available, else native
                        if (e.Item.Cells[13].Text.Trim() != "" && e.Item.Cells[13].Text != "&nbsp;") {
                            e.Item.Cells[11].Text = e.Item.Cells[13].Text;
                        } else {
                            e.Item.Cells[11].Text = e.Item.Cells[12].Text;
                        }
                    }
                } else {
                    if (nativeCult) {
                        //Set Name - Native if available, else EN
                        if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[5].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[6].Text;
                        }

                        //Set Address - Native if available, else EN
                        if (e.Item.Cells[12].Text.Trim() != "" && e.Item.Cells[12].Text != "&nbsp;") {
                            e.Item.Cells[11].Text = e.Item.Cells[12].Text;
                        } else {
                            e.Item.Cells[11].Text = e.Item.Cells[13].Text;
                        }
                    } else {
                        //Set Name - EN if available, else native
                        if (e.Item.Cells[6].Text.Trim() != "" && e.Item.Cells[6].Text != "&nbsp;") {
                            e.Item.Cells[4].Text = e.Item.Cells[6].Text;
                        } else {
                            e.Item.Cells[4].Text = e.Item.Cells[5].Text;
                        }

                        //Set Address - EN if available, else native
                        if (e.Item.Cells[13].Text.Trim() != "" && e.Item.Cells[13].Text != "&nbsp;") {
                            e.Item.Cells[11].Text = e.Item.Cells[13].Text;
                        } else {
                            e.Item.Cells[11].Text = e.Item.Cells[12].Text;
                        }
                    }
                }
            }

            WebDesign.CreateExplanationIcons(dtgrAuditors.Columns, lblDatagridIcons, rm, ci);
        }

        private void dtgrNameSearch_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                if (e.Item.Cells[7].Text != "&nbsp;") {
                    txtAddressNative.Text = e.Item.Cells[7].Text;
                }
                if (e.Item.Cells[8].Text != "&nbsp;") {
                    txtAddressEN.Text = e.Item.Cells[8].Text;
                }
                if (e.Item.Cells[9].Text != "&nbsp;") {
                    txtPostalCode.Text = e.Item.Cells[9].Text;
                }

                txtCIID.Text = e.Item.Cells[1].Text;
                txtNationalID.Text = e.Item.Cells[2].Text;
                if (txtNationalID.Text == "&nbsp;") {
                    txtNationalID.Text = "";
                }
                if (txtCIID.Text == "&nbsp;") {
                    txtCIID.Text = "";
                }

                string name = e.Item.Cells[3].Text.Trim();
                if (name == "&nbsp;") {
                    name = "";
                }
                string[] arrName = name.Split(' ');
                if (arrName.Length > 0) {
                    if (arrName.Length == 1) {
                        txtFirstname.Text = arrName[0];
                    } else {
                        txtSurname.Text = arrName[arrName.Length - 1];
                        txtFirstname.Text = name.Substring(0, name.Length - (txtSurname.Text.Length + 1));
                    }
                }
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }

        private void btnReg_Click(object sender, EventArgs e) {
            bool person = rbtnIndividual.Checked;
            if (!Page.IsValid) {
                return;
            }
            bool insertOK = true;
            try {
                Address myAddress = null;

                if (txtAddressNative.Text.Length > 0 || txtAddressEN.Text.Length > 0) {
                    myAddress = new Address
                                {
                                    StreetEN = txtAddressEN.Text,
                                    StreetNative = txtAddressNative.Text,
                                    PostalCode = txtPostalCode.Text,
                                    CityID = (-1)
                                };
                }

                if (person) // indivitual
                {
                    var theAuditorIndi = new AuditorIndividualBLLC();
                    if (nativeCult) {
                        theAuditorIndi.FirstNameNative = txtFirstname.Text;
                        theAuditorIndi.SurNameNative = txtSurname.Text;
                    } else {
                        theAuditorIndi.FirstNameEN = txtFirstname.Text;
                        theAuditorIndi.SurNameEN = txtSurname.Text;
                    }
                    if (txtCIID.Text.Trim() != "") {
                        theAuditorIndi.CreditInfoID = int.Parse(txtCIID.Text.Trim());
                    }
                    theAuditorIndi.OfficeNumber = txtOfficeNumber.Text;
                    theAuditorIndi.Building = txtBuilding.Text;
                    theAuditorIndi.HistoryNative = txtHistoryNative.Text;
                    theAuditorIndi.HistoryEN = txtHistoryEN.Text;
                    theAuditorIndi.NationalID = txtNationalID.Text;

                    if (myAddress != null) {
                        theAuditorIndi.Address.Add(myAddress);
                    }

                    insertOK = factory.AddAuditor(theAuditorIndi, CIID);
                } else // is company
                {
                    var theAuditorCompany = new AuditorCompanyBLLC();
                    if (nativeCult) {
                        theAuditorCompany.NameNative = txtFirstname.Text;
                    } else {
                        theAuditorCompany.NameEN = txtFirstname.Text;
                    }
                    if (txtCIID.Text.Trim() != "") {
                        theAuditorCompany.CreditInfoID = int.Parse(txtCIID.Text.Trim());
                    }
                    theAuditorCompany.OfficeNumber = txtOfficeNumber.Text;
                    theAuditorCompany.Building = txtBuilding.Text;
                    theAuditorCompany.HistoryNative = txtHistoryNative.Text;
                    theAuditorCompany.HistoryEN = txtHistoryEN.Text;
                    theAuditorCompany.NationalID = txtNationalID.Text;

                    if (myAddress != null) {
                        theAuditorCompany.Address.Add(myAddress);
                    }

                    insertOK = factory.AddAuditor(theAuditorCompany, CIID);
                }
            } catch (Exception err) {
                Logger.WriteToLog(err.ToString(), true);
                insertOK = false;
            }
            if (insertOK) {
                DisplayMessage(rm.GetString("txtTheInfoHasBeenRegisted", ci));
            } else {
                DisplayErrorMessage(rm.GetString("txtErrorHasOccurred", ci));
            }

            LoadAuditors(CIID);
            ClearBoxes();
        }

        private void rblPersComp_SelectedIndexChanged(object sender, EventArgs e) {
            if (rbtnIndividual.Checked) {
                //Person
                lblFirstName.Text = rm.GetString("txtFirstName", ci);
                lblSurname.Visible = true;
                txtSurname.Visible = true;
            } else {
                //Company
                lblFirstName.Text = rm.GetString("txtCompany", ci);
                lblSurname.Visible = false;
                txtSurname.Visible = false;
            }
        }

        private void dtgrAuditors_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Update")) {
                txtCIID.Text = e.Item.Cells[2].Text.Trim();
                txtNationalID.Text = e.Item.Cells[3].Text.Trim();

                txtOfficeNumber.Text = e.Item.Cells[14].Text.Trim();
                txtBuilding.Text = e.Item.Cells[15].Text.Trim();
                txtHistoryNative.Text = e.Item.Cells[16].Text.Trim();
                txtHistoryEN.Text = e.Item.Cells[17].Text.Trim();

                if (txtNationalID.Text == "&nbsp;") {
                    txtNationalID.Text = "";
                }
                if (txtCIID.Text == "&nbsp;") {
                    txtCIID.Text = "";
                }
                if (txtOfficeNumber.Text == "&nbsp;") {
                    txtOfficeNumber.Text = "";
                }
                if (txtBuilding.Text == "&nbsp;") {
                    txtBuilding.Text = "";
                }
                if (txtHistoryNative.Text == "&nbsp;") {
                    txtHistoryNative.Text = "";
                }
                if (txtHistoryEN.Text == "&nbsp;") {
                    txtHistoryEN.Text = "";
                }

                if (e.Item.Cells[18].Text.Trim() == CigConfig.Configure("lookupsettings.individualID")) {
                    string name = e.Item.Cells[4].Text.Trim();
                    if (name == "&nbsp;") {
                        name = "";
                    }
                    string[] arrName = name.Split(' ');
                    if (arrName.Length > 0) {
                        if (arrName.Length == 1) {
                            txtFirstname.Text = arrName[0];
                        } else {
                            txtSurname.Text = arrName[arrName.Length - 1];
                            txtFirstname.Text = name.Substring(0, name.Length - (txtSurname.Text.Length + 1));
                        }
                    }
                } else {
                    txtFirstname.Text = e.Item.Cells[4].Text.Trim();
                    if (txtFirstname.Text == "&nbsp;") {
                        txtFirstname.Text = "";
                    }
                }
            } else if (e.CommandName.Equals("Delete")) {
                int auditorCIID = -1;
                try {
                    auditorCIID = int.Parse(e.Item.Cells[2].Text.Trim());
                } catch (Exception) {
                    auditorCIID = -1;
                }
                if (auditorCIID > -1) {
                    factory.DeleteAuditor(auditorCIID, CIID);
                    LoadAuditors(CIID);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e) { TransferToPreviousPage(); }
        private void btnSearch_Click(object sender, EventArgs e) { SearchForNames(txtCIID.Text, txtNationalID.Text, txtFirstname.Text, txtSurname.Text); }

        private void DisplayMessage(String message) {
            lblMsg.Text = message;
            lblMsg.ForeColor = Color.Blue;
            lblMsg.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblMsg.Text = errorMessage;
            lblMsg.ForeColor = Color.Red;
            lblMsg.Visible = true;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rbtnIndividual.CheckedChanged += new EventHandler(this.rblPersComp_SelectedIndexChanged);
            this.rbtnCompany.CheckedChanged += new EventHandler(this.rblPersComp_SelectedIndexChanged);
            this.btnSearch.Click += new EventHandler(this.btnSearch_Click);
            this.dtgrNameSearch.ItemCommand += new DataGridCommandEventHandler(this.dtgrNameSearch_ItemCommand);
            this.dtgrNameSearch.ItemDataBound += new DataGridItemEventHandler(this.dtgrNameSearch_ItemDataBound);
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.btnContinue.Click += new EventHandler(this.btnContinue_Click);
            this.btnReg.Click += new EventHandler(this.btnReg_Click);
            this.dtgrAuditors.ItemCommand += new DataGridCommandEventHandler(this.dtgrAuditors_ItemCommand);
            this.dtgrAuditors.ItemDataBound += new DataGridItemEventHandler(this.dtgrAuditors_ItemDataBound);
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion
    }
}