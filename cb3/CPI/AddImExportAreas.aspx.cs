#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.Localization;
using DBauer.Web.UI.WebControls;
using Logging.BLL;
using Telerik.WebControls;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for AddImportAreas.
    /// </summary>
    public class AddImExportAreas : Page {
        private const int CONTINENT_LEVEL = 1;
        public static CultureInfo ci;
        private const string pageName = "AddImExportAreas.aspx";
        public static ResourceManager rm;
        private readonly ArrayList arrExportCountrieID = new ArrayList();
        private readonly ArrayList arrImportCountrieID = new ArrayList();
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnAdd;
        protected Button btnCancel;
        private int CIID = -1;
        public string currentPageStep;
        protected RadTreeView exportRadTree;
        protected RadTreeView importRadTree;
        protected Label lblError;
        protected Label lblImportAreas;
        protected Label lblMainExportAreas;
        protected Label lblPageStep;
        private bool nativeCult;
        protected HtmlTable outerGridTable;
        private const string pageClass = "AddImExportAreas.aspx";
        protected HtmlTable Table1;
        public string totalPageSteps;
        public int CIID_ { get { return CIID; } }

        private void Page_Load(object sender, EventArgs e) {
            // Add <ENTER> event
            // AddEnterEvent();
            Page.ID = CigConfig.Configure("lookupsettings.PageIDAddImExportAreas.aspx");

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=AddImExportAreas");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            // check the current culture
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            SetGridHeaders();
            string import = "";
            if (!IsPostBack) {
                if (Request.QueryString["import"] != null) {
                    import = Request.QueryString["import"];
                }
                if (import == "false") {
                    importRadTree.Enabled = false;
                    exportRadTree.Enabled = true;
                } else if (import == "true") {
                    importRadTree.Enabled = true;
                    exportRadTree.Enabled = false;
                }
                importRadTree.Sort = RadTreeViewSort.Ascending;
                exportRadTree.Sort = RadTreeViewSort.Ascending;

                InitControls();
            }
        }

        private void InitControls() {
            SetInExportAreasGrid();
            SetPageSteps();
            LocalizeText();
        }

        // Vinna �etta ofan� hierGriddi�
        public void SetInExportAreasGrid() {
            DataSet dsContinents;
            DataSet dsCountries;
            var dsCoCo = new DataSet();
            if (Session["dsContinents"] == null) {
                dsContinents = myFact.GetContinentsAsDataSet();
                dsContinents.Tables[0].TableName = "Continents";
                Session["dsContinents"] = dsContinents;
            } else {
                dsContinents = (DataSet) Session["dsContinents"];
            }
            // now the date is ordered by nativeCult. We then need to disable caching
            if (Session["dsCountries"] == null) {
                dsCountries = myFact.GetCountriesAsDataSet(nativeCult);
                dsCountries.Tables[0].TableName = "Countries";
                //		Session["dsCountries"] = dsCountries;
            } else {
                dsCountries = (DataSet) Session["dsCountries"];
            }
            if (Session["dsCoCo"] == null) {
                dsCoCo.Tables.Add(dsContinents.Tables[0].Copy());
                dsCoCo.Tables.Add(dsCountries.Tables[0].Copy());
                // start setting relations
                var dc1 = dsCoCo.Tables[0].Columns["ContinentID"];
                var dc2 = dsCoCo.Tables[1].Columns["ContinentID"];
                var dr = new DataRelation("Continent_Countrie", dc1, dc2, false);
                dsCoCo.Relations.Add(dr);
                // end setting relations
                Session["dsCoCo"] = dsCoCo;
            } else {
                dsCoCo = (DataSet) Session["dsCoCo"];
            }

            /*	DataColumn dc1;
				DataColumn dc2;
				dc1 = dsCoCo.Tables[0].Columns["ContinentID"];
				dc2 = dsCoCo.Tables[1].Columns["ContinentID"];
				DataRelation dr = new DataRelation("Continent_Countrie", dc1, dc2, false);
				dsCoCo.Relations.Add(dr);
			*/
            // set Import areas grid
            //IDictionary importNodeTable = new Hashtable(myView.Count);
            //GenerateTreeView(dsCoCo, importNodeTable, importTree);

            //Get the ROOT node (that does not have a parent), and call the RecursivelyPopulate method

            PopulateTree(dsCoCo, importRadTree);
            PopulateTree(dsCoCo, exportRadTree);
        }

        public void CheckChildControls(System.Web.UI.Control theControl, bool matchFound, bool importAreas) {
            var cbCountrie = new CheckBox();
            Label lblCountrieID;
            bool checkLabel = false;
            foreach (System.Web.UI.Control ctl in theControl.Controls) {
                if ((ctl.HasControls()) && (!matchFound)) {
                    CheckChildControls(ctl, false, importAreas);
                }

                if (ctl.GetType() == typeof (CheckBox)) {
                    cbCountrie = (CheckBox) ctl;
                    if (cbCountrie.Checked) {
                        checkLabel = true;
                    }
                }
                if (!checkLabel) {
                    continue;
                }
                if (ctl.GetType() != typeof (Label)) {
                    continue;
                }
                lblCountrieID = (Label) ctl;
                string countrieID = lblCountrieID.Text;
                // check whether import or export
                // and add to the "right" Countrie Array
                var theCBL = new CheckBoxValues
                             {
                                 Value = countrieID,
                                 Checked = cbCountrie.Checked,
                                 Text = cbCountrie.Text
                             };
                if (importAreas) {
                    //	this.arrImportCountrieID.Add(Convert.ToInt32(countrieID));
                    arrImportCountrieID.Add(theCBL);
                } else {
                    //	this.arrExportCountrieID.Add(Convert.ToInt32(countrieID));
                    arrExportCountrieID.Add(theCBL);
                }

                // only check for label right after countrie checked was found
                checkLabel = false;
            }
            return;
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            const string funcName = "btnAdd_Click";
            //int arrCount = 0;
            try {
                // check Import areas
                /*foreach(DataGridItem dgi in this.HG1.Items)
				{
					thePlaceHolder = (PlaceHolder) dgi.Cells[0].Controls[1];
					CheckChildControls(thePlaceHolder,false,true);
				}*/
                // check export areas
                /*foreach(DataGridItem dgi in this.HG2.Items)
				{
					thePlaceHolder = (PlaceHolder) dgi.Cells[0].Controls[1];
					CheckChildControls(thePlaceHolder,false,false);
				}*/
                // debug
                /*
				arrCount = this.arrImportCountrieID.Count;
				arrCount = this.arrExportCountrieID.Count;
				arrCount = 0;
			*/

                WriteTree();

                if (arrImportCountrieID.Count > 0) {
                    Session["arrImportCountrieID"] = arrImportCountrieID;
                }
                if (arrExportCountrieID.Count > 0) {
                    Session["arrExportCountrieID"] = arrExportCountrieID;
                }
            } catch (Exception err) {
                Logger.WriteToLog(pageClass + " : " + funcName + " : " + err, true);
                lblError.Visible = true;
            }
            Server.Transfer("Operation.aspx");
        }

        private void btnCancel_Click(object sender, EventArgs e) { Server.Transfer("Operation.aspx"); }

        private void SetPageSteps() {
            // get the totalPageSteps
            if (Cache["TotalPageSteps"] != null) {
                totalPageSteps = (string) Cache["TotalPageSteps"];
            } else {
                totalPageSteps = CigConfig.Configure("lookupsettings.TotalPageSteps");
                Cache["TotalPageSteps"] = totalPageSteps;
            }
            // get current page step
            currentPageStep = CigConfig.Configure(pageName);
        }

        private void LocalizeText() {
            lblImportAreas.Text = rm.GetString("txtMainImportAreas", ci);
            lblMainExportAreas.Text = rm.GetString("txtMainExportAreas", ci);
            lblError.Text = rm.GetString("txtErrorOnPage", ci);
            btnAdd.Text = rm.GetString("txtAdd", ci);
            btnCancel.Text = rm.GetString("txtCancel", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
        }

        private static void SetGridHeaders() {
            //this.HG1.Columns[0].HeaderText = rm.GetString("txtMainImportAreas",ci);
            //this.HG2.Columns[0].HeaderText = rm.GetString("txtMainExportAreas",ci);
        }

        private void PopulateTree(DataSet dsCoCo, RadTreeView tree) {
            RadTreeNode parent;

            foreach (DataRow dbRow in dsCoCo.Tables[0].Rows) {
                parent = new RadTreeNode
                         {
                             ID = dbRow["NameEN"].ToString(),
                             Text = (nativeCult ? dbRow["NameNative"].ToString() : dbRow["NameEN"].ToString())
                         };

                parent.ID = parent.ID.Replace("'", " ");
                parent.Text = parent.Text.Replace("'", "\'");

                foreach (var row in dbRow.GetChildRows("Continent_Countrie")) {
                    var child = new RadTreeNode
                                        {
                                            ID = row["CountryID"].ToString(),
                                            Text =
                                                (nativeCult
                                                     ? row["NameNative"].ToString().Replace("'", "\'")
                                                     : row["NameEN"].ToString().Replace("'", "\'"))
                                        };

                    parent.AddNode(child);

                    /*if(dbRow.GetChildRows("Continent_Countrie").Length > 0)
					{
						RecursivelyPopulate(row);
					}*/
                }

                tree.AddNode(parent);
            }
        }

        private static void GetCheckedNodeList(IList nodeList, RadTreeNodeCollection nodeCollection, int nLevel) {
            nLevel++;
            foreach (RadTreeNode node in nodeCollection) {
                if (node.Checked && nLevel != CONTINENT_LEVEL) {
                    var values = new CheckBoxValues {Checked = node.Checked, Value = node.ID, Text = node.Text};
                    nodeList.Add(values);
                }

                if (node.Nodes != null) {
                    GetCheckedNodeList(nodeList, node.Nodes, nLevel);
                }
            }
        }

        private void WriteTree() {
            GetCheckedNodeList(arrImportCountrieID, importRadTree.Nodes, 0);
            GetCheckedNodeList(arrExportCountrieID, exportRadTree.Nodes, 0);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}