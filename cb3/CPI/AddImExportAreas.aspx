<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="DBWC" Namespace="DBauer.Web.UI.WebControls" Assembly="DBauer.Web.UI.WebControls.HierarGrid" %>
<%@ Register TagPrefix="radt" Namespace="Telerik.WebControls" Assembly="RadTreeView" %>
<%@ Page language="c#" Codebehind="AddImExportAreas.aspx.cs" AutoEventWireup="false" Inherits="CPI.AddImExportAreas" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>AddImportAreas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script>

			function UpdateAllChildren(nodes, checked)
			{
				var i;
				for (i=0; i<nodes.length; i++)
				{
					if (checked)       
						nodes[i].Check();       
					else      
						nodes[i].UnCheck(); 
				      
					if (nodes[i].Nodes.length > 0)      
						UpdateAllChildren(nodes[i].Nodes, checked);      
				}
			}

			function AfterCheck(node)
			{
				if (!node.Checked && node.Parent != null)
					node.Parent.UnCheck();   
				      
				var siblingCollection = (node.Parent != null) ? node.Parent.Nodes : node.TreeView.Nodes;
				   
				var allChecked = true;
				for (var i=0; i<siblingCollection.length; i++)
				{
					if (!siblingCollection[i].Checked)
					{
						allChecked = false;
						break;
					}
				}
				if (allChecked && node.Parent != null)
				{
					node.Parent.Check();
				}
				   
				UpdateAllChildren(node.Nodes, node.Checked);
			}

		</script>
	</head>
	<body ms_positioning="GridLayout">
		<form id="ImportAreasForm" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tbody>
					<tr>
						<td colspan="4">
							<uc1:head id="Head1" runat="server"></uc1:head>
						</td>
					</tr>
					<tr valign="top">
						<td width="1"></td>
						<td>
							<table width="100%" height="100%" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td>
											<uc1:language id="Language1" runat="server"></uc1:language>
										</td>
										<td></td>
										<td align="right">
											<ucl:options id="Options1" runat="server"></ucl:options>
										</td>
									</tr>
									<tr>
										<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
									</tr>
									<tr>
										<td height="10"></td>
									</tr>
									<tr>
										<td></td>
										<td>
											<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
										</td>
										<td align="right">
											<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
										</td>
									</tr>
									<tr>
										<td height="10"></td>
									</tr>
									<tr valign="top">
										<td width="150" valign="top" align="left">
											<table width="98%">
												<tr>
													<td>
														<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="2">
											<!-- Main Body Starts -->
											<table width="100%">
												<tr>
													<td>
														<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
														<table class="grid_table" id="outerGridTable" cellspacing="0" cellpadding="0" runat="server">
															<tr>
																<th>
																	<table class="grid_table_head">
																		<tr>
																			<td>
																				<asp:label id="lblImportAreas" runat="server">Main Import areas</asp:label>
																			</td>
																			<td align="right">
																				<asp:label id="lblPageStep" runat="server">Step</asp:label>
																			</td>
																		</tr>
																	</table>
																</th>
															</tr>
															<tr>
																<td>
																	<table class="fields" cellspacing="0" cellpadding="0">
																		<tr>
																			<td>
																				<div class="tree">
																					<radt:radtreeview id="importRadTree" runat="server" causesvalidation="False" checkboxes="True" afterclientcheck="AfterCheck"></radt:radtreeview></div>
																			</td>
																		</tr>
																		<tr>
																			<td height="23"></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table> <!-- END // TABLE FOR DATAGRID // END --></td>
												</tr>
												<tr>
													<td height="10"></td>
												</tr>
												<tr>
													<td>
														<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
														<table class="grid_table" id="Table1" cellspacing="0" cellpadding="0" runat="server">
															<tr>
																<th>
																	<asp:label id="lblMainExportAreas" runat="server">Main Export areas</asp:label>
																</th>
															</tr>
															<tr>
																<td>
																	<table class="fields" cellspacing="0" cellpadding="0">
																		<tr>
																			<td>
																				<div class="tree">
																					<radt:radtreeview id="exportRadTree" runat="server" causesvalidation="False" checkboxes="True" afterclientcheck="AfterCheck"></radt:radtreeview>
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td height="23"></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<!-- END // TABLE FOR DATAGRID // END -->
													</td>
												</tr>
												<tr>
													<td height="10"></td>
												</tr>
												<tr>
													<td>
														<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
														<table class="empty_table" cellspacing="0">
															<tr>
																<td align="left">
																	<asp:label id="lblError" runat="server" cssclass="error_text" visible="False">Error on page!</asp:label>
																</td>
																<td align="right">
																	<asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel"></asp:button><asp:button id="btnAdd" runat="server" cssclass="confirm_button" text="Add"></asp:button>
																</td>
															</tr>
														</table>
														<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
													</td>
												</tr>
											</table>
											<!-- Main Body Ends --></td>
									</tr>
									<tr>
										<td height="20"></td>
									</tr>
									<tr>
										<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td width="2"></td>
					</tr>
					<tr>
						<td align="center" colspan="4">
							<uc1:footer id="Footer1" runat="server"></uc1:footer>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</body>
</html>
