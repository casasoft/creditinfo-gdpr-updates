#region

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.CIUsers;
using Image=System.Web.UI.WebControls.Image;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for RegBoardMembers.
    /// </summary>
    public class RegBoardMembers : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnLoadFromCompanyRegistry;
        protected Button btnReg;
        protected Button btnRegHistory;
        protected Button btnSearch;
        private int CIID = 1;
        protected DropDownList ddManagementPosition;
        protected HtmlGenericControl divNameSearch;
        private DataSet dsDetailedBoardMembers;
        private DataSet dsManagementPositions = new DataSet();
        protected DataGrid dtgrBoardMOverview;
        protected DataGrid dtgrNameSearch;
        protected Image imgCompany;
        protected Image imgIndividual;
        protected Label lblAddress2;
        protected Label lblBoardMembers;
        protected Label lblBoardMOverviewDatagridHeader;
        protected Label lblBoardMOverviewDatagridIcons;
        protected Label lblCIID;
        protected Label lblCity;
        protected Label lblFirstName;
        protected Label lblHistoryEN;
        protected Label lblHistoryNative;
        protected Label lblManagementPosition;
        protected Label lblMessage;
        protected Label lblNameSearchDatagridHeader;
        protected Label lblNameSearchDatagridIcons;
        protected Label lblPageStep;
        protected Label lblPersonalID;
        protected Label lblSubHeader;
        protected LinkButton lbnNameSearch;
        protected LinkButton lbnSearch;
        protected Label lbSurName;
        protected RadioButton rbtnCompany;
        protected RadioButton rbtnIndividual;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected HtmlTable Table1;
        protected TextBox tbFirstName;
        protected TextBox tbHistoryEN;
        protected TextBox tbHistoryNative;
        protected TextBox tbPersonalID;
        protected TextBox tbSurname;
        protected HtmlTableRow Td1;
        protected HtmlTableRow trNameSearchGrid;
        protected TextBox txtAddress;
        protected TextBox txtCIID;
        protected TextBox txtCity;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                //Server.Transfer("SearchForReport.aspx?redirect=RegBoardMembers");
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            pageName = "RegBoardMembers";

            PreparePageLoad();

            string toLoadFromCompanyRegistry = CigConfig.Configure("lookupsettings.EnableToLoadBoardFromCompanyRegistry");
            if (toLoadFromCompanyRegistry != null && toLoadFromCompanyRegistry.ToLower().Equals("true")) {
                btnLoadFromCompanyRegistry.Visible = true;
            } else {
                btnLoadFromCompanyRegistry.Visible = false;
            }
            // Add <ENTER> event
            AddEnterEvent();

            FillDataSets();
            SetGridHeaders();
            LocalizeNameSearchGridHeader();
            InitBoardMembersGrid();
            BindBoardMembersGrid(false);
            lblMessage.Visible = false;
            if (!IsPostBack) {
                trNameSearchGrid.Visible = false;
                InitDDBoxes();
                LocalizeText();
                SetHistoryBoxes();
            }
        }

        private void SetHistoryBoxes() {
            var theHistory = myFact.GetHistory(
                CIID, Convert.ToInt32(CigConfig.Configure("lookupsettings.BoardMembersHistoryType")));
            tbHistoryNative.Text = theHistory.HistoryNative;
            tbHistoryEN.Text = theHistory.HistoryEN;
        }

        private void FillDataSets() {
            if (Session["dsDetailedBoardMembers"] == null) {
                dsDetailedBoardMembers = myFact.GetDetailedBoardAsDataSet(CIID);
                Session["dsDetailedBoardMembers"] = dsDetailedBoardMembers;
            } else {
                dsDetailedBoardMembers = (DataSet) Session["dsDetailedBoardMembers"];
            }
        }

        private void InitBoardMembersGrid() {
            dtgrBoardMOverview.Columns[6].Visible = false;
            dtgrBoardMOverview.Columns[7].Visible = false;
            dtgrBoardMOverview.Columns[8].Visible = false;
            dtgrBoardMOverview.Columns[9].Visible = false;
            dtgrBoardMOverview.Columns[10].Visible = false;
            dtgrBoardMOverview.Columns[11].Visible = false;
        }

        private void BindBoardMembersGrid(bool reload) {
            //	dsBoardMembers = myFact.GetBoardPrincipalsAsDataSet(CIID,true);
            if (reload) {
                dsDetailedBoardMembers = myFact.GetDetailedBoardAsDataSet(CIID);
            }
            dsDetailedBoardMembers = cleanDuplicatedCIIDS(dsDetailedBoardMembers);
            dtgrBoardMOverview.DataSource = dsDetailedBoardMembers;
            dtgrBoardMOverview.DataBind();
            Session["dsDetailedBoardMembers"] = dsDetailedBoardMembers;
        }

        public void InitDDBoxes() {
            if (Session["dsManagementPositions"] == null) {
                dsManagementPositions = myFact.GetManagementPositionAsDataSet();
                Session["dsManagementPositions"] = dsManagementPositions;
            } else {
                dsManagementPositions = (DataSet) Session["dsManagementPositions"];
            }

            //	if(dsManagementPositions.Tables[0] != null) 
            //	{
            ddManagementPosition.DataSource = dsManagementPositions;
            ddManagementPosition.DataTextField = nativeCult ? "TitleNative" : "TitleEN";
            ddManagementPosition.DataValueField = "ManagementPositionID";
            ddManagementPosition.DataBind();
            //	}
        }

        private void btnReg_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                registerBoardMember(
                    txtCIID.Text,
                    tbPersonalID.Text,
                    tbFirstName.Text,
                    tbSurname.Text,
                    txtAddress.Text,
                    Convert.ToInt32(ddManagementPosition.SelectedValue));
            }
        }

        private void registerBoardMember(
            string theCIID, string nationalID, string firstName, string surName, string address, int positionID) {
            var theHistory = new HistoryBLLC();
            bool historyError = false;
            var theBoardMember = new BoardMemberBLLC();
            if (nativeCult) {
                theBoardMember.FirstNameNative = firstName;
                theBoardMember.SurNameNative = surName;
            } else {
                theBoardMember.FirstNameEN = firstName;
                theBoardMember.SurNameEN = surName;
            }

            if (theCIID.Trim() != "") {
                theBoardMember.CreditInfoID = int.Parse(theCIID);
            }
            theBoardMember.NationalID = nationalID;
            theBoardMember.SingleAddressNative = address;
            theBoardMember.ManagementPostionID = positionID;

            // take the history
            theHistory.HistoryNative = tbHistoryNative.Text;
            theHistory.HistoryEN = tbHistoryEN.Text;
            theHistory.HistoryID = Convert.ToInt32(CigConfig.Configure("lookupsettings.DirectorHistoryType"));
            if (!myFact.AddHistory(CIID, theHistory)) {
                historyError = true;
            }

            if (myFact.AddBordMember(theBoardMember, CIID)) {
                Session["dsDetailedBoardMembers"] = null;
                FillDataSets();
                BindBoardMembersGrid(false);
                ClearBoxes();
            } else {
                DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
            }
            if (historyError) {
                DisplayErrorMessage(rm.GetString("txtSavingHistoryFailed", ci));
            }
        }

        private void dtgrBoardMOverview_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    if (!(e.Item.Cells[6].Text == "") || (e.Item.Cells[6].Text == "&nbsp;")) // non native info
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[6].Text + " " + e.Item.Cells[7].Text;
                    } else // then display english rather then showing empty fields
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[8].Text + " " + e.Item.Cells[9].Text;
                    }

                    if (!(e.Item.Cells[10].Text == "") || (e.Item.Cells[10].Text == "&nbsp;")) // non native info
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[10].Text;
                    } else // then display english rather then showing empty fields
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[11].Text;
                    }
                } else {
                    if (!(e.Item.Cells[8].Text == "&nbsp;") || (e.Item.Cells[8].Text == "")) // non english info
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[8].Text + " " + e.Item.Cells[9].Text;
                    } else // then display native rather then showing empty fields
                    {
                        e.Item.Cells[4].Text = e.Item.Cells[6].Text + " " + e.Item.Cells[7].Text;
                    }
                    if (!(e.Item.Cells[11].Text == "") || (e.Item.Cells[11].Text == "&nbsp;")) // non english info
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[11].Text;
                    } else // then display native rather then showing empty fields
                    {
                        e.Item.Cells[5].Text = e.Item.Cells[10].Text;
                    }
                }

                // For delete confirmation
                var btnTemp = (LinkButton) e.Item.Cells[1].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
            }

            WebDesign.CreateExplanationIcons(dtgrBoardMOverview.Columns, lblBoardMOverviewDatagridIcons, rm, ci);
        }

        private void ClearBoxes() {
            txtCIID.Text = "";
            tbPersonalID.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            tbFirstName.Text = "";
            tbSurname.Text = "";
            ddManagementPosition.SelectedIndex = 0;
            ddManagementPosition.SelectedIndex = 0;
            // init Reg. button etc.
            btnReg.Text = rm.GetString("txtSave", ci);
            btnReg.CommandName = "Register";
            tbPersonalID.ReadOnly = false;
            txtCIID.ReadOnly = false;
            RequiredFieldValidator1.Enabled = true;
            trNameSearchGrid.Visible = false;
        }

        private void dtgrBoardMOverview_ItemCommand(object source, DataGridCommandEventArgs e) {
            int rowToDelete = -1;
            try {
                int boardMemberCIID = Convert.ToInt32(dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString());

                if (e.CommandName == "Delete") {
                    rowToDelete = e.Item.ItemIndex;
                }
                if (rowToDelete >= 0) {
                    dsDetailedBoardMembers.Tables[0].Rows[rowToDelete].Delete();
                    if (boardMemberCIID > 0) {
                        myFact.DeleteBoardMember(boardMemberCIID, CIID);
                    }
                    BindBoardMembersGrid(true);
                }
                if (e.CommandName == "Update") {
                    if (dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["isCompany"].ToString().Equals("true")) {
                        rbtnIndividual.Checked = false;
                        rbtnCompany.Checked = true;
                        lblFirstName.Text = rm.GetString("txtCompany", ci);
                        lbSurName.Visible = false;
                        tbSurname.Visible = false;
                    } else {
                        rbtnCompany.Checked = false;
                        rbtnIndividual.Checked = true;
                        lblFirstName.Text = rm.GetString("txtFirstName", ci);
                        lbSurName.Visible = true;
                        tbSurname.Visible = true;
                    }

                    // do some fancy stuff
                    txtCIID.Text = dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["CreditInfoID"].ToString();
                    tbFirstName.Text =
                        dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["FirstNameNative"].ToString();
                    tbSurname.Text = dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["SurNameNative"].ToString();
                    tbPersonalID.Text = dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["Number"].ToString();
                    try {
                        ddManagementPosition.SelectedValue =
                            dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["ManagementPositionID"].ToString();
                    } catch (Exception ex) {
                        Logger.WriteToLog("RegBoarMember.aspx : dtgrBoardMOverview_ItemCommand " + ex.Message, true);
                    }
                    txtAddress.Text = dsDetailedBoardMembers.Tables[0].Rows[e.Item.ItemIndex]["StreetNative"].ToString();
                    txtCity.Text = "";
                    btnReg.Text = rm.GetString("txtUpdate", ci);
                    btnReg.CommandName = "Update";
                    tbPersonalID.ReadOnly = true;
                    txtCIID.ReadOnly = true;
                }
            } catch (Exception err) {
                Logger.WriteToLog("RegBoarMember.aspx : dtgrBoardMOverview_ItemCommand " + err.Message, true);
            }
        }

        private void btnContinue_Click(object sender, EventArgs e) { Server.Transfer(nextPage); }
        private void btnCancel_Click(object sender, EventArgs e) { Server.Transfer(previousPage); }

        private void LocalizeText() {
            lblCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblBoardMembers.Text = rm.GetString("txtBoardMembers", ci);
            lblFirstName.Text = rm.GetString("txtFirstName", ci);
            lbSurName.Text = rm.GetString("txtSurName", ci);
            lblPersonalID.Text = rm.GetString("txtPersonalID", ci);
            lblManagementPosition.Text = rm.GetString("txtManagementPosition", ci);
            btnLoadFromCompanyRegistry.Text = rm.GetString("txtLoadBoardFromCompanyRegistry", ci);
            btnReg.Text = rm.GetString("txtSave", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);
            RequiredFieldValidator2.ErrorMessage = rm.GetString("txtFirstNameMissing", ci);
            RequiredFieldValidator1.ErrorMessage = rm.GetString("txtPersonalIDMissing", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            btnCancel.Text = rm.GetString("txtBack", ci);
            btnRegHistory.Text = rm.GetString("txtSaveHistory", ci);
            lblHistoryEN.Text = rm.GetString("txtHistoryEnglish", ci);
            lblHistoryNative.Text = rm.GetString("txtHistoryNative", ci);
            lblNameSearchDatagridHeader.Text = rm.GetString("txtResults", ci);
            lblBoardMOverviewDatagridHeader.Text = rm.GetString("txtBoardMembers", ci);
            lblSubHeader.Text = rm.GetString("txtAdditionalInfo", ci);
            lblAddress2.Text = rm.GetString("txtAddress", ci);
            lblCity.Text = rm.GetString("txtCity", ci);
            rbtnCompany.Text = rm.GetString("txtCompany", ci);
            rbtnIndividual.Text = rm.GetString("txtIndividual", ci);
        }

        private void SetGridHeaders() {
            dtgrBoardMOverview.Columns[2].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrBoardMOverview.Columns[3].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrBoardMOverview.Columns[4].HeaderText = rm.GetString("txtName", ci);
            dtgrBoardMOverview.Columns[5].HeaderText = rm.GetString("txtTitle", ci);
        }

        private void AddEnterEvent() {
            tbFirstName.Attributes.Add("onkeypress", "checkSearchEnterKey();");
            tbSurname.Attributes.Add("onkeypress", "checkSearchEnterKey();");
            txtAddress.Attributes.Add("onkeypress", "checkSearchEnterKey();");
            txtCity.Attributes.Add("onkeypress", "checkSearchEnterKey();");
            ddManagementPosition.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        /// <summary>
        /// Localizes the search grid header
        /// </summary>
        private void LocalizeNameSearchGridHeader() {
            dtgrNameSearch.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dtgrNameSearch.Columns[2].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrNameSearch.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrNameSearch.Columns[6].HeaderText = rm.GetString("txtAddress", ci);
            dtgrNameSearch.Columns[9].HeaderText = rm.GetString("txtCity", ci);
        }

        private void btnLoadFromCompanyRegistry_Click(object sender, EventArgs e) {
            string nationalId = myFact.GetNationalIDByCIID(CIID);
            if (nationalId != null) {
                DataSet ds = myFact.GetCompanyBoardFromCompanyRegistryAsDataSet(nationalId);
                if (ds.Tables.Count > 0 && ds.Tables[0] != null) {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                        //Get information about board member from CompanyRegistry
                        string firstName = "";
                        string surName = "";
                        string ssn = ds.Tables[0].Rows[i]["PersonSSN"].ToString();
                        string uName = ds.Tables[0].Rows[i]["Name"].ToString();
                        string[] arrName = uName.Split(' ');
                        if (arrName.Length > 0) {
                            if (arrName.Length == 1) {
                                firstName = arrName[0];
                            } else {
                                surName = arrName[arrName.Length - 1];
                                firstName = uName.Substring(0, uName.Length - (surName.Length + 1));
                            }
                        }
                        string address = ds.Tables[0].Rows[i]["Address"].ToString();
                        //string pcode = ds.Tables[0].Rows[i]["PostalCode"].ToString();
                        string mPosId = ds.Tables[0].Rows[i]["PositionID"].ToString();
                        //string mPos = ds.Tables[0].Rows[i]["Position"].ToString();
                        if (ssn == "" || ssn.Equals(null) || ssn == " ") {
                            ssn = ds.Tables[0].Rows[i]["PersonSSN1"].ToString();
                            firstName = ds.Tables[0].Rows[i]["NameComp"].ToString();
                        }
                        registerBoardMember("-1", ssn, firstName, surName, address, int.Parse(mPosId));
                        //Add board member to cpi
                    }
                }
            }
        }

        /// <summary>
        /// Searches for given name in the national name database
        /// </summary>
        /// <param name="name">The name or part of name to search for</param>
        /// <param name="ciid"></param>
        public void SearchForNames(
            string ciid, string nationalID, string name, string surName, string address, string city, bool compan) {
            var uaf = new uaFactory();

            var comp = new Company();
            var debtor = new Debtor();

            DataSet dsNameSearch;
            //Person
            if (!compan) {
                if (name.Trim() != "") {
                    debtor.FirstName = name;
                }
                if (surName.Trim() != "") {
                    debtor.SurName = surName;
                }
                if (ciid.Trim() != "") {
                    debtor.CreditInfo = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    debtor.IDNumber1 = nationalID;
                    debtor.IDNumber2Type = int.Parse(CigConfig.Configure("lookupsettings.nationalID"));
                }

                if ((address != null && address.Trim() != "") || (city != null && city.Trim() != "")) {
                    var addr = new Address {StreetNative = address, CityNameNative = city};
                    if (debtor.Address == null) {
                        debtor.Address = new ArrayList();
                    }
                    debtor.Address.Add(addr);
                }
            }
            //Company
            if (compan) {
                comp.NameNative = name;
                comp.NationalID = nationalID;
                comp.IsSearchable = true;

                if (name.Trim() != "") {
                    comp.NameNative = name;
                }
                if (ciid.Trim() != "") {
                    comp.CreditInfoID = int.Parse(ciid);
                }
                if (nationalID.Trim() != "") {
                    comp.NationalID = nationalID;
                }
                if ((address != null && address.Trim() != "") || (city != null && city.Trim() != "")) {
                    var addr = new Address {StreetNative = address, CityNameNative = city};
                    if (comp.Address == null) {
                        comp.Address = new ArrayList();
                    }
                    comp.Address.Add(addr);
                }
            }

            if (compan) {
                dsNameSearch = uaf.FindCompany(comp);
            } else {
                if (CigConfig.Configure("lookupsettings.CompanyNationalRegistry") == "True") {
                    dsNameSearch = uaf.FindCustomerInNationalAndCreditInfo(debtor);
                    // if not found in national registry (f.ex. the Cyprus Whitelist is incomplete data) then search in our db
                    if (dsNameSearch.Tables.Count > 0) {
                        if (dsNameSearch.Tables[0] == null || dsNameSearch.Tables[0].Rows == null ||
                            dsNameSearch.Tables[0].Rows.Count <= 0) {
                            dsNameSearch = uaf.FindCustomer(debtor);
                        }
                    } else {
                        dsNameSearch = uaf.FindCustomer(debtor);
                    }
                } else {
                    dsNameSearch = uaf.FindCustomer(debtor);
                }
            }

            if (dsNameSearch.Tables.Count > 0) {
                //if(dsNameSearch.Tables[0] !=null&& dsNameSearch.Tables.Count>0 && dsNameSearch.Tables[0].Rows!=null&&dsNameSearch.Tables[0].Rows.Count>0) 
                if (dsNameSearch.Tables[0] != null && dsNameSearch.Tables[0].Rows != null &&
                    dsNameSearch.Tables[0].Rows.Count > 0) {
                    dtgrNameSearch.DataSource = dsNameSearch;
                    dtgrNameSearch.DataBind();

                    int gridRows = dtgrNameSearch.Items.Count;
                    divNameSearch.Style["HEIGHT"] = gridRows < int.Parse(CigConfig.Configure("lookupsettings.MaxItemsNoScroll")) ? "auto" : "164px";
                    divNameSearch.Style["OVERFLOW"] = "auto";
                    divNameSearch.Style["OVERFLOW-X"] = "auto";

                    trNameSearchGrid.Visible = true;
                } else {
                    trNameSearchGrid.Visible = false;
                    DisplayMessage(rm.GetString("txtNoEntryFound", ci));
                }
            } else {
                DisplayMessage(rm.GetString("txtNoEntryFound", ci));
            }
        }

        private void DisplayMessage(String message) {
            lblMessage.Text = message;
            lblMessage.ForeColor = Color.Blue;
            lblMessage.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblMessage.Text = errorMessage;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Visible = true;
        }

        private void dtgrNameSearch_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                if (nativeCult) {
                    //Set Name - Native if available, else EN
                    if (e.Item.Cells[4].Text.Trim() != "" && e.Item.Cells[4].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    }

                    //Set Address - Native if available, else EN
                    if (e.Item.Cells[7].Text.Trim() != "" && e.Item.Cells[7].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    }

                    //Set city - Native if available, else EN
                    if (e.Item.Cells[10].Text.Trim() != "" && e.Item.Cells[10].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    }
                } else {
                    //Set Name - EN if available, else native
                    if (e.Item.Cells[5].Text.Trim() != "" && e.Item.Cells[5].Text != "&nbsp;") {
                        e.Item.Cells[3].Text = e.Item.Cells[5].Text;
                    } else {
                        e.Item.Cells[3].Text = e.Item.Cells[4].Text;
                    }

                    //Set Address - EN if available, else native
                    if (e.Item.Cells[8].Text.Trim() != "" && e.Item.Cells[8].Text != "&nbsp;") {
                        e.Item.Cells[6].Text = e.Item.Cells[8].Text;
                    } else {
                        e.Item.Cells[6].Text = e.Item.Cells[7].Text;
                    }

                    //Set city - Native if available, else EN
                    if (e.Item.Cells[11].Text.Trim() != "" && e.Item.Cells[11].Text != "&nbsp;") {
                        e.Item.Cells[9].Text = e.Item.Cells[11].Text;
                    } else {
                        e.Item.Cells[9].Text = e.Item.Cells[10].Text;
                    }
                }

                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);	
            }

            WebDesign.CreateExplanationIcons(dtgrNameSearch.Columns, lblNameSearchDatagridIcons, rm, ci);
        }

        private void dtgrNameSearch_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName.Equals("Select")) {
                if (e.Item.Cells[1].Text == "&nbsp;") {
                    txtCIID.Text = "";
                    RequiredFieldValidator1.Enabled = true;
                } else {
                    RequiredFieldValidator1.Enabled = false;
                    txtCIID.Text = e.Item.Cells[1].Text;
                }

                tbPersonalID.Text = e.Item.Cells[2].Text;
                if (tbPersonalID.Text == "&nbsp;") {
                    tbPersonalID.Text = "";
                }

                txtAddress.Text = e.Item.Cells[6].Text;
                if (txtAddress.Text == "&nbsp;") {
                    txtAddress.Text = "";
                }
                txtCity.Text = e.Item.Cells[9].Text;
                if (txtCity.Text == "&nbsp;") {
                    txtCity.Text = "";
                }

                string name = e.Item.Cells[3].Text.Trim();
                if (name == "&nbsp;") {
                    name = "";
                }
                string[] arrName = name.Split(' ');
                if (arrName.Length > 0) {
                    if (arrName.Length == 1) {
                        tbFirstName.Text = arrName[0];
                    } else {
                        tbSurname.Text = arrName[arrName.Length - 1];
                        tbFirstName.Text = name.Substring(0, name.Length - (tbSurname.Text.Length + 1));
                    }
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            if (rbtnIndividual.Checked) {
                SearchForNames(
                    txtCIID.Text,
                    tbPersonalID.Text,
                    tbFirstName.Text,
                    tbSurname.Text,
                    txtAddress.Text,
                    txtCity.Text,
                    false);
            }
            if (rbtnCompany.Checked) {
                SearchForNames(
                    txtCIID.Text,
                    tbPersonalID.Text,
                    tbFirstName.Text,
                    tbSurname.Text,
                    txtAddress.Text,
                    txtCity.Text,
                    true);
            }
        }

        private void btnRegHistory_Click(object sender, EventArgs e) {
            var theHistory = new HistoryBLLC();
            bool historyError = false;
            // take the history
            theHistory.HistoryNative = tbHistoryNative.Text;
            theHistory.HistoryEN = tbHistoryEN.Text;
            theHistory.HistoryID = Convert.ToInt32(CigConfig.Configure("lookupsettings.BoardMembersHistoryType"));
            if (!myFact.AddHistory(CIID, theHistory)) {
                historyError = true;
            }

            if (historyError) {
                DisplayErrorMessage(rm.GetString("txtSavingHistoryFailed", ci));
            }
        }

        private static DataSet cleanDuplicatedCIIDS(DataSet ds) {
            if (ds.Tables.Count > 0) {
                var arrCiids = new ArrayList();
                //	ArrayList arrShareTypesIDs = new ArrayList();
                for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                    var creditInfoId = ds.Tables[0].Rows[i][0].ToString();
                    //	string shareTypesID = ds.Tables[0].Rows[i]["OwnershipID"].ToString();
                    for (int j = arrCiids.Count - 1; j >= 0; j--) {
                        if (!((string) arrCiids[j]).Equals(creditInfoId)) {
                            continue;
                        }
                        ds.Tables[0].Rows.Remove(ds.Tables[0].Rows[i]);
                        break;
                    }
                    arrCiids.Add(creditInfoId);
                    //	arrShareTypesIDs.Add(shareTypesID);
                }
            }
            return ds;
        }

        private void rbtnIndividual_CheckedChanged(object sender, EventArgs e) {
            ClearBoxes();
            if (rbtnIndividual.Checked) {
                //Person
                rbtnCompany.Checked = false; // some VS bug! Events do not work unless this is done!				
                lblFirstName.Text = rm.GetString("txtFirstName", ci);
                lbSurName.Visible = true;
                tbSurname.Visible = true;
            } else {
                //Company
                rbtnIndividual.Checked = false; // some VS bug! Events do not work unless this is done!
                lblFirstName.Text = rm.GetString("txtCompany", ci);
                lbSurName.Visible = false;
                tbSurname.Visible = false;
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rbtnIndividual.CheckedChanged += new System.EventHandler(this.rbtnIndividual_CheckedChanged);
            this.rbtnCompany.CheckedChanged += new System.EventHandler(this.rbtnIndividual_CheckedChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.dtgrNameSearch.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrNameSearch_ItemCommand);
            this.dtgrNameSearch.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrNameSearch_ItemDataBound);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnLoadFromCompanyRegistry.Click += new System.EventHandler(this.btnLoadFromCompanyRegistry_Click);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnRegHistory.Click += new System.EventHandler(this.btnRegHistory_Click);
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            this.dtgrBoardMOverview.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrBoardMOverview_ItemCommand);
            this.dtgrBoardMOverview.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrBoardMOverview_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}