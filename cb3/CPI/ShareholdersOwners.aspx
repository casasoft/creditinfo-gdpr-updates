<%@ Page language="c#" Codebehind="ShareholdersOwners.aspx.cs" AutoEventWireup="false" Inherits="CPI.ShareholdersOwners" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>ShareholdersOwners</title>
		<% System.Web.HttpContext.Current.Response.AddHeader("Cache-Control","no-cache");%>
		<% System.Web.HttpContext.Current.Response.Expires = 0;%>
		<% System.Web.HttpContext.Current.Response.Cache.SetNoStore();%>
		<% System.Web.HttpContext.Current.Response.AddHeader("Pragma", "no-cache");%>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../DynamicFunctions.js"></script>
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						ShareholdersOwnersForm.btnReg.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.ShareholdersOwnersForm.tbFirstname.focus();
				}

		</script>
</head>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="ShareholdersOwnersForm" method="post" runat="server">
			<table height="600" width="997" align="center" border="0">
				<tr>
					<td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table height="100%" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td><uc1:language id="Language1" runat="server"></uc1:language></td>
								<td></td>
								<td align="right"><ucl:options id="Options1" runat="server"></ucl:options></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td><uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar></td>
								<td align="right"><ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td valign="top" align="left" width="150">
									<table width="98%">
										<tr>
											<td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblShareholdersOwners" runat="server">Shareholders/Owner</asp:label></td>
																	<td align="right"><asp:label id="lblPageStep" runat="server">Step</asp:label></td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td colspan="3">
																		<p>
																			<asp:radiobutton id="rbtnIndividual" runat="server" cssclass="radio" checked="True" groupname="CustomerType"></asp:radiobutton>
																			<asp:image id="imgIndividual" runat="server" imageurl="../img/individual.gif"></asp:image>&nbsp;
																			<asp:radiobutton id="rbtnCompany" runat="server" cssclass="radio" groupname="CustomerType"></asp:radiobutton>
																			<asp:image id="imgCompany" runat="server" imageurl="../img/company.gif"></asp:image></p>
																	</td>
																	<td><asp:textbox id="txtAddressEN" runat="server" visible="False" width="8px"></asp:textbox><asp:textbox id="txtAddressNative" runat="server" visible="False" width="8px"></asp:textbox><asp:textbox id="txtPostalCode" runat="server" visible="False" width="8px"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 25%"><asp:label id="lblCIID" runat="server">CIID</asp:label><br>
																		<asp:textbox id="txtCIID" runat="server" maxlength="13"></asp:textbox></td>
																	<td style="WIDTH: 25%"><asp:label id="lblNationalID" runat="server">National ID</asp:label><br>
																		<asp:textbox id="tbNationalID" runat="server" maxlength="100"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" controltovalidate="tbNationalID" errormessage="Value missing!"
																			cssclass="label">*</asp:requiredfieldvalidator></td>
																	<td style="WIDTH: 25%"><asp:label id="lblFirstName" runat="server">Firstname/Company name</asp:label><br>
																		<asp:textbox id="tbFirstname" runat="server" maxlength="100"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" controltovalidate="tbFirstname" errormessage="Value missing!"
																			cssclass="label">*</asp:requiredfieldvalidator></td>
																	<td><asp:label id="lblSurname" runat="server">Surname</asp:label><br>
																		<asp:textbox id="tbSurname" runat="server" maxlength="50"></asp:textbox></td>
																</tr>
																<tr>
																	<td align="right" colspan="4"><asp:button id="btnSearch" runat="server" cssclass="search_button" text="Search" causesvalidation="False"></asp:button>&nbsp;
																		<INPUT onclick="GoNordurNidur('txtCIID','tbNationalID','tbFirstname','tbSurname',document.ShareholdersOwnersForm.rbtnIndividual, 670, 900);"
																			type="button" value="..." class="popup"></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITHOUT STEPS // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<asp:label id="lblSubHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td style="WIDTH: 50%"><asp:label id="lblOwnership" runat="server">Ownership (shares)</asp:label><asp:label id="lblOwnershipPercentage" runat="server" Visible="False">Ownership (Percentage)</asp:label><br>
																		<asp:textbox id="tbOwnership" runat="server" maxlength="13"></asp:textbox><asp:textbox id="txtOwnershipPercentage" runat="server" maxlength="13" Visible="False"></asp:textbox><asp:customvalidator id="CustomValidator1" runat="server" controltovalidate="tbOwnership" errormessage="Error in data input!"
																			cssclass="label">*</asp:customvalidator><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" controltovalidate="tbOwnership" errormessage="Value missing!"
																			cssclass="label">*</asp:requiredfieldvalidator></td>
																	<td><asp:label id="lblOwnershipType" runat="server">Ownership type</asp:label><br>
																		<asp:dropdownlist id="ddlOwnershipType" runat="server"></asp:dropdownlist></td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITHOUT STEPS // END --></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerNameSearchGridTable" cellspacing="0" cellpadding="0"
													runat="server">
													<tr>
														<td height="10"></td>
													</tr>
													<tr>
														<td align="right"><asp:label id="lblNameSearchDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblNameSearchDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td id="tdNameSearchGrid" style="HEIGHT: 25px" align="left" colspan="8" runat="server">
																		<div class="TA" id="divNameSearch" style="OVERFLOW-X: scroll; OVERFLOW: scroll; HEIGHT: 164px"
																			runat="server"><asp:datagrid id="dtgrNameSearch" runat="server" cssclass="grid" autogeneratecolumns="False" gridlines="None">
																				<footerstyle cssclass="grid_footer"></footerstyle>
																				<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																				<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																				<itemstyle cssclass="grid_item"></itemstyle>
																				<headerstyle cssclass="grid_header"></headerstyle>
																				<columns>
																					<asp:buttoncolumn text="&lt;img src=&quot;../img/select.gif&quot; alt=&quot;Select&quot; border=&quot;0&quot;/&gt;" 
 commandname="Select">
																						<itemstyle cssclass="leftpadding"></itemstyle>
																					</asp:buttoncolumn>
																					<asp:boundcolumn datafield="CreditInfoID" headertext="Creditinfo ID">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn datafield="Number" headertext="National ID">
																						<headerstyle width="10%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Name">
																						<headerstyle width="30%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameNative" headertext="NameNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="NameEN" headertext="NameEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="Address">
																						<headerstyle width="25%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetNative" headertext="AddressNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="StreetEN" headertext="AddressEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn headertext="City">
																						<headerstyle width="20%"></headerstyle>
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityNative" headertext="CityNative">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																					<asp:boundcolumn visible="False" datafield="CityEN" headertext="CityEN">
																						<itemstyle cssclass="padding"></itemstyle>
																					</asp:boundcolumn>
																				</columns>
																				<pagerstyle cssclass="grid_pager"></pagerstyle>
																			</asp:datagrid></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // TABLE FOR DATAGRID // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left"><asp:label id="lblErrMsg" runat="server" cssclass="error_text" visible="False">Insert/Update failed</asp:label><asp:validationsummary id="ValidationSummary1" runat="server"></asp:validationsummary></td>
														<td align="right"><asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel" causesvalidation="False"></asp:button><asp:button id="btnLoadFromCompanyRegistry" runat="server" cssclass="gray_button" text="Load"
																causesvalidation="False"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" text="Continue" causesvalidation="False"></asp:button><asp:button id="btnReg" runat="server" cssclass="confirm_button" text="Reg."></asp:button></td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END --></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // TABLE FOR DATAGRID // BEGIN -->
												<table class="grid_table" id="outerShareholdersGridTable" cellspacing="0" cellpadding="0"
													runat="server">
													<tr>
														<td align="right"><asp:label id="lblShareholdersDatagridIcons" runat="server"></asp:label></td>
													</tr>
													<tr>
														<td height="5"></td>
													</tr>
													<tr>
														<th>
															<asp:label id="lblShareholdersDatagridHeader" runat="server"></asp:label></th></tr>
													<tr>
														<td>
															<table class="datagrid" cellspacing="0" cellpadding="0">
																<tr>
																	<td><asp:datagrid id="dtgrShareholders" runat="server" cssclass="grid" autogeneratecolumns="False"
																			gridlines="None">
																			<footerstyle cssclass="grid_footer"></footerstyle>
																			<selecteditemstyle cssclass="grid_selecteditem"></selecteditemstyle>
																			<alternatingitemstyle cssclass="grid_alternatingitem"></alternatingitemstyle>
																			<itemstyle cssclass="grid_item"></itemstyle>
																			<headerstyle cssclass="grid_header"></headerstyle>
																			<columns>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/edit.gif&quot; alt=&quot;Edit&quot; border=&quot;0&quot;/&gt;" 
 commandname="Update">
																					<itemstyle cssclass="leftpadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:buttoncolumn text="&lt;img src=&quot;../img/delete.gif&quot; alt=&quot;Delete&quot; border=&quot;0&quot;/&gt;" 
 commandname="Delete">
																					<itemstyle cssclass="nopadding"></itemstyle>
																				</asp:buttoncolumn>
																				<asp:boundcolumn datafield="CreditInfoID" headertext="CIID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Number" headertext="ID">
																					<headerstyle width="19%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Name">
																					<headerstyle width="40%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameNative" headertext="FirstNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameNative" headertext="SurNameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="FirstNameEN" headertext="FirstNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="SurNameEN" headertext="SurNameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Ownership" headertext="Ownership">
																					<headerstyle width="15%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Ownership_perc" headertext="Ownership">
																					<headerstyle width="15%"></headerstyle>
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="Type" headertext="Type">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameNative" headertext="NameNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn datafield="NameEN" headertext="NameEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn headertext="Ownership type">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="OwnershipDescriptionNative" headertext="OwnershipDescriptionNative">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="OwnershipDescriptionEN" headertext="OwnershipDescriptionEN">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																				<asp:boundcolumn visible="False" datafield="OwnershipID" headertext="OwnershipID">
																					<itemstyle cssclass="padding"></itemstyle>
																				</asp:boundcolumn>
																			</columns>
																			<pagerstyle cssclass="grid_pager"></pagerstyle>
																		</asp:datagrid></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // END --></td>
										</tr>
									</table>
									<!-- Main Body Ends --></td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td align="center" bgcolor="#000000" colspan="3" height="1"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</html>
