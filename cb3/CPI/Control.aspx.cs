#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for Control.
    /// </summary>
    public class Control : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        protected HtmlTableCell blas;
        protected Button btnBack;
        protected Button btnFinish;
        private int CIID;
        protected CustomValidator CustomValidator1;
        protected DropDownList ddReportStatus;
        private DataSet dsReportStatus = new DataSet();
        protected Label lblComment;
        protected Label lblCommentEN;
        protected Label lblControl;
        protected Label lblMessage;
        protected Label lblPageStep;
        protected Label lblReportsExpiry;
        protected Label lblReportStatus;
        protected TextBox tbInternalComment;
        protected TextBox tbReportsExpiryDate;
        private ReportCompanyBLLC theRCompany = new ReportCompanyBLLC();
        protected TextBox txtInternalCommentEN;
        protected ValidationSummary ValidationSummary1;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "Control";
            // Add <ENTER> event
            AddEnterEvent();

            lblMessage.Visible = false;

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                //Server.Transfer("SearchForReport.aspx?redirect=Control");
                Server.Transfer("SearchForReport.aspx?redirect=BasicInformation&action=new");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            FillDataSets();
            GetReportCompany();

            // h�r �arf einnig a� setja Reportbaunina og stilla box me� tilliti til hennar
            if (!IsPostBack) {
                BindBoxes();
                InitBoxes();
                LocalizeText();
            }
        }

        private void GetReportCompany() {
            //	public ReportCompanyBLLC GetCompanyReport(string nationalID,int CIID, bool useNationalID)
            //	theRCompany = myDALC.GetCompanyReport(CIID,0,false);
            if (Session["ReportCompany"] != null) {
                theRCompany = (ReportCompanyBLLC) Session["ReportCompany"];
            } else {
                theRCompany = myFact.GetCompanyReport(null, CIID, false);
            }
        }

        private void btnFinish_Click(object sender, EventArgs e) {
            if (Page.IsValid) {
                theRCompany.StatusID = Convert.ToInt32(ddReportStatus.SelectedValue);
                theRCompany.ExternalCommentNative = tbInternalComment.Text;
                theRCompany.ExternalCommentEN = txtInternalCommentEN.Text;
                theRCompany.ExpireDate = Convert.ToDateTime(tbReportsExpiryDate.Text);
                // h�r �arf a� g�ta �ess a� update bara �essi �rj� sv��i !
                if (myFact.UpdateReportCompanyFromControlPage(theRCompany)) {
                    Server.Transfer("ReportSelection.aspx");
                } else {
                    DisplayErrorMessage(rm.GetString("txtErrorSavingInfo", ci));
                }
            }
        }

        private void DisplayErrorMessage(String errorMessage) {
            lblMessage.Text = errorMessage;
            lblMessage.Visible = true;
        }

        private void InitBoxes() {
            try {
                ddReportStatus.SelectedValue = theRCompany.StatusID.ToString();
            } catch (ArgumentOutOfRangeException outOfEx) {
                Logger.WriteToLog("Control.aspx : InitBoxes " + outOfEx.Message, true);
            }
            if (theRCompany.ExpireDate.Year > 1900) {
                tbReportsExpiryDate.Text = theRCompany.ExpireDate.ToShortDateString();
            } else {
                //Get the default expireDate from config
                try {
                    var myDate = DateTime.Today;
                    tbReportsExpiryDate.Text =
                        myDate.AddMonths(int.Parse(CigConfig.Configure("lookupsettings.defaultReportExpireDate").Trim()))
                            .ToShortDateString();
                } catch (Exception) {
                    tbReportsExpiryDate.Text = DateTime.Today.ToShortDateString();
                }
            }
            tbInternalComment.Text = theRCompany.ExternalCommentNative;
            txtInternalCommentEN.Text = theRCompany.ExternalCommentEN;
        }

        private void FillDataSets() { FillStatusSet(); }

        private void FillStatusSet() {
            if (Session["dsReportStatus"] != null) {
                dsReportStatus = (DataSet) Session["dsReportStatus"];
            } else {
                dsReportStatus = myFact.GetReportStatusAsDataSet();
                Session["dsReportStatus"] = dsReportStatus;
            }
        }

        private void BindBoxes() { BindStatusList(); }

        private void BindStatusList() {
            ddReportStatus.DataSource = dsReportStatus;
            ddReportStatus.DataTextField = nativeCult ? "StatusNative" : "StatusEN";
            ddReportStatus.DataValueField = "StatusID";
            ddReportStatus.DataBind();

            //	this.ddReportStatus.SelectedValue = theRCompany.StatusID.ToString();
        }

        public void DateValidate(object source, ServerValidateEventArgs value) {
            // date string correct?
            if (value.Value != "") {
                IFormatProvider format = CultureInfo.CurrentCulture;
                try {
                    DateTime.Parse(value.Value, format, DateTimeStyles.AllowWhiteSpaces);
                    value.IsValid = true;
                } catch (Exception) {
                    value.IsValid = false;
                    return;
                }
            } else {
                value.IsValid = true;
            }
        }

        private void LocalizeText() {
            lblControl.Text = rm.GetString("txtControl", ci);
            lblReportStatus.Text = rm.GetString("txtReportStatus", ci);
            lblReportsExpiry.Text = rm.GetString("txtReportsExpiryDate", ci);
            lblComment.Text = rm.GetString("txtExternalComments", ci);
            lblCommentEN.Text = rm.GetString("txtExternalCommentsEN", ci);
            btnFinish.Text = rm.GetString("txtFinishEntry", ci);
            btnBack.Text = rm.GetString("txtBack", ci);
            CustomValidator1.ErrorMessage = rm.GetString("txtDateInputInWrongFormat", ci);
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
        }

        private void AddEnterEvent() {
            ddReportStatus.Attributes.Add("onkeypress", "checkEnterKey();");
            tbReportsExpiryDate.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        private void btnBack_Click(object sender, EventArgs e) { TransferToPreviousPage(); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.CustomValidator1.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.DateValidate);
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}