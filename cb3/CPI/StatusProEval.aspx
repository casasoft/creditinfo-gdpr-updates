<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Page language="c#" Codebehind="StatusProEval.aspx.cs" AutoEventWireup="false" Inherits="CPI.StatusProEval" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>StatusProEval</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/CIGStyles.css" type="text/css" rel="stylesheet">
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						StatusProsEvalForm.btnReg.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.StatusProsEvalForm.tbHistoryN.focus();
				}

		</SCRIPT>
	</HEAD>
	<body onload="SetFormFocus()" ms_positioning="GridLayout">
		<form id="StatusProsEvalForm" method="post" runat="server">
			<table width="997" height="600" align="center" border="0">
				<tr>
					<td colspan="4">
						<uc1:head id="Head1" runat="server"></uc1:head>
					</td>
				</tr>
				<tr valign="top">
					<td width="1"></td>
					<td>
						<table width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<uc1:language id="Language1" runat="server"></uc1:language>
								</td>
								<td></td>
								<td align="right">
									<ucl:options id="Options1" runat="server"></ucl:options>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
								</td>
								<td align="right">
									<ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							<tr valign="top">
								<td width="150" valign="top" align="left">
									<table width="98%">
										<tr>
											<td>
												<uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
											</td>
										</tr>
									</table>
								</td>
								<td colspan="2">
									<!-- Main Body Starts -->
									<table width="100%">
										<tr>
											<td>
												<!-- BEGIN // GRID TABLE WITH STEP n OF N // BEGIN -->
												<table class="grid_table" cellspacing="0" cellpadding="0">
													<tr>
														<th>
															<table class="grid_table_head">
																<tr>
																	<td>
																		<asp:label id="lblHisOpReview" runat="server">History, operation and review</asp:label>
																	</td>
																	<td align="right">
																		<asp:label id="lblPageStep" runat="server">Step</asp:label>
																	</td>
																</tr>
															</table>
														</th>
													</tr>
													<tr>
														<td>
															<table class="fields" id="tbCriteria" cellspacing="0" cellpadding="0">
																<tr>
																	<td>
																		<asp:label id="lblHistoryN" runat="server">History (native)</asp:label>
																		<br>
																		<asp:textbox id="tbHistoryN" runat="server" width="100%" height="102px" textmode="MultiLine"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblHistoryE" runat="server">History (english)</asp:label>
																		<br>
																		<asp:textbox id="tbHistoryE" runat="server" width="100%" height="102px" textmode="MultiLine"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblOperationN" runat="server">Operation (native)</asp:label>
																		<br>
																		<asp:textbox id="tbOperationN" runat="server" width="100%" height="102px" textmode="MultiLine"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblOperationE" runat="server">Operation (english)</asp:label>
																		<br>
																		<asp:textbox id="tbOperationE" runat="server" width="100%" height="102px" textmode="MultiLine"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblCompanyReviewN" runat="server">Company review (native)</asp:label>
																		<br>
																		<asp:textbox id="tbCompanyReviewN" runat="server" width="100%" textmode="MultiLine" height="102px"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblCompanyReviewE" runat="server">Company review (english)</asp:label>
																		<br>
																		<asp:textbox id="tbCompanyReviewE" runat="server" width="100%" textmode="MultiLine" height="102px"></asp:textbox>
																	</td>
																</tr>
																<tr id="trRegistrationText" runat="server">
																	<td style="HEIGHT: 15px" align="left" colspan="8">
																		<table width="100%">
																			<tr>
																				<td>
																					<asp:label id="lblRegistryTextNative" runat="server">Business registry text (native)</asp:label>
																					<br>
																					<asp:textbox id="tbRegistryTextNative" runat="server" width="100%" textmode="MultiLine" height="102px"></asp:textbox>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<asp:label id="lblRegistryTextEN" runat="server">Business registry text (english)</asp:label>
																					<br>
																					<asp:textbox id="tbRegistryTextEN" runat="server" width="100%" textmode="MultiLine" height="102px"></asp:textbox>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblCompanyStatusDescriptionNative" runat="server">Company status description (native)</asp:label>
																		<br>
																		<asp:textbox id="txtCompanyStatusDescriptionNative" runat="server" width="100%" textmode="MultiLine"
																			height="102px"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td>
																		<asp:label id="lblCompanyStatusDescriptionEN" runat="server">Company status description (english)</asp:label>
																		<br>
																		<asp:textbox id="txtCompanyStatusDescriptionEN" runat="server" width="100%" textmode="MultiLine"
																			height="102px"></asp:textbox>
																	</td>
																</tr>
																<tr>
																	<td height="23"></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- END // GRID TABLE WITH STEP n OF N // END -->
											</td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>
												<!-- BEGIN // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // BEGIN -->
												<table class="empty_table" cellspacing="0">
													<tr>
														<td align="left">
														</td>
														<td align="right">
															<asp:button id="btnCancel" runat="server" cssclass="cancel_button" text="Cancel" causesvalidation="False"></asp:button><asp:button id="btnContinue" runat="server" cssclass="gray_button" text="Continue"></asp:button><asp:button id="btnReg" runat="server" cssclass="confirm_button" text="Reg."></asp:button>
														</td>
													</tr>
												</table>
												<!-- END // EMPTY TABLE FOR ERROR TEXTS AND BUTTONS // END -->
											</td>
										</tr>
									</table>
									<!-- Main Body Ends -->
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" height="1" bgcolor="#000000"></td>
							</tr>
						</table>
					</td>
					<td width="2"></td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
