#region

using System;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.Localization;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI
{
    /// <summary>
    /// Summary description for SearchForReport.
    /// </summary>
    public class SearchForReport : Page
    {
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected Button btnCreateNewReport;
        protected Button btnGetReport;
        protected DataGrid dgReportOwners;
        protected Label lblCIID;
        protected Label lblDatagridHeader;
        protected Label lblDatagridIcons;
        protected Label lblErrorMsg;
        protected Label lblName;
        protected Label lblPageTitle;
        protected Label lblUniqueIndentifier;
        private bool nativeCult;
        protected HtmlTable outerGridTable;
        private string redirectURL;
        protected TextBox tbUniqueIndentifier;
        public string totalPageSteps;
        protected TextBox txtCreditInfoID;
        protected TextBox txtName;

        private void Page_Load(object sender, EventArgs e)
        {
            
            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();

            Page.ID = CigConfig.Configure("lookupsettings.PageIDSearchForReport.aspx");
            lblErrorMsg.Visible = false;

            redirectURL = Request.QueryString["redirect"];

            // check the current culture
            string culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            if (culture.Equals(nativeCulture))
            {
                nativeCult = true;
            }

            if (Request.QueryString["new"] != null)
            {
                ClearAllCPISessionVariables();
            }
            // get the totalPageSteps
            if (Cache["TotalPageSteps"] != null)
            {
                totalPageSteps = (string)Cache["TotalPageSteps"];
            }
            else
            {
                totalPageSteps = CigConfig.Configure("lookupsettings.TotalPageSteps");
                Cache["TotalPageSteps"] = totalPageSteps;
            }
            if (!IsPostBack)
            {
                LocalizeText();
                Session["ReportCompany"] = null;
                outerGridTable.Visible = false;
            }
        }

        private void DisplayMessage(string message)
        {
            lblErrorMsg.Text = message;
            lblErrorMsg.ForeColor = Color.Blue;
            lblErrorMsg.Visible = true;
        }

        private void DisplayErrorMessage(string errorMessage)
        {
            lblErrorMsg.Text = errorMessage;
            lblErrorMsg.ForeColor = Color.Red;
            lblErrorMsg.Visible = true;
        }

        private void SendToBasicInfoPage(int creditInfoID)
        {
            var myFactory = new CPIFactory();

            ReportCompanyBLLC theRCompany = myFactory.GetCompanyReport(creditInfoID);
            // h�r �yrfti �g einnig a� t�kka � hvort einhverjar grunnuppl�singar s�u til ...

            if (theRCompany != null)
            {
                // athuga hvort ekki s� b�i� a� skr� inn grunnuppl�singar � report
                if (theRCompany.ReportID > 0)
                {
                    Session["ReportExists"] = true;
                }
                Session["ReportCompany"] = theRCompany;
                Session["CIID"] = theRCompany.CompanyCIID;
                Session["cpiTemplate"] = "General";

                if (redirectURL != null)
                {
                    Server.Transfer(redirectURL + ".aspx");
                }
                else
                {
                    Server.Transfer("BasicInformation.aspx?action=update");
                }
            }
            else
            {
                DisplayErrorMessage(rm.GetString("txtErrorGettingReport", ci));
            }
        }

        private void AddEnterEvent()
        {
            tbUniqueIndentifier.Attributes.Add("onkeypress", "checkEnterKey();");
            txtCreditInfoID.Attributes.Add("onkeypress", "checkEnterKey();");
            txtName.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        private void LocalizeText()
        {
            lblPageTitle.Text = rm.GetString("txtGetReport", ci);
            lblErrorMsg.Text = rm.GetString("txtReportMatchingRegnoNotFound", ci);
            lblUniqueIndentifier.Text = rm.GetString("txtUniqueIdentifier", ci);
            lblCIID.Text = rm.GetString("txtCreditInfoID", ci);
            lblName.Text = rm.GetString("txtName", ci);
            btnGetReport.Text = rm.GetString("txtSearch", ci);
            btnCreateNewReport.Text = rm.GetString("txtCreateNew", ci);
            lblDatagridHeader.Text = rm.GetString("txtResults", ci);

            setGridHeaders();
            setGridHeadersVisibility();
        }

        private void setGridHeaders()
        {
            dgReportOwners.Columns[1].HeaderText = rm.GetString("txtCreditInfoID", ci);
            dgReportOwners.Columns[2].HeaderText = rm.GetString("txtName", ci);
            dgReportOwners.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dgReportOwners.Columns[4].HeaderText = rm.GetString("txtUniqueIdentifier", ci);
        }

        private void setGridHeadersVisibility()
        {
            if (nativeCult)
            {
                dgReportOwners.Columns[2].Visible = true;
                dgReportOwners.Columns[3].Visible = false;
            }
            else
            {
                dgReportOwners.Columns[2].Visible = false;
                dgReportOwners.Columns[3].Visible = true;
            }
        }

        /// <summary>
        /// Clear all possible session variables before the user starts to work on new company report
        /// </summary>
        private void ClearAllCPISessionVariables()
        {
            Session.Remove("ReportCompany");
            Session.Remove("CIID");
            Session.Remove("arrAddetBanks");
            Session.Remove("arrAddetCustomerTypes");
            Session.Remove("dsStaffCount");
            Session.Remove("dsContinents");
            Session.Remove("dsCountries");
            Session.Remove("dsCoCo");
            Session.Remove("arrImportCountrieID");
            Session.Remove("arrExportCountrieID");
            Session.Remove("arrCB");
            Session.Remove("dsReportStatus");
            Session.Remove("dsDetailedBoardMembers");
            Session.Remove("dsDetailedPrincipals");
            Session.Remove("dsStaffCount");
            Session.Remove("arrCompanyOperation");
            Session.Remove("arrCompanyBanks");
            Session.Remove("arrCompanyCustomerTypes");
            Session.Remove("dsJobtitle");
            Session.Remove("dsEducationList");
            Session.Remove("dsEstates");
            Session.Remove("EstateID");
            Session.Remove("dsManagementPositions");
            Session.Remove("dsShareHolders");
            Session.Remove("dsSubsidiaries");
            Session.Remove("ReportExists");
            Session.Remove("FormerOperation");
        }

        private void dgReportOwners_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                //((LinkButton)e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect",ci);

                WebDesign.CreateExplanationIcons(dgReportOwners.Columns, lblDatagridIcons, rm, ci);
            }
        }

        private void dgReportOwners_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (!e.CommandName.Equals("Select")) { }
            else
            {
                //Senda hann � basic info page
                string creditInfoID = e.Item.Cells[1].Text.Trim();

                try
                {
                    int ciid = int.Parse(creditInfoID);

                    SendToBasicInfoPage(ciid);
                }
                catch (Exception)
                {
                    DisplayErrorMessage(rm.GetString("txtErrorHasOccurred", ci));
                }
            }
        }

        private void btnCreateNewReport_Click(object sender, EventArgs e)
        {
            Session["cpiTemplate"] = "General";
            if (!string.IsNullOrEmpty(redirectURL))
            {
                Server.Transfer(redirectURL + ".aspx?action=new");
            }
            Server.Transfer("BasicInformation.aspx?action=new");
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetReport.Click += new System.EventHandler(this.btnGetReport_Click);
            this.dgReportOwners.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgReportOwners_ItemCommand);
            this.dgReportOwners.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgReportOwners_ItemDataBound);
            this.btnCreateNewReport.Click += new System.EventHandler(this.btnCreateNewReport_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion


        protected void btnGetReport_Click(object sender, EventArgs e)
        {
            var myFactory = new CPIFactory();

            int ciid;

            try
            {
                ciid = int.Parse(txtCreditInfoID.Text.Trim());
            }
            catch (Exception)
            {
                ciid = -1;
            }

            var myReports = myFactory.FindCompanyReport(ciid, txtName.Text.Trim(), tbUniqueIndentifier.Text.Trim());

            if (myReports.Count > 0)
            {
                if (myReports.Count == 1)
                {
                    var report = (ReportCompanyBLLC)myReports[0];
                    if (report.CompanyCIID > -1)
                    {
                        SendToBasicInfoPage(report.CompanyCIID);
                        return;
                    }
                }
                dgReportOwners.DataSource = myReports;
                dgReportOwners.DataBind();

                outerGridTable.Visible = true;
            }
            else
            {
                DisplayMessage(rm.GetString("txtNoEntryFound", ci));
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string s = "";
        }
    }
}