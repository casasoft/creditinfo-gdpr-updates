#region

using System;
using System.Web.UI.WebControls;

#endregion

namespace CPI {
    /// <summary>
    /// Summary description for SamplePage.
    /// </summary>
    public class SamplePage : BasePage {
        protected Button Button1;
        protected TextBox TextBox1;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
        }

        private void Button1_Click(object sender, EventArgs e) { TextBox1.Text = "Virkar �etta svona?!"; }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}