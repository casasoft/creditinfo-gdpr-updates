#region

using System;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CPI.WLF;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI {
    /// <summary>
    /// Summary description for StatusProEval.
    /// </summary>
    public class StatusProEval : BaseCPIPage {
        private readonly CPIFactory myFact = new CPIFactory();
        protected Button btnCancel;
        protected Button btnContinue;
        protected Button btnReg;
        private int CIID = 1;
        private bool infoExists;
        protected Label lblCompanyReviewE;
        protected Label lblCompanyReviewN;
        protected Label lblCompanyStatusDescriptionEN;
        protected Label lblCompanyStatusDescriptionNative;
        protected Label lblHisOpReview;
        protected Label lblHistoryE;
        protected Label lblHistoryN;
        protected Label lblOperationE;
        protected Label lblOperationN;
        protected Label lblPageStep;
        protected Label lblRegistryTextEN;
        protected Label lblRegistryTextNative;
        protected TextBox tbCompanyReviewE;
        protected TextBox tbCompanyReviewN;
        protected TextBox tbHistoryE;
        protected TextBox tbHistoryN;
        protected TextBox tbOperationE;
        protected TextBox tbOperationN;
        protected TextBox tbRegistryTextEN;
        protected TextBox tbRegistryTextNative;
        protected HtmlTableRow trRegistrationText;
        protected TextBox txtCompanyStatusDescriptionEN;
        protected TextBox txtCompanyStatusDescriptionNative;

        private void Page_Load(object sender, EventArgs e) {
            pageName = "StatusProEval";
            // Add <ENTER> event
            //AddEnterEvent(); - HAUKUR - removed - need to be able to add enter to text

            // check the current culture
            trRegistrationText.Visible = CigConfig.Configure("lookupsettings.currentVersion") != null && CigConfig.Configure("lookupsettings.currentVersion").Equals("czech");

            if (Session["CIID"] != null) {
                CIID = (int) Session["CIID"];
            } else {
                Server.Transfer("SearchForReport.aspx?redirect=StatusProEval");
            }

            // athuga hvort b�i� s� a� skr� inn grunnuppl�singar 
            if (Session["ReportExists"] == null) // ef ekki �� senda tilbaka � BasicInformation
            {
                Server.Transfer("BasicInformation.aspx?action=update");
            }

            PreparePageLoad();

            SetGridHeaders();
            if (!IsPostBack) {
                FillFields();
                LocalizeText();
                //		InitSubsidiariesGrid();
                //		BindSubsidiariesGrid();				
            }
        }

        private void FillFields() {
            // fylla �etta tilvik o.sv.frv...
            HistoryOperationReviewBLLC theReview = myFact.GetHistoryOperationReview(CIID);
            tbCompanyReviewN.Text = theReview.CompanyReviewNative;
            tbCompanyReviewE.Text = theReview.CompanyReviewEN;
            tbHistoryE.Text = theReview.HistoryEN;
            tbHistoryN.Text = theReview.HistoryNative;
            tbOperationE.Text = theReview.OperationEN;
            tbOperationN.Text = theReview.OperationNative;
            tbRegistryTextNative.Text = theReview.BusinessRegistryTextNative;
            tbRegistryTextEN.Text = theReview.BusinessRegistryTextEN;
            txtCompanyStatusDescriptionNative.Text = theReview.StatusDescriptionNative;
            txtCompanyStatusDescriptionEN.Text = theReview.StatusDescriptionEN;

            infoExists = theReview.Exists;
        }

        private void btnReg_Click(object sender, EventArgs e) {
            // do some insert stuff here ...
            var theReview = new HistoryOperationReviewBLLC();
            if (tbCompanyReviewN.Text != "") {
                theReview.CompanyReviewNative = tbCompanyReviewN.Text;
            }
            if (tbCompanyReviewE.Text != "") {
                theReview.CompanyReviewEN = tbCompanyReviewE.Text;
            }
            if (tbHistoryE.Text != "") {
                theReview.HistoryEN = tbHistoryE.Text;
            }
            if (tbHistoryN.Text != "") {
                theReview.HistoryNative = tbHistoryN.Text;
            }
            if (tbOperationE.Text != "") {
                theReview.OperationEN = tbOperationE.Text;
            }
            if (tbOperationN.Text != "") {
                theReview.OperationNative = tbOperationN.Text;
            }
            if (tbRegistryTextNative.Text != "") {
                theReview.BusinessRegistryTextNative = tbRegistryTextNative.Text;
            }
            if (tbRegistryTextEN.Text != "") {
                theReview.BusinessRegistryTextEN = tbRegistryTextEN.Text;
            }
            if (txtCompanyStatusDescriptionNative.Text != "") {
                theReview.StatusDescriptionNative = txtCompanyStatusDescriptionNative.Text;
            }
            if (txtCompanyStatusDescriptionEN.Text != "") {
                theReview.StatusDescriptionEN = txtCompanyStatusDescriptionEN.Text;
            }
            myFact.AddHistoryOperationReview(theReview, CIID);
        }

        private void btnContinue_Click(object sender, EventArgs e) { TransferToNextPage(); }

        private void LocalizeText() {
            lblHisOpReview.Text = rm.GetString("txtHistoryOperationAndReview", ci);
            lblHistoryN.Text = rm.GetString("txtHistoryNative", ci);
            lblHistoryE.Text = rm.GetString("txtHistoryEnglish", ci);
            lblOperationE.Text = rm.GetString("txtOperationEnglish", ci);
            lblOperationN.Text = rm.GetString("txtOperationNative", ci);
            lblCompanyReviewN.Text = rm.GetString("txtCompanyReviewNative", ci);
            lblCompanyReviewE.Text = rm.GetString("txtCompanyReviewEnglish", ci);
            lblCompanyStatusDescriptionNative.Text = rm.GetString("txtCompanyStatusDescriptionNative", ci);
            lblCompanyStatusDescriptionEN.Text = rm.GetString("txtCompanyStatusDescriptionEnglish", ci);
            lblRegistryTextNative.Text = rm.GetString("txtRegistryTextNative", ci);
            lblRegistryTextEN.Text = rm.GetString("txtRegistryTextEN", ci);
            btnContinue.Text = rm.GetString("txtContinue", ci);
            if (!infoExists) {
                btnReg.Text = rm.GetString("txtSave", ci);
            } else {
                btnReg.Text = rm.GetString("txtUpdate", ci);
                btnReg.CommandName = "Update";
            }
            lblPageStep.Text = rm.GetString("txtStep", ci) + " " + currentPageStep + " " + rm.GetString("txtOf", ci) +
                               " " + totalPageSteps;
            //	this.btnCancel.Text = rm.GetString("txtCancel",ci);
            btnCancel.Text = rm.GetString("txtBack", ci);
        }

        private static void SetGridHeaders() {
            // Board members grid
            //	dtgrBoardMOverview.Columns[0].HeaderText = rm.GetString("txtName",ci);
        }

        private void btnCancel_Click(object sender, EventArgs e) { TransferToPreviousPage(); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}