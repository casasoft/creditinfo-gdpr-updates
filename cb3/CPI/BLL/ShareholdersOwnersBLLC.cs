#region

using UserAdmin.BLL.CIUsers;

#endregion

namespace CPI.BLL {
    /// <remarks>
    /// This class inherits Person and adds one attribute
    /// </remarks>
    public class ShareholdersOwnerPersonBLLC : Indivitual {
        /// <summary>
        /// Gets/Sets the persons ownership
        /// </summary>
        public int Ownership { get; set; }

        /// <summary>
        /// Gets/Sets the ownership Percentage
        /// </summary>
        public decimal OwnershipPercentage { get; set; }

        /// <summary>
        /// Gets/Sets the share type id
        /// </summary>
        public int OwnershipTypeID { get; set; }
    } // END CLASS DEFINITION ShareholdersOwnersBLLC
}