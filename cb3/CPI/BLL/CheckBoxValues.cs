#region

using System;

#endregion

namespace CPI.BLL {
    /// <summary>
    /// This class is an helper class for ChecBox. Because sessions are kept
    /// out of process the CheckBox control can't be kept in session but this class can
    /// </summary>
    [Serializable]
    public class CheckBoxValues {
        /// <summary>
        /// The CheckBox id
        /// </summary>
        private String _ID = "";

        /// <summary>
        /// The (label) texts
        /// </summary>
        private String _text = "";

        /// <summary>
        /// The checkBox value
        /// </summary>
        private String _value = "";

        /// <summary>
        /// get/sets the ID value
        /// </summary>
        public String ID { get { return _ID; } set { _ID = value; } }

        /// <summary>
        /// get/sets the value value
        /// </summary>
        public String Value { get { return _value; } set { _value = value; } }

        /// <summary>
        /// get/sets the checked value
        /// </summary>
        public bool Checked { get; set; }

        /// <summary>
        /// get/sets the Text value
        /// </summary>
        public String Text { get { return _text; } set { _text = value; } }
    }
}