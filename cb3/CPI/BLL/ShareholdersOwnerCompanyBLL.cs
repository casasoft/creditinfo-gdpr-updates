#region

using UserAdmin.BLL.CIUsers;

#endregion

namespace CPI.BLL {
    /// <summary>
    /// This class inherits Company and adds one attribute
    /// </summary>
    public class ShareholdersOwnerCompanyBLLC : Company {
        /// <summary>
        /// Gets/Sets the ownership
        /// </summary>
        public int Ownership { get; set; }

        /// <summary>
        /// Gets/Sets the ownership Percentage
        /// </summary>
        public decimal OwnershipPercentage { get; set; }

        /// <summary>
        /// Gets/Sets the share type id
        /// </summary>
        public int OwnershipTypeID { get; set; }
    }
}