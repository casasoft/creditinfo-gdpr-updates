#region

using System;

#endregion

namespace CPI.BLL {
    /// <summary>
    /// Summary description for Charges.
    /// </summary>
    public class ChargesBLLC {
        public ChargesBLLC() {
            ID = -1;
            CreditInfoID = -1;
            DateRegistered = new DateTime(1899, 1, 1);
            DatePrepared = new DateTime(1899, 1, 1);
            DescriptionID = -1;
            Amount = -1;
            CurrencyCode = "";
            Sequence = "";
            BankID = "";
        }

        public int ID { get; set; }
        public int CreditInfoID { get; set; }
        public DateTime DateRegistered { get; set; }
        public DateTime DatePrepared { get; set; }
        public int DescriptionID { get; set; }
        public decimal Amount { get; set; }
        public String CurrencyCode { get; set; }
        public String Sequence { get; set; }
        public String BankID { get; set; }
        public String DescriptionFreeTextNative { get; set; }
        public String DescriptionFreeTextEN { get; set; }
    }
}