namespace CPI.BLL {
    /// <remarks>
    /// This class hold information about the real estates of the company
    /// </remarks>
    public class RealEstatesBLLC {
        /// <summary>
        /// Gets/sets the CityID
        /// </summary>
        public int CityID { get; set; }

        /// <summary>
        /// Gets/sets the CountryID
        /// </summary>
        public int CountryID { get; set; }

        /// <summary>
        /// Gets/sets the RealEstatesID
        /// </summary>
        public int RealEstateID { get; set; }

        /// <summary>
        /// Gets/set the address native attribute
        /// </summary>
        public string AddressNative { get; set; }

        /// <summary>
        /// Gets/sets the address EN attribute
        /// </summary>
        public string AddressEN { get; set; }

        /// <summary>
        /// Gets/set the areaCode attribute
        /// </summary>
        public string AreaCode { get; set; }

        /// <summary>
        /// Gets/set the size attribute
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// Gets/set the yearBuilt attribute
        /// </summary>
        public string YearBuilt { get; set; }

        /// <summary>
        /// Gets/set the loaction attribute
        /// </summary>
        public int Location { get; set; }

        /// <summary>
        /// Gets/set the tenure attribute
        /// </summary>
        public int Tenure { get; set; }

        /// <summary>
        /// Gets/set the permiseType attribute
        /// </summary>
        public string PermisesType { get; set; }

        /// <summary>
        /// Gets/sets the estateTypeID
        /// </summary>
        public int EstateTypeID { get; set; }

        /// <summary>
        /// Gets/sets the insurance value attribute
        /// </summary>
        public float InsuranceValue { get; set; }
    } // END CLASS DEFINITION RealEstatesBLLC
}