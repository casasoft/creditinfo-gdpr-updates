#region

using System;

#endregion

namespace CPI.BLL {
    /// <summary>
    /// Summary description for Capital.
    /// </summary>
    public class CapitalBLLC {
        public CapitalBLLC() {
            CompanyCIID = -1;
            AuthorizedCapital = -1;
            IssuedNumberOfShares = -1;
            Asked = -1;
            PaidUp = -1;
            NominalNumberOfShares = -1;
            SharesDescription = "";
        }

        public int CompanyCIID { get; set; }
        public decimal AuthorizedCapital { get; set; }
        public decimal IssuedNumberOfShares { get; set; }
        public decimal Asked { get; set; }
        public decimal PaidUp { get; set; }
        public decimal NominalNumberOfShares { get; set; }
        public String ShareClass { get; set; }
        public String SharesDescription { get; set; }
        public String CurrencyCode { get; set; }
    }
}