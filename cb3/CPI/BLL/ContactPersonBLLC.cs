#region

using System;

#endregion

namespace CPI.BLL {
    [Serializable]
    /// <summary>
        /// Summary description for ContactPersonBLLC.
        /// </summary>
    public class ContactPersonBLLC {
        public ContactPersonBLLC() {
            CreditInfoID = -1;
            FirstNameEN = "";
            LastNameEN = "";
            FirstNameNative = "";
            LastNameNative = "";
            EmailAddress = "";
            Position = "";
            MessengerID = "";
            PhoneNumber = "";
            MobileNumber = "";
            FaxNumber = "";
            DateContacted = new DateTime(1899, 1, 1);
        }

        public int CreditInfoID { get; set; }
        public String FirstNameEN { get; set; }
        public String LastNameEN { get; set; }
        public String FirstNameNative { get; set; }
        public String LastNameNative { get; set; }
        public String EmailAddress { get; set; }
        public String Position { get; set; }
        public String MessengerID { get; set; }
        public String PhoneNumber { get; set; }
        public String MobileNumber { get; set; }
        public String FaxNumber { get; set; }
        public DateTime DateContacted { get; set; }
    }
}