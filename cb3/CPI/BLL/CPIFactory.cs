#region

using System;
using System.Collections;
using System.Data;
using CPI.DAL;
using UserAdmin.BLL.CIUsers;
using UserAdmin.DAL.CIUsers;

#endregion

namespace CPI.BLL {
    /// <summary>
    /// Summary description for CPIFactory.
    /// </summary>
    public class CPIFactory {
        /// <summary>
        /// Instance of the DALC class
        /// </summary>
        private readonly CompanyProfileInputDALC theCPIDAL = new CompanyProfileInputDALC();

        private readonly CreditInfoUserDALC theCreditInfoDAL = new CreditInfoUserDALC();
        public DataSet GetFileFormats() { return theCPIDAL.GetFileFormats(); }
        public DataSet GetContinentsAsDataSet() { return theCPIDAL.GetContinentsAsDataSet(); }
        public DataSet GetCountriesAsDataSet(bool native) { return theCPIDAL.GetCountriesAsDataSet(native); }
        public DataSet GetCompanyOperationAsDataSet(int companyCIID, bool current) { return theCPIDAL.GetCompanyOperationAsDataSet(companyCIID, current); }
        public DataSet GetCompanyBanksAsDataSet(int companyCIID) { return theCPIDAL.GetCompanyBanksAsDataSet(companyCIID); }
        public DataSet GetBanksAsDataSet() { return theCPIDAL.GetBanksAsDataSet(); }
        public DataSet GetBanksAsDataSet(bool native) { return theCPIDAL.GetBanksAsDataSet(native); }
        public DataSet GetMTBanksAsDataSet() { return theCPIDAL.GetMTBanksAsDataSet(); }
        public DataSet GetOwnershipTypesAsDataSet() { return theCPIDAL.GetOwnershipTypesAsDataSet(); }
        public DataSet GetOperationAsDataSet() { return theCPIDAL.GetOperationAsDataSet(); }
        public DataSet GetCompaniesCustomersAsDataSet(int companyCIID) { return theCPIDAL.GetCompaniesCustomersAsDataSet(companyCIID); }
        public DataSet GetCustomerTypesAsDataSet() { return theCPIDAL.GetCustomerTypesAsDataSet(); }
        public DataSet GetBoardPrincipalsAsDataSet(int companyCIID, bool board) { return theCPIDAL.GetBoardPrincipalsAsDataSet(companyCIID, board); }
        public DataSet GetStaffCountAsDataSet(int companyCIID) { return theCPIDAL.GetStaffCountAsDataSet(companyCIID); }
        public DataSet GetManagementPositionAsDataSet() { return theCPIDAL.GetManagementPositionAsDataSet(); }
        public Indivitual GetIndivitualByNationalID(String personalID) { return theCreditInfoDAL.GetIndivitualByNationalID(personalID); }
        public Indivitual GetIndivitualByNationalID(string nationalID, bool isSearchable) { return theCreditInfoDAL.GetIndivitualByNationalID(nationalID, isSearchable); }
        public bool InsertEmployeeCountYears(ArrayList arrEmCount, int CIID) { return theCPIDAL.InsertEmployeeCountYears(arrEmCount, CIID); }
        public DataSet GetDetailedBoardAsDataSet(int companyCIID) { return theCPIDAL.GetDetailedBoardAsDataSet(companyCIID); }
        public bool AddBordMember(BoardMemberBLLC theBoardMember, int companyCIID) { return theCPIDAL.AddBordMember(theBoardMember, companyCIID); }
        public bool DeleteBoardMember(int boardMemebersCIID, int companyCIID) { return theCPIDAL.DeleteBoardMember(boardMemebersCIID, companyCIID); }
        public DataSet GetDetailedPrincipalsAsDataSet(int companyCIID) { return theCPIDAL.GetDetailedPrincipalsAsDataSet(companyCIID); }
        public DataSet GetIndividualFromNationalInterfaceAsDataSet(string name) { return theCPIDAL.GetIndividualFromNationalInterfaceAsDataSet(name); }
        public DataSet GetCompanyBySSNFromNationalInterfaceAsDataSet(string CompanySSN) { return theCPIDAL.GetCompanyBySSNFromNationalInterfaceAsDataSet(CompanySSN); }
        public DataSet GetCompanyFromNationalInterfaceAsDataSet(string name) { return theCPIDAL.GetCompanyFromNationalInterfaceAsDataSet(name); }
        public DataSet GetCompanyBoardFromCompanyRegistryAsDataSet(string companySSN) { return theCPIDAL.GetCompanyBoardFromCompanyRegistryAsDataSet(companySSN); }
        public DataSet GetCompanyShareholdersFromCompanyRegistryAsDataSet(string companySSN) { return theCPIDAL.GetCompanyShareholdersFromCompanyRegistryAsDataSet(companySSN); }
        public DataSet GetCompanyBoardSecretaryFromCompanyRegistryAsDataSet(string companySSN) { return theCPIDAL.GetCompanyBoardSecretaryFromCompanyRegistryAsDataSet(companySSN); }
        public DataSet GetCapitalFromCompanyRegistryAsDataSet(string companySSN) { return theCPIDAL.GetCapitalFromCompanyRegistryAsDataSet(companySSN); }
        public DataSet GetSubsidiariesFromCompanyRegistryAsDataSet(string companySSN) { return theCPIDAL.GetSubsidiariesFromCompanyRegistryAsDataSet(companySSN); }
        public DataSet GetJobTitlesAsDataSet() { return theCPIDAL.GetJobTitlesAsDataSet(); }
        public DataSet GetEducationListAsDataSet() { return theCreditInfoDAL.GetEducationListAsDataSet(); }
        public bool AddPrincipal(PrincipalsBLLC thePrincipal, int companyCIID) { return theCPIDAL.AddPrincipal(thePrincipal, companyCIID); }
        public bool DeletePrincipal(int principalsCIID, int companyCIID, int jobTitleID) { return theCPIDAL.DeletePrincipal(principalsCIID, companyCIID, jobTitleID); }
        public bool AddHistory(int CIID, HistoryBLLC theHistory) { return theCPIDAL.AddHistory(CIID, theHistory); }
        public HistoryBLLC GetHistory(int CIID, int historyTypeID) { return theCPIDAL.GetHistory(CIID, historyTypeID); }
        public DateTime GetStructureReportDate(int creditInfoID) { return theCPIDAL.GetStructureReportDate(creditInfoID); }
        public DataSet GetShareHolderOwnerAsDataSet(int companyCIID) { return theCPIDAL.GetShareHolderOwnerAsDataSet(companyCIID); }

        /// <summary>
        /// This function returns all shares owned by a specific owner.
        /// </summary>
        /// <param name="ownerCiid">Ciid for the owner.</param>
        /// <returns>All shares as data set.</returns>
        public DataSet GetInvolvements(int ownerCiid) { return theCPIDAL.GetInvolvements(ownerCiid); }

        public BoardSecretaryBLLC GetBoardSecretary(int companyCIID) { return theCPIDAL.GetBoardSecretary(companyCIID); }
        public BoardSecretaryCompanyBLLC GetCompanyBoardSecretary(int companyCIID) { return theCPIDAL.GetCompanyBoardSecretary(companyCIID); }
        public DataSet GetBoardSecretaryAsDataSet(int companyCIID) { return theCPIDAL.GetBoardSecretaryAsDataSet(companyCIID); }
        public bool AddBoardSecretary(BoardSecretaryBLLC theSecretary, int companyCIID) { return theCPIDAL.AddBoardSecretary(theSecretary, companyCIID); }
        public bool AddBoardSecretaryCompany(BoardSecretaryCompanyBLLC bSecretaryCompany, int CIID) { return theCPIDAL.AddBoardSecretaryCompany(bSecretaryCompany, CIID); }
        public bool DeleteBoardSecretary(int companyCIID) { return theCPIDAL.DeleteBoardSecretary(companyCIID); }
        public bool AddShareholderOwner(ShareholdersOwnerPersonBLLC theShareholder, int companyCIID) { return theCPIDAL.AddShareholderOwner(theShareholder, companyCIID); }
        public String GetUserTypeByNationalID(String nationalID) { return theCPIDAL.GetUserTypeByNationalID(nationalID); }
        public Company GetCompany(int creditInfoID) { return theCreditInfoDAL.GetCompany(creditInfoID); }
        public int GetCIIDByNationalID(String nationalID) { return theCPIDAL.GetCIIDByNationalID(nationalID); }
        public string GetNationalIDByCIID(int CIID) { return theCPIDAL.GetNationalIDByCIID(CIID); }
        public bool AddShareholderOwner(ShareholdersOwnerCompanyBLLC theShareholder, int companyCIID) { return theCPIDAL.AddShareholderOwner(theShareholder, companyCIID); }
        public bool DeleteShareholdersOwner(int shareholdersCIID, int companyCIID, int ownershipID) { return theCPIDAL.DeleteShareholdersOwner(shareholdersCIID, companyCIID, ownershipID); }
        public DataSet GetSubsidiariesAsDataSet(int companyCIID) { return theCPIDAL.GetSubsidiariesAsDataSet(companyCIID); }
        public bool AddSubsidaries(SubsidiariesBLLC theSubsidiaries, int companyCIID) { return theCPIDAL.AddSubsidiaries(theSubsidiaries, companyCIID); }
        public HistoryOperationReviewBLLC GetHistoryOperationReview(int companyCIID) { return theCPIDAL.GetHistoryOperationReview(companyCIID); }
        public bool AddHistoryOperationReview(HistoryOperationReviewBLLC theHOR, int companyCIID) { return theCPIDAL.AddHistoryOperationReview(theHOR, companyCIID); }
        public DataSet GetAreaCodesAsDataSet() { return theCPIDAL.GetAreaCodesAsDataSet(); }
        public DataSet GetEstatesPostalCodeAsDataSet() { return theCPIDAL.GetEstatesPostalCodeAsDataSet(); }
        public DataSet GetTenureAsDataSet() { return theCPIDAL.GetTenureAsDataSet(); }
        public DataSet GetRealEstatesLocationAsDataSet() { return theCPIDAL.GetRealEstatesLocationAsDataSet(); }
        public DataSet GetEstatesPermisesTypeAsDataSet() { return theCPIDAL.GetEstatesPermisesTypeAsDataSet(); }
        public DataSet GetRealEstatesAsDataSet(int companyCIID) { return theCPIDAL.GetRealEstatesAsDataSet(companyCIID); }
        public bool AddRealEstate(RealEstatesBLLC forReal, int companyCIID) { return theCPIDAL.AddRealEstate(forReal, companyCIID); }
        public bool DeleteRealEstate(int realEstateID, int companyCIID) { return theCPIDAL.DeleteRealEstate(realEstateID, companyCIID); }
        public DataSet GetRegistrationFormAsDataSet() { return theCPIDAL.GetRegistrationFormAsDataSet(); }
        public ArrayList FindCompanyReport(int creditInfoID, String name, String nationalID) { return theCPIDAL.FindCompanyReport(creditInfoID, name, nationalID); }
        public ReportCompanyBLLC GetCompanyReport(String nationalID, int CIID, bool useNationalID) { return theCPIDAL.GetCompanyReport(nationalID, CIID, useNationalID, false); }
        public ReportCompanyBLLC GetCompanyReport(String nationalID, int CIID, bool useNationalID, bool isSearchable) { return theCPIDAL.GetCompanyReport(nationalID, CIID, useNationalID, isSearchable); }
        public ReportCompanyBLLC GetCompanyReport(int creditInfoID) { return theCPIDAL.GetCompanyReport(creditInfoID); }
        public bool AddReportCompany(ReportCompanyBLLC theReport) { return theCPIDAL.AddReportCompany(theReport); }
        public int UpdateCompanyFromBasicInfo(Company theComp) { return theCreditInfoDAL.UpdateCompanyFromBasicInfo(theComp); }
        public DataSet GetCityListAsDataSet(bool en) { return theCreditInfoDAL.GetCityListAsDataSet(en); }
        public DataSet GetCityListAsDataSet(string cityName, bool en) { return theCreditInfoDAL.GetCityListAsDataSet(cityName, en); }
        public string GetCityName(int cityID, bool en) { return theCreditInfoDAL.GetCityName(cityID, en); }
        public DataSet GetCityListAsDataSet() { return theCreditInfoDAL.GetCityListAsDataSet(); }

        public bool AddOperationInfo(
            ArrayList arrImportCountrieID,
            ArrayList arrExportCountrieID,
            ArrayList arrCompanyOperation,
            ArrayList arrFormerOperation,
            ArrayList arrCompanyBanks,
            ArrayList arrCompanyCustomerTypes,
            ArrayList arrSalesTerms,
            ArrayList arrPayingTerms,
            int companyCIID) {
            return theCPIDAL.AddOperationInfo(
                arrImportCountrieID,
                arrExportCountrieID,
                arrCompanyOperation,
                arrFormerOperation,
                arrCompanyBanks,
                arrCompanyCustomerTypes,
                arrSalesTerms,
                arrPayingTerms,
                companyCIID);
        }

        public bool AddCompanyOperation(ArrayList arrCompanyOperation, bool currentOperation, int companyCIID) { return theCPIDAL.AddCompanyOperation(arrCompanyOperation, currentOperation, companyCIID); }
        public DataSet GetExportAreasAsDataSet(int companyCIID) { return theCPIDAL.GetExportAreasAsDataSet(companyCIID); }
        public DataSet GetImportAreasAsDataSet(int companyCIID) { return theCPIDAL.GetImportAreasAsDataSet(companyCIID); }
        public bool DeleteSubsidaries(int subsidariesID, int companyCIID) { return theCPIDAL.DeleteSubsidaries(subsidariesID, companyCIID); }
        public DataSet GetEmployeesAsDataSet() { return theCreditInfoDAL.GetEmployeesAsDataSet(); }
        public DataSet GetReportStatusAsDataSet() { return theCPIDAL.GetReportStatusAsDataSet(); }
        public bool UpdateReportCompanyFromControlPage(ReportCompanyBLLC theReport) { return theCPIDAL.UpdateReportCompanyFromControlPage(theReport); }
        public DataSet GetAvailableReportYearsAsDataSet(int companyCIID) { return theCPIDAL.GetAvailableReportYearsAsDataSet(companyCIID); }
        public DataSet GetMaxDatesAsDataSet(int companyCIID) { return theCPIDAL.GetMaxDatesAsDataSet(companyCIID); }
        public DataSet GetArchivedDatesAsDataSet(int companyCIID) { return theCPIDAL.GetArchivedDatesAsDataSet(companyCIID); }
        public DataSet GetAnnualReturAndAccountsDatesAsDataSet(int companyCIID) { return theCPIDAL.GetAnnualReturAndAccountsDatesAsDataSet(companyCIID); }

   
        public bool DeleteStaffCountYear(string year, int companyCIID) { return theCPIDAL.DeleteStaffCountYear(year, companyCIID); }
        public DataSet GetTradeTermsAsDataSet(bool sales) { return theCPIDAL.GetTradeTermsAsDataSet(sales); }
        public DataSet GetCompanyTradeTermsAsDateSet(int companyCIID, bool sales) { return theCPIDAL.GetCompanyTradeTermsAsDateSet(companyCIID, sales); }
        public CompanyNaceCodeBLLC GetCompanyNaceCode(int companyCIID) { return theCPIDAL.GetCompanyNaceCode(companyCIID); }
        public bool AddCompanyHistory_Structure(HistoryOperationReviewBLLC theHOR, int companyCIID) { return theCPIDAL.AddCompanyHistory_Structure(theHOR, companyCIID); }
        public int GetReportStatus(int creditInfoID) { return theCPIDAL.GetReportStatus(creditInfoID); }

        #region Company_Laywers

        public DataSet GetLaywersAsDataSet(int companyCIID) { return theCPIDAL.GetLaywersAsDataSet(companyCIID); }
        public bool AddLaywer(LaywerCompanyBLLC theLaywer, int companyCIID) { return theCPIDAL.AddLaywer(theLaywer, companyCIID); }
        public bool AddLaywer(LaywerIndividualBLLC theLaywer, int companyCIID) { return theCPIDAL.AddLaywer(theLaywer, companyCIID); }
        public bool DeleteLaywer(int laywerCIID, int companyCIID) { return theCPIDAL.DeleteLaywer(laywerCIID, companyCIID); }

        #endregion

        #region Company_Auditors

        public DataSet GetAuditorsAsDataSet(int companyCIID) { return theCPIDAL.GetAuditorsAsDataSet(companyCIID); }
        public bool AddAuditor(AuditorCompanyBLLC theAuditor, int companyCIID) { return theCPIDAL.AddAuditor(theAuditor, companyCIID); }
        public bool AddAuditor(AuditorIndividualBLLC theAuditor, int companyCIID) { return theCPIDAL.AddAuditor(theAuditor, companyCIID); }
        public bool DeleteAuditor(int auditorCIID, int companyCIID) { return theCPIDAL.DeleteAuditor(auditorCIID, companyCIID); }

        #endregion

        #region Company_Capital

        public bool AddCapital(CapitalBLLC myCapital, int companyCIID) { return theCPIDAL.AddCapital(myCapital, companyCIID); }
        public bool UpdateCapital(CapitalBLLC myCapital, int companyCIID) { return theCPIDAL.UpdateCapital(myCapital, companyCIID); }
        public bool DeleteCapital(int companyCIID) { return theCPIDAL.DeleteCapital(companyCIID); }
        public CapitalBLLC GetCapital(int companyCIID) { return theCPIDAL.GetCapital(companyCIID); }

        #endregion

        #region Charges

        public bool AddCharges(ChargesBLLC myCharges, int companyCIID) { return theCPIDAL.AddCharges(myCharges, companyCIID); }
        public bool UpdateCharges(ChargesBLLC myCharges) { return theCPIDAL.UpdateCharges(myCharges); }
        public bool DeleteCharges(int companyCIID) { return theCPIDAL.DeleteCharges(companyCIID); }
        public ChargesBLLC GetCharges(int companyCIID) { return theCPIDAL.GetCharges(companyCIID); }
        public DataSet GetChargesAsDataSet(int companyCIID) { return theCPIDAL.GetChargesAsDataSet(companyCIID); }
        public DataSet GetCurrencyAsDataSet() { return theCPIDAL.GetCurrencyAsDataSet(); }
        public DataSet GetChargesDescriptionAsDataSet() { return theCPIDAL.GetChargesDescriptionAsDataSet(); }

        #endregion
    }
}