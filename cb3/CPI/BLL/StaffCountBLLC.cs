#region

using System;

#endregion

namespace CPI.BLL {
    /// <remarks>
    /// This class holds inormations about companies staff count per year
    /// </remarks>
    [Serializable]
    public class StaffCountBLLC {
        /// <summary>
        /// gets/sets the companies internal system number
        /// </summary>
        public int CIID { get; set; }

        /// <summary>
        /// Gets/sets the year attribute
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// Gets/sets the employeesCount attribute
        /// </summary>
        public int EmployeesCount { get; set; }
    } // END CLASS DEFINITION StaffCountBLLC
}