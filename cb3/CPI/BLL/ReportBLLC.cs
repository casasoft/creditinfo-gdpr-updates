#region

using System;

#endregion

namespace CPI.BLL {
    /// <remarks>
    /// This class services as a data container for Report
    /// </remarks>
    public class ReportBLLC {
        /// <summary>
        /// Gets/Sets the ReportCompany instance
        /// </summary>
        public ReportCompanyBLLC ReportCompanyBLLC { get; set; }

        /// <summary>
        /// Get/Set readyForPublish
        /// </summary>
        public bool ReadyForPublish { get; set; }

        /// <summary>
        /// Gets/Sets reportsAuthorID
        /// </summary>
        public int ReportsAuthorID { get; set; }

        /// <summary>
        /// Gets/Set reportsExpireDate
        /// </summary>
        public DateTime ReportsExpireDate { get; set; }

        /// <summary>
        /// Gets/sets the reportID attribute
        /// </summary>
        public int ReportID { get; set; }
    } // END CLASS DEFINITION ReportBLLC
}