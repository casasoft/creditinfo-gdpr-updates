namespace CPI.BLL {
    /// <remarks>
    /// This class holds information about the companies history, operation and review
    /// </remarks>
    public class HistoryOperationReviewBLLC {
        /// <summary>
        /// Gets/sets the historyNative attribute
        /// </summary>
        public string HistoryNative { get; set; }

        /// <summary>
        /// Gets/sets the historyEN attribute
        /// </summary>
        public string HistoryEN { get; set; }

        /// <summary>
        /// Gets/sets the operationNative attribute
        /// </summary>
        public string OperationNative { get; set; }

        /// <summary>
        /// Gets/sets the operationEN attribute
        /// </summary>
        public string OperationEN { get; set; }

        /// <summary>
        /// Gets/sets the companyReviewNative attribute
        /// </summary>
        public string CompanyReviewNative { get; set; }

        /// <summary>
        /// Gets/sets the companyReviewEN attribute
        /// </summary>
        public string CompanyReviewEN { get; set; }

        /// <summary>
        /// Gets/sets the exists attribute
        /// </summary>
        public bool Exists { get; set; }

        /// <summary>
        /// Gets/sets the businessRegistryTextNative attribute
        /// </summary>
        public string BusinessRegistryTextNative { get; set; }

        /// <summary>
        /// Gets/sets the businessRegistryTextEN attribute
        /// </summary>
        public string BusinessRegistryTextEN { get; set; }

        /// <summary>
        /// Gets/sets the businessRegistryTextNative attribute
        /// </summary>
        public string StatusDescriptionNative { get; set; }

        /// <summary>
        /// Gets/sets the businessRegistryTextEN attribute
        /// </summary>
        public string StatusDescriptionEN { get; set; }
    } // END CLASS DEFINITION History-Operation-ReviewBLLC
}