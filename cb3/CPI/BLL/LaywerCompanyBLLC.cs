#region

using UserAdmin.BLL.CIUsers;

#endregion

namespace CPI.BLL {
    /// <summary>
    /// This class represents a laywer that is a company
    /// </summary>
    public class LaywerCompanyBLLC : Company {
        /// <summary> The native laywer history </summary>
        protected string historyEN;

        /// <summary> The english laywer history </summary>
        protected string historyNative;

        /// <summary>
        /// Gets/Sets the building
        /// </summary>
        public string Building { get; set; }

        /// <summary>
        /// Gets/Sets the office number
        /// </summary>
        public string OfficeNumber { get; set; }

        /// <summary>
        /// Gets/Sets the native laywer histroy
        /// </summary>
        public string HistoryNative { get { return historyNative; } set { historyNative = value; } }

        /// <summary>
        /// Gets/Sets the english laywer histroy
        /// </summary>
        public string HistoryEN { get { return historyEN; } set { historyEN = value; } }
    }
}