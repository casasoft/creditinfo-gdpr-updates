#region

using UserAdmin.BLL.CIUsers;

#endregion

namespace CPI.BLL {
    /// <remarks>
    /// This class inherits the Company class and adds one attribute to it.
    /// </remarks>
    public class SubsidiariesBLLC : Company {
        /// <summary>
        /// Gets/Sets the ownership of company
        /// </summary>
        public double Ownership { get; set; }
    } // END CLASS DEFINITION SubsidariesBLLC
}