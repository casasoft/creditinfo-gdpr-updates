#region

using UserAdmin.BLL.CIUsers;

#endregion

namespace CPI.BLL {
    /// <remarks>
    /// This class inherits the Person class and adds one attribute
    /// </remarks>
    public class PrincipalsBLLC : Indivitual {
        /// <summary>
        /// Gets/Sets the principals jobtitle attribute
        /// </summary>
        public string Jobtitle { get; set; }

        public int JobTitleID { get; set; }
    } // END CLASS DEFINITION PrincipalsBLLC
}