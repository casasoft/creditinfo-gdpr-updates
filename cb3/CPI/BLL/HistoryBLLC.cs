namespace CPI.BLL {
    /// <summary>
    /// Summary description for HistoryBLLC.
    /// </summary>
    public class HistoryBLLC {
        /// <summary>
        /// Gets/sets the _historyID attribute
        /// </summary>
        public int HistoryID { get; set; }

        /// <summary>
        /// Gets/sets the _historyNative attribute
        /// </summary>
        public string HistoryNative { get; set; }

        /// <summary>
        /// Gets/sets the _historyEN attribute
        /// </summary>
        public string HistoryEN { get; set; }
    }
}