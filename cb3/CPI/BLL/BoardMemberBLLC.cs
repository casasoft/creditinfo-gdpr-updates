#region

using UserAdmin.BLL.CIUsers;

#endregion

namespace CPI.BLL {
    /// <remarks>
    /// This class inherits the Person class and adds one attirbute
    /// </remarks>
    public class BoardMemberBLLC : Indivitual {
        /// <summary>
        /// gets/sets the managementPositionID attribute
        /// </summary>
        public int ManagementPostionID { get; set; }

        /// <summary>
        /// Gets/Sets the ManagementPositionName attribute
        /// </summary>
        public string ManagementPositionName { get; set; }
    } // END CLASS DEFINITION BoardMemberBLLC
}