namespace CPI.BLL {
    /// <summary>
    /// Summary description for CompanyNaceCodeBLLC.
    /// </summary>
    public class CompanyNaceCodeBLLC {
        public int CreditInfoID { get; set; }
        public string NaceCode { get; set; }
        public string DescriptionNative { get; set; }
        public string DescriptionEN { get; set; }
        public string ShortDescriptionNative { get; set; }
        public string ShortDescriptionEN { get; set; }
        public string AbbreviationNative { get; set; }
        public string AbbreviationEN { get; set; }
        public string DefinitionNative { get; set; }
        public string DefinitionEN { get; set; }
        public string CodeType { get; set; }
        public int NumberOfCompaniesInNaceCode { get; set; }
    }
}