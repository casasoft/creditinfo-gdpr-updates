#region

using System;

#endregion

namespace CPI.BLL {
    /// <summary>
    /// Summary description for NameValuesPair.
    /// </summary>
    public class NameValuesPair {
        /// <summary>
        /// The name part
        /// </summary>
        private String _Name = "";

        /// <summary>
        /// The value part
        /// </summary>
        private String _Value = "";

        /// <summary>
        /// gets/sets the Name attribute
        /// </summary>
        public String Name { get { return _Name; } set { _Name = value; } }

        /// <summary>
        /// gets/sets the Value attribute
        /// </summary>
        public String Value { get { return _Value; } set { _Value = value; } }
    }
}