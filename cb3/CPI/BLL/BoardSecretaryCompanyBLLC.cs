#region

using UserAdmin.BLL.CIUsers;

#endregion

namespace CPI.BLL {
    /// <summary>
    /// Summary description for BoardSecretaryCompanyBLLC.
    /// </summary>
    public class BoardSecretaryCompanyBLLC : Company {
        /// <summary> The native secretary history </summary>
        protected string historyEN;

        /// <summary> The english secretary history </summary>
        protected string historyNative;

        /// <summary>
        /// Gets/Sets the native secretary histroy
        /// </summary>
        public string HistoryNative { get { return historyNative; } set { historyNative = value; } }

        /// <summary>
        /// Gets/Sets the english secretary histroy
        /// </summary>
        public string HistoryEN { get { return historyEN; } set { historyEN = value; } }
    }
}