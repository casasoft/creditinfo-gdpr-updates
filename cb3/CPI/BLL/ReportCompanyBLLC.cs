#region

using System;
using System.Collections;
using System.Configuration;

#endregion

using Cig.Framework.Base.Configuration;

namespace CPI.BLL {
    /// <remarks>
    /// This class provides all the neccessary info for Report.  Inherits Company
    /// </remarks>
    [Serializable]
    public class ReportCompanyBLLC {
        /// <summary>
        /// The status id
        /// </summary>
        private int statusID = 2; // meaning not ready

// Addon for Lithuania
        // <summary>
// Lithuania addon ends
        /// <summary>
        /// The constructor
        /// </summary>
        public ReportCompanyBLLC() {
            ContactPerson = new ContactPersonBLLC();
            Address = new ArrayList();
        }

        /// <summary>
        /// Gets/sets the contact persons information
        /// </summary>
        public ContactPersonBLLC ContactPerson { get; set; }

        /// <summary>
        /// Gets/sets the company's stockmarket symbol
        /// </summary>
        public String Symbol { get; set; }

        /// <summary>
        /// Gets/sets the name native attribute
        /// </summary>
        public string NameNative { get; set; }

        /// <summary>
        /// Gets/sets the name en attribute
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Gets/sets the legalForm attribute
        /// </summary>
        public int LegalForm { get; set; }

        /// <summary>
        /// Gets/sets the uniqueID attribute
        /// </summary>
        public string UniqueID { get; set; }

        /// <summary>
        /// Gets/sets the tradeName attribute
        /// </summary>
        public string TradeNameNative { get; set; }

        public string TradeNameEN { get; set; }

        /// <summary>
        /// Gets/sets the formerNameNative attribute
        /// </summary>
        public string FormerNameNative { get; set; }

        /// <summary>
        /// Gets/sets the formerNameEN attribute
        /// </summary>
        public string FormerNameEN { get; set; }

        /// <summary>
        /// Gets/sets the email attribute
        /// </summary>
        public string Email { get; set; }

        public string Status { get; set; }

        /// <summary>
        /// Gets/sets the tel attribute
        /// </summary>
/*		public string Tel
		{
			get {return this.tel;}
			set {this.tel = value;}
		}
*/
        /// <summary>
        /// Gets/sets the mobile attribute
        /// </summary>
/*
		public string Mobile
		{
			get {return this.mobile;}
			set {this.mobile = value;}
		}
*/
        /// <summary>
        /// Gets/sets the fax attribute
        /// </summary>
/*
		public string Fax
		{
			get {return this.fax;}
			set {this.fax = value;}
		}
*/
/*Svenni t�k �t
 * 
		/// <summary>
		/// Gets/sets the addressNative attribute
		/// </summary>
		public string AddressNative
		{
			get {return this.addressNative;}
			set {this.addressNative = value;}
		}

		/// <summary>
		/// Gets/sets the addressEN attribute
		/// </summary>
		public string AddressEN
		{
			get {return this.addressEN;}
			set {this.addressEN = value;}
		}
		
		/// <summary>
		/// Gets/sets the cityNative atrribute
		/// </summary>
		public string CityNative
		{
			get {return this.cityNative;}
			set {this.cityNative = value;}
		}
		
		/// <summary>
		/// Gets/sets the cityEN attribute
		/// </summary>
		public string CityEN
		{
			get {return this.cityEN;}
			set {this.cityEN = value;}
		}
		/// <summary>
		///	Gets/sets the areaCode attribute 
		/// </summary>
		public string AreaCode
		{
			get {return this.areaCode;}
			set {this.areaCode = value;}
		}

		/// <summary>
		/// Gets/sets the cityName attribute 
		/// </summary>
		public string CityName
		{
			get {return this.cityName;}
			set {this.cityName = value;}
		}
		
		/// <summary>
		/// Gets/sets the cityID attribute
		/// </summary>
		public int CityID
		{
			get {return this.cityID;}
			set {this.cityID = value;}
		}
		
		/// <summary>
		/// Gets/sets the postBox attribute 
		/// </summary>
		public string PostBox
		{
			get {return this.postBox;}
			set {this.postBox = value;}
		}
*/
        /// <summary>
        /// Gets/sets the homePage attribute
        /// </summary>
        public string HomePage { get; set; }

        /// <summary>
        /// Gets/sets the established attribute 
        /// </summary>
        public DateTime Established { get; set; }

        /// <summary>
        /// Gets/sets the registered attribute
        /// </summary>
        public DateTime Registered { get; set; }

        /// <summary>
        /// Gets/sets the lastContacted attribute
        /// </summary>
        public DateTime LastContacted { get; set; }

        /// <summary>
        /// Gets/sets the lastUpdated attribute
        /// </summary>
        public DateTime LastUpdated { get; set; }

        /// <summary>
        /// Gets/sets the reports expire Date
        /// </summary>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// Gets/sets the internalComments attribute
        /// </summary>
        public string InternalComments { get; set; }

        /// <summary>
        /// Gets/sets the internalComments attribute
        /// </summary>
        public string InternalCommentsEN { get; set; }

        /// <summary>
        /// Gets/sets the naceCodeName attribute
        /// </summary>
        public string NaceCodeName { get; set; }

        /// <summary>
        /// Gets/sets the tradeBankName attribute
        /// </summary>
        public string TradeBankName { get; set; }

        /// <summary>
        /// Gets/sets the registrationInfoNative attribute
        /// </summary>
        public string RegistrationInfoNative { get; set; }

        /// <summary>
        /// Gets/sets the registrationInfoEN attribute
        /// </summary>
        public string RegistrationInfoEN { get; set; }

        /// <summary>
        /// Gets/sets the commentI attribute
        /// </summary>
        public string ExternalCommentNative { get; set; }

        /// <summary>
        /// Gets/sets the commentII attribute
        /// </summary>
        public string ExternalCommentEN { get; set; }

        /// <summary>
        /// Gets/sets the vat attribute
        /// </summary>
        public string VAT { get; set; }

        /// <summary>
        /// Gets/sets the customerComments attribute
        /// </summary>
        public string CustomerComments { get; set; }

        /// <summary>
        /// Gets/sets the importComments attribute
        /// </summary>
        public string ImportComments { get; set; }

        /// <summary>
        /// Gets/sets the exportComments attribute
        /// </summary>
        public string ExportComments { get; set; }

        public string PaymentsComments { get; set; }

        /// <summary>
        /// Gets/sets the salesComments attribute
        /// </summary>
        public string SalesComments { get; set; }

        /// <summary>
        /// Gets/sets the statusID attribute
        /// </summary>
        public int StatusID { get { return statusID; } set { statusID = value; } }

        /// <summary>
        /// Gets/sets the customerCount attribute
        /// </summary>
        public int CustomerCount { get; set; }

        /// <summary>
        /// Gets/sets the registerCIID attribute
        /// </summary>
        public int RegisterCIID { get; set; }

        /// <summary>
        /// Gets/sets the registrationFormID attribute
        /// </summary>
        public int RegistrationFormID { get; set; }

        /// <summary>
        /// Gets/sets the companyCIID attribute
        /// </summary>
        public int CompanyCIID { get; set; }

        /// <summary>
        /// Gets/sets the mainImportAreas attribute
        /// </summary>
        public ArrayList MainImportAreas { get; set; }

        /// <summary>
        /// Gets/sets the mainExportAreas attribute
        /// </summary>
        public ArrayList MainExportAreas { get; set; }

        /// <summary>
        /// Gets/sets the boardMembers attribute
        /// </summary>
        public ArrayList BoardMembers { get; set; }

        /// <summary>
        /// Gets/sets the keyStaff attribute
        /// </summary>
        public ArrayList KeyStaff { get; set; }

        /// <summary>
        /// Gets/sets the staffCount attribute
        /// </summary>
        public ArrayList StaffCount { get; set; }

        /// <summary>
        /// Gets/sets the subsidaries attribute
        /// </summary>
        public ArrayList Subsidaries { get; set; }

        /// <summary>
        /// Gets/sets the financialStatements attribute
        /// </summary>
        public ArrayList FinancialStatements { get; set; }

        /// <summary>
        /// Gets/sets the pNumber attribute
        /// </summary>
        public ArrayList PNumbers { get; set; }

        /// <summary>
        /// Gets/sets the reportID attribute
        /// </summary>
        public int ReportID { get; set; }

        public ArrayList Address { get; set; }

        /// <summary>
        /// Gets/sets the lar attribute
        /// </summary>
        public DateTime LAR { get; set; }

        /// <summary>
        /// Gets/sets the lr attribute
        /// </summary>
        public DateTime LR { get; set; }

        /// <summary>
        /// Gets/sets the structureReportResearchDate attribute
        /// </summary>
        public DateTime StructureReportResearchDate { get; set; }

        // addons for Lithuania
        /// <summary>
        /// Gets/sets the uniqueID attribute
        /// </summary>
        public string NewUniqueID { get; set; }

        public string LERStatusName { get; set; }
        public string SLERStatusName { get; set; }
        public string LERStatusNameEN { get; set; }
        public string SLERStatusNameEN { get; set; }
        public string TypeNative { get; set; }
        public string TypeEN { get; set; }
        public string LERStatusNative { get; set; }
        public string LERStatusEN { get; set; }
        public string SLERStatusNative { get; set; }
        public string SLERStatusEN { get; set; }
        public string CityNative { get; set; }
        public string CityEN { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string JurAddress { get; set; }
        public string OfficeAddress { get; set; }
        public string Director { get; set; }
        public string DirectorAddress { get; set; }
        public string DirectorPhone { get; set; }
        public string NaceCodeNameEN { get; set; }
        public string WWW { get; set; }
        public string Mobile { get; set; }
        public int NUniqueID { get; set; }

        /// <summary>
        /// Gets/sets the established attribute 
        /// </summary>
        public string SEstablished { get; set; }

        /// <summary>
        /// Gets/sets the sRegistered attribute
        /// </summary>
        public string SRegistered { get; set; }

// addon for Lithuania ends
        /// <summary>
        /// Tells if the report has expired
        /// </summary>
        public bool HasExpired {
            get {
                if (ExpireDate < DateTime.Now) {
                    return true;
                } else if (LastContacted <
                           DateTime.Now.AddMonths(
                               -int.Parse(
                                    CigConfig.Configure("lookupsettings.monthsReportExpiresAfterLastContacted").Trim()))) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        /// <summary>
        /// Tells if the report has been set as ready
        /// </summary>
        public bool IsReady {
            get {
                if (statusID == 1) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    } // END CLASS DEFINITION ReportCompanyBLLC
}