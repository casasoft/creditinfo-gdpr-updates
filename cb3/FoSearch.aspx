<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="new_user_controls/FoOptions.ascx" %>

<%@ Page Language="c#" CodeBehind="FoSearch.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.FoSearch" %>

<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Creditinfo Malta</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script language="JavaScript" src="popup.js"></script>

    <link href="css/FoCIGStyles.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        BR.pageEnd
        {
            page-break-after: always;
        }
    </style>

    <script language="javascript">
        function checkEnterKey() {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                FoSearchForm.btnFind.click();
            }
        }

        function SetFormFocus() {
            document.FoSearchForm.txtName.focus();
        }

        function ShowHidePopup(parent) {
            var helpdiv = document.getElementById('cis_search_info_div');
            if (helpdiv.style.display == 'none') {
                helpdiv.style.display = 'block';
                helpdiv.innerHTML = '<p>&nbsp;</p><h3>Instructions</h3><ol><li>To search by ID-/Reg.no OR by VAT-number, all other search boxes must be empty.</li><li>If you search for a company by Registration number, leave a space after the letter (eg. C 12345).</li><li>To search for a trader by VAT number, enter it in the VAT-number Box. (eg. 12345678). In VAT results, the \'NOTE\' field shows identified traders as ACTIVE or INACTIVE in the latest VAT Register.</li><li>To search for a person by ID, insert ID (without leading zeroes) folllowed by letter (eg. to find 00012345M use 12345M).</li><li>To search for a person by Name, insert surname followed by name.</li><li>To search for a company, any unique part of the company name should be sufficient.</li><li>When searching by Name, you may narrow the search by choosing the City from dropdown.</li><li>When rare or unique names are searched a part of the name is usually enough to get quick results.</li><li>Should you not be able to find a certain person then they might be Interdicted/Incapacitated. You may search in \'Other Information\' section using only the surname.</li><li>If you are not successful in your search kindly contact us for assistance.</li></ol>';
            } else { helpdiv.style.display = 'none'; helpdiv.innerHTML = ''; }
        }
    </script>

</head>
<body style="background-image: url(img/mainback.gif)" leftmargin="0" onload="SetFormFocus()"
    rightmargin="0" ms_positioning="GridLayout">
    <form id="FoSearchForm" name="Creditinfo Malta" method="post" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <uc1:head ID="Head2" runat="server"></uc1:head>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <td style="background-image: url(img/pagename_large_back.gif)" width="50%">
                        </td>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="#951e16"
                                border="0">
                                <tr>
                                    <td bgcolor="#951e16">
                                        <span style="width: 3px"></span>
                                    </td>
                                    <td bgcolor="#951e16">
                                        <uc1:panelBar ID="Panelbar2" runat="server"></uc1:panelBar>
                                    </td>
                                    <td align="right" bgcolor="#951e16">
                                        <uc1:language ID="Language2" runat="server"></uc1:language>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-image: url(img/pagename_large_back.gif)" width="50%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="white"
                    border="0">
                    <tr>
                        <td>
                            <p>
                                <table id="tbDefault" cellspacing="0" cellpadding="0" width="100%" align="center">
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
                                                <tr>
                                                    <td>
                                                        <uc1:UserInfo ID="UserInfo1" runat="server"></uc1:UserInfo>
                                                    </td>
                                                    <td align="right">
                                                        <uc1:FoOptions ID="FoOptions1" runat="server"></uc1:FoOptions>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <!-- Content begins -->
                                    <tr>
                                        <td>
                                            <table id="Table5" cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
                                                <tr>
                                                    <td class="pageheader" colspan="4">
                                                        <asp:Label ID="lblSearch" runat="server" CssClass="HeadMain">Search</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblNationalID" runat="server">ID:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblName" runat="server">Name</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblCity" runat="server">City</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblvatNumber" runat="server">Vat - number:</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtNationalID" runat="server" Width="60px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtName" runat="server" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlCity" runat="server" Width="100px">
                                                            <asp:ListItem Value="0">Not specified</asp:ListItem>
                                                            <asp:ListItem Value="2445">Attard</asp:ListItem>
                                                            <asp:ListItem Value="2446">Balzan</asp:ListItem>
                                                            <asp:ListItem Value="2447">Birgu</asp:ListItem>
                                                            <asp:ListItem Value="2448">Birkirkara</asp:ListItem>
                                                            <asp:ListItem Value="2449">Birzebbuga</asp:ListItem>
                                                            <asp:ListItem Value="2450">Bormla</asp:ListItem>
                                                            <asp:ListItem Value="2451">Dingli</asp:ListItem>
                                                            <asp:ListItem Value="2452">Fgura</asp:ListItem>
                                                            <asp:ListItem Value="2453">Floriana</asp:ListItem>
                                                            <asp:ListItem Value="2454">Fontana</asp:ListItem>
                                                            <asp:ListItem Value="2455">Ghajnsielem</asp:ListItem>
                                                            <asp:ListItem Value="2456">Gharb</asp:ListItem>
                                                            <asp:ListItem Value="2457">Gharghur</asp:ListItem>
                                                            <asp:ListItem Value="2458">Ghasri</asp:ListItem>
                                                            <asp:ListItem Value="2459">Ghaxaq</asp:ListItem>
                                                            <asp:ListItem Value="2460">Gudja</asp:ListItem>
                                                            <asp:ListItem Value="2461">Gzira</asp:ListItem>
                                                            <asp:ListItem Value="2462">Hamrun</asp:ListItem>
                                                            <asp:ListItem Value="2463">Iklin</asp:ListItem>
                                                            <asp:ListItem Value="2464">Isla</asp:ListItem>
                                                            <asp:ListItem Value="2465">Kalkara</asp:ListItem>
                                                            <asp:ListItem Value="2466">Kercem</asp:ListItem>
                                                            <asp:ListItem Value="2467">Kirkop</asp:ListItem>
                                                            <asp:ListItem Value="2468">Lija</asp:ListItem>
                                                            <asp:ListItem Value="2469">Luqa</asp:ListItem>
                                                            <asp:ListItem Value="2470">Marsa</asp:ListItem>
                                                            <asp:ListItem Value="2471">Marsaskala</asp:ListItem>
                                                            <asp:ListItem Value="2472">Marsaxlokk</asp:ListItem>
                                                            <asp:ListItem Value="2473">Mdina</asp:ListItem>
                                                            <asp:ListItem Value="2474">Mellieha</asp:ListItem>
                                                            <asp:ListItem Value="2475">Mgarr</asp:ListItem>
                                                            <asp:ListItem Value="2476">Mosta</asp:ListItem>
                                                            <asp:ListItem Value="2477">Mqabba</asp:ListItem>
                                                            <asp:ListItem Value="2478">Msida</asp:ListItem>
                                                            <asp:ListItem Value="2479">Mtarfa</asp:ListItem>
                                                            <asp:ListItem Value="2480">Munxar</asp:ListItem>
                                                            <asp:ListItem Value="2481">Nadur</asp:ListItem>
                                                            <asp:ListItem Value="2482">Naxxar</asp:ListItem>
                                                            <asp:ListItem Value="2483">Paola</asp:ListItem>
                                                            <asp:ListItem Value="2484">Pembroke</asp:ListItem>
                                                            <asp:ListItem Value="2485">Pieta'</asp:ListItem>
                                                            <asp:ListItem Value="2486">Qala</asp:ListItem>
                                                            <asp:ListItem Value="2487">Qormi</asp:ListItem>
                                                            <asp:ListItem Value="2488">Qrendi</asp:ListItem>
                                                            <asp:ListItem Value="2489">Rabat (Ghawdex)</asp:ListItem>
                                                            <asp:ListItem Value="2490">Rabat (Malta)</asp:ListItem>
                                                            <asp:ListItem Value="2491">Safi</asp:ListItem>
                                                            <asp:ListItem Value="2492">San Giljan</asp:ListItem>
                                                            <asp:ListItem Value="2493">San Gwann</asp:ListItem>
                                                            <asp:ListItem Value="2494">San Lawrenz</asp:ListItem>
                                                            <asp:ListItem Value="2495">San Pawl il-Bahar</asp:ListItem>
                                                            <asp:ListItem Value="2496">Sannat</asp:ListItem>
                                                            <asp:ListItem Value="2497">Santa Lucija</asp:ListItem>
                                                            <asp:ListItem Value="2498">Santa Venera</asp:ListItem>
                                                            <asp:ListItem Value="2499">Siggiewi</asp:ListItem>
                                                            <asp:ListItem Value="2500">Sliema</asp:ListItem>
                                                            <asp:ListItem Value="2501">Swieqi</asp:ListItem>
                                                            <asp:ListItem Value="2502">Ta' Xbiex</asp:ListItem>
                                                            <asp:ListItem Value="2503">Tarxien</asp:ListItem>
                                                            <asp:ListItem Value="2504">Valletta</asp:ListItem>
                                                            <asp:ListItem Value="2505">Xaghra</asp:ListItem>
                                                            <asp:ListItem Value="2506">Xewkija</asp:ListItem>
                                                            <asp:ListItem Value="2507">Xghajra</asp:ListItem>
                                                            <asp:ListItem Value="2508">Zabbar</asp:ListItem>
                                                            <asp:ListItem Value="2509">Zebbug (Ghawdex)</asp:ListItem>
                                                            <asp:ListItem Value="2510">Zebbug (Malta)</asp:ListItem>
                                                            <asp:ListItem Value="2511">Zejtun</asp:ListItem>
                                                            <asp:ListItem Value="2512">Zurrieq</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblVatNumberPrefix" runat="server">MT</asp:Label>
                                                        <asp:TextBox ID="txtVatNumber" runat="server" Width="100px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="15" colspan="4">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <table id="Table7" cellspacing="0" cellpadding="0" width="200" border="0">
                                                            <tr>
                                                                <td>
                                                                    <div class="AroundButtonSmallMargin" style="width: 90px; height: 10px">
                                                                        <asp:Button ID="btnFind" runat="server" CssClass="RegisterButton" Text="Find"></asp:Button></div>
                                                                </td>
                                                                <td>
                                                                    <div class="AroundButtonSmallMargin" style="width: 90px; height: 10px">
                                                                        <asp:Button ID="btnClear" runat="server" CssClass="RegisterButton" Text="Clear">
                                                                        </asp:Button></div>
                                                                </td>
                                                                <td>
                                                                    &nbsp;<a href="#" onclick="return false;" onmouseover="ShowHidePopup(this);" onmouseout="ShowHidePopup(this);"><img
                                                                        height="20" src="CreditWatch/img/question2.gif" width="12" border="0"></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <div id="cis_search_info_div" style="display: none;">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="15">
                                                    </td>
                                                    <td height="15">
                                                    </td>
                                                    <td height="15">
                                                    </td>
                                                    <td height="15">
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="Table6" cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
                                                <tr>
                                                    <td style="width: 612px" align="right">
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tbDefault12" cellspacing="0" cellpadding="0" width="97%" align="center"
                                                border="0">
                                                <tr id="trCompanyInfoRow" runat="server">
                                                    <td valign="top" align="left">
                                                        <table width="100%">
                                                            <tr>
                                                                <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblCreditinfoIDLabel" runat="server" Visible="False" Font-Bold="True">Creditinfo ID</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                    <asp:Label ID="lblResultCreditinfoID" runat="server" Visible="False">ResultCreditinfoID</asp:Label>
                                                                    &nbsp;
                                                                    <td valign="top">
                                                                        <asp:Label ID="lblDateOfReportTitle" runat="server" Font-Bold="True">Date of report:</asp:Label>
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblNationalIDLabel" runat="server" Font-Bold="True">National id</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                    <asp:Label ID="lblResultNationalID" runat="server">ResultNationalID</asp:Label>&nbsp;
                                                                    <asp:LinkButton ID="lbtnNatIdTAFReport" runat="server">TAF-report</asp:LinkButton>&nbsp;
                                                                    <asp:Label ID="lblNote" runat="server" Visible="True">Note</asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblDateOfReport" runat="server">03.11.2004 16:40:22</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trRegDate" runat="server">
                                                                <td style="width: 111px; height: 15px" valign="top">
                                                                    <asp:Label ID="lblRegDateTitle" runat="server" Font-Bold="True">Reg date</asp:Label>
                                                                </td>
                                                                <td style="width: 315px; height: 15px" valign="top">
                                                                    <asp:Label ID="lblResultRegDate" runat="server">ResultRegDate</asp:Label>
                                                                </td>
                                                                <td style="height: 15px" valign="top">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 111px; height: 15px" valign="top">
                                                                    <asp:Label ID="lblNameLabel" runat="server" Font-Bold="True">Name</asp:Label>
                                                                </td>
                                                                <td style="width: 315px; height: 15px" valign="top">
                                                                    <asp:Label ID="lblResultName" runat="server">ResultName</asp:Label>
                                                                </td>
                                                                <td style="height: 15px" valign="top">
                                                                    <asp:Label ID="lblSubjectIsOnMonitoring" runat="server">Subject is on monitoring</asp:Label><asp:LinkButton
                                                                        ID="lbtnAddToMonitoring" runat="server">Add to monitoring</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 111px; height: 15px" valign="top">
                                                                    <asp:Label ID="lblAddressLabel" runat="server" Font-Bold="True">Address</asp:Label>
                                                                </td>
                                                                <td style="width: 315px; height: 15px" valign="top">
                                                                    <asp:LinkButton ID="lblResultAddress" runat="server">ResultAddress</asp:LinkButton>
                                                                </td>
                                                                <td style="height: 15px" valign="top">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 111px; height: 14px" valign="top">
                                                                    <asp:Label ID="lblCityLabel" runat="server" Font-Bold="True">City</asp:Label>
                                                                </td>
                                                                <td style="width: 315px; height: 14px" valign="top">
                                                                    <asp:Label ID="lblResultCity" runat="server">ResultCity</asp:Label>
                                                                </td>
                                                                <td style="height: 14px" valign="top">
                                                                    <asp:Label ID="lblReportRefIDTitle" runat="server" Font-Bold="True">Report ref.:</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 111px; height: 15px" valign="top">
                                                                    <asp:Label ID="lblCountryLabel" runat="server" Font-Bold="True">Country</asp:Label>
                                                                </td>
                                                                <td style="width: 315px; height: 15px" valign="top">
                                                                    <asp:Label ID="lblResultCountry" runat="server">ResultCountry</asp:Label>
                                                                </td>
                                                                <td style="height: 15px" valign="top">
                                                                    <asp:Label ID="lblReportRefID" runat="server">11005:22</asp:Label>
                                                                </td>
                                                            </tr>
                                                              <tr>
                                                                <td style="width: 111px; height: 15px" valign="top">
                                                                    <asp:Label ID="lblPostalCodeLabel" runat="server" Font-Bold="True">PostalCode</asp:Label>
                                                                </td>
                                                                <td style="width: 315px; height: 15px" valign="top">
                                                                    <asp:Label ID="lblResultPostalCode" runat="server">ResultPostalCode</asp:Label>
                                                                </td>
                                                                <td style="height: 15px" valign="top">
                                                                </td>
                                                            </tr>
                                                            <tr id="trSecretary" runat="server" style="display:none"> 
                                                                <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblSecretaryLabel" runat="server" Font-Bold="True">Secretarty</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                    <asp:Label ID="lblResultSecretary" runat="server">ResultSecretary</asp:Label>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblIsCompany" runat="server" Visible="False">true</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trTelephone" runat="server">
                                                                <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblTelephoneLabel" runat="server" Font-Bold="True">Telphone</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                    <asp:Label ID="lblResultTelephone" runat="server">ResultTelphone</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trFax" runat="server">
                                                                <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblFaxLabel" runat="server" Font-Bold="True">Fax</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                    <asp:Label ID="lblResultFax" runat="server">ResultFax</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trWeb" runat="server">
                                                                <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblWebLabel" runat="server" Font-Bold="True">Web</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                    <asp:Label ID="lblResultWeb" runat="server">ResultWeb</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trEmail" runat="server">
                                                                <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblEmailLabel" runat="server" Font-Bold="True">E-Mail</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                <asp:Label ID="lblResultEmail" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr id="trLegalForm" runat="server">
                                                                <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblLegalFormLabel" runat="server" Font-Bold="True">Legal Form</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                    <asp:Label ID="lblResultLegalForm" runat="server">ResultLegalForm</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trMainActivity" runat="server"  visible=false>
                                                             <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblMainActivity" runat="server" Font-Bold="True">Main Activity</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                    <asp:Label ID="lblMainActivityResult" runat="server">MainActivityResult</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trCompanyStatus" runat="server">
                                                                <td style="width: 111px" valign="top">
                                                                    <asp:Label ID="lblCompanyStatusLabel" runat="server" Font-Bold="True">Company status</asp:Label>
                                                                </td>
                                                                <td style="width: 315px" valign="top">
                                                                    <asp:Label ID="lblResultCompanyStatus" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr id="trVatNumbers" runat="server" visible=false>
                                                                <td colspan="5">
                                                                    <asp:Repeater ID="rptVatNumbers" runat="server">
                                                                        <HeaderTemplate>
                                                                            <table style="width: 100%;" >
                                                                                <tr style="background-color: #C0C0C0; font-weight: bold; width: 100%; height:20px;">
                                                                                    <td width="20%">
                                                                                        <asp:Label ID="lblHeaderVatNumber" runat="server" Text="VAT Number MT"></asp:Label>
                                                                                    </td>
                                                                                    <td width="80%">
                                                                                        <asp:Label ID="lblHeaderTradeName" runat="server" Text="Trader name"></asp:Label>
                                                                                    </td>
                                                                              <%--      <td width="17%">
                                                                                        <asp:Label ID="lblHeaderTel" runat="server" Text="Tel."></asp:Label>
                                                                                    </td>
                                                                                    <td width="17%">
                                                                                        <asp:Label ID="lblHeaderFax" runat="server" Text="Fax"></asp:Label>
                                                                                    </td>
                                                                                    <td width="21%">
                                                                                        <asp:Label ID="lblHeaderNote" runat="server" Text="Note"></asp:Label>
                                                                                    </td>--%>
                                                                                </tr>
                                       <%--                                         <tr>
                                                                                    <td colspan="1"></td>
                                                                                    <td colspan="4" style="background-color: #C0C0C0; font-weight: bold; width: 100%; height:20px;">
                                                                                    <asp:Label ID="lblHeaderAddress" runat="server" Text="Address"></asp:Label>
                                                                                    </td>
                                                                                </tr>--%>
                                                                           
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr style="border-bottom-style: solid; border-bottom-width: 2px; border-bottom-color: #000000">
                                                                                <td width="20%">
                                                                                    <%# DataBinder.Eval(Container.DataItem , "MTNumber") %>
                                                                                </td>
                                                                                <td width="80%">
                                                                                    <%# DataBinder.Eval(Container.DataItem , "TraderName") %>
                                                                                    <span style='<%#this.GetStyleForVat(DataBinder.Eval(Container.DataItem , "Note"))%>'>
                                                                                       <%# this.TransformText(DataBinder.Eval(Container.DataItem , "Note")) %> 
                                                                                    </span>
                                                                                </td>
                                                                                <%--<td>
                                                                                    <%# DataBinder.Eval(Container.DataItem , "Tel") %>
                                                                                </td>
                                                                                <td width="15%">
                                                                                    <%# DataBinder.Eval(Container.DataItem , "Fax") %>
                                                                                </td>--%>
                                                                               
                                                                            </tr>
                                                                            <tr >
                                                                                <td colspan="1">
                                                                                </td>
                                                                                <td colspan="4">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "FullAddressString")%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="background-color: #C0C0C0; width: 100%;" >
                                                                            <td colspan="5"></td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                         </table>
                                                                        </FooterTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tbDefault123" cellspacing="0" cellpadding="0" width="97%" align="center"
                                                border="0">
                                                <tr id="trAdditionalInformation" runat="server">
                                                    <td valign="top" align="left">
                                                        <table id="Table2" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblAdditionalInformations" runat="server" Font-Bold="True" Font-Size="Medium">Additional informations</asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trTAFReport" runat="server">
                                                                    <td>
                                                                        <asp:LinkButton ID="lbtnTAFReport" runat="server">TAF Report</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="trBasicRow" runat="server">
                                                                        <asp:LinkButton ID="lbtnBasicReport" runat="server">Basic report</asp:LinkButton>
                                                                        <BR />
                                                                        Company Profile includes TAF report, company management and ownership, capital structure and other selected information
                                                                        <BR />
                                                                                <BR />
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompanyRow" runat="server">
                                                                    <td style="height: 15px">
                                                                        <asp:LinkButton ID="lbtnCompanyReport" runat="server">Company report</asp:LinkButton>&nbsp;<asp:Label
                                                                            ID="lblLastUpdate" runat="server" Visible="True"></asp:Label>
                                                                            <br />
                                                                            Credit Report includes Company Profile, 3 years financials with key financial ratios, Financial Strength Indicator, Credit Limit, Shareholders' percentages, other selected information.
                                                                             <br />
                                                                                <br />
                                                                    </td>
                                                                    <td>
                                                                    <asp:LinkButton ID="lbtnOrderFiscalUpdate" runat="server" Visible="True">Request update</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCreditInfoRow" runat="server">
                                                                    <td style="height: 14px">
                                                                        <asp:LinkButton ID="lbtnCreditinfoReport" runat="server">Creditinfo report</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trOrderCompanyReportRow" runat="server">
                                                                    <td style="height: 16px">
                                                                        <asp:LinkButton ID="lbtnOrderCompanyReport" runat="server">Order company report</asp:LinkButton>
                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAFS" runat="server"><td >
                                                                <!--Credit Report includes 3 years of filed Financial Statements-->
                                                                <!-- (AFS) - to get the latest filed AFS, please Request Update-->
                                                                    Company Report includes Company Profile,  3 years financials with key financial ratios, Financial Strength Indicator, Credit Limit, Shareholders' percentages, other selected information
                                                                 <br />
                                                                </td></tr>
                                                                 <tr id="trOrderRow" runat="server">
                                                                    <td style="height: 16px">
                                                                        <asp:LinkButton ID="lbtnOrderReport" runat="server">Order report</asp:LinkButton>
                                                                    <br />
                                                                    An in-depth analysis of all of the above products in more detail and Other relevant information such as company history, recently filed documents at MFSA, subsidiaries percentages and up to 5 years of financial statements
                                                                  
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                <tr id="trLastArchived" runat="server">
                                                                    <td style="height: 16px"><BR />
                                                                    <!--Company Analysis report includes up to 5 years of filed Financial Statements-->
                                                                        <asp:Label ID="lblLastArchived"  runat="server" Visible="False"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFinancialStatementSelect" runat="server">
                                                                    <td style="height: 16px">                                                                        
                                                                        <asp:DropDownList ID="ddlFS" runat="server" Width="150px" AutoPostBack="True" EnableViewState ="true" >
                                                                        </asp:DropDownList>
                                                                        &nbsp;                                                                       
                                                                    </td>
                                                                </tr>
                                                                <tr id="trOrderStatementsUpdate" runat="server">
                                                                    <td style="height: 10px">
                                                                    </td>
                                                                </tr>
                                                                <tr id="trDDDSelect" runat="server">
                                                                    <td style="height: 15px">
                                                                        <asp:LinkButton ID="lbtnDDD" runat="server" Visible="False">DDD</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trPaymentBehaviourSelect" runat="server">
                                                                    <td>
                                                                        <asp:LinkButton ID="lbtnPaymentBehaviour" runat="server">Payment behaviour</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table id="tbDefault1234" cellspacing="0" cellpadding="0" width="97%" align="center"
                                    border="0">
                                    <tr id="trOrderReport" runat="server">
                                        <td style="height: 312px" valign="top" align="left" colspan="2">
                                            <table id="Table4" cellspacing="1" cellpadding="1" width="100%" border="0">
                                                <tr>
                                                    <td colspan="2">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pageheader" colspan="12">
                                                        <asp:Label ID="lblReportOrder" runat="server" Font-Bold="True">Order Company Analysis report</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px; height: 15px">
                                                    </td>
                                                    <td style="height: 18px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                        <asp:Label ID="lblOrderNationalIDLabel" runat="server" Font-Bold="True">Company national id</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOrderDispCompanyNationalID" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                        <asp:Label ID="lblOrderNameLabel" runat="server" Font-Bold="True">Company name</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOrderDispCompanyName" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                        <asp:Label ID="lblOrderSubscriberLabel" runat="server" Font-Bold="True"> Subscriber</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOrderDispSubscriber" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                        <asp:Label ID="lblOrderUserNameLabel" runat="server" Font-Bold="True">User name</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOrderDispUserName" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                        <asp:Label ID="lblOrderProductLabel" runat="server" Font-Bold="True">Report Type:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOrderProduct" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOrderProductID" runat="server" Visible="False">-1</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                        <asp:Label ID="lblOrderDeliveryTypeLabel" runat="server" Font-Bold="True">Delivery type</asp:Label>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr id="trDeliveryType" runat="server">
                                                    <td style="width: 162px">
                                                        <asp:RadioButtonList ID="rbtnlDeliveryType" runat="server">
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px">
                                                        <div class="AroundButtonSmallMargin" style="width: 150px; height: 10px">
                                                            <asp:Button ID="btnOrderReport" runat="server" CssClass="RegisterButton" Text="Order Report">
                                                            </asp:Button></div>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdPaymentBahaviour" valign="top" align="left" colspan="2" runat="server">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdFinancialStatement" valign="top" align="left" colspan="2" runat="server">
                                        </td>
                                    </tr>
                                </table>
                                <table id="tbDefault12345" cellspacing="0" cellpadding="0" width="97%" align="center"
                                    border="0">
                                    <tr id="trOrderReceipe" runat="server">
                                        <td align="left" colspan="2">
                                            <table id="Table3" cellspacing="1" cellpadding="1" width="100%" border="0">
                                                <tr>
                                                    <td class="pageheader" valign="top" colspan="2">
                                                        <asp:Label ID="lblOrderSummary" runat="server">Order summary</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 162px; height: 4px" valign="top">
                                                    </td>
                                                    <td style="height: 4px" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 120px" valign="top">
                                                        <asp:Label ID="lblRecNationalIDLabel" runat="server" Font-Bold="True">National ID</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblRecNationalID" runat="server">Label</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 120px; height: 15px" valign="top">
                                                        <asp:Label ID="lblRecNameLabel" runat="server" Font-Bold="True">Name</asp:Label>
                                                    </td>
                                                    <td style="height: 15px" valign="top">
                                                        <asp:Label ID="lblRecName" runat="server">Label</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 120px" valign="top">
                                                    </td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" colspan="2">
                                                        <asp:Label ID="lblOrderReceived" runat="server" Font-Bold="True">Order received</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table id="Table1" cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
                                    <tr id="trReportDisplay" runat="server" enableviewstate="false">
                                        <td id="tdReportDisplay" runat="server">
                                        </td>
                                    </tr>
                                    <tr id="trIndividualGridHeader" runat="server">
                                        <td>
                                            &nbsp;<asp:Label ID="lblIndividualGridHeader" runat="server" ForeColor="Black">Individuals</asp:Label>
                                            <asp:Label ID="lblIndividualLImit" runat="server" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trIndividualSearchGrid" runat="server">
                                        <td>
                                            <asp:DataGrid ID="dtgrIndividual" runat="server" Width="100%" Font-Size="Smaller"
                                                ForeColor="Black" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="None"
                                                BorderWidth="1px" BackColor="White" CellPadding="4" GridLines="Vertical">
                                                <FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
                                                <SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
                                                <AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
                                                <ItemStyle ForeColor="Black" BackColor="White"></ItemStyle>
                                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#895A4F"></HeaderStyle>
                                                <Columns>
                                                    <asp:ButtonColumn Text="Select" HeaderText="Select" CommandName="Select"></asp:ButtonColumn>
                                                    <asp:BoundColumn DataField="Number" SortExpression="Number" HeaderText="National ID">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Name"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="NameNative" SortExpression="NameNative"
                                                        HeaderText="Name"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="NameEN" SortExpression="NameEN" HeaderText="Name (EN)">
                                                    </asp:BoundColumn>
                                                    <asp:ButtonColumn Text="Address" HeaderText="Address" CommandName="Address"></asp:ButtonColumn>
                                                    <asp:BoundColumn Visible="False" DataField="StreetNative" SortExpression="StreetNative"
                                                        HeaderText="AddressNative"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="StreetEN" SortExpression="StreetEN" HeaderText="Address (EN)">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="PostalCode" HeaderText="Post code"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="City"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="CityNative" SortExpression="CityNative"
                                                        HeaderText="CityNative"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="CityEN" SortExpression="CityEN" HeaderText="City (EN)">
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages">
                                                </PagerStyle>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trCompanyGridHeader" runat="server">
                                        <td>
                                            &nbsp;<asp:Label ID="lblCompanyGridHeader" runat="server" ForeColor="Black"> Companies</asp:Label>
                                            <asp:Label ID="lblCompanyLImit" runat="server" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trCompanySearchGrid" runat="server">
                                        <td>
                                            <asp:DataGrid ID="dtgrCompanys" runat="server" Width="100%" Font-Size="Smaller" ForeColor="Black"
                                                AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                BackColor="White" CellPadding="4" GridLines="Vertical">
                                                <FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
                                                <SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
                                                <AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
                                                <ItemStyle ForeColor="Black" BackColor="White"></ItemStyle>
                                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#895A4F"></HeaderStyle>
                                                <Columns>
                                                    <asp:ButtonColumn Text="Select" HeaderText="Select" CommandName="Select"></asp:ButtonColumn>
                                                    <asp:BoundColumn DataField="Number" SortExpression="Number" HeaderText="National ID">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Name"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="NameNative" SortExpression="NameNative"
                                                        HeaderText="Name"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="NameEN" SortExpression="NameEN" HeaderText="Name (EN)">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Address"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="StreetNative" SortExpression="StreetNative"
                                                        HeaderText="AddressNative"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="StreetEN" SortExpression="StreetEN" HeaderText="Address (EN)">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="PostalCode" HeaderText="Post code"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="City"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="CityNative" SortExpression="CityNative"
                                                        HeaderText="CityNative"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="CityEN" SortExpression="CityEN" HeaderText="City (EN)">
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages">
                                                </PagerStyle>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 15px" align="left">
                                            <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Black"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <table id="Table10" cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="disclaimer" align="left">
                                            <asp:Label ID="lblDisclaimer1" runat="server"> Disclaimer 1</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 15px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="disclaimer" align="left">
                                            <asp:Label ID="lblDisclaimer2" runat="server"> Disclaimer 2</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 15px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 15px" align="center" bgcolor="#951e16">
                                        </td>
                                    </tr>
                                </table>
                                &nbsp;</p>
                        </td>
                    </tr>
                    <!-- Content ends -->
                </table>
                <p>
                </p>
            </td>
        </tr>
    </table>
    </TD></TR><tr>
        <td height="6" bgcolor="transparent">
        </td>
    </tr>
    <tr>
        <td align="center">
            <uc1:footer ID="Footer1" runat="server"></uc1:footer>
        </td>
    </tr>
    </TBODY></TABLE></form>

    <script language="JavaScript" src="wz_tooltip.js" type="text/javascript"></script>

</body>
</html>
