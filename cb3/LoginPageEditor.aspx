﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPageEditor.aspx.cs" Inherits="cb3.LoginPageEditor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="html" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="new_user_controls/UserInfo.ascx" %>

<html>
<head runat="server">
    <title>default</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="css/CIGStyles.css" type="text/css" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
</head>

<body ms_positioning="GridLayout">
    <form id="fromDefault" method="post" runat="server">
        <table width="997" height="600" align="center" border="0">
            <tr>
                <td colspan="4">
                    <uc1:head ID="Head1" runat="server"></uc1:head>
                </td>
            </tr>
            <tr valign="top">
                <td width="1"></td>
                <td>
                    <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <uc1:language ID="Language1" runat="server"></uc1:language>
                            </td>
                            <td></td>
                            <td align="right">
                                <ucl:options ID="Options1" runat="server"></ucl:options>
                                </UCL:OPTIONS></UCL:OPTIONS>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="right">
                                <ucl:UserInfo ID="UserInfo1" runat="server"></ucl:UserInfo>
                            </td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr valign="top">
                            <td colspan="3">
                                <!-- Main Body Starts -->
                                <asp:ToolkitScriptManager runat="server"></asp:ToolkitScriptManager>

                                <asp:UpdateProgress runat="server">
                                    <ProgressTemplate>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-striped active" id="bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            </div>
                                        </div>
                                        <h3 class="text-center">Processing, Please wait... </h3>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>

                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <section>
                                            <h1>Left Menu box text:  </h1>
                                            <html:Editor runat="server" ID="leftBoxEditor" />
                                            <button runat="server" id="submitLeftText" onserverclick="submitLeftText_Click" class="btn btn-success">Save </button>
                                            <button runat="server" id="resetLeftBtn" onserverclick="LoadLeftBox" class="btn btn-danger">Reset </button>
                                        </section>

                                        <section>
                                            <h1>Right Menu box text: </h1>
                                            <html:Editor runat="server" ID="rightBoxEditor" />
                                            <button runat="server" class="btn btn-success" onserverclick="submitRightText_Click">Save </button>
                                            <button runat="server" id="resetRightBtn" onserverclick="LoadRightBox" class="btn btn-danger">Reset </button>
                                        </section>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="resetLeftBtn" />
                                        <asp:AsyncPostBackTrigger ControlID="resetRightBtn" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <br />
                                <br />
                                <a href="Default.aspx" class="btn btn-primary">Back to Main </a>
                                <!-- Main Body Ends -->
                            </td>
                        </tr>
                        <tr>
                            <td height="20"></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
                        </tr>
                    </table>
                </td>
                <td width="2"></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <uc1:footer ID="Footer1" runat="server"></uc1:footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
