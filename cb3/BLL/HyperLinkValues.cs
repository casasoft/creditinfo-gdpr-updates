#region

using System;

#endregion

namespace CreditInfoGroup.BLL {
    /// <summary>
    /// Summary description for HyperLinkValues.
    /// </summary>
    [Serializable]
    public class HyperLinkValues {
        private string _Class = "";
        private string _Href = "";
        private string _ID = "";
        private string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; } }
        public string Href { get { return _Href; } set { _Href = value; } }
        public string Class { get { return _Class; } set { _Class = value; } }
        public string ID { get { return _ID; } set { _ID = value; } }
    }
}