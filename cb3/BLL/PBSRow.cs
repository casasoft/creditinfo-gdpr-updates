namespace CreditInfoGroup.BLL {
    /// <summary>
    /// Summary description for PBSRow.
    /// </summary>
    public class PBSRow {
        public string Year { get; set; }
        public string Quarter { get; set; }
        public string Days { get; set; }
        public string Std { get; set; }
        public string Count { get; set; }
    }
}