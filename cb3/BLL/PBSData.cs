#region

using System.Collections;

#endregion

namespace CreditInfoGroup.BLL {
    /// <summary>
    /// Summary description for PBSData.
    /// </summary>
    public class PBSData {
        private readonly ArrayList rows;
        public PBSData() { rows = new ArrayList(); }
        public ArrayList Rows { get { return rows; } }
        public int RowCount { get { return rows.Count; } }
        public void AddRow(PBSRow row) { rows.Add(row); }

        public PBSRow GetRow(int index) {
            if (rows.Count > index) {
                return (PBSRow) rows[index];
            }
            return null;
        }
    }
}