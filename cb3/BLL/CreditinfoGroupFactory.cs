#region

using System;
using System.Data;
using CreditInfoGroup.DAL;
using UserAdmin.DAL.CIUsers;

#endregion

namespace CreditInfoGroup.BLL {
    /// <summary>
    /// Summary description for CRFactory.
    /// </summary>
    public class CreditinfoGroupFactory {
        private readonly CreditinfoGroupDALC dacl;
        private readonly CreditInfoUserDALC myCIUserDALC;

        public CreditinfoGroupFactory() {
            dacl = new CreditinfoGroupDALC();
            myCIUserDALC = new CreditInfoUserDALC();
        }

        public String GetPrincipalManager(string companyCreditinfoID, bool native) {
            int ciid;
            try {
                ciid = int.Parse(companyCreditinfoID);
            } catch (Exception) {
                return "";
            }

            DataSet mySet = dacl.GetPrincipalManager(ciid);

            try {
                if (native) {
                    return mySet.Tables[0].Rows[0]["FirstNameNative"] + " " + mySet.Tables[0].Rows[0]["SurNameNative"];
                }
                return mySet.Tables[0].Rows[0]["FirstNameEN"] + " " + mySet.Tables[0].Rows[0]["SurNameEN"];
            } catch (Exception) {
                return "";
            }
        }

        public bool IsPaymentBehaviourDataAvailable(int creditInfoID) { return dacl.IsPaymentBehaviourDataAvailable(creditInfoID); }
        public PBSData GetPBSAllCompanies() { return dacl.GetPBSAllCompanies(); }
        public PBSData GetPBSThisIndustry(string nace) { return dacl.GetPBSThisIndustry(nace); }
        public PBSData GetPBSCompany(int creditInfoID) { return dacl.GetPBSCompany(creditInfoID); }
        public DataSet GetCityListAsDataSet(bool nativeCult) { return myCIUserDALC.GetCityListAsDataSet(nativeCult); }
        public string GetPaymentIndex(int creditInfoID, bool EN) { return dacl.GetPaymentIndex(creditInfoID, EN); }
        public String GetCityName(int cityID, bool nativeCult) { return myCIUserDALC.GetCityName(cityID, nativeCult); }
    }
}