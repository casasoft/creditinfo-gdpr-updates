﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.BLL.Scoring
{
    public interface IScoringItem
    {
        int Id { get; set; }
        bool Delete { get; set; }
        int ItemIndex { get; set; }
        int MinTest { get; set; }
        int? MaxTest { get; set; }
    }
}
