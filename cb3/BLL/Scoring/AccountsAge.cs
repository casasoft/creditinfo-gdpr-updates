﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.BLL.Scoring
{
    public class AccountsAge : IScoringItem
    {
        public int MinTest { get; set; }
        public int? MaxTest { get; set; }
        public decimal? Value { get; set; }
        public int Id { get; set; }
        public bool Delete {get; set;}
        public int ItemIndex { get; set; }
    }
}
