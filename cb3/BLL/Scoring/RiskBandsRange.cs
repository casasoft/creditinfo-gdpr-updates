﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.BLL.Scoring
{
    public class RiskBandsRange : IScoringItem
    {
        public int Id { get; set; }
        public decimal MinTest { get; set; }
        public decimal? MaxTest { get; set; }
        public string Value { get; set; }
        public string RiskDescription { get; set; }
        public string Color { get; set; }
        public int ItemIndex { get; set; }

        public bool Delete { get; set; }

        #region IScoringItem Members


        int IScoringItem.MinTest
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int? IScoringItem.MaxTest
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
