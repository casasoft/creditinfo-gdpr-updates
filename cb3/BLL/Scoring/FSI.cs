﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Globalization;

namespace cb3.BLL.Scoring
{
    public class FSI : IScoringItem
    {
        public int Id { get; set; }
        public int MinTest { get; set; }
        public int? MaxTest { get; set; }
        public string Value { get; set; }
        public string RiskDescription { get; set; }
        public string Color { get; set; }
        public int ItemIndex { get; set; }
        public int DDDNumber { get; set; }
        public bool Delete { get; set; }
    }
}
