#region

using System.Web.Mail;

#endregion

namespace CreditInfoGroup.BLL {
    /// <summary>
    /// Summary description for HelpFunctions.
    /// </summary>
    public class HelpFunctions {
        public void SendMail(string Message, string Subject) {
            var myMail = new MailMessage
                         {
                             From = "error@lt.is",
                             To = "error@lt.is",
                             Subject = Subject,
                             Priority = MailPriority.High,
                             BodyFormat = MailFormat.Text,
                             Body = Message
                         };
            SmtpMail.SmtpServer = "hermes.lt.is";
            SmtpMail.Send(myMail);
        }
    }
}