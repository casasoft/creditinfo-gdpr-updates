<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/FoHead.ascx" %>
<%@ Page language="c#" Codebehind="FoUserProfile.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.FoUserProfile" %>
<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="new_user_controls/FoOptions.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Creditinfo</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<script language="JavaScript" src="popup.js"></script>
		<LINK href="css/FoCIGStyles.css" type="text/css" rel="stylesheet" />
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<script language="javascript" type="text/javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						FoSearchForm.btnFind.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					//document.FoSearchForm.txtName.focus();
				}
				
		</script>
	</HEAD>
	<body style="BACKGROUND-IMAGE: url(img/mainback.gif)" leftMargin="0" onload="SetFormFocus()"
		rightMargin="0" ms_positioning="GridLayout">
		<form id="_FoSearchForm" name="Creditinfo Malta" method="post" runat="server">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
								<td>
									<table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="#951e16" border="0">
										<tr>
											<td bgcolor="#951e16"><span style="WIDTH: 3px"></span></td>
											<td bgcolor="#951e16"><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
											<td align="right" bgcolor="#951e16"><uc1:language id="Language2" runat="server"></uc1:language></td>
										</tr>
									</table>
								</td>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="white" border="0">
							<tr>
								<td>
									<p>
										<table id="tbDefault" cellspacing="0" cellpadding="0" width="100%" align="center">
											<tr>
												<td>
													<table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
														<tr>
															<td>
																<uc1:userinfo id="UserInfo1" runat="server"></uc1:userinfo>
															</td>
															<td align="right">
																<uc1:fooptions id="FoOptions1" runat="server"></uc1:fooptions>
															</td>
														</tr>
														<!-- Content begins -->
														<tr>
															<td colspan="2">
																<table id="tblHeader" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
																	<tr>
																		<td class="pageheader">
																			<asp:label id="lblMyProfile" runat="server" cssclass="HeadMain">My profile</asp:label>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td colspan="2">
																<table class="fields" id="tbMain" cellspacing="0" cellpadding="0" width="100%">
																	<tr>
																		<td>
																			<asp:label id="SubscriberNameLabel" runat="server" Font-Bold="True">Subscriber!</asp:label>
																			<br/>
																			<asp:label id="SubscriberName" runat="server" EnableViewState="True"></asp:label>
																		</td>
																		<td>
																			<asp:label id="DepartmentLabel" runat="server" Font-Bold="True">Department!</asp:label>
																			<br/>
																			<asp:label id="Department" runat="server" EnableViewState="True"></asp:label>
																		</td>
																		<td style="WIDTH: 25%">
																			<asp:label id="NameLabel" runat="server" Font-Bold="True">Name!</asp:label>
																			<br/>
																			<asp:label id="Name" runat="server" EnableViewState="True"></asp:label>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3">
																			&nbsp;
																		</td>
																	</tr>
																	<tr>
																		<td style="WIDTH: 25%">
																			<asp:label id="DateCreatedLabel" runat="server" Font-Bold="True">Date Created!</asp:label>
																			<br/>
																			<asp:label id="DateCreated" runat="server" EnableViewState="True"></asp:label>
																		</td>
																		<td style="WIDTH: 25%">
																			<asp:label id="RegisteredByLabel" runat="server" Font-Bold="True">Registered by!</asp:label>
																			<br/>
																			<asp:label id="RegisteredBy" runat="server" EnableViewState="True"></asp:label>
																		</td>
																		<td>
																			<asp:label id="DateUpdatedLabel" runat="server" Font-Bold="True">Date Updated!</asp:label>
																			<br/>
																			<asp:label id="DateUpdated" runat="server" EnableViewState="True"></asp:label>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3" height="30">
																			&nbsp;
																		</td>
																	</tr>
																</table>
																<table class="fields" id="tbSettings1" cellspacing="0" cellpadding="0" width="100%">
																	<tr>
																		<th colspan="3">
																			<asp:label id="UserSettingsLabel" runat="server" Font-Bold="True" Font-Size="Small">User settings</asp:label>
																		</th>
																	</tr>
																	<tr>
																		<td colspan="3">
																			&nbsp;
																		</td>
																	</tr>
																	<tr>
																		<td style="WIDTH: 25%">
																			<asp:label id="lblUserNameLabel" runat="server" Font-Bold="True">Username!</asp:label>
																			<br/>
																			<asp:label id="lblUsername" runat="server" EnableViewState="True"></asp:label>
																		</td>
																		<td style="WIDTH: 23.12%">
																			<asp:label id="lblPasswordLabel" runat="server" Font-Bold="True">Password!</asp:label>
																			<br/>
																			<asp:textbox id="txtPassword" runat="server" textmode="Password"></asp:textbox>
																			<asp:comparevalidator id="cvRepeatedPassword0" runat="server" 
                                                                                ControlToCompare="txtRepeatedPassword" ControlToValidate="txtPassword"
																				ErrorMessage="Password does not match">*</asp:comparevalidator>
																		</td>
																		<td style="WIDTH: 25%">
																			<asp:label id="lblRepeatedPasswordLabel" runat="server" Font-Bold="True">Repeated password!</asp:label>
																			<br/>
																			<asp:textbox id="txtRepeatedPassword" runat="server" textmode="Password"></asp:textbox>
																			<asp:comparevalidator id="cvRepeatedPassword" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtRepeatedPassword"
																				ErrorMessage="Password does not match">*</asp:comparevalidator>
																		</td>
																	</tr>
																	<tr>
																		<td>&nbsp;</td>
																		<td style="WIDTH: 311px"></td>
																		<td></td>
																	</tr>
																	<tr>
																		<td>
																			<asp:label id="EmailLabel" runat="server" Font-Bold="True">Email!</asp:label>
																			<br/>
																			<asp:textbox id="txtEmail" runat="server"></asp:textbox>
																			<asp:RegularExpressionValidator id="regExpEmailValidator" runat="server" ErrorMessage="Not a valid email address"
																				ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail">*</asp:RegularExpressionValidator>
																		</td>
																		<td style="WIDTH: 311px">
																			<asp:label id="CreditWatchLimitLabel" runat="server" Font-Bold="True">Credit Watch limit!</asp:label>
																			<br/>
																			<asp:label id="CreditWatchLimit" runat="server" EnableViewState="True">1000</asp:label>
																		</td>
																		<td>
																			&nbsp;
																		</td>
																	</tr>
																	<tr>
																		<td height="23" colspan="3"></td>
																	</tr>
																</table>
																<table class="empty_table" id="Table5" cellspacing="0" width="100%">
																	<TR>
																		<td style="WIDTH: 740px" align="left">
																			<asp:label id="lblMessage" runat="server" visible="False" forecolor="Red">- registration not finished</asp:label>
																			<asp:validationsummary id="valSummary" runat="server"></asp:validationsummary>
																		</td>
																		<td align="right">
																			<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px">
																				<asp:button id="SaveButton" runat="server" cssclass="RegisterButton" text="Save"></asp:button>
																			</div>
																		</td>
																	</TR>
																</table>
															</td>
														</tr>
														<!-- Content ends -->
													</table>
												</td>
											</tr>
										</table>
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor="transparent" height="6"></td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
