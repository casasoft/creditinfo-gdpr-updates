#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditInfoGroup.BLL;
using DataProtection;
using Logging.BLL;
using cb3;

#endregion

namespace CreditInfoGroup.DAL {
    /// <summary>
    /// This class provides the database funtions for Company Reports
    /// </summary>
    public class CreditinfoGroupDALC {
        /// <summary>
        /// For logging purpose
        /// </summary>
        private const string className = "CreditinfoGroupDALC";

        public DataSet GetPrincipalManager(int companyCreditInfoID) {
            var ds = new DataSet();
            const string funcName = "GetPrincipalManager(int companyCreditInfoID)";

            string query =
                "select * from cpi_companyprincipals, np_individual where np_individual.Creditinfoid = cpi_companyprincipals.principalciid and companyciid = " +
                companyCreditInfoID;
            try {
                return ds = executeSelectStatement(query);
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return ds;
        }

        public bool IsPaymentBehaviourDataAvailable(int creditInfoID) {
            DataSet ds;
            const string funcName = "IsPaymentBehaviourDataAvailable(int creditInfoID)";

            //string query = "select top 6 * from pbs_sum_subj where regno_subject = '"+nationalID+"' and regno_participant = '*' order by res_year desc, res_quarter desc";
            string query = "select top 6 * from pbs_sum_subj where ciidSubject = " + creditInfoID +
                           " order by resYear desc, resQuarter desc";
            try {
                ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        return true;
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return false;
        }

        public PBSData GetPBSAllCompanies() {
            var data = new PBSData();
            DataSet ds;
            const string funcName = "GetPBSAllCompanies()";

            const string query = "select top 6 * from pbs_sum_ind where nace = '*'  order by resYear desc, resQuarter desc";
            try {
                ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                            var row = new PBSRow
                                      {
                                          Days = ds.Tables[0].Rows[i]["daysAvg"].ToString(),
                                          Count = ds.Tables[0].Rows[i]["noRecs"].ToString(),
                                          Std = ds.Tables[0].Rows[i]["daysStd"].ToString(),
                                          Quarter = ds.Tables[0].Rows[i]["resQuarter"].ToString(),
                                          Year = ds.Tables[0].Rows[i]["resYear"].ToString()
                                      };
                            data.AddRow(row);
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return data;
        }

        public PBSData GetPBSThisIndustry(string nace) {
            var data = new PBSData();
            DataSet ds;
            const string funcName = "GetPBSThisIndustry(string nace)";

            string query = "select top 6 * from pbs_sum_ind where nace = '" + nace +
                           "'  order by resYear desc, resQuarter desc";
            try {
                ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                            var row = new PBSRow
                                      {
                                          Days = ds.Tables[0].Rows[i]["daysAvg"].ToString(),
                                          Count = ds.Tables[0].Rows[i]["noRecs"].ToString(),
                                          Std = ds.Tables[0].Rows[i]["daysStd"].ToString(),
                                          Quarter = ds.Tables[0].Rows[i]["resQuarter"].ToString(),
                                          Year = ds.Tables[0].Rows[i]["resYear"].ToString()
                                      };
                            data.AddRow(row);
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return data;
        }

        public PBSData GetPBSCompany(int creditInfoID) {
            var data = new PBSData();
            DataSet ds;
            const string funcName = "GetPBSCompany(string nationalID)";

            //string query = "select top 6 * from pbs_sum_subj where regno_subject = '"+nationalID+"' and regno_participant = '*' order by res_year desc, res_quarter desc";
            string query = "select top 6 * from pbs_sum_subj where ciidSubject = " + creditInfoID +
                           " order by resYear desc, resQuarter desc";
            try {
                ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        for (int i = ds.Tables[0].Rows.Count - 1; i >= 0; i--) {
                            var row = new PBSRow
                                         {
                                             Days = ds.Tables[0].Rows[i]["daysAvg"].ToString(),
                                             Count = ds.Tables[0].Rows[i]["noRecs"].ToString(),
                                             Std = ds.Tables[0].Rows[i]["daysStd"].ToString(),
                                             Quarter = ds.Tables[0].Rows[i]["resQuarter"].ToString(),
                                             Year = ds.Tables[0].Rows[i]["resYear"].ToString()
                                         };
                            data.AddRow(row);
                        }
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return data;
        }

        public string GetPaymentIndex(int creditInfoID, bool EN) {
            const string funcName = "GetPaymentIndex(int creditInfoID, bool EN)";
            string query = "select top 1 paymentIndex, nameEN, nameNative, code, resYear, resQuarter " +
                           "from pbs_sum_subj pss, pbs_PaymentIndex pid where pss.PaymentIndex = pid.paymentIndexID " +
                           "and ciidSubject = " + creditInfoID + " order by resYear desc, resQuarter desc";
            try {
                var ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        string pIndex = ds.Tables[0].Rows[0]["code"] + " - ";
                        if (EN) {
                            if (ds.Tables[0].Rows[0]["NameEN"] != null &&
                                ds.Tables[0].Rows[0]["NameEN"].ToString().Trim() != "") {
                                pIndex += ds.Tables[0].Rows[0]["NameEN"].ToString();
                            } else {
                                pIndex += ds.Tables[0].Rows[0]["NameNative"].ToString();
                            }
                        } else {
                            if (ds.Tables[0].Rows[0]["NameNative"] != null &&
                                ds.Tables[0].Rows[0]["NameNative"].ToString().Trim() != "") {
                                pIndex += ds.Tables[0].Rows[0]["NameNative"].ToString();
                            } else {
                                pIndex += ds.Tables[0].Rows[0]["NameEN"].ToString();
                            }
                        }
                        return pIndex;
                    }
                }
            } catch (Exception err) {
                Logger.WriteToLog(className + " : " + funcName, err, true);
            }
            return null;
        }

        private static DataSet executeSelectStatement(string query) {
            

            var ds = new DataSet();
            var myAdapter = new OleDbDataAdapter();
            var myOleConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            try {
                myOleConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleConn);
                myAdapter.Fill(ds);
                return ds;
            } finally {
                myOleConn.Close();
            }
        }

    }
}