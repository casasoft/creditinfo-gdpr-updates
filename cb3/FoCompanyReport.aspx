<%@ Page language="c#" Codebehind="FoCompanyReport.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.FoCompanyReport" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Company report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="popup.js"></script>
		<LINK href="css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						FoCompanyReportForm.btnFind.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.FoCompanyReportForm.txtName.focus();
				}
				
		</SCRIPT>
	</HEAD>
	<body style="BACKGROUND-IMAGE: url(img/mainback.gif)" onload="SetFormFocus()" MS_POSITIONING="GridLayout">
		<form id="FoCompanyReportForm" title="Company report" name="Company report" method="post"
			runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
								<td>
									<table cellSpacing="0" cellPadding="0" width="760" align="center" bgColor="#951e16" border="0">
										<tr>
											<td bgColor="#951e16"><span style="WIDTH: 3px"></span></td>
											<td bgColor="#951e16"><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
											<td align="right" bgColor="#951e16"><uc1:language id="Language2" runat="server"></uc1:language></td>
										</tr>
									</table>
								</td>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="760" align="center" bgColor="white" border="0">
							<tr>
								<td>
									<p>
										<table id="tbDefault" cellSpacing="0" cellPadding="0" width="100%" align="center">
											<TR>
												<TD>
													<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
														<tr>
															<TD><uc1:userinfo id="UserInfo1" runat="server"></uc1:userinfo></TD>
														</tr>
													</table>
												</TD>
											</TR>
											<!-- Content begins -->
											<tr>
												<td>
													<TABLE id="tbDefault" style="WIDTH: 678px" borderColor="#0000ff" cellSpacing="0" cellPadding="0"
														width="678" align="center">
														<TR>
															<td style="WIDTH: 239px; HEIGHT: 17px"></td>
															<TD style="WIDTH: 224px; HEIGHT: 17px"></TD>
															<TD style="WIDTH: 224px; HEIGHT: 17px"></TD>
														</TR>
														<TR>
															<TD align="left" colSpan="8" Class="pageheader"><asp:label id="lblReport" runat="server" Width="100%" CssClass="HeadMain"> Report </asp:label></TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="center" colSpan="8"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="center"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="center"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left" colSpan="8"><asp:label id="lblFindReport" runat="server" CssClass="HeadLists">Find report</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
														</TR>
														<TR class="dark-row">
															<TD style="WIDTH: 491px; HEIGHT: 10px" align="center" colSpan="8"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 10px" align="center"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 10px" align="center"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 239px; HEIGHT: 13px" align="left"><asp:label id="lblNationalID" runat="server">National ID</asp:label></TD>
															<TD style="WIDTH: 224px; HEIGHT: 13px" align="left"><asp:label id="lblName" runat="server">Name</asp:label></TD>
															<TD style="WIDTH: 224px; HEIGHT: 13px" align="left">
																<asp:Label id="lblAddress" runat="server">Address</asp:Label></TD>
														</TR>
														<tr>
															<TD style="WIDTH: 239px; HEIGHT: 15px" align="left"><asp:textbox id="txtNationalID" runat="server" Width="112px"></asp:textbox></TD>
															<TD style="WIDTH: 382px; HEIGHT: 15px" align="left"><asp:textbox id="txtName" runat="server" Width="200px"></asp:textbox></TD>
															<TD style="WIDTH: 382px; HEIGHT: 15px" align="left" colSpan="3">
																<asp:TextBox id="txtAddress" runat="server" Width="192px"></asp:TextBox></TD>
															<TD style="WIDTH: 416px; HEIGHT: 15px" align="left"></TD>
														</tr>
														<TR>
															<TD style="WIDTH: 491px; HEIGHT: 10px" align="center" colSpan="8"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 10px" align="center"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 10px" align="center"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 445px; HEIGHT: 25px" align="left" colSpan="2">
																<table>
																	<tr>
																		<td>
																			<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnFind" runat="server" CssClass="RegisterButton" Text="Find"></asp:button></DIV>
																		</td>
																		<td>
																			<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnClear" runat="server" CssClass="RegisterButton" Text="Clear"></asp:button></DIV>
																		</td>
																	</tr>
																</table>
															</TD>
															<TD style="WIDTH: 602px; HEIGHT: 25px" align="left"></TD>
															<TD style="WIDTH: 602px; HEIGHT: 25px" align="left"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 491px; HEIGHT: 13px" align="left" colSpan="8"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 13px" align="left"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 13px" align="left"></TD>
														</TR>
														<TR id="trCompanyInfoRow" runat="server">
															<TD style="WIDTH: 445px" vAlign="top" align="left" colSpan="7">
																<table style="WIDTH: 408px; HEIGHT: 94px">
																	<TR>
																		<TD vAlign="top" style="WIDTH: 111px">
																			<asp:Label id="lblDispNationalID" runat="server" Font-Bold="True">National id</asp:Label></TD>
																		<TD vAlign="top">
																			<asp:label id="lblResultNationalID" runat="server">ResultNationalID</asp:label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 111px; HEIGHT: 15px" vAlign="top">
																			<asp:Label id="lblDispName" runat="server" Font-Bold="True">Name</asp:Label></TD>
																		<TD style="HEIGHT: 15px" vAlign="top">
																			<asp:label id="lblResultName" runat="server">ResultName</asp:label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 111px" vAlign="top">
																			<asp:Label id="lblDispAddress" runat="server" Font-Bold="True">Address</asp:Label></TD>
																		<TD vAlign="top">
																			<asp:label id="lblResultAddress" runat="server">ResultAddress</asp:label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" style="WIDTH: 111px">
																			<asp:Label id="lblDispCity" runat="server" Font-Bold="True">City</asp:Label></TD>
																		<TD vAlign="top">
																			<asp:label id="lblResultCity" runat="server">ResultCity</asp:label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" style="WIDTH: 111px"></TD>
																		<TD vAlign="top"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" style="WIDTH: 111px"><asp:label id="lblOperation" runat="server" Font-Bold="True">Operation</asp:label>&nbsp;</TD>
																		<TD vAlign="top">
																			<asp:label id="lblResultOperation" runat="server">ResultOperation</asp:label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" style="WIDTH: 111px"><asp:label id="lblNACE" runat="server" Font-Bold="True">Nace</asp:label>&nbsp;</TD>
																		<TD vAlign="top">
																			<asp:label id="lblResultNACE" runat="server">ResultNace</asp:label></TD>
																	</TR>
																</table>
															</TD>
														</TR>
														<TR id="trAdditionalInformation" runat="server">
															<TD vAlign="top" align="left" colSpan="7"><TABLE id="Table2" width="100%">
																	<TR>
																		<TD style="HEIGHT: 15px"></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="lblAdditionalInformations" runat="server" Font-Bold="True" Font-Size="Medium">Additional informations</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD id="trBasicRow" runat="server">
																			<asp:LinkButton id="lbtnBasicReport" runat="server">Basic report</asp:LinkButton></TD>
																	</TR>
																	<TR>
																		<TD id="trCompanyRow" runat="server">
																			<asp:LinkButton id="lbtnCompanyReport" runat="server">Company report</asp:LinkButton></TD>
																	</TR>
																	<TR>
																		<TD id="trCreditInfoRow" runat="server">
																			<asp:LinkButton id="lbtnCreditinfoReport" runat="server">Creditinfo report</asp:LinkButton></TD>
																	</TR>
																	<TR>
																		<TD id="trOrderRow" runat="server">
																			<asp:LinkButton id="lbtnOrderReport" runat="server">Order report</asp:LinkButton></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 15px"></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR ID="trOrderReport" runat="server">
															<TD vAlign="top" align="left" colSpan="7">
																<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
																	<TR>
																		<TD colSpan="2">
																		</TD>
																	</TR>
																	<TR>
																		<TD Class="pageheader" colSpan="2">
																			<asp:Label id="lblReportOrder" runat="server" Font-Bold="True">Order a credit report4</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px; HEIGHT: 15px"></TD>
																		<TD style="HEIGHT: 18px"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px">
																			<asp:Label id="lblOrderCIID" runat="server" Font-Bold="True">Company CIID</asp:Label></TD>
																		<TD>
																			<asp:Label id="lblOrderDispCompanyCIID" runat="server">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px">
																			<asp:Label id="lblOrderName" runat="server" Font-Bold="True">Company name</asp:Label></TD>
																		<TD>
																			<asp:Label id="lblOrderDispCompanyName" runat="server">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px"></TD>
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px">
																			<asp:Label id="lblOrderSubscriberCIID" runat="server" Font-Bold="True">Subscriber CIID</asp:Label></TD>
																		<TD>
																			<asp:Label id="lblOrderDispSubscriberCIID" runat="server">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px">
																			<asp:Label id="lblOrderUserName" runat="server" Font-Bold="True">User name</asp:Label></TD>
																		<TD>
																			<asp:Label id="lblOrderDispUserName" runat="server">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px"></TD>
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px">
																			<asp:Label id="lblOrderDeliveryType" runat="server" Font-Bold="True">Delivery type</asp:Label></TD>
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px">
																			<asp:RadioButtonList id="rbtnlDeliveryType" runat="server"></asp:RadioButtonList></TD>
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px"></TD>
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 162px">
																			<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px">
																				<asp:button id="btnOrderReport" runat="server" CssClass="RegisterButton" Text="Order Report"></asp:button></DIV>
																		</TD>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR id="trOrderReceipe" runat="server">
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left" colSpan="8">
																<TABLE id="Table3" style="WIDTH: 648px; HEIGHT: 94px">
																	<TR>
																		<TD style="WIDTH: 120px" vAlign="top"><asp:label id="lblRecNationalIDLabel" runat="server" Font-Bold="True">National ID</asp:label></TD>
																		<TD vAlign="top"><asp:label id="lblRecNationalID" runat="server">Label</asp:label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 120px; HEIGHT: 15px" vAlign="top"><asp:label id="lblRecCreditInfoIDLabel" runat="server" Font-Bold="True">CreditInfo ID</asp:label></TD>
																		<TD style="HEIGHT: 15px" vAlign="top"><asp:label id="lblRecCreditInfoID" runat="server">Label</asp:label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 120px; HEIGHT: 7px" vAlign="top"><asp:label id="lblRecNameLabel" runat="server" Font-Bold="True">Name</asp:label></TD>
																		<TD style="HEIGHT: 7px" vAlign="top"><asp:label id="lblRecName" runat="server">Label</asp:label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 120px" vAlign="top"></TD>
																		<TD vAlign="top"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 120px" vAlign="top" colSpan="3"><asp:label id="lblOrderReceived" runat="server" Font-Bold="True">Order received</asp:label></TD>
																		<TD vAlign="top"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left" colSpan="8"><asp:label id="lblNoCompanyFound" runat="server" ForeColor="Red">No company found</asp:label></TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
															<TD style="WIDTH: 491px; HEIGHT: 15px" align="left"></TD>
														</TR>
														<TR id="trSearchGridRow" runat="server">
															<TD style="WIDTH: 621px" colSpan="4">
																<TABLE id="Table1" style="WIDTH: 408px">
																	<TR>
																		<TD><asp:datagrid id="dtgrCompanys" runat="server" Width="600px" ForeColor="Black" AutoGenerateColumns="False"
																				BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="4"
																				GridLines="Vertical" Font-Size="Smaller">
																				<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
																				<AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
																				<ItemStyle ForeColor="Black" BackColor="White"></ItemStyle>
																				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#895A4F"></HeaderStyle>
																				<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
																				<Columns>
																					<asp:ButtonColumn Text="Select" HeaderText="Select" CommandName="Select"></asp:ButtonColumn>
																					<asp:BoundColumn DataField="Number" SortExpression="Number" HeaderText="National ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="NameNative" SortExpression="NameNative" HeaderText="Name"></asp:BoundColumn>
																					<asp:BoundColumn DataField="NameEN" SortExpression="NameEN" HeaderText="Name (EN)"></asp:BoundColumn>
																					<asp:BoundColumn DataField="StreetNative" SortExpression="StreetNative" HeaderText="Address"></asp:BoundColumn>
																					<asp:BoundColumn DataField="StreetEN" SortExpression="StreetEN" HeaderText="Address (EN)"></asp:BoundColumn>
																					<asp:BoundColumn DataField="PostalCode" HeaderText="Post code"></asp:BoundColumn>
																					<asp:BoundColumn DataField="CityNative" SortExpression="CityNative" HeaderText="City"></asp:BoundColumn>
																					<asp:BoundColumn DataField="CityEN" SortExpression="CityEN" HeaderText="City (EN)"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD style="WIDTH: 602px"></TD>
															<TD style="WIDTH: 602px"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 239px; HEIGHT: 25px" align="left"></TD>
															<TD style="WIDTH: 224px; HEIGHT: 25px" align="left"></TD>
															<TD style="WIDTH: 224px; HEIGHT: 25px" align="left"></TD>
															<TD style="HEIGHT: 25px" align="left"></TD>
														</TR>
														<tr>
															<td style="WIDTH: 491px; HEIGHT: 15px" align="center" bgColor="#951e16" colSpan="8"></td>
															<TD style="HEIGHT: 15px" align="center" bgColor="#951e16"></TD>
															<TD style="HEIGHT: 15px" align="center" bgColor="#951e16"></TD>
															<TD style="HEIGHT: 15px" align="center" bgColor="#951e16"></TD>
															<TD style="HEIGHT: 15px" align="center" bgColor="#951e16"></TD>
														</tr>
														<TR class="dark-row">
															<TD style="WIDTH: 491px; HEIGHT: 10px" align="center" colSpan="8"></TD>
															<TD style="HEIGHT: 10px" align="center"></TD>
															<TD style="HEIGHT: 10px" align="center"></TD>
															<TD style="HEIGHT: 10px" align="center"></TD>
															<TD style="HEIGHT: 10px" align="center"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 239px; HEIGHT: 10px" align="left"></TD>
															<TD style="WIDTH: 224px; HEIGHT: 15px" align="center"></TD>
															<TD style="WIDTH: 224px; HEIGHT: 15px" align="center"></TD>
															<TD style="HEIGHT: 15px" align="center"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 491px" align="center" colSpan="8"></TD>
															<TD align="center"></TD>
															<TD align="center"></TD>
															<TD align="center"></TD>
															<TD align="center"></TD>
														</TR>
													</TABLE>
												</td>
											</tr>
											<!-- Content ends --></table>
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgColor="transparent" height="6"></td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
