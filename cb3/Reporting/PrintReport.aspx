﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintReport.aspx.cs" Inherits="cb3.Reporting.PrintReport" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="~/UserAdmin/user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Report</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="pragma" content="no-cache">
    <link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <script language=JavaScript src="../DatePicker.js"></script>
    
   
</head>
    

<body ms_positioning="GridLayout">
<form id="Report" name="Report"  method="post" runat="server">
<table height="600" width="997" align="center" border="0">
<tr>
    <td colspan="4"><uc1:head id="Head1" runat="server"></uc1:head></td>
</tr>
<tr valign="top">
<td width="1"></td>
<td>
    <table height="100%" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <uc1:language id="Language1" runat="server"></uc1:language>
            </td>
            <td></td>
            <td align="right">
                <ucl:options id="Options1" runat="server"></ucl:options>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
        </tr>
        <tr>
            <td height="10"></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
            </td>
            <td align="right">
                <ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
            </td>
        </tr>
        <tr>
            <td height="10"></td>
        </tr>
        <tr valign="top">
            <td valign="top" align="left" width="150">
                <table width="98%">
                    <tr>
                        <td><uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar></td>
                    </tr>
                </table>
            </td>
            <td id="blas" colspan="2" runat="server">
                <!-- Main Body -->
                <table width="100%">
                    <tr>
                        <td>
                            <table class="grid_table" cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>
                                        <asp:label id="lblPageHeader" runat="server"> PageHeader</asp:label></th></tr>
                                <tr>
                                    <td>
                                        <table class="fields" id="tbReportSelect" cellspacing="0" cellpadding="0">
                                            
                                            <tr>
                                                <td><asp:label id="lblSelectReport" runat="server">Select Report</asp:label> </td>
                                                <td><asp:dropdownlist id="ddlReports" AutoPostBack="True"  runat="server" OnSelectedIndexChanged="ddlReports_SelectedIndexChanged" ></asp:dropdownlist></td>
                                            </tr>
                                            
                                            <tr>
                                                <td height="10"></td>
                                            </tr>
                                           
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <table class="grid_table" id="reportParametersTable" cellspacing="0" cellpadding="0" runat="server">
                                            <tr>
                                                <th>
                                                    <asp:label id="lblGridHeader" runat="server">Report Parameters</asp:label></th></tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="ReportParameters" cellspacing="0" cellpadding="0">
                                            
                                                        <tr>
                                                            <%=ParametersHTML%>
                                                        </tr>
                                                        <tr>
                                                            <td height="10"></td>
                                                        </tr>
                                                        <input type="hidden" value="" name="ReportParametersList" id="ReportParametersList" runat="server" /> 
                                                        <tr>
                                                            <td valign="bottom"><asp:button id="Print" runat="server" cssclass="search_button" text="Print"></asp:button></td>
                                                        </tr>
                                           
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr id="outerGridTableInfo" runat="server">
                        <td align="right">
                            <img id="ajax-loader" src="<%=Application.Get("AppPath")%>/img/ajax-loader.gif" alt="" border="0" style="margin: 0 auto; display: block; height: 118px;">

                            <table id="report-view" class="display" style="width:100%;">
            
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr>
                        <td>
                            <table class="empty_table" cellspacing="0">
                                <tr>
                                    <td align="left"><asp:label id="lblErrorMessage" runat="server" cssclass="error_text" width="100%" visible="False">Error</asp:label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Main Body --></td>
        </tr>
        <tr>
            <td height="20"></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#000000" colspan="3" height="1"></td>
        </tr>
    </table>
</td>
<td width="2"></td>
</tr>
<tr>
    <td align="center" colspan="4"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
</tr>
</table>
</form>
    <div style="width: 800px; overflow-x: auto"> </div>
    
    
    

</body>

    
<script>

    $(document).ready(function () {
        $("#ajax-loader").toggle();


        $('#Print').click(function (event) {
            event.preventDefault();
            var url = "/Reporting/ReportDataJSON.aspx?ddlReports=" + $('#ddlReports').val();
            
            var reportview = $("#report-view");

            $("#ajax-loader").toggle('slow');
            $('#Print').prop("disabled",true);


            if ($.fn.DataTable.isDataTable("#report-view")) {
                reportview.DataTable().clear().destroy();
                reportview.empty();
            }

            var objects = [];
            var reportElements = document.getElementsByClassName('report-param');
            for (var i = 0; i < reportElements.length; ++i) {
                var paramObj = {
                    id: reportElements[i].id,
                    value: reportElements[i].value
                };
                objects.push(paramObj);
            }
            
 
            $.ajax(url + '&getColumns=true', {
                data: {reportObjects : JSON.stringify(objects)},
                success: function (columns) {

                    $("#ajax-loader").toggle('slow');
                    $('#Print').prop("disabled",false);

                    if (columns !== '') {

                       
            
 
                        reportview.DataTable({
                            "processing": true,
                            "serverSide": true,
                            "columns": columns,
                            "searching": false,
                            "order": [],
                            fixedHeader: {
                                header: true,
                                footer: false
                            },
                            "lengthMenu": [ [10, 25, 50, 100, 500, 1000, 10000, 0], [10, 25, 50, 100, 500, 1000, 10000, "ALL"] ],
                            dom: 'lBfrtip',
                            buttons: [
                                'colvis', 'copy', 'csv', 'excel', 'pdf', 'print'
                            ],
                            "ajax": {
                                "url": url+'&getColumns=false',
                                "type": "POST",
                                "data": {reportObjects : JSON.stringify(objects)}
                            }
                            
                        });
                        reportview.wrap('<div style="width: 800px; overflow-x: auto"></div>');

                    }
                },
                error: function (e) {
                    alert("An error occured while getting the report fields" + e);
                    $("#ajax-loader").toggle('slow');
                    $('#Print').prop("disabled",false);
                }
            });



        });
        
            
    });

</script>
</html>
