﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Reporting
{
    public class SelectModel
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public string Description { get; set; }
    }
}