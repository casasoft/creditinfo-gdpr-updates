﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Reporting
{
    public class QueryModel
    {
        public string Description { get; set; }

        public string UniqueName { get; set; }

        public string Body { get; set; }

        public string Declarations { get; set; }

        public List<SelectModel> Selects { get; set; }

        public List<OrderModel> Orders { get; set; }

        public List<ParameterModel> Parameters { get; set; }
    }
}