﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Logging.BLL;
using Newtonsoft.Json;
using NHibernate.Dialect;

namespace cb3.Reporting
{
    public class ReportingFactory
    {
        private ReportingService _reportingService { get; set;  }

        public List<string> Errors { get; set; }

        private DataSet _reportset { get; set; }
        

        public ReportingFactory()
        {
            _reportingService = new ReportingService();
            Errors = new List<string>();

            Errors = _reportingService.Errors; 
            LogErrors(new Exception());
        }
        

        public DataTable GetReportList()
        {
            var queryModels =  _reportingService.GetQueryModels();
            DataTable dt = new DataTable();

            if(queryModels != null)
            {

                var anyDuplicate = queryModels.GroupBy(x => x.UniqueName).Any(g => g.Count() > 1);
                if (anyDuplicate)
                {
                    Errors.Add("Duplicate Report ID exist");
                    LogErrors(new Exception());
                    return null;
                }


                if (queryModels != null && queryModels.Any())
                {
                    dt.Columns.Add(new DataColumn("id", typeof(String)));
                    dt.Columns.Add(new DataColumn("desc", typeof(String)));

                    foreach (var queryModel in queryModels)
                    {
                        DataRow dr = dt.NewRow();
                        dr["id"] = queryModel.UniqueName;
                        dr["desc"] = queryModel.Description;
                        dt.Rows.Add(dr);
                    }
                }
            }
            Errors = _reportingService.Errors; // add errors if any occurs in the service 

            LogErrors(new Exception());
            return dt;
            
        }



        public string GetReportParametersHTML(string uniqueName)
        {
            var queryModel = _reportingService.GetQueryModel(uniqueName);
            var parameterHTML = "";

            if(queryModel?.Parameters != null && queryModel.Parameters.Any())
            {
                int count = 0;
                foreach (var parameter in queryModel.Parameters)
                {
                    parameterHTML += String.Format("<td><asp:label id=\"{0}\" runat=\"server\">{0}</asp:label><br><input ", parameter.Description);

                    if(parameter.Type?.ToLower() == "date" )
                    {
                        parameterHTML += String.Format(" onclick=\"PopupPicker('{0}', 250, 250);\" ", parameter.Key);
                    }

                    parameterHTML += String.Format(" id=\"{0}\" class=\"report-param\" runat=\"server\"></input></td>", parameter.Key);

                    
                    if (count == 3)
                    {
                        parameterHTML += "<br>";
                    }
                    count++;
                }

            }

            return parameterHTML;
            
        }



        public string GetReportDataJson(ReportParamModel reportParam)
        {
            var json = "";
            if (!string.IsNullOrEmpty(reportParam.ReportID))
            {
                var queryModel = _reportingService.GetQueryModel(reportParam.ReportID);
                if (queryModel != null)
                {
                    // if user order was passed
                    var userCustomOrder = getOrderModelOfAnyColumn(reportParam);
                    if(userCustomOrder != null && userCustomOrder.Any())
                    {
                        queryModel.Orders = userCustomOrder;
                    }

                    _reportset = _reportingService.ExecuteReportSet(queryModel,  reportParam.Start, reportParam.Length, reportParam.ReportParamObjects);

                    if(_reportset != null && _reportset.Tables.Count > 0)
                    {
                        
                        var recordCount = getTotalReportRecords(reportParam.ReportID, reportParam.ReportParamObjects);

                        json = JsonConvert.SerializeObject(
                            new
                            {
                                data = _reportset.Tables[0],
                                recordsTotal = recordCount,
                                recordsFiltered = recordCount
                            },
                            Formatting.None);

                    }
                }

                Errors = _reportingService.Errors; // add errors if any occurs in the service 
                LogErrors(new Exception());
            }
            return json;

        }


        private List<OrderModel> getCustomOrderModel(ReportParamModel reportParam)
        {
            List<OrderModel> orders = new List<OrderModel>(); 

            if(!string.IsNullOrEmpty(reportParam.OrderDir))
            {
                // first check if the report query uses selects in the report file
                var queryModel = _reportingService.GetQueryModel(reportParam.ReportID);

                // if order data exist
                if(queryModel.Orders != null && queryModel.Orders.Any())
                {
                    var reportset = _reportingService.ExecuteReportSet(queryModel, 0, 1, reportParam.ReportParamObjects); // generate a report set just to get the columns
                    int i = 0;
                    foreach (DataColumn item in reportset.Tables[0].Columns)
                    {
                        if (i == reportParam.OrderColumn)
                        {
                            // get the order record in the order list using the column name
                            var customOrder = queryModel.Orders.FirstOrDefault(x => x.Key.ToUpper() == item.ColumnName.ToUpper());
                            if(customOrder != null)
                            {
                                orders.Add(new OrderModel()
                                {
                                    Key = customOrder.Key,
                                    Value = customOrder.Value,
                                    Type = reportParam.OrderDir.ToUpper(),
                                    Description = customOrder.Description
                                });

                            }
                            

                            break;
                        }
                        i++;
                    }
                    


                }



            }

            return orders;

        }



        private List<OrderModel> getOrderModelOfAnyColumn(ReportParamModel reportParam)
        {
            // since we are using a new kind of processed query method, this new processed query method can allow us to order any column

            List<OrderModel> orders = new List<OrderModel>();

            if (!string.IsNullOrEmpty(reportParam.OrderDir))
            {
                var queryModel = _reportingService.GetQueryModel(reportParam.ReportID);
                
                var reportset = _reportingService.ExecuteReportSet(queryModel, 0, 1, reportParam.ReportParamObjects); // generate a report set just to get the columns
                int i = 0;
                foreach (DataColumn item in reportset.Tables[0].Columns)
                {
                    if (i == reportParam.OrderColumn)
                    {

                        orders.Add(new OrderModel()
                        {
                            Key = item.ColumnName.ToUpper(),
                            Value = item.ColumnName.ToUpper(),
                            Type = reportParam.OrderDir.ToUpper()
                        });
                        
                        break;
                    }
                    i++;
                }
                

            }

            return orders;

        }



        public string GetSelectsJson(string uniqueName, List<ReportParamObjects> paramObjects=null)
        {
            var queryModel = _reportingService.GetQueryModel(uniqueName);
            var json = "";

            if (queryModel != null)
            {
                var jsonObject = new List<object>();
                var selects = queryModel.Selects;
                if (selects != null && selects.Any())
                {
                    foreach (var item in selects)
                    {
                        jsonObject.Add(new { title = item.Description, data = item.Key });
                    }
                }
                //else if(_reportset != null) // if no selects were defined in the query model, then get the selects from the data set **deprecated**
                //{
                //    foreach (DataColumn item in _reportset.Tables[0].Columns)
                //    {
                //        jsonObject.Add(new { title = item.ColumnName, data = item.ColumnName });
                //    }

                //}
                else
                {
                    var reportset = _reportingService.ExecuteReportSet(queryModel, 0, 1, paramObjects); // generate a report set just to get the column

                    if (reportset.Tables.Count > 0 && reportset.Tables[0].Columns != null)
                    {
                        foreach (DataColumn item in reportset.Tables[0].Columns)
                        {
                            jsonObject.Add(new { title = item.ColumnName, data = item.ColumnName });
                        }
                    }

                }

                json = JsonConvert.SerializeObject(jsonObject, Formatting.None);
            }

            Errors = _reportingService.Errors; // add errors if any occurs in the service 
            LogErrors(new Exception());
            return json;

        }



        private string getTotalReportRecords(string uniqueName, List<ReportParamObjects> paramObjects = null)
        {
            long recordCount = 0;
            if (!string.IsNullOrEmpty(uniqueName))
            {
                var queryModel = _reportingService.GetQueryModel(uniqueName);
                if (queryModel != null)
                {
                    recordCount = _reportingService.GetReportCount(queryModel, paramObjects);
                }

            }

            return Convert.ToString(recordCount);
        }



        private void LogErrors(Exception ex = null)
        {
            if(Errors != null && Errors.Any())
            {
                foreach(var error in Errors)
                {
                    Logger.WriteToLog("Error on Reporting. "+error, ex, true);
                }

            }
            
        }

    }
}