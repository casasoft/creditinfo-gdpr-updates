﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHibernate.Type;

namespace cb3.Reporting
{
    public partial class ReportDataJSON : System.Web.UI.Page
    {


        private ReportingFactory _reportingFactory;
        private string jsonResponse;
        private ReportParamModel reportParam;


        protected void Page_Load(object sender, EventArgs e)
        {

            _reportingFactory = new ReportingFactory();

            List<ReportParamObjects> reportParamObjectList = null;
            if(Request.Params["reportObjects"] != null && Request.Params["reportObjects"] != "[]")
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();
                reportParamObjectList = ser.Deserialize<List<ReportParamObjects>>(Request.Params["reportObjects"]);
            }
            
            
            


            if (Request.QueryString["getColumns"] == "true")
            {

                this.jsonResponse = _reportingFactory.GetSelectsJson(Request.QueryString["ddlReports"], reportParamObjectList);
            }
            else
            {
                var start = (Request.Form["start"] == "NaN") ? "0" : Request.Form["start"];
                
                reportParam = new ReportParamModel(){
                    ReportID = Request.QueryString["ddlReports"],
                    Length = Convert.ToInt64(Request.Form["length"]),
                    Start = Convert.ToInt64(start),
                    OrderColumn = Convert.ToInt32(Request.Form["order[0][column]"]),
                    OrderDir = Request.Form["order[0][dir]"],
                    ReportParamObjects = reportParamObjectList

                };
                

                this.jsonResponse = _reportingFactory.GetReportDataJson(reportParam); 

                
            }



            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write(jsonResponse);
            Response.End();

        }
    }
}