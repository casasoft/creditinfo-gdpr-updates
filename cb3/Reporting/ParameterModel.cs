﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Reporting
{
    public class ParameterModel
    {
        public string Key { get; set; }

        public string Label { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }

        public string Type { get; set; }


    }
}