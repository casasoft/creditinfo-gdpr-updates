﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Reporting
{
    public class OrderModel
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }
    }

    public struct OrderType
    {
        public const string ASCENDING = "ASC";
        public const string DESCENTDING = "DESC";
    }
}