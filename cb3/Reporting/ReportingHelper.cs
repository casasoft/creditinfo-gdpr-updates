﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using Newtonsoft.Json;

namespace cb3.Reporting
{
    public class ReportingHelper
    {

        public static List<QueryModel> LoadQueryModelsFromJSON()
        {
            //write a method to validate report unique name are not duplicated
            var listOfQueryModel = new List<QueryModel>();
            try
            {

                var queryFiles = loadQueryModelFilePaths();
                if (queryFiles != null)
                {
                    foreach (var file in queryFiles)
                    {
                        using (StreamReader r = new StreamReader(file.FullName))
                        {
                            string json = r.ReadToEnd();
                            try
                            {
                                var queryModelJson = JsonConvert.DeserializeObject<QueryModel>(json);
                                listOfQueryModel.Add(queryModelJson);
                            }
                            catch(Exception ex)
                            {
                                throw new Exception("An error occured parsing the report file: " + file.Name + " "+ ex.Message);
                            }

                        }

                    }

                }
                return listOfQueryModel;

            }
            catch(Exception ex)
            {
                throw new Exception("An error occured getting the report files " + ex.Message);
            }
        }



        private static FileInfo[] loadQueryModelFilePaths()
        {
            var queryDirectory = HttpContext.Current.Server.MapPath("/Reporting/queries/") ; // this should be editable in a setting
            DirectoryInfo di = new DirectoryInfo(queryDirectory);
            FileInfo[] files = di.GetFiles("*.txt");
            return files;
        }


        


    }
}