﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using CreditWatch.BLL;
using Microsoft.Office.Interop.Owc11;

namespace cb3.Reporting
{
    public class ReportingService
    {
        private List<QueryModel> _queryModels { get; set; }
        public List<string> Errors { get; set; }
        

        public ReportingService()
        {
            Errors = new List<string>();

            try
            {
                _queryModels = ReportingHelper.LoadQueryModelsFromJSON();
            }
            catch(Exception exc)
            {
                Errors.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + exc.Message);
            }

        }


        public List<QueryModel> GetQueryModels()
        {
            return _queryModels;

        }
        

        public QueryModel GetQueryModel(string uniqueName)
        {
            QueryModel queryModelResult = null;
            foreach (var queryModel in _queryModels)
            {
                if(uniqueName == queryModel.UniqueName)
                {
                    queryModelResult = queryModel;
                    break;
                }
                
            }
            return queryModelResult;

        }
        

        public DataSet ExecuteReportSet(QueryModel queryModel, long start, long lenght, List<ReportParamObjects> parameters = null)
        {
            var mySet = new DataSet();

            var processedQueryString = processQuery(queryModel, start, lenght, parameters);


            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    var myAdapter = new OleDbDataAdapter();
                    myOleDbConn.Open();
                    myAdapter.SelectCommand = new OleDbCommand(processedQueryString, myOleDbConn);
                    myAdapter.Fill(mySet);
                    if(lenght > 0)
                        mySet.Tables[0].Columns.Remove("RowNumber");
                }
                catch (Exception exc)
                {
                    Errors.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + exc.Message+ " query:" + processedQueryString);
                }

                return mySet;
            }

        }



        public long GetReportCount(QueryModel queryModel, List<ReportParamObjects> parameters = null)
        {
            var processedQueryString = processQuery(queryModel, 0, -1, parameters);
            long total = 0;

            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                try
                {
                    myOleDbConn.Open();
                    var selectCommand = new OleDbCommand(processedQueryString, myOleDbConn);

                     total = (Int64)selectCommand.ExecuteScalar();

                }
                catch (Exception exc)
                {
                    Errors.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + exc.Message + " query:" + processedQueryString);
                }

                return total;
            }

        }




        private string processQuery(QueryModel queryModel, long start, long lenght, List<ReportParamObjects> parameters = null)
        {
            var body = queryModel.Body.ToUpper();
            var returnString = "";

            if (!string.IsNullOrEmpty(body))
            {
                try
                {
                    var ordersString = processOrders(queryModel.Orders);
                    var selectsString = processSelects(queryModel.Selects);
                    returnString = body.Replace("@@SELECTS@@", selectsString);

                    // add limits on the record
                    if (lenght > 0)
                    {
                       if (ordersString.Trim() == "") ordersString = "ORDER BY (SELECT NULL)";
                       start = start + 1;
                        returnString = "WITH tableLimit AS ( " + returnString + " ),  "; //first cte
                        returnString = string.Format(returnString +
                            " tableLimit2 AS ( SELECT ROW_NUMBER() OVER(" + ordersString + ") as 'RowNumber' , * FROM tableLimit  ) SELECT * FROM tableLimit2 WHERE RowNumber BETWEEN {0} AND {1} ",
                            start, lenght + start - 1);

                        //returnString = regex.Replace(returnString, " SELECT ROW_NUMBER() OVER (" + ordersString + " ) AS 'RowNumber', ", 1).Replace("@@ORDERS@@", " ");
                       // returnString = string.Format("WITH tableLimit AS ( " + returnString + " )  SELECT * FROM tableLimit WHERE RowNumber BETWEEN {0} AND {1} ", start, lenght + start - 1);

                    }
                    else if(lenght == -1) // used to get record count
                    {
                        returnString = "WITH CountRecord AS ( " + returnString + " ),  "; //first cte
                        returnString = string.Format(returnString +
                                                     " CountRecord2 AS ( SELECT ROW_NUMBER() OVER( ORDER BY (SELECT NULL) ) as 'RowNumber' FROM CountRecord  ) SELECT TOP 1 RowNumber FROM CountRecord2 order by RowNumber desc ");

                        //returnString = regex.Replace(returnString, " SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL) ) AS 'RowNumber', ", 1).Replace("@@ORDERS@@", " ");
                        //returnString = string.Format("WITH CountRecord AS ( " + returnString + " )  SELECT TOP 1 RowNumber FROM CountRecord order by RowNumber desc ");

                    }

                    if(!String.IsNullOrEmpty(queryModel.Declarations))
                    {
                        returnString = processDeclarations(queryModel, parameters) + returnString;
                    }



                    returnString = Regex.Replace(returnString, @"\r\n?|\n", " "); // replace line breaks on body

                }
                catch (Exception exc)
                {
                    Errors.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + exc.Message);

                }

            }

            // if we go this far no orders or an error occured
            return returnString;
        }
        

        private string processOrders(List<OrderModel> orderModels)
        {
            var orders = orderModels;
            var orderResult = new StringBuilder();
            var returnString = "";

            if (orders != null && orders.Any())
            {
                try
                {
                    orderResult.Append("ORDER BY ");
                    foreach (var order in orders)
                    {
                        var orderType = order.Type;
                        orderResult.Append("["+order.Value+"]").Append(" ").Append(orderType).Append(" ").Append(",");
                    }
                    returnString = orderResult.ToString().Trim(',');
                    return returnString;

                }
                catch(Exception exc)
                {
                    Errors.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + exc.Message);

                }

            }

            // if we go this far no orders or an error occured
            return returnString;
        }


        private string processSelects(List<SelectModel> selectModels)
        {
            var selects = selectModels;
            var selectResult = new StringBuilder();
            var returnString = " ";

            if (selects != null && selects.Any())
            {
                try
                {
                    selectResult.Append(" ");
                    foreach (var select in selects)
                    {

                        selectResult.Append(" ").Append(select.Value).Append(" ").Append(",");

                    }
                    returnString = selectResult.ToString().Trim(',');
                    return returnString;

                }
                catch (Exception exc)
                {
                    Errors.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + exc.Message);

                }

            }

            // if we go this far no orders or an error occured
            return returnString;

        }



        private string processDeclarations(QueryModel queryModel, List<ReportParamObjects> parametersObjects)
        {
            var declarationSetString = "";
            if (queryModel?.Parameters != null && queryModel.Parameters.Any() && !String.IsNullOrEmpty(queryModel.Declarations))
            {
                try
                {

                    foreach (var parameterModel in queryModel.Parameters)
                    {
                        if (queryModel.Declarations.Contains(parameterModel.Label))
                        {
                            var parameterValue = parametersObjects.FirstOrDefault(x => x.Id == parameterModel.Key).Value;
                            parameterValue = (parameterModel.Type == "date") ? DateTime.Parse(parameterValue).ToString("yyyy-MM-dd HH:mm:ss") : parameterValue;
                            declarationSetString += String.Format(" SET {0} = '{1}'; ", parameterModel.Label, parameterValue);
                        }
                    }
                    return queryModel.Declarations + declarationSetString;
                }
                catch (Exception exc)
                {
                    Errors.Add(System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + exc.Message);

                }
            }
            else if(!String.IsNullOrEmpty(queryModel?.Declarations))
            {
                return queryModel.Declarations;
            }

            return "";
            
        }



    }
}