﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Reporting
{
    public class ReportParamModel
    {
        public string ReportID { get; set; }

        public long Start { get; set; }

        public long Length { get; set; }

        public int OrderColumn { get; set; }

        public string OrderDir { get; set; }

        public List<ReportParamObjects> ReportParamObjects { get; set; }
    }


    public class ReportParamObjects
    {
        public string Id { get; set; }

        public string Value { get; set; }
    }
}