﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cb3.Reporting
{
    public partial class PrintReport : System.Web.UI.Page
    {

        public static ResourceManager rm;
        public static CultureInfo ci;
        private bool EN = false;
        private ReportingFactory _reportingFactory;
        public string ParametersHTML;


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ID = "20003";


            String culture = Thread.CurrentThread.CurrentCulture.Name;
            if (culture.Equals("en-US"))
                EN = true;
            

            _reportingFactory = new ReportingFactory();

            //rm = CIResource.CurrentManager;
            // ci = Thread.CurrentThread.CurrentCulture;
            // to work on localization

            this.lblPageHeader.Text = "Reports";

            if (!Page.IsPostBack)
            {

                SetReportsDropDownLists();

                reportParametersTable.Visible = false;
            }


        }



        private void SetReportsDropDownLists()
        {
             var reportlist = _reportingFactory.GetReportList();
            if(reportlist != null)
            {

                ddlReports.DataSource = reportlist;
                ddlReports.DataValueField = "id";
                ddlReports.DataTextField = "desc";
                ddlReports.DataBind();

                ListItem listItem = new ListItem();
                listItem.Value = "";
                listItem.Text = "";
                listItem.Selected = true;
                ddlReports.Items.Insert(0, listItem);
            }
        }
        



        protected void ddlReports_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlReports.SelectedValue != "")
            {
                this.reportParametersTable.Visible = true;
                this.ParametersHTML = _reportingFactory.GetReportParametersHTML(ddlReports.SelectedValue);
            }
            else
            {
                this.reportParametersTable.Visible = false;
            }

        }

    }
}