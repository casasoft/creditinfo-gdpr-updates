﻿{

"Description" : "Data protection usage",
UniqueName    : "dpprousg",

  "body": " select * from (

select x.SubscriberName, x.UserName, x.Created, x.IsBillable, x.typeEN as ReportName, nn.Number, cc.NameNative as Name, 1 as IsCompany from (
select u.created, u.IsBillable, u.query as CreditInfoId, us.UserName, c.NameNative as SubscriberName, ut.typeEN from np_Usage u, au_users us, np_Companys c, np_UsageTypes ut, np_IDNumbers sn
where u.Creditinfoid = us.CreditInfoID and us.SubscriberID = c.CreditInfoID and us.SubscriberID = sn.CreditInfoID and sn.Number = @subscriberId and u.query_type = ut.Id
and u.created >@from and u.created < @to
and u.query is not null 
) x, 
np_Companys cc, np_IDNumbers nn where x.CreditInfoId = cast(cc.CreditInfoID as nvarchar(30))
and cc.CreditInfoID = nn.CreditInfoID 

union all

select x.SubscriberName, x.UserName, x.Created, x.IsBillable, x.typeEN as ReportName, nn.Number, cc.FirstNameNative + ' ' + cc.SurNameNative as Name, 0 as IsCompany from (
select u.created, u.IsBillable, u.query as CreditInfoId, us.UserName, c.NameNative as SubscriberName, ut.typeEN from np_Usage u, au_users us, np_Companys c, np_UsageTypes ut, np_IDNumbers sn
where u.Creditinfoid = us.CreditInfoID and us.SubscriberID = c.CreditInfoID and us.SubscriberID = sn.CreditInfoID and sn.Number = @subscriberId and u.query_type = ut.Id
and u.created >@from and u.created < @to
and u.query is not null 
) x, np_Individual cc, np_IDNumbers nn where x.CreditInfoId = cast(cc.CreditInfoID as nvarchar(30))
and cc.CreditInfoID = nn.CreditInfoID 

) x ",
 


  "orders": [
    {
      "key": "created",
      "value": "created",
      "type": "DESC"
    }
  ],


   "parameters": [
    {
      "key": "datefrom",
      "label": "@from",
      "description": "Date from",
	  "value": "2017-01-01",
	  "type" : "date"
    },
    {
      "key": "dateto",
      "label": "@to",
      "description": "Date to",
	  "value": "",
	  "type" : "date"
    },
	{
      "key": "subscriberId",
      "label": "@subscriberId",
      "description": "Subscriber ID",
	  "value": "",
	  "type" : ""
    }
  ],

  "declarations": "
  
		declare @from nvarchar(50)
		declare @to nvarchar(50)
		declare @subscriberId nvarchar(30);

  
  "

}
