﻿{

"Description" : "Accounts with year",
UniqueName    : "accyear",

  "body": " SELECT   dbo.FSI_TemplateGeneralInfo.Financial_year, dbo.FSI_TemplateGeneralInfo.Ready_for_web_publishing, dbo.FSI_TemplateGeneralInfo.Currency, dbo.FSI_TemplateGeneralInfo.CreditInfoID, dbo.ua_IDNumberCreditInfoIDs.Number, 
                      dbo.ua_IDNumberCreditInfoIDs.CreditInfoID AS Expr1
FROM         dbo.FSI_TemplateGeneralInfo INNER JOIN
                      dbo.ua_IDNumberCreditInfoIDs ON dbo.FSI_TemplateGeneralInfo.CreditInfoID = dbo.ua_IDNumberCreditInfoIDs.CreditInfoID RIGHT OUTER JOIN
                      dbo.FSI_TemplateStatements ON dbo.FSI_TemplateGeneralInfo.AFS_id = dbo.FSI_TemplateStatements.AFSID "


}
