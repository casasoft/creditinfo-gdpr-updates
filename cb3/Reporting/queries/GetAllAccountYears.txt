﻿{

"Description" : "Get All Account Years",
UniqueName    : "GetAllAccountYears",

  "body": " 
select distinct l.companyid,
case when t.cnumber is null then 'N/A' else 'True' end as '1993',
case when t1.cnumber is null then 'N/A' else 'True' end as '1994',
case when t2.cnumber is null then 'N/A' else 'True' end as '1995',
case when t3.cnumber is null then 'N/A' else 'True' end as '1996',
case when t4.cnumber is null then 'N/A' else 'True' end as '1997',
case when t5.cnumber is null then 'N/A' else 'True' end as '1998',
case when t6.cnumber is null then 'N/A' else 'True' end as '1999',
case when t7.cnumber is null then 'N/A' else 'True' end as '2000',
case when t8.cnumber is null then 'N/A' else 'True' end as '2001',
case when t9.cnumber is null then 'N/A' else 'True' end as '2002',
case when t10.cnumber is null then 'N/A' else 'True' end as '2003',
case when t11.cnumber is null then 'N/A' else 'True' end as '2004',
case when t12.cnumber is null then 'N/A' else 'True' end as '2005',
case when t13.cnumber is null then 'N/A' else 'True' end as '2006',
case when t14.cnumber is null then 'N/A' else 'True' end as '2007',
case when t15.cnumber is null then 'N/A' else 'True' end as '2008',
case when t16.cnumber is null then 'N/A' else 'True' end as '2009',
case when t17.cnumber is null then 'N/A' else 'True' end as '2010',
case when t18.cnumber is null then 'N/A' else 'True' end as '2011',
case when t19.cnumber is null then 'N/A' else 'True' end as '2012',
case when t20.cnumber is null then 'N/A' else 'True' end as '2013',
case when t21.cnumber is null then 'N/A' else 'True' end as '2014',
case when t22.cnumber is null then 'N/A' else 'True' end as '2015',
case when t23.cnumber is null then 'N/A' else 'True' end as '2016',
case when t24.cnumber is null then 'N/A' else 'True' end as '2017',
case when t25.cnumber is null then 'N/A' else 'True' end as '2018'
from lmt_companies l
left join @fin1993 t on t.cnumber = l.companyid
left join @fin1994 t1 on t1.cnumber = l.companyid
left join @fin1995 t2 on t2.cnumber = l.companyid
left join @fin1996 t3 on t3.cnumber = l.companyid
left join @fin1997 t4 on t4.cnumber = l.companyid
left join @fin1998 t5 on t5.cnumber = l.companyid
left join @fin1999 t6 on t6.cnumber = l.companyid
left join @fin2000 t7 on t7.cnumber = l.companyid
left join @fin2001 t8 on t8.cnumber = l.companyid
left join @fin2002 t9 on t9.cnumber = l.companyid
left join @fin2003 t10 on t10.cnumber = l.companyid
left join @fin2004 t11 on t11.cnumber = l.companyid
left join @fin2005 t12 on t12.cnumber = l.companyid
left join @fin2006 t13 on t13.cnumber = l.companyid
left join @fin2007 t14 on t14.cnumber = l.companyid
left join @fin2008 t15 on t15.cnumber = l.companyid
left join @fin2009 t16 on t16.cnumber = l.companyid
left join @fin2010 t17 on t17.cnumber = l.companyid
left join @fin2011 t18 on t18.cnumber = l.companyid
left join @fin2012 t19 on t19.cnumber = l.companyid
left join @fin2013 t20 on t20.cnumber = l.companyid
left join @fin2014 t21 on t21.cnumber = l.companyid
left join @fin2015 t22 on t22.cnumber = l.companyid
left join @fin2016 t23 on t23.cnumber = l.companyid
left join @fin2017 t24 on t24.cnumber = l.companyid
left join @fin2018 t25 on t25.cnumber = l.companyid
where l.companystatusid = 1
	",




	"declarations": "

 declare @fin2018 table(
cnumber varchar(25)
)
insert into @fin2018
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2018 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2017 table(
cnumber varchar(25)
)
insert into @fin2017
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2017 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2016 table(
cnumber varchar(25)
)
insert into @fin2016
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2016 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2015 table(
cnumber varchar(25)
)
insert into @fin2015
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2015 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2014 table(
cnumber varchar(25)
)
insert into @fin2014
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2014 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2013 table(
cnumber varchar(25)
)
insert into @fin2013
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2013 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2012 table(
cnumber varchar(25)
)
insert into @fin2012
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2012 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2011 table(
cnumber varchar(25)
)
insert into @fin2011
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2011 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2010 table(
cnumber varchar(25)
)
insert into @fin2010
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2010 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2009 table(
cnumber varchar(25)
)
insert into @fin2009
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2009 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number



declare @fin2008 table(
cnumber varchar(25)
)
insert into @fin2008
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2008 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2007 table(
cnumber varchar(25)
)
insert into @fin2007
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2007 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2006 table(
cnumber varchar(25)
)
insert into @fin2006
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2006 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2005 table(
cnumber varchar(25)
)
insert into @fin2005
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2005 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2004 table(
cnumber varchar(25)
)
insert into @fin2004
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2004 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2003 table(
cnumber varchar(25)
)
insert into @fin2003
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2003 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2002 table(
cnumber varchar(25)
)
insert into @fin2002
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2002 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2001 table(
cnumber varchar(25)
)
insert into @fin2001
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2001 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin2000 table(
cnumber varchar(25)
)
insert into @fin2000
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 2000 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin1999 table(
cnumber varchar(25)
)
insert into @fin1999
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 1999 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin1998 table(
cnumber varchar(25)
)
insert into @fin1998
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 1998 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin1997 table(
cnumber varchar(25)
)
insert into @fin1997
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 1997 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin1996 table(
cnumber varchar(25)
)
insert into @fin1996
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 1996 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin1995 table(
cnumber varchar(25)
)
insert into @fin1995
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 1995 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number

declare @fin1994 table(
cnumber varchar(25)
)
insert into @fin1994
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 1994 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number


declare @fin1993 table(
cnumber varchar(25)
)
insert into @fin1993
select id.Number from FSI_TemplateGeneralInfo f
join np_IDNumbers id on id.CreditInfoID = f.CreditInfoID
where f.Financial_year = 1993 and f.Ready_for_web_publishing = 'true' and id.Number in (select companyid from lmt_companies where companystatusid = 1)
order by id.Number;

  ",


   "orders": [
    {
      "key": "companyid",
      "value": "companyid",
      "type": "ASC"
    }
  ]


}
