﻿{

"Description" : "Non Usage record",
UniqueName    : "nonusgrec",

  "body": " SELECT *
  FROM [CreditInfoGroup_mt].[dbo].[np_Usage]where result_count='0'and created > @from
 ",

 

  "declarations": "
  
		declare @from nvarchar(50)

  
  ",

  "parameters": [
    {
      "key": "datefrom",
      "label": "@from",
      "description": "Date from",
	  "value": "",
	  "type" : "date"
    }
  ],

   "orders": [
    {
      "key": "created",
      "value": "created",
      "type": "ASC"
    }
  ]



  }
