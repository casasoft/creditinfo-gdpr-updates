﻿{

"Description" : "Usage record",
UniqueName    : "usgrec",

  "body": " SELECT usg.created, usg.query, tp.typeNative,
usr.UserName, usr.Email, usr.UserType, usr.SubscriberID, dep.NameNative as [Department]
FROM np_Usage usg
left join au_Users usr on usr.CreditInfoID = usg.Creditinfoid
left join np_UsageTypes tp on tp.id = usg.query_type
left join au_Subscribers sub on sub.ID = usr.SubscriberID
left join c_au_department dep on dep.DepartmentID = usr.DepartmentID
where query = 'C 10865' 
and usg.created BETWEEN DATEADD(month, -12, getdate()) and getdate() 
 ",






  }
