﻿{

"Description" : "ALL ID CARDS Address Town",
UniqueName    : "allIdcardaddtown",

  "body": " SELECT     dbo.np_IDNumbers.Number, dbo.np_Individual_FullName.Surname, dbo.np_Individual_FullName.OtherSurname, dbo.np_Individual_FullName.Name, 
                      dbo.np_Individual_FullName.OtherName, dbo.np_Address.StreetNative, dbo.np_City.NameNative
FROM         dbo.np_City INNER JOIN
                      dbo.np_Address ON dbo.np_City.CityID = dbo.np_Address.CityID RIGHT OUTER JOIN
                      dbo.np_Individual LEFT OUTER JOIN
                      dbo.np_IDNumbers ON dbo.np_Individual.CreditInfoID = dbo.np_IDNumbers.CreditInfoID ON 
                      dbo.np_Address.CreditInfoID = dbo.np_Individual.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Individual_FullName ON dbo.np_Individual.CreditInfoID = dbo.np_Individual_FullName.CreditInfoID
	"


}
