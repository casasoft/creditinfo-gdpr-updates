﻿{

"Description" : "All Claims TAF",
UniqueName    : "allclmTAF",

  "body": " SELECT 

cl.[ID]
,[ClaimTypeID]
,[InformationSourceID]
,convert(varchar, ClaimDate, 103)[ClaimDate]
,[CaseNr]
,[Amount]
,d.Number as ClaimOwnerNumber

FROM [CreditInfoGroup_mt].[dbo].[np_Claims] cl

left join [CreditInfoGroup_mt].[dbo].[np_IDNumbers] id on cl.CreditInfoID=id.CreditInfoID
left join [CreditInfoGroup_mt].[dbo].[np_IDNumbers] d on cl.ClaimOwnerCIID=d.CreditInfoID
left join [CreditInfoGroup_mt].[dbo].[np_Status] st on cl.StatusID=st.StatusID

where RegDate >= @regdate ",
 


  "orders": [
    {
      "key": "ID",
      "value": "ID",
      "type": "DESC"
    }
  ],


   "parameters": [
    {
      "key": "regdate",
      "label": "@regdate",
      "description": "Registeration date After",
	  "value": "1990-01-01 00:00:00",
	  "type" : "date"
    }
  ],

  "declarations": "
  
		declare @regdate varchar(50)

  
  "

}
