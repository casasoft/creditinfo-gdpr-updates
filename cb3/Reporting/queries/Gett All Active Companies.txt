﻿{

"Description" : "Gett All Active Companies",
UniqueName    : "getallactivecomp",

  "body": " select distinct
companyid as [Company Id],
companyname as [Company Name],
case 
when len(coalesce(companyaddress1+' '+companyaddress2,companyaddress1)) <= 70 then coalesce(companyaddress1+' '+companyaddress2,companyaddress1) else '' end as [Address],
companylocality as Locality,
companypostcode as [Post Code],
companycountryid as [Country Code],
'' as [Legal Entity],
'' as [EMC Code],
'E' as [Payment],
registrationdate as [Date of Registration],
'OR' as [Registartion],
'L' as [L Column],
'' as Duns,
356 as ReportNumber
from CreditInfoGroup_mt.dbo.lmt_companies comp
WHERE companyid NOT LIKE 'P%' and comp.companystatusid NOT IN (3, 4, 6, 9) 

	"


}
