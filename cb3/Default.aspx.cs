using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;

namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for WebForm1.
    /// </summary>
    public partial class Default : Page {
        // Fyrir multilanguage d�mi. 
        public static CultureInfo ci;
        public static ResourceManager rm;
        protected HyperLink hlMail;
        protected Label lblFooter;
        protected Label lblFooter2;
        protected Label lblMainSubject;
        protected Label lblMainSubject2;
        protected Label lblMainSubject3;
        protected Label lblMainSubject4;
        protected Label lblMainSubject5;
        protected Label lblWelcome;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
            Page.ID = "0";
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();
        }

        private void LocalizeText() {
            lblWelcome.Text = rm.GetString("lbWelcome", ci);
            lblMainSubject.Text = rm.GetString("lbMainSubject", ci);
            lblMainSubject2.Text = rm.GetString("lbMainSubject2", ci);
            lblMainSubject3.Text = rm.GetString("lbMainSubject3", ci);
            lblMainSubject4.Text = rm.GetString("lbMainSubject4", ci);
            lblMainSubject5.Text = rm.GetString("lbMainSubject5", ci);
            lblFooter.Text = rm.GetString("lbFooter", ci);
            lblFooter2.Text = rm.GetString("lbFooter2", ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}