﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using UserAdmin.DAL;
using DataProtection;
using System.Text;

using Cig.Framework.Base.Configuration;
using Cig.Framework.Base.Common.DataProtection;


namespace cb3
{
    /// <summary>
    /// Helper for Data layer. 
    /// </summary>
    public class DatabaseHelper
    {
        /// <summary>
        /// Return the Connection string from config file.
        /// </summary>
        /// <returns></returns>
        public static string ConnectionString()
        {
            bool supportApplicationCenter;
            try
            {
                supportApplicationCenter =
                    bool.Parse(CigConfig.Configure("lookupsettings.ConnectionString.SupportsMultipleMachines"));
            }
            catch
            {
                supportApplicationCenter = false;
            }

            //PH: new configuration used.
            string appSettingValue = CigConfig.Configure("hibernate.connection.connection_string");
            string connStr;
            if (appSettingValue.ToLower().StartsWith("decrypted:"))
            {
                connStr = appSettingValue.Substring(10);
            }
            else
            {
                if (supportApplicationCenter)
                {
                    connStr = TripleDesCrypto.Decrypt(appSettingValue);
                }
                else
                {
                    var dp = new DataProtector(DataProtector.Store.USE_MACHINE_STORE);
                    var dataToDecrypt = Convert.FromBase64String(appSettingValue);
                    connStr = Encoding.ASCII.GetString(dp.Decrypt(dataToDecrypt, null));
                }
            }

            return connStr;
        }
    }
}
