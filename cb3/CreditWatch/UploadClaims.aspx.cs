#region

using System;
using System.Collections;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

#endregion

namespace CreditWatch {
    /// <summary>
    /// Summary description for UploadClaims.
    /// </summary>
    public class UploadClaims : Page {
        protected Button btAdd;
        protected Button btnRegisterUnique;
        protected Button btnRemove;
        public ArrayList files = new ArrayList();
        public int filesUploaded;
        protected HtmlAnchor help;
        protected HtmlAnchor help2;
        public ArrayList hif = new ArrayList();
        protected ListBox lbFiles;
        protected Label lblCreditWatch;
        protected Label lblWatchListRegistrationProcess;
        protected Label lbMessage;
        protected ListBox listbUnique;
        protected HtmlInputFile myUniqueFile;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
            if (Session["hif"] != null) {
                hif = (ArrayList) Session["hif"];
            }
        }

        private void btnRegisterUnique_Click(object sender, EventArgs e) {
            //	if (this.myUniqueFile.Value != "")
            //	{
            HandleFileWithUniqueID();
            //	}
        }

        private void HandleFileWithUniqueID() {
            // Vista skr� � server, taka g�gn upp �r henni og ey�a svo skr�nni...

            const string baseLocation = "C:\\temp\\";
            string status = "";

            if ((lbFiles.Items.Count == 0) && (filesUploaded == 0)) {
                lbMessage.Text = "Error - a file name must be specified.";
                return;
            }
            foreach (HtmlInputFile HIF in hif) {
                try {
                    string fn = Path.GetFileName(HIF.PostedFile.FileName);
                    HIF.PostedFile.SaveAs(baseLocation + fn);
                    filesUploaded++;
                    status += fn + "<br>";
                    var myStreamReader = new StreamReader(HIF.PostedFile.InputStream);
                    // choppa textaskjali�
                    if (!string.IsNullOrEmpty(fn)) {
                        if (fn.StartsWith("J")) {
                            // check whether the files is for legal entity or person
                            ParseLegalEntity(myStreamReader);
                        } else {
                            ParsePerson(myStreamReader);
                        }
                    }
                } catch (Exception err) {
                    lbMessage.Text = "Error saving file " + baseLocation
                                     + "<br>" + err;
                }
            }

            if (filesUploaded == hif.Count) {
                lbMessage.Text = "These " + filesUploaded + " file(s) were "
                                 + "uploaded:<br>" + status;
            }
            hif.Clear();
            Session["hif"] = null;
            lbFiles.Items.Clear();

/* Fr� Mr. Jones			
			try
			{
				// StramReader sem beinist a� skr�nni sem veri� er a� reyna a� vinna me� (innihaldi)
				StreamReader myStreamReader = new StreamReader(myUniqueFile.PostedFile.InputStream);
				char[] mySeperatorArray = new char[] {'\t'}; // Bara tab

				// Tek textann �r skr�nni, splitta honum upp samkv. leyfilegum "seperators og skila string array.
				// �arf a� setja inn me�h�ndlun � l�nubilum (\r\n)
				string[] myStringArray = myStreamReader.ReadToEnd().Replace('\r',',').Split(mySeperatorArray);
				
				for (int i=0; i<myStringArray.Length; i++)
				{
					txtbxUnique.Text += myStringArray[i];
				}
				
			}
			catch
			{
			}
		*/
        }

        private void ParseLegalEntity(TextReader myStreamReader) {
            var mySeperatorArray = new[] {'\t'}; // Bara tab
            //	string[] myStringArray = myStreamReader.ReadToEnd().Replace('\r',',').Split(mySeperatorArray);
            string[] myStringArray = myStreamReader.ReadToEnd().Split(mySeperatorArray);

            for (int i = 0; i < myStringArray.Length; i++) {
                listbUnique.Items.Add(myStringArray[i]);
            }
            return;
        }

        private void ParsePerson(TextReader myStreamReader) {
            var mySeperatorArray = new[] {'\t'}; // Bara tab
            //	string[] myStringArray = myStreamReader.ReadToEnd().Replace('\r',',').Split(mySeperatorArray);
            string[] myStringArray = myStreamReader.ReadToEnd().Split(mySeperatorArray);

            for (int i = 0; i < myStringArray.Length; i++) {
                listbUnique.Items.Add(myStringArray[i]);
            }
            return;
        }

        private void btAdd_Click(object sender, EventArgs e) {
            if (!Page.IsPostBack) {
                return;
            }
            hif.Add(myUniqueFile);
            Session["hif"] = hif;
            lbFiles.Items.Add(myUniqueFile.PostedFile.FileName);
        }

        private void btnRemove_Click(object sender, EventArgs e) {
            if (lbFiles.Items.Count != 0) {
                hif.RemoveAt(lbFiles.SelectedIndex);
                lbFiles.Items.Remove(lbFiles.SelectedItem.Text);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            this.btnRegisterUnique.Click += new System.EventHandler(this.btnRegisterUnique_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}