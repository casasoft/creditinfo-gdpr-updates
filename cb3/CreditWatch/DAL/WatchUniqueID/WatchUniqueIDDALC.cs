#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using CreditWatch.BLL.WatchUniqueID;
using DataProtection;
using cb3;
using cb3.Audit;

#endregion

namespace CreditWatch.DAL.WatchUniqueID {
    /// <summary>
    /// Summary description for WatchDALC.
    /// </summary>
    public class WatchUniqueIDDALC {

        /// <summary>
        ///  Skrifar tilvik af WatchUniqueIDBLLC ni�ur � grunn. Tilvik inniheldur notandan�mer og allar uppl�singar sem vaktin
        ///  �arf � a� halda. Rofar � grunni eru nota�ir til �ess a� tengja saman vaktina og vanskilaskr�na frekar.
        /// </summary>
        public bool AddNewToUniqueWatch(WatchUniqueIDBLLC myWatch) {
            OleDbCommand myCommand;
            var myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp INSERT skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                            "INSERT INTO cw_WatchUniqueID (CreditInfoID, UniqueID, IsCompany, Sent, Created, UserID) VALUES(?,?,?,?,?,?)",
                            myConnection) {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = myWatch.CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    /* Grunnurinn �thlutar �essu sj�lfkrafa...
					// Fyrir WatchID
					int myWatchID = -1;
					myWatchID = GetLatestWatchIDFromUniqueWatch();
					myParameter = new OleDbParameter("WatchID", OleDbType.Integer);
					myParameter.Value = ++myWatchID;
					myParameter.Direction = ParameterDirection.Input;
					myCommand.Parameters.Add(myParameter);
					*/

                    // Fyrir UniqueID
                    myParameter = new OleDbParameter("UniqueID", OleDbType.WChar)
                                  {
                                      Value = myWatch.UniqueID.ToUpper(),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsCompany
                    myParameter = new OleDbParameter("IsCompany", OleDbType.Char)
                                  {
                                      Value = myWatch.IsCompany,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Sent
                    myParameter = new OleDbParameter("Sent", OleDbType.Char)
                                  {
                                      Value = myWatch.Sent,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Created
                    myParameter = new OleDbParameter("Created", OleDbType.Date)
                                  {
                                      Value = myWatch.Created,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir UserID
                    myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                  {
                                      Value = myWatch.UserID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Skilar lista af �eim sem hafa veri� skr��ir af vakt af �kve�num a�ila. Tekur �v� inn CreditInfoID.
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetUniqueOnWatchListForUserAsDataset(int UserID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            var myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand(
                        "SELECT * FROM cw_WatchUniqueID watch WHERE watch.UserID = ?", myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Skilar lista af �eim sem hafa veri� skr��ir af vakt af �kve�num a�ila. Tekur �v� inn CreditInfoID og IsCompany.
        /// Skilar � anna� hvort lista af l�ga�ilum e�a lista af einstaklingum.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany"></param>
        /// <returns></returns>
        public DataSet GetUniqueOnWatchListForUserAsDataset(int UserID, bool IsCompany) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand =
                        new OleDbCommand(
                            "SELECT * FROM cw_WatchUniqueID watch WHERE watch.UserID = ? AND watch.IsCompany = ?",
                            myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // IsCompany sett �annig a� f�rslur s�u s�ttar m.v. �a�, �.e.a.s. anna� hvort l�ga�ilar e�a einstaklingar...
                    myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                  {
                                      Value = IsCompany.ToString(),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Ey�ir einni f�rslu fyrir einn notanda eftir einkv�mu n�meri f�rslu sem er "fali�" � DataGrid.
        /// </summary>
        /// <param name="WatchID"></param>
        /// <returns></returns>
        public bool DeleteUniqueWatchRecordByWatchID(int WatchID) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("DELETE FROM cw_WatchUniqueID WHERE WatchID = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir WatchID
                    var myParameter = new OleDbParameter("WatchID", OleDbType.Integer)
                                      {
                                          Value = WatchID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Ey�ir einni f�rslu fyrir einn notanda eftir einkv�mu n�meri (kt).
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public bool DeleteUniqueWatchRecordByUniqueID(string UniqueID) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("DELETE FROM cw_WatchUniqueID WHERE UniqueID = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir WatchID
                    var myParameter = new OleDbParameter("UniqueID", OleDbType.WChar, 50)
                                      {
                                          Value = UniqueID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Ey�ir �llum f�rslum fyrir einn notanda eftir notandaau�kenni hans.
        /// </summary>
        /// <returns></returns>
        public bool DeleteUniqueWatchRecordsByUserID(int UserID) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("DELETE FROM cw_WatchUniqueID WHERE UserID = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// skilar fj�lda ra�a � WatchUniqueID t�flu sem tilheyra �kve�num notanda.
        /// </summary>
        /// <returns>int sem er fj�ldi ra�a � t�flu m.v. �kve�inn notanda.</returns>
        public int CountUniqueID(int UserID) {
            int myCount = -1;
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp COUNT skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("SELECT COUNT(*) FROM cw_WatchUniqueID WHERE UserID = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    object objCount = myCommand.ExecuteScalar();
                    myCount = (int) objCount;

                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return myCount;
                }
                return myCount;
            }
        }

        /// <summary>
        /// Skilar fj�lda ra�a � WatchUniqueID t�flu sem tilheyra �kve�num notanda og eru anna� hvort
        /// l�ga�ilar e�a einstaklingar.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany">True fyrir l�ga�ila, False fyrir einstaklinga</param>
        /// <returns>int sem er fj�ldi ra�a � t�flu m.v. �kve�inn notanda og �kve�na tegund.</returns>
        public int CountUniqueID(int UserID, bool IsCompany) {
            int myCount = -1;
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp COUNT skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                            "SELECT COUNT(*) FROM cw_WatchUniqueID WHERE UserID = ? AND IsCompany = ?", myConnection)
                        {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsCompany
                    myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                  {
                                      Value = IsCompany.ToString(),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    object objCount = myCommand.ExecuteScalar();
                    myCount = (int) objCount;

                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return myCount;
                }
                return myCount;
            }
        }

        /// <summary>
        /// Skilar s��ast nota�a WatchID n�meri �r cw_WatchUniqueID t�flunni.
        /// </summary>
        /// <returns></returns>
        public int GetLatestWatchIDFromUniqueWatch() {
            
            using (var myOleDbConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var myOleDbCommand = new OleDbCommand(
                    "SELECT MAX(WatchID) FROM cw_WatchUniqueID", myOleDbConnection);
                myOleDbCommand.Connection.Open();
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }
                return 0;
            }
        }

        /// <summary>
        /// leitar eftir notanda og einkv�mu einkenni f�rslu og ef f�rsla finnst er skila� gildinu "true", annars "false".
        /// </summary>
        /// <returns></returns>
        public bool SearchUniqueWatch(WatchUniqueIDBLLC myWatch) {
            
            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var myCommand =
                    new OleDbCommand("SELECT * FROM cw_WatchUniqueID WHERE UserID = ? AND UniqueID = ?", myConnection);

                // Fyrir UserID
                var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                  {
                                      Value = myWatch.UserID,
                                      Direction = ParameterDirection.Input
                                  };
                myCommand.Parameters.Add(myParameter);

                // Fyrir UniqueID
                myParameter = new OleDbParameter("UniqueID", OleDbType.WChar, 50)
                              {
                                  Value = myWatch.UniqueID,
                                  Direction = ParameterDirection.Input
                              };
                myCommand.Parameters.Add(myParameter);

                myCommand.Connection.Open();
                var myReader = myCommand.ExecuteReader();

                return myReader.Read();
            }
        }

// Insert here
    }
}