#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using DataProtection;
using cb3;

#endregion

namespace CreditWatch.DAL.cw_make_monitlist_puser {
    /// <summary>
    /// Summary description for cw_make_monitlist_puserDALC.
    /// </summary>
    public class cw_make_monitlist_puserDALC {
//		/// <summary>
//		/// Skilar lista af �eim sem hafa veri� skr��ir � vakt af �kve�num a�ila og finnast � Vanskilaskr�.
//		/// Tekur UserID (CreditWatchID).
//		/// Skilar DataSet
//		/// </summary>
//		/// <param name="CreditInfoID"></param>
//		/// <param name="IsCompany"></param>
//		/// <returns>DataSet</returns>
//		public DataSet GetClaimListAsDataSet(int UserID, int Native)
//		{
//			// �tb� t�mt dataset
//			DataSet mySet = new DataSet();
//			OleDbCommand myCommand;
//			string myConnectionString = DatabaseHelper.ConnectionString();
//
//			using (OleDbConnection myConnection = new OleDbConnection(myConnectionString))
//			{
//				try
//				{
//					myConnection.Open();
//					myCommand = new OleDbCommand("select * from cw_make_monitlist_puser(?,?)", myConnection);
//					//myCommand.CommandType = CommandType.StoredProcedure;
//
//					// CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
//					OleDbParameter myParameter = new OleDbParameter("UserID", OleDbType.Integer);
//					myParameter.Value = UserID;
//					myParameter.Direction = ParameterDirection.Input;
//					myCommand.Parameters.Add(myParameter);
//
//					// Native sett �annig a� f�rslur s�u s�ttar m.v. �a�...
//					myParameter = new OleDbParameter("native", OleDbType.Integer);
//					myParameter.Value = Native;
//					myParameter.Direction = ParameterDirection.Input;
//					myCommand.Parameters.Add(myParameter);
//
//					// �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
//					OleDbDataAdapter myAdapter = new OleDbDataAdapter();
//					myAdapter.SelectCommand = myCommand;
//					
//					// Skila ni�urst��um � dataset...
//					myAdapter.Fill(mySet);
//					return mySet;
//				}
//				catch(Exception e)
//				{
//					BLL.Logger.WriteToLog(e.Message, true);
//					return null;
//				}
//				finally
//				{
//					// Ef �a� �arf a� ganga fr� einhverju �� � a� gera �a� h�r. � ekki a� �urfa me� "using" statements.
//				}
//			}
//		}
        /// <summary>
        /// Skilar lista af �eim sem hafa veri� skr��ir � vakt af �kve�num a�ila og finnast � Vanskilaskr�.
        /// Tekur UserID (CreditWatchID).
        /// Skilar DataSet
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetClaimListAsDataSet(int UserID, int Native) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand =
                        new OleDbCommand(
                            "select distinct subject_name, subject_address, subject_city, creditinfoid from cw_make_monitlist_puser(?,?,?)",
                            myConnection);
                    //myCommand.CommandType = CommandType.StoredProcedure;

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Native sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("native", OleDbType.Integer)
                                  {
                                      Value = Native,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Param fyrir vaktina
                    myParameter = new OleDbParameter("only_new", OleDbType.Integer)
                                  {
                                      Value = 0,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }
    }
}