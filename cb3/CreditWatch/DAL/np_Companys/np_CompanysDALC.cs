#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using DataProtection;
using cb3;

#endregion

namespace CreditWatch.DAL.np_Companys {
    /// <summary>
    /// Summary description for np_CompanysDALC.
    /// </summary>
    public class np_CompanysDALC {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CreditInfoID"></param>
        /// <param name="native"></param>
        /// <returns>string</returns>
        public string CheckForCompanyStatus(int CreditInfoID, bool native) {
            string dbField = "np_org_status.nameNative";
            if (!native) {
                dbField = "np_org_status.nativeEN";
            }
            try {
                

                using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    var myCommand =
                        new OleDbCommand(
                            "SELECT " + dbField +
                            " FROM np_Companys INNER JOIN np_org_status ON np_Companys.org_status_code = np_org_status.id WHERE np_Companys.CreditInfoID = ?",
                            myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    myCommand.Connection.Open();

                    var myReader = myCommand.ExecuteReader();

                    return myReader.Read() ? myReader.GetString(0) : "";
                }
            } catch (Exception e) {
                Logger.WriteToLog(e.Message, true);
                return "";
            }
        }

        public bool CheckIfCompanyIsInBadShape(int CreditInfoID) {
            int[] RedNumbers = {810, 820, 830, 930};
            try {
                

                using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    var myCommand =
                        new OleDbCommand(
                            "SELECT np_companys.org_status_code FROM np_Companys WHERE np_Companys.CreditInfoID = ?",
                            myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    myCommand.Connection.Open();

                    var myReader = myCommand.ExecuteReader();

                    if (myReader.Read()) {
                        // Athuga hvort fyrirt�ki� eigi � vandr��um og eigi �ar me� a� litast rautt � vi�m�ti
                        // �egar n�kv�m leit (cwClaimSearchDetails) er sko�u�...
                        foreach (int iValue in RedNumbers) {
                            if (myReader.GetInt32(0) == iValue) {
                                return true;
                            }
                        }

                        return false;
                    }
                    return false;
                }
            } catch (Exception e) {
                Logger.WriteToLog(e.Message, true);
                return false;
            }
        }

        public string CheckForCompanyNationalID(int CreditInfoID) {
            try {
                

                using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    var myCommand =
                        new OleDbCommand(
                            "SELECT np_IDNumbers.Number FROM np_IDNumbers WHERE np_IDNumbers.CreditInfoID = ?",
                            myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    myCommand.Connection.Open();

                    OleDbDataReader myReader = myCommand.ExecuteReader();

                    if (myReader.Read()) {
                        return myReader.GetString(0);
                    } else {
                        return "";
                    }
                }
            } catch (Exception e) {
                Logger.WriteToLog(e.Message, true);
                return "";
            }
        }
    }
}