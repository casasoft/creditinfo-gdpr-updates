#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using CreditWatch.BLL.WatchWords;
using DataProtection;
using cb3;
using cb3.Audit;

#endregion

namespace CreditWatch.DAL.WatchWords {
    /// <summary>
    /// Summary description for CreditInfoUserDALC.
    /// </summary>
    public class WatchWordsDALC : BaseDALC {

        /// <summary>
        ///  Skrifar tilvik af WatchWordsBLLC ni�ur � grunn. Tilvik inniheldur notandan�mer og allar uppl�singar sem vaktin
        ///  �arf � a� halda. Rofar � grunni eru nota�ir til �ess a� tengja saman vaktina og vanskilaskr�na frekar.
        /// </summary>
        public bool AddNewWordsToWatch(WatchWordsBLLC myWatch) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                OleDbTransaction myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp INSERT skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                            "INSERT INTO cw_WatchWords (CreditInfoID, FirstNameNative, FirstNameEN, SurNameNative, SurNameEN, AddressNative, AddressEN, TownNative, TownEN, IsCompany, Sent, Created, UserID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",
                            myConnection) {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = myWatch.CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    /*	Grunnurinn s�r um a� setja �etta ni�ur sj�lfur...
					
					// Fyrir WatchID
					int myWatchID = -1;
					myWatchID = GetLatestWatchIDFromWatchWords();
					myParameter = new OleDbParameter("WatchID", OleDbType.Integer);
					myParameter.Value = ++myWatchID;
					myParameter.Direction = ParameterDirection.Input;
					myCommand.Parameters.Add(myParameter);
					*/

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myCommand,
                        "FirstNameNative",
                        "FirstNameEN",
                        ParameterDirection.Input,
                        myWatch.FirstNameNative,
                        myWatch.FirstNameEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myCommand,
                        "SurNameNative",
                        "SurNameEN",
                        ParameterDirection.Input,
                        myWatch.SurNameNative,
                        myWatch.SurNameEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myCommand,
                        "AddressNative",
                        "AddressEN",
                        ParameterDirection.Input,
                        myWatch.AddressNative,
                        myWatch.AddressEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myCommand,
                        "TownNative",
                        "TownEN",
                        ParameterDirection.Input,
                        myWatch.TownNative,
                        myWatch.TownEN,
                        OleDbType.WChar);

                    // Fyrir IsCompany
                    myParameter = new OleDbParameter("IsCompany", OleDbType.Char)
                                  {
                                      Value = myWatch.IsCompany,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Sent
                    myParameter = new OleDbParameter("Sent", OleDbType.Char)
                                  {
                                      Value = myWatch.Sent,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir Created
                    myParameter = new OleDbParameter("Created", OleDbType.Date)
                                  {
                                      Value = myWatch.Created,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir UserID sem nota� er fyrir alla leit - leita� eftir notanda
                    myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                  {
                                      Value = myWatch.UserID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Skilar lista af �eim sem hafa veri� skr��ir af stikkor�avakt af �kve�num a�ila. Tekur �v� inn UserID.
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetWordsToWatchListForUserAsDataSet(int UserID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand(
                        "SELECT TOP 1000 * FROM cw_WatchWords watch WHERE watch.UserID = ?", myConnection);

                    // UserID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Skilar lista af �eim sem hafa veri� skr��ir � stikkor�avakt af �kve�num a�ila. 
        /// Tekur �v� inn UserID og IsCompany.
        /// Skilar �� anna� hvort lista af l�ga�ilum e�a lista af einstaklingum.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany"></param>
        /// <returns></returns>
        public DataSet GetWordsToWatchListForUserAsDataSet(int UserID, bool IsCompany) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand =
                        new OleDbCommand(
                            "SELECT CreditInfoID, WatchID, ISNULL(FirstNameEN, FirstNameNative) FirstName, ISNULL(SurNameEN, SurNameNative) SurName, ISNULL(AddressEN, AddressNative) Address, ISNULL(TownEN, TownNative) Town, IsCompany, Sent, Created, CASE WHEN FirstNameEN IS NOT NULL THEN 'False' ELSE 'True' END AS IsNative FROM cw_WatchWords WHERE UserID=? AND IsCompany=?",
                            myConnection);

                    // UserID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // IsCompany sett �annig a� f�rslur s�u s�ttar m.v. �a�, �.e.a.s. anna� hvort l�ga�ilar e�a einstaklingar...
                    myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                  {
                                      Value = IsCompany.ToString(),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Ey�ir einni stakri f�rslu �r cw_WatchWords t�flunni eftir einkv�mu n�meri f�rslunnar.
        /// </summary>
        /// <param name="WatchID"></param>
        /// <returns></returns>
        public bool DeleteWatchWordsRecordByWatchID(int WatchID) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                OleDbTransaction myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("DELETE FROM cw_WatchWords WHERE WatchID = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir WatchID
                    var myParameter = new OleDbParameter("WatchID", OleDbType.Integer)
                                      {
                                          Value = WatchID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Ey�ir einni stakri f�rslu �r cw_WatchWords t�flunni eftir �llum uppl�singum um stikkor� f�rslunnar.
        /// </summary>
        /// <returns></returns>
        public bool DeleteWatchWordsRecordByWatchWords(
            int UserID, string FirstName, string SurName, string Address, string Town, string IsCompany, bool IsNative) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                OleDbTransaction myTransaction = myConnection.BeginTransaction();

                // Ef f�rslan er sett inn � "native" m�li �� �arf a� ey�a hennni samkv�mt native sv��unum.
                if (IsNative) {
                    try {
                        //string myCommandString;

                        //myCommandString = "DELETE FROM cw_WatchWords WHERE CreditInfoID = ? AND FirstNameNative = ? AND SurNameNative= ? AND AddressNative = ? AND TownNative = ? AND IsCompany = ?";

                        // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                        myCommand =
                            new OleDbCommand(
                                "DELETE FROM cw_WatchWords WHERE UserID = ? AND FirstNameNative = ? AND SurNameNative= ? AND AddressNative = ? AND TownNative = ? AND IsCompany = ?",
                                myConnection) {Transaction = myTransaction};

                        // Fyrir UserID
                        var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                          {
                                              Value = UserID,
                                              Direction = ParameterDirection.Input
                                          };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir FirstNameNative
                        myParameter = new OleDbParameter("FirstNameNative", OleDbType.WChar, 100)
                                      {
                                          Value = FirstName,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir SurNameNative
                        myParameter = new OleDbParameter("SurNameNative", OleDbType.WChar, 100)
                                      {
                                          Value = SurName,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir AddressNative
                        myParameter = new OleDbParameter("AddressNative", OleDbType.WChar, 100)
                                      {
                                          Value = Address,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir TownNative
                        myParameter = new OleDbParameter("TownNative", OleDbType.WChar, 100)
                                      {
                                          Value = Town,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir IsCompany
                        myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                      {
                                          Value = IsCompany,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                        myTransaction.Commit();
                    } catch (Exception e) {
                        Logger.WriteToLog(e.Message, true);
                        myTransaction.Rollback();
                        return false;
                    }
                }
                    // Ef veri� er a� ey�a einhverju sem er skr�� me� Ensku...
                else {
                    try {
                        // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                        myCommand =
                            new OleDbCommand(
                                "DELETE FROM cw_WatchWords WHERE UserID = ? AND FirstNameEN = ? AND SurNameEN = ? AND AddressEN = ? AND TownEN = ? AND IsCompany = ?",
                                myConnection) {Transaction = myTransaction};

                        // Fyrir UserID
                        var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                          {
                                              Value = UserID,
                                              Direction = ParameterDirection.Input
                                          };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir FirstNameEN
                        myParameter = new OleDbParameter("FirstNameEN", OleDbType.WChar, 100)
                                      {
                                          Value = FirstName,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir SurNameEN
                        myParameter = new OleDbParameter("SurNameEN", OleDbType.WChar, 100)
                                      {
                                          Value = SurName,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir AddressEN
                        myParameter = new OleDbParameter("AddressEN", OleDbType.WChar, 100)
                                      {
                                          Value = Address,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir TownEN
                        myParameter = new OleDbParameter("TownEN", OleDbType.WChar, 100)
                                      {
                                          Value = Town,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        // Fyrir IsCompany
                        myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                      {
                                          Value = IsCompany,
                                          Direction = ParameterDirection.Input
                                      };
                        myCommand.Parameters.Add(myParameter);

                        new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                        myTransaction.Commit();
                    } catch (Exception e) {
                        Logger.WriteToLog(e.Message, true);
                        myTransaction.Rollback();
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Ey�ir �llum f�rslum fyrir einn notanda eftir notandaau�kenni hans.
        /// </summary>
        /// <returns></returns>
        public bool DeleteWatchWordsRecordsByUserID(int UserID) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                OleDbTransaction myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp DELETE skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("DELETE FROM cw_WatchWords WHERE UserID = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        public bool UpdateWordsToWatch(WatchWordsBLLC myWatch) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                OleDbTransaction myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp UPDATE skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                            "UPDATE cw_WatchWords SET  FirstNameNative = ?, FirstNameEN = ?, SurNameNative = ?, SurNameEN = ?, AddressNative = ?, AddressEN = ?, TownNative = ?, TownEN = ?, IsCompany = ? WHERE WatchID = ?",
                            myConnection) {Transaction = myTransaction};

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myCommand,
                        "FirstNameNative",
                        "FirstNameEN",
                        ParameterDirection.Input,
                        myWatch.FirstNameNative,
                        myWatch.FirstNameEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myCommand,
                        "SurNameNative",
                        "SurNameEN",
                        ParameterDirection.Input,
                        myWatch.SurNameNative,
                        myWatch.SurNameEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myCommand,
                        "AddressNative",
                        "AddressEN",
                        ParameterDirection.Input,
                        myWatch.AddressNative,
                        myWatch.AddressEN,
                        OleDbType.WChar);

                    // Insert Native and EN strings
                    CreateTransliteratedParameterFromString(
                        myCommand,
                        "TownNative",
                        "TownEN",
                        ParameterDirection.Input,
                        myWatch.TownNative,
                        myWatch.TownEN,
                        OleDbType.WChar);

                    // Fyrir IsCompany
                    var myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                      {
                                          Value = myWatch.IsCompany,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir WatchID
                    myParameter = new OleDbParameter("WatchID", OleDbType.Integer)
                                  {
                                      Value = myWatch.WatchID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// skilar fj�lda ra�a � WatchWords t�flu sem tilheyra �kve�num notanda.
        /// </summary>
        /// <returns>int sem er fj�ldi ra�a � t�flu m.v. �kve�inn notanda.</returns>
        public int CountWatchWords(int UserID) {
            int myCount = -1;
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                OleDbTransaction myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp COUNT skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand("SELECT COUNT(*) FROM cw_WatchWords WHERE UserID = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir UserID
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    object objCount = myCommand.ExecuteScalar();
                    myCount = (int) objCount;

                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return myCount;
                }
                return myCount;
            }
        }

        /// <summary>
        /// Skilar fj�lda ra�a � WatchWords t�flu sem tilheyra �kve�num notanda og eru anna� hvort
        /// l�ga�ilar e�a einstaklingar.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany">True fyrir l�ga�ila, False fyrir einstaklinga</param>
        /// <returns>int sem er fj�ldi ra�a � t�flu m.v. �kve�inn notanda og �kve�na tegund.</returns>
        public int CountWatchWords(int UserID, bool IsCompany) {
            int myCount = -1;
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                OleDbTransaction myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp COUNT skipun fyrir gagnagrunninn me� params...
                    myCommand = new OleDbCommand(
                        "SELECT COUNT(*) FROM cw_WatchWords WHERE UserID = ? AND IsCompany = ?", myConnection)
                                {Transaction = myTransaction};

                    // Fyrir UserID
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsCompany
                    myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                  {
                                      Value = IsCompany.ToString(),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    object objCount = myCommand.ExecuteScalar();
                    myCount = (int) objCount;

                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return myCount;
                }
                return myCount;
            }
        }

        /// <summary>
        /// Skilar s��ast nota�a WatchID n�meri �r cw_WatchWords t�flunni.
        /// </summary>
        /// <returns></returns>
        public int GetLatestWatchIDFromWatchWords() {
            
            using (var myOleDbConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var myOleDbCommand = new OleDbCommand(
                    "SELECT MAX(WatchID) FROM cw_WatchWords", myOleDbConnection);
                myOleDbCommand.Connection.Open();
                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                }

                reader.Close();
                return 0;
            }
        }

        /// <summary>
        /// leitar eftir notanda og leitaror�um f�rslu og ef f�rsla finnst er skila� gildinu "true", annars "false".
        /// </summary>
        /// <returns></returns>
        public bool SearchWatchWords(WatchWordsBLLC myWatch, bool isNative) {
            
            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                if (isNative) {
                    var myCommand =
                        new OleDbCommand(
                            "SELECT * FROM cw_WatchWords WHERE UserID = ? AND FirstNameNative = ? AND SurNameNative = ? AND AddressNative = ? AND TownNative = ? AND IsCompany = ?",
                            myConnection);

                    // Fyrir UserID
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = myWatch.UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir FirstNameNative
                    myParameter = new OleDbParameter("FirstNameNative", OleDbType.WChar)
                                  {
                                      Value = (myWatch.FirstNameNative ?? null),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir SurNameNative
                    myParameter = new OleDbParameter("SurNamenative", OleDbType.WChar)
                                  {
                                      Value = (myWatch.SurNameNative ?? null),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir AddressNative
                    myParameter = new OleDbParameter("AddressNative", OleDbType.WChar)
                                  {
                                      Value = (myWatch.AddressNative ?? null),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir TownNative
                    myParameter = new OleDbParameter("TownNative", OleDbType.WChar)
                                  {
                                      Value = (myWatch.TownNative ?? null),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsCompany
                    myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                  {
                                      Value = myWatch.IsCompany,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    myCommand.Connection.Open();
                    OleDbDataReader myReader = myCommand.ExecuteReader();

                    if (myReader.Read()) {
                        myReader.Close();
                        return true;
                    }
                    myReader.Close();
                    return false;
                } else {
                    var myCommand =
                        new OleDbCommand(
                            "SELECT * FROM cw_WatchWords WHERE UserID = ? AND FirstNameEN = ? AND SurNameEN = ? AND AddressEN = ? AND TownEN = ? AND IsCompany = ?",
                            myConnection);

                    // Fyrir UserID
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = myWatch.UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir FirstNameEN
                    myParameter = new OleDbParameter("FirstNameEN", OleDbType.WChar)
                                  {
                                      Value = myWatch.FirstNameEN,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir SurNameEN
                    myParameter = new OleDbParameter("SurNameEN", OleDbType.WChar)
                                  {
                                      Value = myWatch.SurNameEN,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir AddressEN
                    myParameter = new OleDbParameter("AddressEN", OleDbType.WChar)
                                  {
                                      Value = myWatch.AddressEN,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir TownEN
                    myParameter = new OleDbParameter("TownEN", OleDbType.WChar)
                                  {
                                      Value = myWatch.TownEN,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir IsCompany
                    myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                  {
                                      Value = myWatch.IsCompany,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    myCommand.Connection.Open();
                    var myReader = myCommand.ExecuteReader();

                    if (myReader.Read()) {
                        myReader.Close();
                        return true;
                    }
                    myReader.Close();
                    return false;
                }
            }
        }

        //Insert here
    }
}