#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using DataProtection;
using cb3;

#endregion

namespace CreditWatch.DAL.np_Employee {
    /// <summary>
    /// Summary description for np_EmployeeDALC.
    /// </summary>
    public class np_EmployeeDALC {

        /// <summary>
        /// 
        /// </summary>
        /// <returns>DataSet</returns>
        public bool CheckForEmployee(string UserLoginName) {
            try {
                

                using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                    var myCommand = new OleDbCommand(
                        "SELECT * FROM np_Employee emp WHERE emp.Initials = ?", myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("Initials", OleDbType.WChar)
                                      {
                                          Value = UserLoginName,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    myCommand.Connection.Open();

                    var myReader = myCommand.ExecuteReader();

                    return myReader.Read();
                }
            } catch (Exception e) {
                Logger.WriteToLog(e.Message, true);
                return false;
            }
        }
    }
}