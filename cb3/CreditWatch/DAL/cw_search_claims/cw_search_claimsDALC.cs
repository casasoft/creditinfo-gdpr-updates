#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using DataProtection;
using cb3;

#endregion

namespace CreditWatch.DAL.cw_search_claims {
    /// <summary>
    /// Summary description for cw_search_claimsDALC.
    /// </summary>
    public class cw_search_claimsDALC {

        /// <summary>
        /// 
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetNativeSearchList(string Number, string FirstName, string Surname, string Address) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand(
                        "select TOP 20 * from np_search_claims_native2 (?,?,?,?)", myConnection);

                    // Number sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("number", OleDbType.WChar)
                                      {
                                          Value = Number,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // FirstName sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("firstname", OleDbType.WChar)
                                  {
                                      Value = FirstName,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Surname sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("surname", OleDbType.WChar)
                                  {
                                      Value = Surname,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Address sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("address", OleDbType.WChar)
                                  {
                                      Value = Address,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetENSearchList(string Number, string FirstName, string Surname, string Address) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand("select TOP 50 * from np_search_claims_en (?,?,?,?)", myConnection);

                    // Number sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("number", OleDbType.WChar)
                                      {
                                          Value = Number,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // FirstName sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("firstname", OleDbType.WChar)
                                  {
                                      Value = FirstName,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Surname sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("surname", OleDbType.WChar)
                                  {
                                      Value = Surname,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Address sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    myParameter = new OleDbParameter("address", OleDbType.WChar)
                                  {
                                      Value = Address,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }
    }
}