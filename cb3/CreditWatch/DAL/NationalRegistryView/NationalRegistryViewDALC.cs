#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using CreditWatch.BLL.NationalRegistryView;
using DataProtection;
using UserAdmin.BLL.CIUsers;
using cb3;

#endregion

namespace CreditWatch.DAL.NationalRegistryView {
    /// <summary>
    /// Summary description for NationalRegistryViewDALC.
    /// </summary>
    public class NationalRegistryViewDALC {

        /// <summary>
        /// Leitar a� sk��um l�ga�ila e�a einstakling � "NationalRegistryView" mi�a� vi� "kennit�lu"
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public bool SearchNationalRegistry(string UniqueID) {
            
            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var myCommand = new OleDbCommand(
                    "SELECT * FROM cw_NationalRegistryView WHERE UniqueID = ?", myConnection);

                // Fyrir UniqueID
                var myParameter = new OleDbParameter("UniqueID", OleDbType.WChar, 50)
                                  {
                                      Value = UniqueID,
                                      Direction = ParameterDirection.Input
                                  };
                myCommand.Parameters.Add(myParameter);

                myCommand.Connection.Open();
                return myCommand.ExecuteReader().Read();
            }
        }

        /// <summary>
        /// Skilar tilviki af NationalRegistryView m.v. gefna "kennit�lu".
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public NationalRegistryViewBLLC GetNationalRegistry(string UniqueID) {
            
            using (var myConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var myCommand = new OleDbCommand(
                    "SELECT * FROM cw_NationalRegistryView WHERE UniqueID = ?", myConnection);

                // Fyrir UniqueID
                var myParameter = new OleDbParameter("UniqueID", OleDbType.WChar, 50)
                                  {
                                      Value = UniqueID,
                                      Direction = ParameterDirection.Input
                                  };
                myCommand.Parameters.Add(myParameter);

                myCommand.Connection.Open();
                var myReader = myCommand.ExecuteReader();
                var myRegistry = new NationalRegistryViewBLLC();

                if (myReader.Read()) {
                    myRegistry.UniqueID = myReader.GetString(0);
                    if (!myReader.IsDBNull(1)) {
                        myRegistry.Name = myReader.GetString(1);
                    }
                    if (!myReader.IsDBNull(2)) {
                        myRegistry.Address = myReader.GetString(2);
                    }
                    if (!myReader.IsDBNull(3)) {
                        myRegistry.IsCompany = myReader.GetString(3);
                    }
                }

                return myRegistry;
            }
        }

        /// <summary>
        /// This function is taken from Elitas Lithunaian version
        /// </summary>
        /// <param name="creditInfoID">The subjects internal system id</param>
        /// <returns>Instance of CompanyLT</returns>
        public CompanyLT GetCompanyReport(int creditInfoID) {
            var theReportCompany = new CompanyLT();
            const string funcName = "GetCompanyReport(string creditInfoID) ";

            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();
/*
					OleDbCommand myOleDbCommand = new OleDbCommand("SELECT ros_NationalCompanyInterfaceView.IDNumber, "+
						" ros_NationalCompanyInterfaceView.Name, "+
						" ros_NationalCompanyInterfaceView.Org_name_status_code, "+ 
						" CASE WHEN shadow.dbo.findeksa.telefonas IS NOT NULL THEN shadow.dbo.findeksa.telefonas ELSE "+
						" (CASE WHEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) IS NOT NULL THEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) "+
						" ELSE cast(ros_NationalCompanyInterfaceView.PhoneNumber AS nvarchar) END)END AS TELEF, "+
						" CASE WHEN shadow.dbo.findeksa.faksas IS NOT NULL THEN rtrim(shadow.dbo.findeksa.faksas) ELSE cast(ros_NationalCompanyInterfaceView.FaxNumber AS nvarchar) END AS FAX, "+
						" ros_NationalCompanyInterfaceView.Address AS JURIDICAL_ADDRESS, "+
						" ros_NationalCompanyInterfaceView.vadov, "+
						" ros_NationalCompanyInterfaceView.vad_adres, ros_NationalCompanyInterfaceView.vad_telef, "+
						" ros_NationalCompanyInterfaceView.ist_data, "+
						" ros_NationalCompanyInterfaceView.reg_data, "+
						" shadow.dbo.findeksa.mobilus AS MOBILE, "+ 
						" CASE WHEN shadow.dbo.findeksa.adresas IS NOT NULL THEN shadow.dbo.findeksa.adresas ELSE "+
						" (CASE WHEN ros_NationalCompanyInterfaceView.adres IS NOT NULL THEN ros_NationalCompanyInterfaceView.adres ELSE ros_NationalCompanyInterfaceView.Address END)END AS OFFICE_ADDRESS, "+
						" CASE WHEN shadow.dbo.findeksa.email IS NOT NULL THEN rtrim(shadow.dbo.findeksa.email) ELSE ros_NationalCompanyInterfaceView.email END AS EMAIL, "+ 
						" shadow.dbo.findeksa.www AS F_WWW, "+
						" ros_NationalCompanyInterfaceView.Org_nameEN_status_code, "+
						" ros_NationalCompanyInterfaceView.LERStatus_ID, ros_NationalCompanyInterfaceView.SLERStatus_ID, "+
						" np_city.NameNative, np_city.NameEN, " +
						"ros_NationalCompanyInterfaceView.BUKLE_Native, ros_NationalCompanyInterfaceView.BUKLE_EN," +
						"ros_NationalCompanyInterfaceView.BUKLES_Native, ros_NationalCompanyInterfaceView.BUKLES_EN" + 
						" FROM ros_NationalCompanyInterfaceView "+
						" LEFT OUTER JOIN shadow.dbo.findeksa ON shadow.dbo.findeksa.imone = ros_NationalCompanyInterfaceView.IDNumber "+
						" LEFT OUTER JOIN np_city ON np_city.CityID = VIETK "+
						" WHERE IDNumber = "+ creditInfoID +" OR JAR_KODAS = " + creditInfoID,myOleDbConn);
	*/

                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT ros_NationalCompanyInterfaceView.IDNumber, " +
                            "ros_NationalCompanyInterfaceView.Name, " +
                            "ros_NationalCompanyInterfaceView.Org_name_status_code, " +
                            "CASE WHEN CreditInfoGroup_lt_work.dbo.findeksa.telefonas IS NOT NULL THEN CreditInfoGroup_lt_work.dbo.findeksa.telefonas ELSE " +
                            "(CASE WHEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) IS NOT NULL THEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) " +
                            "ELSE cast(ros_NationalCompanyInterfaceView.PhoneNumber AS nvarchar) END)END AS TELEF, " +
                            "CASE WHEN CreditInfoGroup_lt_work.dbo.findeksa.faksas IS NOT NULL THEN rtrim(CreditInfoGroup_lt_work.dbo.findeksa.faksas) ELSE cast(ros_NationalCompanyInterfaceView.FaxNumber AS nvarchar) END AS FAX, " +
                            "ros_NationalCompanyInterfaceView.Address AS JURIDICAL_ADDRESS, " +
                            "ros_NationalCompanyInterfaceView.vadov, " +
                            "ros_NationalCompanyInterfaceView.vad_adres, ros_NationalCompanyInterfaceView.vad_telef, " +
                            "ros_NationalCompanyInterfaceView.ist_data, " +
                            "ros_NationalCompanyInterfaceView.reg_data, " +
                            "CreditInfoGroup_lt_work.dbo.findeksa.mobilus AS MOBILE, " +
                            "CASE WHEN CreditInfoGroup_lt_work.dbo.findeksa.adresas IS NOT NULL THEN CreditInfoGroup_lt_work.dbo.findeksa.adresas ELSE " +
                            "(CASE WHEN ros_NationalCompanyInterfaceView.adres IS NOT NULL THEN ros_NationalCompanyInterfaceView.adres ELSE ros_NationalCompanyInterfaceView.Address END)END AS OFFICE_ADDRESS, " +
                            "CASE WHEN CreditInfoGroup_lt_work.dbo.findeksa.email IS NOT NULL THEN rtrim(CreditInfoGroup_lt_work.dbo.findeksa.email) ELSE ros_NationalCompanyInterfaceView.email END AS EMAIL,  " +
                            "CreditInfoGroup_lt_work.dbo.findeksa.www AS F_WWW, " +
                            "ros_NationalCompanyInterfaceView.Org_nameEN_status_code, " +
                            "ros_NationalCompanyInterfaceView.LERStatus_ID, ros_NationalCompanyInterfaceView.SLERStatus_ID, " +
                            "np_city.NameNative, np_city.NameEN, " +
                            "ros_NationalCompanyInterfaceView.BUKLE_Native, ros_NationalCompanyInterfaceView.BUKLE_EN," +
                            "ros_NationalCompanyInterfaceView.BUKLES_Native, ros_NationalCompanyInterfaceView.BUKLES_EN " +
                            "FROM ros_NationalCompanyInterfaceView " +
                            "LEFT OUTER JOIN CreditInfoGroup_lt_work.dbo.findeksa ON CreditInfoGroup_lt_work.dbo.findeksa.imone = ros_NationalCompanyInterfaceView.IDNumber " +
                            "LEFT OUTER JOIN np_city ON np_city.CityID = VIETK " +
                            " WHERE IDNumber = " + creditInfoID + " OR JAR_KODAS = " + creditInfoID,
                            myOleDbConn);

                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            theReportCompany.UniqueID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theReportCompany.NameNative = reader.GetString(1);
                            theReportCompany.NameEN = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theReportCompany.TypeNative = reader.GetString(2);
                            theReportCompany.TypeEN = reader.GetString(15);
                        }
                        if (!reader.IsDBNull(3)) {
                            theReportCompany.Tel = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            theReportCompany.Fax = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            theReportCompany.JurAddress = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            theReportCompany.Director = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            theReportCompany.DirectorAddress = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            theReportCompany.DirectorPhone = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            theReportCompany.Established = reader.GetString(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            theReportCompany.Registered = reader.GetString(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            theReportCompany.Mobile = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            theReportCompany.OfficeAddress = reader.GetString(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            theReportCompany.Email = reader.GetString(13);
                        }
                        if (!reader.IsDBNull(14)) {
                            theReportCompany.WWW = reader.GetString(14);
                        }
                        if (!reader.IsDBNull(16)) {
                            theReportCompany.LERStatusID = reader.GetInt32(16);
                        }
                        if (!reader.IsDBNull(17)) {
                            theReportCompany.SLERStatusID = reader.GetInt32(17);
                        }
                        if (!reader.IsDBNull(18)) {
                            theReportCompany.CityNative = reader.GetString(18);
                        }
                        if (!reader.IsDBNull(19)) {
                            theReportCompany.CityEN = reader.GetString(19);
                        }
                        if (!reader.IsDBNull(20)) {
                            theReportCompany.LERStatusNative = reader.GetString(20);
                            theReportCompany.LERStatusEN = reader.GetString(21);
                        }
                        if (!reader.IsDBNull(22)) {
                            theReportCompany.SLERStatusNative = reader.GetString(22);
                            theReportCompany.SLERStatusEN = reader.GetString(23);
                        }
                    }
                } catch (Exception) {
                    Logger.WriteToLog("NationalRegistryViewDALC : " + funcName, true);
                    return null;
                }
            }

            return theReportCompany;
        }

        /// <summary>
        /// This function is taken from Elitas Lithuanian version
        /// </summary>
        /// <param name="nationalID">The subjects national id</param>
        /// <param name="creditInfoID">The subjects internal system id</param>
        /// <returns>Instance of CompanyLT</returns>
        public CompanyLT GetCompanyReport(int nationalID, int creditInfoID) {
            var theReportCompany = new CompanyLT();
            const string funcName = "GetCompanyReport(string creditInfoID) ";

            
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                try {
                    myOleDbConn.Open();

/*
					OleDbCommand myOleDbCommand = new OleDbCommand("SELECT ros_NationalCompanyInterfaceView.IDNumber, "+
						" ros_NationalCompanyInterfaceView.Name, "+
						" ros_NationalCompanyInterfaceView.Org_name_status_code, "+ 
						" CASE WHEN shadow.dbo.findeksa.telefonas IS NOT NULL THEN shadow.dbo.findeksa.telefonas ELSE "+
						" (CASE WHEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) IS NOT NULL THEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) "+
						" ELSE cast(ros_NationalCompanyInterfaceView.PhoneNumber AS nvarchar) END)END AS TELEF, "+
						" CASE WHEN shadow.dbo.findeksa.faksas IS NOT NULL THEN rtrim(shadow.dbo.findeksa.faksas) ELSE cast(ros_NationalCompanyInterfaceView.FaxNumber AS nvarchar) END AS FAX, "+
						" ros_NationalCompanyInterfaceView.Address AS JURIDICAL_ADDRESS, "+
						" ros_NationalCompanyInterfaceView.vadov, "+
						" ros_NationalCompanyInterfaceView.vad_adres, ros_NationalCompanyInterfaceView.vad_telef, "+
						" ros_NationalCompanyInterfaceView.ist_data, "+
						" ros_NationalCompanyInterfaceView.reg_data, "+
						" shadow.dbo.findeksa.mobilus AS MOBILE, "+ 
						" CASE WHEN shadow.dbo.findeksa.adresas IS NOT NULL THEN shadow.dbo.findeksa.adresas ELSE "+
						" (CASE WHEN ros_NationalCompanyInterfaceView.adres IS NOT NULL THEN ros_NationalCompanyInterfaceView.adres ELSE ros_NationalCompanyInterfaceView.Address END)END AS OFFICE_ADDRESS, "+
						" CASE WHEN shadow.dbo.findeksa.email IS NOT NULL THEN rtrim(shadow.dbo.findeksa.email) ELSE ros_NationalCompanyInterfaceView.email END AS EMAIL, "+ 
						" shadow.dbo.findeksa.www AS F_WWW, "+
						" ros_NationalCompanyInterfaceView.Org_nameEN_status_code, "+
						" ros_NationalCompanyInterfaceView.LERStatus_ID, ros_NationalCompanyInterfaceView.SLERStatus_ID "+
						" FROM ros_NationalCompanyInterfaceView "+
						" LEFT OUTER JOIN shadow.dbo.findeksa ON shadow.dbo.findeksa.imone = ros_NationalCompanyInterfaceView.IDNumber "+
						" WHERE IDNumber = "+ nationalID +" OR CreditInfoID = "+ creditInfoID,myOleDbConn);
*/
                    var myOleDbCommand =
                        new OleDbCommand(
                            "SELECT ros_NationalCompanyInterfaceView.IDNumber, " +
                            " ros_NationalCompanyInterfaceView.Name, " +
                            " ros_NationalCompanyInterfaceView.Org_name_status_code, " +
                            " CASE WHEN CreditInfoGroup_lt_work.dbo.findeksa.telefonas IS NOT NULL THEN CreditInfoGroup_lt_work.dbo.findeksa.telefonas ELSE " +
                            " (CASE WHEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) IS NOT NULL THEN cast(ros_NationalCompanyInterfaceView.telef AS nvarchar) " +
                            " ELSE cast(ros_NationalCompanyInterfaceView.PhoneNumber AS nvarchar) END)END AS TELEF, " +
                            " CASE WHEN CreditInfoGroup_lt_work.dbo.findeksa.faksas IS NOT NULL THEN rtrim(CreditInfoGroup_lt_work.dbo.findeksa.faksas) ELSE cast(ros_NationalCompanyInterfaceView.FaxNumber AS nvarchar) END AS FAX, " +
                            " ros_NationalCompanyInterfaceView.Address AS JURIDICAL_ADDRESS, " +
                            " ros_NationalCompanyInterfaceView.vadov, " +
                            " ros_NationalCompanyInterfaceView.vad_adres, ros_NationalCompanyInterfaceView.vad_telef, " +
                            " ros_NationalCompanyInterfaceView.ist_data, " +
                            " ros_NationalCompanyInterfaceView.reg_data, " +
                            " CreditInfoGroup_lt_work.dbo.findeksa.mobilus AS MOBILE, " +
                            " CASE WHEN CreditInfoGroup_lt_work.dbo.findeksa.adresas IS NOT NULL THEN CreditInfoGroup_lt_work.dbo.findeksa.adresas ELSE " +
                            " (CASE WHEN ros_NationalCompanyInterfaceView.adres IS NOT NULL THEN ros_NationalCompanyInterfaceView.adres ELSE ros_NationalCompanyInterfaceView.Address END)END AS OFFICE_ADDRESS, " +
                            " CASE WHEN CreditInfoGroup_lt_work.dbo.findeksa.email IS NOT NULL THEN rtrim(CreditInfoGroup_lt_work.dbo.findeksa.email) ELSE ros_NationalCompanyInterfaceView.email END AS EMAIL, " +
                            " CreditInfoGroup_lt_work.dbo.findeksa.www AS F_WWW, " +
                            " ros_NationalCompanyInterfaceView.Org_nameEN_status_code, " +
                            " ros_NationalCompanyInterfaceView.LERStatus_ID, ros_NationalCompanyInterfaceView.SLERStatus_ID " +
                            " FROM ros_NationalCompanyInterfaceView " +
                            " LEFT OUTER JOIN CreditInfoGroup_lt_work.dbo.findeksa ON CreditInfoGroup_lt_work.dbo.findeksa.imone = ros_NationalCompanyInterfaceView.IDNumber " +
                            " WHERE IDNumber = " + nationalID + " OR CreditInfoID = " + creditInfoID,
                            myOleDbConn);

                    OleDbDataReader reader = myOleDbCommand.ExecuteReader();
                    if (reader.Read()) {
                        if (!reader.IsDBNull(0)) {
                            theReportCompany.UniqueID = reader.GetInt32(0);
                        }
                        if (!reader.IsDBNull(1)) {
                            theReportCompany.NameNative = reader.GetString(1);
                            theReportCompany.NameEN = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2)) {
                            theReportCompany.TypeNative = reader.GetString(2);
                            theReportCompany.TypeEN = reader.GetString(15);
                        }
                        if (!reader.IsDBNull(3)) {
                            theReportCompany.Tel = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4)) {
                            theReportCompany.Fax = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5)) {
                            theReportCompany.JurAddress = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6)) {
                            theReportCompany.Director = reader.GetString(6);
                        }
                        if (!reader.IsDBNull(7)) {
                            theReportCompany.DirectorAddress = reader.GetString(7);
                        }
                        if (!reader.IsDBNull(8)) {
                            theReportCompany.DirectorPhone = reader.GetString(8);
                        }
                        if (!reader.IsDBNull(9)) {
                            theReportCompany.Established = reader.GetString(9);
                        }
                        if (!reader.IsDBNull(10)) {
                            theReportCompany.Registered = reader.GetString(10);
                        }
                        if (!reader.IsDBNull(11)) {
                            theReportCompany.Mobile = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12)) {
                            theReportCompany.OfficeAddress = reader.GetString(12);
                        }
                        if (!reader.IsDBNull(13)) {
                            theReportCompany.Email = reader.GetString(13);
                        }
                        if (!reader.IsDBNull(14)) {
                            theReportCompany.WWW = reader.GetString(14);
                        }
                        if (!reader.IsDBNull(16)) {
                            theReportCompany.LERStatusID = reader.GetInt32(16);
                        }
                        if (!reader.IsDBNull(17)) {
                            theReportCompany.SLERStatusID = reader.GetInt32(17);
                        }
                    }
                } catch (Exception) {
                    Logger.WriteToLog("NationalRegistryViewDALC : " + funcName, true);
                    return null;
                }
            }
            return theReportCompany;
        }

        /// <summary>
        /// This function is taken from Elitas Lithuanian version
        /// </summary>
        /// <param name="StatusID">The claims status id</param>
        /// <param name="nativeCult">Wether we are working with native cult or not</param>
        /// <returns>string with the status name</returns>
        public string GetStatusAsString(int StatusID, bool nativeCult) {
            DataSet ds;
            const string funcName = "GetStatusAsString(int StatusID, bool nativeCult)";

            string query = "select NameNative, NameEN from cpi_StatusCodes where StatusID=" + StatusID;
            try {
                ds = executeSelectStatement(query);
                if (ds.Tables.Count > 0) {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0) {
                        return nativeCult ? ds.Tables[0].Rows[0]["NameNative"].ToString() : ds.Tables[0].Rows[0]["NameEN"].ToString();
                    }
                }
            } catch (Exception) {
                Logger.WriteToLog("NationalRegistryViewDALC" + " : " + funcName, true);
            }
            return "";
        }

        /// <summary>
        /// This function is taken from Elitas Lithuanian version
        /// </summary>
        /// <param name="query">Query string to execute</param>
        /// <returns>DataSet</returns>
        protected static DataSet executeSelectStatement(string query) {
            

            var ds = new DataSet();
            var myAdapter = new OleDbDataAdapter();
            var myOleConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            try {
                myOleConn.Open();
                myAdapter.SelectCommand = new OleDbCommand(query, myOleConn);
                myAdapter.Fill(ds);
                return ds;
            } finally {
                myOleConn.Close();
            }
        }
    }
}