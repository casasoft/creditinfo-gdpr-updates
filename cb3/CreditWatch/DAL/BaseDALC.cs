#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using Logging.BLL;

#endregion

using Cig.Framework.Base.Configuration;

namespace CreditWatch.DAL {
    /// <summary>
    /// Summary description for BaseDALC.
    /// </summary>
    public class BaseDALC {
        /// <summary>
        /// Creates a parameter for an INSERT/UPDATE statement. For EN and Native strings. If both strings have values then they're both
        /// created as parameters. If only one has values then it's text is set, by default, into both fields in the db.
        /// </summary>
        /// <remarks>Creates a parameter for an INSERT/UPDATE statement.</remarks>
        /// <param name="command">OleDbCommand - parameter parent command</param>
        /// <param name="strNameNative">Variable name - both for source column and variable</param>
        /// <param name="strNameEN">Variable name - both for source column and variable</param>
        /// <param name="oValueNative">The entered native value</param>
        /// <param name="oValueEN">The entered EN value</param>
        /// <param name="paramDirect">Parameter direction</param>
        /// <param name="type">Column datatype</param>
        /// <returns></returns>
        public void CreateParameterFromString(
            OleDbCommand command,
            String strNameNative,
            String strNameEN,
            ParameterDirection paramDirect,
            object oValueNative,
            object oValueEN,
            OleDbType type) {
            OleDbParameter paramNative = command.Parameters.Add(strNameNative, type);
            OleDbParameter paramEN = command.Parameters.Add(strNameEN, type);
            paramNative.Direction = paramDirect;
            paramEN.Direction = paramDirect;

            // If both values are null then we'll enter them both as null to the database
            if (oValueNative == null && oValueEN == null) {
                paramNative.Value = DBNull.Value;
                paramEN.Value = DBNull.Value;
                return;
            }

            // If either value (but not both) is null we'd like to convert it into an empty string
            if (oValueNative == null) {
                oValueNative = "";
            }

            if (oValueEN == null) {
                oValueEN = "";
            }

            // Check the stings and switch them if either is an empty string.
            if (oValueNative.ToString().Trim() != string.Empty) {
                paramNative.Value = oValueNative;
            } else if (oValueEN.ToString().Trim() != string.Empty) {
                paramNative.Value = oValueEN;
                // oValueNative = oValueEN;
            }

            if (oValueEN.ToString().Trim() != string.Empty) {
                paramEN.Value = oValueEN;
            } else if (oValueNative.ToString().Trim() != string.Empty) {
                paramEN.Value = oValueNative;
                // oValueEN = oValueNative;
            }
        }

        /// <summary>
        /// Creates a parameter for an INSERT/UPDATE statement. Native param will be transliterated 
        /// into english.  If English param is not empty or null, then the param is not modified.
        /// If Native param is empty or null, then this method will forward the call to 
        /// the CreateParameterFromString method.
        /// </summary>
        /// <remarks>Creates a transliterated parameter for an INSERT/UPDATE statement.</remarks>
        /// <param name="command">OleDbCommand - parameter parent command</param>
        /// <param name="strNameNative">Variable name - both for source column and variable</param>
        /// <param name="strNameEN">Variable name - both for source column and variable</param>
        /// <param name="oValueNative">The entered native value</param>
        /// <param name="oValueEN">The entered EN value</param>
        /// <param name="paramDirect">Parameter direction</param>
        /// <param name="type">Column datatype</param>
        /// <returns></returns>
        public void CreateTransliteratedParameterFromString(
            OleDbCommand command,
            String strNameNative,
            String strNameEN,
            ParameterDirection paramDirect,
            object oValueNative,
            object oValueEN,
            OleDbType type) {
            string useTransliterate = CigConfig.Configure("lookupsettings.UseTransliterateTool");
            if (useTransliterate == null || useTransliterate.Trim().ToLower() != "true") {
                CreateParameterFromString(command, strNameNative, strNameEN, paramDirect, oValueNative, oValueEN, type);
                return;
            } else {
                // If native value is null or empty then we will forward the call to the CreateParamFromString method
                if (oValueNative == null || oValueNative.ToString().Trim() == string.Empty) {
                    CreateParameterFromString(
                        command, strNameNative, strNameEN, paramDirect, oValueNative, oValueEN, type);
                    return;
                }

                if (oValueEN == null) {
                    oValueEN = "";
                }

                OleDbParameter paramNative = command.Parameters.Add(strNameNative, type);
                OleDbParameter paramEN = command.Parameters.Add(strNameEN, type);
                paramNative.Direction = paramDirect;
                paramEN.Direction = paramDirect;

                // We now know that the native value is neither null or empty - set it
                paramNative.Value = oValueNative;

                //If english value is empty - then transliterate
                if (oValueEN.ToString().Trim() == string.Empty) {
                    string translituratedText = Util.Transliterate(oValueNative.ToString());
                    if (translituratedText != null) {
                        paramEN.Value = translituratedText;
                    } else {
                        CreateParameterFromString(
                            command, strNameNative, strNameEN, paramDirect, oValueNative, oValueEN, type);
                        return;
                    }
                } else {
                    paramEN.Value = oValueEN;
                }
            }
        }
    }
}