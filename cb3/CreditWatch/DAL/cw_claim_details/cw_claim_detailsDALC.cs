#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using DataProtection;
using cb3;

#endregion

namespace CreditWatch.DAL.cw_claim_details {
    /// <summary>
    /// Summary description for cw_claim_detailsDALC.
    /// </summary>
    public class cw_claim_detailsDALC {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CreditInfoID"></param>
        /// <returns>DataSet</returns>
        public DataSet GetNativeSearchDetailsList(int CreditInfoID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand("select * from np_claim_details_native (?)", myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("creditinfoid", OleDbType.Integer)
                                      {
                                          Value = CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CreditInfoID"></param>
        /// <returns>DataSet</returns>
        public DataSet GetENSearchDetailsList(int CreditInfoID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand("select * from np_claim_details_en (?)", myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("creditinfoid", OleDbType.Integer)
                                      {
                                          Value = CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        public int CheckForOldCompanyNames(int CreditInfoID) {
            
            using (var myOleDbConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var myOleDbCommand =
                    new OleDbCommand(
                        "SELECT COUNT (CreditInfoID) FROM np_CompaniesHouse WHERE CreditInfoID = ?", myOleDbConnection);
                myOleDbCommand.Connection.Open();

                // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                var myParameter = new OleDbParameter("creditinfoid", OleDbType.Integer)
                                  {
                                      Value = CreditInfoID,
                                      Direction = ParameterDirection.Input
                                  };
                myOleDbCommand.Parameters.Add(myParameter);

                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return !reader.IsDBNull(0) ? reader.GetInt32(0) : 0;
                }

                reader.Close();
                return 0;
            }
        }

        public string GetTheNewestNameForACompany(int CreditInfoID) {
            
            using (var myOleDbConnection = new OleDbConnection(DatabaseHelper.ConnectionString())) {
                var myOleDbCommand =
                    new OleDbCommand(
                        "SELECT Org_name FROM np_CompaniesHouse WHERE CreditInfoID = ? AND Org_name_status_code = 'ACR'",
                        myOleDbConnection);
                myOleDbCommand.Connection.Open();

                // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                var myParameter = new OleDbParameter("creditinfoid", OleDbType.Integer)
                                  {
                                      Value = CreditInfoID,
                                      Direction = ParameterDirection.Input
                                  };
                myOleDbCommand.Parameters.Add(myParameter);

                var reader = myOleDbCommand.ExecuteReader();
                if (reader.Read()) {
                    return !reader.IsDBNull(0) ? reader.GetString(0) : "";
                }

                reader.Close();
                return "";
            }
        }

        public DataSet GetOldNamesForCompanyAsDataSet(int CreditInfoID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            var myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand =
                        new OleDbCommand(
                            "SELECT Org_name, Org_name_status_code, Reg_date, CompanyAddress, Postcode, City FROM np_CompaniesHouse WHERE CreditInfoID = ? ORDER BY Reg_date",
                            myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        public int GetLogCountForLastXDaysPerCreditInfoUser(int IntervalAsDays, int CreditInfoID) {
            int myCount = -1;
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp COUNT skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                            "select count(*) from dbo.np_Usage where created BETWEEN getdate() - ? and getdate() AND query = ?",
                            myConnection) {Transaction = myTransaction};

                    // Fyrir IntervalAsDays
                    var myParameter = new OleDbParameter("IntervalAsDays", OleDbType.Integer)
                                      {
                                          Value = IntervalAsDays,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir CreditInfoID
                    myParameter = new OleDbParameter("CreditInfoID", OleDbType.WChar)
                                  {
                                      Value = CreditInfoID.ToString(),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    object objCount = myCommand.ExecuteScalar();
                    myCount = (int) objCount;

                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return myCount;
                }
                return myCount;
            }
        }

        public int GetLogCountForLastXDaysPerCreditInfoUserExcludingEmployees(int IntervalAsDays, int CreditInfoID) {
            int myCount = -1;
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                var myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp COUNT skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                         //   "select count(*) from dbo.np_Usage where created BETWEEN getdate() - ? and getdate() AND query = ? and query_type in('41','42','43','44','45') and UserId in (select Id from au_users where UserType = 'Regular User')",
                           "select count(*) from dbo.np_Usage a, au_users b where a.created BETWEEN getdate() - ? and getdate() AND a.query = ? and a.query_type in('41','42','43','44','45') and a.UserId = b.id  and b.UserType = 'Regular User'",

                            myConnection) {Transaction = myTransaction};

                    // Fyrir IntervalAsDays
                    var myParameter = new OleDbParameter("IntervalAsDays", OleDbType.Integer)
                                      {
                                          Value = IntervalAsDays,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir CreditInfoID
                    myParameter = new OleDbParameter("CreditInfoID", OleDbType.WChar)
                                  {
                                      Value = CreditInfoID.ToString(),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    object objCount = myCommand.ExecuteScalar();
                    myCount = (int) objCount;

                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return myCount;
                }
                return myCount;
            }
        }

        /// <summary>
        /// This function is taken from Elitas Lithunaian version and programmed by her
        /// </summary>
        /// <param name="CreditInfoID">The person internal unique system id</param>
        /// <returns>Dataset</returns>
        public DataSet GetPersonDebtsDetailsList(int CreditInfoID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand = new OleDbCommand("select * from np_claim_details_native3 (?)", myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("creditinfoid", OleDbType.Integer)
                                      {
                                          Value = CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }
    }
}