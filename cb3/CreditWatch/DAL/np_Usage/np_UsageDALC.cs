#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using CreditWatch.BLL.np_Usage;
using DataProtection;
using cb3;
using cb3.Audit;

#endregion

namespace CreditWatch.DAL.np_Usage {
    /// <summary>
    /// Loggun af vakt vegna flettinga � vanskilaskr�
    /// </summary>
    public class np_UsageDALC {

        /// <summary>
        ///  Skrifar tilvik af UsageBLLC ni�ur � grunn. Tilvik inniheldur allar uppl�singar sem loggunin
        ///  �arf � a� halda. .
        /// </summary>
        public bool AddNewUsageLog(np_UsageBLLC myLog) {
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                myConnection.Open();
                OleDbTransaction myTransaction = myConnection.BeginTransaction();

                try {
                    // Setur upp INSERT skipun fyrir gagnagrunninn me� params...
                    myCommand =
                        new OleDbCommand(
                            "INSERT INTO np_Usage (CreditInfoID, query_type, query, result_count, ip, UserID) VALUES(?,?,?,?,?,?)",
                            myConnection) {Transaction = myTransaction};

                    // Fyrir CreditInfoID
                    var myParameter = new OleDbParameter("CreditInfoID", OleDbType.Integer)
                                      {
                                          Value = myLog.CreditInfoID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir query_type
                    myParameter = new OleDbParameter("query_type", OleDbType.Integer)
                                  {
                                      Value = myLog.QueryType,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir query
                    myParameter = new OleDbParameter("query", OleDbType.WChar, 50)
                                  {
                                      Value = myLog.Query,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir result_count
                    myParameter = new OleDbParameter("result_count", OleDbType.Integer)
                                  {
                                      Value = myLog.ResultCount,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir ip
                    myParameter = new OleDbParameter("Created", OleDbType.VarChar, 15)
                                  {
                                      Value = myLog.IP,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // Fyrir UserID
                    myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                  {
                                      Value = myLog.UserID,
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    new AuditFactory(myCommand).PerformAuditProcess(); myCommand.ExecuteNonQuery();
                    myTransaction.Commit();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    myTransaction.Rollback();
                    return false;
                }
                return true;
            }
        }
    }
}