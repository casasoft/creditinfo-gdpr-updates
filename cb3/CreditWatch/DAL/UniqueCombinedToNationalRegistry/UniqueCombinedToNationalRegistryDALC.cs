#region

using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using CreditWatch.BLL;
using DataProtection;
using cb3;

#endregion

namespace CreditWatch.DAL.UniqueCombinedToNationalRegistry {
    /// <summary>
    /// Summary description for UniqueCombinedToNationalRegistryDALC.
    /// </summary>
    public class UniqueCombinedToNationalRegistryDALC {

        /// <summary>
        /// Skilar lista af �eim sem hafa veri� skr��ir af vakt af �kve�num a�ila. Tekur �v� inn CreditInfoID og IsCompany.
        /// Skilar � anna� hvort lista af l�ga�ilum e�a lista af einstaklingum.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany"></param>
        /// <returns></returns>
        public DataSet GetUniqueOnWatchListWithNameAndAddressForUserAsDataset(int UserID, bool IsCompany) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            string myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand =
                        new OleDbCommand(
                            "SELECT  * FROM cw_UniqueCombinedToNationalRegistry watch WHERE watch.UserID = ? AND watch.IsCompany = ?",
                            myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // IsCompany sett �annig a� f�rslur s�u s�ttar m.v. �a�, �.e.a.s. anna� hvort l�ga�ilar e�a einstaklingar...
                    myParameter = new OleDbParameter("IsCompany", OleDbType.Char, 10)
                                  {
                                      Value = IsCompany.ToString(),
                                      Direction = ParameterDirection.Input
                                  };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        /// <summary>
        /// Skilar lista af �eim sem hafa veri� skr��ir af vakt af �kve�num a�ila. Tekur �v� inn CreditInfoID og IsCompany.
        /// Skilar � anna� hvort lista af l�ga�ilum e�a lista af einstaklingum.
        /// </summary>
        /// <param name="UserID">Unique identifier for user.</param>
        /// <returns>DataSet</returns>
        public DataSet GetUniqueOnWatchListWithNameAndAddressForUserAsDataset(int UserID) {
            // �tb� t�mt dataset
            var mySet = new DataSet();
            OleDbCommand myCommand;
            var myConnectionString = DatabaseHelper.ConnectionString();

            using (var myConnection = new OleDbConnection(myConnectionString)) {
                try {
                    myConnection.Open();
                    myCommand =
                        new OleDbCommand(
                            "SELECT * FROM cw_UniqueCombinedToNationalRegistry watch WHERE watch.UserID = ?",
                            myConnection);

                    // CreditInfoID sett �annig a� f�rslur s�u s�ttar m.v. �a�...
                    var myParameter = new OleDbParameter("UserID", OleDbType.Integer)
                                      {
                                          Value = UserID,
                                          Direction = ParameterDirection.Input
                                      };
                    myCommand.Parameters.Add(myParameter);

                    // �tb� n�jan adapter sem tekur vi� SQL skipuninni minni me� params...
                    var myAdapter = new OleDbDataAdapter {SelectCommand = myCommand};

                    // Skila ni�urst��um � dataset...
                    myAdapter.Fill(mySet);
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(
                        "GetUniqueOnWatchListWithNameAndAddressForUserAsDataset(int UserID) - Error:" + e.Message, true);
                    return null;
                }
            }
        }
    }
}