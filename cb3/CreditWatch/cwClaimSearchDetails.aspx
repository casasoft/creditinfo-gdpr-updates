<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="../new_user_controls/FoOptions.ascx" %>
<%@ Page language="c#" Codebehind="cwClaimSearchDetails.aspx.cs" AutoEventWireup="false" Inherits="CreditWatch.cwClaimSearchDetails" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>cwClaimSearchDetails</title>
		<LINK href="../css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="popup.js"></script>
	</HEAD>
	<body style="BACKGROUND-IMAGE: url(../img/mainback.gif)" leftMargin="0" rightMargin="0"
		ms_positioning="GridLayout">
		<form id="SearchDetails" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
								<td>
									<table cellSpacing="0" cellPadding="0" width="640" align="center" bgColor="#951e16" border="0">
										<tr>
											<td bgColor="#951e16"><span style="WIDTH: 3px"></span></td>
											<td bgColor="#951e16"><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
											<td align="right" bgColor="#951e16"><uc1:language id="Language2" runat="server"></uc1:language></td>
										</tr>
									</table>
								</td>
								<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="640" align="center" bgColor="white" border="0">
							<tr>
								<td>
									<p>
										<table id="tbDefault" cellSpacing="0" cellPadding="0" width="100%" align="center">
											<tr>
												<td>
													<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
														<tr>
															<td><uc1:userinfo id="UserInfo1" runat="server"></uc1:userinfo></td>
															<td align="right"><uc1:fooptions id="FoOptions1" runat="server"></uc1:fooptions></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
														<tr>
															<td class="pageheader" width="97%"><asp:label id="lblClaimDetails" runat="server">[lt] - Claim Details</asp:label></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<table id="tbContent" cellSpacing="5" cellPadding="5" width="97%" align="center" border="0">
														<tr>
															<td><asp:button id="btnPrint" runat="server" cssclass="button"></asp:button></td>
														</tr>
														<tr>
															<td align="center">
																<table class="list" id="Table1" cellSpacing="0" cellPadding="0" width="600" border="0">
																	<tr>
																		<td style="WIDTH: 123px; HEIGHT: 13px"><strong><asp:label id="lblNameLabel" runat="server"></asp:label></strong></td>
																		<td style="WIDTH: 343px; HEIGHT: 13px"><asp:label id="lblName" runat="server"></asp:label></td>
																		<td style="HEIGHT: 13px" align="right"><asp:label id="ldlToday" runat="server" font-bold="True"></asp:label></td>
																	</tr>
																	<tr>
																		<td style="WIDTH: 123px"><strong><asp:label id="lblAddressLabel" runat="server"></asp:label></strong></td>
																		<td style="WIDTH: 343px"><asp:label id="lblAddress" runat="server"></asp:label></td>
																		<td align="right"><asp:label id="lblTimeStamp" runat="server"></asp:label></td>
																	</tr>
																	<tr>
																		<td style="WIDTH: 123px; HEIGHT: 13px"><strong><asp:label id="lblCityLabel" runat="server"></asp:label></strong></td>
																		<td style="WIDTH: 343px; HEIGHT: 13px"><asp:label id="lblCity" runat="server"></asp:label></td>
																		<td style="HEIGHT: 13px"></td>
																	</tr>
																	<tr>
																		<td style="WIDTH: 123px"><asp:label id="lblCompStLabel" runat="server" font-bold="True"></asp:label><strong></strong></td>
																		<td style="WIDTH: 343px"><asp:label id="lblCompanyStatus" runat="server"></asp:label></td>
																		<td></td>
																	</tr>
																	<tr>
																		<td style="WIDTH: 123px"><asp:label id="lblNationalIDlbl" runat="server" font-bold="True"></asp:label></td>
																		<td style="WIDTH: 343px"><asp:label id="lblNationalID" runat="server"></asp:label></td>
																		<td></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td style="HEIGHT: 33px" align="center"><asp:label id="lblNoClaimRegistered" runat="server" forecolor="Red" visible="False">No claim registered for the subject</asp:label></td>
														</tr>
														<tr>
															<td class="sectionheader" id="headerBankruptcy" style="HEIGHT: 33px" align="left" runat="server"><asp:label id="lblDeptsHeader" runat="server" visible="False"></asp:label>Bankruptcy</td>
														</tr>
														<tr id="bankruptcyELine" runat="server">
															<td align="center"></td>
														</tr>
														<tr id="bankruptcyRepeaterRow" runat="server">
															<td align="left"><asp:repeater id="repeatBankruptcy" runat="server">
																	<itemtemplate>
																		<table runat="server" id="BankruptcyListing" class="list" border="0" cellspacing="0" cellpadding="3"
																			width="100%">
																			<tr>
																				<td rowspan="3"></td>
																				<td colspan="2" class="listhead">
																					<b>
																						<%#rm.GetString("lbCaseID",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "case_nr")%>
																				</td>
																				<td colspan="2" class="listhead">
																					<b>
																						<%#rm.GetString("lbLtReference",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
																					<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbCaseType",ci)%></b><br>
																					<!-- Athuga hva�a streng � a� birta... -->
																					<%# GetCorrectTypeString(DataBinder.Eval(Container.DataItem, "claim_type"),DataBinder.Eval(Container.DataItem, "case_decision_type")) %>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbGazetteYear",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "gazette_year")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbGazettePage",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "gazette_page")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbInfoOrigin",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
																				</td>
																			</tr>
																			<tr>
																				<td></td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, ParseAmount("amount"))%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbAgentID",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "agent")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "comment")%>
																				</td>
																			</tr>
																			<tr>
																				<td><br>
																				</td>
																			</tr>
																		</table>
																	</itemtemplate>
																</asp:repeater></td>
														</tr>
														<tr>
															<td class="sectionheader" id="headerBouncedChecks" style="HEIGHT: 33px" align="left"
																runat="server">Bounched checks</td>
														</tr>
														<tr id="bouncedChecksELine" runat="server">
															<td align="center"></td>
														</tr>
														<tr id="bouncedChecksRepeaterRow" runat="server">
															<td align="left"><asp:repeater id="repeatBouncedChecks" runat="server">
																	<itemtemplate>
																		<table runat="server" id="bouncedChecksListing" class="list" border="0" cellspacing="0"
																			cellpadding="3" width="100%">
																			<tr>
																				<td rowspan="3"></td>
																				<td class="listhead">
																					<b>
																						<%#rm.GetString("lbCaseID",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "case_nr")%>
																				</td>
																				<td class="listhead" colspan="2">
																					<b>
																						<%#rm.GetString("lbLtReference",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
																					<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbCaseType",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "amount")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbAgentID",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "agent")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbBank",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "cheque_issue_bank")%>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="3" class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "comment")%>
																				</td>
																			</tr>
																			<tr>
																				<td><br>
																				</td>
																			</tr>
																		</table>
																	</itemtemplate>
																</asp:repeater></td>
														</tr>
														<tr>
															<td class="sectionheader" id="headerCasesAgainstCompanies" style="HEIGHT: 33px" align="left"
																runat="server">Cases against companies</td>
														</tr>
														<tr id="casesAgainstCompaniesELine" runat="server">
															<td align="center"></td>
														</tr>
														<tr id="casesAgainstCompaniesRepeaterRow" runat="server">
															<td align="left"><asp:repeater id="repeatCasesAgainstCompanies" runat="server">
																	<itemtemplate>
																		<table runat="server" id="CasesAgainstCompaniesListing" class="list" border="0" cellspacing="0"
																			cellpadding="3" width="100%">
																			<tr>
																				<td rowspan="4"></td>
																				<td class="listhead">
																					<b>
																						<%#rm.GetString("lbCaseID",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "case_nr")%>
																				</td>
																				<td colspan="2" class="listhead">
																					<b>
																						<%#rm.GetString("lbLtReference",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
																					<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbCaseType",ci)%></b><br>
																					<%# GetCorrectTypeString(DataBinder.Eval(Container.DataItem, "claim_type"),DataBinder.Eval(Container.DataItem, "case_decision_type")) %>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbGazetteYear",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "gazette_year")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbGazettePage",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "gazette_page")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbInfoOrigin",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="3" class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "comment")%>
																				</td>
																			</tr>
																			<tr>
																				<td><br>
																				</td>
																			</tr>
																		</table>
																	</itemtemplate>
																</asp:repeater></td>
														</tr>
														<tr>
															<td class="sectionheader" id="headerOther" style="HEIGHT: 33px" align="left" runat="server">Misc. 
																cases</td>
														</tr>
														<tr id="otherELine" runat="server">
															<td align="center"></td>
														</tr>
														<tr id="otherRepeaterRow" runat="server">
															<td align="left"><asp:repeater id="repeatOther" runat="server">
																	<itemtemplate>
																		<table runat="server" id="OtherCasesListing" class="list" border="0" cellspacing="0" cellpadding="3"
																			width="100%">
																			<tr>
																				<td rowspan="3"></td>
																				<td colspan="2" class="listhead">
																					<b>
																						<%#rm.GetString("lbCaseID",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "case_nr")%>
																				</td>
																				<td colspan="2" class="listhead">
																					<b>
																						<%#rm.GetString("lbLtReference",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
																					<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbCaseType",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "amount")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbAgentID",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "agent")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "comment")%>
																				</td>
																			</tr>
																			<tr>
																				<td><br>
																				</td>
																			</tr>
																		</table>
																	</itemtemplate>
																</asp:repeater></td>
														</tr>
														<tr>
															<td class="sectionheader" id="headCzDDD" style="HEIGHT: 33px" align="left" runat="server"><asp:label id="lblClaims" runat="server" cssclass="sectionheader">Claims</asp:label></td>
														</tr>
														<tr id="czDDDELine" runat="server">
															<td align="center"></td>
														</tr>
														<tr id="czDDDRepeaterRow" runat="server">
															<td align="left"><asp:repeater id="repeatCzDDD" runat="server">
																	<itemtemplate>
																		<table runat="server" id="CzListing" class="list" border="0" cellspacing="0" cellpadding="3"
																			width="100%">
																			<tr>
																				<td rowspan="3"></td>
																				<td colspan="2" class="listhead">
																					<b>
																						<%#rm.GetString("lbCaseID",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "comment")%>
																				</td>
																				<td colspan="2" class="listhead">
																					<b>
																						<%#rm.GetString("lbLtReference",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
																					<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
																				</td>
																				<td class="dark-row" valign="top"><b></b><br>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbCZClaimAmount",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "amount","{0:N}")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "comment")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("txtInformationSource",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
																				</td>
																			</tr>
																			<tr>
																				<td><br>
																				</td>
																			</tr>
																		</table>
																	</itemtemplate>
																</asp:repeater></td>
														</tr>
														<tr>
															<td class="sectionheader" id="headLtDDD" style="HEIGHT: 33px" align="left" runat="server"><asp:label id="lblLtClaims" runat="server" cssclass="sectionheader">Claims LT</asp:label></td>
														</tr>
														<tr id="ltDDDELine" runat="server">
															<td align="center"></td>
														</tr>
														<tr id="ltDDDRepeaterRow" runat="server">
															<td align="left"><asp:repeater id="repeatLtDDD" runat="server">
																	<itemtemplate>
																		<table runat="server" id="LtListing" class="list" border="0" cellspacing="0" cellpadding="3"
																			width="100%">
																			<tr>
																				<td rowspan="3"></td>
																				<td colspan="2" class="listhead">
																					<b>
																						<%#rm.GetString("lbCaseID",ci)%>
																					</b>
																					<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
																				</td>
																				<td colspan="2" class="listhead">
																					<b>
																						<%#rm.GetString("lbDirector",ci)%>
																						: </b>
																					<%#DataBinder.Eval(Container.DataItem, "pname")%>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimType",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
																					<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "amount")%>
																				</td>
																				<td class="dark-row" valign="top"><b></b><br>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerCode",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "creditor_code","{0:N}")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "creditor")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("txtInformationSource",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
																				</td>
																			</tr>
																			<tr>
																				<td><br>
																				</td>
																			</tr>
																		</table>
																	</itemtemplate>
																</asp:repeater></td>
														</tr>
														<tr id="ltPersonsDDDRepeaterRow" runat="server">
															<td align="left"><asp:repeater id="repeatLtPersonDDD" runat="server">
																	<itemtemplate>
																		<table runat="server" id="LtPersonListing" class="list" border="0" cellspacing="0" cellpadding="3"
																			width="100%">
																			<tr>
																				<td rowspan="4"></td>
																				<td class="listhead">
																					<b>	<%#rm.GetString("lbCaseID",ci)%>: </b>
																						<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
																				</td>
																				<td class="listhead">
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbPersonCode",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "code")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbPersonName",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "pname")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbPersonAddress",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "address")%>
																				</td>
																				<td class="dark-row" valign="top"><b></b><br>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimType",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
																					<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "amount")%>
																				</td>
																				<td class="dark-row" valign="top"><b></b><br>
																				</td>
																			</tr>
																			<tr>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerCode",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "creditor_code","{0:N}")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "creditor")%>
																				</td>
																				<td class="dark-row" valign="top"><b><%#rm.GetString("txtInformationSource",ci)%></b><br>
																					<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
																				</td>
																			</tr>
																			<tr>
																				<td><br>
																				</td>
																			</tr>
																		</table>
																	</itemtemplate>
																</asp:repeater></td>
														</tr>
														<tr>
															<td class="sectionheader" style="HEIGHT: 33px" align="left"><asp:label id="lblRecent" runat="server"></asp:label></td>
														</tr>
														<tr>
															<td align="left"><asp:label id="lblSixMonths" runat="server" cssclass="list"></asp:label></td>
														</tr>
														<tr>
															<td style="HEIGHT: 15px" align="left"><asp:label id="lblThreeMonths" runat="server" cssclass="list"></asp:label></td>
														</tr>
														<tr>
															<td align="left"><asp:label id="lblOneMonth" runat="server" cssclass="list"></asp:label></td>
														</tr>
														<tr>
															<td style="HEIGHT: 55px" align="left"><A id="linkBack" href="javascript:history.back()" runat="server">Back</A></td>
														</tr>
														<tr>
															<td style="HEIGHT: 55px" align="left"><asp:label id="lblDisclaimer" runat="server"></asp:label></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgColor="transparent" height="6"></td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
