function CheckSurname(txtSurName, chkbxIsCompany, theMessage)
{
	if((!chkbxIsCompany.checked) && (txtSurName.value.length == 0))
	{
		alert(theMessage)
		return false
	} 

	return true
}

function CheckID(valur, checkbx, nameList, filePath, theMessage)
{
	var success = false

	if(checkbx.checked)
	{
		if(valur.value.length > 0)
		{	
			success = CheckCompanyID(valur.value, theMessage)
			if(success == false)
			{
				return false
			}
		}
		
		if(nameList.value.length > 0)
		{
			success = CheckCompanyArrayID(nameList.value, theMessage)
			if(success == false)
			{
				return false
			}
		}
	}
	else
	{
		if(valur.value.length > 0)
		{
			success = CheckIndividualID(valur.value, theMessage)
			if(success == false)
			{
				return false
			}
		}

		if(nameList.value.length > 0)
		{		
			success = CheckIndividualArrayID(nameList.value, theMessage)
			if(success == false)
			{
				return false
			}
		}
	}
	
	if(filePath.value.length > 0)
	{
		success = true
	}
	
	return success
}

function CheckCompanyArrayID(nameList, theMessage)
{
	var regexp = /\D+/

	var newArray = nameList.split(regexp)
	
	var success = false

	for(var i=0; i < newArray.length; i++)
	{
		var id = newArray[i]
		
		success = CheckCompanyID(id, theMessage)
		if(success == false)
		{
			return false
		}
	}
	
	return true
}

function CheckIndividualArrayID(nameList, theMessage)
{
	var regexp = /\D+/
	
	var newArray = nameList.split(regexp)
	
	var success = false

	for(var i=0; i < newArray.length; i++)
	{
		var id = newArray[i]
		
		success = CheckIndividualID(id, theMessage)
		if(success == false)
		{
			return false
		}
	}
	
	return true
}

function CheckCompanyID(valur, theMessage)
{
	if(isNaN(valur))
	{
		alert(theMessage + " " + valur)
		return false
	}
	else
	{
		if((valur > 9999999) && (valur < 100000000))
		{
			return true
		}
		else
		{
			alert(theMessage + " " + valur)
			return false
		}
	}
}

function CheckIndividualID(valur, theMessage)
{
	if(isNaN(valur))
	{
		alert(theMessage + " " + valur)
		return false
	}
	else
	{
		if((valur > 9999999) && (valur < 100000000))
		{
			return true
		}
		else
		{
			alert(theMessage + " " + valur)
			return false
		}
	}
}

function CheckCyprusID(value, checkbx, nameList, filePath, theMessage)
{
	var success = false
	
	if(checkbx.checked) // then company
	{
		if(valur.value.length > 0)
		{
			success = CheckCyprusCompanyID(valur.value, theMessage)
			if(success == false)
			{
				return false
			}
		}
		
		if(nameList.value.length > 0)
		{
			success = CheckCyprusCompanyArrayID(nameList.value, theMessage)
			if(success == false)
			{
				return false
			}
		}
	}
	else // individual
	{
		if(valur.value.length > 0)
		{
			success = CheckCyprusIndividualID(valur.value, theMessage)
			if(success == false)
			{
				return false
			}
		}

		if(nameList.value.length > 0)
		{		
			success = CheckCyprusIndividualArrayID(nameList.value, theMessage)
			if(success == false)
			{
				return false
			}
		}
	}
	
	if(filePath.value.length > 0)
	{
		success = true
	}
	
	return success
}
function CheckCyprusCompanyID(valur, theMessage)
{
	
	if((valur.length < 8) && (isNaN(valur.substring[0,1])))
	{
		return true
	}
	else
	{
		alert(theMessage + " " + valur)
		return false
	}
}
function CheckCyprusIndividualID(valur, theMessage)
{
	if(isNaN(valur))
	{
		alert(theMessage + " " + valur)
		return false
	}
	else
	{
		if(valur.length < 6)
		{
			return true
		}
		else
		{
			alert(theMessage + " " + valur)
			return false
		}
	}
}
function CheckCyprusIndividualArrayID(nameList, theMessage)
{
	var regexp = /\D+/
	
	var newArray = nameList.split(regexp)
	
	var success = false

	for(var i=0; i < newArray.length; i++)
	{
		var id = newArray[i]
		
		success = CheckCyprusIndividualID(id, theMessage)
		if(success == false)
		{
			return false
		}
	}
	
	return true
}
function CheckCyprusCompanyArrayID(nameList, theMessage)
{
	var regexp = /\D+/
	
	var newArray = nameList.split(regexp)
	
	var success = false

	for(var i=0; i < newArray.length; i++)
	{
		var id = newArray[i]
		
		success = CheckCyprusCompanyID(id, theMessage)
		if(success == false)
		{
			return false
		}
	}
	
	return true
}

function CheckMaltaID(valur, checkbx, nameList, filePath, theMessage)
{
	var success = false
	
	if(checkbx.checked) // then company
	{
		if(valur.value.length > 0)
		{
			success = CheckMaltaCompanyID(valur.value, theMessage)
			if(success == false)
			{
				return false
			}
		}
		
		if(nameList.value.length > 0)
		{
			success = CheckMaltaCompanyArrayID(nameList.value, theMessage)
			if(success == false)
			{
				return false
			}
		}
	}
	else // individual
	{
		if(valur.value.length > 0)
		{
			success = CheckMaltaIndividualID(valur.value, theMessage)
			if(success == false)
			{
				return false
			}
		}

		if(nameList.value.length > 0)
		{		
			success = CheckMaltaIndividualArrayID(nameList.value, theMessage)
			if(success == false)
			{
				return false
			}
		}
	}
	
	if(filePath.value.length > 0)
	{
		success = true
	}
	
	return success
}
function CheckMaltaCompanyID(valur, theMessage)
{
	if(valur.length < 20)
	{
		return true
	}
	else
	{
		alert(theMessage+ " " + valur)
		return false
	}
}
function CheckMaltaIndividualID(valur, theMessage)
{
	/*if(isNaN(valur))
	{
		alert(theMessage + " " + valur)
		return false
	}
	else
	{*/
		if(valur.length < 20)
		{
			return true
		}
		else
		{
			alert(theMessage + " " + valur)
			return false
		}
	//}
}
function CheckMaltaIndividualArrayID(nameList, theMessage)
{
	var regexp = /\D+/
	
	var newArray = nameList.split(regexp)
	
	var success = false

	for(var i=0; i < newArray.length; i++)
	{
		var id = newArray[i]
		
		success = CheckMaltaIndividualID(id, theMessage)
		if(success == false)
		{
			return false
		}
	}
	
	return true
}
function CheckMaltaCompanyArrayID(nameList, theMessage)
{
	var regexp = /\D+/
	
	var newArray = nameList.split(regexp)
	
	var success = false

	for(var i=0; i < newArray.length; i++)
	{
		var id = newArray[i]
		
		success = CheckMaltaCompanyID(id, theMessage)
		if(success == false)
		{
			return false
		}
	}
	
	return true
}
/*
// T�kk � �v� hvort kennitala s� � leyfilegu formi...
			if (this.chkIDComp.Checked)
			{
				// Fyrirt�ki - Max lengd 7 stafir og fyrsti stafur ver�ur a� vera b�kstafur.
				if (this.txtID.Text.Length <= 7 && char.IsLetter(this.txtID.Text[0]))
				{
					for (int i = 1; i < this.txtID.Text.Length; i++)
					{
						if (char.IsLetter(this.txtID.Text[i]))
						{
							isAOK = false;
							break;
						}
					}
				}
				else
				{
					isAOK = false;
				}
			}
			else
			{
				// Einstaklingar - Max lengd 6 stafir og ver�a allir a� vera t�lustafir.
				if (this.txtID.Text.Length <= 6)
				{
					for (int i = 0; i < this.txtID.Text.Length; i++)
					{
						if (char.IsLetter(this.txtID.Text[i]))
						{
							isAOK = false;
							break;
						}
					}
				}
				else
				{
					isAOK = false;
				}
			}

			if (!isAOK)
			{
				string myScript = "<script language=Javascript>alert('Illigal format for ID / Registration number');</script>"; 
				Page.RegisterClientScriptBlock("alert", myScript); 
				return;
			}

*/

/*
	START OF GEORGIA CHECK
*/

function CheckGeorgiaID(valur, checkbx, nameList, filePath, theMessage)
{
		if(valur.value.length > 0)
		{
			return true	
		}
		
		if(nameList.value.length > 0)
		{
			return true
		}
		
		if(filePath.value.length > 0)
		{
			return true
		}
		
		return false
}