#region

using System;

#endregion

namespace CreditWatch.BLL.UniqueCombinedToNationalRegistry {
    /// <summary>
    /// Summary description for UniqueCombinedToNationalRegistryBLLC.
    /// </summary>
    public class UniqueCombinedToNationalRegistryBLLC {
        public int CreditInfoID { get; set; }
        public int WatchID { get; set; }
        public string UniqueID { get; set; }
        public string IsCompany { get; set; }
        public string Sent { get; set; }
        public DateTime Created { get; set; }
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}