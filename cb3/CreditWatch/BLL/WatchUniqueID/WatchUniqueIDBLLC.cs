#region

using System;

#endregion

namespace CreditWatch.BLL.WatchUniqueID {
    /// <summary>
    /// Gangagrind fyrir cw_WatchUniqueID t�fluna. Notu� til �ess a� a� setja og s�kja g�gn sem m� svo
    /// skrifa ni�ur � grunninn.
    /// </summary>
    public class WatchUniqueIDBLLC {
        public int CreditInfoID { get; set; }
        public int WatchID { get; set; }
        public string UniqueID { get; set; }
        public string IsCompany { get; set; }
        public string Sent { get; set; }
        public DateTime Created { get; set; }
        public int UserID { get; set; }
    }
}