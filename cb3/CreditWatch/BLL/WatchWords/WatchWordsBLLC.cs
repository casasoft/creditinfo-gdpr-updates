#region

using System;

#endregion

namespace CreditWatch.BLL.WatchWords {
    /// <summary>
    /// Gangagrind fyrir cw_WatchWords t�fluna. Notu� til �ess a� a� setja og s�kja g�gn sem m� svo
    /// skrifa ni�ur � grunninn.
    /// </summary>
    public class WatchWordsBLLC {
        public int CreditInfoID { get; set; }
        public int WatchID { get; set; }
        public string FirstNameEN { get; set; }
        public string FirstNameNative { get; set; }
        public string SurNameEN { get; set; }
        public string SurNameNative { get; set; }
        public string AddressEN { get; set; }
        public string AddressNative { get; set; }
        public string TownEN { get; set; }
        public string TownNative { get; set; }
        public string IsCompany { get; set; }
        public string Sent { get; set; }
        public DateTime Created { get; set; }
        public int UserID { get; set; }
    }
}