#region

using System;

#endregion

namespace CreditWatch.BLL.np_Usage {
    /// <summary>
    /// Summary description for np_UsageBLLC.
    /// </summary>
    public class np_UsageBLLC {
        public int ID { get; set; }
        public int CreditInfoID { get; set; }
        public int QueryType { get; set; }
        public string Query { get; set; }
        public int ResultCount { get; set; }
        public string IP { get; set; }
        public DateTime Created { get; set; }
        public int UserID { get; set; }
    }
}