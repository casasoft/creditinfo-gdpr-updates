#region

using System;
using System.Configuration;
using System.Data;
using CreditWatch.BLL.NationalRegistryView;
using CreditWatch.BLL.np_Usage;
using CreditWatch.BLL.WatchUniqueID;
using CreditWatch.BLL.WatchWords;
using CreditWatch.DAL.cw_claim_details;
using CreditWatch.DAL.cw_make_monitlist_puser;
using CreditWatch.DAL.cw_search_claims;
using CreditWatch.DAL.NationalRegistryView;
using CreditWatch.DAL.np_Companys;
using CreditWatch.DAL.np_Employee;
using CreditWatch.DAL.np_Usage;
using CreditWatch.DAL.UniqueCombinedToNationalRegistry;
using CreditWatch.DAL.WatchUniqueID;
using CreditWatch.DAL.WatchWords;
using UserAdmin.BLL.CIUsers;

#endregion

using Cig.Framework.Base.Configuration;

namespace CreditWatch.BLL {
    /// <summary>
    /// cwFactory klasinn s�r um �ll samskipti � milli vi�m�ts og BLL lags.
    /// </summary>
    public class cwFactory {
        private readonly cw_claim_detailsDALC _myCw_claim_detailsDALC;
        private readonly cw_make_monitlist_puserDALC _myCw_make_monitlist_puserDALC;
        private readonly cw_search_claimsDALC _myCw_search_claimsDALC;
        private readonly NationalRegistryViewDALC _myNationalRegistryViewDALC;
        private readonly np_CompanysDALC _myNp_CompanysDALC;
        private readonly np_EmployeeDALC _myNp_EmployeeDALC;
        private readonly np_UsageDALC _myNp_UsageDALC;
        private readonly UniqueCombinedToNationalRegistryDALC _myUniqueCombinedToNationalRegistryDALC;
        private readonly WatchUniqueIDDALC _myWatchUniqueIDDALC;
        private readonly WatchWordsDALC _myWatchWordsDALC;

        public cwFactory() {
            //
            // TODO: Add constructor logic here
            //
            _myWatchUniqueIDDALC = new WatchUniqueIDDALC();
            _myWatchWordsDALC = new WatchWordsDALC();
            _myNationalRegistryViewDALC = new NationalRegistryViewDALC();
            _myUniqueCombinedToNationalRegistryDALC = new UniqueCombinedToNationalRegistryDALC();
            _myCw_make_monitlist_puserDALC = new cw_make_monitlist_puserDALC();
            _myNp_UsageDALC = new np_UsageDALC();
            _myCw_search_claimsDALC = new cw_search_claimsDALC();
            _myCw_claim_detailsDALC = new cw_claim_detailsDALC();
            _myNp_EmployeeDALC = new np_EmployeeDALC();
            _myNp_CompanysDALC = new np_CompanysDALC();
        }

        #region cwFactory hluti

        /// <summary>
        /// Ey�ir �llum f�rslum fyrir einstakan notanda �r vaktat�flum (WatchUniqueID og WatchWords).
        /// </summary>
        /// <returns>True ef a�ger� tekst, False ef h�n tekst ekki.</returns>
        public bool DeleteWatch(int UserID) {
            bool DeleteUnique = DeleteUniqueWatchRecordsByUserID(UserID);
            bool DeleteWatchWords = DeleteWatchWordsRecordsByUserID(UserID);

            return DeleteUnique && DeleteWatchWords;
        }

        /// <summary>
        /// Telur allar f�rslur � vaktat�flum (WatchUniqueID og WatchWords) fyrir einstakan notanda.
        /// </summary>
        /// <returns>int, fj�ldi skr�ninga � heild.</returns>
        public int CountWatch(int UserID) {
            int myCheck, myCount;

            if ((myCheck = CountUniqueID(UserID)) != -1) {
                myCount = myCheck;
            } else {
                myCount = -1;
            }

            if ((myCheck = CountWatchWords(UserID)) != -1) {
                myCount += myCheck;
            } else {
                myCount = -1;
            }

            return myCount;
        }

        /// <summary>
        /// Telur allar f�rslur � vaktat�flum (WatchUniqueID og WatchWords) fyrir einstakan notanda og 
        /// eftir �v� hvort �etta s�u l�ga�ilar e�a einstaklingar.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany">True fyrir l�ga�ila, False fyrir einstaklinga</param>
        /// <returns>int, fj�ldi skr�ninga � heild.</returns>
        public int CountWatch(int UserID, bool IsCompany) {
            int myCheck, myCount;

            if ((myCheck = CountUniqueID(UserID, IsCompany)) != -1) {
                myCount = myCheck;
            } else {
                myCount = -1;
            }

            if ((myCheck = CountWatchWords(UserID, IsCompany)) != -1) {
                myCount += myCheck;
            } else {
                myCount = -1;
            }

            return myCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany"></param>
        /// <returns></returns>
        public DataSet GetTrimmedWordsOnWatchList(int UserID, bool IsCompany) {
            using (DataSet mySet = GetWordsToWatchListForUserAsDataSet(UserID, IsCompany)) {
                mySet.Tables.Add("TrimmedWordsTable");
                mySet.Tables[1].Columns.Add("CreditInfoID");
                mySet.Tables[1].Columns.Add("WatchID");
                mySet.Tables[1].Columns.Add("FirstName");
                mySet.Tables[1].Columns.Add("SurName");
                mySet.Tables[1].Columns.Add("Address");
                mySet.Tables[1].Columns.Add("Town");
                mySet.Tables[1].Columns.Add("IsCompany");
                mySet.Tables[1].Columns.Add("Sent");
                mySet.Tables[1].Columns.Add("Created");
                mySet.Tables[1].Columns.Add("IsNative");

                var myObjectCollection = new object[]
                                              {
                                                  "CreditInfoID", "WatchID", "FirstName", "SurName", "Address", "Town",
                                                  "IsCompany", "Sent", "Created", "IsNative"
                                              };

                try {
                    foreach (DataRow myRow in mySet.Tables[0].Rows) {
                        if (myRow["FirstNameEN"].ToString() != "") {
                            myObjectCollection[0] = myRow["CreditInfoID"];
                            myObjectCollection[1] = myRow["WatchID"];
                            myObjectCollection[2] = myRow["FirstNameEN"];
                            myObjectCollection[3] = myRow["SurNameEN"];
                            myObjectCollection[4] = myRow["AddressEN"];
                            myObjectCollection[5] = myRow["TownEN"];
                            myObjectCollection[6] = myRow["IsCompany"];
                            myObjectCollection[7] = myRow["Sent"];
                            myObjectCollection[8] = myRow["Created"];
                            myObjectCollection[9] = false;

                            mySet.Tables[1].Rows.Add(myObjectCollection);
                        } else {
                            myObjectCollection[0] = myRow["CreditInfoID"];
                            myObjectCollection[1] = myRow["WatchID"];
                            myObjectCollection[2] = myRow["FirstNameNative"];
                            myObjectCollection[3] = myRow["SurNameNative"];
                            myObjectCollection[4] = myRow["AddressNative"];
                            myObjectCollection[5] = myRow["TownNative"];
                            myObjectCollection[6] = myRow["IsCompany"];
                            myObjectCollection[7] = myRow["Sent"];
                            myObjectCollection[8] = myRow["Created"];
                            myObjectCollection[9] = true;

                            mySet.Tables[1].Rows.Add(myObjectCollection);
                        }
                    }
                    // Fyrir hverja r�� � datasetti sem inniheldur allar l�nur notanda �r WatchWords t�flunni...
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }

                return mySet;
            }
        }

        /// <summary>
        /// Skilar UniqueWatch sem hefur veri� samkeyrt vi� NationalRegistryView fyrir tiltekinn notanda.
        /// Gefur lista me� kt, nafni og heimilisfangi.
        /// </summary>
        /// <param name="CreditInfoID"></param>
        /// <param name="IsCompany"></param>
        /// <returns></returns>
        public DataSet GetUniqueWatchListJoinedToNationalRegistry(int CreditInfoID, bool IsCompany) {
            NationalRegistryViewBLLC myRegistry;

            using (DataSet mySet = GetUniqueOnWatchListForUserAsDataset(CreditInfoID, IsCompany)) {
                mySet.Tables[0].Columns.Add("Name");
                mySet.Tables[0].Columns.Add("Address");

                try {
                    // Fyrir hverja r�� � datasetti sem inniheldur allar l�nur notanda �r UniqueWatchID t�flunni...
                    foreach (DataRow myRow in mySet.Tables[0].Rows) {
                        new NationalRegistryViewBLLC();
                        myRegistry = GetNationalRegistry(myRow[2].ToString());
                        myRow["Name"] = myRegistry.Name;
                        myRow["Address"] = myRegistry.Address;
                    }
                    return mySet;
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }
            }
        }

        public bool TryToRegisterNewUniqueID(WatchUniqueIDBLLC myWatch) {
//			NationalRegistryViewBLLC myRegistry = this.GetNationalRegistry(myWatch.UniqueID);
//			myWatch.IsCompany = myRegistry.IsCompany.ToString();
            return AddNewToUniqueWatch(myWatch);
        }

        public DataSet GetSplitUpNativeSearchDetailsList(int CreditInfoID) {
            using (DataSet mySet = GetNativeSearchDetailsList(CreditInfoID)) {
                DataColumn myColumn;

                //mySet.Tables[0].Columns.CopyTo(myColumnCollection,0);

                // B�ti vi� t�flum � datasetti� fyrir hverja t�pu sem �g �arf a� f�...
                mySet.Tables.Add("Bankruptcy");
                mySet.Tables.Add("BouncedCheques");
                mySet.Tables.Add("CaseDecisionAgainstCompanies");
                mySet.Tables.Add("OtherCheques");
                mySet.Tables.Add("CzechDDD");
                // added for lt 31.05.05
                mySet.Tables.Add("LtDDD");

                // K�pera columns yfir � n�ju t�flurnar...
                for (int i = 1; i < mySet.Tables.Count; i++) {
                    for (int iCount = 0; iCount < mySet.Tables[0].Columns.Count; iCount++) {
                        myColumn = mySet.Tables[0].Columns[iCount];
                        mySet.Tables[i].Columns.Add(mySet.Tables[0].Columns[iCount].ColumnName);
                        mySet.Tables[i].Columns[iCount].DataType = myColumn.DataType;
                    }
                }

                try {
                    foreach (DataRow myRow in mySet.Tables[0].Rows) {
                        // specially quick and dirty fix for Czech
                        if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech")) {
                            mySet.Tables["CzechDDD"].Rows.Add(myRow.ItemArray);
                            continue;
                        }
                        // same for Lithuania (added 31.05.05)
                        if (CigConfig.Configure("lookupsettings.currentVersion").Equals("lt")) {
                            mySet.Tables["LtDDD"].Rows.Add(myRow.ItemArray);
                            continue;
                        }
                        // Ef f�rslan er af tegundinni "bankruptcy" (np_Case -> ClaimTypeID = 1)
                        // �� b�ti �g vi� �essa t�flu...
                        if (myRow["deciding_case_type"].ToString() == "1") {
                            mySet.Tables["Bankruptcy"].Rows.Add(myRow.ItemArray);
                        }

                        // Ef f�rslan er af tegundinni "Cheques and other" (np_Case -> ClaimTypeID = 2)
                        // �� b�ti �g vi� �essa t�flu...
                        if (myRow["deciding_case_type"].ToString() == "2") {
                            mySet.Tables["BouncedCheques"].Rows.Add(myRow.ItemArray);
                        }

                        // Ef f�rslan er af tegundinni "Case Decision Against Companies" (np_Case -> ClaimTypeID = 6)
                        // �� b�ti �g vi� �essa t�flu...
                        if (myRow["deciding_case_type"].ToString() == "6") {
                            mySet.Tables["CaseDecisionAgainstCompanies"].Rows.Add(myRow.ItemArray);
                        }

                        // Ef f�rslan er af tegundinni "Cheques and other - all other" (np_Case -> ClaimTypeID = 7)
                        // �� b�ti �g vi� �essa t�flu...
                        if (myRow["deciding_case_type"].ToString() == "7") {
                            mySet.Tables["OtherCheques"].Rows.Add(myRow.ItemArray);
                        }
                    }

                    // Taka �t allt data sem er � grunn t�flunni til �ess a� minnka datasetti�...
                    // mySet.Tables[0].Clear();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }

                return mySet;
            }
        }

        public DataSet GetSplitUpSearchDetailsList(int CreditInfoID, bool EN) {
            var mySet = EN ? GetENSearchDetailsList(CreditInfoID) : GetNativeSearchDetailsList(CreditInfoID);

            using (mySet) {
                DataColumn myColumn;

                //mySet.Tables[0].Columns.CopyTo(myColumnCollection,0);

                // B�ti vi� t�flum � datasetti� fyrir hverja t�pu sem �g �arf a� f�...
                mySet.Tables.Add("Bankruptcy");
                mySet.Tables.Add("BouncedCheques");
                mySet.Tables.Add("CaseDecisionAgainstCompanies");
                mySet.Tables.Add("OtherCheques");
                mySet.Tables.Add("CzechDDD");

                // K�pera columns yfir � n�ju t�flurnar...
                for (int i = 1; i < mySet.Tables.Count; i++) {
                    for (int iCount = 0; iCount < mySet.Tables[0].Columns.Count; iCount++) {
                        myColumn = mySet.Tables[0].Columns[iCount];
                        mySet.Tables[i].Columns.Add(mySet.Tables[0].Columns[iCount].ColumnName);
                        mySet.Tables[i].Columns[iCount].DataType = myColumn.DataType;
                    }
                }

                try {
                    foreach (DataRow myRow in mySet.Tables[0].Rows) {
                        // specially quick and dirty fix for Czech
                        if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech")) {
                            mySet.Tables["CzechDDD"].Rows.Add(myRow.ItemArray);
                            continue;
                        }
                        // Ef f�rslan er af tegundinni "bankruptcy" (np_Case -> ClaimTypeID = 1)
                        // �� b�ti �g vi� �essa t�flu...
                        if (myRow["deciding_case_type"].ToString() == "1") {
                            mySet.Tables["Bankruptcy"].Rows.Add(myRow.ItemArray);
                        }

                        // Ef f�rslan er af tegundinni "Cheques and other" (np_Case -> ClaimTypeID = 2)
                        // �� b�ti �g vi� �essa t�flu...
                        if (myRow["deciding_case_type"].ToString() == "2") {
                            mySet.Tables["BouncedCheques"].Rows.Add(myRow.ItemArray);
                        }

                        // Ef f�rslan er af tegundinni "Case Decision Against Companies" (np_Case -> ClaimTypeID = 6)
                        // �� b�ti �g vi� �essa t�flu...
                        if (myRow["deciding_case_type"].ToString() == "6") {
                            mySet.Tables["CaseDecisionAgainstCompanies"].Rows.Add(myRow.ItemArray);
                        }

                        // Ef f�rslan er af tegundinni "Cheques and other - all other" (np_Case -> ClaimTypeID = 7)
                        // �� b�ti �g vi� �essa t�flu...
                        if (myRow["deciding_case_type"].ToString() == "7") {
                            mySet.Tables["OtherCheques"].Rows.Add(myRow.ItemArray);
                        }
                    }

                    // Taka �t allt data sem er � grunn t�flunni til �ess a� minnka datasetti�...
                    // mySet.Tables[0].Clear();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }

                return mySet;
            }
        }

        /// <summary>
        /// This function is taken from Elitas Lithuanian code
        /// </summary>
        /// <param name="CreditInfoID">Unique internal system id</param>
        /// <returns>DataSet</returns>
        public DataSet GetPersonSearchDetailsList(int CreditInfoID) {
            using (DataSet mySet = GetPersonDebtsDetailsList(CreditInfoID)) {
                DataColumn myColumn;

                //mySet.Tables[0].Columns.CopyTo(myColumnCollection,0);

                // B�ti vi? t�flum ? datasetti? fyrir hverja t?pu sem �g ?arf a? f?...
                mySet.Tables.Add("LtPersonDDD");

                // K�pera columns yfir ? n?ju t�flurnar...
                for (int i = 1; i < mySet.Tables.Count; i++) {
                    for (int iCount = 0; iCount < mySet.Tables[0].Columns.Count; iCount++) {
                        myColumn = mySet.Tables[0].Columns[iCount];
                        mySet.Tables[i].Columns.Add(mySet.Tables[0].Columns[iCount].ColumnName);
                        mySet.Tables[i].Columns[iCount].DataType = myColumn.DataType;
                    }
                }

                try {
                    foreach (DataRow myRow in mySet.Tables[0].Rows) {
                        mySet.Tables["LtPersonDDD"].Rows.Add(myRow.ItemArray);
                        continue;
                    }

                    // Taka ?t allt data sem er ? grunn t�flunni til ?ess a? minnka datasetti?...
                    // mySet.Tables[0].Clear();
                } catch (Exception e) {
                    Logger.WriteToLog(e.Message, true);
                    return null;
                }

                return mySet;
            }
        }

        /// <summary>
        /// This function is taken from Elitas Lithuanian code
        /// </summary>
        /// <param name="creditInfoID">Internal system id</param>
        /// <returns>Instance of CompanyLT</returns>
        public CompanyLT GetCompanyReport(int creditInfoID) { return _myNationalRegistryViewDALC.GetCompanyReport(creditInfoID); }

        /// <summary>
        /// This function is taken from Elitas Lithuanian code
        /// </summary>
        /// <param name="nationalID">Subjects national id</param>
        /// <param name="creditInfoID">Subjects internal system id</param>
        /// <returns>Instance of CompanyLT</returns>
        public CompanyLT GetCompanyReport(int nationalID, int creditInfoID) { return _myNationalRegistryViewDALC.GetCompanyReport(nationalID, creditInfoID); }

        /// <summary>
        /// This function is taken from Elitas Lithuanian code
        /// </summary>
        /// <param name="StatusID">Status id</param>
        /// <param name="nativeCult">Whether it is native cult or not</param>
        /// <returns></returns>
        public string GetStatusAsString(int StatusID, bool nativeCult) { return _myNationalRegistryViewDALC.GetStatusAsString(StatusID, nativeCult); }

        /// <summary>
        /// This function is taken from Elitas Lithuanian code
        /// </summary>
        /// <param name="CreditInfoID">Subjects internal system id</param>
        /// <returns>DataSet</returns>
        public DataSet GetPersonDebtsDetailsList(int CreditInfoID) { return _myCw_claim_detailsDALC.GetPersonDebtsDetailsList(CreditInfoID); }

        #endregion

        #region WatchUniqueID hluti

        /// <summary>
        /// Tekur tilvik af vakt og setur �a� inn � cw_WatchUniqueID t�fluna.
        /// </summary>
        /// <param name="myWatch">Tilvik af vakt sem inniheldur einkv�mt n�mer</param>
        /// <returns>True ef a�ger� tekst, False ef h�n mistekst</returns>
        public bool AddNewToUniqueWatch(WatchUniqueIDBLLC myWatch) { return _myWatchUniqueIDDALC.AddNewToUniqueWatch(myWatch); }

        /// <summary>
        /// Tekur notandaeinkenni og skilar �llum r��um sem notandi � sem DataSet.
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetUniqueOnWatchListForUserAsDataset(int UserID) { return _myWatchUniqueIDDALC.GetUniqueOnWatchListForUserAsDataset(UserID); }

        /// <summary>
        /// Tekur notandaeinkenni og val um hvort skila eigi l�ga�ilum e�a einstaklingum. 
        /// Skilar �llum v�ldum r��um sem notandi � sem DataSet.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany">True fyrir l�ga�ila, False fyrir einstaklinga</param>
        /// <returns>DataSet</returns>
        public DataSet GetUniqueOnWatchListForUserAsDataset(int UserID, bool IsCompany) { return _myWatchUniqueIDDALC.GetUniqueOnWatchListForUserAsDataset(UserID, IsCompany); }

        /// <summary>
        /// Ey�ir einni stakri f�rslu �r t�flu eftir einkv�mu "vaktan�meri" sem fylgir hverri f�rslu.
        /// </summary>
        /// <param name="WatchID">Einkv�mt n�mer sem fylgir hverri f�rslu.</param>
        /// <returns>True ef a�ger� tekst, False ef h�n tekst ekki.</returns>
        public bool DeleteUniqueWatchRecordByWatchID(int WatchID) { return _myWatchUniqueIDDALC.DeleteUniqueWatchRecordByWatchID(WatchID); }

        public bool DeleteUniqueWatchRecordByUniqueID(string UniqueID) { return _myWatchUniqueIDDALC.DeleteUniqueWatchRecordByUniqueID(UniqueID); }

        /// <summary>
        /// Ey�ir �llum f�rslum �r WatchUniqueID t�flu fyrir fyrir einn �kve�inn notanda.
        /// </summary>
        /// <returns>True ef a�ger� tekst, False ef h�n tekst ekki.</returns>
        public bool DeleteUniqueWatchRecordsByUserID(int UserID) { return _myWatchUniqueIDDALC.DeleteUniqueWatchRecordsByUserID(UserID); }

        /// <summary>
        /// Skilar int t�lu sem er talning � f�rslum �kve�ins notanda upp �r WatchUniqueID t�flunni.
        /// </summary>
        /// <returns>Fj�ldi f�rslna.</returns>
        public int CountUniqueID(int UserID) { return _myWatchUniqueIDDALC.CountUniqueID(UserID); }

        /// <summary>
        /// Skilar fj�lda ra�a � WatchUniqueID t�flu sem tilheyra �kve�num notanda og eru anna� hvort 
        /// l�ga�ilar e�a einstaklingar.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany">True fyrir l�ga�ila, False fyrir einstaklinga</param>
        /// <returns>Fj�ldi l�ga�ila / einstaklinga � WathchUniqueID t�flu m.v. notanda.</returns>
        public int CountUniqueID(int UserID, bool IsCompany) { return _myWatchUniqueIDDALC.CountUniqueID(UserID, IsCompany); }

        /// <summary>
        /// Leitar a� f�rslu eftir notanda og einkv�mu n�meri
        /// </summary>
        /// <param name="myWatch"></param>
        /// <returns></returns>
        public bool SearchUniqueWatch(WatchUniqueIDBLLC myWatch) { return _myWatchUniqueIDDALC.SearchUniqueWatch(myWatch); }

        #endregion

        #region WatchWords hluti

        /// <summary>
        /// Tekur tilvik af vakt og setur �a� inn � cw_WatchWords t�fluna.
        /// </summary>
        /// <param name="myWatch">Tilvik af vakt sem inniheldur stikkor�</param>
        /// <returns>True ef a�ger� tekst, False ef h�n mistekst</returns>
        public bool AddNewWordsToWatch(WatchWordsBLLC myWatch) { return _myWatchWordsDALC.AddNewWordsToWatch(myWatch); }

        /// <summary>
        /// Tekur notandaeinkenni og skilar �llum r��um sem notandi � sem DataSet.
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetWordsToWatchListForUserAsDataSet(int UserID) { return _myWatchWordsDALC.GetWordsToWatchListForUserAsDataSet(UserID); }

        /// <summary>
        /// Tekur notandaeinkenni og val um hvort skila eigi l�ga�ilum e�a einstaklingum. 
        /// Skilar �llum v�ldum r��um sem notandi � sem DataSet.
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetWordsToWatchListForUserAsDataSet(int UserID, bool isCompany) { return _myWatchWordsDALC.GetWordsToWatchListForUserAsDataSet(UserID, isCompany); }

        /// <summary>
        /// Ey�ir einni stakri f�rslu �r t�flu eftir einkv�mu "vaktan�meri" sem fylgir hverri f�rslu.
        /// </summary>
        /// <param name="WatchID">Einkv�mt n�mer sem fylgir hverri f�rslu.</param>
        /// <returns>True ef a�ger� tekst, False ef h�n tekst ekki.</returns>
        public bool DeleteWatchWordsRecordByWatchID(int WatchID) { return _myWatchWordsDALC.DeleteWatchWordsRecordByWatchID(WatchID); }

        /// <summary>
        /// Ey�ir �llum f�rslum �r WatchUniqueID t�flu fyrir einn �kve�inn notanda.
        /// </summary>
        /// <returns>True ef a�ger� tekst, False ef h�n tekst ekki.</returns>
        public bool DeleteWatchWordsRecordsByUserID(int UserID) { return _myWatchWordsDALC.DeleteWatchWordsRecordsByUserID(UserID); }

        /// <summary>
        /// Ey�ir f�rslu �r WatchWords t�flu eftir gefnum stikkor�um.
        /// </summary>
        /// <param name="Town"></param>
        /// <param name="IsCompany"></param>
        /// <param name="UserID"></param>
        /// <param name="FirstName"></param>
        /// <param name="SurName"></param>
        /// <param name="Address"></param>
        /// <param name="IsNative"></param>
        /// <returns></returns>
        public bool DeleteWatchWordsRecordByWatchWords(
            int UserID, string FirstName, string SurName, string Address, string Town, string IsCompany, bool IsNative) {
            return _myWatchWordsDALC.DeleteWatchWordsRecordByWatchWords(
                UserID, FirstName, SurName, Address, Town, IsCompany, IsNative);
        }

        /// <summary>
        /// Uppf�rir eina f�rslu �  cw_WatchWords t�flunni.
        /// </summary>
        /// <param name="myWatch">Eitt tilvik af f�rslu fyrir t�fluna</param>
        /// <returns>Skila True ef a�ger� tekst, False ef h�n tekst ekki.</returns>
        public bool UpdateWordsToWatch(WatchWordsBLLC myWatch) { return _myWatchWordsDALC.UpdateWordsToWatch(myWatch); }

        /// <summary>
        /// skilar fj�lda ra�a � WatchWords t�flu sem tilheyra �kve�num notanda.
        /// </summary>
        /// <returns>int sem er fj�ldi ra�a � t�flu m.v. �kve�inn notanda.</returns>
        public int CountWatchWords(int UserID) { return _myWatchWordsDALC.CountWatchWords(UserID); }

        /// <summary>
        /// Skilar fj�lda ra�a � WatchWords t�flu sem tilheyra �kve�num notanda og eru anna� hvort 
        /// l�ga�ilar e�a einstaklingar.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany">True fyrir l�ga�ila, False fyrir einstaklinga</param>
        /// <returns>Fj�ldi l�ga�ila / einstaklinga � WatchWords t�flu m.v. notanda.</returns>
        public int CountWatchWords(int UserID, bool IsCompany) { return _myWatchWordsDALC.CountWatchWords(UserID, IsCompany); }

        /// <summary>
        /// leitar eftir notanda og leitaror�um f�rslu og ef f�rsla finnst er skila� gildinu "true", annars "false".
        /// </summary>
        /// <param name="myWatch"></param>
        /// <param name="isNative"></param>
        /// <returns></returns>
        public bool SearchWatchWords(WatchWordsBLLC myWatch, bool isNative) { return _myWatchWordsDALC.SearchWatchWords(myWatch, isNative); }

        #endregion

        #region NationalRegistryView hluti

        /// <summary>
        /// Leitar a� sk��um l�ga�ila e�a einstakling � "NationalRegistryView" mi�a� vi� "kennit�lu"
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public bool SearchNationalRegistry(string UniqueID) { return _myNationalRegistryViewDALC.SearchNationalRegistry(UniqueID); }

        /// <summary>
        /// Skilar tilviki af NationalRegistryView m.v. gefna "kennit�lu".
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public NationalRegistryViewBLLC GetNationalRegistry(string UniqueID) { return _myNationalRegistryViewDALC.GetNationalRegistry(UniqueID); }

        #endregion

        #region UniqueCombinedToNationalRegistry hluti

        /// <summary>
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="IsCompany"></param>
        /// <returns></returns>
        public DataSet GetUniqueOnWatchListWithNameAndAddressForUserAsDataset(int UserID, bool IsCompany) {
            return _myUniqueCombinedToNationalRegistryDALC.GetUniqueOnWatchListWithNameAndAddressForUserAsDataset(
                UserID, IsCompany);
        }

        public DataSet GetUniqueOnWatchListWithNameAndAddressForUserAsDataset(int UserID) { return _myUniqueCombinedToNationalRegistryDALC.GetUniqueOnWatchListWithNameAndAddressForUserAsDataset(UserID); }

        #endregion

        #region cw_make_monitlist_puser (user function) hluti

        public DataSet GetClaimListAsDataSet(int UserID, int Native) { return _myCw_make_monitlist_puserDALC.GetClaimListAsDataSet(UserID, Native); }

        #endregion

        #region cw_search_list (user function) hluti

        public DataSet GetNativeSearchList(string Number, string FirstName, string Surname, string Address) { return _myCw_search_claimsDALC.GetNativeSearchList(Number, FirstName, Surname, Address); }
        public DataSet GetENSearchList(string Number, string FirstName, string Surname, string Address) { return _myCw_search_claimsDALC.GetENSearchList(Number, FirstName, Surname, Address); }

        #endregion

        #region cw_claim_details_list (user function) hluti

        public DataSet GetNativeSearchDetailsList(int CreditInfoID) { return _myCw_claim_detailsDALC.GetNativeSearchDetailsList(CreditInfoID); }
        public DataSet GetENSearchDetailsList(int CreditInfoID) { return _myCw_claim_detailsDALC.GetENSearchDetailsList(CreditInfoID); }
        public int GetLogCountForLastXDaysPerCreditInfoUser(int IntervalAsDays, int CreditInfoID) { return _myCw_claim_detailsDALC.GetLogCountForLastXDaysPerCreditInfoUser(IntervalAsDays, CreditInfoID); }

        public int GetLogCountForLastXDaysPerCreditInfoUserExcludingEmployees(int IntervalAsDays, int CreditInfoID) {
            return _myCw_claim_detailsDALC.GetLogCountForLastXDaysPerCreditInfoUserExcludingEmployees(
                IntervalAsDays, CreditInfoID);
        }

        public DataSet GetOldNamesForCompanyAsDataSet(int CreditInfoID) { return _myCw_claim_detailsDALC.GetOldNamesForCompanyAsDataSet(CreditInfoID); }
        public string GetTheNewestNameForACompany(int CreditInfoID) { return _myCw_claim_detailsDALC.GetTheNewestNameForACompany(CreditInfoID); }
        public int CheckForOldCompanyNames(int CreditInfoID) { return _myCw_claim_detailsDALC.CheckForOldCompanyNames(CreditInfoID); }

        #endregion

        #region np_Usage (loggun) hluti

        public bool AddNewUsageLog(np_UsageBLLC myLog) { return _myNp_UsageDALC.AddNewUsageLog(myLog); }

        #endregion

        #region np_Employee hluti

        public bool CheckForEmployee(string UserLoginName) { return _myNp_EmployeeDALC.CheckForEmployee(UserLoginName); }

        #endregion

        #region np_Companys hluti

        public string CheckForCompanyStatus(int CreditInfoID, bool native) { return _myNp_CompanysDALC.CheckForCompanyStatus(CreditInfoID, native); }
        public bool CheckIfCompanyIsInBadShape(int CreditInfoID) { return _myNp_CompanysDALC.CheckIfCompanyIsInBadShape(CreditInfoID); }
        public string CheckForCompanyNationalID(int CreditInfoID) { return _myNp_CompanysDALC.CheckForCompanyNationalID(CreditInfoID); }

        #endregion
    }
}