#region

using System;

#endregion

namespace CreditWatch.BLL.NationalRegistryView {
    /// <summary>
    /// Gangagrind fyrir cw_NationalRegistryView "Viewi�". Notu� til �ess a� a� setja og s�kja g�gn sem m� svo
    /// skrifa ni�ur � grunninn.
    /// </summary>
    public class NationalRegistryViewBLLC {
        public string UniqueID { get; set; }
        public string Name { get; set; }
        public String Address { get; set; }
        public String IsCompany { get; set; }
    }
}