#region

using System;
using System.IO;

#endregion

namespace CreditWatch.BLL {
    /// <summary>
    /// Logger - setur allar gripnar "execs" ni�ur � skr�r.
    /// </summary>
    public class Logger {
        private static readonly string debugFilePath = Environment.GetEnvironmentVariable("TMP") + @"\debug.txt";
        private static readonly string errorFilePath = Environment.GetEnvironmentVariable("TMP") + @"\error.txt";

        public static void WriteToLog(string input, bool error) {
            string filePath = error ? errorFilePath : debugFilePath;

            var logFile = new FileInfo(filePath);

            if (logFile.Exists) {
                if (logFile.Length >= 100000) {
                    File.Delete(filePath);
                }
            }

            var fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            var w = new StreamWriter(fs);
            w.BaseStream.Seek(0, SeekOrigin.End);

            w.Write("\nLog Entry : ");
            w.Write(
                "{0} {1} \n\n",
                DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());

            w.Write(input + "\n");
            w.Write("------------------------------------\n");

            w.Flush();

            w.Close();
        }
    }
}