#region

using System;
using System.Reflection;
using System.Resources;
using System.Web;

#endregion

namespace CreditWatch.BLL.Localization {
    /// <summary>
    /// Summary description for CIResource.
    /// </summary>
    public class CIResource {
        private const string ApplicationVariableName = "CreditWatch";
        //private static string _baseResourceFile = "CreditWatch.resources.strings";  // take this from config
        //private string myTest;
        /// <summary>
        /// <para>The CurrentManager property returns and caches the shared ResourceManager instance.
        /// </para>
        /// </summary>
        public static ResourceManager CurrentManager {
            get {
                var context = HttpContext.Current;
                if (null == context) {
                    throw new ArgumentException("Global_NoHttpContext");
                }
                var mgr = context.Cache[ApplicationVariableName] as ResourceManager;

                if (null == mgr) {
                    mgr = new ResourceManager(
                        "cb3.CreditWatch.resources.strings", Assembly.GetExecutingAssembly(), null);

                    // Add to the cache
                    context.Cache.Insert(ApplicationVariableName, mgr);
                }
                return mgr;
            }
        }
    }
}