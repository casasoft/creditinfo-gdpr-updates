#region

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditWatch.BLL;

#endregion

namespace CreditWatch {
    /// <summary>
    /// Summary description for OldNames.
    /// </summary>
    public class OldNames : Page {
        private readonly cwFactory myFactory = new cwFactory();
        protected DataGrid dgOldNamesList;
        protected Label lblNewestName;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here

            DataSet mySet =
                myFactory.GetOldNamesForCompanyAsDataSet(Convert.ToInt32(Session["cwClaimSearchCreditInfoID"]));
            dgOldNamesList.DataSource = mySet;
            dgOldNamesList.DataBind();

            SetNewestNameLabel();
        }

        private void SetNewestNameLabel() {
            DataGridItem myItem;

            for (int iCount = 0; iCount < dgOldNamesList.Items.Count; iCount++) {
                myItem = dgOldNamesList.Items[iCount];
                if (myItem.Cells[1].Text == "ACR") {
                    lblNewestName.Text = myItem.Cells[0].Text + " is the name in use";
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}