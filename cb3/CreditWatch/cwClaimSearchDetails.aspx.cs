#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CreditWatch.BLL;
using CreditWatch.BLL.Localization;
using CreditWatch.BLL.np_Usage;

#endregion

using Cig.Framework.Base.Configuration;


namespace CreditWatch {
    /// <summary>
    /// Summary description for cwClaimSearchDetails.
    /// </summary>
    public class cwClaimSearchDetails : Page {
        // Skilgreina instance af Factory...
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly cwFactory myFactory = new cwFactory();
        protected HtmlTableRow bankruptcyELine;
        protected HtmlTableRow bankruptcyRepeaterRow;
        protected HtmlTableRow bouncedChecksELine;
        protected HtmlTableRow bouncedChecksRepeaterRow;
        protected Button btnBack;
        protected Button btnPrint;
        protected HtmlTableRow casesAgainstCompaniesELine;
        protected HtmlTableRow casesAgainstCompaniesRepeaterRow;
        protected HtmlTableRow czDDDELine;
        protected HtmlTableRow czDDDRepeaterRow;
        private bool EN;
        protected HtmlTableCell headCzDDD;
        protected HtmlTableCell headerBankruptcy;
        protected HtmlTableCell headerBouncedChecks;
        protected HtmlTableCell headerCasesAgainstCompanies;
        protected HtmlTableCell headerOther;
        protected HtmlTableCell headLtDDD;
        protected Label lblAddress;
        protected Label lblAddressLabel;
        protected Label lblCity;
        protected Label lblCityLabel;
        protected Label lblClaimDetails;
        protected Label lblClaims;
        protected Label lblCompanyStatus;
        protected Label lblCompStLabel;
        protected Label lblDeptsHeader;
        protected Label lblDisclaimer;
        protected Label lblLtClaims;
        protected Label lblName;
        protected Label lblNameLabel;
        protected Label lblNationalID;
        protected Label lblNationalIDlbl;
        protected Label lblNoClaimRegistered;
        protected Label lblOneMonth;
        protected Label lblRecent;
        protected Label lblSixMonths;
        protected Label lblStatusLER;
        protected Label lblStatusLERLabel;
        protected Label lblStatusSLER;
        protected Label lblStatusSLERLabel;
        protected Label lblThreeMonths;
        protected Label lblTimeStamp;
        protected Label ldlToday;
        protected HtmlAnchor linkBack;
        protected HtmlTableRow ltDDDELine;
        protected HtmlTableRow ltDDDRepeaterRow;
        protected HtmlTableRow ltPersonsDDDRepeaterRow;
        private bool nativeCult;
        protected HtmlTableRow otherELine;
        protected HtmlTableRow otherRepeaterRow;
        protected Repeater repeatBankruptcy;
        protected Repeater repeatBouncedChecks;
        protected Repeater repeatCasesAgainstCompanies;
        protected Repeater repeatCzDDD;
        protected Repeater repeatLtDDD;
        protected Repeater repeatLtPersonDDD;
        protected Repeater repeatOther;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here

            // Fyrir multilanguage d�mi...
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            String culture = Thread.CurrentThread.CurrentCulture.Name;
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            LocalizeText();
            /*if (myFactory.CheckForOldCompanyNames(Convert.ToInt32(Session["cwClaimSearchCreditInfoID"])) > 1)
				OldNames.Visible = true;
			else
				OldNames.Visible = false;*/

            /*
			 * Teki� �t svo a� leitin � forminu skili alltaf "native" Datasetti 
			 * (user function "np_search_claims_native")
			 * 
			 *	if (culture.Equals("en-US"))
			 *		EN = true;
			 */

            GetDataForDataGrid();
        }

        private void LocalizeText() {
            // Eftirfarandi l�nur eru nota�ar til �ess a� n� � Company status (org_status) ef um fyrirt�ki er a� r��a
            // og ef hann er til. Gert svona �ar sem �etta er flj�tlegasta lei�in (ekki �d�rasta... hva� var�ar 
            // tengingar og gagnas�kn samt...)

            // Einnig eru textar sem sn�a a� vi�komandi a�ila lita�ir rau�ir ef hann er t.d. or�inn gjald�rota. Gert
            // samkv�mt �kve�num k��um sem m� sj� � np_CompanysDALC klasanum.

            lblCompStLabel.Visible = false;
            lblCompanyStatus.Visible = false;
            lblNationalIDlbl.Visible = false;
            lblNationalID.Visible = false;

            string myCompanyStatus =
                myFactory.CheckForCompanyStatus(
                    Convert.ToInt32(Session["cwClaimSearchCreditInfoID"].ToString()), nativeCult);

            if (myCompanyStatus != "") {
                lblCompStLabel.Visible = true;
                lblCompanyStatus.Visible = true;
                lblCompStLabel.Text = rm.GetString("lbCompStLabel", ci);
                lblCompanyStatus.Text = myCompanyStatus;

                if (
                    myFactory.CheckIfCompanyIsInBadShape(
                        Convert.ToInt32(Session["cwClaimSearchCreditInfoID"].ToString()))) {
                    lblNameLabel.ForeColor = Color.Red;
                    lblName.ForeColor = Color.Red;
                    lblAddressLabel.ForeColor = Color.Red;
                    lblAddress.ForeColor = Color.Red;
                    lblCityLabel.ForeColor = Color.Red;
                    lblCity.ForeColor = Color.Red;
                    lblCompStLabel.ForeColor = Color.Red;
                    lblCompanyStatus.ForeColor = Color.Red;
                    lblNationalIDlbl.ForeColor = Color.Red;
                    lblNationalID.ForeColor = Color.Red;
                }

                string NationalID =
                    myFactory.CheckForCompanyNationalID(
                        Convert.ToInt32(Session["cwClaimSearchCreditInfoID"].ToString()));
                if (NationalID != "") {
                    lblNationalIDlbl.Visible = true;
                    lblNationalID.Visible = true;
                    lblNationalIDlbl.Text = rm.GetString("lbNationalID", ci);
                    lblNationalID.Text = NationalID;
                }
            }

            // Heiti sett � labels. S�tt �r Resource t�flum...

            //this.lblName.Text = myFactory.GetTheNewestNameForACompany(Convert.ToInt32(Session["cwClaimSearchCreditInfoID"]));

            if (lblName.Text == "") {
                if (Session["cwClaimSearchName"] != null) {
                    lblName.Text = Session["cwClaimSearchName"].ToString();
                }
            }

            if (Session["cwClaimSearchAddress"] != null) {
                lblAddress.Text = Session["cwClaimSearchAddress"].ToString();
            }
            if (Session["cwClaimSearchCity"] != null) {
                lblCity.Text = Session["cwClaimSearchCity"].ToString();
            }

            lblClaimDetails.Text = rm.GetString("lbClaimDetails", ci);
            //this.linkBack.Title = rm.GetString("lbLinkBack",ci);
            lblNameLabel.Text = rm.GetString("lbName", ci);
            lblAddressLabel.Text = rm.GetString("lbAddress", ci);
            lblCityLabel.Text = rm.GetString("lbCity", ci);
            ldlToday.Text = rm.GetString("lbToday", ci);
            lblTimeStamp.Text = DateTime.Now.ToShortDateString();
            lblDeptsHeader.Text = rm.GetString("lbDeptsHeader", ci);
            lblRecent.Text = rm.GetString("lbRecent", ci);
            btnPrint.Text = rm.GetString("lbPrint", ci);
            //this.OldNames.InnerText = rm.GetString("txtOldNames",ci);
            linkBack.InnerText = rm.GetString("txtBack", ci);
            lblDisclaimer.Text = rm.GetString("lblDisclaimer", ci);
            lblNoClaimRegistered.Text = rm.GetString("txtNoClaimRegistered", ci);
            lblClaims.Text = rm.GetString("txtClaims", ci);
            //TODO: finna label lbLTClaims  (ThrosturG)

            /* 
			 * JBA - 17.09.2003
			 * Labels �ar sem teknar eru flettingar s��asta m�nu�, 3 m�nu�i og 6 m�nu�i. Teki� �r log skr�nni.
			 * Er fyrir hvern skr��an a�ila (CreditInfoID).
			 */

            lblOneMonth.Text = rm.GetString("lbLastMonth", ci) + "\t\t" +
                               myFactory.GetLogCountForLastXDaysPerCreditInfoUser(
                                   30, Convert.ToInt32(Session["cwClaimSearchCreditInfoID"].ToString()));
            lblThreeMonths.Text = rm.GetString("lbLast3Months", ci) + "\t\t" +
                                  myFactory.GetLogCountForLastXDaysPerCreditInfoUser(
                                      90, Convert.ToInt32(Session["cwClaimSearchCreditInfoID"].ToString()));
            lblSixMonths.Text = rm.GetString("lbLast6Months", ci) + "\t\t" +
                                myFactory.GetLogCountForLastXDaysPerCreditInfoUser(
                                    180, Convert.ToInt32(Session["cwClaimSearchCreditInfoID"].ToString()));
        }

        private void GetDataForDataGrid() {
            try {
                if (EN) {
                    using (
                        DataSet mySet =
                            myFactory.GetENSearchDetailsList(
                                Convert.ToInt32(Session["cwClaimSearchCreditInfoID"].ToString()))) {
                        // Session["dgSearchDetailsList"] = mySet.Tables[0];
                        lblNoClaimRegistered.Visible = mySet.Tables[0].Rows.Count == 0;

                        if (!IsPostBack) {
                            if (!myFactory.CheckForEmployee(Session["UserLoginName"].ToString())) {
                                LogThis(mySet);
                            }
                        }
                    }
                }
                    // Fer alltaf hinga� vegna �ess a� EN breytan er alltaf sett "false" �v� enska leitin var tekin �t...
                else {
                    using (
                        DataSet mySet =
                            myFactory.GetSplitUpNativeSearchDetailsList(
                                Convert.ToInt32(Session["cwClaimSearchCreditInfoID"].ToString()))) {
                        // hmm �arf �g a� t�kka � hverri t�flu fyrir sig?
                        if (mySet.Tables[1].Rows.Count == 0 && mySet.Tables[2].Rows.Count == 0 &&
                            mySet.Tables[3].Rows.Count == 0 && mySet.Tables[4].Rows.Count == 0 &&
                            mySet.Tables[5].Rows.Count == 0 && mySet.Tables[6].Rows.Count == 0) {
                            lblNoClaimRegistered.Visible = true;
                        } else {
                            lblNoClaimRegistered.Visible = false;
                        }

                        // Tek inn t�fluna sem inniheldur ClaimTypeID = 1 fyrir Bankruptcy
                        var myBankrptcyView = mySet.Tables[1].DefaultView;
                        // Tek inn t�fluna sem inniheldur ClaimTypeID = 2 fyrir Bounced checks
                        var myBouncedChecksView = mySet.Tables[2].DefaultView;
                        // Tek inn t�fluna sem inniheldur ClaimTypeID = 6 fyrir Case decision agains companies
                        var myCasesAgainstCompaniesView = mySet.Tables[3].DefaultView;
                        // Tek inn t�fluna sem inniheldur ClaimTypeID = 7 fyrir other checks
                        var myOtherChecksView = mySet.Tables[4].DefaultView;
                        // for Czech version
                        var myCzDDDView = mySet.Tables[5].DefaultView;
                        // for LT version 
                        var myLtDDDView = mySet.Tables[6].DefaultView;

                        // Ra�a eftir "claim_date". Fyrst �arf �g a� v�sa � Default View � t�flunni...
                        myBankrptcyView.Sort = "claim_date DESC";
                        myBouncedChecksView.Sort = "claim_date DESC";
                        myCasesAgainstCompaniesView.Sort = "claim_date DESC";
                        myOtherChecksView.Sort = "claim_date DESC";

                        // Setja DataSource fyrir alla repeaters (t�flur 1-4 � DataSet)
                        repeatBankruptcy.DataSource = myBankrptcyView;
                        repeatBankruptcy.DataBind();

                        repeatBouncedChecks.DataSource = myBouncedChecksView;
                        repeatBouncedChecks.DataBind();

                        repeatCasesAgainstCompanies.DataSource = myCasesAgainstCompaniesView;
                        repeatCasesAgainstCompanies.DataBind();

                        repeatOther.DataSource = myOtherChecksView;
                        repeatOther.DataBind();

                        repeatCzDDD.DataSource = myCzDDDView;
                        repeatCzDDD.DataBind();

                        //Lithuania version. Check if currentVersin is lt. then bind LT repeaters to LTview.
                        if (CigConfig.Configure("lookupsettings.currentVersion").Equals("lt")) //check if this is lituanian version
                        {
                            //If the value in code column is null then this is company (the userdefined function (sql) returns
                            // "null" in the code columnt if this is company info. else the Native code )
                            if (mySet.Tables[6].Rows[0]["code"] == DBNull.Value) {
                                repeatLtDDD.DataSource = myLtDDDView;
                                repeatLtDDD.DataBind();
                            } else //this is person 
                            {
                                repeatLtPersonDDD.DataSource = myLtDDDView;
                                repeatLtPersonDDD.DataBind();
                            }
                        }

                        // Athuga innihald repeaters. Ef �eir eru t�mir �� viljum vi� fela hausana fyrir hvern flokk...
                        if (repeatBankruptcy.Items.Count > 0) {
                            headerBankruptcy.Visible = true;
                            bankruptcyELine.Visible = true;
                            bankruptcyRepeaterRow.Visible = true;
                        } else {
                            headerBankruptcy.Visible = false;
                            bankruptcyELine.Visible = false;
                            bankruptcyRepeaterRow.Visible = false;
                        }

                        if (repeatBouncedChecks.Items.Count > 0) {
                            headerBouncedChecks.Visible = true;
                            bouncedChecksELine.Visible = true;
                            bouncedChecksRepeaterRow.Visible = true;
                        } else {
                            headerBouncedChecks.Visible = false;
                            bouncedChecksELine.Visible = false;
                            bouncedChecksRepeaterRow.Visible = false;
                        }

                        if (repeatCasesAgainstCompanies.Items.Count > 0) {
                            headerCasesAgainstCompanies.Visible = true;
                            casesAgainstCompaniesELine.Visible = true;
                            casesAgainstCompaniesRepeaterRow.Visible = true;
                        } else {
                            headerCasesAgainstCompanies.Visible = false;
                            casesAgainstCompaniesELine.Visible = false;
                            casesAgainstCompaniesRepeaterRow.Visible = false;
                        }

                        if (repeatOther.Items.Count > 0) {
                            headerOther.Visible = true;
                            otherELine.Visible = true;
                            otherRepeaterRow.Visible = true;
                        } else {
                            headerOther.Visible = false;
                            otherELine.Visible = false;
                            otherRepeaterRow.Visible = false;
                        }

                        if (repeatCzDDD.Items.Count > 0) {
                            headCzDDD.Visible = true;
                            czDDDELine.Visible = true;
                            czDDDRepeaterRow.Visible = true;
                        } else {
                            headCzDDD.Visible = false;
                            czDDDELine.Visible = false;
                            czDDDRepeaterRow.Visible = false;
                        }

                        //Lithuanian version
                        if (repeatLtDDD.Items.Count > 0) {
                            headLtDDD.Visible = true;
                            ltDDDELine.Visible = true;
                            ltDDDRepeaterRow.Visible = true;
                        } else if (repeatLtPersonDDD.Items.Count > 0) {
                            headLtDDD.Visible = true;
                            ltDDDELine.Visible = true;
                            ltPersonsDDDRepeaterRow.Visible = true;
                        } else {
                            headLtDDD.Visible = false;
                            ltDDDELine.Visible = false;
                            ltDDDRepeaterRow.Visible = false;
                            ltPersonsDDDRepeaterRow.Visible = false;
                        }

                        // Loggun - Fer ekki af sta� fyrir �� sem eru skr��ir sem starfsmenn LT
                        //			Athuga� me� �v� a� sko�a np_employee t�fluna � grunninum.
                        if (!IsPostBack) {
                            if (!myFactory.CheckForEmployee(Session["UserLoginName"].ToString())) {
                                LogThis(mySet);
                            }
                        }
                    }
                }
            } catch (Exception err) {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwClaimList.aspx caught, message is : " + err.Message, true);
            }
        }

        private void LogThis(DataSet mySet) {
            var myLog = new np_UsageBLLC();

            try {
                myLog.CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]);
                // UserID nota� til a� a�greina f�rslur � loggun � milli notenda... tekur vi� af CreditInfoID
                // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)
                myLog.UserID = Convert.ToInt32(Session["UserLoginID"]);
                myLog.QueryType = 20;
                myLog.Query = Session["cwClaimSearchCreditInfoID"].ToString();
                myLog.ResultCount = mySet.Tables[0].Rows.Count;
                myLog.IP = Request.ServerVariables["REMOTE_ADDR"];

                myFactory.AddNewUsageLog(myLog);
            } catch (Exception err) {
                Logger.WriteToLog(
                    "Logging exception (probable 0 error on CreditInfoID) on cwClaimList.aspx caught, message is : " +
                    err.Message,
                    true);
                Server.Transfer("error.aspx?err=1");
            }

            /*
			if (!myFactory.AddNewUsageLog (myLog))
			{
				Logger.WriteToLog("Logging exception (probable 0 error on CreditInfoID) on cwClaimList.aspx caught, message is : " + err.Message, true);
				Server.Transfer("error.aspx?err=1");
			}
			*/
        }

        private void btnPrint_Click(object sender, EventArgs e) {
            const string myScript = "<script language=Javascript>window.print();</script>";
            Page.RegisterClientScriptBlock("alert", myScript);
        }

        // Fyrir Data repeater - Athugar hvorn strenginn � a� birta fyrir case type !
        protected static string GetCorrectTypeString(object myclaim_typeObject, object mycase_decision_typeOject) {
            string myTypeString = Convert.ToString(myclaim_typeObject);
            string myCaseString = Convert.ToString(mycase_decision_typeOject);

            return myCaseString != "" ? myCaseString : myTypeString;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }

    /*<!-- <%#	if (Container.DataItem("case_decision_type") != "" ) 
														DataBinder.Eval(Container.DataItem, "case_decision_type");
													else
														DataBinder.Eval(Container.DataItem, "claim_type");
												%> -->*/
}