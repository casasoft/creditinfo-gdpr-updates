<%@ Page language="c#" Codebehind="cwOverView.aspx.cs" AutoEventWireup="false" Inherits="CreditWatch.cwOverView" %>
<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="../new_user_controls/FoOptions.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>Creditwatch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="popup.js"></script>
		<script language="JavaScript" src="validation.js"></script>
		<link href="../css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
	</style>
		<script language="javascript"> 
				function checkEnterKey(i) 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						if(i==1)
							Monitoring.btnRegisterUnique.click(); 
						else
							Monitoring.btnRegisterWords.click(); 
					}
				} 
				
				function SetFormFocus()
				{
					//document.frmMain.txtID.focus();
				}
				
		</script>
</head>
	<body style="BACKGROUND-IMAGE: url(../img/mainback.gif)" leftmargin="0" onload="SetFormFocus()"
		rightmargin="0" ms_positioning="GridLayout">
		<form id="Monitoring" name="Credit Watch" method="post" enctype="multipart/form-data"
			runat="server">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
								<td>
									<table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="#951e16" border="0">
										<tr>
											<td bgcolor="#951e16"><span style="WIDTH: 3px"></span></td>
											<td bgcolor="#951e16"><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
											<td align="right" bgcolor="#951e16"><uc1:language id="Language2" runat="server"></uc1:language></td>
										</tr>
									</table>
								</td>
								<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="white" border="0">
							<tr>
								<td>
									<table id="Frame" cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
										<tr>
											<td colspan="3">
												<table id="tbDefault" cellspacing="0" cellpadding="0" width="100%" align="center">
													<tr>
														<td colspan="3">
															<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
																<tr>
																	<td><uc1:userinfo id="UserInfo1" runat="server"></uc1:userinfo></td>
																	<td align="right">
																		<uc1:fooptions id="FoOptions1" runat="server"></uc1:fooptions></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="pageheader" width="49%" colspan="3"><asp:label id="lblCreditWatch" runat="server">[lt] - Credit Watch</asp:label></td>
													</tr>
													<tr>
														<td width="49%" colspan="3">&nbsp;</td>
													</tr>
													<tr>
														<td class="light-dark-row" width="49%" colspan="3">
															<table id="Table8" cellspacing="0" cellpadding="0" width="97%" border="0">
																<tr>
																	<td width="40%">
																		<p><strong><asp:label id="lblWatchListRegistrationProcess" runat="server"> Watch list registration process </asp:label></strong></p>
																	</td>
																	<td align="right" width="60%"><asp:label id="lblOnWatch" runat="server" tooltip="This is the number of records already registered to your watch."></asp:label>&nbsp;/
																		<asp:label id="lblWatchLimit" runat="server" tooltip="this is your registration limit. Your watch can not hold more records than the indicated number."></asp:label></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td width="49%" colspan="3">&nbsp;</td>
													</tr>
													<tr>
														<td id="tdIDRegistration" valign="top" width="49%" runat="server">
															<table id="Table6" cellspacing="0" cellpadding="0" width="97%" border="0">
																<tr>
																	<td><asp:label id="lblIDRegistration" runat="server" font-bold="True" cssclass="subject">Register by ID / Regist</asp:label></td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td><asp:radiobutton id="rbtnCompanyID" runat="server" groupname="CompanyID" text="Company" checked="True" visible="false"></asp:radiobutton>&nbsp;
																		<asp:radiobutton id="rbtnIndividualID" runat="server" groupname="CompanyID" text="Individual" Visible="false"></asp:radiobutton></td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td><asp:label id="lblID" runat="server" width="100">ID</asp:label></td>
																</tr>
																<tr>
																	<td>
																		<p><asp:textbox id="txtID" runat="server" width="175px"></asp:textbox></p>
																	</td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td height="15">&nbsp;</td>
																</tr>
																<tr>
																	<td><asp:label id="lblFilePathToUpload" runat="server">FilePath</asp:label></td>
																</tr>
																<tr>
																	<td style="HEIGHT: 24px"><input class="rammi" id="myUniqueFile" style="WIDTH: 100%; HEIGHT: 22px" type="file" size="35"
																			name="myUniqueFile" runat="server"></td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td><asp:textbox id="txtbxUnique" runat="server" tooltip="You can insert multiple ID's into this textbox. Seperate them with a comma (,), a column (;) or a pipe (|)"
																			width="100%" height="150px" textmode="MultiLine"></asp:textbox></td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																</tr>
															</table>
															<table id="Table2" cellspacing="0" cellpadding="0" width="97%" border="0">
																<tr>
																	<td id="DeleteIDCell" style="WIDTH: 219px" align="right" runat="server">
																		<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnDeleteUnique" runat="server" tooltip="Only works with the ID textbox. Deletes one record from the watch if the ID matches"
																				cssclass="RegisterButton" text="Delete" visible="False"></asp:button></div>
																	</td>
																	<td align="right">
																		<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnRegisterUnique" runat="server" tooltip="Register unique ID(s)" cssclass="RegisterButton"
																				text="Register"></asp:button></div>
																	</td>
																	<td><A href='javascript:OpenHelpfileWithLanguage("<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name%>");' ><img height="20" src="img/question2.gif" width="12" border="0"></A></td>
																</tr>
															</table>
														</td>
														<td></td>
														<td id="tdRegisterByWord" valign="top" width="49%" runat="server">
															<table id="tblRegisterByWord" cellspacing="0" cellpadding="0" width="97%" border="0" runat="server">
																<tr>
																	<td colspan="2"><asp:label id="lblWordsRegistration" runat="server" font-bold="True" cssclass="subject">Register by words to watch</asp:label></td>
																</tr>
																<tr>
																	<td style="WIDTH: 193px">&nbsp;</td>
																	<td></td>
																</tr>
																<tr>
																	<td style="HEIGHT: 20px" colspan="2"><asp:radiobutton id="rbtnCompanyWord" runat="server" groupname="CompanyWord" text="Company" checked="True"></asp:radiobutton>&nbsp;&nbsp;
																		<asp:radiobutton id="rbtnIndividual" runat="server" groupname="CompanyWord" text="Individual"></asp:radiobutton></td>
																</tr>
																<tr>
																	<td style="WIDTH: 193px">&nbsp;</td>
																	<td></td>
																</tr>
																<tr>
																	<td style="WIDTH: 193px"><asp:label id="lblFirstName" runat="server">First Name</asp:label></td>
																	<td><asp:label id="lblSurName" runat="server">SurName</asp:label></td>
																</tr>
																<tr>
																	<td style="WIDTH: 193px"><asp:textbox id="txtFirstName" runat="server" width="175px"></asp:textbox></td>
																	<td><asp:textbox id="txtSurName" runat="server" width="175px"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 193px"><asp:label id="lblAddress" runat="server" width="120px">Address</asp:label></td>
																	<td><asp:label id="lblTown" runat="server">Town</asp:label></td>
																</tr>
																<tr>
																	<td style="WIDTH: 193px"><asp:textbox id="txtAddress" runat="server" width="175px"></asp:textbox></td>
																	<td><asp:textbox id="txtTown" runat="server" width="175px"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 193px">&nbsp;</td>
																	<td></td>
																</tr>
																<tr>
																	<td colspan="2"><asp:checkbox id="chkbxNative" runat="server" tooltip="checked if the registration is in the native language"
																			text="Native Registration" checked="True" width="136px"></asp:checkbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 193px">&nbsp;</td>
																	<td></td>
																</tr>
																<tr>
																	<td colspan="2"><asp:textbox id="txtbxWords" runat="server" tooltip="Use this textbox to insert multiple records. Each record must contain a name, a sur name, an AddressEN, a TownEN and an indication wheter the registration is for a company or an individual. Therefor each record is made up by five fields. Use a comma (,), a column (;) or a pipe (|) to seperate fields and records."
																			width="100%" height="150px" textmode="MultiLine"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 193px">&nbsp;</td>
																	<td></td>
																</tr>
															</table>
															<table id="Table3" cellspacing="0" cellpadding="0" width="97%" border="0">
																<tr>
																	<td id="DeleteWordsCell" style="WIDTH: 236px" align="right" runat="server">
																		<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnDeleteWords" runat="server" tooltip="Deletes one record from the watch. Only works with the textboxes that are used for single registration."
																				cssclass="RegisterButton" text="Delete" visible="False"></asp:button></div>
																	</td>
																	<td align="right">
																		<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnRegisterWords" runat="server" tooltip="Register to watch" cssclass="RegisterButton"
																				text="Register"></asp:button></div>
																	</td>
																	<td><A href='javascript:OpenHelpfileWithLanguage("<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name%>");' ><img height="20" src="img/question2.gif" width="12" border="0"></A></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td valign="top" width="49%">&nbsp;</td>
														<td></td>
														<td valign="top" width="49%"></td>
													</tr>
													<tr>
														<td valign="top" width="49%" colspan="3"><asp:label id="lblMessage" runat="server" font-bold="True" visible="False"></asp:label></td>
													</tr>
													<tr>
														<td valign="top" width="49%" bgcolor="#951e16" colspan="3" height="15"></td>
													</tr>
													<tr>
														<td valign="top" width="49%">&nbsp;</td>
														<td></td>
														<td valign="top" width="49%"></td>
													</tr>
												</table>
												<table id="Table4" cellspacing="0" cellpadding="0" width="97%" border="0">
													<tr>
														<td style="WIDTH: 635px" align="right"><asp:label id="lblPaging" runat="server" tooltip="Select number of rows to display at once in the datagrids">Datagrid Paging</asp:label>&nbsp;</td>
														<td style="WIDTH: 67px"><asp:dropdownlist id="ddlPaging" runat="server" autopostback="True">
																<asp:listitem value="10" selected="True">10</asp:listitem>
																<asp:listitem value="25">25</asp:listitem>
																<asp:listitem value="50">50</asp:listitem>
																<asp:listitem value="100">100</asp:listitem>
																<asp:listitem value="none">None</asp:listitem>
															</asp:dropdownlist></td>
														<td style="WIDTH: 145px" align="right"><div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnToggleList" runat="server" tooltip="Show lists for all registered parties"
																	cssclass="RegisterButton" text="Show List"></asp:button></div>
														</td>
														<td id="tdSearchClaims" runat="server" style="WIDTH: 114px" align="right"><div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnSearchClaims" runat="server" tooltip="Searches for claims registered to watched parties"
																	cssclass="RegisterButton" text="Search claims" causesvalidation="False"></asp:button></div>
														</td>
														<td>
															<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnDeleteAll" runat="server" tooltip="Delete the whole watch" cssclass="RegisterButton"
																	text="Delete all"></asp:button></div>
														</td>
													</tr>
													<tr>
														<td style="WIDTH: 635px" align="left">&nbsp;</td>
														<td style="WIDTH: 67px"></td>
														<td style="WIDTH: 145px" align="right"></td>
														<td style="WIDTH: 114px" align="right"></td>
														<td></td>
													</tr>
												</table>
												<table id="Table7" cellspacing="0" cellpadding="0" width="97%" border="0">
													<tr>
														<td class="light-dark-row" id="UniqueIDShowCell" valign="top" runat="server">
															<p><asp:label id="lblUniqueIDLists" runat="server" font-underline="True">Registered by unique ID</asp:label></p>
															<p><asp:label id="lblCompanies" runat="server"></asp:label><asp:customvalidator id="CustomValidator1" runat="server" enabled="False" errormessage="�etta er villan m�n"
																	clientvalidationfunction="CheckCompanyID" controltovalidate="txtID">*</asp:customvalidator></p>
															<p><asp:datagrid id="dgCompanies" runat="server" width="100%" allowsorting="True" autogeneratecolumns="False"
																	bordercolor="Gray" borderstyle="None" borderwidth="1px" backcolor="White" cellpadding="4"
																	allowpaging="True">
																	<footerstyle forecolor="#330099" backcolor="#FFFFCC"></footerstyle>
																	<selecteditemstyle font-bold="True" forecolor="#663399" backcolor="#FFCC66"></selecteditemstyle>
																	<alternatingitemstyle backcolor="#E0E0E0"></alternatingitemstyle>
																	<itemstyle forecolor="Black" backcolor="White"></itemstyle>
																	<headerstyle font-bold="True" forecolor="White" backcolor="#895A4F"></headerstyle>
																	<columns>
																		<asp:boundcolumn visible="False" datafield="CreditInfoID" headertext="ID"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="WatchID" headertext="WatchID"></asp:boundcolumn>
																		<asp:boundcolumn datafield="UniqueID" sortexpression="UniqueID" headertext="UniqueID"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="IsCompany" headertext="Company"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="Created" headertext="Created"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="UserID" sortexpression="UserID" headertext="UserID"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Name" sortexpression="Name" headertext="Name"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Address" sortexpression="Address" headertext="Address"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Sent" headertext="Sent"></asp:boundcolumn>
																		<asp:buttoncolumn text="Delete" commandname="Delete"></asp:buttoncolumn>
																		<asp:boundcolumn visible="False" datafield="sentDate" headertext="SentDate"></asp:boundcolumn>
																	</columns>
																	<pagerstyle horizontalalign="Center" forecolor="#330099" backcolor="#FFFFCC"></pagerstyle>
																</asp:datagrid></p>
															<p><asp:label id="lblIndividuals" runat="server"></asp:label></p>
															<p><asp:datagrid id="dgIndividuals" runat="server" width="100%" allowsorting="True" autogeneratecolumns="False"
																	bordercolor="Gray" borderstyle="None" borderwidth="1px" backcolor="White" cellpadding="4"
																	allowpaging="True">
																	<footerstyle forecolor="#330099" backcolor="#FFFFCC"></footerstyle>
																	<selecteditemstyle font-bold="True" forecolor="#663399" backcolor="#FFCC66"></selecteditemstyle>
																	<alternatingitemstyle backcolor="#E0E0E0"></alternatingitemstyle>
																	<itemstyle forecolor="Black" backcolor="White"></itemstyle>
																	<headerstyle font-bold="True" forecolor="White" backcolor="#895A4F"></headerstyle>
																	<columns>
																		<asp:boundcolumn visible="False" datafield="CreditInfoID" headertext="ID"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="WatchID" headertext="WatchID"></asp:boundcolumn>
																		<asp:boundcolumn datafield="UniqueID" sortexpression="UniqueID" headertext="UniqueID"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="IsCompany" headertext="Company"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="Created" headertext="Created"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="UserID" sortexpression="UserID" headertext="UserID"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Name" sortexpression="Name" headertext="Name"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Address" sortexpression="Address" headertext="Address"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Sent" headertext="Sent"></asp:boundcolumn>
																		<asp:buttoncolumn text="Delete" commandname="Delete"></asp:buttoncolumn>
																	</columns>
																	<pagerstyle horizontalalign="Center" forecolor="#330099" backcolor="#FFFFCC"></pagerstyle>
																</asp:datagrid></p>
														</td>
													</tr>
													<tr>
														<td class="light-dark-row" id="WordsShowCell" valign="top" runat="server">
															<p><asp:label id="lblWordsLists" runat="server" font-underline="True">Registered by words to watch</asp:label></p>
															<p><asp:label id="lblWordsCompanies" runat="server"></asp:label></p>
															<p><asp:datagrid id="dgWordsCompanies" runat="server" width="100%" allowsorting="True" autogeneratecolumns="False"
																	bordercolor="Gray" borderstyle="None" borderwidth="1px" backcolor="White" cellpadding="4"
																	allowpaging="True">
																	<footerstyle forecolor="#330099" backcolor="#FFFFCC"></footerstyle>
																	<selecteditemstyle font-bold="True" forecolor="#663399" backcolor="#FFCC66"></selecteditemstyle>
																	<alternatingitemstyle backcolor="#E0E0E0"></alternatingitemstyle>
																	<itemstyle forecolor="Black" backcolor="White"></itemstyle>
																	<headerstyle font-bold="True" forecolor="White" backcolor="#895A4F"></headerstyle>
																	<columns>
																		<asp:boundcolumn visible="False" datafield="CreditInfoID" headertext="ID"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="WatchID" headertext="WatchID"></asp:boundcolumn>
																		<asp:boundcolumn datafield="FirstName" sortexpression="FirstName" headertext="Name"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="SurName" sortexpression="SurName" headertext="Sur name"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Address" sortexpression="Address" headertext="Address"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Town" sortexpression="Town" headertext="Town/City"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="IsCompany" headertext="Company"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Sent" headertext="Sent"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="Created" headertext="Created"></asp:boundcolumn>
																		<asp:buttoncolumn text="Delete" commandname="Delete"></asp:buttoncolumn>
																		<asp:editcommandcolumn buttontype="LinkButton" updatetext="Update" canceltext="Cancel" edittext="Edit"></asp:editcommandcolumn>
																		<asp:boundcolumn visible="False" datafield="IsNative"></asp:boundcolumn>
																	</columns>
																	<pagerstyle horizontalalign="Center" forecolor="#330099" backcolor="#FFFFCC"></pagerstyle>
																</asp:datagrid></p>
															<p><asp:label id="lblWordsIndividuals" runat="server"></asp:label></p>
															<p><asp:datagrid id="dgWordsIndividuals" runat="server" width="100%" allowsorting="True" autogeneratecolumns="False"
																	bordercolor="Gray" borderstyle="None" borderwidth="1px" backcolor="White" cellpadding="4"
																	allowpaging="True">
																	<footerstyle forecolor="#330099" backcolor="#FFFFCC"></footerstyle>
																	<selecteditemstyle font-bold="True" forecolor="#663399" backcolor="#FFCC66"></selecteditemstyle>
																	<alternatingitemstyle backcolor="#E0E0E0"></alternatingitemstyle>
																	<itemstyle forecolor="Black" backcolor="White"></itemstyle>
																	<headerstyle font-bold="True" forecolor="White" backcolor="#895A4F"></headerstyle>
																	<columns>
																		<asp:boundcolumn visible="False" datafield="CreditInfoID" headertext="ID"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="WatchID" headertext="WatchID"></asp:boundcolumn>
																		<asp:boundcolumn datafield="FirstName" sortexpression="FirstName" headertext="First Name"></asp:boundcolumn>
																		<asp:boundcolumn datafield="SurName" sortexpression="SurName" headertext="Sur name"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Address" sortexpression="Address" headertext="Address"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Town" sortexpression="Town" headertext="Town/City"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="IsCompany" headertext="Company"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="Sent" headertext="Sent"></asp:boundcolumn>
																		<asp:boundcolumn visible="False" datafield="Created" headertext="Created"></asp:boundcolumn>
																		<asp:boundcolumn datafield="Sent" headertext="Sent"></asp:boundcolumn>
																		<asp:buttoncolumn text="Delete" commandname="Delete"></asp:buttoncolumn>
																		<asp:editcommandcolumn buttontype="LinkButton" updatetext="Update" canceltext="Cancel" edittext="Edit"></asp:editcommandcolumn>
																		<asp:boundcolumn visible="False" datafield="IsNative"></asp:boundcolumn>
																	</columns>
																	<pagerstyle horizontalalign="Center" forecolor="#330099" backcolor="#FFFFCC"></pagerstyle>
																</asp:datagrid></p>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor="transparent" height="6"></td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</html>
