#region

using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditWatch.BLL;
using CreditWatch.BLL.Localization;
using CreditWatch.BLL.np_Usage;

#endregion

using Cig.Framework.Base.Configuration;

namespace CreditWatch {
    /// <summary>
    /// Summary description for cwClaimList.
    /// </summary>
    public class cwClaimList : Page {
        // Fyrir multilanguage d�mi. 
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly cwFactory myFactory = new cwFactory();
        protected DataGrid dgSearchList;
        private bool EN;
        protected Label lblRegisteredClaimsText;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here

            // Fyrir multilanguage d�mi...
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();

            //if (culture.Equals("en-US"))
            //	EN = true;

            try {
                if (EN) {
                    using (DataSet mySet = myFactory.GetClaimListAsDataSet(Convert.ToInt32(Session["UserLoginID"]), 0)) {
                        dgSearchList.DataSource = mySet;
                        dgSearchList.DataBind();

                        // Athuga hvort �etta s� starfsma�ur sem er a� leita. Ef svo er �� ekkert log...
                        if (!myFactory.CheckForEmployee(Session["UserLoginName"].ToString())) {
                            LogThis(mySet);
                        }

                        Session["dgSearchList"] = mySet.Tables[0];
                    }
                } else {
                    using (DataSet mySet = myFactory.GetClaimListAsDataSet(Convert.ToInt32(Session["UserLoginID"]), 1)) {
                        dgSearchList.DataSource = mySet;
                        dgSearchList.DataBind();

                        // Athuga hvort �etta s� starfsma�ur sem er a� leita. Ef svo er �� ekkert log...
                        if (!myFactory.CheckForEmployee(Session["UserLoginName"].ToString())) {
                            LogThis(mySet);
                        }

                        Session["dgSearchList"] = mySet.Tables[0];
                    }
                }
            } catch (Exception err) {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwClaimList.aspx caught, message is : " + err.Message, true);
            }
        }

        private void LogThis(DataSet mySet) {
            // Loggun � flettingum...		
            var myLog = new np_UsageBLLC
                        {
                            CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]),
                            UserID = Convert.ToInt32(Session["UserLoginID"]),
                            QueryType = 11,
                            ResultCount = mySet.Tables[0].Rows.Count,
                            IP = Request.ServerVariables["REMOTE_ADDR"]
                        };

            // UserID nota� til a� a�greina f�rslur � loggun � milli notenda... tekur vi� af CreditInfoID
            // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)
            // QueryType segir til um uppruna og tegund loggunnar
            // Skila fj�lda flettinga
            // Tek IP t�luna fr� notanda

            if (!myFactory.AddNewUsageLog(myLog)) {
                Server.Transfer("error.aspx?err=1");
            }
        }

        private void dgSearchList_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                ((LinkButton) e.Item.Cells[3].Controls[0]).Text = rm.GetString("txtSelect", ci);
            }
        }

        private void LocalizeText() {
            lblRegisteredClaimsText.Text = rm.GetString("lbClaimListText", ci);

            dgSearchList.Columns[0].HeaderText = rm.GetString("lbName", ci);
            dgSearchList.Columns[1].HeaderText = rm.GetString("lbAddress", ci);
            dgSearchList.Columns[2].HeaderText = rm.GetString("lbClaimDate", ci);

            //this.dgSearchList.Columns[3].HeaderText = rm.GetString("lbClaimOwnerName",ci);
            //this.dgSearchList.Columns[4].HeaderText = rm.GetString("lbCaseType",ci);
        }

        private void dgSearchList_SortCommand(object source, DataGridSortCommandEventArgs e) {
            var myTable = (DataTable) Session["dgSearchList"];
            var myView = myTable.DefaultView;

            myView.Sort = e.SortExpression;
            dgSearchList.DataSource = myView;
            dgSearchList.DataBind();
        }

        private void cwClaimList_Unload(object sender, EventArgs e) {
            //Session.Remove("dgSearchList");
        }

        private void dgSearchList_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName == "Select") {
                string Name = e.Item.Cells[0].Text;
                string Address = e.Item.Cells[1].Text;
                string City = e.Item.Cells[4].Text;
                int CreditInfoID = Convert.ToInt32(e.Item.Cells[2].Text);

                Session["cwClaimSearchName"] = Name;
                Session["cwClaimSearchAddress"] = Address;
                Session["cwClaimSearchCreditInfoID"] = CreditInfoID;
                Session["cwClaimSearchCity"] = City;

                // Transfer to ClaimDetails page 

                /* Marek 30.3.2006
				 * Added configurable report url - used by CZ and SK version				 * 
				 */

                string url = CigConfig.Configure("lookupsettings.ReportDDDUrl");
                url = !string.IsNullOrEmpty(url) ? ResolveUrl(url) : "cwClaimSearchDetails.aspx";

                Server.Transfer(url);
            }
        }

        private void dgSearchList_PageIndexChanged(object source, DataGridPageChangedEventArgs e) {
            dgSearchList.CurrentPageIndex = e.NewPageIndex;

            var myTable = (DataTable) Session["dgSearchList"];
            var myView = myTable.DefaultView;

            dgSearchList.DataSource = myView;
            dgSearchList.DataBind();
        }

        private void dgSearchList_SelectedIndexChanged(object sender, EventArgs e) { }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dgSearchList.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgSearchList_ItemCommand);
            this.dgSearchList.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgSearchList_SortCommand);
            this.dgSearchList.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSearchList_ItemDataBound);
            this.dgSearchList.PageIndexChanged +=
                new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgSearchList_PageIndexChanged);
            this.dgSearchList.SelectedIndexChanged += new System.EventHandler(this.dgSearchList_SelectedIndexChanged);
            this.Unload += new System.EventHandler(this.cwClaimList_Unload);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}