<%@ Page language="c#" Codebehind="OldNames.aspx.cs" AutoEventWireup="false" Inherits="CreditWatch.OldNames" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OldNames</title>
		<LINK href="../css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="OldNames" method="post" runat="server">
			<asp:Label id="lblNewestName" runat="server" CssClass="subject" Font-Bold="True" Width="100%"></asp:Label>
			<br>
			<br>
			<asp:DataGrid id="dgOldNamesList" runat="server" CellPadding="4" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="Gray"
				Width="100%" AutoGenerateColumns="False">
				<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
				<AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="White"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#895A4F"></HeaderStyle>
				<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="Org_name" SortExpression="Org_name" HeaderText="Name"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="Org_name_status_code"></asp:BoundColumn>
					<asp:BoundColumn DataField="Reg_date" SortExpression="Reg_date" HeaderText="Reg. date" DataFormatString="{0:d}"></asp:BoundColumn>
					<asp:BoundColumn DataField="CompanyAddress" SortExpression="CompanyAddress" HeaderText="CompanyAddress"></asp:BoundColumn>
					<asp:BoundColumn DataField="Postcode" SortExpression="Postcode" HeaderText="Postcode"></asp:BoundColumn>
					<asp:BoundColumn DataField="City" SortExpression="City" HeaderText="City"></asp:BoundColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
			</asp:DataGrid>
		</form>
	</body>
</HTML>
