<%@ Page language="c#" Codebehind="UploadClaims.aspx.cs" AutoEventWireup="false" Inherits="CreditWatch.UploadClaims" %>
<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="../new_user_controls/FoOptions.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/FoHead.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Creditwatch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<style type="text/css"> BR.pageEnd { PAGE-BREAK-AFTER: always } </style>
		<script language="javascript"> 
				function checkEnterKey(i) 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						if(i==1)
							frmMain.btnRegisterUnique.click(); 
						else
							frmMain.btnRegisterWords.click(); 
					}
				} 
				
				function SetFormFocus()
				{
					//document.frmMain.txtID.focus();
				}
				
		</script>
	</head>
	<body ms_positioning="GridLayout" onload="SetFormFocus()" style="BACKGROUND-IMAGE: url(../img/mainback.gif)"
		leftmargin="0" rightmargin="0">
		<form id="frmMain" title="Credit Watch" name="Credit Watch" method="post" enctype="multipart/form-data"
			runat="server">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tbody>
					<tr>
						<td><uc1:head id="Head2" runat="server"></uc1:head></td>
					</tr>
					<tr>
						<td>
							<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
								<tr>
									<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
									<td>
										<table cellspacing="0" cellpadding="0" width="760" align="center" border="0" bgcolor="#951e16">
											<tr>
												<td bgcolor="#951e16"><span style="WIDTH: 3px"></span></td>
												<td bgcolor="#951e16"><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
												<td bgcolor="#951e16" align="right">
													<uc1:language id="Language2" runat="server"></uc1:language></td>
											</tr>
										</table>
									</td>
									<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table cellspacing="0" cellpadding="0" width="760" align="center" border="0" bgcolor="white">
								<tr>
									<td>
										<p>
											<table id="tbDefault" cellspacing="0" cellpadding="0" width="100%" align="center">
												<tr>
													<td>
														<table cellspacing="0" cellpadding="0" border="0" width="97%" align="center">
															<tr>
																<td>
																	<uc1:userinfo id="UserInfo1" runat="server"></uc1:userinfo>
																</td>
																<td align="right">
																	<uc1:fooptions id="FoOptions1" runat="server"></uc1:fooptions></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td>
														<table cellspacing="0" cellpadding="0" border="0" width="97%" align="center">
															<tr>
																<td class="pageheader" width="97%">
																	<asp:label id="lblCreditWatch" runat="server">[lt] - Credit Watch</asp:label></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td>
														<table id="tbContent" width="97%" align="center" cellspacing="5" cellpadding="5" border="0">
															<tr>
																<td valign="middle">
																	<p><strong><asp:label id="lblWatchListRegistrationProcess" runat="server"> Watch list registration process </asp:label></strong></p>
																</td>
															</tr>
															<tr class="light-dark-row">
																<td style="HEIGHT: 10px" colspan="1"></td>
															</tr>
															<tr>
																<td>
																	<table id="tblInsertFrame" bordercolor="#0000ff" cellspacing="0" cellpadding="3" width="100%"
																		align="center" bgcolor="#ffffff" border="0">
																		<tr>
																			<td style="WIDTH: 300px; HEIGHT: 215px" colspan="2"><input id="myUniqueFile" style="WIDTH: 263px; HEIGHT: 22px" type="file" size="24" name="myUniqueFile"
																					runat="server">
																				<table id="tblSingleInsert" bordercolor="#0000ff" cellspacing="0" cellpadding="0" align="left"
																					bgcolor="#ffffff" border="0">
																					<tr>
																						<td valign="bottom"></td>
																					</tr>
																				</table>
																				<p>&nbsp;</p>
																				<p><asp:listbox id="lbFiles" runat="server" width="467px"></asp:listbox></p>
																				<p>&nbsp;</p>
																				<p>
																					<asp:listbox id="listbUnique" runat="server" width="466px"></asp:listbox></p>
																				<p>&nbsp;</p>
																				<p>&nbsp;</p>
																			</td>
																		</tr>
																		<tr>
																			<td style="WIDTH: 300px; HEIGHT: 39px">
																				<table>
																					<tr>
																						<td>
																							<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btAdd" runat="server" cssclass="RegisterButton" text="Add"></asp:button></div>
																						</td>
																						<td>
																							<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnRemove" runat="server" cssclass="RegisterButton" text="Remove"></asp:button></div>
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="HEIGHT: 39px" align="right">
																				<table>
																					<tr>
																						<td>
																							<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnRegisterUnique" runat="server" cssclass="RegisterButton" tooltip="Upload selected files"
																									text="Upload files"></asp:button></div>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td style="WIDTH: 300px; HEIGHT: 25px">&nbsp;&nbsp;
																				<asp:label id="lbMessage" runat="server">Msg</asp:label><a id="help" href="javascript:OpenHelpfileWithLanguage();" runat="server"></a></td>
																			<td style="HEIGHT: 4px">&nbsp;<a id="help2" href="javascript:OpenHelpfileWithLanguage();" runat="server"></a></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<p></p>
													</td>
												</tr>
											</table>
										</p>
									</td>
								</tr>
								<tr>
									<td height="6" bgcolor="transparent"></td>
								</tr>
								<tr>
									<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
								</tr>
							</table>
		</form>
		</TD></TR></TBODY></TABLE>
	</body>
</html>
