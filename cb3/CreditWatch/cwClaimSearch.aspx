<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Page language="c#" Codebehind="cwClaimSearch.aspx.cs" AutoEventWireup="false" Inherits="CreditWatch.cwClaimSearch" %>
<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="../new_user_controls/FoOptions.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>ClaimSearch</title>
		<link href="../css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						Search.btnSearch.click(); 
					}
				} 
				
				function SetFormFocus()
				{
					document.Search.txtID.focus();
				}
				
		</script>
		<script language="JavaScript" src="popup.js"></script>
	</head>
	<body style="BACKGROUND-IMAGE: url(../img/mainback.gif)" onload="SetFormFocus()" ms_positioning="GridLayout"
		leftmargin="0" rightmargin="0">
		<form id="Search" method="post" runat="server">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head1" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
								<td>
									<table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="#951e16" border="0">
										<tr>
											<td bgcolor="#951e16"><span style="WIDTH: 3px"></span></td>
											<td bgcolor="#951e16"><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
											<td align="right" bgcolor="#951e16"><uc1:language id="Language2" runat="server"></uc1:language></td>
										</tr>
									</table>
								</td>
								<td style="BACKGROUND-IMAGE: url(../img/pagename_large_back.gif)" width="50%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="white" border="0">
							<tr>
								<td>
									<p>
										<table id="tbDefault" cellspacing="0" cellpadding="0" width="100%" align="center">
											<tr>
												<td>
													<table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
														<tr>
															<td><uc1:userinfo id="UserInfo1" runat="server"></uc1:userinfo></td>
															<td align="right">
																<uc1:fooptions id="FoOptions1" runat="server"></uc1:fooptions></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
														<tr>
															<td class="pageheader" width="97%"><asp:label id="lblSearchClaims" runat="server"> [lt] -Search claims</asp:label></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<table id="tbContent" cellspacing="5" cellpadding="5" width="97%" align="center" border="0">
														<tr>
															<td>
																<table id="tblBoxes" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
																	<tr>
																		<td><asp:textbox id="txtID" runat="server"></asp:textbox></td>
																		<td><asp:textbox id="txtFirstname" runat="server"></asp:textbox><asp:customvalidator id="CustomValidator1" runat="server" errormessage="*"></asp:customvalidator></td>
																		<td><asp:textbox id="txtSurname" runat="server"></asp:textbox><asp:customvalidator id="CustomValidator2" runat="server" errormessage="*"></asp:customvalidator></td>
																		<td><asp:textbox id="txtAddress" runat="server"></asp:textbox><asp:customvalidator id="CustomValidator3" runat="server" errormessage="*"></asp:customvalidator></td>
																	</tr>
																	<tr>
																		<td><asp:label id="lblID" runat="server">Registration no.</asp:label></td>
																		<td><asp:label id="lblFirstname" runat="server">Firstname</asp:label></td>
																		<td><asp:label id="lblSurname" runat="server">Surname</asp:label></td>
																		<td><asp:label id="lblAddress" runat="server">Address</asp:label></td>
																	</tr>
																	<tr>
																		<td></td>
																		<td colspan="3"><asp:label id="lblCompanyName" runat="server">Company name</asp:label></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td style="HEIGHT: 10px" colspan="1"></td>
														</tr>
														<tr>
															<td style="HEIGHT: 15px" bgcolor="#951e16" align="center" colspan="1">
															</td>
														</tr>
														<tr class="dark-row">
															<td style="HEIGHT: 10px" colspan="1"></td>
														</tr>
														<tr>
															<td>
																<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
																	<tr>
																		<td><asp:label id="lbErrMsg" runat="server" forecolor="Red" enableviewstate="False" visible="False">* at least one character input is required</asp:label>
																		</td>
																		<td align="right">
																			<div class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px">
																				<asp:button id="btnSearch" runat="server" text="Search" cssclass="RegisterButton"></asp:button></div>
																		</td>
																		<td align="right" width="3%"><a id="help" href="javascript:CreateWnd('explainSearch.htm', 500, 600, true);" runat="server">
																				<img height="20" src="img/question2.gif" width="12" border="0"></a></td>
																	</tr>
																</table>
															</td>
														<tr>
															<td style="HEIGHT: 14px" align="left"></td>
														</tr>
														<tr>
															<td style="HEIGHT: 32px" align="left"><asp:label id="lblRecordCount" runat="server" cssclass="subject"></asp:label><asp:label id="lblExplain" runat="server" cssclass="subject"></asp:label></td>
														</tr>
														<tr>
															<td align="left">
																<p>
																	<asp:datagrid id="dgSearchList" runat="server" autogeneratecolumns="False" allowsorting="True"
																		cellpadding="4" backcolor="White" borderwidth="1px" borderstyle="None" bordercolor="Gray"
																		width="100%" pagesize="3">
																		<selecteditemstyle font-bold="True" forecolor="#663399" backcolor="#FFCC66"></selecteditemstyle>
																		<alternatingitemstyle backcolor="#E0E0E0"></alternatingitemstyle>
																		<itemstyle forecolor="Black" backcolor="White"></itemstyle>
																		<headerstyle font-bold="True" forecolor="White" backcolor="#895A4F"></headerstyle>
																		<footerstyle forecolor="#330099" backcolor="#FFFFCC"></footerstyle>
																		<columns>
																			<asp:boundcolumn datafield="subject_name" sortexpression="subject_name" headertext="subject_name"></asp:boundcolumn>
																			<asp:boundcolumn datafield="address" sortexpression="address" headertext="address"></asp:boundcolumn>
																			<asp:boundcolumn visible="False" datafield="creditinfoid" sortexpression="creditinfoid" headertext="creditinfoid"></asp:boundcolumn>
																			<asp:boundcolumn datafield="Org_name_status_code" sortexpression="Org_name_status_code" headertext="Status"></asp:boundcolumn>
																			<asp:buttoncolumn text="Select" commandname="Select"></asp:buttoncolumn>
																			<asp:boundcolumn visible="False" datafield="city_name" sortexpression="city_name" headertext="city_name"></asp:boundcolumn>
																		</columns>
																		<pagerstyle horizontalalign="Center" forecolor="#330099" backcolor="#FFFFCC"></pagerstyle>
																	</asp:datagrid>
																</p>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="6" bgcolor="transparent"></td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>
</html>
