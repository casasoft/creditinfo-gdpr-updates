#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace CreditWatch {
    /// <summary>
    /// Summary description for error.
    /// </summary>
    public class error : Page {
        protected Label lbErrorMsg;
        protected Table Table2;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
            String errorType = Request.Params.Get("Err");
            lbErrorMsg.Text = ErrorMessage(Int32.Parse(errorType));
        }

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() { Load += Page_Load; }

        public String ErrorMessage(int errorType) {
            String errMsg;
            switch (errorType) {
                case 1:
                    errMsg = "An error occurred while registering to watchlist. Please notify the system administrator.";
                    break;
                case 2:
                    errMsg = "Error in data input. Please check your input and try again.";
                    break;
                case 3:
                    errMsg = "You must specify at least one item. Please check your input and try again.";
                    break;
                case 4:
                    errMsg =
                        "You have reached your registration limit. For further information contact the system administrator.";
                    break;
                case 5:
                    errMsg =
                        "Error occurred while trying to delete the record. For further information contact the system administrator.";
                    break;
                case 6:
                    errMsg =
                        "Error occurred while trying to updete the record. For further information contact the system administrator.";
                    break;
                default:
                    errMsg = "Error occurred. Please notify the system administrator.";
                    break;
            }
            return errMsg;
        }
    }
}