#region

using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CreditWatch.BLL;
using CreditWatch.BLL.Localization;
using CreditWatch.BLL.WatchUniqueID;
using CreditWatch.BLL.WatchWords;
using System.Web.Security;

using Cig.Framework.Base.Configuration;

#endregion

namespace CreditWatch {
    /// <summary>
    /// Summary description for cwOverView.
    /// </summary>
    public class cwOverView : Page
    {
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly cwFactory myFactory = new cwFactory();
        protected Button btnDeleteAll;
        protected Button btnDeleteUnique;
        protected Button btnDeleteWords;
        protected Button btnRegisterUnique;
        protected Button btnRegisterWords;
        protected Button btnSearchClaims;
        protected Button btnToggleList;
        protected CheckBox chkbxNative;
        protected CustomValidator CustomValidator1;
        protected DropDownList ddlPaging;
        protected HtmlTableCell DeleteIDCell;
        protected HtmlTableCell DeleteWordsCell;
        protected DataGrid dgCompanies;
        protected DataGrid dgIndividuals;
        protected DataGrid dgWordsCompanies;
        protected DataGrid dgWordsIndividuals;
        protected HtmlForm frmMain;
        protected Label lblAddress;
        protected Label lblCompanies;
        protected Label lblCreditWatch;
        protected Label lblFilePathToUpload;
        protected Label lblFirstName;
        protected Label lblID;
        protected Label lblIDRegistration;
        protected Label lblIndividuals;
        protected Label lblMessage;
        protected Label lblOnWatch;
        protected Label lblPaging;
        protected Label lblSurName;
        protected Label lblTown;
        protected Label lblUniqueIDLists;
        protected Label lblWatchLimit;
        protected Label lblWatchListRegistrationProcess;
        protected Label lblWordsCompanies;
        protected Label lblWordsIndividuals;
        protected Label lblWordsLists;
        protected Label lblWordsRegistration;
        protected HtmlInputFile myUniqueFile;
        protected RadioButton rbtnCompanyID;
        protected RadioButton rbtnCompanyWord;
        protected RadioButton rbtnIndividual;
        protected RadioButton rbtnIndividualID;
        protected HtmlTable tblRegisterByWord;
        protected HtmlTableCell tdIDRegistration;
        protected HtmlTableCell tdRegisterByWord;
        protected HtmlTableCell tdSearchClaims;
        protected TextBox txtAddress;
        protected TextBox txtbxUnique;
        protected TextBox txtbxWords;
        protected TextBox txtFirstName;
        protected TextBox txtID;
        protected TextBox txtSurName;
        protected TextBox txtTown;
        protected HtmlTableCell UniqueIDShowCell;
        protected HtmlTableCell WordsShowCell;

        private bool IsMalta
        {
            get
            {
                if (CigConfig.Configure("lookupsettings.currentVersion") != null)
                {
                    if (CigConfig.Configure("lookupsettings.currentVersion").Equals("malta"))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserLoginID"] == null || Session["UserLoginID"].ToString() == string.Empty)
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session.Abandon();
                Response.Redirect("~/fodefault.aspx");
                Response.End();
            }

            CheckForAccess();
            Server.ScriptTimeout = 300;

            // Put user code to initialize the page here

            // TODO: Notast vi� �etta �ar til a�gangsst�ring er komin...
            // Session.Add("CreditInfoID",1);
            // Session.Add("CreditWatch",100);

            // b�ta <ENTER> event handler � textboxin
            AddEnterEvent();
            DeleteWordsCell.Visible = false;
            DeleteIDCell.Visible = false;

            // Fyrir multilanguage d�mi...
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();

            if (CigConfig.Configure("lookupsettings.currentVersion") != null)
            {
                if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech") ||
                    CigConfig.Configure("lookupsettings.currentVersion").Equals("slovakia"))
                {
                    btnRegisterUnique.Attributes["onclick"] =
                        "javascript: return CheckID(txtID, rbtnCompanyID, txtbxUnique, myUniqueFile, '" +
                        rm.GetString("txtIllegalFormatOfID", ci) + "');";
                }
                else if (CigConfig.Configure("lookupsettings.currentVersion").Equals("georgia"))
                {
                    btnRegisterUnique.Attributes["onclick"] =
                        "javascript: return CheckGeorgiaID(txtID, rbtnCompanyID, txtbxUnique, myUniqueFile, '" +
                        rm.GetString("txtIllegalFormatOfID", ci) + "');";
                }
                else if (CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus"))
                {
                    btnRegisterUnique.Attributes["onclick"] =
                        "javascript: return CheckCyprusID(txtID, rbtnCompanyID, txtbxUnique, myUniqueFile, '" +
                        rm.GetString("txtIllegalFormatOfID", ci) + "');";
                }
                else if (IsMalta)
                {
                    btnRegisterUnique.Attributes["onclick"] =
                        "javascript: return CheckMaltaID(txtID, rbtnCompanyID, txtbxUnique, myUniqueFile, '" +
                        rm.GetString("txtIllegalFormatOfID", ci) + "');";
                }

                if (CigConfig.Configure("lookupsettings.currentVersion").Equals("lt"))
                {
                    HideRegisterByWord();
                    tdIDRegistration.Width = "100%";
                }
            }

            btnRegisterWords.Attributes["onclick"] = "javascript: return CheckSurname(txtSurName, rbtnCompanyWord, '" +
                                                     rm.GetString("txtMustRegisterSurname", ci) + "');";

            lblMessage.Visible = false;

            if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech") ||
                CigConfig.Configure("lookupsettings.currentVersion").Equals("slovakia") || IsMalta)
            {
                HideIndividualControls();
            }

            if (IsMalta)
            {
                HideRegisterByWord();
            }
            lblWordsLists.Visible = false;

            if (!IsPostBack)
            {
                lblWatchLimit.Text = rm.GetString("lbWatchLimit", ci) + Convert.ToString(Session["CreditWatch"]);

                btnDeleteAll.Attributes["onclick"] = "javascript: return confirm('" +
                                                     rm.GetString("txtConfirmDelete", ci) + "');";
                btnDeleteUnique.Attributes["onclick"] = "javascript: return confirm('" +
                                                        rm.GetString("txtConfirmDelete", ci) + "');";
                btnDeleteWords.Attributes["onclick"] = "javascript: return confirm('" +
                                                       rm.GetString("txtConfirmDelete", ci) + "');";

                InitControls();
                Session.Add("viewState", false);
            }

            if (IsMalta)
            {
                tdSearchClaims.Visible = false;
            }
        }

        /// <summary>
        /// Check user access rights for the url he is currently trying to access
        /// NOTE: THIS IS A FIX ONLY AND SHOULD BE REMOVED WHEN FRONT END IS UPDATED!!!
        /// </summary>
        public void CheckForAccess()
        {
            // Set current page and base path
            String path = CigConfig.Configure("lookupsettings.rootURL");

            // Check user access rights
            if (!Context.User.IsInRole("300"))
            {
                Session["noAuthorizationPage"] = Request.FilePath;
                Response.Redirect(path + "FoNoAuthorization.aspx", true);
            }
        }

        private void AddEnterEvent()
        {
            // submit unique list
            txtID.Attributes.Add("onkeypress", "checkEnterKey(1);");

            // submit fuzzy list
            txtFirstName.Attributes.Add("onkeypress", "checkEnterKey(2);");
            txtSurName.Attributes.Add("onkeypress", "checkEnterKey(2);");
            txtAddress.Attributes.Add("onkeypress", "checkEnterKey(2);");
            txtTown.Attributes.Add("onkeypress", "checkEnterKey(2);");
        }

        private void dgCompanies_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            e.Item.Cells[0].Visible = false; 

            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                if (e.Item.Cells[8].Text.Trim() == "True")
                {
                    e.Item.Cells[8].Text = rm.GetString("txtSent", ci);
                    if (IsMalta)
                    {
                        if (e.Item.Cells[10].Text != "" || e.Item.Cells[10].Text != "&nbsp;")
                        {
                            e.Item.Cells[8].Text += " " + e.Item.Cells[10].Text;
                        }
                    }
                }
                else
                {
                    e.Item.Cells[8].Text = rm.GetString("txtMessageNotSent", ci);
                }
                if (IsMalta)
                {
                    var temp = e.Item.Cells[2].Text;
                    e.Item.Cells[2].Text = "<A HREF='../FoSearch.aspx?TAFReport=true&reqNationalID=" +
                                           e.Item.Cells[2].Text + "'>" + temp + "</a>";
                }
                LinkButton btnTemp = (LinkButton)e.Item.Cells[9].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                //	((LinkButton)e.Item.Cells[8].Controls[0]).Text = rm.GetString("txtDelete",ci);
                ((LinkButton)e.Item.Cells[9].Controls[0]).Text = rm.GetString("txtDelete", ci);
            }
        }

        private void dgWordsCompanies_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                if (e.Item.Cells[7].Text == "True")
                {
                    e.Item.Cells[7].Text = rm.GetString("txtMessageSent", ci);
                }
                else
                {
                    e.Item.Cells[7].Text = rm.GetString("txtMessageNotSent", ci);
                }

                LinkButton btnTemp = (LinkButton)e.Item.Cells[9].Controls[0];
                btnTemp.Attributes["onclick"] = "javascript:return confirm('" + rm.GetString("txtConfirmDelete", ci) +
                                                "')";
                ((LinkButton)e.Item.Cells[9].Controls[0]).Text = rm.GetString("txtDelete", ci);
                ((LinkButton)e.Item.Cells[10].Controls[0]).Text = rm.GetString("txtUpdate", ci);
            }
        }

        private void dgIndividuals_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                if (e.Item.Cells[8].Text == "True")
                {
                    e.Item.Cells[8].Text = rm.GetString("txtMessageSent", ci);
                }
                else
                {
                    e.Item.Cells[8].Text = rm.GetString("txtMessageNotSent", ci);
                }

                ((LinkButton)e.Item.Cells[9].Controls[0]).Text = rm.GetString("txtDelete", ci);
            }
        }

        private void dgWordsIndividuals_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                if (e.Item.Cells[8].Text == "True")
                {
                    e.Item.Cells[8].Text = rm.GetString("txtMessageSent", ci);
                }
                else
                {
                    e.Item.Cells[8].Text = rm.GetString("txtMessageNotSent", ci);
                }

                ((LinkButton)e.Item.Cells[10].Controls[0]).Text = rm.GetString("txtDelete", ci);
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteUnique.Click += new System.EventHandler(this.btnDeleteUnique_Click);
            this.btnRegisterUnique.Click += new System.EventHandler(this.btnRegisterUnique_Click);
            this.btnDeleteWords.Click += new System.EventHandler(this.btnDeleteWords_Click);
            this.btnRegisterWords.Click += new System.EventHandler(this.btnRegisterWords_Click);
            this.ddlPaging.SelectedIndexChanged += new System.EventHandler(this.ddlPaging_SelectedIndexChanged);
            this.btnToggleList.Click += new System.EventHandler(this.btnToggleList_Click);
            this.btnSearchClaims.Click += new System.EventHandler(this.btnSearchClaims_Click);
            this.btnDeleteAll.Click += new System.EventHandler(this.btnDeleteAll_Click);
            this.dgCompanies.ItemCreated +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCompanies_ItemCreated);
            this.dgCompanies.PageIndexChanged +=
                new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgCompanies_PageIndexChanged);
            this.dgCompanies.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgCompanies_SortCommand);
            this.dgCompanies.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCompanies_DeleteCommand);
            this.dgCompanies.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgCompanies_ItemDataBound);
            this.dgIndividuals.ItemCreated +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgIndividuals_ItemCreated);
            this.dgIndividuals.PageIndexChanged +=
                new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgIndividuals_PageIndexChanged);
            this.dgIndividuals.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgIndividuals_SortCommand);
            this.dgIndividuals.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgIndividuals_DeleteCommand);
            this.dgIndividuals.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgIndividuals_ItemDataBound);
            this.dgWordsCompanies.ItemCreated +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgWordsCompanies_ItemCreated);
            this.dgWordsCompanies.PageIndexChanged +=
                new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgWordsCompanies_PageIndexChanged);
            this.dgWordsCompanies.CancelCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgWordsCompanies_CancelCommand);
            this.dgWordsCompanies.EditCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgWordsCompanies_EditCommand);
            this.dgWordsCompanies.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgWordsCompanies_SortCommand);
            this.dgWordsCompanies.UpdateCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgWordsCompanies_UpdateCommand);
            this.dgWordsCompanies.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgWordsCompanies_DeleteCommand);
            this.dgWordsCompanies.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgWordsCompanies_ItemDataBound);
            this.dgWordsIndividuals.ItemCreated +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgWordsIndividuals_ItemCreated);
            this.dgWordsIndividuals.PageIndexChanged +=
                new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgWordsIndividuals_PageIndexChanged);
            this.dgWordsIndividuals.CancelCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgWordsIndividuals_CancelCommand);
            this.dgWordsIndividuals.EditCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgWordsIndividuals_EditCommand);
            this.dgWordsIndividuals.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgWordsIndividuals_SortCommand);
            this.dgWordsIndividuals.UpdateCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgWordsIndividuals_UpdateCommand);
            this.dgWordsIndividuals.DeleteCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgWordsIndividuals_DeleteCommand);
            this.dgWordsIndividuals.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgWordsIndividuals_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

        #region Hj�lparf�ll

        /// <summary>
        /// N�llstillir textasv��i, hnappa o.fl.
        /// </summary>
        private void PrepareControlsForRegistration()
        {
            txtID.Text = "";
            txtFirstName.Text = "";
            txtSurName.Text = "";
            txtAddress.Text = "";
            txtTown.Text = "";
            rbtnCompanyWord.Checked = true;
            rbtnCompanyID.Checked = true;
            chkbxNative.Checked = true;
        }

        /// <summary>
        /// Setur upphafsgildi fyrir formi�
        /// </summary>
        private void InitControls()
        {
            UpdateRecordCount();
            PrepareControlsForRegistration();
            btnToggleList.Text = rm.GetString("txtShowList", ci);

            // �etta eru sellur sem DataGridin eru sta�sett � (sett � runat = "server")
            UniqueIDShowCell.Visible = false;
            WordsShowCell.Visible = false;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null)
            {
                /*
				if(CigConfig.Configure("lookupsettings.currentVersion").ToString().Equals("czech"))
				{
					((BoundColumn)dgCompanies.Columns[7]).DataField = "City";
				}
				*/
            }
        }

        /// <summary>
        /// Ra�ar DataTable eftir gefnu heiti d�lks
        /// </summary>
        /// <param name="dt">DataTable sem skal ra�a (ath reference f�ribreyta)</param>
        /// <param name="SortField">D�lkur sem skal ra�a eftir �samt ASC e�a DESC</param>
        private void SortDataTable(ref DataTable dt, string SortField)
        {
            if (Convert.ToBoolean(Session["viewState"]))
            {
                DataRow[] sortedRows;
                sortedRows = dt.Select("", SortField);

                //b�a til n�ja t�flu, ef dt er hreinsu� og reynt a� b�ta � hana �r sortedRows f� DataRows �ar RowState=Detached og ekki h�gt a� importa aftur
                DataTable dt2 = dt.Clone();
                foreach (DataRow r in sortedRows)
                {
                    dt2.ImportRow(r);
                }
                dt = dt2;
            }
        }

        /// <summary>
        /// S�r um a� setja datasource � datagrid en a�eins ef �au eru s�nileg. Setur 4 datagrid.
        /// </summary>
        private void RefreshDataSetThingy()
        {
            if (Convert.ToBoolean(Session["viewState"]))
            {
                // Fyrir l�ga�ila me� einkv�mt n�mer
                using (
                    DataSet mySet =
                        myFactory.GetUniqueOnWatchListWithNameAndAddressForUserAsDataset(
                            Convert.ToInt32(Session["UserLoginID"]), true))
                {
                    if (mySet == null || mySet.Tables.Count == 0 || mySet.Tables[0].Rows.Count <= 0)
                    {
                        dgCompanies.Visible = false;
                        lblCompanies.Visible = false;
                    }
                    else
                    {
                        dgCompanies.Visible = true;
                        if (!IsMalta)
                        {
                            lblCompanies.Visible = true;
                        }
                        dgCompanies.DataSource = mySet;
                        dgCompanies.DataBind();
                        Session["dgCompanies"] = mySet.Tables[0];
                    }
                    if (IsMalta)
                    {
                        dgCompanies.Columns[0].Visible = true;
                    }

                    // this.dgCompanies.Columns[6].Visible = false;
                    // this.dgCompanies.Columns[7].Visible = false;
                }

                // Fyrir einstaklinga me� einkv�mt n�mer
                using (
                    DataSet mySet =
                        myFactory.GetUniqueOnWatchListWithNameAndAddressForUserAsDataset(
                            Convert.ToInt32(Session["UserLoginID"]), false))
                {
                    if (mySet.Tables[0].Rows.Count <= 0)
                    {
                        dgIndividuals.Visible = false;
                        lblIndividuals.Visible = false;
                    }
                    else
                    {
                        dgIndividuals.Visible = true;
                        lblIndividuals.Visible = true;
                        dgIndividuals.DataSource = mySet;
                        dgIndividuals.DataBind();
                        Session["dgIndividuals"] = mySet.Tables[0];
                    }
                    // this.dgIndividuals.Columns[6].Visible = false;
                    // this.dgIndividuals.Columns[7].Visible = false;
                }

                // Fyrir l�ga�ila me� stikkor�
                using (
                    DataSet mySet = myFactory.GetTrimmedWordsOnWatchList(
                        (Convert.ToInt32(Session["UserLoginID"])), true))
                {
                    if (mySet.Tables.Count > 1)
                    {
                        if (mySet.Tables[1].Rows.Count <= 0)
                        {
                            dgWordsCompanies.Visible = false;
                            lblWordsCompanies.Visible = false;
                        }
                        else
                        {
                            dgWordsCompanies.Visible = true;
                            if (!IsMalta)
                            {
                                lblWordsCompanies.Visible = true;
                            }
                            dgWordsCompanies.DataSource = mySet.Tables[1];
                            dgWordsCompanies.DataBind();
                            Session["dgWordsCompanies"] = mySet.Tables[1];
                        }
                    }
                }

                // Fyrir einstaklinga me� einkv�mt n�mer
                using (
                    DataSet mySet = myFactory.GetTrimmedWordsOnWatchList(
                        (Convert.ToInt32(Session["UserLoginID"])), false))
                {
                    if (mySet.Tables.Count > 1)
                    {
                        if (mySet.Tables[1].Rows.Count <= 0)
                        {
                            dgWordsIndividuals.Visible = false;
                            lblWordsIndividuals.Visible = false;
                        }
                        else
                        {
                            dgWordsIndividuals.Visible = true;
                            lblWordsIndividuals.Visible = true;
                            dgWordsIndividuals.DataSource = mySet.Tables[1];
                            dgWordsIndividuals.DataBind();
                            Session["dgWordsIndividuals"] = mySet.Tables[1];
                        }
                    }
                }
            }
            else
            {
                dgCompanies.Dispose();
                dgIndividuals.Dispose();
                dgWordsCompanies.Dispose();
                dgWordsIndividuals.Dispose();
            }
        }

        /// <summary>
        /// Hide all individuals controls
        /// </summary>
        private void HideIndividualControls()
        {
            //rbtnIndividual.Visible = false;
            //rbtnIndividualID.Visible = false;
            txtSurName.Visible = false;
            lblSurName.Visible = false;
            rbtnCompanyID.Checked = true; // make sure that this is company monitoring
            rbtnCompanyWord.Checked = true; // ----
            //  rbtnCompanyID.Visible = false;
            //rbtnCompanyWord.Visible = false;
            lblWordsIndividuals.Visible = false;
            dgIndividuals.Visible = false;
            dgWordsIndividuals.Visible = false;
            lblFirstName.Text = rm.GetString("lbCompanyName", ci);
            if (IsMalta)
            {
                lblCompanies.Visible = false;
                lblWordsCompanies.Visible = false;
            }
        }

        private void HideRegisterByWord()
        {
            //tblRegisterByWord.Visible=false;
            tdRegisterByWord.Visible = false;
        }

        /// <summary>
        /// S�r um a� updeita alla lables me� talningu.
        /// </summary>
        private void UpdateRecordCount()
        {
            lblOnWatch.Text = rm.GetString("lbOnWatch", ci) +
                              Convert.ToString(myFactory.CountWatch(Convert.ToInt32(Session["UserLoginID"])));
            lblCompanies.Text = rm.GetString("lbCompanies", ci) + " (" +
                                Convert.ToString(myFactory.CountUniqueID(Convert.ToInt32(Session["UserLoginID"]), true)) +
                                ")";
            lblIndividuals.Text = rm.GetString("lbIndividuals", ci) + " (" +
                                  Convert.ToString(
                                      myFactory.CountUniqueID(Convert.ToInt32(Session["UserLoginID"]), false)) + ")";
            lblWordsCompanies.Text = rm.GetString("lbCompanies", ci) + " (" +
                                     Convert.ToString(
                                         myFactory.CountWatchWords(Convert.ToInt32(Session["UserLoginID"]), true)) + ")";
            lblWordsIndividuals.Text = rm.GetString("lbIndividuals", ci) + " (" +
                                       Convert.ToString(
                                           myFactory.CountWatchWords(Convert.ToInt32(Session["UserLoginID"]), false)) +
                                       ")";
        }

        #endregion

        #region Hnappar � forminu

        /// <summary>
        /// S�r um a� s�na lista yfir einstaklinga og fyrirt�ki og breyta skj�myndinni � samr�mi vi� �a�.
        /// S�r um a� setja g�gn � datagrids o.s.frv.
        /// </summary>
        private void btnToggleList_Click(object sender, EventArgs e)
        {
            // �egar s�na � lista...
            //TODO: G�ti veri� r�� a� setja upp s�rstakan event handler sem segir til um �a� hvort onshow s� � gangi e�a ekki
            if (!Convert.ToBoolean(Session["viewState"]))
            {
                Session["viewState"] = true;
                RefreshDataSetThingy();
                UniqueIDShowCell.Visible = true;
                WordsShowCell.Visible = true;
                btnToggleList.Text = rm.GetString("txtHideList", ci);
            }
            // �egar fela � allt "drasli�"...
            else
            {
                Session["viewState"] = false;
                btnToggleList.Text = rm.GetString("txtShowList", ci);
                UniqueIDShowCell.Visible = false;
                WordsShowCell.Visible = false;
            }
        }

        /// <summary>
        /// Ey�ir �llum skr�ningum af vakt. �essu event er ekki hleypt af nema js confirm gluggi skili "true" gildi
        /// til baka. JavaScript glugginn er festur vi� eventi� �egar forminu er hla�i� inn (load).
        /// </summary>
        private void btnDeleteAll_Click(object sender, EventArgs e)
        {
            if (!myFactory.DeleteWatch(Convert.ToInt32(Session["UserLoginID"])))
            {
                DisplayErrorMessage(rm.GetString("txtErrorDeleting", ci));
                //Server.Transfer("error.aspx?err=5");
            }
            else
            {
                RefreshDataSetThingy();
                UpdateRecordCount();
            }
        }

        /// <summary>
        /// Sendir eina staka kt ni�ur � gagnagrunn.
        /// </summary>
        private void btnRegisterUnique_Click(object sender, EventArgs e)
        {
            // Ef �a� er eitthva� � sv��inu �� viljum vi� gera eitthva�...
            if (txtID.Text != "")
            {
                RegisterOneUniqueID();
            }

            // Ef eitthva� er � textbox...
            if (txtbxUnique.Text != "")
            {
                RegisterUniqueIDFromTextBox();
            }

            if (myUniqueFile.Value != "")
            {
                HandleFileWithUniqueID();
            }

            // Uppf�ra talningu � fj�lda skr�ninga � vakt
            RefreshDataSetThingy();
            UpdateRecordCount();
        }

        /// <summary>
        /// Skr�ning � stikkor�um � gagnagrunn.
        /// </summary>
        private void btnRegisterWords_Click(object sender, EventArgs e)
        {
            // Ef �a� er eitthva� � sv��inu �� viljum vi� gera eitthva�...
            if (txtFirstName.Text != "")
            {
                RegisterOneByWordsID();
            }

            // Ef eitthva� er � textbox...
            if (txtbxWords.Text != "")
            {
                RegisterWordsFromTextBox();
            }

            // Uppf�ra talningu � fj�lda skr�ninga � vakt
            RefreshDataSetThingy();
            UpdateRecordCount();
        }

        private void btnSearchClaims_Click(object sender, EventArgs e)
        {
            string myScript = "<script language=Javascript>CreateWnd('cwClaimList.aspx', 800, 900, true);</script>";
            Page.RegisterClientScriptBlock("alert", myScript);
        }

        private void btnDeleteUnique_Click(object sender, EventArgs e)
        {
            if (txtID.Text != "")
            {
                if (!myFactory.DeleteUniqueWatchRecordByUniqueID(txtID.Text))
                {
                    DisplayErrorMessage(rm.GetString("txtErrorDeleting", ci));
                    //Server.Transfer("error.aspx?err=5");
                }
                else
                {
                    RefreshDataSetThingy();
                    UpdateRecordCount();
                }
            }
        }

        private void btnDeleteWords_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text == "")
            {
                return;
            }

            if (
                !myFactory.DeleteWatchWordsRecordByWatchWords(
                     Convert.ToInt32(Session["UserLoginID"]),
                     txtFirstName.Text,
                     txtSurName.Text,
                     txtAddress.Text,
                     txtTown.Text,
                     rbtnCompanyWord.Checked.ToString(),
                     chkbxNative.Checked))
            {
                DisplayErrorMessage(rm.GetString("txtErrorDeleting", ci));
                //Server.Transfer("error.aspx?err=5");
            }
            else
            {
                RefreshDataSetThingy();
                UpdateRecordCount();
            }
        }

        private void ddlPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlPaging.SelectedValue)
            {
                case "none":
                    {
                        dgCompanies.AllowPaging = false;
                        dgIndividuals.AllowPaging = false;
                        dgWordsCompanies.AllowPaging = false;
                        dgWordsIndividuals.AllowPaging = false;
                        RefreshDataSetThingy();
                        break;
                    }
                case "10":
                    {
                        dgCompanies.AllowPaging = true;
                        dgIndividuals.AllowPaging = true;
                        dgWordsCompanies.AllowPaging = true;
                        dgWordsIndividuals.AllowPaging = true;
                        dgCompanies.PageSize = 10;
                        dgIndividuals.PageSize = 10;
                        dgWordsCompanies.PageSize = 10;
                        dgWordsIndividuals.PageSize = 10;
                        RefreshDataSetThingy();
                        break;
                    }
                case "25":
                    {
                        dgCompanies.AllowPaging = true;
                        dgIndividuals.AllowPaging = true;
                        dgWordsCompanies.AllowPaging = true;
                        dgWordsIndividuals.AllowPaging = true;
                        dgCompanies.PageSize = 25;
                        dgIndividuals.PageSize = 25;
                        dgWordsCompanies.PageSize = 25;
                        dgWordsIndividuals.PageSize = 25;
                        RefreshDataSetThingy();
                        break;
                    }
                case "50":
                    {
                        dgCompanies.AllowPaging = true;
                        dgIndividuals.AllowPaging = true;
                        dgWordsCompanies.AllowPaging = true;
                        dgWordsIndividuals.AllowPaging = true;
                        dgCompanies.PageSize = 50;
                        dgIndividuals.PageSize = 50;
                        dgWordsCompanies.PageSize = 50;
                        dgWordsIndividuals.PageSize = 50;
                        RefreshDataSetThingy();
                        break;
                    }
                case "100":
                    {
                        dgCompanies.AllowPaging = true;
                        dgIndividuals.AllowPaging = true;
                        dgWordsCompanies.AllowPaging = true;
                        dgWordsIndividuals.AllowPaging = true;
                        dgCompanies.PageSize = 100;
                        dgIndividuals.PageSize = 100;
                        dgWordsCompanies.PageSize = 100;
                        dgWordsIndividuals.PageSize = 100;
                        RefreshDataSetThingy();
                        break;
                    }
            }
        }

        #endregion

        #region Registering UniqueID

        private void HandleFileWithUniqueID()
        {
            // Vista skr� � server, taka g�gn upp �r henni og ey�a svo skr�nni...

            try
            {
                // StramReader sem beinist a� skr�nni sem veri� er a� reyna a� vinna me� (innihaldi)
                StreamReader myStreamReader = new StreamReader(myUniqueFile.PostedFile.InputStream);
                char[] mySeperatorArray = new[] { ';', '?', '|', '\n', ',' };
                // Tek textann �r skr�nni, splitta honum upp samkv. leyfilegum "seperators og skila string array.
                // �arf a� setja inn me�h�ndlun � l�nubilum (\r\n)
                //string[] myStringArray = myStreamReader.ReadToEnd().Replace('\r',',').Split(mySeperatorArray);
                string[] myStringArray = myStreamReader.ReadToEnd().Replace('\r', ' ').Trim().Split(mySeperatorArray);
                // af hverju er �etta ekki sent beint � db? L�kt og h�r?				
                RegisterUniqueIDFromStringArray(myStringArray);

                //	for (int i=0; i<myStringArray.Length; i++)
                //	{
                //		txtbxUnique.Text += myStringArray[i];
                //	}
                DisplayMessage(rm.GetString("txtInformationUploaded", ci));
            }
            catch
            {
                DisplayErrorMessage(rm.GetString("txtErrorReadingFile", ci) + " " + myUniqueFile.Value);
            }
        }

        /// <summary>
        /// Kalla� � �etta fall �egar skr� � eina staka kennit�lu ni�ur � grunn.
        /// </summary>
        private void RegisterOneUniqueID()
        {
            WatchUniqueIDBLLC myWatch = new WatchUniqueIDBLLC();

            // Ef vi�komandi skr�ning reynir a� fara yfir leyfilegt h�mark...
            if (myFactory.CountWatch(Convert.ToInt32(Session["UserLoginID"])) >= Convert.ToInt32(Session["CreditWatch"]))
            {
                DisplayErrorMessage(rm.GetString("txtRegistrationLimitReached", ci));
                return;
                //Server.Transfer("error.aspx?err=4");
            }

            // Ef "kennitala" finnst �� �tlum vi� a� reyna a� skr� n�ja f�rslu � cw_WatchUniqueID
            try
            {
                // Sett inn � "hlut" sem stendur fyrir eina f�rslu � vaktaskr�...
                myWatch.CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]);
                // UserID nota� til a� a�greina f�rslur � vakt � milli notenda... tekur vi� af CreditInfoID
                // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)
                myWatch.UserID = Convert.ToInt32(Session["UserLoginID"]);
                myWatch.WatchID = -1;
                myWatch.UniqueID = txtID.Text;
                myWatch.Sent = "False";
                myWatch.IsCompany = rbtnCompanyID.Checked.ToString();
                myWatch.Created = DateTime.Now;

                // Athuga hvort skr�ning s� til fyrir...
                if (myFactory.SearchUniqueWatch(myWatch))
                {
                    // �� �arf einhvern veginn a� l�ta notandann vita (gert me� label h�r).
                    DisplayMessage(rm.GetString("txtEntryAlreadyExists", ci));
                    return;
                }

                if (myFactory.SearchNationalRegistry(myWatch.UniqueID))
                {
                    if (!myFactory.TryToRegisterNewUniqueID(myWatch))
                    {
                        DisplayErrorMessage(rm.GetString("txtErrorRegistering", ci));
                        //Server.Transfer("error.aspx?err=1");
                        return;
                    }
                    else
                    {
                        PrepareControlsForRegistration();
                        DisplayMessage(rm.GetString("txtInformationSaved", ci));
                    }
                }
                else
                {
                    DisplayErrorMessage(rm.GetString("txtErrorRegisteringNotExistEntity", ci));
                    return;
                }
            }
            catch (Exception err)
            {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwOverView.aspx caught, message is : " + err.Message, true);
                DisplayErrorMessage(rm.GetString("txtException", ci));
            }
        }

        /// <summary>
        /// Kalla� � �etta fall �egar skr� � margar kennit�lur � einu ni�ur � grunn � gegn um textbox.
        /// Gerir r�� fyrir a� einhver texti s� � textboxi og notandi hafi �tt � hnapp.
        /// </summary>
        private void RegisterUniqueIDFromTextBox()
        {
            try
            {
                WatchUniqueIDBLLC myWatch;

                // �tb� char array sem heldur utan um alla leyfilega "seperators" fyrir textbox.
                char[] mySeperatorArray = new[] { ',', ';', '?', '|' };

                // Tek textann �r textboxi, splitta honum upp samkv. leyfilegum "seperators og skila string array.
                // �arf a� setja inn me�h�ndlun � l�nubilum (\r\n)
                string[] myStringArray = txtbxUnique.Text.Replace("\r\n", "").Split(mySeperatorArray);
                txtbxUnique.Text = "";

                // Fyrir hverja f�rslu � myStringArray �tla �g a� athuga hvort �etta s� leyfileg kennitala m.v.
                // �j��skr� og hvort veri� s� a� skr� umfram leyfilegan fj�lda. Ef � lagi �� reyni �g a� skr� 
                // ni�ur � grunn.
                for (int iCount = 0; iCount < myStringArray.Length; iCount++)
                {
                    // Ef vi�komandi skr�ning reynir a� fara yfir leyfilegt h�mark...
                    if (myFactory.CountWatch(Convert.ToInt32(Session["UserLoginID"])) >=
                        Convert.ToInt32(Session["CreditWatch"]))
                    {
                        DisplayErrorMessage(rm.GetString("txtRegistrationLimitReached", ci));
                        return;
                        //Server.Transfer("error.aspx?err=4");
                    }

                    // Sett inn � "hlut" sem stendur fyrir eina f�rslu � vaktaskr�...
                    myWatch = new WatchUniqueIDBLLC();
                    myWatch.CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]);
                    // UserID nota� til a� a�greina f�rslur � vakt � milli notenda... tekur vi� af CreditInfoID
                    // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)
                    myWatch.UserID = Convert.ToInt32(Session["UserLoginID"]);
                    myWatch.WatchID = -1;
                    myWatch.UniqueID = myStringArray[iCount];
                    myWatch.Sent = "False";
                    myWatch.IsCompany = rbtnCompanyID.Checked.ToString();
                    myWatch.Created = DateTime.Now;

                    // Athuga hvort skr�ning s� til fyrir...
                    if (myFactory.SearchUniqueWatch(myWatch))
                    {
                        // �� �arf einhvern veginn a� l�ta notandann vita (gert me� label h�r).
                        txtbxUnique.Text += myStringArray[iCount] + rm.GetString("txtIsAlreadyOnWach", ci) + "\n";
                        continue;
                    }

                    if (myFactory.SearchNationalRegistry(myWatch.UniqueID))
                    {
                        if (!myFactory.TryToRegisterNewUniqueID(myWatch))
                        {
                            txtbxUnique.Text += myStringArray[iCount] + rm.GetString("txtIsNotRegistered", ci) + "\n";
                            // + " is not registered\n";
                            continue;
                        }
                    }
                }
                DisplayMessage(rm.GetString("txtInformationSaved", ci));
                PrepareControlsForRegistration();
            }
            catch (Exception err)
            {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwOverView.aspx caught, message is : " + err.Message, true);
                DisplayErrorMessage(rm.GetString("txtException", ci));
            }
        }

        /// <summary>
        /// Kalla� � �etta fall �egar skr� � margar kennit�lur � einu ni�ur � grunn � gegn um textbox.
        /// Gerir r�� fyrir a� einhver texti s� � textboxi og notandi hafi �tt � hnapp.
        /// </summary>
        private void RegisterUniqueIDFromStringArray(string[] myStringArray)
        {
            try
            {
                WatchUniqueIDBLLC myWatch;

                // �tb� char array sem heldur utan um alla leyfilega "seperators" fyrir textbox.
                //char[] mySeperatorArray = new char[] {',',';','?','|'};

                // Tek textann �r textboxi, splitta honum upp samkv. leyfilegum "seperators og skila string array.
                // �arf a� setja inn me�h�ndlun � l�nubilum (\r\n)
                //myStringArray = txtbxUnique.Text.Split(mySeperatorArray);
                //txtbxUnique.Text = "";

                // Fyrir hverja f�rslu � myStringArray �tla �g a� athuga hvort �etta s� leyfileg kennitala m.v.
                // �j��skr� og hvort veri� s� a� skr� umfram leyfilegan fj�lda. Ef � lagi �� reyni �g a� skr� 
                // ni�ur � grunn.
                for (int iCount = 0; iCount < myStringArray.Length; iCount++)
                {
                    // Ef vi�komandi skr�ning reynir a� fara yfir leyfilegt h�mark...
                    if (myFactory.CountWatch(Convert.ToInt32(Session["UserLoginID"])) >=
                        Convert.ToInt32(Session["CreditWatch"]))
                    {
                        DisplayErrorMessage(rm.GetString("txtRegistrationLimitReached", ci));
                        return;
                        //Server.Transfer("error.aspx?err=4");
                    }

                    // Sett inn � "hlut" sem stendur fyrir eina f�rslu � vaktaskr�...
                    myWatch = new WatchUniqueIDBLLC();
                    myWatch.CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]);
                    // UserID nota� til a� a�greina f�rslur � vakt � milli notenda... tekur vi� af CreditInfoID
                    // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)
                    myWatch.UserID = Convert.ToInt32(Session["UserLoginID"]);
                    myWatch.WatchID = -1;
                    myWatch.UniqueID = myStringArray[iCount];
                    myWatch.Sent = "False";
                    try
                    {
                        myWatch.IsCompany = rbtnCompanyID.Checked.ToString();
                    }
                    catch (Exception e)
                    {
                        myWatch.IsCompany = null;
                    }
                    myWatch.Created = DateTime.Now;

                    // Athuga hvort skr�ning s� til fyrir...
                    if (myFactory.SearchUniqueWatch(myWatch))
                    {
                        // �� �arf einhvern veginn a� l�ta notandann vita (gert me� label h�r).
                        txtbxUnique.Text += myStringArray[iCount] + " is already on watch\n";
                        continue;
                    }

                    if (myFactory.SearchNationalRegistry(myWatch.UniqueID))
                    {
                        if (!myFactory.TryToRegisterNewUniqueID(myWatch))
                        {
                            txtbxUnique.Text += myStringArray[iCount] + " is not registered\n";
                            continue;
                        }
                    }
                }
                DisplayMessage(rm.GetString("txtInformationSaved", ci));
                PrepareControlsForRegistration();
            }
            catch (Exception err)
            {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwOverView.aspx caught, message is : " + err.Message, true);
                DisplayErrorMessage(rm.GetString("txtException", ci));
            }
        }

        #endregion

        #region Registering WatchWords

        /// <summary>
        /// Setur eina stikkor�af�rslu ni�ur � grunn.
        /// </summary>
        private void RegisterOneByWordsID()
        {
            WatchWordsBLLC myWatch = new WatchWordsBLLC();

            // Ef vi�komandi skr�ning reynir a� fara yfir leyfilegt h�mark...
            if (myFactory.CountWatch(Convert.ToInt32(Session["UserLoginID"])) >= Convert.ToInt32(Session["CreditWatch"]))
            {
                DisplayErrorMessage(rm.GetString("txtRegistrationLimitReached", ci));
                return;
                //Server.Transfer("error.aspx?err=4");
            }

            /*
			// Skilyr�i um heimilisfang teki� �t vegna �ess a� einstaklingar (og fyrirt�ki) eru ekki alltaf skr��ir me�
			// heimilisfang � g�gnunum eins og �au eru � dag...
			
			if (this.txtAddress.Text == "")
			{
				if (EN)
				{
					string myScript = "<script language=Javascript>alert('You have to register an address.');</script>"; 
					Page.RegisterClientScriptBlock("alert", myScript); 
					return;
				}
				else
				{
					string myScript = "<script language=Javascript>alert('You have to register an address.');</script>"; 
					Page.RegisterClientScriptBlock("alert", myScript); 
					return;
				}
			}
			*/

            try
            {
                // Sett inn � "hlut" sem stendur fyrir eina f�rslu � vaktaskr�...
                myWatch.CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]);
                // UserID nota� til a� a�greina f�rslur � vakt � milli notenda... tekur vi� af CreditInfoID
                // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)
                myWatch.UserID = Convert.ToInt32(Session["UserLoginID"]);
                myWatch.WatchID = -1;
                if (chkbxNative.Checked)
                {
                    if (txtFirstName.Text != "")
                    {
                        myWatch.FirstNameNative = txtFirstName.Text;
                    }
                    if (txtSurName.Text != "")
                    {
                        myWatch.SurNameNative = txtSurName.Text;
                    }
                    if (txtAddress.Text != "")
                    {
                        myWatch.AddressNative = txtAddress.Text;
                    }
                    if (txtTown.Text != "")
                    {
                        myWatch.TownNative = txtTown.Text;
                    }
                }
                else
                {
                    if (txtFirstName.Text != "")
                    {
                        myWatch.FirstNameEN = txtFirstName.Text;
                    }
                    if (txtSurName.Text != "")
                    {
                        myWatch.SurNameEN = txtSurName.Text;
                    }
                    if (txtAddress.Text != "")
                    {
                        myWatch.AddressEN = txtAddress.Text;
                    }
                    if (txtTown.Text != "")
                    {
                        myWatch.TownEN = txtTown.Text;
                    }
                }
                myWatch.IsCompany = rbtnCompanyWord.Checked.ToString();
                myWatch.Sent = "False";
                myWatch.Created = DateTime.Now;

                /*
				// Athuga hvort skr�ning s� til fyrir...
				if (myFactory.SearchWatchWords(myWatch, this.chkbxNative.Checked))
				{
					// �� �arf einhvern veginn a� l�ta notandann vita...
					if (EN)
					{
						string myScript = "<script language=Javascript>alert('Entry already exists on Watch.');</script>"; 
						Page.RegisterClientScriptBlock("alert", myScript); 
						return;
					}
					else
					{
						string myScript = "<script language=Javascript>alert('Entry already exists on Watch.');</script>"; 
						Page.RegisterClientScriptBlock("alert", myScript); 
						return;
					}
				}
				*/

                if (!myFactory.AddNewWordsToWatch(myWatch))
                {
                    DisplayErrorMessage(rm.GetString("txtErrorRegistering", ci));
                    //Server.Transfer("error.aspx?err=1");
                }
                else
                {
                    DisplayMessage(rm.GetString("txtInformationSaved", ci));
                    PrepareControlsForRegistration();
                }
            }
            catch (Exception err)
            {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwOverView.aspx caught, message is : " + err.Message, true);
                DisplayErrorMessage(rm.GetString("txtException", ci));
            }
        }

        private void RegisterWordsFromTextBox()
        {
            bool czVersion = false;
            if (CigConfig.Configure("lookupsettings.currentVersion") != null)
            {
                if (CigConfig.Configure("lookupsettings.currentVersion").Equals("czech"))
                {
                    czVersion = true;
                }
            }
            try
            {
                int iLoop = 0;
                WatchWordsBLLC myWatch;

                // �tb� char array sem heldur utan um alla leyfilega "seperators" fyrir textbox.
                char[] mySeperatorArray = new[] { ',', ';', '?', '|' };
                string[] myPositives = new[] { "y", "yes", "t", "true", "j", "ja", "1" };

                // Tek textann �r textboxi, splitta honum upp samkv. leyfilegum "seperators og skila string array.
                // �arf a� setja inn me�h�ndlun � l�nubilum (\r\n)
                string[] myStringArray = txtbxWords.Text.Split(mySeperatorArray);
                txtbxWords.Text = "";

                // Fyrir hverja f�rslu � myStringArray �tla �g a� athuga hvort �etta s� leyfileg skr�ning
                // og hvort veri� s� a� skr� umfram leyfilegan fj�lda. Ef � lagi �� reyni �g a� skr� 
                // ni�ur � grunn.
                for (int iCount = 0; iCount < myStringArray.Length; iCount += 5)
                {
                    iLoop++;
                    // Ef vi�komandi skr�ning reynir a� fara yfir leyfilegt h�mark...
                    if (myFactory.CountWatch(Convert.ToInt32(Session["UserLoginID"])) >=
                        Convert.ToInt32(Session["CreditWatch"]))
                    {
                        DisplayErrorMessage(rm.GetString("txtRegistrationLimitReached", ci));
                        return;
                        //Server.Transfer("error.aspx?err=4");
                    }

                    // Sett inn � "hlut" sem stendur fyrir eina f�rslu � vaktaskr�...
                    myWatch = new WatchWordsBLLC();
                    myWatch.CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]);
                    // UserID nota� til a� a�greina f�rslur � vakt � milli notenda... tekur vi� af CreditInfoID
                    // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)
                    myWatch.UserID = Convert.ToInt32(Session["UserLoginID"]);
                    myWatch.WatchID = -1;

                    // Athuga fornafn
                    if (myStringArray[iCount] == "")
                    {
                        txtbxWords.Text += "Record number: " + iLoop + " Name needed\n";
                        continue;
                    }
                    else
                    {
                        myWatch.FirstNameNative = myStringArray[iCount];
                        //myWatch.FirstNameEN = null;
                    }

                    /*
					 * Skilyr�i um heimilisfang teki� �t vegna �ess a� einstaklingar (og fyrirt�ki) eru ekki 
					 * alltaf skr��ir me� heimilisfang � g�gnunum eins og �au eru � dag...
					 * 
					// Athuga heimilisfang
					if (myStringArray[iCount+2].ToString()=="")
					{
						txtbxWords.Text += "Record number: " + iLoop.ToString() + " Address needed\n";
						continue;
					}
					else
					{
						myWatch.AddressNative = myStringArray[iCount+2].ToString();
						//myWatch.AddressEN = null;
					}
					*/

                    if (czVersion)
                    {
                        if ((iCount + 1) < myStringArray.Length)
                        {
                            myWatch.AddressNative = myStringArray[iCount + 1];
                        }
                        if ((iCount + 2) < myStringArray.Length)
                        {
                            myWatch.TownNative = myStringArray[iCount + 2];
                        }
                    }
                    else // einkennilegt hefur �etta einhvernt�man virka�?
                    {
                        myWatch.AddressNative = myStringArray[iCount + 2];
                        myWatch.TownNative = myStringArray[iCount + 3];
                    }
                    //myWatch.TownEN = null;

                    myWatch.Sent = "False";

                    myWatch.IsCompany = "False";
                    if (!czVersion)
                    {
                        foreach (string s in myPositives)
                        {
                            if (myStringArray[iCount + 4].ToLower() == s)
                            {
                                myWatch.IsCompany = "True";
                                continue;
                            }
                        }
                    }
                    // if czech then we are only using companies
                    if (czVersion)
                    {
                        myWatch.IsCompany = "True";
                    }

                    if (!czVersion)
                    {
                        //Athuga eftirnafn (�arf ef �etta er ekki fyrirt�ki)
                        if (myStringArray[iCount + 1] == "" && myWatch.IsCompany == "False")
                        {
                            txtbxWords.Text += "Record number: " + iLoop + "Surname needed\n";
                            continue;
                        }
                        else
                        {
                            myWatch.SurNameNative = myStringArray[iCount + 1];
                            //myWatch.SurNameEN = null;
                        }
                    }

                    myWatch.Created = DateTime.Now;

                    /*
					// Athuga hvort skr�ning s� til fyrir...
					if (myFactory.SearchWatchWords(myWatch, this.chkbxNative.Checked))
					{
						// �� �arf einhvern veginn a� l�ta notandann vita (gert me� label h�r).
						txtbxWords.Text += myStringArray[iCount].ToString() + " " + myStringArray[iCount+1].ToString() + " is already on watch\n";
						continue;
					}
					*/

                    if (!myFactory.AddNewWordsToWatch(myWatch))
                    {
                        txtbxUnique.Text += myStringArray[iCount] + " is not registered\n";
                        continue;
                    }
                }
                DisplayMessage(rm.GetString("txtInformationSaved", ci));
            }
            catch (Exception err)
            {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwOverView.aspx caught, message is : " + err.Message, true);
            }
            finally
            {
                PrepareControlsForRegistration();
            }
        }

        #endregion

        #region DataGrid Commands

        #region DeleteCommand

        // Delete skipanir eru allar uppbygg�ar � �ann h�tt a� fyrst er eytt �t �r gagnagrunni og svo �t �r
        // DataGrid (handvirkt). Gert me� �essum h�tti svo ekki �urfi a� s�kja g�gnin aftur ni�ur � grunn.
        // Hugsanlega h�gt a� pakka ni�ur � hj�lparfall �egar t�mi gefst til.
        private void dgCompanies_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            if (!myFactory.DeleteUniqueWatchRecordByWatchID(Convert.ToInt32(e.Item.Cells[1].Text)))
            {
                DisplayErrorMessage(rm.GetString("txtErrorDeleting", ci));
                //Server.Transfer("error.aspx?err=5");
            }
            else
            {
                DataView myView;
                DataTable myTable = (DataTable)Session["dgCompanies"];
                myView = myTable.DefaultView;
                int myRowToDelete = dgCompanies.PageSize * (dgCompanies.CurrentPageIndex);
                DataRowCollection myRows = myView.Table.Rows;
                DataRow thisRow = myView[e.Item.ItemIndex + myRowToDelete].Row;

                if (thisRow != null)
                {
                    myRows.Remove(thisRow);
                }

                dgCompanies.DataSource = myView;
                dgCompanies.DataBind();
                UpdateRecordCount();

                if (myView.Table.Rows.Count <= 0)
                {
                    dgCompanies.Visible = false;
                }
            }
        }

        private void dgIndividuals_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            if (!myFactory.DeleteUniqueWatchRecordByWatchID(Convert.ToInt32(e.Item.Cells[1].Text)))
            {
                DisplayErrorMessage(rm.GetString("txtErrorDeleting", ci));
                //Server.Transfer("error.aspx?err=5");
            }
            else
            {
                DataView myView;
                DataTable myTable = (DataTable)Session["dgIndividuals"];
                myView = myTable.DefaultView;
                int myRowToDelete = dgIndividuals.PageSize * (dgIndividuals.CurrentPageIndex);
                DataRowCollection myRows = myView.Table.Rows;
                DataRow thisRow = myView[e.Item.ItemIndex + myRowToDelete].Row;

                if (thisRow != null)
                {
                    myRows.Remove(thisRow);
                }

                dgIndividuals.DataSource = myView;
                dgIndividuals.DataBind();
                UpdateRecordCount();

                if (myView.Table.Rows.Count <= 0)
                {
                    dgIndividuals.Visible = false;
                }
            }
        }

        private void dgWordsIndividuals_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            int WatchID = 0;
            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsIndividuals"];
            myView = myTable.DefaultView;

            if (Convert.ToInt32(myView.Table.Rows[e.Item.ItemIndex].ItemArray[1].ToString()) > 0)
            {
                WatchID = Convert.ToInt32(myView.Table.Rows[e.Item.ItemIndex].ItemArray[1].ToString());
            }
            else
            {
                WatchID = Convert.ToInt32(e.Item.Cells[1].Text);
            }

            if (!myFactory.DeleteWatchWordsRecordByWatchID(WatchID))
            {
                DisplayErrorMessage(rm.GetString("txtErrorDeleting", ci));
                //Server.Transfer("error.aspx?err=5");
            }
            else
            {
                int myRowToDelete = dgWordsIndividuals.PageSize * (dgWordsIndividuals.CurrentPageIndex);
                DataRowCollection myRows = myView.Table.Rows;
                DataRow thisRow = myView[e.Item.ItemIndex + myRowToDelete].Row;

                if (thisRow != null)
                {
                    myRows.Remove(thisRow);
                }

                dgWordsIndividuals.DataSource = myView;
                dgWordsIndividuals.DataBind();
                UpdateRecordCount();

                if (myView.Table.Rows.Count <= 0)
                {
                    dgWordsIndividuals.Visible = false;
                }
            }
        }

        private void dgWordsCompanies_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            int WatchID = 0;
            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsCompanies"];
            myView = myTable.DefaultView;

            if (Convert.ToInt32(myView.Table.Rows[e.Item.ItemIndex].ItemArray[1].ToString()) > 0)
            {
                WatchID = Convert.ToInt32(myView.Table.Rows[e.Item.ItemIndex].ItemArray[1].ToString());
            }
            else
            {
                WatchID = Convert.ToInt32(e.Item.Cells[1].Text);
            }

            if (!myFactory.DeleteWatchWordsRecordByWatchID(WatchID))
            {
                DisplayErrorMessage(rm.GetString("txtErrorDeleting", ci));
                //Server.Transfer("error.aspx?err=5");
            }
            else
            {
                int myRowToDelete = dgWordsCompanies.PageSize * (dgWordsCompanies.CurrentPageIndex);
                DataRowCollection myRows = myView.Table.Rows;
                DataRow thisRow = myView[e.Item.ItemIndex + myRowToDelete].Row;

                if (thisRow != null)
                {
                    myRows.Remove(thisRow);
                }

                dgWordsCompanies.DataSource = myView;
                dgWordsCompanies.DataBind();
                UpdateRecordCount();

                if (myView.Table.Rows.Count <= 0)
                {
                    dgWordsCompanies.Visible = false;
                }
            }
        }

        #endregion

        #region SortCommand

        /// <summary>
        /// Fall sem heldur utan um d�lka sem h�gt er a� ra�a eftir og svissar ASC og DESC.  Geymt � session til a� lifa af postback
        /// </summary>
        /// <param name="SortExp">D�lkur sem veri� er a� ra�a eftir</param>
        /// <returns></returns>
        private string SortDir(string SortExp)
        {
            if (Session["SortExpression_" + SortExp] == null)
            {
                Session["SortExpression_" + SortExp] = "ASC";
                return " ASC";
            }
            else
            {
                if (Session["SortExpression_" + SortExp].ToString() == "ASC")
                {
                    Session["SortExpression_" + SortExp] = "DESC";
                    return " DESC";
                }
                else
                {
                    Session["SortExpression_" + SortExp] = "ASC";
                    return " ASC";
                }
            }
        }

        private void dgCompanies_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            string SortDirection = SortDir("dgCompanies_" + e.SortExpression);
            DataTable dtCompanies = (DataTable)Session["dgCompanies"];
            SortDataTable(ref dtCompanies, e.SortExpression + " " + SortDirection);
            dgCompanies.DataSource = dtCompanies;
            dgCompanies.DataBind();
            Session["dgCompanies"] = dtCompanies;
        }

        private void dgWordsIndividuals_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            string SortDirection = SortDir("dgWordsIndividuals_" + e.SortExpression);
            DataTable dt = (DataTable)Session["dgWordsIndividuals"];
            SortDataTable(ref dt, e.SortExpression + " " + SortDirection);
            dgWordsIndividuals.DataSource = dt;
            dgWordsIndividuals.DataBind();
            Session["dgWordsIndividuals"] = dt;
        }

        private void dgWordsCompanies_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            string SortDirection = SortDir("dgWordsCompanies_" + e.SortExpression);
            DataTable dt = (DataTable)Session["dgWordsCompanies"];
            SortDataTable(ref dt, e.SortExpression + " " + SortDirection);
            dgWordsCompanies.DataSource = dt;
            dgWordsCompanies.DataBind();
            Session["dgWordsCompanies"] = dt;
        }

        private void dgIndividuals_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            string SortDirection = SortDir("dgIndividuals_" + e.SortExpression);
            DataTable dt = (DataTable)Session["dgIndividuals"];
            SortDataTable(ref dt, e.SortExpression + " " + SortDirection);
            dgIndividuals.DataSource = dt;
            dgIndividuals.DataBind();
            Session["dgIndividuals"] = dt;
        }

        #endregion

        #region Edit/Cancel/Update

        private void dgWordsCompanies_EditCommand(object source, DataGridCommandEventArgs e)
        {
            dgWordsCompanies.EditItemIndex = e.Item.ItemIndex;
            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsCompanies"];

            // �annig a� r��un haldi s�r
            myView = myTable.DefaultView;

            dgWordsCompanies.DataSource = myView;
            dgWordsCompanies.DataBind();
        }

        private void dgWordsIndividuals_EditCommand(object source, DataGridCommandEventArgs e)
        {
            dgWordsIndividuals.EditItemIndex = e.Item.ItemIndex;
            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsIndividuals"];
            myView = myTable.DefaultView;

            dgWordsIndividuals.DataSource = myView;
            dgWordsIndividuals.DataBind();
        }

        private void dgWordsCompanies_CancelCommand(object source, DataGridCommandEventArgs e)
        {
            dgWordsCompanies.EditItemIndex = -1;
            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsCompanies"];
            myView = myTable.DefaultView;

            dgWordsCompanies.DataSource = myView;
            dgWordsCompanies.DataBind();
        }

        private void dgWordsIndividuals_CancelCommand(object source, DataGridCommandEventArgs e)
        {
            dgWordsIndividuals.EditItemIndex = -1;
            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsIndividuals"];
            myView = myTable.DefaultView;

            dgWordsIndividuals.DataSource = myView;
            dgWordsIndividuals.DataBind();
        }

        private void dgWordsCompanies_UpdateCommand(object source, DataGridCommandEventArgs e)
        {
            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsCompanies"];
            myView = myTable.DefaultView;
            int mySelectedRow = dgWordsCompanies.PageSize * (dgWordsCompanies.CurrentPageIndex);

            // Finn "row" sem veri� er a� vinna me� me� �v� a� leggja ItemIndex vi� fj�lda � hverri s��u (page).
            // Tek ra�irnar fr� myView sem er view � gagnat�flu � minni. View er teki� fr� DefaultView � t�fluna �annig
            // a� �ll r��un � a� halda s�r.

            DataRow thisRow = myView[e.Item.ItemIndex + mySelectedRow].Row;

            if (((TextBox)e.Item.Cells[2].Controls[0]).Text == "")
            {
                string myScript = "<script language=Javascript>alert('" + rm.GetString("txtMustRegisterName", ci) +
                                  "');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
                return;
            }

            if (((TextBox)e.Item.Cells[4].Controls[0]).Text == "")
            {
                string myScript = "<script language=Javascript>alert('" + rm.GetString("txtMustRegisterAddress", ci) +
                                  "');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
                return;
            }

            try
            {
                // Sett inn � "hlut" sem stendur fyrir eina f�rslu � vaktaskr�...
                WatchWordsBLLC myWatch = new WatchWordsBLLC();

                myWatch.CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]);
                // �arf a� s�kja �etta � tableview �ar sem um "fali�" sv��i er a� r��a og er �v� ekki til � edit mode...
                myWatch.WatchID = Convert.ToInt32(myView.Table.Rows[e.Item.ItemIndex].ItemArray[1].ToString());

                // Ef um "native" skr�ningu er a� r��a... (Convert.ToBoolean (isNative sv��i �r datagrid)
                if (Convert.ToBoolean(myView.Table.Rows[e.Item.ItemIndex].ItemArray[9]))
                {
                    myWatch.FirstNameNative = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
                    // Til �ess a� uppf�ra datagrid � lei�inni �n �ess a� �urfa a� s�kja g�gnin aftur.

                    thisRow[2] = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
                    myWatch.SurNameNative = ((TextBox)e.Item.Cells[3].Controls[0]).Text;

                    thisRow[3] = ((TextBox)e.Item.Cells[3].Controls[0]).Text;
                    myWatch.AddressNative = ((TextBox)e.Item.Cells[4].Controls[0]).Text;

                    thisRow[4] = ((TextBox)e.Item.Cells[4].Controls[0]).Text;
                    myWatch.TownNative = ((TextBox)e.Item.Cells[5].Controls[0]).Text;
                }
                else
                {
                    myWatch.FirstNameEN = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
                    // Til �ess a� uppf�ra datagrid � lei�inni �n �ess a� �urfa a� s�kja g�gnin aftur.

                    thisRow[2] = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
                    myWatch.SurNameEN = ((TextBox)e.Item.Cells[3].Controls[0]).Text;

                    thisRow[3] = ((TextBox)e.Item.Cells[3].Controls[0]).Text;
                    myWatch.AddressEN = ((TextBox)e.Item.Cells[4].Controls[0]).Text;

                    thisRow[4] = ((TextBox)e.Item.Cells[4].Controls[0]).Text;
                    myWatch.TownEN = ((TextBox)e.Item.Cells[5].Controls[0]).Text;
                }

                thisRow[5] = ((TextBox)e.Item.Cells[5].Controls[0]).Text;
                myWatch.IsCompany = "True"; // myView.Table.Rows[e.Item.ItemIndex].ItemArray[6].ToString();

                myWatch.Sent = "False"; // myView.Table.Rows[e.Item.ItemIndex].ItemArray[7].ToString();
                myWatch.Created = Convert.ToDateTime(myView.Table.Rows[e.Item.ItemIndex].ItemArray[8].ToString());

                if (!myFactory.UpdateWordsToWatch(myWatch))
                {
                    DisplayErrorMessage(rm.GetString("txtErrorUpdating", ci));
                    //Server.Transfer("error.aspx?err=6");
                }
            }
            catch (Exception err)
            {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwOverView.aspx caught, message is : " + err.Message, true);
            }
            finally
            {
                dgWordsCompanies.EditItemIndex = -1;
                dgWordsCompanies.DataSource = myView;
                dgWordsCompanies.DataBind();
            }
        }

        private void dgWordsIndividuals_UpdateCommand(object source, DataGridCommandEventArgs e)
        {
            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsIndividuals"];
            myView = myTable.DefaultView;
            int mySelectedRow = dgWordsIndividuals.PageSize * (dgWordsIndividuals.CurrentPageIndex);

            // Finn "row" sem veri� er a� vinna me� me� �v� a� leggja ItemIndex vi� fj�lda � hverri s��u (page).
            // Tek ra�irnar fr� myView sem er view � gagnat�flu � minni. View er teki� fr� DefaultView � t�fluna �annig
            // a� �ll r��un � a� halda s�r.

            DataRow thisRow = myView[e.Item.ItemIndex + mySelectedRow].Row;

            if (((TextBox)e.Item.Cells[2].Controls[0]).Text == "")
            {
                string myScript = "<script language=Javascript>alert('" + rm.GetString("txtMustRegisterName", ci) +
                                  "');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
                return;
            }

            if (((TextBox)e.Item.Cells[2].Controls[0]).Text == "")
            {
                string myScript = "<script language=Javascript>alert('" + rm.GetString("txtMustRegisterSurname", ci) +
                                  "');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
                return;
            }

            if (((TextBox)e.Item.Cells[4].Controls[0]).Text == "")
            {
                string myScript = "<script language=Javascript>alert('" + rm.GetString("txtMustRegisterAddress", ci) +
                                  "');</script>";
                Page.RegisterClientScriptBlock("alert", myScript);
                return;
            }

            try
            {
                // Sett inn � "hlut" sem stendur fyrir eina f�rslu � vaktaskr�...
                WatchWordsBLLC myWatch = new WatchWordsBLLC();

                myWatch.CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]);
                // �arf a� s�kja �etta � tableview �ar sem um "fali�" sv��i er a� r��a og er �v� ekki til � edit mode...
                myWatch.WatchID = Convert.ToInt32(myView.Table.Rows[e.Item.ItemIndex].ItemArray[1].ToString());

                // Ef um "native" skr�ningu er a� r��a... (Convert.ToBoolean (isNative sv��i �r datagrid)
                if (Convert.ToBoolean(myView.Table.Rows[e.Item.ItemIndex].ItemArray[9]))
                {
                    myWatch.FirstNameNative = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
                    // Til �ess a� uppf�ra datagrid � lei�inni �n �ess a� �urfa a� s�kja g�gnin aftur.

                    thisRow[2] = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
                    myWatch.SurNameNative = ((TextBox)e.Item.Cells[3].Controls[0]).Text;

                    thisRow[3] = ((TextBox)e.Item.Cells[3].Controls[0]).Text;
                    myWatch.AddressNative = ((TextBox)e.Item.Cells[4].Controls[0]).Text;

                    thisRow[4] = ((TextBox)e.Item.Cells[4].Controls[0]).Text;
                    myWatch.TownNative = ((TextBox)e.Item.Cells[5].Controls[0]).Text;
                }
                else
                {
                    myWatch.FirstNameEN = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
                    // Til �ess a� uppf�ra datagrid � lei�inni �n �ess a� �urfa a� s�kja g�gnin aftur.

                    thisRow[2] = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
                    myWatch.SurNameEN = ((TextBox)e.Item.Cells[3].Controls[0]).Text;

                    thisRow[3] = ((TextBox)e.Item.Cells[3].Controls[0]).Text;
                    myWatch.AddressEN = ((TextBox)e.Item.Cells[4].Controls[0]).Text;

                    thisRow[4] = ((TextBox)e.Item.Cells[4].Controls[0]).Text;
                    myWatch.TownEN = ((TextBox)e.Item.Cells[5].Controls[0]).Text;
                }

                myWatch.IsCompany = "False"; // myView.Table.Rows[e.Item.ItemIndex].ItemArray[6].ToString();
                myWatch.Sent = "False"; // myView.Table.Rows[e.Item.ItemIndex].ItemArray[7].ToString();
                myWatch.Created = Convert.ToDateTime(myView.Table.Rows[e.Item.ItemIndex].ItemArray[8].ToString());

                if (!myFactory.UpdateWordsToWatch(myWatch))
                {
                    DisplayErrorMessage(rm.GetString("txtErrorUpdating", ci));
                    //Server.Transfer("error.aspx?err=6");
                }
            }
            catch (Exception err)
            {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwOverView.aspx caught, message is : " + err.Message, true);
            }
            finally
            {
                dgWordsIndividuals.EditItemIndex = -1;
                dgWordsIndividuals.DataSource = myView;
                dgWordsIndividuals.DataBind();
            }
        }

        #endregion

        #region ItemCreated

        // Nota ItemCreated � �llum DataGrids til �ess a� tengja "onclick" Event vi� Delete hnappinn sem fylgir 
        // hverri l�nu. �annig er h�gt a� setja inn javascript confirm glugga til �ess a� sta�festa a� �a� eigi
        // a� ey�a l�nunni �t. Hugsanlega h�gt a� pakka saman � hj�lparfall ef og �egar t�mi vinnst til.
        private void dgCompanies_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                WebControl button = (WebControl)e.Item.Cells[9].Controls[0];
                button.Attributes.Add("onclick", "return confirm (\"" + rm.GetString("txtConfirmDelete", ci) + "\");");
            }
        }

        private void dgIndividuals_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                WebControl button = (WebControl)e.Item.Cells[9].Controls[0];
                button.Attributes.Add("onclick", "return confirm (\"" + rm.GetString("txtConfirmDelete", ci) + "\");");
            }
        }

        private void dgWordsCompanies_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                WebControl button = (WebControl)e.Item.Cells[9].Controls[0];
                button.Attributes.Add("onclick", "return confirm (\"" + rm.GetString("txtConfirmDelete", ci) + "\");");
            }
        }

        private void dgWordsIndividuals_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                WebControl button = (WebControl)e.Item.Cells[10].Controls[0];
                button.Attributes.Add("onclick", "return confirm (\"" + rm.GetString("txtConfirmDelete", ci) + "\");");
            }
        }

        #endregion

        #region PageIndexChanged

        private void dgWordsCompanies_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgWordsCompanies.CurrentPageIndex = e.NewPageIndex;

            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsCompanies"];
            myView = myTable.DefaultView;

            dgWordsCompanies.DataSource = myView;
            dgWordsCompanies.DataBind();
        }

        private void dgCompanies_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgCompanies.CurrentPageIndex = e.NewPageIndex;

            DataView myView;
            DataTable myTable = (DataTable)Session["dgCompanies"];
            myView = myTable.DefaultView;

            dgCompanies.DataSource = myView;
            dgCompanies.DataBind();
        }

        private void dgIndividuals_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgIndividuals.CurrentPageIndex = e.NewPageIndex;

            DataView myView;
            DataTable myTable = (DataTable)Session["dgIndividuals"];
            myView = myTable.DefaultView;

            dgIndividuals.DataSource = myView;
            dgIndividuals.DataBind();
        }

        private void dgWordsIndividuals_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgWordsIndividuals.CurrentPageIndex = e.NewPageIndex;

            DataView myView;
            DataTable myTable = (DataTable)Session["dgWordsIndividuals"];
            myView = myTable.DefaultView;

            dgWordsIndividuals.DataSource = myView;
            dgWordsIndividuals.DataBind();
        }

        #endregion

        #endregion

        #region Textad�t

        private void DisplayMessage(String message)
        {
            lblMessage.Text = message;
            lblMessage.ForeColor = Color.Blue;
            lblMessage.Visible = true;
        }

        private void DisplayErrorMessage(String errorMessage)
        {
            lblMessage.Text = errorMessage;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Visible = true;
        }

        private void LocalizeText()
        {
            lblCreditWatch.Text = rm.GetString("txtCreditWatchPage", ci);
            lblWatchListRegistrationProcess.Text = rm.GetString("txtWatchlistRegistrationProcess", ci);
            lblIDRegistration.Text = rm.GetString("lbIDRegistration", ci);
            lblWordsRegistration.Text = rm.GetString("lbWordsRegistration", ci);
            lblID.Text = rm.GetString("lbID", ci);
            lblFirstName.Text = rm.GetString("lbFirstName", ci);
            lblSurName.Text = rm.GetString("lbSurName", ci);
            lblAddress.Text = rm.GetString("lbAddress", ci);
            lblTown.Text = rm.GetString("lbTown", ci);
            rbtnCompanyWord.Text = rm.GetString("chkCompany", ci);
            rbtnIndividual.Text = rm.GetString("chkIndividual", ci);
            rbtnCompanyID.Text = rm.GetString("chkCompany", ci);
            rbtnIndividualID.Text = rm.GetString("chkIndividual", ci);
            chkbxNative.Text = rm.GetString("txtNative", ci);
            btnRegisterUnique.Text = rm.GetString("txtRegister", ci);
            btnRegisterWords.Text = rm.GetString("txtRegister", ci);
            btnDeleteUnique.Text = rm.GetString("txtDelete", ci);
            btnDeleteWords.Text = rm.GetString("txtDelete", ci);
            btnSearchClaims.Text = rm.GetString("txtSearchClaims", ci);
            btnDeleteAll.Text = rm.GetString("txtDeleteAll", ci);
            lblPaging.Text = rm.GetString("lbPaging", ci);
            lblUniqueIDLists.Text = rm.GetString("lbUniqueIDLists", ci);
            lblWordsLists.Text = rm.GetString("lbWordsLists", ci);
            lblFilePathToUpload.Text = rm.GetString("lblFilePathToUpload", ci);

            dgCompanies.Columns[2].HeaderText = rm.GetString("lbUniqueID", ci);
            dgCompanies.Columns[6].HeaderText = rm.GetString("lbName", ci);
            if (CigConfig.Configure("lookupsettings.currentVersion") != null &&
                CigConfig.Configure("lookupsettings.currentVersion").Equals("czech"))
            {
                dgCompanies.Columns[7].HeaderText = rm.GetString("lbCity", ci);
            }
            else
            {
                dgCompanies.Columns[7].HeaderText = rm.GetString("lbAddress", ci);
            }
            dgCompanies.Columns[8].HeaderText = rm.GetString("txtSent", ci);

            dgIndividuals.Columns[2].HeaderText = rm.GetString("lbUniqueID", ci);
            dgIndividuals.Columns[6].HeaderText = rm.GetString("lbName", ci);
            dgIndividuals.Columns[7].HeaderText = rm.GetString("lbAddress", ci);
            dgIndividuals.Columns[8].HeaderText = rm.GetString("txtSent", ci);

            dgWordsCompanies.Columns[2].HeaderText = rm.GetString("lbName", ci);
            dgWordsCompanies.Columns[3].HeaderText = rm.GetString("lbSurName", ci);
            dgWordsCompanies.Columns[4].HeaderText = rm.GetString("lbAddress", ci);
            dgWordsCompanies.Columns[5].HeaderText = rm.GetString("lbTown", ci);
            dgWordsIndividuals.Columns[2].HeaderText = rm.GetString("lbFirstName", ci);
            dgWordsIndividuals.Columns[3].HeaderText = rm.GetString("lbSurName", ci);
            dgWordsIndividuals.Columns[4].HeaderText = rm.GetString("lbAddress", ci);
            dgWordsIndividuals.Columns[5].HeaderText = rm.GetString("lbTown", ci);
            dgWordsIndividuals.Columns[9].HeaderText = rm.GetString("txtSent", ci);

            // tooltips ...
            txtbxWords.ToolTip = rm.GetString("txtWordsUploadToolTip", ci);
            btnDeleteWords.ToolTip = rm.GetString("txtDeleteWordsToolTip", ci);
            btnRegisterWords.ToolTip = rm.GetString("txtRegisterWordsToolTip", ci);
            txtbxUnique.ToolTip = rm.GetString("txtUniqueIDUploadToolTip", ci);
            btnDeleteUnique.ToolTip = rm.GetString("txtDeleteUniqueToolTip", ci);
            btnRegisterUnique.ToolTip = rm.GetString("txtRegisterUniqueToolTip", ci);
            btnToggleList.ToolTip = rm.GetString("txtShowListToolTip", ci);
            btnSearchClaims.ToolTip = rm.GetString("txtSearchClaimsToolTip", ci);
            btnDeleteAll.ToolTip = rm.GetString("txtDeleteAllToolTip", ci);
            chkbxNative.ToolTip = rm.GetString("txtNativeCheckBoxToolTip", ci);
            lblPaging.ToolTip = rm.GetString("txtPagingToolTip", ci);
        }

        #endregion
    }
}