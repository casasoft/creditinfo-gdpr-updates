#region

using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CreditWatch.BLL;
using CreditWatch.BLL.Localization;
using CreditWatch.BLL.np_Usage;

#endregion

namespace CreditWatch {
    /// <summary>
    /// Summary description for cwClaimSearch.
    /// </summary>
    public class cwClaimSearch : Page {
        // Skilgreina instance af Factory...
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly cwFactory myFactory = new cwFactory();
        private string _address;
        private string _firstName;
        private string _id;
        private string _surName;
        protected Button btnSearch;
        protected CustomValidator CustomValidator1;
        protected CustomValidator CustomValidator2;
        protected CustomValidator CustomValidator3;
        protected DataGrid dgSearchList;
        private bool EN;
        protected HtmlAnchor help;
        protected Label lbErrMsg;
        protected Label lblAddress;
        protected Label lblCompanyName;
        protected Label lblExplain;
        protected Label lblFirstname;
        protected Label lblID;
        protected Label lblRecordCount;
        protected Label lblSearchClaims;
        protected Label lblSurname;
        protected TextBox txtAddress;
        protected TextBox txtFirstname;
        protected TextBox txtID;
        protected TextBox txtSurname;

        private void Page_Load(object sender, EventArgs e) {
            // Put user code to initialize the page here
            // add <ENTER> event to text boxes..
            AddEnterEvent();
            // Fyrir multilanguage d�mi...
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;

            LocalizeText();

            /*
			 * Teki� �t svo a� leitin � forminu skili alltaf "native" Datasetti 
			 * (user function "np_search_claims_native")
			 * 
			if (culture.Equals("en-US"))
				EN = true;
			*/

            dgSearchList.DataSource = null;
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            try {
                if (!Page.IsValid) {
                    return;
                }
                SetSearchStrings();
                if (EN) {
                    using (DataSet mySet = myFactory.GetENSearchList(_id, _firstName, _surName, _address)) {
                        dgSearchList.DataSource = mySet;
                        dgSearchList.DataBind();
                        Session["dgSearchList"] = mySet.Tables[0];

                        // Athuga hvort �etta s� starfsma�ur sem er a� leita. Ef svo er �� ekkert log...
                        if (!myFactory.CheckForEmployee(Session["UserLoginName"].ToString())) {
                            LogThis(mySet);
                        }

                        lblExplain.Visible = false;
                        if (mySet.Tables[0].Rows.Count >= 20) {
                            lblExplain.Visible = true;
                            lblExplain.Text = rm.GetString("lbSearchExplain", ci);
                        }

                        lblRecordCount.Text = mySet.Tables[0].Rows.Count + " Records found";
                    }
                } else {
                    using (DataSet mySet = myFactory.GetNativeSearchList(_id, _firstName, _surName, _address)) {
                        dgSearchList.DataSource = mySet;
                        dgSearchList.DataBind();
                        Session["dgSearchList"] = mySet.Tables[0];
                        // ColorOldCompanies();

                        // Athuga hvort �etta s� starfsma�ur sem er a� leita. Ef svo er �� ekkert log...
                        if (!myFactory.CheckForEmployee(Session["UserLoginName"].ToString())) {
                            LogThis(mySet);
                        }

                        lblExplain.Visible = false;
                        if (mySet.Tables[0].Rows.Count >= 20) {
                            lblExplain.Visible = true;
                            lblExplain.Text = rm.GetString("lbSearchExplain", ci);
                            lblRecordCount.Text = rm.GetString("lbAtLeast", ci) + mySet.Tables[0].Rows.Count + " " +
                                                  rm.GetString("lbRecordsFound", ci);
                        } else {
                            lblRecordCount.Text = mySet.Tables[0].Rows.Count + " " + rm.GetString("lbRecordsFound", ci);
                        }
                    }
                }
            } catch (Exception err) {
                // Villu hent ni�ur � skr� � gegn um loggerinn :-)
                Logger.WriteToLog("Exception on cwClaimList.aspx caught, message is : " + err.Message, true);
            }
        }

        private void LocalizeText() {
            lblID.Text = rm.GetString("lbID", ci);
            lblFirstname.Text = rm.GetString("lbFirstName", ci);
            lblSurname.Text = rm.GetString("lbSurName", ci);
            lblAddress.Text = rm.GetString("lbAddress", ci);
            btnSearch.Text = rm.GetString("txtSearch", ci);
            lblSearchClaims.Text = rm.GetString("lbSearchClaims", ci);
            lblCompanyName.Text = rm.GetString("txtCompany", ci);

            dgSearchList.Columns[0].HeaderText = rm.GetString("lbName", ci);
            dgSearchList.Columns[1].HeaderText = rm.GetString("lbAddress", ci);

            // Teki� �t vegna breytinga � skilum "np_search_claims" unserfunctions...
            // this.dgSearchList.Columns[2].HeaderText = rm.GetString("lbClaimDate",ci);
            // this.dgSearchList.Columns[3].HeaderText = rm.GetString("lbClaimOwnerName",ci);
            // this.dgSearchList.Columns[4].HeaderText = rm.GetString("lbCaseType",ci);
        }

        private void dgSearchList_SortCommand(object source, DataGridSortCommandEventArgs e) {
            var myTable = (DataTable) Session["dgSearchList"];
            var myView = myTable.DefaultView;

            myView.Sort = e.SortExpression;
            dgSearchList.DataSource = myView;
            dgSearchList.DataBind();
        }

        private void LogThis(DataSet mySet) {
            try {
                var myLog = new np_UsageBLLC
                            {
                                CreditInfoID = Convert.ToInt32(Session["UserCreditInfoID"]),
                                UserID = Convert.ToInt32(Session["UserLoginID"]),
                                QueryType = 10,
                                ResultCount = mySet.Tables[0].Rows.Count,
                                IP = Request.ServerVariables["REMOTE_ADDR"]
                            };

                // UserID nota� til a� a�greina f�rslur � loggun � milli notenda... tekur vi� af CreditInfoID
                // Sem notast framvegis til �ess a� a�greina pers�nur og fyrirt�ki (JBA - 14.10.2003)

                myFactory.AddNewUsageLog(myLog);
            } catch (Exception err) {
                Logger.WriteToLog(
                    "Logging exception (probable 0 error on CreditInfoID) on cwClaimList.aspx caught, message is : " +
                    err.Message,
                    true);
                Server.Transfer("error.aspx?err=1");
            }

            /*
			if (!myFactory.AddNewUsageLog (myLog))
			{
				Logger.WriteToLog("Logging exception (probable 0 error on CreditInfoID) on cwClaimList.aspx caught, message is : " + err.Message, true);
				Server.Transfer("error.aspx?err=1");
			}
			*/
        }

        private void SetSearchStrings() {
            // Ef �a� er eitthva� ID � gangi �� er allt teki� � "face value"...
            if (txtID.Text != "") {
                _id = txtID.Text;
                _firstName = txtFirstname.Text;
                _surName = txtSurname.Text;
                _address = txtAddress.Text;
            }

                // Annars, ef �a� er texti � einhverjum textfield �� �arf a� setja hina, sem eru au�ir
                // sem "%" svo a� leitin skili einhverri ni�urst��u...
            else if (txtFirstname.Text != "" || txtSurname.Text != "" || txtAddress.Text != "") {
                // txtFirstname field
                if (txtFirstname.Text != "") // || txtFirstname.Text != null)
                {
                    if (txtFirstname.Text.IndexOf("%") < 0) {
                        _firstName = "%" + txtFirstname.Text + "%";
                    } else {
                        _firstName = txtFirstname.Text;
                    }
                } else {
                    _firstName = "%";
                }
                // txtSurname field
                if (txtSurname.Text != "") // || txtSurname.Text != null)
                {
                    if (txtSurname.Text.IndexOf("%") < 0) {
                        _surName = "%" + txtSurname.Text + "%";
                    } else {
                        _surName = txtSurname.Text;
                    }
                } else {
                    _surName = "%";
                }
                // txtAddress field
                if (txtAddress.Text != "") // || txtAddress.Text != null)
                {
                    if (txtAddress.Text.IndexOf("%") < 0) {
                        _address = "%" + txtAddress.Text + "%";
                    } else {
                        _address = txtAddress.Text;
                    }
                } else {
                    _address = "%";
                }
            }
        }

        private void dgSearchList_ItemCommand(object source, DataGridCommandEventArgs e) {
            string Name = e.Item.Cells[0].Text;
            string Address = e.Item.Cells[1].Text;
            string City = e.Item.Cells[5].Text;
            int CreditInfoID = 0;
            if (e.Item.Cells[2].Text != "") {
                CreditInfoID = Convert.ToInt32(e.Item.Cells[2].Text);
            }

            Session["cwClaimSearchName"] = Name;
            Session["cwClaimSearchAddress"] = Address;
            Session["cwClaimSearchCreditInfoID"] = CreditInfoID;
            Session["cwClaimSearchCity"] = City;

            // Transfer to ClaimDetails page 

            if (CreditInfoID > 0) {
                Server.Transfer("cwClaimSearchDetails.aspx");
            }
        }

        private void AddEnterEvent() {
            //	this.tbClaimOwnerNative.Attributes.Add("onkeypress", "checkEnterKey();");

            txtID.Attributes.Add("onkeypress", "checkEnterKey();");
            txtFirstname.Attributes.Add("onkeypress", "checkEnterKey();");
            txtSurname.Attributes.Add("onkeypress", "checkEnterKey();");
            txtAddress.Attributes.Add("onkeypress", "checkEnterKey();");
        }

        public void CheckFirstNameInputLength(object source, ServerValidateEventArgs value) {
            if (!(string.IsNullOrEmpty(txtID.Text))) {
                value.IsValid = true;
                return;
            }
            if (!(string.IsNullOrEmpty(txtFirstname.Text))) {
                value.IsValid = true;
                return;
            }

            if (txtFirstname.Text == null && txtSurname.Text == null && txtAddress.Text == null) {
                value.IsValid = false;
                lbErrMsg.Visible = true;
                return;
            }
            if (txtFirstname.Text == "" && txtSurname.Text == "" && txtAddress.Text == "") {
                value.IsValid = false;
                lbErrMsg.Visible = true;
                return;
            }
        }

        public void CheckSurNameInputLength(object source, ServerValidateEventArgs value) {
            if (!(string.IsNullOrEmpty(txtID.Text))) {
                value.IsValid = true;
                return;
            }
            if (!(string.IsNullOrEmpty(txtSurname.Text))) {
                value.IsValid = true;
                return;
            }
            if (txtFirstname.Text == null && txtSurname.Text == null && txtAddress.Text == null) {
                value.IsValid = false;
                lbErrMsg.Visible = true;
                return;
            }
            if (txtFirstname.Text == "" && txtSurname.Text == "" && txtAddress.Text == "") {
                value.IsValid = false;
                lbErrMsg.Visible = true;
                return;
            }
        }

        public void CheckAddressInputLength(object source, ServerValidateEventArgs value) {
            if (!(string.IsNullOrEmpty(txtID.Text))) {
                value.IsValid = true;
                return;
            }
            if (!(string.IsNullOrEmpty(txtAddress.Text))) {
                value.IsValid = true;
                return;
            }
            if (txtFirstname.Text == null && txtSurname.Text == null && txtAddress.Text == null) {
                value.IsValid = false;
                lbErrMsg.Visible = true;
                return;
            }
            if (txtFirstname.Text == "" && txtSurname.Text == "" && txtAddress.Text == "") {
                value.IsValid = false;
                lbErrMsg.Visible = true;
                return;
            }
        }

        private void dgSearchList_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.Cells[3].Text != "OLD NAME") {
                return;
            }
            e.Item.BackColor = Color.OliveDrab;
            e.Item.Cells[3].Text = rm.GetString("txtOldName", ci);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.CustomValidator1.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CheckFirstNameInputLength);
            this.CustomValidator2.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CheckSurNameInputLength);
            this.CustomValidator3.ServerValidate +=
                new System.Web.UI.WebControls.ServerValidateEventHandler(this.CheckAddressInputLength);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.dgSearchList.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgSearchList_ItemCommand);
            this.dgSearchList.SortCommand +=
                new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgSearchList_SortCommand);
            this.dgSearchList.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSearchList_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}