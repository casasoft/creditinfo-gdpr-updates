<%@ Page language="c#" Codebehind="cwClaimList.aspx.cs" AutoEventWireup="false" Inherits="CreditWatch.cwClaimList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>cwClaimList</title>
		<LINK href="../css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="SearchResults" method="post" runat="server">
			<asp:Label id="lblRegisteredClaimsText" runat="server" Width="100%" CssClass="subject"></asp:Label><br><br>
			<asp:datagrid id="dgSearchList" runat="server" AllowPaging="True" CellPadding="4" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="Gray"
				Width="100%" AutoGenerateColumns="False" AllowSorting="True">
				<SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
				<AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="White"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#895A4F"></HeaderStyle>
				<FooterStyle ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="subject_name" SortExpression="subject_name" HeaderText="subject_name"></asp:BoundColumn>
					<asp:BoundColumn DataField="subject_address" SortExpression="subject_address" HeaderText="subject_address"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="creditinfoid" SortExpression="creditinfoid" HeaderText="creditinfoid"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
					<asp:BoundColumn Visible="False" DataField="subject_city" SortExpression="subject_city" HeaderText="subject_city"></asp:BoundColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
			</asp:datagrid>
		</form>
	</body>
</HTML>
