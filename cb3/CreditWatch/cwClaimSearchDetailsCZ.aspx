<%@ Page language="c#" Codebehind="cwClaimSearchDetailsCZ.aspx.cs" AutoEventWireup="false" Inherits="CreditWatch.cwClaimSearchDetailsCZ" %>
<%@ Register TagPrefix="uc1" TagName="FoOptions" Src="../new_user_controls/FoOptions.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<!DOCTYPE html public "-//w3c//dtd html 4.0 transitional//en" >
<HTML>
	<HEAD>
		<title>cwClaimSearchDetails</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="popup.js"></script>
		<style>.header { FONT-SIZE: 10px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
		.header2 { FONT-SIZE: 12px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
		.header3 { FONT-SIZE: 12px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
		.headerb { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
		.headerwhite { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
		.headerblack { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
		.conbig { FONT-WEIGHT: bolder; FONT-SIZE: 15px; COLOR: #891618; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
		.conbig2 { FONT-WEIGHT: bolder; FONT-SIZE: 14px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
		.con { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
		P.break { PAGE-BREAK-AFTER: always }
		break { PAGE-BREAK-AFTER: always }
		</style>
		<LINK href='<asp:literal id="litCssUrl" runat="server" />' type="text/css" rel="stylesheet">
	</HEAD>
	<body  leftmargin="0" rightmargin="0"
		ms_positioning="GridLayout">
		<form id="SearchDetails" method="post" runat="server">
			<table id="tblMain" cellspacing="0" cellpadding="0" width="608">
				<tr>
					<td>
						<table id="tblTitle" cellspacing="0" cellpadding="0" width="608">
						<tr>
							<td width="15" bgcolor="#891618">&nbsp;</td>
							<td width="333" height="41" bgcolor="#891618" class="headerwhite">
								<asp:label id="lblClaimDetails" runat="server">[lt] - Claim Details</asp:label>
								<asp:button id="btnPrint" runat="server" cssclass="button" visible="False"></asp:button>
							</td>
							<td background="img/redline.gif">
							&nbsp;
							</td>
							<td>
							<img src='<asp:literal id="litLogoUrl" runat="server"/>' width="279" height="41" alt="" border="0"/>
							</td>
						</tr>
						<tr height="2">
							<td>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>			
						<table class="list" id="tblHeader" cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr bgcolor="#666666">
								<td width="15" bgcolor="#666666">&nbsp;</td>
								<td>&nbsp;</td>								
								<td>&nbsp;</td>								
								<td>&nbsp;</td>								
							</tr>
							<tr bgcolor="#666666">
								<td width="15" bgcolor="#666666">
									&nbsp;
								</td>
								<td class="header2" ><strong><asp:label id="lblNameLabel" runat="server"></asp:label></strong></td>
								<td class="header2" ><asp:label id="lblName" runat="server"></asp:label></td>
								<td class="header2"  align="right" style="padding-right:5px"><asp:label id="ldlToday" runat="server" font-bold="True"></asp:label></td>
							</tr>
							<tr bgcolor="#666666">
								<td width="15" bgcolor="#666666">
									&nbsp;
								</td>
								<td class="header2" ><strong><asp:label id="lblAddressLabel" runat="server"></asp:label></strong></td>
								<td class="header2" ><asp:label id="lblAddress" runat="server"></asp:label></td>
								<td class="header2" align="right" style="padding-right:5px"><asp:label id="lblTimeStamp" runat="server"></asp:label></td>
							</tr>
							<tr bgcolor="#666666">
								<td width="15" bgcolor="#666666">
									&nbsp;
								</td>
								<td class="header2" ><strong><asp:label id="lblCityLabel" runat="server"></asp:label></strong></td>
								<td class="header2" ><asp:label id="lblCity" runat="server"></asp:label></td>
								<td class="header2" ></td>
							</tr>
							<tr bgcolor="#666666">
								<td width="15" bgcolor="#666666">
									&nbsp;
								</td>
								<td class="header2" ><asp:label id="lblCompStLabel" runat="server" font-bold="True"></asp:label><strong></strong></td>
								<td class="header2" ><asp:label id="lblCompanyStatus" runat="server"></asp:label></td>
								<td></td>
							</tr>
							<tr bgcolor="#666666">
								<td width="15" bgcolor="#666666">
									&nbsp;
								</td>
								<td class="header2" ><asp:label id="lblNationalIDlbl" runat="server" font-bold="True"></asp:label></td>
								<td class="header2" ><asp:label id="lblNationalID" runat="server"></asp:label></td>
								<td>&nbsp;</td>
							</tr>
							<tr bgcolor="#666666">
								<td width="15" bgcolor="#666666">&nbsp;</td>
								<td>&nbsp;</td>								
								<td>&nbsp;</td>								
								<td>&nbsp;</td>								
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table id="tblLegalForm" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
							<tr>
								<td width="15">&nbsp;</td>
								<td></td>
							</tr>
							<tr>
							<tr>
								<td width="15">&nbsp;</td>
								<td style="HEIGHT: 33px" align="center"><asp:label id="lblNoClaimRegistered" runat="server" forecolor="Red" visible="False">No claim registered for the subject</asp:label></td>
							</tr>
							<tr bgcolor="#666666" id="headerBankruptcy" runat="server">
								<td width="15" bgcolor="#666666">&nbsp;</td>
								<td class="header2" style="HEIGHT: 33px" align="left" ><asp:label id="lblDeptsHeader" runat="server" visible="true"></asp:label></td>
							</tr>
							<tr id="bankruptcyELine" runat="server">
								<td width="15">&nbsp;</td>
								<td align="center"></td>
							</tr>
							<tr id="bankruptcyRepeaterRow" runat="server">
								<td width="15">&nbsp;</td>
								<td align="left"><asp:repeater id="repeatBankruptcy" runat="server">
										<itemtemplate>
											<table runat="server" id="BankruptcyListing" class="list" border="0" cellspacing="0" cellpadding="3"
												width="100%">
												<tr>
													<td rowspan="3"></td>
													<td colspan="2" class="listhead">
														<b>
															<%#rm.GetString("lbCaseID",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "case_nr")%>
													</td>
													<td colspan="2" class="listhead">
														<b>
															<%#rm.GetString("lbLtReference",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
														<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbCaseType",ci)%></b><br>
														<!-- Athuga hva�a streng � a� birta... -->
														<%# GetCorrectTypeString(DataBinder.Eval(Container.DataItem, "claim_type"),DataBinder.Eval(Container.DataItem, "case_decision_type")) %>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbGazetteYear",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "gazette_year")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbGazettePage",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "gazette_page")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbInfoOrigin",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
													</td>
												</tr>
												<tr>
													<td></td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
														<%#ParseAmount(DataBinder.Eval(Container.DataItem, "amount").ToString())%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbAgentID",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "agent")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "comment")%>
													</td>
												</tr>
												<tr>
													<td><br>
													</td>
												</tr>
											</table>
										</itemtemplate>
									</asp:repeater></td>
							</tr>
							<tr bgcolor="#666666" id="headerBouncedChecks" runat="server">
								<td width="15" bgcolor="#666666">&nbsp;</td>
								<td class="header2" style="HEIGHT: 33px" align="left">Bounched checks</td>
							</tr>
							<tr id="bouncedChecksELine" runat="server">
								<td width="15">&nbsp;</td>
								<td align="center"></td>
							</tr>
							<tr id="bouncedChecksRepeaterRow" runat="server">
								<td width="15">&nbsp;</td>
								<td align="left"><asp:repeater id="repeatBouncedChecks" runat="server">
										<itemtemplate>
											<table runat="server" id="bouncedChecksListing" class="list" border="0" cellspacing="0"
												cellpadding="3" width="100%">
												<tr>
													<td rowspan="3"></td>
													<td class="listhead">
														<b>
															<%#rm.GetString("lbCaseID",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "case_nr")%>
													</td>
													<td class="listhead" colspan="2">
														<b>
															<%#rm.GetString("lbLtReference",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
														<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbCaseType",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
													
														<%#ParseAmount(DataBinder.Eval(Container.DataItem, "amount").ToString())%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbAgentID",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "agent")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbBank",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "cheque_issue_bank")%>
													</td>
												</tr>
												<tr>
													<td colspan="3" class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "comment")%>
													</td>
												</tr>
												<tr>
													<td><br>
													</td>
												</tr>
											</table>
										</itemtemplate>
									</asp:repeater></td>
							</tr>
							<tr bgcolor="#666666" id="headerCasesAgainstCompanies" runat="server">
								<td width="15" >&nbsp;</td>
								<td class="header2" style="HEIGHT: 33px" align="left">Cases against companies</td>
							</tr>
							<tr id="casesAgainstCompaniesELine" runat="server">
								<td width="15">&nbsp;</td>
								<td align="center"></td>
							</tr>
							<tr id="casesAgainstCompaniesRepeaterRow" runat="server">
								<td width="15">&nbsp;</td>
								<td align="left"><asp:repeater id="repeatCasesAgainstCompanies" runat="server">
										<itemtemplate>
											<table runat="server" id="CasesAgainstCompaniesListing" class="list" border="0" cellspacing="0"
												cellpadding="3" width="100%">
												<tr>
													<td rowspan="4"></td>
													<td class="listhead">
														<b>
															<%#rm.GetString("lbCaseID",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "case_nr")%>
													</td>
													<td colspan="2" class="listhead">
														<b>
															<%#rm.GetString("lbLtReference",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
														<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbCaseType",ci)%></b><br>
														<%# GetCorrectTypeString(DataBinder.Eval(Container.DataItem, "claim_type"),DataBinder.Eval(Container.DataItem, "case_decision_type")) %>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbGazetteYear",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "gazette_year")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbGazettePage",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "gazette_page")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbInfoOrigin",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
													</td>
												</tr>
												<tr>
													<td colspan="3" class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "comment")%>
													</td>
												</tr>
												<tr>
													<td><br>
													</td>
												</tr>
											</table>
										</itemtemplate>
									</asp:repeater></td>
							</tr>
							<tr bgcolor="#666666" id="headerOther" runat="server">
								<td width="15" >&nbsp;</td>
								<td class="header2" style="HEIGHT: 33px" align="left" >Misc. 
									cases</td>
							</tr>
							<tr id="otherELine" runat="server">
								<td width="15">&nbsp;</td>
								<td align="center"></td>
							</tr>
							<tr id="otherRepeaterRow" runat="server">
								<td width="15">&nbsp;</td>
								<td align="left"><asp:repeater id="repeatOther" runat="server">
										<itemtemplate>
											<table runat="server" id="OtherCasesListing" class="list" border="0" cellspacing="0" cellpadding="3"
												width="100%">
												<tr>
													<td rowspan="3"></td>
													<td colspan="2" class="listhead">
														<b>
															<%#rm.GetString("lbCaseID",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "case_nr")%>
													</td>
													<td colspan="2" class="listhead">
														<b>
															<%#rm.GetString("lbLtReference",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
														<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbCaseType",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
														<%#ParseAmount(DataBinder.Eval(Container.DataItem, "amount").ToString())%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbAgentID",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "agent")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "comment")%>
													</td>
												</tr>
												<tr>
													<td><br>
													</td>
												</tr>
											</table>
										</itemtemplate>
									</asp:repeater></td>
							</tr>
							<tr bgcolor="#666666" id="headCzDDD" runat="server" >
								<td width="15" >&nbsp;</td>
								<td class="header2" style="HEIGHT: 33px" align="left" ><asp:label id="lblClaims" cssclass="header2" runat="server">Claims</asp:label></td>
							</tr>
							<tr id="czDDDELine" runat="server">
								<td width="15">&nbsp;</td>
								<td aign="center"></td>
							</tr>
							<tr id="czDDDRepeaterRow" runat="server">
								<td width="15">&nbsp;</td>
								<td align="left"><asp:repeater id="repeatCzDDD" runat="server">
										<itemtemplate>
											<table runat="server" id="CzListing" class="list" border="0" cellspacing="0" cellpadding="3"
												width="100%">
												<tr>
													<td rowspan="3"></td>
													<td colspan="2" class="listhead">
														<b>
															<%#rm.GetString("lbCaseID",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "comment")%>
													</td>
													<td colspan="2" class="listhead">
														<b>
															<%#rm.GetString("lbLtReference",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
														<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "claim_owner_name")%>
													</td>
													<td class="dark-row" valign="top"><b></b><br>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbCZClaimAmount",ci)%></b><br>
														<%#ParseAmount(DataBinder.Eval(Container.DataItem, "amount","{0:N}").ToString())%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbComment",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "comment")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("txtInformationSource",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
													</td>
												</tr>
												<tr>
													<td><br>
													</td>
												</tr>
											</table>
										</itemtemplate>
									</asp:repeater></td>
							</tr>
							<tr id="headLtDDD" runat="server">
								<td width="15" bgcolor="#666666">&nbsp;</td>
								<td class="header2" style="HEIGHT: 33px" align="left" ><asp:label id="lblLtClaims" runat="server" cssclass="header2">Claims LT</asp:label></td>
							</tr>
							<tr id="ltDDDELine" runat="server">
								<td width="15">&nbsp;</td>
								<td align="center"></td>
							</tr>
							<tr id="ltDDDRepeaterRow" runat="server">
								<td width="15">&nbsp;</td>
								<td align="left"><asp:repeater id="repeatLtDDD" runat="server">
										<itemtemplate>
											<table runat="server" id="LtListing" class="list" border="0" cellspacing="0" cellpadding="3"
												width="100%">
												<tr>
													<td rowspan="3"></td>
													<td colspan="2" class="listhead">
														<b>
															<%#rm.GetString("lbCaseID",ci)%>
														</b>
														<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
													</td>
													<td colspan="2" class="listhead">
														<b>
															<%#rm.GetString("lbDirector",ci)%>
															: </b>
														<%#DataBinder.Eval(Container.DataItem, "pname")%>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimType",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
														<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
														<%#ParseAmount(DataBinder.Eval(Container.DataItem, "amount").ToString())%>
													</td>
													<td class="dark-row" valign="top"><b></b><br>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerCode",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "creditor_code","{0:N}")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "creditor")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("txtInformationSource",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
													</td>
												</tr>
												<tr>
													<td><br>
													</td>
												</tr>
											</table>
										</itemtemplate>
									</asp:repeater></td>
							</tr>
							<tr id="ltPersonsDDDRepeaterRow" runat="server">
								<td width="15">&nbsp;</td>
								<td align="left"><asp:repeater id="repeatLtPersonDDD" runat="server">
										<itemtemplate>
											<table runat="server" id="LtPersonListing" class="list" border="0" cellspacing="0" cellpadding="3"
												width="100%">
												<tr>
													<td rowspan="4"></td>
													<td class="listhead">
														<b>	<%#rm.GetString("lbCaseID",ci)%>: </b>
															<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
													</td>
													<td class="listhead">
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbPersonCode",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "code")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbPersonName",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "pname")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbPersonAddress",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "address")%>
													</td>
													<td class="dark-row" valign="top"><b></b><br>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimType",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
														<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
														<%#ParseAmount(DataBinder.Eval(Container.DataItem, "amount").ToString())%>
													</td>
													<td class="dark-row" valign="top"><b></b><br>
													</td>
												</tr>
												<tr>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerCode",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "creditor_code","{0:N}")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "creditor")%>
													</td>
													<td class="dark-row" valign="top"><b><%#rm.GetString("txtInformationSource",ci)%></b><br>
														<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
													</td>
												</tr>
												<tr>
													<td><br>
													</td>
												</tr>
											</table>
										</itemtemplate>
									</asp:repeater></td>
							</tr>
							<tr bgcolor="#666666">
								<td width="15" bgcolor="#666666">&nbsp;</td>
								<td class="header2" style="HEIGHT: 33px" align="left"><asp:label id="lblRecent" runat="server"></asp:label></td>
							</tr>
							<tr>
								<td width="15">&nbsp;</td>
								<td align="left"><asp:label id="lblSixMonths" runat="server" cssclass="list"></asp:label></td>
							</tr>
							<tr>
								<td width="15">&nbsp;</td>
								<td style="HEIGHT: 15px" align="left"><asp:label id="lblThreeMonths" runat="server" cssclass="list"></asp:label></td>
							</tr>
							<tr>
								<td width="15">&nbsp;</td>
								<td align="left"><asp:label id="lblOneMonth" runat="server" cssclass="list"></asp:label></td>
							</tr>
							<tr>
								<td width="15">&nbsp;</td>
								<td style="HEIGHT: 55px" align="left"><asp:label id="lblDisclaimer" runat="server"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>													
			</table>
			<table width="608">
				<tr>
					<td style="HEIGHT: 15px" align="center" bgcolor="#951e16" colspan="8">
					</td>
				</tr>
				<tr class="dark-row">
					<td style="HEIGHT: 10px" align="center" colspan="8">
					</td>
				</tr>
				<tr>
					<td align="center" colspan="8">
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
