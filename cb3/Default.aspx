<%@ Page Language="c#" CodeBehind="Default.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.Default" %>

<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/head.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="new_user_controls/options.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>default</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="css/CIGStyles.css" type="text/css" rel="stylesheet">
</head>

<body ms_positioning="GridLayout">
    <form id="fromDefault" method="post" runat="server">
        <table width="997" height="600" align="center" border="0">
            <tr>
                <td colspan="4">
                    <uc1:head ID="Head1" runat="server"></uc1:head>
                </td>
            </tr>
            <tr valign="top">
                <td width="1"></td>
                <td>
                    <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <uc1:language ID="Language1" runat="server"></uc1:language>
                            </td>
                            <td></td>
                            <td align="right">
                                <ucl:options ID="Options1" runat="server"></ucl:options>
                                </UCL:OPTIONS></UCL:OPTIONS>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <uc1:sitePositionBar ID="SitePositionBar1" runat="server"></uc1:sitePositionBar>
                            </td>
                            <td align="right">
                                <ucl:UserInfo ID="UserInfo1" runat="server"></ucl:UserInfo>
                            </td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr valign="top">
                            <td width="150" valign="top" align="left">
                                <table width="98%">
                                    <tr>
                                        <td>
                                            <uc1:panelBar ID="PanelBar1" runat="server"></uc1:panelBar>
                                            <table class="menu" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <th>
                                                        <a href="LoginPageEditor.aspx" class="menu">Login page editor</a>
                                                    </th>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                </table>
                            </td>
                            <td colspan="2">
                                <!-- Main Body Starts -->

                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table class="grid_table" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <th>
                                                        <asp:Label ID="lblWelcome" runat="server" CssClass="HeadMain">Welcome to CREDITINFOGROUP</asp:Label>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="fields" id="tbDefault" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblMainSubject" runat="server" Width="100%">Creditinfo Cyprus Ltd provides the business community with credit information on Cypriot companies and individuals. We can also provide information on foreign companies. Creditinfo�s reports are useful to all kinds of businesses, especially those that provide loans or extend credit to customers on a regular basis. Any company needing information on customers, suppliers or other business partners can benefit from Creditinfo�s services. Creditinfo Cyprus Ltd is owned by the LT Creditinfo Holding Ltd and is part of Creditinfo Group, which operates credit information businesses in four countries.</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblMainSubject2" runat="server" Width="100%"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblMainSubject3" runat="server" Width="100%"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblMainSubject4" runat="server" Width="100%"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblMainSubject5" runat="server" Width="100%"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblFooter" runat="server" Width="100%">Do you need to contact us? You can use the link below to get in touch with us at any time. We will be more than happy to help..</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblFooter2" runat="server" Width="100%"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:HyperLink ID="hlMail" runat="server" NavigateUrl="mailto:info@creditinfo.com.mt">e-mail:info@creditinfo.com.mt</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="23"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                                <!-- Main Body Ends -->
                            </td>
                        </tr>
                        <tr>
                            <td height="20"></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
                        </tr>
                    </table>
                </td>
                <td width="2"></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <uc1:footer ID="Footer1" runat="server"></uc1:footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
