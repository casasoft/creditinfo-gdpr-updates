<%@ Page language="c#" Codebehind="DatePicker.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.DatePicker" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>DatePicker</title>
		<link href="css/CIGStyles.css" type="text/css" rel="stylesheet">
			<script language="Javascript">
        function SetDate(dateValue)
        {
            // retrieve from the querystring the value of the Ctl param,
            // that is the name of the input control on the parent form
            // that the user want to set with the clicked date
            ctl = window.location.search.substr(1).substring(4);
            // set the value of that control with the passed date
           window.opener.document.forms[0].elements[ctl].value = dateValue;
            // close this popup
            self.close();
        }
			</script>
	</head>
	<body>
		<form id="frmPopupCalendar" method="post" runat="server">
			<asp:calendar id="DatePick" runat="server" height="100%" width="100%" cssclass="datepicker" cellpadding="0"
				borderstyle="None">
				<todaydaystyle cssclass="datepicker_today"></todaydaystyle>
				<titlestyle cssclass="datepicker_title" backcolor="White"></titlestyle>
				<othermonthdaystyle cssclass="datepicker_othermonthday"></othermonthdaystyle>
			</asp:calendar></form>
	</body>
</html>
