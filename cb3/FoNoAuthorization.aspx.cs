using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;
using Logging.BLL;

namespace CreditInfoGroup
{
	/// <summary>
	/// Summary description for FoNoAuthorization.
	/// </summary>
	public class FoNoAuthorization : Page
	{
		protected HyperLink hlMail;
		public static CultureInfo ci;
		protected Label lblNoAuthorizationHeader;
		protected Label lblNoAuthorization;
		public static ResourceManager rm;
	
		private void Page_Load(object sender, EventArgs e)
		{
			rm = CIResource.CurrentManager;
			ci = Thread.CurrentThread.CurrentCulture;
			
			string ipAddress = Request.ServerVariables["REMOTE_ADDR"];
			string user = Request.ServerVariables["AUTH_USER"];
			string page = Request.ServerVariables["HTTP_REFERER"];
			string crackPage = Convert.ToString(Session["noAuthorizationPage"]);
			string message = "Violator IP: " + ipAddress + 
				" | Violating user: " + user + 
				" | Coming from page: " + page +
				" | Violating page: " + crackPage;

			Logger.WriteToLog(message, false);
			
			LocalizeText();
		}

		private void LocalizeText()
		{
			this.lblNoAuthorizationHeader.Text = rm.GetString("lblNoAuthorizationHeader",ci);
			this.lblNoAuthorization.Text = rm.GetString("lblNoAuthorization",ci);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
