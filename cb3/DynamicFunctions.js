
function CurrencyBlur(ctl,groupSep,decSep,decCount,NaN)
{
	Currencyformat(ctl,groupSep,decSep,decCount,NaN);
	
	if (ctl.value != NaN)
	{
		var preId = ctl.id.substring(0,ctl.id.indexOf("_",ctl.id.indexOf("__")+2)+1);
		var totalAmountCtl = document.getElementById(preId+"TotalAmount");
		var paymentAmountCtl = document.getElementById(preId+"PaymentAmount");
		var discountAmountCtl = document.getElementById(preId+"DiscountAmount");
		var remainderAmountCtl = document.getElementById(preId+"RemainderAmount");
		remainderAmountCtl.value = getCurrency(totalAmountCtl.value,groupSep,decSep)-(getCurrency(paymentAmountCtl.value,groupSep,decSep)+getCurrency(discountAmountCtl.value,groupSep,decSep));
		Currencyformat(remainderAmountCtl,groupSep,decSep,decCount,NaN);
	}
}

function getCurrency(strvalue,groupSep,decSep)
{
	//store value.
	var sdec = strvalue;
	//Make regular expression for group separator
	if (groupSep == ".")
		re = /\./g;
	else
		re = new RegExp("["+groupSep+"]");
	//Declare
	var dec = null;
	var cat = null;
	//Replace groupSetp with regular expression to nothing.
	sdec = sdec.replace(re,"");
	//Replace decSetp with regular expression to "." for calculating.
	if (decSep != ".")
	{
		re = new RegExp("["+decSep+"]");
		sdec = sdec.replace(re,".");
	}
	return parseFloat(sdec);
}


function Currencyformat(ctl,groupSep,decSep,decCount,NaN)
{
	//store value.
	var sdec = ctl.value;
	
	var re = null;
	var strRe = null;
	//Check if number ok.
	if (groupSep == ".")
		strRe = "\\d+(\\.{1}\\d{3})*"
	else
		strRe = "\\d+("+groupSep+"{1}\\d{3})*"

	if (decCount > 0)
	{
		if (decSep == ".")
			strRe += "(\\.{1}\\d*)?"
		else
			strRe += "("+decSep+"{1}(\\d)*)?"
	}
	strRe += "$";
	re = new RegExp(strRe);
	
	var b = re.test(sdec);

	if ((b == false) && (sdec.length > 0))
		ctl.value = NaN;
	else
	{
		//Make regular expression for group separator
		if (groupSep == ".")
			re = /\./g;
		else
			re = new RegExp("["+groupSep+"]");
		//Declare
		var dec = null;
		var cat = null;
		//Replace groupSetp with regular expression to nothing.
		sdec = sdec.replace(re,"");
		//Replace decSetp with regular expression to "." for calculating.
		if (decSep != ".")
		{
			re = new RegExp("["+decSep+"]");
			sdec = sdec.replace(re,".");
		}
		
		//Check if negative.
		var isNegative = false;
		if (sdec.substring(0,1) == "-")
		{
			isNegative = true;
			sdec = Math.abs(sdec);
		}
		
		//Get the rest and make 0.25 to 25, add 0.000001 calculation error.
		var ddec = Math.floor((sdec%1)*Math.pow(10,decCount)+0.00000001);
		sdec = Math.floor(sdec);
		
		cat = "";
		//Calculate count of 1000 seperators.
		dec = Math.floor(Math.floor(Math.log(sdec)/Math.LN10+0.00000001)/3)+1;
		//Format number
		var part = null;
		for (var i=dec;i>0;i--)
		{
			part = Math.floor(sdec%Math.pow(1000,i)/Math.pow(1000,i-1));
			
			//Check if zero fill is needed.
			if (i!=dec)
			{
				if (part <= 9)
					part = "00"+part;
				else if (part <= 99)
					part = "0"+part;
			}
			
			//Check if groupSep is needed.
			if (i>1)
			part = part + groupSep;
					
			cat = cat + part;
		}
		
		//Check if empty.
		if (cat.length == 0)
		cat = "0";
		
		//Set decimal separator in place.
		if (decCount > 0)
		{
			var strdec  = new String(ddec);
			cat += decSep;
			for (var z=strdec.length;z<decCount;z++)
				cat += "0";
			cat += ddec;
		}
		
		if(isNegative)
		  cat = "-"+cat;
		
		//Return the value.
		ctl.value = cat;
	}
}

function SelectAllCheckboxes(chk) 
{ 
	for(i=0;i<chk.form.elements.length;i++) 
	{ 
		if ((chk.form.elements[i].type=='checkbox') && (chk.form.elements[i].id!=chk.id)) 
		{                                                        
			chk.form.elements[i].checked=chk.checked; 
			HighlightRowOnCheck(chk.form.elements[i]);
		}  
	}
}

function HighlightRowOnCheck(chkB)
{
	if (chkB.checked)
	{
		chkB.parentElement.parentElement.style.backgroundColor='#D0FFC0';
		chkB.parentElement.parentElement.tabIndex=1;
	}
	else
	{
		chkB.parentElement.parentElement.style.backgroundColor='White';
		chkB.parentElement.parentElement.tabIndex=0;
	}
}
function PopupExtHandler(ctl,nationalfield,namefield1,namefield2,w,h,pickvalue)
{
	var PopupWindow=null;
	var settings='width='+ w + ',height='+ h + ',location=no,directories=no, menubar=no,toolbar=no,status=no,scrollbars=yes,resizable=yes, dependent=yes';
	PopupWindow=window.open('../UserAdmin/ExtUserHandling.aspx?Ctl=' + ctl +'&nationalfield='+nationalfield+'&namefield1='+namefield1+'&namefield2='+namefield2+'&type=' +pickvalue,'ExtHandling', settings);	
	PopupWindow.focus();
}

function GoNordurNidur(fieldname,nationalfield,namefield1,namefield2,radControl,w,h)
{
	var selvalue = "2";
	if(radControl.type == "radio") { // then only the individual radio button is passed
		if(radControl.checked){ // then individual
			selvalue = "2";
		}
		else {
			selvalue = "1";
		}
			
	}
	else if(radControl == "1" || radControl == "2") {
		selvalue = radControl;
	}
	else {
		selvalue = getRadioSelValue(radControl);
	}
	PopupExtHandler(fieldname,nationalfield,namefield1,namefield2,w,h,selvalue);

}
function PopupExtHandler1(ctl, nationalfield, namefield1, namefield2, w, h, pickvalue) {
    var PopupWindow = null;
    var settings = 'width=' + w + ',height=' + h + ',location=no,directories=no, menubar=no,toolbar=no,status=no,scrollbars=yes,resizable=yes, dependent=yes';
    PopupWindow = window.open('../../UserAdmin/ExtUserHandling.aspx?Ctl=' + ctl + '&nationalfield=' + nationalfield + '&namefield1=' + namefield1 + '&namefield2=' + namefield2 + '&type=' + pickvalue, 'ExtHandling', settings);
    PopupWindow.focus();
}

function GoNordurNidur1(fieldname, nationalfield, namefield1, namefield2, radControl, w, h) {
    var selvalue = "2";
    if (radControl.type == "radio") { // then only the individual radio button is passed
        if (radControl.checked) { // then individual
            selvalue = "2";
        }
        else {
            selvalue = "1";
        }

    }
    else if (radControl == "1" || radControl == "2") {
        selvalue = radControl;
    }
    else {
        selvalue = getRadioSelValue(radControl);
    }
    PopupExtHandler1(fieldname, nationalfield, namefield1, namefield2, w, h, selvalue);

}
function getRadioSelValue(radControl) 
{ 
	for(var i = 0; i < radControl.length; i++) 
	{ 
		if(radControl[i].checked) 
		{ 
			return radControl[i].value; 
		} 
	}
	return false; 
}
