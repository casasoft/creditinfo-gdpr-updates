﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using Logging.BLL;

namespace cb3.Audit
{
    public class AuditRepository : IDisposable
    {
        private OleDbConnection _myOleDbConn { get; set; }

        public AuditRepository()
        {
            _myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString());
            _myOleDbConn.Open();
        }


        public bool Add(AuditModel audit, IDbCommand dBCommand) //add funtionality uses the main audit query DBcommand and connection 
        {
            //var myTrans = _myOleDbConn.BeginTransaction();
            try
            {
                //var myOleDbCommand = new OleDbCommand(
                //        "INSERT INTO audit(DateTimeStamp, AuditActionType, AuditTableName, FieldName, ValueBefore, ValueAfter, AuditActionID, UserId) VALUES(?,?,?,?,?,?,?,?)",
                //        _myOleDbConn)
                //    { Transaction = myTrans };


                var myOleDbCommand = new OleDbCommand(dBCommand.CommandText, (OleDbConnection)dBCommand.Connection) { Transaction = (OleDbTransaction)dBCommand.Transaction };

                myOleDbCommand.CommandText = "INSERT INTO audit(DateTimeStamp, AuditActionType, AuditTableName, FieldName, ValueBefore, ValueAfter, AuditActionID, UserId) VALUES(?,?,?,?,?,?,?,?)";
                myOleDbCommand.Parameters.Clear();

                var myParam = new OleDbParameter("DateTimeStamp", OleDbType.Date)
                {
                    Value = audit.DateTimeStamp,
                    Direction = ParameterDirection.Input
                };
                myOleDbCommand.Parameters.Add(myParam);

                myParam = new OleDbParameter("AuditActionType", OleDbType.Integer)
                {
                    Value = audit.AuditActionType,
                    Direction = ParameterDirection.Input
                };
                myOleDbCommand.Parameters.Add(myParam);

                myParam = new OleDbParameter("AuditTableName", OleDbType.VarChar)
                {
                    Value = audit.AuditTableName,
                    Direction = ParameterDirection.Input
                };
                myOleDbCommand.Parameters.Add(myParam);

                myParam = new OleDbParameter("FieldName", OleDbType.VarChar)
                {
                    Value = audit.FieldName,
                    Direction = ParameterDirection.Input
                };
                myOleDbCommand.Parameters.Add(myParam);


                myParam = new OleDbParameter("ValueBefore", OleDbType.VarChar)
                {
                    Value = audit.ValueBefore,
                    Direction = ParameterDirection.Input
                };
                myOleDbCommand.Parameters.Add(myParam);


                myParam = new OleDbParameter("ValueAfter", OleDbType.VarChar)
                {
                    Value = audit.ValueAfter,
                    Direction = ParameterDirection.Input
                };
                myOleDbCommand.Parameters.Add(myParam);


                myParam = new OleDbParameter("AuditActionID", OleDbType.VarChar)
                {
                    Value = audit.AuditActionID,
                    Direction = ParameterDirection.Input
                };
                myOleDbCommand.Parameters.Add(myParam);


                myParam = new OleDbParameter("UserId", OleDbType.Integer)
                {
                    Value = audit.UserId,
                    Direction = ParameterDirection.Input
                };
                myOleDbCommand.Parameters.Add(myParam);


                myOleDbCommand.ExecuteNonQuery();
                //myTrans.Commit();


            }
            catch(Exception ex)
            {
                
                Logger.WriteToLog("Error adding aduit record. " + nameof(AuditRepository) + ": " + nameof(Add), ex, true);
                return false;
                
            }

            return true;

        }



        public List<AuditModel> Get(AuditParamModel param)
        {
            var result = new List<AuditModel>();
            var returnString = "";
            try
            {
                /// prepare query
                string strSql = "SELECT ROW_NUMBER() OVER (ORDER BY ID desc ) AS 'RowNumber', * "
                                        + "FROM audit ";

                var start = param.Start;
                var lenght = param.Length;

                if (param.DateFrom != DateTime.MinValue && param.DateTo != DateTime.MinValue)
                {
                    strSql += string.Format(" WHERE  DateTimeStamp between '{0} 00:00:00' AND '{1} 23:59:59' ", param.DateFrom.ToString("yyyy-MM-dd"), param.DateTo.ToString("yyyy-MM-dd"));
                }

                if (lenght > 0)
                {
                    start = start + 1;
                    returnString = string.Format("WITH tableLimit AS ( " + strSql + " )  SELECT * FROM tableLimit WHERE RowNumber BETWEEN {0} AND {1} ", start, lenght + start - 1);
                    

                }
                

                OleDbCommand myCommand = new OleDbCommand(returnString, _myOleDbConn);
                var reader = myCommand.ExecuteReader();

                while (reader.Read())
                {
                    result.Add(new AuditModel()
                    {
                        DateTimeStamp = (DateTime)reader["DateTimeStamp"],
                        AuditActionID = (string)reader["AuditActionID"],
                        AuditActionType = (AuditActionType)reader["AuditActionType"],
                        AuditTableName = (string)reader["AuditTableName"],
                        FieldName = (string)reader["FieldName"],
                        ValueAfter = (string)reader["ValueAfter"],
                        ValueBefore = (string)reader["ValueBefore"],
                        UserId = (int)reader["UserId"]
                    });
                }

                return result;
            }
            catch (Exception e)
            {
                throw new Exception("An Error Occured Getting Audit Data", e);

            }

        }



        public int GetTotalAuditCount(AuditParamModel param)
        {
            try
            {
                var strSql = "SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL) ) AS 'RowNumber', * "
                                      + "FROM audit ";


                if (param.DateFrom != DateTime.MinValue && param.DateTo != DateTime.MinValue)
                {
                    strSql += string.Format(" WHERE  DateTimeStamp between '{0} 00:00:00' AND '{1} 23:59:59' ", param.DateFrom.ToString("yyyy-MM-dd"), param.DateTo.ToString("yyyy-MM-dd"));
                }


                var returnString = string.Format("WITH CountRecord AS ( " + strSql + " )  SELECT TOP 1 RowNumber FROM CountRecord order by RowNumber desc ");




                OleDbCommand myCommand = new OleDbCommand(returnString, _myOleDbConn);
                object value = myCommand.ExecuteScalar();

                if (value == null) return 0;

                return Convert.ToInt32(value);


            }
            catch (Exception e)
            {
                throw new Exception("An Error Occured Getting Audit Data", e);

            }

        }



        public void Dispose()
        {
            _myOleDbConn.Close();
        }

    }
}