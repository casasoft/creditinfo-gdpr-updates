﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Audit
{
    public class AuditModel
    {
        public DateTime? DateTimeStamp { get; set; }

        public AuditActionType AuditActionType { get; set; }

        public string AuditTableName { get; set; }

        public string AuditActionID { get; set; }

        public string FieldName { get; set; }

        public string ValueBefore { get; set; }

        public string ValueAfter { get; set; }

        public int UserId { get; set; }
    }
}