﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Audit
{
    public class AuditDelta
    {
        public string FieldName { get; set; }
        public string ValueBefore { get; set; } = "";
        public string ValueAfter { get; set; }
    }
}