﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Audit
{
    public class AuditViewModel
    {
        public string DateTimeStamp { get; set; }

        public string AuditActionType { get; set; }

        public string AuditTableName { get; set; }

        public string AuditActionID { get; set; }

        public string FieldName { get; set; }

        public string ValueBefore { get; set; }

        public string ValueAfter { get; set; }

        public string UserName { get; set; }
    }
}