﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using cb3.Audit;
using System.Web.Script.Serialization;
using UserAdmin.BLL;

namespace cb3.Audit
{
    public partial class ViewAudit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ID = "20002";

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();

        }


        //public string GetAuditList()
        //{
        //    var auditRepo = new AuditRepository();
        //    var listOfAudit = auditRepo.Get();
        //    var userFactory = new uaFactory();

        //    var listAuditView = listOfAudit.ConvertAll(t => new AuditViewModel {
        //        DateTimeStamp = t.DateTimeStamp.ToString(),
        //        AuditActionType = Enum.GetName(typeof(AuditActionType), t.AuditActionType),
        //        AuditTableName = t.AuditTableName.ToString(),
        //        AuditActionID = t.AuditActionID.ToString(),
        //        FieldName = t.FieldName.ToString(),
        //        ValueBefore = t.ValueBefore.ToString(),
        //        ValueAfter = t.ValueAfter.ToString(),
        //        UserName = (t.UserId != 0)? userFactory.GetUserName(t.UserId) : "",
        //    });

        //    return new JavaScriptSerializer().Serialize(listAuditView);
            
        //}
    }
}