﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Audit
{
    public class AuditChange
    {
        public string AuditTableName { get; set; }
        public List<AuditDelta> AuditDeltas { get; set; }
        public AuditChange()
        {
            AuditDeltas = new List<AuditDelta>();
        }
    }
}