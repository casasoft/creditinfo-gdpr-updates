﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Audit
{
    public class DbParameterAudit
    {
        public string ParameterName { get; set; }
        public object Value { get; set; }
    }
}