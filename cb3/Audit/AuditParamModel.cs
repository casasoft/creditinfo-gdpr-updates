﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Audit
{
    public class AuditParamModel
    {
        public long Start { get; set; }

        public long Length { get; set; }

        public int OrderColumn { get; set; }

        public string OrderDir { get; set; }

        public DateTime  DateFrom { get; set; }

        public DateTime DateTo { get; set; }
    }
}