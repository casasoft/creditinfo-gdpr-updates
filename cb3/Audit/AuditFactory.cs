﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;
using System.Web;
using Logging.BLL;

namespace cb3.Audit
{
    public class AuditFactory
    {
        private AuditChange _auditChange { get; set; }
        private IDbCommand _myDbCommand { get;}
        private string _commandText { get; set; }
        private int _userId { get; }
        private List<DbParameterAudit> _parameters { get; set; }


        public AuditFactory(IDbCommand DbCommand, int UserId = 0)
        {
            this._myDbCommand = DbCommand;
            this._commandText = _myDbCommand.CommandText.ToUpper();
            this._userId = UserId;
            if (UserId == 0 && HttpContext.Current.Session["UserLoginID"] != null)
            {
                this._userId = Convert.ToInt32(HttpContext.Current.Session["UserLoginID"]);

            }
            
        }

        

        public bool PerformAuditProcess()
        {
            try
            {

                var sqlOperation = _commandText
                    .Substring(0, _commandText.IndexOf(" ")).ToUpper();
                var tableName = getSqlTableName();
                var auditDeltas = new List<AuditDelta>();
                populateParameters(_myDbCommand.Parameters);

                switch (sqlOperation)
                {
                    case "UPDATE":
                    case "DELETE":
                    {
                        var sqlGeneratedString = getGeneratedSql();
                        var whereString = sqlGeneratedString.Substring(sqlGeneratedString.LastIndexOf("WHERE"), sqlGeneratedString.Length - sqlGeneratedString.LastIndexOf("WHERE")); //return where part of the query string
                        
                        if(sqlOperation == "DELETE")
                        {
                            auditDeltas = getAuditDeltas(whereString, tableName, true);
                            _auditChange = new AuditChange() { AuditTableName = tableName, AuditDeltas = auditDeltas };
                            saveAudit(AuditActionType.Delete);

                        }
                        else
                        {
                            auditDeltas = getAuditDeltas(whereString, tableName);
                            _auditChange = new AuditChange() { AuditTableName = tableName, AuditDeltas = auditDeltas };
                            saveAudit(AuditActionType.Update);

                        }

                        break;
                    }
                    case "INSERT":
                    {
                        foreach (var p in _parameters)
                        {
                            if (!string.IsNullOrEmpty(p.Value.ToString()))
                            {
                                auditDeltas.Add(new AuditDelta()
                                {
                                    FieldName = p.ParameterName,
                                    ValueBefore = "",
                                    ValueAfter = p.Value.ToString()
                                });
                            }

                        }
                        this._auditChange = new AuditChange() {AuditTableName = tableName, AuditDeltas = auditDeltas };
                        saveAudit(AuditActionType.Create);

                        break;
                    }
                    default:
                    {
                        throw new Exception("An error occured, Wrong SQL operation: "+ sqlOperation);
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.WriteToLog("An Error Occured during audit process " + nameof(AuditFactory) + ": " + nameof(PerformAuditProcess), ex, true);
                return false;

            }

            return true;



    }



        #region Private Members

        private void saveAudit(AuditActionType action)
        {
            try
            {

                using (var auditRepo = new AuditRepository())
                {
                    var auditChanges = this._auditChange.AuditDeltas;
                    var auditActionId = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
                    foreach (var change in auditChanges)
                    {
                        auditRepo.Add(new AuditModel()
                        {
                            DateTimeStamp = DateTime.Now,
                            FieldName = change.FieldName,
                            AuditActionType = action,
                            AuditTableName = _auditChange.AuditTableName,
                            ValueAfter = change.ValueAfter,
                            ValueBefore = change.ValueBefore,
                            AuditActionID = auditActionId,
                            UserId = this._userId

                        }, _myDbCommand);

                    }

                }

            }
            catch (Exception ex)
            {
                Logger.WriteToLog("Execption thrown while Saving audit " + nameof(AuditFactory) + ": " + nameof(saveAudit), ex, true);
            }


        }


        private List<AuditDelta> getAuditDeltas(string whereString, string tableName, bool isDelete = false)
        {
            using (var myOleDbConn = new OleDbConnection(DatabaseHelper.ConnectionString()))
            {
                myOleDbConn.Open();
                try
                {
                    var changes = new List<AuditDelta>();
                    var myOleDbCommand = new OleDbCommand(
                        "SELECT * FROM " + tableName + " " + whereString, myOleDbConn);

                    using (OleDbDataReader dr = myOleDbCommand.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (isDelete)
                            {
                                var table = dr.GetSchemaTable();
                                var nameCol = table.Columns["ColumnName"];
                                foreach (DataRow row in table.Rows)
                                {
                                    var columnName = (string)row[nameCol];

                                    if(string.IsNullOrEmpty(dr[columnName]?.ToString())) continue; //skip adding the item to the audit if the previous item is empty

                                    changes.Add(new AuditDelta() { FieldName = row[nameCol].ToString(), ValueBefore = dr[columnName].ToString(), ValueAfter = "" });
                                }
                                
                            }
                            else
                            {
                                foreach (DbParameterAudit p in _parameters)
                                {
                                    object colValue;
                                    try
                                    {
                                        colValue = (object)dr[p.ParameterName];
                                        // this quick hack solves the isssue of error thrown if the column name does not exist in the table 
                                        // there are some queries in which they defined the wrong column name in the code
                                    }
                                    catch (Exception e)
                                    {
                                        continue;
                                    }
                                    var pbefore = sanitizeOleDbParameter(colValue).ToString();
                                    if (pbefore.Trim() != p.Value.ToString().Trim())
                                    {

                                        changes.Add(new AuditDelta() { FieldName = p.ParameterName, ValueBefore = pbefore, ValueAfter = p.Value.ToString() });

                                    }



                                }
                            }
                            
                           
                        }
                    }
                    return changes;


                }
                catch (Exception ex)
                {
                    Logger.WriteToLog("Error Getting old changes from table: " + nameof(AuditFactory) + ": " + nameof(getAuditDeltas), ex, true);
                    return null;

                }
            }

        }

       
        #endregion




        #region Utilities

        private string getSqlTableName()
        {
            Regex r = new Regex(@"(?<=FROM|JOIN|INTO|UPDATE)(\s+\w+\b)",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);
            Match m = r.Match(_commandText);

            return m.Value.Trim();

        }



        private static string replaceFirstOccurrence(string Source, string Find, string Replace)
        {
            int Place = Source.IndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }



        private string getGeneratedSql()
        {
            var result = _commandText;
            foreach (DbParameterAudit p in _parameters)
            {
                string isQuted = (p.Value is string) ? "'" : "";
                //p.Value = sanitizeOleDbParameter(p.Value);
                result = replaceFirstOccurrence(result, "?", isQuted + p.Value + isQuted);
            }
            return result;
        }


        private void populateParameters(IDataParameterCollection parameters)
        {

            _parameters = new List<DbParameterAudit>();
            foreach (IDataParameter p in parameters)
            {
                 var sanitizedOleDbParameterValue = sanitizeOleDbParameter(p.Value);
                _parameters.Add(new DbParameterAudit() { ParameterName = p.ParameterName, Value = sanitizedOleDbParameterValue });

            }
            
        }



        private object sanitizeOleDbParameter(object value)
        {
            if(value is null)
            {
                value = "";

            }
            //else if(value is string)
            //{
            //    value = value.ToString().Trim();
            //    if (value.ToString().ToLower() == "true") { value = "1"; }
            //    if (value.ToString().ToLower() == "false") { value = "0"; }
            //}
            else if(value is bool)
            {
                value = ((bool)value)? "1" : "0";
            }
            

            return value;
        }

        #endregion

    }
}