﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAudit.aspx.cs" Inherits="cb3.Audit.ViewAudit" %>
<%@ Register TagPrefix="uc1" TagName="SecureUserAdmin" Src="~/UserAdmin/user_controls/SecureUserAdmin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="../new_user_controls/panelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="../new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../new_user_controls/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="../new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="../new_user_controls/head.ascx" %>
<%@ Register TagPrefix="ucl" TagName="UserInfo" Src="../new_user_controls/UserInfo.ascx" %>
<%@ Register TagPrefix="ucl" TagName="options" Src="../new_user_controls/options.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <title>View Audit </title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="pragma" content="no-cache">
    <link href="../css/CIGStyles.css" type="text/css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    
    <script language=JavaScript src="../DatePicker.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
    
    <style>
        tr.group.group-start {
            background-color: antiquewhite;
        }
    </style>
</head>
    
<body>
<form id="form1" runat="server">
    <table width="997" height="600" align="center" border="0">
        <tr>
            <td colspan="4">
                <uc1:head id="Head1" runat="server"></uc1:head>
            </td>
        </tr>
        <tr valign="top">
            <td width="1"></td>
            <td>
                <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <uc1:language id="Language1" runat="server"></uc1:language>
                        </td>
                        <td></td>
                        <td align="right">
                            <ucl:options id="Options1" runat="server"></ucl:options>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <uc1:sitepositionbar id="SitePositionBar1" runat="server"></uc1:sitepositionbar>
                        </td>
                        <td align="right">
                            <ucl:userinfo id="UserInfo1" runat="server"></ucl:userinfo>
                        </td>
                    </tr>
                    <tr>
                        <td height="10"></td>
                    </tr>
                    <tr valign="top">
                        <td width="150" valign="top" align="left">
                            <table width="98%">
                                <tr>
                                    <td>
                                        <uc1:panelbar id="PanelBar1" runat="server"></uc1:panelbar>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2">
                            <!-- Main Body Starts -->
                            <table width="100%">
                                
                                <tr>
                                    <td>
                                        <table class="grid_table" id="auditParametersTable" cellspacing="0" cellpadding="0" runat="server">
                                            <tr>
                                                <th>
                                                    <asp:label id="lblGridHeader" runat="server">Audit Parameters</asp:label></th></tr>
                                            <tr>
                                                <td>
                                                    <table class="fields" id="AuditParameters" cellspacing="0" cellpadding="0">
                                            
                                                        <tr>
                                                            <td style="WIDTH: 25%"><asp:label id="lblDateFrom" runat="server">Date From</asp:label><br>
                                                                <asp:textbox id="txtDateFrom" runat="server"></asp:textbox><input class=popup onclick="PopupPicker('txtDateFrom', 250, 250);" type=button value=...> </td>
                                                            
                                                            <td style="WIDTH: 25%"><asp:label id="lblDateTo" runat="server">Date To</asp:label><br>
                                                                <asp:textbox id="txtDateTo" runat="server"></asp:textbox><input class=popup onclick="PopupPicker('txtDateTo', 250, 250);" type=button value=...> </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="10"></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="bottom"><asp:button id="Print" runat="server" cssclass="search_button" text="Print"></asp:button></td>
                                                        </tr>
                                           
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                
                                <tr id="outerGridTableInfo" runat="server">
                                    <td align="right">
                                    <div style="width:833px; overflow-x: scroll">
                                        <table id="auditList" class="display" style="width:100%">
            
                                        </table>
                                    </div>
            
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                            </table>
                            <!-- Main Body Ends -->
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" height="1" bgcolor="#000000"></td>
                    </tr>
                </table>
            </td>
            <td width="2"></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <uc1:footer id="Footer1" runat="server"></uc1:footer>
                <uc1:secureuseradmin id="SecureUserAdmin1" runat="server"></uc1:secureuseradmin>
            </td>
        </tr>
    </table>
</form>
</body>
    
<script>

    $(document).ready(function () {

        $('#Print').click(function (event) {
            event.preventDefault();

            //Load  datatable
            var oTblReport = $("#auditList");
            var dateFrom = $("#txtDateFrom").val();
            var dateTo = $("#txtDateTo").val();

            if ($.fn.DataTable.isDataTable("#auditList")) {
                oTblReport.DataTable().clear().destroy();
            }
 
            oTblReport.DataTable ({
                "searching": false,
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "ajax": {
                    "url": '/Audit/AuditDataJSON.aspx?dateFrom='+dateFrom+'&dateTo='+dateTo,
                    "type": "POST"
                },
                "columns" : [

                
                    { "title": "ID", "data": "AuditActionID" },
                    { "title": "Audit Type", "data" : "AuditActionType" },
                    { "title": "Date Created", "data" : "DateTimeStamp" },
                    { "title": "Table Name", "data" : "AuditTableName" },
                    { "title": "Field Changed", "data" : "FieldName" },
                    { "title": "Value Before", "data" : "ValueBefore" },
                    { "title": "Value After", "data": "ValueAfter" },
                    { "title": "User", "data": "UserName" }
                ],
                "order": [[0, "desc"]],
                rowGroup: {
                    dataSrc: 'AuditActionID'
                }
            });



        });
     
        
    });

</script>

</html>
