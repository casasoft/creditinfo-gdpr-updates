﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using UserAdmin.BLL;

namespace cb3.Audit
{
    public partial class AuditDataJSON : System.Web.UI.Page
    {

        private string jsonResponse;
        private AuditParamModel _auditParam;

        protected void Page_Load(object sender, EventArgs e)
        {
            var start = (Request.Form["start"] == "NaN") ? "0" : Request.Form["start"];

            var dateFrom = new DateTime();
            DateTime.TryParse(Request.Params["dateFrom"], out dateFrom);

            var dateTo = new DateTime();
            DateTime.TryParse(Request.Params["dateTo"], out dateTo);

            this._auditParam = new AuditParamModel()
            {
                Length = Convert.ToInt64(Request.Form["length"]),
                Start = Convert.ToInt64(start),
                OrderColumn = Convert.ToInt32(Request.Form["order[0][column]"]),
                OrderDir = Request.Form["order[0][dir]"],
                DateFrom = dateFrom,
                DateTo = dateTo

            };


            jsonResponse = getAuditDataJson();

            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write(jsonResponse);
            Response.End();


            

        }



        private string getAuditDataJson()
        {
            var jsonResult = "{\"data\":[],\"recordsTotal\":0,\"recordsFiltered\":0}";


            var auditRepo = new AuditRepository();
            var userFactory = new uaFactory();

            var listOfAudit = auditRepo.Get(this._auditParam);


            if(listOfAudit != null && listOfAudit.Any())
            {
                var listAuditView = listOfAudit.ConvertAll(t => new AuditViewModel
                {
                    DateTimeStamp = t.DateTimeStamp.ToString(),
                    AuditActionType = Enum.GetName(typeof(AuditActionType), t.AuditActionType),
                    AuditTableName = t.AuditTableName.ToString(),
                    AuditActionID = t.AuditActionID.ToString(),
                    FieldName = t.FieldName.ToString(),
                    ValueBefore = t.ValueBefore.ToString(),
                    ValueAfter = t.ValueAfter.ToString(),
                    UserName = (t.UserId != 0) ? userFactory.GetUserName(t.UserId) : "",
                });

                // get record count
                var recordCount = auditRepo.GetTotalAuditCount(this._auditParam);



                jsonResult = JsonConvert.SerializeObject(
                    new
                    {
                        data = listAuditView,
                        recordsTotal = recordCount,
                        recordsFiltered = recordCount
                    },
                    Formatting.None);

            }

            return jsonResult;



        }
    }
}