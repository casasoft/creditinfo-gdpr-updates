﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cb3.Audit
{
    ///  this enum is also used as SQL command types
    /// </summary>
    public enum AuditActionType
    {
        Create = 1,
        Update = 2,
        Delete =3
    }
}