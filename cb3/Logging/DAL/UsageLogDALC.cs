#region

using System.Data;

#endregion

namespace CPI.DAL {
    /// <remarks>
    /// This class provides the database functions for UsageLog
    /// </remarks>
    public class UsageLogDALC {
        /// <summary>
        /// returns the logging types as DataSet
        /// </summary>
        /// <returns><Logging types as DataSet</returns>
        public DataSet GetLoggingTypeAsDataSet() {
            var dsLoggingType = new DataSet();
            return dsLoggingType;
        }

        /// <summary>
        /// Gets the  usage data as DataSet
        /// </summary>
        /// <returns>Usage data as DataSet</returns>
        public DataSet GetUsageData() {
            var dsUsageData = new DataSet();
            return dsUsageData;
        }

        /// <summary>
        /// Gets usage log summary as DataSet
        /// </summary>
        /// <returns>Usege log summary as DataSet</returns>
        public DataSet GetUsageLogSummaryForUserAsDataSet() {
            var dsUsageLogSummaryForUser = new DataSet();
            return dsUsageLogSummaryForUser;
        }
    } // END CLASS DEFINITION UsageLogDALC
}