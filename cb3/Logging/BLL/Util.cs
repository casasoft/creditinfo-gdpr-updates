#region

using System;
using Translitaration;

#endregion

namespace Logging.BLL {
    /// <summary>
    /// Summary description for Util.
    /// </summary>
    public class Util {
        public static string Transliterate(string text) {
            try {
                return GreekToLatin.Translate(text);
            } catch (Exception exc) {
                Logger.WriteToLog("Error translating text " + text, exc, true);
                return null;
            }
        }

        /// <summary>
        /// Creates a unique string. For example to crate unique filename.
        /// </summary>
        /// <returns>A unique string.</returns>
        public static string CreateUniqueString() { return Guid.NewGuid().ToString("N"); }
    }
}