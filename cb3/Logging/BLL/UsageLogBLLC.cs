#region

using System;

#endregion

namespace Logging.BLL {
    /// <remarks>
    /// This class provides all necessary info for UsageLog
    /// </remarks>
    public class UsageLogBLLC {
        /// <summary>
        /// Gets/sets the logID attribute
        /// </summary>
        public int LogID { get; set; }

        /// <summary>
        /// Gets/sets the creditInfoID attribute
        /// </summary>
        public int CreditInfoID { get; set; }

        /// <summary>
        /// Gets/sets the queryTypeID attribute
        /// </summary>
        public int QueryTypeID { get; set; }

        /// <summary>
        /// Gets/sets the query attribute
        /// </summary>
        public int Query { get; set; }

        /// <summary>
        /// Gets/sets the resultCount attribute
        /// </summary>
        public int ResultCount { get; set; }

        /// <summary>
        /// Gets/sets the ipAddresss attribute
        /// </summary>
        public string IpAddresss { get; set; }

        /// <summary>
        /// Gets/sets the createdDate attribute
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets/sets the subscriberID attribute
        /// </summary>
        public int SubscriberID { get; set; }
    } // END CLASS DEFINITION UsageLogBLLC
}