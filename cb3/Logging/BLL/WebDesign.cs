#region

using System.Globalization;
using System.Resources;
using System.Web.UI.WebControls;

#endregion

namespace Logging.BLL {
    /// <summary>
    /// Summary description for WebDesign.
    /// </summary>
    public class WebDesign {
        /// <summary>
        /// Adds a explanation icons for actions in a specific grid  to a label
        /// </summary>
        /// <param name="arrColumns">List of datagrid columns.</param>
        /// <param name="lbl">Lable to store icons in.</param>
        /// <param name="rm">Resource manager.</param>
        /// <param name="ci">Culture info.</param>
        public static void CreateExplanationIcons(
            DataGridColumnCollection arrColumns, Label lbl, ResourceManager rm, CultureInfo ci) { CreateExplanationIcons(arrColumns, lbl, rm, ci, 1); }

        /// <summary>
        /// Adds a explanation icons for actions in a specific grid  to a label
        /// </summary>
        /// <param name="arrColumns">List of datagrid columns.</param>
        /// <param name="lbl">Lable to store icons in.</param>
        /// <param name="rm">Resource manager.</param>
        /// <param name="ci">Culture info.</param>
        /// <param name="nLevelsDown">levels to img folder path</param>
        public static void CreateExplanationIcons(
            DataGridColumnCollection arrColumns, Label lbl, ResourceManager rm, CultureInfo ci, int nLevelsDown) {
            lbl.Text = "";
            string strDownLevels = "";
            for (int i = 0; i < nLevelsDown; i++) {
                strDownLevels += "../";
            }

            foreach (DataGridColumn column in arrColumns) {
                if (column is ButtonColumn && column.Visible) {
                    var button = (ButtonColumn) column;

                    if (button.CommandName.ToLower().Equals(DataGrid.SelectCommandName.ToLower())) {
                        lbl.Text += "<img alt=\"Select\" src=\"" + strDownLevels +
                                    "img/select.gif\" border=\"0\" align=\"middle\"> = " + rm.GetString("txtSelect", ci) +
                                    "&nbsp;&nbsp;";
                    } else if (button.CommandName.ToLower().Equals("insertafter") ||
                               button.CommandName.ToLower().Equals("add") || button.CommandName.ToLower().Equals("insert") ||
                               button.CommandName.ToLower().Equals("new") || button.CommandName.ToLower().Equals("newuser")) {
                        lbl.Text += "<img alt=\"New\" src=\"" + strDownLevels +
                                    "img/new.gif\" border=\"0\" align=\"middle\"> = " + rm.GetString("txtNew", ci) +
                                    "&nbsp;&nbsp;";
                    } else if (button.CommandName.ToLower().Equals(DataGrid.EditCommandName.ToLower()) ||
                               button.CommandName.ToLower().Equals("update")) {
                        lbl.Text += "<img alt=\"Edit\" src=\"" + strDownLevels +
                                    "img/edit.gif\" border=\"0\" align=\"middle\"> = " +
                                    rm.GetString("txtUpdate", ci) + "&nbsp;&nbsp;";
                    } else if (button.CommandName.ToLower().Equals("view")) {
                        lbl.Text += "<img alt=\"View\" src=\"" + strDownLevels +
                                    "img/view.gif\" border=\"0\" align=\"middle\"> = " +
                                    rm.GetString("txtView", ci) + "&nbsp;&nbsp;";
                    } else if (button.CommandName.ToLower().Equals(DataGrid.DeleteCommandName.ToLower())) {
                        lbl.Text += "<img alt=\"Delete\" src=\"" + strDownLevels +
                                    "img/delete.gif\" border=\"0\" align=\"middle\"> = " +
                                    rm.GetString("txtDelete", ci) + "&nbsp;&nbsp;";
                    }
                } else if (column is EditCommandColumn && column.Visible) {
                    lbl.Text += "<img alt=\"Edit\" src=\"" + strDownLevels +
                                "img/edit.gif\" border=\"0\" align=\"middle\"> = " + rm.GetString("txtUpdate", ci) +
                                "&nbsp;&nbsp;";
                    lbl.Text += "<img alt=\"Cancel\" src=\"" + strDownLevels +
                                "img/cancel.gif\" border=\"0\" align=\"middle\"> = " + rm.GetString("txtCancel", ci) +
                                "&nbsp;&nbsp;";
                }
            }
        }
    }
}