using System;
using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CreditInfoGroup.Localization;
using UserAdmin.BLL;
using System.Web.Security;

using Cig.Framework.Base.Configuration;

namespace CreditInfoGroup
{
    /// <summary>
    /// Summary description for FoDefault.
    /// </summary>
    public class FoDefault : CigPage
    {
        // Fyrir multilanguage d�mi. 
        public static CultureInfo ci;
        public static ResourceManager rm;
        private readonly uaFactory myFactory = new uaFactory();
        protected HyperLink hlMail;
        protected Label lblFoDefault;
        protected Label lblProducts;

        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here

            if (Context.User.Identity.Name == "")
            {
                return;
            }

            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            LocalizeText();
            if (CigConfig.Configure("lookupsettings.supportEmail") != null)
            {
                hlMail.Text = CigConfig.Configure("lookupsettings.supportEmail");
                hlMail.NavigateUrl = "mailto:" + CigConfig.Configure("lookupsettings.supportEmail");
            }

            if (IsPostBack)
            {
                return;
            }

            if (Session["UserLoginID"] == null || Session["UserLoginID"].ToString() == string.Empty)
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session.Abandon();
                Response.Redirect("~/fodefault.aspx");
                Response.End();
            }

            var builder = new StringBuilder();
            var myProductSet = myFactory.GetAllTheOpenProducts();

            builder.Append(@"<table width='70%' align='center' cellSpacing='5' cellPadding='5' border='0'><tr>");

            // Athuga hvort notandi hafi a�gang a� v�rum. Birta menu samkv�mt a�gangi notenda...
            for (int i = 0; i < myProductSet.Tables[0].Rows.Count; i++)
            {
                // Fyrir Parent gr�ppu - n.b. Datasetti� kemur ra�a� m.v. parentID og svo ProductID. Parents eru me�
                // "0" � ParentID og koma �v� fremst...
                if (myProductSet.Tables[0].Rows[i]["ProductID"].ToString().Equals("1")) // not display home link at the default page
                {
                    continue;
                }
                if (myProductSet.Tables[0].Rows[i]["ParentID"] == DBNull.Value ||
                    (int)myProductSet.Tables[0].Rows[i]["ParentID"] != 0)
                {
                    continue;
                }
                // Ef notandinn hefur a�gang a� �essari v�ru...
                if (!Context.User.IsInRole(myProductSet.Tables[0].Rows[i]["ProductID"].ToString()) ||
                    Convert.ToBoolean(myProductSet.Tables[0].Rows[i]["IsInner"]))
                {
                    continue;
                }
                // Fyrir native e�a ensku...
                if (ci.Name == Session["Defculture"].ToString())
                {
                    builder.AppendFormat(
                        @"<td align='right' width='35%' valign='top'><a id='{0}' class='MainMenu' href='{1}'>{2}</a></td>",
                        myProductSet.Tables[0].Rows[i]["ProductID"],
                        //	AppPath + myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString(),
                        Application.Get("AppPath") +
                        myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString(),
                        myProductSet.Tables[0].Rows[i]["ProductNameNative"]);

                    builder.AppendFormat(
                        @"<td align='left'>{0}</td></tr><tr>",
                        myProductSet.Tables[0].Rows[i]["ProductDescriptionNative"]);
                }
                else
                {
                    builder.AppendFormat(
                        @"<td align='right' width='35%' valign='top'><a id='{0}' class='MainMenu' href='{1}'>{2}</a></td>",
                        myProductSet.Tables[0].Rows[i]["ProductID"],
                        //	AppPath + myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString(),
                        Application.Get("AppPath") +
                        myProductSet.Tables[0].Rows[i]["NavigateUrl"].ToString(),
                        myProductSet.Tables[0].Rows[i]["ProductNameEN"]);

                    builder.AppendFormat(
                        @"<td align='left'>{0}</td></tr><tr>",
                        myProductSet.Tables[0].Rows[i]["ProductDescriptionEN"]);
                }
            }

            // if Cyprus version then add the default disclaimer
            if (CigConfig.Configure("lookupsettings.currentVersion") != null)
            {
                if (CigConfig.Configure("lookupsettings.currentVersion").Equals("cyprus"))
                {
                    builder.AppendFormat(@"<td align='left' colspan=2></td></tr><tr>");
                    builder.AppendFormat(
                        @"<td align='left' colspan=2>{0}</td></tr><tr>",
                        rm.GetString("txtDefaultPageDisclaimer", ci));
                }
            }
            builder.Append(@"</tr></table>");
            lblProducts.Text = builder.ToString();
        }

        private void LocalizeText() { lblFoDefault.Text = rm.GetString("FoDefault.lblFoDefault", ci); }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { this.Load += new System.EventHandler(this.Page_Load); }

        #endregion
    }
}