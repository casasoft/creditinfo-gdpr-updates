using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CPI.BLL;
using CR.BLL;
using CR.Localization;
using ROS.BLL;
using UserAdmin.BLL;
using UserAdmin.BLL.auUsers;
using UserAdmin.BLL.CIUsers;

using Cig.Framework.Base.Configuration;

namespace CreditInfoGroup {
    /// <summary>
    /// Summary description for FoCompanyReport.
    /// </summary>
    public class FoCompanyReport : Page {
        private readonly CPIFactory cpiFactory = new CPIFactory();
        private readonly CRFactory crFactory = new CRFactory();
        private readonly uaFactory userAdminFactory = new uaFactory();
        protected Button btnClear;
        protected Button btnFind;
        protected Button btnOrderReport;
        private CultureInfo ci;
        protected DataGrid dtgrCompanys;
        protected Label lblAdditionalInformations;
        protected Label lblAddress;
        protected Label lblDispAddress;
        protected Label lblDispCity;
        protected Label lblDispName;
        protected Label lblDispNationalID;
        protected Label lblFindReport;
        protected Label lblNACE;
        protected Label lblName;
        protected Label lblNationalID;
        protected Label lblNoCompanyFound;
        protected Label lblOperation;
        protected Label lblOrderCIID;
        protected Label lblOrderDeliveryType;
        protected Label lblOrderDispCompanyCIID;
        protected Label lblOrderDispCompanyName;
        protected Label lblOrderDispSubscriberCIID;
        protected Label lblOrderDispUserName;
        protected Label lblOrderName;
        protected Label lblOrderReceived;
        protected Label lblOrderSubscriberCIID;
        protected Label lblOrderUserName;
        protected Label lblRecCreditInfoID;
        protected Label lblRecCreditInfoIDLabel;
        protected Label lblRecName;
        protected Label lblRecNameLabel;
        protected Label lblRecNationalID;
        protected Label lblRecNationalIDLabel;
        protected Label lblReport;
        protected Label lblReportOrder;
        protected Label lblResultAddress;
        protected Label lblResultCity;
        protected Label lblResultNACE;
        protected Label lblResultName;
        protected Label lblResultNationalID;
        protected Label lblResultOperation;
        protected LinkButton lbtnBasicReport;
        protected LinkButton lbtnCompanyReport;
        protected LinkButton lbtnCreditinfoReport;
        protected LinkButton lbtnOrderReport;
        private bool nativeCult;
        protected RadioButtonList rbtnlDeliveryType;
        private ResourceManager rm;
        protected HtmlTableRow trAdditionalInformation;
        protected HtmlTableCell trBasicRow;
        protected HtmlTableRow trCompanyInfoRow;
        protected HtmlTableCell trCompanyRow;
        protected HtmlTableCell trCreditInfoRow;
        protected HtmlTableRow trOrderReceipe;
        protected HtmlTableRow trOrderReport;
        protected HtmlTableCell trOrderRow;
        protected HtmlTableRow trSearchGridRow;
        protected TextBox txtAddress;
        protected TextBox txtName;
        protected TextBox txtNationalID;

        private void Page_Load(object sender, EventArgs e) {
            AddEnterEvent();
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            var nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            if (culture.Equals(nativeCulture)) {
                nativeCult = true;
            }

            LocalizeText();
            LocalizeGridHeader();
            InitializeGrid();
            if (Page.IsPostBack) {
                return;
            }
            lblNoCompanyFound.Visible = false;
            trCompanyInfoRow.Visible = false;
            trSearchGridRow.Visible = false;
            trOrderReceipe.Visible = false;
            trOrderReport.Visible = false;
            trAdditionalInformation.Visible = false;
        }

        private void FillDeliveryTypeDropDownBox() {
            var rosFactory = new FactoryBLLC();

            //Get the delivery speed from the database
            var theSpeeds = rosFactory.GetDeliverySpeeds();

            foreach (DataRow row in theSpeeds.Tables[0].Rows) {
                var theItem = new ListItem
                              {
                                  Value = ("ID=" + row["ID"] + "TIME=" + row["DaysCount"]),
                                  Text =
                                      (nativeCult
                                           ? row["DeliverySpeedNative"].ToString()
                                           : row["DeliverySpeedEN"].ToString())
                              };

                //Setja � ddlDeliverySpeed
                rbtnlDeliveryType.Items.Add(theItem);
            }
        }

        private void AddEnterEvent() {
            var frm = FindControl("FoCompanyReportForm");
            foreach (Control ctrl in frm.Controls) {
                if (ctrl is TextBox) {
                    ((TextBox) ctrl).Attributes.Add("onkeypress", "checkEnterKey();");
                }
            }
        }

        private void LocalizeText() {
            lblNACE.Text = rm.GetString("txtNACECode", ci);
            lblName.Text = rm.GetString("txtName", ci);
            lblNationalID.Text = rm.GetString("txtNationalID", ci);
            lblNoCompanyFound.Text = rm.GetString("txtNoCompanyFound", ci);
            lblOperation.Text = rm.GetString("txtRegistrationForm", ci);
            lblReport.Text = rm.GetString("txtReport", ci);
            lblFindReport.Text = rm.GetString("txtFindReport", ci);
            lblAddress.Text = rm.GetString("txtAddress", ci);
            btnClear.Text = rm.GetString("txtClear", ci);
            btnFind.Text = rm.GetString("txtFind", ci);
            lbtnBasicReport.Text = rm.GetString("txtBasicReport", ci);
            lbtnCompanyReport.Text = rm.GetString("txtCompanyReport", ci);
            lbtnCreditinfoReport.Text = rm.GetString("txtCreditInfoReport", ci);
            btnOrderReport.Text = rm.GetString("txtOrderReport", ci);

            lblDispAddress.Text = rm.GetString("txtAddress", ci);
            lblDispCity.Text = rm.GetString("txtCity", ci);
            lblDispName.Text = rm.GetString("txtName", ci);
            lblDispNationalID.Text = rm.GetString("txtNationalID", ci);

            lblRecNameLabel.Text = rm.GetString("txtName", ci);
            lblRecNationalIDLabel.Text = rm.GetString("txtNationalID", ci);
            lblRecCreditInfoIDLabel.Text = rm.GetString("txtCreditInfoID", ci);
            lblOrderReceived.Text = rm.GetString("txtOrderReceived", ci);
        }

        private void LocalizeGridHeader() {
            dtgrCompanys.Columns[1].HeaderText = rm.GetString("txtNationalID", ci);
            dtgrCompanys.Columns[2].HeaderText = rm.GetString("txtName", ci);
            dtgrCompanys.Columns[3].HeaderText = rm.GetString("txtName", ci);
            dtgrCompanys.Columns[4].HeaderText = rm.GetString("txtAddress", ci);
            dtgrCompanys.Columns[5].HeaderText = rm.GetString("txtAddress", ci);
            dtgrCompanys.Columns[6].HeaderText = rm.GetString("txtPostalCode", ci);
            dtgrCompanys.Columns[7].HeaderText = rm.GetString("txtCity", ci);
            dtgrCompanys.Columns[8].HeaderText = rm.GetString("txtCity", ci);
        }

        private void InitializeGrid() {
            dtgrCompanys.Columns[0].Visible = true; //select
            dtgrCompanys.Columns[1].Visible = true; //National ID
            dtgrCompanys.Columns[6].Visible = true; //PostalCode
            if (nativeCult) {
                dtgrCompanys.Columns[2].Visible = true;
                dtgrCompanys.Columns[3].Visible = false;
                dtgrCompanys.Columns[4].Visible = true;
                dtgrCompanys.Columns[5].Visible = false;
                dtgrCompanys.Columns[7].Visible = true;
                dtgrCompanys.Columns[8].Visible = false;
            } else {
                dtgrCompanys.Columns[2].Visible = false;
                dtgrCompanys.Columns[3].Visible = true;
                dtgrCompanys.Columns[4].Visible = false;
                dtgrCompanys.Columns[5].Visible = true;
                dtgrCompanys.Columns[7].Visible = false;
                dtgrCompanys.Columns[8].Visible = true;
            }
        }

        private void getCompanyInfoFromNameOrAddress(string name, string address) {
            var company = new Company {NameNative = name};
            var addr = new Address {StreetNative = address};
            company.Address = new ArrayList {addr};
            DataSet ds = CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True" ? userAdminFactory.FindCompanyInNationalAndCreditInfo(company) : userAdminFactory.FindCompany(company);
            //now we have the company 
            if (ds == null || ds.Tables.Count < 1 || ds.Tables[0].Rows.Count < 1) {
                //if null or no row display error
                lblNoCompanyFound.Visible = true;
                lblNoCompanyFound.Text = rm.GetString("txtNoCompanyFound", ci);
                trCompanyInfoRow.Visible = false;
                trAdditionalInformation.Visible = false;
                trSearchGridRow.Visible = false;
                trOrderReceipe.Visible = false;
            } else if (ds.Tables[0].Rows.Count == 1) {
                //If only one company - display details about that company
                trCompanyInfoRow.Visible = true;
                trAdditionalInformation.Visible = true;
                trSearchGridRow.Visible = false;
                var report = new ReportCompanyBLLC
                             {
                                 NameNative = ds.Tables[0].Rows[0]["NameNative"].ToString().Trim(),
                                 NameEN = ds.Tables[0].Rows[0]["NameEN"].ToString().Trim()
                             };
                /*Svenni t�k �t
				 * 
				 * 				report.AddressNative = ds.Tables[0].Rows[0]["StreetNative"].ToString().Trim();
								report.AddressEN = ds.Tables[0].Rows[0]["StreetEN"].ToString().Trim();
								report.AreaCode = ds.Tables[0].Rows[0]["PostalCode"].ToString();
				*/
                var myAddress = new Address
                                {
                                    StreetNative = ds.Tables[0].Rows[0]["StreetNative"].ToString().Trim(),
                                    StreetEN = ds.Tables[0].Rows[0]["StreetEN"].ToString().Trim(),
                                    PostalCode = ds.Tables[0].Rows[0]["PostalCode"].ToString()
                                };

                report.Address = new ArrayList {myAddress};

                report.UniqueID = ds.Tables[0].Rows[0]["Number"].ToString();
                report.CompanyCIID = cpiFactory.GetCIIDByNationalID(report.UniqueID);
                displayCompanyInfo(report);
            } else {
                //Display list in grid
                dtgrCompanys.DataSource = ds;
                dtgrCompanys.DataBind();
                trCompanyInfoRow.Visible = false;
                trAdditionalInformation.Visible = false;
                trSearchGridRow.Visible = true;
                trOrderReceipe.Visible = false;
            }
        }

        /// <summary>
        /// Finds company with given national id and displays 
        /// informations about that company
        /// </summary>
        /// <param name="nationalID">The national id for the company</param>
        private void getCompanyInfoFromNationalID(string nationalID) {
            ReportCompanyBLLC report = cpiFactory.GetCompanyReport(nationalID, -1, true);
            if (report == null || report.UniqueID == null) {
                //Report not found - try to find CIID for the national ID
                int ciid = cpiFactory.GetCIIDByNationalID(nationalID);
                if (ciid != -1) {
                    //OK - CIID found - try to find the company in user admin
                    Company company = userAdminFactory.GetCompany(ciid);
                    if (company != null && company.CreditInfoID > -1) {
                        //If company found in user admin - display that
                        company.NationalID = nationalID;
                        displayCompanyInfo(company);
                    } else {
                        //Strange not to find the company but only the CIID
                        lblNoCompanyFound.Visible = true;
                        lblNoCompanyFound.Text = rm.GetString("txtNoCompanyFound", ci);
                        trCompanyInfoRow.Visible = false;
                        trAdditionalInformation.Visible = false;
                        trSearchGridRow.Visible = false;
                        trOrderReceipe.Visible = false;
                    }
                } else {
                    if (CigConfig.Configure("lookupsettings.UserAdminNationalRegistry") == "True") {
                        //Search in national registry
                        Company company = userAdminFactory.GetCompanyFromNationalRegistry(nationalID);
                        if (company != null && company.NationalID != null) {
                            //If company found in user admin - display that
                            displayCompanyInfo(company);
                        } else {
                            //Company not found
                            lblNoCompanyFound.Visible = true;
                            lblNoCompanyFound.Text = rm.GetString("txtNoCompanyFound", ci);
                            trCompanyInfoRow.Visible = false;
                            trAdditionalInformation.Visible = false;
                            trSearchGridRow.Visible = false;
                            trOrderReceipe.Visible = false;
                        }
                    } else {
                        //Company not found
                        lblNoCompanyFound.Visible = true;
                        lblNoCompanyFound.Text = rm.GetString("txtNoCompanyFound", ci);
                        trCompanyInfoRow.Visible = false;
                        trAdditionalInformation.Visible = false;
                        trSearchGridRow.Visible = false;
                        trOrderReceipe.Visible = false;
                    }
                }
            } else {
                displayCompanyInfo(report);
            }
        }

        private void displayCompanyInfo(Company company) {
            trSearchGridRow.Visible = false;
            trCompanyInfoRow.Visible = true;
            trAdditionalInformation.Visible = true;
            trOrderReceipe.Visible = false;
            lblResultNationalID.Text = company.NationalID;
            if (nativeCult) {
                lblResultName.Text = company.NameNative;
                if (company.Address != null && company.Address.Count > 0) {
                    var address = (Address) company.Address[0];
                    if (address != null) {
                        if (address.StreetNumber > 0) {
                            lblResultAddress.Text = address.StreetNative + " " + address.StreetNumber;
                        } else {
                            lblResultAddress.Text = address.StreetNative;
                        }
                        lblResultCity.Text = address.PostalCode + " " + address.CityNameNative;
                    }
                }
            } else {
                lblResultName.Text = company.NameEN;
                if (company.Address != null && company.Address.Count > 0) {
                    var address = (Address) company.Address[0];
                    if (address != null) {
                        if (address.StreetNumber > 0) {
                            lblResultAddress.Text = address.StreetEN + " " + address.StreetNumber;
                        } else {
                            lblResultAddress.Text = address.StreetEN;
                        }
                        lblResultCity.Text = address.PostalCode + " " + address.CityNameEN;
                    }
                }
            }
            lblResultNACE.Text = "";
            lblResultOperation.Text = "";

            SetReportButtonVisability(false);
        }

        private void displayCompanyInfo(ReportCompanyBLLC report) {
            trSearchGridRow.Visible = false;
            trAdditionalInformation.Visible = true;
            trCompanyInfoRow.Visible = true;
            trOrderReceipe.Visible = false;
            lblResultNationalID.Text = report.UniqueID;
            if (nativeCult) {
                lblResultName.Text = report.NameNative;

                if (report.Address.Count > 0) {
                    lblResultAddress.Text = ((Address) report.Address[0]).StreetNative;
                    lblResultCity.Text = ((Address) report.Address[0]).PostalCode + " " +
                                         ((Address) report.Address[0]).CityNameNative;
                }
            } else {
                lblResultName.Text = report.NameEN;

                if (report.Address.Count > 0) {
                    lblResultAddress.Text = ((Address) report.Address[0]).StreetEN;
                    lblResultCity.Text = ((Address) report.Address[0]).PostalCode + " " +
                                         ((Address) report.Address[0]).CityNameEN;
                }
            }
            lblResultOperation.Text = crFactory.GetRegistrationFormAsString(report.RegistrationFormID, nativeCult);
            lblResultNACE.Text = report.NaceCodeName;

            if (report.CompanyCIID < 0) {
                SetReportButtonVisability(false);
            } else {
                if (CheckIfReportIsExpired(report.CompanyCIID)) {
                    lblNoCompanyFound.Text = rm.GetString("txtReportHasExpired", ci);
                    lblNoCompanyFound.Visible = true;
                } else {
                    SetReportButtonVisability(true);
                }
            }
        }

        private void dtgrCompanys_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) {
                ((LinkButton) e.Item.Cells[0].Controls[0]).Text = rm.GetString("txtSelect", ci);
            }
        }

        protected void displayReport(int reportType) {
            string nationalID = lblResultNationalID.Text;
            int ciid = -1;
            if (nationalID != "") {
                ciid = cpiFactory.GetCIIDByNationalID(nationalID);
            }

            if (ciid > -1) {
                DataSet reportYears = cpiFactory.GetAvailableReportYearsAsDataSet(ciid);

                string afsIds = GetAFSIDs(reportYears);

                if (!string.IsNullOrEmpty(afsIds)) {
                    Session["CompanyCIID"] = ciid;
                    Session["ReportType"] = reportType;
                    Session["ReportCulture"] = nativeCult ? "Native" : "en-US";
                    Session["AFSID"] = afsIds;

                    Server.Transfer("CR/FoReport.aspx");
                } else {
                    lblNoCompanyFound.Text = rm.GetString("txtNoFinancialInfoFound", ci);
                    lblNoCompanyFound.Visible = true;
                }
            } else {
                lblNoCompanyFound.Text = rm.GetString("txtNoReportFound", ci);
                lblNoCompanyFound.Visible = true;
            }
        }

        private static string GetAFSIDs(DataSet ds) {
            var afs_id = new StringBuilder();
            bool first = true;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    if (!first) {
                        afs_id.Append(","); // Add the comma
                    }
                    afs_id.Append(ds.Tables[0].Rows[i]["AFS_ID"].ToString());
                    first = false;
                }
                return afs_id.ToString();
            }
            return null;
        }

        /// <summary>
        /// Checks if report for given company has expired and returns true if expired
        /// </summary>
        /// <param name="ciid">CreditInfo id for the company to check</param>
        /// <returns>True if the report has expired, false if not</returns>
        private bool CheckIfReportIsExpired(int ciid) {
            var report = cpiFactory.GetCompanyReport("", ciid, false);
            if (report.ExpireDate < DateTime.Now) {return true;}
            return report.LastContacted < DateTime.Now.AddYears(-1);
        }

        private void dtgrCompanys_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (!e.CommandName.Equals("Select")) {
                return;
            }
            string nationalID = e.Item.Cells[1].Text;
            if (!string.IsNullOrEmpty(nationalID)) {
                //Clear old values
                lblResultName.Text = "";
                lblResultAddress.Text = "";
                lblResultCity.Text = "";
                lblResultOperation.Text = "";
                lblResultNACE.Text = "";
                trOrderReceipe.Visible = false;
                trOrderReport.Visible = false;
                lblNoCompanyFound.Visible = false;
                getCompanyInfoFromNationalID(nationalID);
            }
        }

        private void SetReportButtonVisability(bool reportAvailable) {
            trBasicRow.Visible = reportAvailable;
            trCompanyRow.Visible = reportAvailable;
            trCreditInfoRow.Visible = reportAvailable;
            trOrderRow.Visible = !reportAvailable;
        }

        private void btnClear_Click(object sender, EventArgs e) {
            txtName.Text = "";
            txtNationalID.Text = "";
            lblNoCompanyFound.Visible = false;
            trCompanyInfoRow.Visible = false;
            trAdditionalInformation.Visible = false;
            trSearchGridRow.Visible = false;
            trOrderReceipe.Visible = false;
            trOrderReport.Visible = false;
        }

        private void btnOrderReport_Click(object sender, EventArgs e) {
            string nationalID = lblResultNationalID.Text;
            if (nationalID == "") {
                return;
            }
            var ciid = cpiFactory.GetCIIDByNationalID(nationalID);

            if (ciid == -1) {
                var company = new Company
                              {
                                  NationalID = nationalID,
                                  NameNative = lblResultName.Text,
                                  NameEN = lblResultName.Text,
                                  Type = CigConfig.Configure("lookupsettings.companyID")
                              };
                ciid = userAdminFactory.AddCompany(company);
            }
            if (ciid != -1) {
                var myOrder = new OrderBLLC();
                var subject = new CustomerBLLC {CreditInfoID = ciid};
                myOrder.Subject = subject;
                var customer = new CustomerBLLC
                               {
                                   CreditInfoID = int.Parse(Session["UserCreditInfoID"].ToString())
                               };
                myOrder.Customer = customer;

                myOrder.OrderDate = DateTime.Now;

                myOrder.OrderTypeID = 3;

                //String deliveryID = this.ddlDeliverySpeed.SelectedValue.Substring(0, this.ddlDeliverySpeed.SelectedValue.IndexOf("T"));
                //myOrder.DeliverySpeedID = int.Parse(deliveryID.Substring(deliveryID.IndexOf("=" , 0) + 1));
                //ID=1TIME=7

                var rosFactory = new FactoryBLLC();
                rosFactory.CreateReportOrder(myOrder);
                lblRecNationalID.Text = nationalID;
                lblRecCreditInfoID.Text = ciid.ToString();
                lblRecName.Text = lblResultName.Text;

                btnClear_Click(this, null);
                trOrderReceipe.Visible = true;
            } else {
                lblNoCompanyFound.Text = rm.GetString("txtCouldNotOrderReport", ci);
                lblNoCompanyFound.Visible = true;
            }
        }

        private void btnFind_Click(object sender, EventArgs e) {
            //Clear old values
            lblResultName.Text = "";
            lblResultAddress.Text = "";
            lblResultCity.Text = "";
            lblResultOperation.Text = "";
            lblResultNACE.Text = "";
            trOrderReceipe.Visible = false;
            trOrderReport.Visible = false;

            lblNoCompanyFound.Visible = false;
            if (txtNationalID.Text.Trim() != "") {
                getCompanyInfoFromNationalID(txtNationalID.Text);
            } else if (txtName.Text.Trim() != "" || txtAddress.Text.Trim() != "") {
                getCompanyInfoFromNameOrAddress(txtName.Text, txtAddress.Text);
            }
        }

        private void lbtnBasicReport_Click(object sender, EventArgs e) { displayReport(1); }
        private void lbtnCompanyReport_Click(object sender, EventArgs e) { displayReport(2); }
        private void lbtnCreditinfoReport_Click(object sender, EventArgs e) { displayReport(3); }

        private void lbtnOrderReport_Click(object sender, EventArgs e) {
            FillDeliveryTypeDropDownBox();
            lblOrderDispCompanyName.Text = lblResultName.Text;
            string nationalID = lblResultNationalID.Text;
            if (nationalID != "") {
                int ciid = cpiFactory.GetCIIDByNationalID(nationalID);
                if (ciid != -1) {
                    lblOrderDispCompanyCIID.Text = ciid.ToString();
                }
            }
            string userName = Session["UserLoginName"].ToString();
            lblOrderDispUserName.Text = userName;

            auUsersBLLC auUser = userAdminFactory.GetSpecificUser(userName);
            if (auUser != null) {
                lblOrderDispSubscriberCIID.Text = auUser.SubscriberID.ToString();
            }

            trOrderReport.Visible = true;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.lbtnBasicReport.Click += new System.EventHandler(this.lbtnBasicReport_Click);
            this.lbtnCompanyReport.Click += new System.EventHandler(this.lbtnCompanyReport_Click);
            this.lbtnCreditinfoReport.Click += new System.EventHandler(this.lbtnCreditinfoReport_Click);
            this.lbtnOrderReport.Click += new System.EventHandler(this.lbtnOrderReport_Click);
            this.btnOrderReport.Click += new System.EventHandler(this.btnOrderReport_Click);
            this.dtgrCompanys.ItemCommand +=
                new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dtgrCompanys_ItemCommand);
            this.dtgrCompanys.ItemDataBound +=
                new System.Web.UI.WebControls.DataGridItemEventHandler(this.dtgrCompanys_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}