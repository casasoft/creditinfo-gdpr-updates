﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServiceWarning.aspx.cs" Inherits="cb3.ServiceWarning" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    Due to Upgrades, the Creditinfo Service is Currently Unavailable. This should only take a few hours and may resume after 18:00hrs. We apologize for any inconvenience caused. Thank you. Creditinfo Malta
    </div>
    </form>
</body>
</html>
