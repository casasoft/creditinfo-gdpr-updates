<%@ Page language="c#" Codebehind="FoDebtSearch.aspx.cs" AutoEventWireup="false" Inherits="CreditInfoGroup.FoDebtSearch" %>
<%@ Register TagPrefix="uc1" TagName="head" Src="new_user_controls/FoHead.ascx" %>
<%@ Register TagPrefix="uc1" TagName="language" Src="new_user_controls/language.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="new_user_controls/FoFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="sitePositionBar" Src="new_user_controls/sitePositionBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="panelBar" Src="new_user_controls/FoPanelBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UserInfo" Src="new_user_controls/UserInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>FoDebtSearch</title>
		<LINK href="css/FoCIGStyles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="popup.js"></script>
		<style type="text/css">BR.pageEnd { PAGE-BREAK-AFTER: always }
		</style>
		<SCRIPT language="javascript"> 
				function checkEnterKey() 
				{    
					if (event.keyCode == 13) 
					{        
						event.cancelBubble = true;
						event.returnValue = false;
						FoDebtSearchForm.btnFind.click(); 
					}
				} 
		
				function SetFormFocus()
				{
					document.FoSearchForm.txtNationalID.focus();
				}
				
		</SCRIPT>
	</HEAD>
	<body style="BACKGROUND-IMAGE: url(img/mainback.gif)" leftMargin="0" onload="SetFormFocus()"
		rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="FoSearchForm" title="Company report" name="Company report" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><uc1:head id="Head2" runat="server"></uc1:head></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<tr>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
								<td>
									<table cellSpacing="0" cellPadding="0" width="760" align="center" bgColor="#951e16" border="0">
										<tr>
											<td bgColor="#951e16"><span style="WIDTH: 3px"></span></td>
											<td bgColor="#951e16"><uc1:panelbar id="Panelbar2" runat="server"></uc1:panelbar></td>
											<td align="right" bgColor="#951e16"><uc1:language id="Language2" runat="server"></uc1:language></td>
										</tr>
									</table>
								</td>
								<td style="BACKGROUND-IMAGE: url(img/pagename_large_back.gif)" width="50%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="760" align="center" bgColor="white" border="0">
							<tr>
								<td>
									<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
										<tr>
											<TD><uc1:userinfo id="UserInfo1" runat="server"></uc1:userinfo></TD>
										</tr>
									</table>
								</td>
							</tr>
							<!-- Content begins -->
							<tr>
								<td>
									<TABLE id="tbDefault1" borderColor="#0000ff" cellSpacing="0" cellPadding="0" width="97%"
										align="center">
										<TBODY>
											<TR>
												<TD class="pageheader" align="left"><asp:label id="lblSearch" runat="server" CssClass="HeadMain">Search
													</asp:label></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 491px; HEIGHT: 15px" align="center"></TD>
											</TR>
											<TR class="dark-row">
												<TD style="WIDTH: 491px; HEIGHT: 15px" align="center"></TD>
											</TR>
											<TR>
												<TD align="left"><asp:radiobuttonlist id="rbtnlUserType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
														<asp:ListItem Value="Individual">Individual</asp:ListItem>
														<asp:ListItem Value="Company">Company</asp:ListItem>
													</asp:radiobuttonlist></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 239px; HEIGHT: 15px" align="left"><asp:label id="lblNationalID" runat="server">ID:</asp:label></TD>
											</TR>
											<tr>
												<TD style="WIDTH: 239px; HEIGHT: 15px" align="left"><asp:textbox id="txtNationalID" runat="server" Width="96px"></asp:textbox></TD>
											</tr>
											<TR>
												<TD style="WIDTH: 491px; HEIGHT: 15px" align="center"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 491px; HEIGHT: 25px" align="left">
													<table>
														<tr>
															<td>
																<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnFind" runat="server" CssClass="RegisterButton" Text="Find"></asp:button></DIV>
															</td>
															<td>
																<DIV class="AroundButtonSmallMargin" style="WIDTH: 150px; HEIGHT: 10px"><asp:button id="btnClear" runat="server" CssClass="RegisterButton" Text="Clear"></asp:button></DIV>
															</td>
														</tr>
													</table>
												</TD>
											</TR>
											<TR id="Tr2" runat="server">
												<TD style="HEIGHT: 15px" align="left"></TD>
											</TR>
											<TR id="trCompanyInfoRow" runat="server">
												<TD vAlign="top" align="center">
													<TABLE class="list" id="Table1" cellSpacing="0" cellPadding="0" width="600" border="0">
														<TR class="dark-row">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><asp:label id="lblNationalIDLabel" runat="server" Font-Bold="True"></asp:label></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblNationalID1" runat="server"></asp:label></TD>
															<TD style="HEIGHT: 15px" align="right"><asp:label id="ldlToday" runat="server" Font-Bold="True">Date of report:</asp:label></TD>
														</TR>
														<TR class="dark-row">
															<TD class="dark-row" style="WIDTH: 123px; HEIGHT: 13px" vAlign="top"><STRONG><asp:label id="lblNameLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px; HEIGHT: 13px" vAlign="top"><asp:label id="lblName" runat="server"></asp:label></TD>
															<TD style="HEIGHT: 15px" align="right"><asp:label id="lblTimeStamp" runat="server">2005.01.01 11:50</asp:label></TD>
														</TR>
														<TR class="dark-row" id="lblTypeRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px; HEIGHT: 13px" vAlign="top"><STRONG><asp:label id="lblTypeLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px; HEIGHT: 13px" vAlign="top"><asp:label id="lblType" runat="server"></asp:label></TD>
															<TD class="dark-row" vAlign="top"></TD>
														</TR>
														<TR class="dark-row" id="lblStatusLERRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><STRONG><asp:label id="lblStatusLERLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblStatusLER" runat="server"></asp:label></TD>
															<TD class="dark-row" vAlign="top"></TD>
														</TR>
														<TR class="dark-row" id="lblStatusSLERRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><STRONG><asp:label id="lblStatusSLERLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblStatusSLER" runat="server"></asp:label></TD>
															<TD class="dark-row" vAlign="top"></TD>
														</TR>
														<TR class="dark-row" id="lblCityRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><STRONG><asp:label id="lblCityLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblCity" runat="server"></asp:label></TD>
															<TD class="dark-row" vAlign="top"></TD>
														</TR>
														<TR class="dark-row" id="lblPhoneRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><STRONG><asp:label id="lblPhoneLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblPhone" runat="server"></asp:label></TD>
															<TD class="dark-row" vAlign="top"></TD>
														</TR>
														<TR class="dark-row" id="lblFaxRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><STRONG><asp:label id="lblFaxLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblFax" runat="server"></asp:label></TD>
															<TD class="dark-row" vAlign="top"></TD>
														</TR>
														<TR class="dark-row" id="lblAddressRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><STRONG><asp:label id="lblAddressLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblAddress" runat="server"></asp:label></TD>
															<TD class="dark-row" vAlign="top"></TD>
														</TR>
														<TR class="dark-row" id="lblOfficeAddressRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><STRONG><asp:label id="lblOfficeAddressLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblOfficeAddress" runat="server"></asp:label></TD>
															<TD class="dark-row" vAlign="top"></TD>
														</TR>
														<TR class="dark-row" id="lblEmailRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><STRONG><asp:label id="lblEmailLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblEmail" runat="server"></asp:label></TD>
															<TD class="dark-row" vAlign="top"></TD>
														</TR>
														<TR class='dark-row' id="lblEstablishmentRow" runat="server">
															<TD class="dark-row" style="WIDTH: 123px" vAlign="top"><STRONG><asp:label id="lblEstablishmentLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" style="WIDTH: 343px" vAlign="top"><asp:label id="lblEstablishment" runat="server"></asp:label></TD>
															<TD class="dark-row" valign="top"></TD>
														</TR>
														<TR class='dark-row' id="lblRegistrationRow" runat="server">
															<TD class="dark-row" valign="top" style="WIDTH: 123px"><STRONG><asp:label id="lblRegistrationLabel" runat="server"></asp:label></STRONG></TD>
															<TD class="dark-row" valign="top" style="WIDTH: 343px"><asp:label id="lblRegistration" runat="server"></asp:label></TD>
															<TD class="dark-row" valign="top"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR id="Errorr" runat="server">
												<TD style="HEIGHT: 33px" align="center"><asp:label id="lblNoClaimRegistered" runat="server" ForeColor="Red" Visible="False">No claim registered for the subject</asp:label></TD>
											</TR>
											<TR id="Tr1" runat="server">
												<TD style="HEIGHT: 15px" align="left"></TD>
											</TR>
											<TR id="headLtDDDLine" runat="server">
												<TD class="sectionheader" id="headLtDDD" style="HEIGHT: 33px" align="left" runat="server"><asp:label id="lblLtClaims" runat="server" CssClass="sectionheader">Claims LT</asp:label></TD>
											</TR>
											<TR id="ltDDDELine" runat="server">
												<TD align="center"></TD>
											</TR>
											<TR id="ltDDDRepeaterRow" runat="server">
												<TD align="left"><asp:repeater id="repeatLtDDD" runat="server">
														<ItemTemplate>
															<table runat="server" id="LtListing" class="list" BORDER="0" CELLSPACING="0" CELLPADDING="3"
																width="100%">
																<tr>
																	<td rowspan="3"></td>
																	<td colspan="2" class="listhead">
																		<b>
																			<%#rm.GetString("lbCaseID",ci)%>
																			: </b>
																		<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
																	</td>
																	<td colspan="2" class="listhead">
																		<b>
																			<%#rm.GetString("lbDirector",ci)%>
																			: </b>
																		<%#DataBinder.Eval(Container.DataItem, "director")%>
																	</td>
																</tr>
																<tr>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimType",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
																		<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "amount")%>
																	</td>
																	<td class="dark-row" valign="top"><b></b><br>
																	</td>
																</tr>
																<tr>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerCode",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "creditor_code")%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "creditor")%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("txtInformationSource",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
																	</td>
																</tr>
																<tr>
																	<td><br>
																	</td>
																</tr>
															</table>
														</ItemTemplate>
													</asp:repeater></TD>
											</TR>
											<TR id="ltDDDPersonRepeaterRow" runat="server">
												<TD align="left"><asp:repeater id="repeatPersonLtDDD" runat="server">
														<ItemTemplate>
															<table runat="server" id="LtListingPerson" class="list" BORDER="0" CELLSPACING="0" CELLPADDING="3"
																width="100%">
																<tr>
																	<td rowspan="4"></td>
																	<td class="listhead">
																		<b>
																			<%#rm.GetString("lbCaseID",ci)%>
																			: </b>
																		<%#DataBinder.Eval(Container.DataItem, "claim_id")%>
																	</td>
																	<td class="listhead">
																	</td>
																</tr>
																<tr>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbPersonCode",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "code")%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbPersonName",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "pname")%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbPersonAddress",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "address")%>
																	</td>
																	<td class="dark-row" valign="top"><b></b><br>
																	</td>
																</tr>
																<tr>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimType",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "claim_type")%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimDate",ci)%></b><br>
																		<%#((DateTime)DataBinder.Eval(Container.DataItem, "claim_date")).ToShortDateString()%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimAmount",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "amount")%>
																	</td>
																	<td class="dark-row" valign="top"><b></b><br>
																	</td>
																</tr>
																<tr>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerCode",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "creditor_code")%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("lbClaimOwnerName",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "creditor")%>
																	</td>
																	<td class="dark-row" valign="top"><b><%#rm.GetString("txtInformationSource",ci)%></b><br>
																		<%#DataBinder.Eval(Container.DataItem, "information_source_name")%>
																	</td>
																</tr>
																<tr>
																	<td><br>
																	</td>
																</tr>
															</table>
														</ItemTemplate>
													</asp:repeater></TD>
											</TR>
											<TR id="lblRecentLine" runat="server">
												<TD class="sectionheader" style="HEIGHT: 33px" align="left"><asp:label id="lblRecent" runat="server"></asp:label></TD>
											</TR>
											<TR id="lblSixMonthsLine" runat="server">
												<TD align="left"><asp:label id="lblSixMonths" runat="server" CssClass="list"></asp:label></TD>
											</TR>
											<TR id="lblThreeMonthsLine" runat="server">
												<TD style="HEIGHT: 15px" align="left"><asp:label id="lblThreeMonths" runat="server" CssClass="list"></asp:label></TD>
											</TR>
											<TR id="lblOneMonthLine" runat="server">
												<TD align="left"><asp:label id="lblOneMonth" runat="server" CssClass="list"></asp:label></TD>
											</TR>
											<TR id="linkBackLine" runat="server">
												<TD style="HEIGHT: 55px" align="left"><A id="linkBack" href="javascript:history.back()" runat="server">Back</A></TD>
											</TR>
											<TR id="lblDisclaimerLine" runat="server">
												<TD style="HEIGHT: 55px" align="left"><asp:label id="lblDisclaimer" runat="server"></asp:label></TD>
											</TR>
											<tr id="Line" runat="server">
												<TD style="HEIGHT: 15px" align="center" bgColor="#951e16"></TD>
											</tr>
										</TBODY>
										<!-- Content ends --></TABLE>
								</td>
							</tr>
						</table>
						<P></P>
					</td>
				</tr>
				<tr>
					<td align="center"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
		</TD></TR></TBODY></TABLE>
	</body>
</HTML>
