﻿using Billing.BLL;
using cb3.CacheBase;
using Cig.Framework.Base.Configuration;
using CreditInfoGroup.Localization;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Threading;
using System.Web;
using System.Web.Security;
using cb3.Settings;
using UserAdmin.BLL;

namespace cb3
{
    public partial class Login : System.Web.UI.Page
    {
        private uaFactory myUserAdminFactory;
        public static ResourceManager rm;
        private CultureInfo ci;
        public string rightBoxText { get; set; }
        public string leftBoxText { get; set; }
        public string rightBoxHeader { get; set; }
        public string leftBoxHeader { get; set; }
        public string alertCSSStyle { get; set; }
                
        public const string LeftBoxPath = "../TextBox-Left.html";
        public const string RightBoxPath = "../TextBox-Right.html";

        protected void Page_Load(object sender, EventArgs e)
        {
            rm = CIResource.CurrentManager;
            ci = Thread.CurrentThread.CurrentCulture;
            myUserAdminFactory = new uaFactory();
            var cache = new HttpCacheAdapter();

            var descriptions =  myUserAdminFactory.GetSearchAndTafDescriptions();

            var leftText = File.ReadAllText(LeftBoxPath);
            var rightText = File.ReadAllText(RightBoxPath);

            //cache.Store("loginLeftBox", leftText);
            //cache.Store("loginRightBox", rightText);

            leftBoxText = HttpUtility.HtmlDecode(leftText);
            rightBoxText = HttpUtility.HtmlDecode(rightText);
            alertCSSStyle = "alert alert-danger";
        }

        private bool IsAuthenticated(string username, string password)
        {
            // Lookup code omitted for clarity
            // This code would typically validate the user name and password
            // combination against a SQL database or Active Directory
            // Simulate an authenticated user

            bool passwordVerified = false;
            try
            {
                passwordVerified = myUserAdminFactory.VerifyPassword(username, password);
            }
            catch (Exception ex)
            {
                errormessage.Visible = true;
                loginErrorMessage.Text = ex.Message;
            }
            return passwordVerified;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string userName = tbUserName.Text;
                bool isAuthenticated = IsAuthenticated(userName, tbPassword.Text);

                if (isAuthenticated)
                {
                    int userLogInID = myUserAdminFactory.GetUserID(userName);
                    // Athuga hvort aр notandi sй opinn. Ef ekki юб hefur hann ekki aрgang
                    if (!myUserAdminFactory.IsUserOpen(userLogInID))
                    {
                        errormessage.Visible = true;
                        loginErrorMessage.Text = "User account is closed";
                        return;
                    }

                    if (myUserAdminFactory.IsAccountLocked(userLogInID))
                    {
                        errormessage.Visible = true;
                        loginErrorMessage.Text = rm.GetString("lblLoginAccountLockedMessage", ci);
                        return;
                    }

                    // Athuga hvort aр notandi sй ъtrunninn (Open until). Ef hann er ъtrunninn - Enginn aрgangur
                    if (!myUserAdminFactory.IsUserExpiryDateOK(userLogInID))
                    {
                        errormessage.Visible = true;
                        loginErrorMessage.Text = rm.GetString("lblLoginAccountExpired", ci);
                        passwordExpired.Visible = true;
                        btnLogin.Visible = false;
                        lbPassword.InnerText = "Old Password: ";
                        return;
                    }

                    // Athuga hvort aр deild sй lokuр. Ef hъn er lokuр юб er lokaр б alla skrбрa notendur undi henni.
                    if (!myUserAdminFactory.IsDepartmentOpen(userLogInID))
                    {
                        errormessage.Visible = true;
                        loginErrorMessage.Text = "Department account is closed";
                        return;
                    }

                    // Athuga hvort aр бskrifandi sй lokaрur. Ef hann er lokaрur юб er lokaр б alla skrбрa notendur
                    // Undir honum.
                    if (!myUserAdminFactory.IsSubscriberOpen(userLogInID))
                    {
                        errormessage.Visible = true;
                        loginErrorMessage.Text = "Subscriber account is closed";
                        return;
                    }

                    //OK - the user is ok and ready to use the web
                    Session.Add("UserCreditInfoID", myUserAdminFactory.GetCreditInfoID(userName));
                    Session.Add("CreditWatch", myUserAdminFactory.GetMaxCreditWatch(userName));
                    Session.Add("UserLoginID", userLogInID.ToString());
                    Session["UserLoginName"] = userName;

                    //Load user info (name and subscriber name into session
                    var user = myUserAdminFactory.GetSpecificUser(userName);
                    var subscriber =
                        myUserAdminFactory.GetOneSubscriberByCreditInfoIDAsSubscriber(user.SubscriberID);

                    string nativeUserName = myUserAdminFactory.GetCreditInfoUserNativeName(user.CreditInfoID);
                    string nativeSubscriberName = myUserAdminFactory.GetCreditInfoUserNativeName(
                        subscriber.CreditInfoID);
                    // if no native name the get the english one
                    if (nativeUserName.Trim() == "")
                    {
                        nativeUserName = myUserAdminFactory.GetCreditInfoUserENName(user.CreditInfoID);
                    }
                    if (nativeSubscriberName.Trim() == "")
                    {
                        nativeSubscriberName = myUserAdminFactory.GetCreditInfoUserENName(user.CreditInfoID);
                    }
                    Session.Add("NativeUserName", nativeUserName);
                    Session.Add("NativeSubscriberName", nativeSubscriberName);

                    string enUserName = myUserAdminFactory.GetCreditInfoUserENName(user.CreditInfoID);
                    string enSubscriberName = myUserAdminFactory.GetCreditInfoUserENName(subscriber.CreditInfoID);
                    // if no english name then get the native one
                    if (enUserName.Trim() == "")
                    {
                        enUserName = myUserAdminFactory.GetCreditInfoUserNativeName(user.CreditInfoID);
                    }
                    if (enSubscriberName.Trim() == "")
                    {
                        enSubscriberName = myUserAdminFactory.GetCreditInfoUserNativeName(user.CreditInfoID);
                    }

                    Session.Add("ENUserName", enUserName);
                    Session.Add("ENSubscriberName", enSubscriberName);
                    Session.Add("isPrintBlocked", subscriber.isPrintBlocked);

                    // Redirect the user to the originally requested page
                    // Response.Redirect(FormsAuthentication.GetRedirectUrl(tbUserName.Text, false));
                    FormsAuthentication.RedirectFromLoginPage(tbUserName.Text, false);
                }
                else
                {
                    var TryCount = (int)Session["LoginTryCount"];

                    if (TryCount <= 1)
                    {
                        bool isAccountLocked = myUserAdminFactory.AccountLock(userName, true);

                        string myMessage = "User failed to log in three times. User is coming from IP address: " +
                                           Request.ServerVariables["REMOTE_ADDR"] + "\n\nUser tried to use name: " +
                                           tbUserName.Text + " and password: " + tbPassword.Text;

                        string mySubject = "CreditInfoGroup Login Failed (" +
                                           CigConfig.Configure("lookupsettings.rootURL") + ")";

                        var myFunctions = new HelpFunctions();
                        //myFunctions.SendMail(myMessage, mySubject);

                        errormessage.Visible = true;
                        var loginFailedResourceString = rm.GetString("lblLoginFailedMessage", ci);

                        loginErrorMessage.Text = String.Format(loginFailedResourceString, AuUserSettings.NUMBER_OF_WRONG_PASSWORD_ATTEMPT);
                        if(isAccountLocked) { loginErrorMessage.Text += " Your Account has also been locked"; }

                    }
                    else
                    {
                        Session["LoginTryCount"] = --TryCount;
                        errormessage.Visible = true;
                        loginErrorMessage.Text = rm.GetString("lblLoginTryAgainMessage", ci) + TryCount;
                    }
                }
            }
            catch (Exception ex)
            {
                //				if ( 0 == FailedLoginMessage.InnerText.Length)
                errormessage.Visible = true;
                loginErrorMessage.Text = ex.Message;
            }
        }

        protected void ChangePass_Click(object sender, EventArgs e)
        {
            try
            {

                passwordExpired.Visible = true; // set to false upon no error
                var user = myUserAdminFactory.GetSpecificUser(tbUserName.Text);

                if (tbNewPass.Text != tbReEnterNewPass.Text)
                {
                    loginErrorMessage.Text = "New Password and Re-entered password are not the same";
                    tbReEnterNewPass.Focus();
                    return;
                }

                if(tbUserName.Text.Trim() == "" || tbPassword.Text.Trim() == "")
                {
                    loginErrorMessage.Text = "Username and Old Password must be entered";
                    tbPassword.Focus();
                    return;

                }

                if (tbNewPass.Text.Trim() == "")
                {
                    loginErrorMessage.Text = "Please Enter your new password";
                    tbNewPass.Focus();
                    return;
                }

                if (tbNewPass.Text == tbPassword.Text)
                {
                    loginErrorMessage.Text = "New Password should not be the same as old password";
                    tbNewPass.Focus();
                    return;
                }

                
                if (!IsAuthenticated(tbUserName.Text, tbPassword.Text))
                {
                    loginErrorMessage.Text = "Authentication failed: Old Password is not correct";
                    return;
                }



                if (myUserAdminFactory.ChangePassword(user, tbNewPass.Text))
                {
                    loginErrorMessage.Text = "Password changed successfully";
                    alertCSSStyle = "success alert-success";
                    passwordExpired.Visible = false;
                    btnLogin.Visible = true;
                    lbPassword.InnerText = "Password:";

                }


            }
            catch (Exception ex)
            {
                errormessage.Visible = true;
                loginErrorMessage.Text = ex.Message;
            }



        }
    }
}