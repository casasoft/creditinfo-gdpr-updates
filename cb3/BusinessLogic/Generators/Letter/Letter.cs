/* 
 * $Header: /CigSuite_v2/CreditInfoGroup_mt/CreditInfoGroup/BusinessLogic/Generators/Letter/Letter.cs 1     24.08.07 10:20 M.dubec $
 * $Log: /CigSuite_v2/CreditInfoGroup_mt/CreditInfoGroup/BusinessLogic/Generators/Letter/Letter.cs $ 
 * 
 * 1     24.08.07 10:20 M.dubec
 * 
 * 3     29.06.05 9:07 Ondrakj
 * comments

 */
using System;

namespace BusinessLogic.Generators.Letter
{
	/// <summary>
	/// Letter object
	/// <para>Copyright (C) 2005 Haukur �rn Har�arson & Jiri Ondrak - Creditinfo Group ltd.</para>
	/// </summary>
	[Serializable]
	public class Letter
	{
		private String _FirstName = "";
		private String _SurName = "";
		private String _Address = "";
		private String _postcode = "";
		private String _Town = "";
		private String _debtor_Ref = "";
		private String _informationSource = "";
		private String _informationSourceEN = "";
		private String _caseNo = "";
		private String _caseDate = "";
		private String _type = "";
		private String _typeEN = "";
		private String _claimOwnerName="";
		private DateTime _letterDate = DateTime.Now;
		private int _letterType = 0;
		/// <summary>
		/// Default constructor
		/// </summary>
		public Letter()
		{
			
		}

		/// <summary>
		/// Firstname of consignee 
		///
		/// </summary>
		public String FirstName
		{
			get {return this._FirstName;}
			set {this._FirstName = value;}
		}

		/// <summary>
		/// Surname of consignee
		/// </summary>
		public String SurName
		{
			get {return this._SurName;}
			set {this._SurName = value;}
		}

		/// <summary>
		/// Adress of consignee
		/// </summary>
		public String Address
		{
			get {return this._Address;}
			set {this._Address = value;}
		}

		/// <summary>
		/// Postcode of consignee
		/// </summary>
		public String Postcode
		{
			get {return this._postcode;}
			set {this._postcode = value;}
		}

		/// <summary>
		/// Town of consignee
		/// </summary>
		public String Town 
		{
			get {return this._Town;}
			set {this._Town = value;}
		}

		/// <summary>
		/// Ref of consignee
		/// </summary>
		public String Debtor_Ref
		{
			get {return this._debtor_Ref;}
			set {this._debtor_Ref = value;}
		}

		/// <summary>
		/// Creditors adressis for the case
		/// </summary>
		public String CreditorsList 
		{
			get {return this._claimOwnerName;}
			set {this._claimOwnerName = value;}
		}

		/// <summary>
		/// InformationSource for the case
		/// </summary>
		public String InformationSource
		{
			get {return this._informationSource;}
			set {this._informationSource = value;}
		}

		/// <summary>
		/// InformationSource ENG
		/// </summary>
		public String InformationSourceEN
		{
			get {return this._informationSourceEN;}
			set {this._informationSourceEN = value;}
		}

		/// <summary>
		/// Case number
		/// </summary>
		public String CaseNo
		{
			get {return this._caseNo;}
			set {this._caseNo = value;}
		}

		/// <summary>
		/// Case date
		/// </summary>
		public String CaseDate 
		{
			get {return this._caseDate;}
			set {this._caseDate = value;}
		}

		/// <summary>
		/// Case type
		/// </summary>
		public String Type
		{
			get {return this._type;}
			set {this._type = value;}
		}

		/// <summary>
		/// Case Type ENG
		/// </summary>
		public String TypeEN
		{
			get {return this._typeEN;}
			set {this._typeEN = value;}
		}
	
		/// <summary>
		/// Date of the letter
		/// </summary>
		public DateTime LetterDate
		{
			get {return this._letterDate;}
			set {this._letterDate = value;}
		}

		/// <summary>
		/// Type of the letter
		/// </summary>
		public int LetterType
		{
			get {return this._letterType;}
			set {this._letterType = value;}
		}
	}
}
