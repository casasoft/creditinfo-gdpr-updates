#region

using System.Collections;
using System.IO;

#endregion

namespace BusinessLogic.Generators.Letter {
    /// <summary>
    /// The Letter generator
    /// </summary>
    public class LetterGenerator {
        /// <summary>
        /// string of grnerating letters
        /// </summary>
        /// <param name="path">path to the letter template</param>
        /// <param name="genLetter">the letter</param>
        /// <returns></returns>
        public string Generate(string path, Letter genLetter) {
            var objReader = new StreamReader(path);
            var docTemplate = "";
            var arrText = new ArrayList();

            var sLine = objReader.ReadToEnd();
            if (sLine != null) {
                arrText.Add(sLine);
                docTemplate += sLine;
            }

            objReader.Close();

            return GenerateLetters(docTemplate, genLetter);
        }

        /// <summary>
        /// Letter generating
        /// </summary>
        /// <param name="docTemplate">Template for letter</param>
        /// <param name="genLetter">Letter</param>
        /// <returns></returns>
        public string GenerateLetters(string docTemplate, Letter genLetter) {
            //values to replace definition
            string[] myStringArr = {
                                       "%DebtorFirstName%", "%DebtorSurName%", "%DebtorAddress%", "%Postcode%",
                                       "%DebtorTown%", "%HeadDate%", "%Debtor_Ref%", "%InformationSourceNative%",
                                       "%CaseNo%", "%CaseDate%", "%Type%", "%Claim_Owner%"
                                   };
            string[] myReplaceValues = {
                                           genLetter.FirstName, genLetter.SurName, genLetter.Address, genLetter.Postcode,
                                           genLetter.Town, genLetter.LetterDate.ToShortDateString(), genLetter.Debtor_Ref,
                                           genLetter.InformationSource, genLetter.CaseNo, genLetter.CaseDate,
                                           genLetter.Type, genLetter.CreditorsList
                                       };

            int count = 0;
            foreach (string searchString in myStringArr) {
                docTemplate = docTemplate.Replace(searchString, myReplaceValues[count]);
                count++;
            }
            return docTemplate;
        }
    }
}