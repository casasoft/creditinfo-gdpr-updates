#region

using System.Collections;
using System.Globalization;
using System.Resources;
using System.Xml;
using BusinessLogic.Service.FSI;
using BusinessLogic.Util.Xml;
using MathExpr;
using ValueObjects.FSI.Templates;

#endregion

namespace BusinessLogic.Generators.Report {
    /// <summary>
    /// This class is responsible to generate xml element that represents 
    /// ratios for financial statements accounts.  The generator creates the
    /// formula used to calculate the ratio and use given values to calculate
    /// the result for ratios.  
    /// </summary>
    public class RatioGenerator : BaseReportGenerator {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rm">The resource manager to use</param>
        /// <param name="ci">The culture info to use</param>
        public RatioGenerator(ResourceManager rm, CultureInfo ci) : base(rm, ci) { }

        /// <summary>
        /// Creates and returns description for all ratios in given template
        /// </summary>
        /// <param name="doc">The document to create xml from</param>
        /// <param name="templateID">The id of the template</param>
        /// <returns>Description for all ratios for given template as xml element</returns>
        public XmlElement GetRatiosDescription(XmlDocument doc, int templateID) {
            var defOfRatios = XmlHelper.GetXmlTable(doc, "DefinitionOfRatios", "tblDefinitionOfRatios");
            XmlHelper.AddAttribute(doc, defOfRatios, "title", rm.GetString("txtDefinitionOfRatios", ci));

            var template = TemplatesService.GetInstance().GetTemplate(templateID);
            if (template != null) {
                for (int i = 0; i < template.TemplateRatios.Count; i++) {
                    IRatio ratio = ((ITemplateRatio) template.TemplateRatios[i]).Ratio;

                    var item = XmlHelper.GetXmlTableItem(doc);
                    if (isNativeCulture) {
                        XmlHelper.AddAttribute(doc, item, "Title", ratio.NameNative);
                        XmlHelper.AddAttribute(doc, item, "Value", ratio.DescriptionNative);
                    } else {
                        XmlHelper.AddAttribute(doc, item, "Title", ratio.NameEN);
                        XmlHelper.AddAttribute(doc, item, "Value", ratio.DescriptionEN);
                    }
                    defOfRatios.AppendChild(item);
                }
            }
            return defOfRatios;
        }

        /// <summary>
        /// Creates and returns all ratios for given template
        /// </summary>
        /// <param name="doc">The document to create xml from</param>
        /// <param name="templateID">The id of the template</param>
        /// <param name="fsiValues">The account values</param>
        /// <returns>All ratios for given template as xml element</returns>
        public XmlElement GetRatios(XmlDocument doc, int templateID, ArrayList fsiValues) {
            XmlElement ratios = XmlHelper.CreateElement(doc, "Ratios", "");
            XmlHelper.AddAttribute(doc, ratios, "Title", rm.GetString("txtRatios", ci));

            ITemplate template = TemplatesService.GetInstance().GetTemplate(templateID);
            if (template != null) {
                for (int i = 0; i < template.TemplateRatios.Count; i++) {
                    IRatio ratio = ((ITemplateRatio) template.TemplateRatios[i]).Ratio;
                    ratios.AppendChild(GetRatio(doc, ratio, fsiValues));
                }
            }
            return ratios;
        }

        /// <summary>
        /// Returns the ratio as xml element
        /// </summary>
        /// <param name="doc">The document to create xml from</param>
        /// <param name="ratio">The ratio to calculate and generate xml for</param>
        /// <param name="fsiValues">The account values</param>
        /// <returns>The calculated ratio as xml element</returns>
        protected XmlElement GetRatio(XmlDocument doc, IRatio ratio, ArrayList fsiValues) {
            XmlElement el = XmlHelper.CreateElement(doc, "Ratio", "");
            if (isNativeCulture) {
                XmlHelper.AddAttribute(doc, el, "Title", ratio.NameNative);
            } else {
                XmlHelper.AddAttribute(doc, el, "Title", ratio.NameEN);
            }

            for (int i = 0; i < fsiValues.Count; i++) {
                XmlElement item = XmlHelper.GetXmlTableItem(doc);
                XmlHelper.AddAttribute(
                    doc, item, "Value", GetRatioValue(ratio.Formula, i, fsiValues, ratio.InPercentageFormat));
                el.AppendChild(item);
            }
            return el;
        }

        /// <summary>
        /// Created and evaluates the ratio formula based on given values.  Returns the
        /// result as string
        /// </summary>
        /// <param name="formulas">List of formula fields</param>
        /// <param name="year">Index of the year to calculate formula for</param>
        /// <param name="fsiValues">The account values</param>
        /// <returns>Result of the calculations</returns>
        protected string GetRatioValue(IList formulas, int year, ArrayList fsiValues, bool resultInPercentage) {
            string expression = "";
            for (int i = 0; i < formulas.Count; i++) {
                var formula = (IRatioFormula) formulas[i];
                if (formula.IsField) {
                    IField field = TemplatesService.GetInstance().GetField(formula.FormulaItemID);
                    Hashtable val;
                    if (formula.IsFormerYear) {
                        if (fsiValues.Count > (year + 1)) {
                            val = (Hashtable) fsiValues[year + 1];
                        } else {
                            return "N/A";
                        }
                    } else {
                        val = (Hashtable) fsiValues[year];
                    }
                    if (val.Contains(field.FieldName)) {
                        expression += val[field.FieldName];
                    }
                } else {
                    expression += TemplatesService.GetInstance().GetOperator(formula.FormulaItemID).Symbol;
                }
            }
            var eval = new Evaluator();
            var parser = new Parser();
            float res = eval.Evaluate(parser.Parse(expression));
            return resultInPercentage ? res.ToString("P", GetPercentageFormat()) : res.ToString("N");
        }

        private static NumberFormatInfo GetPercentageFormat() {
            var fInfo = (NumberFormatInfo) NumberFormatInfo.CurrentInfo.Clone();
            fInfo.PercentDecimalDigits = 2;
            fInfo.NaNSymbol = "N/A";
            fInfo.PercentPositivePattern = 0;
            fInfo.PercentNegativePattern = 0;
            return fInfo;
        }
    }
}