#region

using System.Configuration;
using System.Globalization;
using System.Resources;
using System.Threading;

using Cig.Framework.Base.Configuration;

#endregion

namespace BusinessLogic.Generators.Report {
    /// <summary>
    /// Summary description for BaseReportGenerator.
    /// </summary>
    public class BaseReportGenerator {
        protected CultureInfo ci;
        protected bool isNativeCulture = true;
        protected ResourceManager rm;

        public BaseReportGenerator(ResourceManager rm, CultureInfo ci) {
            this.rm = rm;
            this.ci = ci;

            string culture = Thread.CurrentThread.CurrentCulture.Name;

            // check the current culture
            string nativeCulture = CigConfig.Configure("lookupsettings.nativeCulture");
            if (culture.Equals(nativeCulture)) {
                isNativeCulture = true;
            }
        }
    }
}