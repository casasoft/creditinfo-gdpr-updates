#region

using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;

#endregion

namespace BusinessLogic.Generators.Page {
    /// <summary>
    /// Summary description for Transformer.
    /// </summary>
    public class Transformer {
        private readonly Panel controlContainer;
        private readonly string xslFile;

        public Transformer(Panel container, string xslFile) {
            controlContainer = container;
            this.xslFile = xslFile;
        }

        public void TransformFields(string pageId, XmlDocument doc) {
            /**
			 * Transform aspx page with fields from xml file
			 * Get transform result as a string
			 * Parse controls into a parent control holder
			 */

            // load xslt to do transformation
            var xsl = new XslTransform();
            xsl.Load(xslFile);

            // load xslt arguments to load specific page from xml file
            // this can be used if you have multiple pages in your xml file and you loading them one at a time
            var xslarg = new XsltArgumentList();
            xslarg.AddParam("pageid", "", pageId);

            // get transformed results
            var sw = new StringWriter();
            xsl.Transform(doc, xslarg, sw);
            string result = sw.ToString().Replace("xmlns:asp=\"remove\"", "").Replace("&lt;", "<").Replace("&gt;", ">");

            // free up the memory of objects that are not used anymore
            sw.Close();

            // parse the controls and add it to the page
            Control ctrl = controlContainer.Page.ParseControl(result);
            controlContainer.Controls.Add(ctrl);
        }
    }
}