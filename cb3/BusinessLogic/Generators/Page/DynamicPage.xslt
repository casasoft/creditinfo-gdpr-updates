<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" 
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="remove">
	<xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes">
	</xsl:output>
	<xsl:template match="/">

		<!-- page id parameter if we have more than 1 page in our XML file -->
		<xsl:param name="pageid"/>

		<!-- start form page -->
		<xsl:element name="table">
			<xsl:attribute name="cellspacing">0</xsl:attribute>
			<xsl:attribute name="cellspacing">0</xsl:attribute>
			<xsl:attribute name="width">100%</xsl:attribute>	

			<!-- set title of the current page -->
			<!--<xsl:element name="tr">
				<xsl:element name="td">	
					<xsl:attribute name="colspan">3</xsl:attribute>
					<xsl:attribute name="align">center</xsl:attribute>	
					<xsl:attribute name="style">font-size:25px</xsl:attribute>	
					<xsl:value-of select="FORM/PAGES/PAGE[@id=$pageid]/@title" />
				</xsl:element>
			</xsl:element>-->
			
			<xsl:element name="tr">
				<xsl:element name="td">	
					<xsl:attribute name="colspan">3</xsl:attribute>
					<xsl:attribute name="style">height:20px</xsl:attribute>	
				</xsl:element>
			</xsl:element>
			<!-- iterate through page fields -->
			<xsl:for-each select="FORM/PAGES/PAGE[@id=$pageid]/SECTION">
				<xsl:call-template name="Section">
				</xsl:call-template>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
  
	<xsl:template name='Section' match='Section'>
		<xsl:element name="tr">
			<xsl:element name="td">				
				<xsl:element name="table">
					<xsl:attribute name="class">grid_table</xsl:attribute>
					<xsl:attribute name="cellspacing">0</xsl:attribute>
					<xsl:attribute name="cellpadding">0</xsl:attribute>					
					<xsl:element name="tr">
						<xsl:element name="th">
							<xsl:element name="span">
								<xsl:value-of select="@title" />
							</xsl:element>
						</xsl:element>
					</xsl:element>	
					<xsl:element name="tr">
						<xsl:element name="td">
							<xsl:element name="table">
								<xsl:attribute name="id">
									<xsl:value-of select="@id" />
								</xsl:attribute>	
								<xsl:attribute name="class">fields</xsl:attribute>
								<xsl:attribute name="cellspacing">0</xsl:attribute>
								<xsl:attribute name="cellpadding">0</xsl:attribute>		
								<xsl:for-each select="ROW">
									<xsl:call-template name="Row"/>
								</xsl:for-each>	
								<xsl:element name="tr">
									<xsl:element name="td">
										<xsl:attribute name="height">23</xsl:attribute>
									</xsl:element>
								</xsl:element>	
							</xsl:element>
						</xsl:element>
					</xsl:element>																									
				</xsl:element>	
			</xsl:element>
		</xsl:element>		
	</xsl:template>
	
	<xsl:template name='Row' match='Row'>
		<xsl:element name="tr">
			<xsl:for-each select="PROPERTIES/PROPERTY">
				<xsl:attribute name="{@name}">
					<xsl:value-of select="current()"></xsl:value-of>
				</xsl:attribute>
			</xsl:for-each>
			<xsl:for-each select="COLUMN">
				<xsl:call-template name="Column"/>
			</xsl:for-each>		
		</xsl:element>		
	</xsl:template>
	
	<xsl:template name='Column' match='Column'>
		<xsl:element name="td">
			<xsl:for-each select="PROPERTIES/PROPERTY">
				<xsl:attribute name="{@name}">
					<xsl:value-of select="current()"></xsl:value-of>
				</xsl:attribute>
			</xsl:for-each>
			<xsl:for-each select="FIELD">
				<xsl:call-template name="Field"/>
			</xsl:for-each>			
		</xsl:element>		
	</xsl:template>
	
	<xsl:template name='Field' match='Field'>
		<xsl:element name="span">
			<xsl:value-of select="@label" />
		</xsl:element>
		<xsl:element name="br"/>
		
		<xsl:element name="asp:{@type}">
			<xsl:attribute name="runat">server</xsl:attribute>
			<xsl:for-each select="PROPERTIES/PROPERTY">
				<xsl:attribute name="{@name}">
					<xsl:value-of select="current()"></xsl:value-of>
				</xsl:attribute>
			</xsl:for-each>
			<xsl:for-each select="LISTITEMS/LISTITEM">
				<xsl:element name="asp:ListItem">
				<xsl:attribute name="value"><xsl:value-of select="@value" /></xsl:attribute>
				<xsl:attribute name="runat">server</xsl:attribute>
					<xsl:for-each select="PROPERTIES/PROPERTY">
						<xsl:attribute name="{@name}">
							<xsl:value-of select="current()"></xsl:value-of>
						</xsl:attribute>
					</xsl:for-each>
				</xsl:element>
			</xsl:for-each>
			<xsl:value-of select="@value" />
		</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>