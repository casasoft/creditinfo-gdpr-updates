#region

using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using BusinessLogic.Util.Xml;

#endregion

namespace BusinessLogic.Generators.Page {
    /// <summary>
    /// This class contains helper methods for dynamic page creating and handling.
    /// </summary>
    public class DynamicPageHelper {
        /// <summary>
        /// Transform page from xml into html controls - add result to given container
        /// </summary>
        /// <param name="container">The container to add html controls to</param>
        /// <param name="styleSheet">Url to the stylesheet to use for parsing</param>
        /// <param name="doc">The xml description of the page</param>
        /// <param name="pageName">Name of the page inside the xml document</param>
        public static void TransformPage(Panel container, string styleSheet, XmlDocument doc, string pageName) {
            var xsl = new Transformer(container, styleSheet);
            xsl.TransformFields(pageName, doc);
        }

        /// <summary>
        /// Returns a hashtable containing all fields along with their type. 
        /// The keys in the hashtable are field ids and value for each key is 
        /// the field type.
        /// </summary>
        /// <param name="doc">The document describing the page.</param>
        /// <param name="pageName"></param>
        /// <returns>Hashtable containing all fields along with their type.</returns>
        public static Hashtable GetAllFields(XmlDocument doc, string pageName) {
            var h = new Hashtable();
            var pageList = doc.GetElementsByTagName("PAGE");
            for (int l = 0; l < pageList.Count; l++) {
                if (((XmlElement) pageList[l]).GetAttribute("id") != pageName) {
                    continue;
                }
                var list = ((XmlElement) pageList[l]).GetElementsByTagName("FIELD");

                for (int i = 0; i < list.Count; i++) {
                    string type = ((XmlElement) list[i]).GetAttribute("type");
                    string name = "";
                    var pList = ((XmlElement) list[i]).GetElementsByTagName("PROPERTY");
                    for (int j = 0; j < pList.Count; j++) {
                        if (((XmlElement) pList[j]).GetAttribute("name") == "ID") {
                            name = (pList[j]).InnerText;
                        }
                    }
                    if (name != "") {
                        h.Add(name, type);
                    }
                }
            }
            return h;
        }

        /// <summary>
        /// Loads all values for dynamic page defined in document into hashtable
        /// </summary>
        /// <param name="request">The request to load from</param>
        /// <param name="doc">The document describing the page.</param>
        /// <param name="pageName"></param>
        /// <returns>Hashtable containing name of field along with value</returns>
        public static Hashtable GetAllRequestValues(HttpRequest request, XmlDocument doc, string pageName) {
            var h = GetAllFields(doc, pageName);
            var myEnumerator = h.GetEnumerator();
            var hValues = new Hashtable();
            while (myEnumerator.MoveNext()) {
                if (request[(string) myEnumerator.Key] != null) {
                    hValues.Add(myEnumerator.Key, request[(string) myEnumerator.Key]);
                }
            }
            return hValues;
        }

        /// <summary>
        /// Loads values from request into xml document that describes the page.  This function
        /// simulates viewstate.
        /// </summary>
        /// <param name="request">The request to load from</param>
        /// <param name="doc">The document describing the page.  Values are loaded in to the document</param>
        /// <param name="pageName"></param>
        public static void LoadRequestValuesIntoDocument(HttpRequest request, XmlDocument doc, string pageName) {
            var values = GetAllRequestValues(request, doc, pageName);
            var list = doc.GetElementsByTagName("FIELD");
            for (int i = 0; i < list.Count; i++) {
                var field = (XmlElement) list[i];
                var properties = (XmlElement) field.SelectSingleNode("PROPERTIES");
                var pList = properties.GetElementsByTagName("PROPERTY");
                for (int j = 0; j < pList.Count; j++) {
                    if (((XmlElement) pList[j]).GetAttribute("name") != "ID") {
                        continue;
                    }
                    var name = (pList[j]).InnerText;

                    switch (field.GetAttribute("type")) {
                        case "TextBox":
                            if (values.Contains(name)) {
                                XmlHelper.AddAttribute(doc, field, "value", (string) values[name]);
                            }
                            break;
                        case "DropDownList":
                        case "RadioButtonList":
                            if (values.Contains(name)) {
                                var listitems = (XmlElement) field.SelectSingleNode("LISTITEMS");
                                var lList = listitems.GetElementsByTagName("LISTITEM");
                                for (int g = 0; g < lList.Count; g++) {
                                    var listItem = (XmlElement) lList[g];
                                    if (listItem.GetAttribute("value") != (string) values[name]) {
                                        continue;
                                    }
                                    var lProperties =
                                        (XmlElement) listItem.SelectSingleNode("PROPERTIES");
                                    lProperties.AppendChild(
                                        XmlHelper.CreateElement(doc, "PROPERTY", "name", "Selected", "True"));
                                    break;
                                }
                            }
                            break;
                        case "CheckBoxList": {
                            var listitems = (XmlElement) field.SelectSingleNode("LISTITEMS");
                            var lList = listitems.GetElementsByTagName("LISTITEM");
                            for (var g = 0; g < lList.Count; g++) {
                                if (request[name + ":" + g] == null) {
                                    continue;
                                }
                                var listItem = (XmlElement) lList[g];
                                var lProperties = (XmlElement) listItem.SelectSingleNode("PROPERTIES");
                                lProperties.AppendChild(
                                    XmlHelper.CreateElement(doc, "PROPERTY", "name", "Selected", "True"));
                            }
                        }
                            break;
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Loads values from request into xml document that describes the page.  This function
        /// simulates viewstate.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="request">The request to load from</param>
        /// <param name="doc">The document describing the page.  Values are loaded in to the document</param>
        /// <param name="pageName"></param>
        public static XmlDocument LoadRequestValuesIntoDocumentWithHashtable(
            Hashtable values, HttpRequest request, XmlDocument doc, string pageName) {
            //Hashtable values = GetAllRequestValues(request, doc, pageName);
            var list = doc.GetElementsByTagName("FIELD");
            for (int i = 0; i < list.Count; i++) {
                var field = (XmlElement) list[i];
                var properties = (XmlElement) field.SelectSingleNode("PROPERTIES");
                var pList = properties.GetElementsByTagName("PROPERTY");
                for (int j = 0; j < pList.Count; j++) {
                    if (((XmlElement) pList[j]).GetAttribute("name") != "ID") {
                        continue;
                    }
                    string name = (pList[j]).InnerText;

                    switch (field.GetAttribute("type")) {
                        case "TextBox":
                            if (values.Contains(name)) {
                                XmlHelper.AddAttribute(doc, field, "value", (string) values[name]);
                            }
                            break;
                        case "DropDownList":
                        case "RadioButtonList":
                            if (values.Contains(name)) {
                                var listitems = (XmlElement) field.SelectSingleNode("LISTITEMS");
                                var lList = listitems.GetElementsByTagName("LISTITEM");
                                for (int g = 0; g < lList.Count; g++) {
                                    var listItem = (XmlElement) lList[g];
                                    if (listItem.GetAttribute("value") != (string) values[name]) {
                                        continue;
                                    }
                                    var lProperties =
                                        (XmlElement) listItem.SelectSingleNode("PROPERTIES");
                                    lProperties.AppendChild(
                                        XmlHelper.CreateElement(doc, "PROPERTY", "name", "Selected", "True"));
                                    break;
                                }
                            }
                            break;
                        case "CheckBoxList": {
                            var listitems = (XmlElement) field.SelectSingleNode("LISTITEMS");
                            var lList = listitems.GetElementsByTagName("LISTITEM");
                            for (int g = 0; g < lList.Count; g++) {
                                if (request[name + ":" + g] == null) {
                                    continue;
                                }
                                var listItem = (XmlElement) lList[g];
                                var lProperties = (XmlElement) listItem.SelectSingleNode("PROPERTIES");
                                lProperties.AppendChild(
                                    XmlHelper.CreateElement(doc, "PROPERTY", "name", "Selected", "True"));
                            }
                        }
                            break;
                    }
                    break;
                }
            }
            return doc;
        }
    }
}