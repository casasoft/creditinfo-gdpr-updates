#region

using System.Collections;
using ValueObjects.NP;

#endregion

namespace BusinessLogic.Util.Comparer {
    /// <summary>
    /// Summary description for CaseComparer.
    /// </summary>
    public class CaseComparer : IComparer {
        /// <summary>
        /// Filed what is the list sorting by
        /// </summary>
        public string SortField { get; set; }

        /// <summary>
        /// ASC or DESC ways to order collection
        /// </summary>
        public string SortOrder { get; set; }

        #region IComparer Members

        /// <summary>
        /// Compare function
        /// </summary>
        /// <param name="x">First object</param>
        /// <param name="y">Second object</param>
        /// <returns></returns>
        public int Compare(object x, object y) {
            var xcase = (NPCase) x;
            var ycase = (NPCase) y;

            switch (SortField) {
                case ("CaseNumber"):
                    switch (xcase.CaseNumber.CompareTo(ycase.CaseNumber)) {
                        case -1:
                            return (SortOrder == "ASC") ? -1 : 1;
                        case 1:
                            return (SortOrder == "ASC") ? 1 : -1;
                        default:
                            return 0;
                    }

                case ("StatusId"):
                    if (xcase.StatusId < ycase.StatusId) {
                        return (SortOrder == "ASC") ? -1 : 1;
                    }
                    if (xcase.StatusId > ycase.StatusId) {
                        return (SortOrder == "ASC") ? 1 : -1;
                    }
                    return 0;

                case ("CaseTypeId"):
                    if (xcase.CaseTypeId < ycase.CaseTypeId) {
                        return (SortOrder == "ASC") ? -1 : 1;
                    }
                    if (xcase.CaseTypeId > ycase.CaseTypeId) {
                        return (SortOrder == "ASC") ? 1 : -1;
                    }
                    return 0;

                case ("InformationSourceId"):
                    if (xcase.InformationSourceId < ycase.InformationSourceId) {
                        return (SortOrder == "ASC") ? -1 : 1;
                    }
                    if (xcase.InformationSourceId > ycase.InformationSourceId) {
                        return (SortOrder == "ASC") ? 1 : -1;
                    }
                    return 0;
                case ("CreditorCreditInfoID"):
                    if (xcase.CreditorCreditInfoID < ycase.CreditorCreditInfoID) {
                        return (SortOrder == "ASC") ? -1 : 1;
                    }
                    if (xcase.CreditorCreditInfoID > ycase.CreditorCreditInfoID) {
                        return (SortOrder == "ASC") ? 1 : -1;
                    }

                    return 0;

                case ("DebtorCreditInfoID"):
                    if (xcase.DebtorCreditInfoID < ycase.DebtorCreditInfoID) {
                        return (SortOrder == "ASC") ? -1 : 1;
                    }
                    if (xcase.DebtorCreditInfoID > ycase.DebtorCreditInfoID) {
                        return (SortOrder == "ASC") ? 1 : -1;
                    }
                    return 0;

                case ("WarrantCreditInfoID"):
                    if (xcase.WarrantCreditInfoID < ycase.WarrantCreditInfoID) {
                        return (SortOrder == "ASC") ? -1 : 1;
                    }
                    if (xcase.WarrantCreditInfoID > ycase.WarrantCreditInfoID) {
                        return (SortOrder == "ASC") ? 1 : -1;
                    }
                    return 0;

                case ("CaseDate"):
                    if (xcase.CaseDate < ycase.CaseDate) {
                        return (SortOrder == "ASC") ? -1 : 1;
                    }
                    if (xcase.CaseDate > ycase.CaseDate) {
                        return (SortOrder == "ASC") ? 1 : -1;
                    }
                    return 0;

                default:
                    if (xcase.Id < ycase.Id) {
                        return (SortOrder == "ASC") ? -1 : 1;
                    }
                    if (xcase.Id > ycase.Id) {
                        return (SortOrder == "ASC") ? 1 : -1;
                    }
                    return 0;
            }
        }

        #endregion
    }
}