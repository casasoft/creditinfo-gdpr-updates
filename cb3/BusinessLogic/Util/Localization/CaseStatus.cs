#region

using ValueObjects.NP;

#endregion

namespace BusinessLogic.Util.Localization {
    /// <summary>
    /// Summary description for ICaseStatus.
    /// <para>Copyright (C) 2005 A�algeir �orgr�msson - Creditinfo Group ltd.</para>
    /// </summary>
    public class CaseStatus : ICaseStatus {
        #region ICaseStatus Members

        /// <summary>
        /// Sets the case status according to changes in the case object. Will most likely be different for each
        /// setup of the solution.
        /// </summary>
        /// <param name="myCase">The case to change status.</param>
        /// <returns>The case with new case status value.</returns>
        public INPCase SetCaseStatus(INPCase myCase) {
            // Do checks to the myCase object, change the object status as needed.
            return myCase;
        }

        #endregion
    }
}