#region

using System.Collections;
using ValueObjects.Base;
using ValueObjects.General;
using ValueObjects.NP;

#endregion

namespace BusinessLogic.Service.NP {
    /// <summary>
    /// Summary description for ILookupDataService.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ILookupDataService {
        IList GetCaseStatuses();
        IList GetInformationSources();
        IList GetCaseTypes();
        IList GetRelatedPartyTypes();
        ICigBaseLookupValueObject GetRelatedPartyType(int typeID);

        /// <summary>
        /// Retrieve all collateral value types
        /// </summary>
        /// <returns>IList of CollateralValueTypes</returns>
        IList GetCollateralValueTypes();

        /// <summary>
        /// Retrieve all collateral types
        /// </summary>
        /// <returns>IList of CollateralTypes</returns>
        IList GetCollateralTypes();

        /// <summary>
        /// Retrieve all cities
        /// </summary>
        /// <returns>IList of City</returns>
        IList GetCities();

        /// <summary>
        /// Retrieve all postcodes
        /// </summary>
        /// <returns>IList of Postcode</returns>
        IList GetPostcodes();

        /// <summary>
        /// Retrieve all countries
        /// </summary>
        /// <returns>IList of Country</returns>
        IList GetCountries();

        /// <summary>
        /// Retrieve concrete collateral value type
        /// </summary>
        /// <param name="valueTypeID">collateral value type unique identifier</param>
        /// <returns>concrete collateral value type</returns>
        ICigBaseLookupValueObject GetCollateralValueType(int valueTypeID);

        /// <summary>
        /// Retrieve concrete collateral type
        /// </summary>
        /// <param name="typeID">collateral type unique identifier</param>
        /// <returns>concrete collateral type</returns>
        ICollateralType GetCollateralType(string typeID);

        /// <summary>
        /// Retrieve concrete City
        /// </summary>
        /// <param name="cityID">city unique identifier</param>
        /// <returns>concrete city</returns>
        ICity GetCity(int cityID);

        /// <summary>
        /// Retrieve concrete postcode
        /// </summary>
        /// <param name="postcodeID">postcode unique identifier</param>
        /// <returns>concrete postcode</returns>
        IPostcode GetPostcode(int postcodeID);

        /// <summary>
        /// Retrive concrete country
        /// </summary>
        /// <param name="countryID">country unique identifier</param>
        /// <returns>concrete country</returns>
        ICountry GetCountry(int countryID);

        /// <summary>
        /// Retrives Case type.
        /// </summary>
        /// <param name="caseTypeID">Case type unique identifier.</param>
        /// <returns>The requested case type.</returns>
        ICaseType GetCaseType(int caseTypeID);
    }
}