#region

using System;
using System.Collections;
using ValueObjects.NP;

#endregion

namespace BusinessLogic.Service.NP {
    /// <summary>
    /// Summary description for ICaseService.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ICaseService {
        /// <summary>
        /// Returns all cases registered for the last given hours
        /// </summary>
        /// <param name="forTheLastHours">The hours to get cases for</param>
        /// <returns>List of cases that match the criteria</returns>
        IList GetCases(int forTheLastHours);

        /// <summary>
        /// Returns cases by status Id
        /// </summary>
        /// <returns>List of cases that match the criteria</returns>
        IList GetCasesByStatusId(int statusId);

        /// <summary>
        /// Returns the case with given id
        /// </summary>
        /// <param name="caseID">Id of the case to return</param>
        /// <returns>The case with given id</returns>
        INPCase GetCase(int caseID);

        /// <summary>
        /// Saves given case to the database
        /// </summary>
        /// <param name="theCase">The case to save</param>
        /// <returns>True if case saved successfully, false otherwise</returns>
        bool SaveCase(INPCase theCase);

        /// <summary>
        /// Saves given case to the database
        /// </summary>
        /// <param name="theCase">The case to save</param>
        /// <returns>True if case saved successfully, false otherwise</returns>
        bool SaveCaseWithDynamicFields(INPCase theCase);

        /// <summary>
        /// Searches for a given case in the database
        /// </summary>
        /// <param name="theCase">The case to search by</param>
        /// <param name="registeredFrom"></param>
        /// <param name="registeredTo"></param>
        /// <param name="updatedFrom"></param>
        /// <param name="updatedTo"></param>
        /// <param name="debtor"></param>
        /// <returns>List of cases that match the criteria</returns>
        IList SearchCase(
            INPCase theCase,
            DateTime registeredFrom,
            DateTime registeredTo,
            DateTime updatedFrom,
            DateTime updatedTo,
            int debtor);
    }
}