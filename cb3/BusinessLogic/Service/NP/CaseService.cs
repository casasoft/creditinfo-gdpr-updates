#region

using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using BusinessLogic.Service.General;
using Cig.Framework.Data.BusinessLogic.Service;
using CPI.BLL;
using CreditWatch.BLL;
using DataAccess.Persistant.General;
using DataAccess.Persistant.NP;
using ValueObjects.General;
using ValueObjects.NP;

#endregion

namespace BusinessLogic.Service.NP {
    /// <summary>
    /// Summary description for CaseService.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class CaseService : ServiceBase, ICaseService {
        private static CaseService service;
        private readonly ICaseProxy proxy;

        public CaseService() {
            proxy = (ICaseProxy) context.GetObject("CaseProxy");
            proxy = (ICaseProxy) context.GetObject("CaseProxy");
        }

        #region ICaseService Members

        /// <summary>
        /// Returns all cases registered for the last given hours
        /// </summary>
        /// <param name="forTheLastHours">The hours to get cases for</param>
        /// <returns>List of cases that match the criteria</returns>
        public IList GetCases(int forTheLastHours) { return proxy.GetCases(forTheLastHours); }

        /// <summary>
        /// Returns cases by status Id
        /// </summary>
        /// <param name="statusId">The status id to get cases for</param>
        /// <returns>List of cases that match the criteria</returns>
        public IList GetCasesByStatusId(int statusId) { return proxy.GetCasesByStatusId(statusId); }

        /// <summary>
        /// Returns the case with given id
        /// </summary>
        /// <param name="caseID">Id of the case to return</param>
        /// <returns>The case with given id</returns>
        public INPCase GetCase(int caseID) {
            INPCase myCase = proxy.GetCase(caseID);
            myCase.DynamicValues = proxy.GetDynamicValues(caseID);
            return myCase;
        }

        /// <summary>
        /// Saves given case to the database
        /// </summary>
        /// <param name="theCase">The case to save</param>
        /// <returns>True if case saved successfully, false otherwise</returns>
        public bool SaveCase(INPCase theCase) { return proxy.SaveCaseInTran(theCase); }

        /// <summary>
        /// Saves given case to the database
        /// </summary>
        /// <param name="theCase">The case to save</param>
        /// <returns>True if case saved successfully, false otherwise</returns>
        public bool SaveCaseWithDynamicFields(INPCase theCase) { return proxy.SaveCaseInTran(theCase); }

        /// <summary>
        /// Searches for a given case in the database
        /// </summary>
        /// <param name="theCase">The case to search by</param>
        /// <param name="registeredFrom"></param>
        /// <returns>List of cases that match the criteria</returns>
        public IList SearchCase(
            INPCase theCase,
            DateTime registeredFrom,
            DateTime registeredTo,
            DateTime updatedFrom,
            DateTime updatedTo,
            int debtor) { return proxy.SearchCase(theCase, registeredFrom, registeredTo, updatedFrom, updatedTo, debtor); }

        #endregion

        public static CaseService GetInstance() {
            if (service == null) {
                service = new CaseService();
            }

            return service;
        }

        /// <summary>
        /// Searches for a given case in the database
        /// </summary>
        /// <returns>List of cases that match the criteria</returns>
        public string SearchCase(
            int debtor, bool isCompany, bool nativeCult, CultureInfo ci, string applicationPath, ResourceManager rm) {
            INPCase caseObject = new NPCase();
            IList cases = proxy.SearchCase(
                caseObject, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, debtor);

            return cases.Count + CreateHTML(debtor, cases, isCompany, nativeCult, ci, applicationPath, rm);
        }

        private string CreateHTML(
            int debtor,
            IList cases,
            bool isCompany,
            bool nativeCult,
            CultureInfo ci,
            string applicationPath,
            ResourceManager rm) {
            var sbXml = new StringBuilder();
            sbXml.Append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
            sbXml.Append("<message>");

            //txtCase"Case:"
            //txtInformationSource"Information source:"
            //txtClaimOwner"Claim owner:"
            //txtDate"Date:"
            //txtClaimType"Claim type:"
            //txtAmount"Amount:"
            //txtCIReference"CI Reference:"
            //txtGuarantor"Guarantor:"

            var searchProxy = new SearchProxy();

            sbXml.Append(
                "<casesmainheader><![CDATA[" + rm.GetString("txtCourtInfoAndDefaultingDebts", ci) +
                "]]></casesmainheader>");
            sbXml.Append(
                "<recentenquiriesheader><![CDATA[" + rm.GetString("txtRecentEnquiries", ci) +
                "]]></recentenquiriesheader>");

            sbXml.Append("<caseheader><![CDATA[" + rm.GetString("txtCase", ci) + "]]></caseheader>");
            sbXml.Append(
                "<informationsourceheader><![CDATA[" + rm.GetString("txtInformationSource", ci) +
                "]]></informationsourceheader>");
            sbXml.Append("<claimownerheader><![CDATA[" + rm.GetString("txtClaimOwner", ci) + "]]></claimownerheader>");
            sbXml.Append("<dateheader>" + rm.GetString("txtDefaultDate", ci) + "</dateheader>");
            sbXml.Append("<claimtypeheader><![CDATA[" + rm.GetString("txtClaimType", ci) + "]]></claimtypeheader>");
            //sbXml.Append("<amountheader><![CDATA[" + rm.GetString("txtTotalAmount",ci) + "]]></amountheader>");
            sbXml.Append(
                "<cireferenceheader><![CDATA[" + rm.GetString("txtCIReference", ci) + "]]></cireferenceheader>");
            sbXml.Append("<paidupdateheader><![CDATA[" + rm.GetString("txtPaidUpDate", ci) + "]]></paidupdateheader>");
            sbXml.Append(
                "<guarantorheader><![CDATA[" + rm.GetString("lblGuarantorsHeader", ci) + "]]></guarantorheader>");
            sbXml.Append("<debtorheader><![CDATA[" + rm.GetString("lblDebtorHeader", ci) + "]]></debtorheader>");

            sbXml.Append(
                "<claimrelationheader><![CDATA[" + rm.GetString("lblClaimReleationHeader", ci) +
                "]]></claimrelationheader>");
            sbXml.Append(
                "<casefailingdateheader><![CDATA[" + rm.GetString("lblCaseFailingDateHeader", ci) +
                "]]></casefailingdateheader>");
            sbXml.Append(
                "<courtrulingdateheader><![CDATA[" + rm.GetString("lblCaseRuleDateHeader", ci) +
                "]]></courtrulingdateheader>");
            sbXml.Append("<PaidUpDateHeader><![CDATA[" + rm.GetString("txtPaidUpDate", ci) + "]]></PaidUpDateHeader>");
            sbXml.Append(
                "<PaidUpCasesHeader><![CDATA[" + rm.GetString("txtPaidUpCasesHeader", ci) + "]]></PaidUpCasesHeader>");
            sbXml.Append(
                "<RelationsHeader><![CDATA[" + rm.GetString("lblRelationsHeader", ci) + "]]></RelationsHeader>");

            //Headers for the collateral table				
            sbXml.Append("<collateralheader>" + rm.GetString("lblCollateralHeader", ci) + "</collateralheader>");
            sbXml.Append("<coltypeheader>" + rm.GetString("lblCollateralTypeHeader", ci) + "</coltypeheader>");
            sbXml.Append("<coladdressheader>" + rm.GetString("lblCollateralAddressHeader", ci) + "</coladdressheader>");
            sbXml.Append("<colcityheader>" + rm.GetString("lblCollateralCityHeader", ci) + "</colcityheader>");
            sbXml.Append("<colzipheader>" + rm.GetString("lblCollateralZipHeader", ci) + "</colzipheader>");
            sbXml.Append("<colcountryheader>" + rm.GetString("lblCollateralCountryHeader", ci) + "</colcountryheader>");
            sbXml.Append("<colvalueheader>" + rm.GetString("lblCollateralValueHeader", ci) + "</colvalueheader>");
            sbXml.Append("<colownerheader>" + rm.GetString("lblCollateralOwnerHeader", ci) + "</colownerheader>");

            //Headers for the amount table
            sbXml.Append(
                "<detailedclaiminformationheader>" + rm.GetString("lblDetailedClaimInformationHeader", ci) +
                "</detailedclaiminformationheader>");
            //sbXml.Append("<totalamountheader>" + rm.GetString("lblTotalAmountHeader",ci) + "</totalamountheader>");
            sbXml.Append(
                "<InitiallyDisbursedAmountheader><![CDATA[" + rm.GetString("txtInitiallyDisbursedAmount", ci) +
                "]]></InitiallyDisbursedAmountheader>");
            sbXml.Append(
                "<currentbalanceheader>" + rm.GetString("lblCurrentBalanceHeader", ci) + "</currentbalanceheader>");
            sbXml.Append(
                "<overdueprincipalheader>" + rm.GetString("lblOverduePrincipalHeader", ci) + "</overdueprincipalheader>");
            sbXml.Append(
                "<accruedinterestheader>" + rm.GetString("lblAccruedInterestHeader", ci) + "</accruedinterestheader>");
            sbXml.Append(
                "<accruedpenaltiesheader>" + rm.GetString("lblAccruedPenaltiesHeader", ci) + "</accruedpenaltiesheader>");

            //Headers for the Board Member
            sbXml.Append("<BoardMemberHeader>" + rm.GetString("lblBoardMembersHeader", ci) + "</BoardMemberHeader>");
            sbXml.Append(
                "<BoardMemberNameHeader>" + rm.GetString("lblCompanyInvolvementsNameHeader", ci) +
                "</BoardMemberNameHeader>");
            sbXml.Append(
                "<BoardMemberIdNumberHeader>" + rm.GetString("lblCompanyInvolvementsIdNumberHeader", ci) +
                "</BoardMemberIdNumberHeader>");
            //Headers for the Principals
            sbXml.Append("<PrincipalsHeader>" + rm.GetString("lblPrincipalsHeader", ci) + "</PrincipalsHeader>");
            sbXml.Append(
                "<PrincipalsNameHeader>" + rm.GetString("lblCompanyInvolvementsNameHeader", ci) +
                "</PrincipalsNameHeader>");
            sbXml.Append(
                "<PrincipalsIdNumberHeader>" + rm.GetString("lblCompanyInvolvementsIdNumberHeader", ci) +
                "</PrincipalsIdNumberHeader>");
            //Headers for the Shareholders
            sbXml.Append("<ShareholdersHeader>" + rm.GetString("lblShareholdersHeader", ci) + "</ShareholdersHeader>");
            sbXml.Append(
                "<ShareholdersNameHeader>" + rm.GetString("lblCompanyInvolvementsNameHeader", ci) +
                "</ShareholdersNameHeader>");
            sbXml.Append(
                "<ShareholdersIdNumberHeader>" + rm.GetString("lblCompanyInvolvementsIdNumberHeader", ci) +
                "</ShareholdersIdNumberHeader>");
            sbXml.Append(
                "<ShareholdersOwnershipPercentHeader>" +
                rm.GetString("lblCompanyInvolvementsOwnershipPercentHeader", ci) +
                "</ShareholdersOwnershipPercentHeader>");
            //Headers for the company involvements
            sbXml.Append(
                "<CompanyInvolvementsHeader>" + rm.GetString("lblCompanyInvolvementsHeader", ci) +
                "</CompanyInvolvementsHeader>");
            sbXml.Append(
                "<CompanyInvolvementsNameHeader>" + rm.GetString("lblCompanyInvolvementsNameHeader", ci) +
                "</CompanyInvolvementsNameHeader>");
            sbXml.Append(
                "<CompanyInvolvementsIdNumberHeader>" + rm.GetString("lblCompanyInvolvementsIdNumberHeader", ci) +
                "</CompanyInvolvementsIdNumberHeader>");
            sbXml.Append(
                "<CompanyInvolvementsOwnershipPercentHeader>" +
                rm.GetString("lblCompanyInvolvementsOwnershipPercentHeader", ci) +
                "</CompanyInvolvementsOwnershipPercentHeader>");

            var myFactory = new cwFactory();
            sbXml.Append(
                "<lookuplastmonthheader><![CDATA[" + rm.GetString("txtLastMonth", ci) + "]]></lookuplastmonthheader>");
            sbXml.Append(
                "<lookuplastmonth>" + myFactory.GetLogCountForLastXDaysPerCreditInfoUser(30, debtor) +
                "</lookuplastmonth>");
            sbXml.Append(
                "<lookuplast3monthsheader><![CDATA[" + rm.GetString("txtLast3Months", ci) +
                "]]></lookuplast3monthsheader>");
            sbXml.Append(
                "<lookuplast3months>" + myFactory.GetLogCountForLastXDaysPerCreditInfoUser(90, debtor) +
                "</lookuplast3months>");
            sbXml.Append(
                "<lookuplast6monthsheader><![CDATA[" + rm.GetString("txtLast6Months", ci) +
                "]]></lookuplast6monthsheader>");
            sbXml.Append(
                "<lookuplast6months>" + myFactory.GetLogCountForLastXDaysPerCreditInfoUser(180, debtor) +
                "</lookuplast6months>");

            //		this.lblOneMonth.Text = rm.GetString("lbLastMonth",ci) + "\t\t" + 
            //		this.lblThreeMonths.Text = rm.GetString("lbLast3Months",ci) + "\t\t" +
            //		this.lblSixMonths.Text = rm.GetString("lbLast6Months",ci) + "\t\t" +

            sbXml.Append(
                "<nocasesfoundtext><![CDATA[" + rm.GetString("txtNoCourtInfoAndDefaultingDebts", ci) +
                "]]></nocasesfoundtext>");

            sbXml.Append(
                "<NoBoardMembersFound><![CDATA[" + rm.GetString("lblNoBoardMembersFound", ci) +
                "]]></NoBoardMembersFound>");
            sbXml.Append(
                "<NoPrincipalsFound><![CDATA[" + rm.GetString("lblNoPrincipalsFound", ci) + "]]></NoPrincipalsFound>");
            sbXml.Append(
                "<NoShareholdersFound><![CDATA[" + rm.GetString("lblNoShareholdersFound", ci) +
                "]]></NoShareholdersFound>");
            sbXml.Append(
                "<NoInvolvementsFound><![CDATA[" + rm.GetString("lblNoInvolvementsFound", ci) +
                "]]></NoInvolvementsFound>");

            var cpiFactory = new CPIFactory();
            DataSet ds;

            if (isCompany) {
                sbXml.Append("<BoardMembers>");
                ds = cpiFactory.GetBoardPrincipalsAsDataSet(debtor, true);
                if (ds != null && ds.Tables[0] != null) {
                    foreach (DataRow row in ds.Tables[0].Rows) {
                        sbXml.Append("<BoardMember>");
                        sbXml.AppendFormat("<Ciid>{0}</Ciid>", row["CreditInfoID"]);
                        if (nativeCult) {
                            sbXml.AppendFormat(
                                "<Name>{0} {1}</Name>",
                                row["FirstNameNative"].ToString() != "" ? row["FirstNameNative"] : row["FirstNameEN"],
                                row["SurNameNative"].ToString() != "" ? row["SurNameNative"] : row["SurNameEN"]);
                        } else {
                            sbXml.AppendFormat(
                                "<Name>{0} {1}</Name>",
                                row["FirstNameEN"].ToString() != "" ? row["FirstNameEN"] : row["FirstNameNative"],
                                row["SurNameEN"].ToString() != "" ? row["SurNameEN"] : row["SurNameNative"]);
                        }
                        sbXml.AppendFormat("<IdNumber>{0}</IdNumber>", row["Number"]);
                        sbXml.Append("</BoardMember>");
                    }
                }
                sbXml.Append("</BoardMembers>");

                sbXml.Append("<Principals>");
                ds = cpiFactory.GetBoardPrincipalsAsDataSet(debtor, false);
                if (ds != null && ds.Tables[0] != null) {
                    foreach (DataRow row in ds.Tables[0].Rows) {
                        sbXml.Append("<Principal>");
                        sbXml.AppendFormat("<Ciid>{0}</Ciid>", row["CreditInfoID"]);
                        if (nativeCult) {
                            sbXml.AppendFormat(
                                "<Name>{0} {1}</Name>",
                                row["FirstNameNative"].ToString() != "" ? row["FirstNameNative"] : row["FirstNameEN"],
                                row["SurNameNative"].ToString() != "" ? row["SurNameNative"] : row["SurNameEN"]);
                        } else {
                            sbXml.AppendFormat(
                                "<Name>{0} {1}</Name>",
                                row["FirstNameEN"].ToString() != "" ? row["FirstNameEN"] : row["FirstNameNative"],
                                row["SurNameEN"].ToString() != "" ? row["SurNameEN"] : row["SurNameNative"]);
                        }
                        sbXml.AppendFormat("<IdNumber>{0}</IdNumber>", row["Number"]);
                        sbXml.Append("</Principal>");
                    }
                }
                sbXml.Append("</Principals>");

                sbXml.Append("<Shareholders>");
                ds = cpiFactory.GetShareHolderOwnerAsDataSet(debtor);
                var companyCreditInfoIDS = new Hashtable();
                if (ds != null && ds.Tables[0] != null) {
                    var dv = new DataView(
                        ds.Tables[0], "OwnerShip >= 10", "OwnerShip", DataViewRowState.CurrentRows);

                    foreach (DataRow row in dv.Table.Rows) {
                        //Since GetShareHolderOwnerAsDataSet returns multiple records depending on the addresses assigned to 
                        //the company owner I need to filter the return my self.
                        if (companyCreditInfoIDS.Contains(row["CreditInfoID"])) {
                            continue;
                        }
                        companyCreditInfoIDS.Add(row["CreditInfoID"], row["CreditInfoID"]);

                        sbXml.Append("<Shareholder>");

                        if (row["NameNative"] == DBNull.Value && row["NameEN"] == DBNull.Value) {
                            if (nativeCult) {
                                sbXml.AppendFormat(
                                    "<Name>{0} {1}</Name>",
                                    row["FirstNameNative"].ToString() != ""
                                        ? row["FirstNameNative"]
                                        : row["FirstNameEN"],
                                    row["SurNameNative"].ToString() != "" ? row["SurNameNative"] : row["SurNameEN"]);
                            } else {
                                sbXml.AppendFormat(
                                    "<Name>{0} {1}</Name>",
                                    row["FirstNameEN"].ToString() != "" ? row["FirstNameEN"] : row["FirstNameNative"],
                                    row["SurNameEN"].ToString() != "" ? row["SurNameEN"] : row["SurNameNative"]);
                            }
                        } else {
                            if (nativeCult) {
                                sbXml.AppendFormat(
                                    "<Name>{0}</Name>",
                                    row["NameNative"].ToString() != "" ? row["NameNative"] : row["NameEN"]);
                            } else {
                                sbXml.AppendFormat(
                                    "<Name>{0}</Name>",
                                    row["NameEN"].ToString() != "" ? row["NameEN"] : row["NameNative"]);
                            }
                        }
                        sbXml.AppendFormat(
                            "<OwnershipPercent>{0} %</OwnershipPercent>", Convert.ToInt32(row["ownership_perc"]));
                        sbXml.AppendFormat("<IdNumber>{0}</IdNumber>", row["Number"]);

                        sbXml.Append("</Shareholder>");
                    }
                }
                sbXml.Append("</Shareholders>");

                sbXml.Append("<CompanyInvolvements>");
                ds = cpiFactory.GetSubsidiariesAsDataSet(debtor);
                if (ds != null && ds.Tables[0] != null) {
                    foreach (DataRow row in ds.Tables[0].Rows) {
                        sbXml.Append("<CompanyInvolvement>");

                        if (nativeCult) {
                            sbXml.AppendFormat(
                                "<Name>{0}</Name>",
                                row["NameNative"].ToString() != "" ? row["NameNative"] : row["NameEN"]);
                        } else {
                            sbXml.AppendFormat(
                                "<Name>{0}</Name>", row["NameEN"].ToString() != "" ? row["NameEN"] : row["NameNative"]);
                        }
                        sbXml.AppendFormat("<Ownership>{0} %</Ownership>", row["ownership"]);
                        sbXml.AppendFormat("<IdNumber>{0}</IdNumber>", row["Number"]);

                        sbXml.Append("</CompanyInvolvement>");
                    }
                }
                sbXml.Append("</CompanyInvolvements>");
            } else {
                sbXml.Append("<CompanyInvolvements>");
                ds = cpiFactory.GetInvolvements(debtor);
                if (ds != null && ds.Tables[0] != null) {
                    foreach (DataRow row in ds.Tables[0].Rows) {
                        sbXml.Append("<CompanyInvolvement>");

                        sbXml.AppendFormat("<Name>{0}</Name>", nativeCult ? row["NameNative"] : row["NameEN"]);
                        sbXml.AppendFormat("<Ownership>{0} %</Ownership>", row["ownership_perc"]);
                        sbXml.AppendFormat("<IdNumber>{0}</IdNumber>", row["Number"]);

                        sbXml.Append("</CompanyInvolvement>");
                    }
                }
                sbXml.Append("</CompanyInvolvements>");
            }

            foreach (INPCase aCase in cases) {
                INPCase myCase = GetCase(aCase.Id);
                if (myCase.StatusId == 3) {
                    sbXml.AppendFormat("<PaidUpCase id=\"{0}\">", myCase.CaseNumber);
                    sbXml.Append("<paidup>True</paidup>");
                    sbXml.Append("<paidupdate><![CDATA[" + myCase.DynamicValues["PaidUpDate"] + "]]></paidupdate>");
                } else {
                    sbXml.AppendFormat("<case id=\"{0}\">", myCase.CaseNumber);
                    sbXml.Append("<paidup>False</paidup>");
                }

                if (myCase.CaseTypeId == 7 || myCase.CaseTypeId == 9) {
                    sbXml.Append(
                        "<amountheader><![CDATA[" + rm.GetString("txtTotalClaimAmount", ci) + "]]></amountheader>");
                } else {
                    sbXml.Append(
                        "<amountheader><![CDATA[" + rm.GetString("txtTotalDueAmount", ci) + "]]></amountheader>");
                }

                if (nativeCult) {
                    if (myCase.InformationSource.NameNative.Length > 0) {
                        sbXml.Append(
                            "<informationsource><![CDATA[" + myCase.InformationSource.NameNative +
                            "]]></informationsource>");
                    } else {
                        sbXml.Append(
                            "<informationsource><![CDATA[" + myCase.InformationSource.NameEN + "]]></informationsource>");
                    }
                    if (myCase.CaseType.NameNative.Length > 0) {
                        sbXml.Append("<claimtype><![CDATA[" + myCase.CaseType.NameNative + "]]></claimtype>");
                    } else {
                        sbXml.Append("<claimtype><![CDATA[" + myCase.CaseType.NameEN + "]]></claimtype>");
                    }
                } else {
                    if (myCase.InformationSource.NameEN.Length > 0) {
                        sbXml.Append(
                            "<informationsource><![CDATA[" + myCase.InformationSource.NameEN + "]]></informationsource>");
                    } else {
                        sbXml.Append(
                            "<informationsource><![CDATA[" + myCase.InformationSource.NameNative +
                            "]]></informationsource>");
                    }
                    if (myCase.CaseType.NameEN.Length > 0) {
                        sbXml.Append("<claimtype><![CDATA[" + myCase.CaseType.NameEN + "]]></claimtype>");
                    } else {
                        sbXml.Append("<claimtype><![CDATA[" + myCase.CaseType.NameNative + "]]></claimtype>");
                    }
                }

                /*					foreach(RelatedParty myParty in myCase.RelatedParties)
									{
										if(myParty.TypeId == 2)
											sbXml.Append("<claimowner><![CDATA[" + myParty.EntityName + "]]></claimowner>");
									}*/

                foreach (RelatedParty myParty in myCase.RelatedParties) {
                    if (myParty.TypeId == 2) {
                        string name = "";

                        ISearchService service1 = new SearchService();

                        try {
                            name = service1.FindIndividualByCreditinfoId(myParty.CreditinfoId).NameNative;
                        } catch {
                            name = service1.FindCompanyByCreditinfoId(myParty.CreditinfoId).NameNative;
                        }
                        sbXml.Append("<claimowner><![CDATA[" + name + "]]></claimowner>");
                    }
                }

                if (myCase.DynamicValues.Count > 0) {
                    //TODO:  Only show this in case type 7.  Other wise show the court date
                    if (myCase.CaseTypeId == 7 || myCase.CaseTypeId == 9) {
                        sbXml.Append("<date>" + myCase.DynamicValues["DefaultDate"] + "</date>");
                    } else {
                        sbXml.Append(
                            "<casefailingdate>" + myCase.DynamicValues["CaseFailingDate"] + "</casefailingdate>");
                        sbXml.Append(
                            "<courtrulingdate>" + myCase.DynamicValues["CourtRulingDate"] + "</courtrulingdate>");
                    }
                }
                try {
                    //Total balance in debt. GE RF3b
                    decimal currentBalance = 0;
                    if (myCase.DynamicValues["CurrentBalance"] != null &&
                        myCase.DynamicValues["CurrentBalance"].ToString() != "") {
                        currentBalance =
                            Convert.ToDecimal(
                                ((string) myCase.DynamicValues["CurrentBalance"]).Replace(
                                    ".", ci.NumberFormat.CurrencyDecimalSeparator));
                    }

                    decimal overduePrincipal = 0;
                    if (myCase.DynamicValues["OverduePrincipalAmount"] != null &&
                        myCase.DynamicValues["OverduePrincipalAmount"].ToString() != "") {
                        overduePrincipal =
                            Convert.ToDecimal(
                                ((string) myCase.DynamicValues["OverduePrincipalAmount"]).Replace(
                                    ".", ci.NumberFormat.CurrencyDecimalSeparator));
                    }

                    decimal accruedInterest = 0;
                    if (myCase.DynamicValues["AccruedInterest"] != null &&
                        myCase.DynamicValues["AccruedInterest"].ToString() != "") {
                        accruedInterest =
                            Convert.ToDecimal(
                                ((string) myCase.DynamicValues["AccruedInterest"]).Replace(
                                    ".", ci.NumberFormat.CurrencyDecimalSeparator));
                    }

                    decimal accruedPenalties = 0;
                    if (myCase.DynamicValues["AccruedPenalties"] != null &&
                        myCase.DynamicValues["AccruedPenalties"].ToString() != "") {
                        accruedPenalties =
                            Convert.ToDecimal(
                                ((string) myCase.DynamicValues["AccruedPenalties"]).Replace(
                                    ".", ci.NumberFormat.CurrencyDecimalSeparator));
                    }

                    decimal overduePrincipalAmount = 0;
                    if (myCase.DynamicValues["OverduePrincipalAmount"] != null &&
                        myCase.DynamicValues["OverduePrincipalAmount"].ToString() != "") {
                        overduePrincipalAmount =
                            Convert.ToDecimal(
                                ((string) myCase.DynamicValues["OverduePrincipalAmount"]).Replace(
                                    ".", ci.NumberFormat.CurrencyDecimalSeparator));
                    }

                    decimal totalBalance = 0;
                    switch (myCase.CaseTypeId) {
                        case 7:
                            totalBalance = currentBalance + accruedInterest + accruedPenalties;
                            sbXml.Append(
                                "<InitiallyDisbursedAmount><![CDATA[" + myCase.DynamicValues["TotalAmount"] +
                                "]]></InitiallyDisbursedAmount>");
                            sbXml.Append("<amount><![CDATA[" + totalBalance + "]]></amount>");
                            sbXml.Append("<currentbalance><![CDATA[" + currentBalance + "]]></currentbalance>");
                            sbXml.Append("<overdueprincipal><![CDATA[" + overduePrincipal + "]]></overdueprincipal>");
                            sbXml.Append("<accruedinterest><![CDATA[" + accruedInterest + "]]></accruedinterest>");
                            sbXml.Append("<accruedpenalties><![CDATA[" + accruedPenalties + "]]></accruedpenalties>");
                            break;
                        case 9:
                            totalBalance = int.Parse(myCase.DynamicValues["TotalAmount"].ToString());
                            sbXml.Append("<amount><![CDATA[" + totalBalance + "]]></amount>");
                            sbXml.Append(
                                "<accruedpenalties_t" + myCase.CaseTypeId + "><![CDATA[" + accruedPenalties +
                                "]]></accruedpenalties_t" + myCase.CaseTypeId + ">");
                            break;
                        default:
                            totalBalance = overduePrincipalAmount;
                            sbXml.Append("<amount><![CDATA[" + totalBalance + "]]></amount>");
                            break;
                    }
                } catch (Exception ex) {
                    sbXml.Append("<amount>" + ex.Message + "</amount>");
                }
                try {
                    sbXml.Append("<currency><![CDATA[" + myCase.DynamicValues["Currency"] + "]]></currency>");
                } catch (Exception) {
                    sbXml.Append("<currency></currency>");
                }

                sbXml.Append("<cireference>" + myCase.Id + "</cireference>");

                bool isGuarantor = false;

                //This is the debtor of the claim we are viewing.  Show the guarantors for the claim

                string caseRelation = "";
                foreach (RelatedParty myParty in myCase.RelatedParties) {
                    if (debtor == myParty.CreditinfoId) {
                        caseRelation = nativeCult ? myParty.TypeNameNative : myParty.TypeNameEN;
                    }

                    if (myParty.TypeId == 4) {
                        if (debtor == myParty.CreditinfoId) {
                            //We are looking at the guarantor of the claim							
                            isGuarantor = true;
                        }
                    }

                    sbXml.Append("<RelatedParty>");
                    sbXml.AppendFormat("<Name>{0}</Name>", myParty.EntityName);
                    sbXml.AppendFormat("<Type>{0}</Type>", nativeCult ? myParty.TypeNameNative : myParty.TypeNameEN);
                    if (myParty.CigEntity.IDNumbers != null && myParty.CigEntity.IDNumbers.Count > 0) {
                        var idNumber = myParty.CigEntity.IDNumbers[0] as IIDNumber;
                        sbXml.AppendFormat("<IdNumber>{0}</IdNumber>", idNumber.Number);
                    }
                    if (myParty.CreditinfoId == debtor) {
                        sbXml.Append("<IsSearched>True</IsSearched>");
                    }
                    sbXml.Append("</RelatedParty>");
                }

                sbXml.Append("<claimrelation><![CDATA[" + caseRelation + "]]></claimrelation>");

                if (!isGuarantor) {
                    foreach (RelatedParty myParty in myCase.RelatedParties) {
                        if (myParty.TypeId != 4) {
                            continue;
                        }
                        var id = "";

                        if (myParty.CigEntity.IDNumbers != null && myParty.CigEntity.IDNumbers.Count > 0) {
                            var idNumber = myParty.CigEntity.IDNumbers[0] as IIDNumber;
                            id = idNumber.Number;
                        }

                        sbXml.Append(
                            "<guarantor id=\"" + id + "\"><![CDATA[" + myParty.EntityName + "]]></guarantor>");
                    }
                } else {
                    //This is the creditor of the claim we are displaying.  Show only the debtors
                    foreach (RelatedParty myParty in myCase.RelatedParties) {
                        if (myParty.TypeId != 1) {
                            continue;
                        }
                        string id = "";
                        if (myParty.CigEntity.IDNumbers != null && myParty.CigEntity.IDNumbers.Count > 0) {
                            var idNumber = myParty.CigEntity.IDNumbers[0] as IIDNumber;
                            id = idNumber.Number;
                        }

                        sbXml.Append("<debtor id=\"" + id + "\"><![CDATA[" + myParty.EntityName + "]]></debtor>");
                    }
                }

                foreach (ICollateral collateral in  myCase.Collaterals) {
                    sbXml.Append("<collateral>");

                    sbXml.AppendFormat("<coltypeid><![CDATA[{0}]]></coltypeid>", collateral.CollateralTypeID);
                        //nativeCult ? collateral.CollateralTypeNameNative : collateral.CollateralTypeNameEN);
                    sbXml.AppendFormat("<coladdress><![CDATA[{0}]]></coladdress>", collateral.Address);
                    sbXml.AppendFormat(
                        "<colcity><![CDATA[{0}]]></colcity>",
                        nativeCult ? collateral.AddressCityNameNative : collateral.AddressCityNameEN);
                    sbXml.AppendFormat(
                        "<colzip><![CDATA[{0}]]></colzip>",
                        nativeCult ? collateral.AddressZipNameNative : collateral.AddressZipNameEN);
                    sbXml.AppendFormat(
                        "<colcountry><![CDATA[{0}]]></colcountry>",
                        nativeCult ? collateral.AddressCountryNameNative : collateral.AddressCountryNameEN);
                    sbXml.AppendFormat(
                        "<colvalue>{0}</colvalue>", collateral.MarketValue.ToString("###,###.##", ci.NumberFormat));

                    string ownerName = "NaN";
                    string id = "Nan";
                    try {
                        ICompany company = searchProxy.FindCompanyByCreditinfoId(collateral.Owner);
                        ownerName = nativeCult ? company.NameNative : company.NameEN;
                        if (company.IDNumbers != null && company.IDNumbers.Count > 0) {
                            var idNumber = company.IDNumbers[0] as IIDNumber;
                            id = idNumber.Number;
                        }
                    } catch (Exception) {
                        try {
                            //This wasn't a company.  Lookup in the individual table
                            var individual = searchProxy.FindIndividualByCreditinfoId(collateral.Owner);
                            if (individual != null) {
                                ownerName = nativeCult ? individual.NameNative : individual.NameEN;
                                if (individual.IDNumbers != null && individual.IDNumbers.Count > 0) {
                                    var idNumber = individual.IDNumbers[0] as IIDNumber;
                                    id = idNumber.Number;
                                }
                            }
                        } catch (Exception) {}
                    }

                    sbXml.AppendFormat("<colowner>{0} ({1})</colowner>", ownerName, id);
                    sbXml.Append("</collateral>");
                }

                if (myCase.StatusId == 3) {
                    sbXml.Append("</PaidUpCase>");
                } else {
                    sbXml.Append("</case>");
                }
            }

            sbXml.Append("</message>");

            return Transform(sbXml.ToString(), applicationPath);
        }

        /// <summary>
        /// Transforms a XML string with a spcific XSL file
        /// </summary>
        /// <param name="sXml">The XML that is to be transformed</param>
        /// <param name="applicationPath">Path of the XSL file</param>
        /// <returns>Transformation results in a string</returns>
        private string Transform(string sXml, string applicationPath) //, string sXsl)
        {
            var xmlDoc = new XmlDocument();
            sXml = sXml.Replace("&", "&amp;");
            sXml = sXml.Replace("\r\n", "\r\n&lt;br/&gt;");
            sXml = sXml.Replace("&", "&amp;");
            xmlDoc.LoadXml(sXml);

            string sReturn = Transform(xmlDoc, applicationPath);

            sReturn = sReturn.Replace("&amp;", "&");
            sReturn = sReturn.Replace("&lt;br&gt;", "<br/>\r\n");
            sReturn = sReturn.Replace("&lt;br/&gt;", "<br/>\r\n");
            sReturn = sReturn.Replace("&lt;I&gt;", "<i>");
            sReturn = sReturn.Replace("&lt;i&gt;", "<i>");
            sReturn = sReturn.Replace("&lt;/I&gt;", "</i>");
            sReturn = sReturn.Replace("&lt;/i&gt;", "</i>");
            sReturn = sReturn.Replace("&lt;P&gt;", "<p>");
            sReturn = sReturn.Replace("&lt;p&gt;", "<p>");
            sReturn = sReturn.Replace("&lt;/P&gt;", "</p>");
            sReturn = sReturn.Replace("&lt;/p&gt;", "</p>");

            sReturn = sReturn.Replace("utf-16", "iso-8859-1");

            return sReturn;
        }

        /// <summary>
        /// Transforms a XmlDocument with a specific XSL file
        /// </summary>
        /// <param name="xmlDoc">The XML document</param>
        /// <param name="applicationPath"></param>
        /// <returns>Transformation results in a string</returns>
        private static string Transform(IXPathNavigable xmlDoc, string applicationPath) //, string sXsl)
        {
            var xslTran = new XslTransform();

            //H�r �arf a� s�kja sl�� �r app.config
            //string xslPath = CigConfig.Configure("lookupsettings.xslPath"];
            string xslPath = applicationPath;

            if (!string.IsNullOrEmpty(xslPath)) {
                xslTran.Load(xslPath);
            }

            var objStream = new StringWriter();
            xslTran.Transform(xmlDoc, null, objStream, null);

            return objStream.ToString();
        }
    }
}