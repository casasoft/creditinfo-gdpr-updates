#region

using System.Collections;
using Cig.Framework.Data.BusinessLogic.Service;
using DataAccess.Persistant.NP;
using ValueObjects.Base;
using ValueObjects.General;
using ValueObjects.NP;

#endregion

namespace BusinessLogic.Service.NP {
    /// <summary>
    /// Summary description for LookupDataService.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class LookupDataService : ServiceBase, ILookupDataService {
        private static LookupDataService service;
        private readonly ILookupDataProxy proxy;
        public LookupDataService() { proxy = (ILookupDataProxy) context.GetObject("LookupDataProxy"); }

        #region ILookupDataService Members

        public IList GetCaseStatuses() { return proxy.GetCaseStatuses(); }
        public IList GetInformationSources() { return proxy.GetInformationSources(); }
        public IList GetCaseTypes() { return proxy.GetCaseTypes(); }
        public IList GetRelatedPartyTypes() { return proxy.GetRelatedPartyTypes(); }
        public ICigBaseLookupValueObject GetRelatedPartyType(int typeID) { return proxy.GetRelatedPartyType(typeID); }

        /// <summary>
        /// Retrieve all collateral value types
        /// </summary>
        /// <returns>IList of CollateralValueTypes</returns>
        public IList GetCollateralValueTypes() { return proxy.GetCollateralValueTypes(); }

        /// <summary>
        /// Retrieve all collateral types
        /// </summary>
        /// <returns>IList of CollateralTypes</returns>
        public IList GetCollateralTypes() { return proxy.GetCollateralTypes(); }

        /// <summary>
        /// Retrieve all cities
        /// </summary>
        /// <returns>IList of City</returns>
        public IList GetCities() { return proxy.GetCities(); }

        /// <summary>
        /// Retrieve all postcodes
        /// </summary>
        /// <returns>IList of Postcode</returns>
        public IList GetPostcodes() { return proxy.GetPostcodes(); }

        /// <summary>
        /// Retrieve all countries
        /// </summary>
        /// <returns>IList of Country</returns>
        public IList GetCountries() { return proxy.GetCountries(); }

        /// <summary>
        /// Retrieve concrete collateral value type
        /// </summary>
        /// <param name="valueTypeID">collateral value type unique identifier</param>
        /// <returns>concrete collateral value type</returns>
        public ICigBaseLookupValueObject GetCollateralValueType(int valueTypeID) { return proxy.GetCollateralValueType(valueTypeID); }

        /// <summary>
        /// Retrieve concrete collateral type
        /// </summary>
        /// <param name="typeID">collateral type unique identifier</param>
        /// <returns>concrete collateral type</returns>
        public ICollateralType GetCollateralType(string typeID) { return proxy.GetCollateralType(typeID); }

        /// <summary>
        /// Retrieve concrete City
        /// </summary>
        /// <param name="cityID">city unique identifier</param>
        /// <returns>concrete city</returns>
        public ICity GetCity(int cityID) { return proxy.GetCity(cityID); }

        /// <summary>
        /// Retrieve concrete postcode
        /// </summary>
        /// <param name="postcodeID">postcode unique identifier</param>
        /// <returns>concrete postcode</returns>
        public IPostcode GetPostcode(int postcodeID) { return proxy.GetPostcode(postcodeID); }

        /// <summary>
        /// Retrive concrete country
        /// </summary>
        /// <param name="countryID">country unique identifier</param>
        /// <returns>concrete country</returns>
        public ICountry GetCountry(int countryID) { return proxy.GetCountry(countryID); }

        /// <summary>
        /// Retrives Case type.
        /// </summary>
        /// <param name="caseTypeID">Case type unique identifier.</param>
        /// <returns>The requested case type.</returns>
        public ICaseType GetCaseType(int caseTypeID) { return proxy.GetCaseType(caseTypeID); }

        #endregion

        public static LookupDataService GetInstance() {
            if (service == null) {
                service = new LookupDataService();
            }

            return service;
        }
    }
}