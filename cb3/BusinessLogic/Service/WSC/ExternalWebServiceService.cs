#region

using Cig.Framework.Data.BusinessLogic.Service;
using DataAccess.Persistant.WSC;
using ValueObjects.WSC;

#endregion

namespace BusinessLogic.Service.WSC {
    /// <summary>
    /// Summary description for ExternalWebServiceService.
    /// <para>Copyright (C) 2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
    /// </summary>
    public class ExternalWebServiceService : ServiceBase, IExternalWebServiceService {
        /// <summary> The proxy to use </summary>
        private IExternalWebServiceProxy proxy;

        #region IExternalWebServiceService Members

        /// <summary>
        /// Returns the web service with the given Id.
        /// </summary>
        /// <param name="webServiceId">Unique identifier of the web service to return</param>
        /// <returns>The web service with given unique identifier</returns>		
        public IExternalWebService Get(int webServiceId) { return GetProxy().Get(webServiceId); }

        /// <summary>
        /// Returns the web service with the specified short code.
        /// </summary>
        /// <param name="shortCode">Short code of the web service.</param>
        /// <returns>The web service with given short code.</returns>
        public IExternalWebService Get(string shortCode) { return GetProxy().Get(shortCode); }

        /// <summary>
        /// Saves external web service to the database.
        /// </summary>
        /// <param name="webService">IExternalWebService instance of the web service to save.</param>
        /// <returns>True if successfully saved otherwise false.</returns>
        public bool Save(IExternalWebService webService) { return GetProxy().Save(webService); }

        /// <summary>
        /// Deletes web service with the specific unique identifier.
        /// </summary>
        /// <param name="webServiceId">Unique identi</param>
        /// <returns>True if the delete is successful.</returns>
        public bool Delete(int webServiceId) { return GetProxy().Delete(webServiceId); }

        #endregion

        /// <summary>
        /// Returns the external web service lookup proxy to use.
        /// </summary>
        /// <returns>The external web service lookup proxy to use.</returns>
        private IExternalWebServiceProxy GetProxy() {
            if (proxy == null) {
                proxy = (IExternalWebServiceProxy) context.GetObject("ExternalWebServiceProxy");
            }
            return proxy;
        }
    }
}