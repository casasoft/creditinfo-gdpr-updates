#region

using System;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Web;
using System.Xml;
using System.Xml.Xsl;
using Cig.Framework.Base.Attributes.Configuration;
using Cig.Framework.Data.BusinessLogic.Service;
using DataAccess.Persistant.WSC;
using Logging.BLL;
using ValueObjects.WSC;

#endregion

namespace BusinessLogic.Service.WSC {
    /// <summary>
    /// Summary description for NationalRegistryService.
    /// </summary>
    public class LtWebService : ServiceBase, ILtWebService {
        /// <summary> The proxy to use </summary>
        private ILtWebServiceProxy proxy;

        [ConfigReadable("Application.General.WebService.XsltPath", "")] private string xsltPath = "";

        #region ILtWebService Members

        /// <summary>
        /// Gets string feed from LT web service.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="webServiceId">Unique identifier for a web service.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <param name="XsltFilename">Name of the xslt file to transform the output.</param>
        /// <param name="rm">The resource manager to use.</param>
        /// <param name="ci">The culture info to use.</param>
        /// <returns>String containing information from registry.</returns>
        public string GetRequestWebService(
            IAuthenticator authenticator,
            int webServiceId,
            string query,
            string user,
            string XsltFilename,
            ResourceManager rm,
            CultureInfo ci) {
            if (webServiceId == 1) {
                return GetNationalRegistryInformation(authenticator, query, user, XsltFilename, rm, ci);
            } else if (webServiceId == 2) {
                return GetDebtRegistryInformation(authenticator, query, user, XsltFilename, rm, ci);
            }

            Logger.WriteToLog("WebServiceId(" + webServiceId + ") not recognized.", false);
            return null;
        }

        /// <summary>
        /// Gets information about an individual/company from national regstry.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <param name="XsltFilename">Name of the xslt file to transform the output.</param>
        /// <param name="rm">Resource manager to use.</param>
        /// <param name="ci">Current culture info.</param>
        /// <returns>String containing the information for national registry.</returns>
        public string GetNationalRegistryInformation(
            IAuthenticator authenticator,
            string query,
            string user,
            string XsltFilename,
            ResourceManager rm,
            CultureInfo ci) {
            XmlDocument doc = GetProxy().GetNationalRegistryInformation(authenticator, query, user);
            string[] transStrings = new[] {"lbNationalRegistry", "lbName", "lbSSNo", "lbAddress", "lbPostcode"};
            GetTranslations(ref doc, rm, ci, transStrings);
            return Transform(doc, XsltFilename);
        }

        /// <summary>
        /// Gets information about debts.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <param name="XsltFilename">Name of the xslt file to transform the output.</param>
        /// <param name="rm">Resource manager to use.</param>
        /// <param name="ci">Current culture info.</param>
        /// <returns>XmlDocument containing the information from debt registry.</returns>
        public string GetDebtRegistryInformation(
            IAuthenticator authenticator,
            string query,
            string user,
            string XsltFilename,
            ResourceManager rm,
            CultureInfo ci) {
            XmlDocument doc = GetProxy().GetDebtRegistryInformation(authenticator, query, user);
            string[] transStrings = new[]
                                    {
                                        "lbNationalRegistry", "lbDebtRegistry", "lbName", "lbSSNo", "lbType", "txtCase",
                                        "lbClaimDate", "lbCapitalAmount", "lbOffice"
                                    };
            GetTranslations(ref doc, rm, ci, transStrings);
            return Transform(doc, XsltFilename);
        }

        #endregion

        /// <summary>
        /// Returns the national registry lookup proxy to use.
        /// </summary>
        /// <returns>The national registry lookup proxy to use.</returns>
        private ILtWebServiceProxy GetProxy() {
            if (proxy == null) {
                proxy = (ILtWebServiceProxy) context.GetObject("LtWebServiceProxy");
            }
            return proxy;
        }

        /// <summary>
        /// Adds translation strings into the xml document.
        /// </summary>
        /// <param name="doc">The document to add to.</param>
        /// <param name="rm">The resource manager to use.</param>
        /// <param name="ci">The culture info to use.</param>
        /// <param name="translationStrings">Translations string to add.</param>
        private void GetTranslations(
            ref XmlDocument doc, ResourceManager rm, CultureInfo ci, string[] translationStrings) {
            XmlElement translations = doc.CreateElement("Translations");

            XmlNode root = doc.DocumentElement;

            foreach (string translationString in translationStrings) {
                XmlElement data = doc.CreateElement("Data");
                data.SetAttribute("key", translationString);
                data.InnerText = rm.GetString(translationString, ci);
                translations.AppendChild(data);
            }

            root.AppendChild(translations);
        }

        /// <summary>
        /// Transforms the given xml document.
        /// </summary>
        /// <param name="doc">The document to transform</param>
        /// <param name="XsltFilename">The xsl file use for transformation.</param>
        /// <returns>The transformed document as string.</returns>
        private string Transform(XmlDocument doc, string XsltFilename) {
            XslTransform xslTran = new XslTransform();
            try {
                xslTran.Load(HttpContext.Current.Server.MapPath(xsltPath + XsltFilename));
            } catch (Exception ex) {
                Logger.WriteToLog("Couldn't open XSL", ex, true);
            }
            StringWriter objStream = new StringWriter();
            xslTran.Transform(doc, null, objStream, null);

            return objStream.ToString();
        }
    }
}