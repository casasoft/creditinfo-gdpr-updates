#region

using ValueObjects.WSC;

#endregion

namespace BusinessLogic.Service.WSC {
    /// <summary>
    /// Summary description for IExternalWebServiceService.
    /// <para>Copyright (C) 2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IExternalWebServiceService {
        /// <summary>
        /// Returns the web service with the given Id.
        /// </summary>
        /// <param name="webServiceId">Unique identifier of the web service to return</param>
        /// <returns>The web service with given unique identifier</returns>
        IExternalWebService Get(int webServiceId);

        /// <summary>
        /// Returns the web service with the specified short code.
        /// </summary>
        /// <param name="shortCode">Short code of the web service.</param>
        /// <returns>The web service with given short code.</returns>
        IExternalWebService Get(string shortCode);

        /// <summary>
        /// Saves external web service to the database.
        /// </summary>
        /// <param name="webService">IExternalWebService instance of the web service to save.</param>
        /// <returns>True if successfully saved otherwise false.</returns>
        bool Save(IExternalWebService webService);

        /// <summary>
        /// Deletes web service with the specific unique identifier.
        /// </summary>
        /// <param name="webServiceId">Unique identi</param>
        /// <returns>True if the delete is successful.</returns>
        bool Delete(int webServiceId);
    }
}