#region

using System.Collections;
using ValueObjects.WSC;

#endregion

namespace BusinessLogic.Service.WSC {
    /// <summary>
    /// Summary description for IExternalWebServiceAccessService.
    /// <para>Copyright (C) 2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IExternalWebServiceAccessService {
        /// <summary>
        /// Returns the web service access with the given primary key.
        /// </summary>
        /// <param name="webServiceId">Unique identifier of the web service.</param>
        /// <param name="userId">Unique identifier of the user.</param>
        /// <returns>The access object with the given primary key.</returns>
        IExternalWebServiceAccess Get(int webServiceId, int userId);

        /// <summary>
        /// Returns all access objects for a specific web service.
        /// </summary>
        /// <param name="webServiceId">Unique identifier for the given web service.</param>
        /// <returns>A list of access objects.</returns>
        IList GetByWebService(int webServiceId);

        /// <summary>
        /// Returns all access objects for a specific user.
        /// </summary>
        /// <param name="userId">Unique identifier for the given user.</param>
        /// <returns>A list of access objects.</returns>
        IList GetByUser(int userId);

        /// <summary>
        /// Saves external web service access to the database.
        /// </summary>
        /// <param name="access">IExternalWebServiceAccess instance of the access to save.</param>
        /// <returns>True if successfully saved otherwise false.</returns>
        bool Save(IExternalWebServiceAccess access);

        /// <summary>
        /// Deletes web service access with the given primary key.
        /// </summary>
        /// <param name="webServiceId">Unique identifier of the web service.</param>
        /// <param name="userId">Unique identifier of the user.</param>
        /// <returns>True if the delete is successful.</returns>
        bool Remove(int webServiceId, int userId);
    }
}