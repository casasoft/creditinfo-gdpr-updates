#region

using System.Collections;

using Cig.Framework.Data.BusinessLogic.Service;
using DataAccess.Persistant.WSC;
using ValueObjects.WSC;

#endregion

using Cig.Framework.Base.Common.DataProtection;

namespace BusinessLogic.Service.WSC {
    /// <summary>
    /// Summary description for ExternalWebServiceAccessService.
    /// <para>Copyright (C) 2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
    /// </summary>
    public class ExternalWebServiceAccessService : ServiceBase, IExternalWebServiceAccessService {
        /// <summary> The proxy to use </summary>
        private IExternalWebServiceAccessProxy proxy;

        #region IExternalWebServiceAccessService Members

        /// <summary>
        /// Returns the web service access with the given primary key.
        /// </summary>
        /// <param name="webServiceId">Unique identifier of the web service.</param>
        /// <param name="userId">Unique identifier of the user.</param>
        /// <returns>The access object with the given primary key.</returns>
        public IExternalWebServiceAccess Get(int webServiceId, int userId) { return GetProxy().Get(webServiceId, userId); }

        /// <summary>
        /// Returns all access objects for a specific web service.
        /// </summary>
        /// <param name="webServiceId">Unique identifier for the given web service.</param>
        /// <returns>A list of access objects.</returns>
        public IList GetByWebService(int webServiceId) { return GetProxy().GetByWebService(webServiceId); }

        /// <summary>
        /// Returns all access objects for a specific user.
        /// </summary>
        /// <param name="userId">Unique identifier for the given user.</param>
        /// <returns>A list of access objects.</returns>
        public IList GetByUser(int userId) { return GetProxy().GetByUser(userId); }

        /// <summary>
        /// Saves external web service access to the database.
        /// </summary>
        /// <param name="access">IExternalWebServiceAccess instance of the access to save.</param>
        /// <returns>True if successfully saved otherwise false.</returns>
        public bool Save(IExternalWebServiceAccess access) {
            if (!string.IsNullOrEmpty(access.PasswordEncrypted)) {
                access.PasswordEncrypted = Crypto.Encrypt(access.PasswordEncrypted);
            }
            return GetProxy().SaveAccess(access);
        }

        /// <summary>
        /// Deletes web service access with the given primary key.
        /// </summary>
        /// <param name="webServiceId">Unique identifier of the web service.</param>
        /// <param name="userId">Unique identifier of the user.</param>
        /// <returns>True if the delete is successful.</returns>
        public bool Remove(int webServiceId, int userId) { return GetProxy().Remove(webServiceId, userId); }

        #endregion

        /// <summary>
        /// Returns the external web service access lookup proxy to use.
        /// </summary>
        /// <returns>The external web service access lookup proxy to use.</returns>
        private IExternalWebServiceAccessProxy GetProxy() {
            if (proxy == null) {
                proxy = (IExternalWebServiceAccessProxy) context.GetObject("ExternalWebServiceAccessProxy");
            }
            return proxy;
        }
    }
}