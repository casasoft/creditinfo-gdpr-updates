#region

using System.Globalization;
using System.Resources;
using ValueObjects.WSC;

#endregion

namespace BusinessLogic.Service.WSC {
    /// <summary>
    /// Summary description for INationalRegistry.
    /// <para>Copyright (C) 2005 Gu�j�n Karl Arnarson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ILtWebService {
        /// <summary>
        /// Gets string feed from LT web service.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="webServiceId">Unique identifier for a web service.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <param name="XsltFilename">Name of the xslt file to transform the output.</param>
        /// <param name="rm">Resource manager to use.</param>
        /// <param name="ci">Current culture info.</param>
        /// <returns>String containing information from registry.</returns>
        string GetRequestWebService(
            IAuthenticator authenticator,
            int webServiceId,
            string query,
            string user,
            string XsltFilename,
            ResourceManager rm,
            CultureInfo ci);

        /// <summary>
        /// Gets information about an individual/company from national regstry.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <param name="XsltFilename">Name of the xslt file to transform the output.</param>
        /// <param name="rm">Resource manager to use.</param>
        /// <param name="ci">Current culture info.</param>
        /// <returns>String containing the information for national registry.</returns>
        string GetNationalRegistryInformation(
            IAuthenticator authenticator,
            string query,
            string user,
            string XsltFilename,
            ResourceManager rm,
            CultureInfo ci);

        /// <summary>
        /// Gets information about debts.
        /// </summary>
        /// <param name="authenticator">Username and password information for the connection.</param>
        /// <param name="query">National registry number to get information about.</param>
        /// <param name="user">National registry number for user making request.</param>
        /// <param name="XsltFilename">Name of the xslt file to transform the output.</param>
        /// <param name="rm">Resource manager to use.</param>
        /// <param name="ci">Current culture info.</param>
        /// <returns>XmlDocument containing the information from debt registry.</returns>
        string GetDebtRegistryInformation(
            IAuthenticator authenticator,
            string query,
            string user,
            string XsltFilename,
            ResourceManager rm,
            CultureInfo ci);
    }
}