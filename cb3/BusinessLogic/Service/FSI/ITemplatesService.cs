#region

using System.Collections;
using ValueObjects.FSI.Templates;

#endregion

namespace BusinessLogic.Service.FSI {
    /// <summary>
    /// Interface describing the template service.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ITemplatesService {
        /// <summary>
        /// Returns a list of all templates.
        /// </summary>
        /// <returns>A list of all templates.</returns>
        IList GetTemplates();

        /// <summary>
        /// Returns the template with given id.
        /// </summary>
        /// <param name="templateID">The id of the template.</param>
        /// <returns>The template with given id.</returns>
        ITemplate GetTemplate(int templateID);

        /// <summary>
        /// Returns a list of all fields.
        /// </summary>
        /// <returns>A list of all fields.</returns>
        IList GetFields();

        /// <summary>
        /// Returns a list of all fields for given template.
        /// </summary>
        /// <param name="templateID">The id of the template.</param>
        /// <returns>A list of all fields for given template.</returns>
        IList GetFields(int templateID);

        /// <summary>
        /// Returns the field with given id.
        /// </summary>
        /// <param name="fieldID">Id of the field</param>
        /// <returns>The field with given id.</returns>
        IField GetField(int fieldID);

        /// <summary>
        /// Returns a list of all operators.
        /// </summary>
        /// <returns>A list of all operators.</returns>
        IList GetOperators();

        /// <summary>
        /// Returns the operator with given id.
        /// </summary>
        /// <param name="operatorID">The id of the operator.</param>
        /// <returns>The operator with given id.</returns>
        IOperator GetOperator(int operatorID);

        /// <summary>
        /// Saves the list of given ratios.
        /// </summary>
        /// <param name="templateRatios">The ratios to save.</param>
        /// <returns>True if saved successfully, false otherwise�.</returns>
        bool SaveTemplateRatios(IList templateRatios);
    }
}