#region

using System.Collections;
using ValueObjects.FSI.Templates;

#endregion

namespace BusinessLogic.Service.FSI {
    /// <summary>
    /// Interface describing the ratios service.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface IRatiosService {
        /// <summary>
        /// Returns a list of all ratios.
        /// </summary>
        /// <returns>A list of all ratios.</returns>
        IList GetRatios();

        /// <summary>
        /// Returns the ratio with given id.
        /// </summary>
        /// <param name="ratioID">The id of the ratio to return.</param>
        /// <returns>The ratio with given id.</returns>
        IRatio GetRatio(int ratioID);

        /// <summary>
        /// Saves given ratio.
        /// </summary>
        /// <param name="ratio">The ratio to save.</param>
        /// <returns>True if saved successfyully, false otherwise.</returns>
        bool SaveRatio(IRatio ratio);

        /// <summary>
        /// Deletes given ratio.
        /// </summary>
        /// <param name="ratio">The ratio to delete.</param>
        /// <returns>True if deleted successfyully, false otherwise.</returns>
        bool DeleteRatio(IRatio ratio);
    }
}