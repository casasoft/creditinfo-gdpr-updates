#region

using System.Collections;
using Cig.Framework.Data.BusinessLogic.Service;
using DataAccess.Persistant.FSI;
using ValueObjects.FSI.Templates;

#endregion

namespace BusinessLogic.Service.FSI {
    /// <summary>
    /// Summary description for TemplatesService.
    /// </summary>
    public class TemplatesService : ServiceBase, ITemplatesService {
        private static TemplatesService service;
        private readonly ITemplatesProxy proxy;
        public TemplatesService() { proxy = (ITemplatesProxy) context.GetObject("TemplatesProxy"); }

        #region ITemplatesService Members

        public IList GetTemplates() { return proxy.GetTemplates(); }
        public ITemplate GetTemplate(int templateID) { return proxy.GetTemplate(templateID); }
        public IList GetOperators() { return proxy.GetOperators(); }
        public IOperator GetOperator(int operatorID) { return proxy.GetOperator(operatorID); }
        public IList GetFields() { return proxy.GetFields(); }
        public IList GetFields(int templateID) { return proxy.GetFields(templateID); }
        public IField GetField(int fieldID) { return proxy.GetField(fieldID); }
        public bool SaveTemplateRatios(IList templateRatios) { return proxy.SaveTemplateRatios(templateRatios); }

        #endregion

        public static TemplatesService GetInstance() {
            if (service == null) {
                service = new TemplatesService();
            }

            return service;
        }
    }
}