#region

using System.Collections;
using Cig.Framework.Data.BusinessLogic.Service;
using DataAccess.Persistant.FSI;
using ValueObjects.FSI.Templates;

#endregion

namespace BusinessLogic.Service.FSI {
    /// <summary>
    /// Implementation of ratios service.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class RatiosService : ServiceBase, IRatiosService {
        /// <summary> The proxy to use </summary>
        private readonly IRatiosProxy proxy;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public RatiosService() { proxy = (IRatiosProxy) context.GetObject("RatiosProxy"); }

        #region IRatiosService Members

        /// <summary>
        /// Returns the ratio with given id.
        /// </summary>
        /// <param name="ratioID">The id of the ratio to return.</param>
        /// <returns>The ratio with given id.</returns>
        public IRatio GetRatio(int ratioID) { return proxy.GetRatio(ratioID); }

        /// <summary>
        /// Returns a list of all ratios.
        /// </summary>
        /// <returns>A list of all ratios.</returns>
        public IList GetRatios() { return proxy.GetRatios(); }

        /// <summary>
        /// Deletes given ratio.
        /// </summary>
        /// <param name="ratio">The ratio to delete.</param>
        /// <returns>True if deleted successfyully, false otherwise.</returns>
        public bool DeleteRatio(IRatio ratio) { return proxy.DeleteRatio(ratio); }

        /// <summary>
        /// Saves given ratio.
        /// </summary>
        /// <param name="ratio">The ratio to save.</param>
        /// <returns>True if saved successfyully, false otherwise.</returns>
        public bool SaveRatio(IRatio ratio) { return proxy.SaveRatio(ratio); }

        #endregion
    }
}