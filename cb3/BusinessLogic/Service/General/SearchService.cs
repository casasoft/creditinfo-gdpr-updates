#region

using System.Collections;
using Cig.Framework.Data.BusinessLogic.Service;
using DataAccess.Persistant.General;
using ValueObjects.General;

#endregion

namespace BusinessLogic.Service.General {
    /// <summary>
    /// Summary description for SearchService.
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public class SearchService : ServiceBase, ISearchService {
        private static SearchService service;
        private readonly ISearchProxy proxy;
        public SearchService() { proxy = (ISearchProxy) context.GetObject("SearchProxy"); }

        #region ISearchService Members

        public IList FindIndividual(string firstName) { return proxy.FindIndividual(firstName, null); }
        public IList FindIndividual(string firstName, string surName) { return proxy.FindIndividual(firstName, surName); }
        public IList FindIndividual(string firstName, string surName, string addressNative, string addressEN) { return proxy.FindIndividual(firstName, surName, addressNative, addressEN); }
        public IIndividual FindIndividualByCreditinfoId(int creditinfoId) { return proxy.FindIndividualByCreditinfoId(creditinfoId); }
        public IList FindIndividualByNationalId(string nationalId) { return proxy.FindIndividualByNationalId(nationalId); }
        public IList FindCompany(string name) { return proxy.FindCompany(name); }
        public IList FindCompany(string name, string addressNative, string addressEN) { return proxy.FindCompany(name, addressNative, addressEN); }
        public ICompany FindCompanyByCreditinfoId(int creditinfoId) { return proxy.FindCompanyByCreditinfoId(creditinfoId); }
        public IList FindCompanyByNationalId(string nationalId) { return proxy.FindCompanyByNationalId(nationalId); }

        #endregion

        public static SearchService GetInstance() {
            if (service == null) {
                service = new SearchService();
            }

            return service;
        }
    }
}