#region

using System.Collections;
using ValueObjects.General;

#endregion

namespace BusinessLogic.Service.General {
    /// <summary>
    /// This interface defines search methods the system offers
    /// <para>Copyright (C) 2005 Haukur �rn Har�arson - Creditinfo Group ltd.</para>
    /// </summary>
    public interface ISearchService {
        IList FindIndividual(string firstName);
        IList FindIndividual(string firstName, string surName);
        IList FindIndividual(string firstName, string surName, string addressNative, string addressEN);
        IIndividual FindIndividualByCreditinfoId(int creditinfoId);
        IList FindIndividualByNationalId(string nationalId);
        IList FindCompany(string name);
        IList FindCompany(string name, string addressNative, string addressEN);
        ICompany FindCompanyByCreditinfoId(int creditinfoId);
        IList FindCompanyByNationalId(string nationalId);
    }
}