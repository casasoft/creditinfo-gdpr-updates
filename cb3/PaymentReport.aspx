<%@ Page language="c#" Codebehind="PaymentReport.aspx.cs" AutoEventWireup="false" EnableEventValidation="false" Inherits="CreditInfoGroup.PaymentReport" %>
<%@ Register TagPrefix="uc1" TagName="PaymentReport" Src="new_user_controls/PaymentBehaviourControl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>Creditinfo Group Ltd.</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="expires" content="-1">
		<meta http-equiv="pragma" content="no-cache">
		<meta name="description" content="Lánstraust">
		<meta name="author" content="Lánstraust Brautarholti 10-14. S: 550-9600">
		<style>.header { FONT-SIZE: 10px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.header2 { FONT-SIZE: 12px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.header3 { FONT-SIZE: 12px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.headerb { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.headerwhite { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: white; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.headerblack { FONT-WEIGHT: bolder; FONT-SIZE: 17px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.conbig { FONT-WEIGHT: bolder; FONT-SIZE: 15px; COLOR: #891618; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.conbig2 { FONT-WEIGHT: bolder; FONT-SIZE: 14px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.con { FONT-SIZE: 10px; COLOR: black; FONT-FAMILY: verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	P.break { PAGE-BREAK-AFTER: always }
	break { PAGE-BREAK-AFTER: always }
	</style>
		<LINK href="css/FoCIGStyles.css" type="text/css" rel="stylesheet">
  </head>
	<BODY leftMargin="0" topMargin="0">
		<table width="606">
			
			<tr>
				<td>
					<table id="tblTitle" cellSpacing="0" cellPadding="0" width="608">
						<tr>
							<td width="300" height="41" bgcolor="#891618" class="headerwhite">&nbsp;<asp:Literal ID="liPaymentBehaviour" Runat="Server"></asp:Literal></ASP:ADROTATOR></td>
							<td background="img/redline.gif">&nbsp;</td>
							<td><img src='img/logo_cz.gif' width="279" height="41"
									alt="" border="0"></td>
						</tr>
						<tr height="2">
							<td></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td id="showControl" runat="server"></td>
			</tr>
		</table>
		
	</BODY>
</html>
