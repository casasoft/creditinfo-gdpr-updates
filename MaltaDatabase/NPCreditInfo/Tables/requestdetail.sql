﻿CREATE TABLE [NPCreditInfo].[requestdetail] (
    [id]          INT      NOT NULL,
    [requestid]   INT      NOT NULL,
    [description] NTEXT    NOT NULL,
    [datecreated] DATETIME NOT NULL,
    [createdby]   INT      NOT NULL,
    [dateupdated] DATETIME NULL,
    [updatedby]   INT      NULL
);

