﻿CREATE TABLE [NPCreditInfo].[product] (
    [id]            INT             NOT NULL,
    [name]          NVARCHAR (100)  NOT NULL,
    [isopen]        CHAR (10)       NOT NULL,
    [navigateurl]   NVARCHAR (100)  NULL,
    [parentid]      INT             NOT NULL,
    [isbaseproduct] CHAR (10)       NOT NULL,
    [menu]          CHAR (10)       NOT NULL,
    [menuname]      NVARCHAR (100)  NULL,
    [isinner]       CHAR (10)       NULL,
    [description]   NVARCHAR (1024) NULL,
    [datecreated]   DATETIME        NOT NULL,
    [createdby]     INT             NOT NULL,
    [dateupdated]   DATETIME        NULL,
    [updatedby]     INT             NULL
);

