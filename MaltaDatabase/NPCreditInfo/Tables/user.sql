﻿CREATE TABLE [NPCreditInfo].[user] (
    [id]           INT           NOT NULL,
    [username]     NVARCHAR (50) NOT NULL,
    [email]        NVARCHAR (50) NOT NULL,
    [passwordhash] VARCHAR (40)  NOT NULL,
    [salt]         VARCHAR (10)  NOT NULL,
    [isopen]       CHAR (10)     NOT NULL,
    [openuntil]    DATETIME      NULL,
    [datecreated]  DATETIME      NOT NULL,
    [createdby]    INT           NOT NULL,
    [dateupdated]  DATETIME      NULL,
    [updatedby]    INT           NULL
);

