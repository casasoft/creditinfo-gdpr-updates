﻿CREATE TABLE [NPCreditInfo].[userproducts] (
    [userid]      INT      NOT NULL,
    [productid]   INT      NOT NULL,
    [datecreated] DATETIME NOT NULL,
    [createdby]   INT      NOT NULL,
    [dateupdated] DATETIME NULL,
    [updatedby]   INT      NULL
);

