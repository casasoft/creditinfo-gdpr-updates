﻿CREATE TABLE [NPCreditInfo].[priority] (
    [id]          INT           NOT NULL,
    [name]        NVARCHAR (50) NOT NULL,
    [description] NTEXT         NULL,
    [datecreated] DATETIME      NOT NULL,
    [createdby]   INT           NOT NULL,
    [dateupdated] DATETIME      NULL,
    [updatedby]   INT           NULL
);

