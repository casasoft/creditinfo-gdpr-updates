﻿CREATE PROCEDURE [dbo].[ama_afs_mt_NEW](@v_ctry char(2))
as

BEGIN

INSERT INTO [ama_afs]
           ([currency]
           ,[denomination]
           ,[statement_date]
           ,[statement_type]
           ,[number_months]
           ,[bs_fixed_assets]
           ,[bs_intangible_fixed_assets]
           ,[bs_tangible_fixed_assets]
           ,[bs_other_fixed_assets]
           ,[bs_current_assets]
           ,[bs_stocks]
           ,[bs_debtors]
           ,[bs_other_current_assets]
           ,[bs_cash_and_cash_equivalent]
           ,[bs_total_assets]
           ,[bs_shareholders_funds]
           ,[bs_capital]
           ,[bs_other_shareholders_funds]
           ,[bs_non_current_liabilities]
           ,[bs_long_term_debt]
           ,[bs_other_non_current_liabilities]
           ,[bs_current_liabilities]
           ,[bs_loans]
           ,[bs_creditors]
           ,[bs_other_current_liabilities]
           ,[bs_total_shareholders_funds]
           ,[bs_working_capital]
           ,[bs_number_employees]
           ,[pl_operating_revenue]
           ,[pl_turnover]
           ,[pl_cost_goods_sold]
           ,[pl_gross_profit]
           ,[pl_other_operating_expenses]
           ,[pl_operating_pl]
           ,[pl_financial_revenue]
           ,[pl_financial_expenses]
           ,[pl_financial_pl]
           ,[pl_pl_before_taxation]
           ,[pl_taxation]
           ,[pl_pl_after_taxtation]
           ,[pl_extraordinary_and_other_revenue]
           ,[pl_extraordinary_and_other_expenses]
           ,[pl_extraordinary_and_other_pl]
           ,[pl_pl_for_period]
           ,[pl_export_turnover]
           ,[pl_material_costs]
           ,[pl_cost_employees]
           ,[pl_deprication]
           ,[pl_interest_paid]
           ,[pl_cash_flow]
           ,[pl_added_value]
           ,[comp_id]
           ,[ord]
           ,[ctry])
    
	SELECT TOP 1
	[currency] = Currency,
	[denomination] = Denomination_id,
	[statement_date] = CONVERT(VARCHAR(10), Financial_year_end, 112),
	[statement_type] = NULL,
	[number_months] = Account_period_length,
	[bs_fixed_assets] = Total_Fixed_Assets,
	[bs_intangible_fixed_assets] = Intangible_Assets,
	[bs_tangible_fixed_assets] = Tangible_Assets,
	[bs_other_fixed_assets] = Financial_Assets,
	[bs_current_assets] = Total_Current_Assets,
	[bs_stocks] = Inventory,
	[bs_debtors] = Receivables_And_Debtors,
	[bs_other_current_assets] = Other_Current_Assets,
	[bs_cash_and_cash_equivalent] = Cash,
	[bs_total_assets] = Total_Assets,
	[bs_shareholders_funds] = Total_Equity,
	[bs_capital] = Issued_Share_Capital,
	[bs_other_shareholders_funds] = Other_Equity,
	[bs_non_current_liabilities] = NULL,
	[bs_long_term_debt] = Long_Term_Debts,
	[bs_other_non_current_liabilities] = Other_Liabilities,
	[bs_current_liabilities] = Total_Short_Term_Debt,
	[bs_loans] = Loans_Overdraft,
	[bs_creditors] = Creditors,
	[bs_other_current_liabilities] = Other_Short_Term_Debt,
	[bs_total_shareholders_funds] = Total_Debts_And_Equity,
	[bs_working_capital] = NULL,
	[bs_number_employees] = Employee_count,
	[pl_operating_revenue] = NULL,
	[pl_turnover] = Revenue_from_main_operations,
	[pl_cost_goods_sold] = Cost_of_sales,
	[pl_gross_profit] = Gross_profit,
	[pl_other_operating_expenses] = Administrative_other_expenses,
	[pl_operating_pl] = NULL,
	[pl_financial_revenue] = Financial_Income,
	[pl_financial_expenses] = Financial_cost,
	[pl_financial_pl] = Net_Financial_Items,
	[pl_pl_before_taxation] = Income_from_operations_before_tax,
	[pl_taxation] = Taxes,
	[pl_pl_after_taxtation] = Income_from_operations_after_tax,
	[pl_extraordinary_and_other_revenue] = Extraordinary_Income_Cost,
	[pl_extraordinary_and_other_expenses] = Extraordinary_Income_Cost,
	[pl_extraordinary_and_other_pl] = NULL,
	[pl_pl_for_period] = Net_profit,
	[pl_export_turnover] = NULL,
	[pl_material_costs] = NULL,
	[pl_cost_employees] = Staff_cost,
	[pl_deprication] = Deprication_of_fixed_assets,
	[pl_interest_paid] = NULL,
	[pl_cash_flow] = NULL,
	[pl_added_value] = NULL,
	[comp_id] = g.creditinfoid,
	[ord] = ROW_NUMBER() OVER (PARTITION BY c.number ORDER BY g.financial_year_end DESC),
	@v_ctry AS ctry
	FROM dbo.FSI_TemplateStatements s 
		INNER JOIN dbo.FSI_TemplateGeneralInfo g ON s.AFSID = g.AFS_id                                       
			AND g.Deleted = 'False'
		INNER JOIN creditinfogroup_mt..np_idnumbers c on g.creditinfoid=c.creditinfoid and c.numbertypeid=1
		INNER JOIN Amadeus..ama_company d on c.number=d.national_id collate SQL_Latin1_General_CP1_CI_AS
	where Ready_for_web_publishing = 'True'	and year(g.financial_year_end) <year(getdate())
	ORDER BY c.number, g.financial_year_end desc

END