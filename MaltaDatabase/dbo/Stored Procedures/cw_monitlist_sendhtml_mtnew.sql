﻿

CREATE procedure [dbo].[cw_monitlist_sendhtml_mtnew]
as
  declare
         @vid integer,
         @creditinfoid int,
         @email varchar(255), 
         @nameof varchar(255),
         @type varchar(40),
         @header nvarchar(4000),
         @body nvarchar(4000),
         @footer nvarchar(4000),
         @messbody nvarchar(4000),
         @imess int,
         @cmess int,
         @messmax int,
         @messform int,
         @watchid_id int,
         @watch_subject nvarchar(100),
         @subject_name nvarchar(100)
   declare cur_cmess cursor local for
          select count(*)
          from cw_sendclaims
          where
          sent='False' and
          messform in (1,2)
  declare cur_users cursor local for
      select distinct email, creditinfoid, messform
      from cw_sendclaims
      where sent='False'
      and messform in (1,2)
      and statusid=4 --and email like '%creditinfo%'
      order by email
begin --b0
  set @messmax = 6
  open cur_cmess
  fetch next from cur_cmess into @cmess
  close cur_cmess
  if @cmess >10000
    begin --b1
      return
    end --b1
  open cur_users
  fetch next from cur_users into @email, @creditinfoid, @messform
  while @@fetch_status=0
  begin --b2
   
        set @header = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">' + char(13) + char(10)
        set @header = @header + '<html><head><title>Creditinfo</title></head>' + char(13) + char(10)
        set @header = @header + '<style>' + char(13) + char(10)
        set @header = @header + '.tfyrir {font-family: Verdana,Geneva;font-size: 8pt;color: #FFFFFF;background-color: #333366;font-weight: bold;text-align: left; padding-left:6pt; padding-bottom:4pt;padding-top:4pt;padding-right:6pt;border-bottom: 0pt;}' + char(13) + char(10)
        set @header = @header + 'TD {font-family: Verdana,Geneva;font-size: 8pt;color: #000000;border-bottom: 1pt;border-top: 0;border-left: 0;border-right: 0;border-style: solid; border-color: #333366;padding-bottom: 4pt;padding-top: 4pt;padding-left:6pt;padding-right:6pt;white-space:nowrap}' + char(13) + char(10)
        set @header = @header + 'A {font-family: Verdana,Geneva;font-size: 8pt;color: #333366;border-bottom: 1pt;border-top: 0;border-left: 0;border-right: 0;border-style: solid; border-color: #333366;padding-bottom: 4pt;padding-top: 4pt;padding-left:6pt;padding-right:6pt;font-weight:bold;text-align:center;text-decoration:none}' + char(13) + char(10)
        set @header = @header + 'A:hover {text-decoration:underline}' + char(13) + char(10)
        set @header = @header + '.fyrir {font-family: Verdana,Geneva;font-size: 8pt;font-weight:bold;text-align:center;color:#333366;border-bottom:0pt;}' + char(13) + char(10)
        set @header = @header + '</style>' + char(13) + char(10)
        set @header = @header + '<body bgcolor=#FFFFFF>' + char(13) + char(10) + '<table border=0 style="{margin-left:50pt;background-color:#FFFFFF;}"><tr><td valign=top style="{border-bottom: 0pt;}"><table border=0 cellspacing=0 width=100%>' + char(13) + char(10)
        set @header = @header + '<tr><td align=center style="{border-bottom: 0pt;}"><img src="https://secure.creditinfo.com.mt/img/mt_logo.png" border=0 width=300 height=62></td></tr><tr><td style="{border-bottom:0pt;}">&nbsp;</td></tr>' + char(13) + char(10)
        set @header = @header + '<tr><td width=500 class=fyrir  style="{border-bottom: 0pt;text-align:left}">The changes below apply to the Court Information and Defaulting Debts Database of Creditinfo Malta Limited.</td></tr>' + char(13) + char(10)
        set @header = @header + '<tr><td class=megin  style="{border-bottom: 0pt;text-align:left}">Date sent: ' + convert(varchar(10),year(getdate())) + '-' + convert(varchar(10),month(getdate()))+ '-' + convert(varchar(10),day(getdate())) + '</td></tr><tr><td style="{border-bottom: 0pt;}">&nbsp;</td></tr>' + char(13) + char(10)
        set @header = @header + '<tr><td class=fyrir  style="{border-bottom: 0pt;text-align:center}">New records: </td></tr>' + char(13) + char(10)

        set @header = @header + '<td><tr><table border=0 style="{border-top:0px;border-left:0px;border-bottom:0px;border-right:0;px;border-color:#333366;border-style:solid}" align=center cellspacing=0><tr><td class=tfyrir style="{border-bottom: 0pt;}">#</td><td class=tfyrir style="{border-bottom: 0pt;}">Name</td><td class=tfyrir style="{border-bottom: 0pt;}">Watch subject</td><td class=tfyrir style="{border-bottom: 0pt;}">Case type</td></tr>'  + char(13) + char(10) -- <td class=tfyrir style="{border-bottom: 0pt;}">General info</td><td class=tfyrir style="{border-bottom: 0pt;}">Negative payments</td></tr>' + char(13) + char(10)
        set @footer = '</table></td></tr></table></td></tr></table></body></html>' + char(13) + char(10)
    set @body = ''
    set @imess = 0
  declare cur_mess cursor local for 
      select distinct id,creditinfoid,claim_type, watchid_id, watch_subject,subject_name
      from cw_sendclaims
      where 
        sent='False'
        and messform = 1
        and email=@email
        and statusid=4
      order by subject_name,claim_type
    open cur_mess
    fetch next from cur_mess into @vid, @creditinfoid, @type, @watchid_id, @watch_subject, @subject_name
    while @@fetch_status=0
    begin --b3
     set @imess = @imess + 1
     set @body = @body + '<tr><td>' + cast(@imess as varchar(9)) + '</td><td><A HREF="https://secure.creditinfo.com.mt/FoSearch.aspx?reqNationalID='+@watch_subject+'">' + @subject_name + '</A></td><td><A HREF="https://secure.creditinfo.com.mt/FoSearch.aspx?reqNationalID='+@watch_subject+'">' + @watch_subject + '</A></td><td>' + @type + char(13) + char(10) 
     if @imess % @messmax = 0
       begin --b6
         if len(@email) > 5
           begin --b6b
           if @messform = 1
             begin --b7
           set @messbody=@header+@body+@footer
           EXEC msdb.dbo.sp_send_dbmail @profile_name = 'Malta', @recipients=@email, @subject='Creditinfo Malta Monitor Notification - New records', @body=@messbody, @body_format = 'HTML'
             set @nameof = @nameof --gfn
             end --b7
           else
             if @messform = 2
               begin --b8
             set @nameof = @nameof --gfn
               end --b8
         insert into cw_sent(creditinfoid,email,watch,number,type) values (@creditinfoid,@email,'v',@messmax,1)
         end --b6b
        set @body = ''
    end --b6
    update cw_watchuniqueid set sent='True',sentDate=getdate() where watchid=@watchid_id
    fetch next from cur_mess into @vid, @creditinfoid, @type, @watchid_id, @watch_subject, @subject_name
  end  --b3
  close cur_mess
  deallocate cur_mess
   if @imess % @messmax <> 0
     begin --b9
      if len(@email) > 5
       begin --b10
         if @messform = 1
           begin --b11
           set @messbody=@header+@body+@footer
           EXEC msdb.dbo.sp_send_dbmail @profile_name = 'Malta', @recipients=@email, @subject='Creditinfo Malta Monitor Notification - New records', @body=@messbody, @body_format = 'HTML'
           set @nameof=@nameof --gfn
           end --b11
         else
           if @messform = 2
             begin --b12
           set @nameof=@nameof --gfn
             end --b12
         insert into cw_sent(creditinfoid,email,watch,number,type) values (@creditinfoid,@email,'n',@imess % @messmax,1)
      end --b10
    end --b9
      
  update cw_sendclaims
      set sent='True'
      where email=@email
        and sent='False'
        and messform=@messform
        and statusid=4
  fetch next from cur_users into @email, @creditinfoid, @messform
  end  --b2
  close cur_users
  delete from cw_sendclaims where sent='True'
  deallocate cur_cmess
  deallocate cur_users  
end --b0
