﻿
CREATE PROCEDURE dbo.sc_UpdateVersions
	@pAppMajor char(2),
	@pAppMinor char(2),
	@pAppPatch char(3),
	@pDBPatch char(3)
 AS
	DECLARE @AppMajor char(2);
	DECLARE @AppMinor char(2);
	DECLARE @AppPatch char(3);
	DECLARE @DBPatch char(3);

	--Check if version record exists.
	if exists(SELECT * FROM c_vc_versions)
	begin
		--Then update it.
		--Get record information.
		SELECT @AppMajor=AppMajor,@AppMinor=AppMinor,@AppPatch=AppPatch,@DBPatch=DBPatch FROM c_vc_versions;

		if @pAppMinor IS NULL
		begin	
			SET @pAppPatch = @AppMinor;
		end
		if @pAppPatch IS NULL
		begin	
			SET @pAppPatch = @AppPatch;
		end
		if @pDBPatch IS NULL
		begin	
			SET @pDBPatch = @DBPatch;
		end

		--Update with new information.
		UPDATE c_vc_versions SET AppMajor = @pAppMajor, AppMinor = @pAppMinor, AppPatch = @pAppPatch, DBPatch = @pDBPatch, Updated=getdate();
	end
	else
	begin
		--Then insert it with default values if NULL.
		if @pAppMinor IS NULL
		begin	
			SET @pAppPatch = '00';
		end
		if @pAppPatch IS NULL
		begin	
			SET @pAppPatch = '000';
		end
		if @pDBPatch IS NULL
		begin	
			SET @pDBPatch = '000';
		end

		INSERT INTO c_vc_versions (AppMajor,AppMinor,AppPatch,DBPatch) SELECT @pAppMajor,@pAppMinor,@pAppPatch ,@pDBPatch;
	end
	--Insert into script log.
	INSERT INTO c_vc_revisions(AppMajor,AppMinor,AppPatch,DBPatch) SELECT @pAppMajor,@pAppMinor,@pAppPatch ,@pDBPatch;
