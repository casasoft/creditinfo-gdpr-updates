﻿-- Útbúa procedure fyrir töfluna "np_usage". Skilar int sem er identity fyrir færsluna og verður report_id.
-- Það itdentity þarf svo að fylgja öllum þeim færslum sem tilheyra viðkomandi skýrslu...
CREATE PROCEDURE dbo.AddTemporaryUsage
(
	@userid		int,
	@ReturnId	int OUTPUT
)

AS
	SET NOCOUNT ON
 	
	INSERT INTO np_Usage (creditinfoid, query_type, UserID)
	VALUES (0,0,@userid)
	-- Vel "identity seed" field og skila honum í breytunni...
	SELECT @ReturnId = @@IDENTITY

	-- Athuga með villu / Rollback ef eitthvað kemur uppá...
	IF @@error <> 0 GOTO ERR_HANDLER
	RETURN @ReturnId

	ERR_HANDLER:
	ROLLBACK TRANSACTION
	-- Ef það kemur upp villa þá skilum við -1
	RETURN -1
