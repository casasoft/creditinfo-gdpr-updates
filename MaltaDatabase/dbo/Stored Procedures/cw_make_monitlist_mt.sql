﻿CREATE procedure cw_make_monitlist_mt
as
declare @cw_messform int,
        @email varchar(255),
        @claim_type nvarchar(256),
        @claim_id int,
        @creditinfoid nvarchar(40),
        @sent char(10),
        @cw_allcases varchar(5),
        @watchid_id int,
        @watch_subject nvarchar(100),
        @subject_name nvarchar(100),
	@statusid int;
declare cur_list_id cursor local for
        select b.email,d.id,e.typeen,a.sent,b.cw_allcases,b.cw_messform,b.creditinfoid, a.watchid,a.uniqueid,g.namenative, c.statusid
        from cw_watchuniqueid a,au_users b,cw_newclaims c,np_claims d,np_casetypes e, np_idnumbers f, np_companys g
        where a.userid=b.id
        and c.claim_id=d.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and d.claimtypeid=e.casetypeid
        and b.isopen='True'
        union all
        select b.email,d.id,e.typeen,a.sent,b.cw_allcases,b.cw_messform,b.creditinfoid, a.watchid,a.uniqueid,g.firstnamenative, c.statusid
        from cw_watchuniqueid a,au_users b,cw_newclaims c,np_claims d,np_casetypes e, np_idnumbers f, np_individual g
        where a.userid=b.id
        and c.claim_id=d.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and d.claimtypeid=e.casetypeid
        and b.isopen='True'
declare cur_list_keyw cursor local for
        select b2.email,c.claim_id,e.typeen,a.sent,b2.cw_allcases,b2.cw_messform,b1.creditinfoid, a.watchid, a.firstnamenative, f.firstnamenative, c.statusid
        from cw_watchwords a,np_creditinfouser b1, au_users b2, cw_newclaims c,np_claims d, np_casetypes e, np_individual f, np_individual right outer join np_address g on np_individual.creditinfoid=g.creditinfoid
        where a.userid=b2.id
        and b1.creditinfoid=d.creditinfoid
        and c.claim_id=d.id
        and d.creditinfoid=f.creditinfoid
        and f.creditinfoid=g.creditinfoid
        and (
             ((lower(a.firstnameen)=lower(f.firstnameen))
          or (lower(a.firstnamenative)=lower(f.firstnamenative) or a.firstnamenative is null and a.firstnameen is null))
          and
             ((lower(a.surnameen)=lower(f.surnameen))
          or (lower(a.surnamenative)=lower(f.surnamenative) or a.surnamenative is null and a.surnameen is null))
            )
          and
             ((lower(a.addressen)=lower(g.streeten))
          or (lower(a.addressnative)=lower(g.streetnative) or a.addressnative is null and a.addressen is null)
            )
        and not(a.firstnameen is null and a.firstnamenative is null and a.surnameen is null and a.surnamenative is null)
        and d.claimtypeid=e.casetypeid
        and b2.isopen='True'
begin
delete from cw_newclaims where (select count(*) from cw_newclaims x where cw_newclaims.claim_id=x.claim_id and x.statusid=4) =(select count(*) from cw_newclaims x where cw_newclaims.claim_id=x.claim_id and x.statusid=6)
open cur_list_id
fetch next from cur_list_id into @email, @claim_id, @claim_type, @sent, @cw_allcases, @cw_messform, @creditinfoid, @watchid_id, @watch_subject, @subject_name, @statusid
while @@fetch_status=0
begin
        if @cw_allcases='True' or (@cw_allcases='False' and @sent='False')
           insert into cw_sendclaims(creditinfoid, email,  claim_id,  claim_type, messform, watch_type, watchid_id, watch_subject, subject_name, statusid)
           values                       (@creditinfoid,        @email, @claim_id, @claim_type,@cw_messform, 1, @watchid_id, @watch_subject, @subject_name, @statusid)
fetch next from cur_list_id into @email, @claim_id, @claim_type, @sent, @cw_allcases, @cw_messform, @creditinfoid, @watchid_id, @watch_subject, @subject_name, @statusid
end
close cur_list_id
deallocate cur_list_id
open cur_list_keyw
fetch next from cur_list_keyw into @email, @claim_id, @claim_type, @sent, @cw_allcases, @cw_messform, @creditinfoid, @watchid_id, @watch_subject, @subject_name, @statusid
while @@fetch_status=0
begin
        if @cw_allcases='True' or (@cw_allcases='False' and @sent='False')
           insert into cw_sendclaims(creditinfoid, email,  claim_id,  claim_type, messform, watch_type, watchid_id, watch_subject, subject_name, statusid)
           values                       (@creditinfoid,        @email, @claim_id, @claim_type,@cw_messform, 2, @watchid_id, @watch_subject, @subject_name, @statusid)
   fetch next from cur_list_keyw into @email, @claim_id, @claim_type, @sent, @cw_allcases, @cw_messform, @creditinfoid, @watchid_id, @watch_subject, @subject_name, @statusid
end
close cur_list_keyw
deallocate cur_list_keyw
delete from cw_newclaims
end