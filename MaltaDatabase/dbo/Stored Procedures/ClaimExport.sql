﻿CREATE procedure dbo.ClaimExport (
	@clNumber varchar(20))
as
select claimdate,a.id,a.casenr,.dbo.lmt_getname(d.number) as debtorname,e.number,a.amount,
(select sum(amount) from np_claims x where x.creditinfoid=a.creditinfoid and x.statusid=4 and x.claimownerciid=a.claimownerciid and informationsourceid=9) as totalamountdebtorclaimowner,
(select sum(amount) from np_claims x where x.creditinfoid=a.creditinfoid and x.statusid=4) as totalamountdebtor,
(select sum(amount) from np_claims x where x.claimownerciid=a.claimownerciid and x.statusid=4) as totalamountclaimowner
from np_claims a join np_idnumbers b on a.claimownerciid=b.creditinfoid left join np_idnumbers d on a.creditinfoid=d.creditinfoid and d.numbertypeid=1  left join np_idnumbers e on a.creditinfoid=e.creditinfoid and e.numbertypeid=1
where b.numbertypeid=1 and b.number=@clNumber and informationsourceid=9 and statusid=4
order by  3,2

