﻿
-- Create JOB's category firstly.
--exec msdb.dbo.sp_add_category 'JOB', 'LOCAL', 'MT:DTS'

create procedure stp_ExecuteDTS_MT_Batch
--@GlobalVar1 varchar(50),
--@GlobalVar2 varchar(50)
as

declare @jid uniqueidentifier

-- Specify DTS to be executed
declare @dts varchar(128)
set @dts = 'MT: Batch'

-- Initialize command
declare @cmd varchar(4000)
set @cmd = '"C:\PROGRAM FILES\MICROSOFT SQL SERVER\80\TOOLS\BINN\Dtsrun.exe" /S "(local)" /N "' + @dts + '" /W "0" /E '

-- Specify global variables values to be passed to DTS trough DTSRUN
--set @cmd = @cmd + '/A GV1:8="' + @GlobalVar1 + '" '
--set @cmd = @cmd + '/A GV2:8="' + @GlobalVar2 + '" '

-- Create a unique job name
-- 
declare @jname varchar(128)
--set @jname = cast(newid() as char(36)) -- Uncomment this to ALLOW TO RUN MULTIPLE DTS INSTANCES AT THE SAME TIME
set @jname = 'JOB_DTS_MT: Batch' -- Uncomment this to ALLOW TO RUN ONLY ONE DTS INSTANCE AT TIME (default)

-- Create job
exec msdb.dbo.sp_add_job
	@job_name 			= @jname,
	@enabled			= 1,
	@category_name 		= 'MT:DTS'
	--,
	--@delete_level		= 1
--,
	--@job_id 			= @jid --OUTPUT

exec msdb.dbo.sp_add_jobserver
	--@job_id 			= @jid,
	@job_name 			= @jname,
	@server_name		= 'NEPTUN'

exec msdb.dbo.sp_add_jobstep
	--@job_id 			= @jid,
	@job_name			= @jname,
	@step_name			= 'Execute DTS',
	@subsystem			= 'CMDEXEC',
	@command			= @cmd

-- Start job
--exec msdb.dbo.sp_start_job 
--	@job_id 			= @jid
