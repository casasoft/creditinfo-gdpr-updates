﻿



   CREATE PROCEDURE [dbo].[ddd_UpdateIndividualFullName]
    @CreditInfoID int,
    @Surname nvarchar(80) = NULL,
    @OtherSurname nvarchar(80) = NULL,
    @Name nvarchar(80) = NULL,
    @OtherName nvarchar(80) = NULL,
    @MiddleNames nvarchar(80) = NULL
    AS


    IF NOT EXISTS (SELECT * FROM [dbo].[np_Individual_FullName] WHERE CreditInfoID = @CreditInfoID)
	BEGIN
    INSERT INTO [dbo].[np_Individual_FullName]
    ([CreditInfoID],[Surname],[OtherSurname],[Name],[OtherName],[MiddleNames])
    VALUES
    (@CreditInfoID, @Surname, @OtherSurname, @Name, @OtherName, @MiddleNames)
	END
    ELSE
    BEGIN
    UPDATE [np_Individual_FullName]
    SET [Surname]  = @Surname, [OtherSurname] = @OtherSurname, [Name] = @Name, [OtherName] = @OtherName, [MiddleNames] = @MiddleNames, [Updated] = GETDATE()
    WHERE CreditInfoId = @CreditInfoID

	END

	SET Context_Info 0x55555
   
    update [dbo].[np_Individual] set Processed = 1 where CreditInfoId = @CreditInfoId



