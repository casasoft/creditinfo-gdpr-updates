﻿CREATE view dbo.ros_CIIDNationalCompanyView as
SELECT     dbo.ros_NationalCompanyInterfaceView.PostalCode AS NationalPostalCode, dbo.ros_NationalCompanyInterfaceView.Name AS NationalName, 
                      dbo.ros_NationalCompanyInterfaceView.Address AS NationalAddress, dbo.ros_NationalCompanyInterfaceView.FaxNumber AS NationalFaxNumber, 
                      dbo.ros_NationalCompanyInterfaceView.PhoneNumber AS NationalPhoneNumber, dbo.ros_CompanyView.NameNative, 
                      dbo.ros_CompanyView.NameEN, dbo.ros_CompanyView.CreditInfoID, dbo.ros_CompanyView.AddressNative, dbo.ros_CompanyView.AddressEN, 
                      dbo.ros_CompanyView.CityID, 
			CASE WHEN dbo.ros_CompanyView.CityNative IS NOT NULL 
                      THEN rtrim(dbo.ros_CompanyView.CityNative COLLATE SQL_Latin1_General_CP1_CI_AI) 
                      ELSE rtrim(dbo.ros_NationalCompanyInterfaceView.CityNative) END AS CityNative, 
dbo.ros_CompanyView.CityEN, dbo.ros_CompanyView.PostalCode, 
                      dbo.ros_CompanyView.CountryID, 
CASE WHEN dbo.ros_CompanyView.CountryNative IS NOT NULL 
                      THEN rtrim(dbo.ros_CompanyView.CountryNative COLLATE SQL_Latin1_General_CP1_CI_AI) 
                      ELSE rtrim(dbo.ros_NationalCompanyInterfaceView.CountryNative) END AS CountryNative, 
dbo.ros_CompanyView.CountryEN, 
                      dbo.ros_NationalCompanyInterfaceView.IDNumber AS NationalIDNumber, dbo.ros_NationalCompanyInterfaceView.NumberTypeID, 
                      dbo.ros_CompanyView.IDNumber, dbo.ros_NationalCompanyInterfaceView.RegDate, dbo.np_Notes.Note
FROM         dbo.ros_NationalCompanyInterfaceView FULL OUTER JOIN
                      dbo.ros_CompanyView ON 
                      dbo.ros_NationalCompanyInterfaceView.IDNumber COLLATE SQL_Latin1_General_CP1_CI_AI = dbo.ros_CompanyView.IDNumber AND 
                      dbo.ros_NationalCompanyInterfaceView.NumberTypeID = dbo.ros_CompanyView.NumberTypeID LEFT OUTER JOIN
					  dbo.np_Notes ON dbo.ros_CompanyView.CreditInfoID = dbo.np_Notes.CreditInfoID
