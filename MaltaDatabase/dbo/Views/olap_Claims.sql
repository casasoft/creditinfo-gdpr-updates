﻿CREATE VIEW dbo.olap_Claims
AS
SELECT     ID, CreditInfoID, ClaimTypeID, InformationSourceID, StatusID, CurrencyID, Amount, RegisterID, RegDate, ClaimDate
FROM         dbo.np_Claims
