﻿CREATE VIEW dbo.mt_v_CompanyRegistrationID_2
AS
SELECT     dbo.lmt_companyclassification.classification, dbo.lmt_companies.cig_cid AS creditinfoid, dbo.lmt_companies.companyid AS Registration_No
FROM         dbo.lmt_companies INNER JOIN
                      dbo.lmt_companyclassification ON dbo.lmt_companies.companyclassificationid = dbo.lmt_companyclassification.classificationid
