﻿
CREATE view dbo.ros_CIIDNationalIndividualView as

SELECT  dbo.ros_NationalInterfaceView.Name AS NationalName, 
	dbo.ros_NationalInterfaceView.PostNumber AS NationalPostNumber, 
        dbo.ros_NationalInterfaceView.Address AS NationalAddress, 
	dbo.ros_IndividualView.CreditInfoID, 
	dbo.ros_IndividualView.FirstNameEN, 
        dbo.ros_IndividualView.FirstNameNative, 
	dbo.ros_IndividualView.LastNameEN, 
	dbo.ros_IndividualView.LastNameNative, 
        dbo.ros_IndividualView.AddressEN, 
	dbo.ros_IndividualView.AddressNative, 
	dbo.ros_IndividualView.CityEN, 
        CASE WHEN dbo.ros_IndividualView.CityNative IS NOT NULL THEN rtrim(dbo.ros_IndividualView.CityNative COLLATE SQL_Latin1_General_CP1_CI_AI) 
                      ELSE rtrim(dbo.ros_NationalInterfaceView.CityNative) END AS CityNative, 
	dbo.ros_IndividualView.CityID, 
	dbo.ros_IndividualView.PostalCode, 
        dbo.ros_IndividualView.CountryEN, 
	dbo.ros_IndividualView.CountryNative, 
	dbo.ros_IndividualView.CustomerIsCompany, 
        dbo.ros_IndividualView.CountryID, 
	dbo.np_Notes.Note,
	CASE WHEN dbo.ros_NationalInterfaceView.IDNumber IS NOT NULL THEN dbo.ros_NationalInterfaceView.IDNumber COLLATE SQL_Latin1_General_CP1_CI_AI 
                      ELSE rtrim(dbo.ros_IndividualView.IDNumber) END AS IDNumber, 
	CASE WHEN dbo.ros_NationalInterfaceView.NumberTypeID IS NOT NULL THEN dbo.ros_NationalInterfaceView.NumberTypeID 
                      ELSE rtrim(dbo.ros_IndividualView.NumberTypeID) END AS NumberTypeID
FROM         dbo.ros_NationalInterfaceView FULL OUTER JOIN
                      dbo.ros_IndividualView ON dbo.ros_NationalInterfaceView.NumberTypeID = dbo.ros_IndividualView.NumberTypeID AND 
                      dbo.ros_NationalInterfaceView.IDNumber COLLATE SQL_Latin1_General_CP1_CI_AI = dbo.ros_IndividualView.IDNumber LEFT OUTER JOIN
					  dbo.np_Notes ON dbo.ros_IndividualView.CreditInfoID = dbo.np_Notes.CreditInfoID
