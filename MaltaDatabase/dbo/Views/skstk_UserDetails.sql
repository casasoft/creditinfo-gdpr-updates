﻿CREATE VIEW dbo.skstk_UserDetails
AS
SELECT     dbo.au_users.id, dbo.au_users.CreditInfoID, dbo.au_users.UserName, dbo.au_users.SubscriberID, dbo.au_users.Email, dbo.au_users.UserType, 
                      dbo.au_users.HasWebServices, dbo.au_users.IsOpen, dbo.skstk_SubscriberStatus.AccessLevel, dbo.skstk_SubscriberStatus.Site, 
                      dbo.skstk_SubscriberStatus.LastErrorState
FROM         dbo.au_users INNER JOIN
                      dbo.skstk_SubscriberStatus ON dbo.au_users.SubscriberID = dbo.skstk_SubscriberStatus.SubscriberID


