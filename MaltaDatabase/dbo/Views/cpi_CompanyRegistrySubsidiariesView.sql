﻿CREATE VIEW dbo.cpi_CompanyRegistrySubsidiariesView
AS
SELECT     dbo.lmt_mfsa_companies.company_name, dbo.lmt_mfsa_companies.registration_no, dbo.lmt_mfsa_involvementstypes.involvement, 
                      dbo.lmt_mfsa_involvementstypes.ordering, dbo.lmt_mfsa_companystates.state, dbo.lmt_mfsa_involvedparties.ip_co_reg_no, 
                      dbo.lmt_mfsa_involvedparties.ip_id_no
FROM         dbo.lmt_mfsa_involvedparties LEFT OUTER JOIN
                      dbo.lmt_mfsa_involvements ON dbo.lmt_mfsa_involvedparties.id = dbo.lmt_mfsa_involvements.involved_party_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_involvementstypes ON 
                      dbo.lmt_mfsa_involvements.involve_type_id = dbo.lmt_mfsa_involvementstypes.involv_type_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_companies ON dbo.lmt_mfsa_involvements.company_id = dbo.lmt_mfsa_companies.company_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_companystates ON dbo.lmt_mfsa_companies.state_id = dbo.lmt_mfsa_companystates.state_id
WHERE     (dbo.lmt_mfsa_involvementstypes.ordering = '2')
