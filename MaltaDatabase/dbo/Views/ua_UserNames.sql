﻿
create view ua_UserNames as

SELECT DISTINCT au_Users.UserName, au_Users.Email, au_Users.IsOpen, au_Users.SubscriberID, np_Individual.CreditInfoID, np_Individual.FirstNameNative+' '+np_Individual.SurNameNative NameNative, 
np_Individual.FirstNameEN+' '+np_Individual.SurNameEN NameEN, np_Address.StreetNative, np_Address.StreetEN,np_IDNumbers.Number, 
np_City.NameNative CityNative, np_City.NameEN CityEN, np_Address.PostalCode 
FROM np_Individual LEFT OUTER JOIN np_Address ON np_Individual.CreditInfoID = np_Address.CreditInfoID LEFT OUTER JOIN 
np_IDNumbers ON np_Individual.CreditInfoID = np_IDNumbers.CreditInfoID LEFT Outer JOIN np_City on 
np_Address.CityID = np_City.CityID
left outer join au_users on np_individual.CreditInfoID = au_Users.CreditInfoID
where au_Users.UserName is not null

union

SELECT DISTINCT au_Users.UserName, au_Users.Email, au_Users.IsOpen, au_Users.SubscriberID, np_Companys.CreditInfoID, np_Companys.NameNative, np_Companys.NameEN,
np_Address.StreetNative, np_Address.StreetEN, np_IDNumbers.Number, 
np_City.NameNative CityNative, np_City.NameEN CityEN, np_Address.PostalCode 
FROM np_Companys LEFT OUTER JOIN np_Address ON np_Companys.CreditInfoID = np_Address.CreditInfoID 
LEFT OUTER JOIN np_IDNumbers ON np_Companys.CreditInfoID = np_IDNumbers.CreditInfoID 
LEFT Outer JOIN np_City on np_Address.CityID = np_City.CityID
left outer join au_users on np_Companys.CreditInfoID = au_Users.CreditInfoID
where au_Users.UserName is not null

