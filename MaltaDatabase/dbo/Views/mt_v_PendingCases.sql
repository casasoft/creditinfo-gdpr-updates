﻿CREATE VIEW mt_v_PendingCases
AS
SELECT     dbo.mt_PendingCases.DefendantID, dbo.mt_PendingCases.ID, .dbo.lmt_GetName(dbo.mt_PendingCases.DefendantID) as DefendantName, dbo.mt_PendingCases.PlaintiffID, 
                      .dbo.lmt_GetName(dbo.mt_PendingCases.PlaintiffID) as PlaintiffName,dbo.mt_PendingCases.DateOfWrit, dbo.mt_PendingCases.WritNumber, dbo.mt_PendingCases.ValueOfSuit, 
                      dbo.mt_PendingCases.Comments, dbo.np_InformationSource.NameEN AS CourtEN, dbo.np_InformationSource.NameNative AS CourtNative, 
                      dbo.mt_Adjudicators.AdjudicatorNameNative AS AdjudicatorNative, dbo.mt_Adjudicators.AdjudicatorNameEN AS AdjudicatorEN, 
                      dbo.mt_PendingCases.Court, dbo.mt_PendingCases.Adjudicator, dbo.mt_PendingCases.StatusID, dbo.mt_PendingCaseStatus.StateNative, 
                      dbo.mt_PendingCaseStatus.StateEN, dbo.mt_PendingCases.CurrencyCode
FROM         dbo.mt_PendingCases INNER JOIN
                      dbo.np_InformationSource ON dbo.mt_PendingCases.Court = dbo.np_InformationSource.InformationSourceID INNER JOIN
                      dbo.mt_Adjudicators ON dbo.mt_PendingCases.Adjudicator = dbo.mt_Adjudicators.ID INNER JOIN
                      dbo.mt_PendingCaseStatus ON dbo.mt_PendingCases.StatusID = dbo.mt_PendingCaseStatus.StatusID
