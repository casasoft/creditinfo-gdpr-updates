﻿create view cw_NationalRegistryView as

SELECT     dbo.np_IDNumbers.Number AS UniqueID, dbo.np_Companys.NameNative AS Name, dbo.np_Address.StreetNative+', '+dbo.np_City.NameNative AS Address, 
                      'true' [IsCompany]
FROM         dbo.np_Companys LEFT JOIN
                      dbo.np_IDNumbers ON dbo.np_Companys.CreditInfoID = dbo.np_IDNumbers.CreditInfoID LEFT JOIN
                      dbo.np_Address ON dbo.np_Companys.CreditInfoID = dbo.np_Address.CreditInfoID LEFT JOIN
		dbo.np_City on dbo.np_Address.CityID = dbo.np_City.CityID
UNION
SELECT     dbo.np_IDNumbers.Number AS UniqueID, dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative AS Name, 
                      dbo.np_Address.StreetNative+', '+dbo.np_City.NameNative AS Address, 'false' AS [IsCompany]
FROM         dbo.np_Individual LEFT JOIN
                      dbo.np_IDNumbers ON dbo.np_Individual.CreditInfoID = dbo.np_IDNumbers.CreditInfoID LEFT JOIN
                      dbo.np_Address ON dbo.np_Individual.CreditInfoID = dbo.np_Address.CreditInfoID LEFT JOIN
		dbo.np_City on dbo.np_Address.CityID = dbo.np_City.CityID
