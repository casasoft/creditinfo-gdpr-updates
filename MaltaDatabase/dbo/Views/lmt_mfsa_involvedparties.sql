﻿
create view lmt_mfsa_involvedparties(
  id, ip_surname, ip_name,
  title, ip_id_no, ip_passport_no,
  ip_co_name, ip_co_reg_no, nationality_id,
  ip_address_1, ip_address_2, locality_id,
  postcode, country_id, org_id_no
  ) as
  select
    involvedpartyid, surname, name,
    '', idcardpassport, '',
    companyname, companyregistrationno, nationalityid,
    involvedaddress1, involvedaddress2, locality,
    postcode, countryid, ''
  from lmt_involvedparties
