﻿CREATE VIEW dbo.ros_OrderSurveyView
AS
SELECT     dbo.ros_Order.ID, dbo.ros_Order.[Date], dbo.ros_Order.DispatchDeadline, dbo.ros_Order.CustomerReferenceNumber, dbo.ros_Order.RemarksEN, 
                      dbo.ros_Order.RemarksNative, dbo.ros_Order.Delivered, dbo.ros_Order.DeliverySpeedID, dbo.ros_Order.Deleted, 
                      dbo.np_UsageTypes.typeEN AS productEN, dbo.np_UsageTypes.typeNative AS productNative, dbo.np_UsageTypes.id AS productID, 
                      CustomerCreditInfoUserView.CreditInfoID, CustomerCreditInfoUserView.Type, CustomerCreditInfoUserView.NameNative, 
                      CustomerCreditInfoUserView.NameEN, CustomerCreditInfoUserView.FirstNameNative, CustomerCreditInfoUserView.FirstNameEN, 
                      CustomerCreditInfoUserView.SurNameNative, CustomerCreditInfoUserView.SurNameEN, CustomerCreditInfoUserView.Number, 
                      CustomerCreditInfoUserView.NumberTypeID, SubjectCreditInfoUserView.CreditInfoID AS SubjectCreditInfoID, 
                      SubjectCreditInfoUserView.Type AS SubjectType, SubjectCreditInfoUserView.NameNative AS SubjectNameNative, 
                      SubjectCreditInfoUserView.NameEN AS SubjectNameEN, SubjectCreditInfoUserView.FirstNameNative AS SubjectFirstNameNative, 
                      SubjectCreditInfoUserView.FirstNameEN AS SubjectFirstNameEN, SubjectCreditInfoUserView.SurNameNative AS SubjectSurNameNative, 
                      SubjectCreditInfoUserView.SurNameEN AS SubjectSurNameEN, SubjectCreditInfoUserView.Number AS SubjectNumber, 
                      SubjectCreditInfoUserView.NumberTypeID AS SubjectNumberTypeID
FROM         dbo.ros_Order INNER JOIN
                      dbo.np_UsageTypes ON dbo.ros_Order.np_UsageTypesID = dbo.np_UsageTypes.id INNER JOIN
                      dbo.ros_CreditInfoUserIDNumberView CustomerCreditInfoUserView ON 
                      dbo.ros_Order.CustomerCreditInfoID = CustomerCreditInfoUserView.CreditInfoID INNER JOIN
                      dbo.ros_CreditInfoUserIDNumberView SubjectCreditInfoUserView ON dbo.ros_Order.SubjectCreditInfoID = SubjectCreditInfoUserView.CreditInfoID

