﻿CREATE VIEW dbo.mt_v_CompanyRegistrationID
AS
SELECT     dbo.lmt_mfsa_companyclassification.classification, dbo.lmt_mfsa_companyclassification.ordering, 1 AS creditinfoid, 
                      dbo.lmt_mfsa_companies.registration_no
FROM         dbo.lmt_mfsa_companyclassification INNER JOIN
                      dbo.lmt_mfsa_companies ON dbo.lmt_mfsa_companyclassification.classification_id = dbo.lmt_mfsa_companies.classification_id
