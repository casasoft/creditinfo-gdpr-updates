﻿CREATE VIEW dbo.cpi_CompanyRegistryCapitalView
AS
SELECT     dbo.lmt_mfsa_companies.registration_no AS CompanySSN, dbo.lmt_mfsa_companycapital.fld_class AS ShareClass, 
                      dbo.lmt_mfsa_companycapital.auth_shr_capital AS AuthCap, dbo.lmt_mfsa_companycapital.nominal_value AS Nominal, 
                      dbo.lmt_mfsa_companycapital.issued_shr_capital AS Issued, dbo.lmt_mfsa_companycapital.percentage_pd_up AS PaidUp
FROM         dbo.lmt_mfsa_companycapital LEFT OUTER JOIN
                      dbo.lmt_mfsa_companies ON dbo.lmt_mfsa_companycapital.company_id = dbo.lmt_mfsa_companies.company_id
