﻿CREATE VIEW dbo.mt_v_capital2
AS
SELECT     dbo.lmt_mfsa_Companies.creditinfoid, dbo.lmt_mfsa_CompanyCapital.FLD_Class AS Class, 
                      dbo.lmt_mfsa_CompanyCapital.Auth_Shr_Cap AS Authorized, dbo.lmt_mfsa_CompanyCapital.Nominal_value AS Nominal, 
                      dbo.lmt_mfsa_CompanyCapital.Issued_Shr_Capital AS Issued, dbo.lmt_mfsa_CompanyCapital.Percentage_Pd_Up AS PaidUp, 
                      dbo.lmt_mfsa_Companies.Registration_No
FROM         dbo.lmt_mfsa_CompanyCapital INNER JOIN
                      dbo.lmt_mfsa_Companies ON dbo.lmt_mfsa_CompanyCapital.Company_ID = dbo.lmt_mfsa_Companies.Company_ID
