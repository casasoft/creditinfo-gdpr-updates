﻿

CREATE VIEW [dbo].[fo_mt_DirectorsAndMore]
AS
SELECT     TOP (100) PERCENT dbo.lmt_mfsa_involvedparties.id, dbo.lmt_mfsa_involvedparties.ip_surname, dbo.lmt_mfsa_involvedparties.ip_name, 
                      dbo.lmt_mfsa_involvedparties.title, dbo.lmt_mfsa_involvedparties.ip_id_no, dbo.lmt_mfsa_involvedparties.ip_passport_no, 
                      dbo.lmt_mfsa_involvedparties.ip_co_name, dbo.lmt_mfsa_involvedparties.ip_co_reg_no, dbo.lmt_mfsa_involvedparties.nationality_id, 
                      dbo.lmt_mfsa_involvedparties.ip_address_1, dbo.lmt_mfsa_involvedparties.ip_address_2, dbo.lmt_mfsa_involvedparties.locality_id, 
                      dbo.lmt_mfsa_involvedparties.postcode, dbo.lmt_mfsa_involvedparties.country_id, dbo.lmt_mfsa_involvedparties.org_id_no, 
                      dbo.lmt_mfsa_involvementstypes.involvement, dbo.lmt_mfsa_involvementstypes.ordering, dbo.lmt_mfsa_companies.company_id AS CompanyID, 
                      dbo.lmt_mfsa_companies.registration_no AS RegistrationID, dbo.lmt_mfsa_countries.country, dbo.lmt_mfsa_countries.local AS IsCountryLocal, 
                      dbo.lmt_mfsa_localities.locality, dbo.lmt_mfsa_localities.local AS IsLocalityLocal                      
FROM         dbo.lmt_mfsa_involvedparties LEFT OUTER JOIN
                      dbo.lmt_mfsa_countries ON dbo.lmt_mfsa_involvedparties.country_id = dbo.lmt_mfsa_countries.country_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_localities ON dbo.lmt_mfsa_involvedparties.locality_id = dbo.lmt_mfsa_localities.locality_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_involvements ON dbo.lmt_mfsa_involvedparties.id = dbo.lmt_mfsa_involvements.involved_party_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_involvementstypes ON dbo.lmt_mfsa_involvements.involve_type_id = dbo.lmt_mfsa_involvementstypes.involv_type_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_companies ON dbo.lmt_mfsa_involvements.company_id = dbo.lmt_mfsa_companies.company_id

	
