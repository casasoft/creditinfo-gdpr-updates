﻿CREATE VIEW dbo.cpi_CompanyRegistryLawyersIndividualView
AS
SELECT     dbo.lmt_mfsa_involvedparties.nationality_id, COALESCE (dbo.lmt_mfsa_involvedparties.ip_name, '') 
                      + ' ' + COALESCE (dbo.lmt_mfsa_involvedparties.ip_surname, '') AS Name, COALESCE (dbo.lmt_mfsa_involvedparties.ip_address_1, '') 
                      + ' ' + COALESCE (dbo.lmt_mfsa_involvedparties.ip_address_2, '') AS Address, dbo.lmt_mfsa_involvedparties.postcode AS PostalCode, 
                      dbo.lmt_mfsa_involvementstypes.ordering, dbo.lmt_mfsa_involvedparties.ip_id_no AS PersonSSN, '0' AS IsCompany, 
                      dbo.lmt_mfsa_companies.registration_no AS CompanySSN
FROM         dbo.lmt_mfsa_involvedparties LEFT OUTER JOIN
                      dbo.lmt_mfsa_localities ON dbo.lmt_mfsa_involvedparties.locality_id = dbo.lmt_mfsa_localities.locality_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_involvements ON dbo.lmt_mfsa_involvedparties.id = dbo.lmt_mfsa_involvements.involved_party_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_involvementstypes ON 
                      dbo.lmt_mfsa_involvements.involve_type_id = dbo.lmt_mfsa_involvementstypes.involv_type_id LEFT OUTER JOIN
                      dbo.lmt_mfsa_companies ON dbo.lmt_mfsa_involvements.company_id = dbo.lmt_mfsa_companies.company_id
WHERE     (dbo.lmt_mfsa_involvementstypes.ordering = '4') AND (dbo.lmt_mfsa_involvedparties.ip_id_no IS NOT NULL)
