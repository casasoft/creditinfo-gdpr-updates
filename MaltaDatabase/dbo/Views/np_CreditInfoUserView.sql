﻿CREATE VIEW dbo.np_CreditInfoUserView
AS
SELECT     dbo.np_CreditInfoUser.CreditInfoID AS CreditInfoID, dbo.np_CreditInfoUser.Type AS Type, CASE WHEN dbo.np_Companys.NameNative IS NOT NULL 
                      THEN rtrim(dbo.np_Companys.NameNative) ELSE rtrim(dbo.np_Individual.FirstNameNative) + ' ' + rtrim(dbo.np_Individual.SurNameNative) 
                      END AS ClaimOwnerNameNative, CASE WHEN dbo.np_Companys.NameNative IS NOT NULL THEN rtrim(dbo.np_Companys.NameEN) 
                      ELSE rtrim(dbo.np_Individual.FirstNameEN) + ' ' + rtrim(dbo.np_Individual.SurNameEN) END AS ClaimOwnerNameEN
FROM         dbo.np_CreditInfoUser LEFT OUTER JOIN
                      dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Individual ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID

