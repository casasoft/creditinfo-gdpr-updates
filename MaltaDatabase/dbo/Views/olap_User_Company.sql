﻿CREATE VIEW dbo.olap_User_Company
AS
SELECT     dbo.au_users.id, dbo.au_users.CreditInfoID, dbo.au_users.UserName, dbo.au_users.UserType, dbo.au_users.IsOpen, 
                      CASE WHEN np_CIU_user.Type = 1 THEN np_Cmp_user.NameNative ELSE np_Ind_user.FirstNameNative + ' ' + np_Ind_user.SurNameNative END AS NameUser,
                       CASE WHEN np_CIU_subscriber.Type = 1 THEN np_Cmp_subscriber.NameNative ELSE np_Ind_subscriber.FirstNameNative + ' ' + np_Ind_subscriber.SurNameNative
                       END AS NameSubscriber
FROM         dbo.np_CreditInfoUser np_CIU_subscriber INNER JOIN
                      dbo.np_CreditInfoUser np_CIU_user INNER JOIN
                      dbo.au_users ON np_CIU_user.CreditInfoID = dbo.au_users.CreditInfoID ON 
                      np_CIU_subscriber.CreditInfoID = dbo.au_users.SubscriberID LEFT OUTER JOIN
                      dbo.np_Companys np_Cmp_subscriber ON np_CIU_subscriber.CreditInfoID = np_Cmp_subscriber.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Individual np_Ind_subscriber ON np_CIU_subscriber.CreditInfoID = np_Ind_subscriber.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Individual np_Ind_user ON np_CIU_user.CreditInfoID = np_Ind_user.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Companys np_Cmp_user ON np_CIU_user.CreditInfoID = np_Cmp_user.CreditInfoID
