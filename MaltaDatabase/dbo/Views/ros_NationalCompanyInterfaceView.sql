﻿CREATE VIEW dbo.ros_NationalCompanyInterfaceView
AS
SELECT     dbo.lmt_mfsa_Companies.Registration_No AS IDNumber, dbo.lmt_mfsa_Companies.postcode AS PostalCode, 
                      dbo.lmt_mfsa_Companies.Company_Name AS Name, dbo.lmt_mfsa_Companies.Address_1 + ' ' + dbo.lmt_mfsa_Companies.Address_2 AS Address, 
                      '' AS FaxNumber, '' AS PhoneNumber, dbo.lmt_mfsa_Localities.locality AS CityNative, 1 AS NumberTypeID, 
                      dbo.lmt_mfsa_Companies.Registration_Date AS RegDate, dbo.lmt_mfsa_Countries.Country AS CountryNative

FROM         dbo.lmt_mfsa_Companies INNER JOIN
                      dbo.lmt_mfsa_Countries ON dbo.lmt_mfsa_Companies.Country_ID = dbo.lmt_mfsa_Countries.Country_ID LEFT OUTER JOIN
                      dbo.lmt_mfsa_Localities ON dbo.lmt_mfsa_Companies.Locality_ID = dbo.lmt_mfsa_Localities.Locality_ID
