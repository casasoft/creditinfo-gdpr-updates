﻿CREATE VIEW dbo.vw_ToUnregisterFiveYear
AS
SELECT     a.StatusID AS state, a.UnregistedDate AS [date]
FROM         dbo.np_Claims a INNER JOIN
                      dbo.np_CreditInfoUser b ON a.CreditInfoID = b.CreditInfoID INNER JOIN
                      dbo.np_CaseTypes c ON a.ClaimTypeID = c.CaseTypeID
WHERE     (b.Type = 2) AND (c.CaseID = 1) AND (a.StatusID = 4) AND (DATEDIFF(year, CASE WHEN a.ClaimDate IS NULL THEN a.RegDate ELSE a.ClaimDate END, GETDATE()) >= 5)

