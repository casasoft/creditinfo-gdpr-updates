﻿CREATE VIEW ua_UserNames_extended
AS
SELECT DISTINCT 
                      dbo.au_users.UserName, dbo.au_users.id, dbo.au_users.Email, dbo.au_users.IsOpen, dbo.au_users.SubscriberID, dbo.au_users.UserType, 
                      dbo.np_Individual.CreditInfoID, dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative AS NameNative, 
                      dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN AS NameEN, dbo.np_Address.StreetNative, dbo.np_Address.StreetEN, 
                      dbo.np_IDNumbers.Number, dbo.np_City.NameNative AS CityNative, dbo.np_City.NameEN AS CityEN, dbo.np_Address.PostalCode, 
                      dbo.ua_SubscriberName.NameNative AS SubscriberNameNative, dbo.ua_SubscriberName.NameEN AS SubscriberNameEN, 
                      dbo.c_au_department.NameNative AS DepartmentNT, dbo.c_au_department.NameEN AS DepartmentEN
FROM         dbo.c_au_department INNER JOIN
                      dbo.au_users ON dbo.c_au_department.DepartmentID = dbo.au_users.DepartmentID INNER JOIN
                      dbo.ua_SubscriberName ON dbo.c_au_department.CreditInfoID = dbo.ua_SubscriberName.CreditInfoID RIGHT OUTER JOIN
                      dbo.np_Individual LEFT OUTER JOIN
                      dbo.np_Address ON dbo.np_Individual.CreditInfoID = dbo.np_Address.CreditInfoID LEFT OUTER JOIN
                      dbo.np_IDNumbers ON dbo.np_Individual.CreditInfoID = dbo.np_IDNumbers.CreditInfoID LEFT OUTER JOIN
                      dbo.np_City ON dbo.np_Address.CityID = dbo.np_City.CityID ON dbo.au_users.CreditInfoID = dbo.np_Individual.CreditInfoID
WHERE     (dbo.au_users.UserName IS NOT NULL)
UNION
SELECT DISTINCT 
                      dbo.au_users.UserName, dbo.au_users.id, dbo.au_users.Email, dbo.au_users.IsOpen, dbo.au_users.SubscriberID, dbo.au_users.UserType, 
                      dbo.np_Companys.CreditInfoID, dbo.np_Companys.NameNative, dbo.np_Companys.NameEN, dbo.np_Address.StreetNative, dbo.np_Address.StreetEN, 
                      dbo.np_IDNumbers.Number, dbo.np_City.NameNative AS CityNative, dbo.np_City.NameEN AS CityEN, dbo.np_Address.PostalCode, 
                      dbo.ua_SubscriberName.NameNative AS SubscriberNameNative, dbo.ua_SubscriberName.NameEN AS SubscriberNameEN, 
                      dbo.c_au_department.NameNative AS DepartmentNT, dbo.c_au_department.NameEN AS DepartmentEN
FROM         dbo.c_au_department INNER JOIN
                      dbo.au_users ON dbo.c_au_department.DepartmentID = dbo.au_users.DepartmentID INNER JOIN
                      dbo.ua_SubscriberName ON dbo.c_au_department.CreditInfoID = dbo.ua_SubscriberName.CreditInfoID RIGHT OUTER JOIN
                      dbo.np_Companys LEFT OUTER JOIN
                      dbo.np_Address ON dbo.np_Companys.CreditInfoID = dbo.np_Address.CreditInfoID LEFT OUTER JOIN
                      dbo.np_IDNumbers ON dbo.np_Companys.CreditInfoID = dbo.np_IDNumbers.CreditInfoID LEFT OUTER JOIN
                      dbo.np_City ON dbo.np_Address.CityID = dbo.np_City.CityID ON dbo.au_users.CreditInfoID = dbo.np_Companys.CreditInfoID
WHERE     (dbo.au_users.UserName IS NOT NULL)
