﻿CREATE VIEW dbo.olap_Address
AS
SELECT     dbo.np_CreditInfoUser.CreditInfoID, dbo.np_Address.StreetEN, dbo.np_Address.StreetNumber, dbo.np_Address.PostalCode, 
                      dbo.cpi_Countries.CountryID, dbo.cpi_Countries.NameEN AS CountryName, dbo.np_City.CityID, dbo.np_City.NameEN AS CityName, 
                      dbo.cpi_Continents.ContinentID, dbo.cpi_Continents.NameEN AS ContinentName
FROM         dbo.np_CreditInfoUser LEFT OUTER JOIN
                      dbo.np_Address ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Address.CreditInfoID LEFT OUTER JOIN
                      dbo.cpi_Countries LEFT OUTER JOIN
                      dbo.cpi_Continents ON dbo.cpi_Countries.ContinentID = dbo.cpi_Continents.ContinentID ON 
                      dbo.np_Address.CountryID = dbo.cpi_Countries.CountryID LEFT OUTER JOIN
                      dbo.np_City ON dbo.np_Address.CityID = dbo.np_City.CityID
