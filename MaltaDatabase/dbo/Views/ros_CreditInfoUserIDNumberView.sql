﻿CREATE VIEW dbo.ros_CreditInfoUserIDNumberView
AS
SELECT     dbo.np_CreditInfoUser.CreditInfoID, dbo.np_CreditInfoUser.Type, dbo.np_Companys.NameNative, dbo.np_Companys.NameEN, 
                      dbo.np_Individual.FirstNameNative, dbo.np_Individual.FirstNameEN, dbo.np_Individual.SurNameNative, dbo.np_Individual.SurNameEN, 
                      dbo.np_IDNumbers.Number, dbo.np_IDNumbers.NumberTypeID
FROM         dbo.np_CreditInfoUser LEFT OUTER JOIN
                      dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Individual ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID LEFT OUTER JOIN
                      dbo.np_IDNumbers ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_IDNumbers.CreditInfoID

