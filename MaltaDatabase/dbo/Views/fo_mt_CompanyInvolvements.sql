﻿
CREATE VIEW [dbo].[fo_mt_CompanyInvolvements]

AS

                      
                      SELECT DISTINCT x.company_name, x.registration_no, x.involvement, x.ordering, x.state, x.ip_co_reg_no, x.num, t.ip_id_no
FROM         (SELECT     dbo.lmt_mfsa_companies.company_name, dbo.lmt_mfsa_companies.registration_no, 
											  dbo.lmt_mfsa_involvementstypes.involvement, 
                                              dbo.lmt_mfsa_involvementstypes.ordering, 
                                              dbo.lmt_mfsa_companystates.state, 
											  dbo.lmt_mfsa_involvedparties.ip_co_reg_no, 
                                              dbo.lmt_mfsa_involvedparties.ip_id_no AS num
                       FROM          dbo.lmt_mfsa_involvedparties INNER JOIN
                                              dbo.lmt_mfsa_involvements ON dbo.lmt_mfsa_involvedparties.id = dbo.lmt_mfsa_involvements.involved_party_id LEFT OUTER JOIN
                                              dbo.lmt_mfsa_involvementstypes ON dbo.lmt_mfsa_involvements.involve_type_id = dbo.lmt_mfsa_involvementstypes.involv_type_id LEFT OUTER JOIN
                                              dbo.lmt_mfsa_companies ON dbo.lmt_mfsa_involvements.company_id = dbo.lmt_mfsa_companies.company_id LEFT OUTER JOIN
                                              dbo.lmt_mfsa_companystates ON dbo.lmt_mfsa_companies.state_id = dbo.lmt_mfsa_companystates.state_id) AS x CROSS JOIN
                          (SELECT     y.Number, z.ip_id_no
                            FROM          dbo.np_IDNumbers AS y INNER JOIN
                                                       (SELECT     ip_id_no, CreditInfoID
                                                         FROM          (SELECT     CreditInfoID, Number AS ip_id_no, Number
                                                                                 FROM          dbo.np_IDNumbers) AS x_1) AS z ON y.CreditInfoID = z.CreditInfoID) AS t
WHERE     (x.num IN (t.Number) or x.ip_co_reg_no in (t.Number))






