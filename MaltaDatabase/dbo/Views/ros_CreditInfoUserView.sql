﻿CREATE VIEW dbo.ros_CreditInfoUserView
AS
SELECT     dbo.np_CreditInfoUser.CreditInfoID, dbo.np_CreditInfoUser.Type, dbo.np_Companys.NameNative, dbo.np_Companys.NameEN, 
                      dbo.np_Individual.FirstNameNative, dbo.np_Individual.FirstNameEN, dbo.np_Individual.SurNameNative, dbo.np_Individual.SurNameEN
FROM         dbo.np_CreditInfoUser LEFT OUTER JOIN
                      dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Individual ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID

