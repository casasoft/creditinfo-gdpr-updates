﻿CREATE VIEW dbo.ros_NationalInterfaceView
AS
SELECT     ssno AS IDNumber, '' AS Sex, '' AS DateOfBirth, name AS Name, '' AS PostNumber, street + ' ' + street_floor AS Address, location AS CityNative, 
                      1 AS NumberTypeID
FROM         dbo.mt_individuals
