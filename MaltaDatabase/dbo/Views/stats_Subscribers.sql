﻿
create VIEW dbo.stats_Subscribers
	AS
	SELECT     dbo.au_users.id, dbo.au_users.Creditinfoid,  
	                      dbo.au_Subscribers.IsOpen AS SubsIsOpen, dbo.au_users.IsOpen AS UserIsOpen, dbo.np_IDNumbers.Number, 
	                      CASE WHEN np_CreditInfoUser.Type = 1 THEN dbo.np_Companys.NameEN ELSE dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN
	                       END AS NameEN, 
	                      CASE WHEN np_CreditInfoUser.Type = 1 THEN dbo.np_Companys.NameNative ELSE dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative
	                       END AS NameNative, au_users.subscriberId, au_users.Username, np_Usage.IsBillable
	FROM         dbo.np_Individual RIGHT OUTER JOIN
	                      dbo.np_IDNumbers RIGHT OUTER JOIN
	                      dbo.au_users INNER JOIN
	                      dbo.np_CreditInfoUser ON dbo.au_users.SubscriberID = dbo.np_CreditInfoUser.CreditInfoID INNER JOIN
	                      dbo.au_Subscribers ON dbo.au_users.SubscriberID = dbo.au_Subscribers.CreditInfoID RIGHT OUTER JOIN
	                      dbo.np_Usage ON dbo.au_users.id = dbo.np_Usage.UserID LEFT OUTER JOIN
	                      dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID ON 
	                      dbo.np_IDNumbers.CreditInfoID = dbo.np_CreditInfoUser.CreditInfoID ON dbo.np_Individual.CreditInfoID = dbo.np_CreditInfoUser.CreditInfoID
