﻿CREATE VIEW dbo.ros_IndividualView
AS
SELECT DISTINCT 
                      dbo.np_Individual.CreditInfoID, dbo.np_IDNumbers.Number AS IDNumber, dbo.np_Individual.FirstNameEN, dbo.np_Individual.FirstNameNative, 
                      dbo.np_Individual.SurNameEN AS LastNameEN, dbo.np_Individual.SurNameNative AS LastNameNative, dbo.np_Address.StreetEN AS AddressEN, 
                      dbo.np_Address.StreetNative AS AddressNative, dbo.np_City.NameEN AS CityEN, dbo.np_City.NameNative AS CityNative, dbo.np_City.CityID, 
                      dbo.np_Address.PostalCode, dbo.cpi_Countries.NameEN AS CountryEN, dbo.cpi_Countries.NameNative AS CountryNative, 
                      dbo.cpi_Countries.CountryID, 'False' AS CustomerIsCompany, dbo.np_NumberTypes.NumberTypeID
FROM         dbo.cpi_Countries INNER JOIN
                      dbo.np_City INNER JOIN
                      dbo.np_Address ON dbo.np_City.CityID = dbo.np_Address.CityID ON dbo.cpi_Countries.CountryID = dbo.np_Address.CountryID RIGHT OUTER JOIN
                      dbo.np_Individual ON dbo.np_Address.CreditInfoID = dbo.np_Individual.CreditInfoID LEFT OUTER JOIN
                      dbo.np_NumberTypes INNER JOIN
                      dbo.np_IDNumbers ON dbo.np_NumberTypes.NumberTypeID = dbo.np_IDNumbers.NumberTypeID ON 
                      dbo.np_Individual.CreditInfoID = dbo.np_IDNumbers.CreditInfoID

