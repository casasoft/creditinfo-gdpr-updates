﻿Create view cpi_NationalCompanyInterfaceView as
SELECT     kt AS IDNumber, pnr AS PostalCode, heiti AS Name, heimili_thgf AS Address, fax AS FaxNumber, simi AS PhoneNumber, postholf AS POBox, 
                      vsknr AS VAT, 1 AS NumberTypeID
FROM         Þjóðskrá.dbo.thjod_fyr
