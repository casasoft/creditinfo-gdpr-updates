﻿CREATE VIEW dbo.v_mt_inv_h
AS
SELECT     dbo.lmt_involvedparties.*, dbo.lmt_involvementtypes.involvement AS Involvement, dbo.lmt_involvedpartytocompanylink.involvedtypeid AS Ordering, 
                      dbo.lmt_companies.companyid AS CompanyID, dbo.lmt_companies.companyid AS RegistrationID, dbo.lmt_countries.country AS Country
FROM         dbo.lmt_involvedpartytocompanylink LEFT OUTER JOIN
                      dbo.lmt_involvedparties ON dbo.lmt_involvedpartytocompanylink.involvedpartyid = dbo.lmt_involvedparties.involvedpartyid LEFT OUTER JOIN
                      dbo.lmt_companies ON 
                      dbo.lmt_involvedpartytocompanylink.companyid COLLATE SQL_Latin1_General_CP1_CI_AS = dbo.lmt_companies.companyid LEFT OUTER JOIN
                      dbo.lmt_involvementtypes ON dbo.lmt_involvedpartytocompanylink.involvedtypeid = dbo.lmt_involvementtypes.involvementtypeid LEFT OUTER JOIN
                      dbo.lmt_countries ON dbo.lmt_involvedparties.countryid = dbo.lmt_countries.countryid
