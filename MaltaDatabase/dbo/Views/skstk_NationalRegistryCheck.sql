﻿CREATE VIEW dbo.skstk_NationalRegistryCheck
AS
SELECT     kt AS Kennitala, heiti AS Nafn, heimili_thgf AS Heimilisfang, CreditInfoID AS CreditInfoID, 'True' AS IsCompany
FROM         dbo.ua_NationalRegistryCompanies
UNION
SELECT     kt AS Kennitala, Nafn AS Nafn, hfang_thgf AS Heimilisfang, CreditInfoID AS CreditInfoID, 'False' AS IsCompany
FROM         ua_NationalRegistryIndividuals



