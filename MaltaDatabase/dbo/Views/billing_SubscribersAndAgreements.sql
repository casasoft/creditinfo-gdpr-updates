﻿CREATE VIEW dbo.billing_SubscribersAndAgreements
AS
SELECT     dbo.skstk_SubscriberInformation.SubscriberID, dbo.skstk_SubscriberInformation.CreditInfoID, dbo.skstk_SubscriberInformation.IsOpen, 
                      dbo.skstk_SubscriberInformation.NameNative, dbo.skstk_SubscriberInformation.NameEN, dbo.skstk_SubscriberInformation.StreetNative, 
                      dbo.skstk_SubscriberInformation.StreetEN, dbo.skstk_SubscriberInformation.CityNative, dbo.skstk_SubscriberInformation.CityEN, 
                      dbo.billing_Agreement.AgreementID, dbo.billing_Agreement.AgreementNumber, dbo.billing_Agreement.GeneralDescription, 
                      dbo.billing_Agreement.BeginDate, dbo.billing_Agreement.EndDate, dbo.billing_Agreement.Currency, dbo.billing_Agreement.SignerCIID, 
                      dbo.au_users.UserName AS InsertedBy, dbo.np_IDNumbers.Number
FROM         dbo.np_IDNumbers RIGHT OUTER JOIN
                      dbo.skstk_SubscriberInformation ON dbo.np_IDNumbers.CreditInfoID = dbo.skstk_SubscriberInformation.CreditInfoID LEFT OUTER JOIN
                      dbo.au_users INNER JOIN
                      dbo.billing_Agreement ON dbo.au_users.id = dbo.billing_Agreement.InsertedBy ON 
                      dbo.skstk_SubscriberInformation.CreditInfoID = dbo.billing_Agreement.SubscriberID
WHERE     (ISNULL(dbo.np_IDNumbers.NumberTypeID,0) = (SELECT ISNULL(Min(NumberTypeID),0) FROM dbo.np_IDNumbers IDN WHERE IDN.CreditInfoID=dbo.skstk_SubscriberInformation.CreditInfoID)) 


