﻿CREATE VIEW dbo.skstk_SubscriberInformation
AS
SELECT     dbo.au_Subscribers.ID AS SubscriberID, dbo.au_Subscribers.CreditInfoID, dbo.au_Subscribers.IsOpen, 
                      dbo.np_Companys.NameNative AS NameNative, dbo.np_Companys.NameEN AS NameEN, dbo.np_Address.StreetNative, dbo.np_Address.StreetEN, 
                      dbo.np_Address.PostalCode + ' ' + dbo.np_City.NameNative AS CityNative, 
                      dbo.np_Address.PostalCode COLLATE SQL_Latin1_General_CP1_CI_AI + ' ' + dbo.np_City.NameEN AS CityEN
FROM         dbo.np_City RIGHT OUTER JOIN
                      dbo.np_Address ON dbo.np_City.CityID = dbo.np_Address.CityID RIGHT OUTER JOIN
                      dbo.au_Subscribers INNER JOIN
                      dbo.np_Companys ON dbo.au_Subscribers.CreditInfoID = dbo.np_Companys.CreditInfoID ON 
                      dbo.np_Address.CreditInfoID = dbo.au_Subscribers.CreditInfoID
UNION
SELECT     dbo.au_Subscribers.ID AS SubscriberID, dbo.au_Subscribers.CreditInfoID, dbo.au_Subscribers.IsOpen, 
                      dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative AS NameNative, 
                      dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN AS NameEN, dbo.np_Address.StreetNative, dbo.np_Address.StreetEN, 
                      dbo.np_Address.PostalCode + ' ' + dbo.np_City.NameNative AS CityNative, 
                      dbo.np_Address.PostalCode COLLATE SQL_Latin1_General_CP1_CI_AI + ' ' + dbo.np_City.NameEN AS CityEN
FROM         dbo.np_City RIGHT OUTER JOIN
                      dbo.np_Address ON dbo.np_City.CityID = dbo.np_Address.CityID RIGHT OUTER JOIN
                      dbo.au_Subscribers INNER JOIN
                      dbo.np_Individual ON dbo.au_Subscribers.CreditInfoID = dbo.np_Individual.CreditInfoID ON 
                      dbo.np_Address.CreditInfoID = dbo.au_Subscribers.CreditInfoID
