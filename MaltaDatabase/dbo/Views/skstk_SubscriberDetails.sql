﻿CREATE VIEW dbo.skstk_SubscriberDetails
AS
SELECT     dbo.skstk_SubscriberInformation.*, dbo.skstk_SubscriberStatus.AccessLevel AS AccessLevel, 
                      dbo.skstk_AccessLevels.DescriptionNative AS AccessDescriptionNative, dbo.skstk_AccessLevels.DescriptionEN AS AccessDescriptionEN, 
                      dbo.skstk_SubscriberStatus.Site AS ServerSite, dbo.skstk_SubscriberStatus.LastErrorState AS LastErrorMsgCode, 
                      dbo.skstk_ErrorMessages.MessageNative AS ErrorMessageNative, dbo.skstk_ErrorMessages.MessageEN AS ErrorMessageEN, 
                      dbo.skstk_SubscriberStatus.CreateDate AS skstkCreateDate
FROM         dbo.skstk_ErrorMessages INNER JOIN
                      dbo.skstk_SubscriberStatus ON dbo.skstk_ErrorMessages.Id = dbo.skstk_SubscriberStatus.LastErrorState INNER JOIN
                      dbo.skstk_AccessLevels ON dbo.skstk_SubscriberStatus.AccessLevel = dbo.skstk_AccessLevels.AccessLevel INNER JOIN
                      dbo.skstk_SubscriberInformation ON dbo.skstk_SubscriberStatus.SubscriberID = dbo.skstk_SubscriberInformation.CreditInfoID

