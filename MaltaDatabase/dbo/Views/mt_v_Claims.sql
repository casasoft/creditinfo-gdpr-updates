﻿
CREATE VIEW [dbo].[mt_v_Claims]
AS
SELECT     dbo.np_Claims.CreditInfoID, dbo.np_Claims.RegDate, dbo.np_Claims.StatusID, dbo.np_Claims.CaseNr, dbo.np_Claims.Amount, 
                      dbo.np_Claims.CurrencyID, dbo.np_Claims.ClaimDate, dbo.np_Claims.AnnouncementDate, dbo.np_Claims.UnregistedDate, dbo.np_Claims.DelayDate, 
                      dbo.np_Claims.PaymentDate, dbo.np_Claims.RegisterID, dbo.np_Claims.Valid, dbo.np_Claims.RegisterCommentNative, 
                      dbo.np_Claims.RegisterCommentEN, dbo.np_Claims.LastUpdate, dbo.np_Claims.RegisterInternalCommentNative, 
                      dbo.np_Claims.RegisterInternalCommentEN, dbo.np_Claims.ClaimOwnerCIID, dbo.np_CaseTypes.TypeEN, dbo.np_CaseTypes.TypeNative, 
                      dbo.np_InformationSource.NameEN AS InfoSourceEN, dbo.np_InformationSource.NameNative AS InfoSourceNative, dbo.np_Claims.ID, 
                      dbo.np_CaseTypes.DescriptionEN, dbo.np_CaseTypes.DescriptionNative, dbo.np_Claims.ClaimTypeID
FROM         dbo.np_Claims LEFT OUTER JOIN
                      dbo.np_CaseTypes ON dbo.np_Claims.ClaimTypeID = dbo.np_CaseTypes.CaseTypeID LEFT OUTER JOIN
                      dbo.np_InformationSource ON dbo.np_Claims.InformationSourceID = dbo.np_InformationSource.InformationSourceID

