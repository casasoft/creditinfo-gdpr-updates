﻿Create view ua_NationalRegistryIndividuals as
SELECT     Þjóðskrá.dbo.thjod_eins.*, dbo.ua_IDNumberCreditInfoIDs.CreditInfoID AS CreditInfoID
FROM         Þjóðskrá.dbo.thjod_eins LEFT OUTER JOIN
                      dbo.ua_IDNumberCreditInfoIDs ON Þjóðskrá.dbo.thjod_eins.kt COLLATE SQL_Latin1_General_CP1_CI_AI = dbo.ua_IDNumberCreditInfoIDs.Number
