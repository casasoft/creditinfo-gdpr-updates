﻿CREATE VIEW dbo.olap_UserCompanies
AS
SELECT     dbo.np_CreditInfoUser.CreditInfoID, dbo.np_CreditInfoUser.DateCreated, dbo.np_Companys.NameEN, dbo.np_Companys.FuncID, 
                      dbo.np_CompanyFunction.DescriptionShortEN
FROM         dbo.np_CompanyFunction RIGHT OUTER JOIN
                      dbo.np_Companys ON dbo.np_CompanyFunction.FuncID COLLATE Icelandic_CI_AS = dbo.np_Companys.FuncID COLLATE Icelandic_CI_AS RIGHT OUTER JOIN
                      dbo.np_CreditInfoUser ON dbo.np_Companys.CreditInfoID = dbo.np_CreditInfoUser.CreditInfoID


