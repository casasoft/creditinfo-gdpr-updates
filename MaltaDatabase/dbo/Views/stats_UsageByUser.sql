﻿CREATE VIEW dbo.stats_UsageByUser
	AS
	SELECT     dbo.np_Usage.id, dbo.np_Usage.Creditinfoid, dbo.np_Usage.query_type, dbo.np_Usage.UserID, dbo.np_Usage.created, 
	                      dbo.au_Subscribers.IsOpen AS SubsIsOpen, dbo.au_users.UserName, dbo.au_users.IsOpen AS UserIsOpen, 
	                      dbo.np_IDNumbers.Number AS UserIdNumber, np_IDNumbers_1.Number, 
	                      CASE WHEN np_CreditInfoUser.Type = 1 THEN dbo.np_Companys.NameEN ELSE dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN
	                       END AS User_NameEN, 
	                      CASE WHEN np_CreditInfoUser.Type = 1 THEN dbo.np_Companys.NameNative ELSE dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative
	                       END AS User_NameNative, au_users.subscriberId, 
	                      CASE WHEN np_CreditInfoUser_1.Type = 1 THEN np_Companys_1.NameEN ELSE np_Individual_1.FirstNameEN + ' ' + np_Individual_1.SurNameEN END
	                       AS NameEN, 
	                      CASE WHEN np_CreditInfoUser_1.Type = 1 THEN np_Companys_1.NameNative ELSE np_Individual_1.FirstNameNative + ' ' + np_Individual_1.SurNameNative
	                       END AS NameNative, np_Usage.IsBillable
	FROM         dbo.np_Companys np_Companys_1 RIGHT OUTER JOIN
	                      dbo.np_Individual np_Individual_1 RIGHT OUTER JOIN
	                      dbo.np_CreditInfoUser np_CreditInfoUser_1 ON np_Individual_1.CreditInfoID = np_CreditInfoUser_1.CreditInfoID ON 
	                      np_Companys_1.CreditInfoID = np_CreditInfoUser_1.CreditInfoID RIGHT OUTER JOIN
	                      dbo.au_Subscribers INNER JOIN
	                      dbo.np_IDNumbers np_IDNumbers_1 INNER JOIN
	                      dbo.au_users INNER JOIN
	                      dbo.np_CreditInfoUser ON dbo.au_users.CreditInfoID = dbo.np_CreditInfoUser.CreditInfoID ON 
	                      np_IDNumbers_1.CreditInfoID = dbo.au_users.SubscriberID ON dbo.au_Subscribers.CreditInfoID = dbo.au_users.SubscriberID ON 
	                      np_CreditInfoUser_1.CreditInfoID = dbo.au_Subscribers.CreditInfoID LEFT OUTER JOIN
	                      dbo.np_IDNumbers ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_IDNumbers.CreditInfoID LEFT OUTER JOIN
	                      dbo.np_Individual ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID LEFT OUTER JOIN
	                      dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID RIGHT OUTER JOIN
	                      dbo.np_Usage ON dbo.au_users.id = dbo.np_Usage.UserID
	
	