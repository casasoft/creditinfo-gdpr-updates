﻿CREATE VIEW dbo.cw_UniqueCombinedToNationalRegistry
AS
SELECT     dbo.cw_WatchUniqueID.*, dbo.cw_NationalRegistryView.Name AS Name, dbo.cw_NationalRegistryView.Address AS Address
FROM         dbo.cw_NationalRegistryView INNER JOIN
                      dbo.cw_WatchUniqueID ON dbo.cw_NationalRegistryView.UniqueID = dbo.cw_WatchUniqueID.UniqueID
