﻿CREATE VIEW dbo.skstk_UserDetailsWithName
AS
SELECT     dbo.skstk_UserDetails.*, dbo.np_Companys.NameNative AS NameNative, dbo.np_Companys.NameEN AS NameEN
FROM         dbo.skstk_UserDetails INNER JOIN
                      dbo.np_Companys ON dbo.skstk_UserDetails.CreditInfoID = dbo.np_Companys.CreditInfoID
UNION
SELECT     dbo.skstk_UserDetails.*, dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative AS NameNative, 
                      dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN AS NameEN
FROM         dbo.skstk_UserDetails INNER JOIN
                      dbo.np_Individual ON dbo.skstk_UserDetails.CreditInfoID = dbo.np_Individual.CreditInfoID

