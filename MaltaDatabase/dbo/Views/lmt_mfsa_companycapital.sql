﻿CREATE view [dbo].[lmt_mfsa_companycapital] (
  cap_id, company_id, fld_class,
  auth_shr_capital, nominal_value, issued_shr_capital,
  percentage_pd_up
  ) as
  select a.capitalid,a.companyid,a.class,
  b.totalauthshares*a.nominalvalue,a.nominalvalue,b.totalissuedshares*a.nominalvalue,
  ''
  from lmt_companycapital_authorized a, lmt_companies b
  where a.companyid=b.companyid and a.rowid=(select min(rowid) from lmt_companycapital_authorized x where x.companyid=a.companyid)

