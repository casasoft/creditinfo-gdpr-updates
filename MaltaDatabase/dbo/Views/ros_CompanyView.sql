﻿CREATE VIEW dbo.ros_CompanyView
AS
SELECT     dbo.np_CreditInfoUser.CreditInfoID, dbo.np_IDNumbers.Number AS IDNumber, dbo.np_IDNumbers.NumberTypeID, dbo.np_Companys.NameEN, 
                      dbo.np_Companys.NameNative, dbo.np_CreditInfoUser.Email, dbo.ros_AdditionalCustomerInfo.Email1, dbo.ros_AdditionalCustomerInfo.Email2, 
                      dbo.np_Address.StreetEN AS AddressEN, dbo.np_Address.StreetNative AS AddressNative, dbo.np_City.NameEN AS CityEN, 
                      dbo.np_City.NameNative AS CityNative, dbo.np_City.CityID, dbo.np_Address.PostalCode, dbo.cpi_Countries.NameEN AS CountryEN, 
                      dbo.cpi_Countries.NameNative AS CountryNative, dbo.cpi_Countries.CountryID, dbo.np_Companys.Established, dbo.np_Companys.LastContacted, 
                      dbo.np_Companys.URL, dbo.np_Companys.FuncID, dbo.ros_AdditionalCustomerInfo.RemarksEN, 
                      dbo.ros_AdditionalCustomerInfo.RemarksNative
FROM         dbo.np_CreditInfoUser INNER JOIN
                      dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID INNER JOIN
                      dbo.np_Address ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Address.CreditInfoID INNER JOIN
                      dbo.np_City ON dbo.np_Address.CityID = dbo.np_City.CityID INNER JOIN
                      dbo.np_IDNumbers ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_IDNumbers.CreditInfoID LEFT OUTER JOIN
                      dbo.cpi_Countries ON dbo.np_Address.CountryID = dbo.cpi_Countries.CountryID LEFT OUTER JOIN
                      dbo.ros_AdditionalCustomerInfo ON dbo.np_CreditInfoUser.CreditInfoID = dbo.ros_AdditionalCustomerInfo.CreditInfoID

