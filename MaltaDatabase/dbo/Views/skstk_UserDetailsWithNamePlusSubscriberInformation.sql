﻿CREATE VIEW dbo.skstk_UserDetailsWithNamePlusSubscriberInformation
AS
SELECT     dbo.skstk_UserDetailsWithName.*, dbo.skstk_SubscriberInformation.NameNative AS SubscriberNameNative, 
                      dbo.skstk_SubscriberInformation.NameEN AS SubscriberNameEN, dbo.skstk_SubscriberInformation.StreetNative AS SubscriberStreetNative, 
                      dbo.skstk_SubscriberInformation.StreetEN AS SubscriberStreetEN, dbo.skstk_SubscriberInformation.CityNative AS SubscriberCityNative, 
                      dbo.skstk_SubscriberInformation.CityEN AS SubscriberCityEN
FROM         dbo.skstk_UserDetailsWithName INNER JOIN
                      dbo.skstk_SubscriberInformation ON dbo.skstk_UserDetailsWithName.SubscriberID = dbo.skstk_SubscriberInformation.CreditInfoID

