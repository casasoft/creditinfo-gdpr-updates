﻿CREATE VIEW dbo.ros_OrderView
AS
SELECT     dbo.ros_Order.ID, dbo.ros_Order.Date, dbo.ros_Order.DispatchDeadline, dbo.ros_Order.CustomerReferenceNumber, 
                      dbo.ros_Order.CustomerCreditInfoID, dbo.ros_Order.SubjectCreditInfoID, dbo.ros_CreditInfoUserView.CreditInfoID, dbo.ros_CreditInfoUserView.Type, 
                      dbo.ros_CreditInfoUserView.NameNative, dbo.ros_CreditInfoUserView.NameEN, dbo.ros_CreditInfoUserView.FirstNameNative, 
                      dbo.ros_CreditInfoUserView.FirstNameEN, dbo.ros_CreditInfoUserView.SurNameNative, dbo.ros_CreditInfoUserView.SurNameEN, 
                      dbo.ros_Order.RemarksEN, dbo.ros_Order.RemarksNative, dbo.ros_Order.Delivered, dbo.ros_Order.DeliverySpeedID, dbo.ros_Order.Deleted, 
                      dbo.np_UsageTypes.typeEN AS productEN, dbo.np_UsageTypes.typeNative AS productNative, dbo.np_UsageTypes.id AS productID
FROM         dbo.ros_Order INNER JOIN
                      dbo.np_UsageTypes ON dbo.ros_Order.np_UsageTypesID = dbo.np_UsageTypes.id CROSS JOIN
                      dbo.ros_CreditInfoUserView
WHERE     (dbo.ros_Order.CustomerCreditInfoID = dbo.ros_CreditInfoUserView.CreditInfoID) OR
                      (dbo.ros_Order.SubjectCreditInfoID = dbo.ros_CreditInfoUserView.CreditInfoID)

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1 [56] 4 [18] 2))"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ros_Order"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 310
               Right = 252
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ros_CreditInfoUserView"
            Begin Extent = 
               Top = 4
               Left = 253
               Bottom = 186
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "np_UsageTypes"
            Begin Extent = 
               Top = 159
               Left = 540
               Bottom = 274
               Right = 705
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      RowHeights = 220
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ros_OrderView';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ros_OrderView';

