﻿
create view lmt_mfsa_companies (
  company_id, company_name, registration_no, 
  registration_date, address_1, address_2,
  locality_id, postcode, country_id,
  state_id, capital_issued_for, denomination_id,
  total_auth_shr_cap, total_iss_shr_cap, classification_id,
  acc_ref_date
)
as 
select
 companyid, companyname, companyid,
 registrationdate, companyaddress1, companyaddress2,
 companylocality, companypostcode, companycountryid,
 companystatusid,  '', denominationid,
 totalauthsharecapital, totalissuedsharecapital, companyclassificationid,
 accrefdate
from lmt_companies a
-- select first 11 * from mt_asc_companies
-- select first 11 * from lmt_companies
-- select first 11 * from vw_mt_asc_localities
