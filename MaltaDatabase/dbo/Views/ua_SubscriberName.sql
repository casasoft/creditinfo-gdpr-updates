﻿CREATE VIEW dbo.ua_SubscriberName
AS
SELECT     dbo.au_Subscribers.ID, dbo.au_Subscribers.CreditInfoID, CASE WHEN dbo.np_Companys.NameNative IS NULL 
                      THEN dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative ELSE dbo.np_Companys.NameNative END AS NameNative, 
                      CASE WHEN dbo.np_Companys.NameEN IS NULL 
                      THEN dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN ELSE dbo.np_Companys.NameEN END AS NameEN
FROM         dbo.au_Subscribers LEFT OUTER JOIN
                      dbo.np_Individual ON dbo.au_Subscribers.CreditInfoID = dbo.np_Individual.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Companys ON dbo.au_Subscribers.CreditInfoID = dbo.np_Companys.CreditInfoID
