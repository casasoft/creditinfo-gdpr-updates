﻿
CREATE FUNCTION getCISubscriberName (@p_CIID int, @p_Native bit)  -- @p_Native : 1 = Native, 0 = EN
RETURNS nvarchar(160) AS
BEGIN 
DECLARE @Name nvarchar(160)
	IF (@p_Native = 1)
		BEGIN
			SELECT	@Name = ISNULL(CASE 
					WHEN np_CreditInfoUser.Type = 1 
					THEN dbo.np_Companys.NameNative 
					ELSE dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative
					END, 
					CASE 
					WHEN np_CreditInfoUser.Type = 1 
					THEN dbo.np_Companys.NameEN 
					ELSE dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN
					END)
			FROM	dbo.np_CreditInfoUser 
				LEFT OUTER JOIN dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID 
				LEFT OUTER JOIN dbo.np_Individual ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID
			WHERE dbo.np_CreditInfoUser.CreditInfoID =  @p_CIID
		END
	ELSE
		BEGIN
			SELECT	@Name = ISNULL(CASE 
					WHEN np_CreditInfoUser.Type = 1 
					THEN dbo.np_Companys.NameEN 
					ELSE dbo.np_Individual.FirstNameEN + ' ' + dbo.np_Individual.SurNameEN
					END,
					CASE 
					WHEN np_CreditInfoUser.Type = 1 
					THEN dbo.np_Companys.NameNative 
					ELSE dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative
					END)
			FROM	dbo.np_CreditInfoUser 
				LEFT OUTER JOIN dbo.np_Companys ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Companys.CreditInfoID 
				LEFT OUTER JOIN dbo.np_Individual ON dbo.np_CreditInfoUser.CreditInfoID = dbo.np_Individual.CreditInfoID
			WHERE dbo.np_CreditInfoUser.CreditInfoID =  @p_CIID
		END
	RETURN @Name
END
