﻿
    create function [dbo].[ws_ddd_by_id_with_fullName] (@idnumber nvarchar(100))

    returns @ret table
    (
    caseno 			varchar		(50),
    court 			nvarchar 	(50),
    plaintiff 		nvarchar 	(100),
    idregno 		nvarchar 	(100),
    [fullname] 	    nvarchar 	(150),
    surname         nvarchar    (80),
    othersurname    nvarchar    (80),
    name            nvarchar    (80),
    othername       nvarchar    (80),
    middlenames     nvarchar    (80),
    address			nvarchar 	(200),
    postcode		varchar 	(30),
    city			nvarchar 	(50),
    regdate			datetime	,
    issuedate		datetime	,
    casetype		nvarchar 	(100),
    typedescription	nvarchar	(255),
    agent			nvarchar 	(50),
    amount			decimal		(9),
    currency		char(3),
    gazetteyear		varchar 	(30),
    gazettepage		varchar 	(10),
    issuebank		nvarchar 	(100),
    casedecisiontype	nvarchar	(100),
    --comment			nvarchar	(512),
    cigreference		int
    )

    as
    begin
    insert @ret

    SELECT     a.CaseNr AS caseno, c.NameNative AS court, g.ClaimOwnerNameNative AS plaintiff, h.idregno AS idregno,
    h.name AS fullname,
    h1.surname, h1.othersurname,  h1.name,  h1.othername,  h1.middlenames, h.address AS address,
    h.postcode AS postcode, h.city AS city, a.RegDate AS regdate, a.ClaimDate AS issuedate, b.TypeNative AS casetype, b.DescriptionNative AS typedescription, a.Agent AS agent,
    a.Amount AS amount, a.CurrencyID as currency,a.GazetteYear AS gazetteyear, a.GazettePage AS gazettepage, d.NameNative AS issuebank, f.TypeNative AS casedecisiontype,
    --a.RegisterCommentNative AS comment,
    a.ID AS cigreference
    FROM         dbo.np_Claims a INNER JOIN
    dbo.np_CaseTypes b ON a.ClaimTypeID = b.CaseTypeID INNER JOIN
    dbo.np_Case e ON b.CaseID = e.ClaimTypeID INNER JOIN
    dbo.GetEntity (@idnumber) h ON a.CreditInfoID = h.CreditInfoID LEFT OUTER JOIN
    dbo.np_Individual_FullName h1 on h.CreditInfoId =  h1.CreditInfoId LEFT OUTER JOIN
    dbo.np_InformationSource c ON a.InformationSourceID = c.InformationSourceID LEFT OUTER JOIN
    dbo.np_ChequeIssueBanks d ON a.ChequeIssuedBank = d.BankID LEFT OUTER JOIN
    dbo.np_CaseDecisionsTypes f ON a.CaseDecisionsType = f.StatusType LEFT OUTER JOIN
    dbo.np_CreditInfoUserView g ON a.ClaimOwnerCIID = g.CreditInfoID
    WHERE     (a.StatusID = 4) and a.CreditInfoID = (select creditinfoid from dbo.np_IDNumbers where number = @idnumber)

    return
    end
    