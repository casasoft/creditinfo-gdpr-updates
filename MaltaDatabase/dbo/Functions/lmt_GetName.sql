﻿create FUNCTION lmt_GetName(@v_IDNumber nvarchar(50))
RETURNS nvarchar(100) AS 
BEGIN 
declare @v_ret nvarchar(100)
declare @v_cig int
if @v_IDNumber is null or rtrim(@v_IDNumber)='' set @v_ret='ID missing'
else
  begin
  set @v_cig=(select creditinfoid from np_idnumbers where numbertypeid=1 and number=@v_IDNumber)
  if @v_cig is null set @v_ret='Name missing'
  else
    begin
    set @v_ret=(select firstnamenative from np_individual where creditinfoid=@v_cig)
    if @v_ret is null set @v_ret=(select namenative from np_companys where creditinfoid=@v_cig)
    if @v_ret is null set @v_ret='Name missing'
    end
  end
  return @v_ret  
end
