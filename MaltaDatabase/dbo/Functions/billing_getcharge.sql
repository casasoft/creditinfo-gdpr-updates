﻿CREATE function billing_getcharge(@p_subscriber_id int,@p_period_from datetime, @p_period_to datetime,@p_OnlyBillable bit)
returns @ret table (
	creditinfoid int,
	departmentID int,	--v1.2 departments
	agreementnumber nvarchar(50),
	agreementid int,
	financialkey nvarchar(50),
	currency varchar(3),
	itemid int, 
	agreementDescription varchar(1000), 
	agreementamount money,
	agreementDiscountpercent money,
	AgreementDiscount money,
	agreementUsage int,
	fixedamount money, 
	fixeddiscountpercent money, 
	fixedamountDiscount money,
	fixedUsage int,
	monthlyamount money,
	Monthlydiscountpercent money, 
	monthlyamountDiscount money,
	montlyUsage int,
	usageamount money,
	Usagediscountpercent money,
	itemDiscount money,
	itemNameNative varchar(100), 
	itemNameEN varchar(100),
	UsageIndex money,
	usage int,
	freeZone int)
as
begin
 
--Declare @p_subscriber_id int;
--Declare @p_period_from datetime;
--Declare @p_period_to datetime;
--Declare @p_detail int;
Declare @v_round int;
Declare @v_months int;
Declare @v_billableNotEqual int;

--set @p_period_from = '2004-10-01';
--set @p_period_to = '2004-10-31';
--set @p_detail = 1;
--discount rounding.
set @v_round = 2
set @v_months = datediff(month,@p_period_from, @p_period_to) + 1

if (@p_OnlyBillable = 1)
	SET @v_billableNotEqual = 0;
else
	SET @v_billableNotEqual = -1;

--Insert into the temptable
INSERT INTO @ret
select 
	creditinfoid,		--Subscriber CredtiInfoID
	DepartmentID,		--v1.2 departmentID
	agreementnumber,	--Agreement number.
	agreementid,		--Agreement key.
	financialkey,		--financialkey
	currency,		--Currency of amounts
	itemid, 		--item id
	agreementDescription,	--Agreement discription
	agreementamount,	--fixed agreement amount
	agreementDiscountpercent,	--fixed agreement amount discount
	dbo.MinMoney(round(agreementamount* (agreementDiscountpercent/ 100), @v_round),round(agreementamount, @v_round)) as AgreementDiscount, --agreement amount discount
	1 as agreementUsage,	--Fixed agreement usage
	fixedamount, 		--fixed monthly amount
	fixeddiscountpercent,	--fixed monthly amount discount percent 
	dbo.MinMoney(round(fixedamount*(1+UsageIndex)* (fixeddiscountpercent/ 100), @v_round),round(fixedamount*(1+UsageIndex), @v_round)) as fixedamountDiscount, --fixed monthly amount discount
	--Take Min on discount if percent is more than 100%
	1 as fixedUsage,	--Fixed usage.
	monthlyamount,		--Monthly item amount
	Monthlydiscountpercent, 	--Monthly item discount percents
	dbo.MinMoney(round(monthlyamount* @v_months * (1+UsageIndex)* (Monthlydiscountpercent/100), @v_round),round(monthlyamount* @v_months*(1+UsageIndex), @v_round)) as monthlyamountDiscount, --Montly item amount discount
	--Take Min on discount if percent is more than 100%
	--usageamount* dbo.MaxINT(usage-freeZone,0) * (1+UsageIndex) as usageamount,	--Usage amount
	@v_months as montlyUsage, --
	usageamount,	--Usage amount
	Usagediscountpercent,							--Usage amount discount percent
	dbo.MinMoney(round(usageamount* dbo.MaxINT(usage-freeZone,0)* (1+UsageIndex)* (Usagediscountpercent)/100.0, @v_round),round(usageamount* dbo.MaxINT(usage-freeZone,0)* (1+UsageIndex), @v_round)) as itemDiscount, --Usage amount discount
	--Take Min on discount if percent is more than 100%
	itemNameNative, 	--item Native name
	itemNameEN,		--item English name
	UsageIndex+1.0,		--added index percent on usage amount
	dbo.MaxINT(usage-freeZone,0) usage, --total usage on item.
	freeZone+dbo.MinINT(usage-freeZone,0) as freezone --Free zone if freezone is more than usage display only used.
from (
select 
	a.creditinfoid,
	uu.DepartmentID,--v1.2 departments
	b.agreementnumber,
	b.agreementid,
	uu.FinancialKey,
	b.currency,
	isnull(uu.itemid,0) as itemid, 
	b.GeneralDescription as agreementDescription,
	b.agreementamount,
	b.discountpercentage as agreementDiscountpercent,
	isnull(uu.fixedamount,0.0) as fixedamount, 
	isnull(uu.fixeddiscountpercent,0.0) as fixeddiscountpercent, 
	isnull(uu.Monthlydiscountpercent,0.0) as Monthlydiscountpercent, 
	isnull(uu.Usagediscountpercent,0.0) as Usagediscountpercent,
	isnull(uu.freeZone,0) as freeZone, 
	isnull(uu.monthlyamount,0.0) as monthlyamount, 
	isnull(uu.usageamount,0.0) as usageamount,
	isnull(uu.adpr,0.0) as UsageIndex,
	isnull(uu.itemNameNative,'') as itemNameNative, 
	isnull(uu.itemNameEN,'') as itemNameEN,
	--Check if itemid is 1 then count usage different.
	isnull(
	--v1.1.1.5 (case uu.itemid when 1 then --special for itemid 1
	(case uu.LinkRule when 'COUNT(cw_watchwords)+COUNT(cw_watchuniqueid)' then
		--v1.1((select count(*) from au_users users, cw_watchwords watchw where a.creditinfoid=users.subscriberid and users.id=watchw.userid) --1.1.1.6 users.id instead of creditinfoid
		--v1.1 +(select count(*) from au_users users, cw_watchuniqueid watchun where a.creditinfoid=users.subscriberid and users.id=watchun.userid)) --1.1.1.6 users.id instead of creditinfoid
		((select count(*) from cw_watchwords watchw where watchw.userid in(SELECT userid FROM au_users WHERE au_users.DepartmentID=uu.DepartmentID)) --v1.2 departments
		 +(select count(*) from cw_watchuniqueid watchun where watchun.userid in(SELECT userid FROM au_users WHERE au_users.DepartmentID=uu.DepartmentID))) --v1.2 departments
	else --else if no connection count np_usage.
		--uu.usage  
		--v1.1(select count(*) from np_usage usage join au_users users on usage.userid=users.id  where a.creditinfoid=users.subscriberid and usage.created between uu.FromDate and uu.ToDate and usage.query_type=uu.loggingcodeid and IsBillable<>@v_billableNotEqual)
		(select count(*) from np_usage usage join au_User users on usage.userid=users.id where users.DepartmentID=uu.DepartmentID and usage.created between uu.FromDate and uu.ToDate and usage.query_type=uu.loggingcodeid and IsBillable<>@v_billableNotEqual)--v1.2 departments
	end),0) usage
from au_subscribers a 
--Take all agreements with in period
join billing_agreement b on a.creditinfoid=b.subscriberid
--find all usage with in this agreements.
left join (
SELECT	c.agreementid,
	baid.DepartmentID,--v1.2 departments
	c.itemid,
	d.FinancialKey,
	dbo.MaxDate(c.begindate,@p_period_from) as FromDate,
	dbo.MinDate(@p_period_to,DATEADD(dd,1,c.enddate)) as ToDate, --add 1 day because agreement end at 00:00:00 next day.
	c.fixedamount, 
	c.fixeddiscountpercent, 
	--Check if billing_agreement_items override billing_registry
	case c.monthlydiscountpercent when 0.0 then d.monthlydiscountpercent else c.monthlydiscountpercent end as Monthlydiscountpercent, 
	--Check if billing_agreement_items override billing_registry
	case c.usagediscountpercent when 0.0 then d.usagediscountpercent else c.usagediscountpercent end as Usagediscountpercent,
	--Check if null then put zero
	isnull(c.freeZone,0) as freeZone, 
	d.monthlyamount, 
	d.usageamount,
	d.itemNameNative, 
	d.itemNameEN
	, --calculate adding indexes on item.
	isnull((select SUM((endindex-startindex)/startindex) from
	(select c.id,
		isnull((select indexvalue from billing_indexdetails where indexname=e.indexname and indexdate=(select max(indexDate) as indexdate from billing_indexdetails where indexname=e.indexname and indexdate <= c.begindate)),1) as startindex,
		isnull((select indexvalue from billing_indexdetails where indexname=e.indexname and indexdate=(select max(indexDate) as indexdate from billing_indexdetails where indexname=e.indexname and indexdate <= @p_period_to)),1) as endindex
	 from billing_indexesonagreementitems e 
	 where e.itemid=c.id) as f --find indexes if any.
	 Group by c.id),0) adpr -- if null then zero index.
	--Count usage for this item.
	--,(select count(*) from np_usage usage where usage.created between @p_period_from and @p_period_to and usage.query_type=d.loggingcodeid)  usage  
	,d.loggingcodeid
	,ISNULL(e.LinkRule,'') as LinkRule --v 1.1.1.5
FROM	billing_agreementitems c
	join billing_itemregistry d on c.itemid=d.id 
	left join billing_ItemCounterLink e on e.id=c.CounterLinkID --v 1.1.1.5
	left join c_billing_AgreementItem_Department baid on c.ID = baid.AgreementItemID --v1.2 departments
	--Set preriod filter on usage.
	WHERE 
		--not(c.enddate<@p_period_from or c.begindate > @p_period_to) --more efficient to use "AND"
		c.begindate <= @p_period_to and c.enddate>= @p_period_from
) as uu on b.agreementid=uu.agreementid
--Set preriod filter on agreement start and end.
WHERE	--(a.creditinfoid=@p_subscriber_id or (@p_subscriber_id is null))
	--and 
	--OR IS to slow in store procedure
	--(a.creditinfoid=@p_subscriber_id or (@p_subscriber_id is null))
	--and 
	--not(b.enddate<@p_period_from or b.begindate > @p_period_to) --more efficient to use "AND"
	b.begindate <= @p_period_to and b.enddate>= @p_period_from
) the
order by creditinfoid;

return
end
