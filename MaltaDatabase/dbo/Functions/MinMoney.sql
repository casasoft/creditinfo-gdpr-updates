﻿CREATE FUNCTION dbo.MinMoney(@nFirst money , @nSecond money)
RETURNS money AS 
BEGIN 
	IF (@nFirst > @nSecond)
	BEGIN
		return @nSecond;
	END
	return @nFirst;
END
