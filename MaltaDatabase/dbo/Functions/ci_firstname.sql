﻿create function ci_firstname(@v_name varchar(99))
returns varchar(99)
as
begin
  declare @ret varchar(99)
  declare @i int,@i_max int
  declare @i_name int
  declare @v_komma int
  declare @i_bil int
  set @v_komma = charindex(',',@v_name)
  if 1 <= @v_komma
    begin
    set @i_max = @v_komma -1
    end
  else
    begin
    set @i_max=len(@v_name)
    end
  set @i = 1
  set @i_name = 0
  while @i <= @i_max
    begin
    set @i_bil = charindex(' ', @v_name, @i)
    if @i_bil = 0 or @i_bil > @i_max
      begin
      set @i = @i_max+1
      end
    else
      begin
      set @i_name = @i_bil
      set @i = @i_name + 1
      end
    end
  if 1 <= @i_name 
    begin
    return rtrim(substring(@v_name,1,@i_name))
    end
  else
    begin
    return @v_name
    end
  return @i_name
end
