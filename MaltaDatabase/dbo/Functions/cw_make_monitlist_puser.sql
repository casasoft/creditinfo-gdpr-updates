﻿CREATE function cw_make_monitlist_puser (@userid int ,@native int, @only_new int)
returns @ret table (
        subject_name nvarchar(99),
        subject_address nvarchar(99),
        subject_city nvarchar(99),
        creditinfoid int,
        claim_id int,
        claim_type nvarchar(99),
        watch_subject nvarchar(99),
        watch_id int,
        watch_type int
)
as
begin
-- ID - einst, ekki hfang
insert @ret
        select g.firstnamenative + ' ' + g.surnamenative, '','',d.creditinfoid, d.id,e.typenative,a.uniqueid,a.watchid, 1
        from cw_watchuniqueid a,au_users b,np_claims d,np_casetypes e, 
        np_idnumbers f, np_individual g
        where a.userid=b.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and f.numbertypeid=1
        and d.claimtypeid=e.casetypeid
        and d.statusid=4
        and b.isopen='True'
        and a.userid=@userid
        and @native=1
        and not exists (select * from np_address where creditinfoid=g.creditinfoid)
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
        union
        select g.firstnameen + ' ' + g.surnameen, '','',d.creditinfoid, d.id,e.typeen,a.uniqueid,a.watchid, 1
        from cw_watchuniqueid a,au_users b,np_claims d,np_casetypes e, np_idnumbers f, 
        np_individual g
        where a.userid=b.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and f.numbertypeid=1
        and d.claimtypeid=e.casetypeid
        and d.statusid=4
        and b.isopen='True'
        and a.userid=@userid
        and @native=0
        and not exists (select * from np_address where creditinfoid=g.creditinfoid)
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
-- ID - einst, er hfang
insert @ret
        select g.firstnamenative + ' ' + g.surnamenative, h.streetnative,i.namenative,d.creditinfoid, d.id,e.typenative,a.uniqueid,a.watchid, 1
        from cw_watchuniqueid a,au_users b,np_claims d,np_casetypes e, 
        np_idnumbers f, np_individual g, np_address h, np_city i
        where a.userid=b.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and f.numbertypeid=1
        and g.creditinfoid=h.creditinfoid
        and d.claimtypeid=e.casetypeid
        and d.statusid=4
        and b.isopen='True'
        and h.cityid=i.cityid
        and a.userid=@userid
        and @native=1
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
        union
        select g.firstnameen + ' ' + g.surnameen, h.streeten,i.nameen,d.creditinfoid, d.id,e.typeen,a.uniqueid,a.watchid, 1
        from cw_watchuniqueid a,au_users b,np_claims d,np_casetypes e, np_idnumbers f, 
        np_individual g,np_address h, np_city i
        where a.userid=b.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and f.numbertypeid=1
        and g.creditinfoid=h.creditinfoid
        and d.claimtypeid=e.casetypeid
        and d.statusid=4
        and b.isopen='True'
        and h.cityid=i.cityid
        and a.userid=@userid
        and @native=0
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
-- ID - fyr, ekki hfang
insert @ret
        select g.namenative, '','',d.creditinfoid, d.id,e.typenative,a.uniqueid,a.watchid, 1
        from cw_watchuniqueid a,au_users b,np_claims d,np_casetypes e, np_idnumbers f, 
        np_companys g
        where a.userid=b.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and f.numbertypeid=1
        and d.claimtypeid=e.casetypeid
        and d.statusid=4
        and b.isopen='True'
        and a.userid=@userid
        and @native=1
        and not exists (select * from np_address where creditinfoid=g.creditinfoid)
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
        union
        select g.nameen, '','',d.creditinfoid, d.id,e.typeen,a.uniqueid,a.watchid, 1
        from cw_watchuniqueid a,au_users b,np_claims d,np_casetypes e, np_idnumbers f, 
        np_companys g
        where a.userid=b.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and f.numbertypeid=1
        and d.claimtypeid=e.casetypeid
        and d.statusid=4
        and b.isopen='True'
        and a.userid=@userid
        and @native=0
        and not exists (select * from np_address where creditinfoid=g.creditinfoid)
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
-- ID - fyr, er hfang
insert @ret
        select g.namenative, h.streetnative,i.namenative,d.creditinfoid, d.id,e.typenative,a.uniqueid,a.watchid, 1
        from cw_watchuniqueid a,au_users b,np_claims d,np_casetypes e, 
        np_idnumbers f, np_companys g, np_address h, np_city i
        where a.userid=b.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and f.numbertypeid=1
        and g.creditinfoid=h.creditinfoid
        and d.claimtypeid=e.casetypeid
        and d.statusid=4
        and b.isopen='True'
        and h.cityid=i.cityid
        and a.userid=@userid
        and @native=1
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
        union
        select g.nameen, h.streeten,i.nameen,d.creditinfoid, d.id,e.typeen,a.uniqueid,a.watchid, 1
        from cw_watchuniqueid a,au_users b,np_claims d,np_casetypes e, np_idnumbers f, 
        np_companys g,np_address h, np_city i
        where a.userid=b.id
        and d.creditinfoid=f.creditinfoid
        and a.uniqueid=f.number
        and f.creditinfoid=g.creditinfoid
        and f.numbertypeid=1
        and g.creditinfoid=h.creditinfoid
        and d.claimtypeid=e.casetypeid
        and d.statusid=4
        and b.isopen='True'
        and h.cityid=i.cityid
        and a.userid=@userid
        and @native=0
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
-- Einst - er hfang
insert @ret
        select f.firstnamenative + ' ' + f.surnamenative, g.streetnative, i.namenative, d.creditinfoid, d.id,e.typenative,a.firstnamenative,a.watchid, 2
        from cw_watchwords a,au_users b2, np_claims d, np_case e, np_individual f, 
        np_address g,
        np_city i
        where a.userid=b2.id
        and a.iscompany='False' 
        and d.creditinfoid=f.creditinfoid
        and f.creditinfoid=g.creditinfoid
        and lower(a.firstnamenative)=lower(f.firstnamenative)
        and lower(a.surnamenative)=lower(f.surnamenative)
        and a.firstnamenative <> '' and a.surnamenative <> ''
        and (lower(a.addressnative)=lower(g.streetnative) or a.addressnative is null)
        and d.claimtypeid=e.claimtypeid
        and b2.isopen='True'
        and g.cityid=i.cityid
        and d.statusid=4
        and a.userid=@userid
        and @native=1
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
      union
        select f.firstnameen + ' ' + f.surnameen, g.streeten, i.nameen, d.creditinfoid, d.id,e.typeen,a.firstnameen,a.watchid, 2
        from cw_watchwords a,au_users b2, np_claims d, np_case e, np_individual f, 
        np_address g,
        np_city i
        where a.userid=b2.id
	and a.iscompany='False'
        and d.creditinfoid=f.creditinfoid
        and f.creditinfoid=g.creditinfoid
        and lower(a.firstnameen)=lower(f.firstnameen)
        and lower(a.surnameen)=lower(f.surnameen)
        and a.surnameen <> '' and a.firstnameen <> ''
        and (lower(a.addressen)=lower(g.streeten) or a.addressen is null)
        and d.claimtypeid=e.claimtypeid
        and d.statusid=4
        and b2.isopen='True'
        and g.cityid=i.cityid
        and a.userid=@userid
        and @native=0
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
-- Einst - ekki hfang
insert @ret
        select f.firstnamenative + ' ' + f.surnamenative, '', '', d.creditinfoid, d.id,e.typenative,a.firstnamenative,a.watchid, 2
        from cw_watchwords a,au_users b2, np_claims d, np_case e, np_individual f
        where a.userid=b2.id
        and a.iscompany='False' 
        and d.creditinfoid=f.creditinfoid
        and lower(a.firstnamenative)=lower(f.firstnamenative)
        and lower(a.surnamenative)=lower(f.surnamenative)
        and a.firstnamenative <> '' and a.surnamenative <> ''
        and d.claimtypeid=e.claimtypeid
        and b2.isopen='True'
        and d.statusid=4
        and a.userid=@userid
        and @native=1
        and not exists (select * from np_address where creditinfoid=f.creditinfoid )
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
      union
        select f.firstnameen + ' ' + f.surnameen, '', '', d.creditinfoid, d.id,e.typeen,a.firstnameen,a.watchid, 2
        from cw_watchwords a,au_users b2, np_claims d, np_case e, np_individual f
        where a.userid=b2.id
	and a.iscompany='False'
        and d.creditinfoid=f.creditinfoid
        and lower(a.firstnameen)=lower(f.firstnameen)
        and lower(a.surnameen)=lower(f.surnameen)
        and a.firstnameen <> '' and a.surnameen <> ''
        and d.claimtypeid=e.claimtypeid
        and d.statusid=4
        and b2.isopen='True'
        and a.userid=@userid
        and @native=0
        and not exists (select * from np_address where creditinfoid=f.creditinfoid )
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
-- Fyr - er hfang
insert @ret
        select f.namenative, g.streetnative, i.namenative, d.creditinfoid, d.id,e.typenative,a.firstnamenative,a.watchid, 2
        from cw_watchwords a,au_users b2, np_claims d, np_case e, np_companys f, 
        np_address g,
        np_city i
        where a.userid=b2.id
        and a.iscompany='True' 
        and d.creditinfoid=f.creditinfoid
        and f.creditinfoid=g.creditinfoid
        and lower(a.firstnamenative)=lower(f.namenative)
        and a.firstnamenative <> '' 
        and (lower(a.addressnative)=lower(g.streetnative) or a.addressnative is null)
        and d.claimtypeid=e.claimtypeid
        and b2.isopen='True'
        and g.cityid=i.cityid
        and d.statusid=4
        and a.userid=@userid
        and @native=1
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
      union
        select f.nameen, g.streeten, i.namenative, d.creditinfoid, d.id,e.typeen,a.firstnameen,a.watchid, 2
        from cw_watchwords a,au_users b2, np_claims d, np_case e, np_companys f, 
        np_address g,
        np_city i
        where a.userid=b2.id
	and a.iscompany='True'
        and d.creditinfoid=f.creditinfoid
        and f.creditinfoid=g.creditinfoid
        and lower(a.firstnameen)=lower(f.nameen)
        and a.firstnameen <> ''
        and (lower(a.addressen)=lower(g.streeten) or a.addressen is null)
        and d.claimtypeid=e.claimtypeid
        and d.statusid=4
        and b2.isopen='True'
        and g.cityid=i.cityid
        and a.userid=@userid
        and @native=0
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
-- Fyr - ekki hfang
insert @ret
        select f.namenative, '', '', d.creditinfoid, d.id,e.typenative,a.firstnamenative,a.watchid, 2
        from cw_watchwords a,au_users b2, np_claims d, np_case e, np_companys f
        where a.userid=b2.id
        and a.iscompany='True' 
        and d.creditinfoid=f.creditinfoid
        and lower(a.firstnamenative)=lower(f.namenative)
        and a.firstnamenative <> ''
        and d.claimtypeid=e.claimtypeid
        and b2.isopen='True'
        and d.statusid=4
        and a.userid=@userid
        and @native=1
        and not exists (select * from np_address where creditinfoid=f.creditinfoid )
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))
      union
        select f.nameen, '', '', d.creditinfoid, d.id,e.typeen,a.firstnameen,a.watchid, 2
        from cw_watchwords a,au_users b2, np_claims d, np_case e, np_companys f
        where a.userid=b2.id
	and a.iscompany='True'
        and d.creditinfoid=f.creditinfoid
        and lower(a.firstnameen)=lower(f.nameen)
        and a.firstnameen <> ''
        and d.claimtypeid=e.claimtypeid
        and d.statusid=4
        and b2.isopen='True'
        and a.userid=@userid
        and @native=0
        and not exists (select * from np_address where creditinfoid=f.creditinfoid)
        and (@only_new=0 or exists (select * from cw_newclaims where claim_id=d.id))return
end

