﻿
CREATE FUNCTION dbo.cf_CheckIfVersionIsLessThan
	(@AppMajor char(2), 
	@AppMinor char(2),
	@AppPatch char(3),
	@DBPatch char(3))
RETURNS int AS  
BEGIN 
	--Check Version. 
	--returns: 
		--0 if all less or equal.
		--1 if AppMajor is greater 
		--2 if AppMinor is greater
		--4 if AppPatch is greater
		--8 if DBPatch is greater
		--compined if more than one is greater. e.g. AppMajor + AppMinor = 3
	DECLARE @isLess int;
	SET @isLess = 0;
	--AppMajor is less
	SELECT @isLess=@isLess+1 FROM c_vc_versions WHERE @AppMajor>AppMajor;
	--AppMajor less or same and AppMinor is less
	SELECT @isLess=@isLess+2 FROM c_vc_versions WHERE @AppMajor>=AppMajor and @AppMinor>AppMinor;
	--AppMajor less or same and AppMinor is less or same AppPatch is less.
	SELECT @isLess=@isLess+4 FROM c_vc_versions WHERE @AppMajor>=AppMajor and @AppMinor>=AppMinor and @AppPatch>AppPatch;
	--AppMajor less or same and AppMinor is less or same AppPatch is less or same and DBPatch less.
	SELECT @isLess=@isLess+8 FROM c_vc_versions WHERE @AppMajor>=AppMajor and @AppMinor>=AppMinor and @AppPatch>=AppPatch and @DBPatch>DBPatch;

	return @isLess;
END
