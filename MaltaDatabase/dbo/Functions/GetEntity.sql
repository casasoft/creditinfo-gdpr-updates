﻿CREATE function GetEntity (@number nvarchar(100))
returns @ret table 
(
	CreditInfoID		int,
	idregno 		nvarchar 	(100),
	[name] 			nvarchar 	(150),
	address			nvarchar 	(200),
	postcode		varchar 	(30),
	city			nvarchar 	(50)
)
as
begin
insert @ret
SELECT     np_IDNumbers.CreditInfoID, RTRIM(dbo.np_IDNumbers.Number) AS idregno, dbo.np_Companys.NameNative AS name, 
                      dbo.np_Address.StreetNative AS address, dbo.np_Address.PostalCode AS postcode, dbo.np_City.NameNative AS city
FROM         	dbo.np_IDNumbers INNER JOIN
		dbo.np_Companys ON dbo.np_IDNumbers.CreditInfoID = dbo.np_Companys.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Address ON dbo.np_IDNumbers.CreditInfoID = dbo.np_Address.CreditInfoID LEFT OUTER JOIN
                      dbo.np_City ON dbo.np_Address.CityID = dbo.np_City.CityID
where np_IDNumbers.CreditInfoID IN (select creditinfoid from dbo.np_IDNumbers where number = @number)
UNION ALL
SELECT     np_IDNumbers.CreditInfoID, RTRIM(dbo.np_IDNumbers.Number) AS idregno, 
                      dbo.np_Individual.FirstNameNative + ' ' + dbo.np_Individual.SurNameNative AS name, dbo.np_Address.StreetNative AS address, 
                      dbo.np_Address.PostalCode AS postcode, dbo.np_City.NameNative AS city
FROM         dbo.np_IDNumbers INNER JOIN
                      dbo.np_Individual ON dbo.np_IDNumbers.CreditInfoID = dbo.np_Individual.CreditInfoID LEFT OUTER JOIN
                      dbo.np_Address ON dbo.np_IDNumbers.CreditInfoID = dbo.np_Address.CreditInfoID LEFT OUTER JOIN
                      dbo.np_City ON dbo.np_Address.CityID = dbo.np_City.CityID
where np_IDNumbers.CreditInfoID IN (select creditinfoid from dbo.np_IDNumbers where number = @number)

return
end
