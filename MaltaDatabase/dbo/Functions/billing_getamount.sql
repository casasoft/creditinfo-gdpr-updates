﻿
CREATE function billing_getamount(@p_subscriber_id int,@p_period_from datetime, @p_period_to datetime, @p_detail int)
returns @ret table (
 subscriberID int,
 subscriberSSN nvarchar(15),
 subscriberName nvarchar(100),
 departmentName nvarchar(100), --v1.2 departmentName
 agreementNumber nvarchar(50),
 useDesc nvarchar(50),
 financialkey nvarchar(50),
 itemNameNative nvarchar(1000),
 itemCount int,
 itemType nvarchar(3),
 itemPrice money,
 itemTotalPrice money,
 itemIndex money,
 itemIndexPrice money,
 itemDiscountPerc decimal(5,2),
 itemDiscount money,
 itemTotal money,
 currency char(3))
begin
	--Set date to 23:59:59.998
	SET @p_period_to = DATEADD(hh,23,@p_period_to);	
	SET @p_period_to = DATEADD(mi,59,@p_period_to);	
	SET @p_period_to = DATEADD(ss,59,@p_period_to);	
	SET @p_period_to = DATEADD(ms,998,@p_period_to);
	--Fmv sends + one day.
	--SET @p_period_to = DATEADD(ms,-2,@p_period_to);
	DECLARE @p_OnlyBillable bit;
	SET @p_OnlyBillable=0; --Until IsBillable is added to FMV.

	if (@p_detail = 0)
	begin
		INSERT INTO @ret
		SELECT	--*
 			a.subscriberID,
			a.subscriberSSN, 
			a.subscriberName,
			departmentName, --v1.2 departmentName
			0 as agreementNumber,
			'' as useDesc,
			'' as AccKey,
			'' as itemNameNative,
			1 as itemCount,
			'PCS' as itemType,
			SUM(itemPrice) as itemPrice,
			SUM(itemTotalPrice) as itemTotalPrice,
			SUM(itemIndexPrice)/SUM(itemTotalPrice) as itemIndex,
			SUM(itemIndexPrice) as itemIndexPrice,
			SUM(itemDiscount)/SUM(itemIndexPrice) as itemDiscountPerc,
			SUM(itemDiscount) as itemDiscount,
			SUM(itemTotal) as itemTotal,
			a.Currency
		FROM (SELECT * FROM billing_getfulldetailedusage(@p_subscriber_id,@p_period_from,@p_period_to,@p_detail,@p_OnlyBillable)) as a
		GROUP BY	a.subscriberID,
				a.subscriberSSN, 
				a.subscriberName,
				a.departmentName, --v1.2 departmentName
				a.Currency
		ORDER BY a.subscriberSSN,departmentName --v1.2 departmentName


	end
	if (@p_detail >= 1)
	begin
		INSERT INTO @ret
		SELECT * FROM billing_getfulldetailedusage(@p_subscriber_id,@p_period_from,@p_period_to,@p_detail,@p_OnlyBillable) ORDER BY subscriberSSN,departmentName --v1.2 departmentName;
	end
	return
end
