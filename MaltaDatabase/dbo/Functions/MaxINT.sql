﻿CREATE FUNCTION dbo.MaxINT(@nFirst int , @nSecond int)
RETURNS int AS 
BEGIN 
	IF (@nFirst < @nSecond)
	BEGIN
		return @nSecond;
	END
	return @nFirst;
END
