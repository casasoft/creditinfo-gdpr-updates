﻿CREATE FUNCTION dbo.MaxDate(@nFirst datetime , @nSecond datetime)
RETURNS datetime AS 
BEGIN 
	IF (@nFirst < @nSecond)
	BEGIN
		return @nSecond;
	END
	return @nFirst;
END
