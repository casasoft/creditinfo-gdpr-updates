﻿CREATE FUNCTION dbo.MinDate(@nFirst Datetime , @nSecond datetime)
RETURNS datetime AS 
BEGIN 
	IF (@nFirst > @nSecond)
	BEGIN
		return @nSecond;
	END
	return @nFirst;
END
