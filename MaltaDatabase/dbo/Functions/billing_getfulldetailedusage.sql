﻿
CREATE function billing_getfulldetailedusage(@p_subscriber_id int,@p_period_from datetime, @p_period_to datetime, @p_detail int,@p_OnlyBillable bit)
returns @ret table (
 subscriberID int,
 subscriberSSN nvarchar(15),
 subscriberName nvarchar(100),
 departmentName nvarchar(100), --v1.2 DepartmentName
 agreementNumber nvarchar(50),
 useDesc nvarchar(50),
 financialkey nvarchar(50),
 itemNameNative nvarchar(1000),
 itemCount int,
 itemType nvarchar(3),
 itemPrice money,
 itemTotalPrice money,
 itemIndex money,
 itemIndexPrice money,
 itemDiscountPerc decimal(5,2),
 itemDiscount money,
 itemTotal money,
 currency char(3))
begin
	INSERT INTO @ret
	--Samningsgjald
	select	--*
		a.creditinfoid as subscriberID,
		ISNULL(b.BillingAccount,(SELECT number FROM np_idnumbers where creditinfoid=a.creditinfoid and NumberTypeID=1)) as subscriberSSN, --v1.2
		(SELECT dbo.getCISubscriberName(a.creditinfoid,1)) as subscriberName,
		ISNULL(b.NameNative,''), --v1.2 Department name.
		a.agreementNumber,
		'Agreement price' as useDesc,
		'none' as AccKey,
		a.AgreementDescription as itemNameNative,
		a.agreementUsage as itemCount,
		'PCS' as itemType,
		a.agreementamount as itemPrice,
		a.agreementamount*agreementUsage as itemTotalPrice,
		1.0 as itemIndex,
		(a.agreementamount*agreementUsage) * 1.0 as itemIndexPrice,
		a.agreementDiscountpercent as itemDiscountPerc,
		a.agreementDiscount as itemDiscount,
		(a.agreementamount*agreementUsage)-a.agreementDiscount as itemTotal,
		a.Currency
	from	billing_getcharge(@p_subscriber_id,@p_period_from,@p_period_to,@p_OnlyBillable) a 
		left join c_au_department b on a.DepartmentID=b.DepartmentID --v1.2 Department
	where	a.agreementamount <> 0.0 
		and (a.creditinfoid=@p_subscriber_id or (@p_subscriber_id is null))
		--Group it because the agreement is on each item, we only want to charge it ones.
		--Group by a.creditinfoid,b.identifier,b.SubscriberNickName,a.agreementNumber,a.AgreementDescription,a.agreementUsage,a.agreementamount,a.agreementDiscountpercent,a.agreementDiscount,a.Currency
		Group by a.creditinfoid,b.NameNative,a.agreementNumber,a.AgreementDescription,a.agreementUsage,a.agreementamount,a.agreementDiscountpercent,a.agreementDiscount,a.Currency,b.BillingAccount

	if (@p_detail <> 1)
	begin
		INSERT INTO @ret
		--Fixed price
		select	--*
	 		a.creditinfoid as subscriberID,
			ISNULL(b.BillingAccount,(SELECT number FROM np_idnumbers where creditinfoid=a.creditinfoid and NumberTypeID=1)) as subscriberSSN, --v1.2
			(SELECT dbo.getCISubscriberName(a.creditinfoid,1)) as subscriberName,
			ISNULL(b.NameNative,''), --v1.2 Department name.
			a.agreementNumber,
			'Fixed price' as useDesc,
			financialkey as AccountKey,
			a.itemNameNative,
			fixedUsage as itemCount,
			'PCS' as itemType,
			fixedamount as itemPrice,
			fixedamount*fixedUsage as itemTotalPrice,
			UsageIndex as itemIndex,
			(fixedamount*fixedUsage) * UsageIndex as itemIndexPrice,
			a.fixeddiscountpercent as itemDiscountPerc,
			a.fixedamountDiscount as itemDiscount,
			(a.fixedamount*fixedUsage*UsageIndex)-a.fixedamountDiscount as itemTotal,
			a.currency
		from	billing_getcharge(@p_subscriber_id,@p_period_from,@p_period_to,@p_OnlyBillable) a
			left join c_au_department b on a.DepartmentID=b.DepartmentID --v1.2 Department
		where	a.fixedamount <> 0.0
			and (a.creditinfoid=@p_subscriber_id or (@p_subscriber_id is null))
		union all
		--Monly price
		select	--*
	 		a.creditinfoid as subscriberID,
			ISNULL(b.BillingAccount,(SELECT number FROM np_idnumbers where creditinfoid=a.creditinfoid and NumberTypeID=1)) as subscriberSSN, --v1.2
			(SELECT dbo.getCISubscriberName(a.creditinfoid,1)) as subscriberName,
			ISNULL(b.NameNative,''), --v1.2 Department name.
			a.agreementnumber,
			'Monthly price' as UseType,
			financialkey as AccKey,
			a.itemNameNative,
			montlyUsage as itemCount,
			'PCS' as itemType,
			monthlyamount as itemPrice,
			monthlyamount*montlyUsage as itemTotalPrice,
			UsageIndex as itemIndex,
			(monthlyamount*montlyUsage) *UsageIndex as itemIndexPrice,
			a.Monthlydiscountpercent as itemDiscountPerc,
			a.monthlyamountDiscount as itemDiscount,
			(a.monthlyamount*montlyUsage*UsageIndex)-a.monthlyamountDiscount as itemTotal,
			Currency
		from	billing_getcharge(@p_subscriber_id,@p_period_from,@p_period_to,@p_OnlyBillable) a
			left join c_au_department b on a.DepartmentID=b.DepartmentID --v1.2 Department
		where	a.monthlyamount <> 0.0
			and (a.creditinfoid=@p_subscriber_id or (@p_subscriber_id is null))
		union all
		--Item usage price
		select	--*
	 		a.creditinfoid as subscriberID,
			ISNULL(b.BillingAccount,(SELECT number FROM np_idnumbers where creditinfoid=a.creditinfoid and NumberTypeID=1)) as subscriberSSN, --v1.2
			(SELECT dbo.getCISubscriberName(a.creditinfoid,1)) as subscriberName,
			ISNULL(b.NameNative,''), --v1.2 Department name.
			a.agreementNumber,
			'Usage price' as useDesc,
			financialkey as AccountKey,
			a.itemNameNative,
			usage as itemCount,
			'PCS' as itemType,
			usageamount as itemPrice,
			usageamount*usage as itemTotalPrice,
			UsageIndex as itemIndex,
			(usageamount*usage) * UsageIndex as itemIndexPrice,
			a.Usagediscountpercent as itemDiscountPerc,
			a.itemDiscount as itemDiscount,
			(a.usageamount*usage*UsageIndex)-a.itemDiscount as itemTotal,
			Currency
		from	billing_getcharge(@p_subscriber_id,@p_period_from,@p_period_to,@p_OnlyBillable) a
			left join c_au_department b on a.DepartmentID=b.DepartmentID --v1.2 Department
		where	a.usage > 0 and a.usageamount > 0.0
			and (a.creditinfoid=@p_subscriber_id or (@p_subscriber_id is null))
		union all
		--Free zone Item usage price
		select	--*
	 		a.creditinfoid as subscriberID,
			ISNULL(b.BillingAccount,(SELECT number FROM np_idnumbers where creditinfoid=a.creditinfoid and NumberTypeID=1)) as subscriberSSN, --v1.2
			(SELECT dbo.getCISubscriberName(a.creditinfoid,1)) as subscriberName,
			ISNULL(b.NameNative,''), --v1.2 Department name.
			a.agreementNumber,
			'Usage price (freezone)' as useDesc,
			financialkey as AccountKey,
			a.itemNameNative as itemNameNative,
			freeZone as itemCount,
			'PCS' as itemType,
			usageamount as itemPrice,
			usageamount*freeZone as itemTotalPrice,
			UsageIndex as itemIndex,
			(usageamount*freeZone) * UsageIndex as itemIndexPrice,
			100.0 itemDiscountPerc,  --free zone 100% discount.
			(usageamount*freeZone) * UsageIndex as itemDiscount,
			0.0 as itemTotal,
			Currency
		from	billing_getcharge(@p_subscriber_id,@p_period_from,@p_period_to,@p_OnlyBillable) a
			left join c_au_department b on a.DepartmentID=b.DepartmentID --v1.2 Department
		where	a.freeZone > 0 and a.usageamount > 0.0
			and (a.creditinfoid=@p_subscriber_id or (@p_subscriber_id is null))
	end
	return
end
