﻿CREATE TABLE [dbo].[mt_PendingCases] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [DefendantID]   VARCHAR (10)  NULL,
    [DefendantName] VARCHAR (50)  NULL,
    [PlaintiffID]   VARCHAR (10)  NULL,
    [PlaintiffName] VARCHAR (50)  NULL,
    [Court]         INT           NULL,
    [Adjudicator]   INT           NULL,
    [DateOfWrit]    DATETIME      NOT NULL,
    [WritNumber]    VARCHAR (10)  NULL,
    [ValueOfSuit]   MONEY         NULL,
    [Comments]      VARCHAR (255) NULL,
    [StatusID]      INT           CONSTRAINT [DF_mt_PendingCases_StatusID] DEFAULT (1) NOT NULL,
    [CurrencyCode]  NVARCHAR (3)  NULL
);


GO
CREATE CLUSTERED INDEX [PK_MT_PendingCases]
    ON [dbo].[mt_PendingCases]([ID] ASC) WITH (FILLFACTOR = 90);

