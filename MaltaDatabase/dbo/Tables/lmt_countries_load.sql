﻿CREATE TABLE [dbo].[lmt_countries_load] (
    [countryid] VARCHAR (20)   COLLATE Icelandic_CI_AS NOT NULL,
    [country]   NVARCHAR (100) NULL,
    CONSTRAINT [PK__lmt_countries_lo__22751F6C] PRIMARY KEY CLUSTERED ([countryid] ASC) WITH (FILLFACTOR = 90)
);

