﻿CREATE TABLE [dbo].[np_Case] (
    [ClaimTypeID]       INT            IDENTITY (1, 1) NOT NULL,
    [TypeEN]            NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [TypeNative]        NVARCHAR (100) NULL,
    [DescriptionEN]     NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative] NVARCHAR (100) NULL,
    [ValidMonths]       INT            NULL,
    [ValidComment]      VARCHAR (100)  NULL,
    CONSTRAINT [np_Case_PK] PRIMARY KEY CLUSTERED ([ClaimTypeID] ASC) WITH (FILLFACTOR = 90)
);

