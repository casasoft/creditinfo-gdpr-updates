﻿CREATE TABLE [dbo].[svetax] (
    [CreditInfoID] INT            NULL,
    [WatchID]      INT            NOT NULL,
    [UniqueID]     NVARCHAR (100) NULL,
    [IsCompany]    CHAR (10)      NULL,
    [Sent]         CHAR (10)      NULL,
    [Created]      DATETIME       NULL,
    [UserID]       INT            NOT NULL,
    [sentDate]     SMALLDATETIME  NULL
);

