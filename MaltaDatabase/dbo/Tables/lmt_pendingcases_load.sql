﻿CREATE TABLE [dbo].[lmt_pendingcases_load] (
    [DebtorId]      NVARCHAR (100) NULL,
    [CreditorId]    NVARCHAR (100) NULL,
    [Court]         NVARCHAR (100) NULL,
    [CourtCategory] NVARCHAR (100) NULL,
    [Judicary]      NVARCHAR (100) NULL,
    [Reference]     NVARCHAR (100) NULL,
    [RegDate]       DATETIME       NULL,
    [ClaimDate]     DATETIME       NULL,
    [Litigant1]     NVARCHAR (255) NULL,
    [Litigant2]     NVARCHAR (255) NULL,
    [ClaimValue]    MONEY          NULL,
    [CourtID]       INT            NULL,
    [AdjudicatorID] INT            NULL,
    [Currency]      NCHAR (3)      NULL,
    [Rid]           INT            IDENTITY (1, 1) NOT NULL,
    [Inserted]      DATETIME       NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_lmtpendingcasesload_rid]
    ON [dbo].[lmt_pendingcases_load]([Rid] ASC) WITH (FILLFACTOR = 90);

