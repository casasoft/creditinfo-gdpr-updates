﻿CREATE TABLE [dbo].[FSI_FieldValidations] (
    [ID]                INT           IDENTITY (1, 1) NOT NULL,
    [NameNative]        VARCHAR (50)  NULL,
    [NameEN]            VARCHAR (50)  NULL,
    [DescriptionNative] VARCHAR (512) NULL,
    [DescriptionEN]     VARCHAR (512) NULL
);

