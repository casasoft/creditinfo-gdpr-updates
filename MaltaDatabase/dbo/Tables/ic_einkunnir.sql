﻿CREATE TABLE [dbo].[ic_einkunnir] (
    [einkunn_id]   INT           NOT NULL,
    [einkunn_type] INT           NOT NULL,
    [hierarchy]    INT           NULL,
    [rating]       VARCHAR (255) NULL,
    [rating_en]    VARCHAR (255) NULL
);

