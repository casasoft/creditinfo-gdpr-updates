﻿CREATE TABLE [dbo].[lmt_mfsa_companystates_old] (
    [State_ID] INT            NULL,
    [State]    NVARCHAR (100) NULL,
    [Ordering] INT            NULL
);


GO
CREATE CLUSTERED INDEX [ix_cdcompanystates_stateid]
    ON [dbo].[lmt_mfsa_companystates_old]([State_ID] ASC) WITH (FILLFACTOR = 90);

