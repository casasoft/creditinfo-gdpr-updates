﻿CREATE TABLE [dbo].[lmt_companiesannualreturns_load] (
    [CompanyID]    VARCHAR (50) NULL,
    [DataArchived] DATETIME     NULL,
    [DocumentType] VARCHAR (50) NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_companiesannualreturns_load]
    ON [dbo].[lmt_companiesannualreturns_load]([CompanyID] ASC) WITH (FILLFACTOR = 90);

