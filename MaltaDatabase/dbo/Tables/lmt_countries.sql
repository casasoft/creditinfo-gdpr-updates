﻿CREATE TABLE [dbo].[lmt_countries] (
    [countryid] VARCHAR (20)   NOT NULL,
    [country]   NVARCHAR (100) NULL,
    CONSTRAINT [PK_cd_countries] PRIMARY KEY CLUSTERED ([countryid] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [ix_cdcountry_countrid]
    ON [dbo].[lmt_countries]([countryid] ASC) WITH (FILLFACTOR = 100);

