﻿CREATE TABLE [dbo].[sys_dts_Monitoring] (
    [id]          INT              IDENTITY (1, 1) NOT NULL,
    [dtsName]     NVARCHAR (50)    NOT NULL,
    [dtsStepName] NVARCHAR (50)    NOT NULL,
    [logDate]     DATETIME         CONSTRAINT [DF_sys_dts_Monitoring_logDate] DEFAULT (getdate()) NOT NULL,
    [status]      INT              CONSTRAINT [DF_sys_dts_Monitoring_status] DEFAULT ((0)) NOT NULL,
    [dtsId]       UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_sys_dts_Monitoring] PRIMARY KEY CLUSTERED ([id] ASC)
);

