﻿CREATE TABLE [dbo].[cpi_RealEstatesType] (
    [RealEstatesTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [TypeNative]        NVARCHAR (255) CONSTRAINT [DF__cpi_RealE__TypeN__2DE6D218] DEFAULT (N'0') NULL,
    [TypeEN]            VARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_RealE__TypeE__2EDAF651] DEFAULT ('0') NULL,
    CONSTRAINT [PK_cpi_RealEstatesType] PRIMARY KEY CLUSTERED ([RealEstatesTypeID] ASC) WITH (FILLFACTOR = 90)
);

