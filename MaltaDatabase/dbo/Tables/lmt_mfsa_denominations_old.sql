﻿CREATE TABLE [dbo].[lmt_mfsa_denominations_old] (
    [Denomination_ID] INT          NULL,
    [Denomination]    VARCHAR (10) NULL,
    [Xrate]           FLOAT (53)   NULL
);


GO
CREATE CLUSTERED INDEX [ix_cddenominatons_denominationid]
    ON [dbo].[lmt_mfsa_denominations_old]([Denomination_ID] ASC) WITH (FILLFACTOR = 90);

