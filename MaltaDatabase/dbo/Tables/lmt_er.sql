﻿CREATE TABLE [dbo].[lmt_er] (
    [Name]      NVARCHAR (255) NULL,
    [AKA]       NVARCHAR (255) NULL,
    [Town]      NVARCHAR (255) NULL,
    [Street]    NVARCHAR (255) NULL,
    [House]     NVARCHAR (255) NULL,
    [ID_Number] VARCHAR (20)   NOT NULL,
    [created]   DATETIME       DEFAULT (getdate()) NULL,
    [updated]   DATETIME       NULL,
    CONSTRAINT [PK_lmt_er] PRIMARY KEY CLUSTERED ([ID_Number] ASC) WITH (FILLFACTOR = 90)
);

