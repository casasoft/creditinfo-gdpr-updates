﻿CREATE TABLE [dbo].[cpi_RealEstates] (
    [CompanyCIID]           INT            CONSTRAINT [DF__cpi_RealE__Compa__662B2B3B] DEFAULT (0) NOT NULL,
    [RealEstatesID]         INT            IDENTITY (1, 1) NOT NULL,
    [RealEstatesLocationID] INT            CONSTRAINT [DF__cpi_RealE__RealE__681373AD] DEFAULT ('0') NOT NULL,
    [PostCode]              NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_RealE__PostC__690797E6] DEFAULT (0) NULL,
    [OwnerShipID]           INT            CONSTRAINT [DF__cpi_RealE__Owner__69FBBC1F] DEFAULT (0) NULL,
    [InsuranceValue]        FLOAT (53)     CONSTRAINT [DF__cpi_RealE__Insur__6AEFE058] DEFAULT (0) NULL,
    [MarkedValue]           FLOAT (53)     CONSTRAINT [DF__cpi_RealE__Marke__6BE40491] DEFAULT (0) NULL,
    [Mortage]               FLOAT (53)     CONSTRAINT [DF__cpi_RealE__Morta__6CD828CA] DEFAULT (0) NULL,
    [Size]                  VARCHAR (9)    CONSTRAINT [DF__cpi_RealEs__Size__6DCC4D03] DEFAULT ('0') NULL,
    [YearBuilt]             VARCHAR (20)   CONSTRAINT [DF__cpi_RealE__YearB__6EC0713C] DEFAULT ('0') NULL,
    [RealEstatesTypeID]     INT            CONSTRAINT [DF__cpi_RealE__RealE__6FB49575] DEFAULT (0) NULL,
    [OwnerTypeID]           INT            CONSTRAINT [DF__cpi_RealE__Owner__70A8B9AE] DEFAULT (0) NULL,
    [AddressNative]         NVARCHAR (100) NULL,
    [AddressEN]             NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [CountryID]             INT            NOT NULL,
    [CityID]                INT            NOT NULL,
    CONSTRAINT [ct_ic_fasteignir1] PRIMARY KEY CLUSTERED ([RealEstatesID] ASC, [CompanyCIID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_RealEstates_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID]),
    CONSTRAINT [cpi_RealEstatesOwnerType_cpi_RealEstates_FK1] FOREIGN KEY ([OwnerTypeID]) REFERENCES [dbo].[cpi_RealEstatesOwnerType] ([OwnerTypeID]),
    CONSTRAINT [FK_cpi_RealEstates_cpi_Countries] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[cpi_Countries] ([CountryID]),
    CONSTRAINT [FK_cpi_RealEstates_cpi_EstatesPostalCode] FOREIGN KEY ([PostCode]) REFERENCES [dbo].[cpi_EstatesPostalCode] ([PostalCode]),
    CONSTRAINT [FK_cpi_RealEstates_cpi_RealEstatesLocation] FOREIGN KEY ([RealEstatesLocationID]) REFERENCES [dbo].[cpi_RealEstatesLocation] ([LocationID]),
    CONSTRAINT [FK_cpi_RealEstates_cpi_RealEstatesType] FOREIGN KEY ([RealEstatesTypeID]) REFERENCES [dbo].[cpi_RealEstatesType] ([RealEstatesTypeID]),
    CONSTRAINT [FK_cpi_RealEstates_np_City] FOREIGN KEY ([CityID]) REFERENCES [dbo].[np_City] ([CityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_ic_fasteignir1]
    ON [dbo].[cpi_RealEstates]([CompanyCIID] ASC, [RealEstatesID] ASC) WITH (FILLFACTOR = 90);

