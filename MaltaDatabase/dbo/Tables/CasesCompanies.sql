﻿CREATE TABLE [dbo].[CasesCompanies] (
    [ID]                     INT           NOT NULL,
    [RegisterdBy]            NVARCHAR (50) NULL,
    [CaseNo]                 NVARCHAR (50) NULL,
    [CaseType]               NVARCHAR (50) NULL,
    [InformationSource]      NVARCHAR (50) NULL,
    [Claimownername]         NVARCHAR (50) NULL,
    [GazetteYear]            INT           NULL,
    [GazettePage]            INT           NULL,
    [Amount]                 NVARCHAR (50) NULL,
    [CompRegno]              NVARCHAR (50) NULL,
    [CompName]               NVARCHAR (50) NULL,
    [DebtorAddress]          NVARCHAR (50) NULL,
    [DebtorTown]             NVARCHAR (50) NULL,
    [Administrator]          NVARCHAR (50) NULL,
    [AppliedForByDebtor]     BIT           NOT NULL,
    [Date]                   SMALLDATETIME NULL,
    [Comments]               NTEXT         NULL,
    [creditinfoid]           INT           NULL,
    [newinformationsourceid] INT           NULL
);

