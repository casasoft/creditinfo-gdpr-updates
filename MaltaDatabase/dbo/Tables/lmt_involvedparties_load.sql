﻿CREATE TABLE [dbo].[lmt_involvedparties_load] (
    [involvedpartyid]       INT            NOT NULL,
    [surname]               NVARCHAR (100) NULL,
    [name]                  NVARCHAR (100) NULL,
    [titleid]               INT            NULL,
    [idcardpassport]        NVARCHAR (50)  NULL,
    [companyname]           NVARCHAR (150) NULL,
    [companyregistrationno] NVARCHAR (100) NULL,
    [nationalityid]         INT            NULL,
    [involvedaddress1]      NVARCHAR (100) NULL,
    [involvedaddress2]      NVARCHAR (100) NULL,
    [locality]              NVARCHAR (255) NULL,
    [postcode]              NVARCHAR (100) NULL,
    [countryid]             NVARCHAR (50)  NULL,
    CONSTRAINT [PK__lmt_involvedpart__7F16D496] PRIMARY KEY CLUSTERED ([involvedpartyid] ASC) WITH (FILLFACTOR = 90)
);

