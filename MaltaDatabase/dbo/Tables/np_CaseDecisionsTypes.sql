﻿CREATE TABLE [dbo].[np_CaseDecisionsTypes] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [TypeNative]        NVARCHAR (100) NULL,
    [TypeEN]            NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [StatusType]        VARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [AppliedByDebtor]   VARCHAR (20)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative] NVARCHAR (100) NULL,
    [DescriptionEN]     NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_np_CaseDecisionsTypes] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

