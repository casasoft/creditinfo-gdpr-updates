﻿CREATE TABLE [dbo].[cpi_HistoryTypes] (
    [HistoryTypeID] INT           NOT NULL,
    [NameNative]    NVARCHAR (50) NULL,
    [NameEN]        VARCHAR (50)  NULL
);

