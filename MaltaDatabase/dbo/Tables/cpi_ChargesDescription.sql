﻿CREATE TABLE [dbo].[cpi_ChargesDescription] (
    [DescriptionID]     INT             IDENTITY (1, 1) NOT NULL,
    [DescriptionEN]     NVARCHAR (1024) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative] NVARCHAR (1024) NULL,
    CONSTRAINT [PK_cpi_ChargesDescription] PRIMARY KEY CLUSTERED ([DescriptionID] ASC) WITH (FILLFACTOR = 90)
);

