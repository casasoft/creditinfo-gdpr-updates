﻿CREATE TABLE [dbo].[lmt_VAT] (
    [VAT #]              NVARCHAR (255) NULL,
    [ID#]                NVARCHAR (255) NULL,
    [OfficialTraderName] NVARCHAR (255) NULL,
    [Building]           NVARCHAR (255) NULL,
    [Address]            NVARCHAR (255) NULL,
    [Street]             NVARCHAR (255) NULL,
    [City]               NVARCHAR (255) NULL,
    [Postcode]           NVARCHAR (255) NULL,
    [Tel]                NVARCHAR (255) NULL,
    [Fax]                NVARCHAR (50)  NULL,
    [Note]               NVARCHAR (255) NULL,
    [Show]               INT            NOT NULL
);

