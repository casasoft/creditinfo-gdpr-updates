﻿CREATE TABLE [dbo].[cpi_CompanyPrincipals] (
    [PrincipalCIID] INT NOT NULL,
    [CompanyCIID]   INT NOT NULL,
    [JobTitleID]    INT NULL,
    [EducationID]   INT NULL,
    CONSTRAINT [cpi_CompanyPrincipals_PK] PRIMARY KEY CLUSTERED ([PrincipalCIID] ASC, [CompanyCIID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_CompanyPrincipals_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID]),
    CONSTRAINT [cpi_JobTitle_cpi_CompanyPrincipals_FK1] FOREIGN KEY ([JobTitleID]) REFERENCES [dbo].[cpi_JobTitle] ([JobTitleID]),
    CONSTRAINT [FK_cpi_CompanyPrincipals_np_Education] FOREIGN KEY ([EducationID]) REFERENCES [dbo].[np_Education] ([EducationID])
);

