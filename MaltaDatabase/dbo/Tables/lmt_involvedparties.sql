﻿CREATE TABLE [dbo].[lmt_involvedparties] (
    [involvedpartyid]       INT            NOT NULL,
    [surname]               NVARCHAR (100) NULL,
    [name]                  NVARCHAR (100) NULL,
    [titleid]               INT            NULL,
    [idcardpassport]        NVARCHAR (50)  NULL,
    [companyname]           NVARCHAR (150) NULL,
    [companyregistrationno] NVARCHAR (100) NULL,
    [nationalityid]         INT            NULL,
    [involvedaddress1]      NVARCHAR (100) NULL,
    [involvedaddress2]      NVARCHAR (100) NULL,
    [locality]              NVARCHAR (255) NULL,
    [postcode]              NVARCHAR (100) NULL,
    [countryid]             NVARCHAR (50)  NULL,
    CONSTRAINT [PK_cd_involvedparties] PRIMARY KEY CLUSTERED ([involvedpartyid] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [cd_involvedparties_involvedpartyid]
    ON [dbo].[lmt_involvedparties]([involvedpartyid] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [ix_lmtmfsainvolvedparties_companyregistrationno]
    ON [dbo].[lmt_involvedparties]([companyregistrationno] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [ix_lmtinvolvedparties_idcardpassport]
    ON [dbo].[lmt_involvedparties]([idcardpassport] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [ix_lmtinvolvedparties_companyregistrationno]
    ON [dbo].[lmt_involvedparties]([companyregistrationno] ASC) WITH (FILLFACTOR = 100);

