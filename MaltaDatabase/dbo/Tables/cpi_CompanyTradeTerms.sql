﻿CREATE TABLE [dbo].[cpi_CompanyTradeTerms] (
    [TermsID]         INT          CONSTRAINT [DF__cpi_Trade__Terms__22751F6C] DEFAULT (0) NOT NULL,
    [SalesTerm]       VARCHAR (10) CONSTRAINT [DF__cpi_Trade__is_sa__25518C17] DEFAULT ('True') NOT NULL,
    [CompanyCIID]     INT          NOT NULL,
    [CommercialTrade] VARCHAR (10) CONSTRAINT [DF_cpi_CompanyTradeTerms_CommercialTrade] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_cpi_TradeTerms] PRIMARY KEY CLUSTERED ([TermsID] ASC, [SalesTerm] ASC, [CompanyCIID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_cpi_CompanyTradeTerms_cpi_TradeTerms] FOREIGN KEY ([TermsID]) REFERENCES [dbo].[cpi_TradeTerms] ([TermsID]),
    CONSTRAINT [FK_cpi_CompanyTradeTerms_np_CreditInfoUser] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);

