﻿CREATE TABLE [dbo].[lmt_nationalities_load] (
    [nationalityid] INT            NOT NULL,
    [nationality]   NVARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    PRIMARY KEY CLUSTERED ([nationalityid] ASC) WITH (FILLFACTOR = 90)
);

