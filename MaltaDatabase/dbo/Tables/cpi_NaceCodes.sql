﻿CREATE TABLE [dbo].[cpi_NaceCodes] (
    [NaceCodeID]             VARCHAR (10)   NOT NULL,
    [DescriptionNative]      NVARCHAR (255) NULL,
    [DescriptionEN]          VARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [ShortDescriptionNative] NVARCHAR (255) NULL,
    [ShortDescriptionEN]     VARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [AbbreviationNative]     NVARCHAR (100) NULL,
    [AbbreviationEN]         VARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DefinitionNative]       NVARCHAR (100) NULL,
    [DefinitionEN]           VARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [CodeType]               VARCHAR (50)   NULL,
    CONSTRAINT [cpi_NaceCodes_PK] PRIMARY KEY CLUSTERED ([NaceCodeID] ASC) WITH (FILLFACTOR = 90)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'F.ex. isat95 or version of NaceCodes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'cpi_NaceCodes', @level2type = N'COLUMN', @level2name = N'CodeType';

