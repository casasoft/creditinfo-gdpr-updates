﻿CREATE TABLE [dbo].[ic_education] (
    [id]           INT           NOT NULL,
    [education]    VARCHAR (255) NULL,
    [education_en] VARCHAR (255) NULL,
    [education_dk] VARCHAR (255) NULL
);

