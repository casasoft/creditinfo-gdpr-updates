﻿CREATE TABLE [dbo].[stats_cube] (
    [cube_id]     INT           NOT NULL,
    [nameNative]  VARCHAR (50)  COLLATE SQL_Latin1_General_CP1253_CI_AI NULL,
    [nameEN]      VARCHAR (50)  COLLATE SQL_Latin1_General_CP1253_CI_AI NULL,
    [nameDB]      VARCHAR (50)  COLLATE SQL_Latin1_General_CP1253_CI_AI NOT NULL,
    [description] VARCHAR (100) COLLATE SQL_Latin1_General_CP1253_CI_AI NULL
);

