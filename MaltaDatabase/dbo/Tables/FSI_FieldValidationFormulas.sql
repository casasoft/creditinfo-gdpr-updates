﻿CREATE TABLE [dbo].[FSI_FieldValidationFormulas] (
    [ID]                 INT          IDENTITY (1, 1) NOT NULL,
    [ValidationID]       INT          NOT NULL,
    [ValidationItemType] VARCHAR (10) NOT NULL,
    [ValidationItemID]   INT          NOT NULL,
    [Order]              INT          NOT NULL
);

