﻿CREATE TABLE [dbo].[cpi_Countries] (
    [CountryID]   INT            IDENTITY (1, 1) NOT NULL,
    [NameNative]  NVARCHAR (100) NULL,
    [NameEN]      VARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [ContinentID] INT            NULL,
    CONSTRAINT [np_Countries_PK] PRIMARY KEY CLUSTERED ([CountryID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_Continents_np_Countries_FK1] FOREIGN KEY ([ContinentID]) REFERENCES [dbo].[cpi_Continents] ([ContinentID])
);

