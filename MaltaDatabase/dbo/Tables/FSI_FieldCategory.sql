﻿CREATE TABLE [dbo].[FSI_FieldCategory] (
    [ID]         INT          IDENTITY (1, 1) NOT NULL,
    [NameNative] VARCHAR (50) NULL,
    [NameEN]     VARCHAR (50) NULL
);

