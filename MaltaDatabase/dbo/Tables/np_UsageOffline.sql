﻿CREATE TABLE [dbo].[np_UsageOffline] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [CreditInfoID]    INT           NOT NULL,
    [QueryType]       INT           NOT NULL,
    [Query]           NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [ResultCount]     INT           NULL,
    [IP]              VARCHAR (15)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Created]         DATETIME      CONSTRAINT [DF_np_UsageOffline_Created] DEFAULT (getdate()) NULL,
    [UserID]          INT           NOT NULL,
    [DeliverySpeedID] INT           CONSTRAINT [DF_np_UsageOffline_DeliverySpeedID] DEFAULT (1) NOT NULL,
    CONSTRAINT [PK_np_UsageOffline] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_UsageOffline_ros_DeliverySpeed] FOREIGN KEY ([DeliverySpeedID]) REFERENCES [dbo].[ros_DeliverySpeed] ([ID])
);

