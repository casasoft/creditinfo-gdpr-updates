﻿CREATE TABLE [dbo].[lmt_mfsa_Country_load] (
    [Country_ID] INT            NOT NULL,
    [Country]    NVARCHAR (255) NULL,
    [Local]      INT            NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_mfsa_Country_load]
    ON [dbo].[lmt_mfsa_Country_load]([Country_ID] ASC) WITH (FILLFACTOR = 90);

