﻿CREATE TABLE [dbo].[stats_report] (
    [report_id]    INT          IDENTITY (1, 1) NOT NULL,
    [xmldata]      TEXT         COLLATE SQL_Latin1_General_CP1253_CI_AI NOT NULL,
    [name]         VARCHAR (50) COLLATE SQL_Latin1_General_CP1253_CI_AI NOT NULL,
    [ispublic]     BIT          NOT NULL,
    [creditinfoid] INT          NOT NULL,
    [created]      DATETIME     NOT NULL,
    [cube_id]      INT          NOT NULL
);

