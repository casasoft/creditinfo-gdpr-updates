﻿CREATE TABLE [dbo].[c_vc_configs] (
    [ConfigName]  VARCHAR (10)  NOT NULL,
    [ConfigValue] VARCHAR (255) NOT NULL,
    [Inserted]    DATETIME      CONSTRAINT [DF__c_vc_conf__Inser__5B4E756C] DEFAULT (getdate()) NOT NULL,
    [Updated]     DATETIME      NULL,
    CONSTRAINT [PK__c_vc_configs__5A5A5133] PRIMARY KEY CLUSTERED ([ConfigName] ASC) WITH (FILLFACTOR = 90)
);

