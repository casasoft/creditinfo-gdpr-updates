﻿CREATE TABLE [dbo].[billing_IndexesOnAgreementItems] (
    [ItemID]     INT           NOT NULL,
    [IndexName]  NVARCHAR (20) NOT NULL,
    [Inserted]   DATETIME      NOT NULL,
    [InsertedBy] INT           NOT NULL,
    CONSTRAINT [PK_billing_IndexesOnAgreementItems] PRIMARY KEY CLUSTERED ([ItemID] ASC, [IndexName] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_billing_IndexesOnAgreementItems_billing_AgreementItems] FOREIGN KEY ([ItemID]) REFERENCES [dbo].[billing_AgreementItems] ([ID]),
    CONSTRAINT [FK_billing_IndexesOnAgreementItems_billing_Indexes] FOREIGN KEY ([IndexName]) REFERENCES [dbo].[billing_Indexes] ([IndexName])
);

