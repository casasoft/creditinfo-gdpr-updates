﻿CREATE TABLE [dbo].[lmt_nationalities] (
    [nationalityid] INT            NOT NULL,
    [nationality]   NVARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    CONSTRAINT [PK_cd_nationality] PRIMARY KEY CLUSTERED ([nationalityid] ASC) WITH (FILLFACTOR = 100)
);

