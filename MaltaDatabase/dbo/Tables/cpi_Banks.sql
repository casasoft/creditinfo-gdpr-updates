﻿CREATE TABLE [dbo].[cpi_Banks] (
    [BankID]        VARCHAR (10)   NOT NULL,
    [NameNative]    NVARCHAR (255) CONSTRAINT [DF__cpi_Banks__NameN__7EF6D905] DEFAULT (N'0') NULL,
    [NameEN]        VARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Banks__NameE__7FEAFD3E] DEFAULT ('0') NULL,
    [Location]      VARCHAR (255)  CONSTRAINT [DF__cpi_Banks__Locat__00DF2177] DEFAULT ('0') NULL,
    [PostalCode]    VARCHAR (20)   CONSTRAINT [DF__cpi_Banks__Posta__01D345B0] DEFAULT ('0') NULL,
    [Tel]           VARCHAR (30)   CONSTRAINT [DF__cpi_Banks__Tel__02C769E9] DEFAULT ('0') NULL,
    [CommentNative] NTEXT          CONSTRAINT [DF__cpi_Banks__Comme__03BB8E22] DEFAULT (N'0') NULL,
    [CommentEN]     VARCHAR (30)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_cpi_Banks] PRIMARY KEY CLUSTERED ([BankID] ASC) WITH (FILLFACTOR = 90)
);

