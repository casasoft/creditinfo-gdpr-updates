﻿CREATE TABLE [dbo].[np_VatNumbers] (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [CreditInfoID] INT            NOT NULL,
    [Number]       NVARCHAR (255) NOT NULL,
    [TraderName]   NVARCHAR (255) NULL,
    [Building]     NVARCHAR (255) NULL,
    [Address]      NVARCHAR (255) NULL,
    [Street]       NVARCHAR (255) NULL,
    [City]         NVARCHAR (255) NULL,
    [PostCode]     NVARCHAR (255) NULL,
    [Tel]          NVARCHAR (255) NULL,
    [Fax]          NVARCHAR (64)  NULL,
    [Note]         NVARCHAR (255) NULL,
    [Show]         BIT            CONSTRAINT [DF_np_VatNumbers_Show] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_np_np_VatNumbers] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_VatNumbers_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);

