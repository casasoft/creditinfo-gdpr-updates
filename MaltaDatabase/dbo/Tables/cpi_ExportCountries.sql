﻿CREATE TABLE [dbo].[cpi_ExportCountries] (
    [CompanyCIID] INT NOT NULL,
    [CountryID]   INT NOT NULL,
    CONSTRAINT [cpi_ExportCountries_PK] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC, [CountryID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_ExportCountries_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID]),
    CONSTRAINT [np_Countries_cpi_ExportCountries_FK1] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[cpi_Countries] ([CountryID])
);

