﻿CREATE TABLE [dbo].[lmt_individuals] (
    [id]           INT           NULL,
    [location]     VARCHAR (100) NULL,
    [street]       VARCHAR (100) NULL,
    [name]         VARCHAR (100) NULL,
    [english_name] VARCHAR (100) NULL,
    [street_floor] VARCHAR (100) NULL,
    [ssno]         CHAR (10)     NULL,
    [last_upd]     VARCHAR (19)  NULL,
    [cig_cid]      INT           IDENTITY (1000, 1) NOT NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_individuals]
    ON [dbo].[lmt_individuals]([ssno] ASC) WITH (FILLFACTOR = 90);

