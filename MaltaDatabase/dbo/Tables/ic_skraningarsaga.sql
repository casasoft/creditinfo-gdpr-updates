﻿CREATE TABLE [dbo].[ic_skraningarsaga] (
    [ft_kennitala]            CHAR (10)     NOT NULL,
    [skraningarsaga_dags]     SMALLDATETIME NOT NULL,
    [skraningarsaga_texti]    TEXT          NULL,
    [skraningarsaga_texti_en] TEXT          NULL,
    [skraningarsaga_texti_dk] TEXT          NULL
);

