﻿CREATE TABLE [dbo].[cpi_Subsidiaries] (
    [SubsidariesID]    INT        NOT NULL,
    [Ownership]        FLOAT (53) CONSTRAINT [DF__cpi_Subsi__Owner__30C33EC3] DEFAULT (0) NULL,
    [RegistrationDate] DATETIME   CONSTRAINT [DF_cpi_Subsidiaries_RegistrationDate] DEFAULT (getdate()) NULL,
    [CompanyCIID]      INT        NOT NULL,
    CONSTRAINT [PK_cpi_Subsidiaries] PRIMARY KEY CLUSTERED ([SubsidariesID] ASC, [CompanyCIID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_cpi_Subsidiaries_np_CreditInfoUser] FOREIGN KEY ([SubsidariesID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID]),
    CONSTRAINT [FK_cpi_Subsidiaries_np_CreditInfoUser1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);

