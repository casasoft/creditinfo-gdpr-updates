﻿CREATE TABLE [dbo].[scorRiskBandsRanges] (
    [id]              INT             IDENTITY (1, 1) NOT NULL,
    [MinTest]         DECIMAL (18, 2) NOT NULL,
    [MaxTest]         DECIMAL (18, 2) NULL,
    [Value]           NVARCHAR (10)   NOT NULL,
    [RiskDescription] NVARCHAR (255)  NULL,
    [Color]           NVARCHAR (10)   NULL,
    CONSTRAINT [PK_scorRiskBandsRanges] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 70)
);

