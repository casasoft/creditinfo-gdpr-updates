﻿CREATE TABLE [dbo].[ros_AdditionalCustomerInfo] (
    [CreditInfoID]  INT             NOT NULL,
    [Email1]        NVARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Email2]        NVARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [RemarksEN]     NVARCHAR (1024) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [RemarksNative] NVARCHAR (1024) NULL,
    CONSTRAINT [PK_ros_AdditionalCustomerInfo] PRIMARY KEY CLUSTERED ([CreditInfoID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ros_AdditionalCustomerInfo_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);

