﻿CREATE TABLE [dbo].[lmt_mfsa_CompanyStates_load] (
    [State_ID] INT            NULL,
    [State]    NVARCHAR (255) NULL,
    [Ordering] INT            NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_mfsa_CompanyStates_load]
    ON [dbo].[lmt_mfsa_CompanyStates_load]([State_ID] ASC) WITH (FILLFACTOR = 90);

