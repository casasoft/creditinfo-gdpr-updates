﻿CREATE TABLE [dbo].[cpi_BoardMembers] (
    [BoardMemberCIID]      INT CONSTRAINT [DF__cpi_Board__Board__32AB8735] DEFAULT (0) NOT NULL,
    [ManagementPositionID] INT CONSTRAINT [DF__cpi_Board__Manag__339FAB6E] DEFAULT (0) NULL,
    [CompanyCIID]          INT NOT NULL,
    CONSTRAINT [PK_cpi_BoardMembers] PRIMARY KEY CLUSTERED ([BoardMemberCIID] ASC, [CompanyCIID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_BoardMembers_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID]),
    CONSTRAINT [cpi_ManagementPositons_cpi_BoardMembers_FK1] FOREIGN KEY ([ManagementPositionID]) REFERENCES [dbo].[cpi_ManagementPositions] ([ManagementPositionID]),
    CONSTRAINT [FK_cpi_BoardMembers_np_CreditInfoUser] FOREIGN KEY ([BoardMemberCIID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);


GO
CREATE NONCLUSTERED INDEX [cpi_BoardMembers_PK]
    ON [dbo].[cpi_BoardMembers]([BoardMemberCIID] ASC, [CompanyCIID] ASC) WITH (FILLFACTOR = 90);

