﻿CREATE TABLE [dbo].[lmt_coop0511] (
    [regno]      NVARCHAR (255) NULL,
    [name]       NVARCHAR (255) NULL,
    [address]    NVARCHAR (255) NULL,
    [street]     NVARCHAR (255) NULL,
    [city]       NVARCHAR (255) NULL,
    [postal]     NVARCHAR (255) NULL,
    [tel]        NVARCHAR (255) NULL,
    [Fax]        NVARCHAR (255) NULL,
    [Email]      NVARCHAR (255) NULL,
    [website]    NVARCHAR (255) NULL,
    [founded]    NVARCHAR (255) NULL,
    [cid]        INT            NULL,
    [cig_cityid] INT            NULL,
    [cig_isnew]  BIT            NULL
);

