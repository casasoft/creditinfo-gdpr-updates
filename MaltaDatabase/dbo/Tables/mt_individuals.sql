﻿CREATE TABLE [dbo].[mt_individuals] (
    [id]           INT           NULL,
    [location]     VARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    [street]       VARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    [name]         VARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    [english_name] VARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    [street_floor] VARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    [ssno]         CHAR (10)     COLLATE Icelandic_CI_AS NULL,
    [last_upd]     VARCHAR (19)  COLLATE Icelandic_CI_AS NULL
);

