﻿CREATE TABLE [dbo].[c_billing_AgreementItem_Department] (
    [DepartmentID]    INT NOT NULL,
    [AgreementItemID] INT NOT NULL,
    CONSTRAINT [PK_c_billing_AgreementItem_Department] PRIMARY KEY CLUSTERED ([DepartmentID] ASC, [AgreementItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_c_billing_AgreementItem_Department_billing_AgreementItems] FOREIGN KEY ([AgreementItemID]) REFERENCES [dbo].[billing_AgreementItems] ([ID]),
    CONSTRAINT [FK_c_billing_AgreementItem_Department_c_au_department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[c_au_department] ([DepartmentID])
);

