﻿CREATE TABLE [dbo].[c_au_department] (
    [DepartmentID]   INT            IDENTITY (1, 1) NOT NULL,
    [CreditInfoID]   INT            NOT NULL,
    [NameNative]     NVARCHAR (100) NULL,
    [NameEN]         NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Postcode]       NVARCHAR (30)  NULL,
    [StreetNative]   NVARCHAR (150) NULL,
    [StreetEN]       NVARCHAR (150) NULL,
    [Phone]          NVARCHAR (50)  NULL,
    [Email]          NVARCHAR (128) NULL,
    [ContactPerson]  NVARCHAR (200) NULL,
    [IsOpen]         BIT            NOT NULL,
    [IsHeadOffice]   BIT            NOT NULL,
    [BillingAccount] NVARCHAR (100) NULL,
    CONSTRAINT [PK_c_au_department] PRIMARY KEY CLUSTERED ([DepartmentID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_c_au_department_au_Subscribers] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[au_Subscribers] ([CreditInfoID])
);

