﻿CREATE TABLE [dbo].[FSI_Operators] (
    [ID]                 INT          IDENTITY (1, 1) NOT NULL,
    [OperatorNameNative] VARCHAR (50) NULL,
    [OperatorNameEN]     VARCHAR (50) NULL,
    [Symbol]             VARCHAR (10) NOT NULL
);

