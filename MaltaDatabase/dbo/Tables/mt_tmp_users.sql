﻿CREATE TABLE [dbo].[mt_tmp_users] (
    [id]                INT            NOT NULL,
    [username]          VARCHAR (40)   NULL,
    [name]              VARCHAR (40)   NULL,
    [ssno]              VARCHAR (10)   NULL,
    [subscriber]        VARCHAR (20)   NULL,
    [comment]           VARCHAR (2048) NULL,
    [email]             VARCHAR (100)  NULL,
    [phone]             VARCHAR (15)   NULL,
    [open]              INT            NULL,
    [passdword]         VARCHAR (30)   NULL,
    [password]          VARCHAR (30)   NULL,
    [access_level]      INT            NULL,
    [seller]            VARCHAR (30)   NULL,
    [watch_max]         INT            NULL,
    [allow_webservices] INT            NULL,
    [basic_taf]         INT            NULL,
    [sub_ciid]          INT            NULL,
    [usr_ciid]          INT            NULL,
    [PasswordHash]      VARCHAR (40)   NULL,
    [salt]              VARCHAR (10)   NULL
);

