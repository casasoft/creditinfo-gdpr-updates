﻿CREATE TABLE [dbo].[lmt_individuals2] (
    [id]           INT           NULL,
    [location]     VARCHAR (100) NULL,
    [street]       VARCHAR (100) NULL,
    [name]         VARCHAR (100) NULL,
    [english_name] VARCHAR (100) NULL,
    [street_floor] VARCHAR (100) NULL,
    [ssno]         CHAR (10)     NULL,
    [last_upd]     VARCHAR (19)  NULL
);

