﻿CREATE TABLE [dbo].[lmt_companies_load] (
    [companyid]               VARCHAR (18)   NULL,
    [companyname]             NVARCHAR (255) NULL,
    [registrationdate]        DATETIME       NULL,
    [companyaddress1]         NVARCHAR (255) NULL,
    [companyaddress2]         NVARCHAR (255) NULL,
    [companylocality]         NVARCHAR (50)  NULL,
    [companypostcode]         VARCHAR (20)   NULL,
    [companycountryid]        VARCHAR (5)    NULL,
    [companystatusid]         INT            NULL,
    [denominationid]          VARCHAR (20)   NULL,
    [totalauthshares]         NVARCHAR (255) NULL,
    [totalauthsharecapital]   BIGINT         NULL,
    [totalissuedshares]       NVARCHAR (255) NULL,
    [totalissuedsharecapital] BIGINT         NULL,
    [companyclassificationid] INT            NULL,
    [accrefdate]              DATETIME       NULL
);

