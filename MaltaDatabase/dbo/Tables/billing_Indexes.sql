﻿CREATE TABLE [dbo].[billing_Indexes] (
    [IndexName]         NVARCHAR (20)  NOT NULL,
    [IsActive]          CHAR (10)      COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative] NVARCHAR (255) NULL,
    [DescriptionEN]     NVARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Inserted]          DATETIME       NULL,
    [InsertedBy]        INT            NULL,
    [Updated]           DATETIME       NULL,
    [UpdatedBy]         INT            NULL,
    CONSTRAINT [PK_billing_Indexes] PRIMARY KEY CLUSTERED ([IndexName] ASC) WITH (FILLFACTOR = 90)
);

