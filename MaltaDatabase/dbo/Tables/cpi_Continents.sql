﻿CREATE TABLE [dbo].[cpi_Continents] (
    [ContinentID] INT            IDENTITY (1, 1) NOT NULL,
    [NameNative]  NVARCHAR (100) NULL,
    [NameEN]      VARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [cpi_Continents_PK] PRIMARY KEY CLUSTERED ([ContinentID] ASC) WITH (FILLFACTOR = 90)
);

