﻿CREATE TABLE [dbo].[c_vc_revisions] (
    [AppMajor] CHAR (2) CONSTRAINT [DF__c_vc_revi__App_M__52B92F6B] DEFAULT ('00') NOT NULL,
    [AppMinor] CHAR (2) CONSTRAINT [DF__c_vc_revi__App_M__53AD53A4] DEFAULT ('00') NOT NULL,
    [AppPatch] CHAR (3) CONSTRAINT [DF__c_vc_revi__App_P__54A177DD] DEFAULT ('000') NOT NULL,
    [DBPatch]  CHAR (3) CONSTRAINT [DF__c_vc_revi__DB_Pa__55959C16] DEFAULT ('000') NOT NULL,
    [Inserted] DATETIME CONSTRAINT [DF__c_vc_revi__Inser__5689C04F] DEFAULT (getdate()) NOT NULL
);

