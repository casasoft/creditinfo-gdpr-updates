﻿CREATE TABLE [dbo].[np_Address] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [StreetNative]     NVARCHAR (150) NULL,
    [StreetEN]         NVARCHAR (150) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [StreetNumber]     INT            NULL,
    [CityID]           INT            NULL,
    [PostalCode]       VARCHAR (20)   NULL,
    [PostBox]          VARCHAR (20)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [OtherInfoNative]  NVARCHAR (50)  NULL,
    [OtherInfoEN]      NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [CreditInfoID]     INT            NOT NULL,
    [CountryID]        INT            NULL,
    [IsTradingAddress] VARCHAR (10)   CONSTRAINT [DF_np_Address_IsTradingAddress] DEFAULT ('False') NULL,
    CONSTRAINT [PK_np_Address] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_Address_cpi_Countries] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[cpi_Countries] ([CountryID]),
    CONSTRAINT [FK_np_Address_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID]),
    CONSTRAINT [np_City_np_Address_FK1] FOREIGN KEY ([CityID]) REFERENCES [dbo].[np_City] ([CityID])
);


GO
ALTER TABLE [dbo].[np_Address] NOCHECK CONSTRAINT [FK_np_Address_cpi_Countries];


GO
ALTER TABLE [dbo].[np_Address] NOCHECK CONSTRAINT [FK_np_Address_np_CreditInfoUser];


GO
ALTER TABLE [dbo].[np_Address] NOCHECK CONSTRAINT [np_City_np_Address_FK1];


GO
CREATE NONCLUSTERED INDEX [IX_np_Address]
    ON [dbo].[np_Address]([ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_np_Address_CreditinfoID]
    ON [dbo].[np_Address]([CreditInfoID] ASC) WITH (FILLFACTOR = 90);

