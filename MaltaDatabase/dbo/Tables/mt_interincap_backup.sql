﻿CREATE TABLE [dbo].[mt_interincap_backup] (
    [id]             INT            IDENTITY (1, 1) NOT NULL,
    [decree_nr]      NVARCHAR (50)  NULL,
    [inf_source]     NVARCHAR (50)  NULL,
    [ident]          NVARCHAR (50)  NULL,
    [surname]        NVARCHAR (50)  NULL,
    [son_daughterof] NVARCHAR (100) NULL,
    [birthplace]     NVARCHAR (50)  NULL,
    [type]           NVARCHAR (50)  NULL,
    [comments]       NVARCHAR (255) NULL
);

