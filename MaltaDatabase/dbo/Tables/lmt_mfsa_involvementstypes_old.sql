﻿CREATE TABLE [dbo].[lmt_mfsa_involvementstypes_old] (
    [Involv_Type_ID] INT           NOT NULL,
    [Involvement]    VARCHAR (100) NULL,
    [Ordering]       INT           NULL
);


GO
CREATE CLUSTERED INDEX [ix_cdinvolvementtypes_involvtypeid]
    ON [dbo].[lmt_mfsa_involvementstypes_old]([Involv_Type_ID] ASC) WITH (FILLFACTOR = 90);

