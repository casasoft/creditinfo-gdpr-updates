﻿CREATE TABLE [dbo].[billing_Currency] (
    [CurrencyCode]              NVARCHAR (3)  COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [CurrencyDescriptionEN]     NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [CurrencyDescriptionNative] NVARCHAR (50) NOT NULL,
    [Inserted]                  DATETIME      CONSTRAINT [DF_billing_Currency_Inserted] DEFAULT (getdate()) NULL,
    [InsertedBy]                INT           NULL,
    [Updated]                   DATETIME      CONSTRAINT [DF_billing_Currency_Updated] DEFAULT (null) NULL,
    [UpdatedBy]                 INT           CONSTRAINT [DF_billing_Currency_UpdatedBy] DEFAULT (null) NULL,
    CONSTRAINT [PK_billing_Currency] PRIMARY KEY CLUSTERED ([CurrencyCode] ASC) WITH (FILLFACTOR = 90)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'3 letter currency code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'billing_Currency', @level2type = N'COLUMN', @level2name = N'CurrencyCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'General currency name or description (English)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'billing_Currency', @level2type = N'COLUMN', @level2name = N'CurrencyDescriptionEN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'General currency name or description (Native)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'billing_Currency', @level2type = N'COLUMN', @level2name = N'CurrencyDescriptionNative';

