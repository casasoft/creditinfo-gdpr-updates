﻿CREATE TABLE [dbo].[cpi_RealEstatesOwnerType] (
    [OwnerTypeID]     INT           IDENTITY (1, 1) NOT NULL,
    [OwnerTypeNative] NVARCHAR (80) NULL,
    [OwnerTypeEN]     VARCHAR (80)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [cpi_RealEstatesOwnerType_PK] PRIMARY KEY CLUSTERED ([OwnerTypeID] ASC) WITH (FILLFACTOR = 90)
);

