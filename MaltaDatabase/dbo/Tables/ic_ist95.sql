﻿CREATE TABLE [dbo].[ic_ist95] (
    [nr]             VARCHAR (5)   NOT NULL,
    [skyring]        VARCHAR (255) NOT NULL,
    [skamstofun]     VARCHAR (255) NULL,
    [nanari_skyring] VARCHAR (255) NULL,
    [nace]           VARCHAR (5)   NULL,
    [definition]     VARCHAR (255) NULL
);

