﻿CREATE TABLE [dbo].[ic_vidskiptakjor] (
    [terms_id]         INT           NULL,
    [terms_en]         VARCHAR (255) NULL,
    [terms]            VARCHAR (255) NULL,
    [terms_dk]         VARCHAR (255) NULL,
    [is_sales_terms]   VARCHAR (1)   NULL,
    [is_payment_terms] VARCHAR (1)   NULL
);

