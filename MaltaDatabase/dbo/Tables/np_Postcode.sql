﻿CREATE TABLE [dbo].[np_Postcode] (
    [RecordID]      NVARCHAR (1)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [AddressType]   NVARCHAR (5)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [CityNative]    NVARCHAR (100) NULL,
    [CityEN]        NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [AddressNative] NVARCHAR (100) NULL,
    [addressEN]     NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [square_avenue] NVARCHAR (5)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [NumberFrom]    NVARCHAR (10)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [NumberTo]      NVARCHAR (10)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [RangeCode]     NVARCHAR (5)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Postcode]      INT            NULL,
    [SerialNumber]  BIGINT         NULL
);

