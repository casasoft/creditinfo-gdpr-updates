﻿CREATE TABLE [dbo].[lmt_mfsa_Involvements_load] (
    [Involvement_ID]    INT NULL,
    [Company_ID]        INT NULL,
    [Involved_Party_ID] INT NOT NULL,
    [Involv_Type_ID]    INT NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_mfsa_Involvements_load]
    ON [dbo].[lmt_mfsa_Involvements_load]([Involvement_ID] ASC) WITH (FILLFACTOR = 90);

