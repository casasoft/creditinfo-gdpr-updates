﻿CREATE TABLE [dbo].[np_Usage] (
    [id]           INT           IDENTITY (1, 1) NOT NULL,
    [Creditinfoid] INT           NOT NULL,
    [query_type]   INT           NOT NULL,
    [query]        NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [result_count] INT           NULL,
    [ip]           VARCHAR (15)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [created]      DATETIME      CONSTRAINT [DF_np_Usage_created] DEFAULT (getdate()) NULL,
    [UserID]       INT           NOT NULL,
    [IsBillable]   BIT           CONSTRAINT [DF_np_Usage_IsBillable] DEFAULT (1) NOT NULL,
    CONSTRAINT [PK_np_Usage] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_Usage_np_UsageTypes] FOREIGN KEY ([query_type]) REFERENCES [dbo].[np_UsageTypes] ([id])
);


GO
CREATE NONCLUSTERED INDEX [ix_npusage_created]
    ON [dbo].[np_Usage]([created] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_npusage_query]
    ON [dbo].[np_Usage]([query] ASC, [query_type] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_np_Usage_userID]
    ON [dbo].[np_Usage]([UserID] ASC) WITH (FILLFACTOR = 90);


GO


 CREATE TRIGGER [dbo].[trCheckForFreeTime] ON [dbo].[np_Usage] 
INSTEAD OF INSERT
AS
	
	INSERT INTO np_Usage (Creditinfoid,query_type,query,result_count,ip,created,UserID,IsBillable)
	SELECT	usr.Creditinfoid,query_type,query,result_count,ip,created,UserID,
		(CASE (	SELECT COUNT(*) 
			FROM np_Usage cnt 
			WHERE	cnt.IsBillable=1 and 
				ins.userid=cnt.userid and 
				ins.query_type=cnt.query_type and
				ins.query=cnt.query and
				cnt.created between DATEADD(ss,uType.FreeTime*-1,ins.Created) and DATEADD(ms,-2,ins.Created))
			WHEN 0 THEN 
			case when  usr.UserType = 'Regular User' then uType.IsBillable else 0 END ELSE 0 END)
	FROM	inserted ins
		JOIN np_UsageTypes uType on ins.query_type=uType.id
		JOIN au_users usr on ins.UserID = usr.id
			;




