﻿CREATE TABLE [dbo].[skstk_AccessLevels] (
    [AccessLevel]       INT            NOT NULL,
    [DescriptionNative] NVARCHAR (100) NULL,
    [DescriptionEN]     NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL
);

