﻿CREATE TABLE [dbo].[ros_ContactPerson] (
    [CreditInfoID]    INT            NOT NULL,
    [FirstNameEN]     NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [SurNameEN]       NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [FirstNameNative] NVARCHAR (50)  NULL,
    [SurNameNative]   NVARCHAR (50)  NULL,
    [Email]           NVARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [ContactPersonID] INT            IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_ros_ContactPerson] PRIMARY KEY CLUSTERED ([ContactPersonID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ros_ContactPerson_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);

