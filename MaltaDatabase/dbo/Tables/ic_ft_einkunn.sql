﻿CREATE TABLE [dbo].[ic_ft_einkunn] (
    [ft_kennitala] CHAR (10)     NOT NULL,
    [dags]         SMALLDATETIME NULL,
    [mat]          INT           NULL,
    [astand]       INT           NULL,
    [throun]       INT           NULL,
    [ahaetta]      INT           NULL
);

