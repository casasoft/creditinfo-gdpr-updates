﻿CREATE TABLE [dbo].[ic_bankar] (
    [bankar_numer]    VARCHAR (6)   NULL,
    [bankar_heiti]    VARCHAR (255) NULL,
    [bankar_heiti_en] VARCHAR (255) NULL,
    [bankar_heiti_dk] VARCHAR (255) NULL,
    [bankar_adsetur]  VARCHAR (255) NULL,
    [postnumer]       CHAR (3)      NULL,
    [bankar_simi]     CHAR (20)     NULL,
    [bankar_skyring]  TEXT          NULL
);

