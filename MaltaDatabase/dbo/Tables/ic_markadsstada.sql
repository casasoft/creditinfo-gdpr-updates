﻿CREATE TABLE [dbo].[ic_markadsstada] (
    [ft_kennitala]          CHAR (10)     NOT NULL,
    [markadsstada_dags]     SMALLDATETIME NOT NULL,
    [markadsstada_texti]    TEXT          NULL,
    [markadsstada_texti_en] TEXT          NULL,
    [markadsstada_texti_dk] TEXT          NULL
);

