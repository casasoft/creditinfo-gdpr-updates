﻿CREATE TABLE [dbo].[au_Subscribers] (
    [ID]                 INT           IDENTITY (1, 1) NOT NULL,
    [CreditInfoID]       INT           NOT NULL,
    [RegisteredBy]       INT           NULL,
    [SubscriberNickName] NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [NationalID]         INT           NULL,
    [MaxUsers]           INT           NULL,
    [IsOpen]             CHAR (10)     COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [Updated]            DATETIME      NOT NULL,
    [Created]            DATETIME      CONSTRAINT [DF_au_Subscribers_Created] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_au_Subscribers] PRIMARY KEY CLUSTERED ([CreditInfoID] ASC) WITH (FILLFACTOR = 90)
);

