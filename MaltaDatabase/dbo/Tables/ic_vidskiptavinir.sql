﻿CREATE TABLE [dbo].[ic_vidskiptavinir] (
    [id]          INT           NOT NULL,
    [nafn]        VARCHAR (50)  NULL,
    [heimili]     VARCHAR (100) NULL,
    [borg]        VARCHAR (50)  NULL,
    [pnr]         VARCHAR (10)  NULL,
    [land]        VARCHAR (3)   NULL,
    [postbox]     VARCHAR (10)  NULL,
    [simi]        VARCHAR (15)  NULL,
    [fax]         VARCHAR (25)  NULL,
    [email]       VARCHAR (50)  NULL,
    [lt_username] VARCHAR (40)  NULL
);

