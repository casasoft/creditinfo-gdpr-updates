﻿CREATE TABLE [dbo].[lmt_involvedpartytocompanylink_load] (
    [involvementid]   INT          NOT NULL,
    [companyid]       VARCHAR (50) NULL,
    [involvedpartyid] INT          NOT NULL,
    [involvedtypeid]  INT          NULL,
    CONSTRAINT [PK_lmt_involvedpartytocompanylink_load] PRIMARY KEY CLUSTERED ([involvementid] ASC) WITH (FILLFACTOR = 90)
);

