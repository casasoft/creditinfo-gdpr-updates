﻿CREATE TABLE [dbo].[cw_sent] (
    [creditinfoid] INT           NULL,
    [email]        VARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [watch]        CHAR (1)      NULL,
    [number]       INT           NULL,
    [type]         INT           NULL,
    [created]      DATETIME      CONSTRAINT [DF__cw_sent__created__490FC9A0] DEFAULT (getdate()) NULL
);

