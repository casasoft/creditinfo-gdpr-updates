﻿CREATE TABLE [dbo].[ic_lond] (
    [countrie_id]        INT           NOT NULL,
    [countries_en]       VARCHAR (255) NULL,
    [countries_ice]      VARCHAR (255) NULL,
    [countries_dk]       VARCHAR (255) NULL,
    [is_export_countrie] VARCHAR (1)   NULL,
    [is_import_countrie] VARCHAR (1)   NULL
);

