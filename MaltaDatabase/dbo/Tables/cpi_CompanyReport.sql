﻿CREATE TABLE [dbo].[cpi_CompanyReport] (
    [CompanyCIID]             INT             NOT NULL,
    [CompanyFormerNameNative] NVARCHAR (255)  NULL,
    [CompanyFormerNameEN]     VARCHAR (255)   COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Compa__Compa__45BE5BA9] DEFAULT ('0') NULL,
    [CompanyTradeNameNative]  NVARCHAR (255)  NULL,
    [CompanyTradeNameEN]      VARCHAR (255)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [grheinkunn]              VARCHAR (100)   COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Compa__grhei__46B27FE2] DEFAULT ('0') NULL,
    [duns]                    VARCHAR (100)   COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Compan__duns__47A6A41B] DEFAULT ('0') NULL,
    [ExpireDate]              DATETIME        NULL,
    [gildistimi_iv]           DATETIME        NULL,
    [RegistrationInfoNative]  NVARCHAR (2048) CONSTRAINT [DF__cpi_Compa__Regis__4A8310C6] DEFAULT (N'1') NULL,
    [RegistrationInfoEN]      VARCHAR (2048)  COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Compa__Regis__4B7734FF] DEFAULT ('1') NULL,
    [InternalComment]         VARCHAR (2048)  CONSTRAINT [DF__cpi_Compa__Comme__4C6B5938] DEFAULT ('1') NULL,
    [ExternalCommentNative]   VARCHAR (2048)  NULL,
    [IcrStatus]               INT             CONSTRAINT [DF__cpi_Compa__IcrSt__4D5F7D71] DEFAULT (0) NULL,
    [iv_status]               INT             CONSTRAINT [DF__cpi_Compa__iv_st__4E53A1AA] DEFAULT (0) NULL,
    [Vat]                     NVARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Company__Vat__4F47C5E3] DEFAULT ('0') NULL,
    [CustomerCount]           INT             CONSTRAINT [DF__cpi_Compa__Custo__503BEA1C] DEFAULT (0) NULL,
    [ReportsAuthorCIID]       INT             CONSTRAINT [DF__cpi_Compa__Repor__51300E55] DEFAULT (0) NOT NULL,
    [CustomerComments]        VARCHAR (255)   CONSTRAINT [DF__cpi_Compa__Custo__5224328E] DEFAULT ('0') NULL,
    [ImportComments]          VARCHAR (255)   CONSTRAINT [DF__cpi_Compa__Impor__531856C7] DEFAULT ('0') NULL,
    [ExportComments]          VARCHAR (255)   CONSTRAINT [DF__cpi_Compa__Expor__540C7B00] DEFAULT ('0') NULL,
    [PaymentComments]         VARCHAR (255)   CONSTRAINT [DF__cpi_Compa__Payme__55009F39] DEFAULT ('0') NULL,
    [SalesComments]           VARCHAR (255)   CONSTRAINT [DF__cpi_Compa__Sales__55F4C372] DEFAULT ('0') NULL,
    [TermsID]                 INT             NULL,
    [Year]                    VARCHAR (10)    NULL,
    [StatusID]                INT             CONSTRAINT [DF_cpi_CompanyReport_StatusID] DEFAULT (2) NULL,
    [RegistrationFormID]      INT             NULL,
    [ReportID]                INT             IDENTITY (1, 1) NOT NULL,
    [InternalCommentEN]       VARCHAR (2048)  NULL,
    [Symbol]                  VARCHAR (10)    NULL,
    [ExternalCommentEN]       VARCHAR (2048)  NULL,
    [LAR]                     DATETIME        NULL,
    [LR]                      DATETIME        NULL,
    [StructureReportDate]     DATETIME        NULL,
    [CompanyRegistedDate]     DATETIME        NULL,
    CONSTRAINT [ct_ic_fyrirtaeki1] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_ReportStatus_cpi_CompanyReport_FK1] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[cpi_ReportStatus] ([StatusID]),
    CONSTRAINT [FK_cpi_CompanyReport_cpi_RegistrationForm] FOREIGN KEY ([RegistrationFormID]) REFERENCES [dbo].[cpi_RegistrationForm] ([FormID]),
    CONSTRAINT [FK_cpi_CompanyReport_np_Companys] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[np_Companys] ([CreditInfoID]),
    CONSTRAINT [FK_cpi_CompanyReport_np_CreditInfoUser] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);


GO
CREATE NONCLUSTERED INDEX [ix435_21]
    ON [dbo].[cpi_CompanyReport]([ExpireDate] ASC) WITH (FILLFACTOR = 90);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cyprus specific date', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'cpi_CompanyReport', @level2type = N'COLUMN', @level2name = N'CompanyRegistedDate';

