﻿CREATE TABLE [dbo].[FSI_TemplateGeneralInfo] (
    [AFS_id]                   INT          IDENTITY (1, 1) NOT NULL,
    [TemplateID]               INT          NOT NULL,
    [CreditInfoID]             INT          NOT NULL,
    [Financial_year]           VARCHAR (15) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [Financial_year_end]       DATETIME     NULL,
    [Account_period_length]    INT          NOT NULL,
    [Currency]                 VARCHAR (15) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [Consolidated]             VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Qualified_audit]          VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Reviewed]                 VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Denomination_id]          INT          NOT NULL,
    [Employee_count]           INT          NULL,
    [Comments_id]              INT          NULL,
    [Date_of_delivery]         DATETIME     NULL,
    [Ready_for_web_publishing] VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Origin_id]                INT          NULL,
    [Registrator_id]           INT          NULL,
    [Created]                  DATETIME     NULL,
    [Modified]                 DATETIME     NULL,
    [Deleted]                  VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF_FSI_TemplateGeneralInfo_Deleted] DEFAULT ('False') NOT NULL,
    [Smallstatus]              VARCHAR (5)  NULL,
    [CreatedBy]                INT          NULL,
    [ModifiedBy]               INT          NULL,
    CONSTRAINT [PK_FSI_TemplateGeneralInfo] PRIMARY KEY NONCLUSTERED ([AFS_id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FSI_TemplateGeneralInfo_FSI_Comments] FOREIGN KEY ([Comments_id]) REFERENCES [dbo].[FSI_Comments] ([Comments_id]),
    CONSTRAINT [FK_FSI_TemplateGeneralInfo_FSI_Denomination] FOREIGN KEY ([Denomination_id]) REFERENCES [dbo].[FSI_Denomination] ([Denomination_id]),
    CONSTRAINT [FK_FSI_TemplateGeneralInfo_FSI_Origin] FOREIGN KEY ([Origin_id]) REFERENCES [dbo].[FSI_Origin] ([Origin_id]),
    CONSTRAINT [FK_FSI_TemplateGeneralInfo_FSI_Templates] FOREIGN KEY ([TemplateID]) REFERENCES [dbo].[FSI_Templates] ([ID]),
    CONSTRAINT [FK_FSI_TemplateGeneralInfo_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);


GO
CREATE CLUSTERED INDEX [IX_FSI_TemplateGeneralInfo]
    ON [dbo].[FSI_TemplateGeneralInfo]([AFS_id] ASC) WITH (FILLFACTOR = 90);

