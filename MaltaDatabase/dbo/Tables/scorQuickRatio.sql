﻿CREATE TABLE [dbo].[scorQuickRatio] (
    [id]       INT             IDENTITY (1, 1) NOT NULL,
    [QRBound]  DECIMAL (18, 2) CONSTRAINT [DF_scorQuickRatio_QRBound] DEFAULT ((0)) NOT NULL,
    [DDDBound] DECIMAL (18, 2) CONSTRAINT [DF_scorQuickRatio_DDDBound] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_scorQuickRatio] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 70)
);

