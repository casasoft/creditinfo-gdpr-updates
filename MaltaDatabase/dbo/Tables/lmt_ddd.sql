﻿CREATE TABLE [dbo].[lmt_ddd] (
    [id]              INT           NOT NULL,
    [caseno]          VARCHAR (30)  NULL,
    [court]           VARCHAR (100) NULL,
    [plaintiff]       VARCHAR (100) NULL,
    [idregno]         VARCHAR (20)  NULL,
    [issuedate]       SMALLDATETIME NULL,
    [case_type]       VARCHAR (50)  NULL,
    [amount]          FLOAT (53)    NULL,
    [case_id]         INT           NULL,
    [myndun]          DATETIME      NULL,
    [type2]           VARCHAR (20)  NULL,
    [open]            INT           NULL,
    [reg_date]        SMALLDATETIME NULL,
    [type_2]          VARCHAR (10)  NULL,
    [cig_plaintiffid] INT           NULL
);

