﻿CREATE TABLE [dbo].[mt_subscriptions_strengur] (
    [id]       INT          NOT NULL,
    [username] VARCHAR (40) NULL,
    [ssno]     VARCHAR (20) NULL,
    [service]  VARCHAR (40) NULL,
    [page_id]  INT          NULL
);

