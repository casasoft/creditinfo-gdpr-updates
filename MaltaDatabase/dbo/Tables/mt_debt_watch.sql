﻿CREATE TABLE [dbo].[mt_debt_watch] (
    [id]           INT           NOT NULL,
    [subject]      VARCHAR (10)  NULL,
    [username]     VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [user_id]      INT           NULL,
    [created]      DATETIME      NULL,
    [sent]         CHAR (1)      NULL,
    [sent_date]    DATETIME      NULL,
    [type_sent]    CHAR (1)      NULL,
    [cig_username] VARCHAR (100) NULL
);


GO
CREATE NONCLUSTERED INDEX [mt_debt_watch]
    ON [dbo].[mt_debt_watch]([username] ASC) WITH (FILLFACTOR = 90);

