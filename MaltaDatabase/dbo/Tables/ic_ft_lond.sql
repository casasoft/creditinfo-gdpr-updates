﻿CREATE TABLE [dbo].[ic_ft_lond] (
    [ft_kennitala]     CHAR (10)       NOT NULL,
    [lond_id]          INT             NOT NULL,
    [innf_utf_id]      INT             NULL,
    [ft_lond_hlutfall] DECIMAL (16, 2) NULL
);

