﻿CREATE TABLE [dbo].[cpi_Education] (
    [EducationID]     INT            CONSTRAINT [DF__cpi_Educa__Educa__7B264821] DEFAULT (0) NOT NULL,
    [EducationNative] NVARCHAR (255) CONSTRAINT [DF__cpi_Educa__Educa__7C1A6C5A] DEFAULT (N'0') NULL,
    [EducationEN]     VARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Educa__Educa__7D0E9093] DEFAULT ('0') NULL,
    CONSTRAINT [PK_cpi_Education] PRIMARY KEY CLUSTERED ([EducationID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix433_1]
    ON [dbo].[cpi_Education]([EducationID] ASC) WITH (FILLFACTOR = 90);

