﻿CREATE TABLE [dbo].[np_UsageTypes] (
    [id]                INT            NOT NULL,
    [typeNative]        NVARCHAR (50)  NULL,
    [typeEN]            NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Orderable]         VARCHAR (5)    CONSTRAINT [DF_np_UsageTypes_Orderable] DEFAULT ('False') NOT NULL,
    [DescriptionEn]     NVARCHAR (512) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative] NVARCHAR (512) NULL,
    [IsBillable]        BIT            CONSTRAINT [DF_np_UsageTypes_IsBillable] DEFAULT (1) NOT NULL,
    [Freetime]          INT            CONSTRAINT [DF_np_UsageTypes_Freetime] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_np_UsageTypes] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 90)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'value is in seconds', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'np_UsageTypes', @level2type = N'COLUMN', @level2name = N'Freetime';

