﻿CREATE TABLE [dbo].[np_Notes] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [CreditInfoID] INT           NULL,
    [Note]         NVARCHAR (50) NULL,
    CONSTRAINT [np_Notes_PK] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_np_Notes_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);

