﻿CREATE TABLE [dbo].[lmt_involvedpartytocompanylink] (
    [involvementid]   INT          NOT NULL,
    [companyid]       VARCHAR (50) NULL,
    [involvedpartyid] INT          NOT NULL,
    [involvedtypeid]  INT          NULL,
    CONSTRAINT [PK_lmt_involvedpartytocompanylink] PRIMARY KEY CLUSTERED ([involvementid] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [ix_cdinvolvements_involvementid]
    ON [dbo].[lmt_involvedpartytocompanylink]([involvementid] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [ix_cdinvolvements_companyid]
    ON [dbo].[lmt_involvedpartytocompanylink]([companyid] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [ix_cdinvolvements_involvtypeid]
    ON [dbo].[lmt_involvedpartytocompanylink]([involvedtypeid] ASC) WITH (FILLFACTOR = 100);

