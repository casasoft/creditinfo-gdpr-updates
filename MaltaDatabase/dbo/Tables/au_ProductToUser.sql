﻿CREATE TABLE [dbo].[au_ProductToUser] (
    [UserID]    INT      NOT NULL,
    [ProductID] INT      NOT NULL,
    [Created]   DATETIME CONSTRAINT [DF_au_ProductToUser_Created] DEFAULT (getdate()) NULL
);

