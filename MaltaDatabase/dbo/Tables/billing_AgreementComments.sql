﻿CREATE TABLE [dbo].[billing_AgreementComments] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [AgreementID]        INT             NOT NULL,
    [Comment]            NVARCHAR (1024) NULL,
    [ReminderDate]       DATETIME        NULL,
    [NotificationEmails] NVARCHAR (128)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Inserted]           DATETIME        NULL,
    [InsertedBy]         INT             NULL,
    [Updated]            DATETIME        NULL,
    [UpdatedBy]          INT             NULL,
    [SendStatus]         INT             CONSTRAINT [DF_billing_AgreementComments_SendStatus] DEFAULT (0) NULL,
    CONSTRAINT [PK_billing_AgreementComments] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_billing_AgreementComments_billing_Agreement] FOREIGN KEY ([AgreementID]) REFERENCES [dbo].[billing_Agreement] ([AgreementID])
);

