﻿CREATE TABLE [dbo].[FSI_Shareholders] (
    [AFS_id]                         INT            NOT NULL,
    [Shareholders_number]            INT            IDENTITY (1, 1) NOT NULL,
    [Name]                           NVARCHAR (100) NULL,
    [Company_or_private_person]      INT            NULL,
    [National_identification_number] VARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Percentage_hold]                VARCHAR (5)    COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [ISO_country_code]               VARCHAR (5)    COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_FSI_Shareholders] PRIMARY KEY CLUSTERED ([AFS_id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FSI_Shareholders_FSI_GeneralInfo] FOREIGN KEY ([AFS_id]) REFERENCES [dbo].[FSI_GeneralInfo] ([AFS_id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1 == Company, 2 == person', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FSI_Shareholders', @level2type = N'COLUMN', @level2name = N'Company_or_private_person';

