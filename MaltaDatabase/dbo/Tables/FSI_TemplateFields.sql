﻿CREATE TABLE [dbo].[FSI_TemplateFields] (
    [ID]         INT IDENTITY (1, 1) NOT NULL,
    [TemplateID] INT NOT NULL,
    [FieldID]    INT NOT NULL,
    [Order]      INT NOT NULL
);

