﻿CREATE TABLE [dbo].[np_Word2CIID] (
    [type]         CHAR (1)       NOT NULL,
    [creditinfoid] INT            NOT NULL,
    [word]         NVARCHAR (150) NULL
);


GO
CREATE CLUSTERED INDEX [ix_npword2ciid_ciidtype]
    ON [dbo].[np_Word2CIID]([creditinfoid] ASC, [type] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_npword2ciid_word]
    ON [dbo].[np_Word2CIID]([word] ASC) WITH (FILLFACTOR = 90);

