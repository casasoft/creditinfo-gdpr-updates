﻿CREATE TABLE [dbo].[cpi_HistoryOperationReview] (
    [CompanyCIID]                INT   NOT NULL,
    [HistoryNative]              NTEXT NULL,
    [HistoryEN]                  TEXT  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [OperationNative]            NTEXT NULL,
    [OperationEN]                TEXT  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [CompanyReviewNative]        NTEXT NULL,
    [CompanyReviewEN]            TEXT  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [BusinessRegistryTextNative] NTEXT NULL,
    [BusinessRegistryTextEN]     TEXT  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [StatusDescriptionNative]    NTEXT NULL,
    [StatusDescriptionEN]        TEXT  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [cpi_HistoryOperationReview_PK] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_HistoryOperationReview_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID])
);

