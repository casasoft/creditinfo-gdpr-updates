﻿CREATE TABLE [dbo].[np_Individual] (
    [FirstNameNative] NVARCHAR (80) NULL,
    [FirstNameEN]     NVARCHAR (80) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [SurNameNative]   NVARCHAR (80) CONSTRAINT [DF_np_Individual_SurNameNative] DEFAULT ('') NOT NULL,
    [SurNameEN]       NVARCHAR (80) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [ProfessionID]    INT           NULL,
    [EducationID]     INT           NULL,
    [CreditInfoID]    INT           NOT NULL,
    [LastUpdate]      DATETIME      NULL,
    [Processed]       BIT           CONSTRAINT [DF_np_Individual_Processed] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_np_Individual] PRIMARY KEY CLUSTERED ([CreditInfoID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_Individual_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID]),
    CONSTRAINT [FK_np_Individual_np_Education] FOREIGN KEY ([EducationID]) REFERENCES [dbo].[np_Education] ([EducationID]),
    CONSTRAINT [np_Profession_np_Indivitual_FK1] FOREIGN KEY ([ProfessionID]) REFERENCES [dbo].[np_Profession] ([ProfessionID])
);


GO


    CREATE trigger [dbo].[tr_individual_upd]
    on [dbo].[np_Individual]
    for update
    as

    -- Context Info changed in procedure ddd_UpdateIndividualFullName, trigger do not execute in this context
    DECLARE @Cinfo VARBINARY(128)
    SELECT @Cinfo = Context_Info()
    IF @Cinfo = 0x55555
    RETURN
    

    declare @i int
    declare @ciid int, @d_nat nvarchar(150), @i_nat nvarchar(150)
    declare @np nvarchar(150)
    declare cur_upd cursor for
    select del.creditinfoid, del.firstnamenative, ins.firstnamenative
    from deleted del join inserted ins on del.creditinfoid=ins.creditinfoid

    open cur_upd
    fetch next from cur_upd into @ciid, @d_nat, @i_nat

    while @@fetch_status=0
    begin


    if @ciid is not null --and (@d_nen <> @i_nen or @d_nna <> @i_nna or not exists (select * from np_word2ciid where creditinfoid=@ciid))
    begin
    delete from np_word2ciid
    where creditinfoid=@ciid and type='i'
    set @np = @i_nat
    insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'i', @np)
    set @i=charindex(' ',@np)
    while @i  > 0
    begin
    if not exists(select * from np_word2ciid where creditinfoid=@ciid and type='i' and word=ltrim(substring(@np, @i+1, len(@np))))
    begin
    insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'i', ltrim(substring(@np, @i+1, len(@np))))
    end
    set @i = charindex(' ',@np, @i+1)
    end
    end
    fetch next from cur_upd into @ciid, @d_nat, @i_nat
    end
    close cur_upd
    deallocate cur_upd

    
GO
create trigger tr_npindividual_del
  on np_individual
  for delete 
  as
    delete from np_word2ciid
      where creditinfoid=(select creditinfoid from deleted)


GO
create trigger tr_npindividual_ins
  on np_individual
  for insert
  as
    declare @i int
    declare @ciid int, @i_nen nvarchar(150), @i_nna nvarchar(150)
    declare @np nvarchar(150)
    select @ciid=creditinfoid,@i_nna=firstnamenative
      from inserted
    
    if @ciid is not null
      begin
	set @np = @i_nna
	insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'i', @np)
	set @i=charindex(' ',@np)
	while @i  > 0
	  begin
	  if 0=(select count(*) from np_word2ciid where creditinfoid=@ciid and type='i' and word=ltrim(substring(@np, @i+1, len(@np))))
	    begin
	    insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'i', ltrim(substring(@np, @i+1, len(@np))))
	    end
          set @i = charindex(' ',@np, @i+1)
          end
       end


