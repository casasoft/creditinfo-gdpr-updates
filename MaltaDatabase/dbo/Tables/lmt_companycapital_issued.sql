﻿CREATE TABLE [dbo].[lmt_companycapital_issued] (
    [captalid]           INT            NOT NULL,
    [companyid]          NVARCHAR (255) NULL,
    [involvedpartyid]    INT            NULL,
    [class]              NVARCHAR (255) NULL,
    [issuedsharecapital] FLOAT (53)     NULL,
    [percentagepaidup]   FLOAT (53)     NULL,
    [NominalValue]       FLOAT (53)     NULL,
    [NumberOfShares]     BIGINT         NULL
);

