﻿CREATE TABLE [dbo].[np_City] (
    [CityID]         INT           IDENTITY (1, 1) NOT NULL,
    [NameNative]     NVARCHAR (50) NULL,
    [NameEN]         NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [LocationNative] NVARCHAR (50) NULL,
    [LocationEN]     NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [CountryID]      INT           NULL,
    CONSTRAINT [np_City_PK] PRIMARY KEY CLUSTERED ([CityID] ASC) WITH (FILLFACTOR = 100)
);

