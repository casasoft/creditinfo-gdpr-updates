﻿CREATE TABLE [dbo].[lmt_companystates_load] (
    [statusid] INT            NOT NULL,
    [state]    NVARCHAR (255) NULL,
    CONSTRAINT [PK__lmt_companystate__795DFB40] PRIMARY KEY CLUSTERED ([statusid] ASC) WITH (FILLFACTOR = 90)
);

