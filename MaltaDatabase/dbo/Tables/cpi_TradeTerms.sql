﻿CREATE TABLE [dbo].[cpi_TradeTerms] (
    [TermsID]                INT            NOT NULL,
    [TermsDescriptionNative] NVARCHAR (100) NULL,
    [TermsDescriptionEN]     VARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [SalesTerm]              VARCHAR (10)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [PaymentTerm]            VARCHAR (10)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_cpi_SPTerms] PRIMARY KEY CLUSTERED ([TermsID] ASC) WITH (FILLFACTOR = 90)
);

