﻿CREATE TABLE [dbo].[scorFSI] (
    [id]              INT            IDENTITY (1, 1) NOT NULL,
    [MinTest]         INT            NOT NULL,
    [MaxTest]         INT            NULL,
    [Value]           NVARCHAR (2)   NOT NULL,
    [RiskDescription] NVARCHAR (255) NULL,
    [Color]           NVARCHAR (10)  NULL,
    [DDDNumber]       INT            NOT NULL,
    CONSTRAINT [PK_scorFSI] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 70)
);

