﻿CREATE TABLE [dbo].[np_Claims] (
    [ID]                            INT             IDENTITY (1, 1) NOT NULL,
    [CreditInfoID]                  INT             NOT NULL,
    [ClaimTypeID]                   INT             NOT NULL,
    [InformationSourceID]           INT             NOT NULL,
    [RegDate]                       DATETIME        CONSTRAINT [DF_np_Claims_RegDate] DEFAULT (getdate()) NOT NULL,
    [StatusID]                      INT             NULL,
    [CaseNr]                        VARCHAR (50)    COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [CurrencyID]                    VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Amount]                        DECIMAL (18, 2) NULL,
    [ClaimDate]                     DATETIME        NULL,
    [AnnouncementDate]              DATETIME        NULL,
    [UnregistedDate]                DATETIME        NULL,
    [DelayDate]                     DATETIME        NULL,
    [PaymentDate]                   DATETIME        NULL,
    [RegisterID]                    INT             NULL,
    [Valid]                         CHAR (10)       CONSTRAINT [DF__np_Claims__Valid__34C8D9D1] DEFAULT ('1') NULL,
    [GazetteYear]                   VARCHAR (30)    COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [GazettePage]                   VARCHAR (10)    COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [AppliedForByDebtor]            NVARCHAR (20)   COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__np_Claims__Appli__35BCFE0A] DEFAULT ('0') NULL,
    [RegisterCommentNative]         NVARCHAR (1024) NULL,
    [RegisterCommentEN]             NVARCHAR (1024) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [LastUpdate]                    DATETIME        NULL,
    [Agent]                         NVARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [ChequeIssuedBank]              INT             NULL,
    [CaseDecisionsType]             VARCHAR (20)    COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [OnHold]                        DATETIME        NULL,
    [CBA]                           NVARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [RegisterInternalCommentNative] NVARCHAR (1024) NULL,
    [RegisterInternalCommentEN]     NVARCHAR (1024) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [PublicDate]                    DATETIME        NULL,
    [ClaimOwnerCIID]                INT             NULL,
    CONSTRAINT [np_claims_PK] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_Claims_np_CaseTypes] FOREIGN KEY ([ClaimTypeID]) REFERENCES [dbo].[np_CaseTypes] ([CaseTypeID]),
    CONSTRAINT [FK_np_Claims_np_ChequeIssueBanks] FOREIGN KEY ([ChequeIssuedBank]) REFERENCES [dbo].[np_ChequeIssueBanks] ([BankID]),
    CONSTRAINT [FK_np_Claims_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID]),
    CONSTRAINT [FK_np_Claims_np_InformationSource] FOREIGN KEY ([InformationSourceID]) REFERENCES [dbo].[np_InformationSource] ([InformationSourceID]),
    CONSTRAINT [FK_np_Claims_np_Status] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[np_Status] ([StatusID])
);


GO
CREATE NONCLUSTERED INDEX [ix_npclaims_creditinfoid]
    ON [dbo].[np_Claims]([CreditInfoID] ASC) WITH (FILLFACTOR = 90);


GO
create trigger tr_npclaims_upd
on dbo.np_Claims
after update
as
    insert into cw_newclaims(claim_id, statusid) --values (1)
    select inserted.id, inserted.statusid
    from inserted join deleted on inserted.id=deleted.id
    where inserted.statusid<>deleted.statusid and 
          (inserted.statusid =4  or (inserted.statusid =6 and deleted.statusid = 4))

GO
create trigger tr_npclaims_ins
on dbo.np_Claims
after insert 
as
    insert into cw_newclaims(claim_id, statusid)
    select inserted.id, inserted.statusid
    from inserted
    where inserted.statusid in (4,6)
