﻿CREATE TABLE [dbo].[ic_eigendur_mt] (
    [owner_id]     VARCHAR (99)  NULL,
    [ft_kennitala] VARCHAR (50)  NULL,
    [ei_kennitala] VARCHAR (100) NULL,
    [ft_nafn]      VARCHAR (255) NULL,
    [nafn]         VARCHAR (201) NULL,
    [eignarhlutur] VARCHAR (99)  NULL,
    [menntun_id]   INT           NULL,
    [kaupdags]     VARCHAR (99)  NULL
);

