﻿CREATE TABLE [dbo].[cpi_ContactPerson] (
    [CreditInfoID]    INT            NOT NULL,
    [FirstNameNative] NVARCHAR (50)  NULL,
    [LastNameNative]  NVARCHAR (50)  NULL,
    [FirstNameEN]     VARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [LastNameEN]      VARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Email]           NVARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Position]        NVARCHAR (255) NULL,
    [MessengerID]     NVARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [PhoneNumber]     NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [MobileNumber]    NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [FaxNumber]       NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DateContacted]   DATETIME       NULL
);

