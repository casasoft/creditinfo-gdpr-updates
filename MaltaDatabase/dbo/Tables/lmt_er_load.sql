﻿CREATE TABLE [dbo].[lmt_er_load] (
    [Name]      NVARCHAR (255) NULL,
    [AKA]       NVARCHAR (255) NULL,
    [Town]      NVARCHAR (255) NULL,
    [Street]    NVARCHAR (255) NULL,
    [House]     NVARCHAR (255) NULL,
    [ID_Number] VARCHAR (20)   NOT NULL,
    PRIMARY KEY CLUSTERED ([ID_Number] ASC) WITH (FILLFACTOR = 90)
);

