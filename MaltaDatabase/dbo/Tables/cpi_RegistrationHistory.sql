﻿CREATE TABLE [dbo].[cpi_RegistrationHistory] (
    [CompanyCIID]               INT      CONSTRAINT [DF__cpi_Regis__Compa__395884C4] DEFAULT (0) NOT NULL,
    [RegistrationHistoryDate]   DATETIME CONSTRAINT [DF__cpi_Regis__Regis__3A4CA8FD] DEFAULT (getdate()) NOT NULL,
    [RegistrationHistoryNative] NTEXT    CONSTRAINT [DF__cpi_Regis__Regis__3B40CD36] DEFAULT (N'0') NULL,
    [RegistrationHistoryEN]     TEXT     COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Regis__Regis__3C34F16F] DEFAULT ('0') NULL,
    CONSTRAINT [PK_cpi_RegistrationHistory] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [ix376_1]
    ON [dbo].[cpi_RegistrationHistory]([CompanyCIID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix376_2]
    ON [dbo].[cpi_RegistrationHistory]([RegistrationHistoryDate] ASC) WITH (FILLFACTOR = 90);

