﻿CREATE TABLE [dbo].[np_RegLog] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [Action]     VARCHAR (30)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [RegisterID] INT           NULL,
    [ClaimID]    INT           NULL,
    [Date]       SMALLDATETIME NULL,
    [Comment]    VARCHAR (50)  NULL,
    [StatusID]   INT           NULL,
    CONSTRAINT [np_RegLog_PK] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

