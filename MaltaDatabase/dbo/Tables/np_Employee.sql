﻿CREATE TABLE [dbo].[np_Employee] (
    [Initials]     NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [CreditInfoID] INT           NOT NULL,
    [LastUpdate]   DATETIME      NOT NULL,
    CONSTRAINT [PK_np_Employee] PRIMARY KEY CLUSTERED ([Initials] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_Employee_np_Individual] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_Individual] ([CreditInfoID])
);

