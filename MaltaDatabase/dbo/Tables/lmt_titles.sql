﻿CREATE TABLE [dbo].[lmt_titles] (
    [titleid] INT            NOT NULL,
    [title]   NVARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    CONSTRAINT [PK_lmt_titles] PRIMARY KEY CLUSTERED ([titleid] ASC) WITH (FILLFACTOR = 100)
);

