﻿CREATE TABLE [dbo].[lmt_denominations] (
    [denominationid] VARCHAR (20)  COLLATE Icelandic_CI_AS NOT NULL,
    [denomination]   NVARCHAR (50) NULL,
    [rate]           MONEY         NULL,
    CONSTRAINT [PK_cd_denominations] PRIMARY KEY CLUSTERED ([denominationid] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [ix_cddenominatons_denominationid]
    ON [dbo].[lmt_denominations]([denominationid] ASC) WITH (FILLFACTOR = 100);

