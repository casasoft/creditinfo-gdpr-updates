﻿CREATE TABLE [dbo].[np_Individual_Fn_Lookup] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (80) NOT NULL,
    [Type] CHAR (1)      NOT NULL,
    CONSTRAINT [PK_np_Individual_Fn_Lookup] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 70)
);

