﻿CREATE TABLE [dbo].[billing_IndexDetails] (
    [IndexName]  NVARCHAR (20)   NOT NULL,
    [IndexDate]  DATETIME        NOT NULL,
    [IndexValue] DECIMAL (11, 5) NULL,
    [Inserted]   DATETIME        CONSTRAINT [DF_billing_IndexDetails_Inserted] DEFAULT (getdate()) NULL,
    [InsertedBy] INT             NULL,
    [Updated]    DATETIME        CONSTRAINT [DF_billing_IndexDetails_Updated] DEFAULT (null) NULL,
    [UpdatedBy]  INT             CONSTRAINT [DF_billing_IndexDetails_UpdatedBy] DEFAULT (null) NULL,
    CONSTRAINT [PK_billing_IndexDetails] PRIMARY KEY CLUSTERED ([IndexName] ASC, [IndexDate] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_billing_IndexDetails_billing_Indexes] FOREIGN KEY ([IndexName]) REFERENCES [dbo].[billing_Indexes] ([IndexName]) ON UPDATE CASCADE
);

