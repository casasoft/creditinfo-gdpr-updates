﻿CREATE TABLE [dbo].[mt_menus_strengur] (
    [id]            INT            NOT NULL,
    [ref_id]        INT            NULL,
    [isl]           VARCHAR (100)  NULL,
    [ens]           VARCHAR (100)  NULL,
    [action]        VARCHAR (255)  NULL,
    [skyring]       VARCHAR (2048) NULL,
    [description]   VARCHAR (2048) NULL,
    [einstakl]      CHAR (1)       NULL,
    [access]        INT            NULL,
    [use_input]     INT            NULL,
    [ind]           INT            NULL,
    [comp]          INT            NULL,
    [hide_in_menus] INT            NULL
);

