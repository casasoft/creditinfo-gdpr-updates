﻿CREATE TABLE [dbo].[lmt_er_idchanges_load] (
    [rid]    INT          IDENTITY (1, 1) NOT NULL,
    [id_new] VARCHAR (40) NULL,
    [id_old] VARCHAR (40) NULL,
    PRIMARY KEY CLUSTERED ([rid] ASC) WITH (FILLFACTOR = 90)
);

