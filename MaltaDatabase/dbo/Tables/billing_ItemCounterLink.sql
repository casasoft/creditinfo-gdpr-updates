﻿CREATE TABLE [dbo].[billing_ItemCounterLink] (
    [ID]       INT             IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (50)   NULL,
    [LinkRule] NVARCHAR (1000) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_billing_ItemCounterLink] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

