﻿CREATE TABLE [dbo].[lmt_mfsa_companies_old] (
    [creditinfoid]       INT            IDENTITY (10001, 1) NOT NULL,
    [Company_ID]         INT            NOT NULL,
    [Company_Name]       NVARCHAR (100) NULL,
    [Registration_No]    VARCHAR (20)   NULL,
    [Registration_Date]  SMALLDATETIME  NULL,
    [Address_1]          NVARCHAR (100) NULL,
    [Address_2]          NVARCHAR (100) NULL,
    [Locality_ID]        INT            NULL,
    [postcode]           VARCHAR (255)  NULL,
    [Country_ID]         INT            NOT NULL,
    [State_ID]           INT            NULL,
    [Capital_Issued_For] FLOAT (53)     NULL,
    [Denomination_ID]    INT            NULL,
    [Total_Auth_Shr_Cap] FLOAT (53)     NULL,
    [Total_Iss_Shr_Cap]  FLOAT (53)     NULL,
    [Classification_ID]  INT            NULL,
    [Acc_Ref_Date]       VARCHAR (20)   NULL,
    CONSTRAINT [ix_cdcompany_comanyid] UNIQUE CLUSTERED ([Company_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [ix_cdcompany_registrationno]
    ON [dbo].[lmt_mfsa_companies_old]([Registration_No] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_cdcompany_companyname]
    ON [dbo].[lmt_mfsa_companies_old]([Company_Name] ASC) WITH (FILLFACTOR = 90);

