﻿CREATE TABLE [dbo].[FSI_RatioFormulas] (
    [ID]              INT          IDENTITY (1, 1) NOT NULL,
    [FormulaID]       INT          NOT NULL,
    [FormulaItemType] VARCHAR (10) NOT NULL,
    [FormulaItemID]   INT          NOT NULL,
    [Order]           INT          NOT NULL
);

