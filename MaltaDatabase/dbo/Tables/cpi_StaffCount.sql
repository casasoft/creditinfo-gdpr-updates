﻿CREATE TABLE [dbo].[cpi_StaffCount] (
    [CompanyCIID] INT          CONSTRAINT [DF__cpi_Staff__Compa__3587F3E0] DEFAULT (0) NOT NULL,
    [Year]        VARCHAR (10) CONSTRAINT [DF__cpi_StaffC__Year__367C1819] DEFAULT ('0') NOT NULL,
    [Count]       INT          CONSTRAINT [DF__cpi_Staff__Count__37703C52] DEFAULT (0) NULL,
    CONSTRAINT [PK_cpi_StaffCount] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC, [Year] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_StaffCount_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID])
);


GO
CREATE NONCLUSTERED INDEX [ix558_1]
    ON [dbo].[cpi_StaffCount]([CompanyCIID] ASC, [Year] ASC) WITH (FILLFACTOR = 90);

