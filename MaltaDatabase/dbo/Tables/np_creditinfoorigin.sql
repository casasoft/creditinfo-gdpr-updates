﻿CREATE TABLE [dbo].[np_creditinfoorigin] (
    [originid]          INT            NOT NULL,
    [descriptionen]     NVARCHAR (100) NULL,
    [descriptionnative] NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([originid] ASC) WITH (FILLFACTOR = 90)
);

