﻿CREATE TABLE [dbo].[ic_adsetur] (
    [ft_kennitala]        CHAR (10)     NOT NULL,
    [postnumer]           INT           NULL,
    [lond_id]             INT           NULL,
    [adsetur_postfang]    VARCHAR (255) NULL,
    [adsetur_postfang_en] VARCHAR (255) NULL,
    [adsetur_postfang_dk] VARCHAR (255) NULL
);

