﻿CREATE TABLE [dbo].[lmt_capitaltypes_load] (
    [capitalid] INT           NOT NULL,
    [capital]   NVARCHAR (25) NULL,
    CONSTRAINT [PK_lmt_capitaltypes_load] PRIMARY KEY CLUSTERED ([capitalid] ASC) WITH (FILLFACTOR = 90)
);

