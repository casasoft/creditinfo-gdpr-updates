﻿CREATE TABLE [dbo].[lmt_mfsa_localities_old] (
    [Locality_ID] INT           NOT NULL,
    [locality]    VARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    [is_local]    INT           NULL
);


GO
CREATE CLUSTERED INDEX [ix_cdlocalities_localityid]
    ON [dbo].[lmt_mfsa_localities_old]([Locality_ID] ASC) WITH (FILLFACTOR = 90);

