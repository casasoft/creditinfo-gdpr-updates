﻿CREATE TABLE [dbo].[cw_sendclaims] (
    [id]            INT            IDENTITY (1, 1) NOT NULL,
    [creditinfoid]  INT            NULL,
    [claim_id]      INT            NULL,
    [email]         VARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [sent]          CHAR (10)      CONSTRAINT [DF__cw_sendcla__sent__2BC97F7C] DEFAULT ('False') NULL,
    [claim_type]    VARCHAR (99)   NULL,
    [watch_type]    INT            NOT NULL,
    [watchid_id]    INT            NULL,
    [watch_subject] NVARCHAR (100) NULL,
    [subject_name]  NVARCHAR (100) NULL,
    [messform]      INT            NULL,
    [statusid]      INT            NULL,
    [created]       DATETIME       CONSTRAINT [DF__cw_sendcl__creat__2CBDA3B5] DEFAULT (getdate()) NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_cwsendclaimsid1]
    ON [dbo].[cw_sendclaims]([creditinfoid] ASC, [email] ASC, [messform] ASC) WITH (FILLFACTOR = 90);

