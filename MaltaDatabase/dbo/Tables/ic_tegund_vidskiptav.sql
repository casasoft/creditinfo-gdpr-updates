﻿CREATE TABLE [dbo].[ic_tegund_vidskiptav] (
    [customer_type_id] INT           NULL,
    [customer_type_en] VARCHAR (255) NULL,
    [customer_type]    VARCHAR (255) NULL,
    [customer_type_dk] VARCHAR (255) NULL,
    [selected_option]  VARCHAR (1)   NULL
);

