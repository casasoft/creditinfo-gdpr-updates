﻿CREATE TABLE [dbo].[cw_WatchUniqueID] (
    [CreditInfoID] INT            NULL,
    [WatchID]      INT            IDENTITY (1, 1) NOT NULL,
    [UniqueID]     NVARCHAR (100) NULL,
    [IsCompany]    CHAR (10)      COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Sent]         CHAR (10)      COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Created]      DATETIME       NULL,
    [UserID]       INT            NOT NULL,
    [sentDate]     SMALLDATETIME  NULL,
    CONSTRAINT [PK_cw_WatchUniqueID] PRIMARY KEY CLUSTERED ([WatchID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_cw_WatchUniqueID]
    ON [dbo].[cw_WatchUniqueID]([UserID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_cw_WatchUniqueID_1]
    ON [dbo].[cw_WatchUniqueID]([UniqueID] ASC) WITH (FILLFACTOR = 90);

