﻿CREATE TABLE [dbo].[np_PNumbers] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [NumberTypeID] INT           NULL,
    [Number]       NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [CreditInfoID] INT           NULL,
    CONSTRAINT [np_PNumbers_PK] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_PNumbers_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID]),
    CONSTRAINT [np_PNumberTypes_np_PNumbers_FK1] FOREIGN KEY ([NumberTypeID]) REFERENCES [dbo].[np_PNumberTypes] ([NumberTypeID])
);

