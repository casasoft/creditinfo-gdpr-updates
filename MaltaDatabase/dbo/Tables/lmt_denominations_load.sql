﻿CREATE TABLE [dbo].[lmt_denominations_load] (
    [denominationid] VARCHAR (20)  NOT NULL,
    [denomination]   NVARCHAR (50) NULL,
    [rate]           MONEY         NULL,
    CONSTRAINT [PK__lmt_denomination__245D67DE] PRIMARY KEY CLUSTERED ([denominationid] ASC) WITH (FILLFACTOR = 90)
);

