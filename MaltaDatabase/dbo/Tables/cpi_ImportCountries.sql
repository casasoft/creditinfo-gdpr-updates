﻿CREATE TABLE [dbo].[cpi_ImportCountries] (
    [CompanyCIID] INT NOT NULL,
    [CountryID]   INT NOT NULL,
    CONSTRAINT [cpi_ImportCountries_PK] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC, [CountryID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_ImportCountries_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID]),
    CONSTRAINT [np_Countries_cpi_ImportCountries_FK1] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[cpi_Countries] ([CountryID])
);

