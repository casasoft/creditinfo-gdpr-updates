﻿CREATE TABLE [dbo].[FSI_Fields] (
    [ID]                INT           IDENTITY (1, 1) NOT NULL,
    [NameNative]        VARCHAR (500) NULL,
    [NameEN]            VARCHAR (50)  NULL,
    [FieldName]         VARCHAR (256) NOT NULL,
    [Type]              VARCHAR (50)  NOT NULL,
    [DescriptionNative] VARCHAR (512) NULL,
    [DescriptionEN]     VARCHAR (512) NULL,
    [Category]          INT           NOT NULL
);

