﻿CREATE TABLE [dbo].[FSI_Templates] (
    [ID]                INT           NOT NULL,
    [NameNative]        VARCHAR (50)  NULL,
    [NameEN]            VARCHAR (50)  NULL,
    [DescriptionNative] VARCHAR (512) NULL,
    [DescriptionEN]     VARCHAR (512) NULL,
    CONSTRAINT [PK_FSI_Templates] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

