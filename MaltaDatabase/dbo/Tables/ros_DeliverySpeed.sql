﻿CREATE TABLE [dbo].[ros_DeliverySpeed] (
    [ID]                  INT           IDENTITY (1, 1) NOT NULL,
    [DeliverySpeedEN]     NVARCHAR (30) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DeliverySpeedNative] NVARCHAR (30) NULL,
    [DaysCount]           INT           NOT NULL,
    [UsageOfflineID]      INT           NOT NULL,
    [VisibleInCheckBoxes] VARCHAR (10)  CONSTRAINT [DF_ros_DeliverySpeed_VisibleInCheckBoxes] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_ros_DeliverySpeed] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

