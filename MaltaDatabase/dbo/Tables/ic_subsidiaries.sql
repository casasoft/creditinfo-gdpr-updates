﻿CREATE TABLE [dbo].[ic_subsidiaries] (
    [id]           INT          NOT NULL,
    [ft_kennitala] CHAR (10)    NULL,
    [kennitala]    CHAR (10)    NULL,
    [nafn]         VARCHAR (50) NULL,
    [eignarhluti]  VARCHAR (20) NULL,
    [nafn_eiganda] CHAR (20)    NULL
);

