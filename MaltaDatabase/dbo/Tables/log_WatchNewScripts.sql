﻿CREATE TABLE [dbo].[log_WatchNewScripts] (
    [stamp]     DATETIME DEFAULT (getdate()) NULL,
    [opn]       CHAR (1) NULL,
    [script_id] INT      NULL,
    [created]   DATETIME NULL
);

