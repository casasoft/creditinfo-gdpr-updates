﻿CREATE TABLE [dbo].[lmt_localities_load] (
    [localityid] INT            NOT NULL,
    [locality]   NVARCHAR (100) NULL,
    [countryid]  NVARCHAR (100) NULL,
    CONSTRAINT [PK__lmt_localities_l__04CFADEC] PRIMARY KEY CLUSTERED ([localityid] ASC) WITH (FILLFACTOR = 90)
);

