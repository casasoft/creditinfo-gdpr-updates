﻿CREATE TABLE [dbo].[cpi_Capital] (
    [CompanyCIID]           INT            NOT NULL,
    [AuthorizedCapital]     MONEY          NULL,
    [IssuedNumberOfShares]  MONEY          NULL,
    [Asked]                 MONEY          NULL,
    [PaidUp]                MONEY          NULL,
    [NominalNumberOfShares] MONEY          NULL,
    [SharesDescription]     NVARCHAR (255) NULL,
    [CurrencyCode]          NVARCHAR (3)   COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF_cpi_Capital_CurrencyCode] DEFAULT (N'MTL') NOT NULL,
    [ShareClass]            NVARCHAR (255) NULL
);

