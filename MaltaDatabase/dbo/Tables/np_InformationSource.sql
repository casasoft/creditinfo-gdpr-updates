﻿CREATE TABLE [dbo].[np_InformationSource] (
    [InformationSourceID] INT           IDENTITY (1, 1) NOT NULL,
    [NameEN]              NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [NameNative]          NVARCHAR (50) NULL,
    [CountryNative]       NVARCHAR (50) NULL,
    [CountryEN]           NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [np_InformationSource_PK] PRIMARY KEY CLUSTERED ([InformationSourceID] ASC) WITH (FILLFACTOR = 90)
);

