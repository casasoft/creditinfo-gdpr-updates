﻿CREATE TABLE [dbo].[np_VatNumbers_14082014] (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [CreditInfoID] INT            NOT NULL,
    [Number]       NVARCHAR (255) NOT NULL,
    [TraderName]   NVARCHAR (255) NULL,
    [Building]     NVARCHAR (255) NULL,
    [Address]      NVARCHAR (255) NULL,
    [Street]       NVARCHAR (255) NULL,
    [City]         NVARCHAR (255) NULL,
    [PostCode]     NVARCHAR (255) NULL,
    [Tel]          NVARCHAR (255) NULL,
    [Fax]          NVARCHAR (64)  NULL,
    [Note]         NVARCHAR (255) NULL,
    [Show]         BIT            NOT NULL
);

