﻿CREATE TABLE [dbo].[cpi_ReportStatus] (
    [StatusID]     INT           CONSTRAINT [DF__cpi_Repor__Statu__3E1D39E1] DEFAULT (0) NOT NULL,
    [StatusNative] NVARCHAR (50) CONSTRAINT [DF__cpi_Repor__Statu__3F115E1A] DEFAULT (N'0') NULL,
    [StatusEN]     VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_cpi_ReportStatus] PRIMARY KEY CLUSTERED ([StatusID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_ic_report_status1]
    ON [dbo].[cpi_ReportStatus]([StatusID] ASC) WITH (FILLFACTOR = 90);

