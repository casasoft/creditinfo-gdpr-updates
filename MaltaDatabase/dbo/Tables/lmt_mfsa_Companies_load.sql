﻿CREATE TABLE [dbo].[lmt_mfsa_Companies_load] (
    [Company_ID]         INT            NOT NULL,
    [Company_Name]       NVARCHAR (255) NULL,
    [Registration_No]    NVARCHAR (255) NULL,
    [Registration_Date]  SMALLDATETIME  NULL,
    [Address_1]          NVARCHAR (255) NULL,
    [Address_2]          NVARCHAR (255) NULL,
    [Locality_ID]        INT            NULL,
    [Postcode]           NVARCHAR (255) NULL,
    [Country_ID]         INT            NOT NULL,
    [State_ID]           INT            NULL,
    [Capital_Issued_For] FLOAT (53)     NULL,
    [Denomination_ID]    INT            NULL,
    [Total_Auth_Shr_Cap] FLOAT (53)     NULL,
    [Total_Iss_Shr_Cap]  FLOAT (53)     NULL,
    [Classification_ID]  INT            NULL,
    [Acc_Ref_Date]       NVARCHAR (255) NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_mfsa_Companies_load]
    ON [dbo].[lmt_mfsa_Companies_load]([Company_ID] ASC) WITH (FILLFACTOR = 90);

