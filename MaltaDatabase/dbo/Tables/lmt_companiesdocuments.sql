﻿CREATE TABLE [dbo].[lmt_companiesdocuments] (
    [CompanyID]       VARCHAR (50)   NULL,
    [CompanyName]     NVARCHAR (255) NULL,
    [Status]          VARCHAR (50)   NULL,
    [Classification]  NVARCHAR (255) NULL,
    [DataArchivedPub] DATETIME       NULL,
    [DocumentType]    NVARCHAR (255) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_lmt_companiesdocuments]
    ON [dbo].[lmt_companiesdocuments]([CompanyID] ASC) WITH (FILLFACTOR = 70);

