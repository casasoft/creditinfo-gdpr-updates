﻿CREATE TABLE [dbo].[lmt_coop] (
    [regno]      NVARCHAR (255) NOT NULL,
    [nameen]     NVARCHAR (255) NULL,
    [namenative] NVARCHAR (255) NULL,
    [address]    NVARCHAR (255) NULL,
    [town]       NVARCHAR (255) NULL,
    [pcode]      NVARCHAR (255) NULL,
    [tel]        NVARCHAR (255) NULL,
    [fax]        NVARCHAR (255) NULL,
    [email]      NVARCHAR (255) NULL,
    CONSTRAINT [PK_lmt_coop] PRIMARY KEY CLUSTERED ([regno] ASC) WITH (FILLFACTOR = 90)
);

