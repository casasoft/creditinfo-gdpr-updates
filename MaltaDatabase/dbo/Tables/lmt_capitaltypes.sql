﻿CREATE TABLE [dbo].[lmt_capitaltypes] (
    [capitalid] INT           NOT NULL,
    [capital]   NVARCHAR (25) NULL,
    CONSTRAINT [PK_cd_capitaltypes] PRIMARY KEY CLUSTERED ([capitalid] ASC) WITH (FILLFACTOR = 100)
);

