﻿CREATE TABLE [dbo].[cpi_EstatesPostalCode] (
    [PostalCode]     NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [LocationNative] NVARCHAR (50) NULL,
    [LocationEN]     NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_cpi_EstatesPostalCode] PRIMARY KEY CLUSTERED ([PostalCode] ASC) WITH (FILLFACTOR = 90)
);

