﻿CREATE TABLE [dbo].[FSI_Ratios] (
    [ID]                INT           IDENTITY (1, 1) NOT NULL,
    [TemplateID]        INT           NOT NULL,
    [NameNative]        VARCHAR (50)  NULL,
    [NameEN]            VARCHAR (50)  NULL,
    [DescriptionNative] VARCHAR (512) NULL,
    [DescriptionEN]     VARCHAR (512) NULL,
    [PercentageFormat]  VARCHAR (10)  NULL
);

