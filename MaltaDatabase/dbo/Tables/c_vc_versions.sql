﻿CREATE TABLE [dbo].[c_vc_versions] (
    [AppMajor] CHAR (2) CONSTRAINT [DF__c_vc_vers__AppMa__4D005615] DEFAULT ('00') NOT NULL,
    [AppMinor] CHAR (2) CONSTRAINT [DF__c_vc_vers__AppMi__4DF47A4E] DEFAULT ('00') NOT NULL,
    [AppPatch] CHAR (3) CONSTRAINT [DF__c_vc_vers__AppPa__4EE89E87] DEFAULT ('000') NOT NULL,
    [DBPatch]  CHAR (3) CONSTRAINT [DF__c_vc_vers__DBPat__4FDCC2C0] DEFAULT ('000') NOT NULL,
    [Inserted] DATETIME CONSTRAINT [DF__c_vc_vers__Inser__50D0E6F9] DEFAULT (getdate()) NOT NULL,
    [Updated]  DATETIME NULL
);

