﻿CREATE TABLE [dbo].[cpi_Charges] (
    [CompanyCIID]               INT           NOT NULL,
    [DateRegistered]            DATETIME      NULL,
    [DatePrepared]              DATETIME      NULL,
    [DescriptionID]             INT           NOT NULL,
    [Amount]                    MONEY         NULL,
    [Sequence]                  NVARCHAR (50) NULL,
    [BankID]                    VARCHAR (10)  NOT NULL,
    [CurrencyCode]              NVARCHAR (3)  COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [ID]                        INT           IDENTITY (1, 1) NOT NULL,
    [DescriptionFreeTextNative] VARCHAR (500) NULL,
    [DescriptionFreeTextEN]     VARCHAR (500) NULL,
    CONSTRAINT [FK_cpi_Charges_billing_Currency] FOREIGN KEY ([CurrencyCode]) REFERENCES [dbo].[billing_Currency] ([CurrencyCode]),
    CONSTRAINT [FK_cpi_Charges_cpi_Banks] FOREIGN KEY ([BankID]) REFERENCES [dbo].[cpi_Banks] ([BankID]),
    CONSTRAINT [FK_cpi_Charges_cpi_ChargesDescription] FOREIGN KEY ([DescriptionID]) REFERENCES [dbo].[cpi_ChargesDescription] ([DescriptionID])
);

