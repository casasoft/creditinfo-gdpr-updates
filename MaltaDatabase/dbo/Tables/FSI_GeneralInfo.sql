﻿CREATE TABLE [dbo].[FSI_GeneralInfo] (
    [AFS_id]                   INT          IDENTITY (1, 1) NOT NULL,
    [CreditInfoID]             INT          NOT NULL,
    [Financial_year]           VARCHAR (15) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [Financial_year_end]       DATETIME     NOT NULL,
    [Account_period_length]    INT          NOT NULL,
    [Currency]                 VARCHAR (15) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [Consolidated]             VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Qualified_audit]          VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Reviewed]                 VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Denomination_id]          INT          NOT NULL,
    [Employee_count]           INT          NULL,
    [Comments_id]              INT          NULL,
    [Date_of_delivery]         DATETIME     NULL,
    [Ready_for_web_publishing] VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Origin_id]                INT          NULL,
    [Registrator_id]           INT          NULL,
    [Created]                  DATETIME     CONSTRAINT [DF_FSI_GeneralInfo_Created] DEFAULT (getdate()) NULL,
    [Modified]                 DATETIME     NULL,
    [Deleted]                  VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF_FSI_GeneralInfo_Deleted] DEFAULT ('False') NULL,
    CONSTRAINT [PK_FSI_GeneralInfo] PRIMARY KEY CLUSTERED ([AFS_id] ASC) WITH (FILLFACTOR = 90)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1 == 0 , 2 == thousands, 3 == millions', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FSI_GeneralInfo', @level2type = N'COLUMN', @level2name = N'Denomination_id';

