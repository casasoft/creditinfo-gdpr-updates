﻿CREATE TABLE [dbo].[np_Companys] (
    [NameNative]           NVARCHAR (255) NULL,
    [NameEN]               NVARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Established]          DATETIME       CONSTRAINT [DF_np_Companys_Established] DEFAULT (getdate()) NULL,
    [LastContacted]        DATETIME       NULL,
    [URL]                  VARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [FuncID]               VARCHAR (30)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [org_status_code]      INT            NULL,
    [org_name_status_code] CHAR (3)       COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [org_status_date]      SMALLDATETIME  NULL,
    [CreditInfoID]         INT            NOT NULL,
    [LastUpdate]           DATETIME       NULL,
    [UpdateTimeStamp]      ROWVERSION     NOT NULL,
    [Registered]           DATETIME       NULL,
    CONSTRAINT [PK_np_Companys] PRIMARY KEY CLUSTERED ([CreditInfoID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_Companys_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID]),
    CONSTRAINT [FK_np_Companys_np_org_status] FOREIGN KEY ([org_status_code]) REFERENCES [dbo].[np_org_status] ([id]),
    CONSTRAINT [np_ComanyFunction_np_companys_FK1] FOREIGN KEY ([FuncID]) REFERENCES [dbo].[np_CompanyFunction] ([FuncID])
);


GO
create trigger tr_npcompanys_del
  on np_companys
  for delete 
  as
    delete from np_word2ciid
      where creditinfoid=(select creditinfoid from deleted)


GO
create trigger tr_npcompanys_upd
  on np_companys
  for update
  as
    declare @i int
    declare @ciid int, @d_nen nvarchar(150), @i_nen nvarchar(150), @d_nna nvarchar(150), @i_nna nvarchar(150)
    declare @np nvarchar(150)
    declare cur_trnpcompanysupd cursor for
      select del.creditinfoid, del.nameen, del.namenative, ins.nameen, ins.namenative
        from deleted del join inserted ins on del.creditinfoid=ins.creditinfoid

    open cur_trnpcompanysupd
    fetch next from cur_trnpcompanysupd into @ciid, @d_nen, @d_nna, @i_nen, @i_nna
    while @@fetch_status=0
    begin
      if @ciid is not null --and (@d_nen <> @i_nen or @d_nna <> @i_nna or not exists (select * from np_word2ciid where creditinfoid=@ciid))
        begin
        delete from np_word2ciid
          where creditinfoid=@ciid and type='n'
        set @np = @i_nna
        insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'n', @np)
        set @i=charindex(' ',@np)
        while @i  > 0
          begin
          if not exists(select * from np_word2ciid where creditinfoid=@ciid and type='n' and word=ltrim(substring(@np, @i+1, len(@np))))
            begin
            insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'n', ltrim(substring(@np, @i+1, len(@np))))
            end
          set @i = charindex(' ',@np, @i+1)
          end

        set @np = @i_nen
        if not exists (select * from np_word2ciid where creditinfoid=@ciid and type='n' and word=@np)
          begin
          insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'n', @np)
          set @i=charindex(' ',@np)
          while @i  > 0
            begin
            if not exists(select * from np_word2ciid where creditinfoid=@ciid and type='n' and word=ltrim(substring(@np, @i+1, len(@np))))
              begin
              insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'n', ltrim(substring(@np, @i+1, len(@np))))
              end
            set @i = charindex(' ',@np, @i+1)
            end
          end
        end
      fetch next from cur_trnpcompanysupd into @ciid, @d_nen, @d_nna, @i_nen, @i_nna
      end
    close cur_trnpcompanysupd
    deallocate cur_trnpcompanysupd



GO
create trigger tr_npcompanys_ins
  on np_companys
  for insert
  as
    declare @i int
    declare @ciid int, @i_nen nvarchar(150), @i_nna nvarchar(150)
    declare @np nvarchar(150)
    select @ciid=creditinfoid,@i_nen=nameen, @i_nna=namenative
      from inserted
    
    if @ciid is not null
      begin
	set @np = @i_nna
	insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'n', @np)
	set @i=charindex(' ',@np)
	while @i  > 0
	  begin
	  if 0=(select count(*) from np_word2ciid where creditinfoid=@ciid and type='n' and word=ltrim(substring(@np, @i+1, len(@np))))
	    begin
	    insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'n', ltrim(substring(@np, @i+1, len(@np))))
	    end
          set @i = charindex(' ',@np, @i+1)
          end
        set @np = @i_nen
        if 0=(select count(*) from np_word2ciid where creditinfoid=@ciid and type='n' and word=@np)
          begin
          insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'n', @np)
          end
        set @i=charindex(' ',@np)
        while @i  > 0
          begin
          if 0=(select count(*) from np_word2ciid where creditinfoid=@ciid and type='n' and word=ltrim(substring(@np, @i+1, len(@np))))
            begin
            insert into np_Word2CIID(creditinfoid, type, word) values (@ciid, 'n', ltrim(substring(@np, @i+1, len(@np))))
            end
          set @i = charindex(' ',@np, @i+1)
          end
       end

