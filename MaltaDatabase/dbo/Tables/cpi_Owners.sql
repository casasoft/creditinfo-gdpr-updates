﻿CREATE TABLE [dbo].[cpi_Owners] (
    [CompanyCIID]    INT            CONSTRAINT [DF__cpi_Owner__Compa__76619304] DEFAULT (0) NOT NULL,
    [OwnerCIID]      INT            CONSTRAINT [DF__cpi_Owner__Owner__7755B73D] DEFAULT (0) NOT NULL,
    [OwnershipID]    INT            NOT NULL,
    [Ownership]      BIGINT         CONSTRAINT [DF__cpi_Owner__Owner__7849DB76] DEFAULT (0) NULL,
    [BuyDate]        DATETIME       CONSTRAINT [DF__cpi_Owner__BuyDa__793DFFAF] DEFAULT (getdate()) NULL,
    [ownership_perc] DECIMAL (8, 5) NULL,
    CONSTRAINT [PK_cpi_Owners] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC, [OwnerCIID] ASC, [OwnershipID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_Owners_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID]),
    CONSTRAINT [FK_cpi_Owners_cpi_Ownership] FOREIGN KEY ([OwnershipID]) REFERENCES [dbo].[cpi_Ownership] ([OwnershipID]),
    CONSTRAINT [FK_cpi_Owners_np_CreditInfoUser] FOREIGN KEY ([OwnerCIID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID])
);


GO
CREATE NONCLUSTERED INDEX [ix_ic_eigendur1]
    ON [dbo].[cpi_Owners]([CompanyCIID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_ic_eigendur2]
    ON [dbo].[cpi_Owners]([OwnerCIID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [cpi_Owners_PK]
    ON [dbo].[cpi_Owners]([OwnershipID] ASC, [CompanyCIID] ASC, [OwnerCIID] ASC) WITH (FILLFACTOR = 90);

