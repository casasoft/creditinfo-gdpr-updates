﻿CREATE TABLE [dbo].[np_CreditInfoUser] (
    [CreditInfoID] INT           IDENTITY (1, 1) NOT NULL,
    [DateCreated]  DATETIME      CONSTRAINT [DF__np_Credit__DateC__2E1BDC42] DEFAULT (getdate()) NULL,
    [Type]         NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Email]        VARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [StatusID]     INT           NULL,
    [cw_allcases]  INT           NULL,
    [cw_messform]  INT           CONSTRAINT [DF_np_CreditInfoUser_cw_messform] DEFAULT (1) NULL,
    [originid]     INT           NULL,
    [IsSearchable] CHAR (5)      CONSTRAINT [DF_np_CreditInfoUser_IsSearchable] DEFAULT ('True') NOT NULL,
    CONSTRAINT [np_CreditInfoUser_PK] PRIMARY KEY CLUSTERED ([CreditInfoID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_np_CreditInfoUser]
    ON [dbo].[np_CreditInfoUser]([IsSearchable] ASC) WITH (FILLFACTOR = 90);

