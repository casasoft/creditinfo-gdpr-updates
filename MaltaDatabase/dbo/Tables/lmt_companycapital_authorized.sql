﻿CREATE TABLE [dbo].[lmt_companycapital_authorized] (
    [capitalid]        INT            NOT NULL,
    [companyid]        NVARCHAR (255) NULL,
    [class]            NVARCHAR (255) NULL,
    [authsharecapital] BIGINT         NULL,
    [nominalvalue]     FLOAT (53)     NULL,
    [rowid]            INT            IDENTITY (1, 1) NOT NULL
);

