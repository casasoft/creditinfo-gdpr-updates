﻿CREATE TABLE [dbo].[np_Education] (
    [EducationID]       INT            IDENTITY (1, 1) NOT NULL,
    [NameEN]            NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [NameNative]        NVARCHAR (100) NULL,
    [DescriptionEN]     NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative] NVARCHAR (100) NULL,
    CONSTRAINT [np_Education_PK] PRIMARY KEY CLUSTERED ([EducationID] ASC) WITH (FILLFACTOR = 90)
);

