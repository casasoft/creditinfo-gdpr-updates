﻿CREATE TABLE [dbo].[lmt_titles_load] (
    [titleid] INT            NOT NULL,
    [title]   NVARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    PRIMARY KEY CLUSTERED ([titleid] ASC) WITH (FILLFACTOR = 90)
);

