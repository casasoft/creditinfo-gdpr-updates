﻿CREATE TABLE [dbo].[lmt_involvementtypes] (
    [involvementtypeid] INT            NOT NULL,
    [involvement]       NVARCHAR (100) NULL,
    CONSTRAINT [PK_cd_involvementtypes] PRIMARY KEY CLUSTERED ([involvementtypeid] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [ix_cdinvolvementtypes_involvtypeid]
    ON [dbo].[lmt_involvementtypes]([involvementtypeid] ASC) WITH (FILLFACTOR = 100);

