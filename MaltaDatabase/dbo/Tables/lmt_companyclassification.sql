﻿CREATE TABLE [dbo].[lmt_companyclassification] (
    [classificationid] INT            NOT NULL,
    [classification]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_cd_companyclassification] PRIMARY KEY CLUSTERED ([classificationid] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [ix_cdcompanyclassificatoni_classificationid]
    ON [dbo].[lmt_companyclassification]([classificationid] ASC) WITH (FILLFACTOR = 100);

