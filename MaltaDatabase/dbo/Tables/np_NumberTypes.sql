﻿CREATE TABLE [dbo].[np_NumberTypes] (
    [NumberTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [TypeEN]       NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [TypeNative]   NVARCHAR (50) NULL,
    CONSTRAINT [np_NumberTypes_PK] PRIMARY KEY CLUSTERED ([NumberTypeID] ASC) WITH (FILLFACTOR = 90)
);

