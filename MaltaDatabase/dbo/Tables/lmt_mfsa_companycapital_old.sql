﻿CREATE TABLE [dbo].[lmt_mfsa_companycapital_old] (
    [Cap_ID]             INT           NOT NULL,
    [Company_ID]         INT           NOT NULL,
    [FLD_Class]          VARCHAR (255) NULL,
    [Auth_Shr_Cap]       FLOAT (53)    NULL,
    [Nominal_value]      FLOAT (53)    NULL,
    [Issued_Shr_Capital] FLOAT (53)    NULL,
    [Percentage_Pd_Up]   FLOAT (53)    NULL
);


GO
CREATE CLUSTERED INDEX [ix_cdcompanycapital_companyid]
    ON [dbo].[lmt_mfsa_companycapital_old]([Cap_ID] ASC) WITH (FILLFACTOR = 90);

