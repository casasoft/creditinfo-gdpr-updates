﻿CREATE TABLE [dbo].[cr_FileFormat] (
    [ID]         INT          IDENTITY (1, 1) NOT NULL,
    [NameNative] VARCHAR (50) COLLATE SQL_Latin1_General_CP1253_CI_AI NOT NULL,
    [NameEN]     CHAR (10)    COLLATE SQL_Latin1_General_CP1253_CI_AI NULL,
    [IsOpen]     VARCHAR (10) DEFAULT ('False') NOT NULL
);

