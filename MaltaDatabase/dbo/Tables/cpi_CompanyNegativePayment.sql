﻿CREATE TABLE [dbo].[cpi_CompanyNegativePayment] (
    [CompanyCIID]       INT CONSTRAINT [DF__cpi_Compa__Compa__58D1301D] DEFAULT (0) NOT NULL,
    [NegativePaymentID] INT CONSTRAINT [DF__cpi_Compa__Negat__59C55456] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_cpi_CompanyNegativePayment] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC, [NegativePaymentID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_CompanyNegativePayment_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID])
);


GO
CREATE NONCLUSTERED INDEX [ix446_1]
    ON [dbo].[cpi_CompanyNegativePayment]([CompanyCIID] ASC, [NegativePaymentID] ASC) WITH (FILLFACTOR = 90);

