﻿CREATE TABLE [dbo].[cpi_RegistrationForm] (
    [FormID]     INT            NOT NULL,
    [NameNative] NVARCHAR (100) NULL,
    [NameEN]     VARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [cpi_RegistrationForm_PK] PRIMARY KEY CLUSTERED ([FormID] ASC) WITH (FILLFACTOR = 90)
);

