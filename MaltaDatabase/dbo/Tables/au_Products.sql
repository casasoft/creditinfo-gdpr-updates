﻿CREATE TABLE [dbo].[au_Products] (
    [ProductID]                INT             NOT NULL,
    [ProductNameNative]        NVARCHAR (100)  NOT NULL,
    [ProductNameEN]            NVARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [IsOpen]                   CHAR (10)       COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [NavigateUrl]              NVARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [ParentID]                 INT             NULL,
    [Created]                  DATETIME        NOT NULL,
    [IsBaseProduct]            CHAR (10)       COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [Menu]                     CHAR (10)       COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF_au_Products_Menu] DEFAULT ('False') NOT NULL,
    [MenuNameNative]           NVARCHAR (100)  NULL,
    [MenuNameEN]               NVARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [IsInner]                  CHAR (10)       COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [ProductDescriptionNative] NVARCHAR (1024) NULL,
    [ProductDescriptionEN]     NVARCHAR (1024) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_au_Products] PRIMARY KEY CLUSTERED ([ProductID] ASC) WITH (FILLFACTOR = 90)
);

