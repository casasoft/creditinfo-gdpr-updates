﻿CREATE TABLE [dbo].[ic_einstaklingar] (
    [person_id] INT           NOT NULL,
    [kennitala] CHAR (10)     NULL,
    [nafn]      VARCHAR (50)  NULL,
    [adsetur]   VARCHAR (255) NULL,
    [postnumer] VARCHAR (99)  NULL,
    [ci_cid]    INT           NOT NULL
);

