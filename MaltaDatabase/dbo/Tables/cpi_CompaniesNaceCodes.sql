﻿CREATE TABLE [dbo].[cpi_CompaniesNaceCodes] (
    [CompanyCIID] INT          CONSTRAINT [DF__cpi_Compa__Compa__625A9A57] DEFAULT (0) NOT NULL,
    [NaceCodeID]  VARCHAR (10) CONSTRAINT [DF__cpi_Compa__NaceC__634EBE90] DEFAULT ('0') NOT NULL,
    [CurrentCode] VARCHAR (10) CONSTRAINT [DF_cpi_CompaniesNaceCodes_Current] DEFAULT ('True') NOT NULL,
    [OrderLevel]  INT          NULL,
    CONSTRAINT [u223_123] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC, [NaceCodeID] ASC, [CurrentCode] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_CompaniesNaceCodes_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID]),
    CONSTRAINT [cpi_NaceCodes_cpi_CompaniesNaceCodes_FK1] FOREIGN KEY ([NaceCodeID]) REFERENCES [dbo].[cpi_NaceCodes] ([NaceCodeID])
);

