﻿CREATE TABLE [dbo].[np_IDNumbers] (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [Number]       NVARCHAR (100) NULL,
    [NumberTypeID] INT            NOT NULL,
    [CreditInfoID] INT            NOT NULL,
    CONSTRAINT [np_IDNumbers_PK] PRIMARY KEY CLUSTERED ([CreditInfoID] ASC, [NumberTypeID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_np_IDNumbers] CHECK (len([Number]) > 0),
    CONSTRAINT [FK_np_IDNumbers_np_CreditInfoUser] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_CreditInfoUser] ([CreditInfoID]),
    CONSTRAINT [np_NumberTypes_np_IDNumbers_FK1] FOREIGN KEY ([NumberTypeID]) REFERENCES [dbo].[np_NumberTypes] ([NumberTypeID]),
    CONSTRAINT [IX_np_IDNumbers] UNIQUE NONCLUSTERED ([Number] ASC, [NumberTypeID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_np_IDNumbers_1]
    ON [dbo].[np_IDNumbers]([Number] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_npidnumbes_creditinfoid]
    ON [dbo].[np_IDNumbers]([CreditInfoID] ASC) WITH (FILLFACTOR = 90);

