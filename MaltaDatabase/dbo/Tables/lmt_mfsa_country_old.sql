﻿CREATE TABLE [dbo].[lmt_mfsa_country_old] (
    [Country_ID] INT            NOT NULL,
    [Country]    NVARCHAR (100) NULL,
    [is_local]   INT            NULL
);


GO
CREATE CLUSTERED INDEX [ix_cdcountry_countrid]
    ON [dbo].[lmt_mfsa_country_old]([Country_ID] ASC) WITH (FILLFACTOR = 90);

