﻿CREATE TABLE [dbo].[lmt_companystates] (
    [statusid] INT            NOT NULL,
    [state]    NVARCHAR (255) NULL,
    CONSTRAINT [PK_cd_companystates] PRIMARY KEY CLUSTERED ([statusid] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [ix_cdcompanystates_stateid]
    ON [dbo].[lmt_companystates]([statusid] ASC) WITH (FILLFACTOR = 100);

