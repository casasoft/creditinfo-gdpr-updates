﻿CREATE TABLE [dbo].[lmt_ddd_load] (
    [LT_REF]         INT            NULL,
    [DEBTOR_ID]      NVARCHAR (15)  COLLATE Icelandic_CI_AS NULL,
    [INFO_SOURCE]    NVARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    [CASE_NO]        NVARCHAR (30)  COLLATE Icelandic_CI_AS NULL,
    [ISSUE_DATE]     SMALLDATETIME  NULL,
    [AMOUNT]         DECIMAL (18)   NULL,
    [SUBJECT_MATTER] NVARCHAR (50)  COLLATE Icelandic_CI_AS NULL,
    [PLAINTIFF]      NVARCHAR (100) COLLATE Icelandic_CI_AS NULL,
    [REG_DATE]       SMALLDATETIME  NULL,
    [OPEN]           NVARCHAR (3)   COLLATE Icelandic_CI_AS NULL
);

