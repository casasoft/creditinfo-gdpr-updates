﻿CREATE TABLE [dbo].[ic_throun] (
    [ft_kennitala]    CHAR (10)     NOT NULL,
    [throun_dags]     SMALLDATETIME NOT NULL,
    [throun_texti]    TEXT          NULL,
    [throun_texti_en] TEXT          NULL,
    [throun_texti_dk] TEXT          NULL
);

