﻿CREATE TABLE [dbo].[lmt_companiesdocuments_load] (
    [CompanyID]       VARCHAR (50)   NULL,
    [CompanyName]     NVARCHAR (255) NULL,
    [Status]          VARCHAR (50)   NULL,
    [Classification]  NVARCHAR (255) NULL,
    [DataArchivedPub] DATETIME       NULL,
    [DocumentType]    NVARCHAR (255) NULL
);

