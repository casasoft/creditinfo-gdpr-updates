﻿CREATE TABLE [dbo].[lmt_mfsa_involvedparties_old] (
    [Involved_Party_ID] INT            NULL,
    [IP_Surname]        NVARCHAR (100) NULL,
    [IP_Name]           NVARCHAR (20)  NULL,
    [Title]             NVARCHAR (255) NULL,
    [IP_ID_No]          VARCHAR (20)   NULL,
    [IP_Passport_No]    VARCHAR (20)   NULL,
    [IP_Co_Name]        NVARCHAR (150) NULL,
    [IP_Co_Reg_No]      VARCHAR (20)   NULL,
    [Nationality_ID]    INT            NULL,
    [IP_Address_1]      NVARCHAR (100) NULL,
    [IP_Address_2]      NVARCHAR (100) NULL,
    [Locality_ID]       INT            NULL,
    [Postcode]          VARCHAR (20)   NULL,
    [Country_ID]        INT            NULL
);


GO
CREATE CLUSTERED INDEX [cd_involvedparties_involvedpartyid]
    ON [dbo].[lmt_mfsa_involvedparties_old]([Involved_Party_ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_cdinvolvedparties_ipidno]
    ON [dbo].[lmt_mfsa_involvedparties_old]([IP_ID_No] ASC) WITH (FILLFACTOR = 90);

