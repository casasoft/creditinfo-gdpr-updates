﻿CREATE TABLE [dbo].[np_org_status] (
    [id]         INT            NOT NULL,
    [nameNative] NVARCHAR (100) NULL,
    [nativeEN]   NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_np_org_status] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 90)
);

