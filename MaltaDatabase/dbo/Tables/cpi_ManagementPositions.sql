﻿CREATE TABLE [dbo].[cpi_ManagementPositions] (
    [ManagementPositionID] INT           NOT NULL,
    [TitleNative]          NVARCHAR (40) NULL,
    [TitleEN]              VARCHAR (40)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [cpi_ManagementPositons_PK] PRIMARY KEY CLUSTERED ([ManagementPositionID] ASC) WITH (FILLFACTOR = 90)
);

