﻿CREATE TABLE [dbo].[cpi_CompanyBanks] (
    [CompanyCIID] INT          NOT NULL,
    [BankID]      VARCHAR (10) NOT NULL,
    CONSTRAINT [cpi_CompanyBanks_PK] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC, [BankID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_CompanyBanks_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID]),
    CONSTRAINT [FK_cpi_CompanyBanks_cpi_Banks] FOREIGN KEY ([BankID]) REFERENCES [dbo].[cpi_Banks] ([BankID])
);

