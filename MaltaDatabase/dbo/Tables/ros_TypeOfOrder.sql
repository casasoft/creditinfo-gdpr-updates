﻿CREATE TABLE [dbo].[ros_TypeOfOrder] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [TypeEN]     NVARCHAR (30) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [TypeNative] NVARCHAR (30) NULL,
    CONSTRAINT [PK_ros_TypeOfOrder] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

