﻿CREATE TABLE [dbo].[FSI_Comments] (
    [Comments_id]        INT           IDENTITY (1, 1) NOT NULL,
    [Description_native] NVARCHAR (80) NULL,
    [Description_en]     VARCHAR (80)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_FSI_Comments] PRIMARY KEY CLUSTERED ([Comments_id] ASC) WITH (FILLFACTOR = 90)
);

