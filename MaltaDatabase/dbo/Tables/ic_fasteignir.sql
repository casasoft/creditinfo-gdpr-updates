﻿CREATE TABLE [dbo].[ic_fasteignir] (
    [ft_kennitala]                CHAR (10)     NOT NULL,
    [fasteignir_id]               INT           NOT NULL,
    [fasteignir_adsetur]          VARCHAR (255) NULL,
    [postnumer]                   INT           NULL,
    [eignaradild_id]              INT           NULL,
    [fasteignir_tryggingaverdmat] FLOAT (53)    NULL,
    [fasteignir_markadsverd]      FLOAT (53)    NULL,
    [fasteignir_ved]              FLOAT (53)    NULL,
    [fasteignir_fermetrar]        VARCHAR (9)   NULL,
    [fasteignir_bygginarar]       VARCHAR (20)  NULL,
    [teg_fasteign_id]             INT           NULL,
    [teg_eignar_id]               INT           NULL
);

