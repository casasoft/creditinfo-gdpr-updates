﻿CREATE TABLE [dbo].[au_users] (
    [id]             INT            IDENTITY (1, 1) NOT NULL,
    [CreditInfoID]   INT            NULL,
    [UserName]       NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [SubscriberID]   INT            NULL,
    [NationalID]     INT            NULL,
    [Email]          NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [PasswordHash]   VARCHAR (40)   COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [salt]           VARCHAR (10)   COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [groups]         NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [UserType]       NVARCHAR (50)  NULL,
    [cntCreditWatch] INT            NULL,
    [RegisteredBy]   INT            NULL,
    [HasWebServices] CHAR (10)      COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [IsOpen]         CHAR (10)      COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [OpenUntil]      DATETIME       NULL,
    [DateCreated]    DATETIME       CONSTRAINT [DF_au_users_DateCreated] DEFAULT (getdate()) NULL,
    [DateUpdated]    DATETIME       NULL,
    [cw_allcases]    VARCHAR (5)    COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__au_users__cw_all__78F3E6EC] DEFAULT ('True') NULL,
    [cw_messform]    INT            CONSTRAINT [DF__au_users__cw_mes__79E80B25] DEFAULT (1) NULL,
    [DepartmentID]   INT            NULL,
    CONSTRAINT [PK_au_users] PRIMARY KEY CLUSTERED ([UserName] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_au_users_c_au_department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[c_au_department] ([DepartmentID])
);


GO
CREATE NONCLUSTERED INDEX [IX_au_users_subscriberID]
    ON [dbo].[au_users]([SubscriberID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_au_users_id]
    ON [dbo].[au_users]([id] ASC) WITH (FILLFACTOR = 90);

