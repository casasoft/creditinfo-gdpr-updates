﻿CREATE TABLE [dbo].[scorAccountsAge] (
    [id]      INT             IDENTITY (1, 1) NOT NULL,
    [MinTest] INT             NOT NULL,
    [MaxTest] INT             NULL,
    [Value]   DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_scorAccountsAge] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 70)
);

