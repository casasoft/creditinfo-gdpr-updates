﻿CREATE TABLE [dbo].[np_CompanyFunction] (
    [FuncID]                 VARCHAR (30)   COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [DescriptionShortEN]     NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionShortNative] NVARCHAR (50)  NULL,
    [DescriptionEN]          NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative]      NVARCHAR (100) NULL,
    CONSTRAINT [np_ComanyFunction_PK] PRIMARY KEY CLUSTERED ([FuncID] ASC) WITH (FILLFACTOR = 90)
);

