﻿CREATE TABLE [dbo].[cw_newclaims] (
    [id]       INT      IDENTITY (1, 1) NOT NULL,
    [claim_id] INT      NULL,
    [statusid] INT      NULL,
    [created]  DATETIME CONSTRAINT [DF__cw_newcla__creat__2EA5EC27] DEFAULT (getdate()) NULL
);

