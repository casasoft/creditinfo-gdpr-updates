﻿CREATE TABLE [dbo].[v2_Debtors_postcodes] (
    [Debtor_Ref]        INT           NOT NULL,
    [RegisterdBy]       NVARCHAR (50) NULL,
    [CaseNo]            NVARCHAR (50) NULL,
    [InformationSource] NVARCHAR (50) NULL,
    [Case type]         INT           NULL,
    [Date]              SMALLDATETIME NULL,
    [DebtorId]          NVARCHAR (50) NULL,
    [DebtorFirstName]   NVARCHAR (50) NULL,
    [DebtorSurName]     NVARCHAR (50) NULL,
    [Profession]        NVARCHAR (50) NULL,
    [DebtorAddress]     NVARCHAR (50) NULL,
    [DebtorTown]        NVARCHAR (50) NULL,
    [Postcode]          INT           NULL,
    [Use]               BIT           NOT NULL,
    [Address2]          NVARCHAR (50) NULL,
    [Comments]          NTEXT         NULL,
    [Sent]              SMALLDATETIME NULL,
    [newprofessionid]   INT           NULL,
    [newcityid]         INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_v2_Debtors_postcodes]
    ON [dbo].[v2_Debtors_postcodes]([Debtor_Ref] ASC) WITH (FILLFACTOR = 90);

