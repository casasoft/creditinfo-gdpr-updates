﻿CREATE TABLE [dbo].[lmt_mfsa_involvements_old] (
    [Involvement_ID]    INT NULL,
    [Company_ID]        INT NULL,
    [Involved_Party_ID] INT NOT NULL,
    [Involv_Type_ID]    INT NULL
);


GO
CREATE CLUSTERED INDEX [ix_cdinvolvements_involvementid]
    ON [dbo].[lmt_mfsa_involvements_old]([Involvement_ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_cdinvolvements_companyid]
    ON [dbo].[lmt_mfsa_involvements_old]([Company_ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_cdinvolvements_involvedpartyid]
    ON [dbo].[lmt_mfsa_involvements_old]([Involved_Party_ID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix_cdinvolvements_involvtypeid]
    ON [dbo].[lmt_mfsa_involvements_old]([Involv_Type_ID] ASC) WITH (FILLFACTOR = 90);

