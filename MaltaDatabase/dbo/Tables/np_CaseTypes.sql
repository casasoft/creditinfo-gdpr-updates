﻿CREATE TABLE [dbo].[np_CaseTypes] (
    [CaseTypeID]        INT            IDENTITY (1, 1) NOT NULL,
    [CaseID]            INT            NOT NULL,
    [TypeEN]            NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [TypeNative]        NVARCHAR (100) NULL,
    [DescriptionEN]     NVARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative] NVARCHAR (255) NULL,
    CONSTRAINT [PK_np_CaseTypes] PRIMARY KEY CLUSTERED ([CaseTypeID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_CaseTypes_np_Case] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[np_Case] ([ClaimTypeID])
);

