﻿CREATE TABLE [dbo].[np_ChequeIssueBanks] (
    [BankID]     INT            IDENTITY (1, 1) NOT NULL,
    [NameNative] NVARCHAR (100) NULL,
    [NameEN]     NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_np_ChequeIssueBanks] PRIMARY KEY CLUSTERED ([BankID] ASC) WITH (FILLFACTOR = 90)
);

