﻿CREATE TABLE [dbo].[cpi_CompanyLaywers] (
    [LaywerCIID]    INT           NOT NULL,
    [CompanyCIID]   INT           NOT NULL,
    [OfficeNumber]  VARCHAR (50)  NULL,
    [Building]      VARCHAR (50)  NULL,
    [HistoryNative] VARCHAR (500) NULL,
    [HistoryEN]     VARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AI NULL
);

