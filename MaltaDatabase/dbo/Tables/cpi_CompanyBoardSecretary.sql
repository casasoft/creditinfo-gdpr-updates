﻿CREATE TABLE [dbo].[cpi_CompanyBoardSecretary] (
    [SecretaryCIID] INT           NOT NULL,
    [CompanyCIID]   INT           NOT NULL,
    [HistoryNative] VARCHAR (500) NULL,
    [HistoryEN]     VARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AI NULL
);

