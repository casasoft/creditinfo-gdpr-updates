﻿CREATE TABLE [dbo].[lmt_localities] (
    [localityid] INT            NOT NULL,
    [locality]   NVARCHAR (100) NULL,
    [countryid]  NVARCHAR (100) NULL,
    CONSTRAINT [PK_cd_localities] PRIMARY KEY CLUSTERED ([localityid] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [ix_cdlocalities_localityid]
    ON [dbo].[lmt_localities]([localityid] ASC) WITH (FILLFACTOR = 100);

