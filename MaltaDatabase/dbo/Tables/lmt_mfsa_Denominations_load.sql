﻿CREATE TABLE [dbo].[lmt_mfsa_Denominations_load] (
    [Denomination_ID] INT            NULL,
    [Denomination]    NVARCHAR (255) NULL,
    [Xrate]           FLOAT (53)     NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_mfsa_Denominations_load]
    ON [dbo].[lmt_mfsa_Denominations_load]([Denomination_ID] ASC) WITH (FILLFACTOR = 90);

