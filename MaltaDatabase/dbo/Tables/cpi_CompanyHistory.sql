﻿CREATE TABLE [dbo].[cpi_CompanyHistory] (
    [CompanyCIID]   INT            NOT NULL,
    [HistoryTypeID] INT            NOT NULL,
    [HistoryNative] NVARCHAR (500) NULL,
    [HistoryEN]     VARCHAR (500)  NULL
);

