﻿CREATE TABLE [dbo].[mt_Adjudicators] (
    [ID]                    INT          IDENTITY (1, 1) NOT NULL,
    [AdjudicatorNameNative] VARCHAR (50) NULL,
    [AdjudicatorNameEN]     VARCHAR (50) NULL
);

