﻿CREATE TABLE [dbo].[billing_ItemRegistry] (
    [ID]                     INT            IDENTITY (1, 1) NOT NULL,
    [Currency]               NVARCHAR (3)   COLLATE SQL_Latin1_General_CP1_CI_AI NOT NULL,
    [ItemNameNative]         NVARCHAR (100) NOT NULL,
    [ItemNameEN]             NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [MonthlyAmount]          MONEY          NULL,
    [UsageAmount]            MONEY          NULL,
    [MonthlyDiscountPercent] DECIMAL (5, 2) NULL,
    [UsageDiscountPercent]   DECIMAL (5, 2) NULL,
    [LoggingCodeID]          INT            CONSTRAINT [DF_billing_ItemRegistry_LoggingCodeID] DEFAULT (null) NULL,
    [ProductCodeID]          INT            CONSTRAINT [DF_billing_ItemRegistry_ProductCodeID] DEFAULT (null) NULL,
    [IsActive]               CHAR (10)      COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [FinancialKey]           NVARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [MasterItemID]           INT            NULL,
    [Inserted]               DATETIME       NULL,
    [InsertedBy]             INT            NULL,
    [Updated]                DATETIME       NULL,
    [UpdatedBy]              INT            NULL,
    CONSTRAINT [PK_billing_ItemRegistry] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_billing_ItemRegistry_au_Products] FOREIGN KEY ([ProductCodeID]) REFERENCES [dbo].[au_Products] ([ProductID]),
    CONSTRAINT [FK_billing_ItemRegistry_billing_Currency] FOREIGN KEY ([Currency]) REFERENCES [dbo].[billing_Currency] ([CurrencyCode]),
    CONSTRAINT [FK_billing_ItemRegistry_np_UsageTypes] FOREIGN KEY ([LoggingCodeID]) REFERENCES [dbo].[np_UsageTypes] ([id])
);

