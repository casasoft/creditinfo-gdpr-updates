﻿CREATE TABLE [dbo].[cpi_Ownership] (
    [OwnershipID]                INT           IDENTITY (1, 1) NOT NULL,
    [OwnershipDescriptionNative] NVARCHAR (50) CONSTRAINT [DF__cpi_Owner__Owner__73852659] DEFAULT (N'0') NULL,
    [OwnershipDescriptionEN]     VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Owner__Owner__74794A92] DEFAULT ('0') NULL,
    CONSTRAINT [PK_cpi_Ownership] PRIMARY KEY CLUSTERED ([OwnershipID] ASC) WITH (FILLFACTOR = 90)
);

