﻿CREATE TABLE [dbo].[cw_WatchWords] (
    [CreditInfoID]    INT            NULL,
    [WatchID]         INT            IDENTITY (1, 1) NOT NULL,
    [FirstNameEN]     NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [FirstNameNative] NVARCHAR (100) NULL,
    [SurNameEN]       NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [SurNameNative]   NVARCHAR (100) NULL,
    [AddressEN]       NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [AddressNative]   NVARCHAR (100) NULL,
    [TownEN]          NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [TownNative]      NVARCHAR (100) NULL,
    [IsCompany]       CHAR (10)      COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Sent]            CHAR (10)      COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [Created]         DATETIME       NULL,
    [UserID]          INT            NOT NULL,
    [sentDate]        SMALLDATETIME  NULL,
    CONSTRAINT [PK_cw_WatchWords] PRIMARY KEY CLUSTERED ([WatchID] ASC) WITH (FILLFACTOR = 90)
);

