﻿CREATE TABLE [dbo].[lmt_involvementtypes_load] (
    [involvementtypeid] INT            NOT NULL,
    [involvement]       NVARCHAR (100) NULL,
    CONSTRAINT [PK__lmt_involvementt__02E7657A] PRIMARY KEY CLUSTERED ([involvementtypeid] ASC) WITH (FILLFACTOR = 90)
);

