﻿CREATE TABLE [dbo].[lmt_mfsa_CompanyClassification_load] (
    [Classification_ID] INT            NOT NULL,
    [Classification]    NVARCHAR (255) NOT NULL,
    [Ordering]          INT            NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_mfsa_CompanyClassification_load]
    ON [dbo].[lmt_mfsa_CompanyClassification_load]([Classification_ID] ASC) WITH (FILLFACTOR = 90);

