﻿CREATE TABLE [dbo].[cpi_CustomerType] (
    [CustomerTypeID]     INT            IDENTITY (1, 1) NOT NULL,
    [CustomerTypeEN]     VARCHAR (255)  COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Custo__Custo__29221CFB] DEFAULT ('0') NULL,
    [CustomerTypeNative] NVARCHAR (255) CONSTRAINT [DF__cpi_Custo__Custo__2A164134] DEFAULT (N'0') NULL,
    [selected_option]    VARCHAR (1)    COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Custo__selec__2B0A656D] DEFAULT ('0') NULL,
    CONSTRAINT [PK_cpi_CustomerType] PRIMARY KEY CLUSTERED ([CustomerTypeID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix386_1]
    ON [dbo].[cpi_CustomerType]([CustomerTypeID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix386_5]
    ON [dbo].[cpi_CustomerType]([selected_option] ASC) WITH (FILLFACTOR = 90);

