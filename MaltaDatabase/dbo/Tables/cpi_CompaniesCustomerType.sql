﻿CREATE TABLE [dbo].[cpi_CompaniesCustomerType] (
    [CustomerTypeID] INT CONSTRAINT [DF__cpi_Compa__Custo__5BAD9CC8] DEFAULT (0) NOT NULL,
    [CompanyCIID]    INT NOT NULL,
    CONSTRAINT [PK_cpi_CompaniesCustomerType] PRIMARY KEY CLUSTERED ([CustomerTypeID] ASC, [CompanyCIID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_CompaniesCustomerType_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID]),
    CONSTRAINT [cpi_CustomerType_cpi_CompaniesCustomerType_FK1] FOREIGN KEY ([CustomerTypeID]) REFERENCES [dbo].[cpi_CustomerType] ([CustomerTypeID])
);


GO
CREATE NONCLUSTERED INDEX [ix387_1]
    ON [dbo].[cpi_CompaniesCustomerType]([CustomerTypeID] ASC, [CompanyCIID] ASC) WITH (FILLFACTOR = 90);

