﻿CREATE TABLE [dbo].[FSI_Cashflow] (
    [AFS_id]                                          INT NOT NULL,
    [Operating_profit_before_working_capital_changes] INT NULL,
    [Cash_from_operations]                            INT NULL,
    [Net_cash_from_operating_activities]              INT NULL,
    [Cash_flow_from_investing_activities]             INT NULL,
    [Cash_flow_from_financing_activities]             INT NULL,
    [Increase_or_decrease_in_cash]                    INT NULL,
    CONSTRAINT [PK_FSI_Cashflow] PRIMARY KEY CLUSTERED ([AFS_id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FSI_Cashflow_FSI_GeneralInfo] FOREIGN KEY ([AFS_id]) REFERENCES [dbo].[FSI_GeneralInfo] ([AFS_id])
);

