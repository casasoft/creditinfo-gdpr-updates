﻿CREATE TABLE [dbo].[lmt_mfsa_CompanyCapital_load] (
    [Cap_ID]             INT            NULL,
    [Company_ID]         INT            NULL,
    [FLD_Class]          NVARCHAR (255) NULL,
    [Auth_Shr_Cap]       FLOAT (53)     NULL,
    [Nominal_value]      FLOAT (53)     NULL,
    [Issued_Shr_Capital] FLOAT (53)     NULL,
    [Percentage_Pd_Up]   FLOAT (53)     NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_mfsa_CompanyCapital_load]
    ON [dbo].[lmt_mfsa_CompanyCapital_load]([Cap_ID] ASC) WITH (FILLFACTOR = 90);

