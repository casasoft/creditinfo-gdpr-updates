﻿CREATE TABLE [dbo].[lmt_companyclassification_load] (
    [classificationid] INT            NOT NULL,
    [classification]   NVARCHAR (255) COLLATE Icelandic_CI_AS NOT NULL,
    PRIMARY KEY CLUSTERED ([classificationid] ASC) WITH (FILLFACTOR = 90)
);

