﻿CREATE TABLE [dbo].[FSI_Origin] (
    [Origin_id]          INT           IDENTITY (1, 1) NOT NULL,
    [Description_native] NVARCHAR (70) NULL,
    [Description_en]     VARCHAR (70)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_FSI_Origin] PRIMARY KEY CLUSTERED ([Origin_id] ASC) WITH (FILLFACTOR = 90)
);

