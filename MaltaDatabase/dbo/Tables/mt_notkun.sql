﻿CREATE TABLE [dbo].[mt_notkun] (
    [id]            INT          NOT NULL,
    [tr_date]       DATETIME     NULL,
    [ip_address]    VARCHAR (15) NULL,
    [user_name]     VARCHAR (20) NULL,
    [subject]       VARCHAR (20) NULL,
    [action]        VARCHAR (30) NULL,
    [amount]        INT          NULL,
    [userlevel]     INT          NULL,
    [tag_used]      VARCHAR (30) NULL,
    [ref_id]        INT          NULL,
    [billable]      INT          NULL,
    [cig_querytype] INT          NULL
);

