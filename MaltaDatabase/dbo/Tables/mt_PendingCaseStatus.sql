﻿CREATE TABLE [dbo].[mt_PendingCaseStatus] (
    [StatusID]          INT            IDENTITY (1, 1) NOT NULL,
    [StateNative]       VARCHAR (50)   NULL,
    [StateEN]           VARCHAR (50)   NULL,
    [DescriptionNative] VARCHAR (1024) NULL,
    [DescriptionEN]     VARCHAR (1024) NULL
);

