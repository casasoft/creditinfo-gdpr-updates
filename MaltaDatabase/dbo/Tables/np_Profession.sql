﻿CREATE TABLE [dbo].[np_Profession] (
    [ProfessionID]      INT           IDENTITY (1, 1) NOT NULL,
    [NameEN]            NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [NameNative]        NVARCHAR (50) NULL,
    [DescriptionEN]     NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative] NVARCHAR (50) NULL,
    CONSTRAINT [np_Profession_PK] PRIMARY KEY CLUSTERED ([ProfessionID] ASC) WITH (FILLFACTOR = 90)
);

