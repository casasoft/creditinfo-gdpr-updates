﻿CREATE TABLE [dbo].[lmt_mfsa_InvolvementsTypes_load] (
    [Involv_Type_ID] INT            NOT NULL,
    [Involvement]    NVARCHAR (255) NULL,
    [Ordering]       INT            NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_mfsa_InvolvementsTypes_load]
    ON [dbo].[lmt_mfsa_InvolvementsTypes_load]([Involv_Type_ID] ASC) WITH (FILLFACTOR = 90);

