﻿CREATE TABLE [dbo].[np_Individual_FullName] (
    [id]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [CreditInfoID] INT           NOT NULL,
    [Surname]      NVARCHAR (80) NULL,
    [OtherSurname] NVARCHAR (80) NULL,
    [Name]         NVARCHAR (80) NULL,
    [OtherName]    NVARCHAR (80) NULL,
    [MiddleNames]  NVARCHAR (80) NULL,
    [Inserted]     DATETIME      CONSTRAINT [DF_np_Individual_FullName_Inserted] DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME      NULL,
    CONSTRAINT [np_Individual_FullName1] PRIMARY KEY CLUSTERED ([id] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_np_Individual_FullName_np_Individual] FOREIGN KEY ([CreditInfoID]) REFERENCES [dbo].[np_Individual] ([CreditInfoID])
);

