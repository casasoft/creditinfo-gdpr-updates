﻿CREATE TABLE [dbo].[np_Status] (
    [StatusID]          INT             IDENTITY (1, 1) NOT NULL,
    [StateEN]           NVARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionEN]     NVARCHAR (1024) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [DescriptionNative] NVARCHAR (1024) NULL,
    [StateNative]       NVARCHAR (50)   NULL,
    CONSTRAINT [np_Status_PK] PRIMARY KEY CLUSTERED ([StatusID] ASC) WITH (FILLFACTOR = 90)
);

