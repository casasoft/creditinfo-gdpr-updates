﻿CREATE TABLE [dbo].[lmt_plaintiff] (
    [case_id]      INT            NOT NULL,
    [plaintiff]    NVARCHAR (255) NULL,
    [plaintiff_id] NVARCHAR (255) NULL,
    CONSTRAINT [PK_lmt_plaintiff] PRIMARY KEY CLUSTERED ([case_id] ASC) WITH (FILLFACTOR = 90)
);

