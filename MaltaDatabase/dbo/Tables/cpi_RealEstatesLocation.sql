﻿CREATE TABLE [dbo].[cpi_RealEstatesLocation] (
    [LocationID]        INT            IDENTITY (1, 1) NOT NULL,
    [DescriptionNative] NVARCHAR (100) NULL,
    [DescriptionEN]     VARCHAR (100)  COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    CONSTRAINT [PK_cpi_RealEstatesLocation] PRIMARY KEY CLUSTERED ([LocationID] ASC) WITH (FILLFACTOR = 90)
);

