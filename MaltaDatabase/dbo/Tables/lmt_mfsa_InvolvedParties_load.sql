﻿CREATE TABLE [dbo].[lmt_mfsa_InvolvedParties_load] (
    [Involved_Party_ID] INT            NULL,
    [IP_Surname]        NVARCHAR (255) NULL,
    [IP_Name]           NVARCHAR (255) NULL,
    [Title]             NVARCHAR (255) NULL,
    [IP_ID_No]          NVARCHAR (255) NULL,
    [IP_Passport_No]    NVARCHAR (255) NULL,
    [IP_Co_Name]        NVARCHAR (255) NULL,
    [IP_Co_Reg_No]      NVARCHAR (255) NULL,
    [Nationality_ID]    FLOAT (53)     NULL,
    [IP_Address_1]      NVARCHAR (255) NULL,
    [IP_Address_2]      NVARCHAR (255) NULL,
    [Locality_ID]       INT            NULL,
    [Postcode]          NVARCHAR (255) NULL,
    [Country_ID]        INT            NULL
);


GO
CREATE CLUSTERED INDEX [IX_lmt_mfsa_InvolvedParties_load]
    ON [dbo].[lmt_mfsa_InvolvedParties_load]([Involved_Party_ID] ASC) WITH (FILLFACTOR = 90);

