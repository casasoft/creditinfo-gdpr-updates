﻿CREATE TABLE [dbo].[ic_pantanir] (
    [id]               INT           NOT NULL,
    [numer]            CHAR (30)     NULL,
    [verdflokkur]      INT           NULL,
    [skraningartegund] INT           NULL,
    [skraningardags]   SMALLDATETIME NULL,
    [skiladags]        SMALLDATETIME NULL,
    [afgreidsludags]   SMALLDATETIME NULL,
    [kt_ft]            CHAR (10)     NULL,
    [nafn_ft]          VARCHAR (50)  NULL,
    [kt_tengilids]     CHAR (10)     NULL,
    [nafn_tengilids]   VARCHAR (50)  NULL,
    [postfang]         VARCHAR (255) NULL,
    [email]            VARCHAR (100) NULL,
    [sendingamati]     INT           NULL,
    [lt_username]      VARCHAR (40)  NULL,
    [notandi]          INT           NULL
);

