﻿CREATE TABLE [dbo].[lmt_mfsa_companyclassification_old] (
    [Classification_ID] INT            NOT NULL,
    [Classification]    NVARCHAR (255) NOT NULL,
    [Ordering]          INT            NULL
);


GO
CREATE CLUSTERED INDEX [ix_cdcompanyclassificatoni_classificationid]
    ON [dbo].[lmt_mfsa_companyclassification_old]([Classification_ID] ASC) WITH (FILLFACTOR = 90);

