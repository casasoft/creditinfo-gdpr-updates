﻿CREATE TABLE [dbo].[skstk_SubscriberStatus] (
    [ID]             INT            NOT NULL,
    [SubscriberID]   INT            NOT NULL,
    [AccessLevel]    INT            NOT NULL,
    [Site]           NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AI NULL,
    [LastErrorState] INT            NOT NULL,
    [CreateDate]     DATETIME       NOT NULL
);

