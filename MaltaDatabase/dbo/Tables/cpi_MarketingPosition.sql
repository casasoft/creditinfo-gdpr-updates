﻿CREATE TABLE [dbo].[cpi_MarketingPosition] (
    [CompanyCIID]       INT      CONSTRAINT [DF__cpi_Marke__Compa__40F9A68C] DEFAULT (0) NOT NULL,
    [RegDate]           DATETIME CONSTRAINT [DF__cpi_Marke__RegDa__41EDCAC5] DEFAULT (getdate()) NOT NULL,
    [DescriptionNative] NTEXT    CONSTRAINT [DF__cpi_Marke__Descr__42E1EEFE] DEFAULT (N'0') NULL,
    [DescriptionEN]     TEXT     COLLATE SQL_Latin1_General_CP1_CI_AI CONSTRAINT [DF__cpi_Marke__Descr__43D61337] DEFAULT ('0') NULL,
    CONSTRAINT [PK_cpi_MarketingPosition] PRIMARY KEY CLUSTERED ([CompanyCIID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [cpi_CompanyReport_cpi_MarketingPosition_FK1] FOREIGN KEY ([CompanyCIID]) REFERENCES [dbo].[cpi_CompanyReport] ([CompanyCIID])
);


GO
CREATE NONCLUSTERED INDEX [ix377_1]
    ON [dbo].[cpi_MarketingPosition]([CompanyCIID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [ix377_2]
    ON [dbo].[cpi_MarketingPosition]([RegDate] ASC) WITH (FILLFACTOR = 90);

