﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CrossBorder")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Creditinfo solutions s.r.o.")]
[assembly: AssemblyProduct("CrossBorder")]
[assembly: AssemblyCopyright("Copyright © Creditinfo solutions s.r.o. 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]





[assembly: WebResource("CrossBorder.css.stl.css", "text/css", PerformSubstitution = true)]

[assembly: WebResource("CrossBorder.img.btnSearch.gif", "image/gif")]
[assembly: WebResource("CrossBorder.img.closeWindow.png", "image/png")]
[assembly: WebResource("CrossBorder.img.done.png", "image/png")]
[assembly: WebResource("CrossBorder.img.fail.png", "image/png")]
[assembly: WebResource("CrossBorder.img.head-background.png", "image/png")]
[assembly: WebResource("CrossBorder.img.inProgress.png", "image/png")]
[assembly: WebResource("CrossBorder.img.new.png", "image/png")]
[assembly: WebResource("CrossBorder.img.newOnDemand.png", "image/png")]
[assembly: WebResource("CrossBorder.img.onDemandWaiting.png", "image/png")]
[assembly: WebResource("CrossBorder.img.refresh.png", "image/png")]
[assembly: WebResource("CrossBorder.img.selectedCommand.png", "image/png")]
[assembly: WebResource("CrossBorder.img.unprocessable.png", "image/png")]
[assembly: WebResource("CrossBorder.img.btn170.gif", "image/gif")]


[assembly: WebResource("CrossBorder.img.Mask.png", "image/png")]


[assembly: WebResource("CrossBorder.img.menu-background.png", "image/png")]

[assembly: WebResource("CrossBorder.img.menu-selected-left.png", "image/png")]

[assembly: WebResource("CrossBorder.img.menu-selected-right.png", "image/png")]


[assembly: WebResource("CrossBorder.img.gb.png", "image/png")]
[assembly: WebResource("CrossBorder.img.lt.png", "image/png")]
[assembly: WebResource("CrossBorder.img.pl.png", "image/png")]
[assembly: WebResource("CrossBorder.img.tr.png", "image/png")]







// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7ffa5a89-8750-44ea-90ba-2c4bb871552b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
