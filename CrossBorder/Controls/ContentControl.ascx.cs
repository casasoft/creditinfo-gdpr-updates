﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Proxy;
using System.Globalization;
using System.Threading;
using System.Net.Mime;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Enums;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp
{   
  
    public partial class ContentControl : System.Web.UI.UserControl
    {
        #region Private fields
        private CultureInfo _culture;
        private string _onAccount;
        private string _email;

        const string SubmitButton = "__PARAM__CommandType_SubmitButton";
        const string ReportRequestButton = "__PARAM__CommandType_ReportRequest";
        const string InternetionalReportRequest = "InternetionalReportRequest_CommandType_QueueTab";
        const string OnDemandReportRequest = "CommandType_OnDemandReportRequest";
        const string ExportReport = "exportreport_exportformat";
        const string ReportRequest = "ReportRequest";
        const string OtherParams = "OtherParams";
        const string CompanyNumber = "__PARAMS__OtherParams.OtherParam.CompanyNumber";

        public string WebServiceURL { get; set; }
        public string WebServiceUser { get; set; }
        public string WebServicePwd { get; set; }
        public string WebServiceSubscriber { get; set; }
        public string WebServiceSubscriberUnit { get; set; }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        string _content;
        string _fileName;
        bool _exportReport = false;
        #endregion

        #region Constructor
        public ContentControl()
        {

        }
        #endregion

        #region Properties
        public string OnAccount
        {
            get { return _onAccount; }
            set { _onAccount = value; }
        }

        public CultureInfo Culture
        {
            get { return _culture; }
            set { _culture = value; }
        }

    
       
        #endregion;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

     
        protected void Page_Load(object sender, EventArgs e)
        {
            AddStylesheet();
            this.SendRequest();
            if (!_exportReport)
            {
                this._content = this.TransformResponse();
                ParseControls(this._content);
            }
            else
            {
                string contentType = Helper.GetResponseContentType(this._fileName);
                Response.ContentType = contentType;
                Response.ContentEncoding = System.Text.Encoding.UTF8;
                Response.AddHeader("Content-Disposition",
                    string.Format("attachment; filename={0}", this._fileName.ToString()));

                Response.BinaryWrite(Helper.GetExtraReportOutput(this._content));
                Response.Flush();
                Response.End();
            }
        }

        private string TransformResponse()
        {
            WebServiceProxy proxy = new WebServiceProxy(this.WebServiceURL, this.WebServiceUser, this.WebServicePwd,
                this.WebServiceSubscriber, this.WebServiceSubscriberUnit, _onAccount, _culture, _email);
        
            String ret = proxy.TransformResponse(this._content);
            return ret.Replace("xmlns:asp=\"remove\"", String.Empty);
        }
     
        private void SendRequest()
        {
            WebServiceProxy proxy = new WebServiceProxy(this.WebServiceURL, this.WebServiceUser, this.WebServicePwd,
                this.WebServiceSubscriber, this.WebServiceSubscriberUnit, _onAccount, _culture, _email);
            ParamCollection param = GetParamCollection();
            this._content = proxy.Send(param);
        }


        #region Support functions
        private void AddStylesheet()
        {
            string fullName = "{0}.css.{1}";
            fullName = string.Format(fullName, (typeof(ContentControl)).Assembly.GetName().Name, "stl.css");
            string cssUrl = Page.ClientScript.GetWebResourceUrl(typeof(ContentControl), fullName);
            HtmlLink cssLink = new HtmlLink();
            cssLink.Href = cssUrl;
            cssLink.Attributes.Add("rel", "stylesheet");
            cssLink.Attributes.Add("type", "text/css");
            this.Page.Header.Controls.Add(cssLink);
        }

        private void ParseControls(string content)
        {
            Control ctrl = this.Page.ParseControl(content);
            Control[] dest = new Control[ctrl.Controls.Count];
            ctrl.Controls.CopyTo(dest, 0);
            this.AddImageUrl(dest);
            foreach(Control c in dest)
                this.Controls.Add(c);
        }
     

        private void AddImageUrl(Control[] ctrlCollection)
        {
            foreach (Control c in ctrlCollection)
            {
                ImageButton button = c as ImageButton;
                string fullName = "{0}.img.{1}";
                string imgUrl = string.Empty;

                if (button != null)
                    imgUrl = button.ImageUrl;
                HtmlInputSubmit input = c as HtmlInputSubmit;
                if (input != null && (input.ID == SubmitButton || input.ID == ReportRequestButton))
                    imgUrl = input.Attributes["ImageUrl"];

                fullName = string.Format(fullName, (typeof(ContentControl)).Assembly.GetName().Name, imgUrl);
                string webResourceURL = Page.ClientScript.GetWebResourceUrl(typeof(ContentControl), fullName);

                if (button != null)
                    button.ImageUrl = webResourceURL;

                if (input != null && (input.ID == SubmitButton || input.ID == ReportRequestButton))
                    input.Style.Add("background-image", webResourceURL);

                Control[] dest = new Control[c.Controls.Count];
                c.Controls.CopyTo(dest, 0);
                this.AddImageUrl(dest);
            }
        }

      
        private ParamCollection GetParamCollection()
        {
            Hashtable other = new Hashtable();

            string requestId = string.Empty;
            ParamCollection paramCollection = new ParamCollection();

            bool reportRequest = false;
            foreach (string key in Page.Request.Form.Keys)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    
                    string value = Page.Request.Form.Get(key);

                    if (key.IndexOf(ReportRequest) != -1)
                        reportRequest = true;

                    if (paramCollection.CheckPrefix(key))
                    {
                        if (key.IndexOf(OtherParams) != -1)
                        {
                            other.Add(key, value);
                            continue;
                        }

                        ParentItem param = paramCollection.Get(key);
                        param.Parse(key, value);
                        if (key.IndexOf(InternetionalReportRequest) != -1 || key.IndexOf(OnDemandReportRequest) != -1)
                        {
                            requestId = param.Value;
                        }
                        this.IsExport(key, value);
                    }
                    else
                    {
                        if (paramCollection.CheckExcludedPrefix(key))
                        {
                            ParentItem param = new ParentItem(false);
                            param.Key = key;
                            param.Value = value;
                            paramCollection.Add(param.Key, param);
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(requestId) || reportRequest)
            {
                if (other.Count > 0)
                {
                    if (!string.IsNullOrEmpty(requestId))
                    {
                        foreach (object key in other.Keys)
                        {
                            if (key.ToString().IndexOf(requestId) != -1)
                            {
                                ParentItem param = paramCollection.Get(key.ToString());
                                param.Parse(key.ToString(), other[key].ToString(), requestId);
                            }
                        }
                    }
                    else
                    {
                        foreach (object key in other.Keys)
                        {
                            ParentItem param = paramCollection.Get(key.ToString());
                            param.Parse(key.ToString(), other[key].ToString());
                        }
                    }
                }
            }
            return paramCollection;
        }

        private bool IsExport(string key, string value)
        {
            _exportReport = (key.ToLower().IndexOf(ExportReport) != -1);
            if (_exportReport)
            {
                _fileName = value;
            }
            return _exportReport;
        }
        #endregion
    }
}
