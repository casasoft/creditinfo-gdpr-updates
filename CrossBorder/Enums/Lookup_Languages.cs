﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Enums
{
    public enum Lookup_Languages : int
    {
        Icelandic = 1,
        Czech = 2,
        Poland = 3,
        Lithuanian = 4,
        Greek = 5,
        Slovak = 6,
        Romanian = 7,
        Maltese = 8,
        Dutch = 9,
        English = 10,
        Russian = 11,
        Turkish = 12,
    }
}
