﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Compilation;
using System.Web.Routing;
using System.Web.UI;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Routing
{
	/// <summary>
	/// Route handler that handles all files (mostly images) sent to url SecuredPages/Document/{DocumentId}
	/// </summary>
	public class DocumentRouteHandler : IRouteHandler
	{
      	public IHttpHandler GetHttpHandler(RequestContext requestContext)
		{
			//string filename = requestContext.RouteData.Values["DocumentId"] as string;
			foreach (KeyValuePair<string, object> token in requestContext.RouteData.Values)
			{
				requestContext.HttpContext.Items.Add(token.Key, token.Value);
			} 

            DocumentHandler handler = new DocumentHandler(); 

			//return (DocumentHandler)BuildManager.CreateInstanceFromVirtualPath("~/Routing/DocumentHandler.ashx", typeof(DocumentHandler));
            return handler;
		}
	}
}
