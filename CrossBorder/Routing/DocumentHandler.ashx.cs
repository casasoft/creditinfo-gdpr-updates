﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Routing
{
	/// <summary>
	/// Handler responsible for writing byte data images to response
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	public class DocumentHandler : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			if (!context.Items.Contains("DocumentId")) 
                return;

			string documentId = context.Items["DocumentId"].ToString();
			string cacheKey = "document/" + documentId;

			byte[] data = HttpContext.Current.Cache.Get(cacheKey) as byte[];
			if (data != null && data.Length != 0)
			{
				string extension = documentId.Substring(documentId.Length - 3);
				if (extension == "jpg" || extension == "gif" || extension == "png")
				{
					context.Response.ContentType = "image/" + extension;
					context.Response.AppendHeader("Content-Length", data.Length.ToString());
					context.Response.AppendHeader("Content-Disposition", "attachment;filename=" + documentId);
					context.Response.Cache.SetCacheability(HttpCacheability.Public);
					context.Response.BinaryWrite(data);
				}
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}