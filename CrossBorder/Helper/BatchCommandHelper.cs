﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Enums;
using System.Collections.Generic;
using System.Globalization;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp
{
    public class BatchCommandHelper
    {
        public static XmlDocument ConstructBatch(BatchCommandType type, string subscriber, string subscriberUnit, string onAccount, CultureInfo culture, string email)
        {

            switch (type)
            {
                case BatchCommandType.GetPage:
                    return ConstructBatch(
                        GetPageCommand(onAccount, culture, email), subscriber, subscriberUnit);
                case BatchCommandType.Transform:
                    return ConstructBatch(
                        GetTransformCommand(), subscriber, subscriberUnit);
            }
            return null;
        }

        private static XmlDocument ConstructBatch(String commands, string subscriber, string subscriberUnit)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.Append("<Batch xmlns=\"http://cb4.creditinfosolutions.com/BatchUploader/Batch\"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  xsi:schemaLocation=\"http://cb4.creditinfosolutions.com/BatchUploader/Batch file://C:/testing/FullBatchSchemaIR081007.xsd\">");
            sb.Append("<Header>");
            sb.Append(String.Format("<Identifier>{0}</Identifier>", Guid.NewGuid().ToString("N")));
            sb.Append(string.Format("<Subscriber>{0}</Subscriber>", subscriber));
            sb.Append(string.Format("<SubscriberUnit>{0}</SubscriberUnit>", subscriberUnit));
            sb.Append("<Description>Description01</Description>");
            sb.Append("</Header>");
            sb.Append("<Commands><Command identifier=\"identifier1\">");
            sb.Append(commands);
            sb.Append("</Command></Commands>");
            sb.Append("</Batch>");

            XmlDocument command = new XmlDocument();
            command.LoadXml(sb.ToString());
            return command;
        }

        private static string GetPageCommand(string onAccount, CultureInfo culture, string email)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.Body.Products.GetPage>");
            sb.Append(string.Format("<Reports.OnAccount>{0}</Reports.OnAccount>", onAccount));
            if (!string.IsNullOrEmpty(email))
                sb.Append(string.Format("<Common.Email>{0}</Common.Email>", email));
            sb.Append(string.Format("<Lookups.ResponseLcid>Languages.{0}</Lookups.ResponseLcid>", Helper.CultureToLanguageLookup(culture)));
            sb.Append(string.Format("<Common.IpAddress>{0}</Common.IpAddress>", Helper.GetIP4Address()));
            sb.Append("</Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.Body.Products.GetPage>");
            return sb.ToString();
        }


        private static string GetTransformCommand()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.Body.Products.GetTransformation>");
            sb.Append("<Request.TransformInput>");
            sb.Append("CrossBorder.xslt");
            sb.Append("</Request.TransformInput>");
            sb.Append("</Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.Body.Products.GetTransformation>");
            return sb.ToString();
        }



        internal static XmlDocument ConstructBatch(System.Collections.Specialized.NameValueCollection param, string webServiceSubscriber, string webServiceSubscriberUnit)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.Body.Products.GetPage>");

            foreach (string key in param.Keys)
            {
                string tag = "<{0}>{1}</{0}>";
                tag = string.Format(tag, key, param.Get(key));
                sb.Append(tag);
            }
            sb.Append(string.Format("<Common.IpAddress>{0}</Common.IpAddress>", Helper.GetIP4Address()));
            sb.Append("</Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.Body.Products.GetPage>");
            return ConstructBatch(sb.ToString(), webServiceSubscriber, webServiceSubscriberUnit);
        }



        internal static XmlDocument ConstructBatch(ParamCollection param, string webServiceSubscriber, string webServiceSubscriberUnit, string onAccount, CultureInfo culture, string email)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.Body.Products.GetPage>");
            sb.Append(string.Format("<Reports.OnAccount>{0}</Reports.OnAccount>", onAccount));
            if (!string.IsNullOrEmpty(email))
                sb.Append(string.Format("<Common.Email>{0}</Common.Email>", email));
            sb.Append(string.Format("<Lookups.ResponseLcid>Languages.{0}</Lookups.ResponseLcid>", Helper.CultureToLanguageLookup(culture)));

            string tag = "<{0}>{1}</{0}>";
            foreach (ParentItem parent in param.Collection)
            {
                if (parent.Collection.Count == 0)
                {
                    if (!string.IsNullOrEmpty(parent.Value))
                        sb.Append(string.Format(tag, parent.Key, parent.Value));
                }
                else
                    sb.Append(parent.GetXml());
            }
            sb.Append(string.Format("<Common.IpAddress>{0}</Common.IpAddress>", Helper.GetIP4Address()));
            sb.Append("</Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.Body.Products.GetPage>");
            return ConstructBatch(sb.ToString(), webServiceSubscriber, webServiceSubscriberUnit);
        }
    }
}
