﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Xml.XPath;
using System.Text;
using Cis.CB4.Tools.ServiceClient;
using Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp.Enums;
using System.Globalization;
using System.Web.Caching;
using ICSharpCode.SharpZipLib;
using ICSharpCode.SharpZipLib.Zip;
using System.Text.RegularExpressions;
using System.Net;

namespace Cis.CB4.Projects.DE.CreditinfoSchufa.WebWrap.WebApp
{
    public class Helper
    {
        static string _xslt = string.Empty;

        public static string XSLT
        {
            get { return _xslt; }
            set { _xslt = value; }
        }

        public static Lookup_Languages CultureToLanguageLookup(CultureInfo culture)
        {
            switch (culture.LCID)
            {
                case 1039: return Lookup_Languages.Icelandic;
                case 1045: return Lookup_Languages.Poland;
                case 1082: return Lookup_Languages.Maltese;
                case 1051: return Lookup_Languages.Slovak;
                case 1029: return Lookup_Languages.Czech;
                case 1032: return Lookup_Languages.Greek;
                case 1063: return Lookup_Languages.Lithuanian;
                case 1048: return Lookup_Languages.Romanian;
                case 1049: return Lookup_Languages.Russian;
                case 1043: return Lookup_Languages.Dutch;
                case 1055: return Lookup_Languages.Turkish;
                case 6153: return Lookup_Languages.English;
                case 2057: return Lookup_Languages.English;
                case 1033: return Lookup_Languages.English;
                default: return Lookup_Languages.English;
            }
        }

        private const string BatchNS = "http://cb4.creditinfosolutions.com/BatchUploader/Batch";

        public static byte[] GetExtraReportOutput(string response)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(response);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(new NameTable());
            nsmgr.AddNamespace("ns", BatchNS);

            XmlNode node = xDoc.SelectSingleNode("//ns:Common.ExtraReportOutput", nsmgr);
            if (node != null)
            {
                byte[] buffer = Convert.FromBase64String(node.InnerText);
                return buffer;
            }
            throw new Exception("");
        }


        private static string GenerateHtmlReport(Cache cache, string inputBase64String)
        {
            Regex pattern =
                new Regex(
                    @"^reportFile-([0-9]{4})([0-1][0-9])([0-3][0-9])([0-1][0-9]|[2][0-3])([0-5][0-9])([0-5][0-9])-report-([a-z][a-z]-[A-Z][A-Z])\.xml$",
                    RegexOptions.IgnoreCase);
            byte[] data = new byte[] { };

            data = Convert.FromBase64String(inputBase64String);
            string report = String.Empty;
            var cssImportBuilder = new StringBuilder();
            MemoryStream ms = new MemoryStream(data);
            using (ZipInputStream s = new ZipInputStream(ms))
            {

                ZipEntry entry;
                while ((entry = s.GetNextEntry()) != null)
                {
                    if (pattern.IsMatch(entry.Name))
                    {
                        byte[] buffer = new byte[entry.Size];
                        s.Read(buffer, 0, (int)entry.Size);
                        using (MemoryStream target = new MemoryStream(buffer))
                        {
                            //  Stream xmlStream = new S//Zip.GetFileData(filename);
                            XmlReader reader = XmlReader.Create(target);
                            reader.MoveToContent();
                            while (reader.Read())
                            {
                                if (reader.NodeType == XmlNodeType.Element)
                                {
                                    if (reader.IsEmptyElement) continue;
                                    if (!reader.HasAttributes) continue;
                                    if (reader.LocalName.ToLowerInvariant() == "part")
                                    {
                                        if (!reader.MoveToAttribute("mimetype"))
                                        {
                                            continue;
                                        }
                                        string mimeType = reader.Value;
                                        if (reader.MoveToAttribute("id"))
                                        {
                                            string cacheKey = "document/" + reader.Value.ToLower();

                                            reader.MoveToContent();
                                            string extData = reader.ReadElementContentAsString();

                                            if (mimeType.ToLowerInvariant().Contains("text"))
                                            {
                                                if (mimeType == "text/css")
                                                {
                                                    //create the css style
                                                    cssImportBuilder.AppendLine(@"<style type=""text/css"">");
                                                    cssImportBuilder.Append(extData);
                                                    cssImportBuilder.AppendLine(@"</style>");
                                                }
                                                else
                                                {
                                                    cache.Add(cacheKey, extData, null, DateTime.Now.AddMinutes(1),
                                                              Cache.NoSlidingExpiration,
                                                              CacheItemPriority.Normal,
                                                              null);
                                                }
                                            }
                                            else
                                            {
                                                cache.Add(cacheKey, Convert.FromBase64String(extData), null,
                                                          DateTime.Now.AddMinutes(1),
                                                          Cache.NoSlidingExpiration,
                                                          CacheItemPriority.Normal,
                                                          null);
                                            }
                                            continue;
                                        }
                                        if (mimeType == "text/html")
                                        {
                                            reader.MoveToContent();
                                            report = reader.ReadElementContentAsString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return cssImportBuilder.ToString() + report;
        }


        public static string Transform(string content, string xslt)
        {

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(content);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(new NameTable());
            nsmgr.AddNamespace("ns", BatchNS);

            XmlNode node = xDoc.SelectSingleNode("//ns:Common.ExtraReportOutput", nsmgr);
            string report = string.Empty;
            if (node != null)
            {
                report = GenerateHtmlReport(HttpContext.Current.Cache, node.InnerText);
                node.InnerText = report;
            }

            XslCompiledTransform xslTrans = new XslCompiledTransform();

            XmlParserContext context = new XmlParserContext(null, null, null, XmlSpace.None);
            XmlTextReader xmltr = new XmlTextReader(xslt, XmlNodeType.Document, context);
            xslTrans.Load(xmltr);

            MemoryStream ms;
            if (node != null)
                ms = new MemoryStream(Encoding.UTF8.GetBytes(xDoc.InnerXml));
            else
                ms = new MemoryStream(Encoding.UTF8.GetBytes(content));

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            ms.Position = 0;

            using (XmlReader reader = XmlReader.Create(ms, settings))
            {
                reader.Read();
                reader.MoveToContent();
                XPathDocument xdoc = new XPathDocument(reader);

                using (ms = new MemoryStream())
                {
                    XmlWriterSettings writerSettings = new XmlWriterSettings();
                    writerSettings.ConformanceLevel = ConformanceLevel.Auto;

                    using (XmlWriter writer = XmlWriter.Create(ms, writerSettings))
                        xslTrans.Transform(xdoc, new XsltArgumentList(), writer);

                    return Encoding.UTF8.GetString(ms.ToArray());
                }
            }
        }

        public static string GetResponseContentType(string ext)
        {
            int pos = ext.LastIndexOf('.');
            if (pos >= 0)
            {
                ext = ext.Substring(pos + 1);
            }
            switch (ext.Trim().ToLowerInvariant())
            {
                case "html": return "text/html; charset=utf-8";
                case "htm": return "text/html; charset=utf-8";
                case "bmp": return "image/bmp";
                case "emf": return "image/emf";
                case "exif": return "image/exif";
                case "gif": return "image/gif";
                case "icon": return "image/icon";
                case "jpeg": return "image/jpeg";
                case "jpg": return "image/jpg";
                case "png": return "image/png";
                case "tiff": return "image/png";
                case "tif": return "image/png";
                case "wmf": return "image/wmf";
                case "js": return "application/x-javascript";
                case "doc": return "application/vnd.ms-word";
                case "docx": return "application/vnd.ms-word";
                case "xml": return "text/xml; charset-UTF-8;";
                case "pdf": return "application/pdf";
                case "mpeg": return "video/mpeg";
                case "txt": return "text/plain";
                case "csv": return "text/csv; charset-UTF-8;";
                case "xls": return "application/vnd.ms-excel";
                case "xlsx": return "application/x-msexcel";
                case "zip": return "application/zip";
                case "rtf": return "application/rtf";
                case "mht": return "message/rfc822";
                default:
                    return String.Empty;
            }
        }

        public static string GetIP4Address()
        {
            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            if (IP4Address != String.Empty)
            {
                return IP4Address;
            }

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            if (IP4Address == string.Empty)
            {
                IP4Address = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return IP4Address;
        }
    }
}
