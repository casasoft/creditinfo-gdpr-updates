﻿using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.IO;

namespace PackageBuilder {
	public class TeamBuild : Task {
	    [Required]
	    public string Project { get; set; }

	    [Required]
	    public string TFS { get; set; }

	    [Required]
	    public string Root { get; set; }

	    [Required]
	    public string Packages { get; set; }

	    [Required]
	    public string Install { get; set; }

	    [Required]
	    public string Tagtype { get; set; }

	    [Required]
	    public string Configs { get; set; }

        [Required]
        public string XmlDir { get; set; }
        
        public string LogPath { get; set; }

	    public override bool Execute() {

			// if (!Directory.Exists(LogPath)) {Directory.CreateDirectory(LogPath);}
           // StreamWriter loger = new StreamWriter(string.Format("{0}/{1}.{2}", LogPath, Project, "log")) { AutoFlush = true };

            String tempFile = Path.Combine(Path.GetTempPath(), Path.GetTempFileName());
            StreamWriter loger = new StreamWriter(tempFile);

            loger.AutoFlush = true;

            Console.SetOut(loger);

            //File.AppendText(string.Format("{0}/{1}.{2}", Root, Project, "log"));
            loger.WriteLine("---------------------------------------------------------------------");
            try 
            {
                loger.WriteLine("(" + DateTime.Now + ")");
                loger.WriteLine("PackageBuilder, project: {0}, tagtype={1}, tfs={2}, root={3}, packages={4}, install={5}, configs={6}", Project, Tagtype, TFS, Root, Packages, Install, Configs);
                BuildManager bm = new BuildManager(Project, Tagtype, TFS, Root, Install, Packages, Configs, XmlDir); 
                bm.Build();
                loger.WriteLine("Done ("+DateTime.Now+")");
                loger.Close();
                this.Log.LogMessagesFromFile(tempFile, MessageImportance.Normal);
                return true;
            } 
            catch (Exception exc) 
            {
                loger.WriteLine("Deploy failed! (" + DateTime.Now + ")");
                loger.WriteLine(exc.ToString());
                loger.Close();
                this.Log.LogMessagesFromFile(tempFile, MessageImportance.Normal);
                this.Log.LogErrorFromException(exc, true);
                return false;
            }
        }

	    public TeamBuild() { LogPath = String.Empty; }
	}
}
