﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PackageBuilder
{
    class Program
    {

        static string project = "MT.CIG";
        static string tagType = @"trunk";
        static string tfs = "http://atlas:8080/TFS/CIS";
        static string root = @"C:\MT3";
        static string install;
        static string packages = @"C:\MT3\PACKAGEdIR";
        static string configs;
        static string xmlDir = @"C:\src\CreditBureau_v3\trunk\mt.cig\PackageBuilder\";


        public static void Main()
        {
            BuildManager build = new BuildManager(project, tagType, tfs, root, install, packages, configs, xmlDir);
            build.Build();
        }
    }
}
