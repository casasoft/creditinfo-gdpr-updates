using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;

namespace PackageBuilder
{
    internal class TFSGet
    {
        private readonly string tfsName;
        private readonly  Dictionary<string,string> resourceList = new Dictionary<string,string>();

        public TFSGet(String tfsName)
        {
            this.tfsName = tfsName;
        }

        public bool AddResource(string tfsPath, string getDirectory, bool withDelete)
        {
            if (resourceList.ContainsKey(tfsPath)) return false;
            IOHelper.TryCreateDirectory(getDirectory);
            if (withDelete) {
                this.CleanDirectory(getDirectory);
            }
            resourceList.Add(tfsPath, getDirectory);
            return true;
        }

		public void GetSources(String _tfsName, String tfsPath, string baseDirectory)
		{
			string workSpaceName = "PackageBuilderWorkspace";
			this.CleanDirectory(baseDirectory);
			Console.WriteLine("Connect TSF");

			TeamFoundationServer tfs = new TeamFoundationServer(_tfsName);
			VersionControlServer versionControl = (VersionControlServer)tfs.GetService(typeof(VersionControlServer));
			versionControl.NonFatalError += this.OnNonFatalError;

			Workspace workspace = null;
			try
			{
				workspace = versionControl.GetWorkspace(workSpaceName, versionControl.AuthenticatedUser);
				versionControl.DeleteWorkspace(workSpaceName, versionControl.AuthenticatedUser);
			}
			catch (WorkspaceNotFoundException)
			{
			}
			workspace = versionControl.CreateWorkspace(workSpaceName, versionControl.AuthenticatedUser);

			Console.WriteLine("Read Workspace Files");
			workspace.Map(tfsPath, baseDirectory);
			workspace.Get();
		}

        public void GetSources()
        {
            string workSpaceName = "CB3PackagerWSpace";

            Console.WriteLine("Connect TSF");

            TeamFoundationServer tfs = new TeamFoundationServer(tfsName);

            VersionControlServer versionControl = (VersionControlServer)tfs.GetService(typeof(VersionControlServer));
            versionControl.NonFatalError += this.OnNonFatalError;

            Workspace workspace = null;
            try
            {
                workspace = versionControl.GetWorkspace(workSpaceName, versionControl.AuthenticatedUser);
                versionControl.DeleteWorkspace(workSpaceName, versionControl.AuthenticatedUser);
            }
            catch (WorkspaceNotFoundException)
            {
            }
            workspace = versionControl.CreateWorkspace(workSpaceName, versionControl.AuthenticatedUser);

            Console.WriteLine("Read Workspace Files");
            foreach (KeyValuePair<string, string> pair in resourceList)
            {
                workspace.Map(pair.Key, pair.Value); //tfsPath, getDirectory);     
            }
            workspace.Get();
            //GetRequest request = new GetRequest(localPath, RecursionType.Full, VersionSpec.Latest);
            //GetStatus status = this.workSpace.Get(request, GetOptions.Overwrite & GetOptions.GetAll);
        }

        private void CleanDirectory(string path)
        {
            Console.WriteLine("Cleaning {0}", path);
            string[] files = Directory.GetFiles(path);
            foreach (string fileName in files)
            {
                File.SetAttributes(fileName, FileAttributes.Normal);
                File.Delete(fileName);
            }
            string[] dirs = Directory.GetDirectories(path);
            foreach (string dirName in dirs)
            {
                //System.Security.AccessControl.DirectorySecurity sec = Directory.GetAccessControl(path);
                //Directory.SetAccessControl(path, System.Security.AccessControl.DirectorySecurity
                this.CleanDirectory(dirName);   
                try { Directory.Delete(dirName); } catch {} 
            }
        }

        void OnNonFatalError(Object sender, ExceptionEventArgs e)
        {
            if (e.Exception != null)
            {
                Console.Error.WriteLine("Non-fatal exception: " + e.Exception.Message);
            }
            else
            {
                Console.Error.WriteLine("Non-fatal failure: " + e.Failure.Message);
            }
        }

		
	}
}
