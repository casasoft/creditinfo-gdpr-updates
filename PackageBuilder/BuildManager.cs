using System;
using System.Diagnostics;
using System.IO;
using System.Xml;

namespace PackageBuilder
{
    internal class BuildManager
    {
        private readonly String tfsName = "http://atlas:8080";
        private readonly string rootDirectory;
        private readonly string packageDirectory;
        private readonly string installDirectory;
        private readonly string configsDirectory;
        private const string msbuild35 = @"C:\WINDOWS\Microsoft.NET\Framework\v3.5\MSBuild.exe";
        private readonly string xmlDirectory = @"";
        private const string winzip = @"C:\Program Files\7-Zip\7z.exe";
        private const string buildmode = "Release";
        readonly string project;
        private string tfsPath = "$/CreditBureau_v3/trunk/mt.cig";
        readonly string tagtype = "venus";

        private string baseDirectory
        {
            get
            {
                return Path.Combine(rootDirectory, "packagebuilder");
            }
        }

        private string publishDirectory
        {
            get
            {
                return Path.Combine(rootDirectory, "publish");
            }
        }

        public BuildManager(string project, string tagtype, string tfs, string root, string install, string packages, string configs, string xmldir)
        {
            this.tfsName = tfs;
            this.project = project;
            this.tagtype = tagtype;
            this.rootDirectory = root;
            this.installDirectory = install;
            this.packageDirectory = packages;
            this.configsDirectory = configs;
            this.xmlDirectory = xmldir;

            tfsPath = string.Format("$/CreditBureau_v3/{0}/{1}", tagtype, "mt.cig");
        }

        public void Build()
        {
            Console.WriteLine(string.Format(@"{0:HH:mm} Preparing ...", DateTime.Now));
            IOHelper.DeleteAll(new DirectoryInfo(this.rootDirectory), "*");
            IOHelper.TryCreateDirectory(this.rootDirectory);
            IOHelper.TryCreateDirectory(this.baseDirectory);
            IOHelper.TryCreateDirectory(this.packageDirectory);

            Console.WriteLine(string.Format(@"{0:HH:mm} Getting sources ...", DateTime.Now));
            this.GetSources();

            Console.WriteLine(string.Format(@"{0:HH:mm} Building solution ...", DateTime.Now));
            this.BuildSolution();

            Console.WriteLine(string.Format(@"{0:HH:mm} Creating zip ...", DateTime.Now));
            var zipName =
                String.Format(@"{0}-{1}-({2}).zip", project, DateTime.Now.ToString("yyyyMMdd-HHmmss"), tagtype.Replace(@"\", "-"));

            BuildHelper.FastZipDirectory(publishDirectory, Path.Combine(packageDirectory, zipName), true, false);

            Console.WriteLine(string.Format(@"{0:HH:mm} Installing zip ...", DateTime.Now));
            // this.Install(zipName);
            this.CopyAll(publishDirectory, installDirectory, "*.*", false);

            Console.WriteLine(string.Format(@"{0:HH:mm} Cleaning ...", DateTime.Now));
            IOHelper.DeleteAll(new DirectoryInfo(rootDirectory), "*");

            Console.WriteLine(string.Format(@"{0:HH:mm} Done", DateTime.Now));
        }

        private void GetSources()
        {
            TFSGet tfs = new TFSGet(tfsName);
            tfs.GetSources(tfsName, tfsPath, baseDirectory);
        }

        private void PublaishMTCIG(string docname, FileSystemInfo source, FileSystemInfo target)
        {
            var doc = new XmlDocument();
            doc.Load(Path.Combine(xmlDirectory, docname));
            if (doc.DocumentElement == null)
            {
                return;
            }
            foreach (XmlNode n in doc.DocumentElement.ChildNodes)
            {
                var sourcePath = Path.Combine(source.FullName, n.InnerText);
                var destPath = Path.Combine(target.FullName, n.InnerText);
                if (!Directory.Exists(Path.GetDirectoryName(destPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(destPath));
                }
                CopyFile(sourcePath, destPath);
            }
        }
        /// <summary>
        /// Publish files of webservice by list of files. 
        /// List of files is define at xml file.
        /// </summary>
        /// <param name="docname"></param>
        /// <param name="source"></param>
        /// <param name="target"></param>
        private void publishMTCIGWebservice(string docname, FileSystemInfo source, FileSystemInfo target)
        {
            var doc = new XmlDocument();
            doc.Load(Path.Combine(xmlDirectory, docname));
            if (doc.DocumentElement == null)
            {
                return;
            }
            foreach (XmlNode n in doc.DocumentElement.ChildNodes)
            {
                var sourcePath = Path.Combine(source.FullName, n.InnerText);
                var destPath = Path.Combine(target.FullName, n.InnerText);
                if (!Directory.Exists(Path.GetDirectoryName(destPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(destPath));
                }
                CopyFile(sourcePath, destPath);
            }
        }

        private void BuildSolution()
        {
            MSBuild35(Path.Combine(baseDirectory, @"mt.cig.sln"), "REBUILD");
            if (!Directory.Exists(Path.Combine(publishDirectory, @"Preprocessor")))
            {
                Directory.CreateDirectory(Path.Combine(publishDirectory, @"Preprocessor"));
            }
            if (!Directory.Exists(Path.Combine(publishDirectory, @"cb3")))
            {
                Directory.CreateDirectory(Path.Combine(publishDirectory, @"cb3"));
            }

            if (!Directory.Exists(Path.Combine(publishDirectory, @"MT.CIG.FullName.Parser")))
            {
                Directory.CreateDirectory(Path.Combine(publishDirectory, @"MT.CIG.FullName.Parser"));
            }
         
          
            #region WebService
            //---------------------------------------------------------
            //Create Directory if don't exist.
            if (!Directory.Exists(Path.Combine(publishDirectory, @"cb3\\webservice")))
            {
                Directory.CreateDirectory(Path.Combine(publishDirectory, @"cb3\\webservice"));
            }
            //Publish files by xml definition.
            this.publishMTCIGWebservice(@"cb3_webservice.xml", new DirectoryInfo(Path.Combine(baseDirectory, @"WebService")), new DirectoryInfo(Path.Combine(publishDirectory, @"cb3/webservice")));
            //Create sample of web config.
           // CopyFile(Path.Combine(Path.Combine(configsDirectory, @"cb3\\webservice"), "Web.config"),
           //     Path.Combine(Path.Combine(publishDirectory, @"cb3\\webservice"), "Web.config.sample"));

            MoveFile(Path.Combine(Path.Combine(publishDirectory, @"cb3\\webservice"), "Web.config"),
             Path.Combine(Path.Combine(publishDirectory, @"cb3\\webservice"), "Web.config.sample"));

            //Publish latest version of local config.
            CopyFile(Path.Combine(Path.Combine(configsDirectory, @"cb3\\webservice"), "cig.cfg.local.xml"),
                Path.Combine(Path.Combine(publishDirectory, @"cb3\\webservice"), "cig.cfg.local.xml.latest"));
            //---------------------------------------------------------
            #endregion

            #region BackOffice
            //---------------------------------------------------------
            //Create Directory if don't exist.
            if (!Directory.Exists(Path.Combine(publishDirectory, @"cb3\\backoffice")))
            {
                Directory.CreateDirectory(Path.Combine(publishDirectory, @"cb3\\backoffice"));
            }
            //Publish files by xml definition.
            PublaishMTCIG(@"cb3.xml", new DirectoryInfo(Path.Combine(baseDirectory, @"cb3")), new DirectoryInfo(Path.Combine(publishDirectory, @"cb3/backoffice")));
            
            MoveFile(Path.Combine(Path.Combine(publishDirectory, @"cb3\\backoffice"), "Web.config"),
                Path.Combine(Path.Combine(publishDirectory, @"cb3\\backoffice"), "Web.config.sample"));            
            //Publish latest version of local config.
            CopyFile(Path.Combine(Path.Combine(configsDirectory, @"cb3\\backoffice"), "cig.cfg.local.xml"),
                Path.Combine(Path.Combine(publishDirectory, @"cb3\\backoffice"), "cig.cfg.local.xml.latest"));
            //---------------------------------------------------------
            #endregion

            #region FrontOffice
            //---------------------------------------------------------
            //Create Directory if don't exist.
            if (!Directory.Exists(Path.Combine(publishDirectory, @"cb3\\frontoffice")))
            {
                Directory.CreateDirectory(Path.Combine(publishDirectory, @"cb3\\frontoffice"));
            }
            //Create sample of web config.
            PublaishMTCIG(@"cb3.xml", new DirectoryInfo(Path.Combine(baseDirectory, @"cb3")), new DirectoryInfo(Path.Combine(publishDirectory, @"cb3/frontoffice")));

            MoveFile(Path.Combine(Path.Combine(publishDirectory, @"cb3\\frontoffice"), "Web.config"),
                Path.Combine(Path.Combine(publishDirectory, @"cb3\\frontoffice"), "Web.config.sample"));
            //Publish latest version of local config.
            CopyFile(Path.Combine(Path.Combine(configsDirectory, @"cb3\\frontoffice"), "cig.cfg.local.xml"),
                Path.Combine(Path.Combine(publishDirectory, @"cb3\\frontoffice"), "cig.cfg.local.xml.latest"));
            #endregion


            #region FullIndividual parser


            if (Directory.Exists(Path.Combine(baseDirectory, @"MT.CIG.FullName.Parser\\bin\\Debug")))
            {
                PublaishMTCIG(@"MT.CIG.FullName.Parser.xml", new DirectoryInfo(Path.Combine(baseDirectory, @"MT.CIG.FullName.Parser\\bin\\Debug")), new DirectoryInfo(Path.Combine(publishDirectory, @"cb3/MT.CIG.FullName.Parser")));
            }
            else if (Directory.Exists(Path.Combine(baseDirectory, @"MT.CIG.FullName.Parser\\bin\\Release")))
            {
                PublaishMTCIG(@"MT.CIG.FullName.Parser.xml", new DirectoryInfo(Path.Combine(baseDirectory, @"MT.CIG.FullName.Parser\\bin\\Release")), new DirectoryInfo(Path.Combine(publishDirectory, @"cb3/MT.CIG.FullName.Parser")));
            }

            if (Directory.Exists(Path.Combine(baseDirectory, @"cb3/MT.CIG.FullName.Parser\\bin\\Debug")))
            {
                MoveFile(Path.Combine(Path.Combine(baseDirectory, @"cb3\MT.CIG.FullName.Parser\\bin\\Debug"), "MT.CIG.FullName.Parser.exe.config"),
                    Path.Combine(Path.Combine(publishDirectory, @"cb3\MT.CIG.FullName.Parser"), "MT.CIG.FullName.Parser.exe.sample"));
            }
            else if (Directory.Exists(Path.Combine(baseDirectory, @"cb3\MT.CIG.FullName.Parser\\bin\\Release")))
            {
                MoveFile(Path.Combine(Path.Combine(baseDirectory, @"cb3\MT.CIG.FullName.Parser\\bin\\Release"), "MT.CIG.FullName.Parser.exe.config"),
                    Path.Combine(Path.Combine(publishDirectory, @"cb3\MT.CIG.FullName.Parser"), "MT.CIG.FullName.Parser.exe.sample"));
            }

            CopyFile(Path.Combine(Path.Combine(configsDirectory, @"cb3\MT.CIG.FullName.Parser"), "MT.CIG.FullName.Parser.exe.config"),
             Path.Combine(Path.Combine(publishDirectory, @"cb3\MT.CIG.FullName.Parser"), "MT.CIG.FullName.Parser.exe.config"));



            MoveFile(Path.Combine(Path.Combine(publishDirectory, @"cb3\MT.CIG.FullName.Parser"), "MT.CIG.FullName.Parser.exe.config"),
             Path.Combine(Path.Combine(publishDirectory, @"cb3\MT.CIG.FullName.Parser"), "MT.CIG.FullName.Parser.exe.sample"));

            #endregion
            #region Preprocessor


            if (Directory.Exists(Path.Combine(baseDirectory, @"Preprocessor\\Preprocessor\\bin\\Debug")))
            {
                PublaishMTCIG(@"preprocessor.xml", new DirectoryInfo(Path.Combine(baseDirectory, @"Preprocessor\\Preprocessor\\bin\\Debug")), new DirectoryInfo(Path.Combine(publishDirectory, @"Preprocessor")));
            }
            else if (Directory.Exists(Path.Combine(baseDirectory, @"Preprocessor\\Preprocessor\\bin\\Release")))
            {
                PublaishMTCIG(@"preprocessor.xml", new DirectoryInfo(Path.Combine(baseDirectory, @"Preprocessor\\Preprocessor\\bin\\Release")), new DirectoryInfo(Path.Combine(publishDirectory, @"Preprocessor")));
            }

            if (Directory.Exists(Path.Combine(baseDirectory, @"Preprocessor\\Preprocessor\\bin\\Debug")))
            {
                MoveFile(Path.Combine(Path.Combine(baseDirectory, @"Preprocessor\\Preprocessor\\bin\\Debug"), "CIS.Web.Preprocessor.exe.config"),
                    Path.Combine(Path.Combine(publishDirectory, @"Preprocessor"), "CIS.Web.Preprocessor.exe.config.sample"));
            }
            else if (Directory.Exists(Path.Combine(baseDirectory, @"Preprocessor\\Preprocessor\\bin\\Release")))
            {
                MoveFile(Path.Combine(Path.Combine(baseDirectory, @"Preprocessor\\Preprocessor\\bin\\Release"), "CIS.Web.Preprocessor.exe.config"),
                    Path.Combine(Path.Combine(publishDirectory, @"Preprocessor"), "CIS.Web.Preprocessor.exe.config.sample"));
            }
            CopyFile(Path.Combine(Path.Combine(configsDirectory, @"Preprocessor"), "CIS.Web.Preprocessor.exe.config"),
                Path.Combine(Path.Combine(publishDirectory, @"Preprocessor"), "CIS.Web.Preprocessor.exe.config"));



            MoveFile(Path.Combine(Path.Combine(publishDirectory, @"Preprocessor"), "CIS.Web.Preprocessor.exe.config"),
             Path.Combine(Path.Combine(publishDirectory, @"Preprocessor"), "CIS.Web.Preprocessor.exe.config.sample"));

#endregion

        }

        private void MoveFile(string srcname, string dstname)
        {
            if (File.Exists(Path.Combine(this.rootDirectory, srcname)) == false) return;

            IOHelper.DeleteForMoveFile(Path.Combine(rootDirectory, dstname));
            File.Move(Path.Combine(rootDirectory, srcname), Path.Combine(rootDirectory, dstname));
        }

        public void CopyFile(string srcname, string dstname)
        {
            if (File.Exists(Path.Combine(this.rootDirectory, srcname)) == false) return;

            IOHelper.DeleteForMoveFile(Path.Combine(this.rootDirectory, dstname));
            File.Copy(Path.Combine(this.rootDirectory, srcname), Path.Combine(this.rootDirectory, dstname), true);
        }

        public void CopyAll(string source, string target, string searchPattern, bool fixReadOnly)
        {
            IOHelper.CopyAll(new DirectoryInfo(Path.Combine(this.rootDirectory, source)),
                    new DirectoryInfo(Path.Combine(this.rootDirectory, target)), searchPattern, fixReadOnly);
        }

        public void DeleteAll(string source, string searchPattern)
        {
            IOHelper.DeleteAll(new DirectoryInfo(Path.Combine(this.rootDirectory, source)), searchPattern);
        }

        private void MSBuild35(string solution, string target)
        {
            MSBuild35(solution, target, "");
        }

        private void MSBuild35(string solution, string target, string opts)
        {
            BuildHelper.ExecCommand(msbuild35, this.rootDirectory, string.Format("\"{0}\" /p:RunCodeAnalysis=false;Configuration={1} /t:{2} {3}", solution, buildmode, target, opts), true);
        }

        private void Install(string zipName)
        {
            if (String.IsNullOrEmpty(installDirectory)) return;
            IOHelper.TryCreateDirectory(installDirectory);
            ExecCommand(winzip, publishDirectory, string.Format(@"x -y {0} -o{1}", Path.Combine(packageDirectory, zipName), installDirectory), false);
        }

        private static void ExecCommand(string file, string workingdir, string args, bool checkExitCode)
        {
            var proc = new Process
            {
                StartInfo =
                {
                    FileName = file,
                    WorkingDirectory = workingdir,
                    Arguments = args,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true
                }
            };

            proc.OutputDataReceived += process_OutputDataReceived;
            proc.ErrorDataReceived += process_ErrorDataReceived;

            proc.Start();

            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();

            proc.WaitForExit();
            if (checkExitCode)
            {
                if (proc.ExitCode != 0)
                {
                    throw new Exception(String.Format("Command {0} with args {1} failed.", file, args));
                }
            }
        }

        static void process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }

        static void process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }
    }
}
