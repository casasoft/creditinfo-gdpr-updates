﻿using System;
using System.IO;
using System.Security;

namespace PackageBuilder 
{
    static class IOHelper 
    {
        public static void DeleteFile(string pathFile) 
        {
            if (File.Exists(pathFile) == false) return;
            var fi = new FileInfo(pathFile) {IsReadOnly = false};
            fi.Delete();
        }

        public static void DeleteForMoveFile(string pathFile) 
        {
            var dirName = Path.GetDirectoryName(pathFile);
            if (Directory.Exists(dirName) == false) Directory.CreateDirectory(dirName);
            if (!File.Exists(pathFile)) return;
            var fi = new FileInfo(pathFile) {IsReadOnly = false};
            fi.Delete();
        }

        public static void DeleteAll(DirectoryInfo source, string searchPattern) 
        {
            if (Directory.Exists(source.FullName) == false)
            {
                return;
            }
            // Delete each file into directory.
            foreach (var fi in source.GetFiles(searchPattern)) 
            {
                Console.WriteLine(@"Deleting {0}\{1}", source.FullName, fi.Name);
                fi.IsReadOnly = false;
                fi.Delete();
            }

            // Delete subdirectory using recursion.
            foreach (var diSourceSubDir in source.GetDirectories())
            {
                DeleteAll(diSourceSubDir, searchPattern);
            }
            
            try
            {
                source.Delete();
            } 
            catch { }
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target, string searchPattern, bool fixReadOnly) 
        {
            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into itâ€™s new directory.
            foreach (var fi in source.GetFiles(searchPattern)) 
            {
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                IOHelper.DeleteForMoveFile(Path.Combine(target.ToString(), fi.Name));
                if (fixReadOnly) fi.IsReadOnly = false;
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }
            // Copy each subdirectory using recursion.
            foreach (var diSourceSubDir in source.GetDirectories()) 
            {
                var nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir, searchPattern, fixReadOnly);
            }
        }

        public static void TryDeleteDirectory(string path, bool recursive) 
        {
            try
            {
                Directory.Delete(path, recursive);
            } 
            catch { }
        }


        public static void TryCreateDirectory(string path) 
        {
            try
            {
                Directory.CreateDirectory(path);
            } 
            catch { }
        }

        static public string GetTempFilePath() 
        {
            string result = null;
            try
            {
                result = Path.GetTempPath();
            }
            catch (SecurityException) {}
            return result;
        }
    }
}
