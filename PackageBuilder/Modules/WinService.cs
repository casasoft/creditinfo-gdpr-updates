﻿using System;
using Microsoft.Build.Utilities;
using Microsoft.Build.Framework;
using System.ServiceProcess;

namespace PackageBuilder.Modules 
{
	public class WinService : Task
    {
		private string machinename = ".";
	    private string command;
        private Boolean ignoreexitcode = false;


	    public string MachineName {
            get { return machinename; }
            set { machinename = value; }
        }

        public Boolean IgnoreExitCode
        {
            get { return ignoreexitcode; }
            set { ignoreexitcode = value; }
        }

	    [Required]
	    public string ServiceName { get; set; }

	    [Required]
        public string Command
        {
            get { return command.ToLower(); }
            set { command = value; }
        }

        public override bool Execute() 
        {
            try {
                Log.LogMessage(MessageImportance.Normal, String.Format("{0} MachineName:{1} ServiceName:{2}", Command, MachineName, ServiceName));
                ServiceController controller = new ServiceController { MachineName = MachineName, ServiceName = ServiceName };
                
                switch (Command) {
                    case "start":
						if ((controller.Status.Equals(ServiceControllerStatus.Stopped)) ||
							(controller.Status.Equals(ServiceControllerStatus.StopPending)))
							controller.Start();                        
                        break;
                    case "stop":
						if ((controller.Status.Equals(ServiceControllerStatus.Running)) ||
							(controller.Status.Equals(ServiceControllerStatus.StartPending)))
							controller.Stop();
                        break;
                    case "restart":
                        controller.Stop(); 
                        controller.Start();
                        break;
                }                
                return true;
            } 
            catch (Exception exc)
            {
                Log.LogWarningFromException(exc, true);
                return IgnoreExitCode;
            }
        }
	}
}
