﻿using System;
using Microsoft.Build.Utilities;
using System.DirectoryServices;
using Microsoft.Build.Framework;

namespace PackageBuilder.Modules {
	public class AppPoolRecycle : Task 
    {
	    [Required]
	    public string AppPoolName { get; set; }
	
        public Boolean IgnoreExitCode { get; set; }
	    
        public string MachineName { get; set; }

        public AppPoolRecycle()
        {
            IgnoreExitCode = false;
        }

	    public override bool Execute() 
        {
            try {
                string appPoolPath;
                if (String.IsNullOrEmpty(MachineName))
                {
                    appPoolPath = "IIS://localhost/W3SVC/AppPools/" + AppPoolName;
                }
                else
                {
                    appPoolPath = "IIS://" + MachineName + "/W3SVC/AppPools/" + AppPoolName;
                }
                Log.LogMessage(MessageImportance.Normal, String.Format("Recycle AppPoolPath:{0}", appPoolPath));
                
                DirectoryEntry appPoolEntry = new DirectoryEntry(appPoolPath);
                appPoolEntry.Invoke("Recycle");                
                
                return true;
            } 
            catch (Exception exc) 
            {
                Log.LogErrorFromException(exc, true);
                return IgnoreExitCode;
            }
	    }
	}
}
