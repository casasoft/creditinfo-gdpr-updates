﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace PackageBuilder 
{
    static class BuildHelper 
    {
        public static void ZipDirectory(string dirToZip, string pathWithZipFile) 
        {
            var filenames = Directory.GetFiles(dirToZip);
            using (ZipOutputStream s = new ZipOutputStream(File.Create(pathWithZipFile)))
            {
                s.SetLevel(9); // 0 - store only to 9 - means best compression
                var buffer = new byte[4096];
                foreach (string file in filenames)
                {
                    ZipEntry entry = new ZipEntry(Path.GetFileName(file)) { DateTime = DateTime.Now };
                    s.PutNextEntry(entry);
                    using (var fs = File.OpenRead(file))
                    {
                        int sourceBytes;
                        do 
                        {
                            sourceBytes = fs.Read(buffer, 0, buffer.Length);
                            s.Write(buffer, 0, sourceBytes);
                        } while (sourceBytes > 0);
                    }
                }
                s.Finish();
                s.Close();
            }
        }

        public static bool CreateZipFile(string dirToZip, string pathWithZipFile)
        {
            bool isCreated;
            DirectoryInfo zipDir = new DirectoryInfo(dirToZip);
            try 
            {
                ZipFile z = ZipFile.Create(pathWithZipFile);
                z.BeginUpdate();
                GetFilesToZip(new FileSystemInfo[] {zipDir}, z);
                z.CommitUpdate();
                z.Close();
                isCreated = true;
            }
            catch (Exception ex)
            {
                isCreated = false;
            }
            return isCreated;
        }


        private static void GetFilesToZip(IEnumerable<FileSystemInfo> fileSystemInfosToZip, ZipFile z) 
        {
            if (fileSystemInfosToZip == null || z == null) return;
            foreach (FileSystemInfo fi in fileSystemInfosToZip)
            {
                if (fi is DirectoryInfo) 
                {
                    DirectoryInfo di = (DirectoryInfo)fi;
                    z.AddDirectory(di.FullName);
                    GetFilesToZip(di.GetFileSystemInfos(), z);
                } 
                else
                {
                    z.Add(fi.FullName);
                }
            }
        }

        public static void FastZipDirectory(string dirToZip, string pathWithZipFile, bool verbose, bool progress) 
        {
            var interval = TimeSpan.FromSeconds(1); //Progress interval in seconds
            FastZipEvents events = null;

            if (verbose) 
            {
                events = new FastZipEvents {
                    ProcessDirectory = ProcessDirectory,
                    ProcessFile = CompresProcessFile
                };

                if (progress) 
                {
                    events.Progress = ShowProgress;
                    events.ProgressInterval = interval;
                }
            }
            var sz = new FastZip(events) {CreateEmptyDirectories = false};
            sz.CreateZip(pathWithZipFile, dirToZip, true,null);
        }

        public static void FastUnzip(string dirToZip, string pathWithZipFile, bool verbose, bool progress) 
        {
            TimeSpan interval = TimeSpan.FromSeconds(1); //Progress interval in seconds
            var overwrite = FastZip.Overwrite.Always;
            const FastZip.ConfirmOverwriteDelegate confirmOverwrite = null;
            FastZipEvents events = null;

            if (verbose) 
            {
                events = new FastZipEvents {
                     ProcessDirectory = ProcessDirectory,
                     ProcessFile = UnzipProcessFile
                 };

                if (progress) 
                {
                    events.Progress = ShowProgress;
                    events.ProgressInterval = interval;
                }
            }
            FastZip sz = new FastZip(events) {
                                 RestoreAttributesOnExtract = true, 
                                 RestoreDateTimeOnExtract = true
                             };
            Console.WriteLine("Extracting Zip");
            sz.ExtractZip(pathWithZipFile, dirToZip, overwrite, confirmOverwrite, null, null, true);
        }

        public static void UnzipFile(string filePath, string instalDir) 
        {
            FileStream rs = new FileStream(filePath, FileMode.Open);
            using (ZipInputStream s = new ZipInputStream(rs)) 
            {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null) 
                {
                    Console.WriteLine(theEntry.Name);

                    string directoryName = Path.GetDirectoryName(theEntry.Name);
                    string fileName = Path.GetFileName(theEntry.Name);
                  
                    // create directory  theEntry.IsDirectory
                    if (directoryName.Length > 0) 
                    {
                        Directory.CreateDirectory(directoryName);
                    }

                    if (fileName == String.Empty) continue;
                    using (var streamWriter = File.Create(theEntry.Name)) 
                    {
                        var data = new byte[2048];
                        while (true) {
                            var size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        static void ShowProgress(object sender, ProgressEventArgs e) 
        {
            Console.WriteLine("{0}%", e.PercentComplete);
        }

        static void CompresProcessFile(object sender, ScanEventArgs e)
        {
            Console.WriteLine("Compressing {0}",e.Name);
        }

        static void UnzipProcessFile(object sender, ScanEventArgs e)
        {
            Console.WriteLine("Unzipping {0}", e.Name);
        }

        static  void ProcessDirectory(object sender, DirectoryEventArgs e) 
        {
            if (!e.HasMatchingFiles)
            {
                Console.WriteLine(e.Name);
            }
        }

        public static void ExecCommand(string file, string workingdir, string args, bool checkExitCode) {
            Process proc = new Process {
            StartInfo = {
                    FileName = file,
                    WorkingDirectory = workingdir,
                    Arguments = args,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true
                }
            };


            proc.OutputDataReceived += process_OutputDataReceived;
            proc.ErrorDataReceived += process_ErrorDataReceived;

            proc.Start();

            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();

            proc.WaitForExit();
            if (checkExitCode) 
            {
                if (proc.ExitCode != 0)
                {
                    throw new Exception(String.Format("Command {0} with args {1} failed.", file, args));
                }
            }
        }

        static void process_ErrorDataReceived(object sender, DataReceivedEventArgs e) 
        {
            Console.WriteLine(e.Data);
        }

        static void process_OutputDataReceived(object sender, DataReceivedEventArgs e) 
        {
            Console.WriteLine(e.Data);
        }
    }
}
