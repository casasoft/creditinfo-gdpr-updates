﻿using System;
using CIS.Web.Preprocessor.Interfaces;
using CIS.Web.Preprocessor.Classes;
using CIS.Web.Preprocessor.Helpers;
using System.Security;
using CIS.Web.Preprocessor;

[assembly: AllowPartiallyTrustedCallersAttribute]
namespace CIS.Web.Preprocessor {
	class Program {
		static void Main() {
		    var resources = Properties.Settings.Default.Resources.Split('|');
            var connStringApp = Properties.Settings.Default.CfgConnectionString;
            ResourceStreamFactory.ConnectionString = connStringApp;
		    for (var i = 0; i < resources.Length; i++) {
                ResourceStreamFactory.ResourcesAssembly = null;
                ISqlScriptUpdater updater = new SqlScriptUpdater();
                try { updater.ApplyScripts(resources[i], "MT.CIG"); }
                catch (Exception ex) { ExceptionHelper.Process(ex); }
            }
            /*
            if (localUpdateScript != null) {return;}
		    
            var salt = uaFactory.CreateSalt(5);
		    var myUser = new auUsersBLLC {
                             UserName = "admin",
                             Salt = salt,
                             Email = "admin@test.com",
                             Groups = "",
                             CntCreditWatch = 1000,
                             HasWebServices = "False",
                             UserType = "Employee",
                             isOpen = "True",
                             OpenUntil = DateTime.Now.AddYears(100),
                             RegisteredBy = 1,
                             Created = DateTime.Now,
                             Updated = DateTime.Now,
                             PasswordHash = uaFactory.CreatePasswordHash("admin", salt),
                             DepartmentID = 1
                         };
		    var factory = new uaFactory();
		    factory.StoreUserAccountDetails(myUser);
		    factory.UpdateCreditInfoUserTableWithInfo(myUser.Email, 1, 1);
		    factory.DeleteEmployeeDetails(myUser.UserName);
		    var myEmp = new UserAdmin.BLL.npEmployee.npEmployeeBLLC
		                {
		                    CreditInfoID = (1),
		                    Initials = myUser.UserName,
		                    LastUpdated = DateTime.Now
		                };
		    factory.StoreEmployeeDetails(myEmp);
            */
		}
	}
}
