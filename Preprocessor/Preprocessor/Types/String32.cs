using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// String32
    /// </summary>
    public class String32 : StringBase, IComparable<String32>
    {
        #region Private vars

        private StringHelper<String32> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public String32()
            : base(32)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="str">string</param>
        public String32(string str)
            : base(32, str)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">string base</param>
        public String32(StringBase stringBase)
            : base(32, stringBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public StringHelper<String32> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new StringHelper<String32>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(String32 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
