namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// Base class for length-limited strings.
    /// </summary>
    public class DecimalComparer<T> /*: IDecimalComparer<T>*/ where T : DecimalBase
    {
        #region Public methods

        /// <summary>
        /// implemetnts IComparer
        /// </summary>
        /// <param name="x">IDecimalBase</param>
        /// <param name="y">IDecimalBase</param>
        /// <returns></returns>
        public int Compare(T x, T y)
        {
            //if (x == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentX, null);

            return x.CompareTo(y);
        }

        #endregion
    }
}