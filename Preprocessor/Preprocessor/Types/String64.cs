using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types {
    /// <summary>
    /// String64
    /// </summary>
    public class String64 : StringBase, IComparable<String64>
    {
        #region Private vars

        private StringHelper<String64> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public String64()
            : base(64)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="str">string</param>
        public String64(string str)
            : base(64, str)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">string base</param>
        public String64(StringBase stringBase)
            : base(64, stringBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public StringHelper<String64> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new StringHelper<String64>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(String64 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
