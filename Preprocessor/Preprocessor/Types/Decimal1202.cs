using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// Decimal1202
    /// </summary>
    public class Decimal1202 : DecimalBase, IComparable<Decimal1202>
    {
        #region Private vars

        private DecimalHelper<Decimal1202> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Decimal1202()
            : base(12,2)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">decimal</param>
        public Decimal1202(decimal value)
            : base(12,2, value)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="valueBase">decimal base</param>
        public Decimal1202(DecimalBase valueBase)
            : base(12,2, valueBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public DecimalHelper<Decimal1202> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new DecimalHelper<Decimal1202>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(Decimal1202 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
