using System;
using System.Globalization;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// Base class for length-limited strings.
    /// </summary>
    public class StringBase //: IStringBase
    {
        #region Private vars

        private bool inited;
        private int maxLength;
        private string myValue;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="maxLength">Maximal length</param>
        /// <param name="value">string value</param>
        public StringBase(int maxLength)
        {
            this.maxLength = maxLength;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="maxLength">Maximal length</param>
        /// <param name="value">string value</param>
        public StringBase(int maxLength, string value)
        {
            this.maxLength = maxLength;
            this.Init(value);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="maxLength">Maximal length</param>
        /// <param name="value">string value</param>
        public StringBase(int maxLength, StringBase value)
        {
            //if (value == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentValue, null);

            this.maxLength = maxLength;
            this.Init(value.Value);

        }

        #endregion

        #region Public properties

        /// <summary>
        /// Maximal length.
        /// </summary>
        public int MaxLength
        {
            get
            {
                return this.maxLength;
            }
        }

        /// <summary>
        /// Value
        /// </summary>
        public string Value
        {
            get
            {
                return this.myValue;
            }
        }


        /// <summary>
        /// string length.
        /// </summary>
        public int Length
        {
            get
            {
				return this.myValue.Length;
				//if (this.myValue != null)
				//{
				//    return this.myValue.Length;
				//}
				//else
				//{
				//    throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentPropertyMyValue, null);
				//}
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="value">Value</param>
        public void Init(string initValue)
        {
            //if (initValue == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentInitValue, null);

            if (!this.inited)
            {
                this.inited = true;
                if (initValue.Length <= this.maxLength)
                {
                    this.myValue = initValue;
                }
				//else
				//{
				//    throw CommonException.LogAndException(CommonExceptionEnums.StringOverflow, null, new ExcParam("initValue", initValue), new ExcParam("initValue.Length", initValue.Length), new ExcParam("maxLength", this.maxLength));
				//}
            }
			//else
			//{
			//    throw CommonException.LogAndException(CommonExceptionEnums.StringAlreadyInited, null, new ExcParam("initValue", initValue));
			//}
        }

        /// <summary>
        /// Compare IStringBase.Value vs (string)value
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>string.CmpareTo() value</returns>
        public int CompareTo(string value)
        {
            return this.Value.CompareTo(value);
        }


        /// <summary>
        /// Compare IStringBase.Value vs IStringBase.Value
        /// </summary>
        /// <param name="value">IStringBase</param>
        /// <returns>string.CmpareTo() value</returns>
        public int CompareTo(StringBase other)
        {
            //if (other == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentOther, null);

            return this.Value.CompareTo(other.Value);

        }

        /// <summary>
        /// Compare IStringBase.Value vs (object)value
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>string.CmpareTo() value</returns>
        public int CompareTo(object value)
        {
            return this.Value.CompareTo(value);
        }


        /// <summary>
        /// Starts with.
        /// </summary>
        /// <param name="prefix">Prefix</param>
        /// <returns>Decision</returns>
        public bool StartsWith(string prefix)
        {
            //if (prefix == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentPrefix, null);

            return this.Value.StartsWith(prefix);
        }

        /// <summary>
        /// Starts with.
        /// </summary>
        /// <param name="prefix">Prefix</param>
        /// <returns>Decision</returns>
        public bool StartsWith(StringBase prefix)
        {
			//if (this.Value == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentThisValue, null);

			//if (prefix == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentPrefix, null);

            return this.Value.StartsWith(prefix.Value);
        }

        /// <summary>
        /// ToString, FOR DEBUG ONLY!!
        /// </summary>
        /// <returns>string, FOR DEBUG ONLY!!</returns>
        public override string ToString()
        {
            string typeInfo = this.GetType().Name;
            if (typeInfo.LastIndexOf('.') >= 0)
            {
                typeInfo = typeInfo.Substring(typeInfo.LastIndexOf('.') + 1);
            }
            return string.Format(CultureInfo.InvariantCulture, "{0}/{1}: {2}", typeInfo, this.Length, this.Value);
        }

        #endregion
    }
}
