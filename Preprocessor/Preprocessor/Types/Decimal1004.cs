using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// Decimal1004
    /// </summary>
    public class Decimal1004 : DecimalBase, IComparable<Decimal1004>
    {
        #region Private vars

        private DecimalHelper<Decimal1004> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Decimal1004()
            : base(10,4)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">decimal</param>
        public Decimal1004(decimal value)
            : base(10,4, value)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="valueBase">decimal base</param>
        public Decimal1004(DecimalBase valueBase)
            : base(10,4, valueBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public DecimalHelper<Decimal1004> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new DecimalHelper<Decimal1004>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(Decimal1004 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
