using System.Globalization;
using CIS.Web.Preprocessor.Interfaces;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// Nullable workaround.
    /// </summary>
    /// <typeparam name="T">Base type</typeparam>
    public class NullableType<T> : INullableType<T>
    {
        #region Private vars

        private static NullableType<T> nullValue = new NullableType<T>();

        private bool hasValue;
        private T myValue;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public NullableType()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="value">Value</param>
        public NullableType(T value)
        {
            //if (value == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentValue, null);

            this.myValue = value;
            this.hasValue = true;
        }
        #endregion

        #region Public properties

        /// <summary>
        /// Typed null.
        /// </summary>        
        public static NullableType<T> Null
        {
            get
            {
                return NullableType<T>.nullValue;
            }
        }

        /// <summary>
        /// Has value.
        /// </summary>
        public bool HasValue
        {
            get
            {
                return this.hasValue;
            }
        }

        /// <summary>
        /// Value
        /// </summary>
        public T Value
        {
            get
            {
				return this.myValue;
				//if (this.hasValue)
				//{
				//    return this.myValue;
				//}
				//else
				//{
				//    throw CommonException.LogAndException(CommonExceptionEnums.DoNotTouchNullValue, null, new ExcParam("type", typeof(T)));
				//}
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// ToString for debugging purposes.
        /// </summary>
        /// <returns>Debugging info</returns>
        public override string ToString()
        {
            string typeInfo = typeof(T).Name;
            if (typeInfo.LastIndexOf('.') >= 0)
            {
                typeInfo = typeInfo.Substring(typeInfo.LastIndexOf('.') + 1);
            }
            if (this.hasValue)
            {
                string str = string.Empty;
                if (this.Value != null)
                {
                    str = this.Value.ToString();
                }
                if (str.StartsWith(typeInfo))
                {
                    return string.Format(CultureInfo.InvariantCulture, "Nullable{0}", str);
                }
                else
                {
                    return string.Format(CultureInfo.InvariantCulture, "Nullable{0}:{1}", typeInfo, this.Value);
                }
            }
            else
            {
                return string.Format(CultureInfo.InvariantCulture, "Nullable{0}:NULL", typeInfo);
            }
        }
        #endregion
    }
}
