using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// String128
    /// </summary>
    public class String128 : StringBase, IComparable<String128>
    {
        #region Private vars

        private StringHelper<String128> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public String128()
            : base(128)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="str">string</param>
        public String128(string str)
            : base(128, str)
        {
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">string base</param>
        public String128(StringBase stringBase)
            : base(128, stringBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public StringHelper<String128> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new StringHelper<String128>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(String128 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
