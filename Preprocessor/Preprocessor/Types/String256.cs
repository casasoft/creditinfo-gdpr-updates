using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// String256
    /// </summary>
    public class String256 : StringBase, IComparable<String256>
    {
        #region Private vars

        private StringHelper<String256> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public String256()
            : base(256)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="str">string</param>
        public String256(string str)
            : base(256, str)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">string base</param>
        public String256(StringBase stringBase)
            : base(256, stringBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public StringHelper<String256> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new StringHelper<String256>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(String256 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
