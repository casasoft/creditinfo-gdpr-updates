namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// Base class for length-limited strings.
    /// </summary>
    //public class StringComparer<T> : IStringComparer<T> where T : StringBase
	public class StringComparer<T> where T : StringBase
    {
        #region Public methods

        /// <summary>
        /// implemetnts IComparer
        /// </summary>
        /// <param name="x">IStringBase</param>
        /// <param name="y">IStringBase</param>
        /// <returns></returns>
        public int Compare(T x, T y)
        {
            //if (x == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentX, null);

            return x.CompareTo(y);
        }

        #endregion
    }
}