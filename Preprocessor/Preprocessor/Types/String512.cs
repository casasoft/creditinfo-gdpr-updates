using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// String512
    /// </summary>
    public class String512 : StringBase, IComparable<String512>
    {
        #region Private vars

        private StringHelper<String512> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public String512()
            : base(512)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="str">string</param>
        public String512(string str)
            : base(512, str)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">string base</param>
        public String512(StringBase stringBase)
            : base(512, stringBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public StringHelper<String512> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new StringHelper<String512>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(String512 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
