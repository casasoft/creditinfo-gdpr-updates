using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// String1024
    /// </summary>
    public class String1024 : StringBase, IComparable<String1024>
    {
        #region Private vars

        private StringHelper<String1024> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public String1024()
            : base(1024)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="str">string</param>
        public String1024(string str)
            : base(1024, str)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">string base</param>
        public String1024(StringBase stringBase)
            : base(1024, stringBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public StringHelper<String1024> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new StringHelper<String1024>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(String1024 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
