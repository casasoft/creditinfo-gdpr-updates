using System;
using System.Globalization;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// Base class for length-limited strings.
    /// </summary>
    public class DecimalBase //: IDecimalBase
    {
        #region Private vars

        private bool inited;
        private int length;
        private int precision;
        private decimal myValue;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="length">max enght of decimal</param>
        /// <param name="precision">precision digits</param>
        public DecimalBase(int length, int precision)
        {
            this.length = length;
            this.precision = precision;
            //if (this.length <= this.precision) throw CommonException.LogAndException(CommonExceptionEnums.TooBigPrecision, null, new ExcParam("lenght", this.length), new ExcParam("precision", this.precision));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="length">max enght of decimal</param>
        /// <param name="precision">precision digits</param>
        /// <param name="value">Decimal value</param>
        public DecimalBase(int length, int precision, decimal value)
        {
            this.length = length;
            this.precision = precision;
            //if (this.length <= this.precision) throw CommonException.LogAndException(CommonExceptionEnums.TooBigPrecision, null, new ExcParam("lenght", this.length), new ExcParam("precision", this.precision));
            this.Init(value);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="length">max enght of decimal</param>
        /// <param name="precision">precision digits</param>
        /// <param name="value">Decimal value</param>
        public DecimalBase(int length, int precision, DecimalBase value)
        {
            //if (value == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentValue, null);

            this.length = length;
            this.precision = precision;
			//if (this.length <= this.precision) throw CommonException.LogAndException(CommonExceptionEnums.TooBigPrecision, null, new ExcParam("lenght", this.length), new ExcParam("precision", this.precision));
            this.Init(value.Value);
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Length
        /// </summary>
        public int Length
        {
            get
            {
                return this.length;
            }
        }

        /// <summary>
        /// Precision
        /// </summary>
        public int Precision
        {
            get
            {
                return this.precision;
            }
        }

        /// <summary>
        /// Value
        /// </summary>
        public decimal Value
        {
            get
            {
                return this.myValue;
            }
        }


        #endregion

        #region Public methods

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="value">Value</param>
        public void Init(decimal initValue)
        {
            if (!this.inited)
            {
                this.inited = true;

                decimal minThenValue = Convert.ToDecimal(Math.Pow(10, this.length - this.precision));
                if (initValue < minThenValue)
                {
                    if (decimal.Round(initValue, this.precision) == initValue)
                    {
                        this.myValue = initValue;
                    }
					//else
					//{
					//    throw CommonException.LogAndException(CommonExceptionEnums.DecimalPrecisionOverflow, null, new ExcParam("initValue", initValue), new ExcParam("this.precision", this.precision), new ExcParam("minThenValue", minThenValue));
					//}
                }
				//else
				//{
				//    throw CommonException.LogAndException(CommonExceptionEnums.DecimalOverflow, null, new ExcParam("initValue", initValue), new ExcParam("this.length", this.length), new ExcParam("minThenValue", minThenValue));
				//}
            }
			//else
			//{
			//    throw CommonException.LogAndException(CommonExceptionEnums.DecimalAlreadyInited, null, new ExcParam("initValue", initValue));
			//}
        }

        /// <summary>
        /// Compare IDecimalBase.Value vs (decimal)value
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>decimal.CompareTo() value</returns>
        public int CompareTo(decimal value)
        {
            return this.Value.CompareTo(value);
        }


        /// <summary>
        /// Compare IDecimalBase.Value vs IDecimalBase.Value
        /// </summary>
        /// <param name="value">IDecimalBase</param>
        /// <returns>decimal.CompareTo() value</returns>
        public int CompareTo(DecimalBase other)
        {
            //if (other == null) throw CommonException.LogAndException(CommonExceptionEnums.NullArgumentOther, null);

            return this.Value.CompareTo(other.Value);
        }

        /// <summary>
        /// Compare IDecimalBase.Value vs (object)value
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>decimal.CompareTo() value</returns>
        public int CompareTo(object value)
        {
            return this.Value.CompareTo(value);
        }

        /// <summary>
        /// ToString, FOR DEBUG ONLY!!
        /// </summary>
        /// <returns>string, FOR DEBUG ONLY!!</returns>
        public override string ToString()
        {
            string typeInfo = this.GetType().Name;
            if (typeInfo.LastIndexOf('.') >= 0)
            {
                typeInfo = typeInfo.Substring(typeInfo.LastIndexOf('.') + 1);
            }
            return string.Format(CultureInfo.InvariantCulture, "{0}/{1},{2}: {3}", typeInfo, this.Length, this.Precision, this.Value);
        }

        #endregion
    }
}
