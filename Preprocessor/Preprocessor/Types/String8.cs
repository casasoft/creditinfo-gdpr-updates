using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// String8
    /// </summary>
    public class String8 : StringBase, IComparable<String8>
    {
        #region Private vars

        private StringHelper<String8> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public String8()
            : base(8)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="str">string</param>
        public String8(string str)
            : base(8, str)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">string base</param>
        public String8(StringBase stringBase)
            : base(8, stringBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public StringHelper<String8> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new StringHelper<String8>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(String8 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
