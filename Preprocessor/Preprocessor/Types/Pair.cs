﻿namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// Basic pair object(nonstruc) type implements IPair
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    //public class Pair<TKey, TValue> : IPair<TKey, TValue>
	public class Pair<TKey, TValue>
    {
        private TKey key;
        private TValue value;

        public Pair()
        {
            key = default(TKey);
            value = default(TValue);
        }

        public Pair(TKey key, TValue value)
        {
            this.key = key;
            this.value = value;
        }


        #region IPair<TKey,TValue> Members

        public TKey Key
        {
            get { return this.key; }
        }

        public TValue Value
        {
            get { return this.value; }
        }

        #endregion
    }
}
