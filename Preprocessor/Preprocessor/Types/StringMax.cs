using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// StringMax
    /// </summary>
    public class StringMax : StringBase, IComparable<StringMax>
    {
        #region Private vars

        private StringHelper<StringMax> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public StringMax()
            : base(1024*1024*1024)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="str">string</param>
        public StringMax(string str)
            : base(1024 * 1024 * 1024, str)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">string base</param>
        public StringMax(StringBase stringBase)
            : base(1024 * 1024 * 1024, stringBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public StringHelper<StringMax> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new StringHelper<StringMax>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(StringMax other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
