using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// Decimal1804
    /// </summary>
    public class Decimal1804 : DecimalBase, IComparable<Decimal1804>
    {
        #region Private vars

        private DecimalHelper<Decimal1804> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Decimal1804()
            : base(18,4)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">decimal</param>
        public Decimal1804(decimal value)
            : base(18,4, value)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="valueBase">decimal base</param>
        public Decimal1804(DecimalBase valueBase)
            : base(18,4, valueBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public DecimalHelper<Decimal1804> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new DecimalHelper<Decimal1804>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(Decimal1804 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
