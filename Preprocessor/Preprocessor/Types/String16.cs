using System;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Types
{
    /// <summary>
    /// String16
    /// </summary>
    public class String16 : StringBase, IComparable<String16>
    {
        #region Private vars

        private StringHelper<String16> helper;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public String16()
            : base(16)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="str">string</param>
        public String16(string str)
            : base(16, str)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">string base</param>
        public String16(StringBase stringBase)
            : base(16, stringBase)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Helper
        /// </summary>
        public StringHelper<String16> Helper
        {
            get
            {
                if (this.helper == null)
                {
                    this.helper = new StringHelper<String16>(this);
                }
                return this.helper;
            }
        }

        /// <summary>
        /// Compare to base
        /// </summary>
        /// <param name="other">typed other</param>
        /// <returns>by base compared value</returns>
        public int CompareTo(String16 other)
        {
            return base.CompareTo(other);
        }

        #endregion
    }
}
