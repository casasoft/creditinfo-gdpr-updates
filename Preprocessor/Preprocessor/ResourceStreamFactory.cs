﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor {

	/// <summary>
	/// embeded data files provider, local only for preprocessor
	/// </summary>
	internal static class ResourceStreamFactory {
		#region Private vars

		private static Assembly resourcesAssembly;
		private static Assembly projectResourcesAssembly;
		private static string resourcesNamespace;
// ReSharper disable FieldCanBeMadeReadOnly
        private static string projectResourcesNamespace = string.Empty;
// ReSharper restore FieldCanBeMadeReadOnly
        private const string sqlScriptsPattern = @".SqlScripts.SqlScript.{0}.([0-9]{{5}}).xml$";
        private static string connectionString;	

		private static string frontDatabaseName;

		#endregion

		#region Public methods

		public static string ConnectionString {
            get { return connectionString; }
            set { connectionString = value; }
		}

		public static string ResourcesNamespace {
			get { return resourcesNamespace; }
			set { resourcesNamespace = value; }
		}

		public static Assembly ResourcesAssembly {
			get { return resourcesAssembly; }
			set { resourcesAssembly = value; }
		}

		public static string FrontDatabaseName {
			get { return frontDatabaseName; }
			set { frontDatabaseName = value; }
		}
		
		/// <summary>
		/// provide Sorted List of update scripts
		/// </summary>
		public static SortedList<int, Stream> GetSqlScripts(string suffix) {
			var listOfStreams = new SortedList<int, Stream>();
			var pattern = new Regex(string.Format(sqlScriptsPattern, suffix));

			foreach (var scriptName in GetResourceNames(pattern)) {
				var scriptNumber = pattern.Match(scriptName).Groups[1].Value;
				int scriptIndex;
				if (!int.TryParse(scriptNumber, out scriptIndex)) {
					throw new Exception("ResourceStreamFactoryNumberParseErrorImpossible");
				}
				listOfStreams.Add(scriptIndex, GetResourceStream(scriptName));
			}
			return listOfStreams;
		}

		/// <summary>
		/// SqlScript XMLSchema resource
		/// </summary>
		public static Stream GetSqlScriptXmlSchema() {
			return GetResourceStream(string.Concat(resourcesNamespace, ".SqlScripts.SqlScript.xsd"));
		}	

		#endregion

		#region Private methods

		/// <summary>
		/// obtain stream of core resource
		/// </summary>
#pragma warning disable 1574
		/// <param name="fileName"></param>
#pragma warning restore 1574
		/// <returns></returns>
		private static Stream GetResourceStream(string resourceName) {
			CheckResourcesAssembly();
			return resourcesAssembly.GetManifestResourceStream(resourceName);
		}

		/// <summary>
		/// obtain all core resources
		/// </summary>
		/// <returns>all resource names</returns>
		private static string[] GetResourceNames() {
			CheckResourcesAssembly();
			return resourcesAssembly.GetManifestResourceNames();
		}

		/// <summary>
		/// obtain only resourcess which match pattern core resources from core
		/// </summary>
		/// <param name="pattern">regular expression</param>
		/// <returns>matched  resourcess</returns>
		private static IList<string> GetResourceNames(Regex pattern) {
			var returnList = new List<string>();
			foreach (string resourceName in GetResourceNames()) {
				if (pattern.IsMatch(resourceName)) {
					returnList.Add(resourceName);
				}
			}
			return returnList.AsReadOnly();
		}

	    /// <summary>
		/// check core assembly Ressources reference
		/// </summary>
		private static void CheckResourcesAssembly() {
			if (resourcesAssembly == null)
			{
			    string typename = string.Concat(resourcesNamespace, ".ModuleInfo, ", resourcesNamespace);
                Type t = Type.GetType(typename);
			    resourcesAssembly = t.Assembly;
			}
		}

		/// <summary>
		/// check project assembly Ressources reference
		/// </summary>
		private static void CheckProjectResourcesAssembly() {
		    if (projectResourcesAssembly != null) return;
		    if (string.IsNullOrEmpty(projectResourcesNamespace)) {
		        ExceptionHelper.Process(new Exception("ResourceStreamFactoryNotInitialized"));
		    }
		    projectResourcesAssembly = Type.GetType(string.Concat(projectResourcesNamespace, ".ModuleInfo, ", projectResourcesNamespace)).Assembly;
		}

		#endregion
	}
}
