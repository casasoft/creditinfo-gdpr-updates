﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using System.Xml.XPath;

using CIS.Web.Preprocessor.Interfaces;
using CIS.Web.Preprocessor.Enums;
using CIS.Web.Preprocessor.Types;
using CIS.Web.Preprocessor.Helpers;



namespace CIS.Web.Preprocessor.Classes {
	/// <summary>
	/// value object for SQL script, repesents  one SqlScript.xsd type 
	/// </summary>
	class SqlScript : ISqlScript {

		#region Private vars

		/// <summary>
		/// holds for debug reasons
		/// </summary>
		private IXPathNavigable currentNode;

	    /// <summary>
		/// collection of ISqlScriptCommands
		/// </summary>
		private List<ISqlScriptCommand> commands;

		/// <summary>
		/// collection of ISqlScriptParameters
		/// </summary>
		private List<ISqlScriptParameter> parameters;

		/// <summary>
		/// sorted collection of ISqlScriptParameters
		/// </summary>
		private SortedList<string, ISqlScriptParameter> parametersByName;

		/// <summary>
		/// collection of ISqlScriptRows
		/// </summary>
		private List<ISqlScriptRow> rows;


		#endregion

		#region Constructor

		#endregion

		#region Public properties

		/// <summary>
		/// delegate  to resolve parameterg
		/// </summary>
		/// <param name="name">name of parameter</param>
		/// <returns>IServiceProvider</returns>
		public delegate IServiceProvider GetParameterByNameDelegate(String128 name);

	    /// <summary>
	    /// Id of sql script
	    /// </summary>
	    public int Id { get; private set; }

	    /// <summary>
	    /// Id of sql script
	    /// </summary>
	    public bool Transact { get; private set; }

	    /// <summary>
	    /// Description of sql script
	    /// </summary>
	    public StringMax Description { get; private set; }

	    /// <summary>
	    /// Safety of sql script
	    /// </summary>
	    public PreprocessingSafeties Safety { get; private set; }

	    /// <summary>
		/// Read only Commands
		/// </summary>
		public IList<ISqlScriptCommand> Commands
		{
			get
			{
				return commands.AsReadOnly();
			}
		}

		/// <summary>
		/// Read only Parameters
		/// </summary>
		public IList<ISqlScriptParameter> Parameters
		{
			get
			{
			    return parameters != null ? parameters.AsReadOnly() : null;
			}
		}

		/// <summary>
		/// Read only Rows
		/// </summary>
		public IList<ISqlScriptRow> Rows
		{
			get
			{
			    return rows != null ? rows.AsReadOnly() : null;
			}
		}


		#endregion

		#region Public methods

		/// <summary>
		/// Load Document
		/// </summary>
#pragma warning disable 1574
		/// <param name="doc">XmlDocument of SqlScript.xsd</param>
#pragma warning restore 1574
		public void Load(IXPathNavigable node)
		{
			currentNode = node;
			var navigator = currentNode.CreateNavigator();

			var nsmgr = new XmlNamespaceManager(new NameTable());
			nsmgr.AddNamespace("ns", "http://bc.creditinfosolutions.com/SqlScripts/SqlScript");


			Id = Convert.ToInt32(XPathNavigatorHelper.SelectSingleNode(navigator, nsmgr, "ns", "/SqlScript/Id").Value, CultureInfo.InvariantCulture);

		    Transact = true;
            try {Transact = Convert.ToBoolean(XPathNavigatorHelper.SelectSingleNode(navigator, nsmgr, "ns", "/SqlScript/Transact").Value, CultureInfo.InvariantCulture);}
            catch {}

			Description = new StringMax(XPathNavigatorHelper.SelectSingleNode(navigator, nsmgr, "ns", "/SqlScript/Description").Value);

			Safety = EnumHelper<PreprocessingSafeties>.Parse(XPathNavigatorHelper.SelectSingleNode(navigator, nsmgr, "ns", "/SqlScript/Safety").Value);

			var iterator = XPathNavigatorHelper.Select(navigator, nsmgr, "ns", "/SqlScript/Commands/CommandText");
			while (iterator.MoveNext()) {
				if (commands == null) {
					commands = new List<ISqlScriptCommand>();
				}
				ISqlScriptCommand command = new SqlScriptCommand();
				command.Load(iterator.Current);
				commands.Add(command);
			}

			iterator = XPathNavigatorHelper.Select(navigator, nsmgr, "ns", "/SqlScript/Parameters/Parameter");
			while (iterator.MoveNext())
			{
				if (parameters == null) {
					parameters = new List<ISqlScriptParameter>();
					parametersByName = new SortedList<string, ISqlScriptParameter>();
				}
				ISqlScriptParameter parameter = new SqlScriptParameter();
				parameter.Load(iterator.Current);
				parameters.Add(parameter);
				if (!parametersByName.ContainsKey(parameter.Name)) {
					parametersByName.Add(parameter.Name, parameter);
				}
				else {
					throw new Exception("SqlScriptAmbigousParameter");
				}
			}

			iterator = XPathNavigatorHelper.Select(navigator, nsmgr, "ns", "/SqlScript/Rows/Row");
			while (iterator.MoveNext())
			{
				if (rows == null) {
					rows = new List<ISqlScriptRow>();
				}
				ISqlScriptRow row = new SqlScriptRow();
				row.Load(iterator.Current, GetParameterByName);
				rows.Add(row);
			}
		}

		#endregion

		#region Private methods

		/// <summary>
		/// return parameter by name
		/// </summary>
		/// <param name="name">nake key in collection</param>
		/// <returns>ISqlScriptParameter</returns>
		private static ISqlScriptParameter GetParameterByName(string name)
		{
		    const ISqlScriptParameter parameter = null;

		    return parameter;
		}

	    #endregion
	}
}