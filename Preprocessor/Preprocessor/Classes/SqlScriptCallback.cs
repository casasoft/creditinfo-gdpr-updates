using System;
using System.Xml.XPath;

using CIS.Web.Preprocessor.Enums;
using CIS.Web.Preprocessor.Interfaces;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Classes
{
    /// <summary>
    /// represents property callback in SqlScript
    /// </summary>
    class SqlScriptCallback : ISqlScriptCallback
    {
        #region Private vars

        /// <summary>
        /// xml node stored for debuging reason
        /// </summary>
        private IXPathNavigable currentNode;

        /// <summary>
        /// holds callback type
        /// </summary>
        private SqlScriptCallbackTypes type;

       #endregion

        #region Public properties

        /// <summary>
        /// command text
        /// </summary>
        public SqlScriptCallbackTypes Type
        {
            get
            {
                return type;
            }
        }
       
        #endregion

        #region Public methods

        /// <summary>
        /// load and parse body of Callback
        /// </summary>
        /// <param name="node">Xml representation of Callback</param>
        public void Load(IXPathNavigable node)
        {
            if (node == null) {
                throw new Exception("SqlScriptCallbackNullArgument");
            }
            currentNode = node;
            var navigator = currentNode.CreateNavigator();
            type = EnumHelper<SqlScriptCallbackTypes>.Parse(navigator.Value);
        }

        #endregion
    }
}
