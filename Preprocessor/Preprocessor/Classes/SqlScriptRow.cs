using System;
using System.Collections.Generic;
using System.Xml.XPath;
using CIS.Web.Preprocessor.Interfaces;

namespace CIS.Web.Preprocessor.Classes
{
    /// <summary>
    /// represents row in SqlScript
    /// </summary>
    class SqlScriptRow : ISqlScriptRow
    {

        #region Private vars

        /// <summary>
        /// xml node stored for debuging reason
        /// </summary>
        private IXPathNavigable currentNode;

        /// <summary>
        /// collecion of ISqlScriptValue from row
        /// </summary>
        private List<ISqlScriptValue> values;

        #endregion


        #region Public properties

        /// <summary>
        /// Read only Values
        /// </summary>
        public IList<ISqlScriptValue> Values
        {
            get
            {
                return this.values.AsReadOnly();
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// load and parse body of Row
        /// </summary>
        /// <param name="node">Xml representation of Row</param>
        public void Load(IXPathNavigable node, SqlScriptParameterByNameCallback parameterResolver)
        {
            if (node != null)
            {
                this.currentNode = node;
                XPathNavigator navigator = this.currentNode.CreateNavigator();
                string uri = "http://bc.creditinfosolutions.com/SqlScripts/SqlScript";

                XPathNodeIterator iterator = navigator.SelectChildren("Value", uri);
                while (iterator.MoveNext())
                {
                    if (this.values == null)
                    {
                        this.values = new List<ISqlScriptValue>();
                    }

                    ISqlScriptValue value = new SqlScriptValue();
                    value.Load(iterator.Current, parameterResolver);
                    this.values.Add(value);
                }
            }
			else
			{
				throw new Exception("SqlScriptRowNullArgument");
			}
        }

        #endregion
    }
}
