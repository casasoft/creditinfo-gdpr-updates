﻿using System.Data.SqlClient;
using CIS.Web.Preprocessor;

namespace CIS.Web.Preprocessor.Classes
{
	
	public static class SqlDataAccess
	{
		/// <summary>
		/// Vykoná požadované kroky v rámci transakce.
		/// Je spuštěna a commitována nová samostatná transakce.
		/// </summary>
		public static void ExecuteTransaction(SqlTransactionDelegate transactionWork)
		{
			ExecuteTransaction(transactionWork, null);
		}

		/// <summary>
		/// Vykoná požadované kroky v rámci transakce.
		/// Pokud je zadaná transakce <c>null</c>, je vytvořena, spuštěna a commitována nová.
		/// Pokud zadaná transakce není <c>null</c>, jsou zadané kroky pouze v rámci transakce vykonány.
		/// </summary>
		/// <param name="transactionWork"></param>
		/// <param name="transaction">transakce (vnější)</param>
		public static void ExecuteTransaction(SqlTransactionDelegate transactionWork, SqlTransaction transaction)
		{
			SqlTransaction currentTransaction = transaction;
			SqlConnection connection;
			if (transaction == null)
			{
				// otevření spojení, pokud jsme iniciátory transakce
				//connection = SqlDataAccess.GetConnection(); 
				connection = new SqlConnection(ResourceStreamFactory.ConnectionString);
				connection.Open();
				currentTransaction = connection.BeginTransaction();
			}
			else
			{
				connection = currentTransaction.Connection;
			}

			try
			{
				transactionWork(currentTransaction);

				if (transaction == null)
				{
					// commit chceme jen v případě, že nejsme uvnitř vnější transakce
					currentTransaction.Commit();
				}
			}
			catch
			{
				try
				{
					currentTransaction.Rollback();
				}
				catch
				{
					// chceme vyhodit vnější výjimku, ne problém s rollbackem
				}
				throw;
			}
			finally
			{
				// uzavření spojení, pokud jsme iniciátory transakce
				if (transaction == null)
				{
					connection.Close();
				}
			}
		}
	}
}
