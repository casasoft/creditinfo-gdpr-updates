using System;
using System.Collections.Generic;
using System.Xml.XPath;

using CIS.Web.Preprocessor.Interfaces;
using CIS.Web.Preprocessor.Enums;
using CIS.Web.Preprocessor.Helpers;

namespace CIS.Web.Preprocessor.Classes
{
    /// <summary>
    /// represents parameters in SqlScript
    /// </summary>
    class SqlScriptParameter : ISqlScriptParameter
    {
        #region Private vars

        /// <summary>
        /// xml node stored for debuging reason
        /// </summary>
        private IXPathNavigable currentNode;

        /// <summary>
        /// parameter name
        /// </summary>
        private string name;

        /// <summary>
        /// holds type
        /// </summary>
        private DataTypes type;

        /// <summary>
        /// holds operation
        /// </summary>
        private SqlScriptParameterOperations operation;

        /// <summary>
        /// collecion of ISqlScriptCommand from parameter(only executed scalar, mainly one per driver)
        /// </summary>
        private List<ISqlScriptCommand> commands;

        /// <summary>
        /// collecion of ISqlScriptCallback from parameter, callback  back to some resolver, to obtain local cluster name etc...
        /// </summary>
        private List<ISqlScriptCallback> callbacks;

        #endregion


        #region Public properties

        /// <summary>
        /// parameter name
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
        }

        /// <summary>
        /// type of parameter
        /// </summary>
        public DataTypes Type
        {
            get
            {
                return this.type;
            }
        }

        /// <summary>
        /// operation with parameter
        /// </summary>
        public SqlScriptParameterOperations Operation
        {
            get
            {
                return this.operation;
            }
        }

        /// <summary>
        /// Read only Commands
        /// </summary>
        public IList<ISqlScriptCommand> Commands
        {
            get
            {
                if (this.commands != null)
                {
                    return this.commands.AsReadOnly();
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Read only Callbacks
        /// </summary>
        public IList<ISqlScriptCallback> Callbacks
        {
            get
            {
                if (this.callbacks != null)
                {
                    return this.callbacks.AsReadOnly();
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// load and parse body of Parameter
        /// </summary>
        /// <param name="node">Xml representation of Parameter</param>
        public void Load(IXPathNavigable node)
        {
            if (node != null)
            {
                this.currentNode = node;
                XPathNavigator navigator = this.currentNode.CreateNavigator();
                string uri = string.Empty;

                this.name = navigator.GetAttribute("name", uri);
                this.type = EnumHelper<DataTypes>.Parse(navigator.GetAttribute("type", uri));
                string opr = navigator.GetAttribute("operation", uri);
                if (string.IsNullOrEmpty(opr))
                {
                    this.operation = SqlScriptParameterOperations.CmdParameter;
                }
                else
                {
                    this.operation = EnumHelper<SqlScriptParameterOperations>.Parse(opr);
                }

                if (this.operation != SqlScriptParameterOperations.CmdParameter)
                {
                    XPathNodeIterator iterator;
					uri = "http://bc.creditinfosolutions.com/SqlScripts/SqlScript";
                    switch (this.operation)
                    {
                        case SqlScriptParameterOperations.ReplacePatternBySqlCommand:
                            iterator = navigator.SelectChildren("CommandText", uri);
                            while (iterator.MoveNext())
                            {
                                if (this.commands == null)
                                {
                                    this.commands = new List<ISqlScriptCommand>();
                                }

                                ISqlScriptCommand command = new SqlScriptCommand();
                                command.Load(iterator.Current);
                                this.commands.Add(command);
                            }
                            break;
                        case SqlScriptParameterOperations.ReplacePatternByCallback:
                            iterator = navigator.SelectChildren("Callback", uri);
                            while (iterator.MoveNext())
                            {
                                if (this.callbacks == null)
                                {
                                    this.callbacks = new List<ISqlScriptCallback>();
                                }

                                ISqlScriptCallback callback = new SqlScriptCallback();
                                callback.Load(iterator.Current);
                                this.callbacks.Add(callback);
                            }
                            break;
                        default:
							throw new Exception("SqlScriptParameterOperationNotSupported");
                    }
                }
            }
			else
			{
				throw new Exception("SqlScriptParameterNullArgument");
			}
        }

        #endregion
    }
}
