using System;
using System.Xml;
using System.Text.RegularExpressions;
using CIS.Web.Preprocessor.Interfaces;
using CIS.Web.Preprocessor.Enums;
using CIS.Web.Preprocessor.Types;
using System.Data.SqlClient;
using CIS.Web.Preprocessor.Helpers;
using CIS.Web.Preprocessor;
using System.Data;
using System.Configuration;

namespace CIS.Web.Preprocessor.Classes {
	/// <summary>
	/// check  version of database and apply unexecuted scripts
	/// </summary>
	class SqlScriptUpdater : ISqlScriptUpdater {

		#region Constructor
        int commandTimeout = 0;

        public SqlScriptUpdater()
        {

            var appSettings = new AppSettingsReader();
            try
            {
                if (appSettings.GetValue("CommandTimeout", typeof(string)) != null)
                {
                    commandTimeout = int.Parse(appSettings.GetValue("CommandTimeout", typeof(string)).ToString());
                }
            }
            catch { }
        }
   

	    #endregion

		#region Public methods

        /// <summary>
        /// check validity of every script, when all scripts valid execute unexecuted
        /// every script have to be performed sorted by id(no script can be skipped)
        /// </summary>
        public void ApplyScripts(string resourcesNamespace, string suffix) {
            //Set resources to be processed
            ResourceStreamFactory.ResourcesNamespace = resourcesNamespace;

            var streams = ResourceStreamFactory.GetSqlScripts(suffix);
            var valid = true;
            foreach (var sqlScriptStream in streams) {
                if (!XmlSchemaValidatorHelper.Validate(sqlScriptStream.Value, ResourceStreamFactory.GetSqlScriptXmlSchema())) {
                    valid = false;
                }
                sqlScriptStream.Value.Seek(0, 0);
            }

            //perform checks only when every xsd is valid
            if (!valid) {
                throw new Exception("SomeSqlScriptsNotValid");
            }

            LocalUpdateScript localUpdateScript = null;
            try { localUpdateScript = DataAccessHelper.ReadLatestUpdateScript(); } catch {}

            foreach (var sqlScriptStream in streams) {
                using (var xmlreader = XmlReader.Create(sqlScriptStream.Value)) {
                    var doc = new XmlDocument();
                    doc.Load(xmlreader);
                    ISqlScript sqlScript = new SqlScript();
                    sqlScript.Load(doc);

                    //if Id is same check descriptions
                    if (localUpdateScript != null
                        && localUpdateScript.Id == sqlScript.Id
                        && NullableTypeComparerHelper<StringMax>.Compare(localUpdateScript.Description,
                                                                         new NullableType<StringMax>(
                                                                             sqlScript.Description)) != 0) {
                                                                                 throw new Exception("SqlScriptAlreadyAppliedButDescriptionDiffers");
                                                                             }

                    if ((localUpdateScript != null || sqlScript.Id != 1) &&
                        (localUpdateScript == null || (sqlScript.Id - 1) != localUpdateScript.Id)) { }
                    else {
                        if (TryApplyScript(sqlScript, commandTimeout))
                        {localUpdateScript = DataAccessHelper.ReadLatestUpdateScript();}
                        else { throw new Exception("SqlScriptApplicationFailed"); }
                    }
                }
            }
        }
		#endregion

		#region Private methods

		/// <summary>
		/// Method wich will try perform script in transaction
		/// </summary>
		/// <param name="sqlScript">ISqlScript which we will try to apply</param>
		/// <returns></returns>
		private static bool TryApplyScript(ISqlScript sqlScript, int commandTimeout) {
			const bool tryScript = true;
            if (sqlScript.Transact) {
                SqlDataAccess.ExecuteTransaction(
                    delegate(SqlTransaction transaction) {
                        var localUpdateScript = DataAccessHelper.ReadLatestUpdateScript();
                        if ((localUpdateScript == null && sqlScript.Id == 1) ||
                            (localUpdateScript != null && (sqlScript.Id - 1) == localUpdateScript.Id)) {
                            //parse commands
                            foreach (var command in sqlScript.Commands) {
                                if (sqlScript.Rows != null) {
                                    foreach (var row in sqlScript.Rows) {
                                        using (var cmd = new SqlCommand()) {
                                            var commandText = command.Text;
                                            foreach (var parameter in sqlScript.Parameters) {
                                                ISqlScriptValue paramValue = null;
                                                foreach (var value in row.Values) {
                                                    if (parameter.Equals(value.Parameter)) {
                                                        paramValue = value;
                                                    }
                                                }
                                                ResolveParameter(cmd, ref commandText, parameter, paramValue);
                                            }
                                            if (commandTimeout != 0)
                                                cmd.CommandTimeout = commandTimeout;
                                            cmd.Transaction = transaction;
                                            cmd.Connection = transaction.Connection;
                                            cmd.CommandText = commandText;
                                            cmd.ExecuteNonQuery();
                                        }
                                    }
                                }
                                else {
                                    using (var cmd = new SqlCommand()) {
                                        var commandText = command.Text;
                                        if (sqlScript.Parameters != null) {
                                            foreach (var parameter in sqlScript.Parameters) {
                                                ResolveParameter(cmd, ref commandText, parameter, null);
                                            }
                                        }



                                        if (commandTimeout != 0)
                                            cmd.CommandTimeout = commandTimeout;
                                        cmd.Transaction = transaction;
                                        cmd.Connection = transaction.Connection;
                                        cmd.CommandText = commandText;
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }

                            //here should be implemented increment
                            using (var cmd = new SqlCommand()) {
                                cmd.Parameters.Add(new SqlParameter("@Id", sqlScript.Id));
                                cmd.Parameters.Add(new SqlParameter("@Description", sqlScript.Description.Value));
                                cmd.Transaction = transaction;
                                cmd.Connection = transaction.Connection;
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText =
                                    @"
								INSERT INTO [localUpdateScript] ([Id], [Description], [Inserted]) VALUES (@Id, @Description, GETDATE())
							";
                                cmd.ExecuteNonQuery();
                            }
                        }
                    });
            }
            else {
                var c = new SqlConnection(ResourceStreamFactory.ConnectionString);
                var localUpdateScript = DataAccessHelper.ReadLatestUpdateScript();
                if ((localUpdateScript == null && sqlScript.Id == 1) ||
                    (localUpdateScript != null && (sqlScript.Id - 1) == localUpdateScript.Id)) {
                    //parse commands
                    foreach (var command in sqlScript.Commands) {
                        if (sqlScript.Rows != null) {
                            foreach (var row in sqlScript.Rows) {
                                using (var cmd = new SqlCommand()) {
                                    var commandText = command.Text;
                                    foreach (var parameter in sqlScript.Parameters) {
                                        ISqlScriptValue paramValue = null;
                                        foreach (var value in row.Values) {
                                            if (parameter.Equals(value.Parameter)) {
                                                paramValue = value;
                                            }
                                        }
                                        ResolveParameter(cmd, ref commandText, parameter, paramValue);
                                    }
                                    cmd.Connection = c;
                                    cmd.CommandText = commandText;
                                    if (c.State != ConnectionState.Open) {c.Open();}
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        else {
                            using (var cmd = new SqlCommand()) {
                                var commandText = command.Text;
                                if (sqlScript.Parameters != null) {
                                    foreach (ISqlScriptParameter parameter in sqlScript.Parameters) {
                                        ResolveParameter(cmd, ref commandText, parameter, null);
                                    }
                                }
                                cmd.Connection = c;
                                cmd.CommandText = commandText;
                                if (c.State != ConnectionState.Open) { c.Open(); }
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }

                    //here should be implemented increment
                    using (var cmd = new SqlCommand()) {
                        cmd.Parameters.Add(new SqlParameter("@Id", sqlScript.Id));
                        cmd.Parameters.Add(new SqlParameter("@Description", sqlScript.Description.Value));
                        cmd.Connection = c;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText =
                            @"
							INSERT INTO [localUpdateScript] ([Id], [Description], [Inserted]) VALUES (@Id, @Description, GETDATE())
						";
                        if (c.State != ConnectionState.Open) { c.Open(); }
                        cmd.ExecuteNonQuery();
                    }
                }
            }
		    return tryScript;
		}


		/// <summary>
		/// Parameter resolver.
		/// </summary>
		/// <param name="cmd">Sql command</param>
		/// <param name="commandText">commandText should be builded before asigning</param>
		/// <param name="parameter">Parameter</param>
		/// <param name="value">Value</param>
		private static void ResolveParameter(SqlCommand cmd, ref string commandText, ISqlScriptParameter parameter, ISqlScriptValue value)
		{
			switch (parameter.Operation)
			{
				case SqlScriptParameterOperations.CmdParameter:
					ResolveCmdParameter(cmd, parameter, value);
					break;
				//case SqlScriptParameterOperations.ReplacePatternBySqlCommand:
				//    this.ReplacePatternBySqlCommand(cmd, ref commandText, parameter, value);
				//    break;
				case SqlScriptParameterOperations.ReplacePatternByCallback:
					ReplacePatternByCallback(ref commandText, parameter);
					break;
				default:
					//Operation is not implemented.
					throw new Exception("SqlScriptParameterOperationNotSupported");
			}
		}

		private static void ReplacePattern(string parameterName, string output, ref string commandText) {
			var replaceBy = output;
			var pattern = string.Concat("(\\$\\{", parameterName, "\\})");
			var patternExpr = new Regex(pattern);
			commandText = patternExpr.Replace(commandText, m => replaceBy);
		}

		/// <summary>
		/// resolve replace patterns by callback( replace itself in CommandText)
		/// </summary>
		/// <param name="commandText"></param>
		/// <param name="parameter">Parameter</param>
		private static void ReplacePatternByCallback(ref string commandText, ISqlScriptParameter parameter) {
			//only for type string
			if (parameter.Callbacks != null) {
				//this is sort of stupid  becouse only one  callback can be, let it be
				foreach (var callback in parameter.Callbacks) {
					string output = null;
					switch (callback.Type) {
						case SqlScriptCallbackTypes.FrontDatabaseName:
							output = ResourceStreamFactory.FrontDatabaseName;
							break;
					}
					ReplacePattern(parameter.Name, output, ref commandText);
				}
			}
			else {
				ExceptionHelper.Process(new Exception("SqlScriptCallbackNullArgument"));
			}
		}

		/// <summary>
		/// resolve command parameters
		/// </summary>
		/// <param name="cmd">Sql command</param>
		/// <param name="parameter">Parameter</param>
		/// <param name="value">Value</param>
		private static void ResolveCmdParameter(SqlCommand cmd, ISqlScriptParameter parameter, ISqlScriptValue value)
		{
#pragma warning disable 612,618
			cmd.Parameters.Add(parameter.Name, value.Value);
#pragma warning restore 612,618
			/*switch (parameter.Type)
			{
				case DataTypes.Byte:
					cmd.SetParameter(parameter.Name, XmlConvert.ToByte(value.Value));
					cmd.Parameters.Add(parameter.Name, XmlConvert.ToByte(value.Value));
					break;
				case DataTypes.NullableByte:
					NullableType<Byte> nullableByte = NullableType<Byte>.Null;
					if (value != null)
					{
						nullableByte = new NullableType<Byte>(XmlConvert.ToByte(value.Value));
					}
					cmd.SetParameter(parameter.Name, nullableByte);
					break;
				case DataTypes.Char:
					cmd.SetParameter(parameter.Name, XmlConvert.ToChar(value.Value));
					break;
				case DataTypes.NullableChar:
					NullableType<Char> nullableChar = NullableType<Char>.Null;
					if (value != null)
					{
						nullableChar = new NullableType<Char>(XmlConvert.ToChar(value.Value));
					}
					cmd.SetParameter(parameter.Name, nullableChar);
					break;
				case DataTypes.Int32:
					cmd.SetParameter(parameter.Name, XmlConvert.ToInt32(value.Value));
					break;
				case DataTypes.NullableInt32:
					NullableType<Int32> nullableInt32 = NullableType<Int32>.Null;
					if (value != null)
					{
						nullableInt32 = new NullableType<Int32>(XmlConvert.ToInt32(value.Value));
					}
					cmd.SetParameter(parameter.Name, nullableInt32);
					break;
				case DataTypes.Int64:
					cmd.SetParameter(parameter.Name, XmlConvert.ToInt64(value.Value));
					break;
				case DataTypes.NullableInt64:
					NullableType<Int64> nullableInt64 = NullableType<Int64>.Null;
					if (value != null)
					{
						nullableInt64 = new NullableType<Int64>(XmlConvert.ToInt64(value.Value));
					}
					cmd.SetParameter(parameter.Name, nullableInt64);
					break;
				case DataTypes.Decimal1202:
					cmd.SetParameter(parameter.Name, new Decimal1202(XmlConvert.ToDecimal(value.Value)));
					break;
				case DataTypes.NullableDecimal1202:
					NullableType<Decimal1202> nullableDecimal1202 = NullableType<Decimal1202>.Null;
					if (value != null)
					{
						nullableDecimal1202 = new NullableType<Decimal1202>(new Decimal1202(XmlConvert.ToDecimal(value.Value)));
					}
					cmd.SetParameter(parameter.Name, nullableDecimal1202);
					break;
				case DataTypes.Decimal1004:
					cmd.SetParameter(parameter.Name, new Decimal1004(XmlConvert.ToDecimal(value.Value)));
					break;
				case DataTypes.NullableDecimal1004:
					NullableType<Decimal1004> nullableDecimal1004 = NullableType<Decimal1004>.Null;
					if (value != null)
					{
						nullableDecimal1004 = new NullableType<Decimal1004>(new Decimal1004(XmlConvert.ToDecimal(value.Value)));
					}
					cmd.SetParameter(parameter.Name, nullableDecimal1004);
					break;
				case DataTypes.Decimal1804:
					cmd.SetParameter(parameter.Name, new Decimal1804(XmlConvert.ToDecimal(value.Value)));
					break;
				case DataTypes.NullableDecimal1804:
					NullableType<Decimal1804> nullableDecimal1804 = NullableType<Decimal1804>.Null;
					if (value != null)
					{
						nullableDecimal1804 = new NullableType<Decimal1804>(new Decimal1804(XmlConvert.ToDecimal(value.Value)));
					}
					cmd.SetParameter(parameter.Name, nullableDecimal1804);
					break;
				case DataTypes.DateTime:
					cmd.SetParameter(parameter.Name, XmlConvert.ToDateTime(value.Value, XmlDateTimeSerializationMode.Unspecified));
					break;
				case DataTypes.NullableDateTime:
					NullableType<DateTime> nullableDateTime = NullableType<DateTime>.Null;
					if (value != null)
					{
						nullableDateTime = new NullableType<DateTime>(XmlConvert.ToDateTime(value.Value, XmlDateTimeSerializationMode.Unspecified));
					}
					cmd.SetParameter(parameter.Name, nullableDateTime);
					break;
				case DataTypes.String8:
					cmd.SetParameter(parameter.Name, new String8(value.Value));
					break;
				case DataTypes.NullableString8:
					NullableType<String8> nullableString8 = NullableType<String8>.Null;
					if (value != null)
					{
						String8 nullableStrValue = new String8(value.Value);
						nullableString8 = new NullableType<String8>(nullableStrValue);
					}
					cmd.SetParameter(parameter.Name, nullableString8);
					break;
				case DataTypes.String16:
					cmd.SetParameter(parameter.Name, new String16(value.Value));
					break;
				case DataTypes.NullableString16:
					NullableType<String16> nullableString16 = NullableType<String16>.Null;
					if (value != null)
					{
						String16 nullableStrValue = new String16(value.Value);
						nullableString16 = new NullableType<String16>(nullableStrValue);
					}
					cmd.SetParameter(parameter.Name, nullableString16);
					break;
				case DataTypes.String32:
					cmd.SetParameter(parameter.Name, new String32(value.Value));
					break;
				case DataTypes.NullableString32:
					NullableType<String32> nullableString32 = NullableType<String32>.Null;
					if (value != null)
					{
						String32 nullableStrValue = new String32(value.Value);
						nullableString32 = new NullableType<String32>(nullableStrValue);
					}
					cmd.SetParameter(parameter.Name, nullableString32);
					break;
				case DataTypes.String64:
					cmd.SetParameter(parameter.Name, new String64(value.Value));
					break;
				case DataTypes.NullableString64:
					NullableType<String64> nullableString64 = NullableType<String64>.Null;
					if (value != null)
					{
						String64 nullableStrValue = new String64(value.Value);
						nullableString64 = new NullableType<String64>(nullableStrValue);
					}
					cmd.SetParameter(parameter.Name, nullableString64);
					break;
				case DataTypes.String128:
					cmd.SetParameter(parameter.Name, new String128(value.Value));
					break;
				case DataTypes.NullableString128:
					NullableType<String128> nullableString128 = NullableType<String128>.Null;
					if (value != null)
					{
						String128 nullableStrValue = new String128(value.Value);
						nullableString128 = new NullableType<String128>(nullableStrValue);
					}
					cmd.SetParameter(parameter.Name, nullableString128);
					break;
				case DataTypes.String256:
					cmd.SetParameter(parameter.Name, new String256(value.Value));
					break;
				case DataTypes.NullableString256:
					NullableType<String256> nullableString256 = NullableType<String256>.Null;
					if (value != null)
					{
						String256 nullableStrValue = new String256(value.Value);
						nullableString256 = new NullableType<String256>(nullableStrValue);
					}
					cmd.SetParameter(parameter.Name, nullableString256);
					break;
				case DataTypes.String512:
					cmd.SetParameter(parameter.Name, new String512(value.Value));
					break;
				case DataTypes.NullableString512:
					NullableType<String512> nullableString512 = NullableType<String512>.Null;
					if (value != null)
					{
						String512 nullableStrValue = new String512(value.Value);
						nullableString512 = new NullableType<String512>(nullableStrValue);
					}
					cmd.SetParameter(parameter.Name, nullableString512);
					break;
				case DataTypes.String1024:
					cmd.SetParameter(parameter.Name, new String1024(value.Value));
					break;
				case DataTypes.NullableString1024:
					NullableType<String1024> nullableString1024 = NullableType<String1024>.Null;
					if (value != null)
					{
						String1024 nullableStrValue = new String1024(value.Value);
						nullableString1024 = new NullableType<String1024>(nullableStrValue);
					}
					cmd.SetParameter(parameter.Name, nullableString1024);
					break;
				case DataTypes.StringMax:
					cmd.SetParameter(parameter.Name, new StringMax(value.Value));
					break;
				case DataTypes.NullableStringMax:
					NullableType<StringMax> nullableStringMax = NullableType<StringMax>.Null;
					if (value != null)
					{
						StringMax nullableStrValue = new StringMax(value.Value);
						nullableStringMax = new NullableType<StringMax>(nullableStrValue);
					}
					cmd.SetParameter(parameter.Name, nullableStringMax);
					break;

				default:
					//DataTypes is not implemented.
					throw PreprocessorException.LogAndException(SqlScriptParameterTypeNotSupported, null);
			}*/
		}

		#endregion
	}
}
