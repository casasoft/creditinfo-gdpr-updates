using System;
using System.Xml.XPath;
using CIS.Web.Preprocessor.Interfaces;


namespace CIS.Web.Preprocessor.Classes
{
    /// <summary>
    /// represents commands in SqlScript
    /// </summary>
    class SqlScriptCommand : ISqlScriptCommand
    {
        #region Private vars

        /// <summary>
        /// xml node stored for debuging reason
        /// </summary>
        private IXPathNavigable currentNode;

        /// <summary>
        /// holds command text
        /// </summary>
        private string text;

        /// <summary>
        /// driver of command(this command should be use only with appropriate driver)
        /// </summary>
        //private DatabaseDrivers driver;

        #endregion

        #region Public properties

        /// <summary>
        /// command text
        /// </summary>
        public string Text
        {
            get
            {
                return this.text;
            }
        }
       

        #endregion

        #region Public methods

        /// <summary>
        /// load and parse body of Command
        /// </summary>
        /// <param name="node">Xml representation of Command</param>
        public void Load(IXPathNavigable node)
        {
            if (node != null)
            {
                this.currentNode = node;
                XPathNavigator navigator = this.currentNode.CreateNavigator();
                string uri = string.Empty;
                this.text = navigator.Value;
            }
			else
			{
				throw new Exception("SqlScriptCommandNullArgument");
			}
        }

        #endregion
    }
}
