﻿using System;
using CIS.Web.Preprocessor.Types;
using CIS.Web.Preprocessor.Helpers;
using CIS.Web.Preprocessor.Interfaces;

namespace CIS.Web.Preprocessor.Classes
{
	public class LocalUpdateScript
	{

		#region Fields

		private readonly Int32 id;
		private readonly DateTime inserted;
		private readonly INullableType<StringMax> description;


		#endregion

		#region Constructor


		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="id">Id</param>
		/// <param name="inserted">Inserted</param>
		/// <param name="description">Description</param>

		public LocalUpdateScript(Int32 id, DateTime inserted, INullableType<StringMax> description)
		{
			this.id = id;
			this.inserted = inserted;
			this.description = description;

		}


		#endregion

		#region Properties


		/// <summary>
		/// Id
		/// </summary>
		public Int32 Id
		{
			get { return id; }
		}

		/// <summary>
		/// Inserted
		/// </summary>
		public DateTime Inserted
		{
			get { return inserted; }
		}

		/// <summary>
		/// Description
		/// </summary>
		public INullableType<StringMax> Description
		{
			get { return description; }
		}


		#endregion



		/// <summary>
		/// implements IComparer
		/// </summary>
		/// <param name="other">ILocalUpdateScript</param>
		/// <returns>compare result</returns>
		public int CompareTo(LocalUpdateScript other)
		{
			if (other == null) throw new Exception("DataAccessExceptionEnum.NullArgumentOther");


		    int cmp = Id.CompareTo(other.Id);
			if (cmp != 0)
			{
				return cmp;
			}

			cmp = Inserted.CompareTo(other.Inserted);
			if (cmp != 0)
			{
				return cmp;
			}

			cmp = NullableTypeComparerHelper<StringMax>.Compare(Description, other.Description);
			if (cmp != 0)
			{
				return cmp;
			}

			return cmp;
		}
	}
}
