using System.Xml.XPath;

using CIS.Web.Preprocessor.Interfaces;

namespace CIS.Web.Preprocessor.Classes
{
    /// <summary>
    /// represents value in row for SqlScript
    /// </summary>
    class SqlScriptValue : ISqlScriptValue
    {

        #region Private vars

        /// <summary>
        /// xml node stored for debuging reason
        /// </summary>
        private IXPathNavigable currentNode;

        /// <summary>
        /// parameter of value
        /// </summary>
        private ISqlScriptParameter parameter;

        /// <summary>
        /// value of parameter in row
        /// </summary>
        private string value;

        #endregion


        #region Public properties

        /// <summary>
        /// parameter of value
        /// </summary>
        public ISqlScriptParameter Parameter
        {
            get
            {
                return parameter;
            }
        }

        /// <summary>
        /// value of parameter in row
        /// </summary>
        public string Value
        {
            get
            {
                if (value != null) return value;
                return "";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// load and parse body of Value
        /// </summary>
        /// <param name="node">Xml representation of Value</param>
        /// <param name="parameterResolver"></param>
        public void Load(IXPathNavigable node, SqlScriptParameterByNameCallback parameterResolver)
        {
            if (node != null)
            {
                if (parameterResolver != null)
                {
                    currentNode = node;
                    XPathNavigator navigator = currentNode.CreateNavigator();
                    string uri = string.Empty;

                    value = navigator.Value;
                    string name = navigator.GetAttribute("name", uri);
                    //check if parameter exists
                    parameter = parameterResolver.Invoke(name);
                }				
            }			
        }

        #endregion


    }
}
