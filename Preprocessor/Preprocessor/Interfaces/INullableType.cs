﻿namespace CIS.Web.Preprocessor.Interfaces
{
	/// <summary>
	/// Nullable workaround.
	/// </summary>
	/// <typeparam name="T">Base type</typeparam>
	public interface INullableType<T>
    {
        /// <summary>
        /// Has value.
        /// </summary>
        bool HasValue { get; }

        /// <summary>
        /// Value
        /// </summary>
        T Value { get; }
	
	}
}
