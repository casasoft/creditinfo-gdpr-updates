using System.Collections.Generic;
using System.Xml.XPath;

namespace CIS.Web.Preprocessor.Interfaces
{
    /// <summary>
    /// represents row in SqlScript
    /// </summary>
    public interface ISqlScriptRow
    {

        #region Properties

        /// <summary>
        /// Read only Values
        /// </summary>
        IList<ISqlScriptValue> Values { get;}


        #endregion

        #region Methods

        /// <summary>
        /// load and parse body of Row
        /// </summary>
        /// <param name="node">Xml representation of row</param>
        /// <param name="parameterResolver"></param>
        void Load(IXPathNavigable node, SqlScriptParameterByNameCallback parameterResolver);

        #endregion
    }
}
