using System.Xml.XPath;

namespace CIS.Web.Preprocessor.Interfaces
{
    /// <summary>
    /// represents value in row for SqlScript
    /// </summary>
    public interface ISqlScriptValue
    {
        #region Properties

        /// <summary>
        /// parameter of value
        /// </summary>
        ISqlScriptParameter Parameter { get;}

        /// <summary>
        /// value of parameter in row
        /// </summary>
        string Value { get;}

        #endregion

        #region Methods

        /// <summary>
        /// load and parse body of Value
        /// </summary>
        /// <param name="node">Xml representation of Value</param>
        /// <param name="parameterResolver"></param>
        void Load(IXPathNavigable node, SqlScriptParameterByNameCallback parameterResolver);

        #endregion
    }
}
