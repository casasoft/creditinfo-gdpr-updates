using System.Xml.XPath;

namespace CIS.Web.Preprocessor.Interfaces
{
    /// <summary>
    /// represents commands in SqlScript
    /// </summary>
    public interface ISqlScriptCommand
    {
        #region Properties

        /// <summary>
        /// command text
        /// </summary>
        string Text { get;}

        /// <summary>
        /// command for driver
        /// </summary>
        //DatabaseDrivers Driver { get;}

        #endregion

        #region Methods

        /// <summary>
        /// load and parse body of Command
        /// </summary>
        /// <param name="node">Xml representation of Command</param>
        void Load(IXPathNavigable node);

        #endregion

    }
}
