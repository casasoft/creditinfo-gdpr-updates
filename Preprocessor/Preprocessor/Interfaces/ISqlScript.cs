﻿using System.Collections.Generic;
using System.Xml.XPath;
using CIS.Web.Preprocessor.Enums;
using CIS.Web.Preprocessor.Types;

namespace CIS.Web.Preprocessor.Interfaces
{
	/// <summary>
	/// repesents one SqlScript value object
	/// </summary>
	public interface ISqlScript
	{

		#region Properties

		/// <summary>
		/// Id of sql script
		/// </summary>
		int Id { get; }

        /// <summary>
        /// If the query must ran in transact mode
        /// </summary>
        bool Transact { get; }
        
        /// <summary>
		/// Description of sql script
		/// </summary>
		StringMax Description { get; }

		/// <summary>
		/// Safety of sql script
		/// </summary>
		PreprocessingSafeties Safety { get; }

		/// <summary>
		/// Read only Commands
		/// </summary>
		IList<ISqlScriptCommand> Commands { get; }

		/// <summary>
		/// Read only Parameters
		/// </summary>
		IList<ISqlScriptParameter> Parameters { get; }


		/// <summary>
		/// Read only Rows
		/// </summary>
		IList<ISqlScriptRow> Rows { get; }

		#endregion

		#region Methods

		/// <summary>
		/// Load Document
		/// </summary>
		/// <param name="doc">XmlDocument of SqlScript.xsd</param>
		void Load(IXPathNavigable node);

		#endregion


	}
}
