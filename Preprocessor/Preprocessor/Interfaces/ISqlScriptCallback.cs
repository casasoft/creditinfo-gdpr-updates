using System.Xml.XPath;

using CIS.Web.Preprocessor.Enums;

namespace CIS.Web.Preprocessor.Interfaces
{
    /// <summary>
    /// represents property callback in SqlScript
    /// </summary>
    public interface ISqlScriptCallback
    {
        #region Properties

        /// <summary>
        /// command text
        /// </summary>
        SqlScriptCallbackTypes Type { get;}

        #endregion

        #region Methods

        /// <summary>
        /// load and parse body of Callback
        /// </summary>
        /// <param name="node">Xml representation of Callback</param>
        void Load(IXPathNavigable node);

        #endregion

    }
}
