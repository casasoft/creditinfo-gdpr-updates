namespace CIS.Web.Preprocessor.Interfaces
{
    /// <summary>
    /// interface for SQL script updater
    /// </summary>
    public interface ISqlScriptUpdater {
		void ApplyScripts(string resourcesNamespace, string suffix);
    }
}
