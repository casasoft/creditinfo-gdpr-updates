﻿namespace CIS.Web.Preprocessor.Interfaces
{
	/// <summary>
	/// SqlScriptParameter delegate.
	/// </summary>
	/// <param name="name">Name</param>
	/// <returns>Parameter</returns>
	public delegate ISqlScriptParameter SqlScriptParameterByNameCallback(string name);
}
