using System.Collections.Generic;
using System.Xml.XPath;

using CIS.Web.Preprocessor.Enums;

namespace CIS.Web.Preprocessor.Interfaces
{
    /// <summary>
    /// represents parameters in SqlScript
    /// </summary>
    public interface ISqlScriptParameter
    {
        #region Properties

        /// <summary>
        /// parameter name
        /// </summary>
        string Name { get;}

        /// <summary>
        /// type of parameter
        /// </summary>
        DataTypes Type { get;}

        /// <summary>
        /// operation with parameter
        /// </summary>
        SqlScriptParameterOperations Operation { get;}

        /// <summary>
        /// Read only Commands
        /// </summary>
        IList<ISqlScriptCommand> Commands { get;}

        /// <summary>
        /// Read only Callbacks
        /// </summary>
        IList<ISqlScriptCallback> Callbacks { get;}

        #endregion

        #region Methods

        /// <summary>
        /// load and parse body of Parameter
        /// </summary>
        /// <param name="node">Xml representation of Parameter</param>
        void Load(IXPathNavigable node);

        #endregion
    }
}
