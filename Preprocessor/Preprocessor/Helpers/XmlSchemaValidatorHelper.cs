using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace CIS.Web.Preprocessor.Helpers {
    /// <summary>
    /// Class which help with XML validation
    /// </summary>
    public static class XmlSchemaValidatorHelper {

        #region Public methods

        /// <summary>
        /// Validate xml stream with xsd stream
        /// </summary>
        /// <param name="xml">xml stream</param>
        /// <param name="xsd">xsd stream</param>
        /// <returns>return true when xml is valid</returns>
        public static bool Validate(Stream xml, Stream xsd) {
            return Validate(xml, xsd, null,false);
        }

        public delegate void XmlReaderDelegate(XmlReader reader);

        /// <summary>
        /// Validate xml stream with xsd stream, call callback
        /// </summary>
        /// <param name="xml">xml stream</param>
        /// <param name="xsd">xsd stream</param>
        /// <param name="callback"></param>
        /// <param name="throwOnError"></param>
        /// <returns>return true when xml is valid</returns>
        public static bool Validate(Stream xml, Stream xsd, XmlReaderDelegate callback,bool throwOnError) {
            var schema = XmlSchema.Read(xsd, XsdValidationCallBack);
            var schemaSet = new XmlSchemaSet();
            schemaSet.Add(schema);
            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema, Schemas = schemaSet};
            settings.ValidationEventHandler += XmlValidationCallBack;
            var valid = true;
            using (var reader = XmlReader.Create(xml, settings)) {
                try {
                    if (callback == null) {while (reader.Read()) { CheckValidity(reader); }}
                    else {while (reader.Read()) { CheckValidity(reader); callback.Invoke(reader); }}
                }
                //catch (CommonException)
				catch (Exception) {
                    valid = false;
                    if (throwOnError) throw;
                }
            }
            return valid;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// callback when schema have error
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">ValidationEventArgs</param>
        private static void XsdValidationCallBack(object sender, ValidationEventArgs e) {
			//if (e.Severity == XmlSeverityType.Error)
			//{
			//    XmlReader valReader = sender as XmlReader;
			//    IXmlLineInfo valInfo = sender as IXmlLineInfo;

			//    if (valReader == null)
			//    {
			//        throw CommonException.LogAndException(CommonExceptionEnums.XsdValidationFailed, e.Exception);
			//    }
			//    else
			//    {
			//        throw CommonException.LogAndException(CommonExceptionEnums.XsdValidationFailed, e.Exception,
			//            new ExcParam("NodeType", valReader.NodeType),
			//            new ExcParam("LocalName", valReader.LocalName),
			//            new ExcParam("lineNumber(+-2)", valInfo.LineNumber),
			//            new ExcParam("linePosition", valInfo.LinePosition),
			//            new ExcParam("depth", valReader.Depth),
			//            new ExcParam("ValueType", valReader.ValueType),
			//            new ExcParam("Value", valReader.Value)
			//        );
			//    }
			//}
			//else
			//{
			//    LoggerProvider.Log(XmlSchemaValidatorHelper.ProxyLogSeverity(e.Severity), ResourceFactory.XmlSchemaParse(e.Severity, e.Message));
			//}
        }

        /// <summary>
        /// callback when xml have error
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">ValidationEventArgs</param>
        private static void XmlValidationCallBack(object sender, ValidationEventArgs e) {

			//if (e.Severity == XmlSeverityType.Error)
			//{
			//    XmlReader valReader = sender as XmlReader;
			//    IXmlLineInfo valInfo = sender as IXmlLineInfo;

			//    if (valReader == null)
			//    {
			//        throw CommonException.LogAndException(CommonExceptionEnums.XmlValidationFailed, e.Exception);
			//    }
			//    else
			//    {
			//        throw CommonException.LogAndException(CommonExceptionEnums.XmlValidationFailed, e.Exception,
			//            new ExcParam("NodeType", valReader.NodeType),
			//            new ExcParam("LocalName",valReader.LocalName),
			//            new ExcParam("lineNumber(+-2)", valInfo.LineNumber),
			//            new ExcParam("linePosition", valInfo.LinePosition),
			//            new ExcParam("depth", valReader.Depth),
			//            new ExcParam("ValueType", valReader.ValueType),
			//            new ExcParam("Value", valReader.Value)
			//        );
			//    }
			//}
			//else
			//{
			//    LoggerProvider.Log(XmlSchemaValidatorHelper.ProxyLogSeverity(e.Severity), ResourceFactory.XmlParse(e.Severity, e.Message));
			//}
        }

        /// <summary>
        /// Checks validity of current status of reader... because of validation problems during validation in wrong namespace etc.
        /// </summary>
        /// <param name="reader">Reader</param>
        private static void CheckValidity(XmlReader reader) {
            switch (reader.NodeType) {
                case XmlNodeType.Attribute:
                case XmlNodeType.Element:
                    ///TODO - important to check that SchemaTypes are not null (because of wrong namespaces), but 
                    ///for anytype XML it is null and it is correct, must be solved somehow!
                    //if (reader.SchemaInfo.SchemaType == null)
                    //    throw CommonException.LogAndException("XmlValidationCheckSchemaTypeMissing", null);
                    break;
            }
        }


        #endregion
    }
}
