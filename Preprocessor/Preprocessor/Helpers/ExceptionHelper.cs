﻿using System;

namespace CIS.Web.Preprocessor.Helpers {
	public static class ExceptionHelper {

		public static void Process(Exception ex) {
			Console.WriteLine(ex.ToString());
			Console.ReadLine();
			throw ex;
		}
	}
}
