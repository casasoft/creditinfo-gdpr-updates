using System;
using CIS.Web.Preprocessor.Types;

namespace CIS.Web.Preprocessor.Helpers {
    /// <summary>
    /// String helper generic template
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class StringHelper<T> where T : StringBase, new() {
        #region Private vars

        private readonly T stringBase;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stringBase">String base</param>
        public StringHelper(T stringBase)
        {
            if (stringBase == null) throw new Exception("CommonExceptionEnums.NullArgumentStringBase");

            this.stringBase = stringBase;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Replace
        /// </summary>
        /// <param name="oldValue">Old value</param>
        /// <param name="newValue">New value</param>
        /// <returns>Replaced string</returns>
        public T Replace(string oldValue, string newValue) {		
            var str = stringBase.Value.Replace(oldValue, newValue);
            var ret = new T();
            ret.Init(str);
            return ret;
        }

        /// <summary>
        /// Replace
        /// </summary>
        /// <param name="oldValue">Old value</param>
        /// <param name="newValue">New value</param>
        /// <returns>Replaced string</returns>
        public T Replace(StringBase oldValue, StringBase newValue) {
            var ret = Replace(oldValue.Value, newValue.Value);
            return ret;
        }

        /// <summary>
        /// String.Format
        /// </summary>
        /// <param name="formatProvider">Format provider, recommended CultureInfo.InvariantCulture</param>
        /// <param name="formatStr"></param>
        /// <param name="args">Arguments</param>
        /// <param name="formatProvider"></param>
        /// <returns>Formatted string</returns>
        public static T Format(IFormatProvider formatProvider, string formatStr, params object[] args) {
            var str = string.Format(formatProvider, formatStr, args);
            var ret = new T();
            ret.Init(str);
            return ret;
        }

        #endregion
    }
}
