using System;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace CIS.Web.Preprocessor.Helpers {

    /// <summary>
    /// helper  to anning renaming of Xpaths with default namespace
    /// </summary>
    public static class XPathNavigatorHelper {
        /// <summary>
        /// holds some value for resolving matches in regular expression
        /// </summary>
        private class Formater {
            readonly string prefix;
            readonly string xpath;

            /// <summary>
            /// constructorr
            /// </summary>
            /// <param name="prefix">namespace prefix</param>
            /// <param name="xpath">xpath expression</param>
            public Formater(string prefix, string xpath) {
                this.prefix=prefix;
                this.xpath=xpath;
            }

            /// <summary>
            /// Formater
            /// </summary>
            /// <returns>formated expression</returns>
            public string Format() {
                var quoted = false;
                var sb = new StringBuilder();
                for (var i = 0; i < xpath.Length; i++) {
                    if (xpath[i] == '\'') {
                        quoted = !quoted;
                    }
                    if (!quoted) {
                        var prevChar = ' ';
                        if (i > 0) {
                            prevChar = xpath[i - 1];
                        }
                        if (Char.IsLetter(xpath[i]) && !Char.IsLetterOrDigit(prevChar) && prevChar != '@' && prevChar != '.') {
                            sb.Append(prefix);
                            sb.Append(":");
                        }
                    }
                    sb.Append(xpath[i]);
                }

                return sb.ToString();
            }
        }

        #region Public Methods

        /// <summary>
        /// wrapper around XPathNavigator
        /// </summary>
        /// <param name="navigator">XPathNavigator</param>
        /// <param name="resolver">namespace resolver</param>
        /// <param name="prefix">namespace prefix</param>
        /// <param name="xpath">xpath expression</param>
        /// <returns>wrapped XPathNavigator</returns>
        public static XPathNavigator SelectSingleNode(XPathNavigator navigator, IXmlNamespaceResolver resolver, string prefix, string xpath) {
            if (navigator == null) { return null; }
            var formatedXpath = new Formater(prefix, xpath).Format();
            return navigator.SelectSingleNode(formatedXpath, resolver);
        }

        /// <summary>
        /// wrapper around XPathNavigator
        /// </summary>
        /// <param name="navigator">XPathNavigator</param>
        /// <param name="resolver">namespace resolver</param>
        /// <param name="prefix">namespace prefix</param>
        /// <param name="xpath">xpath expression</param>
        /// <returns>wrapped XPathNodeIterator</returns>
        public static XPathNodeIterator Select(XPathNavigator navigator, IXmlNamespaceResolver resolver, string prefix, string xpath) {
            if (navigator == null) { return null; }
            var formatedXpath = new Formater(prefix, xpath).Format();
            return navigator.Select(formatedXpath, resolver);
        }

        #endregion

    }
}
