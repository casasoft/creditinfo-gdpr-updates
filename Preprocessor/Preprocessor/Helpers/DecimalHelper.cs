using CIS.Web.Preprocessor.Types;

namespace CIS.Web.Preprocessor.Helpers {
    /// <summary>
    /// Decimal helper generic template
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DecimalHelper<T> where T : DecimalBase, new() {
        #region Private vars

        private readonly T valueBase;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="valueBase">Decimal base</param>
        public DecimalHelper(T valueBase)
        {
            this.valueBase = valueBase;
        }

        #endregion

        #region Public methods

        #endregion

        public T ValueBase {
            get { return valueBase; }
        }
    }
}
