using System;

namespace CIS.Web.Preprocessor.Helpers {
    /// <summary>
    /// Enum helper.
    /// </summary>
    /// <typeparam name="EnumType">Enum type for template</typeparam>    
    public static class EnumHelper<EnumType> {
        /// <summary>
        /// Parse string value and convert to enum.
        /// </summary>
        /// <param name="value">string value</param>
        /// <returns>Enum value</returns>
        public static EnumType Parse(string value) {
            return (EnumType) Enum.Parse(typeof(EnumType), value);
        }
    }
}
