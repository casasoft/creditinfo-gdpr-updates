﻿using System;
using CIS.Web.Preprocessor.Classes;
using CIS.Web.Preprocessor.Interfaces;
using CIS.Web.Preprocessor.Types;
using System.Data.SqlClient;
using CIS.Web.Preprocessor;

namespace CIS.Web.Preprocessor.Helpers {
	public static class DataAccessHelper {
		public static LocalUpdateScript ReadLatestUpdateScript() {
			int id;
			DateTime inserted;
			INullableType<StringMax> description;

			using (var connection =
				new SqlConnection(ResourceStreamFactory.ConnectionString)) {
				connection.Open();
				using (var cmd = new SqlCommand()) {
					cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
					cmd.Parameters.Add("@Inserted", System.Data.SqlDbType.DateTime);
					cmd.Parameters.Add("@Description", System.Data.SqlDbType.NVarChar, 3000);
					cmd.Parameters[0].Direction = System.Data.ParameterDirection.Output;
					cmd.Parameters[1].Direction = System.Data.ParameterDirection.Output;
					cmd.Parameters[2].Direction = System.Data.ParameterDirection.Output;
					cmd.Connection = connection;
					cmd.CommandType = System.Data.CommandType.Text;
					cmd.CommandText = @"
                        SELECT @Id=0
                        IF EXISTS (SELECT * FROM sysobjects WHERE name = N'localUpdateScript' AND type = 'U')
                        BEGIN
	                        SELECT TOP 1 @Id=[Id],@Inserted=[Inserted], @Description=[Description] FROM [localUpdateScript] ORDER BY Id DESC
                        END
                        ";
					cmd.ExecuteNonQuery();

					id = Convert.ToInt32(cmd.Parameters["@Id"].Value);
					if (id != 0) {
						inserted = Convert.ToDateTime(cmd.Parameters["@Inserted"].Value);
						var str = Convert.ToString(cmd.Parameters["@Description"].Value);
						description = new NullableType<StringMax>(new StringMax(str));

					} else {
						inserted = DateTime.Now;
						description = new NullableType<StringMax>();
					}
				}
			}

			if (id > 0) {
				var ret = new LocalUpdateScript(id, inserted, description);
				return ret;
			}
		    return null;
		}
	}
}
