﻿using System;
using CIS.Web.Preprocessor.Interfaces;

namespace CIS.Web.Preprocessor.Helpers {
	/// <summary>
	/// helper class to compare nullable types
	/// </summary>
	/// <typeparam name="T">AnyType which implements IComparer<></typeparam>
	public static class NullableTypeComparerHelper<T> where T : IComparable<T> {
		/// <summary>
		/// compare nullable types
		/// </summary>
		/// <param name="x">INullableType<T></param>
		/// <param name="y">INullableType<T></param>
		/// <returns></returns>
		public static int Compare(INullableType<T> x, INullableType<T> y) {
			if (x.HasValue != y.HasValue) {return x.HasValue ? 1 : -1;}
			return !x.HasValue ? 0 : x.Value.CompareTo(y.Value);
		}    
	}
}
