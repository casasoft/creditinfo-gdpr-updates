﻿namespace CIS.Web.Preprocessor.Enums
{
	/// <summary>
	/// Supported data types.
	/// </summary>
	public enum DataTypes
	{
		/// <summary>
		/// Not defined, not to use.
		/// </summary>
		None = 0,

		/// <summary>
		/// System.Char
		/// </summary>
		Char = 1,

		/// <summary>
		/// Nullable<System.Char>
		/// </summary>
		NullableChar = 2,

		/// <summary>
		/// System.Byte
		/// </summary>
		Byte = 3,

		/// <summary>
		/// Nullable<System.Byte>
		/// </summary>
		NullableByte = 4,

		/// <summary>
		/// System.Int32
		/// </summary>
		Int32 = 5,

		/// <summary>
		/// Nullable<System.Int32>
		/// </summary>
		NullableInt32 = 6,

		/// <summary>
		/// System.Int64
		/// </summary>
		Int64 = 7,

		/// <summary>
		/// Nullable<System.Int64>
		/// </summary>
		NullableInt64 = 8,

		/// <summary>
		/// System.Decimal(12,2)
		/// </summary>
		Decimal1202 = 9,

		/// <summary>
		/// System.Decimal(10,4)
		/// </summary>
		Decimal1004 = 10,

		/// <summary>
		/// System.Decimal(18,4)
		/// </summary>
		Decimal1804 = 11,

		/// <summary>
		/// Nullable<System.Decimal1202>
		/// </summary>
		NullableDecimal1202 = 12,

		/// <summary>
		/// Nullable<System.Decimal1004>
		/// </summary>
		NullableDecimal1004 = 13,

		/// <summary>
		/// Nullable<System.Decimal1804>
		/// </summary>
		NullableDecimal1804 = 14,

		/// <summary>
		/// System.DateTime
		/// </summary>
		DateTime = 15,

		/// <summary>
		/// Nullable<System.DateTime>
		/// </summary>
		NullableDateTime = 16,

		/// <summary>
		/// System.String length 8
		/// </summary>
		String8 = 17,

		/// <summary>
		/// System.String length 16
		/// </summary>
		String16 = 18,

		/// <summary>
		/// System.String length 32
		/// </summary>
		String32 = 19,

		/// <summary>
		/// System.String length 64
		/// </summary>
		String64 = 20,

		/// <summary>
		/// System.String length 128
		/// </summary>
		String128 = 21,

		/// <summary>
		/// System.String length 256
		/// </summary>
		String256 = 22,

		/// <summary>
		/// System.String length 512
		/// </summary>
		String512 = 23,

		/// <summary>
		/// System.String length 1024
		/// </summary>
		String1024 = 24,

		/// <summary>
		/// System.String
		/// </summary>
		StringMax = 25,

		/// <summary>
		/// System.NullableString length 8
		/// </summary>
		NullableString8 = 26,

		/// <summary>
		/// System.NullableString length 16
		/// </summary>
		NullableString16 = 27,

		/// <summary>
		/// System.NullableString length 32
		/// </summary>
		NullableString32 = 28,

		/// <summary>
		/// System.NullableString length 64
		/// </summary>
		NullableString64 = 29,

		/// <summary>
		/// System.NullableString length 128
		/// </summary>
		NullableString128 = 30,

		/// <summary>
		/// System.NullableString length 256
		/// </summary>
		NullableString256 = 31,

		/// <summary>
		/// System.NullableString length 512
		/// </summary>
		NullableString512 = 32,

		/// <summary>
		/// System.NullableString length 1024
		/// </summary>
		NullableString1024 = 33,

		/// <summary>
		/// System.NullableString
		/// </summary>
		NullableStringMax = 34,

		/// <summary>
		/// Binary
		/// </summary>
		Binary = 35,

		/// <summary>
		/// Nullable binary
		/// </summary>
		NullableBinary = 36,

		/// <summary>
		/// Boolean
		/// </summary>
		Boolean = 37,

		/// <summary>
		/// Nullable boolean
		/// </summary>
		NullableBoolean = 38,

		/// <summary>
		/// Guid
		/// </summary>
		Guid = 39,

		/// <summary>
		/// Nullable Guid
		/// </summary>
		NullableGuid = 40,

	}
}
