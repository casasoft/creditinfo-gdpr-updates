using System;

namespace CIS.Web.Preprocessor.Enums
{
    /// <summary>
    /// system safety when performing change
    /// </summary>
    [Flags]
    public enum PreprocessingSafeties
    {
        /// <summary>
        /// No safety, possible to be executed  action on running system(without any signals  reloads etc)
        /// </summary>
        None = 0,
        /// <summary>
        /// execute possible to run Online (only under preprocessing signal to stop processing and restart)
        /// </summary>
        Online = 1,
        /// <summary>
        /// execute forced to run Ofline (all appservices and webservices needs to bu shutdowned manually before and  full backup needs to be taken)
        /// </summary>
        Offline = 2,
        /// <summary>
        /// repeatable
        /// </summary>
        Repeatable = 4,
        /// <summary>
        /// All safeties used as mask
        /// </summary>
        All = Online | Offline | Repeatable
    }
}
