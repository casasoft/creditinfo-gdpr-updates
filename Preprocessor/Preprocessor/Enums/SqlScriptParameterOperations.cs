namespace CIS.Web.Preprocessor.Enums
{
    /// <summary>
    /// types of operations with parameters
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1027:MarkEnumsWithFlags")]
    public enum SqlScriptParameterOperations
    {
        /// <summary>
        /// Not defined, not to use.
        /// </summary>
        None = 0,

        /// <summary>
        /// Assign value as ISqlCommand parameter
        /// </summary>
        CmdParameter = 1,

        /// <summary>
        /// replace pattern in Commands by result of SqlCommand
        /// </summary>
        ReplacePatternBySqlCommand = 2,

        /// <summary>
        /// replace pattern in Commands by result of Callback
        /// </summary>
        ReplacePatternByCallback = 3,

    }
}
