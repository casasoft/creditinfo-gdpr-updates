namespace CIS.Web.Preprocessor.Enums
{
    /// <summary>
    /// types of Callbacks performed on parameters
    /// </summary>
    public enum SqlScriptCallbackTypes
    {
        /// <summary>
        /// Not defined, not to use.
        /// </summary>
        None = 0,

		/// <summary>
		/// Get front-end database name
		/// </summary>
		FrontDatabaseName = 1,
    }
}
