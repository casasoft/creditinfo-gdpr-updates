namespace CIS.Web.Preprocessor.Enums
{
    /// <summary>
    /// types of operations with Replace paterns in parameters
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1027:MarkEnumsWithFlags")]
    public enum SqlScriptReplaceParameters
    {
        /// <summary>
        /// Not defined, not to use.
        /// </summary>
        None = 0,

        /// <summary>
        /// perform scalar sql command
        /// </summary>
        CommandText = 1,

        /// <summary>
        /// Callback to resolve system vars
        /// </summary>
        Callback = 2,
    }
}