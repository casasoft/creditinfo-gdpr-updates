﻿using System;

namespace CIS.Web.Preprocessor.Enums
{
	/// <summary>
	/// levels of logging
	/// one log entry can have more facilities, therefor flags
	/// </summary>
	[Flags]
	public enum LogSeverities
	{
		/// <summary>
		/// All Severities flags are cleared
		/// </summary>
		None = 0,
		/// <summary>
		///  debug-level messages
		/// </summary>
		Debug = 1,
		/// <summary>
		/// informational messages
		/// </summary>
		Notice = 2,
		/// <summary>
		/// normal but significant condition
		/// </summary>
		Info = 4,
		/// <summary>
		/// warning conditions
		/// </summary>
		Warning = 8,
		/// <summary>
		/// error conditions
		/// </summary>
		Error = 16,
		/// <summary>
		///  critical conditions
		/// </summary>        
		Critical = 32,
		/// <summary>
		/// action must be taken immediately
		/// </summary>
		Alert = 64,
		/// <summary>
		/// system is unusable
		/// </summary>
		FatalError = 128,
		/// <summary>
		/// All events
		/// </summary>
		All = 255
	}
}
