﻿ALTER TABLE au_users
ADD lockUserAccount BIT;

USE [CreditInfoGroup_mt]
GO

/****** Object:  Table [dbo].[au_SearchFilter]    Script Date: 1/2/2019 12:08:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[au_SearchFilter](
	[isInd] [bit] NOT NULL,
	[isIndVat] [bit] NOT NULL,
	[isComp] [bit] NOT NULL,
	[filterId] [int] NOT NULL,
	[isSubscriber] [nchar](10) NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [au_Subscribers]
ADD  isPrintBlocked BIT;

CREATE TABLE [dbo].[audit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AuditTableName] [varchar](100) NULL,
	[FieldName] [varchar](100) NULL,
	[ValueBefore] [nvarchar](max) NULL,
	[ValueAfter] [nvarchar](max) NULL,
	[AuditActionType] [int] NULL,
	[DateTimeStamp] [datetime] NULL,
	[AuditActionID] [nvarchar](max) NOT NULL,
	[UserId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


USE [CreditInfoGroup_mt]
GO

/****** Object:  Table [dbo].[au_ProductToGroup]    Script Date: 15/02/2019 14:06:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[au_ProductToGroup](
	[GroupID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Created] [datetime] NULL
) ON [PRIMARY]
GO


---- new products
----102	Users Groups Listing	Users Groups	True      	/UserAdmin/UserGroupList.aspx	100	2004-07-13 00:00:00.000	True      	True      	NULL	NULL	True      	NULL	NULL
----20001	Users Group	Users Group	True      	/UserAdmin/UserGroup.aspx	100	2004-07-13 00:00:00.000	True      	False     	NULL	NULL	True     	NULL	NULL
---20002	Audit	Audit	True      	/audit/viewaudit.aspx	4000	2004-07-13 00:00:00.000	True      	True      	NULL	NULL	True      	NULL	NULL

---20003	Reports	Reports	True      	/reporting/PrintReport.aspx	4000	2004-07-13 00:00:00.000	True      	True      	NULL	NULL	True      	NULL	NULL

USE [CreditInfoGroup_mt]
GO

/****** Object:  Table [dbo].[au_UserGroups]    Script Date: 15/02/2019 14:47:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[au_UserGroups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [varchar](100) NOT NULL,
	[GroupCode] [varchar](100) NULL,
	[GroupDesc] [varchar](100) NULL
) ON [PRIMARY]
GO

ALTER TABLE NP_CLAIMS
  ADD DoNotDelete BIT;

  ALTER TABLE np_Individual
  ADD DoNotDelete BIT;

  ALTER TABLE np_Companys
  ADD DoNotDelete BIT;


ALTER TABLE au_users
  ADD GroupCode VARCHAR(50);



  ALTER TABLE np_Individual
  ADD IsPEP BIT;

  ALTER TABLE np_Individual
  ADD IsGDPRConsent BIT;




------ creation of archive table----------------

 IF NOT EXISTS (SELECT schema_name 
    FROM information_schema.schemata 
    WHERE schema_name = 'archive' )
BEGIN
    EXEC sp_executesql N'CREATE SCHEMA archive;';
END


SELECT TOP 0 *
INTO archive.np_Individual
FROM np_Individual


SELECT TOP 0 *
INTO archive.np_Employee
FROM np_Employee


SELECT TOP 0 *
INTO archive.np_Individual_FullName
FROM np_Individual_FullName


------------------------------------------------------------ 

SELECT TOP 0 *
INTO archive.np_PNumbers
FROM np_PNumbers

SELECT TOP 0 *
INTO archive.NP_ADDRESS
FROM NP_ADDRESS

SELECT TOP 0 *
INTO archive.NP_IDNUMBERS
FROM NP_IDNUMBERS

SELECT TOP 0 *
INTO archive.NP_NOTES
FROM NP_NOTES


----------------------------------------------------------- credit info archives
EXEC sp_fkeys 'NP_CREDITINFOUSER'

SELECT TOP 0 *
INTO archive.cpi_BoardMembers
FROM cpi_BoardMembers

SELECT TOP 0 *
INTO archive.cpi_CompanyReport
FROM cpi_CompanyReport

SELECT TOP 0 *
INTO archive.cpi_CompanyTradeTerms
FROM cpi_CompanyTradeTerms

SELECT TOP 0 *
INTO archive.cpi_Owners
FROM cpi_Owners

SELECT TOP 0 *
INTO archive.cpi_Subsidiaries
FROM cpi_Subsidiaries

SELECT TOP 0 *
INTO archive.FSI_TemplateGeneralInfo
FROM FSI_TemplateGeneralInfo

SELECT TOP 0 *
INTO archive.np_Claims
FROM np_Claims

SELECT TOP 0 *
INTO archive.np_Companys
FROM np_Companys

SELECT TOP 0 *
INTO archive.np_VatNumbers
FROM np_VatNumbers

SELECT TOP 0 *
INTO archive.ros_AdditionalCustomerInfo
FROM ros_AdditionalCustomerInfo

SELECT TOP 0 *
INTO archive.ros_ContactPerson
FROM ros_ContactPerson

SELECT TOP 0 *
INTO archive.ros_Order
FROM ros_Order





------- cpi_CompanyReport archives
EXEC sp_fkeys 'cpi_CompanyReport'


SELECT TOP 0 *
INTO archive.cpi_CompaniesCustomerType
FROM cpi_CompaniesCustomerType

SELECT TOP 0 *
INTO archive.cpi_CompaniesNaceCodes
FROM cpi_CompaniesNaceCodes

SELECT TOP 0 *
INTO archive.cpi_CompanyBanks
FROM cpi_CompanyBanks

SELECT TOP 0 *
INTO archive.cpi_CompanyNegativePayment
FROM cpi_CompanyNegativePayment

SELECT TOP 0 *
INTO archive.cpi_CompanyPrincipals
FROM cpi_CompanyPrincipals

SELECT TOP 0 *
INTO archive.cpi_ExportCountries
FROM cpi_ExportCountries

SELECT TOP 0 *
INTO archive.cpi_HistoryOperationReview
FROM cpi_HistoryOperationReview

SELECT TOP 0 *
INTO archive.cpi_ImportCountries
FROM cpi_ImportCountries

SELECT TOP 0 *
INTO archive.cpi_MarketingPosition
FROM cpi_MarketingPosition


SELECT TOP 0 *
INTO archive.cpi_RealEstates
FROM cpi_RealEstates

SELECT TOP 0 *
INTO archive.cpi_StaffCount
FROM cpi_StaffCount



