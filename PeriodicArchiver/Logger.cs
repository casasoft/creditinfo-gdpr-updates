﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cig.Framework.Base.Configuration;

namespace PeriodicArchiver
{
    class Logger
    {
        private static readonly string debugFilePath = CigConfig.Configure("lookupsettings.logFilePath") + @"\debug.txt";
        private static readonly string errorFilePath = CigConfig.Configure("lookupsettings.logFilePath") + @"\error.txt";

        private static readonly string saisWSFilePath = CigConfig.Configure("lookupsettings.logFilePath") +
                                                        @"\SAISWSLog.txt";


        /// <summary>
        /// Writes given error message to log along with error details from the exception
        /// </summary>
        /// <param name="input">The error message</param>
        /// <param name="err">The exception that occurred</param>
        /// <param name="error">If it is error or just some message</param>
        public static void WriteToLog(string input, Exception err, bool error) { WriteToLog(input + " -> " + err.Message + "-->" + err.StackTrace, error); }

        public static void WriteToLog(string input, bool error)
        {
            try
            {
                if (CigConfig.Configure("lookupsettings.rootURL") != null)
                {
                    input += "\r\n\r\n" + CigConfig.Configure("lookupsettings.rootURL");
                }
                var filePath = error ? errorFilePath : debugFilePath;

                var logFile = new FileInfo(filePath);

                if (logFile.Exists)
                {
                    if (logFile.Length >= 100000)
                    {
                        File.Delete(filePath);
                    }
                }

                var fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                var w = new StreamWriter(fs);
                w.BaseStream.Seek(0, SeekOrigin.End);

                w.Write("\nLog Entry : ");
                w.Write(
                    "{0} {1} \n\n",
                    DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());

                w.Write(input + "\n");
                w.Write("------------------------------------\n");

                w.Flush();

                w.Close();
            }
            finally
            {
            }
        }

    }
}
