﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeriodicArchiver
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length == 0) throw new Exception("months old arguement is required");

            int monthsOlder = Convert.ToInt32(args[0]);
            var archiveObject = new ArchiveData();
            archiveObject.DeleteAndArchiveClaims(monthsOlder);
        }
    }
}
