﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cig.Framework.Base.Configuration;

namespace PeriodicArchiver
{
    class ArchiveData
    {

        private static string connectionString()
        {


            //PH: new configuration used.
            string appSettingValue = CigConfig.Configure("hibernate.connection.connection_string");
            string connStr = "";
            if (appSettingValue.ToLower().StartsWith("decrypted:"))
            {
                connStr = appSettingValue.Substring(10);
            }

            return connStr;
        }



        private void deleteAndArchive(string tableName, string whereClause)
        {
            using (var myOleDbConn = new OleDbConnection(connectionString()))
            {
                myOleDbConn.Open();

                // begin transaction...
                var myTrans = myOleDbConn.BeginTransaction();

                try
                {
                    var myOleDbCommand =
                        new OleDbCommand(" ", myOleDbConn)
                        {
                            Transaction = myTrans
                        };
                    //myOleDbCommand.CommandText = "EXEC [dbo].[DeleteClaimRecordPeriodically]  @MonthsOlder = 240";

                    myOleDbCommand.CommandText = $@"IF NOT EXISTS(SELECT TABLE_NAME
                                                FROM information_schema.TABLES
                                            WHERE TABLE_SCHEMA = 'archive' and TABLE_NAME = '{tableName}')

                                            BEGIN
                                                EXEC sp_executesql N'SELECT TOP 0 * INTO archive.{tableName} FROM {tableName}';
                                            END";
                    myOleDbCommand.ExecuteNonQuery();



                    myOleDbCommand.CommandText = $@"declare @colsUnpivot nvarchar(MAX) 
                                                 declare @query nvarchar(MAX)
                                                 declare @incrementID nvarchar(30)
                                                 set  @incrementID = (select top 1 name from sys.identity_columns where OBJECT_NAME(object_id) = '{tableName}')

                                                 select @colsUnpivot = stuff((select DISTINCT ','+quotename(C.name) 
                                                    FROM sys.columns c JOIN sys.types y ON y.system_type_id = c.system_type_id
                                                    WHERE c.object_id = OBJECT_ID('{tableName}') and C.name != ISNULL(@incrementID, 'id')  and y.name NOT IN ('timestamp') 
                                                    for xml path('')), 1, 1, '') 

                                                 set @query  = 'INSERT INTO archive.{tableName}  ('+ @colsUnpivot +')  SELECT '+ @colsUnpivot +' 
                                                    FROM {tableName} WHERE {whereClause} ' 

                                                 exec sp_executesql @query; ";

                    myOleDbCommand.ExecuteNonQuery();


                    //deletion of the data
                    myOleDbCommand.CommandText = $"DELETE FROM {tableName} WHERE {whereClause} ";
                    myOleDbCommand.ExecuteNonQuery();


                    myTrans.Commit();

                }
                catch
                {
                    myTrans.Rollback();
                    throw;

                }
            }
        }



        public bool DeleteAndArchiveClaims(int monthsOlder)
        {
           
            try
            {
                if (monthsOlder < 1) throw new Exception("monthsOlder can not be less than 1");

                string whereClasue = $" ISNULL(np_claims.DoNotDelete , 0) = 0 AND DATEDIFF(MONTH, RegDate, GETDATE()) > {monthsOlder} ";

                deleteAndArchive("np_Claims", whereClasue);
                return true;

            }
            catch(Exception ex)
            {
                Logger.WriteToLog($"Error occured archiving np_claims table : ", ex, true);
                
                return false;

            }
        }




    }
}
