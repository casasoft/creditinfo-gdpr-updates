﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.OleDb;

using DataProtection;
using System.Security.Cryptography;
using System.IO;
using System.Threading;
using System.Configuration;

namespace MT.CIG.FullName.Parser
{
    #region TODO Duplicated CB3 Project
    public partial class TripleDesCrypto
    {
        private static byte[] key = { 60, 21, 217, 24, 4, 60, 58, 1, 208, 134, 95, 162, 108, 198, 34, 52, 57, 222, 248, 97, 48, 164, 24, 168 };
        private static byte[] iv = { 22, 153, 20, 175, 188, 5, 44, 80 };

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TripleDesCrypto()
        {
        }

        /// <summary>
        /// Encrypts given text
        /// </summary>
        /// <param name="dataToEncrypt">Text to encrypt</param>
        /// <returns>Encrypted text</returns>
        public static string Encrypt(string dataToEncrypt)
        {
            SymmetricAlgorithm mCryptoService = new TripleDESCryptoServiceProvider();

            //Create byte arrays to hold original, encrypted, and decrypted data.

            byte[] plainByte = Encoding.ASCII.GetBytes(dataToEncrypt);

            // Set private key
            mCryptoService.Key = key;
            mCryptoService.IV = iv;

            // Encryptor object
            ICryptoTransform cryptoTransform = mCryptoService.CreateEncryptor();

            // Memory stream object
            MemoryStream ms = new MemoryStream();

            // Crpto stream object
            CryptoStream cs = new CryptoStream(ms, cryptoTransform,
                CryptoStreamMode.Write);

            // Write encrypted byte to memory stream
            cs.Write(plainByte, 0, plainByte.Length);
            cs.FlushFinalBlock();

            // Get the encrypted byte length
            byte[] cryptoByte = ms.ToArray();

            return Convert.ToBase64String(cryptoByte);

        }

        /// <summary>
        /// Decrypts given text
        /// </summary>
        /// <param name="dataToDecrypt">Text to decrypt</param>
        /// <returns>Decrypted text</returns>
        public static string Decrypt(string dataToDecrypt)
        {
            SymmetricAlgorithm mCryptoService = new TripleDESCryptoServiceProvider();

            //Create byte arrays to hold original, encrypted, and decrypted data.
            byte[] plainByte = Convert.FromBase64String(dataToDecrypt);

            // Set private key
            mCryptoService.Key = key;
            mCryptoService.IV = iv;

            // Encryptor object
            ICryptoTransform cryptoTransform = mCryptoService.CreateDecryptor();

            // Memory stream object
            MemoryStream ms = new MemoryStream();

            // Crpto stream object
            CryptoStream cs = new CryptoStream(ms, cryptoTransform,
                CryptoStreamMode.Write);

            // Write encrypted byte to memory stream
            cs.Write(plainByte, 0, plainByte.Length);
            cs.FlushFinalBlock();

            // Get the encrypted byte length
            byte[] cryptoByte = ms.ToArray();

            return Encoding.ASCII.GetString(cryptoByte);

        }
    }

    public class DatabaseHelper
    {
        /// <summary>
        /// Return the Connection string from config file.
        /// </summary>
        /// <returns></returns>
        public static string ConnectionString()
        {
            bool supportApplicationCenter = false;
      
            //PH: new configuration used.
            string appSettingValue = new AppSettingsReader().GetValue("ConnectionString", typeof(string)).ToString();
            string connStr;
            if (appSettingValue.ToLower().StartsWith("decrypted:"))
            {
                connStr = appSettingValue.Substring(10);
            }
            else
            {
                if (supportApplicationCenter)
                {
                    connStr = TripleDesCrypto.Decrypt(appSettingValue);
                }
                else
                {
                    var dp = new DataProtector(DataProtector.Store.USE_MACHINE_STORE);
                    var dataToDecrypt = Convert.FromBase64String(appSettingValue);
                    connStr = Encoding.ASCII.GetString(dp.Decrypt(dataToDecrypt, null));
                }
            }

            return connStr;
        }
    }
    #endregion

    struct SubjectFullName
    {
        public string Surname;
        public string OthreSurname;
        public string Name;
        public string OtherName;
        public string MiddleNames;
        public int CreditInfoId;
    }

    class FullNameParser
    {
        string connectionString;
        int commantTimeOut = 0;

        AppSettingsReader settings = new AppSettingsReader();
       
        int batchSize = 1000;
        public FullNameParser()
        {
            try
            {
                batchSize = int.Parse(settings.GetValue("BatchSize", typeof(string)).ToString());
                commantTimeOut = int.Parse(settings.GetValue("CommandTimeout", typeof(string)).ToString());

            }
            catch { }
            OleDbConnectionStringBuilder conBuilder = new OleDbConnectionStringBuilder(DatabaseHelper.ConnectionString());
            var _sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
            _sqlConnectionStringBuilder.Password = (string)conBuilder["Password"];
            _sqlConnectionStringBuilder.UserID = (string)conBuilder["User Id"];
            _sqlConnectionStringBuilder.InitialCatalog = (string)conBuilder["Initial Catalog"];
            _sqlConnectionStringBuilder.DataSource = conBuilder.DataSource;
            _sqlConnectionStringBuilder.PersistSecurityInfo = true;
            connectionString = _sqlConnectionStringBuilder.ConnectionString;
        }

        bool LoadLookups()
        {
            string sql = "select count(*) from np_Individual_Fn_Lookup";
                
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    return  int.Parse(cmd.ExecuteScalar().ToString()) == 0;
                }
            }
        }

        void LoadNames()
        {
            string sql = "select distinct FirstNameNative from np_Individual where FirstNameNative is not null";

            Console.WriteLine("Load names...");
            
           
            List<string> surnames = new List<string>();
            List<string> names = new List<string>();

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    IDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        string name = Normalize(dr["FirstNameNative"]);
                        var splt = name.Split(' ');
                        if (splt.Count() > 1)
                        {

                            if (!surnames.Exists(x => x == splt[0]))
                            {
                                surnames.Add(splt[0]);
                            }
                            if(!names.Exists(x => x == splt[splt.Length - 1]));
                            {
                                names.Add(splt[splt.Length - 1]);
                            }
                        }
                    }
                }
            }

            SaveLookups(surnames, names);
            Console.WriteLine("Load names finished...");

        }

        private void SaveLookups(List<string> surnames, List<string> names)
        {
            string sql = "insert into np_Individual_Fn_Lookup (Name, Type) values  ('{0}', '{1}')";
            StringBuilder sb = new StringBuilder();
            foreach (var sn in surnames)
            {
                sb.AppendLine(string.Format(sql, Normalize(sn).Replace("'", "''"), 'S'));
            }
            foreach (var sn in names)
            {
                sb.AppendLine(string.Format(sql, Normalize(sn).Replace("'", "''"), 'N'));
            }
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sb.ToString(), conn))
                {
                    if (commantTimeOut != 0)
                        cmd.CommandTimeout = commantTimeOut;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private string Normalize(object value)
        {
            return value.ToString().Replace("(", "").Replace(")", "").Replace(",", "").Replace(".", "");
        }

       
        public void DoWork()
        {
            if(LoadLookups())
                LoadNames();
            UpdateIndividual();
        }

        private Queue<SubjectFullName> GetSubject(int lastId)
        {
            string sql = string.Format(@"select TOP {0} * from np_Individual where Processed = 0 
                and CreditInfoId > {1}
                order by CreditInfoId", batchSize, lastId);



//            var t = parse("Verbraeken Picus1 Jacobus1 Fikus Dikus Hendrikus", surnames, names);

            Queue<SubjectFullName> result = new Queue<SubjectFullName>();
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    IDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        string fisrtNameNative = dr["FirstNameNative"].ToString().Replace("(", " ").Replace(")", "").Replace(",", " ").Replace(".", " ");
                        var creditInfoId = int.Parse(dr["CreditInfoID"].ToString());
                        SubjectFullName fullName = Parse(fisrtNameNative);
                        fullName.CreditInfoId = creditInfoId;
                        result.Enqueue(fullName);
                    }
                }
            }
            return result;
        }

        private void UpdateIndividual()
        {

            var subjectList = GetSubject(0);
            int count = subjectList.Count;
            StringBuilder sb = new StringBuilder();
            while (subjectList.Count != 0)
            {
               
                var fullName = subjectList.Dequeue();
                sb.AppendLine(string.Format("exec ddd_UpdateIndividualFullName {0}, {1}, {2}, {3}, {4}, {5}",
                         fullName.CreditInfoId, FormatString(fullName.Surname),
                         FormatString(fullName.OthreSurname),
                         FormatString(fullName.Name),
                         FormatString(fullName.OtherName),
                         FormatString(fullName.MiddleNames)
                        ));
                if (subjectList.Count == 0)
                {
                    UpdatePersonalInfo(sb.ToString());
                    Console.WriteLine("Uploading: " + count.ToString());
                    subjectList = GetSubject(fullName.CreditInfoId);
                    count += subjectList.Count;
                    sb = new StringBuilder();
                }
            }
        }

        private string FormatString(string val)
        {
            return !string.IsNullOrEmpty(val) ? string.Format("N'{0}'", val.Replace("'", "''")) : "default";
        }

        private void UpdatePersonalInfo(string sql)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    if(commantTimeOut != 0)
                        cmd.CommandTimeout = commantTimeOut;
                    cmd.ExecuteNonQuery();
                }
            }
        }



        SubjectFullName Parse(string firstNameNative)
        {
            SubjectFullName fullName = new SubjectFullName();

            if (string.IsNullOrEmpty(firstNameNative))
                return fullName;

            var split = firstNameNative.Split(' ').ToList();
            fullName.Surname = split[0];

            if (!IsExists(fullName.Surname, 'S'))
            {
                SaveLookups(new List<string>(new string[] { fullName.Surname }), new List<string>());
            }

            split.RemoveAt(0);
            if (split.Count != 0)
            {
                fullName.Name = split[split.Count - 1];

                if (!IsExists(fullName.Name, 'N'))
                {
                    SaveLookups(new List<string>(), new List<string>(new string[] { fullName.Name }));
                }

                split.RemoveAt(split.Count - 1);

                if (split.Count != 0)
                {
                    var match = GetValue(split, 'S');
                    if (match != null)
                        fullName.OthreSurname = match;
                    else
                    {
                        fullName.MiddleNames = split[0];
                        split.RemoveAt(0);
                    }
                    match = GetValue(split, 'N');
                    if (match != null)
                        fullName.OtherName = match;
                    
                    if (split.Count != 0)
                    {
                        if(!string.IsNullOrEmpty(fullName.MiddleNames))
                            split.Insert(0, fullName.MiddleNames);
                        fullName.MiddleNames = string.Join(" ", split.ToArray() );
                    }
                }
            }

            return fullName;
        }


        private bool IsExists(string name, char type)
        {
            string sql = "select count(*) from np_Individual_Fn_Lookup where Name = @name and Type = @type";

             using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(string.Format(sql, name, type), conn))
                {
                    cmd.Parameters.Add(new SqlParameter("@name", name));
                    cmd.Parameters.Add(new SqlParameter("@type", type));

                    int result =  int.Parse(cmd.ExecuteScalar().ToString());
                    return result > 0;
                }
            }
        }

        string GetValue(List<string> splittedName, char type)
        {
            if (splittedName.Count != 0)
            {
                var middleName = splittedName[0].Replace(" ", "");
                if(IsExists(middleName, type))
                {
                    splittedName.RemoveAt(0);
                    return middleName;
                }
            }
            return null;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                new FullNameParser().DoWork();
            }
            catch (Exception ex)
            {
                WriteToLog(ex.Message, ex);
            }
        }

        public static void WriteToLog(string input, Exception err) { WriteToLog(input + Environment.NewLine + err.Message + Environment.NewLine + err.StackTrace); }

        public static void WriteToLog(string input)
        {
            try
            {
            
                var filePath = Environment.CurrentDirectory + @"\log.txt";

                var logFile = new FileInfo(filePath);

                if (logFile.Exists)
                {
                    if (logFile.Length >= 100000)
                    {
                        File.Delete(filePath);
                    }
                }

                var fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                var w = new StreamWriter(fs);
                w.BaseStream.Seek(0, SeekOrigin.End);

                w.Write(Environment.NewLine);

                w.Write("\nLog Entry : ");
                w.Write(
                    "{0} {1}",
                    DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());

             
                w.Write(input);

                w.Write(Environment.NewLine);
                w.Write("------------------------------------");
                w.Write(Environment.NewLine);

                w.Flush();

                w.Close();
            }
            finally
            {
                
            }
        }
    }
}
