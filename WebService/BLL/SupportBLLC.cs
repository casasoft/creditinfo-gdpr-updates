﻿using DataProtection;
using System;
using System.Text;
using System.Web.Mail;

namespace WebService.BLL
{
    public class SupportBLLC
    {
        #region Constructors

        public SupportBLLC()
        {
        }

        #endregion

        #region Methods

        public string Encrypt(string UncryptedString)
        {
            byte[] byteArray1;
            string string1;
            Exception exception1;
            string string2;
            DataProtector dataProtector1 = new DataProtector(DataProtection.DataProtector.Store.USE_MACHINE_STORE);
            try
            {
                byteArray1 = Encoding.ASCII.GetBytes(UncryptedString);
                string1 = Convert.ToBase64String(dataProtector1.Encrypt(byteArray1, ((byte[])null)));
                string2 = string1;
            }
            catch (Exception exception2)
            {
                exception1 = exception2;
                throw exception1;
            }
            return string2;
        }

        public void SendMail(string mailText, string mailSubject)
        {
            MailMessage mailMessage1 = new MailMessage();
            string string1 = mailText;
            mailMessage1.From = "error@lt.is";
            mailMessage1.To = "error@lt.is";
            mailMessage1.Subject = mailSubject;
            mailMessage1.Priority = MailPriority.High;
            mailMessage1.BodyFormat = MailFormat.Text;
            mailMessage1.Body = string1;
            SmtpMail.SmtpServer = "hermes.lt.is";
            SmtpMail.Send(mailMessage1);
        }

        #endregion
    }
}
