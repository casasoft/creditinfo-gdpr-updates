﻿using System;

namespace WebService.BLL
{
    public class User
    {
        #region Fields
        private int cntCreditWatch;
        private DateTime created;
        private int creditInfoID;
        private string email;
        private string groups;
        private string hasWebServices;
        private int id;
        private string isOpen;
        private int nationalID;
        private DateTime openUntil;
        private string passwordHash;
        private int registeredBy;
        private string salt;
        private int subscriberID;
        private DateTime updated;
        private string userName;
        private string userType;
        #endregion

        #region Constructors

        public User()
        {
        }

        #endregion

        #region Properties

        public int CntCreditWatch
        {
            get
            {
                return this.cntCreditWatch;
            }
            set
            {
                this.cntCreditWatch = value;
            }
        }


        public DateTime Created
        {
            get
            {
                return this.created;
            }
            set
            {
                this.created = value;
            }
        }


        public int CreditInfoID
        {
            get
            {
                return this.creditInfoID;
            }
            set
            {
                this.creditInfoID = value;
            }
        }


        public string Email
        {
            get
            {
                return this.email;
            }
            set
            {
                this.email = value;
            }
        }


        public string Groups
        {
            get
            {
                return this.groups;
            }
            set
            {
                this.groups = value;
            }
        }


        public string HasWebServices
        {
            get
            {
                return this.hasWebServices;
            }
            set
            {
                this.hasWebServices = value;
            }
        }


        public int ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }


        public string IsOpen
        {
            get
            {
                return this.isOpen;
            }
            set
            {
                this.isOpen = value;
            }
        }


        public int NationalID
        {
            get
            {
                return this.nationalID;
            }
            set
            {
                this.nationalID = value;
            }
        }


        public DateTime OpenUntil
        {
            get
            {
                return this.openUntil;
            }
            set
            {
                this.openUntil = value;
            }
        }


        public string PasswordHash
        {
            get
            {
                return this.passwordHash;
            }
            set
            {
                this.passwordHash = value;
            }
        }


        public int RegisteredBy
        {
            get
            {
                return this.registeredBy;
            }
            set
            {
                this.registeredBy = value;
            }
        }


        public string Salt
        {
            get
            {
                return this.salt;
            }
            set
            {
                this.salt = value;
            }
        }


        public int SubscriberID
        {
            get
            {
                return this.subscriberID;
            }
            set
            {
                this.subscriberID = value;
            }
        }


        public DateTime Updated
        {
            get
            {
                return this.updated;
            }
            set
            {
                this.updated = value;
            }
        }


        public string UserName
        {
            get
            {
                return this.userName;
            }
            set
            {
                this.userName = value;
            }
        }


        public string UserType
        {
            get
            {
                return this.userType;
            }
            set
            {
                this.userType = value;
            }
        }

        #endregion
    }
}
