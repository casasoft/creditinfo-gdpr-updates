﻿using WebService.DAL;
using System.Data;
using System;

namespace WebService.BLL
{
    public class DddService
    {
        #region Fields
        private dddDALC mydddDALC;
        private SupportBLLC mySupportBLLC;
        private SupportDALC mySupportDALC;
        #endregion

        #region Constructors

        public DddService()
        {
            this.mySupportBLLC = new SupportBLLC();
            this.mySupportDALC = new SupportDALC();
            this.mydddDALC = new dddDALC();
        }

        #endregion

        #region Methods

        public void DeleteTemporaryUsage(int Record)
        {
            this.mySupportDALC.DeleteTemporaryUsage(Record);
        }

        public string Encrypt(string UncryptedString)
        {
            return this.mySupportBLLC.Encrypt(UncryptedString);
        }

        public DataSet GetDebtData(string Query, int ReportID)
        {
            return this.mydddDALC.GetDebtData(Query, ReportID);
        }

        public DataSet GetDebtDataWitFullName(string Query, int ReportID)
        {
            return this.mydddDALC.GetDebtDataWithFullName(Query, ReportID);
        }

        public DataSet GetDebtDataOrNational(string Query, int ReportID)
        {
            return this.mydddDALC.GetDebtDataOrNational(Query, ReportID);
        }

        public User Login(ref string UserName, string Password, int Service)
        {
            return this.mySupportDALC.Login(ref UserName, Password, Service);
        }

        public int LogTemporaryUsage(int userid)
        {
            return this.mySupportDALC.LogTemporaryUsage(userid);
        }

        public void SendMail(string mailText, string mailSubject)
        {
            this.mySupportBLLC.SendMail(mailText, mailSubject);
        }

        public void UpdateUsage(int ReportID, int CreditinfoID, string QuerySubject, int ResultCount, string IP)
        {
            this.mySupportDALC.UpdateUsage(ReportID, CreditinfoID, QuerySubject, ResultCount, IP);
        }

        #endregion

      
    }
}
