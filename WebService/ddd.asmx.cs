﻿using WebService.BLL;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Xml;

using Cig.Framework.Base.Configuration;

namespace WebService
{
    /// <summary>
    /// Summary description for ddd
    /// </summary>
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    [WebServiceAttribute(Description = "CreditInfoGroup - DDD query Web service for the CIG Suite.", Namespace = "CIG_DDD")]
    public class ddd : System.Web.Services.WebService
    {

        #region Fields
        private IContainer components;
        private static DddService service;
        #endregion

        #region Constructors

        public ddd()
        {
            this.components = ((IContainer)null);
            this.InitializeComponent();
        }


        static ddd()
        {
            ddd.service = new DddService();
        }

        #endregion

        #region Methods

        [SoapDocumentMethodAttribute(ResponseElementName = "SSN2DDDFULL")]
        [WebMethodAttribute(Description = "Send username, password and query SSN for DDD query. Returns an XML node")]
        public XmlNode DDDRequestWithFullName(string UserName, string Password, string Query)
        {
            DataSet dataSet1;
            Exception exception1;
            Exception exception2;
            Exception exception3;
            XmlDocument xmlDocument1;
            Exception exception4;
            XmlNode xmlNode1;
            int i1 = Convert.ToInt32(CigConfig.Configure("lookupsettings.DDDServiceKey"));

            User user1 = this.CheckLogin(ref UserName, Password, i1);
            int i2 = -1;
            try
            {
                i2 = ddd.service.LogTemporaryUsage(user1.ID);
            }
            catch (Exception exception5)
            {
                exception1 = exception5;
                throw new SoapException(("Could not create temporary log transaction (DDDRequest) : " + exception1.Message), SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception1);
            }
            if (Query.ToUpper().StartsWith("C") && (Query.IndexOf(" ") == -1))
            {
                Query = Query.Insert(1, " ");
            }
            try
            {
                dataSet1 = ddd.service.GetDebtDataWitFullName(Query, i2);
            }
            catch (Exception exception6)
            {
                exception2 = exception6;
                ddd.service.DeleteTemporaryUsage(i2);
                throw new SoapException(("Could not retrive any data (DDDRequest) : " + exception2.Message), SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception2);
            }
            try
            {
                ddd.service.UpdateUsage(i2, user1.CreditInfoID, Query, dataSet1.Tables["records"].Rows.Count, base.Context.Request.UserHostAddress);
            }
            catch (Exception exception7)
            {
                exception3 = exception7;
                ddd.service.DeleteTemporaryUsage(i2);
                throw new SoapException(("Could not log usage (LogUsage): " + exception3.Message), SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception3);
            }
            try
            {
                if (dataSet1.Tables["records"].Rows.Count > 0)
                {
                    return ((XmlNode)new XmlDataDocument(dataSet1));
                }
                else
                {
                    xmlDocument1 = new XmlDocument();
                    xmlDocument1.LoadXml(dataSet1.GetXmlSchema());
                    xmlNode1 = ((XmlNode)xmlDocument1);
                }
            }
            catch (Exception exception8)
            {
                exception4 = exception8;
                throw new SoapException("No records found (DDDRequest): ", SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception4);
            }
            return xmlNode1;
        }

        [SoapDocumentMethodAttribute(ResponseElementName = "SSN2DDD")]
        [WebMethodAttribute(Description = "Send username, password and query SSN for DDD query. Returns an XML node")]
        public XmlNode DDDRequest(string UserName, string Password, string Query)
        {
            DataSet dataSet1;
            Exception exception1;
            Exception exception2;
            Exception exception3;
            XmlDocument xmlDocument1;
            Exception exception4;
            XmlNode xmlNode1;
            int i1 =  Convert.ToInt32(CigConfig.Configure("lookupsettings.DDDServiceKey"));
            
            User user1 = this.CheckLogin(ref UserName, Password, i1);
            int i2 = -1;
            try
            {
                i2 = ddd.service.LogTemporaryUsage(user1.ID);
            }
            catch (Exception exception5)
            {
                exception1 = exception5;
                throw new SoapException(("Could not create temporary log transaction (DDDRequest) : " + exception1.Message), SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception1);
            }
            if (Query.ToUpper().StartsWith("C") && (Query.IndexOf(" ") == -1))
            {
                Query = Query.Insert(1, " ");
            }
            try
            {
                dataSet1 = ddd.service.GetDebtData(Query, i2);
            }
            catch (Exception exception6)
            {
                exception2 = exception6;
                ddd.service.DeleteTemporaryUsage(i2);
                throw new SoapException(("Could not retrive any data (DDDRequest) : " + exception2.Message), SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception2);
            }
            try
            {
                ddd.service.UpdateUsage(i2, user1.CreditInfoID, Query, dataSet1.Tables["records"].Rows.Count, base.Context.Request.UserHostAddress);
            }
            catch (Exception exception7)
            {
                exception3 = exception7;
                ddd.service.DeleteTemporaryUsage(i2);
                throw new SoapException(("Could not log usage (LogUsage): " + exception3.Message), SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception3);
            }
            try
            {
                if (dataSet1.Tables["records"].Rows.Count > 0)
                {
                    return ((XmlNode)new XmlDataDocument(dataSet1));
                }
                else
                {
                    xmlDocument1 = new XmlDocument();
                    xmlDocument1.LoadXml(dataSet1.GetXmlSchema());
                    xmlNode1 = ((XmlNode)xmlDocument1);
                }
            }
            catch (Exception exception8)
            {
                exception4 = exception8;
                throw new SoapException("No records found (DDDRequest): ", SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception4);
            }
            return xmlNode1;
        }


        [SoapDocumentMethodAttribute(ResponseElementName = "SSN2DDDORNATIONAL")]
        [WebMethodAttribute(Description = "Send username, password and query SSN for DDD or National Registry query (np_Com"
             + "panies, np_Individuals). Returns an XML node")]
        public XmlNode DDDRequestNationalFocus(string UserName, string Password, string Query)
        {
            DataSet dataSet1;
            Exception exception1;
            Exception exception2;
            Exception exception3;
            XmlDocument xmlDocument1;
            Exception exception4;
            XmlNode xmlNode1;
            int i1 = Convert.ToInt32(CigConfig.Configure("lookupsettings.DDDServiceKey"));
            User user1 = this.CheckLogin(ref UserName, Password, i1);
            int i2 = -1;
            try
            {
                i2 = ddd.service.LogTemporaryUsage(user1.ID);
            }
            catch (Exception exception5)
            {
                exception1 = exception5;
                throw new SoapException(("Could not create temporary log transaction (DDDRequest) : " + exception1.Message), SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception1);
            }
            if (Query.ToUpper().StartsWith("C") && (Query.IndexOf(" ") == -1))
            {
                Query = Query.Insert(1, " ");
            }
            try
            {
                dataSet1 = ddd.service.GetDebtDataOrNational(Query, i2);
            }
            catch (Exception exception6)
            {
                exception2 = exception6;
                ddd.service.DeleteTemporaryUsage(i2);
                throw new SoapException(("Could not retrive any data (DDDRequest) : " + exception2.Message), SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception2);
            }
            try
            {
                ddd.service.UpdateUsage(i2, user1.CreditInfoID, Query, dataSet1.Tables["records"].Rows.Count, base.Context.Request.UserHostAddress);
            }
            catch (Exception exception7)
            {
                exception3 = exception7;
                ddd.service.DeleteTemporaryUsage(i2);
                throw new SoapException(("Could not log usage (LogUsage): " + exception3.Message), SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception3);
            }
            try
            {
                if (dataSet1.Tables["records"].Rows.Count > 0)
                {
                    return ((XmlNode)new XmlDataDocument(dataSet1));
                }
                else
                {
                    xmlDocument1 = new XmlDocument();
                    xmlDocument1.LoadXml(dataSet1.GetXmlSchema());
                    xmlNode1 = ((XmlNode)xmlDocument1);
                }
            }
            catch (Exception exception8)
            {
                exception4 = exception8;
                throw new SoapException("No records found (DDDRequestNationalFocus): ", SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, exception4);
            }
            return xmlNode1;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private User CheckLogin(ref string UserName, string Password, int Service)
        {
            User user1 = ddd.service.Login(ref UserName, Password, Service);
            if (user1 != null)
            {
                return user1;
            }
            UnauthorizedAccessException unauthorizedAccessException1 = new UnauthorizedAccessException("login failed");
            unauthorizedAccessException1.Source = "Login(UserName, Password)";
            throw new SoapException("Login failed", SoapException.ClientFaultCode, base.Context.Request.Url.AbsoluteUri, ((Exception)unauthorizedAccessException1));
        }

        private void InitializeComponent()
        {
        }

        #endregion
    }
}
