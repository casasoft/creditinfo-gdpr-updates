﻿using DataProtection;
using System.Collections.Specialized;
using System.Configuration;
using System;
using System.Text;

using Cig.Framework.Base.Configuration;

namespace WebService.DAL
{
    public class BaseDALC
    {
        #region Constructors

        public BaseDALC()
        {
        }

        #endregion

        #region Methods

        protected string ConnectionString()
        {
            DataProtector dataProtector1 = new DataProtector(DataProtection.DataProtector.Store.USE_MACHINE_STORE);
            string string1 = CigConfig.Configure("hibernate.connection.WSconnectionString");
            if (string1.Trim().StartsWith("Decrypted:"))
            {
                return string1.Replace("Decrypted:", "");
            }
            byte[] byteArray1 = Convert.FromBase64String(string1);
            return Encoding.ASCII.GetString(dataProtector1.Decrypt(byteArray1, ((byte[])null)));
        }

        #endregion
    }
}
