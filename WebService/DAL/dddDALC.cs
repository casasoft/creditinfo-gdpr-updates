﻿using System.Data.Common;
using System.Data.OleDb;
using System.Data;
using System;

namespace WebService.DAL
{
    public class dddDALC : BaseDALC
    {

        #region Constructors

        public dddDALC()
        {
        }

        #endregion

        #region Methods


        public DataSet GetDebtDataWithFullName(string Query, int ReportID)
        {
            OleDbDataAdapter oleDbDataAdapter1;
            string string1;
            OleDbParameter oleDbParameter1;
            DataSet dataSet1 = new DataSet("ddd");
            using (OleDbConnection oleDbConnection1 = new OleDbConnection(base.ConnectionString()))
            {
                oleDbDataAdapter1 = new OleDbDataAdapter();
                oleDbConnection1.Open();
                string1 = ("SELECT *, GETDATE() AS [reportdate]," + ReportID.ToString() + " AS [report_id] FROM ws_ddd_by_id_with_fullName (?) ORDER BY issuedate DESC");
                oleDbParameter1 = new OleDbParameter("idnumber", OleDbType.WChar, 100);
                oleDbParameter1.Value = Query;
                oleDbParameter1.Direction = ParameterDirection.Input;
                oleDbDataAdapter1.SelectCommand = new OleDbCommand(string1, oleDbConnection1);
                OleDbParameter oleDbParameter2 = oleDbDataAdapter1.SelectCommand.Parameters.Add(oleDbParameter1);
                oleDbDataAdapter1.ContinueUpdateOnError = true;
                int i1 = oleDbDataAdapter1.Fill(dataSet1, "records");
            }
            return dataSet1;
        }

        public DataSet GetDebtData(string Query, int ReportID)
        {
            OleDbDataAdapter oleDbDataAdapter1;
            string string1;
            OleDbParameter oleDbParameter1;
            DataSet dataSet1 = new DataSet("ddd");
            using (OleDbConnection oleDbConnection1 = new OleDbConnection(base.ConnectionString()))
            {
                oleDbDataAdapter1 = new OleDbDataAdapter();
                oleDbConnection1.Open();
                string1 = ("SELECT *, GETDATE() AS [reportdate]," + ReportID.ToString() + " AS [report_id] FROM ws_ddd_by_id (?) ORDER BY issuedate DESC");
                oleDbParameter1 = new OleDbParameter("idnumber", OleDbType.WChar, 100);
                oleDbParameter1.Value = Query;
                oleDbParameter1.Direction = ParameterDirection.Input;
                oleDbDataAdapter1.SelectCommand = new OleDbCommand(string1, oleDbConnection1);
                OleDbParameter oleDbParameter2 = oleDbDataAdapter1.SelectCommand.Parameters.Add(oleDbParameter1);
                oleDbDataAdapter1.ContinueUpdateOnError = true;
                int i1 = oleDbDataAdapter1.Fill(dataSet1, "records");
            }
            return dataSet1;
        }

        public DataSet GetDebtDataOrNational(string Query, int ReportID)
        {
            OleDbDataAdapter oleDbDataAdapter1;
            string string1;
            OleDbParameter oleDbParameter1;
            DataSet dataSet1 = new DataSet("ddd");
            using (OleDbConnection oleDbConnection1 = new OleDbConnection(base.ConnectionString()))
            {
                oleDbDataAdapter1 = new OleDbDataAdapter();
                oleDbConnection1.Open();
                string1 = ("SELECT *, GETDATE() AS [reportdate]," + ReportID.ToString() + " AS [report_id]  FROM ws_all_by_id (?) ORDER BY issuedate DESC");
                oleDbParameter1 = new OleDbParameter("idnumber", OleDbType.WChar, 100);
                oleDbParameter1.Value = Query;
                oleDbParameter1.Direction = ParameterDirection.Input;
                oleDbDataAdapter1.SelectCommand = new OleDbCommand(string1, oleDbConnection1);
                OleDbParameter oleDbParameter2 = oleDbDataAdapter1.SelectCommand.Parameters.Add(oleDbParameter1);
                oleDbDataAdapter1.ContinueUpdateOnError = true;
                int i1 = oleDbDataAdapter1.Fill(dataSet1, "records");
            }
            return dataSet1;
        }

        #endregion
    }

}