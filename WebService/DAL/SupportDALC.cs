﻿using WebService.BLL;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System;
using System.Web.Security;

namespace WebService.DAL
{
    public class SupportDALC : BaseDALC
    {

        #region Fields
        private SupportBLLC mySupport;
        #endregion

        #region Constructors

        public SupportDALC()
        {
            this.mySupport = new SupportBLLC();
        }

        #endregion

        #region Methods

        public void DeleteTemporaryUsage(int Record)
        {
            OleDbParameter oleDbParameter1;
            Exception exception1;
            OleDbCommand oleDbCommand1 = new OleDbCommand();
            using (OleDbConnection oleDbConnection1 = new OleDbConnection(base.ConnectionString()))
            {
                oleDbConnection1.Open();
                try
                {
                    oleDbCommand1 = new OleDbCommand("DELETE FROM dbo.np_Usage WHERE dbo.np_Usage.id = ?", oleDbConnection1);
                    oleDbParameter1 = new OleDbParameter("id", OleDbType.Integer);
                    oleDbParameter1.Direction = ParameterDirection.Input;
                    oleDbParameter1.Value = Record;
                    OleDbParameter oleDbParameter2 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                    int i1 = oleDbCommand1.ExecuteNonQuery();
                }
                catch (Exception exception2)
                {
                    exception1 = exception2;
                    throw new Exception(("Connecting to SQL server. <br> Ex: " + exception1.Message));
                }
            }
        }

        public User GetSpecificUser(string Username)
        {
            OleDbCommand oleDbCommand1;
            OleDbParameter oleDbParameter1;
            OleDbDataReader oleDbDataReader1;
            User user1;
            User user2;
            string string1 = base.ConnectionString();
            using (OleDbConnection oleDbConnection1 = new OleDbConnection(string1))
            {
                oleDbConnection1.Open();
                oleDbCommand1 = new OleDbCommand("SELECT * FROM au_Users WHERE au_Users.UserName = ?", oleDbConnection1);
                oleDbParameter1 = new OleDbParameter("UserName", OleDbType.WChar);
                oleDbParameter1.Value = Username;
                oleDbParameter1.Direction = ParameterDirection.Input;
                OleDbParameter oleDbParameter2 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                oleDbDataReader1 = oleDbCommand1.ExecuteReader();
                user1 = new User();
                if (oleDbDataReader1.Read())
                {
                    user1.ID = oleDbDataReader1.GetInt32(0);
                    user1.CreditInfoID = oleDbDataReader1.GetInt32(1);
                    user1.UserName = oleDbDataReader1.GetString(2);
                    if (!oleDbDataReader1.IsDBNull(3))
                    {
                        user1.SubscriberID = oleDbDataReader1.GetInt32(3);
                    }
                    if (!oleDbDataReader1.IsDBNull(4))
                    {
                        user1.NationalID = oleDbDataReader1.GetInt32(4);
                    }
                    if (!oleDbDataReader1.IsDBNull(5))
                    {
                        user1.Email = oleDbDataReader1.GetString(5);
                    }
                    if (!oleDbDataReader1.IsDBNull(6))
                    {
                        user1.PasswordHash = oleDbDataReader1.GetString(6);
                    }
                    if (!oleDbDataReader1.IsDBNull(7))
                    {
                        user1.Salt = oleDbDataReader1.GetString(7);
                    }
                    if (!oleDbDataReader1.IsDBNull(8))
                    {
                        user1.Groups = oleDbDataReader1.GetString(8);
                    }
                    if (!oleDbDataReader1.IsDBNull(9))
                    {
                        user1.UserType = oleDbDataReader1.GetString(9);
                    }
                    if (!oleDbDataReader1.IsDBNull(10))
                    {
                        user1.CntCreditWatch = oleDbDataReader1.GetInt32(10);
                    }
                    if (!oleDbDataReader1.IsDBNull(11))
                    {
                        user1.RegisteredBy = oleDbDataReader1.GetInt32(11);
                    }
                    if (!oleDbDataReader1.IsDBNull(12))
                    {
                        user1.HasWebServices = oleDbDataReader1.GetString(12);
                    }
                    if (!oleDbDataReader1.IsDBNull(13))
                    {
                        user1.IsOpen = oleDbDataReader1.GetString(13);
                    }
                    if (!oleDbDataReader1.IsDBNull(14))
                    {
                        user1.OpenUntil = oleDbDataReader1.GetDateTime(14);
                    }
                    if (!oleDbDataReader1.IsDBNull(15))
                    {
                        user1.Created = oleDbDataReader1.GetDateTime(15);
                    }
                    if (!oleDbDataReader1.IsDBNull(16))
                    {
                        user1.Updated = oleDbDataReader1.GetDateTime(16);
                    }
                    oleDbDataReader1.Close();
                    return user1;
                }
                else
                {
                    user2 = ((User)null);
                }
            }
            return user2;
        }

        public bool IsSubscriberOpen(int UserID)
        {
            OleDbParameter oleDbParameter1;
            OleDbDataReader oleDbDataReader1;
            Exception exception1;
            bool b1 = false;
            OleDbCommand oleDbCommand1 = new OleDbCommand();
            using (OleDbConnection oleDbConnection1 = new OleDbConnection(base.ConnectionString()))
            {
                oleDbConnection1.Open();
                try
                {
                    oleDbCommand1 = new OleDbCommand("SELECT dbo.au_Subscribers.IsOpen FROM dbo.au_Subscribers INNER JOIN dbo.au_users"
                        + " ON dbo.au_Subscribers.CreditInfoID = dbo.au_users.SubscriberID WHERE dbo.au_use"
                        + "rs.id = ?", oleDbConnection1);
                    oleDbParameter1 = new OleDbParameter("userid", OleDbType.Integer);
                    oleDbParameter1.Direction = ParameterDirection.Input;
                    oleDbParameter1.Value = UserID;
                    OleDbParameter oleDbParameter2 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                    oleDbDataReader1 = oleDbCommand1.ExecuteReader();
                    if (oleDbDataReader1.Read())
                    {
                        b1 = Convert.ToBoolean(oleDbDataReader1.GetString(0));
                    }
                    oleDbDataReader1.Close();
                }
                catch (Exception exception2)
                {
                    exception1 = exception2;
                    throw new Exception(("Connecting to SQL server. <br> Ex: " + exception1.Message));
                }
            }
            return b1;
        }

        public User Login(ref string UserName, string Password, int Service)
        {
            if (UserName.Length <= 0)
            {
                return null;
            }
            if (Password.Length <= 0)
            {
                return null;
            }
            if (!this.VerifyPassword(UserName, Password))
            {
                return null;
            }
            User user1 = this.GetSpecificUser(UserName);
            if (user1 == null)
            {
                return null;
            }
            else if (!Convert.ToBoolean(user1.IsOpen))
            {
                return null;
            }
            else if (user1.OpenUntil < DateTime.Now)
            {
                return null;
            }
            else if (this.IsSubscriberOpen(user1.ID))
            {
                return user1;
            }
            else
            {
                return null;
            }
        }

        public int LogTemporaryUsage(int UserId)
        {
            int i1;
            OleDbCommand oleDbCommand1;
            OleDbParameter oleDbParameter1;
            using (OleDbConnection oleDbConnection1 = new OleDbConnection(base.ConnectionString()))
            {
                oleDbConnection1.Open();
                oleDbCommand1 = new OleDbCommand();
                oleDbCommand1.Connection = oleDbConnection1;
                oleDbCommand1.CommandText = "AddTemporaryUsage";
                oleDbCommand1.CommandType = CommandType.StoredProcedure;
                oleDbParameter1 = new OleDbParameter("userid", OleDbType.Integer);
                oleDbParameter1.Value = UserId;
                oleDbParameter1.Direction = ParameterDirection.Input;
                OleDbParameter oleDbParameter2 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                oleDbParameter1 = new OleDbParameter("@ReturnId", OleDbType.Integer);
                oleDbParameter1.Direction = ParameterDirection.Output;
                OleDbParameter oleDbParameter3 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                int i2 = oleDbCommand1.ExecuteNonQuery();
                oleDbConnection1.Close();
                i1 = ((int)oleDbCommand1.Parameters["@ReturnId"].Value);
            }
            return i1;
        }

        public void UpdateUsage(int ReportID, int CreditinfoID, string QuerySubject, int ResultCount, string IP)
        {
            OleDbCommand oleDbCommand1;
            OleDbTransaction oleDbTransaction1;
            OleDbParameter oleDbParameter1;
            Exception exception1;
            string string1;
            string string2;
            int i1 = 1010;
            using (OleDbConnection oleDbConnection1 = new OleDbConnection(base.ConnectionString()))
            {
                oleDbConnection1.Open();
                oleDbTransaction1 = oleDbConnection1.BeginTransaction();
                try
                {
                    oleDbCommand1 = new OleDbCommand("UPDATE np_Usage SET Creditinfoid = ?, query_type = ?, query = ?, result_count = "
                        + "?, ip = ? WHERE ID = ?", oleDbConnection1);
                    oleDbCommand1.Transaction = oleDbTransaction1;
                    oleDbParameter1 = new OleDbParameter("Creditinfoid", OleDbType.Integer);
                    oleDbParameter1.Value = CreditinfoID;
                    oleDbParameter1.Direction = ParameterDirection.Input;
                    OleDbParameter oleDbParameter2 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                    oleDbParameter1 = new OleDbParameter("query_type", OleDbType.Integer);
                    oleDbParameter1.Value = i1;
                    oleDbParameter1.Direction = ParameterDirection.Input;
                    OleDbParameter oleDbParameter3 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                    oleDbParameter1 = new OleDbParameter("query", OleDbType.WChar);
                    oleDbParameter1.Value = QuerySubject;
                    oleDbParameter1.Direction = ParameterDirection.Input;
                    OleDbParameter oleDbParameter4 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                    oleDbParameter1 = new OleDbParameter("result_count", OleDbType.Integer);
                    oleDbParameter1.Value = ResultCount;
                    oleDbParameter1.Direction = ParameterDirection.Input;
                    OleDbParameter oleDbParameter5 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                    oleDbParameter1 = new OleDbParameter("ip", OleDbType.VarChar);
                    oleDbParameter1.Value = IP;
                    oleDbParameter1.Direction = ParameterDirection.Input;
                    OleDbParameter oleDbParameter6 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                    oleDbParameter1 = new OleDbParameter("ID", OleDbType.Integer);
                    oleDbParameter1.Value = ReportID;
                    oleDbParameter1.Direction = ParameterDirection.Input;
                    OleDbParameter oleDbParameter7 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                    int i2 = oleDbCommand1.ExecuteNonQuery();
                    oleDbTransaction1.Commit();
                }
                catch (Exception exception2)
                {
                    exception1 = exception2;
                    oleDbTransaction1.Rollback();
                    string1 = ("Error updating log table (ReportID): " + ReportID.ToString());
                    string2 = "ddd.asmx (CIG) Error";
                    this.mySupport.SendMail(string1, string2);
                    throw exception1;
                }
            }
        }

        public bool VerifyPassword(string Username, string Password)
        {
            string string1;
            string string2;
            OleDbParameter oleDbParameter1;
            OleDbDataReader oleDbDataReader1;
            string string3;
            string string4;
            Exception exception1;
            bool b1 = false;
            OleDbCommand oleDbCommand1 = new OleDbCommand();
            using (OleDbConnection oleDbConnection1 = new OleDbConnection(base.ConnectionString()))
            {
                oleDbConnection1.Open();
                string1 = "";
                string2 = "";
                try
                {
                    oleDbCommand1 = new OleDbCommand("SELECT PasswordHash, salt FROM au_users WHERE UserName = ?", oleDbConnection1);
                    oleDbParameter1 = new OleDbParameter("username", OleDbType.WChar);
                    oleDbParameter1.Direction = ParameterDirection.Input;
                    oleDbParameter1.Value = Username;
                    OleDbParameter oleDbParameter2 = oleDbCommand1.Parameters.Add(oleDbParameter1);
                    oleDbDataReader1 = oleDbCommand1.ExecuteReader();
                    if (oleDbDataReader1.Read())
                    {
                        string1 = oleDbDataReader1.GetString(0);
                        string2 = oleDbDataReader1.GetString(1);
                    }
                    oleDbDataReader1.Close();
                    string3 = (Password + string2);
                    string4 = FormsAuthentication.HashPasswordForStoringInConfigFile(string3, "SHA1");
                    b1 = string4.Equals(string1);
                }
                catch (Exception exception2)
                {
                    exception1 = exception2;
                    throw new Exception(("Connecting to SQL server. <br> Ex: " + exception1.Message));
                }
            }
            return b1;
        }

        #endregion
    }

}
